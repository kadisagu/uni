package ru.tandemservice.uniusaca.migration;

import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusaca_2x4x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unisession отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
            ModuleMeta deletedModule;
            try
            {
                deletedModule = ApplicationRuntime.getModuleMeta("unisession");
            }
            catch (RuntimeException e)
            {
                deletedModule = null;
            }

			if( deletedModule!=null )
			{
				ModuleMeta mainModule = ApplicationRuntime.getModuleMeta("uniusaca");
				if( mainModule.getDependencyList().contains(deletedModule) )
					throw new RuntimeException("Module 'unisession' is not deleted");
			}
		}

		// удалить персистентный интерфейс ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed
		{
			// удалить view
			tool.dropView("isessionstudentnotallowed_v");

		}

		// удалить сущность unisessionCommonTemplate
		{
			// удалить таблицу
			tool.dropTable("session_template_common_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionCommonTemplate");

		}

		// удалить сущность unisessionBulletinTemplate
		{
			// удалить таблицу
			tool.dropTable("session_template_bulletin_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionBulletinTemplate");

		}

		// удалить сущность unisessionTemplate
		{
			// удалить таблицу
			tool.dropTable("session_template_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionTemplate");

		}

		// удалить сущность unisessionSummaryBulletinReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_summary_bull_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionSummaryBulletinReport");

		}

		// удалить сущность unisessionResultsReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_results_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionResultsReport");

		}

		// удалить сущность unisessionResultsByDiscReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_results_d_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionResultsByDiscReport");

		}

		// удалить сущность unisessionGroupMarksReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_grp_marks_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionGroupMarksReport");

		}

		// удалить сущность unisessionGroupBulletinListReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_grp_bull_list_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionGroupBulletinListReport");

		}

		// удалить сущность unisessionDebtorsReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_debtors_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisessionDebtorsReport");

		}

		// удалить сущность sessionsSimpleDocumentReason
		{
			// удалить таблицу
			tool.dropTable("sessionssimpledocumentreason_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionsSimpleDocumentReason");

		}

		// удалить сущность sessionTransferOutsideOperation
		{
			// удалить таблицу
			tool.dropTable("session_transfer_outop_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferOutsideOperation");

		}

		// удалить сущность sessionTransferInsideOperation
		{
			// удалить таблицу
			tool.dropTable("session_transfer_inop_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferInsideOperation");

		}

		// удалить сущность sessionTransferOperation
		{
			// удалить таблицу
			tool.dropTable("session_transfer_op_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferOperation");

		}

		// удалить сущность sessionStudentNotAllowedForBulletin
		{
			// удалить таблицу
			tool.dropTable("session_student_na4b_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionStudentNotAllowedForBulletin");

		}

		// удалить сущность sessionStudentNotAllowed
		{
			// удалить таблицу
			tool.dropTable("session_student_na_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionStudentNotAllowed");

		}

		// удалить сущность sessionSlotRatingData
		{
			// удалить таблицу
			tool.dropTable("session_slot_rating_data_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSlotRatingData");

		}

		// удалить сущность sessionProjectTheme
		{
			// удалить таблицу
			tool.dropTable("session_project_theme_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionProjectTheme");

		}

		// удалить сущность sessionMarkRatingData
		{
			// удалить таблицу
			tool.dropTable("session_mark_rating_data_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionMarkRatingData");

		}

		// удалить сущность sessionMarkStateCatalogItem
		{
			// удалить таблицу
			tool.dropTable("session_c_mark_state_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionMarkStateCatalogItem");

		}

		// удалить сущность sessionMarkGradeValueCatalogItem
		{
			// удалить таблицу
			tool.dropTable("session_c_mark_grade_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionMarkGradeValueCatalogItem");

		}

		// удалить сущность sessionMarkCatalogItem
		{
			// удалить таблицу
			tool.dropTable("session_c_mark_base_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionMarkCatalogItem");

		}

		// удалить сущность sessionSlotMarkState
		{
			// удалить таблицу
			tool.dropTable("session_mark_state_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSlotMarkState");

		}

		// удалить сущность sessionSlotMarkGradeValue
		{
			// удалить таблицу
			tool.dropTable("session_mark_value_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSlotMarkGradeValue");

		}

		// удалить сущность sessionSlotRegularMark
		{
			// удалить таблицу
			tool.dropTable("session_mark_reg_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSlotRegularMark");

		}

		// удалить сущность sessionSlotLinkMark
		{
			// удалить таблицу
			tool.dropTable("session_mark_link_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSlotLinkMark");

		}

		// удалить сущность sessionMark
		{
			// удалить таблицу
			tool.dropTable("session_mark_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionMark");

		}

		// удалить сущность sessionDocumentSlot
		{
			// удалить таблицу
			tool.dropTable("session_doc_slot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionDocumentSlot");

		}

		// удалить сущность sessionDocumentPrintVersion
		{
			// удалить таблицу
			tool.dropTable("session_doc_printform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionDocumentPrintVersion");

		}

		// удалить сущность sessionTransferOutsideDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_outside_transfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferOutsideDocument");

		}

		// удалить сущность sessionTransferInsideDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_inside_transfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferInsideDocument");

		}

		// удалить сущность sessionTransferDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_transfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionTransferDocument");

		}

		// удалить сущность sessionStudentGradeBookDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_stgbook_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionStudentGradeBookDocument");

		}

		// удалить сущность sessionSheetDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_sheet_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSheetDocument");

		}

		// удалить сущность sessionListDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_list_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionListDocument");

		}

		// удалить сущность sessionSimpleDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_simple_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionSimpleDocument");

		}

		// удалить сущность sessionRetakeDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_retake_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionRetakeDocument");

		}

		// удалить сущность sessionObject
		{
			// удалить таблицу
			tool.dropTable("session_object_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionObject");

		}

		// удалить сущность sessionBulletinDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_bulletin_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionBulletinDocument");

		}

		// удалить сущность sessionDocument
		{
			// удалить таблицу
			tool.dropTable("session_doc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionDocument");

		}

		// удалить сущность sessionComissionPps
		{
			// удалить таблицу
			tool.dropTable("session_comission_pps_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionComissionPps");

		}

		// удалить сущность sessionComission
		{
			// удалить таблицу
			tool.dropTable("session_comission_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionComission");

		}

		// удалить сущность sessionAttestationTotalResultReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_att_total_result_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationTotalResultReport");

		}

		// удалить сущность sessionAttestationSlotAdditionalData
		{
			// удалить таблицу
			tool.dropTable("session_att_slot_adddata_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationSlotAdditionalData");

		}

		// удалить сущность sessionAttestationSlot
		{
			// удалить таблицу
			tool.dropTable("session_att_slot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationSlot");

		}

		// удалить сущность sessionAttestationResultReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_att_result_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationResultReport");

		}

		// удалить сущность sessionAttestationBulletinReport
		{
			// удалить таблицу
			tool.dropTable("session_rep_att_bull_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationBulletinReport");

		}

		// удалить сущность sessionAttestationBulletin
		{
			// удалить таблицу
			tool.dropTable("session_att_bull_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestationBulletin");

		}

		// удалить сущность sessionAttestation
		{
			// удалить таблицу
			tool.dropTable("session_attestation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttestation");

		}

		// удалить сущность sessionAttBullletinPrintVersion
		{
			// удалить таблицу
			tool.dropTable("session_att_bull_printform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("sessionAttBullletinPrintVersion");

		}

		////////////////////////////////////////////////////////////////////////////////
		// модуль uniepp отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
            ModuleMeta deletedModule;
            try
            {
                deletedModule = ApplicationRuntime.getModuleMeta("uniepp");
            }
            catch (RuntimeException e)
            {
                deletedModule = null;
            }

			if( deletedModule!=null )
			{
				ModuleMeta mainModule = ApplicationRuntime.getModuleMeta("uniusaca");
				if( mainModule.getDependencyList().contains(deletedModule) )
					throw new RuntimeException("Module 'uniepp' is not deleted");
			}
		}

		// удалить персистентный интерфейс ru.tandemservice.uniepp.entity.student.slot.IEppStudentWpSlotRow
		{
			// удалить view
			tool.dropView("ieppstudentwpslotrow_v");

		}

		// удалить персистентный интерфейс ru.tandemservice.uniepp.entity.student.group.IEppRealEduGroupRow
		{
			// удалить view
			tool.dropView("iepprealedugrouprow_v");

		}

		// удалить персистентный интерфейс ru.tandemservice.uniepp.entity.student.group.IEppRealEduGroup
		{
			// удалить view
			tool.dropView("iepprealedugroup_v");

		}

		// удалить персистентный интерфейс ru.tandemservice.uniepp.entity.pps.IEppPPSCollectionOwner
		{
			// удалить view
			tool.dropView("ieppppscollectionowner_v");

		}

		// удалить персистентный интерфейс ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement
		{
			// удалить view
			tool.dropView("ieppeducationelement_v");

		}

		// удалить персистентный интерфейс ru.tandemservice.uniepp.entity.catalog.IEppGroupType
		{
			// удалить view
			tool.dropView("ieppgrouptype_v");

		}

		// удалить сущность eppDebitorsPayReport
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("epp_debitors_pay_report_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("epp_debitors_pay_report_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppDebitorsPayReport");

		}

		// удалить сущность eppCtrEducationResult
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("epp_ctr_re_student_t", "ctr_result_t");

			// удалить таблицу
			tool.dropTable("epp_ctr_re_student_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppCtrEducationResult");

		}

		// удалить сущность eppCtrEducationPromice
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("epp_ctr_pr_edu_t", "ctr_promice_t");

			// удалить таблицу
			tool.dropTable("epp_ctr_pr_edu_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppCtrEducationPromice");

		}

		// удалить сущность eppCtrContractVersionTemplateData
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("epp_ctr_ctmpldt_t", "ctr_version_template_data_t");

			// удалить таблицу
			tool.dropTable("epp_ctr_ctmpldt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppCtrContractVersionTemplateData");

		}

		// удалить сущность eppCtrContractTemplateDataSimple
		{
			// удалить таблицу
			tool.dropTable("epp_ctr_ctmpldt_simple_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppCtrContractTemplateDataSimple");

		}

		// удалить сущность eppCtrAgreementTemplateDataSetYearPrice
		{
			// удалить таблицу
			tool.dropTable("epp_ctr_ctmpldt_yprice_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppCtrAgreementTemplateDataSetYearPrice");

		}

		// удалить сущность eppYearPart
		{
			// удалить таблицу
			tool.dropTable("epp_year_part_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppYearPart");

		}

		// удалить сущность eppYearEducationWeek
		{
			// удалить таблицу
			tool.dropTable("epp_year_week_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppYearEducationWeek");

		}

		// удалить сущность eppYearEducationProcess
		{
			// удалить таблицу
			tool.dropTable("epp_year_epp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppYearEducationProcess");

		}

		// удалить сущность eppWorkPlanRowPartLoad
		{
			// удалить таблицу
			tool.dropTable("epp_wprow_part_load_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkPlanRowPartLoad");

		}

		// удалить сущность eppWorkPlanRowObligation
		{
			// удалить таблицу
			tool.dropTable("epp_c_wp_obligation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkPlanRowObligation");

		}

		// удалить сущность eppWorkPlanTemplateRow
		{
			// удалить таблицу
			tool.dropTable("epp_wprow_tmpl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkPlanTemplateRow");

		}

		// удалить сущность eppWorkPlanRegistryElementRow
		{
			// удалить таблицу
			tool.dropTable("epp_wprow_regel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkPlanRegistryElementRow");

		}

		// удалить сущность eppWorkPlanRow
		{
			// удалить таблицу
			tool.dropTable("epp_wprow_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkPlanRow");

		}

		// удалить сущность eppWorkPlanPart
		{
			// удалить таблицу
			tool.dropTable("epp_wppart_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkPlanPart");

		}

		// удалить сущность eppWorkPlanVersion
		{
			// удалить таблицу
			tool.dropTable("epp_workplan_version_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkPlanVersion");

		}

		// удалить сущность eppWorkPlan
		{
			// удалить таблицу
			tool.dropTable("epp_workplan_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkPlan");

		}

		// удалить сущность eppWorkPlanBase
		{
			// удалить таблицу
			tool.dropTable("epp_workplan_base_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkPlanBase");

		}

		// удалить сущность eppWorkGraphRowWeek
		{
			// удалить таблицу
			tool.dropTable("epp_workgraph_rowweek_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkGraphRowWeek");

		}

		// удалить сущность eppWorkGraphRow2EduPlan
		{
			// удалить таблицу
			tool.dropTable("epp_workgraph_rowplan_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkGraphRow2EduPlan");

		}

		// удалить сущность eppWorkGraphRow
		{
			// удалить таблицу
			tool.dropTable("epp_workgraph_row_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkGraphRow");

		}

		// удалить сущность eppWorkGraph
		{
			// удалить таблицу
			tool.dropTable("epp_workgraph_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWorkGraph");

		}

		// удалить сущность eppWeekType
		{
			// удалить таблицу
			tool.dropTable("epp_c_weektype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWeekType");

		}

		// удалить сущность eppWeek
		{
			// удалить таблицу
			tool.dropTable("epp_c_week_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppWeek");

		}

		// удалить сущность eppViewTableDisciplines
		{
			// удалить таблицу
			tool.dropTable("epp_c_viewtbldisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppViewTableDisciplines");

		}

		// удалить сущность eppTutorOrgUnit
		{
			// удалить таблицу
			tool.dropTable("epp_orgunit_tutor_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppTutorOrgUnit");

		}

		// удалить сущность eppStudentWpCActionSlot
		{
			// удалить таблицу
			tool.dropTable("epp_studentepv_slot_caction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStudentWpCActionSlot");

		}

		// удалить сущность eppStudentWpALoadSlot
		{
			// удалить таблицу
			tool.dropTable("epp_studentepv_slot_aload_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStudentWpALoadSlot");

		}

		// удалить сущность eppStudentEpvSlot
		{
			// удалить таблицу
			tool.dropTable("epp_studentepv_slot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStudentEpvSlot");

		}

		// удалить сущность eppStudent2WorkPlan
		{
			// удалить таблицу
			tool.dropTable("epp_student_workplan_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStudent2WorkPlan");

		}

		// удалить сущность eppStudent2EduPlanVersion
		{
			// удалить таблицу
			tool.dropTable("epp_student_eduplanversion_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStudent2EduPlanVersion");

		}

		// удалить сущность eppStdRow2SkillRelation
		{
			// удалить таблицу
			tool.dropTable("epp_stdrow_skill_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdRow2SkillRelation");

		}

		// удалить сущность eppStdStructureRow
		{
			// удалить таблицу
			tool.dropTable("epp_stdrow_structure_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdStructureRow");

		}

		// удалить сущность eppStdGroupReRow
		{
			// удалить таблицу
			tool.dropTable("epp_stdrow_group_re_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdGroupReRow");

		}

		// удалить сущность eppStdGroupImRow
		{
			// удалить таблицу
			tool.dropTable("epp_stdrow_group_im_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdGroupImRow");

		}

		// удалить сущность eppStdGroupRow
		{
			// удалить таблицу
			tool.dropTable("epp_stdrow_group_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdGroupRow");

		}

		// удалить сущность eppStdHierarchyRow
		{
			// удалить таблицу
			tool.dropTable("epp_stdrow_hierarchy_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdHierarchyRow");

		}

		// удалить сущность eppStdDisciplineTopRow
		{
			// удалить таблицу
			tool.dropTable("epp_stdrow_discipline_t_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdDisciplineTopRow");

		}

		// удалить сущность eppStdDisciplineNestedRow
		{
			// удалить таблицу
			tool.dropTable("epp_stdrow_discipline_n_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdDisciplineNestedRow");

		}

		// удалить сущность eppStdDisciplineBaseRow
		{
			// удалить таблицу
			tool.dropTable("epp_stdrow_discipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdDisciplineBaseRow");

		}

		// удалить сущность eppStdActionRow
		{
			// удалить таблицу
			tool.dropTable("epp_stdrow_action_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdActionRow");

		}

		// удалить сущность eppStdRow
		{
			// удалить таблицу
			tool.dropTable("epp_stdrow_base_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdRow");

		}

		// удалить сущность eppStdDisciplineComment
		{
			// удалить таблицу
			tool.dropTable("epp_stddscpn_cmmnt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStdDisciplineComment");

		}

		// удалить сущность eppStateEduStandardSkill
		{
			// удалить таблицу
			tool.dropTable("epp_stateedustd_skill_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStateEduStandardSkill");

		}

		// удалить сущность eppStateEduStandardBlock
		{
			// удалить таблицу
			tool.dropTable("epp_stateedustd_block_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStateEduStandardBlock");

		}

		// удалить сущность eppStateEduStandard
		{
			// удалить таблицу
			tool.dropTable("epp_stateedustd_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppStateEduStandard");

		}

		// удалить сущность eppState
		{
			// удалить таблицу
			tool.dropTable("epp_c_state_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppState");

		}

		// удалить сущность eppSkillType
		{
			// удалить таблицу
			tool.dropTable("epp_c_skill_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppSkillType");

		}

		// удалить сущность eppSkillGroup
		{
			// удалить таблицу
			tool.dropTable("epp_c_skill_group_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppSkillGroup");

		}

		// удалить сущность eppRegistryStructure2WeekTypeRelation
		{
			// удалить таблицу
			tool.dropTable("epp_regstruct_week_type_rel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryStructure2WeekTypeRelation");

		}

		// удалить сущность eppRegistryStructure
		{
			// удалить таблицу
			tool.dropTable("epp_c_reg_struct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryStructure");

		}

		// удалить сущность eppRegistrySkill
		{
			// удалить таблицу
			tool.dropTable("epp_reg_skill_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistrySkill");

		}

		// удалить сущность eppRegistryModuleOutgoingSkill
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("eppRegistryModuleOutgoingSkill");

		}

		// удалить сущность eppRegistryModuleIncomingSkill
		{
			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("eppRegistryModuleIncomingSkill");

		}

		// удалить сущность eppRegistryModuleSkill
		{
			// удалить таблицу
			tool.dropTable("epp_reg_module_skill_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryModuleSkill");

		}

		// удалить сущность eppRegistryModuleIControlAction
		{
			// удалить таблицу
			tool.dropTable("epp_reg_module_ictr_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryModuleIControlAction");

		}

		// удалить сущность eppRegistryModuleALoad
		{
			// удалить таблицу
			tool.dropTable("epp_reg_module_aload_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryModuleALoad");

		}

		// удалить сущность eppRegistryModule
		{
			// удалить таблицу
			tool.dropTable("epp_reg_module_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryModule");

		}

		// удалить сущность eppRegistryElementPartModule
		{
			// удалить таблицу
			tool.dropTable("epp_reg_element_part_mod_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryElementPartModule");

		}

		// удалить сущность eppRegistryElementPartFControlAction
		{
			// удалить таблицу
			tool.dropTable("epp_reg_element_part_fca_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryElementPartFControlAction");

		}

		// удалить сущность eppRegistryElementPart
		{
			// удалить таблицу
			tool.dropTable("epp_reg_element_part_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryElementPart");

		}

		// удалить сущность eppRegistryElementLoad
		{
			// удалить таблицу
			tool.dropTable("epp_reg_base_load_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryElementLoad");

		}

		// удалить сущность eppRegistryDiscipline
		{
			// удалить таблицу
			tool.dropTable("epp_reg_discipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryDiscipline");

		}

		// удалить сущность eppRegistryPractice
		{
			// удалить таблицу
			tool.dropTable("epp_reg_action_practice_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryPractice");

		}

		// удалить сущность eppRegistryAttestation
		{
			// удалить таблицу
			tool.dropTable("epp_reg_action_attestation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryAttestation");

		}

		// удалить сущность eppRegistryAction
		{
			// удалить таблицу
			tool.dropTable("epp_reg_action_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryAction");

		}

		// удалить сущность eppRegistryElement
		{
			// удалить таблицу
			tool.dropTable("epp_reg_base_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryElement");

		}

		// удалить сущность eppRealEduGroupSummary
		{
			// удалить таблицу
			tool.dropTable("epp_realgroup_summary_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRealEduGroupSummary");

		}

		// удалить сущность eppRealEduGroupCompleteLevel
		{
			// удалить таблицу
			tool.dropTable("epp_c_rgclvl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRealEduGroupCompleteLevel");

		}

		// удалить сущность eppRealEduGroup4LoadTypeRow
		{
			// удалить таблицу
			tool.dropTable("epp_rgrp_g4ld_row_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRealEduGroup4LoadTypeRow");

		}

		// удалить сущность eppRealEduGroup4LoadType
		{
			// удалить таблицу
			tool.dropTable("epp_rgrp_g4ld_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRealEduGroup4LoadType");

		}

		// удалить сущность eppRealEduGroup4ActionTypeRow
		{
			// удалить таблицу
			tool.dropTable("epp_rgrp_g4ca_row_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRealEduGroup4ActionTypeRow");

		}

		// удалить сущность eppRealEduGroup4ActionType
		{
			// удалить таблицу
			tool.dropTable("epp_rgrp_g4ca_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppRealEduGroup4ActionType");

		}

		// удалить сущность eppPpsRequest
		{
			// удалить таблицу
			tool.dropTable("epp_pps_request_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppPpsRequest");

		}

		// удалить сущность eppPpsCollectionItem
		{
			// удалить таблицу
			tool.dropTable("epp_pps_collection_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppPpsCollectionItem");

		}

		// удалить сущность eppPlanStructureQualification
		{
			// удалить таблицу
			tool.dropTable("epp_regstruct_qual_rel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppPlanStructureQualification");

		}

		// удалить сущность eppPlanStructure
		{
			// удалить таблицу
			tool.dropTable("epp_c_plan_struct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppPlanStructure");

		}

		// удалить сущность eppPlanOrgUnit
		{
			// удалить таблицу
			tool.dropTable("epp_orgunit_plan_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppPlanOrgUnit");

		}

		// удалить сущность eppModuleStructure
		{
			// удалить таблицу
			tool.dropTable("epp_c_mod_struct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppModuleStructure");

		}

		// удалить сущность eppELoadType
		{
			// удалить таблицу
			tool.dropTable("epp_c_loadtype_e_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppELoadType");

		}

		// удалить сущность eppALoadType
		{
			// удалить таблицу
			tool.dropTable("epp_c_loadtype_a_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppALoadType");

		}

		// удалить сущность eppLoadType
		{
			// удалить таблицу
			tool.dropTable("epp_c_loadtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppLoadType");

		}

		// удалить сущность eppLoadPresentation
		{
			// удалить таблицу
			tool.dropTable("epp_c_loadpresent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppLoadPresentation");

		}

		// удалить сущность eppIndicatorComponent
		{
			// удалить таблицу
			tool.dropTable("epp_c_indicator_component_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppIndicatorComponent");

		}

		// удалить сущность eppIndexRule4Qualification
		{
			// удалить таблицу
			tool.dropTable("epp_edulevel_gen_rel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppIndexRule4Qualification");

		}

		// удалить сущность eppIndexRule
		{
			// удалить таблицу
			tool.dropTable("epp_c_indexrule_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppIndexRule");

		}

		// удалить сущность eppGradeScale
		{
			// удалить таблицу
			tool.dropTable("epp_c_gradescale_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppGradeScale");

		}

		// удалить сущность eppGeneration
		{
			// удалить таблицу
			tool.dropTable("epp_c_generation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppGeneration");

		}

		// удалить сущность eppFControlActionGroup
		{
			// удалить таблицу
			tool.dropTable("epp_c_fcactiongroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppFControlActionGroup");

		}

		// удалить сущность eppEpvRowTermLoad
		{
			// удалить таблицу
			tool.dropTable("epp_epvrowterm_load_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvRowTermLoad");

		}

		// удалить сущность eppEpvRowTermAction
		{
			// удалить таблицу
			tool.dropTable("epp_epvrowterm_caction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvRowTermAction");

		}

		// удалить сущность eppEpvRowTerm
		{
			// удалить таблицу
			tool.dropTable("epp_epvrowterm_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvRowTerm");

		}

		// удалить сущность eppEpvRowLoad
		{
			// удалить таблицу
			tool.dropTable("epp_epvrow_distr_load_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvRowLoad");

		}

		// удалить сущность eppEpvRegistryRow
		{
			// удалить таблицу
			tool.dropTable("epp_epvrow_regel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvRegistryRow");

		}

		// удалить сущность eppEpvGroupImRow
		{
			// удалить таблицу
			tool.dropTable("epp_epvrow_group_im_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvGroupImRow");

		}

		// удалить сущность eppEpvTermDistributedRow
		{
			// удалить таблицу
			tool.dropTable("epp_epvrow_distr_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvTermDistributedRow");

		}

		// удалить сущность eppEpvTemplateRow
		{
			// удалить таблицу
			tool.dropTable("epp_epvrow_template_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvTemplateRow");

		}

		// удалить сущность eppEpvStructureRow
		{
			// удалить таблицу
			tool.dropTable("epp_epvrow_structure_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvStructureRow");

		}

		// удалить сущность eppEpvGroupReRow
		{
			// удалить таблицу
			tool.dropTable("epp_epvrow_group_re_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvGroupReRow");

		}

		// удалить сущность eppEpvHierarchyRow
		{
			// удалить таблицу
			tool.dropTable("epp_epvrow_hierarchy_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvHierarchyRow");

		}

		// удалить сущность eppEpvRow
		{
			// удалить таблицу
			tool.dropTable("epp_epvrow_base_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEpvRow");

		}

		// удалить сущность eppEducationOrgUnitEduPlanVersion
		{
			// удалить таблицу
			tool.dropTable("epp_eduou_epv_rel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEducationOrgUnitEduPlanVersion");

		}

		// удалить сущность eppEduPlanVersionWeekType
		{
			// удалить таблицу
			tool.dropTable("epp_eduplan_ver_week_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEduPlanVersionWeekType");

		}

		// удалить сущность eppEduPlanVersionBlock
		{
			// удалить таблицу
			tool.dropTable("epp_eduplan_verblock_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEduPlanVersionBlock");

		}

		// удалить сущность eppEduPlanVersion
		{
			// удалить таблицу
			tool.dropTable("epp_eduplan_ver_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEduPlanVersion");

		}

		// удалить сущность eppEduPlan
		{
			// удалить таблицу
			tool.dropTable("epp_eduplan_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEduPlan");

		}

		// удалить сущность eppEduLevelToWorkTypesRel
		{
			// удалить таблицу
			tool.dropTable("epp_edulevel_worktypes_rel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppEduLevelToWorkTypesRel");

		}

		// удалить сущность eppELoadWeekType
		{
			// удалить таблицу
			tool.dropTable("epp_eload_week_type_rel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppELoadWeekType");

		}

		// удалить сущность eppDefaultSizeForLabor
		{
			// удалить таблицу
			tool.dropTable("epp_def_size_for_labor_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppDefaultSizeForLabor");

		}

		// удалить сущность eppDefaultSelfworkForSize
		{
			// удалить таблицу
			tool.dropTable("epp_def_selfwork_for_size_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppDefaultSelfworkForSize");

		}

		// удалить сущность eppCtrDocTemplate
		{
			// удалить таблицу
			tool.dropTable("epp_ctr_c_doc_template", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppCtrDocTemplate");

		}

		// удалить сущность eppCtrAcademyPresenter
		{
			// удалить таблицу
			tool.dropTable("epp_ctr_presenter_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppCtrAcademyPresenter");

		}

		// удалить сущность eppIControlActionType
		{
			// удалить таблицу
			tool.dropTable("epp_c_cactiontype_i_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppIControlActionType");

		}

		// удалить сущность eppFControlActionType
		{
			// удалить таблицу
			tool.dropTable("epp_c_cactiontype_f_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppFControlActionType");

		}

		// удалить сущность eppControlActionType
		{
			// удалить таблицу
			tool.dropTable("epp_c_cactiontype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eppControlActionType");

		}


    }
}