/* $Id$ */
package ru.tandemservice.uniusaca.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;

/**
 * @author Dmitry Seleznev
 * @since 13.08.2013
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .overwriteTab(componentTab("abstractOrgUnit_MoveStudent", "ru.tandemservice.movestudent.component.orgunit.OrgUnitMoveStudent").permissionKey("ui:secModel.orgUnit_viewMoveStudent").visible("ognl:@ru.tandemservice.movestudent.dao.MoveStudentDaoFacade@getMoveStudentDao().isOrgUnitMoveStudentTabVisible(presenter.orgUnit)").parameters("mvel:['orgUnitId':presenter.orgUnit.id]").visible("false"))
                .create();
    }
}