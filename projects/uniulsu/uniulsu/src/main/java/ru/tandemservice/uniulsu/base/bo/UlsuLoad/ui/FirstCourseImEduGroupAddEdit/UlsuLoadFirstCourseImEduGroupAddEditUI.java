/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.FirstCourseImEduGroupAddEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.UlsuLoadManager;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.IUlsuLoadDAO;
import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuImEduGroupTimeRuleRel;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.gen.UlsuImEduGroupTimeRuleRelGen;

import java.util.Collection;
import java.util.List;

import static ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.FirstCourseImEduGroupAddEdit.UlsuLoadFirstCourseImEduGroupAddEdit.*;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "recordId"),
    @Bind(key = "ppsId", binding = "ppsId")})
public class UlsuLoadFirstCourseImEduGroupAddEditUI extends UIPresenter
{
    private Long _recordId;
    private Long _ppsId;
    private UlsuFirstCourseImEduGroup _record = new UlsuFirstCourseImEduGroup();
    private EppWorkPlan _workPlan;
    private Collection<UlsuLoadTimeRule> _timeRules;


    public Long getRecordId(){ return _recordId; }
    public void setRecordId(Long recordId){ _recordId = recordId; }

    public Long getPpsId(){ return _ppsId; }
    public void setPpsId(Long ppsId){ _ppsId = ppsId; }

    public UlsuFirstCourseImEduGroup getRecord(){ return _record; }
    public void setRecord(UlsuFirstCourseImEduGroup record){ _record = record; }

    public EppWorkPlan getWorkPlan(){ return _workPlan; }
    public void setWorkPlan(EppWorkPlan workPlan){ _workPlan = workPlan; }

    public Collection<UlsuLoadTimeRule> getTimeRules(){ return _timeRules; }
    public void setTimeRules(Collection<UlsuLoadTimeRule> timeRules){ _timeRules = timeRules; }

    public boolean isAddForm(){ return _recordId == null; }
    public boolean isEditForm(){ return !isAddForm(); }


    @Override
    public void onComponentRefresh()
    {
        if (isEditForm())
        {
            _record = UlsuLoadManager.instance().dao().getNotNull(UlsuFirstCourseImEduGroup.class, _recordId);
            _workPlan = (EppWorkPlan) _record.getRow().getWorkPlan();

            List<UlsuImEduGroupTimeRuleRel> timeRuleRels = UlsuLoadManager.instance().dao().getList(UlsuImEduGroupTimeRuleRel.class, UlsuImEduGroupTimeRuleRel.eduGroup().id(), _recordId, UlsuImEduGroupTimeRuleRel.timeRule().title().s());
            Transformer<UlsuImEduGroupTimeRuleRel, UlsuLoadTimeRule> transformer = UlsuImEduGroupTimeRuleRelGen::getTimeRule;

            _timeRules = CollectionUtils.collect(timeRuleRels, transformer);
        }
    }

    public void onClickApply()
    {
        IUlsuLoadDAO dao = UlsuLoadManager.instance().dao();
        if (isAddForm())
        {
            _record.setPps(dao.getNotNull(PpsEntry.class, _ppsId));
            _record.setActivityPart(_record.getRow().getRegistryElementPart());
        }

        dao.saveOrUpdate(_record);
        dao.saveUlsuImEduGroupTimeRules(_record.getId(), _timeRules);
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case WORK_PLAN_ROW_DS: dataSource.put(WORK_PLAN, _workPlan == null ? null : _workPlan.getId());
            case GROUP_TYPE_DS: dataSource.put(WORK_PLAN_ROW, _record.getRow() == null ? null : _record.getRow().getId());
        }
    }
}