/**
 *$Id$
 */
package ru.tandemservice.uniulsu.entity.eduPlanLoad;

import org.tandemframework.core.common.ITitled;

/**
 * Основание нормы времени. Персистентное перечисление. Удаление/переименование элементов должно сопровождаться МИГРАЦИЕЙ.
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public enum UlsuImEduGroupLoadBase implements ITitled
{
    CONSTANT("Фикс. число"),
    STUDENTS("Студенты"),
    HOURS("Часы");

    private String _title;
    public String getTitle(){ return _title; }

    private UlsuImEduGroupLoadBase(String title){ _title = title; }
}