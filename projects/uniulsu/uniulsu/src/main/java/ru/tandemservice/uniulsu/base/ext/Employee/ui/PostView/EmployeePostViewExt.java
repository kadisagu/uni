/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.ext.Employee.ui.PostView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.PpsTab.UlsuLoadPpsTab;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Configuration
public class EmployeePostViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "ulsu" + EmployeePostViewExtUI.class.getSimpleName();

    public static final String ULSU_LOAD_TAB_NAME = "UlsuLoad";

    @Autowired
    private EmployeePostView _employeePostView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeePostView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeePostViewExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_employeePostView.employeePostTabPanelExtPoint())
                .addAllAfter(EmployeePostView.EMPLOYEE_POST_DATA_TAB)
                .addTab(componentTab(ULSU_LOAD_TAB_NAME, UlsuLoadPpsTab.class)
                        .parameters("addon:" + ADDON_NAME + ".parameters")
                        .visible("addon:" + ADDON_NAME + ".visibleLoadTab")
                        .permissionKey("employeePost_viewUlsuLoadTab"))
                .create();
    }
}