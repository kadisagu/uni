/**
 *$Id$
 */
package ru.tandemservice.uniulsu.component.catalog.ulsuLoadTimeRule.UlsuLoadTimeRuleAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<UlsuLoadTimeRule, Model>
{
}