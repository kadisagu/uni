/**
 *$Id$
 */
package ru.tandemservice.uniulsu.component.catalog.ulsuLoadTimeRule.UlsuLoadTimeRulePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class Controller extends DefaultCatalogPubController<UlsuLoadTimeRule, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<UlsuLoadTimeRule> createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<UlsuLoadTimeRule> dataSource = new DynamicListDataSource<UlsuLoadTimeRule>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", UlsuLoadTimeRule.P_TITLE).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Коэффициент", UlsuLoadTimeRule.coefficientAsDouble()).setFormatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Основание", UlsuLoadTimeRule.base().s() + ".title").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Примечание", UlsuLoadTimeRule.comment()).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", UlsuLoadTimeRule.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));

        return dataSource;
    }
}