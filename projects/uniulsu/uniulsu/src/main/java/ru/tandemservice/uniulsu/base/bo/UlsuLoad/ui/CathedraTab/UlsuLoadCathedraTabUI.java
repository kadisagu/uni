/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.CathedraTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.UlsuLoadManager;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.IUlsuLoadDAO;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.UlsuPpsLoadWrapper;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.util.UlsuPpsLoadDataWrapper;
import ru.tandemservice.uniulsu.base.ext.Employee.ui.PostView.EmployeePostViewExt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Input(@Bind(key = "orgUnitId", binding = "orgUnitId", required = true))
public class UlsuLoadCathedraTabUI extends UIPresenter
{
    private Long _orgUnitId;
    private List<UlsuPpsLoadWrapper> _ppsWrappers;
    private Long _totalEduLoadSize;
    private Long _totalNonEduLoadSize;

    public Long getOrgUnitId(){ return _orgUnitId; }
    public void setOrgUnitId(Long orgUnitId){ _orgUnitId = orgUnitId; }

    public ParametersMap getParameters()
    {
        return ParametersMap
                .createWith("selectedTab", EmployeePostView.EMPLOYEE_POST_TAB)
                .add("selectedDataTab", EmployeePostViewExt.ULSU_LOAD_TAB_NAME);
    }

    public String getTotalEduLoadSize()
    {
        return "Всего учебной нагрузки: " + (_totalEduLoadSize == 0L ? "нет" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(_totalEduLoadSize)));
    }

    public String getTotalNonEduLoadSize()
    {
        return "Всего внеучебной нагрузки: " + (_totalNonEduLoadSize == 0L ? "нет" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(_totalNonEduLoadSize)));
    }

    public String getTotalLoadSize()
    {
        Long total = 0L;
        if (_totalEduLoadSize != null){ total += _totalEduLoadSize; }
        if (_totalNonEduLoadSize != null){ total += _totalNonEduLoadSize; }

        return "Всего: " + (total == 0L ? "нет" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(total)));
    }

    @Override
    public void onComponentRefresh()
    {
        List<UlsuPpsLoadWrapper> ppsWrappers = new ArrayList<>();
        Long totalEduLoadSize = 0L;
        Long totalNonEduLoadSize = 0L;

        IUlsuLoadDAO dao = UlsuLoadManager.instance().dao();
        List<EmployeePost4PpsEntry> ppsList = dao.getList(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.ppsEntry().orgUnit().id(), _orgUnitId);
        Map<Long, UlsuPpsLoadDataWrapper> dataMap = dao.getPpsLoadDataWrapperMap(_orgUnitId, null);
        for (EmployeePost4PpsEntry pps: ppsList)
        {
            UlsuPpsLoadDataWrapper data = dataMap.get(pps.getPpsEntry().getId());
            Long eduLoadSize = data.getTotalEduLoad();
            Long nonEduLoadSize = data.getTotalNonEduLoad();
            ppsWrappers.add(new UlsuPpsLoadWrapper(pps, UniEppUtils.wrap(eduLoadSize), UniEppUtils.wrap(nonEduLoadSize)));

            totalEduLoadSize += eduLoadSize;
            totalNonEduLoadSize += nonEduLoadSize;
        }

        _ppsWrappers = ppsWrappers;
        _totalEduLoadSize = totalEduLoadSize;
        _totalNonEduLoadSize = totalNonEduLoadSize;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (UlsuLoadCathedraTab.PPS_DS.equals(dataSource.getName()))
        {
            dataSource.put("ppsWrappers", _ppsWrappers);
        }
    }
}