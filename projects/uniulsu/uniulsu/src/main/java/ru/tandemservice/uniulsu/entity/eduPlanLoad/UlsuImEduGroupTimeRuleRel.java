package ru.tandemservice.uniulsu.entity.eduPlanLoad;

import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.gen.*;

/**
 * Связь нормы времени и планируемой УГС
 */
public class UlsuImEduGroupTimeRuleRel extends UlsuImEduGroupTimeRuleRelGen
{
    public UlsuImEduGroupTimeRuleRel()
    {

    }

    public UlsuImEduGroupTimeRuleRel(IUlsuImEduGroup eduGroup, UlsuLoadTimeRule timeRule)
    {
        this.setEduGroup(eduGroup);
        this.setTimeRule(timeRule);
    }
}