/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniulsu.component.modularextract.e1001;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract;

/**
 *
 * @author iolshvang
 * @since 01.06.11 16:44
 */
public class TransferToBudgetStuExtractPrint implements IPrintFormCreator<TransferToBudgetStuExtract>
{
    String getDevelopCondition_G(DevelopCondition developCondition)
    {
        if (UniDefines.DEVELOP_CONDITION_SHORT.equals(developCondition.getCode()))
            return " (сокращенной)";
        else if (UniDefines.DEVELOP_CONDITION_FAST.equals(developCondition.getCode()))
            return " (ускоренной)";
        else if (UniDefines.DEVELOP_CONDITION_SHORT_FAST.equals(developCondition.getCode()))
            return " (сокращенной ускоренной)";
        else return "";
    }

    String getDevelopForm_G(DevelopForm developForm)
    {
        if (DevelopFormCodes.FULL_TIME_FORM.equals(developForm.getCode()))
            return "очной";
        else if (DevelopFormCodes.CORESP_FORM.equals(developForm.getCode()))
            return "заочной";
        else if (DevelopFormCodes.PART_TIME_FORM.equals(developForm.getCode()))
            return "очно-заочной";
        else return "";
    }

    String getCompensationType_G(CompensationType compensationType)
    {
        if (UniDefines.COMPENSATION_TYPE_BUDGET.equals(compensationType.getCode()))
            return "бюджетной";
        else if (UniDefines.COMPENSATION_TYPE_CONTRACT.equals(compensationType.getCode()))
            return "внебюджетной";
        else return "";
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, TransferToBudgetStuExtract extract)
    {
        String developCondition_G = getDevelopCondition_G(extract.getEntity().getEducationOrgUnit().getDevelopCondition());
        String lowDevelopCondition_G = getDevelopCondition_G(extract.getStudent().getEducationOrgUnit().getDevelopCondition());

        String developForm_G = getDevelopForm_G(extract.getEntity().getEducationOrgUnit().getDevelopForm());
        String lowDevelopForm_G = getDevelopForm_G(extract.getStudent().getEducationOrgUnit().getDevelopForm());

        String compensationType_G = getCompensationType_G(extract.getEntity().getCompensationType());
        String lowCompensationType_G = getCompensationType_G(extract.getStudent().getCompensationType());

        String orgUnit_G = extract.getEntity().getGroup().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle();
        String extractTitle = extract.getType().getTitle();

        String educationLevel = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getTitle();
        String lowEducationLevel = extract.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getTitle();

        String speciality = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
        String lowSpeciality = extract.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getTitle();

        String group = extract.getEntity().getGroup().getTitle();
        String lowGroup = extract.getStudent().getGroup().getTitle();

        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("extractTitle", extractTitle);
        modifier.put("fromDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getTransferDate()));

        modifier.put("lowStudentPersonalFileNumber", extract.getStudent().getBookNumber());
        modifier.put("lowCourse", extract.getStudent().getCourse().getTitle());
        modifier.put("lowDevelopForm_G", lowDevelopForm_G);
        modifier.put("lowDevelopCondition_G", lowDevelopCondition_G);
        modifier.put("lowOrgUnit_G", orgUnit_G != null ? orgUnit_G : extract.getStudent().getGroup().getEducationOrgUnit().getFormativeOrgUnit().getTitle());
        modifier.put("lowSpeciality", lowSpeciality);
        modifier.put("lowEducationLevel", lowEducationLevel);
        modifier.put("lowGroup", lowGroup);
        modifier.put("lowCompensationType_G", lowCompensationType_G);

        modifier.put("studentPersonalFileNumber", extract.getEntity().getBookNumber());
        modifier.put("course", extract.getEntity().getCourse().getTitle());
        modifier.put("developForm_G", developForm_G);
        modifier.put("developCondition_G", developCondition_G);
        modifier.put("orgUnit_G", orgUnit_G != null ? orgUnit_G : extract.getEntity().getGroup().getEducationOrgUnit().getFormativeOrgUnit().getTitle());
        modifier.put("speciality", speciality);
        modifier.put("educationLevel", educationLevel);
        modifier.put("group", group);
        modifier.put("compensationType_G", compensationType_G);

        modifier.modify(document);
        return document;
    }
}