package ru.tandemservice.uniulsu.entity.eduPlanLoad.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Внеучебная нагрузка
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UlsuNonEduLoadGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad";
    public static final String ENTITY_NAME = "ulsuNonEduLoad";
    public static final int VERSION_HASH = 89852570;
    private static IEntityMeta ENTITY_META;

    public static final String L_YEAR = "year";
    public static final String L_PPS = "pps";
    public static final String P_TITLE = "title";
    public static final String P_LOAD_SIZE = "loadSize";
    public static final String P_LOAD_SIZE_AS_DOUBLE = "loadSizeAsDouble";

    private EppYearEducationProcess _year;     // Учебный год
    private PpsEntry _pps;     // ППС
    private String _title;     // Название
    private long _loadSize;     // Кол-во часов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getYear()
    {
        return _year;
    }

    /**
     * @param year Учебный год. Свойство не может быть null.
     */
    public void setYear(EppYearEducationProcess year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return ППС. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getPps()
    {
        return _pps;
    }

    /**
     * @param pps ППС. Свойство не может быть null.
     */
    public void setPps(PpsEntry pps)
    {
        dirty(_pps, pps);
        _pps = pps;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Кол-во часов. Свойство не может быть null.
     */
    @NotNull
    public long getLoadSize()
    {
        return _loadSize;
    }

    /**
     * @param loadSize Кол-во часов. Свойство не может быть null.
     */
    public void setLoadSize(long loadSize)
    {
        dirty(_loadSize, loadSize);
        _loadSize = loadSize;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UlsuNonEduLoadGen)
        {
            setYear(((UlsuNonEduLoad)another).getYear());
            setPps(((UlsuNonEduLoad)another).getPps());
            setTitle(((UlsuNonEduLoad)another).getTitle());
            setLoadSize(((UlsuNonEduLoad)another).getLoadSize());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UlsuNonEduLoadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UlsuNonEduLoad.class;
        }

        public T newInstance()
        {
            return (T) new UlsuNonEduLoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "year":
                    return obj.getYear();
                case "pps":
                    return obj.getPps();
                case "title":
                    return obj.getTitle();
                case "loadSize":
                    return obj.getLoadSize();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "year":
                    obj.setYear((EppYearEducationProcess) value);
                    return;
                case "pps":
                    obj.setPps((PpsEntry) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "loadSize":
                    obj.setLoadSize((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "year":
                        return true;
                case "pps":
                        return true;
                case "title":
                        return true;
                case "loadSize":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "year":
                    return true;
                case "pps":
                    return true;
                case "title":
                    return true;
                case "loadSize":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "year":
                    return EppYearEducationProcess.class;
                case "pps":
                    return PpsEntry.class;
                case "title":
                    return String.class;
                case "loadSize":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UlsuNonEduLoad> _dslPath = new Path<UlsuNonEduLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UlsuNonEduLoad");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad#getYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> year()
    {
        return _dslPath.year();
    }

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad#getPps()
     */
    public static PpsEntry.Path<PpsEntry> pps()
    {
        return _dslPath.pps();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Кол-во часов. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad#getLoadSize()
     */
    public static PropertyPath<Long> loadSize()
    {
        return _dslPath.loadSize();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad#getLoadSizeAsDouble()
     */
    public static SupportedPropertyPath<Double> loadSizeAsDouble()
    {
        return _dslPath.loadSizeAsDouble();
    }

    public static class Path<E extends UlsuNonEduLoad> extends EntityPath<E>
    {
        private EppYearEducationProcess.Path<EppYearEducationProcess> _year;
        private PpsEntry.Path<PpsEntry> _pps;
        private PropertyPath<String> _title;
        private PropertyPath<Long> _loadSize;
        private SupportedPropertyPath<Double> _loadSizeAsDouble;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad#getYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> year()
        {
            if(_year == null )
                _year = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_YEAR, this);
            return _year;
        }

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad#getPps()
     */
        public PpsEntry.Path<PpsEntry> pps()
        {
            if(_pps == null )
                _pps = new PpsEntry.Path<PpsEntry>(L_PPS, this);
            return _pps;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UlsuNonEduLoadGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Кол-во часов. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad#getLoadSize()
     */
        public PropertyPath<Long> loadSize()
        {
            if(_loadSize == null )
                _loadSize = new PropertyPath<Long>(UlsuNonEduLoadGen.P_LOAD_SIZE, this);
            return _loadSize;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad#getLoadSizeAsDouble()
     */
        public SupportedPropertyPath<Double> loadSizeAsDouble()
        {
            if(_loadSizeAsDouble == null )
                _loadSizeAsDouble = new SupportedPropertyPath<Double>(UlsuNonEduLoadGen.P_LOAD_SIZE_AS_DOUBLE, this);
            return _loadSizeAsDouble;
        }

        public Class getEntityClass()
        {
            return UlsuNonEduLoad.class;
        }

        public String getEntityName()
        {
            return "ulsuNonEduLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getLoadSizeAsDouble();
}
