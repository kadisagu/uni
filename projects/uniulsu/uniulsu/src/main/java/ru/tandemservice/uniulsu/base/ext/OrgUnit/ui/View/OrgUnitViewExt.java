/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.CathedraTab.UlsuLoadCathedraTab;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uniulsu" + OrgUnitViewExtUI.class.getSimpleName();

    // таб "Нагрузка УлГУ"
    public static final String ORG_UNIT_LOAD_TAB = "ulsuOrgUnitLoadTab";

    @Autowired
    private OrgUnitView _orgUnitView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_orgUnitView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, OrgUnitViewExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab(ORG_UNIT_LOAD_TAB, UlsuLoadCathedraTab.class)
                        .parameters("mvel:['orgUnitId':presenter.orgUnit.id]")
//                        .visible("addon:" + ADDON_NAME + ".visibleLoadTab")
                        .permissionKey("ui:secModel.orgUnit_viewUlsuLoadTab"))
                .create();
    }
}