/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class UlsuFirstCourseImEduGroupViewWrapper extends ViewWrapper<UlsuFirstCourseImEduGroup>
{
    public static final String RESULT = "result";
    public static final String TIME_RULE = "timeRule";

    public UlsuFirstCourseImEduGroupViewWrapper(UlsuFirstCourseImEduGroup firstCourseImEduGroup, Double result, List<String> timeRuleList)
    {
        super(firstCourseImEduGroup);
        setViewProperty(RESULT, result);
        setViewProperty(TIME_RULE, timeRuleList);
    }
}