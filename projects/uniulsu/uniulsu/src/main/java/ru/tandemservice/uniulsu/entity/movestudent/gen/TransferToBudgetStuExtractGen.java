package ru.tandemservice.uniulsu.entity.movestudent.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О переводе на бюджетную основу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferToBudgetStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract";
    public static final String ENTITY_NAME = "transferToBudgetStuExtract";
    public static final int VERSION_HASH = -1897561340;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_PREV_YOUNGERS_ORDER_DATE = "prevYoungersOrderDate";
    public static final String P_PREV_YOUNGERS_ORDER_NUMBER = "prevYoungersOrderNumber";

    private Student _student;     // Студент
    private Date _transferDate;     // Дата перевода на бюджетную основу
    private StudentStatus _studentStatusOld;     // Состояние студента
    private Date _prevYoungersOrderDate;     // Дата предыдущего приказа студента младшего курса
    private String _prevYoungersOrderNumber;     // Номер предыдущего приказа студента младшего курса

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Дата перевода на бюджетную основу. Свойство не может быть null.
     */
    @NotNull
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата перевода на бюджетную основу. Свойство не может быть null.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return Состояние студента.
     */
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Состояние студента.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Дата предыдущего приказа студента младшего курса.
     */
    public Date getPrevYoungersOrderDate()
    {
        return _prevYoungersOrderDate;
    }

    /**
     * @param prevYoungersOrderDate Дата предыдущего приказа студента младшего курса.
     */
    public void setPrevYoungersOrderDate(Date prevYoungersOrderDate)
    {
        dirty(_prevYoungersOrderDate, prevYoungersOrderDate);
        _prevYoungersOrderDate = prevYoungersOrderDate;
    }

    /**
     * @return Номер предыдущего приказа студента младшего курса.
     */
    @Length(max=255)
    public String getPrevYoungersOrderNumber()
    {
        return _prevYoungersOrderNumber;
    }

    /**
     * @param prevYoungersOrderNumber Номер предыдущего приказа студента младшего курса.
     */
    public void setPrevYoungersOrderNumber(String prevYoungersOrderNumber)
    {
        dirty(_prevYoungersOrderNumber, prevYoungersOrderNumber);
        _prevYoungersOrderNumber = prevYoungersOrderNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferToBudgetStuExtractGen)
        {
            setStudent(((TransferToBudgetStuExtract)another).getStudent());
            setTransferDate(((TransferToBudgetStuExtract)another).getTransferDate());
            setStudentStatusOld(((TransferToBudgetStuExtract)another).getStudentStatusOld());
            setPrevYoungersOrderDate(((TransferToBudgetStuExtract)another).getPrevYoungersOrderDate());
            setPrevYoungersOrderNumber(((TransferToBudgetStuExtract)another).getPrevYoungersOrderNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferToBudgetStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferToBudgetStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferToBudgetStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "student":
                    return obj.getStudent();
                case "transferDate":
                    return obj.getTransferDate();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "prevYoungersOrderDate":
                    return obj.getPrevYoungersOrderDate();
                case "prevYoungersOrderNumber":
                    return obj.getPrevYoungersOrderNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "prevYoungersOrderDate":
                    obj.setPrevYoungersOrderDate((Date) value);
                    return;
                case "prevYoungersOrderNumber":
                    obj.setPrevYoungersOrderNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "student":
                        return true;
                case "transferDate":
                        return true;
                case "studentStatusOld":
                        return true;
                case "prevYoungersOrderDate":
                        return true;
                case "prevYoungersOrderNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "student":
                    return true;
                case "transferDate":
                    return true;
                case "studentStatusOld":
                    return true;
                case "prevYoungersOrderDate":
                    return true;
                case "prevYoungersOrderNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "student":
                    return Student.class;
                case "transferDate":
                    return Date.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "prevYoungersOrderDate":
                    return Date.class;
                case "prevYoungersOrderNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferToBudgetStuExtract> _dslPath = new Path<TransferToBudgetStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferToBudgetStuExtract");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Дата перевода на бюджетную основу. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return Состояние студента.
     * @see ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Дата предыдущего приказа студента младшего курса.
     * @see ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract#getPrevYoungersOrderDate()
     */
    public static PropertyPath<Date> prevYoungersOrderDate()
    {
        return _dslPath.prevYoungersOrderDate();
    }

    /**
     * @return Номер предыдущего приказа студента младшего курса.
     * @see ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract#getPrevYoungersOrderNumber()
     */
    public static PropertyPath<String> prevYoungersOrderNumber()
    {
        return _dslPath.prevYoungersOrderNumber();
    }

    public static class Path<E extends TransferToBudgetStuExtract> extends ModularStudentExtract.Path<E>
    {
        private Student.Path<Student> _student;
        private PropertyPath<Date> _transferDate;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<Date> _prevYoungersOrderDate;
        private PropertyPath<String> _prevYoungersOrderNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Дата перевода на бюджетную основу. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(TransferToBudgetStuExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return Состояние студента.
     * @see ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Дата предыдущего приказа студента младшего курса.
     * @see ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract#getPrevYoungersOrderDate()
     */
        public PropertyPath<Date> prevYoungersOrderDate()
        {
            if(_prevYoungersOrderDate == null )
                _prevYoungersOrderDate = new PropertyPath<Date>(TransferToBudgetStuExtractGen.P_PREV_YOUNGERS_ORDER_DATE, this);
            return _prevYoungersOrderDate;
        }

    /**
     * @return Номер предыдущего приказа студента младшего курса.
     * @see ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract#getPrevYoungersOrderNumber()
     */
        public PropertyPath<String> prevYoungersOrderNumber()
        {
            if(_prevYoungersOrderNumber == null )
                _prevYoungersOrderNumber = new PropertyPath<String>(TransferToBudgetStuExtractGen.P_PREV_YOUNGERS_ORDER_NUMBER, this);
            return _prevYoungersOrderNumber;
        }

        public Class getEntityClass()
        {
            return TransferToBudgetStuExtract.class;
        }

        public String getEntityName()
        {
            return "transferToBudgetStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
