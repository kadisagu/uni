/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.PpsTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.IPublisherColumnBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.uniepp.IUniEppDefines;
import ru.tandemservice.uniepp.component.edugroup.EppEduGroupPublisherLinkResolver;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.UlsuEduLoadWrapper;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.UlsuFirstCourseImEduGroupViewWrapper;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad;

import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Configuration
public class UlsuLoadPpsTab extends BusinessComponentManager
{
    public static final String EDU_LOAD_DS = "eduLoadDS";
    public static final String FIRST_COURSE_EDU_LOAD_DS = "firstCourseEduLoadDS";
    public static final String NON_EDU_LOAD_DS = "nonEduLoadDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EDU_LOAD_DS, eduLoadCL(), eduLoadDSHandler()))
                .addDataSource(searchListDS(FIRST_COURSE_EDU_LOAD_DS, firstCourseDSColumns(), firstCourseImEduGroupDSHandler()))
                .addDataSource(searchListDS(NON_EDU_LOAD_DS, nonEduLoadCL(), nonEduLoadDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduLoadCL()
    {
        IPublisherColumnBuilder eduGroupColumn = publisherColumn("eduGroupTitle", EppRealEduGroup.title()).publisherLinkResolver(new EppEduGroupPublisherLinkResolver()
        {
            @Override protected Long getOrgUnitId(IEntity entity) { return null; }
            @Override public String getComponentName(IEntity entity){ return "ru.tandemservice.uniepp.component.edugroup.pub.GlobalGroupPub"; }
        });

        IPublisherColumnBuilder registryElementPartColumn = publisherColumn("registryElementPartTitle", EppRealEduGroup.activityPart().title()).publisherLinkResolver(new IPublisherLinkResolver()
        {
            @Override public Object getParameters(IEntity entity)
            {
                @SuppressWarnings("unchecked") EppRealEduGroup eduGroup = ((ViewWrapper<EppRealEduGroup>) entity).getEntity();
                return Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, eduGroup.getActivityPart().getRegistryElement().getId());
            }

            @Override public String getComponentName(IEntity entity){ return IUniEppDefines.EPP_DISCIPLINE_REGISTRY_PUB; }
        });

        return columnListExtPointBuilder(EDU_LOAD_DS)
                .addColumn(eduGroupColumn)
                .addColumn(registryElementPartColumn)
                .addColumn(textColumn("type", EppRealEduGroup.type().title()))
                .addColumn(textColumn("students", UlsuEduLoadWrapper.STUDENTS))
                .addColumn(textColumn("loadSize", UlsuEduLoadWrapper.LOAD_SIZE).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
                .addColumn(textColumn("result", UlsuEduLoadWrapper.RESULT).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
                .addColumn(textColumn("timeRule", UlsuEduLoadWrapper.TIME_RULE).formatter(RowCollectionFormatter.INSTANCE))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditEduLoadDistribution").permissionKey("employeePost_editLoadDistribution"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEduLoadDistribution").permissionKey("employeePost_deleteLoadDistribution"))
                .create();
    }

    @Bean
    public ColumnListExtPoint firstCourseDSColumns()
    {
        return columnListExtPointBuilder(FIRST_COURSE_EDU_LOAD_DS)
                .addColumn(textColumn("title", UlsuFirstCourseImEduGroup.title()))
                .addColumn(textColumn("type", UlsuFirstCourseImEduGroup.type().title()))
                .addColumn(textColumn("students", UlsuFirstCourseImEduGroup.students()))
                .addColumn(textColumn("loadSize", UlsuFirstCourseImEduGroup.hoursAsDouble()).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
                .addColumn(textColumn("result", UlsuFirstCourseImEduGroupViewWrapper.RESULT).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
                .addColumn(textColumn("timeRule", UlsuFirstCourseImEduGroupViewWrapper.TIME_RULE).formatter(RowCollectionFormatter.INSTANCE))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditFirstCourseImEduGroup").permissionKey("employeePost_editFirstCourseEduLoadRecord"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteFirstCourseImEduGroup").permissionKey("employeePost_deleteFirstCourseEduLoadRecord"))
                .create();
    }

    @Bean
    public ColumnListExtPoint nonEduLoadCL()
    {
        return columnListExtPointBuilder(NON_EDU_LOAD_DS)
                .addColumn(textColumn("title", UlsuNonEduLoad.title()))
                .addColumn(textColumn("loadSize", UlsuNonEduLoad.loadSizeAsDouble()).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("employeePost_editNonEduLoadRecord"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("employeePost_deleteNonEduLoadRecord"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduLoadDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            public DSOutput execute(DSInput input, ExecutionContext context)
            {
                return ListOutputBuilder.get(input, context.<List>get("eduLoad")).pageable(true).build();
            }
        };
    }

    @Bean
    public IDefaultSearchDataSourceHandler firstCourseImEduGroupDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            public DSOutput execute(DSInput input, ExecutionContext context)
            {
                return ListOutputBuilder.get(input, context.<List>get("firstCourseEduLoad")).pageable(true).build();
            }
        };
    }

    @Bean
    public IDefaultSearchDataSourceHandler nonEduLoadDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            public DSOutput execute(DSInput input, ExecutionContext context)
            {
                return ListOutputBuilder.get(input, context.<List>get("nonEduLoad")).pageable(true).build();
            }
        };
    }
}