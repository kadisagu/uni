package ru.tandemservice.uniulsu.entity.catalog;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniulsu.entity.catalog.gen.*;

/**
 * Нормы времени (УлГУ)
 */
public class UlsuLoadTimeRule extends UlsuLoadTimeRuleGen
{
    @EntityDSLSupport
    @Override
    public Double getCoefficientAsDouble()
    {
        return UniEppUtils.wrap(this.getCoefficient());
    }

    public void setCoefficientAsDouble(Double coefficientAsDouble)
    {
        this.setCoefficient(UniEppUtils.unwrap(coefficientAsDouble));
    }
}