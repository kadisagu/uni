package ru.tandemservice.uniulsu.entity.eduPlanLoad;

import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.gen.*;

/**
 * Распределение часов УГС
 */
public class UlsuEduGroupPpsDistr extends UlsuEduGroupPpsDistrGen
{
    public UlsuEduGroupPpsDistr()
    {

    }

    public UlsuEduGroupPpsDistr(EppPpsCollectionItem ppsGroup)
    {
        this.setPpsGroup(ppsGroup);
    }
}