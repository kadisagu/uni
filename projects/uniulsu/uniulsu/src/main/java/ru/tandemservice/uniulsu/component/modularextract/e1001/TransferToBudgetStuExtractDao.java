/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniulsu.component.modularextract.e1001;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract;

import java.util.Map;

/**
 *
 * @author iolshvang
 * @since 01.06.11 16:47
 */
public class TransferToBudgetStuExtractDao extends UniBaseDao implements IExtractComponentDao<TransferToBudgetStuExtract>
{
    //студент 1-го курса переводится в состояние "отчислен",
    //студент старшего курса переводится на форму возмещения затрат "бюджет"
    @Override
    public void doCommit(TransferToBudgetStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        //save rollback data
        Student student = extract.getEntity();
        extract.setStudentStatusOld(extract.getStudent().getStatus());

        //set new values
        student.setCompensationType(getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET));
        extract.getStudent().setStatus(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;

        // указываем даты приказов студента
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getChangeCompensationTypeOrderDate());
            extract.setPrevOrderNumber(orderData.getChangeCompensationTypeOrderNumber());
        }
        orderData.setChangeCompensationTypeOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setChangeCompensationTypeOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);

        orderData = get(OrderData.class, OrderData.L_STUDENT, extract.getStudent());
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(extract.getStudent());
        }
        else
        {
            extract.setPrevYoungersOrderDate(orderData.getChangeCompensationTypeOrderDate());
            extract.setPrevYoungersOrderNumber(orderData.getChangeCompensationTypeOrderNumber());
        }
        orderData.setChangeCompensationTypeOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setChangeCompensationTypeOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);

        getSession().saveOrUpdate(student);
        getSession().saveOrUpdate(extract.getStudent());
    }

    @Override
    public void doRollback(TransferToBudgetStuExtract extract, Map parameters)
    {
        Student student = extract.getEntity();
        student.setCompensationType(getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT));

        extract.getStudent().setStatus(extract.getStudentStatusOld());
        extract.getStudent().setCompensationType(getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET));

        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, extract.getEntity());
        orderData.setChangeCompensationTypeOrderDate(extract.getPrevOrderDate());
        orderData.setChangeCompensationTypeOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);

        OrderData youngersOrderData = get(OrderData.class, OrderData.L_STUDENT, extract.getStudent());
        youngersOrderData.setChangeCompensationTypeOrderDate(extract.getPrevYoungersOrderDate());
        youngersOrderData.setChangeCompensationTypeOrderNumber(extract.getPrevYoungersOrderNumber());
        getSession().saveOrUpdate(youngersOrderData);

        getSession().saveOrUpdate(student);
        getSession().saveOrUpdate(extract.getStudent());
    }
}
