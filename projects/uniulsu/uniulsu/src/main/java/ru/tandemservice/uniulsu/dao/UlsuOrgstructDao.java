/* $Id: ProductBoxOrgstructDao.java 5483 2008-11-28 12:06:00Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniulsu.dao;

import ru.tandemservice.uni.dao.importer.OrgstructImportsDao;

/**
 */
public class UlsuOrgstructDao extends OrgstructImportsDao implements IUlsuOrgstructDao
{
    public static final String[][] ORG_UNIT_TYPES_HIERARCHY = {
    };
    
    public static final String[][] ORGUNIT_TYPE_TO_KIND_RELATIONS = {
    };
    
    public static final String[][] ORGSTRUCT_HIERARCHY = {
    };

    @Override
    protected String[][] getOrgstructHierarchy() {
        return ORGSTRUCT_HIERARCHY;
    }

    @Override
    protected String[][] getOrgUnitTypesHierarchy() {
        return ORG_UNIT_TYPES_HIERARCHY;
    }

    @Override
    protected String[][] getOrgUnitTypeToKindRelations() {
        return ORGUNIT_TYPE_TO_KIND_RELATIONS;
    }
}