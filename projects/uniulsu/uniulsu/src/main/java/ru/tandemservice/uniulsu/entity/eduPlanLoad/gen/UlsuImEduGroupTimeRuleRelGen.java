package ru.tandemservice.uniulsu.entity.eduPlanLoad.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.IUlsuImEduGroup;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuImEduGroupTimeRuleRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь нормы времени и планируемой УГС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UlsuImEduGroupTimeRuleRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuImEduGroupTimeRuleRel";
    public static final String ENTITY_NAME = "ulsuImEduGroupTimeRuleRel";
    public static final int VERSION_HASH = 1953892914;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_GROUP = "eduGroup";
    public static final String L_TIME_RULE = "timeRule";

    private IUlsuImEduGroup _eduGroup;     // Планируемая УГС
    private UlsuLoadTimeRule _timeRule;     // Норма времени

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Планируемая УГС. Свойство не может быть null.
     */
    @NotNull
    public IUlsuImEduGroup getEduGroup()
    {
        return _eduGroup;
    }

    /**
     * @param eduGroup Планируемая УГС. Свойство не может быть null.
     */
    public void setEduGroup(IUlsuImEduGroup eduGroup)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && eduGroup!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IUlsuImEduGroup.class);
            IEntityMeta actual =  eduGroup instanceof IEntity ? EntityRuntime.getMeta((IEntity) eduGroup) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_eduGroup, eduGroup);
        _eduGroup = eduGroup;
    }

    /**
     * @return Норма времени. Свойство не может быть null.
     */
    @NotNull
    public UlsuLoadTimeRule getTimeRule()
    {
        return _timeRule;
    }

    /**
     * @param timeRule Норма времени. Свойство не может быть null.
     */
    public void setTimeRule(UlsuLoadTimeRule timeRule)
    {
        dirty(_timeRule, timeRule);
        _timeRule = timeRule;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UlsuImEduGroupTimeRuleRelGen)
        {
            setEduGroup(((UlsuImEduGroupTimeRuleRel)another).getEduGroup());
            setTimeRule(((UlsuImEduGroupTimeRuleRel)another).getTimeRule());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UlsuImEduGroupTimeRuleRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UlsuImEduGroupTimeRuleRel.class;
        }

        public T newInstance()
        {
            return (T) new UlsuImEduGroupTimeRuleRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduGroup":
                    return obj.getEduGroup();
                case "timeRule":
                    return obj.getTimeRule();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduGroup":
                    obj.setEduGroup((IUlsuImEduGroup) value);
                    return;
                case "timeRule":
                    obj.setTimeRule((UlsuLoadTimeRule) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduGroup":
                        return true;
                case "timeRule":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduGroup":
                    return true;
                case "timeRule":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduGroup":
                    return IUlsuImEduGroup.class;
                case "timeRule":
                    return UlsuLoadTimeRule.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UlsuImEduGroupTimeRuleRel> _dslPath = new Path<UlsuImEduGroupTimeRuleRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UlsuImEduGroupTimeRuleRel");
    }
            

    /**
     * @return Планируемая УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuImEduGroupTimeRuleRel#getEduGroup()
     */
    public static IUlsuImEduGroupGen.Path<IUlsuImEduGroup> eduGroup()
    {
        return _dslPath.eduGroup();
    }

    /**
     * @return Норма времени. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuImEduGroupTimeRuleRel#getTimeRule()
     */
    public static UlsuLoadTimeRule.Path<UlsuLoadTimeRule> timeRule()
    {
        return _dslPath.timeRule();
    }

    public static class Path<E extends UlsuImEduGroupTimeRuleRel> extends EntityPath<E>
    {
        private IUlsuImEduGroupGen.Path<IUlsuImEduGroup> _eduGroup;
        private UlsuLoadTimeRule.Path<UlsuLoadTimeRule> _timeRule;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Планируемая УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuImEduGroupTimeRuleRel#getEduGroup()
     */
        public IUlsuImEduGroupGen.Path<IUlsuImEduGroup> eduGroup()
        {
            if(_eduGroup == null )
                _eduGroup = new IUlsuImEduGroupGen.Path<IUlsuImEduGroup>(L_EDU_GROUP, this);
            return _eduGroup;
        }

    /**
     * @return Норма времени. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuImEduGroupTimeRuleRel#getTimeRule()
     */
        public UlsuLoadTimeRule.Path<UlsuLoadTimeRule> timeRule()
        {
            if(_timeRule == null )
                _timeRule = new UlsuLoadTimeRule.Path<UlsuLoadTimeRule>(L_TIME_RULE, this);
            return _timeRule;
        }

        public Class getEntityClass()
        {
            return UlsuImEduGroupTimeRuleRel.class;
        }

        public String getEntityName()
        {
            return "ulsuImEduGroupTimeRuleRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
