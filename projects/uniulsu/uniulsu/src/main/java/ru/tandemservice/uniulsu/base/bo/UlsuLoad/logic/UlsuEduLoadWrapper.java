/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

import java.util.Comparator;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class UlsuEduLoadWrapper extends ViewWrapper<EppRealEduGroup>
{
    public static final String STUDENTS = "students";
    public static final String LOAD_SIZE = "loadSize";
    public static final String RESULT = "result";
    public static final String TIME_RULE = "timeRule";

    public static final Comparator<UlsuEduLoadWrapper> COMPARATOR = new Comparator<UlsuEduLoadWrapper>()
    {
        @Override
        public int compare(UlsuEduLoadWrapper o1, UlsuEduLoadWrapper o2)
        {
            int result = o1.getEntity().getTitle().compareTo(o2.getEntity().getTitle());

            if (result == 0){ result = o1.getEntity().getActivityPart().getTitle().compareTo(o2.getEntity().getActivityPart().getTitle()); }
            if (result == 0){ result = o1.getEntity().getType().getClass().getSimpleName().compareTo(o2.getEntity().getType().getClass().getSimpleName()); }

            return result;
        }
    };

    public UlsuEduLoadWrapper(EppRealEduGroup eduGroup, Integer students, Double loadSize, Double result, List<String> timeRuleList)
    {
        super(eduGroup);
        setViewProperty(RESULT, result);
        setViewProperty(TIME_RULE, timeRuleList);
        setViewProperty(LOAD_SIZE, loadSize);
        setViewProperty(STUDENTS, students);
    }
}