/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.IUlsuLoadDAO;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.UlsuLoadDAO;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Configuration
public class UlsuLoadManager extends BusinessObjectManager
{
    public static UlsuLoadManager instance()
    {
        return instance(UlsuLoadManager.class);
    }

    @Bean
    public IUlsuLoadDAO dao()
    {
        return new UlsuLoadDAO();
    }
}