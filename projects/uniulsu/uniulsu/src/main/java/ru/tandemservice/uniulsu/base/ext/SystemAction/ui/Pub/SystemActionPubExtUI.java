/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.uniepp.dao.group.EppRealGroupRowDAO;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.UlsuLoadManager;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class SystemActionPubExtUI extends UIAddon
{
    public SystemActionPubExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickCreateNextYearEduGroups()
    {
        synchronized (SystemActionPubExtUI.class)
        {
            UlsuLoadManager.instance().dao().createNextYearSummaries();
            EppRealGroupRowDAO.DAEMON.wakeUpAndWaitDaemon(5 * 60);
        }
    }
}