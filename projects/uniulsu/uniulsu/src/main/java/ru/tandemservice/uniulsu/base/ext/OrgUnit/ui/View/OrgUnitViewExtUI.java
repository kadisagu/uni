/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.ext.OrgUnit.ui.View;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitViewUI;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.UlsuLoadManager;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class OrgUnitViewExtUI extends UIAddon
{
    public OrgUnitViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public boolean isVisibleLoadTab()
    {
        OrgUnitViewUI presenter = (OrgUnitViewUI) this.getPresenter();
        return UlsuLoadManager.instance().dao().isVisibleOrgUnitLoadTab(presenter.getOrgUnit().getId());
    }
}