/**
 *$Id$
 */
package ru.tandemservice.uniulsu.component.catalog.ulsuLoadTimeRule.UlsuLoadTimeRuleItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubController;
import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class Controller extends DefaultCatalogItemPubController<UlsuLoadTimeRule, Model, IDAO>
{
}