/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.util.UlsuPpsLoadDataWrapper;
import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public interface IUlsuLoadDAO extends ICommonDAO, INeedPersistenceSupport
{
    /**
     * Проверяет, доступна ли вкладка "Нагрузка ППС (УлГУ)" на подразделении
     * @param orgUnitId id подразделения
     * @return доступность вкладки
     */
    public boolean isVisibleOrgUnitLoadTab(Long orgUnitId);

    /**
     * Возвращает следующий учебный год (после текущего).
     * @return следующий учебный год, null если следующий не задан
     */
    public EppYearEducationProcess getNextYear();

    /**
     * Удаляет созданные сводки контингента на следующий год, создает новые.
     */
    public void createNextYearSummaries();

    /**
     *  Обновляет статус УГС следующего года.
     */
    public void updateNextYearEduGroupsCompleteLevel();

    /**
     * Возвращает справочник с нагрузкой ППС.
     * @param orgUnitId id подразделения
     * @param ppsIds id ППС, null если нужны все
     * @return справочник с нагрузкой ППС
     */
    public Map<Long, UlsuPpsLoadDataWrapper> getPpsLoadDataWrapperMap(Long orgUnitId, List<Long> ppsIds);

    /**
     * Возвращает информаицю о нагрузке преподавателя.
     * @param ppsId id преподавателя
     * @return информацию о нагрузке преподавателя
     */
    public UlsuPpsLoadDataWrapper getPpsLoadDataWrapper(Long ppsId);

    /**
     * Удаляет учебную группу преподавателя.
     * @param ppsId id преподавателя
     * @param eduGroupId id группы
     */
    public void deleteEduGroupDistr(Long ppsId, Long eduGroupId);

    /**
     * Сохраняет выбранные нормы времени, которые следует учитывать при подсчете нагрузки данной планируемой УГС.
     * @param eduGroupId id планируемой УГС УлГУ
     * @param timeRules список норм времени
     */
    public void saveUlsuImEduGroupTimeRules(Long eduGroupId, Collection<UlsuLoadTimeRule> timeRules);
}