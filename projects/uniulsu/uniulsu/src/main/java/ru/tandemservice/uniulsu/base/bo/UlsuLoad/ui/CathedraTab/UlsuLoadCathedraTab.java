/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.CathedraTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.UlsuPpsLoadWrapper;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Configuration
public class UlsuLoadCathedraTab extends BusinessComponentManager
{
    public static final String PPS_DS = "ppsDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PPS_DS, ppsCL(), ppsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint ppsCL()
    {
        return columnListExtPointBuilder(PPS_DS)
                .addColumn(
                        publisherColumn("title", PpsEntry.title())
                                .businessComponent(EmployeePostView.class)
                                .primaryKeyPath(EmployeePost4PpsEntry.employeePost().id())
                                .primaryKeyAsParameter(PublisherActivator.PUBLISHER_ID_KEY)
                                .parameters("ui:parameters"))
                .addColumn(textColumn("eduLoadSize", UlsuPpsLoadWrapper.EDU_LOAD).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).width("10%"))
                .addColumn(textColumn("nonEduLoadSize", UlsuPpsLoadWrapper.NON_EDU_LOAD).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).width("10%"))
                .addColumn(textColumn("totalLoadSize", UlsuPpsLoadWrapper.TOTAL_LOAD).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).width("10%"))
                .create();
    }


    @Bean
    public IDefaultSearchDataSourceHandler ppsDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            public DSOutput execute(DSInput input, ExecutionContext context)
            {
                return ListOutputBuilder.get(input, context.<List>get("ppsWrappers")).pageable(true).build();
            }
        };
    }
}