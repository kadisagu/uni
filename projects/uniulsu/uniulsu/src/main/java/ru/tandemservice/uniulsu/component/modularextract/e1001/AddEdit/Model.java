/* $Id: Model.java.vm 11769 2010-03-05 07:11:49Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniulsu.component.modularextract.e1001.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract;

/**
 * @author iolshvang
 * @since 01.06.2011
 */
public class Model extends CommonModularStudentExtractAddEditModel<TransferToBudgetStuExtract>
{
    private ISelectModel _studentModel;

    public ISelectModel getStudentModel()
    {
        return _studentModel;
    }

    public void setStudentModel(ISelectModel studentModel)
    {
        _studentModel = studentModel;
    }
}
