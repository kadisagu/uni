\fi500\ql\fs24{extractNumber}.\par
1п. {fio}, шифр {studentPersonalFileNumber}, {student_A} {lowCourse} курса {lowDevelopForm_G}{lowDevelopCondition_G} формы обучения {lowOrgUnit_G}, направления подготовки «{lowSpeciality} ({lowEducationLevel})», группы {lowGroup}, {lowCompensationType_G} основы исключить из списков студентов {lowCourse} курса с {fromDate}г.\par\par
2п. {fio}, шифр {studentPersonalFileNumber}, {student_A} {course} курса {developForm_G}{developCondition_G} формы обучения {orgUnit_G}, направления подготовки «{speciality} ({educationLevel})», группы {group}, {compensationType_G} основы перевести на бюджетную основу с {fromDate}г. в связи с поступлением на {lowCourse} курс.\par\par

Основание: {listBasics}.
