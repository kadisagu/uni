package ru.tandemservice.uniulsu.entity.eduPlanLoad.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.IUlsuImEduGroup;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.IUlsuImEduGroup;

/**
 * Планируемая УГС (УлГУ)
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IUlsuImEduGroupGen extends InterfaceStubBase
 implements IUlsuImEduGroup{
    public static final int VERSION_HASH = 1233093390;

    public static final String P_HOURS = "hours";
    public static final String P_STUDENTS = "students";

    private long _hours;
    private int _students;

    @NotNull

    public long getHours()
    {
        return _hours;
    }

    public void setHours(long hours)
    {
        _hours = hours;
    }

    @NotNull

    public int getStudents()
    {
        return _students;
    }

    public void setStudents(int students)
    {
        _students = students;
    }

    private static final Path<IUlsuImEduGroup> _dslPath = new Path<IUlsuImEduGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.uniulsu.entity.eduPlanLoad.IUlsuImEduGroup");
    }
            

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.IUlsuImEduGroup#getHours()
     */
    public static PropertyPath<Long> hours()
    {
        return _dslPath.hours();
    }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.IUlsuImEduGroup#getStudents()
     */
    public static PropertyPath<Integer> students()
    {
        return _dslPath.students();
    }

    public static class Path<E extends IUlsuImEduGroup> extends EntityPath<E>
    {
        private PropertyPath<Long> _hours;
        private PropertyPath<Integer> _students;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.IUlsuImEduGroup#getHours()
     */
        public PropertyPath<Long> hours()
        {
            if(_hours == null )
                _hours = new PropertyPath<Long>(IUlsuImEduGroupGen.P_HOURS, this);
            return _hours;
        }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.IUlsuImEduGroup#getStudents()
     */
        public PropertyPath<Integer> students()
        {
            if(_students == null )
                _students = new PropertyPath<Integer>(IUlsuImEduGroupGen.P_STUDENTS, this);
            return _students;
        }

        public Class getEntityClass()
        {
            return IUlsuImEduGroup.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.uniulsu.entity.eduPlanLoad.IUlsuImEduGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
