package ru.tandemservice.uniulsu.entity.eduPlanLoad;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.gen.*;

/**
 * Внеучебная нагрузка
 */
public class UlsuNonEduLoad extends UlsuNonEduLoadGen
{
    @EntityDSLSupport
    @Override
    public Double getLoadSizeAsDouble()
    {
        return UniEppUtils.wrap(this.getLoadSize());
    }

    public void setLoadSizeAsDouble(Double loadSize)
    {
        this.setLoadSize(UniEppUtils.unwrap(loadSize));
    }
}