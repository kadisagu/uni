/**
 *$Id$
 */
package ru.tandemservice.uniulsu.component.catalog.ulsuLoadTimeRule.UlsuLoadTimeRuleAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuImEduGroupLoadBase;

import java.util.Arrays;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class Model extends DefaultCatalogAddEditModel<UlsuLoadTimeRule>
{
    private List<UlsuImEduGroupLoadBase> _baseOptions = Arrays.asList(UlsuImEduGroupLoadBase.CONSTANT, UlsuImEduGroupLoadBase.STUDENTS, UlsuImEduGroupLoadBase.HOURS);
    public List<UlsuImEduGroupLoadBase> getBaseOptions(){ return _baseOptions; }
}