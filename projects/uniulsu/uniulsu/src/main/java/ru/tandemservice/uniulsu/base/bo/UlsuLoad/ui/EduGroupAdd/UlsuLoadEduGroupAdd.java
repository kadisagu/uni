/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.EduGroupAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.EduGroupDSHandler;
import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Configuration
public class UlsuLoadEduGroupAdd extends BusinessComponentManager
{
    public static final String EDU_GROUP_DS = "eduGroupDS";
    public static final String TIME_RULE_DS = "timeRuleDS";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDU_GROUP_DS, eduGroupDSHandler())
                        .addColumn("title")
                        .addColumn(EppRealEduGroup.type().title().s()))
                .addDataSource(selectDS(TIME_RULE_DS, timeRuleDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduGroupDSHandler()
    {
        return new EduGroupDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler timeRuleDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UlsuLoadTimeRule.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                return super.query(alias, filter).where(likeUpper(property(alias, UlsuLoadTimeRule.title()), value(CoreStringUtils.escapeLike(filter)))).order(property(alias, UlsuLoadTimeRule.title()));
            }
        };
    }
}