/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.FirstCourseImEduGroupAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Configuration
public class UlsuLoadFirstCourseImEduGroupAddEdit extends BusinessComponentManager
{
    public static final String WORK_PLAN_DS = "workPlanDS";
    public static final String WORK_PLAN_ROW_DS = "workPlanRowDS";
    public static final String GROUP_TYPE_DS = "groupTypeDS";
    public static final String TIME_RULE_DS = "timeRuleDS";

    public static final String WORK_PLAN = "workPlan";
    public static final String WORK_PLAN_ROW = "workPlanRow";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(WORK_PLAN_DS, workPlanDSHandler()))
                .addDataSource(selectDS(WORK_PLAN_ROW_DS, workPlanRowDSHandler()))
                .addDataSource(selectDS(GROUP_TYPE_DS, groupTypeDSHandler()))
                .addDataSource(selectDS(TIME_RULE_DS, timeRuleDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler workPlanDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppWorkPlan.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                return super.query(alias, filter)
                        .where(likeUpper(
                                DQLFunctions.concat(
                                        value("№"),
                                        property(alias, EppWorkPlan.number()),
                                        value("Семестр"),
                                        property(alias, EppWorkPlan.term().title()),
                                        property(alias, EppWorkPlan.year().educationYear().title())),
                                 value(CoreStringUtils.escapeLike(filter, true))));
            }

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(eq(minus(
                        property(alias, EppWorkPlan.year().educationYear().intValue()), value(1)),
                        new DQLSelectBuilder()
                                .fromEntity(EducationYear.class, "ey")
                                .column(property("ey", EducationYear.intValue()))
                                .where(eq(property("ey", EducationYear.current()), value(Boolean.TRUE)))
                                .buildQuery()));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler workPlanRowDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppWorkPlanRegistryElementRow.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                return super.query(alias, filter).where(likeUpper(property(alias, EppWorkPlanRegistryElementRow.title()), value(CoreStringUtils.escapeLike(filter))));
            }

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long workPlanId = context.get(WORK_PLAN);
                dql.where(eq(property(alias, EppWorkPlanRegistryElementRow.workPlan().id()), value(workPlanId)));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler groupTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppGroupType.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                return super.query(alias, filter).where(likeUpper(property(alias, EppGroupType.title()), value(CoreStringUtils.escapeLike(filter))));
            }

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Long workPlanRowId = context.get(WORK_PLAN_ROW);
                IDQLSelectableQuery loadQuery = new DQLSelectBuilder()
                        .fromEntity(EppRegistryModuleALoad.class, "ml")
                        .column(property("ml", EppRegistryModuleALoad.loadType().eppGroupType().code()))
                        .joinEntity("ml", DQLJoinType.inner, EppRegistryElementPartModule.class, "repm", eq(property("repm", EppRegistryElementPartModule.module()), property("ml", EppRegistryModuleALoad.module())))
                        .joinEntity("repm", DQLJoinType.inner, EppWorkPlanRegistryElementRow.class, "wpr", eq(property("wpr", EppWorkPlanRegistryElementRow.registryElementPart()), property("repm", EppRegistryElementPartModule.part())))
                        .where(eq(property("wpr", EppWorkPlanRegistryElementRow.id()), value(workPlanRowId)))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery();

                IDQLSelectableQuery actionQuery = new DQLSelectBuilder()
                        .fromEntity(EppRegistryElementPartFControlAction.class, "pca")
                        .column(property("pca", EppRegistryElementPartFControlAction.controlAction().eppGroupType().code()))
                        .joinEntity("pca", DQLJoinType.inner, EppWorkPlanRegistryElementRow.class, "wpr", eq(property("wpr", EppWorkPlanRegistryElementRow.registryElementPart()), property("pca", EppRegistryElementPartFControlAction.part())))
                        .where(eq(property("wpr", EppWorkPlanRegistryElementRow.id()), value(workPlanRowId)))
                        .predicate(DQLPredicateType.distinct)
                        .buildQuery();

                dql.where(or(
                        in(property(alias, EppGroupType.code()), loadQuery),
                        in(property(alias, EppGroupType.code()), actionQuery)
                ));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler timeRuleDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UlsuLoadTimeRule.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                return super.query(alias, filter).where(likeUpper(property(alias, UlsuLoadTimeRule.title()), value(CoreStringUtils.escapeLike(filter)))).order(property(alias, UlsuLoadTimeRule.title()));
            }
        };
    }
}