/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.NonEduLoadRecordAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.UlsuLoadManager;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.IUlsuLoadDAO;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Input({
    @Bind(key = "recordId", binding = "recordId"),
    @Bind(key = "ppsId", binding = "ppsId")})
public class UlsuLoadNonEduLoadRecordAddEditUI extends UIPresenter
{
    private Long _recordId;
    private Long _ppsId;
    private UlsuNonEduLoad _record;

    public Long getRecordId(){ return _recordId; }
    public void setRecordId(Long recordId){ _recordId = recordId; }

    public Long getPpsId(){ return _ppsId; }
    public void setPpsId(Long ppsId){ _ppsId = ppsId; }

    public UlsuNonEduLoad getRecord(){ return _record; }
    public void setRecord(UlsuNonEduLoad record){ _record = record; }


    public Double getLoadSizeAsDouble(){ return _recordId == null ? null : UniEppUtils.wrap(_record.getLoadSize());}
    public void setLoadSizeAsDouble(Double load){ _record.setLoadSize(UniEppUtils.unwrap(load));}

    @Override
    public void onComponentRefresh()
    {
        IUlsuLoadDAO dao = UlsuLoadManager.instance().dao();
        if (_recordId == null)
        {
            _record = new UlsuNonEduLoad();
            _record.setPps(dao.<PpsEntry>getNotNull(_ppsId));
            _record.setYear(dao.getNextYear());

        } else
        {
            _record = dao.getNotNull(_recordId);
        }
    }

    public void onClickApply()
    {
        UlsuLoadManager.instance().dao().saveOrUpdate(_record);
        deactivate();
    }
}