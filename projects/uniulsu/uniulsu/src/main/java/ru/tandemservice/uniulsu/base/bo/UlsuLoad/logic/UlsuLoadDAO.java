/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRealEduGroupCompleteLevelCodes;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.util.UlsuPpsLoadDataWrapper;
import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class UlsuLoadDAO extends CommonDAO implements IUlsuLoadDAO
{
    @Override
    public boolean isVisibleOrgUnitLoadTab(Long orgUnitId)
    {
        for (OrgUnitToKindRelation rel: getList(OrgUnitToKindRelation.class, OrgUnitToKindRelation.orgUnit().id(), orgUnitId))
        {
            if (rel.isAllowAddStudent() || rel.isAllowAddGroup())
            {
                return true;
            }
        }

        return false;
    }

    @Override
    public EppYearEducationProcess getNextYear()
    {
        EducationYear currentYear;
        EducationYear nextYear;
        EppYearEducationProcess result;

        if ((currentYear = get(EducationYear.class, EducationYear.current(), Boolean.TRUE)) == null || (nextYear = get(EducationYear.class, EducationYear.intValue(), currentYear.getIntValue() + 1)) == null || (result = getByNaturalId(new EppYearEducationProcess.NaturalId(nextYear))) == null)
        {
            return null;
        }

        return result;
    }

    @Override
    public void createNextYearSummaries()
    {
        EppYearEducationProcess nextYear = getNextYear();
        if (nextYear == null)
        {
            throw new ApplicationException("Не задан следующий учебный год.");
        }

        IDQLSelectableQuery summaryIdQuery = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroupSummary.class, "egs")
                .column(property("egs", EppRealEduGroupSummary.id()))
                .where(eq(property("egs", EppRealEduGroupSummary.yearPart().year()), value(nextYear)))
                .buildQuery();

        new DQLDeleteBuilder(EppRealEduGroup4ActionType.class).where(in(property(EppRealEduGroup4ActionType.summary().id()), summaryIdQuery)).createStatement(getSession()).execute();
        new DQLDeleteBuilder(EppRealEduGroup4LoadType.class).where(in(property(EppRealEduGroup4ActionType.summary().id()), summaryIdQuery)).createStatement(getSession()).execute();
        new DQLDeleteBuilder(EppRealEduGroupSummary.class).where(in(property(EppRealEduGroupSummary.id()), summaryIdQuery)).createStatement(getSession()).execute();

        List<YearDistributionPart> distributionParts = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, "swp")
                .joinEntity("swp", DQLJoinType.inner, EppWorkPlan.class, "wp", eq(property("wp", EppWorkPlan.id()), property("swp", EppStudent2WorkPlan.workPlan().id())))
                .joinPath(DQLJoinType.inner, EppWorkPlan.parent().eduPlanVersion().eduPlan().developGrid().fromAlias("wp"), "d")
                .joinEntity("wp", DQLJoinType.inner, DevelopGridTerm.class, "dgt", and(
                        eq(property("dgt", DevelopGridTerm.developGrid().id()), property("d", DevelopGrid.id())),
                        eq(property("dgt", DevelopGridTerm.term()), property("wp", EppWorkPlan.term()))))
                .column(property("dgt", DevelopGridTerm.part()))
                .predicate(DQLPredicateType.distinct)
                .where(eq(property("wp", EppWorkPlan.year()), value(nextYear)))
                .createStatement(getSession())
                .list();

        OrgUnit academy = TopOrgUnit.getInstance();
        for (YearDistributionPart part: distributionParts)
        {
            EppYearPart yearPart = getByNaturalId(new EppYearPart.NaturalId(nextYear, part));
            EppRealEduGroupSummary summary = new EppRealEduGroupSummary(academy, yearPart);
            summary.setModificationDate(new Date());

            save(summary);
        }
    }

    @Override
    public void updateNextYearEduGroupsCompleteLevel()
    {
        EppRealEduGroupCompleteLevel operationOrgUnitTakeCompleteLevel = getByNaturalId(new EppRealEduGroupCompleteLevel.NaturalId(EppRealEduGroupCompleteLevelCodes.LEVEL_OPERATION_ORG_UNIT_TAKED));
        new DQLUpdateBuilder(EppRealEduGroup.class)
                .set(EppRealEduGroup.level().s(), value(operationOrgUnitTakeCompleteLevel))
                .where(eq(property(EppRealEduGroup.summary().yearPart().year()), value(getNextYear())))
                .createStatement(getSession())
                .execute();
    }

    @Override
    public Map<Long, UlsuPpsLoadDataWrapper> getPpsLoadDataWrapperMap(Long orgUnitId, List<Long> ppsIds)
    {
        Map<Long, UlsuPpsLoadDataWrapper> result = new HashMap<>();
        if (ppsIds == null)
        {
            if (orgUnitId == null)
            {
                throw new IllegalStateException("orgUnitId is null");
            }

            ppsIds = getList(new DQLSelectBuilder().fromEntity(PpsEntry.class, "p").where(eq(property("p", PpsEntry.orgUnit().id()), value(orgUnitId))).column(property("p.id")));
        }

        EppYearEducationProcess nextYear = getNextYear();
        for (Long ppsId: ppsIds)
        {
            Long totalEduLoad = 0L;
            List<UlsuEduLoadWrapper> eduLoadWrappers = new ArrayList<>();
            DQLSelectBuilder ppsEduGroupsBuilder = new DQLSelectBuilder()
                    .fromEntity(EppPpsCollectionItem.class, "ci")
                    .joinEntity("ci", DQLJoinType.inner, EppRealEduGroup.class, "reg", eq(property("reg", EppRealEduGroup.id()), property("ci", EppPpsCollectionItem.list())))
                    .column(property("reg", EppRealEduGroup.id()))
                    .where(eq(property("ci", EppPpsCollectionItem.pps().id()), value(ppsId)))
                    .where(eq(property("reg", EppRealEduGroup.summary().yearPart().year()), value(nextYear)));


            Map<Long, Integer> groupStudentsMap = new HashMap<>();
            Map<Long, Long> groupHoursMap = new HashMap<>();
            Map<Long, Long> eduGroup2Distribution = new HashMap<>();

            for (UlsuEduGroupPpsDistr distr: getList(UlsuEduGroupPpsDistr.class, UlsuEduGroupPpsDistr.ppsGroup().pps().id(), ppsId))
            {
                Long eduGrouoId = distr.getPpsGroup().getList().getId();
                groupHoursMap.put(eduGrouoId, distr.getHours());
                groupStudentsMap.put(eduGrouoId, distr.getStudents());

                eduGroup2Distribution.put(distr.getPpsGroup().getList().getId(), distr.getId());
            }

            DQLSelectBuilder loadTypePartBuilder = new DQLSelectBuilder()
                    .fromEntity(EppPpsCollectionItem.class, "ci")
                    .joinEntity("ci", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "lt", eq(property("lt", EppRealEduGroup4LoadType.id()), property("ci", EppPpsCollectionItem.list().id())))
                    .column(property("lt", EppRealEduGroup4LoadType.activityPart().id()))
                    .predicate(DQLPredicateType.distinct)
                    .where(in(property("lt", EppRealEduGroup4LoadType.id()), ppsEduGroupsBuilder.buildQuery()));

            Session session = getSession();

            Map<EppRegistryElementPart, Map<EppALoadType, Long>> loadSizePartMap = SafeMap.get(HashMap.class);
            DQLSelectBuilder loadSizeBuilder = new DQLSelectBuilder()
                    .fromEntity(EppRegistryElementPartModule.class, "repm")
                    .joinEntity("repm", DQLJoinType.inner, EppRegistryModuleALoad.class, "rmal", eq(property("rmal", EppRegistryModuleALoad.module()), property("repm", EppRegistryElementPartModule.module())))
                    .column(property("repm", EppRegistryElementPartModule.part()))
                    .column(property("rmal", EppRegistryModuleALoad.loadType()))
                    .column(property("rmal", EppRegistryModuleALoad.load()))
                    .where(in(property("repm", EppRegistryElementPartModule.part().id()), loadTypePartBuilder.buildQuery()));

            for (Object[] row: loadSizeBuilder.createStatement(session).<Object[]>list())
            {
                EppRegistryElementPart part = (EppRegistryElementPart) row[0];
                EppALoadType loadType = (EppALoadType) row[1];
                Long loadSize = (Long) row[2];

                Long oldLoadSize = loadSizePartMap.get(part).get(loadType);

                loadSizePartMap.get(part).put(loadType, loadSize + (oldLoadSize == null ? 0L : oldLoadSize));
            }

            DQLSelectBuilder loadEduGroupsBuilder = new DQLSelectBuilder()
                    .fromEntity(EppRealEduGroup4LoadType.class, "lt")
                    .where(in(property("lt", EppRealEduGroup4LoadType.id()), ppsEduGroupsBuilder.buildQuery()));

            for (EppRealEduGroup4LoadType eduGroup: loadEduGroupsBuilder.createStatement(session).<EppRealEduGroup4LoadType>list())
            {
                Integer studentsSize = groupStudentsMap.get(eduGroup.getId());
                Long load = groupHoursMap.get(eduGroup.getId());
                if (studentsSize == null || load == null)
                {
                    eduLoadWrappers.add(new UlsuEduLoadWrapper(eduGroup, null, null, 0.0d, Collections.<String>emptyList()));
                    continue;
                }

                Long eduLoad = 0L;
                Set<String> timeRules = new TreeSet<>();
                for (UlsuLoadTimeRule timeRule: new DQLSelectBuilder().fromEntity(UlsuImEduGroupTimeRuleRel.class, "rel").column(property("rel", UlsuImEduGroupTimeRuleRel.timeRule())).where(eq(property("rel", UlsuImEduGroupTimeRuleRel.eduGroup()), value(eduGroup2Distribution.get(eduGroup.getId())))).createStatement(session).<UlsuLoadTimeRule>list())
                {
                    boolean students = timeRule.getBase().equals(UlsuImEduGroupLoadBase.STUDENTS);
                    boolean hours = timeRule.getBase().equals(UlsuImEduGroupLoadBase.HOURS);

                    eduLoad += ((Number) (timeRule.getCoefficient() * (students ? studentsSize : hours ? UniEppUtils.wrap(load) : 1L))).longValue();
                    timeRules.add(timeRule.getTitle());
                }

                eduLoadWrappers.add(new UlsuEduLoadWrapper(eduGroup, studentsSize, UniEppUtils.wrap(load), UniEppUtils.wrap(eduLoad), new ArrayList<String>(timeRules)));
                totalEduLoad += eduLoad;
            }

            DQLSelectBuilder controlBuilder = new DQLSelectBuilder()
                    .fromEntity(EppRealEduGroup4ActionType.class, "g4a")
                    .where(in(property("g4a", EppRealEduGroup4ActionType.id()), ppsEduGroupsBuilder.buildQuery()));

            for (EppRealEduGroup4ActionType actionType: controlBuilder.createStatement(session).<EppRealEduGroup4ActionType>list())
            {
                Integer studentsSize = groupStudentsMap.get(actionType.getId());
                Long load = groupHoursMap.get(actionType.getId());
                if (studentsSize == null || load == null)
                {
                    eduLoadWrappers.add(new UlsuEduLoadWrapper(actionType, null, null, 0.0d, Collections.<String>emptyList()));
                    continue;
                }

                Long eduLoad = 0L;
                Set<String> timeRules = new TreeSet<>();
                for (UlsuLoadTimeRule timeRule: new DQLSelectBuilder().fromEntity(UlsuImEduGroupTimeRuleRel.class, "rel").column(property("rel", UlsuImEduGroupTimeRuleRel.timeRule())).where(eq(property("rel", UlsuImEduGroupTimeRuleRel.eduGroup()), value(eduGroup2Distribution.get(actionType.getId())))).createStatement(session).<UlsuLoadTimeRule>list())
                {
                    boolean students = timeRule.getBase().equals(UlsuImEduGroupLoadBase.STUDENTS);
                    boolean hours = timeRule.getBase().equals(UlsuImEduGroupLoadBase.HOURS);

                    eduLoad += ((Number) (timeRule.getCoefficient() * (students ? studentsSize : hours ? UniEppUtils.wrap(load) : 1L))).longValue();
                    timeRules.add(timeRule.getTitle());
                }


                eduLoadWrappers.add(new UlsuEduLoadWrapper(actionType, studentsSize, UniEppUtils.wrap(load), UniEppUtils.wrap(eduLoad), new ArrayList<String>(timeRules)));
                totalEduLoad += eduLoad;
            }

            Collections.sort(eduLoadWrappers, UlsuEduLoadWrapper.COMPARATOR);


            /********************************************************************************************************************************/
            /********************************************************************************************************************************/
            /********************************************************************************************************************************/
            /********************************************************************************************************************************/
            /********************************************************************************************************************************/


            List<UlsuNonEduLoad> nonEduLoadList = new DQLSelectBuilder()
                    .fromEntity(UlsuNonEduLoad.class, "nel")
                    .where(eq(property("nel", UlsuNonEduLoad.pps()), value(ppsId)))
                    .where(eq(property("nel", UlsuNonEduLoad.year()), value(nextYear)))
                    .createStatement(session).list();

            Long totalNonEduLoad = 0L;
            for (UlsuNonEduLoad nonEduLoad: nonEduLoadList)
            {
                totalNonEduLoad += nonEduLoad.getLoadSize();
            }

            /********************************************************************************************************************************/
            /********************************************************************************************************************************/
            /********************************************************************************************************************************/
            /********************************************************************************************************************************/
            /********************************************************************************************************************************/

            Long totalFirstCourseEduLoad = 0L;
            List<UlsuFirstCourseImEduGroup> firstCourseImEduGroups = new DQLSelectBuilder()
                    .fromEntity(UlsuFirstCourseImEduGroup.class, "fc")
                    .column(property("fc"))
//                    .joinEntity("fc", DQLJoinType.inner, EppWorkPlan.class, "wp", eq(property("wp", EppWorkPlan.id()), property("fc", UlsuFirstCourseImEduGroup.row().workPlan().id())))
                    .joinPath(DQLJoinType.inner, UlsuFirstCourseImEduGroup.row().workPlan().fromAlias("fc"), "wpb")
                    .joinEntity("wpb", DQLJoinType.inner, EppWorkPlan.class, "wp", eq(property("wp", EppWorkPlan.id()), property("wpb", EppWorkPlanBase.id())))
                    .where(eq(property("fc", UlsuFirstCourseImEduGroup.pps().id()), value(ppsId)))

                    .where(eq(property("wp", EppWorkPlan.year()), value(nextYear)))
                    .createStatement(session).list();

            List<UlsuFirstCourseImEduGroupViewWrapper> firstCourseImEduGroupViewWrappers = new ArrayList<>();
            for (UlsuFirstCourseImEduGroup firstCourseImEduGroup: firstCourseImEduGroups)
            {
                Long eduLoad = 0L;
                Set<String> timeRules = new TreeSet<>();
                for (UlsuLoadTimeRule timeRule: new DQLSelectBuilder().fromEntity(UlsuImEduGroupTimeRuleRel.class, "rel").column(property("rel", UlsuImEduGroupTimeRuleRel.timeRule())).where(eq(property("rel", UlsuImEduGroupTimeRuleRel.eduGroup()), value(firstCourseImEduGroup))).createStatement(session).<UlsuLoadTimeRule>list())
                {
                    boolean students = timeRule.getBase().equals(UlsuImEduGroupLoadBase.STUDENTS);
                    boolean hours = timeRule.getBase().equals(UlsuImEduGroupLoadBase.HOURS);

                    eduLoad += ((Number) (timeRule.getCoefficient() * (students ? firstCourseImEduGroup.getStudents() : hours ? firstCourseImEduGroup.getHoursAsDouble() : 1L))).longValue();
                    timeRules.add(timeRule.getTitle());
                }

                firstCourseImEduGroupViewWrappers.add(new UlsuFirstCourseImEduGroupViewWrapper(firstCourseImEduGroup, UniEppUtils.wrap(eduLoad), new ArrayList<>(timeRules)));
                totalFirstCourseEduLoad += eduLoad;
            }

            result.put(ppsId, new UlsuPpsLoadDataWrapper(eduLoadWrappers, nonEduLoadList, firstCourseImEduGroupViewWrappers, totalEduLoad, totalNonEduLoad, totalFirstCourseEduLoad));
        }

        return result;
    }

    @Override
    public UlsuPpsLoadDataWrapper getPpsLoadDataWrapper(Long ppsId)
    {
        return getPpsLoadDataWrapperMap(null, Collections.singletonList(ppsId)).get(ppsId);
    }

    @Override
    public void deleteEduGroupDistr(Long ppsId, Long eduGroupId)
    {
        new DQLDeleteBuilder(EppPpsCollectionItem.class).where(eq(property(EppPpsCollectionItem.pps().id()), value(ppsId))).where(eq(property(EppPpsCollectionItem.list().id()), value(eduGroupId))).createStatement(getSession()).execute();
    }

    @Override
    public void saveUlsuImEduGroupTimeRules(Long eduGroupId, Collection<UlsuLoadTimeRule> timeRules)
    {
        IUlsuImEduGroup eduGroup = get(eduGroupId);
        Session session = getSession();
        new DQLDeleteBuilder(UlsuImEduGroupTimeRuleRel.class).where(eq(property(UlsuImEduGroupTimeRuleRel.eduGroup().id()), value(eduGroupId))).createStatement(session).execute();

        for (UlsuLoadTimeRule timeRule: timeRules)
        {
            session.save(new UlsuImEduGroupTimeRuleRel(eduGroup, timeRule));
        }
    }
}