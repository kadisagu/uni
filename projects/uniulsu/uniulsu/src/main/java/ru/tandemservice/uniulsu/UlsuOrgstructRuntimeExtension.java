/* $Id: ProductUniOrgstructRuntimeExtension.java 5483 2008-11-28 12:06:00Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniulsu;

import org.tandemframework.core.runtime.IRuntimeExtension;
import ru.tandemservice.uniulsu.dao.IUlsuOrgstructDao;

/**
 */
public class UlsuOrgstructRuntimeExtension implements IRuntimeExtension {

    private IUlsuOrgstructDao _dao;
    public void setDao(IUlsuOrgstructDao dao) {
        this._dao = dao;
    }

    @Override
    public void init(Object object) {
        _dao.createOrgstructHierarhy();
        _dao.createOrgUnitToKindRelations();
        _dao.createOrgUnitsStructure();
    }

    @Override
    public void destroy() {
    }
}