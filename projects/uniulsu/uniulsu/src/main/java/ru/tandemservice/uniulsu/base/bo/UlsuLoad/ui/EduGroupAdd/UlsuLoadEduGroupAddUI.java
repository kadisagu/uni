/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.EduGroupAdd;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.UlsuLoadManager;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.IUlsuLoadDAO;
import ru.tandemservice.uniulsu.entity.catalog.UlsuLoadTimeRule;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuEduGroupPpsDistr;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuImEduGroupTimeRuleRel;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Input({
    @Bind(key = "ppsId", binding = "ppsId", required = true),
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduGroupId")
})
public class UlsuLoadEduGroupAddUI extends UIPresenter
{
    private static final IdentifiableWrapper<IEntity> SOURCE_OWNER_ORG_UNIT = new IdentifiableWrapper<>(1L, "С читающего подразделения");
    private static final IdentifiableWrapper<IEntity> SOURCE_ALL_ORG_UNIT = new IdentifiableWrapper<>(2L, "Со всех подразделений");

    private static final List<IdentifiableWrapper<IEntity>> SOURCE_FILTER_LIST = Arrays.asList(
        SOURCE_OWNER_ORG_UNIT,
        SOURCE_ALL_ORG_UNIT
    );

    private Long _ppsId;
    private Long _eduGroupId;
    private EppRealEduGroup _eduGroup;
    private IEntity _sourceFilter;

    private Integer _students;
    private Double _loadSize;

    private Integer _maxStudents;
    private Double _maxLoadSize;

    private Collection<UlsuLoadTimeRule> _timeRules;
    private PpsEntry _pps;

    public Long getPpsId(){ return _ppsId; }
    public void setPpsId(Long ppsId){ _ppsId = ppsId; }

    public Long getEduGroupId(){ return _eduGroupId; }
    public void setEduGroupId(Long eduGroupId){ _eduGroupId = eduGroupId; }

    public IEntity getSourceFilter(){ return _sourceFilter; }
    public void setSourceFilter(IEntity sourceFilter){ _sourceFilter = sourceFilter;}

    public Integer getStudents(){ return _students; }
    public void setStudents(Integer students){ _students = students; }

    public Double getLoadSize(){ return _loadSize; }
    public void setLoadSize(Double loadSize){ _loadSize = loadSize; }

    public Collection<UlsuLoadTimeRule> getTimeRules(){ return _timeRules; }
    public void setTimeRules(Collection<UlsuLoadTimeRule> timeRules){ _timeRules = timeRules; }


    public List<IdentifiableWrapper<IEntity>> getSourceFilterList(){ return SOURCE_FILTER_LIST; }
    public boolean isEditForm(){ return _eduGroupId != null; }
    public boolean isAddForm(){ return !isEditForm(); }


    @Override
    public void onComponentRefresh()
    {
        IUlsuLoadDAO dao = UlsuLoadManager.instance().dao();
        _pps = dao.get(PpsEntry.class, _ppsId);
        if (isEditForm())
        {
            _eduGroup = dao.get(_eduGroupId);
        }

        onClickSelectEduGroup();
        if (isEditForm())
        {
            EppPpsCollectionItem ppsEduGroup = dao.getByNaturalId(new EppPpsCollectionItem.NaturalId(_eduGroup, _pps));
            UlsuEduGroupPpsDistr distr = dao.getByNaturalId(new UlsuEduGroupPpsDistr.NaturalId(ppsEduGroup));
            _students = distr.getStudents();
            _loadSize = UniEppUtils.wrap(distr.getHours());

            List<UlsuImEduGroupTimeRuleRel> timeRuleRels = UlsuLoadManager.instance().dao().getList(UlsuImEduGroupTimeRuleRel.class, UlsuImEduGroupTimeRuleRel.eduGroup().id(), distr.getId(), UlsuImEduGroupTimeRuleRel.timeRule().title().s());
            Transformer<UlsuImEduGroupTimeRuleRel, UlsuLoadTimeRule> transformer = UlsuImEduGroupTimeRuleRel::getTimeRule;

            _timeRules = CollectionUtils.collect(timeRuleRels, transformer);
        }

        boolean owner = isAddForm() || _eduGroup.getActivityPart().getRegistryElement().getOwner().getId().equals(_pps.getOrgUnit().getId());
        _sourceFilter = owner ? SOURCE_OWNER_ORG_UNIT : SOURCE_ALL_ORG_UNIT;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(UlsuLoadEduGroupAdd.EDU_GROUP_DS))
        {
            dataSource.put("ppsId", _ppsId);
            dataSource.put("orgUnitId", _sourceFilter == SOURCE_OWNER_ORG_UNIT ? _pps.getOrgUnit().getId() : null);
            if (isEditForm()){ dataSource.put("current", _eduGroup.getId()); }
        }
    }

    public void onClickApply()
    {
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        if (_students > _maxStudents)
        {
            errorCollector.add("В выбранной учебной группе не распределено " + _maxStudents + " студентов. Укажите число студентов не превосходящее данное.", "students");
        }

        if (_loadSize > _maxLoadSize)
        {
            errorCollector.add("В выбранной учебной группе не распределено " + _maxLoadSize + " часов. Укажите число часов не превосходящее данное.", "hours");
        }

        if (errorCollector.hasErrors())
        {
            return;
        }

        IUlsuLoadDAO dao = UlsuLoadManager.instance().dao();

        EppPpsCollectionItem ppsGroup = dao.getByNaturalId(new EppPpsCollectionItem.NaturalId(_eduGroup, _pps));
        if (ppsGroup == null)
        {
            ppsGroup = new EppPpsCollectionItem(_eduGroup, _pps);
            dao.save(ppsGroup);
        }

        UlsuEduGroupPpsDistr distr = dao.getByNaturalId(new UlsuEduGroupPpsDistr.NaturalId(ppsGroup));
        if (distr == null)
        {
            distr = new UlsuEduGroupPpsDistr(ppsGroup);
        }

        distr.setStudents(_students);
        distr.setHours(UniEppUtils.unwrap(_loadSize));

        dao.saveOrUpdate(distr);
        dao.saveUlsuImEduGroupTimeRules(distr.getId(), _timeRules);
        deactivate();
    }

    public void onClickChangeSourceFilter()
    {
        if (_sourceFilter == SOURCE_OWNER_ORG_UNIT && _eduGroup != null && !_eduGroup.getActivityPart().getRegistryElement().getOwner().getId().equals(_pps.getOrgUnit().getId()))
        {
            _eduGroup = null;
            _students = null;
            _loadSize = null;
        }
    }

    public void onClickSelectEduGroup()
    {
        if (_eduGroup != null)
        {
            IUlsuLoadDAO dao = UlsuLoadManager.instance().dao();
            Map<Long, PairKey<Integer, Long>> ppsStudentsAndLoadMap = new HashMap<>();
            for (UlsuEduGroupPpsDistr distr: dao.getList(UlsuEduGroupPpsDistr.class, UlsuEduGroupPpsDistr.ppsGroup().list().id(), _eduGroup.getId()))
            {
                ppsStudentsAndLoadMap.put(distr.getPpsGroup().getPps().getId(), PairKey.create(distr.getStudents(), distr.getHours()));
            }

            ppsStudentsAndLoadMap.remove(_ppsId);

            Integer totalStudents = dao.getCount(EppRealEduGroupRow.class, EppRealEduGroupRow.group().id().s(), _eduGroup.getId());
            Long totalLoadSize = 0L;

            DQLSelectBuilder moduleLoadBuilder = new DQLSelectBuilder()
                    .fromEntity(EppRegistryModuleALoad.class, "ml")
                    .joinEntity("ml", DQLJoinType.inner, EppRegistryElementPartModule.class, "repm", eq(property("repm", EppRegistryElementPartModule.module()), property("ml", EppRegistryModuleALoad.module())))
                    .where(eq(property("repm", EppRegistryElementPartModule.part()), value(_eduGroup.getActivityPart())))
                    .where(eq(property("ml", EppRegistryModuleALoad.loadType().eppGroupType()), value(_eduGroup.getType())))
                    .column(property("ml", EppRegistryModuleALoad.load()));

            for (Long load : dao.<Long>getList(moduleLoadBuilder))
            {
                totalLoadSize += load;
            }

            for (Map.Entry<Long, PairKey<Integer, Long>> ppsEntry: ppsStudentsAndLoadMap.entrySet())
            {
                PairKey<Integer, Long> key = ppsEntry.getValue();
                totalStudents -= key.getFirst();
                totalLoadSize -= key.getSecond();
            }

            _students = _maxStudents = totalStudents;
            _loadSize = _maxLoadSize = UniEppUtils.wrap(totalLoadSize);

        } else
        {
            _students = null;
            _loadSize = null;
        }
    }
}
