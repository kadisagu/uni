\fi500\ql\fs24{extractNumber}. {studentFIO_G}, шифр {studentPersonalFileNumber}, {student_D} {course} курса {developForm_G} {developCondition_G} формы
 обучения {orgUnit_G}, направления подготовки «{speciality} ({educationLevel})», группы {group},
 {compensationTypeStr_G_Alt} перенести сроки сдачи государственного экзамена с {currentDate} г. на {newDate} г. \par

Основание: {listBasics}.
