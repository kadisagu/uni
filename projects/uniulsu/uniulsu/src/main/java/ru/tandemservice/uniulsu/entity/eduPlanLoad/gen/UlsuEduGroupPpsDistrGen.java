package ru.tandemservice.uniulsu.entity.eduPlanLoad.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.IUlsuImEduGroup;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuEduGroupPpsDistr;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Распределение часов УГС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UlsuEduGroupPpsDistrGen extends EntityBase
 implements IUlsuImEduGroup, INaturalIdentifiable<UlsuEduGroupPpsDistrGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuEduGroupPpsDistr";
    public static final String ENTITY_NAME = "ulsuEduGroupPpsDistr";
    public static final int VERSION_HASH = 1666605485;
    private static IEntityMeta ENTITY_META;

    public static final String L_PPS_GROUP = "ppsGroup";
    public static final String P_HOURS = "hours";
    public static final String P_STUDENTS = "students";

    private EppPpsCollectionItem _ppsGroup;     // Учебная группа преподавателя
    private long _hours;     // Часы
    private int _students;     // Студенты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебная группа преподавателя. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppPpsCollectionItem getPpsGroup()
    {
        return _ppsGroup;
    }

    /**
     * @param ppsGroup Учебная группа преподавателя. Свойство не может быть null и должно быть уникальным.
     */
    public void setPpsGroup(EppPpsCollectionItem ppsGroup)
    {
        dirty(_ppsGroup, ppsGroup);
        _ppsGroup = ppsGroup;
    }

    /**
     * @return Часы. Свойство не может быть null.
     */
    @NotNull
    public long getHours()
    {
        return _hours;
    }

    /**
     * @param hours Часы. Свойство не может быть null.
     */
    public void setHours(long hours)
    {
        dirty(_hours, hours);
        _hours = hours;
    }

    /**
     * @return Студенты. Свойство не может быть null.
     */
    @NotNull
    public int getStudents()
    {
        return _students;
    }

    /**
     * @param students Студенты. Свойство не может быть null.
     */
    public void setStudents(int students)
    {
        dirty(_students, students);
        _students = students;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UlsuEduGroupPpsDistrGen)
        {
            if (withNaturalIdProperties)
            {
                setPpsGroup(((UlsuEduGroupPpsDistr)another).getPpsGroup());
            }
            setHours(((UlsuEduGroupPpsDistr)another).getHours());
            setStudents(((UlsuEduGroupPpsDistr)another).getStudents());
        }
    }

    public INaturalId<UlsuEduGroupPpsDistrGen> getNaturalId()
    {
        return new NaturalId(getPpsGroup());
    }

    public static class NaturalId extends NaturalIdBase<UlsuEduGroupPpsDistrGen>
    {
        private static final String PROXY_NAME = "UlsuEduGroupPpsDistrNaturalProxy";

        private Long _ppsGroup;

        public NaturalId()
        {}

        public NaturalId(EppPpsCollectionItem ppsGroup)
        {
            _ppsGroup = ((IEntity) ppsGroup).getId();
        }

        public Long getPpsGroup()
        {
            return _ppsGroup;
        }

        public void setPpsGroup(Long ppsGroup)
        {
            _ppsGroup = ppsGroup;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UlsuEduGroupPpsDistrGen.NaturalId) ) return false;

            UlsuEduGroupPpsDistrGen.NaturalId that = (NaturalId) o;

            if( !equals(getPpsGroup(), that.getPpsGroup()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPpsGroup());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPpsGroup());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UlsuEduGroupPpsDistrGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UlsuEduGroupPpsDistr.class;
        }

        public T newInstance()
        {
            return (T) new UlsuEduGroupPpsDistr();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "ppsGroup":
                    return obj.getPpsGroup();
                case "hours":
                    return obj.getHours();
                case "students":
                    return obj.getStudents();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "ppsGroup":
                    obj.setPpsGroup((EppPpsCollectionItem) value);
                    return;
                case "hours":
                    obj.setHours((Long) value);
                    return;
                case "students":
                    obj.setStudents((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "ppsGroup":
                        return true;
                case "hours":
                        return true;
                case "students":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "ppsGroup":
                    return true;
                case "hours":
                    return true;
                case "students":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "ppsGroup":
                    return EppPpsCollectionItem.class;
                case "hours":
                    return Long.class;
                case "students":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UlsuEduGroupPpsDistr> _dslPath = new Path<UlsuEduGroupPpsDistr>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UlsuEduGroupPpsDistr");
    }
            

    /**
     * @return Учебная группа преподавателя. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuEduGroupPpsDistr#getPpsGroup()
     */
    public static EppPpsCollectionItem.Path<EppPpsCollectionItem> ppsGroup()
    {
        return _dslPath.ppsGroup();
    }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuEduGroupPpsDistr#getHours()
     */
    public static PropertyPath<Long> hours()
    {
        return _dslPath.hours();
    }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuEduGroupPpsDistr#getStudents()
     */
    public static PropertyPath<Integer> students()
    {
        return _dslPath.students();
    }

    public static class Path<E extends UlsuEduGroupPpsDistr> extends EntityPath<E>
    {
        private EppPpsCollectionItem.Path<EppPpsCollectionItem> _ppsGroup;
        private PropertyPath<Long> _hours;
        private PropertyPath<Integer> _students;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебная группа преподавателя. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuEduGroupPpsDistr#getPpsGroup()
     */
        public EppPpsCollectionItem.Path<EppPpsCollectionItem> ppsGroup()
        {
            if(_ppsGroup == null )
                _ppsGroup = new EppPpsCollectionItem.Path<EppPpsCollectionItem>(L_PPS_GROUP, this);
            return _ppsGroup;
        }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuEduGroupPpsDistr#getHours()
     */
        public PropertyPath<Long> hours()
        {
            if(_hours == null )
                _hours = new PropertyPath<Long>(UlsuEduGroupPpsDistrGen.P_HOURS, this);
            return _hours;
        }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuEduGroupPpsDistr#getStudents()
     */
        public PropertyPath<Integer> students()
        {
            if(_students == null )
                _students = new PropertyPath<Integer>(UlsuEduGroupPpsDistrGen.P_STUDENTS, this);
            return _students;
        }

        public Class getEntityClass()
        {
            return UlsuEduGroupPpsDistr.class;
        }

        public String getEntityName()
        {
            return "ulsuEduGroupPpsDistr";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
