package ru.tandemservice.uniulsu.entity.eduPlanLoad;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.gen.*;

/**
 * Планируемая УГС первого курса (УлГУ)
 */
public class UlsuFirstCourseImEduGroup extends UlsuFirstCourseImEduGroupGen
{
    @EntityDSLSupport
    @Override
    public Double getHoursAsDouble()
    {
        return UniEppUtils.wrap(this.getHours());
    }

    public void setHoursAsDouble(Double loadSize)
    {
        this.setHours(UniEppUtils.unwrap(loadSize));
    }
}