package ru.tandemservice.uniulsu.entity.eduPlanLoad.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.IUlsuImEduGroup;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Планируемая УГС первого курса (УлГУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UlsuFirstCourseImEduGroupGen extends EntityBase
 implements IUlsuImEduGroup{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup";
    public static final String ENTITY_NAME = "ulsuFirstCourseImEduGroup";
    public static final int VERSION_HASH = 437277279;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String L_ROW = "row";
    public static final String L_ACTIVITY_PART = "activityPart";
    public static final String L_TYPE = "type";
    public static final String L_PPS = "pps";
    public static final String P_HOURS = "hours";
    public static final String P_STUDENTS = "students";
    public static final String P_HOURS_AS_DOUBLE = "hoursAsDouble";

    private String _title;     // Название
    private EppWorkPlanRegistryElementRow _row;     // Запись РУП
    private EppRegistryElementPart _activityPart;     // Часть элемента реестра
    private EppGroupType _type;     // Обобщенный вид нагрузки
    private PpsEntry _pps;     // ППС
    private long _hours;     // Часы
    private int _students;     // Студенты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Запись РУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkPlanRegistryElementRow getRow()
    {
        return _row;
    }

    /**
     * @param row Запись РУП. Свойство не может быть null.
     */
    public void setRow(EppWorkPlanRegistryElementRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getActivityPart()
    {
        return _activityPart;
    }

    /**
     * @param activityPart Часть элемента реестра. Свойство не может быть null.
     */
    public void setActivityPart(EppRegistryElementPart activityPart)
    {
        dirty(_activityPart, activityPart);
        _activityPart = activityPart;
    }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     */
    @NotNull
    public EppGroupType getType()
    {
        return _type;
    }

    /**
     * @param type Обобщенный вид нагрузки. Свойство не может быть null.
     */
    public void setType(EppGroupType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return ППС. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getPps()
    {
        return _pps;
    }

    /**
     * @param pps ППС. Свойство не может быть null.
     */
    public void setPps(PpsEntry pps)
    {
        dirty(_pps, pps);
        _pps = pps;
    }

    /**
     * @return Часы. Свойство не может быть null.
     */
    @NotNull
    public long getHours()
    {
        return _hours;
    }

    /**
     * @param hours Часы. Свойство не может быть null.
     */
    public void setHours(long hours)
    {
        dirty(_hours, hours);
        _hours = hours;
    }

    /**
     * @return Студенты. Свойство не может быть null.
     */
    @NotNull
    public int getStudents()
    {
        return _students;
    }

    /**
     * @param students Студенты. Свойство не может быть null.
     */
    public void setStudents(int students)
    {
        dirty(_students, students);
        _students = students;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UlsuFirstCourseImEduGroupGen)
        {
            setTitle(((UlsuFirstCourseImEduGroup)another).getTitle());
            setRow(((UlsuFirstCourseImEduGroup)another).getRow());
            setActivityPart(((UlsuFirstCourseImEduGroup)another).getActivityPart());
            setType(((UlsuFirstCourseImEduGroup)another).getType());
            setPps(((UlsuFirstCourseImEduGroup)another).getPps());
            setHours(((UlsuFirstCourseImEduGroup)another).getHours());
            setStudents(((UlsuFirstCourseImEduGroup)another).getStudents());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UlsuFirstCourseImEduGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UlsuFirstCourseImEduGroup.class;
        }

        public T newInstance()
        {
            return (T) new UlsuFirstCourseImEduGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "row":
                    return obj.getRow();
                case "activityPart":
                    return obj.getActivityPart();
                case "type":
                    return obj.getType();
                case "pps":
                    return obj.getPps();
                case "hours":
                    return obj.getHours();
                case "students":
                    return obj.getStudents();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "row":
                    obj.setRow((EppWorkPlanRegistryElementRow) value);
                    return;
                case "activityPart":
                    obj.setActivityPart((EppRegistryElementPart) value);
                    return;
                case "type":
                    obj.setType((EppGroupType) value);
                    return;
                case "pps":
                    obj.setPps((PpsEntry) value);
                    return;
                case "hours":
                    obj.setHours((Long) value);
                    return;
                case "students":
                    obj.setStudents((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "row":
                        return true;
                case "activityPart":
                        return true;
                case "type":
                        return true;
                case "pps":
                        return true;
                case "hours":
                        return true;
                case "students":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "row":
                    return true;
                case "activityPart":
                    return true;
                case "type":
                    return true;
                case "pps":
                    return true;
                case "hours":
                    return true;
                case "students":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "row":
                    return EppWorkPlanRegistryElementRow.class;
                case "activityPart":
                    return EppRegistryElementPart.class;
                case "type":
                    return EppGroupType.class;
                case "pps":
                    return PpsEntry.class;
                case "hours":
                    return Long.class;
                case "students":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UlsuFirstCourseImEduGroup> _dslPath = new Path<UlsuFirstCourseImEduGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UlsuFirstCourseImEduGroup");
    }
            

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Запись РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getRow()
     */
    public static EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getActivityPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> activityPart()
    {
        return _dslPath.activityPart();
    }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getType()
     */
    public static EppGroupType.Path<EppGroupType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getPps()
     */
    public static PpsEntry.Path<PpsEntry> pps()
    {
        return _dslPath.pps();
    }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getHours()
     */
    public static PropertyPath<Long> hours()
    {
        return _dslPath.hours();
    }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getStudents()
     */
    public static PropertyPath<Integer> students()
    {
        return _dslPath.students();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getHoursAsDouble()
     */
    public static SupportedPropertyPath<Double> hoursAsDouble()
    {
        return _dslPath.hoursAsDouble();
    }

    public static class Path<E extends UlsuFirstCourseImEduGroup> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> _row;
        private EppRegistryElementPart.Path<EppRegistryElementPart> _activityPart;
        private EppGroupType.Path<EppGroupType> _type;
        private PpsEntry.Path<PpsEntry> _pps;
        private PropertyPath<Long> _hours;
        private PropertyPath<Integer> _students;
        private SupportedPropertyPath<Double> _hoursAsDouble;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UlsuFirstCourseImEduGroupGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Запись РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getRow()
     */
        public EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> row()
        {
            if(_row == null )
                _row = new EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getActivityPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> activityPart()
        {
            if(_activityPart == null )
                _activityPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_ACTIVITY_PART, this);
            return _activityPart;
        }

    /**
     * @return Обобщенный вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getType()
     */
        public EppGroupType.Path<EppGroupType> type()
        {
            if(_type == null )
                _type = new EppGroupType.Path<EppGroupType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getPps()
     */
        public PpsEntry.Path<PpsEntry> pps()
        {
            if(_pps == null )
                _pps = new PpsEntry.Path<PpsEntry>(L_PPS, this);
            return _pps;
        }

    /**
     * @return Часы. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getHours()
     */
        public PropertyPath<Long> hours()
        {
            if(_hours == null )
                _hours = new PropertyPath<Long>(UlsuFirstCourseImEduGroupGen.P_HOURS, this);
            return _hours;
        }

    /**
     * @return Студенты. Свойство не может быть null.
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getStudents()
     */
        public PropertyPath<Integer> students()
        {
            if(_students == null )
                _students = new PropertyPath<Integer>(UlsuFirstCourseImEduGroupGen.P_STUDENTS, this);
            return _students;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuFirstCourseImEduGroup#getHoursAsDouble()
     */
        public SupportedPropertyPath<Double> hoursAsDouble()
        {
            if(_hoursAsDouble == null )
                _hoursAsDouble = new SupportedPropertyPath<Double>(UlsuFirstCourseImEduGroupGen.P_HOURS_AS_DOUBLE, this);
            return _hoursAsDouble;
        }

        public Class getEntityClass()
        {
            return UlsuFirstCourseImEduGroup.class;
        }

        public String getEntityName()
        {
            return "ulsuFirstCourseImEduGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getHoursAsDouble();
}
