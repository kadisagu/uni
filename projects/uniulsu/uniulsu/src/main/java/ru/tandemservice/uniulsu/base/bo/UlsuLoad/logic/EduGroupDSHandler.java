/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.UlsuLoadManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class EduGroupDSHandler extends DefaultComboDataSourceHandler
{
    public EduGroupDSHandler(String ownerId)
    {
        super(ownerId, EppRealEduGroup.class);
    }

    @Override
    public void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);
        Long orgUnitId = ep.context.get("orgUnitId");
        if (orgUnitId != null)
        {
            ep.dqlBuilder.where(eq(property("e", EppRealEduGroup.activityPart().registryElement().owner().id()), value(orgUnitId)));
        }

        DQLSelectBuilder groupsBuilder = new DQLSelectBuilder()
                .fromEntity(EppPpsCollectionItem.class, "ci")
                .where(eq(property("ci", EppPpsCollectionItem.pps().id()), commonValue(ep.context.get("ppsId"))))
                .column(property("ci", EppPpsCollectionItem.list().id()));

        IDQLExpression inStatement = notIn(property("e", EppRealEduGroup.id()), groupsBuilder.buildQuery());
        Long current;
        if ((current = ep.context.get("current")) != null)
        {
            inStatement = or(inStatement, eq(property("e", EppRealEduGroup.id()), value(current)));
        }

        EppYearEducationProcess nextYear = UlsuLoadManager.instance().dao().getNextYear();
        inStatement = and(inStatement, eq(property("e", EppRealEduGroup.summary().yearPart().year()), value(nextYear)));

        ep.dqlBuilder.where(inStatement);
    }
}