/**
 *$Id$
 */
package ru.tandemservice.uniulsu.entity.eduPlanLoad;

import org.tandemframework.core.entity.IEntity;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public interface IUlsuImEduGroup extends IEntity
{
    public long getHours();

    public int getStudents();
}