/**
 *$Id$
 */
package ru.tandemservice.uniulsu.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 31.10.2013
 */
public class UlsuOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniulsu");
        config.setName("uniulsu-sec-config");
        config.setTitle("");

        ModuleGlobalGroupMeta moduleGlobalGroupMeta = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        ModuleLocalGroupMeta moduleLocalGroupMeta = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();

            ClassGroupMeta globalClassGroup = PermissionMetaUtil.createClassGroup(moduleGlobalGroupMeta, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            ClassGroupMeta localClassGroup = PermissionMetaUtil.createClassGroup(moduleLocalGroupMeta, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            // "Вкладка «Нагрузка (УлГУ)»"
            final PermissionGroupMeta permissionGroupUlsuLoadTab = PermissionMetaUtil.createPermissionGroup(config, code + "UlsuLoadPermissionGroup", "Вкладка «Нагрузка (УлГУ)»");
            PermissionMetaUtil.createPermission(permissionGroupUlsuLoadTab, "orgUnit_viewUlsuLoadTab_" + code, "Просмотр");

            PermissionMetaUtil.createGroupRelation(config, permissionGroupUlsuLoadTab.getName(), globalClassGroup.getName());
            PermissionMetaUtil.createGroupRelation(config, permissionGroupUlsuLoadTab.getName(), localClassGroup.getName());
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}