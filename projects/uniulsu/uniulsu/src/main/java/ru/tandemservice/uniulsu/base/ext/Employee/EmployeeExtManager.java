/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.ext.Employee;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Configuration
public class EmployeeExtManager extends BusinessObjectExtensionManager
{
}