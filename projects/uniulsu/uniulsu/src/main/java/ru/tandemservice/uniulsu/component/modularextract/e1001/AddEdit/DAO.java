/* $Id: DAO.java.vm 11769 2010-03-05 07:11:49Z agolubenko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniulsu.component.modularextract.e1001.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniulsu.entity.movestudent.TransferToBudgetStuExtract;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 01.06.2011
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<TransferToBudgetStuExtract, Model>
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected TransferToBudgetStuExtract createNewInstance()
    {
        return new TransferToBudgetStuExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        final TransferToBudgetStuExtract extract = model.getExtract();
        model.setStudentModel(new DQLFullCheckSelectModel(Student.titleWithFio().s())
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Student.class, alias);
                dql.where(eq(property(Student.status().active().fromAlias(alias)), value(Boolean.TRUE)));
                dql.where(eq(property(Student.archival().fromAlias(alias)), value(Boolean.FALSE)));
                dql.where(eq(property(Student.compensationType().code().fromAlias(alias)), value(UniDefines.COMPENSATION_TYPE_BUDGET)));
                dql.where(eq(property(Student.person().id().fromAlias(alias)), value(extract.getEntity().getPerson().getId())));
                dql.where(ne(property(Student.id().fromAlias(alias)), value(extract.getEntity().getId())));
                dql.where(le(property(Student.course().intValue().fromAlias(alias)), value(extract.getEntity().getCourse().getIntValue())));

                dql.joinPath(DQLJoinType.inner, Student.person().fromAlias(alias), "person");
                dql.joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "idc");
                if (StringUtils.isNotBlank(filter))
                {
                    dql.where(this.like(IdentityCard.fullFio().fromAlias("idc"), filter));
                }
                dql.order(property(IdentityCard.lastName().fromAlias("idc")));
                dql.order(property(IdentityCard.firstName().fromAlias("idc")));
                dql.order(property(IdentityCard.middleName().fromAlias("idc")));
                return dql;
            }
        });

    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        if (model.getExtract().getEntity().getCompensationType().isBudget())
            errorCollector.add("Приказ может быть сформирован только для студента, обучающегося по договору.");

        //проверям, что у студента 1 курса нет других непроведенных выписок
        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "ext")
                .column("ext.id")
                .where(eq(property("ext.entity.id"), value(model.getExtract().getStudent().getId())))
                .where(eq(property("ext.committed"), value(Boolean.FALSE)));

        if (dql.createStatement(getSession()).list().size() > 0)
            errorCollector.add("Приказ не может быть сформирован, так как студента младшего курса имеет другие непроведенные выписки.");

        if (errorCollector.hasErrors())
            return;

        super.update(model);
    }
}
