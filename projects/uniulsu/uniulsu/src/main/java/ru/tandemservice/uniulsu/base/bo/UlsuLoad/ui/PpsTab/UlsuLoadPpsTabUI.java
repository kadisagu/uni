/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.PpsTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.UlsuLoadManager;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.EduGroupAdd.UlsuLoadEduGroupAdd;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.FirstCourseImEduGroupAddEdit.UlsuLoadFirstCourseImEduGroupAddEdit;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.ui.NonEduLoadRecordAddEdit.UlsuLoadNonEduLoadRecordAddEdit;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.util.UlsuPpsLoadDataWrapper;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Input(@Bind(key = "ppsId", binding = "ppsId", required = true))
public class UlsuLoadPpsTabUI extends UIPresenter
{
    private Long _ppsId;
    public Long getPpsId(){ return _ppsId; }
    public void setPpsId(Long ppsId){ _ppsId = ppsId; }


    private UlsuPpsLoadDataWrapper _ppsData;

    public String getTotalEduLoadSize()
    {
        Double totalEduLoadSize = UniEppUtils.wrap(_ppsData.getTotalEduLoad());
        return "Всего учебной нагрузки: " + (_ppsData.getEduLoadRows().isEmpty() ? "нет" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalEduLoadSize));
    }

    public String getTotalNonEduLoadSize()
    {
        Double totalNonEduLoadSize = UniEppUtils.wrap(_ppsData.getTotalNonEduLoad());
        return "Всего внеучебной нагрузки: " + (_ppsData.getNonEduLoadRows().isEmpty() ? "нет" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalNonEduLoadSize));
    }

    public String getTotalLoadSize()
    {
        Long total = 0L;
        if (_ppsData.getTotalEduLoad() != null){ total += _ppsData.getTotalEduLoad(); }
        if (_ppsData.getTotalNonEduLoad() != null){ total += _ppsData.getTotalNonEduLoad(); }

        return "Всего: " + (total == 0L ? "нет" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(UniEppUtils.wrap(total)));
    }


    public boolean isAddNonEduLoadDisabled()
    {
        return UlsuLoadManager.instance().dao().getNextYear() == null;
    }

    public void onClickAddEduGroup()
    {
        _uiActivation.asRegionDialog(UlsuLoadEduGroupAdd.class).parameter("ppsId", _ppsId).activate();
    }

    public void onClickEditEduLoadDistribution()
    {
        _uiActivation.asRegionDialog(UlsuLoadEduGroupAdd.class).parameter("ppsId", _ppsId).parameter(PublisherActivator.PUBLISHER_ID_KEY, this.getListenerParameter()).activate();
    }

    public void onClickDeleteEduLoadDistribution()
    {
        UlsuLoadManager.instance().dao().deleteEduGroupDistr(_ppsId, this.<Long>getListenerParameter());
        _ppsData = UlsuLoadManager.instance().dao().getPpsLoadDataWrapper(_ppsId);
    }

    public void onClickEditFirstCourseImEduGroup()
    {
        _uiActivation.asRegionDialog(UlsuLoadFirstCourseImEduGroupAddEdit.class).parameter("ppsId", _ppsId).parameter(PublisherActivator.PUBLISHER_ID_KEY, this.getListenerParameter()).activate();
    }

    public void onClickDeleteFirstCourseImEduGroup()
    {
        UlsuLoadManager.instance().dao().delete(this.<Long>getListenerParameter());
        _ppsData = UlsuLoadManager.instance().dao().getPpsLoadDataWrapper(_ppsId);
    }

    public void onClickAddNonEduLoad()
    {
        _uiActivation.asRegionDialog(UlsuLoadNonEduLoadRecordAddEdit.class).parameter("ppsId", _ppsId).activate();
    }

    public void onClickAddFirstCourseImEduGroup()
    {
        _uiActivation.asRegionDialog(UlsuLoadFirstCourseImEduGroupAddEdit.class).parameter("ppsId", _ppsId).activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(UlsuLoadNonEduLoadRecordAddEdit.class).parameter("recordId", getListenerParameter()).activate();
    }

    public void onDeleteEntityFromList()
    {
        UlsuLoadManager.instance().dao().delete(this.<Long>getListenerParameter());
        _ppsData = UlsuLoadManager.instance().dao().getPpsLoadDataWrapper(_ppsId);
    }

    @Override
    public void onComponentRefresh()
    {
        _ppsData = UlsuLoadManager.instance().dao().getPpsLoadDataWrapper(_ppsId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(UlsuLoadPpsTab.EDU_LOAD_DS))
        {
            dataSource.put("eduLoad", _ppsData.getEduLoadRows());

        } else if (dataSource.getName().equals(UlsuLoadPpsTab.NON_EDU_LOAD_DS))
        {
            dataSource.put("nonEduLoad", _ppsData.getNonEduLoadRows());

        } else if (dataSource.getName().equals(UlsuLoadPpsTab.FIRST_COURSE_EDU_LOAD_DS))
        {
            dataSource.put("firstCourseEduLoad", _ppsData.getFirstCourseImEduGroups());
        }
    }
}