/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.uniulsu.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("uniulsu_createNextYearEduGroups", new SystemActionDefinition("uniulsu", "createNextYearEduGroups", "onClickCreateNextYearEduGroups", SystemActionPubExt.ULSU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}