/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class UlsuPpsLoadWrapper extends ViewWrapper<EmployeePost4PpsEntry>
{
    public static final String EDU_LOAD = "eduLoad";
    public static final String NON_EDU_LOAD = "nonEduLoad";
    public static final String TOTAL_LOAD = "totalLoad";

    public UlsuPpsLoadWrapper(EmployeePost4PpsEntry pps, Double eduLoad, Double nonEduLoad)
    {
        super(pps);
        this.setViewProperty(EDU_LOAD, eduLoad);
        this.setViewProperty(NON_EDU_LOAD, nonEduLoad);
        this.setViewProperty(TOTAL_LOAD, eduLoad + nonEduLoad);
    }
}