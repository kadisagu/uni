/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.bo.UlsuLoad.util;

import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.UlsuEduLoadWrapper;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.logic.UlsuFirstCourseImEduGroupViewWrapper;
import ru.tandemservice.uniulsu.entity.eduPlanLoad.UlsuNonEduLoad;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class UlsuPpsLoadDataWrapper
{
    private List<UlsuEduLoadWrapper> _eduLoadRows;
    private List<UlsuNonEduLoad> _nonEduLoadRows;
    private List<UlsuFirstCourseImEduGroupViewWrapper> _firstCourseImEduGroups;
    private Long _totalEduLoad;
    private Long _totalNonEduLoad;
    private Long _firstCourseImEduGroupLoadTotal;

    public List<UlsuEduLoadWrapper> getEduLoadRows(){ return _eduLoadRows; }
    public List<UlsuNonEduLoad> getNonEduLoadRows(){ return _nonEduLoadRows; }
    public List<UlsuFirstCourseImEduGroupViewWrapper> getFirstCourseImEduGroups(){ return _firstCourseImEduGroups; }
    public Long getTotalEduLoad(){ return _totalEduLoad; }
    public Long getTotalNonEduLoad(){ return _totalNonEduLoad; }
    public Long getFirstCourseImEduGroupLoadTotal(){ return _firstCourseImEduGroupLoadTotal; }

    public UlsuPpsLoadDataWrapper(List<UlsuEduLoadWrapper> eduLoadRows, List<UlsuNonEduLoad> nonEduLoadRows, List<UlsuFirstCourseImEduGroupViewWrapper> firstCourseImEduGroups, Long totalEduLoad, Long totalNonEduLoad, Long firstCourseImEduGroupLoadTotal)
    {
        _eduLoadRows = eduLoadRows;
        _nonEduLoadRows = nonEduLoadRows;
        _firstCourseImEduGroups = firstCourseImEduGroups;
        _totalNonEduLoad = totalNonEduLoad;

//        _totalEduLoad = totalEduLoad;
        _totalEduLoad = totalEduLoad == null && firstCourseImEduGroupLoadTotal == null ? null : (totalEduLoad == null ? 0L : totalEduLoad) + (firstCourseImEduGroupLoadTotal == null ? 0L : firstCourseImEduGroupLoadTotal);
        _firstCourseImEduGroupLoadTotal = firstCourseImEduGroupLoadTotal;
    }
}