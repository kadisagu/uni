/**
 *$Id$
 */
package ru.tandemservice.uniulsu.base.ext.Employee.ui.PostView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uniulsu.base.bo.UlsuLoad.UlsuLoadManager;

/**
 * @author Alexander Zhebko
 * @since 30.10.2013
 */
public class EmployeePostViewExtUI extends UIAddon
{
    public EmployeePostViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private Long _ppsId;

    public boolean isVisibleLoadTab()
    {
        return _ppsId != null;
    }

    public ParametersMap getParameters()
    {
        return ParametersMap.createWith("ppsId", _ppsId);
    }

    @Override
    public void onComponentRefresh()
    {
        Long employeePostId = ((EmployeePostViewUI) getPresenter()).getEmployeePost().getId();
        EmployeePost4PpsEntry employeePost4PpsEntry = UlsuLoadManager.instance().dao().get(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.employeePost().id(), employeePostId);
        _ppsId = employeePost4PpsEntry == null ? null : employeePost4PpsEntry.getPpsEntry().getId();
    }
}