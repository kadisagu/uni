package unifefu.scripts.diploma

import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentUtils
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddInformationAddEdit.DipDocumentAddInformationAddEdit
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog
import ru.tandemservice.unidip.base.entity.diploma.*
import ru.tandemservice.unidip.settings.entity.DipAssistantManager
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes

import static ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes.BACHELOR_DIPLOMA
import static ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes.MAGISTR_DIPLOMA
import static ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes.*

return new DipFistApplicationPrint(               // стандартные входные параметры скрипта
        session: session,                         // сессия
        template: template,                       // шаблон
        diplomaObject: session.get(DiplomaObject.class, diplomaObjectId),   //объект печати
        contentIssuance: contentIssuance,         //Приложение к факту выдачи диплома
        contentIssuanceList: contentIssuanceList, //Список выбранных приложений
        diplomaIssuance: diplomaIssuance          //Факт выдачи диплома
).print()

/**
 * @author Andrey Avetisov
 * @since 15.09.2014
 */

class DipFistApplicationPrint
{
    Session session
    byte[] template
    DiplomaObject diplomaObject
    DiplomaContentIssuance contentIssuance
    DiplomaIssuance diplomaIssuance
    List<DiplomaContentIssuance> contentIssuanceList


    def print()
    {

        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()

        List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList = DataAccessServices.dao().getList(DiplomaAcademyRenameData.class,
                DiplomaAcademyRenameData.L_DIPLOMA_CONTENT, diplomaObject.getContent(), DiplomaAcademyRenameData.academyRename().date().s())

        def academyRename = getAcademyRename(diplomaAcademyRenameDataList)
        im.put("academyRename", academyRename)
        DipAdditionalInformation additionalInformation = DataAccessServices.dao().get(DipAdditionalInformation.class, DipAdditionalInformation.L_DIPLOMA_OBJECT, diplomaObject)
        def information = getVPOInformation(additionalInformation, diplomaObject)
        im.put("information", information)

        DipAssistantManager dipAssistantManager = new DQLSelectBuilder().fromEntity(DipAssistantManager.class, "e")
                .createStatement(session).<DipAssistantManager> uniqueResult();
        def topOrgUnit = new DQLSelectBuilder().fromEntity(TopOrgUnit.class, "e")
                .createStatement(session).<TopOrgUnit> uniqueResult()
        if (dipAssistantManager != null)
        {
            im.put("fioRector", dipAssistantManager.employeePost.fio)
            im.put("slash", "/")
        }
        else
        {
            im.put("fioRector", topOrgUnit.head!=null?topOrgUnit.head.employee.person.identityCard.fio:"")
            im.put("slash", "")
        }

        def pages = "4"
        def hasSecondContentApplication = false
        for (DiplomaContentIssuance included : contentIssuanceList)
        {
            if (included.contentListNumber == 2)
            {
                pages = "8"
                hasSecondContentApplication = true;
                break;
            }
        }
        im.put("pages", pages)
        im.put("fullNameOrganization", topOrgUnit.nominativeCaseTitle)
        im.put("cityOrganization", topOrgUnit.getTerritorialFullTitle().replace(".", ""))

        def duplicate = "";
        if (contentIssuance.isDuplicate() || diplomaIssuance.replacedIssuance != null)
        {
            duplicate = "ДУБЛИКАТ"
        }
        im.put("duplicate", duplicate)

        im.put("degreeDifferences", DipDocumentUtils.getHigherDegreeDiff(diplomaObject))
        im.put("developPeriod", DipDocumentUtils.getHigherDevelopPeriod(diplomaObject))

        def regNumber = contentIssuance.duplicate ? contentIssuance.duplicateRegustrationNumber : diplomaIssuance.registrationNumber;
        im.put("regNumber", regNumber)

        def issuDate = contentIssuance.duplicate ? RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(contentIssuance.duplicateIssuanceDate) :
                RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(diplomaIssuance.issuanceDate)
        im.put("issuDate", issuDate + " года")

        im.put("lastName", diplomaObject.student.person.identityCard.lastName)
        im.put("firstName", diplomaObject.student.person.identityCard.firstName)
        im.put("middleName", diplomaObject.student.person.identityCard.middleName)
        def birthDate = diplomaObject.student.person.identityCard.birthDate != null ?
                RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(diplomaObject.student.person.identityCard.birthDate) + " года" : "";
        im.put("birthDate", birthDate)

        def eduDocumentKindOld = diplomaObject.student.person.personEduInstitution;
        def eduDocumentKindOldTitle = ""
        def yearEndOld = ""
        if (eduDocumentKindOld != null)
        {
            eduDocumentKindOldTitle = StringUtils.uncapitalize(eduDocumentKindOld.documentType.title)
            if (eduDocumentKindOld.educationLevel.isHighProf())
                eduDocumentKindOldTitle += " о высшем образовании"
            else if (eduDocumentKindOld.educationLevel.isMiddleProf())
                eduDocumentKindOldTitle += " о среднем профессиональном образовании"
            else if (DipDocumentUtils.isMiddleSchool(eduDocumentKindOld.educationLevel))
                eduDocumentKindOldTitle +=" об основном общем образовании"
            else
            {
                def fullEducationLevelTitle = eduDocumentKindOld.educationLevel.shortTitle.contains("образование") ? eduDocumentKindOld.educationLevel.shortTitle : eduDocumentKindOld.educationLevel.shortTitle + " образование"
                eduDocumentKindOldTitle = (fullEducationLevelTitle + ", " + eduDocumentKindOldTitle).toLowerCase()
            }
            if (!eduDocumentKindOld.addressItem.country.title.equals("Россия"))
            {
                eduDocumentKindOldTitle += ", " + eduDocumentKindOld.addressItem.country.title;
            }
            yearEndOld = String.valueOf(eduDocumentKindOld.yearEnd) + " год"
        }
        im.put("eduDocumentKindOld", eduDocumentKindOldTitle)
        im.put("yearEndOld", yearEndOld)

        def eduDocumentKind = diplomaObject.student.eduDocument;
        def eduDocumentKindTitle = ""
        def yearEnd = ""
        if (eduDocumentKind != null)
        {
            eduDocumentKindTitle = eduDocumentKind.documentKindTitle.toLowerCase()
            if (!eduDocumentKind.eduOrganizationAddressItem.country.title.equals("Россия"))
            {
                eduDocumentKindTitle += ", " + eduDocumentKind.eduOrganizationAddressItem.country.title;
            }
            yearEnd = String.valueOf(eduDocumentKind.yearEnd)
        }
        im.put("eduDocumentKind", eduDocumentKindTitle)
        im.put("yearEnd", yearEnd)

        def programSubject = diplomaObject.getContent().getProgramSubject() != null ? diplomaObject.getContent().getProgramSubject().getTitleWithCode() : "";
        im.put("programSubject", programSubject)

		im.put("programQualification", DipDocumentUtils.getHigherProfQualificationTitle(diplomaObject).toUpperCase())

        RtfDocument document = new RtfReader().read(template);


        DipDocTemplateCatalog disciplineTemplate = new DipDocTemplateCatalog();
        disciplineTemplate.setTemplatePath("unifefu/templates/diploma/T_discipline.rtf")
        RtfDocument disciplineDoc = new RtfReader().read(disciplineTemplate.template);
        RtfUtil.modifySourceList(document.header, disciplineDoc.header, disciplineDoc.elementList);


        im.put("T_discipline1", disciplineDoc.elementList)


        List<DiplomaContentRow> diplomaContentRowList = DataAccessServices.dao().getList(DiplomaContentRow.class,
                DiplomaContentRow.owner(), diplomaObject.content, DiplomaContentRow.P_NUMBER)

        ArrayList<String[]> disciplineRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> disciplineRowsVar2 = new ArrayList<String[]>()
        ArrayList<String[]> practiceRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> practiceRowsVar2 = new ArrayList<String[]>()
        double allPracticeLaborLoad = 0
        double allPracticeWeeksLoad = 0

        ArrayList<String[]> stateExamRows = new ArrayList<String[]>()
        double allStateExamLaborLoad = 0
        double allStateExamWeeksLoad = 0
        ArrayList<String[]> graduateWorkRows = new ArrayList<String[]>()
        ArrayList<String[]> optDisciplineRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> optDisciplineRowsVar2 = new ArrayList<String[]>()
        ArrayList<String[]> courseRows = new ArrayList<String[]>()
        boolean hasSecondContentListNumber = false
        double audLoad = 0

        boolean hasQualificationRows = false;
        boolean attestationShowSecond = true
        List<DiplomaContentRow> stateExams = new ArrayList()
        List<DiplomaContentRow> qualifWorks = new ArrayList()

        for (DiplomaContentRow contentRow : diplomaContentRowList)
        {
            if (contentRow instanceof DiplomaQualifWorkRow)
            {
                hasQualificationRows = true
                qualifWorks.add(contentRow)
            }
            if (contentRow instanceof DiplomaStateExamRow)
            {
                if (contentRow.contentListNumber == 1) attestationShowSecond = false
                stateExams.add(contentRow)
            }
            if (contentRow.contentListNumber == 2 )
            {
                if (contentRow instanceof DiplomaDisciplineRow)
                {
                    audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
                }
                if (contentRow instanceof DiplomaStateExamRow || contentRow instanceof DiplomaQualifWorkRow)
                {
                    allStateExamLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                    allStateExamWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                    audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
                }
                if (!(contentRow instanceof DiplomaOptDisciplineRow) && !(contentRow instanceof DiplomaCourseWorkRow))
                    hasSecondContentListNumber = true
            }
            if (contentRow.contentListNumber == 1)
            {
                if (contentRow instanceof DiplomaDisciplineRow)
                {
                    disciplineRowsVar1.add([contentRow.title, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0) + " з.е.", contentRow.mark] as String[])
                    disciplineRowsVar2.add([contentRow.title, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.loadAsDouble != null ? contentRow.loadAsDouble : 0) + " час.", contentRow.mark] as String[])
                    audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
                }

                if (contentRow instanceof DiplomaPracticeRow)
                {
                    allPracticeLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                    allPracticeWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                    practiceRowsVar1.add([contentRow.title, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0) + " з.е.", contentRow.mark] as String[])
                    practiceRowsVar2.add([contentRow.title, DipDocumentUtils.getWeeksWithUnit(contentRow.weeksAsDouble), contentRow.mark] as String[])
                    audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
                }
                if (contentRow instanceof DiplomaStateExamRow)
                {
                    allStateExamLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                    allStateExamWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                    stateExamRows.add([contentRow.title, "x", contentRow.mark] as String[])
                    audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
                }
                if (contentRow instanceof DiplomaQualifWorkRow)
                {
                    allStateExamWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                    allStateExamLaborLoad += contentRow.laborAsDouble!= null ? contentRow.laborAsDouble : 0;
                    String title = contentRow.theme != null ? contentRow.title + " «" + contentRow.theme + "»" : contentRow.title
                    graduateWorkRows.add([title, "x", contentRow.mark] as String[])
                    audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
                }
                if (contentRow instanceof DiplomaCourseWorkRow)
                {
                    String title = contentRow.theme != null ? contentRow.title + " «" + contentRow.theme + "»" : contentRow.title
                    courseRows.add([title, contentRow.mark] as String[])
                    audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;

                }
                if (contentRow instanceof DiplomaOptDisciplineRow)
                {
                    optDisciplineRowsVar1.add([contentRow.title, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0) + " з.е.", contentRow.mark] as String[])
                    optDisciplineRowsVar2.add([contentRow.title,
                                               DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.loadAsDouble != null ? contentRow.loadAsDouble : 0) + " час.",
                                               contentRow.mark] as String[])
                }

            }

        }

        if (stateExams.empty && !qualifWorks.empty)
            attestationShowSecond = qualifWorks.get(0).contentListNumber == 2

        String practiceLoadVar1 = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allPracticeLaborLoad) + " з.е."
        String practiceLoadVar2 = DipDocumentUtils.getWeeksWithUnit(allPracticeWeeksLoad)

        String examLoadVar1 = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allStateExamLaborLoad) + " з.е."
        String examLoadVar2 = DipDocumentUtils.getWeeksWithUnit(allStateExamWeeksLoad)

        if (DipDocumentUtils.isShowLabor(diplomaObject))
        {
            tm.put("discipline", disciplineRowsVar1 as String[][])
            fillPractice(diplomaContentRowList, practiceLoadVar1, practiceRowsVar1, tm, 1)
            tm.put("graduateWork", graduateWorkRows as String[][])
            def eduProgramRow = new ArrayList<String[]>()
            def audLoadRow = new ArrayList<String[]>()
            if (!hasSecondContentListNumber)
            {
                def double laborAsDouble = diplomaObject.content.loadAsDouble ?: 0
                eduProgramRow.add(["Объем образовательной программы", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(laborAsDouble) + " з.е.", "x"] as String[])
                audLoadRow.add(["в том числе объем работы обучающихся во взаимодействии с преподавателем:", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(audLoad) + " час.", "x"] as String[])
            }
            tm.put("labor", eduProgramRow as String[][])
            tm.put("audience", audLoadRow as String[][])
            fillFacultDiscipline(diplomaContentRowList, optDisciplineRowsVar1, additionalInformation, tm, 1)
        }
        else
        {
            tm.put("discipline", disciplineRowsVar2 as String[][])
            fillPractice(diplomaContentRowList, practiceLoadVar2, practiceRowsVar2, tm, 1)
            if (!hasQualificationRows && !hasSecondContentApplication)
            {
                graduateWorkRows.add(["Выполнение и защита выпускной квалификационной работы не предусмотрены"] as String[])
            }
            tm.put("graduateWork", graduateWorkRows as String[][])
            def eduProgramRow = new ArrayList<String[]>()
            def audLoadRow = new ArrayList<String[]>()
            if (!hasSecondContentListNumber && diplomaObject.content.loadInWeeks)
            {
                eduProgramRow.add(["Срок освоения образовательной программы", DipDocumentUtils.getWeeksWithUnit(diplomaObject.content.loadAsDouble), "x"] as String[])
                audLoadRow.add(["в том числе аудиторных часов:", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(audLoad) + " час.", "x"] as String[])
            }
            tm.put("labor", eduProgramRow as String[][])
            tm.put("audience", audLoadRow as String[][])
            fillFacultDiscipline(diplomaContentRowList, optDisciplineRowsVar2, additionalInformation, tm, 1)
        }
        fillVPOStateExam(examLoadVar1, examLoadVar2, stateExamRows, !attestationShowSecond, diplomaObject, tm)

        tm.put("T", courseRows as String[][])

        im.modify(document)
        tm.modify(document)
        return [document: document, fileName: 'diploma.rtf']
    }

    private static void fillPractice(List<DiplomaContentRow> diplomaContentRowList, String practiceRowsLoad, ArrayList<String[]> practiceRows, RtfTableModifier tm, int contentListNumber)
    {

        boolean showTitle = false;
        for (DiplomaContentRow contentRow : diplomaContentRowList)
        {
            if (contentRow instanceof DiplomaPracticeRow) {
                showTitle = contentRow.contentListNumber == contentListNumber;
                break;
            }
        }
        def allRows = new ArrayList<String[]>()

        if (showTitle)
        {
            allRows.add(["Практики", practiceRowsLoad, "x"] as String[])
            allRows.add(["в том числе:"] as String[])
        }
        tm.put("allPractice", allRows as String[][])
        tm.put("practice", practiceRows as String[][])
    }

    static void fillFacultDiscipline(List<DiplomaContentRow> diplomaContentRowList, ArrayList<String[]> optDisciplineRows, DipAdditionalInformation additionalInformation, RtfTableModifier tm, int contentListNumber)
    {
        boolean showTitle = false;
        for (DiplomaContentRow contentRow : diplomaContentRowList)
        {
            if (contentRow instanceof DiplomaOptDisciplineRow) {
                showTitle = contentRow.contentListNumber == contentListNumber;
                break;
            }
        }

        def allRows = new ArrayList<String[]>()
        if (showTitle)
        {
            allRows.add(["Факультативные дисциплины"] as String[])
            allRows.add(["в том числе:"] as String[])
        }
        if (additionalInformation != null && additionalInformation.isShowAdditionalDiscipline())
        {
            tm.put("facultative", allRows as String[][])
            tm.put("facultDiscipline", optDisciplineRows as String[][])
        }
        else
        {
            tm.put("facultative", new ArrayList<String[]>() as String[][])
            tm.put("facultDiscipline", new ArrayList<String[]>() as String[][])
        }
    }

    private static void fillVPOStateExam(String examLoadVar1, String examLoadVar2, ArrayList<String[]> stateExamRows, boolean showTotalRows,
                                 DiplomaObject diplomaObject, RtfTableModifier tm)
    {
        String idx = diplomaObject.content.programSubject.subjectIndex.code

        List<String> vo2009and2013 = [TITLE_2009_62, TITLE_2009_65, TITLE_2009_68, TITLE_2013_03, TITLE_2013_04, TITLE_2013_05, TITLE_2013_06, TITLE_2013_07, TITLE_2013_08, TITLE_2013_10]
        List<String> vo2005 = [TITLE_2005_62, TITLE_2005_65, TITLE_2005_68]
        String examRowsLoad = vo2009and2013.contains(idx) ? examLoadVar1 : vo2005.contains(idx) ? examLoadVar2 : null

        def allRows = new ArrayList<String[]>()
        if (showTotalRows)
        {
            allRows.add(["Государственная итоговая аттестация", examRowsLoad, "x"] as String[])
            allRows.add(["в том числе:"] as String[])
        }
        tm.put("attestation", allRows as String[][])
        tm.put("exam", stateExamRows as String[][])
    }

    private static RtfString getAcademyRename(List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList)
    {
        def academyRename = new RtfString();
        for (DiplomaAcademyRenameData renameData : diplomaAcademyRenameDataList)
        {
            if (diplomaAcademyRenameDataList.indexOf(renameData) > 0)
            {
                academyRename.par()
            }
            academyRename.append("Образовательная организация переименована в " + RussianDateFormatUtils.getYearString(renameData.getAcademyRename().getDate(), false) + " году.").par();
            academyRename.append("Старое полное официальное наименование образовательной организации - " + renameData.getAcademyRename().getPreviousFullTitle() + ".");
        }
        return academyRename
    }

    private static RtfString getVPOInformation(DipAdditionalInformation additionalInformation, DiplomaObject diplomaObject)
    {
        def information = new RtfString();
        if (additionalInformation != null)
        {
            List<DipAddInfoEduForm> addInfoEduFormList = DataAccessServices.dao().getList(DipAddInfoEduForm.class, DipAddInfoEduForm.L_DIP_ADDITIONAL_INFORMATION, additionalInformation)

            if (!additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() == 1 && addInfoEduFormList.get(0) != null)
            {
                information.append("Форма обучения: " + addInfoEduFormList.get(0).getEduProgramForm().getTitle() + ".")
            }

            else if (!additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() > 1)
            {
                information.append("Сочетание форм обучения: ")
                for (DipAddInfoEduForm eduForm : addInfoEduFormList)
                {
                    if (addInfoEduFormList.indexOf(eduForm) > 0)
                    {
                        information.append(", ");
                    }
                    information.append(eduForm.getEduProgramForm().getTitle());
                }
                information.append(".")
            }

            else if (additionalInformation.isShowSelfEduForm() && CollectionUtils.isEmpty(addInfoEduFormList))
            {
                information.append("Форма получения образования: самообразование.")
            }

            else if (additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() == 1 && addInfoEduFormList.get(0) != null)
            {
                String eduFormTitle = ""
                if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.OCHNAYA))
                    eduFormTitle = "очной"
                else if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.ZAOCHNAYA))
                    eduFormTitle = "заочной"
                else if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.OCHNO_ZAOCHNAYA))
                    eduFormTitle = "очно-заочной"
                information.append("Сочетание самообразования и " + eduFormTitle + " формы обучения.")
            }

            else if (additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() > 1)
            {
                information.append("Сочетание самообразования и форм обучения: ")
                for (DipAddInfoEduForm eduForm : addInfoEduFormList)
                {
                    if (addInfoEduFormList.indexOf(eduForm) > 0)
                    {
                        information.append(", ");
                    }
                    information.append(eduForm.getEduProgramForm().getTitle());
                }
                information.append(".")
            }

            String specialization = diplomaObject.getContent().getProgramSpecialization() != null ? diplomaObject.getContent().getProgramSpecialization().getTitle() : "";
            if (additionalInformation.showProgramSpecialization)
            {
                String documentTypeCode = diplomaObject.content.type.code
                if (documentTypeCode.equals(BACHELOR_DIPLOMA) || documentTypeCode.equals(MAGISTR_DIPLOMA))
                {
                    if (information.toList().size() > 0)
                        information.par()
                    information.append("Направленность (профиль) образовательной программы: " + specialization + ".")
                }
            }

            if (additionalInformation.specialization.equals(DipDocumentAddInformationAddEdit.ORIENTATION.title))
            {
                if (information.toList().size() > 0)
                    information.par()
                information.append("Направленность (профиль) образовательной программы: " + specialization + ".")
            }
            else if (additionalInformation.specialization.equals(DipDocumentAddInformationAddEdit.SPECIALIZATION.title))
            {
                if (information.toList().size() > 0)
                    information.par()
                information.append("Специализация:  " + specialization + ".")
            }

            if (additionalInformation.passIntenssiveTraining)
            {
                if (information.toList().size() > 0)
                    information.par()
                information.append("Пройдено ускоренное обучение по образовательной программе.")
            }

            List<DipEduInOtherOrganization> otherOrganizationList = DataAccessServices.dao().getList(DipEduInOtherOrganization.class,
                    DipEduInOtherOrganization.dipAdditionalInformation(), additionalInformation);

            for (DipEduInOtherOrganization otherOrganization : otherOrganizationList)
            {
                if (information.toList().size() > 0)
                    information.par()
                if (otherOrganization.totalCreditsAsDouble != null && otherOrganization.totalCreditsAsDouble > 0)
                {
                    information.append("Часть образовательной программы в объеме "
                            + DipDocumentUtils.getLaborWithUnitInGenitive(otherOrganization.totalCreditsAsDouble)
                            + " освоена в "
                            + otherOrganization.row + ".")
                }

                if (otherOrganization.totalWeeksAsDouble != null && otherOrganization.totalWeeksAsDouble > 0)
                {
                    information.append("Часть образовательной программы в объеме "
                            + DipDocumentUtils.getWeeksWithUnitInGenitive(otherOrganization.totalWeeksAsDouble)
                            + " освоена в "
                            + otherOrganization.row + ".")
                }
            }
        }
        return information
    }
}
