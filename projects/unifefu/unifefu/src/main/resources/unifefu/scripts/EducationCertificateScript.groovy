package unifefu.scripts

import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.CoreDateUtils
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.fias.IKladrDefines
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.PersonEduInstitution
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage
import org.tandemframework.shared.person.catalog.entity.codes.EducationLevelStageCodes
import ru.tandemservice.uni.entity.employee.OrderData
import ru.tandemservice.uni.entity.orgstruct.AcademyRename
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentUtils
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddInformationAddEdit.DipDocumentAddInformationAddEdit
import ru.tandemservice.unidip.base.entity.catalog.DipDocTemplateCatalog
import ru.tandemservice.unidip.base.entity.diploma.*
import ru.tandemservice.unidip.settings.entity.DipAssistantManager
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes

import static org.tandemframework.hibsupport.dql.DQLExpressions.betweenDays
import static org.tandemframework.hibsupport.dql.DQLExpressions.property

/**
 * @author Andrey Avetisov
 * @since 02.10.2014
 */

return new EducationCertificatePrint(                                       // стандартные входные параметры скрипта
        session: session,                                                   // сессия
        template: template,                                                 // шаблон
        diplomaObject: session.get(DiplomaObject.class, diplomaObjectId),   // Диплом
        diplomaIssuance: diplomaIssuance,                                   // Факт выдачи диплома
        formativeHead: formativeHead,                                       // Глава формирующего
        eduProgramAdmin: eduProgramAdmin,                                   // Администратор обр.программы
).print()

class EducationCertificatePrint {
    Session session
    byte[] template
    DiplomaObject diplomaObject
    DiplomaIssuance diplomaIssuance
    EmployeePost formativeHead
    EmployeePost eduProgramAdmin

    @SuppressWarnings("GrDeprecatedAPIUsage")
    def print() {
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()
        RtfDocument document = new RtfReader().read(template);


        def topOrgUnit = new DQLSelectBuilder().fromEntity(TopOrgUnit.class, "e")
                .createStatement(session).<TopOrgUnit> uniqueResult()

        def registrationNumber = diplomaIssuance != null ? diplomaIssuance.registrationNumber : ""
        def issuanceDate = diplomaIssuance != null ? RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(diplomaIssuance.issuanceDate) + " года" : ""

        def person = diplomaObject.student.person
        def lastName = person.identityCard.lastName
        def firstName = person.identityCard.firstName
        def middleName = person.identityCard.middleName
        def birthDate = person.identityCard.birthDate != null ?
                RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(person.identityCard.birthDate) + " года" : "";


        // Метки для ДВФУ, связанные с тем, что они не провели рефакторинг документов об образовании

        def List<PersonEduInstitution> eduInstList = DataAccessServices.dao().getList(PersonEduInstitution.class, PersonEduInstitution.L_PERSON, person)

        Collections.sort(eduInstList, new Comparator<PersonEduInstitution>() {
            @Override
            int compare(PersonEduInstitution o1, PersonEduInstitution o2) {
                return o2.getYearEnd() - o1.getYearEnd()
            }
        })

        def eduDocumentKindTitleOld = ""
        def yearEndOld = ""

        if (!eduInstList.isEmpty()) {

            def eduInst = eduInstList.get(0);
            eduDocumentKindTitleOld = StringUtils.uncapitalize(eduInst.documentType.title)

            if (eduInst.educationLevel.isHighProf())
                eduDocumentKindTitleOld += " о высшем образовании"
            else if (eduInst.educationLevel.isMiddleProf())
                eduDocumentKindTitleOld += " о среднем профессиональном образовании"
            else if (isMiddleSchool(eduInst.educationLevel))
                eduDocumentKindTitleOld += " об основном общем образовании"

            if (eduInst.addressItem.country.code != IKladrDefines.RUSSIA_COUNTRY_CODE)
                eduDocumentKindTitleOld += ", " +  eduInst.addressItem.country.title
            yearEndOld = String.valueOf(eduInst.yearEnd) + " год"

        }

        im.put("eduDocumentKindOld", eduDocumentKindTitleOld)
        im.put("yearEndOld", yearEndOld)

        // ------------

        def eduDocumentKind = diplomaObject.student.eduDocument;
        def eduDocumentKindTitle = ""
        def yearEnd = ""
        if (eduDocumentKind != null) {
            eduDocumentKindTitle = eduDocumentKind.documentKindTitle.toLowerCase()
            if (!eduDocumentKind.eduOrganizationAddressItem.country.title.equals("Россия")) {
                eduDocumentKindTitle += ", " + eduDocumentKind.eduOrganizationAddressItem.country.title;
            }
            yearEnd = String.valueOf(eduDocumentKind.yearEnd)
        }

        def beginTraining = ""

        OrderData orderData = DataAccessServices.dao().get(OrderData.class, OrderData.student(), diplomaObject.student);

        if (orderData != null && orderData.eduEnrollmentOrderEnrDate != null) {
            AcademyRename academyRename = new DQLSelectBuilder()
                    .fromEntity(AcademyRename.class, "r").column("r").top(1)
                    .where(betweenDays(AcademyRename.date().fromAlias("r"), orderData.eduEnrollmentOrderEnrDate, CoreDateUtils.getYearFirstTimeMoment(3000)))
                    .order(property("r", AcademyRename.P_DATE)).createStatement(session).uniqueResult()

            if (academyRename != null) {
                beginTraining = RussianDateFormatUtils.getYearString(orderData.eduEnrollmentOrderEnrDate, false) + " году в " + academyRename.previousFullTitle;
            } else {
                beginTraining = RussianDateFormatUtils.getYearString(orderData.eduEnrollmentOrderEnrDate, false) + " году в " + topOrgUnit.nominativeCaseTitle;
            }
        }

        def completedTraining

        DipAdditionalInformation dipAdditionalInformation = DataAccessServices.dao().get(DipAdditionalInformation.class, DipAdditionalInformation.L_DIPLOMA_OBJECT, diplomaObject)
        if (dipAdditionalInformation != null && dipAdditionalInformation.demand) {
            completedTraining = "продолжает обучение в " + topOrgUnit.getPrepositionalCaseTitle()
        } else {
            completedTraining = diplomaIssuance != null ? (RussianDateFormatUtils.getYearString(diplomaIssuance.issuanceDate, false) + " году в ") : ""
            completedTraining += topOrgUnit.getPrepositionalCaseTitle()
        }

        def programKind = ""
        def programSubjectTitle = ""
        EduProgramSubject programSubject = diplomaObject.content.programSubject
        if (programSubject != null) {
            if (programSubject.eduProgramKind.isBS()) {
                programKind = "бакалавриата/специалитета"
            } else if (programSubject.eduProgramKind.code.equals(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_)
                    || programSubject.eduProgramKind.code.equals(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA)) {
                programKind = "среднего профессионального образования"
            } else if (programSubject.eduProgramKind.code.equals(EduProgramKindCodes.PROGRAMMA_MAGISTRATURY)) {
                programKind = "магистратуры"
            }
            programSubjectTitle = programSubject.getTitleWithCode()
        }

        def developPeriod = DipDocumentUtils.getDevelopPeriod(diplomaObject)
        def programQualification = DipDocumentUtils.getEduCertificateQualificationTitle(diplomaObject)

        def academyRenameList = getAcademyRename()
        def information = getInformation(dipAdditionalInformation)
        def fioHeadUni = ""
        def headUni = ""
        DipAssistantManager dipAssistantManager = new DQLSelectBuilder().fromEntity(DipAssistantManager.class, "e")
                .createStatement(session).<DipAssistantManager> uniqueResult();
        if (dipAssistantManager != null) {
            fioHeadUni = dipAssistantManager.employeePost.person.identityCard.iof
            headUni = dipAssistantManager.employeePost.postRelation.title
        } else if (topOrgUnit.head != null) {
            fioHeadUni = ((IdentityCard) topOrgUnit.head.employee.person.identityCard).iof
            headUni = ((EmployeePost) topOrgUnit.head).postRelation?.title
        }


        List<DiplomaContentRow> diplomaContentRowList = DataAccessServices.dao().getList(DiplomaContentRow.class,
                DiplomaContentRow.owner(), diplomaObject.content, DiplomaContentRow.P_NUMBER)

        ArrayList<String[]> courseRows = new ArrayList<String[]>()
        //var1 - EduPlan HighSchool 3d generation, var2 - other eduPlan
        ArrayList<String[]> disciplineRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> disciplineRowsVar2 = new ArrayList<String[]>()
        ArrayList<String[]> practiceRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> practiceRowsVar2 = new ArrayList<String[]>()
        ArrayList<String[]> stateExamRows = new ArrayList<String[]>()
        ArrayList<String[]> graduateWorkRows = new ArrayList<String[]>()
        ArrayList<String[]> optDisciplineRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> optDisciplineRowsVar2 = new ArrayList<String[]>()
        double allStateExamLaborLoad = 0
        double allStateExamWeeksLoad = 0
        double allPracticeLaborLoad = 0
        double allPracticeWeeksLoad = 0
        double audLoad = 0

        for (DiplomaContentRow contentRow : diplomaContentRowList) {
            if (contentRow instanceof DiplomaCourseWorkRow) {
                String title = contentRow.theme != null ? contentRow.title + " «" + contentRow.theme + "»" : contentRow.title
                courseRows.add([title, contentRow.mark] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }

            if (contentRow instanceof DiplomaDisciplineRow) {
                disciplineRowsVar1.add([contentRow.title, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0) + " з.е.", contentRow.mark] as String[])
                disciplineRowsVar2.add([contentRow.title, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.loadAsDouble != null ? contentRow.loadAsDouble : 0) + " час.", contentRow.mark] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaPracticeRow) {
                allPracticeLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                allPracticeWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                practiceRowsVar1.add([contentRow.title, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0) + " з.е.", contentRow.mark] as String[])
                practiceRowsVar2.add([contentRow.title, getWeeksWithUnit(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0)) + ".", contentRow.mark] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaStateExamRow) {
                allStateExamLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                allStateExamWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                stateExamRows.add([contentRow.title, "x", contentRow.mark] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaQualifWorkRow) {
                allStateExamLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                allStateExamWeeksLoad += contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
                String title = contentRow.theme != null ? contentRow.title + " «" + contentRow.theme + "»" : contentRow.title
                graduateWorkRows.add([title, "x", contentRow.mark] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaOptDisciplineRow) {
                optDisciplineRowsVar1.add([contentRow.title, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0) + " з.е.", contentRow.mark] as String[])
                optDisciplineRowsVar2.add([contentRow.title,
                                           DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(contentRow.loadAsDouble != null ? contentRow.loadAsDouble : 0) + " час.",
                                           contentRow.mark] as String[])
            }

        }

        DipDocTemplateCatalog disciplineTemplate = new DipDocTemplateCatalog();
        disciplineTemplate.setTemplatePath("unifefu/templates/T_disCertificate.rtf")
        RtfDocument disciplineDoc = new RtfReader().read(disciplineTemplate.template);
        RtfUtil.modifySourceList(document.header, disciplineDoc.header, disciplineDoc.elementList);


        im.put("T_discipline", disciplineDoc.elementList)

        im.put("fullNameOrganization", topOrgUnit.nominativeCaseTitle)
        im.put("cityOrganization", topOrgUnit.territorialTitle.replace(".", ""))
        im.put("registrationNumber", registrationNumber)
        im.put("issuanceDate", issuanceDate)
        im.put("lastName", lastName)
        im.put("firstName", firstName)
        im.put("middleName", middleName)
        im.put("birthDate", birthDate)
        im.put("eduDocumentKind", eduDocumentKindTitle)
        im.put("yearEnd", yearEnd)
        im.put("beginTraining", beginTraining)
        im.put("completedTraining", completedTraining)
        im.put("programKind", programKind)
        im.put("developPeriod", developPeriod)
        im.put("programSubject", programSubjectTitle)
        im.put("programQualification", programQualification)
        im.put("academyRename", academyRenameList)
        im.put("information", information)
        // FEFU part(!)
        im.put("headUni", headUni)
        im.put("fioHeadUni", fioHeadUni)
        // --
        tm.put("T", courseRows as String[][])


        String practiceLoadVar1 = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allPracticeLaborLoad) + " з.е."
        String practiceLoadVar2 = getWeeksWithUnit(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allPracticeWeeksLoad))
        String examLoadVar1 = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allStateExamLaborLoad) + " з.е."
        String examLoadVar2 = getWeeksWithUnit(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allStateExamWeeksLoad))

        if (DipDocumentUtils.isShowLabor(diplomaObject))
		{
            tm.put("discipline", disciplineRowsVar1 as String[][])
            fillPractice(practiceLoadVar1, practiceRowsVar1, tm)
            fillStateExam(examLoadVar1, stateExamRows, graduateWorkRows, tm)
            def labor = new ArrayList<String[]>()
            def audLoadRow = new ArrayList<String[]>()
            def certLabor = diplomaObject.content.loadAsDouble ?: 0d
            labor.add(["Объем образовательной программы", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(certLabor) + " з.е.", "x"] as String[])
            audLoadRow.add(["в том числе объем работы обучающихся во взаимодействии с преподавателем:", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(audLoad) + " час.", "x"] as String[])
            tm.put("labor", labor as String[][])
            tm.put("audience", audLoadRow as String[][])
            fillFacultDiscipline(optDisciplineRowsVar1, dipAdditionalInformation, tm)

        }
		else
		{
            tm.put("discipline", disciplineRowsVar2 as String[][])
            fillPractice(practiceLoadVar2, practiceRowsVar2, tm)
            fillStateExam(examLoadVar2, stateExamRows, graduateWorkRows, tm)
            def labor = new ArrayList<String[]>()
            def audLoadRow = new ArrayList<String[]>()
            def certWeeks = diplomaObject.content.loadAsDouble ?: 0d;
            String weeksLoad = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(certWeeks) + " " +
                    CommonBaseStringUtil.numberPostfixCase(diplomaObject.content.weeksAsLong, "неделя", "недели", "недель")
            labor.add(["Срок освоения образовательной программы", weeksLoad, "x"] as String[])
            audLoadRow.add(["в том числе аудиторных часов:", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(audLoad) + " час.", "x"] as String[])
            tm.put("labor", labor as String[][])
            tm.put("audience", audLoadRow as String[][])
            fillFacultDiscipline(optDisciplineRowsVar2, dipAdditionalInformation, tm)
        }

        //двфу часть
        StringBuilder excludeOrderBuilder = new StringBuilder();
        if (orderData?.getExcludeOrderDate() != null && orderData?.getExcludeOrderNumber() != null) {
            excludeOrderBuilder.append("Приказ об отчислении от ")
                    .append(RussianDateFormatUtils.getDayString(orderData.getExcludeOrderDate(), true))
                    .append(" ")
                    .append(RussianDateFormatUtils.getMonthName(orderData.getExcludeOrderDate(), false))
                    .append(" ")
                    .append(RussianDateFormatUtils.getYearString(orderData.getExcludeOrderDate(), false))
                    .append(" №")
                    .append(orderData.getExcludeOrderNumber());
        }
        im.put("order", excludeOrderBuilder.toString())

        im.put("headFormative", formativeHead.postRelation.title)
        im.put("fioHeadFormative", formativeHead.employee.person.identityCard.iof)
        im.put("executor", eduProgramAdmin.postRelation.title)
        im.put("fioExecutor", eduProgramAdmin.employee.person.identityCard.iof)

        im.modify(document)
        tm.modify(document)

        return [document: document, fileName: 'document.rtf']
    }

    private RtfString getAcademyRename() {
        def academyRename = new RtfString();
        List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList = DataAccessServices.dao().getList(DiplomaAcademyRenameData.class,
                DiplomaAcademyRenameData.L_DIPLOMA_CONTENT, diplomaObject.getContent(), DiplomaAcademyRenameData.academyRename().date().s())

        for (DiplomaAcademyRenameData renameData : diplomaAcademyRenameDataList) {
            if (diplomaAcademyRenameDataList.indexOf(renameData) > 0) {
                academyRename.par()
            }
            academyRename.append("Образовательная организация переименована в " + RussianDateFormatUtils.getYearString(renameData.getAcademyRename().getDate(), false) + " году.").par();
            academyRename.append("Старое полное официальное наименование образовательной организации - " + renameData.getAcademyRename().getPreviousFullTitle() + ".");
        }
        return academyRename
    }


    private RtfString getInformation(DipAdditionalInformation additionalInformation) {
        def information = new RtfString();

        if (additionalInformation != null) {

            List<DipAddInfoEduForm> addInfoEduFormList = DataAccessServices.dao().getList(DipAddInfoEduForm.class, DipAddInfoEduForm.L_DIP_ADDITIONAL_INFORMATION, additionalInformation)

            if (!additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() == 1 && addInfoEduFormList.get(0) != null) {
                information.append("Форма обучения: " + addInfoEduFormList.get(0).getEduProgramForm().getTitle() + ".")
            } else if (!additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() > 1) {
                information.append("Сочетание форм обучения: ")
                for (DipAddInfoEduForm eduForm : addInfoEduFormList) {
                    if (addInfoEduFormList.indexOf(eduForm) > 0) {
                        information.append(", ");
                    }
                    information.append(eduForm.getEduProgramForm().getTitle());
                }
                information.append(".")
            } else if (additionalInformation.isShowSelfEduForm() && CollectionUtils.isEmpty(addInfoEduFormList)) {
                information.append("Форма получения образования: самообразование.")
            } else if (additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() == 1 && addInfoEduFormList.get(0) != null) {
                String eduFormTitle = ""
                if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.OCHNAYA))
                    eduFormTitle = "очной"
                else if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.ZAOCHNAYA))
                    eduFormTitle = "заочной"
                else if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.OCHNO_ZAOCHNAYA))
                    eduFormTitle = "очно-заочной"
                information.append("Сочетание самообразования и " + eduFormTitle + " формы обучения..")
            } else if (additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() > 1) {
                information.append("Сочетание самообразования и форм обучения: ")
                for (DipAddInfoEduForm eduForm : addInfoEduFormList) {
                    if (addInfoEduFormList.indexOf(eduForm) > 0) {
                        information.append(", ");
                    }
                    information.append(eduForm.getEduProgramForm().getTitle());
                }
                information.append(".")
            }

            String specialization = diplomaObject.getContent().getProgramSpecialization() != null ? diplomaObject.getContent().getProgramSpecialization().getTitle() : "";
            if (additionalInformation.showProgramSpecialization) {
                if (diplomaObject.content.programSubject.subjectIndex.code.equals(EduProgramSubjectIndexCodes.TITLE_2005_62)
                        || diplomaObject.content.programSubject.subjectIndex.code.equals(EduProgramSubjectIndexCodes.TITLE_2005_68)
                        || diplomaObject.content.programSubject.subjectIndex.code.equals(EduProgramSubjectIndexCodes.TITLE_2009_62)
                        || diplomaObject.content.programSubject.subjectIndex.code.equals(EduProgramSubjectIndexCodes.TITLE_2009_68)
                        || diplomaObject.content.programSubject.subjectIndex.code.equals(EduProgramSubjectIndexCodes.TITLE_2013_03)
                        || diplomaObject.content.programSubject.subjectIndex.code.equals(EduProgramSubjectIndexCodes.TITLE_2013_04)) {
                    if (information.toList().size() > 0)
                        information.par()
                    information.append("Направленность (профиль) образовательной программы: " + specialization + ".")
                }
            }
            if (diplomaObject.content.programSubject.subjectIndex.code.equals(EduProgramSubjectIndexCodes.TITLE_2005_65)
                    || diplomaObject.content.programSubject.subjectIndex.code.equals(EduProgramSubjectIndexCodes.TITLE_2009_65)
                    || diplomaObject.content.programSubject.subjectIndex.code.equals(EduProgramSubjectIndexCodes.TITLE_2013_05)) {
                if (additionalInformation.specialization.equals(DipDocumentAddInformationAddEdit.ORIENTATION.title)) {
                    if (information.toList().size() > 0)
                        information.par()
                    information.append("Направленность (профиль) образовательной программы: " + specialization + ".")
                } else if (additionalInformation.specialization.equals(DipDocumentAddInformationAddEdit.SPECIALIZATION.title)) {
                    if (information.toList().size() > 0)
                        information.par()
                    information.append("Специализация:  " + specialization + ".")
                }
            }

            if (additionalInformation.passIntenssiveTraining) {
                if (information.toList().size() > 0)
                    information.par()
                information.append("Пройдено ускоренное обучение по образовательной программе.")
            }

            if (additionalInformation.demand) {
                if (information.toList().size() > 0)
                    information.par()
                information.append("Справка выдана по требованию.")
            }

            List<DipEduInOtherOrganization> otherOrganizationList = DataAccessServices.dao().getList(DipEduInOtherOrganization.class,
                    DipEduInOtherOrganization.dipAdditionalInformation(), additionalInformation);

            for (DipEduInOtherOrganization otherOrganization : otherOrganizationList) {
                if (information.toList().size() > 0)
                    information.par()

                if (otherOrganization.totalCreditsAsDouble != null && otherOrganization.totalCreditsAsDouble > 0) {
                    information.append("Часть образовательной программы в объеме "
                            + getLaborWithUnitInParent(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(otherOrganization.totalCreditsAsDouble))
                            + " освоена в "
                            + otherOrganization.row + ".")
                }

                if (otherOrganization.totalWeeksAsDouble != null && otherOrganization.totalWeeksAsDouble > 0) {
                    information.append("Часть образовательной программы в объеме "
                            + getWeeksWithUnitInParent(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(otherOrganization.totalWeeksAsDouble))
                            + " освоена в "
                            + otherOrganization.row + ".")
                }
            }
        }
        return information
    }

    private String getWeeksWithUnit(String num) {
        if (num == null) {
            return "0 недель"
        } else if (num.contains(".") || num.contains(",")) {
            return num.replace(".", ",") + " недели"
        } else {
            return num.replace(".", ",") + " " + CommonBaseStringUtil.numberPostfixCase(Long.parseLong(num), "неделя", "недели", "недель")
        }
    }

    private String getWeeksWithUnitInParent(String num) {
        if (num == null) {
            return "0 недель"
        } else if (num.contains(".") || num.contains(",")) {
            return num.replace(".", ",") + " недель"
        } else {
            return num.replace(".", ",") + " " + CommonBaseStringUtil.numberPostfixCase(Long.parseLong(num), "недели", "недель", "недель")
        }
    }

    private String getLaborWithUnitInParent(String num) {
        if (num == null) {
            return "0 зачетных единиц"
        } else if (num.contains(".") || num.contains(",")) {
            return num.replace(".", ",") + " зачетных единиц"
        } else {
            return num.replace(".", ",") + " " + CommonBaseStringUtil.numberPostfixCase(Long.parseLong(num), "зачетной единицы", "зачетных единиц", "зачетных единиц")
        }
    }

    private void fillPractice(String practiceRowsLoad, ArrayList<String[]> practiceRows, RtfTableModifier tm) {

        def allRows = new ArrayList<String[]>()

        if (!CollectionUtils.isEmpty(practiceRows)) {
            allRows.add(["Практики", practiceRowsLoad, "x"] as String[])
            allRows.add(["в том числе:"] as String[])

            tm.put("allPractice", allRows as String[][])
            tm.put("practice", practiceRows as String[][])
        } else {
            tm.put("allPractice", "" as String[][])
            tm.put("practice", "" as String[][])
        }

    }

    private void fillFacultDiscipline(ArrayList<String[]> optDisciplineRows, DipAdditionalInformation additionalInformation, RtfTableModifier tm) {
        def allRows = new ArrayList<String[]>()
        allRows.add(["Факультативные дисциплины"] as String[])
        allRows.add(["в том числе:"] as String[])

        if (additionalInformation != null && additionalInformation.showAdditionalDiscipline) {
            tm.put("facultative", allRows as String[][])
            tm.put("facultDiscipline", optDisciplineRows as String[][])
        } else {
            tm.put("facultative", new ArrayList<String[]>() as String[][])
            tm.put("facultDiscipline", new ArrayList<String[]>() as String[][])
        }
    }

    private void fillStateExam(String examRowsLoad, ArrayList<String[]> stateExamRows, ArrayList<String[]> graduateWorkRows, RtfTableModifier tm) {
        def allRows = new ArrayList<String[]>()
        if (!CollectionUtils.isEmpty(stateExamRows) || !CollectionUtils.isEmpty(graduateWorkRows)) {
            allRows.add(["Государственная итоговая аттестация", examRowsLoad, "x"] as String[])
            allRows.add(["в том числе:"] as String[])

            tm.put("attestation", allRows as String[][])
            tm.put("exam", stateExamRows as String[][])
            tm.put("graduateWork", graduateWorkRows as String[][])
        } else {
            tm.put("attestation", "" as String[][])
            tm.put("exam", "" as String[][])
            tm.put("graduateWork", "" as String[][])
        }
    }

    private boolean isMiddleSchool(EducationLevelStage eduStage)
    {
        if (eduStage.code.equals(EducationLevelStageCodes.MIDDLE_SCHOOL))
            return true;
        return eduStage.getParent() != null && isMiddleSchool(eduStage.getParent())
    }

}
