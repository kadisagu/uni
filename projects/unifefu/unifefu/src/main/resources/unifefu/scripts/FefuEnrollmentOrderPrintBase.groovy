/* $Id$ */
package unifefu.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.common.ITitled
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.hibsupport.dql.DQLFunctions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.RtfParagraph
import org.tandemframework.rtf.document.text.field.RtfField
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.node.IRtfGroup
import org.tandemframework.rtf.node.RtfControl
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.commonbase.utils.CommonCollator
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.catalog.entity.codes.EducationLevelStageCodes
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager
import ru.tandemservice.uniec.entity.entrant.*
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder
import ru.tandemservice.unifefu.entity.entrantOrder.gen.EnrollmentOrderFefuExtGen

import java.text.SimpleDateFormat

import static org.tandemframework.hibsupport.dql.DQLExpressions.*
import static ru.tandemservice.uniec.UniecDefines.*
import static ru.tandemservice.unifefu.UniFefuDefines.*

/**
 * @author Nikolay Fedorovskih
 * @since 27.11.2013
 */
class FefuEnrollmentOrderPrintBase
{
    final static def rtfFactory = RtfBean.getElementFactory()
    final static def qcElement = rtfFactory.createRtfControl(IRtfData.QC) // выравнивание по центру
    final static def qjElement = rtfFactory.createRtfControl(IRtfData.QJ) // выравнивание по ширине
    final static def par = rtfFactory.createRtfControl(IRtfData.PAR) // перевод строки (каретка)

    final static Comparator<ITitled> TITLE_COMPARATOR = new Comparator<ITitled>() {
        @Override
        int compare(ITitled o1, ITitled o2)
        {
            return o1.title.compareToIgnoreCase(o2.title)
        }
    }

    static class PreStudentVO
    {
        Double finalMark
        ExternalOrgUnit externalOrgUnit
        EnrollmentExtract extract
        String fio
        Boolean vpoBased
        Integer individMark;

        PreliminaryEnrollmentStudent getPreStudent()
        { extract.entity }

        String getFinalMarkStr()
        {
            CommonBaseStringUtil.numberWithPostfixCase(finalMark.longValue(), 'балл', 'балла', 'баллов')
        }
    }

    static class SchoolLevel
    {
        String title
        int priority
    }

    static boolean isMasterOrderType(EnrollmentOrder order)
    {
        return order.type.code in [
                ORDER_TYPE_MASTER_BUDGET,
                ORDER_TYPE_MASTER_CONTRACT,
                ORDER_TYPE_MASTER_TARGET_BUDGET,
                ORDER_TYPE_MASTER_FOREIGNERS_QUOTA_BUDGET
        ]
    }

    static boolean isSPO(EnrollmentOrder order)
    {
        return order.type.code in [
                ORDER_TYPE_STUDENT_SPO_BUDGET,
                ORDER_TYPE_STUDENT_SPO_CONTRACT,
                ORDER_TYPE_STUDENT_TARGET_SPO_BUDGET,
                ORDER_TYPE_STUDENT_SPO_BUDGET_BY_CREATIVE,
                ORDER_TYPE_STUDENT_SPO_BUDGET_BY_CERTIFICATE,
                ORDER_TYPE_STUDENT_TARGET_SPO_BUDGET_BY_CERTIFICATE,
                ORDER_TYPE_STUDENT_SPO_CONTRACT_BY_CERTIFICATE
        ]
    }

    /**
     * Метод компактификации шаблона параграфа, для печати его в выписках.
     * Нужно, чтобы влезало по 3 выписки на листе. Для этого убираем межстрочные интервалы, шрифт делаем 12.
     */
    static void constrictTemplateForExtracts(List<IRtfElement> elementList, boolean fixWrongLastPar)
    {
        // Просто удалим все теги, задающие шрифт или межстрочный интервал, чтобы применились соответствующие теги родительского документа
        boolean lastParMarked = !fixWrongLastPar
        for (int i = elementList.size() - 1; i >= 0; i--)
        {
            IRtfElement element = elementList.get(i);
            if (element instanceof RtfControl)
            {
                int idx = (element as RtfControl).index;
                if (idx == IRtfData.SL || idx == IRtfData.FS)
                    elementList.remove(i)
            }
            else if (element instanceof IRtfGroup)
            {
                if (!lastParMarked && element instanceof RtfParagraph)
                {
                    // Это хак. См. DEV-1305 п.1
                    (element as RtfParagraph).setWritePar(false)
                    lastParMarked = true
                }
                constrictTemplateForExtracts((element as IRtfGroup).getElementList(), !lastParMarked)
            }
            if (element instanceof RtfField)
            {
                constrictTemplateForExtracts((element as RtfField).getFormatList(), false)
            }
        }
    }

    static void fillParagraphs(RtfDocument mainDocument, Collection<PreStudentVO> preStudents, EnrollmentOrder order, boolean isExtract)
    {
        RtfDocument paragraphDocument = new RtfReader().read(EcOrderManager.instance().dao().getParagraphTemplate(order.type))

        if (isExtract)
            constrictTemplateForExtracts(paragraphDocument.elementList, true)

        boolean isNeedRemoteEducationMark = order.type.code in [
                ORDER_TYPE_STUDENT_CONTRACT,
                ORDER_TYPE_STUDENT_CONTRACT_SHORT,
                ORDER_TYPE_SECOND_HIGH
        ]

        // Берем первую выписку
        if (preStudents.empty)
            return;
        def firstObject = preStudents.iterator().next()

        def parModifier = new RtfInjectModifier()
        def ou = firstObject.preStudent.educationOrgUnit

        // Форма освоения
        CommonExtractPrint.initDevelopForm(parModifier, ou, '')

        // Курс зачисления
        parModifier.put('course', String.valueOf(firstObject.extract.course.intValue))

        // Срок освоения
        parModifier.put('developPeriod', ou.developPeriod.title)

        // Технология
        def techBuf = new StringBuilder()
        def techCodeSet = new HashSet<String>(2)
        def techName = new HashMap<String, String>(2)
        techName[UniDefines.DEVELOP_TECH_SIMPLE] = 'обычных'
        techName[UniDefines.DEVELOP_TECH_REMOTE] = 'дистанционных'
        preStudents.each {
            def code = it.preStudent.educationOrgUnit.developTech.code
            if (!(code in techCodeSet))
            {
                techCodeSet.add(code)
                if (techBuf.length() > 0)
                    techBuf.append(', ')
                techBuf.append(techName[code])
            }
        }
        boolean isPrintOrgUnitsInHead = UniDefines.DEVELOP_TECH_SIMPLE in techCodeSet
        def techStr = isNeedRemoteEducationMark && UniDefines.DEVELOP_TECH_REMOTE in techCodeSet ? ('с применением ' + techBuf.toString() + ' образовательных технологий') : ''
        parModifier.put('developTech', techStr.isEmpty() ? '' : techStr + ' ')

        // Формирующее подразделение (по-хорошему, должно быть одно на весь приказ)
        boolean spo = isSPO(order)
        if (spo)
            parModifier.put('formativeOrgUnit', isPrintOrgUnitsInHead ? getFormativeOrgUnitListStr(preStudents) : '')

        // Территориальное подразделение (по-хорошему, должно быть одно на весь приказ)
        def terrStr = isPrintOrgUnitsInHead ? getTerritorialOrgUnitListStr(preStudents) : ''
        parModifier.put('territorialOrgUnit', terrStr.isEmpty() ? '' : "в ${terrStr} ")

        // Дата решения приемной комиссии
        def orderFefuExt = IUniBaseDao.instance.get().getByNaturalId(new EnrollmentOrderFefuExtGen.NaturalId(order)) as EnrollmentOrderFefuExtGen
        parModifier.put('resolutionDate', orderFefuExt != null ? orderFefuExt.resolutionDate.format('dd.MM.yyyy') : '')

        // Учебный год
        def eduYear = order.enrollmentCampaign.educationYear.intValue
        parModifier.put('eduYear', String.valueOf(eduYear))

        // Дата зачисления
        def enrollmentDate = order.enrollmentDate
        if (enrollmentDate == null)
        {
            // Если дата зачисления не указана, выводим 1 сентября
            Calendar calendar = Calendar.instance
            calendar.set(eduYear, Calendar.SEPTEMBER, 1)
            enrollmentDate = calendar.time
        }
        parModifier.put('enrollmentDate', new SimpleDateFormat("d MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(enrollmentDate))

        // Формируем список студентов
        //noinspection GrDeprecatedAPIUsage
        parModifier.put('STUDENT_LIST', spo ? addSPOSubparagraphRTF(preStudents) : addSubparagraphRTF(preStudents))

        parModifier.modify(paragraphDocument)

        // В приказах ДВФУ один единственный параграф
        RtfUtil.modifySourceList(mainDocument.header, paragraphDocument.header, paragraphDocument.elementList)
        def group = rtfFactory.createRtfGroup()
        group.elementList = paragraphDocument.elementList
        //noinspection GrDeprecatedAPIUsage
        new RtfInjectModifier().put('PARAGRAPHS', [group]).modify(mainDocument)
    }

    static Collection<PreStudentVO> loadPreStudents(EnrollmentOrder order, Session getSession, Long extractId)
    {
        int colIdx = -1, masterIdx = -1, targetIdx = -1, entityIdx, fioIdx, vpoBasedIdx = -1, indMarkInd = -1
        boolean isContractStudentOrderType = order.type.code in [
                ORDER_TYPE_STUDENT_CONTRACT
        ]
        // Запрос получения всех включенных в приказ абитуриентов (предзачисленных студентов) вместе с итоговымми баллами
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, 'e')
        builder.joinPath(DQLJoinType.inner, EnrollmentExtract.entity().fromAlias('e'), 'pre')
        builder.joinPath(DQLJoinType.inner, PreliminaryEnrollmentStudent.requestedEnrollmentDirection().fromAlias('pre'), 'red')
        builder.joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().fromAlias('red'), 'card')
        builder.column('e')

        // Для печати выписки нужна только одна выписка из приказа
        if (extractId != null)
            builder.where(eq(property("e.id"), value(extractId)))

        entityIdx = ++colIdx
        builder.column(property(IdentityCard.fullFio().fromAlias('card')))
        fioIdx = ++colIdx
        if (!isMasterOrderType(order))
        {
            // Поздапрос получения итогового балла. Для магистратуры не нужно выводить баллы
            DQLSelectBuilder subQuery = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, 'd')
            subQuery.column(DQLFunctions.sum(property(ChosenEntranceDiscipline.finalMark().fromAlias('d'))))
            subQuery.where(isNotNull(property(ChosenEntranceDiscipline.finalMark().fromAlias('d'))))
            subQuery.where(eq(
                    property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias('d')),
                    property(RequestedEnrollmentDirection.id().fromAlias('red'))
            ))

            builder.column(subQuery.buildQuery())
            masterIdx = ++colIdx
        }
        if (isContractStudentOrderType)
        {
            // Для контрактников надо вычленить тех, кто получает второе высшее, чтобы им сделать нужную приписку ("на базе ВПО")
            builder.column(DQLFunctions.nullif(
                    property(PreliminaryEnrollmentStudent.studentCategory().code().fromAlias('pre')),
                    value(UniDefines.STUDENT_CATEGORY_SECOND_HIGH)
            ))
            vpoBasedIdx = ++colIdx
        }
        if (order.type.targetAdmission)
        {
            // Для целевиков нужно будет название организации
            builder.joinPath(DQLJoinType.left, RequestedEnrollmentDirection.externalOrgUnit().fromAlias('red'), 'ext')
            builder.column('ext')
            targetIdx = ++colIdx
        }

        if (order.enrollmentCampaign.useIndividualProgressInAmountMark)
        {
            DQLSelectBuilder indMarkBuilder = new DQLSelectBuilder().fromEntity(EntrantIndividualProgress.class, "enPr")
            indMarkBuilder.column(DQLFunctions.sum(property(EntrantIndividualProgress.mark().fromAlias("enPr"))))
            indMarkBuilder.where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().fromAlias("red")),
                                property(EntrantIndividualProgress.entrant().fromAlias("enPr"))))
            indMarkBuilder.where(exists(new DQLSelectBuilder().fromEntity(EnrCampaignEntrantIndividualProgress.class, "ecp")
                    .where(eq(
                        property(EnrCampaignEntrantIndividualProgress.individualProgress().fromAlias("ecp")),
                        property("enPr", EntrantIndividualProgress.individualProgressType())))
                    .where(eq(property(EnrCampaignEntrantIndividualProgress.enrollmentCampaign().fromAlias("ecp")), value(order.enrollmentCampaign)))
                    .buildQuery()
            ))
            indMarkBuilder.group(property(EntrantIndividualProgress.entrant().fromAlias("enPr")))

            builder.column(indMarkBuilder.buildQuery())
            indMarkInd = ++colIdx
        }
        builder.where(eq(property(EnrollmentExtract.paragraph().order().id().fromAlias('e')), value(order.id)))

        def result = new TreeSet<PreStudentVO>(new Comparator<PreStudentVO>() {
            @Override int compare(PreStudentVO a, PreStudentVO b) {
                int i = CommonCollator.RUSSIAN_COLLATOR.compare(a.fio, b.fio)
                if (i != 0) { return i }
                return Long.compare(a.preStudent.id, b.preStudent.id)
            }
        })

        builder.createStatement(getSession).<Object[]> list().each {
            def preStudentVO = new PreStudentVO()
            preStudentVO.extract = it[entityIdx] as EnrollmentExtract
            preStudentVO.fio = it[fioIdx] as String
            preStudentVO.finalMark = masterIdx < 0 ? null : it[masterIdx] as Double
            preStudentVO.vpoBased = vpoBasedIdx < 0 ? false : (it[vpoBasedIdx] == null)
            preStudentVO.externalOrgUnit = targetIdx < 0 ? null : it[targetIdx] as ExternalOrgUnit
            preStudentVO.individMark = indMarkInd < 0 ? 0 : it[indMarkInd] as Integer
            result.add(preStudentVO)
        }
        return result
    }

    static String getLevelTypeStr(StructureEducationLevels levelType)
    {
        if (levelType.isSpecialization() || (levelType.isProfile() && (levelType.isMaster() || levelType.isBachelor())))
        {
            if (levelType.isSpecialization()) return 'Специализация'
            if (levelType.isMaster()) return 'Магистерская программа'
            return 'Бакалаврский профиль'
        }
        return levelType.isSpecialty() ? 'Специальность' : 'Направление'
    }

    static void injectSubparagraph(Collection<PreStudentVO> items, List<IRtfElement> elements, boolean isSPO)
    {
        // Группируем по направлению подготовки
        Map<EducationLevelsHighSchool, List<PreStudentVO>> map = new TreeMap<>(TITLE_COMPARATOR)
        items.each {
            def highSchool = it.preStudent.educationOrgUnit.educationLevelHighSchool
            def list = map[highSchool]
            if (list == null)
                map.put(highSchool, list = [])
            list.add(it)
        }

        for (def iterator = map.entrySet().iterator(); iterator.hasNext();)
        {
            def entry = iterator.next()
            def rtf = new RtfString()

            // Тип уровня образования
            if (isSPO)
            {
                elements.add(qcElement)
                rtf.boldBegin().append(entry.key.title).boldEnd()
            }
            else
            {
                rtf.append(getLevelTypeStr(entry.key.educationLevel.levelType)).append(' ')
                rtf.boldBegin().append(StringUtils.uncapitalize(entry.key.title)).boldEnd()
            }

            elements.addAll(rtf.toList())
            rtf.toList().clear()

            elements.addAll([par, qjElement])

            // Список абитуриентов
            int counter = 0
            for (def iterator2 = entry.value.iterator(); iterator2.hasNext();)
            {
                def preStudent = iterator2.next()

                // Порядковый номер
                rtf.append("    ${++counter}    ")

                // ФИО
                rtf.append(preStudent.fio)

                // Итоговый балл (если есть) и приписка о втором высшем для контрактников (если есть)
                if (preStudent.finalMark != null || preStudent.vpoBased)
                {
                    rtf.append(' (')
                    if (preStudent.finalMark != null)
                    {
                        preStudent.finalMark += preStudent.individMark? preStudent.individMark:0
                        rtf.append(preStudent.finalMarkStr)
                        if (preStudent.vpoBased)
                            rtf.append(', ')
                    }
                    if (preStudent.vpoBased)
                        rtf.append('на базе ВПО')
                    rtf.append(')')
                }

                // Организация для ЦП (если есть)
                if (preStudent.externalOrgUnit != null)
                    rtf.append(" (${preStudent.externalOrgUnit.title})")

                if (iterator2.hasNext())
                    rtf.par()
            }
            elements.addAll(rtf.toList())

            // Параграф между направлениями
            if (iterator.hasNext())
                elements.add(par)
        }
    }

    // Тут ВПО (для СПО свой метод)
    static List<IRtfElement> addSubparagraphRTF(Collection<PreStudentVO> preStudents)
    {
        // Группируем предстудентов по формирующему подразделению
        Map<OrgUnit, List<PreStudentVO>> orgUnitListMap = new TreeMap<>(TITLE_COMPARATOR)
        preStudents.each {
            def orgUnit = it.preStudent.educationOrgUnit.formativeOrgUnit
            def preStudentVOList = orgUnitListMap[orgUnit]
            if (preStudentVOList == null)
                orgUnitListMap.put(orgUnit, preStudentVOList = [])
            preStudentVOList.add(it)
        }

        def injectElements = []
        for (def iterator = orgUnitListMap.entrySet().iterator(); iterator.hasNext();)
        {
            def entry = iterator.next()

            // Название оргюнита полужирным шрифтом
            injectElements.addAll(new RtfString().boldBegin().append(entry.key.printTitle).boldEnd().toList())
            injectElements.add(par)

            // Вставляем списки студентов, сгруппированные по направлениям
            injectSubparagraph(entry.value, injectElements, false)

            // Пустая строка между оргюнитами
            if (iterator.hasNext())
                injectElements.addAll([par, par])
        }

        return injectElements
    }

    static String getFormativeOrgUnitListStr(Collection<PreStudentVO> preStudents)
    {
        def set = new TreeSet<>(TITLE_COMPARATOR)
        preStudents.each {
            set.add(it.preStudent.educationOrgUnit.formativeOrgUnit)
        }

        def buffer = new StringBuilder()
        for (Iterator<OrgUnit> iterator = set.iterator(); iterator.hasNext();)
        {
            OrgUnit orgUnit = iterator.next()
            buffer.append(orgUnit.accusativeCaseTitle ?: orgUnit.printTitle)
            if (iterator.hasNext())
                buffer.append(', ')
        }
        return buffer.toString()
    }

    static def String getTerritorialOrgUnitListStr(Collection<PreStudentVO> preStudents)
    {
        def set = new TreeSet<>(TITLE_COMPARATOR)
        preStudents.each { set.add(it.preStudent.educationOrgUnit.territorialOrgUnit) }

        def buffer = new StringBuilder()
        for (Iterator<OrgUnit> iterator = set.iterator(); iterator.hasNext();)
        {
            def orgUnit = iterator.next()
            if (!orgUnit.isTop()) // Головной оргюнит (ДВФУ) выводить не надо
            {
                buffer.append(StringUtils.uncapitalize(orgUnit.accusativeCaseTitle ?: orgUnit.printTitle))
                if (iterator.hasNext())
                    buffer.append(', ')
            }
        }
        return buffer.toString()
    }

    static
    def injectSchoolLevel(List<PreStudentVO> preStudents, SchoolLevel level, List<IRtfElement> elements, boolean isSPO)
    {
        if (preStudents != null)
        {
            // Выравнивание по центру
            elements.add(qcElement)

            // Вставляем название уровня полужирным шрифтом
            elements.addAll(new RtfString().boldBegin().append("${level.title}:").boldEnd().toList())
            elements.add(par)

            // Вставляем список абитуриентов по направлениям
            injectSubparagraph(preStudents, elements, isSPO)
        }
    }

    /**
     * Для СПО своя логика - группировка по уровню образования (9 или 11 классов)
     */
    static List<IRtfElement> addSPOSubparagraphRTF(Collection<PreStudentVO> preStudents)
    {
        Map<SchoolLevel, List<PreStudentVO>> map = new TreeMap<>(new Comparator<SchoolLevel>() {
            @Override
            int compare(SchoolLevel o1, SchoolLevel o2)
            {
                if (o1.priority != o2.priority)
                    return o1.priority.compareTo(o2.priority)
                return o1.title.compareTo(o2.title)
            }
        })

        final Map<String, SchoolLevel> LEVELS_STAGE_DEFINES = [
                (EducationLevelStageCodes.MAIN_SCHOOL)  : new SchoolLevel(title: 'на базе основного общего образования', priority: 1),
                (EducationLevelStageCodes.MIDDLE_SCHOOL): new SchoolLevel(title: 'на базе среднего (полного) общего образования', priority: 2),
                (EducationLevelStageCodes.POLNOE_OBTSHEE_PROFILNOE): new SchoolLevel(title: 'на базе основного общего образования', priority: 2),
                (EducationLevelStageCodes.POLNOE_OBTSHEE_SPETSIALNOE): new SchoolLevel(title: 'на базе среднего общего образования', priority: 2),
                (EducationLevelStageCodes.TITLE_1_3_KLASS): new SchoolLevel(title: 'на базе начального общего образования', priority: 3),
                (EducationLevelStageCodes.BEGIN_PROF): new SchoolLevel(title: 'на базе начального профессионального образования', priority: 3),
                (EducationLevelStageCodes.MIDDLE_PROF): new SchoolLevel(title: 'на базе среднего профессионального образования', priority: 3),
                (EducationLevelStageCodes.HIGH_PROF): new SchoolLevel(title: 'на базе высшего профессионального образования', priority: 3),
                (EducationLevelStageCodes.AFTER): new SchoolLevel(title: 'на базе послевузовского профессионального образования', priority: 3),
                (EducationLevelStageCodes.ADDITIONAL): new SchoolLevel(title: 'на базе дополнительного образования', priority: 3),
                (EducationLevelStageCodes.PRE_SCHOOL): new SchoolLevel(title: 'на базе дошкольного образования', priority: 3),
                (EducationLevelStageCodes.PROF_PREP): new SchoolLevel(title: 'на базе профессиональной подготовки', priority: 3),
        ]

        preStudents.each {
            SchoolLevel level
            def stage = it.preStudent.requestedEnrollmentDirection.entrantRequest.entrant.person.personEduInstitution?.educationLevelStage
            if (stage != null)
            {
                while (stage.parent != null && LEVELS_STAGE_DEFINES[stage.code] == null)
                    stage = stage.parent

                level = LEVELS_STAGE_DEFINES[stage.code];
                if (level == null)
                    level = new SchoolLevel(title: StringUtils.uncapitalize(stage.title), priority: 99)
            }
            else
            {
                level = new SchoolLevel(title: 'не предоставившие документ об образовании', priority: 100)
            }

            def items = map[level]
            if (items == null)
                map.put(level, items = [])
            items.add(it)
        }

        List<IRtfElement> injectElements = []

        for (def iterator = map.entrySet().iterator(); iterator.hasNext();)
        {
            def entry = iterator.next()

            // Вставляем список абитуриентов, сгруппированный по направлениям
            injectSchoolLevel(entry.value, entry.key, injectElements, true)

            // Пустая строка между типами
            if (iterator.hasNext())
                injectElements.addAll([par, par])
        }
        return injectElements
    }
}