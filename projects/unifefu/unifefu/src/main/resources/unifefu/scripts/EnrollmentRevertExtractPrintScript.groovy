/* $Id$ */
package unifefu.scripts

import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.runtime.ApplicationRuntime
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract

import java.text.SimpleDateFormat

return new FefuEnrollmentRevertExtractPrint(
        session: session,
        template: templateDocument,
        extract: session.get(EnrollmentRevertExtract.class, object)
).print()

/**
 * @author Nikolay Fedorovskih
 * @since 09.08.2013
 */
class FefuEnrollmentRevertExtractPrint
{
    Session session
    RtfDocument template
    EnrollmentRevertExtract extract

    def print()
    {
        def document = template

        def order = extract.order
        def academy = TopOrgUnit.instance
        def canceledOrder = extract.paragraph.canceledOrder
        def person = extract.entity.entrantRequest.entrant.person

        new RtfInjectModifier()
                .put('highSchoolTitle_G', academy.genitiveCaseTitle ?: academy.printTitle)
                .put('orderDate', order.commitDate != null ? (new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(order.commitDate) + ' г.') : null)
                .put('orderNumber', order.number)
                .put('canceledOrderDate', canceledOrder.commitDate != null ? new SimpleDateFormat("dd.MM.yyyy").format(canceledOrder.commitDate) : null)
                .put('canceledOrderNumber', canceledOrder.number)
                .put('fioEntrant_A', PersonManager.instance().declinationDao().getDeclinationFIO(person.identityCard, GrammaCase.ACCUSATIVE))
                .put('direction', extract.educationOrgUnit.educationLevelHighSchool.printTitle)
                .put('enrFine', person.male ? 'ого' : 'ую')
                .modify(document)

        new RtfTableModifier()
                .put('VISAS', [[ApplicationRuntime.getProperty("fefu.signer.post") ?: "И.о. проректора по учебной и воспитательной работе",
                                ApplicationRuntime.getProperty("fefu.signer.fio") ?: "И.В. Соппа"]] as String[][])
                .modify(document)

        return [document: document, fileName: 'EnrollmentRevertExtract.rtf']
    }
}