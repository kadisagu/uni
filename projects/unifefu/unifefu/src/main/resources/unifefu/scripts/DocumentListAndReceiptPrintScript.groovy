package unifefu.scripts

import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument
import ru.tandemservice.uniec.entity.entrant.EntrantRequest
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument


return new DocumentListAndReceiptPrint (                           // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати описи и расписки абитуриента
 *
 * @author amakarova
 */
class DocumentListAndReceiptPrint extends UnifefuPrintScript
{
    Session session
    byte[] template
    EntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        // получаем список выбранных направлений приема для конкретного заявления
        def directions = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().id()} = ${entrantRequest.id}
                order by ${RequestedEnrollmentDirection.entrantRequest().regDate()}, ${RequestedEnrollmentDirection.priority()}
        /).<RequestedEnrollmentDirection> list()
        RequestedEnrollmentDirection requestedEnrollmentDirection = directions.isEmpty() ? null : directions.get(0);

        def identityCard = entrantRequest.entrant.person.identityCard;
        im.put('highSchoolTitle', TopOrgUnit.instance.title)
        im.put('developForm', getDevelopFormTitle(requestedEnrollmentDirection))
        im.put('personalNumber', entrantRequest.entrant.personalNumber)
        im.put('lastName', identityCard.lastName)
        im.put('firstName', identityCard.firstName)
        im.put('middleName', identityCard.middleName)

        def directionTitles = new ArrayList<String[]>()
        for (RequestedEnrollmentDirection reqDirection : directions) {
            String[] row = new String[1];
            def compensationType = reqDirection.compensationType.budget ? "бюджет" : "договор"
            def remote = (!reqDirection.compensationType.budget) && DevelopTechCodes.DISTANCE.equals(reqDirection.enrollmentDirection.educationOrgUnit.developTech.code)?
                    ", дистанционная":""
            def developCondition = reqDirection.enrollmentDirection.educationOrgUnit.developCondition
            def condition = "";
            if (DevelopConditionCodes.REDUCED_PERIOD.equals(developCondition.code)){
                condition = ", сокращенная программа"
            } else if (DevelopConditionCodes.ACCELERATED_PERIOD.equals(developCondition.code)) {
                condition = ", ускоренная программа"
            } else if (DevelopConditionCodes.REDUCED_ACCELERATED_PERIOD.equals(developCondition.code)) {
                condition = ", сокращенная ускоренная программа"
            }
            row[0] = getEducationLevelTitle(reqDirection) + " (" + compensationType + remote + condition + ")";
            directionTitles.add(row);
        }
        tm.put("eduLevelTitle", directionTitles as String[][]);

        im.put('fromFIO', PersonManager.instance().declinationDao().getCalculatedFIODeclination(identityCard, InflectorVariantCodes.RU_GENITIVE))

        // получаем названия документов согласно настройке 'Используемые документы для подачи в ОУ и их порядок' 
        def titles = getTitles()

        int i = 1
        tm.put('T1', titles.collect {[i++, it]} as String[][])
        i = 1
        tm.put('T2', titles.collect {[(i++) + '.', it]} as String[][])

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Опись и расписка абитуриента ${identityCard.fullFio}.rtf"]
    }

    def getTitles()
    {
        def entrant = entrantRequest.entrant          // абитуриент
        def edu = entrant.person.personEduInstitution // основное законченное образовательное учреждение

        // получаем список документов из заявления в порядке приоритета
        def documents = DQL.createStatement(session, /
                from ${EntrantEnrollmentDocument.class.simpleName}
                where ${EntrantEnrollmentDocument.entrantRequest().id()} = ${entrantRequest.id}
                order by ${EntrantEnrollmentDocument.enrollmentDocument().priority()}
        /).<EntrantEnrollmentDocument> list()

        // получаем номера всех зачтенных свидетельств ЕГЭ абитуриента
        def stateExamCertificateList = DQL.createStatement(session, /
                select ${EntrantStateExamCertificate.number()}
                from ${EntrantStateExamCertificate.class.simpleName}
                where ${EntrantStateExamCertificate.entrant().id()} = ${entrant.id} and
                      ${EntrantStateExamCertificate.accepted()} = ${Boolean.TRUE}
        /).<String> list()

        def stateExamCertificateStr = new StringBuilder()
        for (String certificate: stateExamCertificateList)
            stateExamCertificateStr.append("№ " + [certificate.substring(0,2), certificate.substring(2,11), certificate.substring(11)].join("-")).append(", ")

        def stateExamCertificate = stateExamCertificateStr.toList().isEmpty() ? "" : stateExamCertificateStr.substring(0, stateExamCertificateStr.size() - 2)

        // загружаем настройку 'Используемые документы для подачи в ОУ и их порядок'
        def settingMap = DQL.createStatement(session, /
                from ${UsedEnrollmentDocument.class.simpleName}
                where ${UsedEnrollmentDocument.enrollmentCampaign().id()} = ${entrant.enrollmentCampaign.id}
        /).<UsedEnrollmentDocument> list()
                .<EnrollmentDocument, UsedEnrollmentDocument> collectEntries {[it.enrollmentDocument, it]}

        // для каждого документа, согласно настройке, получаем его название
        def titles = new ArrayList<String>()
        for (def document: documents)
        {
            def title = new StringBuilder(document.enrollmentDocument.title)

            def setting = settingMap.get(document.enrollmentDocument)

            if (setting?.printOriginalityInfo)
                title.append(' (').append(document.copy ? 'копия' : 'оригинал').append(')')

            if (setting?.printEduDocumentAttachmentInfo)
            {
                if (edu.seriaApplication != null) title.append(', серия ').append(edu.seriaApplication);
                if (edu.numberApplication != null) title.append(', № ').append(edu.numberApplication);
            }

            if ((setting?.printStateExamCertificateNumber && !stateExamCertificate.isEmpty()) || (setting?.printEducationDocumentInfo && edu))
            {
                if (setting?.printEducationDocumentInfo && edu)
                {
                    if (edu.seria)
                        title.append(' серия ').append([edu.seria].grep().join(' '))
                    if (edu.number)
                        title.append(' № ').append([edu.number].grep().join(' '))
                    title.append(", выданный ");
                    if (edu.eduInstitution)
                        title.append(edu.eduInstitution.title).append(' ')
                    if (edu.addressItem.titleWithType)
                        title.append(edu.addressItem.titleWithType).append(' ')
                    if (edu.issuanceDate)
                        title.append(edu.issuanceDate.format('dd.MM.yyyy'))
                }

                if ((setting?.printStateExamCertificateNumber && !stateExamCertificate.isEmpty()) && (setting?.printEducationDocumentInfo && edu))
                    title.append("; ")

                if (setting?.printStateExamCertificateNumber && !stateExamCertificate.isEmpty())
                {
                    title.append(stateExamCertificate)
                }
            }
            titles.add(title.toString())
        }
        return titles
    }
}
