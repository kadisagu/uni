package unifefu.scripts

import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool
import ru.tandemservice.uniec.UniecDefines
import ru.tandemservice.uniec.entity.entrant.Entrant
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection
import ru.tandemservice.unifefu.UniFefuDefines

return new EnrollmentExamListPrint(         // стандартные входные параметры скрипта
        session: session,                   // сессия
        template: template,                 // шаблон
        typeList: typeList,
        hs: !hsId ? null : (EducationLevelsHighSchool) session.get(EducationLevelsHighSchool.class, hsId),
        formativeOrgUnit: formativeOrgUnit,
        disciplineTitle: disciplineTitle,
        table: table
).print()

/**
 * Алгоритм печати ведомостей абитуриентов
 *
 * @author amakarova
 * @since 17.05.2013
 */
class EnrollmentExamListPrint
{
    Session session
    byte[] template
    String typeList                  // тип ведомости
    EducationLevelsHighSchool hs     // значение фильтра 'Направление подготовки (специальность)'
    String formativeOrgUnit          // значение фильтра 'Формирующее подразделение'
    String disciplineTitle           // значение фильтра 'Дисциплина'

    /**
     * cтроки таблицы, каждая строка - это map [поле -> значение]
     * все поля not-null, mark, paperCode, examiners может быть пустой строкой
     * <поле>                  <тип>      <описание>
     * entrantId               Long       идентификатор Entrant (Абитуриент)
     * disciplineId            Long       идентификатор ExamPassDiscipline (Дисциплина для сдачи)
     * mark                    String     оценка
     * paperCode               String     Шифр работы
     * examiners               String     ФИО экзаменаторов
     * directionIds            Set<Long>  идентификаторы RequestedEnrollmentDirection (Выбранное направление приема)
     */
    List<Map<String, Object>> table

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def data = []
        for (def row: table)
        {
            Entrant entrant = (Entrant) session.get(Entrant.class, (Long) row['entrantId'])
            // если абитуриент не архивный и есть внп с состоянием не забрал документы, то печатаем такую строку
            if (!entrant.archival && ((Set<Long>) row['directionIds']).find {
                def direction = (RequestedEnrollmentDirection) session.get(RequestedEnrollmentDirection.class, it)
                !direction.entrantRequest.takeAwayDocument
            })
            {
                def discipline = (ExamPassDiscipline) session.get(ExamPassDiscipline.class, (Long) row['disciplineId'])
                if (typeList.equals(UniecDefines.SCRIPT_ENROLLMENT_EXAM_LIST)) {
                    def mark = (String) row['mark']
                    def male = entrant.person.identityCard.sex.male
                    data.add([data.size() + 1,                                            // № п/п
                            discipline.entrantExamList.number,                            // № экз. листа
                            entrant.person.identityCard.fullFio,                          // ФИО
                            mark.equals('н') ? (male ? 'не явился' : 'не явилась') : mark // Балл
                    ] as String[])
                }
                if (typeList.equals(UniFefuDefines.SCRIPT_ENROLLMENT_SIT_LIST)) {
                    def card = entrant.person.identityCard
                    data.add([data.size() + 1,                                            // № п/п
                            card.seria + " № " + card.number,                            // серия, № паспорта
                            entrant.person.identityCard.fullFio,                          // ФИО
                    ] as String[])
                }
                if (typeList.equals(UniFefuDefines.SCRIPT_ENROLLMENT_SIT_EXAM_CARD_LIST)) {
                    def card = entrant.person.identityCard
                    data.add([data.size() + 1,                                            // № п/п
                            card.seria + " № " + card.number,                            // серия, № паспорта
                            entrant.person.identityCard.fullFio,                          // ФИО
                    ] as String[])
                }
                if (typeList.equals(UniFefuDefines.SCRIPT_ENROLLMENT_CIPHER_LIST)) {
                    data.add([data.size() + 1,                                            // № п/п
                            "",                                                             // Шифр
                            entrant.person.identityCard.fullFio,                          // ФИО
                    ] as String[])
                }
            }
        }

        if (data.empty)
            throw new ApplicationException('Нет абитуриентов для печати. Архивные, забравшие документы и ' +
                    'выбывшие из конкурса абитуриенты не попадают в ведомость при печати.')

        im.put('highSchoolTitle', TopOrgUnit.instance.title)
        im.put('educationOrgUnit', hs ? hs.title : "")
        im.put('examPassDiscipline', disciplineTitle)
        im.put('formativeOrgUnit', formativeOrgUnit)
        tm.put('T', data as String[][])

        def fileName = 'Экзаменационная ведомость.rtf';
        if (typeList.equals(UniFefuDefines.SCRIPT_ENROLLMENT_SIT_LIST)) fileName = 'Посадочная ведомость.rtf'
        if (typeList.equals(UniFefuDefines.SCRIPT_ENROLLMENT_SIT_EXAM_CARD_LIST)) fileName = 'Посадочная ведомость c ЭБ.rtf'
        if (typeList.equals(UniFefuDefines.SCRIPT_ENROLLMENT_CIPHER_LIST)) fileName = 'Шифровальная ведомость.rtf'
        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm), fileName: fileName]
    }
}
