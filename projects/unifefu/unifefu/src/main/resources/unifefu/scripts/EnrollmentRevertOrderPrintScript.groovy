/* $Id$ */
package unifefu.scripts

import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.RtfHeader
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertExtract
import ru.tandemservice.uniec.entity.orders.EnrollmentRevertParagraph
import ru.tandemservice.unimove.IAbstractParagraph

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

return new FefuEnrollmentRevertOrderPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        order: session.get(EnrollmentOrder.class, object) // объект печати
).print()

/**
 * @author Nikolay Fedorovskih
 * @since 09.08.2013
 */

class FefuEnrollmentRevertOrderPrint
{
    Session session
    byte[] template
    EnrollmentOrder order
    def rtfFactory = RtfBean.elementFactory
    def parElement = rtfFactory.createRtfControl(IRtfData.PAR) // перевод строки (каретка)

    def print()
    {
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()
        RtfDocument doc = new RtfReader().read(template)

        im.put('commitDate', order.commitDate?.format('dd.MM.yyyy'))
        im.put('enrollmentDate', order.enrollmentDate?.format('dd.MM.yyyy'))
        im.put('orderNumber', order.number)
        im.put('orderType', order.type.title)
        im.put('orderBasicText', order.orderBasicText)
        im.put('orderText', order.orderText)
        im.put('textParagraph', order.textParagraph)
        im.put('executor', order.executor)
        im.put('orderReason', order.reason?.title)
        im.put('orderBasic', order.basic?.title)
        im.put('PARAGRAPHS', getParagraphs(doc.header))
        im.put('orderIdWithLabel', '')
        tm.put('VISAS', [['Ректор', 'С.В. Иванец']] as String[][])

        im.modify(doc)
        tm.modify(doc)
        return [document: RtfUtil.toByteArray(doc), fileName: 'EnrollmentRevertOrder.rtf']
    }

    def List<IRtfElement> getSubParagraphs(IAbstractParagraph paragraph)
    {
        Map<OrgUnit, List<EnrollmentRevertExtract>> map = [:]
        List<EnrollmentRevertExtract> extractList = new DQLSelectBuilder().fromEntity(EnrollmentRevertExtract.class, "e")
                .where(eq(property("e", EnrollmentRevertExtract.paragraph()), value(paragraph)))
                .order(property("e", EnrollmentRevertExtract.entity().entrantRequest().entrant().person().identityCard().fullFio()))
                .createStatement(session).list()

        extractList.each {
            def key = it.educationOrgUnit.formativeOrgUnit
            List<EnrollmentRevertExtract> list = map.get(key)
            if (list == null)
                map.put(key, list = [])
            list.add(it)
        }

        List<IRtfElement> elements = []

        for (def iterator = getSortedOrgUnits(map.keySet()).iterator(); iterator.hasNext();)
        {
            def orgUnit = iterator.next()
            elements.addAll(new RtfString().boldBegin().append(orgUnit.printTitle).boldEnd().par().toList())

            // Группируем по направлению подготовки
            Map<EducationLevelsHighSchool, List<EnrollmentRevertExtract>> subMap = [:]
            map.get(orgUnit).each {
                def key = it.educationOrgUnit.educationLevelHighSchool
                List<EnrollmentRevertExtract> list = subMap.get(key)
                if (list == null)
                    subMap.put(key, list = [])
                list.add(it)
            }

            for (def iter2 = getSortedHighSchools(subMap.keySet()).iterator(); iter2.hasNext(); )
            {
                def dir = iter2.next()
                List<EnrollmentRevertExtract> directionExtractList = subMap.get(dir)
                def rtfString = new RtfString()
                        .append(getLevelTypeStr(dir.educationLevel.levelType))
                        .append(' ').boldBegin()
                        .append(dir.title.toLowerCase())
                        .boldEnd()
                        .par()

                int counter = 1
                directionExtractList.each {
                    rtfString.append("    ${counter++}    ")
                    rtfString.append(it.entity.entrantRequest.entrant.person.identityCard.fullFio)
                    rtfString.par()
                }
                //if (iter2.hasNext())
                //    rtfString.par()

                elements.addAll(rtfString.toList())
            }

            if (iterator.hasNext())
                elements.add(parElement)
        }

        return elements
    }

    def List<IRtfElement> getParagraphs(RtfHeader header)
    {
        byte[] tpl = EcOrderManager.instance().dao().getParagraphTemplate(order.type)
        RtfDocument paragraphDocument = new RtfReader().read(tpl)

        List<IRtfElement> paragraphList = []
        List<IAbstractParagraph> parList = order.paragraphList
        parList.each {
            RtfDocument paragraphPart = paragraphDocument.clone
            RtfUtil.modifySourceList(header, paragraphPart.header, paragraphPart.elementList)

            EnrollmentOrder canceledOrder = (it as EnrollmentRevertParagraph).canceledOrder
            new RtfInjectModifier()
                    .put('parNumber', parList.size() > 1 ? (it.number.toString() + '. ') : '')
                    .put('canceledOrderDate', canceledOrder.commitDate?.format('dd.MM.yyyy'))
                    .put('canceledOrderNumber', canceledOrder.number)
                    .put('STUDENT_LIST', getSubParagraphs(it))
                    .modify(paragraphPart)

            def group = rtfFactory.createRtfGroup()
            group.elementList = paragraphPart.elementList
            paragraphList.add(group)
        }
        return paragraphList
    }

    static List<OrgUnit> getSortedOrgUnits(Set<OrgUnit> set)
    {
        def ret = new ArrayList<>(set)
        Collections.sort(ret, new Comparator<OrgUnit>() {
            @Override
            int compare(OrgUnit o1, OrgUnit o2)
            {
                return o1.title.compareToIgnoreCase(o2.title)
            }
        })
        return ret
    }

    static String getLevelTypeStr(StructureEducationLevels levelType)
    {
        if (levelType.isSpecialization() || (levelType.isProfile() && (levelType.isMaster() || levelType.isBachelor())))
        {
            if (levelType.isSpecialization()) return 'Специализация'
            if (levelType.isMaster()) return 'Магистерская программа'
            return 'Бакалаврский профиль'
        }
        return levelType.isSpecialty() ? 'Специальность' : 'Направление'
    }

    static List<EducationLevelsHighSchool> getSortedHighSchools(Set<EducationLevelsHighSchool> set)
    {
        def highSchools = new ArrayList<>(set)
        Collections.sort(highSchools, new Comparator<EducationLevelsHighSchool>() {
            @Override
            int compare(EducationLevelsHighSchool o1, EducationLevelsHighSchool o2)
            {
                return o1.title.compareToIgnoreCase(o2.title)
            }
        })
        return highSchools
    }
}