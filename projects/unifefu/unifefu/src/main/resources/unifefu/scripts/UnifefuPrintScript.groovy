package unifefu.scripts

import ru.tandemservice.uni.entity.catalog.DevelopForm
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection

/**
 * Created with IntelliJ IDEA.
 * User: amakarova
 */
class UnifefuPrintScript {

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return наименование формы освоения
     */
    def static getDevelopFormTitle(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        if (requestedEnrollmentDirection == null) return null;
        DevelopForm developForm = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(developForm.getCode()))
            return "очная";
        else if (DevelopFormCodes.CORESP_FORM.equals(developForm.getCode()))
            return "заочная";
        else if (DevelopFormCodes.PART_TIME_FORM.equals(developForm.getCode()))
            return "очно-заочная";
        else
            return developForm.getTitle().toLowerCase();
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return наименование уровня образования
     */
    def static String getEducationLevelTitle(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        if (requestedEnrollmentDirection == null) return "";
        return requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle();
    }


}
