package unifefu.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.hibsupport.builder.MQBuilder
import org.tandemframework.hibsupport.builder.expression.MQExpression
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonBenefit
import org.tandemframework.shared.person.base.entity.PersonEduInstitution
import org.tandemframework.shared.person.catalog.entity.codes.EducationLevelStageCodes
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes
import ru.tandemservice.uniec.UniecDefines
import ru.tandemservice.uniec.entity.catalog.StateExamSubject
import ru.tandemservice.uniec.entity.entrant.*
import ru.tandemservice.uniec.entity.settings.ConversionScale
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation
import ru.tandemservice.uniec.util.EntrantDataUtil

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EntrantRequestPrint(                                   // стандартные входные параметры скрипта
                                                                  session: session,                                         // сессия
                                                                  template: template,                                       // шаблон
                                                                  entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати заявления абитуриента
 *
 * @author amakarova
 * @since 23.05.2013
 */
class EntrantRequestPrint
{
    Session session
    byte[] template
    EntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().id()} = ${entrantRequest.id}
                order by ${RequestedEnrollmentDirection.compensationType().code()}, ${RequestedEnrollmentDirection.priority()}
        /).<RequestedEnrollmentDirection> list()

        fillRequestedDirectionList(directions)
        fillInjectParameters(directions)
        fillIndividualProgress()

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    void fillInjectParameters(List<RequestedEnrollmentDirection> directions)
    {
        RequestedEnrollmentDirection firstPriorityDirection = directions[0];
        String dateFormat = 'dd.MM.yyyy';
        def entrant = entrantRequest.entrant
        def person = entrant.person
        def card = person.identityCard
        def sex = card.sex
        def endA = sex.male ? 'ен' : 'на'
        def endB = sex.male ? 'ий' : 'ая'
        def lastEduInstitution = person.personEduInstitution
        def codeQualification = firstPriorityDirection.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification.code
        def isMaster = QualificationsCodes.MAGISTR.equals(codeQualification);
        def isSPO = QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(codeQualification) || QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(codeQualification);
        def isBachelor = QualificationsCodes.BAKALAVR.equals(codeQualification);
        def remote = false;
        if (lastEduInstitution != null)
        {
            def eduLevelStage = lastEduInstitution.educationLevelStage
            if (eduLevelStage?.highProf
                    || EducationLevelStageCodes.BAKALAVRIAT.equals(eduLevelStage.code)
                    || EducationLevelStageCodes.PODGOTOVKA_DIPLOMIROVANNYH_SPETSIALISTOV.equals(eduLevelStage.code)
                    || EducationLevelStageCodes.MAGISTRATURA.equals(eduLevelStage.code))
                remote = false;
            if (DevelopTechCodes.DISTANCE.equals(firstPriorityDirection.enrollmentDirection.educationOrgUnit.developTech.code))
                remote = true
        }
        im.put('highSchoolTitle_G', TopOrgUnit.instance.genitiveCaseTitle)
        im.put('FIO', card.getFullFio())
        im.put('birthDate', card.birthDate?.format(dateFormat))
        im.put('sex', sex.title)
        im.put('email', person.email)
        im.put('registered', endB)
        im.put('registrationAddress', person.identityCard.address?.titleWithFlat)
        im.put('factAddress', person.address?.titleWithFlat)
        im.put('priority', isSPO ? '' : 'с учетом приоритетности')
        im.put('phones', [person.contactData.phoneFact, person.contactData.phoneMobile, person.contactData.phoneRelatives].grep().join(', '))
        //паспорт
        im.put('seria', card.seria)
        im.put('number', card.number)
        im.put('issuanceDate', card.issuanceDate?.format(dateFormat))
        im.put('issuancePlace', card.issuancePlace)
        //образование
        im.put('graduated', endB)
        im.put('education', getEduEducation(lastEduInstitution, isMaster))
        im.put('eduYearEnd', lastEduInstitution?.yearEnd as String)
        im.put('edulevel', lastEduInstitution == null ? null : lastEduInstitution.educationLevelStage.title)
        im.put('eduDocument', lastEduInstitution == null ? null : lastEduInstitution.documentType.title)
        im.put('eduDocSeria', lastEduInstitution == null ? null :
                              lastEduInstitution.seria == null ? null :
                              new RtfString().append(" Серия ").boldBegin().append(lastEduInstitution.seria).boldEnd())
        im.put('eduDocNumber', lastEduInstitution == null ? null :
                               lastEduInstitution.number == null ? null :
                               new RtfString().append(" Номер ").boldBegin().append(lastEduInstitution.number).boldEnd())

        def familiar = sex.male ? "Ознакомлен" : "Ознакомлена";
        im.put('familiar', familiar)
        im.put('agree', sex.male ? "Согласен" : "Согласна")
        im.put('specRight4BachNSpec', (isBachelor || isSPO) ? "с информацией о предоставляемых поступающим особых правах " +
                                                              "и преимуществах при приеме на обучение " +
                                                              "по программам бакалавриата и программам специалитета; " : "");
        im.put('warnRight4BachNSpec', (isBachelor || isSPO) ? " по программам бакалавриата, программам специалитета - " +
                                                              "отсутствие диплома бакалавра, диплома специалиста или диплома магистра; " : "");
        im.put('warnRight4Master', isMaster ? " по программам магистратуры - отсутствие диплома специалиста или диплома магистра, " +
                                                 "за исключением высшего профессионального образования, " +
                                                 "подтверждаемого присвоением квалификации \"дипломированный специалист\";" : "");

        im.put('master', isMaster ? 'в магистратуру' : '')
        im.put('title', isSPO ? 'Наименование специальности среднего профессионального образования' : 'Наименование направления подготовки (специальности)')
        im.put('text1', isMaster ? 'по выбранному направлению ' + familiar : 'по выбранной специальности (направлению)')
        def text3 = isSPO ? '' : 'а также правилами подачи заявления не более, чем в пять вузов и '
        im.put('text2', isMaster ? '' : ', с правилами подачи апелляции и датой предоставления оригинала документа об образовании, ' + text3 + 'с «Порядком организации приема в ДВФУ» ' + familiar)

        im.put('needHotel', entrant.person.needDormitory ? 'Нуждаюсь' : 'Не нуждаюсь')

        im.put('developForm', firstPriorityDirection.enrollmentDirection.educationOrgUnit.developForm.accCaseTitle)

        im.put('forRemote', remote ? '(с применением дистанционных образовательных технологий)' : '')

        im.put('endA', endA)
        im.put('endB', endB)
        im.put('isFromKrym', entrant.fromCrimea ? "Да" : "Нет")

        def query = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, 'd').add(MQExpression.in("d", "id", directions.collect {
            it.id
        }))
        def dataUtil = new EntrantDataUtil(session, firstPriorityDirection.entrantRequest.entrant.enrollmentCampaign, query)

        boolean hasEge = directions.any {
            dataUtil.getChosenEntranceDisciplineSet(it).any {
                dataUtil.getExamPassDisciplineSet(it).any {
                    it.subjectPassForm.code == UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL
                }
            }
        }

        boolean hasNotEge = directions.any {
            dataUtil.getChosenEntranceDisciplineSet(it).any {
                dataUtil.getExamPassDisciplineSet(it).any {
                    it.subjectPassForm.code != UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL
                }
            }
        }

        im.put("howToReturn", entrantRequest.methodDeliveryNReturnDocs == null ? "" : entrantRequest.methodDeliveryNReturnDocs.title)

        def spo2 = isSPO && hasEge ? ' (ГИА)' : ''
        im.put("resultEge", 'Прошу засчитать в качестве результатов вступительных испытаний ЕГЭ' + spo2)
        boolean hasDisc = fillStateExamSubjectBlock(directions)

        //выводим сведения, если не СПО
//        Наличие диплома победителя или призера олимпиады
//        Льготы зачисления
//        Диплом о законченном высшем образовании
        // Не СПО
        if (!isSPO)
        {
            def rowsInfo = new ArrayList<String[]>()
            String[] row = new String[2]
//            String[] rowDiploma = null
//            // не магистратура
//            if (!isMaster) {
//                row[0] = 'Наличие диплома победителя или призера олимпиады'
//                row[1] = getOlympiadTitle(entrant)
//                rowsInfo.add(row)
//                rowDiploma = new String[2]
//                rowDiploma[0] = 'Диплом о законченном высшем образовании'
//                rowDiploma[1] = isDiploma ? 'Имею' : 'Не имею'
//            }
//            row = new String[2]
            row[0] = 'Льготы зачисления'
            row[1] = getBenefits(person)
            rowsInfo.add(row)
//            if (rowDiploma != null) rowsInfo.add(rowDiploma)
            tm.put('INFO', rowsInfo as String[][])
        } else
        {
            tm.remove('INFO')
        }

        def rowsEge = new ArrayList<String[]>()
        if (!isMaster)
        {
            String[] row = new String[2]
            row[0] = 'Участвую во второй волне ЕГЭ'
            row[1] = hasEge ? 'Да' : 'Нет'
            if (!isSPO) rowsEge.add(row)
            row = new String[2]
            def noEge = hasNotEge && hasDisc ? 'Да' : 'Нет'
            row[0] = 'Участвую во вступительных испытаниях, проводимых вузом самостоятельно'
            row[1] = noEge
            rowsEge.add(row)
            tm.put('EGE', rowsEge as String[][])
        } else
            tm.remove('EGE', 1, 1)
    }

    def getBenefits(Person person)
    {
        def benetifs = DQL.createStatement(session, /
                select ${PersonBenefit.benefit().title()}
                from ${PersonBenefit.class.simpleName}
                where ${PersonBenefit.person().id()}=${person.id}
                order by ${PersonBenefit.benefit().title()}
        /).<String> list()

        return benetifs.empty ? 'Льгот нет' : benetifs.join(', ')
    }

    def static getEduEducation(PersonEduInstitution personEduInstitution, boolean isMaster)
    {
        def qualification = ''
        if (isMaster)
        {
            if (personEduInstitution?.diplomaQualification)
                qualification = 'квалификация по диплому: ' + personEduInstitution?.diplomaQualification?.title + ',';
        }
        return personEduInstitution ? [personEduInstitution.eduInstitutionKind?.shortTitle,
                                       personEduInstitution.eduInstitution?.title,
                                       personEduInstitution.addressItem.title, qualification].grep().join(', ') : null;
    }

    def getOlympiadTitle(Entrant entrant)
    {
        //получаем названия всех олимпиад абитуриента
        def olympiads = DQL.createStatement(session, /
                from ${OlympiadDiploma.class.simpleName}
                where ${OlympiadDiploma.entrant().id()} = ${entrant.id}
        /).<OlympiadDiploma> list()

        if (olympiads.size() == 0)
            return "Нет"

        def olympiadTitleList = new ArrayList()
        for (def olympiad : olympiads)
        {
            def seria = olympiad.seria ? olympiad.seria : ''
            olympiadTitleList.add([olympiad.olympiad, ['диплом', seria, '№', olympiad.number].grep().join(' ')].grep().join(', '))
        }
        return StringUtils.join(olympiadTitleList.iterator(), "; ")
    }

    void fillRequestedDirectionList(List<RequestedEnrollmentDirection> directions)
    {
        def rows = new ArrayList<String[]>()
        // для каждого вступительного испытания в нем
        for (int i = 0; i < directions.size(); i++)
        {
            def direction = directions.get(i)
            def devCond = direction.enrollmentDirection.educationOrgUnit.developCondition.title + ' освоения'
            def budget = direction.compensationType.budget
            // формируем строку из 4 столбцов
            String[] row = new String[4]
            row[0] = i + 1
            row[1] = [direction.title, devCond].grep().join(', ')
            row[2] = budget ? 'да' : ''
            row[3] = budget ? '' : 'да'
            rows.add(row)
        }
        tm.put('T1', rows as String[][])
    }

    boolean fillStateExamSubjectBlock(List<RequestedEnrollmentDirection> directions)
    {
        def disciplinesList = getDisciplineList(directions)
        if (disciplinesList.isEmpty())
        {
            tm.remove('T', 1, 1)
            im.put('resultEge', '')
            return false
        }

        // получаем сертификаты ЕГЭ абитуриента
        def certificates = DQL.createStatement(session, /
                from ${EntrantStateExamCertificate.class.simpleName}
                where ${EntrantStateExamCertificate.entrant().id()} = ${entrantRequest.entrant.id}
        /).<EntrantStateExamCertificate> list()

        // если их нет, то удалить таблицу с предметами ЕГЭ
        if (certificates.empty)
        {
            tm.remove('T', 1, 1)
            im.put('resultEge', '')
            return !disciplinesList.isEmpty()
        }

        // получаем для каждого сертификата предметы ЕГЭ с максимальным баллом по предметам
        def maxMarkMap = new HashMap<StateExamSubject, Integer>()
        for (def certificate : certificates)
        {
            def marks = DQL.createStatement(session, /
                    from ${StateExamSubjectMark.class.simpleName}
                    where ${StateExamSubjectMark.certificate().id()}=${certificate.id}
            /).<StateExamSubjectMark> list()

            for (def mark : marks)
            {
                def mark2cert = maxMarkMap.get(mark.subject)
                if (mark2cert == null || mark2cert < mark.mark)
                    maxMarkMap.put(mark.subject, mark.mark)
            }
        }

        def rows = new ArrayList<String[]>()

        // для каждого вступительного испытания в нем
        def mapSubject = new ArrayList<StateExamSubject>()
        for (int i = 0; i < disciplinesList.size(); i++)
        {
            Discipline2RealizationWayRelation discipline = disciplinesList.get(i)
            // получаем возможное соответствие предмета ЕГЭ и дисциплины вступительного испытания
            def conversionScale = DQL.createStatement(session, /
                        from ${ConversionScale.class.simpleName}
                        where ${ConversionScale.discipline().id()}=${discipline.id}
            /).<ConversionScale> uniqueResult()

            // если оно есть
            if (conversionScale != null)
            {
                // и есть сертификат, который покрывает эту дисциплину
                StateExamSubject examSubject = conversionScale.subject
                def mark2cert = maxMarkMap.get(examSubject)
                if (mark2cert != null && !mapSubject.contains(examSubject))
                {
                    mapSubject.add(examSubject)
                    // то заполняем колонки
                    String[] row = new String[3]
                    row[0] = i + 1
                    row[1] = examSubject.title
                    row[2] = mark2cert as String
                    rows.add(row)
                }
            }
        }
        if (rows.size() == 0)
        {
            tm.remove('T', 1, 1)
            im.put('resultEge', '')
            return !disciplinesList.isEmpty()
        }
        tm.put('T', rows as String[][])
        return !disciplinesList.isEmpty()
    }

    def getDisciplineList(List<RequestedEnrollmentDirection> directions)
    {
        def disciplinesList = new ArrayList<Discipline2RealizationWayRelation>()
        for (def direction : directions)
        {
            def disciplines = DQL.createStatement(session, /
                        select ${ChosenEntranceDiscipline.enrollmentCampaignDiscipline()}
                        from ${ChosenEntranceDiscipline.class.simpleName}
                        where ${ChosenEntranceDiscipline.chosenEnrollmentDirection().id()}=${direction.id}
                        order by ${ChosenEntranceDiscipline.enrollmentCampaignDiscipline().educationSubject().title()}
            /).<Discipline2RealizationWayRelation> list()

            disciplinesList.addAll(disciplines)
        }
        return disciplinesList;
    }

    def fillIndividualProgress()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantIndividualProgress.class, "enPr")
        builder.where(eq(property("enPr", EntrantIndividualProgress.entrant()), value(entrantRequest.entrant)))
        builder.where(exists(new DQLSelectBuilder().fromEntity(EnrCampaignEntrantIndividualProgress.class, "ecp")
                                                   .where(eq(
                property(EnrCampaignEntrantIndividualProgress.individualProgress().fromAlias("ecp")),
                property("enPr", EntrantIndividualProgress.individualProgressType())))
                                                   .where(eq(property(EnrCampaignEntrantIndividualProgress.enrollmentCampaign().fromAlias("ecp")), value(entrantRequest.entrant.enrollmentCampaign)))
                                                   .buildQuery()
        ))

        List<EntrantIndividualProgress> individualProgresses = builder.createStatement(session).list();
        im.put("fefuHasIndividualProgress", individualProgresses.isEmpty() ? "Нет" : "Да")

        def rtf = new RtfString().append("Список индивидуальных достижений:").par()

        for (EntrantIndividualProgress item : individualProgresses)
            rtf.append(item.individualProgressType.title).append(" - ").append(CommonBaseStringUtil.numberWithPostfixCase(item.mark.intValue(), 'балл', 'балла', 'баллов')).par()

        im.put("fefuIndividualProgressList", individualProgresses.isEmpty() ? "" : rtf);
    }
}
