/* $Id$ */
package unifefu.scripts

import org.apache.cxf.common.util.StringUtils
import org.hibernate.Session
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection
import ru.tandemservice.unifefu.entity.OnlineEnrDirectionFefuExt

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new OnlineEntrantRequestPrint(                             // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        onlineEntrant: session.get(OnlineEntrant.class, object)   // объект печати
).print()

/**
 * Алгоритм печати заявления онлайн-абитуриента
 *
 * @author Nikolay Fedorovskih
 * @since 02.07.2013
 */

class OnlineEntrantRequestPrint
{
    Session session
    byte[] template
    OnlineEntrant onlineEntrant
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        // Выбранные направления подготовки, отсортированные по приоритетам, которые проставил онлайн-абитуриент
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OnlineRequestedEnrollmentDirection.class, 'e')
                .column('e')
                .joinEntity('e', DQLJoinType.left, OnlineEnrDirectionFefuExt.class, 'f',
                        eq(property(OnlineRequestedEnrollmentDirection.id().fromAlias('e')),
                           property(OnlineEnrDirectionFefuExt.onlineEnrDirection().id().fromAlias('f'))))
                .where(eq(property(OnlineRequestedEnrollmentDirection.entrant().fromAlias('e')), value(onlineEntrant)))
                .order(property(OnlineEnrDirectionFefuExt.priority().fromAlias('f')), OrderDirection.asc)

        List<OnlineRequestedEnrollmentDirection> directions = builder.createStatement(session).<OnlineRequestedEnrollmentDirection> list()

        fillRequestedDirections(directions)
        fillInjectParameters(directions.size() > 0 ? directions[0] : null)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Заявление онлайн-абитуриента ${onlineEntrant.fullFio}.rtf"]
    }

    def fillRequestedDirections(List<OnlineRequestedEnrollmentDirection> directions)
    {
        def rows = new ArrayList<String[]>()

        // для каждого вступительного испытания в нем
        for (int i = 0; i < directions.size(); i++)
        {
            def direction = directions.get(i)
            def devCondition = direction.enrollmentDirection.educationOrgUnit.developCondition.title + ' освоения'
            def budget = direction.compensationType.budget
            // формируем строку из 4 столбцов
            String[] row = new String[4]
            row[0] = i + 1
            row[1] = [direction.enrollmentDirection.title, devCondition].grep().join(', ')
            row[2] = budget ? 'да' : ''
            row[3] = budget ? '' : 'да'
            rows.add(row)
        }
        tm.put('T1', rows as String[][])
    }

    void fillInjectParameters(OnlineRequestedEnrollmentDirection firstDirection)
    {
        String dateFormat = 'dd.MM.yyyy'
        def sex = onlineEntrant.sex
        def endA = sex.male ? 'ен' : 'на'
        def endB = sex.male ? 'ий' : 'ая'



        def remote = false;
        def isSPO = false
        def isMaster = false
        def isBachelor = false;
        def isSpec = false;
        if (firstDirection)
        {
            remote = DevelopTechCodes.DISTANCE.equals(firstDirection.enrollmentDirection.educationOrgUnit.developTech.code)

            def codeQualification = firstDirection.enrollmentDirection.educationOrgUnit.educationLevelHighSchool.educationLevel.qualification.code
            isMaster = QualificationsCodes.MAGISTR.equals(codeQualification);
            isSPO = QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(codeQualification) || QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(codeQualification);
            isBachelor = QualificationsCodes.BAKALAVR.equals(codeQualification);
            isSpec = QualificationsCodes.SPETSIALIST.equals(codeQualification);
        }

        im.put('FIO', onlineEntrant.fullFio)
        im.put('birthDate', onlineEntrant.birthDate?.format(dateFormat))
        im.put('sex', sex.title)
        im.put('email', onlineEntrant.email)
        im.put('endA', endA)
        im.put('endB', endB)
        im.put('registrationAddress', onlineEntrant.registrationAddress?.titleWithFlat)
        im.put('factAddress', onlineEntrant.isLiveAtRegistration() ? onlineEntrant.registrationAddress?.titleWithFlat : onlineEntrant.actualAddress?.titleWithFlat)
        im.put('phones', [onlineEntrant.phoneFact, onlineEntrant.phoneMobile, onlineEntrant.phoneRelatives].grep().join(', '))
        im.put('seria', onlineEntrant.passportSeria)
        im.put('number', onlineEntrant.passportNumber)
        im.put('issuanceDate', onlineEntrant.passportIssuanceDate?.format(dateFormat))
        im.put('issuancePlace', onlineEntrant.passportIssuancePlace)

        //Документ об образовании
        im.put('education', onlineEntrant.eduInstitution ? onlineEntrant.eduInstitution.titleWithAddress : onlineEntrant.eduInstitutionStr)
        im.put('eduYearEnd', onlineEntrant.eduDocumentYearEnd?.toString())
        im.put('edulevel', onlineEntrant.eduDocumentLevel?.title)
        im.put('eduDocument', onlineEntrant.eduDocumentType?.title)
        im.put('eduDocSeria', onlineEntrant.eduDocumentSeria == null ? null :
                              new RtfString().append(" Серия ").boldBegin().append(onlineEntrant.eduDocumentSeria).boldEnd())
        im.put('eduDocNumber', onlineEntrant.eduDocumentNumber == null ? null :
                               new RtfString().append(" Номер ").boldBegin().append(onlineEntrant.eduDocumentNumber).boldEnd())
        im.put("howToReturn", onlineEntrant.methodDeliveryNReturnDocs?.title)

        im.put('needHotel', onlineEntrant.needHotel ? 'Нуждаюсь' : 'Не нуждаюсь')

        im.put('developForm', firstDirection ? firstDirection.enrollmentDirection.educationOrgUnit.developForm.accCaseTitle : '')
        im.put('forRemote', remote ? '(с применением дистанционных образовательных технологий)' : '')
        im.put('isMaster', isMaster ? 'в магистратуру' : '')
        def familiar = sex.male ? 'ознакомлен' : 'ознакомлена'
        im.put('title', isSPO ? 'Наименование специальности среднего профессионального образования' : 'Наименование направления подготовки (специальности)')
        im.put('text1', isMaster ? 'по выбранному направлению ' + familiar : 'по выбранной специальности (направлению)')
        def text3 = isSPO ? '' : 'а также правилами подачи заявления не более, чем в пять вузов и '
        im.put('text2', isMaster ? '' : ', с правилами подачи апелляции и датой предоставления оригинала документа об образовании, ' + text3 + 'с «Порядком организации приема в ДВФУ» ' + familiar)

        familiar = sex.male ? "Ознакомлен" : "Ознакомлена";
        im.put('familiar', familiar)
        im.put('agree', sex.male ? "Согласен" : "Согласна")
        im.put('specRight4BachNSpec', (isBachelor || isSPO || isSpec) ? "с информацией о предоставляемых поступающим особых правах " +
                                                              "и преимуществах при приеме на обучение " +
                                                              "по программам бакалавриата и программам специалитета; " : "");
        im.put('warnRight4BachNSpec', (isBachelor || isSPO || isSpec) ? " по программам бакалавриата, программам специалитета - " +
                                                              "отсутствие диплома бакалавра, диплома специалиста или диплома магистра; " : "");
        im.put('warnRight4Master', isMaster ? " по программам магистратуры - отсутствие диплома специалиста или диплома магистра, " +
                                              "за исключением высшего профессионального образования, " +
                                              "подтверждаемого присвоением квалификации \"дипломированный специалист\";" : "");


        // Выводим сведения, если не СПО:
        // Наличие диплома победителя или призера олимпиады
        // Льготы зачисления
        // Диплом о законченном высшем образовании
        if (!isSPO)
        {
            def rowsInfo = new ArrayList<String[]>()
            String[] row = new String[2]
            //String[] rowDiploma = null

            // не магистратура
            if (!isMaster)
            {
                row[0] = 'Наличие диплома победителя или призера олимпиады'
                row[1] = getOlympiadTitle()
                rowsInfo.add(row)
                //rowDiploma = new String[2]
                //rowDiploma[0] = 'Диплом о законченном высшем образовании'
                //rowDiploma[1] = isDiploma ? 'Имею' : 'Не имею'
            }

            if (onlineEntrant.benefit)
            {
                row = new String[2]
                row[0] = 'Льгота зачисления'
                row[1] = onlineEntrant.benefit.title
                rowsInfo.add(row)
            }

            //if (rowDiploma != null) rowsInfo.add(rowDiploma)

            tm.put('INFO', rowsInfo as String[][])
        } else
        {
            tm.remove('INFO')
        }

        def rowsEge = new ArrayList<String[]>()
        if (!isMaster)
        {
            String[] row = new String[2]
//            row[0] = 'Участвую во второй волне ЕГЭ'
//            row[1] = onlineEntrant.participateInSecondWaveUSE ? 'Да' : 'Нет'
//            if (!isSPO) rowsEge.add(row)
//            row = new String[2]

            def noEge = onlineEntrant.participateInEntranceTests ? 'Да' : 'Нет'
            row[0] = 'Участвую во вступительных испытаниях, проводимых вузом самостоятельно'
            row[1] = noEge
            rowsEge.add(row)

            tm.put('EGE', rowsEge as String[][])
        } else
            tm.remove('EGE', 1, 1)
    }

    def getOlympiadTitle()
    {
        if (StringUtils.isEmpty(onlineEntrant.olympiadDiplomaTitle))
            return 'Нет'

        return [onlineEntrant.olympiadDiplomaTitle,
                ['диплом', onlineEntrant.olympiadDiplomaSeria, '№', onlineEntrant.olympiadDiplomaNumber].grep().join(' ')].grep().join(', ')
    }
}