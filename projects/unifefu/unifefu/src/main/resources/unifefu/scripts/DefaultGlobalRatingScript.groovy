package unifefu.scripts

import com.google.common.collect.Maps
import org.apache.commons.lang.StringUtils
import org.tandemframework.core.CoreDateUtils
import org.tandemframework.core.info.ErrorCollector
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.util.RtfString
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent
import ru.tandemservice.unitraining.base.entity.journal.*
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByLevel.IBrsGlobalScript
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalEventStatScriptFunctions
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.DefaultSessionPointsValidator
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue
import ru.tandemservice.unitraining.brs.entity.catalogs.IBrsCoefficientDef
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef

import static org.tandemframework.hibsupport.dql.DQLExpressions.*
import static ru.tandemservice.unifefu.utils.FefuBrs.*
import static ru.tandemservice.unitraining.brs.dao.IBrsDao.*

/**
 * Скрипт расчета рейтингов студентов по БРС для ДВФУ.
 * !!! Внимание, логика отчетов не предполагает, что данный скрипт будет переопределен на подразделении !!!
 */
class ScriptImpl implements IBrsGlobalScript, IBrsJournalEventStatScriptFunctions, IGetStudentRatingScript
{
    @Override
    boolean validateOnJournalAcceptance(ErrorCollector errors, Map<TrJournalModule, List<TrJournalEvent>> journalContent, IBrsPreparedSettings brsSettings)
    {
        def noMaxPointsTitles = new ArrayList<String>()
        def noWeightTitles = new ArrayList<String>()
        for (Map.Entry<TrJournalModule, List<TrJournalEvent>> e : journalContent.entrySet())
        {
            for (TrJournalEvent event : e.value)
            {
                if (event instanceof TrEventAction)
                {
                    if (null == brsSettings.getEventSettings(event, MAX_POINTS_CODE))
                        noMaxPointsTitles.add(String.valueOf(event.number))
                    if (null == brsSettings.getEventSettings(event, WEIGHT_EVENT_CODE))
                        noWeightTitles.add(String.valueOf(event.number))
                }
            }
        }
        if (noMaxPointsTitles.empty && noWeightTitles.empty)
            return true

        if (!noMaxPointsTitles.empty)
            errors.add("Согласование журнала невозможно: для событий с номерами " + noMaxPointsTitles.join(", ") + " не указан максимальный балл. Укажите максимальный балл для всех событий перед согласованием.")

        if (!noWeightTitles.empty)
            errors.add("Согласование журнала невозможно: для событий с номерами " + noWeightTitles.join(", ") + " не указан весовой коэф. Укажите весовой коэффициент для всех событий перед согласованием.")

        return false
    }

    @Override
    boolean validateMark(ErrorCollector errors, Student student, TrEduGroupEvent event, ITrJournalEventDao.IJournalMarkData mark, IBrsPreparedSettings brsSettings)
    {
        if (mark?.grade != null && mark.grade < 0)
        {
            errors.add("Балл, выставленный студенту " + student.fio + ", должен быть больше нуля.")
            return false
        }
        return true
    }

    @Override
    List<TrBrsCoefficientDef> filterCoefficientDefListForEvent(TrJournalEvent event, List<TrBrsCoefficientDef> defList)
    {
        return event instanceof TrEventAction ? defList : []
    }

    @Override
    List<IBrsCoefficientDef> getCoefficientDefList()
    {
        return [BrsScriptUtils.coeffDefinition(MIN_POINTS_CODE, BrsScriptUtils.OwnerType.EVENT, "Мин. балл", false, false), // для события
                BrsScriptUtils.coeffDefinition(MAX_POINTS_CODE, BrsScriptUtils.OwnerType.EVENT, "Макс. балл"),
                BrsScriptUtils.coeffDefinition(WEIGHT_EVENT_CODE, BrsScriptUtils.OwnerType.EVENT, "Весовой коэфф."),
                BrsScriptUtils.coeffDefinition(WEIGHT_JOURNAL_CODE, BrsScriptUtils.OwnerType.JOURNAL, "Вес мероприятия в сессию"),
                BrsScriptUtils.coeffDefinition(RANGE_3_CODE, BrsScriptUtils.OwnerType.JOURNAL, "Удовл. в журнале"),
                BrsScriptUtils.coeffDefinition(RANGE_4_CODE, BrsScriptUtils.OwnerType.JOURNAL, "Хор. в журнале"),
                BrsScriptUtils.coeffDefinition(RANGE_5_CODE, BrsScriptUtils.OwnerType.JOURNAL, "Отл. в журнале"),
                BrsScriptUtils.coeffDefinition(RANGE_SETOFF_CODE, BrsScriptUtils.OwnerType.JOURNAL, "зачет"),
                BrsScriptUtils.coeffDefinition(RANGE_3_DEF_CODE, BrsScriptUtils.OwnerType.TUTOR_OU, "Удовл."),
                BrsScriptUtils.coeffDefinition(RANGE_4_DEF_CODE, BrsScriptUtils.OwnerType.TUTOR_OU, "Хор."),
                BrsScriptUtils.coeffDefinition(RANGE_5_DEF_CODE, BrsScriptUtils.OwnerType.TUTOR_OU, "Отл."),
                BrsScriptUtils.coeffDefinition(RANGE_SETOFF_DEF_CODE, BrsScriptUtils.OwnerType.TUTOR_OU, "Диапазон - зачет"),
        ]
    }

    @Override
    Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions()
    {
        return [(TOTAL_COLUMN_KEY): BrsScriptUtils.additParamDef(TOTAL_COLUMN_KEY, "Итог", "Итог,%", "Итог"),
                (MARK_COLUMN_KEY) : BrsScriptUtils.additParamDef(MARK_COLUMN_KEY, "Оц.", "Оц.", "Оценка"),
                /*
                // колонки для дебага
                (CURRENT_GROUP_RATING_COLUMN_KEY): BrsScriptUtils.additParamDef(CURRENT_GROUP_RATING_COLUMN_KEY, "Р. гр.", "Р. гр.", "Рейтинг в группе"),
                (MARK_CURRENT_COLUMN_KEY)        : BrsScriptUtils.additParamDef(MARK_CURRENT_COLUMN_KEY, "Оц.тек.", "Оц.тек.", "Оценка тек."),
                (VALID_TOTAL_COLUMN_KEY)         : BrsScriptUtils.additParamDef(VALID_TOTAL_COLUMN_KEY, "Атт.", "Атт.", "Аттестован"),
                (VALID_CURRENT_COLUMN_KEY)       : BrsScriptUtils.additParamDef(VALID_CURRENT_COLUMN_KEY, "Атт.тек.", "Атт.тек.", "Аттестован тек."),
                */
        ]
    }

    @Override
    List<String> getDisplayableAdditParams()
    {
        return [TOTAL_COLUMN_KEY,
                MARK_COLUMN_KEY,
                /*
                // колонки для дебага
                CURRENT_GROUP_RATING_COLUMN_KEY,
                MARK_CURRENT_COLUMN_KEY,
                VALID_TOTAL_COLUMN_KEY,
                VALID_CURRENT_COLUMN_KEY
                */
        ]
    }

    /**
     * Получаем коэффициенты БРС для зачета или экзамена из настроек и задаем их текстовые эквиваленты.
     * По сути хардкодим для экзамена "удовл", "хор" и "отл", а для зачета - "зач"
     */
    private static Map<Double, String> prepareRangeMap(IBrsPreparedSettings brsSettings, TrJournal journal)
    {
        String gradeScaleCode;
        if (journal.getGradeScale() == null)
        {
            def fControlActionGroup = TrBrsCoefficientManager.instance().coefDao().getFcaGroup(journal)
            gradeScaleCode = EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(fControlActionGroup.code) ?
                             EppGradeScaleCodes.SCALE_5 : EppGradeScaleCodes.SCALE_2;
        } else
        {
            gradeScaleCode = journal.getGradeScale().getCode();
        }
        Map<Double, String> rangeMap = new LinkedHashMap<Double, String>(3) {
            @SuppressWarnings("GroovyUnusedDeclaration")
            public void putRange(String code, String defCode, String text)
            {
                def range = (brsSettings.getSettings(code) ?: brsSettings.getSettings(defCode))?.valueAsDouble
                if (range > 0.0d)
                    put(range, text)
            }
        };
        switch (gradeScaleCode)
        {
            case EppGradeScaleCodes.SCALE_5:
                rangeMap.putRange(RANGE_3_CODE, RANGE_3_DEF_CODE, TEXT_MARK_3);
                rangeMap.putRange(RANGE_4_CODE, RANGE_4_DEF_CODE, TEXT_MARK_4);
                rangeMap.putRange(RANGE_5_CODE, RANGE_5_DEF_CODE, TEXT_MARK_5);
                break;
            case EppGradeScaleCodes.SCALE_2:
                rangeMap.putRange(RANGE_SETOFF_CODE, RANGE_SETOFF_DEF_CODE, TEXT_MARK_SETOFF)
                break;
        }

        return rangeMap
    }

    /**
     * Вычисление текстового эквивалента для рейтинга по коэффициентам БРС
     *
     * @param rating рейтинг
     * @param rangeMap набор коэффициентов БРС и их текстовых эквивалентов
     * @return текстовый эквивалент для рейтинга ("удовл", "хор", "отл" и т.п.)
     */
    private static String ratingToTextMark(Double rating, Map<Double, String> rangeMap)
    {
        def result = ""
        def curRange = 0.0d
        rangeMap.each {
            def range = it.key
            if (rating >= range && curRange < range)
            {
                curRange = range
                result = it.value
            }
        }
        return result
    }

    /**
     * Расчет рейтинга в журнале для конкретного студента.
     * Метод вынесен отдельно для возможности использования в отчетах (чтобы лишнего не считать).
     * Если параметрв качестве rangeMap передать null, то результат будет в виде только рейтинга - StudentRatingCalcResult.
     * Иначе вернет расширенную информацию - ExtStudentRatingCalcResult.
     *
     * @param <EK> - event key (long or TrJournalEvent)
     * @param <MP> - mark param (Double or ITrJournalEventDao.IJournalMarkData)
     * @param marks оценки студента по мероприятиям
     * @param events мероприятия
     * @param sum_weight_current предрасчитанный суммарный вес мероприятий по текущую дату
     * @param sum_weight_total предрасчитанный суммарный вес всех мероприятий
     * @param rangeMap шкала перевода рейтинга в текстовый вариант оценки. Внимание!!! если передать null, рассчитанные параметры validTotal и validCurrent использовать нельзя, как и текстовые оценки! Потому что в ДВФУ они зависят от наличия текстовой оценки!
     * @return текущий и итоговый рейтинг и другие сопутствующие данные
     */
    @Override
    <EK, MP> StudentRatingCalcResult calcStudentRating(Map<EK, MP> marks, Map<EK, EventParams> events, double sum_weight_current, double sum_weight_total, Map<Double, String> rangeMap, boolean extRatingData)
    {
        def pointSum = 0.0d // полученные баллы с учетом весовых коэффициентов мероприятий
        def validTotal = true // аттестован ли студент по всему журналу (Итог). все оценки больше минимального балла
        def validCurrent = true // аттестован ли студент на текущую дату
        def wasRequiredEventsCurrent = false // были ли обязательные мероприятия на текущую дату

        for (event in events)
        {
            Double point
            def mark = marks[event.key] // оценка за мероприятие
            if (mark instanceof Double)
                point = mark as Double
            else
                point = mark ? (mark as ITrJournalEventDao.IJournalMarkData).grade : null

            def eventParams = event.value // параметры мероприятия (мин/макс балл, обязательность, и вес)

            if (rangeMap && eventParams.min) // мероприятие обязательное, если у него указан минимальный балл
            {
                if (!point || point < eventParams.min)
                {
                    // Студент не аттестован, если по обязательному мероприятию у него нет оценки либо эта оценка меньше минимальной
                    validTotal = false // не аттестован в общем
                    validCurrent &= !eventParams.current // не аттестован на текущую момент, если мероприятие уже прошло
                }
                wasRequiredEventsCurrent |= eventParams.current // на текущий момент обязательные КМ уже были
            }

            if (point)
            {
                def max = eventParams.max
                def weight = eventParams.weight
                if (max > 0.0d && weight > 0.0d)
                {
                    // Если балл больше максимального, то принимаем его равным максимальному
                    pointSum += point < max ? (weight * point / max) : weight
                }
            }
        }

        // Рейтинг округляется в большую сторону - в пользу студента
        // Если все события в будущем, то в текущем контроле рейтинг будет равен нулю. Поэтому делить на 0 не будем, будем делить на 1 - тогда досрочно
        // сдавший экзамены может иметь рейтинг больше 100%
        double ratingCurrent = Math.ceil(pointSum * (sum_weight_current > 0.0d ? (100.0d / sum_weight_current) : 100.0d))
        // Итоговый рейтинг будет равен нулю, если сумма весовых коэффициентов равна нулю
        double ratingTotal = sum_weight_total > 0.0d ? Math.min(Math.ceil(pointSum * 100.0d / sum_weight_total), 100.0d) : 0.0d

        if (!extRatingData)
            return new StudentRatingCalcResult(ratingCurrent, ratingTotal)

        def textMarkCurrent = ratingToTextMark(ratingCurrent, rangeMap)
        def textMarkTotal = validTotal ? ratingToTextMark(ratingTotal, rangeMap) : ""

        // В ДВФУ аттестованность студента по итоговому рейтингу зависит от наличия текстового эквивалента оценки (рейтинга)
        validTotal &= StringUtils.isNotEmpty(textMarkTotal)
        // С текущим рейтингом ещё сложнее.
        // Если на текущий момент не было обязательных КМ, то аттестован или нет студент на текущий момент проверяем по текущему рейтингу.
        // Если рейтинг дотягивает до минимальной оценки (удовлетворительно для экзамена или зачтено для зачета), то студент аттестован.
        // Наличие текстового эквивалента, собственно, говорит о том, что баллов хватило.
        validCurrent = (validCurrent && !wasRequiredEventsCurrent) ? StringUtils.isNotEmpty(textMarkCurrent) : validCurrent

        return new ExtStudentRatingCalcResult(ratingCurrent, ratingTotal, validCurrent, validTotal, textMarkCurrent, textMarkTotal)
    }

    @Override
    ICurrentRatingCalc calculateCurrentRating(TrJournal journal, Map<TrJournalModule, List<TrJournalEvent>> journalContent,
                                              Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap,
                                              IBrsPreparedSettings preparedBrsSettings)
    {
        final rangeMap = prepareRangeMap(preparedBrsSettings, journal)

        // Запрос получает идентификаторы текущих событий (дата начала (в расписании) которых сегодня или уже прошла)
        //noinspection UnnecessaryQualifiedReference
        final currentEventIds = new HashSet<>(DataAccessServices.dao().<Long> getList(
                new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, 'e')
                        .joinPath(DQLJoinType.inner, TrEduGroupEvent.journalEvent().fromAlias('e'), 'je')
                        .column('je.id')
                        .where(instanceOf('je', TrEventAction.class))
                        .where(DQLExpressions.in(property('e', TrEduGroupEvent.journalEvent().journalModule()), journalContent.keySet()))
                        .where(lt(property('e', TrEduGroupEvent.scheduleEvent().durationBegin()), valueTimestamp(CoreDateUtils.getNextDayFirstTimeMoment(new Date(), 1))))
        ))

        final eventsMap = Maps.<TrJournalEvent, EventParams>newHashMapWithExpectedSize(journalContent.values().size())
//        Map<TrJournalEvent, EppGradeScale> scaleByEventMap = new HashMap<>();
        def sum_weight_current = 0.0d
        def sum_weight_total = 0.0d
        for (eventList in journalContent.values())
        {
            eventList.each {
                // В ДВФУ учитываются только события текущего контроля. Аудиторная нагрузка и доп. мероприятия - никак не учитываются в расчете рейтинга.
                if (it instanceof TrEventAction)
                {
                    Double min = preparedBrsSettings.getEventSettings(it, MIN_POINTS_CODE)?.valueAsDouble
                    Double max = preparedBrsSettings.getEventSettings(it, MAX_POINTS_CODE)?.valueAsDouble
                    Double weight = preparedBrsSettings.getEventSettings(it, WEIGHT_EVENT_CODE)?.valueAsDouble
                    boolean current = it.id in currentEventIds
                    if (weight)
                    {
                        sum_weight_total += weight
                        if (current) sum_weight_current += weight
                    }
                    eventsMap.put(it, new EventParams(min, max, weight, current))
//                    scaleByEventMap.put(it, scaleByControlActionMap.get(it.actionType.id));
                }
            }
        }

        final ratingMap = new HashMap<EppStudentWorkPlanElement, Double>()
        final additionalDataMap = new HashMap<Long, Map<String, ISessionBrsDao.IRatingValue>>()
        int allValidStudentsCurrentCount = 0 // количество аттестованных студентов на текущий момент

        for (student in markMap)
        {
            final ret = (ExtStudentRatingCalcResult) calcStudentRating(student.value, eventsMap, sum_weight_current, sum_weight_total, rangeMap, true)

            // Считаем общее количество аттестованных по текущему рейтингу
            if (ret.validCurrent) allValidStudentsCurrentCount++

            ratingMap.put(student.key, ret.ratingCurrent)

            def studentAdditParamMap = SafeMap.safeGet(additionalDataMap, student.key.id, HashMap.class)
            studentAdditParamMap.put(TOTAL_COLUMN_KEY, BrsScriptUtils.ratingValue(ret.ratingTotal, null)) // Итоговый рейтинг
            studentAdditParamMap.put(MARK_COLUMN_KEY, BrsScriptUtils.ratingValue(null, ret.textMarkTotal))   // Текстовая итоговая оценка

            // Аттестован по всем КМ
            studentAdditParamMap.put(VALID_TOTAL_COLUMN_KEY, ret.validTotal ? TRUE_ROW_VALUE : FALSE_ROW_VALUE)
            // Аттестован на текущую дату
            studentAdditParamMap.put(VALID_CURRENT_COLUMN_KEY, ret.validCurrent ? TRUE_ROW_VALUE : FALSE_ROW_VALUE)
            // Текстовая оценка на текущую дату
            studentAdditParamMap.put(MARK_CURRENT_COLUMN_KEY, BrsScriptUtils.ratingValue(null, ret.textMarkCurrent))
        }

        // Сортируем рейтинги, чтобы можно было вычислить текущий рейтинг студента в рамках группы (2 из 10, к примеру).
        // Массив с текущими рейтингами - это именно ArrayList, а не какой-нибудь Set, т.к. если два студента будут иметь одинаковый текущий рейтинг,
        // то рейтинг (позиция) в группе у них должна быть одинакова, а студент с меньшим рейтингом, будет на 2 позиции ниже. Пример:
        // Иванов 80  - 1 из 4
        // Петров 70  - 2 из 4
        // Сидоров 70 - 2 из 4
        // Ефремов 60 - 4 из 4
        def groupCurrentRatings = new ArrayList<>(ratingMap.values())
                .sort() // сортируем
                .reverse(true) // делаем по убыванию

        // Записываем в доп. параметры для всех студентов их текущий рейтинг в рамках группы
        int count = markMap.size()
        for (def slot in markMap.keySet())
        {
            int pos = groupCurrentRatings.indexOf(ratingMap[slot]) + 1
            additionalDataMap[slot.id].put(CURRENT_GROUP_RATING_COLUMN_KEY, BrsScriptUtils.ratingValue(pos, pos + ' из ' + count))
        }

        return new IExtCurrentRatingCalc() {
            @Override
            TrJournal getJournal()
            { journal }

            @Override
            public boolean isModuleRatingCalculated()
            { false }

            @Override
            public IStudentCurrentRatingData getCurrentRating(EppStudentWorkPlanElement student)
            {
                return new IStudentCurrentRatingData() {
                    @Override
                    public ISessionBrsDao.IRatingValue getModuleRating(EppRegistryModule module)
                    { null }

                    @Override
                    public ISessionBrsDao.IRatingValue getRatingValue()
                    { BrsScriptUtils.ratingValue(ratingMap[student], null) }

                    @Override
                    public boolean isAllowed()
                    { true }

                    @Override
                    public ISessionBrsDao.IRatingValue getRatingAdditParam(String key)
                    { additionalDataMap[student.id]?.get(key) }
                }
            }

            @Override
            String getDisplayableRatingFormula()
            {
                "<div>Расчет рейтинга:</div>" +
                        "<div>Рейтинг = (Сумма по всем КМ (отношение оценки выставленной преподавателем к максимальному баллу по КМ и умноженное на вес КМ))/(Сумма всех весовых коэффициентов).</div>" +
                        "<div>Текущий рейтинг - сумма всех весовых коэффициентов зависит от текущей даты.</div>" +
                        "<div>Итоговый рейтинг - сумма всех весовых коэффициентов постоянна.</div>"
            }

            int getValidCurrentCount()
            { allValidStudentsCurrentCount }
        }
    }

    @Override
    IStudentAttestationData calculateAttestation(SessionAttestationSlot slot, IStudentCurrentRatingData ratingCalc, IBrsPreparedSettings preparedBrsSettings)
    {
        return new IStudentAttestationData() {
            @Override
            Boolean passed()
            {
                def rating = ratingCalc.ratingValue
                return rating.value != null && rating.value > 29
            }

            @Override
            SessionAttestationSlotAdditionalData getAdditionalData()
            { null }
        }
    }

    @Override
    RtfString getPrintableRatingFormula(EppGradeScale gradeScale, EppFControlActionGroup caGroup)
    {
        if (gradeScale == null || !(gradeScale.code.equals(EppGradeScaleCodes.SCALE_2) || gradeScale.code.equals(EppGradeScaleCodes.SCALE_5)))
            return new RtfString().append("Для используемой в мероприятии шкалы оценок не реализован расчет оценки по БРС.")
        def info = new RtfString()
                .append("Рейтинг студента для суммарной оценки в сессии вычисляется по формуле:").par()
                .append("Рейтинг студента за экзамен: [тек. рейтинг студента]+[вес мероприятия в сессии]*[балл за сдачу мероприятия в сессии]").par()
                .append("Рейтинг студента за зачет: [тек. рейтинг студента]+[вес мероприятия в сессии]*[балл за сдачу мероприятия в сессии]").par()
        return info
    }

    @Override
    ISessionBrsDao.ISessionRatingSettings getRatingSettings(final ISessionBrsDao.ISessionRatingSettingsKey key, IBrsPreparedSettings brsSettings, boolean useRating)
    {
        def regElType = key.discipline.registryElement.class

        return new ISessionBrsDao.ISessionRatingSettings() {
            @Override
            ISessionBrsDao.ISessionRatingSettingsKey key()
            { return key }

            @Override
            public boolean usePoints()
            {
                return useRating && ((EppRegistryDiscipline.isAssignableFrom(regElType)) || EppRegistryPractice.isAssignableFrom(regElType) || EppRegistryAttestation.isAssignableFrom(regElType))
            }

            @Override
            public boolean useCurrentRating()
            {
                return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType)) && !EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_PROJECT.equals(key.caGroup.code)
            }

            @Override
            public ISessionBrsDao.ISessionPointsValidator pointsValidator()
            { new DefaultSessionPointsValidator(0, 100, 0) }

            @Override
            public ISessionBrsDao.IBrsCoefficientValue getCoefficientValue(String defCode)
            { brsSettings.getSettings(defCode) }
        }
    }

    @Override
    IStudentSessionMarkData calculateSessionMark(ISessionBrsDao.IStudentCurrentRatingData ratingData, Double pointsForSessionCA, ISessionBrsDao.ISessionRatingSettings ratingSettings)
    {
        def range3 = ratingSettings.getCoefficientValue(RANGE_3_CODE)
        if (range3 == null)
            range3 = ratingSettings.getCoefficientValue(RANGE_3_DEF_CODE)
        def range4 = ratingSettings.getCoefficientValue(RANGE_4_CODE)
        if (range4 == null)
            range4 = ratingSettings.getCoefficientValue(RANGE_4_DEF_CODE)
        def range5 = ratingSettings.getCoefficientValue(RANGE_5_CODE)
        if (range5 == null)
            range5 = ratingSettings.getCoefficientValue(RANGE_5_DEF_CODE)
        def rangeSetOff = ratingSettings.getCoefficientValue(RANGE_SETOFF_CODE)
        if (rangeSetOff == null)
            rangeSetOff = ratingSettings.getCoefficientValue(RANGE_SETOFF_DEF_CODE)

        def gradeScale = ratingSettings.key().scale

        def currentRatingValue = ratingData?.ratingValue
        def currentRating = ratingData?.ratingValue?.value

        def rating = null
        def message = ratingSettings.useCurrentRating() ? currentRatingValue?.message : null

        if (ratingSettings.useCurrentRating() && currentRating != null && pointsForSessionCA != null)
        {
            rating = Math.round(0.6 * currentRating + 0.4 * pointsForSessionCA)
        }
        else if (!ratingSettings.useCurrentRating())
            rating = pointsForSessionCA

        return new IStudentSessionMarkData() {
            @Override
            public SessionMarkGradeValueCatalogItem getSessionMark()
            {
                if (null == rating)
                    return null
                Long long5
                Long long4
                Long long3
                Long longOff
                if (rangeSetOff instanceof TrBrsCoefficientValue)
                    longOff = rangeSetOff.valueAsLong
                else
                    longOff = rangeSetOff as Long
                if (range5 instanceof TrBrsCoefficientValue)
                    long5 = range5.valueAsLong
                else
                    long5 = range5 as Long
                if (range4 instanceof TrBrsCoefficientValue)
                    long4 = range4.valueAsLong
                else
                    long4 = range4 as Long
                if (range3 instanceof TrBrsCoefficientValue)
                    long3 = range3.valueAsLong
                else
                    long3 = range3 as Long

                switch (gradeScale.code)
                {
                    case EppGradeScaleCodes.SCALE_2:
                        return BrsScriptUtils.getSessionMark(rating >= (longOff / 100) ? SessionMarkGradeValueCatalogItemCodes.ZACHTENO : SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO)
                    case EppGradeScaleCodes.SCALE_5:
                        if (rating >= (long5 / 100))
                            return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.OTLICHNO)
                        if (rating >= (long4 / 100))
                            return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.HOROSHO)
                        if (rating >= (long3 / 100))
                            return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO)

                        return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO)
                }
                return null
            }

            @Override
            public ISessionBrsDao.IRatingValue getRatingValue()
            { BrsScriptUtils.ratingValue(rating, message) }

            @Override
            public ISessionBrsDao.IRatingValue getCurrentRatingValue()
            { BrsScriptUtils.ratingValue(currentRating, message) }

            @Override
            ISessionBrsDao.IRatingValue getRatingAdditParam(String key)
            { ratingData?.getRatingAdditParam(key) }

            @Override
            public Boolean isAllowed()
            { return rating == null ? null : rating > 49 }

            @Override
            public Double getRatingMissingForMark(SessionMarkGradeValueCatalogItem mark)
            {
                if (!gradeScale.equals(mark.scale))
                    return null
                if (null == rating)
                    return null
                if (SessionMarkGradeValueCatalogItemCodes.ZACHTENO.equals(mark.code))
                    return Math.max(0, Math.ceil((50 - rating) / 0.2))
                if (SessionMarkGradeValueCatalogItemCodes.OTLICHNO.equals(mark.code))
                    return Math.max(0, Math.ceil((85 - rating) / 0.4))
                if (SessionMarkGradeValueCatalogItemCodes.HOROSHO.equals(mark.code))
                    return Math.max(0, Math.ceil((70 - rating) / 0.4))
                if (SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO.equals(mark.code))
                    return Math.max(0, Math.ceil((50 - rating) / 0.4))
                return null
            }
        }
    }

    private static final ISessionBrsDao.IRatingAdditParamDef ADDIT_PARAM_VALID = BrsScriptUtils.additParamDef(VALID_ADDIT_PARAM, "Аттестовано", "атт.", "Число аттестованных студентов.")
    private static final ISessionBrsDao.IRatingAdditParamDef ADDIT_PARAM_MEDIAN = BrsScriptUtils.additParamDef(MEDIAN_ADDIT_PARAM, "Медиана", "мед.", "Медианный балл.")
    private static final ISessionBrsDao.IRatingAdditParamDef ADDIT_PARAM_MEDIUM = BrsScriptUtils.additParamDef(MEDIUM_ADDIT_PARAM, "Среднее", "ср.", "Средний балл.")

    private static final List<ISessionBrsDao.IRatingAdditParamDef> statParams = [ADDIT_PARAM_VALID, ADDIT_PARAM_MEDIAN, ADDIT_PARAM_MEDIUM]

    public static Double calcMedian(List<Double> items)
    {
        if (!items.empty)
        {
            items.sort()
            int m = items.size() / 2 // middle
            return (items.size() % 2 == 1) ? items[m] : ((items[m] + items[m - 1]) * 0.5d)
        }
        return null
    }

    public static Double calcAvg(List<Double> items)
    {
        return !items.empty ? items.sum() / items.size() : null
    }

    @Override
    IJournalEventStatCalc calculateEventStat(TrJournal journal, TrJournalGroup group, Map<TrJournalModule, List<TrJournalEvent>> journalContent, Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap, IBrsPreparedSettings brsSettings)
    {
        def values = new HashMap<TrJournalEvent, Map<String, Double>>()
        for (events in journalContent.values())
        {
            for (event in events)
            {
                def checkMinValue = brsSettings.getEventSettings(event, MIN_POINTS_CODE)?.valueAsDouble ?: 0d
                List<Double> marks = []
                int count = 0
                for (studentMarkMap in markMap.values())
                {
                    ITrJournalEventDao.IJournalMarkData mark = studentMarkMap[event]
                    if (mark?.grade >= checkMinValue)
                    {
                        count++
                        marks.add(mark.grade)
                    }
                }

                values.put(event, [(ADDIT_PARAM_VALID.key) : count == 0 ? null : count as double,
                                   (ADDIT_PARAM_MEDIAN.key): calcMedian(marks),
                                   (ADDIT_PARAM_MEDIUM.key): calcAvg(marks)])
            }
        }

        // Статистика по итоговым колонкам
        def ratingCalc = calculateCurrentRating(journal, journalContent, markMap, brsSettings) as IExtCurrentRatingCalc
        List<Double> totalList = []
        List<Double> ratingList = []
        int validTotal = 0
        markMap.each {
            IStudentCurrentRatingData rating = ratingCalc.getCurrentRating(it.key)
            totalList.add(rating.getRatingAdditParam(TOTAL_COLUMN_KEY)?.value)
            ratingList.add(rating.ratingValue?.value)

            //Аттестовано (Итог) - число студентов у которых есть текстовая оценка (ну или удовл. правилу ее вывода)
            if (StringUtils.isNotEmpty(rating.getRatingAdditParam(MARK_COLUMN_KEY)?.message))
            {
                validTotal++
            }
        }
        Map<String, Map<String, Double>> aggregatedMap = [
                (TOTAL_COLUMN_KEY)         : [(ADDIT_PARAM_VALID.key): validTotal.doubleValue(), (ADDIT_PARAM_MEDIAN.key): calcMedian(totalList), (ADDIT_PARAM_MEDIUM.key): calcAvg(totalList)],
                (RATING_COLUMN_KEY): [(ADDIT_PARAM_VALID.key): ratingCalc.validCurrentCount.doubleValue(), (ADDIT_PARAM_MEDIAN.key): calcMedian(ratingList), (ADDIT_PARAM_MEDIUM.key): calcAvg(ratingList)]
        ]

        return new IJournalEventStatCalc() {
            @Override
            public TrJournal getJournal()
            { journal }

            @Override
            public TrJournalGroup getJournalGroup()
            { journalGroup }

            @Override
            public Set<EppStudentWorkPlanElement> getIncludedStudents()
            { markMap.keySet() }

            @Override
            public List<ISessionBrsDao.IRatingAdditParamDef> getStatParamDefinitions()
            { statParams }

            @Override
            public ISessionBrsDao.IRatingValue getStatValue(TrJournalEvent event, String statKey)
            { return BrsScriptUtils.ratingValue(values[event]?.get(statKey), null) }

            @Override
            public IAggregatedRatingValue getStatValueAggregated(String statKey, String columnKey)
            {
                def value = aggregatedMap[columnKey]?.get(statKey)
                return new IAggregatedRatingValue() {
                    @Override
                    boolean isApplicable()
                    { value != null }

                    @Override
                    String getMessage()
                    { null }

                    @Override
                    Double getValue()
                    { value }
                }
            }
        }
    }
}

return new ScriptImpl();