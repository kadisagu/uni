/* $Id$ */
package unifefu.scripts

import org.hibernate.Session
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder

return new EnrollmentOrderPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        order: session.get(EnrollmentOrder.class, object) // объект печати
).print()

/**
 * Алгоритм печати приказов о зачислении абитуриентов
 *
 * @author Nikolay Fedorovskih
 * @since 29.07.2013
 */

public class EnrollmentOrderPrint
{
    Session session
    byte[] template
    EnrollmentOrder order

    def print()
    {
        // заполнение меток
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()
        im.put('commitDate', order.commitDate?.format('dd.MM.yyyy'))
        im.put('orderNumber', order.number)
        im.put('orderType', 'О зачислении на 1 курс' + (FefuEnrollmentOrderPrintBase.isSPO(order) ? ' на программы СПО' : '') + (FefuEnrollmentOrderPrintBase.isMasterOrderType(order) ? ' в магистратуру' : ''))
        im.put('orderBasicText', order.orderBasicText)
        im.put('orderText', order.orderText)
        im.put('textParagraph', order.textParagraph)
        im.put('executor', order.executor)
        im.put('orderReason', order.reason?.title)
        im.put('orderBasic', order.basic?.title)
        im.put('orderIdWithLabel', '')

        // заполнение виз
        tm.put('VISAS', [['Ректор', 'С.В. Иванец']] as String[][])

        // заполнение параграфов
        RtfDocument doc = new RtfReader().read(template)
        FefuEnrollmentOrderPrintBase.fillParagraphs(doc, FefuEnrollmentOrderPrintBase.loadPreStudents(order, session, null), order, false)
        im.modify(doc)
        tm.modify(doc)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(doc), fileName: 'EnrollmentOrder.rtf']
    }
}