/* $Id$ */
package unifefu.scripts

import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract

import java.text.SimpleDateFormat

return new EnrollmentExtractPrint(
        session: session,
        template: templateDocument,
        extract: session.get(EnrollmentExtract.class, object)
).print()

/**
 * @author Nikolay Fedorovskih
 * @since 27.11.2013
 */
class EnrollmentExtractPrint
{
    Session session
    RtfDocument template
    EnrollmentExtract extract

    def print()
    {
        def order = extract.order
        def academy = TopOrgUnit.instance
        def document = template
        def im = new RtfInjectModifier()

        im.put('highSchoolTitle_G', academy.genitiveCaseTitle ?: academy.printTitle)
        im.put('highSchoolShortTitle', academy.shortTitle)
        im.put('orderDate', order.commitDate != null ? (new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(order.commitDate) + ' г.') : null)
        im.put('orderNumber', order.number)

        FefuEnrollmentOrderPrintBase.fillParagraphs(document, FefuEnrollmentOrderPrintBase.loadPreStudents(order, session, extract.id), order, true)

        im.modify(document)

        return [document: document, fileName: 'EnrollmentExtract.rtf']
    }
}