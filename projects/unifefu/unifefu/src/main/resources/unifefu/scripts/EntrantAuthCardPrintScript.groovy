package unifefu.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uniec.UniecDefines
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline
import ru.tandemservice.uniec.entity.entrant.EntrantRequest
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation

return new EntrantAuthCardPrint(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати листка абитуриента
 *
 * @author amakarova
 * @since 14.05.2013
 */
class EntrantAuthCardPrint extends UnifefuPrintScript
{
  Session session
  byte[] template
  EntrantRequest entrantRequest
  def im = new RtfInjectModifier()
  def tm = new RtfTableModifier()

  def print()
  {
      // получаем список выбранных направлений приема для заявления
      def requestedDirections = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().id()} = ${entrantRequest.id}
                order by ${RequestedEnrollmentDirection.entrantRequest().regDate()}, ${RequestedEnrollmentDirection.priority()}
      /).<RequestedEnrollmentDirection> list()

      RequestedEnrollmentDirection requestedEnrollmentDirection = requestedDirections.isEmpty() ? null : requestedDirections.get(0);

      def entrant = entrantRequest.entrant
      def person = entrant.person
      def card = person.identityCard

      im.put('entrantNumber', entrant.personalNumber)
      im.put('lastName', card.lastName)
      im.put('firstName', card.firstName)
      im.put('middleName', card.middleName)
      im.put('seria', card.seria)
      im.put('number', card.number)
      im.put("eduTitle", getEducationLevelTitle(requestedEnrollmentDirection))
      im.put("orgUnitTitle", getFormativeOrgUnitTitle(requestedEnrollmentDirection))
      im.put("eduLevelTitle", getEducationLevelTitle(requestedEnrollmentDirection))

      fillEnrollmentResults();

    // стандартные выходные параметры скрипта
    return [document: RtfUtil.toByteArray(template, im, tm),
            fileName: "Листок абитуриента ${entrantRequest.entrant.person.identityCard.lastName} ${entrantRequest.entrant.person.identityCard.firstName}.rtf"]
  }

  def fillEnrollmentResults()
    {
        // список строк
        final List<String[]> rows = new ArrayList<String[]>();
        // получаем список выбранных направлений приема
        def requestedEnrollmentDirections = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().entrant().id()} = ${entrantRequest.entrant.id}
                order by ${RequestedEnrollmentDirection.entrantRequest().regDate()}, ${RequestedEnrollmentDirection.priority()}
        /).<RequestedEnrollmentDirection> list()

        def disciplines = new ArrayList<Discipline2RealizationWayRelation>();

        for (def direction: requestedEnrollmentDirections)
        {
            def choosenDisciplines = DQL.createStatement(session, /
                    from ${ChosenEntranceDiscipline.class.simpleName}
                    where ${ChosenEntranceDiscipline.chosenEnrollmentDirection().id()} = ${direction.id}
                    order by ${ChosenEntranceDiscipline.id()}
            /).<ChosenEntranceDiscipline> list();
            if (!choosenDisciplines.empty)
            {
                for (def item: choosenDisciplines) {
                    def discipline =  item.enrollmentCampaignDiscipline
                    if (!disciplines.contains(discipline)) {
                        disciplines.add(discipline);
                    }
                }
            }
        }

        for (int i = 0; i < disciplines.size(); i++) {
            def discipline = disciplines.get(i)
            def entrant = entrantRequest.entrant;
            def formingForEntrant = entrant.enrollmentCampaign.entrantExamListsFormingFeature.formingForEntrant;
            def examListTargetId = formingForEntrant ? entrant.id : entrantRequest.id
            def examPassDisciplines = DQL.createStatement(session, /
                from ${ExamPassDiscipline.class.simpleName}
                where ${ExamPassDiscipline.entrantExamList().entrant().id()} = ${examListTargetId}
                    and ${ExamPassDiscipline.subjectPassForm().code()} != ${UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL}
                    and ${ExamPassDiscipline.enrollmentCampaignDiscipline().id()} = ${discipline.id}
                /).<ExamPassDiscipline> list();
            if (examPassDisciplines != null && examPassDisciplines.size() > 0) {
                // вычисляем даты вступительных испытаний
                def passDatesList = new ArrayList<String>()
                for (ExamPassDiscipline disc : examPassDisciplines) {
                    def date = disc.passDate ? disc.passDate.format('dd.MM.yyyy') : ''
                    passDatesList.add(date)
                }
                // сформировать строку
                String[] row = new String[2];
                row[0] = discipline.title;
                row[1] = StringUtils.join(passDatesList, ', ');
                rows.add(row);
            }
        }
        tm.put('T', rows as String[][]);
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return формирующее подразделение
     */
    def static String getFormativeOrgUnitTitle(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        if (requestedEnrollmentDirection == null) return "";
        return requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit().getTitle();
    }
}
