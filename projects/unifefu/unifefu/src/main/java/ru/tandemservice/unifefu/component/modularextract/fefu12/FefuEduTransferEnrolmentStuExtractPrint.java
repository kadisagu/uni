/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu12;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unifefu.component.modularextract.fefu10.FefuEduEnrolmentToSecondAndNextCourseStuExtractPrint;
import ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 13.05.2013
 */
public class FefuEduTransferEnrolmentStuExtractPrint implements IPrintFormCreator<FefuEduTransferEnrolmentStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuEduTransferEnrolmentStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("fromSchool", extract.getFromSchool());
        modifier.put("liquidateEduPlanDifference",
                     FefuEduEnrolmentToSecondAndNextCourseStuExtractPrint.getLiquidationPart(extract.isLiquidateEduPlanDifference(), extract.getLiquidationDeadlineDate(), extract));
        CommonExtractPrint.initFefuGroup(modifier, "intoGroupNew", extract.getGroupNew(), extract.getEducationOrgUnitNew().getDevelopForm(), " в группу ");
        modifier.put("developTech", UniDefines.DEVELOP_TECH_REMOTE.equals(extract.getEducationOrgUnitNew().getDevelopTech().getCode())? CommonExtractPrint.WITH_REMOTE_EDU_TECH:"");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}