/*$Id$*/
package ru.tandemservice.unifefu.component.orgunit.SessionBulletinListTab;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab.Model;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 17.09.2015
 */
public class DAO extends ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab.DAO implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        ru.tandemservice.unifefu.component.orgunit.SessionBulletinListTab.Model myModel = (ru.tandemservice.unifefu.component.orgunit.SessionBulletinListTab.Model) model;
        myModel.setEducationLevelsModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                String alias = "b";
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, alias)
                        .column(property(alias))
                        .where(likeUpper(property(EducationLevelsHighSchool.displayableTitle().fromAlias(alias)), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(EducationLevelsHighSchool.displayableTitle().fromAlias(alias)));

                if (set != null)
                    builder.where(in(property(EducationLevelsHighSchool.id().fromAlias(alias)), set));

                return new DQLListResultBuilder(builder, 50);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((EducationLevelsHighSchool) value).getFullTitle();
            }
        });
    }

    @Override
    protected void applyFilters(Model model, DQLSelectBuilder dql, String alias)
    {
        super.applyFilters(model, dql, alias);
        ru.tandemservice.unifefu.component.orgunit.SessionBulletinListTab.Model myModel = (ru.tandemservice.unifefu.component.orgunit.SessionBulletinListTab.Model) model;
        List<EducationLevelsHighSchool> levelsHighSchoolList = myModel.getEducationLevelsHighSchoolList();
        if (levelsHighSchoolList != null && !levelsHighSchoolList.isEmpty())
        {

            final String reg = "reg";
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, reg)
                    .column(property(reg, EppRealEduGroupRow.group().id()), "groupId")
                    .joinPath(DQLJoinType.inner, EppRealEduGroupRow.studentEducationOrgUnit().fromAlias(reg), "eou")
                    .distinct();


            boolean considerChild = myModel.getConsiderChild();
            if (considerChild)
            {
                Collection<EducationLevels> parentLevels = levelsHighSchoolList.stream().map(EducationLevelsHighSchool::getEducationLevel).collect(Collectors.toList());

                builder.where(or(
                        in(property(EducationOrgUnit.educationLevelHighSchool().fromAlias("eou")), levelsHighSchoolList),
                        in(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().parentLevel().fromAlias("eou")), parentLevels)
                ));
            } else
            {
                builder.where(in(property(EducationOrgUnit.educationLevelHighSchool().fromAlias("eou")), levelsHighSchoolList));
            }

            dql.joinPath(DQLJoinType.inner, SessionBulletinDocument.group().fromAlias(alias), reg);
            dql.where(in(property(reg, EppRealEduGroup4ActionType.id()), builder.buildQuery()));
        }
    }
}
