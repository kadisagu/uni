/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard;

/**
 * @author Nikolay Fedorovskih
 * @since 05.02.2014
 */
public class BBConstants
{
    public static final String CONTEXT_WS = "Context.WS";
    public static final String COURSE_WS = "Course.WS";
    public static final String USER_WS = "User.WS";
    public static final String COURSE_MEMBERSHIP_WS = "CourseMembership.WS";
    public static final String GRADEBOOOK_WS = "Gradebook.WS";

    public static final String BB_COULD_NOT_CONNECT = "Не удалось подключиться к Blackboard";
}