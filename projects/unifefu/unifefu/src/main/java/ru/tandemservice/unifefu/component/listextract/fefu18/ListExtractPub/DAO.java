/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public class DAO extends AbstractListExtractPubDAO<FefuOrderContingentStuDPOListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setDpoProgram((FefuAdditionalProfessionalEducationProgram)getProperty(FefuAdditionalProfessionalEducationProgramForStudent.class, FefuAdditionalProfessionalEducationProgramForStudent.L_PROGRAM,
                                        FefuAdditionalProfessionalEducationProgramForStudent.L_STUDENT, model.getExtract().getEntity()));
    }
}