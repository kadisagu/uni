/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.employeebase.catalog.entity.codes.PostTypeCodes;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.dao.daemon.IFefuNsiDaemonDao;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuNsiCatalogTypeCodes;
import ru.tandemservice.unifefu.entity.ws.FefuEmployeePostExt;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.gen.FefuEmployeePostExtGen;
import ru.tandemservice.unifefu.ws.nsi.NsiDatagramUtil;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.datagram.*;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.FefuNsiIdWrapper;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.INsiEntityUtil;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.NsiEntityUtilFactory;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Dmitry Seleznev
 * @since 29.10.2013
 */
public class EmployeeTypeReactor implements INsiEntityReactor<EmployeeType>
{
    private INsiEntityUtil UTIL;

    public INsiEntityUtil getUtil()
    {
        if (null == UTIL) UTIL = NsiEntityUtilFactory.getUtil(EmployeePost.class);
        return UTIL;
    }

    @Override
    public List<Object> retrieve(List<INsiEntity> itemsToRetrieve)
    {
        return null;
    }

    @Override
    public EmployeeType getCatalogElementRetrieveRequestObject(String guid)
    {
        EmployeeType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createEmployeeType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    private NsiCatalogsStore prepareCatalogStore()
    {
        NsiCatalogsStore store = new NsiCatalogsStore();

        // Подготавливаем мап полов. Ключ - название пола в верхнем регистре, значение - элемент справочника Sex
        NsiObjectsHolder.getSexMap();

        // Подготавливаем мап типов удостоверений личности.
        // Ключ - GUID, ID, название типа в верхнем регистре, и/или код элемента справочника. Значение - пара: тип удостоверения личности и Идентификатор НСИ.
        NsiObjectsHolder.getIdentityCardTypesMap();

        // Подготавливаем мап стран мира. Понадобится для выставления гражданства.
        // Ключ - GUID, ID, название страны в верхнем регистре, и/или код элемента справочника. Значение - пара: Страна и Идентификатор НСИ.
        NsiObjectsHolder.getCountryIdToEntityMap();

        // Подготавливаем мап с должностями
        // Ключ - GUID, ID, название должности в верхнем регистре, и/или код элемента справочника. Значение - пара: Должность и Идентификатор НСИ.
        NsiObjectsHolder.getPostBoundedMap();

        // Подготавливаем мап с имеющимися связями типов подразделений и должностей
        // Ключ - пара: идентификатор типа подразделения и идентификатор должности. Значение - связь типа подразделения и должности.
        NsiObjectsHolder.getRelationsMap();

        // Подготавливаем мап подразделениямй
        // Ключ - GUID, ID и/или название подразделения в верхнем регистре. Значение - пара: Подразделение и Идентификатор НСИ.
        NsiObjectsHolder.getOrgUnitIdToEntityMap();

        // Подготавливаем мап персон
        // Ключ - GUID и/или ID. Значение - пара: Персона и Идентификатор НСИ.
        NsiObjectsHolder.getPersonIdToEntityMap();

        // Подготавливаем мап кадровых ресурсов
        // Ключ - ID персоны и/или ID кадрового ресурса. Значение - кадровый ресурс.
        NsiObjectsHolder.getEmployeeMap();

        // Сет логинов персон. Уже имеющиеся логины нельзя повторно занимать другими персонами. Увы.
        Set<String> loginSet = new HashSet<>();
        List<String> logins = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(Principal.class, "p").column(property(Principal.login().fromAlias("p"))));
        for (String login : logins) loginSet.add(login);
        store.setLoginSet(loginSet);
/*
        Set<Long> processedEntitySet = new HashSet<>();
        Map<String, CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds>> additionalEmployeePostMap = new HashMap();
        for (CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> entityToGuidPair : NsiObjectsHolder.getEmployeePostIdToEntityMap().values())
        {
            if (null != entityToGuidPair.getX() && !processedEntitySet.contains(entityToGuidPair.getX().getId()))
            {
                EmployeePost emplPost = entityToGuidPair.getX();

                Employee employee = emplPost.getEmployee();
                IdentityCard icard = employee.getPerson().getIdentityCard();
                FefuNsiIds epNsiId = entityToGuidPair.getY();
                CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> value = new CoreCollectionUtils.Pair<>(emplPost, epNsiId);

                //Вычисляем составной ключ из ФИО, должности и подаразделения
                StringBuilder postKeyBuilder = new StringBuilder();
                postKeyBuilder.append(icard.getLastName().toUpperCase()).append(" ").append(icard.getFirstName().toUpperCase()).append(" ");
                if (null != icard.getMiddleName())
                    postKeyBuilder.append(icard.getMiddleName().toUpperCase()).append(" ");
                postKeyBuilder.append(emplPost.getPostRelation().getPostBoundedWithQGandQL().getTitle().toUpperCase());
                postKeyBuilder.append(emplPost.getOrgUnit().getTitle().toUpperCase());

                // Вносим значения в мап
                additionalEmployeePostMap.put(postKeyBuilder.toString(), value);
            }
        }

        NsiObjectsHolder.getEmployeePostIdToEntityMap().putAll(additionalEmployeePostMap);*/

        return store;
    }

    @Override
    public void synchronizeFullCatalogWithNsi()
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG HAVE STARTED ==========");

        // Общее хранилище для всех мапов
        final NsiCatalogsStore store = prepareCatalogStore();

        // Сет идентификаторов персон, на которых ссылаются студенты.
        // Такие персоны мы не вправе изменять вместе с сотрудниками.
        Set<Long> personIdSet = new HashSet<>();
        List<Long> personIds = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(Student.class, "s").column(property(Student.person().id().fromAlias("s"))));
        for (Long personId : personIds) personIdSet.add(personId);
        store.setStudentPersonsSet(personIdSet);

        Set<Long> processedEntitySet = new HashSet<>();
        Map<String, CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds>> additionalEmployeePostMap = new HashMap();
        for (CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> entityToGuidPair : NsiObjectsHolder.getEmployeePostIdToEntityMap().values())
        {
            if (null != entityToGuidPair.getX() && !processedEntitySet.contains(entityToGuidPair.getX().getId()))
            {
                EmployeePost emplPost = entityToGuidPair.getX();

                Employee employee = emplPost.getEmployee();
                IdentityCard icard = employee.getPerson().getIdentityCard();
                FefuNsiIds epNsiId = entityToGuidPair.getY();
                CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> value = new CoreCollectionUtils.Pair<>(emplPost, epNsiId);

                //Вычисляем составной ключ из ФИО, должности и подаразделения
                StringBuilder postKeyBuilder = new StringBuilder();
                postKeyBuilder.append(icard.getLastName().toUpperCase()).append(" ").append(icard.getFirstName().toUpperCase()).append(" ");
                if (null != icard.getMiddleName())
                    postKeyBuilder.append(icard.getMiddleName().toUpperCase()).append(" ");
                postKeyBuilder.append(emplPost.getPostRelation().getPostBoundedWithQGandQL().getTitle().toUpperCase());
                postKeyBuilder.append(emplPost.getOrgUnit().getTitle().toUpperCase());

                // Вносим значения в мап
                additionalEmployeePostMap.put(postKeyBuilder.toString(), value);
            }
        }
        NsiObjectsHolder.getEmployeePostIdToEntityMap().putAll(additionalEmployeePostMap);

        // Подготавливаем список объектов с указанием идентификаторов подразделений, работники которых нас интересуют,
        // для получения полного перечня сотрудников этих подразделений серией запросов к НСИ
        List<EmployeeType> employeeObjectsForRetrieve = prepareOrgUnitMapsAndObjectsListForRetrieveEmployee();


        /* ================   ЗАБОР СОТРУДНИКОВ ИЗ НСИ   ===================== */

        // Забираем полный список всех элементов справочника из НСИ
        // Увы, полный retrieve отваливается по таймауту, а частичный по ID подразделения можно делать
        // только порциями, по несколько подразделений отдельными запросоми.
        List<EmployeeType> employeeTypeList = NsiReactorUtils.retrieveObjectsFromNSIByElementsList(EmployeeType.class, employeeObjectsForRetrieve/*.subList(0, 1)*/, 20);

        /* TODO del
        byte[] xdatagramstream = new byte[0];
        try
        {
            File file = new File("d:/posts.xml");
            FileInputStream fis = null;
            fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            byte[] buf = new byte[1024];
            try
            {
                for (int readNum; (readNum = fis.read(buf)) != -1; )
                {
                    bos.write(buf, 0, readNum);
                }
            } catch (IOException ex)
            {
            }
            xdatagramstream = bos.toByteArray();
        } catch (Exception e)
        {
        }

        XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, xdatagramstream);
        List<ru.tandemservice.unifefu.ws.nsi.datagram.EmployeeType> employeeTypeList = new ArrayList<>();
        Set<String> guidsSet = new HashSet();
        for (Object retrievedObject : respDatagram.getEntityList())
        {
            if (retrievedObject instanceof ru.tandemservice.unifefu.ws.nsi.datagram.EmployeeType)
            {
                employeeTypeList.add((ru.tandemservice.unifefu.ws.nsi.datagram.EmployeeType) retrievedObject);
                //if(guidsSet)guidsSet.
            }
        }      */

        // Подготавливаем список уникальных должностей
        Set<String> postGuidsSet = new HashSet<>();
        List<PostType> postList = new ArrayList<>();

        List<String> alreadyProcessedEmployee = new ArrayList<>();
        List<EmployeeType> employeeToCreateList = new ArrayList<>();
        List<CoreCollectionUtils.Pair<EmployeeType, CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds>>> employeeToUpdate = new ArrayList<>();

        for (EmployeeType employee : employeeTypeList)
        {
            String employeePostGuid = null != employee.getID() ? employee.getID().toLowerCase() : null;
            // Если сотрудник найден по GUI, то вносим его в список на обновление
            if (null != NsiObjectsHolder.getEmployeePostEntityToGuidPair(employeePostGuid))
            {
                employeeToUpdate.add(new CoreCollectionUtils.Pair<>(employee, NsiObjectsHolder.getEmployeePostEntityToGuidPair(employeePostGuid)));
            } else //TODO if (null == employee.getEmployeeDischargeDate())
            {
                // Формируем ключ для поиска сотрудника среди уже имеющихся по ФИО, должности и подразделению
                StringBuilder postKeyBuilder = new StringBuilder();

                HumanType human = employee.getHumanID() != null ? employee.getHumanID().getHuman() : null;
                if (null != human)
                {
                    if (null != human.getHumanLastName())
                        postKeyBuilder.append(human.getHumanLastName().trim().toUpperCase()).append(" ");
                    if (null != human.getHumanFirstName())
                        postKeyBuilder.append(human.getHumanFirstName().trim().toUpperCase()).append(" ");
                    if (null != human.getHumanMiddleName())
                        postKeyBuilder.append(human.getHumanMiddleName().trim().toUpperCase()).append(" ");
                }

                if (null != employee.getEmployeePostID())
                {
                    PostType post = employee.getEmployeePostID().getPost();
                    if (!postGuidsSet.contains(post.getID()))
                    {
                        postList.add(post);
                        postGuidsSet.add(post.getID());
                    }
                    if (null != post.getPostName())
                        postKeyBuilder.append(post.getPostName().trim().toUpperCase()).append(" ");
                }

                if (null != employee.getEmployeeDepartmentID())
                {
                    DepartmentType dep = employee.getEmployeeDepartmentID().getDepartment();
                    if (null != dep.getDepartmentName())
                        postKeyBuilder.append(dep.getDepartmentName().trim().toUpperCase());
                }

                // Дубли сотрудников на должностях нам не нужны
                if (!alreadyProcessedEmployee.contains(postKeyBuilder.toString()))
                {
                    alreadyProcessedEmployee.add(postKeyBuilder.toString());

                    if (null != NsiObjectsHolder.getEmployeePostIdToEntityMap().get(postKeyBuilder.toString()))
                    {
                        //Найден похожий сотрудник по составному ключу "ФИО + Должность + Подразделение"
                        employeeToUpdate.add(new CoreCollectionUtils.Pair<>(employee, NsiObjectsHolder.getEmployeePostIdToEntityMap().get(postKeyBuilder.toString())));
                    } else
                    {
                        // Новый сотрудник
                        employeeToCreateList.add(employee);
                    }
                }
            }
        }
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- EmployeeType catalog was processed within " + employeeTypeList.size() + " elements. ----------");

        // Модификация имеющихся и добавление недостающих PostBoundedWithQGandQL
        Set<String> alreadyUsedCodes = new HashSet<>(); // Исключаем повторы генерируемых кодов для элементов справочника
        for (PostType postType : postList)
        {
            // Должности без названия нам не нужны
            if (null == postType.getPostName() || postType.getPostName().trim().length() == 0) continue;

            // Ищем в инициализированных ранее мапах сотрудника на должности
            CoreCollectionUtils.Pair<PostBoundedWithQGandQL, FefuNsiIds> post = NsiObjectsHolder.getPostBoundedMap().get(postType.getID());
            if (null != post)
            {
                // Обновляем должность, если она изменилась
                updatePostBoundedIfItIsNeccessary(postType, post.getX(), NsiObjectsHolder.getPostBoundedMap());
            } else
            {
                // Пытаемся найти по названию должности в уже имеющихся
                post = NsiObjectsHolder.getPostBoundedMap().get(postType.getPostName().toUpperCase());
                if (null != post)
                {
                    // Если должность найдена, то просто присваиваем ей GUID из НСИ
                    FefuNsiIds nsiId = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(post.getX(), null, new FefuNsiIdWrapper(postType.getID(), PostBoundedWithQGandQL.ENTITY_NAME));
                    post.setY(nsiId);
                    NsiObjectsHolder.getPostBoundedMap().put(postType.getID(), post);
                } else
                {
                    // Если в имеющемся не найдено похожих должностей, то создаем новую должность и складываем её в мапу
                    CoreCollectionUtils.Pair<PostBoundedWithQGandQL, FefuNsiIds> pbPair = createPostBoundedWithQGandQL(postType, alreadyUsedCodes);
                    NsiObjectsHolder.getPostBoundedMap().put(pbPair.getY().getGuid(), pbPair);
                }
            }
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- PostType catalog was synchronized with NSI. ----------");


        // Обновляем сотрудкников
        BatchUtils.execute(employeeToUpdate, 100, new BatchUtils.Action<CoreCollectionUtils.Pair<EmployeeType, CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds>>>()
        {
            @Override
            public void execute(Collection<CoreCollectionUtils.Pair<EmployeeType, CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds>>> elements)
            {
                for (CoreCollectionUtils.Pair<EmployeeType, CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds>> employeeTypePair : elements)
                {
                    try
                    {
                        createEmployeePost(employeeTypePair.getX(), employeeTypePair.getY(), store, false);
                    } catch (NSIProcessingErrorException e)
                    {

                    }
                }
                DataAccessServices.dao().getComponentSession().flush();
            }
        });

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- There are " + employeeToUpdate.size() + " employee posts were updated at OB. ----------");

        // Добавляем новых сотрудников
        BatchUtils.execute(employeeToCreateList, 100, new BatchUtils.Action<EmployeeType>()
        {
            @Override
            public void execute(Collection<EmployeeType> elements)
            {
                for (EmployeeType employeeType : elements)
                {
                    try
                    {
                        createEmployeePost(employeeType, null, store, false);
                    } catch (NSIProcessingErrorException e)
                    {

                    }
                }
                DataAccessServices.dao().getComponentSession().flush();
            }
        });

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- There are " + employeeToCreateList.size() + " employee posts were created at OB. ----------");

        // Изменяем время последней синхронизации для справочника в целом
        IFefuNsiSyncDAO.instance.get().updateNsiCatalogSyncTime(UniDaoFacade.getCoreDao().getCatalogItem(FefuNsiCatalogType.class, FefuNsiCatalogTypeCodes.EMPLOYEE_TYPE).getNsiCode());
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG WAS FINISHED ==========");
    }

    @Override
    public void insertOrUpdateNewItemsAtNSI(Set<Long> entityToAddIdsSet)
    {
        // Ничего с объектами в НСИ мы делать не можем
    }

    @Override
    public void deleteItemsFromNSI(Set<Long> entityToAddIdsSet)
    {
        // Ничего с объектами в НСИ мы делать не можем
    }

    private List<EmployeeType> prepareOrgUnitMapsAndObjectsListForRetrieveEmployee()
    {
        List<EmployeeType> employeeObjectsToRetrieveByTheDepartments = new ArrayList<>();
        Map<String, FefuNsiIds> orgUnitNsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMap(OrgUnit.ENTITY_NAME);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole nsiIds list (" + orgUnitNsiIdsMap.values().size() + " items) for " + OrgUnit.ENTITY_NAME + " catalog was received from database ----------");

        // Для каждого подразделения подготовливаем свой объект фильтрации сотрудников,
        // дабы не наткнуться на таймаут получения всех сотрудников одним пакетом, а заодно
        // избавиться от сотрудников, которые не имеют отношения к учебным подразделениям.
        Set<String> orgUnitGuidsSet = new HashSet<>();
        for (FefuNsiIds nsiId : orgUnitNsiIdsMap.values())
        {
            if (!orgUnitGuidsSet.contains(nsiId.getGuid()))
            {
                orgUnitGuidsSet.add(nsiId.getGuid());

                DepartmentType dep = NsiReactorUtils.NSI_OBJECT_FACTORY.createDepartmentType();
                EmployeeType.EmployeeDepartmentID depId = NsiReactorUtils.NSI_OBJECT_FACTORY.createEmployeeTypeEmployeeDepartmentID();
                EmployeeType employee = NsiReactorUtils.NSI_OBJECT_FACTORY.createEmployeeType();
                dep.setID(nsiId.getGuid());
                depId.setDepartment(dep);
                employee.setEmployeeDepartmentID(depId);
                employeeObjectsToRetrieveByTheDepartments.add(employee);
            }
        }

        return employeeObjectsToRetrieveByTheDepartments;
    }

    private CoreCollectionUtils.Pair<PostBoundedWithQGandQL, FefuNsiIds> createPostBoundedWithQGandQL(PostType postType, Set<String> alreadyUsedCodes)
    {
        if (null == postType.getPostName() || postType.getPostName().trim().length() == 0) return null;
        String code = String.valueOf(System.currentTimeMillis());
        while (alreadyUsedCodes.contains(code)) code = String.valueOf(System.currentTimeMillis());
        alreadyUsedCodes.add(code);

        org.tandemframework.shared.employeebase.catalog.entity.EmployeeType employeeType = NsiObjectsHolder.getEmployeeTypeRS();
        //boolean aup = null != postType.getPostAUP() ? Boolean.parseBoolean(postType.getPostAUP()) : false;
        String postCategory = null != postType.getPostStatisticalServiceCategory() ? postType.getPostStatisticalServiceCategory().trim().toUpperCase() : null;

        if (null != postCategory)
        {
            if (postCategory.contains("НАУЧ")) employeeType = NsiObjectsHolder.getEmployeeTypePPS();
            else if (postCategory.contains("РУКОВОД")) employeeType = NsiObjectsHolder.getEmployeeTypeAUP();
        }

        Post post = new Post();
        post.setEnabled(true);
        post.setCode("Post" + code);
        post.setUserCode(postType.getPostID());
        post.setTitle(postType.getPostName());
        post.setEmployeeType(employeeType);
        DataAccessServices.dao().save(post);
        FefuNsiSyncDAO.logEvent(Level.INFO, "Post \"" + post.getTitle() + "\" was added.");

        PostBoundedWithQGandQL postBounded = new PostBoundedWithQGandQL();
        postBounded.setCode("PostBounded" + code);
        postBounded.setUserCode(postType.getPostID());
        postBounded.setTitle(postType.getPostName());
        postBounded.setProfQualificationGroup(NsiObjectsHolder.getDefaultProfQualificationGroup());
        postBounded.setQualificationLevel(NsiObjectsHolder.getDefaultQualificationLevel());
        postBounded.setEnabled(true);
        postBounded.setPost(post);
        DataAccessServices.dao().save(postBounded);
        FefuNsiSyncDAO.logEvent(Level.INFO, "PostBounded \"" + postBounded.getTitle() + "\" was added.");

        FefuNsiIds nsiId = new FefuNsiIds();
        nsiId.setEntityType(PostBoundedWithQGandQL.ENTITY_NAME);
        nsiId.setEntityId(postBounded.getId());
        nsiId.setGuid(postType.getID());
        nsiId.setSyncTime(new Date());
        DataAccessServices.dao().save(nsiId);
        FefuNsiSyncDAO.logEvent(Level.INFO, "PostBoundedNsiId \"" + nsiId.getGuid() + "\" was added.");

        CoreCollectionUtils.Pair<PostBoundedWithQGandQL, FefuNsiIds> postPair = new CoreCollectionUtils.Pair<>(postBounded, nsiId);
        NsiObjectsHolder.getPostBoundedMap().put(postBounded.getId().toString(), postPair);
        NsiObjectsHolder.getPostBoundedMap().put(nsiId.getGuid(), postPair);

        return postPair;
    }

    private void updatePostBoundedIfItIsNeccessary(PostType postType, PostBoundedWithQGandQL postBounded, Map<String, CoreCollectionUtils.Pair<PostBoundedWithQGandQL, FefuNsiIds>> postBoundedMap)
    {
        boolean anythingChanged = false;
        if (null == postType.getPostName() || postType.getPostName().trim().length() == 0) return;

        String postName = postType.getPostName().trim();
        String oldPostName = postBounded.getTitle();
        if (isShouldBeUpdated(postBoundedMap, postType.getPostName(), postBounded.getTitle(), postBounded.getId()))
        {
            anythingChanged = true;
            postBounded.setTitle(postType.getPostName().trim());
            postBounded.getPost().setTitle(postBounded.getTitle());
        }

        if (isShouldBeUpdated(null, postType.getPostID(), postBounded.getUserCode(), null))
        {
            anythingChanged = true;
            postBounded.setUserCode(postType.getPostID().trim());
            postBounded.getPost().setUserCode(postBounded.getUserCode());
        }

        if (anythingChanged)
        {
            DataAccessServices.dao().update(postBounded.getPost());
            DataAccessServices.dao().update(postBounded);
            FefuNsiSyncDAO.logEvent(Level.INFO, "Post \"" + oldPostName + "\" was changed to \"" + postName + "\"");
        }
    }

    public static <T extends IEntity> boolean isShouldBeUpdated(Map<String, CoreCollectionUtils.Pair<T, FefuNsiIds>> entityMap, String nsiEntityStr, String entityStr, Long currentEntityId)
    {
        if (null == nsiEntityStr || null == entityStr) return false;
        String str1 = nsiEntityStr.trim().toUpperCase();
        String str2 = entityStr.trim().toUpperCase();
        return !str1.equals(str2) && !(null != entityMap && entityMap.containsKey(str1) && !entityMap.get(str1).getX().getId().equals(currentEntityId));
    }

    private CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> createEmployeePost(EmployeeType employeeType, CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> employeePostPair, NsiCatalogsStore store, boolean fullSync) throws NSIProcessingErrorException
    {
        if (null == employeeType.getHumanID() || null == employeeType.getEmployeePostID() || null == employeeType.getEmployeeDepartmentID())
            return null;

        if (null == NsiObjectsHolder.getOrgUnitIdToEntityMap().get(employeeType.getEmployeeDepartmentID().getDepartment().getID()))
            return null;

        OrgUnit orgUnit = NsiObjectsHolder.getOrgUnitIdToEntityMap().get(employeeType.getEmployeeDepartmentID().getDepartment().getID()).getX();

        if (null == NsiObjectsHolder.getPostBoundedMap().get(employeeType.getEmployeePostID().getPost().getID()))
            return null;
        PostBoundedWithQGandQL post = NsiObjectsHolder.getPostBoundedMap().get(employeeType.getEmployeePostID().getPost().getID()).getX();

        OrgUnitTypePostRelation postRelation = NsiObjectsHolder.getRelationsMap().get(new CoreCollectionUtils.Pair<>(orgUnit.getOrgUnitType().getId(), post.getId()));
        if (null == postRelation)
        {
            postRelation = new OrgUnitTypePostRelation();
            postRelation.setMultiPost(true);
            postRelation.setHeaderPost(false);
            postRelation.setOrgUnitType(orgUnit.getOrgUnitType());
            postRelation.setPostBoundedWithQGandQL(post);
            DataAccessServices.dao().save(postRelation);
            NsiObjectsHolder.getRelationsMap().put(new CoreCollectionUtils.Pair<>(orgUnit.getOrgUnitType().getId(), post.getId()), postRelation);
        }

        EmployeePost employeePost = null != employeePostPair ? employeePostPair.getX() : null;
        HumanType human = employeeType.getHumanID().getHuman();

        Person person = null != employeePost ? employeePost.getPerson() : null;

        if (null != NsiObjectsHolder.getPersonIdToEntityMap().get(employeeType.getHumanID().getHuman().getID().toLowerCase()) && !store.getStudentPersonsSet().contains(NsiObjectsHolder.getPersonIdToEntityMap().get(employeeType.getHumanID().getHuman().getID().toLowerCase()).getX().getId()))
            person = NsiObjectsHolder.getPersonIdToEntityMap().get(employeeType.getHumanID().getHuman().getID().toLowerCase()).getX();

        if (null == person && !fullSync)
            throw new NSIProcessingErrorException("Could not find the given Human with guid=" + employeeType.getHumanID().getHuman().getID() + " at OB.");
        person = DataAccessServices.dao().get(Person.class, person.getId());

        if (fullSync)
        {
            IdentityCard icard = null != person ? person.getIdentityCard() : new IdentityCard();
            if (null == person) person = new Person();

            if (null == icard.getId()) icard.setCreationDate(new Date());
            icard.setBirthPlace(human.getHumanBirthPlace());
            icard.setBirthDate(NsiDatagramUtil.parseDate(human.getHumanBirthdate()));
            icard.setLastName(null != human.getHumanLastName() ? human.getHumanLastName() : "БЕЗ ФАМИЛИИ");
            icard.setFirstName(null != human.getHumanFirstName() ? human.getHumanFirstName() : "БЕЗ ИМЕНИ");
            icard.setMiddleName(human.getHumanMiddleName());

            icard.setSex(null != human.getHumanSex() ? NsiObjectsHolder.getSexMap().get(human.getHumanSex().toUpperCase()) : NsiObjectsHolder.getSexMap().get("МУЖСКОЙ"));

            IdentityCardType rfIcardType = NsiObjectsHolder.getIdentityCardTypesMap().get(IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII).getX();
            ICitizenship country = rfIcardType.getCitizenshipDefault();
            if (null != human.getHumanCitizenship() && null != NsiObjectsHolder.getCountryIdToEntityMap().get(human.getHumanCitizenship().getOksm().getID()))
            {
                country = NsiObjectsHolder.getCountryIdToEntityMap().get(human.getHumanCitizenship().getOksm().getID()).getX();
            }
            icard.setCitizenship(country);

            FefuNsiSyncDAO.logEvent(Level.INFO, "New person " + icard.getLastName() + " " + icard.getFirstName() + " was added to OB.");

            IdentityCardType icardType = NsiObjectsHolder.getIdentityCardTypesMap().get(IdentityCardTypeCodes.BEZ_UDOSTOVERENIYA).getX();
            if (null != human.getIdentityCard())
            {
                ru.tandemservice.unifefu.ws.nsi.datagram.IdentityCardType identityCardType = human.getIdentityCard().getIdentityCard();
                if (null != identityCardType)
                {
                    if (null != identityCardType.getIdentityCardKindID() && null != NsiObjectsHolder.getIdentityCardTypesMap().get(identityCardType.getIdentityCardKindID().getIdentityCardKind().getID()))
                    {
                        icardType = NsiObjectsHolder.getIdentityCardTypesMap().get(identityCardType.getIdentityCardKindID().getIdentityCardKind().getID()).getX();
                    }
                    icard.setIssuancePlace(identityCardType.getIdentityCardWhoGive());
                    icard.setIssuanceDate(NsiDatagramUtil.parseDate(identityCardType.getIdentityCardDateGive()));
                    icard.setSeria(identityCardType.getIdentityCardSeries());
                    icard.setNumber(identityCardType.getIdentityCardNumber());
                }
            }
            icard.setCardType(icardType);
            DataAccessServices.dao().saveOrUpdate(icard);
            IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(icard.getId());

            boolean newPerson = false;
            if (null == person.getId())
            {
                PersonContactData contactData = new PersonContactData();
                DataAccessServices.dao().save(contactData);
                person.setContactData(contactData);
                person.setServiceLengthDays(0);
                person.setServiceLengthMonths(0);
                person.setServiceLengthYears(0);
                newPerson = true;
            }

            person.setIdentityCard(icard);
            person.setNeedDormitory(false);
            person.setInnNumber(human.getHumanINN());
            person.setSnilsNumber(human.getHumanPFR());
            DataAccessServices.dao().saveOrUpdate(person);
            IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(person.getId());

            icard.setPerson(person);
            DataAccessServices.dao().update(icard);
            IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(icard.getId());

            /*FefuNsiIds icardNsiId = null;
            if (null != human.getIdentityCard())
            {
                icardNsiId = new FefuNsiIds();
                icardNsiId.setEntityType(IdentityCard.ENTITY_NAME);
                icardNsiId.setEntityId(icard.getId());
                icardNsiId.setGuid(human.getIdentityCard().getIdentityCard().getID());
                icardNsiId.setSyncTime(new Date());
                DataAccessServices.dao().save(icardNsiId);
                System.out.println("NsiId \"" + icardNsiId.getGuid() + "\" was added.");
            } */

            FefuNsiIds personNsiId = null != NsiObjectsHolder.getPersonIdToEntityMap().get(human.getID().toLowerCase()) ? NsiObjectsHolder.getPersonIdToEntityMap().get(human.getID().toLowerCase()).getY() : null;
            if (null == personNsiId)
            {
                personNsiId = new FefuNsiIds();
                personNsiId.setEntityType(Person.ENTITY_NAME);
            } else
            {
                personNsiId = DataAccessServices.dao().get(personNsiId.getId());
            }
            personNsiId.setEntityId(person.getId());
            personNsiId.setGuid(human.getID().toLowerCase());
            personNsiId.setSyncTime(new Date());
            DataAccessServices.dao().saveOrUpdate(personNsiId);
            FefuNsiSyncDAO.logEvent(Level.INFO, "PersonNsiId \"" + personNsiId.getGuid() + "\" was added.");

            NsiObjectsHolder.getPersonIdToEntityMap().put(human.getID().toLowerCase(), new CoreCollectionUtils.Pair<>(person, personNsiId));

            if (newPerson)
            {
                Principal principal = new Principal();
                Person2PrincipalRelation person2PrincipalRelation;

                String login = human.getHumanLogin();
                if (null == login)
                    login = PersonSecurityUtil.generateLogin(icard.getFirstName(), icard.getLastName(), icard.getMiddleName());
                while (store.getLoginSet().contains(login)) login += login + "1";
                store.getLoginSet().add(login);

                principal.setLogin(login);
                principal.assignNewPassword(login);
                principal.setAuthenticationType(NsiObjectsHolder.getDefaultPrincipalAuthenticationType());

                person2PrincipalRelation = new Person2PrincipalRelation();
                person2PrincipalRelation.setPrincipal(principal);
                person2PrincipalRelation.setPerson(person);

                DataAccessServices.dao().save(principal);
                DataAccessServices.dao().save(person2PrincipalRelation);
            }
        } else if (null == person) throw new NSIProcessingErrorException("Target human could not be found at OB");

        Employee employee = null != employeePost ? employeePost.getEmployee() : NsiObjectsHolder.getEmployeeMap().get(person.getId().toString());

        if (null == employee)
        {
            employee = new Employee();
            employee.setArchival(false);
            employee.setLabourUnionMember(false);
        } else
        {
            employee = DataAccessServices.dao().get(employee.getId());
        }

        if (null == employee.getId())
        {
            Principal principal = PersonManager.instance().dao().getPrincipal(person);
            employee.setPrincipal(principal);
        }

        employee.setEmployeeCode(null != employeeType.getEmployeeID() ? employeeType.getEmployeeID() : "-");
        employee.setPerson(person);
        DataAccessServices.dao().saveOrUpdate(employee);
        NsiObjectsHolder.getEmployeeMap().put(person.getId().toString(), employee);
        NsiObjectsHolder.getEmployeeMap().put(employeeType.getHumanID().getHuman().getID().toLowerCase(), employee);

        if (null == employeePost)
        {
            employeePost = new EmployeePost();
            employeePost.setFreelance(false);
            employeePost.setHourlyPaid(false);
        }

        employeePost.setEmployee(employee);
        employeePost.setOrgUnit(orgUnit);
        employeePost.setPostRelation(postRelation);
        if (null != employeeType.getEmployeeEmploymentDate())
            employeePost.setPostDate(NsiDatagramUtil.parseDate(employeeType.getEmployeeEmploymentDate()));
        employeePost.setDismissalDate(NsiDatagramUtil.parseDate(employeeType.getEmployeeDischargeDate()));
        if (null == employeePost.getPostDate()) employeePost.setPostDate(new Date());

        if (null == employeePost.getDismissalDate())
            employeePost.setPostStatus(UniDaoFacade.getCoreDao().getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_ACTIVE));
        else
            employeePost.setPostStatus(UniDaoFacade.getCoreDao().getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_FIRED));

        employeePost.setPostType(UniDaoFacade.getCoreDao().getCatalogItem(org.tandemframework.shared.employeebase.catalog.entity.PostType.class, PostTypeCodes.MAIN_JOB));
        employeePost.setMainJob(true);

        if (null != employeeType.getEmploymentKind() && employeeType.getEmploymentKind().trim().length() > 0)
        {
            if (StringUtils.containsIgnoreCase(employeeType.getEmploymentKind(), "СОВМ"))
            {
                employeePost.setPostType(UniDaoFacade.getCoreDao().getCatalogItem(org.tandemframework.shared.employeebase.catalog.entity.PostType.class, PostTypeCodes.SECOND_JOB));
                employeePost.setMainJob(false);
            }
        }

        DataAccessServices.dao().saveOrUpdate(employeePost);

        if (null != employeeType.getEmployeeRate())
        {
            try
            {
                Double doubleRate = Double.valueOf(employeeType.getEmployeeRate().trim().replaceAll(",", "\\."));
                FefuEmployeePostExt postExt = DataAccessServices.dao().getByNaturalId(new FefuEmployeePostExtGen.NaturalId(employeePost));
                if (null == postExt)
                {
                    postExt = new FefuEmployeePostExt();
                    postExt.setEmployeePost(employeePost);
                }
                postExt.setRate(((Double) (doubleRate * 10000)).intValue());
                DataAccessServices.dao().saveOrUpdate(postExt);
            } catch (Exception e)
            {
            }
        }

        StringBuilder postKeyBuilder = new StringBuilder();
        postKeyBuilder.append(person.getIdentityCard().getLastName().toUpperCase()).append(" ").append(person.getIdentityCard().getFirstName().toUpperCase()).append(" ");
        if (null != person.getIdentityCard().getMiddleName())
            postKeyBuilder.append(person.getIdentityCard().getMiddleName().toUpperCase()).append(" ");
        postKeyBuilder.append(employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle().toUpperCase());
        postKeyBuilder.append(employeePost.getOrgUnit().getTitle().toUpperCase());

        FefuNsiIds employeePostNsiId = (null != employeePostPair && null != employeePostPair.getY()) ? employeePostPair.getY() : null;
        if (null == employeePostNsiId)
        {
            employeePostNsiId = new FefuNsiIds();
        } else
        {
            employeePostNsiId = DataAccessServices.dao().get(employeePostNsiId.getId());
        }
        if (employeeType.getID().toLowerCase().equals(employeePostNsiId.getGuid()))
        {
            DataAccessServices.dao().delete(employeePostNsiId);
            employeePostNsiId = new FefuNsiIds();
        }

        employeePostNsiId.setEntityId(employeePost.getId());
        employeePostNsiId.setGuid(employeeType.getID().toLowerCase());
        employeePostNsiId.setEntityType(EmployeePost.ENTITY_NAME);
        employeePostNsiId.setSyncTime(new Date());
        DataAccessServices.dao().saveOrUpdate(employeePostNsiId);
        FefuNsiSyncDAO.logEvent(Level.INFO, "EmployeePostNsiId \"" + employeePostNsiId.getGuid() + "\" was added.");

        CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> result = new CoreCollectionUtils.Pair<>(employeePost, employeePostNsiId);
        NsiObjectsHolder.getEmployeePostIdToEntityMap().put(employeePostNsiId.getGuid(), result);
        NsiObjectsHolder.getEmployeePostIdToEntityMap().put(employeePost.getId().toString(), result);
        NsiObjectsHolder.getEmployeePostIdToEntityMap().put(postKeyBuilder.toString(), result);

        return result;
    }

    private class NsiCatalogsStore
    {
        /**
         * Сет идентификаторов персон, используемых в студентах.
         * Данные таких персон мы не можем модифицировать, чтобы не сломать самих студентов.
         */
        private Set<Long> _studentPersonsSet = new HashSet<>();

        private Set<String> _loginSet;

        private Set<Long> getStudentPersonsSet()
        {
            return _studentPersonsSet;
        }

        private void setStudentPersonsSet(Set<Long> studentPersonsSet)
        {
            _studentPersonsSet = studentPersonsSet;
        }

        private Set<String> getLoginSet()
        {
            return _loginSet;
        }

        private void setLoginSet(Set<String> loginSet)
        {
            _loginSet = loginSet;
        }
    }

    private List<Object> insertOrUpdate(List<INsiEntity> itemsToProcess, String operationType) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG AT OB HAVE STARTED ==========");

        // Подготавливаем список консистентных сущностей из НСИ (вдруг там внезапно окажется дубль)
        Set<String> guidsToInsertSet = new HashSet<>();
        List<EmployeeType> itemsToInsertChecked = new ArrayList<>();
        List<Object> itemsProcessed = new ArrayList<>();

        for (INsiEntity nsiEntity : itemsToProcess)
        {
            // Если сущность помечена как неконсистентная (например, дубль), то её не нужно учитывать
            if (null == nsiEntity.getIsNotConsistent() || 1 != nsiEntity.getIsNotConsistent())
            {
                // Если у какой-то из сущностей в пакете не указан GUID, то обрабатывать такой пакет нельзя
                if (null == nsiEntity.getID())
                {
                    FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing incoming package was failed. There are one or more elements without GUID specified for " + EmployeePost.ENTITY_NAME + " catalog ----------");
                    FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
                    throw new NSIProcessingErrorException("Could not " + operationType.toLowerCase() + " element without GUID specified. Please, specify GUID for all items in request package.");
                }

                itemsToInsertChecked.add((EmployeeType) nsiEntity);
                itemsProcessed.add(nsiEntity);

                // Добавляем список guid'ов, для которых может понадобиться объединение дублей
                List<String> mergeDuplicatesList = getUtil().getMergeDuplicatesList(nsiEntity);
                if (null != mergeDuplicatesList) guidsToInsertSet.addAll(mergeDuplicatesList);
            }
        }

        // Если после отсеивания невалидных сущностей в пакете не осталось ничего, то обработка пакета теряет смысл
        if (itemsToInsertChecked.isEmpty())
        {
            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing incoming package was failed. There is no any entity guid to " + operationType.toLowerCase() + " in datagram. Could not process an empty request. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
            throw new NSIProcessingErrorException("There is no any entity guid to " + operationType.toLowerCase() + " in datagram. Could not process an empty request.");
        }

        // Готовим список сущностей НСИ, для которых требуется поднять данные из БД.
        // Здесь же учитываются идентификаторы НСИ для дедубликации
        List<INsiEntity> guidEntitySet = new ArrayList<>();
        guidEntitySet.addAll(itemsToProcess);

        for (String duplicateGuid : guidsToInsertSet)
        {
            guidEntitySet.add(getCatalogElementRetrieveRequestObject(duplicateGuid));
        }

        // Подготавливаем сет идентификаторов ОБ для инициализации мапов
        Set<Long> entityIdSet = IFefuNsiSyncDAO.instance.get().getEntityIdSetByNsiEntityList(guidEntitySet);

        // Получаем список GUID'ов неудаляемых объектов (в силу наличия ссылок с других объектов), на случай, если придётся дедублицировать
        List<String> nonDeletableGuids = IFefuNsiSyncDAO.instance.get().getNonDeletableGuidsList(EmployeePost.class, entityIdSet);

        // Инициализируем мапы, необходимые для создания/обновления сотрудников
        NsiCatalogsStore store = prepareCatalogStore();

        Set<Long> entityDuplicateIdsToDel = new HashSet<>();
        Set<String> alreadyProcessedEntitySet = new HashSet<>();
        List<String> notDeletedDuplicateGuids = new ArrayList<>();
        List<String> nonProcessableGuids = new ArrayList<>();

        for (EmployeeType nsiEntity : itemsToInsertChecked)
        {
            if (!alreadyProcessedEntitySet.contains(nsiEntity.getID()))
            {
                alreadyProcessedEntitySet.add(nsiEntity.getID());
                List<CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds>> entityDuplicates = new ArrayList<>();
                CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> entityPair = NsiObjectsHolder.getEmployeePostEntityToGuidPair(nsiEntity.getID());
                if (null != entityPair) entityDuplicates.add(entityPair);

                // Пробегаемся по дублям, если таковые имеются
                List<String> mergeDuplicatesList = getUtil().getMergeDuplicatesList(nsiEntity);
                if (null != mergeDuplicatesList)
                {
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- There are some entities needs to be deduplicated (" + mergeDuplicatesList.size() + " items) for " + EmployeePost.ENTITY_NAME + " catalog ----------");
                    for (String duplicateGuid : mergeDuplicatesList)
                    {
                        CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> duplicateEntityPair = NsiObjectsHolder.getEmployeePostEntityToGuidPair(duplicateGuid);
                        if (null == entityPair && null != duplicateEntityPair) // Не найден элемент с таким guid'ом, но есть элемент с GUID'ом дубликата
                        {
                            entityPair = duplicateEntityPair;
                        }
                        if (null != duplicateEntityPair && !entityDuplicates.contains(duplicateEntityPair))
                            entityDuplicates.add(duplicateEntityPair);
                    }
                }

                // Если до сих пор не найден объект в ОБ ни по GUID'у оригинала, ни по GUID'ам дублей, то пытаемся найти по hash  TODO опасная уонструкция, хороша только для первоначальной синхронизации
                /*if (null == entityPair || null == entityPair.getX())
                {
                    entityPair = getPossibleDuplicatePair(nsiEntity);
                }*/

                if (null != entityPair && null != entityPair.getX())
                {
                    entityDuplicates.remove(entityPair);
                    try
                    {
                        createEmployeePost(nsiEntity, entityPair, store, false);
                    } catch (NSIProcessingErrorException e)
                    {
                        FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing incoming package was failed. " + e.getMessage() + " ----------");
                        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
                        throw e;
                    }
                } else
                {
                    try
                    {
                        CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> modifiedPair = createEmployeePost(nsiEntity, null, store, false);
                        if (null == modifiedPair || null == modifiedPair.getX())
                        {
                            nonProcessableGuids.add(nsiEntity.getID());
                        }
                    } catch (NSIProcessingErrorException e)
                    {
                        FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing incoming package was failed. " + e.getMessage() + " ----------");
                        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
                        throw e;
                    }
                }

                // Формируем сет идентификаторов сущностей ОБ для удаления в процессе дедубликации
                for (CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> duplicateToDelPair : entityDuplicates)
                {
                    if (!nonDeletableGuids.contains(duplicateToDelPair.getY().getGuid()))
                    {
                        NsiObjectsHolder.getEmployeePostIdToEntityMap().remove(duplicateToDelPair);
                        entityDuplicateIdsToDel.add(duplicateToDelPair.getX().getId());
                    } else notDeletedDuplicateGuids.add(duplicateToDelPair.getY().getGuid());
                }
            }
        }

        // Если есть невставляемые сущности (в силу отсутствия значений обязательных полей, то возвращаем НСИ ошибку
        if (!nonProcessableGuids.isEmpty())
        {
            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing entities for " + EmployeePost.ENTITY_NAME + " catalog was failed due to some required field values absence. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
            throw new NSIProcessingErrorException("One, or more entities could not be processed due to some required field values absence. Error object GUID's: " + CommonBaseStringUtil.joinNotEmpty(nonProcessableGuids, ", "));
        }

        // Если список неудаляемых объектов не пуст, то возвращаем в НСИ ошибку, не выполняя удаления других объектов
        if (!notDeletedDuplicateGuids.isEmpty())
        {
            StringBuilder errGuids = new StringBuilder();
            for (String guid : notDeletedDuplicateGuids)
            {
                errGuids.append(errGuids.length() > 0 ? ", " : "").append(guid);
            }

            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Deleting entities in process of deduplication for " + EmployeePost.ENTITY_NAME + " catalog was failed. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");

            throw new NSIProcessingErrorException("One, or more entities can not be deleted, because of other objects relations with the entities have to be deleted. " +
                    "Please, check dependencies for the objects " + CommonBaseStringUtil.joinNotEmpty(notDeletedDuplicateGuids, ", ") + ".");
        }

        // Удаляем дубликаты и связанные с ними идентификаторы НСИ
        IFefuNsiSyncDAO.instance.get().deleteObjectsSetCheckedForDeletability(EmployeePost.class, entityDuplicateIdsToDel, true);
        IFefuNsiSyncDAO.instance.get().deleteRelatedNsiId(entityDuplicateIdsToDel);

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");

        return itemsProcessed;
    }

    @Override
    public List<Object> insert(List<INsiEntity> itemsToInsert) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        return insertOrUpdate(itemsToInsert, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT);
    }

    @Override
    public List<Object> update(List<INsiEntity> itemsToUpdate) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        return insertOrUpdate(itemsToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE);
    }

    @Override
    public List<Object> delete(List<INsiEntity> itemsToDelete) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG FROM OB HAVE STARTED ==========");

        // Подготавливаем сет идентификаторов ОБ для инициализации мапов
        Set<Long> entityIdSet = IFefuNsiSyncDAO.instance.get().getEntityIdSetByNsiEntityList(itemsToDelete);

        // Не указан ни один идентификатор сущности НСИ на удаление, либо запрос предполагает тотальное удаление всех элементов из ОБ
        if (null == entityIdSet)
        {
            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- There are no any GUID specified for the delete operation. Whole entity set could not be deleted ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG FROM OB WAS FINISHED ==========");
            throw new NSIProcessingErrorException("Could not delete whole entity set. Please, specify entity ids list to delete.");
        }

        // Получаем список GUID'ов неудаляемых объектов (в силу наличия ссылок с других объектов)
        List<String> nonDeletableGuids = IFefuNsiSyncDAO.instance.get().getNonDeletableGuidsList(EmployeePost.class, entityIdSet);

        // Если список неудаляемых объектов не пуст, то возвращаем в НСИ ошибку, не выполняя удаления других объектов
        if (!nonDeletableGuids.isEmpty())
        {
            StringBuilder errGuids = new StringBuilder();
            for (String guid : nonDeletableGuids)
            {
                errGuids.append(errGuids.length() > 0 ? ", " : "").append(guid);
            }

            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Deleting requested list (" + itemsToDelete.size() + " items) for " + EmployeePost.ENTITY_NAME + " catalog was failed. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG FROM OB WAS FINISHED ==========");

            throw new NSIProcessingErrorException("One, or more entities can not be deleted, because of other objects relations with the entities have to be deleted. " +
                    "Please, check dependencies for the objects " + CommonBaseStringUtil.joinNotEmpty(nonDeletableGuids, ", ") + ".");
        }

        // Подготавливаем список объектов реально обработанных и GUID'ов, не найденных в ОБ
        List<String> strangeGuids = new ArrayList<>();
        List<Object> deletedObjectsList = new ArrayList<>();
        for (INsiEntity nsiEntity : itemsToDelete)
        {
            if (null != NsiObjectsHolder.getEmployeePostEntityToGuidPair(nsiEntity.getID()))
            {
                deletedObjectsList.add(getCatalogElementRetrieveRequestObject(nsiEntity.getID()));
            } else strangeGuids.add(nsiEntity.getID());
        }

        // Удаляем удалябельные объекты и связанные с ними идентификаторы НСИ
        IFefuNsiSyncDAO.instance.get().deleteObjectsSetCheckedForDeletability(EmployeePost.class, entityIdSet, true);
        IFefuNsiSyncDAO.instance.get().deleteRelatedNsiId(entityIdSet);

        for (Long id : entityIdSet)
        {
            CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> pair = NsiObjectsHolder.getEmployeePostIdToEntityMap().get(id.toString());
            if (null != pair)
            {
                if (null != pair.getX())
                    NsiObjectsHolder.getEmployeePostIdToEntityMap().remove(pair.getX().getId().toString());
                if (null != pair.getY()) NsiObjectsHolder.getEmployeePostIdToEntityMap().remove(pair.getY().getGuid());
            }
        }

        // Выдаём предупреждение, если среди переданных GUID'ов имеются не найденные в ОБ. Удалять нечего.
        if (!strangeGuids.isEmpty())
        {
            FefuNsiSyncDAO.logEvent(Level.WARN, "---------- Deleting requested list (" + deletedObjectsList.size() + " items) for " + EmployeePost.ENTITY_NAME + " catalog was executed with warnings ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG FROM OB WAS FINISHED ==========");
            throw new NSIProcessingWarnException("One, or more entities were not found at OB by given GUID's, so they were ignored: " + CommonBaseStringUtil.joinNotEmpty(strangeGuids, ", "), deletedObjectsList);
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Deleting requested elements list (" + deletedObjectsList.size() + " items) for " + EmployeePost.ENTITY_NAME + " catalog was executed successfully ----------");
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + EmployeePost.ENTITY_NAME.toUpperCase() + " CATALOG FROM OB WAS FINISHED ==========");

        return deletedObjectsList;
    }
}