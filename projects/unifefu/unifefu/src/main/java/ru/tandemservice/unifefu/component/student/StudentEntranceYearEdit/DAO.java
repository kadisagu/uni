/* $Id$ */
package ru.tandemservice.unifefu.component.student.StudentEntranceYearEdit;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.entity.gen.StudentFefuExtGen;

/**
 * @author Alexey Lopatin
 * @since 03.11.2013
 */
public class DAO extends ru.tandemservice.uni.component.student.StudentEntranceYearEdit.DAO
{
    @Override
    public void prepare(ru.tandemservice.uni.component.student.StudentEntranceYearEdit.Model model)
    {
        super.prepare(model);
        Student student = model.getStudent();
        StudentFefuExt studentFefuExt = getByNaturalId(new StudentFefuExtGen.NaturalId(student));
        ((Model) model).setStudentFefuExt(studentFefuExt);
    }
}
