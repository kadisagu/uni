/* $Id$ */
package ru.tandemservice.unifefu.component.menu.OnlineEntrantList;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author Nikolay Fedorovskih
 * @since 01.07.2013
 */
public class Model extends ru.tandemservice.uniec.component.menu.OnlineEntrantList.Model
{
    private ISelectModel yesNoSelect;

    public ISelectModel getYesNoSelect()
    {
        return yesNoSelect;
    }

    public void setYesNoSelect(ISelectModel yesNoSelect)
    {
        this.yesNoSelect = yesNoSelect;
    }
}