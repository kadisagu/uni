/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.MergePersonsAndUpdateGuids;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Dmitry Seleznev
 * @since 30.05.2014
 */
@Configuration
public class FefuSystemActionMergePersonsAndUpdateGuids extends BusinessComponentManager
{
}