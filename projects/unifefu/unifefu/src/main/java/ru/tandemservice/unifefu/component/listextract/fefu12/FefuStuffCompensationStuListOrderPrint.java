/*$Id$*/
package ru.tandemservice.unifefu.component.listextract.fefu12;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e34.utils.FinAidAssignExtractWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e86.utils.EmployeePostDecl;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.component.listextract.fefu12.utils.FefuStuffCompensationExtractWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu12.utils.FefuStuffCompensationParagraphPartWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu12.utils.FefuStuffCompensationParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public class FefuStuffCompensationStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{

    public static final String TEMPLATE_PARAGRAPH_CODE = StudentExtractTypeCodes.FEFU_STUFF_COMPENSATION_LIST_EXTRACT;

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);
        List<FefuStuffCompensationStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        FefuStuffCompensationStuListExtract extract = extracts.get(0);
        List<FefuStuffCompensationParagraphWrapper> paragraphWrapperList = prepareParagraphsStructure(extracts);
        injectModifier.put("protocol", String.format("%s года № %s", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(extract.getProtocolDate()), extract.getProtocolNumber()));
        injectModifier.put("responsibleForPayments", StringUtils.capitalize(EmployeePostDecl.getEmployeeStrDat(extract.getResponsiblePerson())));
        injectModifier.put("responsibleForAgreement", EmployeePostDecl.getEmployeeStrInst(extract.getMatchingPerson()));
        injectModifier.put("matchingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getMatchingDate()));
        int num = paragraphWrapperList.size();
        injectModifier.put("numberPrevEnd", String.valueOf(++num));
        injectModifier.put("numberEnd", String.valueOf(++num));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, paragraphWrapperList);
        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    private List<FefuStuffCompensationParagraphWrapper> prepareParagraphsStructure(List<FefuStuffCompensationStuListExtract> extracts)
    {
        List<FefuStuffCompensationParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        int ind;
        for (FefuStuffCompensationStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = student.getPerson();

            Group group = student.getGroup();
            EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();
            OrgUnit orgUnit = student.getEducationOrgUnit().getFormativeOrgUnit();

            FefuStuffCompensationParagraphWrapper paragraphWrapper = new FefuStuffCompensationParagraphWrapper(extract.getCompensationType(), educationOrgUnit.getDevelopForm(), educationOrgUnit.getDevelopCondition(), educationOrgUnit.getDevelopTech(), educationOrgUnit.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(group), extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            FefuStuffCompensationParagraphPartWrapper paragraphPartWrapper = new FefuStuffCompensationParagraphPartWrapper(orgUnit, group, extract);

            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);

            paragraphPartWrapper.getExtractWrapperList().add(new FefuStuffCompensationExtractWrapper(person, group, extract.getCompensationSumAsDouble(), extract.getImmediateSumAsDouble()));
        }
        return paragraphWrapperList;
    }

    private void injectParagraphs(RtfDocument document, List<FefuStuffCompensationParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            int parNumber = 0;
            for (FefuStuffCompensationParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, TEMPLATE_PARAGRAPH_CODE), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());

                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, paragraphWrapper.getDevelopForm(), "");

                CompensationType compensationType = paragraphWrapper.getCompensationType();

                if (compensationType != null)
                {
                    CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, compensationType, "", false);
                    if (compensationType.isBudget())
                    {
                        // из стипендиального фонда
                        paragraphInjectModifier.put("paymentSource", "из стипендиального фонда");
                    }
                    else
                    {
                        //из внебюджетных средств школы
                        paragraphInjectModifier.put("paymentSource", "из внебюджетных средств школы");
                    }
                }
                else
                {
                    paragraphInjectModifier.put("fefuCompensationTypeStr", "");
                    paragraphInjectModifier.put("paymentSource", "");
                }

                paragraphInjectModifier.put("parNumber", String.valueOf(++parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), paragraphWrapper.getEduBaseText(), "fefuShortFastExtendedOptionalText");

                paragraphInjectModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    private void injectSubParagraphs(RtfDocument paragraph, List<FefuStuffCompensationParagraphPartWrapper> paragraphPartWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            for (FefuStuffCompensationParagraphPartWrapper paragraphPartWrapper : paragraphPartWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, TEMPLATE_PARAGRAPH_CODE), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, paragraphPartWrapper.getFirstExtract());

                paragraphPartInjectModifier.put("orgUnitTitle", paragraphPartWrapper.getOrgUnit().getTitle());
                paragraphPartInjectModifier.modify(paragraphPart);

                List<FinAidAssignExtractWrapper> extractWrapperList = paragraphPartWrapper.getExtractWrapperList();

                Collections.sort(extractWrapperList);

                int j = 0;
                String[][] tableData = new String[extractWrapperList.size()][];
                for (FinAidAssignExtractWrapper extractWrapper : extractWrapperList)
                {
                    tableData[j++] = new String[]{
                            String.valueOf(j) + ".",
                            extractWrapper.getPerson().getFullFio(),
                            extractWrapper.getGroup().getTitle(),
                            DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(extractWrapper.getGrantSize()),
                            DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(extractWrapper instanceof FefuStuffCompensationExtractWrapper ? ((FefuStuffCompensationExtractWrapper)extractWrapper).getImmediateSum() : null)
                    };
                }

                RtfTableModifier paragraphPartTableModifier = new RtfTableModifier();
                paragraphPartTableModifier.put("STUDENTS_TABLE", tableData);
                paragraphPartTableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}
