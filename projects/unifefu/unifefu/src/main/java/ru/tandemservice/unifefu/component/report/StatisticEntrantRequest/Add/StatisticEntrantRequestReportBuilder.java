package ru.tandemservice.unifefu.component.report.StatisticEntrantRequest.Add;

import org.apache.commons.collections15.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.component.report.FefuReportUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author amakarova
 * @since 13.06.2013
 * Отчет "Статистика поданных заявлений"
 */
public class StatisticEntrantRequestReportBuilder
{
    private Model model;
    private Session session;
    private Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> directionMap = new HashMap<>();
    private Map<EnrollmentDirection, Map<EntrantRequest, List<RequestedEnrollmentDirection>>> groupMap = new HashMap<>();
    private Map<CompetitionGroup, Map<EntrantRequest, List<RequestedEnrollmentDirection>>> competitionMap = new HashMap<>();
    public StatisticEntrantRequestReportBuilder(Model model, Session session)
    {
        this.model = model;
        this.session = session;
    }

    public DatabaseFile getContent()
    {
        DatabaseFile content = new DatabaseFile();
        content.setContent(print());
        return content;
    }

    private byte[] print()
    {
        IUniBaseDao coreDao = UniDaoFacade.getCoreDao();

        IScriptItem templateDocument = coreDao.getCatalogItem(UniecScriptItem.class, UniFefuDefines.TEMPLATE_STATISTIC_ENTRANT_REQUEST);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument document = template.getClone();
        RtfTableModifier tableModifier;
        Calendar lastDate = Calendar.getInstance();
        lastDate.setTime(model.getReport().getDateTo());
        Map<OrgUnit, Map<EducationLevelsHighSchool, List<EnrollmentDirection>>> ouMap = new HashMap<>();
        for (EnrollmentDirection direction : getEnrollmentDirectionBuilder().createStatement(session).<EnrollmentDirection>list()) {
            SafeMap.safeGet(SafeMap.safeGet(ouMap, direction.getEducationOrgUnit().getFormativeOrgUnit(), HashMap.class), direction.getEducationOrgUnit().getEducationLevelHighSchool(), ArrayList.class).add(direction);
        }

        Map<Entrant, List<RequestedEnrollmentDirection>> entrantListMap = new HashMap<>();
        DQLSelectBuilder dqlBuilder = getRequestedBuilder();
        Map<OrgUnit, Map<CompetitionGroup, List<EnrollmentDirection>>> ouMapGroup = new HashMap<>();

        if (!model.isByCompetitionGroup()) {
            dqlBuilder.order(DQLExpressions.property(RequestedEnrollmentDirection.priority().fromAlias("d")), OrderDirection.asc);
            for (RequestedEnrollmentDirection reqDirection : dqlBuilder.column("d").createStatement(session).<RequestedEnrollmentDirection>list()) {
                EnrollmentDirection direction = reqDirection.getEnrollmentDirection();
                SafeMap.safeGet(directionMap, direction, ArrayList.class).add(reqDirection);
                SafeMap.safeGet(entrantListMap, reqDirection.getEntrantRequest().getEntrant(), ArrayList.class).add(reqDirection);
                SafeMap.safeGet(SafeMap.safeGet(groupMap, direction, HashMap.class), reqDirection.getEntrantRequest(), ArrayList.class).add(reqDirection);
                SafeMap.safeGet(SafeMap.safeGet(competitionMap, direction.getCompetitionGroup(), HashMap.class), reqDirection.getEntrantRequest(), ArrayList.class).add(reqDirection);
            }
        } else {
            for (EnrollmentDirection direction : getEnrollmentDirectionBuilder().createStatement(session).<EnrollmentDirection>list()) {
                if (direction.getCompetitionGroup() != null) {
                    SafeMap.safeGet(SafeMap.safeGet(ouMapGroup, direction.getEducationOrgUnit().getFormativeOrgUnit(), HashMap.class), direction.getCompetitionGroup(), ArrayList.class).add(direction);
                }
            }
            dqlBuilder.order(DQLExpressions.property(RequestedEnrollmentDirection.priority().fromAlias("d")), OrderDirection.asc);
            for (RequestedEnrollmentDirection reqDirection : dqlBuilder.column("d").createStatement(session).<RequestedEnrollmentDirection>list()) {
                EnrollmentDirection direction = reqDirection.getEnrollmentDirection();
                SafeMap.safeGet(SafeMap.safeGet(competitionMap, direction.getCompetitionGroup(), HashMap.class), reqDirection.getEntrantRequest(), ArrayList.class).add(reqDirection);
            }
        }

        String titleQualif = "";
        if (model.isQualificationActive()) {
            titleQualif = " (" + UniStringUtils.join(model.getQualificationList(), "title", ", ") + ")";
        }

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("titleQualif", titleQualif);
        injectModifier.modify(document);

        List<OrgUnit> formativeOuList = new ArrayList<>(ouMap.keySet());
        Collections.sort(formativeOuList, ITitled.TITLED_COMPARATOR);

        List<String[]> tableData = new ArrayList<>();
        final int grayColorIndex = document.getHeader().getColorTable().addColor(217, 217, 217);
        final Set<Integer> boldHeaderRowIndexes = new HashSet<>();
        int rowIndex = 0;
        int allCount = 0;
        int allOrig = 0;
        int allPlan = 0;
        boolean isBudget = model.getCompensationType().isBudget();

        for (OrgUnit ou : formativeOuList) {
            int ouCount = 0;
            int ouPlan = 0;
            int ouOrig = 0;
            String ouCompetition = "-";
            if (!model.isByCompetitionGroup()) {
                Map<EducationLevelsHighSchool, List<EnrollmentDirection>> hsMap = ouMap.get(ou);
                List<EducationLevelsHighSchool> hsList = new ArrayList<>(hsMap.keySet());
                Collections.sort(hsList, ITitled.TITLED_COMPARATOR);
                boldHeaderRowIndexes.add(rowIndex++);
                List<String[]> hsRows = new ArrayList<>();
                for (EducationLevelsHighSchool hs : hsList) {
                    int hsReqCount = 0;
                    int hsPlan = 0;
                    int hsOrig = 0;
                    List<EnrollmentDirection> enrDirections = new ArrayList<>(hsMap.get(hs));
                    for (EnrollmentDirection enrDir : enrDirections) {
                        List<RequestedEnrollmentDirection> directions = directionMap.get(enrDir);
                        Integer rowPlan = isBudget ? FefuReportUtil.getBudgetPlanWithoutTarget(enrDir) : FefuReportUtil.getContractPlanWithoutTarget(enrDir);
                        hsReqCount += groupMap.get(enrDir) != null ? groupMap.get(enrDir).size() : 0;
                        hsPlan += rowPlan == null ? 0 : rowPlan;
                        CompetitionGroup competitionGroup = enrDir.getCompetitionGroup();
                        if (competitionGroup != null) {
                            hsOrig += countOrig(enrDir);
                        } else {
                            hsOrig  += directions == null ? 0 : CollectionUtils.countMatches(directions, RequestedEnrollmentDirectionGen::isOriginalDocumentHandedIn);
                        }
                    }
                    String sOrig = model.isIncludeSumEntrantOriginal() ? " (" + String.valueOf(hsOrig) + ")" : "";
                    String competition = "-";
                    if (hsPlan != 0) {
                        competition = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format((float) hsReqCount / hsPlan);
                    }
                    rowIndex++;
                    hsRows.add(new String[] {"   " + hs.getTitle(), String.valueOf(hsReqCount), String.valueOf(hsPlan), competition, sOrig});
                    ouCount += hsReqCount;
                    ouPlan += hsPlan;
                    ouOrig += hsOrig;
                }
                String sOuOrig = model.isIncludeSumEntrantOriginal() ? " (" + String.valueOf(ouOrig) + ")" : "";
                if (ouPlan != 0) {
                    ouCompetition = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format((float) ouCount/ouPlan);
                }
                tableData.add(new String[]{ou.getPrintTitle(), String.valueOf(ouCount), String.valueOf(ouPlan), ouCompetition, sOuOrig});
                tableData.addAll(hsRows);

            } else {
                // по КГ
                Map<CompetitionGroup, List<EnrollmentDirection>> compGroupMap = ouMapGroup.get(ou);
                List<CompetitionGroup> groupList = new ArrayList<>(compGroupMap.keySet());
                Collections.sort(groupList, ITitled.TITLED_COMPARATOR);
                boldHeaderRowIndexes.add(rowIndex++);
                List<String[]> groupRows = new ArrayList<>();
                for (CompetitionGroup competitionGroup : groupList) {
                    Integer entrantInCGCount = competitionMap.get(competitionGroup) != null ? competitionMap.get(competitionGroup).keySet().size() : 0;
                    int CGPlan = 0;
                    int CGOrig = 0;
                    List<EnrollmentDirection> enrDirections = new ArrayList<>(compGroupMap.get(competitionGroup));
                    for (EnrollmentDirection enrDir : enrDirections) {
                        Integer rowPlan = isBudget ? FefuReportUtil.getBudgetPlanWithoutTarget(enrDir) : FefuReportUtil.getContractPlanWithoutTarget(enrDir);
                        CGPlan += rowPlan == null ? 0 : rowPlan;
                        if (competitionGroup != null) {
                            CGOrig += countOrig(enrDir);
                        }
                    }
                    String sOrig = model.isIncludeSumEntrantOriginal() ? " (" + String.valueOf(CGOrig) + ")" : "";
                    String competition = "-";
                    if (CGPlan != 0) {
                        competition = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format((float) entrantInCGCount / CGPlan);
                    }
                    rowIndex++;
                    groupRows.add(new String[] {"   " + competitionGroup.getTitle(), String.valueOf(entrantInCGCount), String.valueOf(CGPlan), competition, sOrig});
                    ouCount += entrantInCGCount;
                    ouPlan += CGPlan;
                    ouOrig += CGOrig;
                }
                String sOuOrig = model.isIncludeSumEntrantOriginal() ? " (" + String.valueOf(ouOrig) + ")" : "";
                if (ouPlan != 0) {
                    ouCompetition = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format((float) ouCount/ouPlan);
                }
                tableData.add(new String[]{ou.getPrintTitle(), String.valueOf(ouCount), String.valueOf(ouPlan), ouCompetition, sOuOrig});
                tableData.addAll(groupRows);
            }

            allCount += ouCount;
            allOrig += ouOrig;
            allPlan += ouPlan;
        }
        tableModifier = new RtfTableModifier().put("T", tableData.toArray(new String[tableData.size()][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (boldHeaderRowIndexes.contains(rowIndex))
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (RtfRow row : newRowList) {
                    SharedRtfUtil.setCellAlignment(row.getCellList().get(0), IRtfData.QL);
                    for (int i = 1; i < row.getCellList().size(); i++)
                        SharedRtfUtil.setCellAlignment(row.getCellList().get(i), IRtfData.QR);
                }

                for (Integer rowIndex : boldHeaderRowIndexes) {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    for (RtfCell cell : row.getCellList()) {
                        cell.setBackgroundColorIndex(grayColorIndex);
                    }
                }
            }
        });

        int countEntrant = getCountEntrant();
        String[] titleData = new String[3];
        titleData[0] = DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReport().getDateTo());
        titleData[1] = "Абитуриентов всего: " + String.valueOf(countEntrant);
        String orig = "";
        if (model.isIncludeSumEntrantOriginal())
            orig = "  Оригиналов: " + String.valueOf(allOrig);
        titleData[2] = "Подано заявлений всего: " + String.valueOf(allCount) + orig;
        tableModifier.put("title", new String[][]{titleData});
        tableModifier.modify(document);
        String[] summData = new String[4];
        summData[0] = "ИТОГО";
        summData[1] = String.valueOf(allCount);
        summData[2] = String.valueOf(allPlan);
        String allCompetition = "-";
        if (allPlan != 0) {
            allCompetition = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format((float) allCount/allPlan);
        }
        summData[3] = allCompetition;
        tableModifier.put("T1", new String[][]{summData});
        tableModifier.put("T1", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (RtfRow row : newRowList) {
                    SharedRtfUtil.setCellAlignment(row.getCellList().get(0), IRtfData.QL);
                    for (int i = 1; i < row.getCellList().size(); i++)
                        SharedRtfUtil.setCellAlignment(row.getCellList().get(i), IRtfData.QR);
                }
            }
        });
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private DQLSelectBuilder getEnrollmentDirectionBuilder()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "d")
                .where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), value(model.getEnrollmentCampaign())))
                .where(or(
                eq(property(EnrollmentDirection.budget().fromAlias("d")), value(Boolean.TRUE)),
                eq(property(EnrollmentDirection.contract().fromAlias("d")), value(Boolean.TRUE))))
                .column("d");

        dql.joinPath(DQLJoinType.inner, "d." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        dql.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        dql.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");

        patchOU(dql);

        return dql;
    }

    private DQLSelectBuilder getRequestedBuilder()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(RequestedEnrollmentDirection.class, "d");
        dql.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        dql.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        dql.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct");
        dql.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        dql.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        dql.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        dql.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");

        dql.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.P_ARCHIVAL), DQLExpressions.value(Boolean.FALSE)));
        dql.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), DQLExpressions.value(model.getReport().getEnrollmentCampaign())));
        dql.where(DQLExpressions.betweenDays("request." + EntrantRequest.P_REG_DATE, model.getReport().getDateFrom(), model.getReport().getDateTo()));

        if (model.getCompensationType() != null)
            dql.where(DQLExpressions.eq(DQLExpressions.property("ct." + CompensationType.P_ID), DQLExpressions.value(model.getCompensationType().getId())));
        if (model.isStudentCategoryActive())
            dql.where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.studentCategory().fromAlias("d")), model.getStudentCategoryList()));
        if (!model.isIncludeForeignPerson())
            dql.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.person().identityCard().citizenship().code()), DQLExpressions.value(IKladrDefines.RUSSIA_COUNTRY_CODE)));

        patchOU(dql);
        return dql;
    }

    private void patchOU(DQLSelectBuilder dql)
    {
        if (model.isQualificationActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("el." + EducationLevels.L_QUALIFICATION), model.getQualificationList()));
        if (model.isFormativeOrgUnitActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT), model.getFormativeOrgUnitList()));
        if (model.isTerritorialOrgUnitActive())
        {
            if (model.getTerritorialOrgUnitList().isEmpty())
                dql.where(DQLExpressions.isNull(DQLExpressions.property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)));
            else
                dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT), model.getTerritorialOrgUnitList()));
        }
        if (model.isEducationLevelHighSchoolActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL), model.getEducationLevelHighSchoolList()));
        if (model.isDevelopFormActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_FORM), model.getDevelopFormList()));
        if (model.isDevelopConditionActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), model.getDevelopConditionList()));
        if (model.isDevelopTechActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_TECH), model.getDevelopTechList()));
        if (model.isDevelopPeriodActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_PERIOD), model.getDevelopPeriodList()));
    }

    private Integer getCountEntrant() {
        DQLSelectBuilder builder = getRequestedBuilder();
        builder.predicate(DQLPredicateType.distinct);
        builder.column(property("entrant.id"));
        return builder.createStatement(session).list().size();
    }

    private int countOrig(EnrollmentDirection direction) {
        int countOrigAll = 0;
        if (direction.getCompetitionGroup() != null) {
            Map<EntrantRequest, List<RequestedEnrollmentDirection>> requests = competitionMap.get(direction.getCompetitionGroup());
            if (requests == null)
                return 0;
            for (EntrantRequest er : requests.keySet()) {
                RequestedEnrollmentDirection red = requests.get(er).get(0);
                if (red.getEnrollmentDirection().equals(direction) && red.isOriginalDocumentHandedIn()) {
                    countOrigAll ++;
                }
            }
        }
        return countOrigAll;
    }
}
