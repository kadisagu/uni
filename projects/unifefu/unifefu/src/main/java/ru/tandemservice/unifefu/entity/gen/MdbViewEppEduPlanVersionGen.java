package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Версия учебного плана
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewEppEduPlanVersionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion";
    public static final String ENTITY_NAME = "mdbViewEppEduPlanVersion";
    public static final int VERSION_HASH = -36621561;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPP_EDU_PLAN_VERSION = "eppEduPlanVersion";
    public static final String L_EDUCATION_LEVELS = "educationLevels";
    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";
    public static final String P_STD_NUMBER = "stdNumber";
    public static final String P_PLAN_DEVELOP_FORM = "planDevelopForm";
    public static final String P_PLAN_DEVELOP_COND = "planDevelopCond";
    public static final String P_PLAN_DEVELOP_TECH = "planDevelopTech";
    public static final String P_PLAN_DEVELOP_GRID = "planDevelopGrid";
    public static final String P_PLAN_NUMBER = "planNumber";
    public static final String P_VERSION_NUMBER = "versionNumber";
    public static final String P_VERSION_TITLE = "versionTitle";

    private EppEduPlanVersion _eppEduPlanVersion;     // Версия учебного плана
    private EducationLevels _educationLevels;     // Направление подготовки (специальность) (базов.)
    private EducationLevelsHighSchool _educationLevelsHighSchool;     // Направление подготовки
    private String _stdNumber; 
    private String _planDevelopForm;     // Форма освоения
    private String _planDevelopCond;     // Технология освоения
    private String _planDevelopTech;     // Условие освоения
    private String _planDevelopGrid;     // Срок освоения
    private String _planNumber;     // Учебный план - номер
    private String _versionNumber;     // Версия учебного плана - Номер
    private String _versionTitle;     // Постфикс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEppEduPlanVersion()
    {
        return _eppEduPlanVersion;
    }

    /**
     * @param eppEduPlanVersion Версия учебного плана. Свойство не может быть null.
     */
    public void setEppEduPlanVersion(EppEduPlanVersion eppEduPlanVersion)
    {
        dirty(_eppEduPlanVersion, eppEduPlanVersion);
        _eppEduPlanVersion = eppEduPlanVersion;
    }

    /**
     * @return Направление подготовки (специальность) (базов.). Свойство не может быть null.
     */
    @NotNull
    public EducationLevels getEducationLevels()
    {
        return _educationLevels;
    }

    /**
     * @param educationLevels Направление подготовки (специальность) (базов.). Свойство не может быть null.
     */
    public void setEducationLevels(EducationLevels educationLevels)
    {
        dirty(_educationLevels, educationLevels);
        _educationLevels = educationLevels;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    /**
     * @param educationLevelsHighSchool Направление подготовки. Свойство не может быть null.
     */
    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        dirty(_educationLevelsHighSchool, educationLevelsHighSchool);
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getStdNumber()
    {
        return _stdNumber;
    }

    /**
     * @param stdNumber 
     */
    public void setStdNumber(String stdNumber)
    {
        dirty(_stdNumber, stdNumber);
        _stdNumber = stdNumber;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPlanDevelopForm()
    {
        return _planDevelopForm;
    }

    /**
     * @param planDevelopForm Форма освоения. Свойство не может быть null.
     */
    public void setPlanDevelopForm(String planDevelopForm)
    {
        dirty(_planDevelopForm, planDevelopForm);
        _planDevelopForm = planDevelopForm;
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPlanDevelopCond()
    {
        return _planDevelopCond;
    }

    /**
     * @param planDevelopCond Технология освоения. Свойство не может быть null.
     */
    public void setPlanDevelopCond(String planDevelopCond)
    {
        dirty(_planDevelopCond, planDevelopCond);
        _planDevelopCond = planDevelopCond;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPlanDevelopTech()
    {
        return _planDevelopTech;
    }

    /**
     * @param planDevelopTech Условие освоения. Свойство не может быть null.
     */
    public void setPlanDevelopTech(String planDevelopTech)
    {
        dirty(_planDevelopTech, planDevelopTech);
        _planDevelopTech = planDevelopTech;
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPlanDevelopGrid()
    {
        return _planDevelopGrid;
    }

    /**
     * @param planDevelopGrid Срок освоения. Свойство не может быть null.
     */
    public void setPlanDevelopGrid(String planDevelopGrid)
    {
        dirty(_planDevelopGrid, planDevelopGrid);
        _planDevelopGrid = planDevelopGrid;
    }

    /**
     * @return Учебный план - номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPlanNumber()
    {
        return _planNumber;
    }

    /**
     * @param planNumber Учебный план - номер. Свойство не может быть null.
     */
    public void setPlanNumber(String planNumber)
    {
        dirty(_planNumber, planNumber);
        _planNumber = planNumber;
    }

    /**
     * @return Версия учебного плана - Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getVersionNumber()
    {
        return _versionNumber;
    }

    /**
     * @param versionNumber Версия учебного плана - Номер. Свойство не может быть null.
     */
    public void setVersionNumber(String versionNumber)
    {
        dirty(_versionNumber, versionNumber);
        _versionNumber = versionNumber;
    }

    /**
     * @return Постфикс.
     */
    @Length(max=255)
    public String getVersionTitle()
    {
        return _versionTitle;
    }

    /**
     * @param versionTitle Постфикс.
     */
    public void setVersionTitle(String versionTitle)
    {
        dirty(_versionTitle, versionTitle);
        _versionTitle = versionTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewEppEduPlanVersionGen)
        {
            setEppEduPlanVersion(((MdbViewEppEduPlanVersion)another).getEppEduPlanVersion());
            setEducationLevels(((MdbViewEppEduPlanVersion)another).getEducationLevels());
            setEducationLevelsHighSchool(((MdbViewEppEduPlanVersion)another).getEducationLevelsHighSchool());
            setStdNumber(((MdbViewEppEduPlanVersion)another).getStdNumber());
            setPlanDevelopForm(((MdbViewEppEduPlanVersion)another).getPlanDevelopForm());
            setPlanDevelopCond(((MdbViewEppEduPlanVersion)another).getPlanDevelopCond());
            setPlanDevelopTech(((MdbViewEppEduPlanVersion)another).getPlanDevelopTech());
            setPlanDevelopGrid(((MdbViewEppEduPlanVersion)another).getPlanDevelopGrid());
            setPlanNumber(((MdbViewEppEduPlanVersion)another).getPlanNumber());
            setVersionNumber(((MdbViewEppEduPlanVersion)another).getVersionNumber());
            setVersionTitle(((MdbViewEppEduPlanVersion)another).getVersionTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewEppEduPlanVersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewEppEduPlanVersion.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewEppEduPlanVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eppEduPlanVersion":
                    return obj.getEppEduPlanVersion();
                case "educationLevels":
                    return obj.getEducationLevels();
                case "educationLevelsHighSchool":
                    return obj.getEducationLevelsHighSchool();
                case "stdNumber":
                    return obj.getStdNumber();
                case "planDevelopForm":
                    return obj.getPlanDevelopForm();
                case "planDevelopCond":
                    return obj.getPlanDevelopCond();
                case "planDevelopTech":
                    return obj.getPlanDevelopTech();
                case "planDevelopGrid":
                    return obj.getPlanDevelopGrid();
                case "planNumber":
                    return obj.getPlanNumber();
                case "versionNumber":
                    return obj.getVersionNumber();
                case "versionTitle":
                    return obj.getVersionTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eppEduPlanVersion":
                    obj.setEppEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "educationLevels":
                    obj.setEducationLevels((EducationLevels) value);
                    return;
                case "educationLevelsHighSchool":
                    obj.setEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
                case "stdNumber":
                    obj.setStdNumber((String) value);
                    return;
                case "planDevelopForm":
                    obj.setPlanDevelopForm((String) value);
                    return;
                case "planDevelopCond":
                    obj.setPlanDevelopCond((String) value);
                    return;
                case "planDevelopTech":
                    obj.setPlanDevelopTech((String) value);
                    return;
                case "planDevelopGrid":
                    obj.setPlanDevelopGrid((String) value);
                    return;
                case "planNumber":
                    obj.setPlanNumber((String) value);
                    return;
                case "versionNumber":
                    obj.setVersionNumber((String) value);
                    return;
                case "versionTitle":
                    obj.setVersionTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eppEduPlanVersion":
                        return true;
                case "educationLevels":
                        return true;
                case "educationLevelsHighSchool":
                        return true;
                case "stdNumber":
                        return true;
                case "planDevelopForm":
                        return true;
                case "planDevelopCond":
                        return true;
                case "planDevelopTech":
                        return true;
                case "planDevelopGrid":
                        return true;
                case "planNumber":
                        return true;
                case "versionNumber":
                        return true;
                case "versionTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eppEduPlanVersion":
                    return true;
                case "educationLevels":
                    return true;
                case "educationLevelsHighSchool":
                    return true;
                case "stdNumber":
                    return true;
                case "planDevelopForm":
                    return true;
                case "planDevelopCond":
                    return true;
                case "planDevelopTech":
                    return true;
                case "planDevelopGrid":
                    return true;
                case "planNumber":
                    return true;
                case "versionNumber":
                    return true;
                case "versionTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eppEduPlanVersion":
                    return EppEduPlanVersion.class;
                case "educationLevels":
                    return EducationLevels.class;
                case "educationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
                case "stdNumber":
                    return String.class;
                case "planDevelopForm":
                    return String.class;
                case "planDevelopCond":
                    return String.class;
                case "planDevelopTech":
                    return String.class;
                case "planDevelopGrid":
                    return String.class;
                case "planNumber":
                    return String.class;
                case "versionNumber":
                    return String.class;
                case "versionTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewEppEduPlanVersion> _dslPath = new Path<MdbViewEppEduPlanVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewEppEduPlanVersion");
    }
            

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getEppEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eppEduPlanVersion()
    {
        return _dslPath.eppEduPlanVersion();
    }

    /**
     * @return Направление подготовки (специальность) (базов.). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getEducationLevels()
     */
    public static EducationLevels.Path<EducationLevels> educationLevels()
    {
        return _dslPath.educationLevels();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
    {
        return _dslPath.educationLevelsHighSchool();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getStdNumber()
     */
    public static PropertyPath<String> stdNumber()
    {
        return _dslPath.stdNumber();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getPlanDevelopForm()
     */
    public static PropertyPath<String> planDevelopForm()
    {
        return _dslPath.planDevelopForm();
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getPlanDevelopCond()
     */
    public static PropertyPath<String> planDevelopCond()
    {
        return _dslPath.planDevelopCond();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getPlanDevelopTech()
     */
    public static PropertyPath<String> planDevelopTech()
    {
        return _dslPath.planDevelopTech();
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getPlanDevelopGrid()
     */
    public static PropertyPath<String> planDevelopGrid()
    {
        return _dslPath.planDevelopGrid();
    }

    /**
     * @return Учебный план - номер. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getPlanNumber()
     */
    public static PropertyPath<String> planNumber()
    {
        return _dslPath.planNumber();
    }

    /**
     * @return Версия учебного плана - Номер. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getVersionNumber()
     */
    public static PropertyPath<String> versionNumber()
    {
        return _dslPath.versionNumber();
    }

    /**
     * @return Постфикс.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getVersionTitle()
     */
    public static PropertyPath<String> versionTitle()
    {
        return _dslPath.versionTitle();
    }

    public static class Path<E extends MdbViewEppEduPlanVersion> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eppEduPlanVersion;
        private EducationLevels.Path<EducationLevels> _educationLevels;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchool;
        private PropertyPath<String> _stdNumber;
        private PropertyPath<String> _planDevelopForm;
        private PropertyPath<String> _planDevelopCond;
        private PropertyPath<String> _planDevelopTech;
        private PropertyPath<String> _planDevelopGrid;
        private PropertyPath<String> _planNumber;
        private PropertyPath<String> _versionNumber;
        private PropertyPath<String> _versionTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getEppEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eppEduPlanVersion()
        {
            if(_eppEduPlanVersion == null )
                _eppEduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EPP_EDU_PLAN_VERSION, this);
            return _eppEduPlanVersion;
        }

    /**
     * @return Направление подготовки (специальность) (базов.). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getEducationLevels()
     */
        public EducationLevels.Path<EducationLevels> educationLevels()
        {
            if(_educationLevels == null )
                _educationLevels = new EducationLevels.Path<EducationLevels>(L_EDUCATION_LEVELS, this);
            return _educationLevels;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
        {
            if(_educationLevelsHighSchool == null )
                _educationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _educationLevelsHighSchool;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getStdNumber()
     */
        public PropertyPath<String> stdNumber()
        {
            if(_stdNumber == null )
                _stdNumber = new PropertyPath<String>(MdbViewEppEduPlanVersionGen.P_STD_NUMBER, this);
            return _stdNumber;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getPlanDevelopForm()
     */
        public PropertyPath<String> planDevelopForm()
        {
            if(_planDevelopForm == null )
                _planDevelopForm = new PropertyPath<String>(MdbViewEppEduPlanVersionGen.P_PLAN_DEVELOP_FORM, this);
            return _planDevelopForm;
        }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getPlanDevelopCond()
     */
        public PropertyPath<String> planDevelopCond()
        {
            if(_planDevelopCond == null )
                _planDevelopCond = new PropertyPath<String>(MdbViewEppEduPlanVersionGen.P_PLAN_DEVELOP_COND, this);
            return _planDevelopCond;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getPlanDevelopTech()
     */
        public PropertyPath<String> planDevelopTech()
        {
            if(_planDevelopTech == null )
                _planDevelopTech = new PropertyPath<String>(MdbViewEppEduPlanVersionGen.P_PLAN_DEVELOP_TECH, this);
            return _planDevelopTech;
        }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getPlanDevelopGrid()
     */
        public PropertyPath<String> planDevelopGrid()
        {
            if(_planDevelopGrid == null )
                _planDevelopGrid = new PropertyPath<String>(MdbViewEppEduPlanVersionGen.P_PLAN_DEVELOP_GRID, this);
            return _planDevelopGrid;
        }

    /**
     * @return Учебный план - номер. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getPlanNumber()
     */
        public PropertyPath<String> planNumber()
        {
            if(_planNumber == null )
                _planNumber = new PropertyPath<String>(MdbViewEppEduPlanVersionGen.P_PLAN_NUMBER, this);
            return _planNumber;
        }

    /**
     * @return Версия учебного плана - Номер. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getVersionNumber()
     */
        public PropertyPath<String> versionNumber()
        {
            if(_versionNumber == null )
                _versionNumber = new PropertyPath<String>(MdbViewEppEduPlanVersionGen.P_VERSION_NUMBER, this);
            return _versionNumber;
        }

    /**
     * @return Постфикс.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion#getVersionTitle()
     */
        public PropertyPath<String> versionTitle()
        {
            if(_versionTitle == null )
                _versionTitle = new PropertyPath<String>(MdbViewEppEduPlanVersionGen.P_VERSION_TITLE, this);
            return _versionTitle;
        }

        public Class getEntityClass()
        {
            return MdbViewEppEduPlanVersion.class;
        }

        public String getEntityName()
        {
            return "mdbViewEppEduPlanVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
