/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu11.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 25.04.2013
 */
public class DAO extends ModularStudentExtractPubDAO<FefuAcadGrantAssignStuEnrolmentExtract, Model>
{
}