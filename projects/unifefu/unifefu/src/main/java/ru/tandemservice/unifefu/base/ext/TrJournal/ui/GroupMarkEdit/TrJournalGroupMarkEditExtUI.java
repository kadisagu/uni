package ru.tandemservice.unifefu.base.ext.TrJournal.ui.GroupMarkEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.unifefu.base.bo.FefuTrJournal.FefuTrJournalManager;
import ru.tandemservice.unifefu.base.bo.FefuTrJournal.ui.StudentMarkEdit.FefuTrJournalStudentMarkEdit;
import ru.tandemservice.unifefu.entity.TrOrgUnitSettingsFefuExt;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unifefu.utils.FefuTrGroupEventDateComparator;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsIRatingValueFormatter;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalMark.TrHomePageJournalMarkUI;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.ITrJournalGroupSortEvents;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditUI;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkColumn;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkColumnGroup;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author amakarova
 */
public class TrJournalGroupMarkEditExtUI extends UIAddon implements ITrJournalGroupSortEvents
{
    public static final String GROUP_BINDING = "groupId";
    public static final String SLOT_BINDING = "studentSlotId";
    final Map<Long, FefuRow> rowMap = new HashMap<>();

    private TrJournalGroupMarkEditUI _presenter;
    private boolean _isShowOnlyActive;
    private boolean _isExam;

    public TrJournalGroupMarkEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        _presenter = getPresenter();
        double allWeight = 0.0d;

        EppFControlActionGroup fControlActionGroup = TrBrsCoefficientManager.instance().coefDao().getFcaGroup(_presenter.getGroup().getJournal());
        _isExam = EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(fControlActionGroup.getCode());

        final IBrsDao.IBrsPreparedSettings brsSettings = TrBrsCoefficientManager.instance().brsDao().getPreparedBrsSettings(_presenter.getGroup().getJournal());

        for (TrJournalMarkColumnGroup group : _presenter.getColumnGroupList())
        {
            for (TrJournalMarkColumn row : group.getColumnList())
            {
                TrBrsCoefficientValue weightSettings = brsSettings.getEventSettings(row.getEvent().getJournalEvent(), FefuBrs.WEIGHT_EVENT_CODE);
                TrBrsCoefficientValue minSettings = brsSettings.getEventSettings(row.getEvent().getJournalEvent(), FefuBrs.MIN_POINTS_CODE);
                TrBrsCoefficientValue maxSettings = brsSettings.getEventSettings(row.getEvent().getJournalEvent(), FefuBrs.MAX_POINTS_CODE);

                Double weightCoefficient = weightSettings != null ? weightSettings.getValueAsDouble() : 0.0d;
                allWeight += weightCoefficient;

                String min = minSettings != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(minSettings.getValueAsDouble()) : "-";
                String max = maxSettings != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(maxSettings.getValueAsDouble()) : "-";

                rowMap.put(row.getEvent().getId(), new FefuRow(weightCoefficient, max, min));
            }
        }

        if (!UniBaseUtils.eq(allWeight, 0.0d))
        {
            allWeight = 100.0d / allWeight;
            for (FefuRow row : rowMap.values())
            {
                double value = row.getWeightCoefficient() * allWeight;
                if (!UniBaseUtils.eq(value, 0.0d))
                    row.setNormWeight(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(value));
            }
        }

        if (!_presenter.isViewModeOnly() && _presenter.getBrsSettings() != null)
        {
            TrOrgUnitSettingsFefuExt orgUnitSettingsFefuExt = DataAccessServices.dao().getByNaturalId(new TrOrgUnitSettingsFefuExt.NaturalId(_presenter.getBrsSettings()));
            _presenter.setViewModeOnly(orgUnitSettingsFefuExt != null && orgUnitSettingsFefuExt.isBlockTutorEdit());
        }
    }

    public String getMarkScale()
    {
        return "Традиционная оценка: " + FefuBrs.getJournalScaleDescription(_isExam, _presenter.getGroup().getJournal());
    }

    public void onClickGoBack()
    {
        _presenter.getSupport().getParentUI().deactivate();
    }

    public boolean isHomePageJournal()
    {
        return _presenter.getSupport().getParentUI().getClass().equals(TrHomePageJournalMarkUI.class);
    }

    public static String getAdditParamStr(ISessionBrsDao.IRatingValue paramValue)
    {
        return (paramValue != null && paramValue.getValue() == null && paramValue.getMessage() != null) ? paramValue.getMessage() : BrsIRatingValueFormatter.instance.format(paramValue);
    }

    public String getAdditParamValue()
    {
        IBrsDao.IStudentCurrentRatingData studentRating = _presenter.getCalculatedRating().getCurrentRating(_presenter.getCurrentRow().getStudent());
        if (studentRating == null || _presenter.getCurrentAdditParam() == null)
            return "";
        return getAdditParamStr(studentRating.getRatingAdditParam(_presenter.getCurrentAdditParam().getKey()));
    }

    public static String getRatingStr(ISessionBrsDao.IRatingValue rating)
    {
        if (rating == null)
            return "-";
        if (rating.getValue() == null)
        {
            if (rating.getMessage() != null)
                return "!";
            return "-";
        }
        return rating.getValue().isNaN() || rating.getValue().isInfinite() ? "0" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(rating.getValue());
    }

    public String getRatingValue()
    {
        return getRatingStr(_presenter.getRowRating());
    }

    public void onClickEditRowStudent()
    {
        getActivationBuilder().asRegionDialog(FefuTrJournalStudentMarkEdit.class)
                .parameter(TrJournalGroupMarkEditExtUI.GROUP_BINDING, _presenter.getGroup().getId())
                .parameter(TrJournalGroupMarkEditExtUI.SLOT_BINDING, getListenerParameterAsLong())
                .activate();
    }

    @Override
    public void sortEvents(List<TrEduGroupEvent> events)
    {
        Collections.sort(events, new FefuTrGroupEventDateComparator());
    }

    public void onClickShowOnlyActiveStudent()
    {
        _isShowOnlyActive = true;
        onComponentRefresh();
    }

    public void onClickShowAllStudent()
    {
        _isShowOnlyActive = false;
        onComponentRefresh();
    }

    public void onClickPrintRatingGroup()
    {
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(FefuTrJournalManager.instance().fefuPrintDao().printRatingGroup(_presenter.getGroup().getId()), "Рейтинг академической группы.rtf");
        getActivationBuilder()
                .asDesktopRoot(IUniComponents.PRINT_REPORT)
                .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "rtf"))
                .activate();
    }

    public void onClickPrintRatingSheetExam()
    {
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(FefuTrJournalManager.instance().fefuPrintDao().printRatingSheet(_presenter.getGroup().getId(), true), "Рейтинговая ведомость на экзамен.rtf");
        getActivationBuilder()
                .asDesktopRoot(IUniComponents.PRINT_REPORT)
                .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "rtf"))
                .activate();
    }

    public void onClickPrintRatingSheetSetoff()
    {
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(FefuTrJournalManager.instance().fefuPrintDao().printRatingSheet(_presenter.getGroup().getId(), false), "Рейтинговая ведомость на зачет.rtf");
        getActivationBuilder()
                .asDesktopRoot(IUniComponents.PRINT_REPORT)
                .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "rtf"))
                .activate();
    }

    public void onClickPrintRatingSheetExamTotal()
    {
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(FefuTrJournalManager.instance().fefuPrintDao().printRatingSheetTotal(_presenter.getGroup().getId(), true), "Итоговая рейтинговая ведомость (экзамен).rtf");
        getActivationBuilder()
                .asDesktopRoot(IUniComponents.PRINT_REPORT)
                .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "rtf"))
                .activate();
    }

    public void onClickPrintRatingSheetSetoffTotal()
    {
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(FefuTrJournalManager.instance().fefuPrintDao().printRatingSheetTotal(_presenter.getGroup().getId(), false), "Итоговая рейтинговая ведомость (зачет).rtf");
        getActivationBuilder()
                .asDesktopRoot(IUniComponents.PRINT_REPORT)
                .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "rtf"))
                .activate();
    }

    public String getColumnParam()
    {
        String all = "";
        if (rowMap.size() > 0)
        {
            FefuRow row = rowMap.get(_presenter.getCurrentColumn().getEvent().getId());
            if (row != null)
            {
                all = row.getValueMin() + "/" + row.getValueMax() + " (" + row.getNormWeight() + "%)";
            }
        }
        return all;
    }

    public String getCurrentRowStyle()
    {
        return _presenter.getCurrentRow().isActive() ? "" : (_isShowOnlyActive ? "display:none" : "font-style:italic; font-weight: bold");
    }

    public boolean isShowOnlyActive()
    {
        return !_isShowOnlyActive;
    }

    public boolean isShowAllStudent()
    {
        return _isShowOnlyActive;
    }

    private class FefuRow
    {
        public FefuRow(Double normWeight, String valueMax, String valueMin)
        {
            _weightCoefficient = normWeight;
            _valueMax = valueMax;
            _valueMin = valueMin;
        }

        public String getNormWeight()
        {
            return _normWeight;
        }

        public String getValueMax()
        {
            return _valueMax;
        }

        public String getValueMin()
        {
            return _valueMin;
        }

        private String _normWeight = "";
        private String _valueMax;
        private String _valueMin;
        private Double _weightCoefficient;

        public Double getWeightCoefficient()
        {
            return _weightCoefficient;
        }

        public void setNormWeight(String normWeight)
        {
            _normWeight = normWeight;
        }
    }

    public boolean getNoExam()
    {
        return !_isExam;
    }

    public boolean getNoSetoff()
    {
        return _isExam;
    }
}