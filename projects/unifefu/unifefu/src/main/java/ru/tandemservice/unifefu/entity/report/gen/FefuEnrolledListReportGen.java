package ru.tandemservice.unifefu.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списки зачисленных
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEnrolledListReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport";
    public static final String ENTITY_NAME = "fefuEnrolledListReport";
    public static final int VERSION_HASH = 1464578931;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_STUDENT_CATEGORY_TITLE = "studentCategoryTitle";
    public static final String P_ORDER_BY_ORIGINALS = "orderByOriginals";
    public static final String P_ONLY_WITH_ORIGINALS = "onlyWithOriginals";
    public static final String P_WITHOUT_DOCUMENT_INFO = "withoutDocumentInfo";
    public static final String P_WITHOUT_DETAIL_SUM_MARK = "withoutDetailSumMark";
    public static final String P_SHOW_DISCIPLINE_TITLES = "showDisciplineTitles";
    public static final String P_SHOW_REQUESTED_DIRECTION_PRIORITY = "showRequestedDirectionPriority";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String P_NOT_PRINT_SPES_WITHOUT_REQUEST = "notPrintSpesWithoutRequest";
    public static final String P_NOT_PRINT_NUM_INFO = "notPrintNumInfo";
    public static final String P_INCLUDE_FOREIGN_PERSON = "includeForeignPerson";
    public static final String P_ENROLLMENT_ORDER_TYPES = "enrollmentOrderTypes";
    public static final String P_ENROLLMENT_ORDERS = "enrollmentOrders";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _studentCategoryTitle;     // Категория поступающего
    private boolean _orderByOriginals;     // Выделить абитуриентов с оригиналами документов
    private boolean _onlyWithOriginals;     // Не включать абитуриентов без оригиналов документов
    private boolean _withoutDocumentInfo;     // Без информации о документах об образовании
    private boolean _withoutDetailSumMark;     // Без расшифровки суммы баллов
    private boolean _showDisciplineTitles;     // Выводить названия дисциплин
    private boolean _showRequestedDirectionPriority;     // Выводить приоритеты
    private String _qualificationTitle;     // Квалификация
    private String _developFormTitle;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private boolean _notPrintSpesWithoutRequest;     // Не печатать направления/специальности, по которым нет заявлений
    private boolean _notPrintNumInfo;     // Не печатать информацию о цифрах приема
    private Boolean _includeForeignPerson;     // Учитывать иностранных граждан
    private String _enrollmentOrderTypes;     // Типы приказа о зачислении
    private String _enrollmentOrders;     // Приказы о зачислении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryTitle()
    {
        return _studentCategoryTitle;
    }

    /**
     * @param studentCategoryTitle Категория поступающего.
     */
    public void setStudentCategoryTitle(String studentCategoryTitle)
    {
        dirty(_studentCategoryTitle, studentCategoryTitle);
        _studentCategoryTitle = studentCategoryTitle;
    }

    /**
     * @return Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isOrderByOriginals()
    {
        return _orderByOriginals;
    }

    /**
     * @param orderByOriginals Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     */
    public void setOrderByOriginals(boolean orderByOriginals)
    {
        dirty(_orderByOriginals, orderByOriginals);
        _orderByOriginals = orderByOriginals;
    }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isOnlyWithOriginals()
    {
        return _onlyWithOriginals;
    }

    /**
     * @param onlyWithOriginals Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     */
    public void setOnlyWithOriginals(boolean onlyWithOriginals)
    {
        dirty(_onlyWithOriginals, onlyWithOriginals);
        _onlyWithOriginals = onlyWithOriginals;
    }

    /**
     * @return Без информации о документах об образовании. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithoutDocumentInfo()
    {
        return _withoutDocumentInfo;
    }

    /**
     * @param withoutDocumentInfo Без информации о документах об образовании. Свойство не может быть null.
     */
    public void setWithoutDocumentInfo(boolean withoutDocumentInfo)
    {
        dirty(_withoutDocumentInfo, withoutDocumentInfo);
        _withoutDocumentInfo = withoutDocumentInfo;
    }

    /**
     * @return Без расшифровки суммы баллов. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithoutDetailSumMark()
    {
        return _withoutDetailSumMark;
    }

    /**
     * @param withoutDetailSumMark Без расшифровки суммы баллов. Свойство не может быть null.
     */
    public void setWithoutDetailSumMark(boolean withoutDetailSumMark)
    {
        dirty(_withoutDetailSumMark, withoutDetailSumMark);
        _withoutDetailSumMark = withoutDetailSumMark;
    }

    /**
     * @return Выводить названия дисциплин. Свойство не может быть null.
     */
    @NotNull
    public boolean isShowDisciplineTitles()
    {
        return _showDisciplineTitles;
    }

    /**
     * @param showDisciplineTitles Выводить названия дисциплин. Свойство не может быть null.
     */
    public void setShowDisciplineTitles(boolean showDisciplineTitles)
    {
        dirty(_showDisciplineTitles, showDisciplineTitles);
        _showDisciplineTitles = showDisciplineTitles;
    }

    /**
     * @return Выводить приоритеты. Свойство не может быть null.
     */
    @NotNull
    public boolean isShowRequestedDirectionPriority()
    {
        return _showRequestedDirectionPriority;
    }

    /**
     * @param showRequestedDirectionPriority Выводить приоритеты. Свойство не может быть null.
     */
    public void setShowRequestedDirectionPriority(boolean showRequestedDirectionPriority)
    {
        dirty(_showRequestedDirectionPriority, showRequestedDirectionPriority);
        _showRequestedDirectionPriority = showRequestedDirectionPriority;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle Квалификация.
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormTitle()
    {
        return _developFormTitle;
    }

    /**
     * @param developFormTitle Форма освоения.
     */
    public void setDevelopFormTitle(String developFormTitle)
    {
        dirty(_developFormTitle, developFormTitle);
        _developFormTitle = developFormTitle;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Направление подготовки (специальность) приема.
     */
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotPrintSpesWithoutRequest()
    {
        return _notPrintSpesWithoutRequest;
    }

    /**
     * @param notPrintSpesWithoutRequest Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     */
    public void setNotPrintSpesWithoutRequest(boolean notPrintSpesWithoutRequest)
    {
        dirty(_notPrintSpesWithoutRequest, notPrintSpesWithoutRequest);
        _notPrintSpesWithoutRequest = notPrintSpesWithoutRequest;
    }

    /**
     * @return Не печатать информацию о цифрах приема. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotPrintNumInfo()
    {
        return _notPrintNumInfo;
    }

    /**
     * @param notPrintNumInfo Не печатать информацию о цифрах приема. Свойство не может быть null.
     */
    public void setNotPrintNumInfo(boolean notPrintNumInfo)
    {
        dirty(_notPrintNumInfo, notPrintNumInfo);
        _notPrintNumInfo = notPrintNumInfo;
    }

    /**
     * @return Учитывать иностранных граждан.
     */
    public Boolean getIncludeForeignPerson()
    {
        return _includeForeignPerson;
    }

    /**
     * @param includeForeignPerson Учитывать иностранных граждан.
     */
    public void setIncludeForeignPerson(Boolean includeForeignPerson)
    {
        dirty(_includeForeignPerson, includeForeignPerson);
        _includeForeignPerson = includeForeignPerson;
    }

    /**
     * @return Типы приказа о зачислении.
     */
    @Length(max=255)
    public String getEnrollmentOrderTypes()
    {
        return _enrollmentOrderTypes;
    }

    /**
     * @param enrollmentOrderTypes Типы приказа о зачислении.
     */
    public void setEnrollmentOrderTypes(String enrollmentOrderTypes)
    {
        dirty(_enrollmentOrderTypes, enrollmentOrderTypes);
        _enrollmentOrderTypes = enrollmentOrderTypes;
    }

    /**
     * @return Приказы о зачислении.
     */
    @Length(max=2000)
    public String getEnrollmentOrders()
    {
        return _enrollmentOrders;
    }

    /**
     * @param enrollmentOrders Приказы о зачислении.
     */
    public void setEnrollmentOrders(String enrollmentOrders)
    {
        dirty(_enrollmentOrders, enrollmentOrders);
        _enrollmentOrders = enrollmentOrders;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEnrolledListReportGen)
        {
            setContent(((FefuEnrolledListReport)another).getContent());
            setFormingDate(((FefuEnrolledListReport)another).getFormingDate());
            setEnrollmentCampaign(((FefuEnrolledListReport)another).getEnrollmentCampaign());
            setDateFrom(((FefuEnrolledListReport)another).getDateFrom());
            setDateTo(((FefuEnrolledListReport)another).getDateTo());
            setCompensationType(((FefuEnrolledListReport)another).getCompensationType());
            setStudentCategoryTitle(((FefuEnrolledListReport)another).getStudentCategoryTitle());
            setOrderByOriginals(((FefuEnrolledListReport)another).isOrderByOriginals());
            setOnlyWithOriginals(((FefuEnrolledListReport)another).isOnlyWithOriginals());
            setWithoutDocumentInfo(((FefuEnrolledListReport)another).isWithoutDocumentInfo());
            setWithoutDetailSumMark(((FefuEnrolledListReport)another).isWithoutDetailSumMark());
            setShowDisciplineTitles(((FefuEnrolledListReport)another).isShowDisciplineTitles());
            setShowRequestedDirectionPriority(((FefuEnrolledListReport)another).isShowRequestedDirectionPriority());
            setQualificationTitle(((FefuEnrolledListReport)another).getQualificationTitle());
            setDevelopFormTitle(((FefuEnrolledListReport)another).getDevelopFormTitle());
            setDevelopConditionTitle(((FefuEnrolledListReport)another).getDevelopConditionTitle());
            setEnrollmentDirection(((FefuEnrolledListReport)another).getEnrollmentDirection());
            setNotPrintSpesWithoutRequest(((FefuEnrolledListReport)another).isNotPrintSpesWithoutRequest());
            setNotPrintNumInfo(((FefuEnrolledListReport)another).isNotPrintNumInfo());
            setIncludeForeignPerson(((FefuEnrolledListReport)another).getIncludeForeignPerson());
            setEnrollmentOrderTypes(((FefuEnrolledListReport)another).getEnrollmentOrderTypes());
            setEnrollmentOrders(((FefuEnrolledListReport)another).getEnrollmentOrders());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEnrolledListReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEnrolledListReport.class;
        }

        public T newInstance()
        {
            return (T) new FefuEnrolledListReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategoryTitle":
                    return obj.getStudentCategoryTitle();
                case "orderByOriginals":
                    return obj.isOrderByOriginals();
                case "onlyWithOriginals":
                    return obj.isOnlyWithOriginals();
                case "withoutDocumentInfo":
                    return obj.isWithoutDocumentInfo();
                case "withoutDetailSumMark":
                    return obj.isWithoutDetailSumMark();
                case "showDisciplineTitles":
                    return obj.isShowDisciplineTitles();
                case "showRequestedDirectionPriority":
                    return obj.isShowRequestedDirectionPriority();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "developFormTitle":
                    return obj.getDevelopFormTitle();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "notPrintSpesWithoutRequest":
                    return obj.isNotPrintSpesWithoutRequest();
                case "notPrintNumInfo":
                    return obj.isNotPrintNumInfo();
                case "includeForeignPerson":
                    return obj.getIncludeForeignPerson();
                case "enrollmentOrderTypes":
                    return obj.getEnrollmentOrderTypes();
                case "enrollmentOrders":
                    return obj.getEnrollmentOrders();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "studentCategoryTitle":
                    obj.setStudentCategoryTitle((String) value);
                    return;
                case "orderByOriginals":
                    obj.setOrderByOriginals((Boolean) value);
                    return;
                case "onlyWithOriginals":
                    obj.setOnlyWithOriginals((Boolean) value);
                    return;
                case "withoutDocumentInfo":
                    obj.setWithoutDocumentInfo((Boolean) value);
                    return;
                case "withoutDetailSumMark":
                    obj.setWithoutDetailSumMark((Boolean) value);
                    return;
                case "showDisciplineTitles":
                    obj.setShowDisciplineTitles((Boolean) value);
                    return;
                case "showRequestedDirectionPriority":
                    obj.setShowRequestedDirectionPriority((Boolean) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "developFormTitle":
                    obj.setDevelopFormTitle((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "notPrintSpesWithoutRequest":
                    obj.setNotPrintSpesWithoutRequest((Boolean) value);
                    return;
                case "notPrintNumInfo":
                    obj.setNotPrintNumInfo((Boolean) value);
                    return;
                case "includeForeignPerson":
                    obj.setIncludeForeignPerson((Boolean) value);
                    return;
                case "enrollmentOrderTypes":
                    obj.setEnrollmentOrderTypes((String) value);
                    return;
                case "enrollmentOrders":
                    obj.setEnrollmentOrders((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategoryTitle":
                        return true;
                case "orderByOriginals":
                        return true;
                case "onlyWithOriginals":
                        return true;
                case "withoutDocumentInfo":
                        return true;
                case "withoutDetailSumMark":
                        return true;
                case "showDisciplineTitles":
                        return true;
                case "showRequestedDirectionPriority":
                        return true;
                case "qualificationTitle":
                        return true;
                case "developFormTitle":
                        return true;
                case "developConditionTitle":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "notPrintSpesWithoutRequest":
                        return true;
                case "notPrintNumInfo":
                        return true;
                case "includeForeignPerson":
                        return true;
                case "enrollmentOrderTypes":
                        return true;
                case "enrollmentOrders":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategoryTitle":
                    return true;
                case "orderByOriginals":
                    return true;
                case "onlyWithOriginals":
                    return true;
                case "withoutDocumentInfo":
                    return true;
                case "withoutDetailSumMark":
                    return true;
                case "showDisciplineTitles":
                    return true;
                case "showRequestedDirectionPriority":
                    return true;
                case "qualificationTitle":
                    return true;
                case "developFormTitle":
                    return true;
                case "developConditionTitle":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "notPrintSpesWithoutRequest":
                    return true;
                case "notPrintNumInfo":
                    return true;
                case "includeForeignPerson":
                    return true;
                case "enrollmentOrderTypes":
                    return true;
                case "enrollmentOrders":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "compensationType":
                    return CompensationType.class;
                case "studentCategoryTitle":
                    return String.class;
                case "orderByOriginals":
                    return Boolean.class;
                case "onlyWithOriginals":
                    return Boolean.class;
                case "withoutDocumentInfo":
                    return Boolean.class;
                case "withoutDetailSumMark":
                    return Boolean.class;
                case "showDisciplineTitles":
                    return Boolean.class;
                case "showRequestedDirectionPriority":
                    return Boolean.class;
                case "qualificationTitle":
                    return String.class;
                case "developFormTitle":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "notPrintSpesWithoutRequest":
                    return Boolean.class;
                case "notPrintNumInfo":
                    return Boolean.class;
                case "includeForeignPerson":
                    return Boolean.class;
                case "enrollmentOrderTypes":
                    return String.class;
                case "enrollmentOrders":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEnrolledListReport> _dslPath = new Path<FefuEnrolledListReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEnrolledListReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getStudentCategoryTitle()
     */
    public static PropertyPath<String> studentCategoryTitle()
    {
        return _dslPath.studentCategoryTitle();
    }

    /**
     * @return Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isOrderByOriginals()
     */
    public static PropertyPath<Boolean> orderByOriginals()
    {
        return _dslPath.orderByOriginals();
    }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isOnlyWithOriginals()
     */
    public static PropertyPath<Boolean> onlyWithOriginals()
    {
        return _dslPath.onlyWithOriginals();
    }

    /**
     * @return Без информации о документах об образовании. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isWithoutDocumentInfo()
     */
    public static PropertyPath<Boolean> withoutDocumentInfo()
    {
        return _dslPath.withoutDocumentInfo();
    }

    /**
     * @return Без расшифровки суммы баллов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isWithoutDetailSumMark()
     */
    public static PropertyPath<Boolean> withoutDetailSumMark()
    {
        return _dslPath.withoutDetailSumMark();
    }

    /**
     * @return Выводить названия дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isShowDisciplineTitles()
     */
    public static PropertyPath<Boolean> showDisciplineTitles()
    {
        return _dslPath.showDisciplineTitles();
    }

    /**
     * @return Выводить приоритеты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isShowRequestedDirectionPriority()
     */
    public static PropertyPath<Boolean> showRequestedDirectionPriority()
    {
        return _dslPath.showRequestedDirectionPriority();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getDevelopFormTitle()
     */
    public static PropertyPath<String> developFormTitle()
    {
        return _dslPath.developFormTitle();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isNotPrintSpesWithoutRequest()
     */
    public static PropertyPath<Boolean> notPrintSpesWithoutRequest()
    {
        return _dslPath.notPrintSpesWithoutRequest();
    }

    /**
     * @return Не печатать информацию о цифрах приема. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isNotPrintNumInfo()
     */
    public static PropertyPath<Boolean> notPrintNumInfo()
    {
        return _dslPath.notPrintNumInfo();
    }

    /**
     * @return Учитывать иностранных граждан.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getIncludeForeignPerson()
     */
    public static PropertyPath<Boolean> includeForeignPerson()
    {
        return _dslPath.includeForeignPerson();
    }

    /**
     * @return Типы приказа о зачислении.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getEnrollmentOrderTypes()
     */
    public static PropertyPath<String> enrollmentOrderTypes()
    {
        return _dslPath.enrollmentOrderTypes();
    }

    /**
     * @return Приказы о зачислении.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getEnrollmentOrders()
     */
    public static PropertyPath<String> enrollmentOrders()
    {
        return _dslPath.enrollmentOrders();
    }

    public static class Path<E extends FefuEnrolledListReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _studentCategoryTitle;
        private PropertyPath<Boolean> _orderByOriginals;
        private PropertyPath<Boolean> _onlyWithOriginals;
        private PropertyPath<Boolean> _withoutDocumentInfo;
        private PropertyPath<Boolean> _withoutDetailSumMark;
        private PropertyPath<Boolean> _showDisciplineTitles;
        private PropertyPath<Boolean> _showRequestedDirectionPriority;
        private PropertyPath<String> _qualificationTitle;
        private PropertyPath<String> _developFormTitle;
        private PropertyPath<String> _developConditionTitle;
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private PropertyPath<Boolean> _notPrintSpesWithoutRequest;
        private PropertyPath<Boolean> _notPrintNumInfo;
        private PropertyPath<Boolean> _includeForeignPerson;
        private PropertyPath<String> _enrollmentOrderTypes;
        private PropertyPath<String> _enrollmentOrders;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(FefuEnrolledListReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(FefuEnrolledListReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(FefuEnrolledListReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getStudentCategoryTitle()
     */
        public PropertyPath<String> studentCategoryTitle()
        {
            if(_studentCategoryTitle == null )
                _studentCategoryTitle = new PropertyPath<String>(FefuEnrolledListReportGen.P_STUDENT_CATEGORY_TITLE, this);
            return _studentCategoryTitle;
        }

    /**
     * @return Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isOrderByOriginals()
     */
        public PropertyPath<Boolean> orderByOriginals()
        {
            if(_orderByOriginals == null )
                _orderByOriginals = new PropertyPath<Boolean>(FefuEnrolledListReportGen.P_ORDER_BY_ORIGINALS, this);
            return _orderByOriginals;
        }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isOnlyWithOriginals()
     */
        public PropertyPath<Boolean> onlyWithOriginals()
        {
            if(_onlyWithOriginals == null )
                _onlyWithOriginals = new PropertyPath<Boolean>(FefuEnrolledListReportGen.P_ONLY_WITH_ORIGINALS, this);
            return _onlyWithOriginals;
        }

    /**
     * @return Без информации о документах об образовании. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isWithoutDocumentInfo()
     */
        public PropertyPath<Boolean> withoutDocumentInfo()
        {
            if(_withoutDocumentInfo == null )
                _withoutDocumentInfo = new PropertyPath<Boolean>(FefuEnrolledListReportGen.P_WITHOUT_DOCUMENT_INFO, this);
            return _withoutDocumentInfo;
        }

    /**
     * @return Без расшифровки суммы баллов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isWithoutDetailSumMark()
     */
        public PropertyPath<Boolean> withoutDetailSumMark()
        {
            if(_withoutDetailSumMark == null )
                _withoutDetailSumMark = new PropertyPath<Boolean>(FefuEnrolledListReportGen.P_WITHOUT_DETAIL_SUM_MARK, this);
            return _withoutDetailSumMark;
        }

    /**
     * @return Выводить названия дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isShowDisciplineTitles()
     */
        public PropertyPath<Boolean> showDisciplineTitles()
        {
            if(_showDisciplineTitles == null )
                _showDisciplineTitles = new PropertyPath<Boolean>(FefuEnrolledListReportGen.P_SHOW_DISCIPLINE_TITLES, this);
            return _showDisciplineTitles;
        }

    /**
     * @return Выводить приоритеты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isShowRequestedDirectionPriority()
     */
        public PropertyPath<Boolean> showRequestedDirectionPriority()
        {
            if(_showRequestedDirectionPriority == null )
                _showRequestedDirectionPriority = new PropertyPath<Boolean>(FefuEnrolledListReportGen.P_SHOW_REQUESTED_DIRECTION_PRIORITY, this);
            return _showRequestedDirectionPriority;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(FefuEnrolledListReportGen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getDevelopFormTitle()
     */
        public PropertyPath<String> developFormTitle()
        {
            if(_developFormTitle == null )
                _developFormTitle = new PropertyPath<String>(FefuEnrolledListReportGen.P_DEVELOP_FORM_TITLE, this);
            return _developFormTitle;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(FefuEnrolledListReportGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isNotPrintSpesWithoutRequest()
     */
        public PropertyPath<Boolean> notPrintSpesWithoutRequest()
        {
            if(_notPrintSpesWithoutRequest == null )
                _notPrintSpesWithoutRequest = new PropertyPath<Boolean>(FefuEnrolledListReportGen.P_NOT_PRINT_SPES_WITHOUT_REQUEST, this);
            return _notPrintSpesWithoutRequest;
        }

    /**
     * @return Не печатать информацию о цифрах приема. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#isNotPrintNumInfo()
     */
        public PropertyPath<Boolean> notPrintNumInfo()
        {
            if(_notPrintNumInfo == null )
                _notPrintNumInfo = new PropertyPath<Boolean>(FefuEnrolledListReportGen.P_NOT_PRINT_NUM_INFO, this);
            return _notPrintNumInfo;
        }

    /**
     * @return Учитывать иностранных граждан.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getIncludeForeignPerson()
     */
        public PropertyPath<Boolean> includeForeignPerson()
        {
            if(_includeForeignPerson == null )
                _includeForeignPerson = new PropertyPath<Boolean>(FefuEnrolledListReportGen.P_INCLUDE_FOREIGN_PERSON, this);
            return _includeForeignPerson;
        }

    /**
     * @return Типы приказа о зачислении.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getEnrollmentOrderTypes()
     */
        public PropertyPath<String> enrollmentOrderTypes()
        {
            if(_enrollmentOrderTypes == null )
                _enrollmentOrderTypes = new PropertyPath<String>(FefuEnrolledListReportGen.P_ENROLLMENT_ORDER_TYPES, this);
            return _enrollmentOrderTypes;
        }

    /**
     * @return Приказы о зачислении.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport#getEnrollmentOrders()
     */
        public PropertyPath<String> enrollmentOrders()
        {
            if(_enrollmentOrders == null )
                _enrollmentOrders = new PropertyPath<String>(FefuEnrolledListReportGen.P_ENROLLMENT_ORDERS, this);
            return _enrollmentOrders;
        }

        public Class getEntityClass()
        {
            return FefuEnrolledListReport.class;
        }

        public String getEntityName()
        {
            return "fefuEnrolledListReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
