/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu8.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 21.10.2013
 */
public class FefuHolidayParagraphPartWrapper implements Comparable<FefuHolidayParagraphPartWrapper>
{
    private final EducationLevelsHighSchool _educationLevelsHighSchool;
    private final ListStudentExtract _firstExtract;
    private final List<Person> _personList = new ArrayList<>();

    public FefuHolidayParagraphPartWrapper(EducationLevelsHighSchool educationLevelsHighSchool, ListStudentExtract firstExtract)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
        _firstExtract = firstExtract;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuHolidayParagraphPartWrapper))
            return false;

        FefuHolidayParagraphPartWrapper that = (FefuHolidayParagraphPartWrapper) o;

        return _educationLevelsHighSchool.equals(that.getEducationLevelsHighSchool());
    }

    @Override
    public int compareTo(FefuHolidayParagraphPartWrapper o)
    {
        boolean isThisProfileOrSpecialization = _educationLevelsHighSchool.getEducationLevel().isProfileOrSpecialization();
        boolean isThatProfileOrSpecialization = o.getEducationLevelsHighSchool().getEducationLevel().isProfileOrSpecialization();

        if (!isThisProfileOrSpecialization)
            return -1;
        else
        {
            if (!isThatProfileOrSpecialization)
                return 1;
            else
                return _educationLevelsHighSchool.getTitle().compareTo(o.getEducationLevelsHighSchool().getTitle());
        }
    }
}