/* $Id$ */
package ru.tandemservice.unifefu.job.bo.FefuScheduleICalendarJob;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.common.job.IJobBusinessObjectManager;
import org.tandemframework.common.job.entity.JobConfig;
import org.tandemframework.common.job.entity.JobType;
import org.tandemframework.core.debug.Debug;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.FefuScheduleManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unifefu.job.entity.UnifefuSendICalendarJobConfig;

/**
 * @author nvankov
 * @since 9/16/13
 */
@Configuration
public class FefuScheduleICalendarJobManager extends BusinessObjectManager implements IJobBusinessObjectManager
{
    public static FefuScheduleICalendarJobManager instance()
    {
        return instance(FefuScheduleICalendarJobManager.class);
    }

    @Override
    public JobConfig createJobConfig(JobType type)
    {
        return new UnifefuSendICalendarJobConfig();
    }

    @Override
    public void runJob(JobConfig config)
    {
        Debug.message("Job " + config.getTitle() + " start");
        FefuScheduleManager.instance().eventsDao().sendICalendars();
        SppScheduleSessionManager.instance().dao().sendICalendars();
        Debug.message("Job " + config.getTitle() + " stop");
    }

    @Override
    public String getParamEditorComponent(JobType type)
    {
        return null;
    }
}
