/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 26.07.2013
 */
public interface IFefuEcOrderDAO extends INeedPersistenceSupport, ICommonDAO
{
    void saveOrderAndParagraphs(EnrollmentOrder order, EnrollmentOrderFefuExt orderExt, Course enrollmentCourse,
                                EmployeePost executor, EnrollmentOrderBasic basic, List<PreliminaryEnrollmentStudent> selectedPreStudentList);
}