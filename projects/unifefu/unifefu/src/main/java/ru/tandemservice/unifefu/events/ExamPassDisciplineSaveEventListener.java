/**
 *$Id$
 */
package ru.tandemservice.unifefu.events;

import org.hibernate.Session;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.listener.IHibernateInsertListener;
import org.tandemframework.hibsupport.event.single.type.HibernateSaveEvent;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 19.02.2013
 */
public class ExamPassDisciplineSaveEventListener extends FilteredSingleEntityEventListener<HibernateSaveEvent> implements IHibernateInsertListener
{
    /**
     * Метод получает для дисциплины, сдаваемой абитуриентом, дату сдачи, которая задается в настройке "Даты сдачи вступительных испытаний".
     * Поскольку в ДВФУ данная настройка учитывает форму освоения и техническую комиссию (которые можно получить из заявления абитуриента),
     * а дисциплина для сдачи ссылается прямо на абитуриента (т.к. может сдаваться сразу на несколько заявлений),
     * принято решение брать самое старое заявление абитуриента, по которому надо сдавать данную дисциплину.
     * См. DEV-2896 (Технические комиссии)
     *
     * @param exam дисциплина для сдачи
     * @return дату сдачи по дисциплине exam
     */
    private static Date getPassDateForExamDiscipline(ExamPassDiscipline exam, Session session)
    {
        EntrantRequest entrantRequest = null;
        DQLSelectBuilder dql;
        if (exam.getEntrantExamList().getEntrant() instanceof EntrantRequest)
        {
            entrantRequest = (EntrantRequest) exam.getEntrantExamList().getEntrant();
        }
        else if (exam.getEntrantExamList().getEntrant() instanceof Entrant)
        {
            dql = new DQLSelectBuilder()
                    .predicate(DQLPredicateType.distinct)
                    .top(1)
                    .fromEntity(EntrantRequest.class, "er")
                    .column("er")
                    .joinEntity("er", DQLJoinType.inner, RequestedEnrollmentDirection.class, "dir",
                                eq(
                                        property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("dir")),
                                        property(EntrantRequest.id().fromAlias("er"))
                                ))
                    .joinEntity("er", DQLJoinType.inner, ChosenEntranceDiscipline.class, "disc",
                                and(eq(
                                        property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().fromAlias("disc")),
                                        value(exam.getEnrollmentCampaignDiscipline())),
                                    eq(
                                            property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("disc")),
                                            property(RequestedEnrollmentDirection.id().fromAlias("dir"))
                                    ))
                    )
                    .where(eq(
                            property(EntrantRequest.entrant().fromAlias("er")),
                            commonValue(exam.getEntrantExamList().getEntrant())
                    ))
                    .order(
                            property(EntrantRequest.regDate().fromAlias("er")), OrderDirection.asc
                    );

            entrantRequest = dql.createStatement(session).uniqueResult();
        }

        if (entrantRequest == null)
            return null;

        FefuTechnicalCommission technicalCommission = UnifefuDaoFacade.getFefuEntrantDAO().getTechnicalCommission(entrantRequest);
        if (technicalCommission == null)
            return null;

        dql = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e")
                .predicate(DQLPredicateType.distinct)
                .column(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().fromAlias("e")))
                .where(eq(
                        property(RequestedEnrollmentDirection.entrantRequest().fromAlias("e")),
                        value(entrantRequest)
                ));
        DevelopForm developForm = dql.createStatement(session).uniqueResult();
        if (developForm == null)
            return null;

        dql = new DQLSelectBuilder().fromEntity(FefuDisciplineDateSetting.class, "e")
                .column(property(FefuDisciplineDateSetting.passDate().fromAlias("e")))
                .where(and(
                        eq(property(FefuDisciplineDateSetting.technicalCommission().fromAlias("e")), value(technicalCommission)),
                        eq(property(FefuDisciplineDateSetting.developForm().fromAlias("e")), value(developForm)),
                        eq(property(FefuDisciplineDateSetting.subjectPassForm().fromAlias("e")), value(exam.getSubjectPassForm())),
                        eq(property(FefuDisciplineDateSetting.discipline().fromAlias("e")), value(exam.getEnrollmentCampaignDiscipline()))
                ));
        return dql.createStatement(session).uniqueResult();
    }

    @Override
    public void onFilteredEvent(HibernateSaveEvent event)
    {
        ExamPassDiscipline discipline = (ExamPassDiscipline) event.getEntity();
        Date passDate = getPassDateForExamDiscipline(discipline, event.getSession());
        if (passDate != null)
            discipline.setPassDate(passDate);
    }
}