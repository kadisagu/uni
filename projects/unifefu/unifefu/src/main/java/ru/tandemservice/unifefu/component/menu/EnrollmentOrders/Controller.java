/* $Id$ */
package ru.tandemservice.unifefu.component.menu.EnrollmentOrders;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.FastCreator.FefuEcOrderFastCreator;

/**
 * @author Nikolay Fedorovskih
 * @since 17.07.2013
 */
public class Controller extends ru.tandemservice.uniec.component.menu.EnrollmentOrders.Controller
{
    public void onStartFastEnrollmentOrderCreator(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(FefuEcOrderFastCreator.class.getSimpleName()));
    }
}