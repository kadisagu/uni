/* $Id$ */
package ru.tandemservice.unifefu.component.catalog.fefuOrphanPaymentType.FefuOrphanPaymentTypePub;

import org.tandemframework.common.catalog.CatalogDefines;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IIconResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;

/**
 * @author nvankov
 * @since 11/18/13
 */
public class Controller extends DefaultCatalogPubController<FefuOrphanPaymentType, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<FefuOrphanPaymentType> createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<FefuOrphanPaymentType> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", FefuOrphanPaymentType.P_TITLE).setOrderable(false));
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Вывод на печать", FefuOrphanPaymentType.P_PRINT).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setOrderable(false));
//        dataSource.addColumn(new SimpleColumn("Коэффициент", FefuLoadTimeRule.coefficientAsDouble()).setFormatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setOrderable(false));
//        dataSource.addColumn(new SimpleColumn("Основание", FefuLoadTimeRule.base().s() + ".title").setOrderable(false));
//        dataSource.addColumn(new SimpleColumn("Примечание", FefuLoadTimeRule.comment()).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", FefuOrphanPaymentType.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));

        return dataSource;
    }


    public void onClickUp(IBusinessComponent context)
    {
        getDao().updatePriorityUp((Long) context.getListenerParameter());
    }

    public void onClickDown(IBusinessComponent context)
    {
        getDao().updatePriorityDown((Long) context.getListenerParameter());
    }
}
