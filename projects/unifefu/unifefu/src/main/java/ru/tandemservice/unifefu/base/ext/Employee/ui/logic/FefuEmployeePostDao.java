/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Employee.ui.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.employeebase.base.bo.Employee.logic.EmployeeDAO;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unifefu.entity.ws.FefuEmployeePostExt;

/**
 * @author Dmitry Seleznev
 * @since 09.07.2014
 */
public class FefuEmployeePostDao extends EmployeeDAO implements IFefuEmployeePostDao
{
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void saveEmployeePostExtension(Long employeeId, EmployeePost employeePostDTO, Integer rate)
    {
        employeePostDTO = super.saveEmployeePost(employeeId, employeePostDTO);
        FefuEmployeePostExt ext = new FefuEmployeePostExt();
        ext.setEmployeePost(employeePostDTO);
        ext.setRate(rate);
        getSession().save(ext);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateEmployeePostExtension(Long employeePostId, EmployeePost employeePostDTO, Integer rate)
    {
        super.updateEmployeePost(employeePostId, employeePostDTO);
        FefuEmployeePostExt ext = getByNaturalId(new FefuEmployeePostExt.NaturalId(employeePostDTO));
        if (null == ext)
        {
            ext = new FefuEmployeePostExt();
            ext.setEmployeePost(employeePostDTO);
        }
        ext.setRate(rate);
        getSession().saveOrUpdate(ext);
    }

    @Override
    public EmployeePost saveEmployeePost(Long employeeId, EmployeePost employeePostDTO)
    {
        return employeePostDTO;
    }

    @Override
    public EmployeePost updateEmployeePost(Long employeePostId, EmployeePost employeePostDTO)
    {
        return employeePostDTO;
    }
}