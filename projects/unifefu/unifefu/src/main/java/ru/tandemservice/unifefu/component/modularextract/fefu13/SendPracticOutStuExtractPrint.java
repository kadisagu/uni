/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu13;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nvankov
 * @since 6/20/13
 */
public class SendPracticOutStuExtractPrint implements IPrintFormCreator<SendPracticOutStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, SendPracticOutStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        Sex sex = extract.getEntity().getPerson().getIdentityCard().getSex();
        if (extract.isDoneEduPlan())
            modifier.put("doneEduPlan", " полностью выполнивш" + (sex.isMale() ? "его" : "ую") + " учебный план " + extract.getPracticeCourse().getTitle() + " курса,");
        else
            modifier.put("doneEduPlan", "");

        if (extract.isDonePractice())
            modifier.put("donePractice", "считать прошедш"  + (sex.isMale() ? "им" : "ей"));
        else
            modifier.put("donePractice", "направить на");

        if (extract.isOutClassTime())
            modifier.put("outClassTime", "в свободное от аудиторных занятий время ");
        else
            modifier.put("outClassTime", "");

        if(StringUtils.isEmpty(extract.getPracticeHeaderInnerStr()))
        {
            modifier.put("practiceHeaderInner_N", getPracticeHeaderInnerStr(extract.getPracticeHeaderInner(), extract.getPracticeHeaderInnerDegree()));
        }
        else
        {
            modifier.put("practiceHeaderInner_N", extract.getPracticeHeaderInnerStr());
        }

        modifier.put("practiceHeaderOut_N", extract.getPracticeHeaderOutStr());

        StringBuilder practiceExtOrgUnitStr = new StringBuilder();
        practiceExtOrgUnitStr.append(extract.getPracticeExtOrgUnit().getTitle());

        if(!StringUtils.isEmpty(extract.getPracticeFactAddressStr()))
            practiceExtOrgUnitStr.append(" Фактический адрес: ").append(extract.getPracticeFactAddressStr());

        modifier.put("practiceExtOrgUnitStr", practiceExtOrgUnitStr.toString());
        modifier.put("practiceExtOrgUnit", extract.getPracticeExtOrgUnit().getTitle());


        ///////////////

        String practiceKind = MoveStudentDaoFacade.getMoveStudentDao().getDeclinableFullPracticeKind(extract.getPracticeKind(), GrammaCase.ACCUSATIVE);
        modifier.put("practiceKind", practiceKind != null ? practiceKind : extract.getPracticeKind());

        modifier.put("practiceCourse", extract.getPracticeCourse().getTitle());

        modifier.put("practiceDuration", extract.getPracticeDurationStr() + " нед.");

        modifier.put("practiceBeginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeBeginDate()));
        modifier.put("practiceEndDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeEndDate()));
        modifier.put("attestationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getAttestationDate()));

        if(StringUtils.isEmpty(extract.getPreventAccidentsICStr()))
        {
            modifier.put("preventAccidentsIC_A", getPreventAccidentsICStr(extract.getPreventAccidentsIC(), extract.getPreventAccidentsICDegree()));
        }
        else
        {
            modifier.put("preventAccidentsIC_A", extract.getPreventAccidentsICStr());
        }

        //////

        modifier.put("topOu", TopOrgUnit.getInstance().getShortTitle());

        if (extract.isProvideFundsAccordingToEstimates())
        {
            RtfString provFndsAccToEstimRtfString = new RtfString();
            provFndsAccToEstimRtfString.append(ApplicationRuntime.getProperty("sendPracticeOut.provFndsAccToEstim")
                    .replaceAll("\\{estimateNum\\}", extract.getEstimateNum())).par();
            modifier.put("provFndsAccToEstim", provFndsAccToEstimRtfString);

            RtfString respForRecCashRtfString = new RtfString();

            if(!StringUtils.isEmpty(extract.getResponsForRecieveCashStr()))
            {
                respForRecCashRtfString.append(ApplicationRuntime.getProperty("sendPracticeOut.respForRecCash")
                        .replaceAll("\\{responsForRecieveCash_A\\}", extract.getResponsForRecieveCashStr())).par();
            }
            else
            {
                EmployeePost respForRecCash = extract.getResponsForRecieveCash();

                String respForRecCashStr = "";
                respForRecCashStr += getDegreeShortTitleWithDots(extract.getResponsForRecieveCashDegree());

                String respForRecCashAT = respForRecCash.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
                String respForRecCashAccustive = !StringUtils.isEmpty(respForRecCashAT) ? respForRecCashAT :
                        respForRecCash.getPostRelation().getPostBoundedWithQGandQL().getTitle();

                respForRecCashStr += " " + respForRecCashAccustive.toLowerCase();
                respForRecCashStr += " " + CommonListExtractPrint.getModifiedFioInitials(respForRecCash.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE);

                respForRecCashRtfString.append(ApplicationRuntime.getProperty("sendPracticeOut.respForRecCash")
                        .replaceAll("\\{responsForRecieveCash_A\\}", respForRecCashStr)).par();
            }
            modifier.put("respForRecCash", respForRecCashRtfString);
        }
        else
        {
            modifier.put("provFndsAccToEstim", "");
            modifier.put("respForRecCash", "");
        }

        if(!StringUtils.isEmpty(extract.getTextParagraph()))
        {
            RtfString textParRtfString = new RtfString().par().append(extract.getTextParagraph()).par().par();


            modifier.put("textPar", textParRtfString);
        }
        else
        {
            modifier.put("textPar", "");
        }

        ///////////////

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);

        //таблица
        RtfTableModifier tableModifier = new RtfTableModifier();

        int cnt = 1;
        List<String[]> paragraphDataLines = new ArrayList<>();


        String practiceHeaderInnerStr;

        if (StringUtils.isEmpty(extract.getPracticeHeaderInnerStr()))
        {
            practiceHeaderInnerStr = getPracticeHeaderInnerStr(extract.getPracticeHeaderInner(), extract.getPracticeHeaderInnerDegree());
        }
        else
        {
            practiceHeaderInnerStr = extract.getPracticeHeaderInnerStr();
        }

        ExternalOrgUnit currentExtOrgUnit = extract.getPracticeExtOrgUnit();

        StringBuilder extOrgUnitStr = new StringBuilder();
        extOrgUnitStr.append(currentExtOrgUnit.getTitle());

        AddressDetailed address = (currentExtOrgUnit.getFactAddress() != null && currentExtOrgUnit.getFactAddress().getSettlement() != null)
                ? currentExtOrgUnit.getFactAddress() : currentExtOrgUnit.getLegalAddress();

        if (null != address)
        {
            if (null != address.getSettlement())
            {
                extOrgUnitStr.append("[PAR]").append(address.getSettlement().getTitleWithType());
                if(address instanceof AddressRu && null != ((AddressRu)address).getStreet())
                {
                    extOrgUnitStr.append(",[PAR] ").append(((AddressRu)address).getStreet().getTitleWithType());
                    if (!StringUtils.isEmpty(((AddressRu)address).getHouseNumber()))
                    {
                        extOrgUnitStr.append(", д.").append(((AddressRu)address).getHouseNumber());
                        if (!StringUtils.isEmpty(((AddressRu)address).getHouseUnitNumber()))
                        {
                            extOrgUnitStr.append("-").append(((AddressRu)address).getHouseUnitNumber());
                        }
                    }
                }
                else if(address instanceof AddressInter && !StringUtils.isEmpty(((AddressInter) address).getAddressLocation()))
                {
                    extOrgUnitStr.append(",[PAR] ").append(((AddressInter) address).getAddressLocation());
                }
            }
            else
            {
                extOrgUnitStr.append("[PAR]").append(currentExtOrgUnit.getComment());
            }
        }

        if (!StringUtils.isEmpty(currentExtOrgUnit.getPhone()))
            extOrgUnitStr.append(",[PAR] тел. ").append(currentExtOrgUnit.getPhone());

        if (!StringUtils.isEmpty(currentExtOrgUnit.getEmail()))
            extOrgUnitStr.append(", ").append(currentExtOrgUnit.getEmail());

        if (!StringUtils.isEmpty(extract.getPracticeFactAddressStr()))
            extOrgUnitStr.append("[PAR]Фактический адрес: ").append(extract.getPracticeFactAddressStr());

        paragraphDataLines.add(new String[]{String.valueOf(cnt++), extract.getEntity().getPerson().getFullFio(), extOrgUnitStr.toString(), extract.getPracticeContract().getContractNum(), practiceHeaderInnerStr, extract.getPracticeHeaderOutStr()});


        tableModifier.put("T", paragraphDataLines.toArray(new String[][]{}));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (value != null && value.contains("[PAR]"))
                {
                    RtfString subject = new RtfString();
                    int b = 0;
                    int a = value.indexOf("[PAR]", 0);
                    while (a != -1)
                    {
                        subject.append(value.substring(b, a)).par();
                        b = a + 5;
                        a = value.indexOf("[PAR]", a + 1);
                    }
                    subject.append(value.substring(b));
                    return subject.toList();
                }
                else
                {
                    return new RtfString().append(value).toList();
                }
            }
        });

        tableModifier.modify(document);

        return document;
    }

    private String getPreventAccidentsICStr(EmployeePost employeePost, PersonAcademicDegree degree)
    {
        StringBuilder preventAccidentsICStrBuilder = new StringBuilder();

        preventAccidentsICStrBuilder.append(getDegreeShortTitleWithDots(degree));

        String preventPostAT = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
        String preventAccidentsICAccustive = !StringUtils.isEmpty(preventPostAT) ? preventPostAT :
                employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(preventAccidentsICAccustive))
            preventAccidentsICStrBuilder.append((StringUtils.isEmpty(preventAccidentsICStrBuilder.toString()) ? "" : " ")).append(preventAccidentsICAccustive.toLowerCase());

        preventAccidentsICStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedIofInitials(employeePost.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));

        return preventAccidentsICStrBuilder.toString();
    }

    private String getPracticeHeaderInnerStr(EmployeePost employeePost, PersonAcademicDegree degree)
    {
        StringBuilder practiceHeaderStrBuilder = new StringBuilder();

        practiceHeaderStrBuilder.append(getDegreeShortTitleWithDots(degree));

        String practiceHeaderPostNT = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        String practiceHeaderNominative = !StringUtils.isEmpty(practiceHeaderPostNT) ? practiceHeaderPostNT :
                employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(practiceHeaderNominative))
            practiceHeaderStrBuilder.append((StringUtils.isEmpty(practiceHeaderStrBuilder.toString()) ? "" : " ")).append(practiceHeaderNominative.toLowerCase());

        practiceHeaderStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedIofInitials(employeePost.getPerson().getIdentityCard(), GrammaCase.NOMINATIVE));

        return practiceHeaderStrBuilder.toString();
    }

    private String getDegreeShortTitleWithDots(PersonAcademicDegree prevDegree)
    {
        String preventAccidentsICStr = "";
        if (null != prevDegree)
        {
            String[] degreeTitle = prevDegree.getAcademicDegree().getTitle().toLowerCase().split(" ");
            StringBuilder shortDegreeTitle = new StringBuilder();
            for (String deg : Lists.newArrayList(degreeTitle))
            {
                shortDegreeTitle.append(deg.charAt(0)).append(".");
            }
            preventAccidentsICStr += shortDegreeTitle;
        }
        return preventAccidentsICStr;
    }

}
