/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.ActivityTypeEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.FefuActivityType;
import ru.tandemservice.unifefu.entity.gen.FefuActivityTypeGen;

import java.util.List;


/**
 * @author Andrey Andreev
 * @since 26.01.2016
 */
@Input({
        @Bind(key = "activityTypeId", binding = "activityTypeId"),
        @Bind(key = "blockId", binding = "blockId")
})
public class FefuEduPlanActivityTypeEditUI extends UIPresenter
{

    private Long _blockId;
    private Long _activityTypeId;
    private FefuActivityType _activityType;

    @Override
    public void onComponentRefresh()
    {
        if (_activityTypeId != null)
        {
            _activityType = DataAccessServices.dao().getNotNull(_activityTypeId);
        }
        else
        {
            EppEduPlanVersionBlock block = DataAccessServices.dao().getNotNull(_blockId);
            List<FefuActivityType> existActivityTypes = DataAccessServices.dao().getList(FefuActivityType.class, FefuActivityType.L_BLOCK, block);
            _activityType = new FefuActivityType(block);
            _activityType.setNumber(existActivityTypes.stream().mapToInt(FefuActivityTypeGen::getNumber).max().orElse(0) + 1);
        }
    }


    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_activityType);
        deactivate();
    }


    public Long getBlockId()
    {
        return _blockId;
    }

    public void setBlockId(Long blockId)
    {
        _blockId = blockId;
    }

    public Long getActivityTypeId()
    {
        return _activityTypeId;
    }

    public void setActivityTypeId(Long activityTypeId)
    {
        _activityTypeId = activityTypeId;
    }

    public FefuActivityType getActivityType()
    {
        return _activityType;
    }

    public void setActivityType(FefuActivityType activityType)
    {
        _activityType = activityType;
    }
}