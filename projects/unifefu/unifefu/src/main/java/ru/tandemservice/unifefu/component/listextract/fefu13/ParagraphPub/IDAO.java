package ru.tandemservice.unifefu.component.listextract.fefu13.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuAdditionalAcademGrantStuListExtract, Model>
{
}
