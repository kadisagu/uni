package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import ru.tandemservice.unifefu.entity.ws.MdbViewPerson;
import ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ о полученном образовании (базовый)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewPersonEduinstitutionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution";
    public static final String ENTITY_NAME = "mdbViewPersonEduinstitution";
    public static final int VERSION_HASH = -377700769;
    private static IEntityMeta ENTITY_META;

    public static final String L_MDB_VIEW_PERSON = "mdbViewPerson";
    public static final String L_PERSON_EDU_INSTITUTION = "personEduInstitution";
    public static final String L_EDU_INSTITUTION = "eduInstitution";
    public static final String P_EDUCATIONAL_INSTITUTION_COUNTRY = "educationalInstitutionCountry";
    public static final String P_EDUCATIONAL_INSTITUTION_SETTLEMENT = "educationalInstitutionSettlement";
    public static final String P_EDUCATIONAL_INSTITUTION_TYPE_KIND = "educationalInstitutionTypeKind";
    public static final String P_EDUCATION_DOCUMENT_TYPE = "educationDocumentType";
    public static final String P_EDUCATION_LEVEL_STAGE = "educationLevelStage";
    public static final String P_EMPLOYEE_SPECIALITY = "employeeSpeciality";
    public static final String P_DIPLOMA_QUALIFICATION = "diplomaQualification";
    public static final String P_QUALIFICATION = "qualification";
    public static final String P_DATE = "date";
    public static final String P_SERIA = "seria";
    public static final String P_NUMBER = "number";
    public static final String P_MARKS = "marks";
    public static final String P_REGION_CODE = "regionCode";
    public static final String P_GRADUATION_HONOUR = "graduationHonour";
    public static final String P_YEAR_END = "yearEnd";

    private MdbViewPerson _mdbViewPerson;     // Персона
    private PersonEduInstitution _personEduInstitution;     // Документ о полученном образовании (базовый)
    private EduInstitution _eduInstitution;     // Образовательное учреждение
    private String _educationalInstitutionCountry;     // Адрес ОУ (страна)
    private String _educationalInstitutionSettlement;     // Адрес ОУ (административная единица)
    private String _educationalInstitutionTypeKind;     // Тип образовательного учреждения
    private String _educationDocumentType;     // Тип документа о полученном образовании
    private String _educationLevelStage;     // Уровень/ступень образования
    private String _employeeSpeciality;     // Специальность по диплому
    private String _diplomaQualification;     // Квалификация по диплому
    private String _qualification;     // Квалификация
    private Date _date;     // Дата выдачи
    private String _seria;     // Серия
    private String _number;     // Номер
    private String _marks;     // Кол-во оценок «Отлично» «Хорошо» «Удовлетворительно»
    private String _regionCode;     // Код субъекта РФ
    private String _graduationHonour;     // Степень отличия по окончании образовательного учреждения
    private int _yearEnd;     // Год окончания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона. Свойство не может быть null.
     */
    @NotNull
    public MdbViewPerson getMdbViewPerson()
    {
        return _mdbViewPerson;
    }

    /**
     * @param mdbViewPerson Персона. Свойство не может быть null.
     */
    public void setMdbViewPerson(MdbViewPerson mdbViewPerson)
    {
        dirty(_mdbViewPerson, mdbViewPerson);
        _mdbViewPerson = mdbViewPerson;
    }

    /**
     * @return Документ о полученном образовании (базовый). Свойство не может быть null.
     */
    @NotNull
    public PersonEduInstitution getPersonEduInstitution()
    {
        return _personEduInstitution;
    }

    /**
     * @param personEduInstitution Документ о полученном образовании (базовый). Свойство не может быть null.
     */
    public void setPersonEduInstitution(PersonEduInstitution personEduInstitution)
    {
        dirty(_personEduInstitution, personEduInstitution);
        _personEduInstitution = personEduInstitution;
    }

    /**
     * @return Образовательное учреждение.
     */
    public EduInstitution getEduInstitution()
    {
        return _eduInstitution;
    }

    /**
     * @param eduInstitution Образовательное учреждение.
     */
    public void setEduInstitution(EduInstitution eduInstitution)
    {
        dirty(_eduInstitution, eduInstitution);
        _eduInstitution = eduInstitution;
    }

    /**
     * @return Адрес ОУ (страна).
     */
    @Length(max=255)
    public String getEducationalInstitutionCountry()
    {
        return _educationalInstitutionCountry;
    }

    /**
     * @param educationalInstitutionCountry Адрес ОУ (страна).
     */
    public void setEducationalInstitutionCountry(String educationalInstitutionCountry)
    {
        dirty(_educationalInstitutionCountry, educationalInstitutionCountry);
        _educationalInstitutionCountry = educationalInstitutionCountry;
    }

    /**
     * @return Адрес ОУ (административная единица).
     */
    @Length(max=255)
    public String getEducationalInstitutionSettlement()
    {
        return _educationalInstitutionSettlement;
    }

    /**
     * @param educationalInstitutionSettlement Адрес ОУ (административная единица).
     */
    public void setEducationalInstitutionSettlement(String educationalInstitutionSettlement)
    {
        dirty(_educationalInstitutionSettlement, educationalInstitutionSettlement);
        _educationalInstitutionSettlement = educationalInstitutionSettlement;
    }

    /**
     * @return Тип образовательного учреждения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationalInstitutionTypeKind()
    {
        return _educationalInstitutionTypeKind;
    }

    /**
     * @param educationalInstitutionTypeKind Тип образовательного учреждения. Свойство не может быть null.
     */
    public void setEducationalInstitutionTypeKind(String educationalInstitutionTypeKind)
    {
        dirty(_educationalInstitutionTypeKind, educationalInstitutionTypeKind);
        _educationalInstitutionTypeKind = educationalInstitutionTypeKind;
    }

    /**
     * @return Тип документа о полученном образовании. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationDocumentType()
    {
        return _educationDocumentType;
    }

    /**
     * @param educationDocumentType Тип документа о полученном образовании. Свойство не может быть null.
     */
    public void setEducationDocumentType(String educationDocumentType)
    {
        dirty(_educationDocumentType, educationDocumentType);
        _educationDocumentType = educationDocumentType;
    }

    /**
     * @return Уровень/ступень образования. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationLevelStage()
    {
        return _educationLevelStage;
    }

    /**
     * @param educationLevelStage Уровень/ступень образования. Свойство не может быть null.
     */
    public void setEducationLevelStage(String educationLevelStage)
    {
        dirty(_educationLevelStage, educationLevelStage);
        _educationLevelStage = educationLevelStage;
    }

    /**
     * @return Специальность по диплому.
     */
    @Length(max=255)
    public String getEmployeeSpeciality()
    {
        return _employeeSpeciality;
    }

    /**
     * @param employeeSpeciality Специальность по диплому.
     */
    public void setEmployeeSpeciality(String employeeSpeciality)
    {
        dirty(_employeeSpeciality, employeeSpeciality);
        _employeeSpeciality = employeeSpeciality;
    }

    /**
     * @return Квалификация по диплому.
     */
    @Length(max=255)
    public String getDiplomaQualification()
    {
        return _diplomaQualification;
    }

    /**
     * @param diplomaQualification Квалификация по диплому.
     */
    public void setDiplomaQualification(String diplomaQualification)
    {
        dirty(_diplomaQualification, diplomaQualification);
        _diplomaQualification = diplomaQualification;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация.
     */
    public void setQualification(String qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Дата выдачи.
     */
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата выдачи.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Серия.
     */
    @Length(max=255)
    public String getSeria()
    {
        return _seria;
    }

    /**
     * @param seria Серия.
     */
    public void setSeria(String seria)
    {
        dirty(_seria, seria);
        _seria = seria;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Кол-во оценок «Отлично» «Хорошо» «Удовлетворительно».
     */
    @Length(max=255)
    public String getMarks()
    {
        return _marks;
    }

    /**
     * @param marks Кол-во оценок «Отлично» «Хорошо» «Удовлетворительно».
     */
    public void setMarks(String marks)
    {
        dirty(_marks, marks);
        _marks = marks;
    }

    /**
     * @return Код субъекта РФ.
     */
    @Length(max=255)
    public String getRegionCode()
    {
        return _regionCode;
    }

    /**
     * @param regionCode Код субъекта РФ.
     */
    public void setRegionCode(String regionCode)
    {
        dirty(_regionCode, regionCode);
        _regionCode = regionCode;
    }

    /**
     * @return Степень отличия по окончании образовательного учреждения.
     */
    @Length(max=255)
    public String getGraduationHonour()
    {
        return _graduationHonour;
    }

    /**
     * @param graduationHonour Степень отличия по окончании образовательного учреждения.
     */
    public void setGraduationHonour(String graduationHonour)
    {
        dirty(_graduationHonour, graduationHonour);
        _graduationHonour = graduationHonour;
    }

    /**
     * @return Год окончания. Свойство не может быть null.
     */
    @NotNull
    public int getYearEnd()
    {
        return _yearEnd;
    }

    /**
     * @param yearEnd Год окончания. Свойство не может быть null.
     */
    public void setYearEnd(int yearEnd)
    {
        dirty(_yearEnd, yearEnd);
        _yearEnd = yearEnd;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewPersonEduinstitutionGen)
        {
            setMdbViewPerson(((MdbViewPersonEduinstitution)another).getMdbViewPerson());
            setPersonEduInstitution(((MdbViewPersonEduinstitution)another).getPersonEduInstitution());
            setEduInstitution(((MdbViewPersonEduinstitution)another).getEduInstitution());
            setEducationalInstitutionCountry(((MdbViewPersonEduinstitution)another).getEducationalInstitutionCountry());
            setEducationalInstitutionSettlement(((MdbViewPersonEduinstitution)another).getEducationalInstitutionSettlement());
            setEducationalInstitutionTypeKind(((MdbViewPersonEduinstitution)another).getEducationalInstitutionTypeKind());
            setEducationDocumentType(((MdbViewPersonEduinstitution)another).getEducationDocumentType());
            setEducationLevelStage(((MdbViewPersonEduinstitution)another).getEducationLevelStage());
            setEmployeeSpeciality(((MdbViewPersonEduinstitution)another).getEmployeeSpeciality());
            setDiplomaQualification(((MdbViewPersonEduinstitution)another).getDiplomaQualification());
            setQualification(((MdbViewPersonEduinstitution)another).getQualification());
            setDate(((MdbViewPersonEduinstitution)another).getDate());
            setSeria(((MdbViewPersonEduinstitution)another).getSeria());
            setNumber(((MdbViewPersonEduinstitution)another).getNumber());
            setMarks(((MdbViewPersonEduinstitution)another).getMarks());
            setRegionCode(((MdbViewPersonEduinstitution)another).getRegionCode());
            setGraduationHonour(((MdbViewPersonEduinstitution)another).getGraduationHonour());
            setYearEnd(((MdbViewPersonEduinstitution)another).getYearEnd());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewPersonEduinstitutionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewPersonEduinstitution.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewPersonEduinstitution();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mdbViewPerson":
                    return obj.getMdbViewPerson();
                case "personEduInstitution":
                    return obj.getPersonEduInstitution();
                case "eduInstitution":
                    return obj.getEduInstitution();
                case "educationalInstitutionCountry":
                    return obj.getEducationalInstitutionCountry();
                case "educationalInstitutionSettlement":
                    return obj.getEducationalInstitutionSettlement();
                case "educationalInstitutionTypeKind":
                    return obj.getEducationalInstitutionTypeKind();
                case "educationDocumentType":
                    return obj.getEducationDocumentType();
                case "educationLevelStage":
                    return obj.getEducationLevelStage();
                case "employeeSpeciality":
                    return obj.getEmployeeSpeciality();
                case "diplomaQualification":
                    return obj.getDiplomaQualification();
                case "qualification":
                    return obj.getQualification();
                case "date":
                    return obj.getDate();
                case "seria":
                    return obj.getSeria();
                case "number":
                    return obj.getNumber();
                case "marks":
                    return obj.getMarks();
                case "regionCode":
                    return obj.getRegionCode();
                case "graduationHonour":
                    return obj.getGraduationHonour();
                case "yearEnd":
                    return obj.getYearEnd();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mdbViewPerson":
                    obj.setMdbViewPerson((MdbViewPerson) value);
                    return;
                case "personEduInstitution":
                    obj.setPersonEduInstitution((PersonEduInstitution) value);
                    return;
                case "eduInstitution":
                    obj.setEduInstitution((EduInstitution) value);
                    return;
                case "educationalInstitutionCountry":
                    obj.setEducationalInstitutionCountry((String) value);
                    return;
                case "educationalInstitutionSettlement":
                    obj.setEducationalInstitutionSettlement((String) value);
                    return;
                case "educationalInstitutionTypeKind":
                    obj.setEducationalInstitutionTypeKind((String) value);
                    return;
                case "educationDocumentType":
                    obj.setEducationDocumentType((String) value);
                    return;
                case "educationLevelStage":
                    obj.setEducationLevelStage((String) value);
                    return;
                case "employeeSpeciality":
                    obj.setEmployeeSpeciality((String) value);
                    return;
                case "diplomaQualification":
                    obj.setDiplomaQualification((String) value);
                    return;
                case "qualification":
                    obj.setQualification((String) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "seria":
                    obj.setSeria((String) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "marks":
                    obj.setMarks((String) value);
                    return;
                case "regionCode":
                    obj.setRegionCode((String) value);
                    return;
                case "graduationHonour":
                    obj.setGraduationHonour((String) value);
                    return;
                case "yearEnd":
                    obj.setYearEnd((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mdbViewPerson":
                        return true;
                case "personEduInstitution":
                        return true;
                case "eduInstitution":
                        return true;
                case "educationalInstitutionCountry":
                        return true;
                case "educationalInstitutionSettlement":
                        return true;
                case "educationalInstitutionTypeKind":
                        return true;
                case "educationDocumentType":
                        return true;
                case "educationLevelStage":
                        return true;
                case "employeeSpeciality":
                        return true;
                case "diplomaQualification":
                        return true;
                case "qualification":
                        return true;
                case "date":
                        return true;
                case "seria":
                        return true;
                case "number":
                        return true;
                case "marks":
                        return true;
                case "regionCode":
                        return true;
                case "graduationHonour":
                        return true;
                case "yearEnd":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mdbViewPerson":
                    return true;
                case "personEduInstitution":
                    return true;
                case "eduInstitution":
                    return true;
                case "educationalInstitutionCountry":
                    return true;
                case "educationalInstitutionSettlement":
                    return true;
                case "educationalInstitutionTypeKind":
                    return true;
                case "educationDocumentType":
                    return true;
                case "educationLevelStage":
                    return true;
                case "employeeSpeciality":
                    return true;
                case "diplomaQualification":
                    return true;
                case "qualification":
                    return true;
                case "date":
                    return true;
                case "seria":
                    return true;
                case "number":
                    return true;
                case "marks":
                    return true;
                case "regionCode":
                    return true;
                case "graduationHonour":
                    return true;
                case "yearEnd":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mdbViewPerson":
                    return MdbViewPerson.class;
                case "personEduInstitution":
                    return PersonEduInstitution.class;
                case "eduInstitution":
                    return EduInstitution.class;
                case "educationalInstitutionCountry":
                    return String.class;
                case "educationalInstitutionSettlement":
                    return String.class;
                case "educationalInstitutionTypeKind":
                    return String.class;
                case "educationDocumentType":
                    return String.class;
                case "educationLevelStage":
                    return String.class;
                case "employeeSpeciality":
                    return String.class;
                case "diplomaQualification":
                    return String.class;
                case "qualification":
                    return String.class;
                case "date":
                    return Date.class;
                case "seria":
                    return String.class;
                case "number":
                    return String.class;
                case "marks":
                    return String.class;
                case "regionCode":
                    return String.class;
                case "graduationHonour":
                    return String.class;
                case "yearEnd":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewPersonEduinstitution> _dslPath = new Path<MdbViewPersonEduinstitution>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewPersonEduinstitution");
    }
            

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getMdbViewPerson()
     */
    public static MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
    {
        return _dslPath.mdbViewPerson();
    }

    /**
     * @return Документ о полученном образовании (базовый). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getPersonEduInstitution()
     */
    public static PersonEduInstitution.Path<PersonEduInstitution> personEduInstitution()
    {
        return _dslPath.personEduInstitution();
    }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEduInstitution()
     */
    public static EduInstitution.Path<EduInstitution> eduInstitution()
    {
        return _dslPath.eduInstitution();
    }

    /**
     * @return Адрес ОУ (страна).
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEducationalInstitutionCountry()
     */
    public static PropertyPath<String> educationalInstitutionCountry()
    {
        return _dslPath.educationalInstitutionCountry();
    }

    /**
     * @return Адрес ОУ (административная единица).
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEducationalInstitutionSettlement()
     */
    public static PropertyPath<String> educationalInstitutionSettlement()
    {
        return _dslPath.educationalInstitutionSettlement();
    }

    /**
     * @return Тип образовательного учреждения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEducationalInstitutionTypeKind()
     */
    public static PropertyPath<String> educationalInstitutionTypeKind()
    {
        return _dslPath.educationalInstitutionTypeKind();
    }

    /**
     * @return Тип документа о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEducationDocumentType()
     */
    public static PropertyPath<String> educationDocumentType()
    {
        return _dslPath.educationDocumentType();
    }

    /**
     * @return Уровень/ступень образования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEducationLevelStage()
     */
    public static PropertyPath<String> educationLevelStage()
    {
        return _dslPath.educationLevelStage();
    }

    /**
     * @return Специальность по диплому.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEmployeeSpeciality()
     */
    public static PropertyPath<String> employeeSpeciality()
    {
        return _dslPath.employeeSpeciality();
    }

    /**
     * @return Квалификация по диплому.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getDiplomaQualification()
     */
    public static PropertyPath<String> diplomaQualification()
    {
        return _dslPath.diplomaQualification();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getQualification()
     */
    public static PropertyPath<String> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getSeria()
     */
    public static PropertyPath<String> seria()
    {
        return _dslPath.seria();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Кол-во оценок «Отлично» «Хорошо» «Удовлетворительно».
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getMarks()
     */
    public static PropertyPath<String> marks()
    {
        return _dslPath.marks();
    }

    /**
     * @return Код субъекта РФ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getRegionCode()
     */
    public static PropertyPath<String> regionCode()
    {
        return _dslPath.regionCode();
    }

    /**
     * @return Степень отличия по окончании образовательного учреждения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getGraduationHonour()
     */
    public static PropertyPath<String> graduationHonour()
    {
        return _dslPath.graduationHonour();
    }

    /**
     * @return Год окончания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getYearEnd()
     */
    public static PropertyPath<Integer> yearEnd()
    {
        return _dslPath.yearEnd();
    }

    public static class Path<E extends MdbViewPersonEduinstitution> extends EntityPath<E>
    {
        private MdbViewPerson.Path<MdbViewPerson> _mdbViewPerson;
        private PersonEduInstitution.Path<PersonEduInstitution> _personEduInstitution;
        private EduInstitution.Path<EduInstitution> _eduInstitution;
        private PropertyPath<String> _educationalInstitutionCountry;
        private PropertyPath<String> _educationalInstitutionSettlement;
        private PropertyPath<String> _educationalInstitutionTypeKind;
        private PropertyPath<String> _educationDocumentType;
        private PropertyPath<String> _educationLevelStage;
        private PropertyPath<String> _employeeSpeciality;
        private PropertyPath<String> _diplomaQualification;
        private PropertyPath<String> _qualification;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _seria;
        private PropertyPath<String> _number;
        private PropertyPath<String> _marks;
        private PropertyPath<String> _regionCode;
        private PropertyPath<String> _graduationHonour;
        private PropertyPath<Integer> _yearEnd;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getMdbViewPerson()
     */
        public MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
        {
            if(_mdbViewPerson == null )
                _mdbViewPerson = new MdbViewPerson.Path<MdbViewPerson>(L_MDB_VIEW_PERSON, this);
            return _mdbViewPerson;
        }

    /**
     * @return Документ о полученном образовании (базовый). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getPersonEduInstitution()
     */
        public PersonEduInstitution.Path<PersonEduInstitution> personEduInstitution()
        {
            if(_personEduInstitution == null )
                _personEduInstitution = new PersonEduInstitution.Path<PersonEduInstitution>(L_PERSON_EDU_INSTITUTION, this);
            return _personEduInstitution;
        }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEduInstitution()
     */
        public EduInstitution.Path<EduInstitution> eduInstitution()
        {
            if(_eduInstitution == null )
                _eduInstitution = new EduInstitution.Path<EduInstitution>(L_EDU_INSTITUTION, this);
            return _eduInstitution;
        }

    /**
     * @return Адрес ОУ (страна).
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEducationalInstitutionCountry()
     */
        public PropertyPath<String> educationalInstitutionCountry()
        {
            if(_educationalInstitutionCountry == null )
                _educationalInstitutionCountry = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_EDUCATIONAL_INSTITUTION_COUNTRY, this);
            return _educationalInstitutionCountry;
        }

    /**
     * @return Адрес ОУ (административная единица).
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEducationalInstitutionSettlement()
     */
        public PropertyPath<String> educationalInstitutionSettlement()
        {
            if(_educationalInstitutionSettlement == null )
                _educationalInstitutionSettlement = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_EDUCATIONAL_INSTITUTION_SETTLEMENT, this);
            return _educationalInstitutionSettlement;
        }

    /**
     * @return Тип образовательного учреждения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEducationalInstitutionTypeKind()
     */
        public PropertyPath<String> educationalInstitutionTypeKind()
        {
            if(_educationalInstitutionTypeKind == null )
                _educationalInstitutionTypeKind = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_EDUCATIONAL_INSTITUTION_TYPE_KIND, this);
            return _educationalInstitutionTypeKind;
        }

    /**
     * @return Тип документа о полученном образовании. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEducationDocumentType()
     */
        public PropertyPath<String> educationDocumentType()
        {
            if(_educationDocumentType == null )
                _educationDocumentType = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_EDUCATION_DOCUMENT_TYPE, this);
            return _educationDocumentType;
        }

    /**
     * @return Уровень/ступень образования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEducationLevelStage()
     */
        public PropertyPath<String> educationLevelStage()
        {
            if(_educationLevelStage == null )
                _educationLevelStage = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_EDUCATION_LEVEL_STAGE, this);
            return _educationLevelStage;
        }

    /**
     * @return Специальность по диплому.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getEmployeeSpeciality()
     */
        public PropertyPath<String> employeeSpeciality()
        {
            if(_employeeSpeciality == null )
                _employeeSpeciality = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_EMPLOYEE_SPECIALITY, this);
            return _employeeSpeciality;
        }

    /**
     * @return Квалификация по диплому.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getDiplomaQualification()
     */
        public PropertyPath<String> diplomaQualification()
        {
            if(_diplomaQualification == null )
                _diplomaQualification = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_DIPLOMA_QUALIFICATION, this);
            return _diplomaQualification;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getQualification()
     */
        public PropertyPath<String> qualification()
        {
            if(_qualification == null )
                _qualification = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Дата выдачи.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(MdbViewPersonEduinstitutionGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getSeria()
     */
        public PropertyPath<String> seria()
        {
            if(_seria == null )
                _seria = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_SERIA, this);
            return _seria;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Кол-во оценок «Отлично» «Хорошо» «Удовлетворительно».
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getMarks()
     */
        public PropertyPath<String> marks()
        {
            if(_marks == null )
                _marks = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_MARKS, this);
            return _marks;
        }

    /**
     * @return Код субъекта РФ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getRegionCode()
     */
        public PropertyPath<String> regionCode()
        {
            if(_regionCode == null )
                _regionCode = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_REGION_CODE, this);
            return _regionCode;
        }

    /**
     * @return Степень отличия по окончании образовательного учреждения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getGraduationHonour()
     */
        public PropertyPath<String> graduationHonour()
        {
            if(_graduationHonour == null )
                _graduationHonour = new PropertyPath<String>(MdbViewPersonEduinstitutionGen.P_GRADUATION_HONOUR, this);
            return _graduationHonour;
        }

    /**
     * @return Год окончания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution#getYearEnd()
     */
        public PropertyPath<Integer> yearEnd()
        {
            if(_yearEnd == null )
                _yearEnd = new PropertyPath<Integer>(MdbViewPersonEduinstitutionGen.P_YEAR_END, this);
            return _yearEnd;
        }

        public Class getEntityClass()
        {
            return MdbViewPersonEduinstitution.class;
        }

        public String getEntityName()
        {
            return "mdbViewPersonEduinstitution";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
