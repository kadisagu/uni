package ru.tandemservice.unifefu.entity.catalog;

import ru.tandemservice.unifefu.entity.catalog.gen.*;

import java.util.Arrays;
import java.util.Collection;

/**
 * Виды практик студентов (ДВФУ)
 */
public class FefuStudentPractice extends FefuStudentPracticeGen
{
    @Override
    public Collection<String> getDeclinableProperties()
    {
        return Arrays.asList(P_TITLE);
    }
}