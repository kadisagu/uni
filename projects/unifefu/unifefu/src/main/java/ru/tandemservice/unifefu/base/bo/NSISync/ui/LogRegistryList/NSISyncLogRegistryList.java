/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.LogRegistryList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.base.bo.NSISync.NSISyncManager;
import ru.tandemservice.unifefu.base.bo.NSISync.ui.LogRowView.NSISyncLogRowView;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 28.01.2015
 */
@Configuration
public class NSISyncLogRegistryList extends BusinessComponentManager
{
    public static final String NSI_CATALOG_LOG_DS = "nsiLogRegistryListDS";
    public static final String STATE_VIEW_PROPERTY = "stateViewProperty";
    public static final String NSI_CATALOG = "nsiCatalog";
    public static final String REQUEST_DATE_FROM_GUID = "requestDateFrom";
    public static final String REQUEST_DATE_TO_GUID = "requestDateTo";
    public static final String MESSAGE_GUID = "messageGuid";
    public static final String EVENT_TYPE = "eventType";
    public static final String OPERATION_TYPE = "operationType";
    public static final String MESSAGE_STATUS = "messageStatus";
    public static final String ENTITY_GUID = "entityGuid";
    public static final String ANY_XML_BODY_GUID = "anyXmlBodyGuid";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(NSI_CATALOG_LOG_DS, getNsiCatalogLogDS(), nsiCatalogLogDSHandler()))
                .addDataSource(NSISyncManager.instance().eventTypeOptionDSConfig())
                .addDataSource(NSISyncManager.instance().operationTypeOptionDSConfig())
                .addDataSource(NSISyncManager.instance().messageStatusOptionDSConfig())
                .addDataSource(NSISyncManager.instance().catalogTypeOptionDSConfig())
                .create();
    }

    @Bean
    public ColumnListExtPoint getNsiCatalogLogDS()
    {
        DateFormatter dateFormatterWithTimeMillisec = new DateFormatter("dd.MM.yyyy HH:mm:ss.SSS");
        return columnListExtPointBuilder(NSI_CATALOG_LOG_DS)
                .addColumn(publisherColumn("messageTime", FefuNsiLogRow.messageTime()).formatter(dateFormatterWithTimeMillisec)
                        .publisherLinkResolver(new IPublisherLinkResolver()
                        {
                            @Override
                            public Object getParameters(IEntity entity)
                            {
                                Map<String, Object> params = new HashMap();
                                params.put(UIPresenter.PUBLISHER_ID, entity.getId());
                                return params;
                            }

                            @Override
                            public String getComponentName(IEntity entity)
                            {
                                return NSISyncLogRowView.class.getSimpleName();
                            }
                        }).order().create())
                .addColumn(textColumn("directoryId", FefuNsiLogRow.directoryId()).order())
                .addColumn(textColumn("objectId", FefuNsiLogRow.objectId()).order())
                .addColumn(textColumn("operationType", FefuNsiLogRow.operationType()).order())
                .addColumn(textColumn("eventType", FefuNsiLogRow.eventType()).order())
                .addColumn(textColumn("messageId", FefuNsiLogRow.messageId()).order())
                        //.addColumn(textColumn("correlationId", FefuNsiLogRow.correlationId()).order())
                        //.addColumn(textColumn("parentId", FefuNsiLogRow.parentId()).order())
                .addColumn(textColumn("sourceId", FefuNsiLogRow.sourceId()).order())
                .addColumn(blockColumn("messageStatus", "messageStatusBlock").order())
                .addColumn(actionColumn("getPack", new Icon("template_save", "Получить пакет с описанием"), "onGetMessagePack"))
                .addColumn(actionColumn("reSend", new Icon("daemon", "Отправить повторно"), "onSendMessageAgain", alert("nsiLogRegistryListDS.reSend.alert")).disabled(FefuNsiLogRow.couldNotBeReSent().s()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> nsiCatalogLogDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                context.getSession().clear();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuNsiLogRow.class, "e").column("e")
                        .order(property("e", input.getEntityOrder().getColumnName()), input.getEntityOrder().getDirection());

                FefuNsiCatalogType catalogType = context.get(NSI_CATALOG);
                if (null != catalogType)
                {
                    String[] types = catalogType.getNsiCode().split(",");
                    List<String> typesList = new ArrayList<>();
                    for (String type : types) typesList.add(type.trim());
                    builder.where(in(property(FefuNsiLogRow.directoryId().fromAlias("e")), typesList));
                }

                Date requestDateFrom = context.get(REQUEST_DATE_FROM_GUID);
                if (null != requestDateFrom)
                {
                    builder.where(ge(property(FefuNsiLogRow.messageTime().fromAlias("e")), valueTimestamp(CoreDateUtils.getDayFirstTimeMoment(requestDateFrom))));
                }

                Date requestDateTo = context.get(REQUEST_DATE_TO_GUID);
                if (null != requestDateTo)
                {
                    builder.where(le(property(FefuNsiLogRow.messageTime().fromAlias("e")), valueTimestamp(CoreDateUtils.getNextDayFirstTimeMoment(requestDateTo, 1))));
                }

                String messageGuid = context.get(MESSAGE_GUID);
                if (null != messageGuid)
                {
                    builder.where(eq(property(FefuNsiLogRow.messageId().fromAlias("e")), value(messageGuid)));
                }

                IdentifiableWrapper eventTypeWrapper = context.get(EVENT_TYPE);
                String eventType = null != eventTypeWrapper ? (NSISyncManager.EVENT_TYPE_IN.equals(eventTypeWrapper.getId()) ? NSISyncManager.EVENT_TYPE_IN_CODE : NSISyncManager.EVENT_TYPE_OUT_CODE) : null;
                if (null != eventType)
                {
                    builder.where(eq(property(FefuNsiLogRow.eventType().fromAlias("e")), value(eventType)));
                }

                IdentifiableWrapper operationTypeWrapper = context.get(OPERATION_TYPE);
                String operationType = null != operationTypeWrapper ? operationTypeWrapper.getTitle() : null;
                if (null != operationType)
                {
                    builder.where(eq(property(FefuNsiLogRow.operationType().fromAlias("e")), value(operationType)));
                }

                IdentifiableWrapper messageStatusWrapper = context.get(MESSAGE_STATUS);
                if (null != messageStatusWrapper)
                {
                    Integer messageStatus = null;
                    if (NSISyncManager.MESSAGE_STATUS_UNKNOWN.equals(messageStatusWrapper.getId()))
                        messageStatus = FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_INITIAL.intValue();
                    else if (NSISyncManager.MESSAGE_STATUS_SUCCESS.equals(messageStatusWrapper.getId()))
                        messageStatus = FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_OK.intValue();
                    else if (NSISyncManager.MESSAGE_STATUS_WARNING.equals(messageStatusWrapper.getId()))
                        messageStatus = FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_WARN.intValue();
                    else if (NSISyncManager.MESSAGE_STATUS_ERROR.equals(messageStatusWrapper.getId()))
                        messageStatus = FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_ERROR.intValue();

                    if (null != messageStatus)
                    {
                        builder.where(eq(property(FefuNsiLogRow.messageStatus().fromAlias("e")), value(messageStatus)));
                    }
                }

                String entityGuid = context.get(ENTITY_GUID);
                if (null != entityGuid)
                {
                    builder.where(eq(property(FefuNsiLogRow.objectId().fromAlias("e")), value(entityGuid)));
                }

                String anyXmlBodyGuid = context.get(ANY_XML_BODY_GUID);
                if (null != anyXmlBodyGuid)
                {
                    builder.where(like(property(FefuNsiLogRow.messageBody().fromAlias("e")), value(CoreStringUtils.escapeLike(anyXmlBodyGuid, true).toLowerCase())));
                }

                /*List<DataWrapper> wrappersList = new ArrayList<>();
                for (FefuNsiCatalogType catalogType : catalogTypes)
                {
                    DataWrapper wrapper = new DataWrapper(catalogType.getId(), catalogType.getTitle(), catalogType);
                    wrapper.setProperty(SYNC_DISABLED, Boolean.FALSE);
                    if (FefuNsiDaemonDao.isEntitySyncInProcess(catalogType.getId()))
                    {
                        wrapper.setProperty(STATE_VIEW_PROPERTY, "Идет синхронизация");
                        wrapper.setProperty(SYNC_DISABLED, Boolean.TRUE);
                    } else if (FefuNsiDaemonDao.isEntityInQueueForSync(catalogType.getId()))
                    {
                        wrapper.setProperty(STATE_VIEW_PROPERTY, "Ожидает синхронизации");
                        wrapper.setProperty(SYNC_DISABLED, Boolean.TRUE);
                    } else if (null == catalogType.getSyncDateTime())
                    {
                        wrapper.setProperty(STATE_VIEW_PROPERTY, "Не синхронизировался");
                    } else
                    {
                        wrapper.setProperty(STATE_VIEW_PROPERTY, "Актуален");
                    }
                    //TODO: отображать ошибки синхронизации
                    wrappersList.add(wrapper);
                }*/

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }
}