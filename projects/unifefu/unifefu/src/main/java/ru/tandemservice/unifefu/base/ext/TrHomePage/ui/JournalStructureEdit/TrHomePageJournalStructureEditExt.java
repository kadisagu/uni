/* $Id$ */
package ru.tandemservice.unifefu.base.ext.TrHomePage.ui.JournalStructureEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalStructureEdit.TrHomePageJournalStructureEdit;

/**
 * @author Nikolay Fedorovskih
 * @since 13.02.2014
 */
@Configuration
public class TrHomePageJournalStructureEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrHomePageJournalStructureEditExtUI.class.getSimpleName();

    @Autowired
    private TrHomePageJournalStructureEdit _trHomePageJournalStructureEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trHomePageJournalStructureEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrHomePageJournalStructureEditExtUI.class))
                .create();
    }
}