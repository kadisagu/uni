/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.student.StudentPersonCard;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfPictureUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.uni.component.student.StudentPersonCard.IData;
import ru.tandemservice.uni.component.student.StudentPersonCard.IStudentPersonCardPrintFactory;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.StudentExtractGroupCodes;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.dao.student.FefuStudentExtractData;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public class StudentPersonCardPrintFactory implements IStudentPersonCardPrintFactory
{

    private Data _data = new Data();

    @Override
    public Data getData(){
        return _data;
    }

    public static class Data implements IData
    {
        PersonNextOfKin _mother;
        PersonNextOfKin _father;
        PersonNextOfKin _spouse;
        List<PersonNextOfKin> _children = new ArrayList<>();

        private RtfDocument _template;
        private String _fileName;
        private Student _student;
        private OrderData _orderData;
        private String education;

        public PersonNextOfKin getMother()
        {
            return _mother;
        }

        public void setMother(PersonNextOfKin mother)
        {
            _mother = mother;
        }

        public PersonNextOfKin getFather()
        {
            return _father;
        }

        public void setFather(PersonNextOfKin father)
        {
            _father = father;
        }

        public PersonNextOfKin getSpouse()
        {
            return _spouse;
        }

        public void setSpouse(PersonNextOfKin spouse)
        {
            _spouse = spouse;
        }

        public List<PersonNextOfKin> getChildren()
        {
            return _children;
        }

        public void setChildren(List<PersonNextOfKin> children)
        {
            _children = children;
        }

        @Override
        public String getFileName()
        {
            return _fileName;
        }

        @Override
        public void setFileName(String fileName)
        {
            this._fileName = fileName;
        }

        @Override
        public RtfDocument getTemplate()
        {
            return _template;
        }

        @Override
        public void setTemplate(RtfDocument document)
        {
            this._template = document;
        }

        @Override
        public Student getStudent()
        {
            return _student;
        }

        public void setStudent(Student student)
        {
            _student = student;
        }

        @Override
        public String[][] getPersonEducationData()
        {
            return new String[0][];
        }

        @Override
        public String[][] getRelativesData()
        {
            return new String[0][];
        }
    }


    private EnrollmentExtract _enrollmentExtract;
    private AbstractStudentExtract _enrollmentExtractByGroup;
    private AbstractStudentExtract _dirtyEnrollmentExtract;
    private Date _enrollmentOrderDate;
    private String _enrollmentOrderNumber;
    private String _enrollmentCourse;
    private List<PersonBenefit> _personBenefits;

    private List<Map<String, String>> _mainEduProgrammExtractsData = Lists.newArrayList();
    private List<Map<String, String>> _partComExtractsData = Lists.newArrayList();
    private List<Map<String, String>> _practiceExtractsData = Lists.newArrayList();
    private List<Map<String, String>> _orphanPaymentsData = Lists.newArrayList();

    public Date getEnrollmentOrderDate()
    {
        return _enrollmentOrderDate;
    }

    public void setEnrollmentOrderDate(Date enrollmentOrderDate)
    {
        _enrollmentOrderDate = enrollmentOrderDate;
    }

    public String getEnrollmentOrderNumber()
    {
        return _enrollmentOrderNumber;
    }

    public void setEnrollmentOrderNumber(String enrollmentOrderNumber)
    {
        _enrollmentOrderNumber = enrollmentOrderNumber;
    }

    public String getEnrollmentCourse()
    {
        return _enrollmentCourse;
    }

    public void setEnrollmentCourse(String enrollmentCourse)
    {
        _enrollmentCourse = enrollmentCourse;
    }

    public AbstractStudentExtract getEnrollmentExtractByGroup()
    {
        return _enrollmentExtractByGroup;
    }

    public void setEnrollmentExtractByGroup(AbstractStudentExtract enrollmentExtractByGroup)
    {
        _enrollmentExtractByGroup = enrollmentExtractByGroup;
    }

    public AbstractStudentExtract getDirtyEnrollmentExtract()
    {
        return _dirtyEnrollmentExtract;
    }

    public void setDirtyEnrollmentExtract(AbstractStudentExtract dirtyEnrollmentExtract)
    {
        _dirtyEnrollmentExtract = dirtyEnrollmentExtract;
    }

    public static Comparator ORDER_COMP = new Comparator<AbstractStudentExtract>()
    {
        @Override
        public int compare(AbstractStudentExtract o1, AbstractStudentExtract o2)
        {
            return o1.getParagraph().getOrder().getCommitDate().compareTo(o2.getParagraph().getOrder().getCommitDate());
        }
    };

    public EnrollmentExtract getEnrollmentExtract()
    {
        return _enrollmentExtract;
    }

    public void setEnrollmentExtract(EnrollmentExtract enrollmentExtract)
    {
        _enrollmentExtract = enrollmentExtract;
    }

    public List<PersonBenefit> getPersonBenefits()
    {
        return _personBenefits;
    }

    public void setPersonBenefits(List<PersonBenefit> personBenefits)
    {
        _personBenefits = personBenefits;
    }

    public List<Map<String, String>> getMainEduProgrammExtractsData()
    {
        return _mainEduProgrammExtractsData;
    }

    public void setMainEduProgrammExtractsData(List<Map<String, String>> mainEduProgrammExtractsData)
    {
        _mainEduProgrammExtractsData = mainEduProgrammExtractsData;
    }

    public List<Map<String, String>> getPartComExtractsData()
    {
        return _partComExtractsData;
    }

    public void setPartComExtractsData(List<Map<String, String>> partComExtractsData)
    {
        _partComExtractsData = partComExtractsData;
    }

    public List<Map<String, String>> getPracticeExtractsData()
    {
        return _practiceExtractsData;
    }

    public void setPracticeExtractsData(List<Map<String, String>> practiceExtractsData)
    {
        _practiceExtractsData = practiceExtractsData;
    }

    public List<Map<String, String>> getOrphanPaymentsData()
    {
        return _orphanPaymentsData;
    }

    public void setOrphanPaymentsData(List<Map<String, String>> orphanPaymentsData)
    {
        _orphanPaymentsData = orphanPaymentsData;
    }

    @Override
    public RtfDocument createCard()
    {
        RtfDocument document = getData().getTemplate().getClone();
        modifyByStudent(document);
        return document;
    }

    protected void resetPrintData()
    {
        getData().setChildren(new ArrayList<PersonNextOfKin>());
        getData().setMother(null);
        getData().setFather(null);
        getData().setSpouse(null);
        setEnrollmentCourse(null);
        setEnrollmentOrderDate(null);
        setEnrollmentOrderNumber(null);
        setDirtyEnrollmentExtract(null);
        setEnrollmentExtract(null);
    }

    @Override
    public void initPrintData(Student student, List<PersonEduInstitution> personEduInstitutionList, List<PersonNextOfKin> personNextOfKinList, Session session)
    {
        resetPrintData();
        getData().setStudent(student);
//        getData().setOrderData(DataAccessServices.dao().get(OrderData.class, OrderData.L_STUDENT, student));

        for (PersonNextOfKin kin : personNextOfKinList)
        {
            switch (kin.getRelationDegree().getCode())
            {
                case RelationDegreeCodes.MOTHER:
                    getData().setMother(kin);
                    break;
                case RelationDegreeCodes.FATHER:
                    getData().setFather(kin);
                    break;
                case RelationDegreeCodes.DAUGHTER:
                case RelationDegreeCodes.SON:
                    getData().getChildren().add(kin);
                    break;
                case RelationDegreeCodes.HUSBAND:
                case RelationDegreeCodes.WIFE:
                    getData().setSpouse(kin);
                    break;
            }
        }

        // ВНИМАНИЕ КОСТЫЛЬ!
        // По каким то причинам в ДВФУ имеется 3 разных вида выписок о зачислении:
        // 1. Те что наследуются от EnrollmentExtract - эти выписки подгружаются в DAO (вероятно это самые первые и самые правильные выписки, потому что раньше подгружались только они и никакие другие)
        // 2. Те что наследуются от AbstractStudentExtract и имеют тип выписки входящий в группу StudentExtractGroupCodes.ENROLLMENT. - эти выписки подгружаются кодом ниже
        // 3. Те что наследуются от AbstractStudentExtract, не входят ни в какую группу, имеются следующую иерархию:
        // "Прочие приказы"/"Приказы по основной образовательной программе"/"Приказы о зачислении", код у этих типов приказов зачисления
        // сгенерирован системой и разный на разных версиях БД (сам не знаю как это возможно). - эти выписки подгружаются кодом ниже

        // второй вид выписок о зачислении
        DQLSelectBuilder extractTypesFromGroups = new DQLSelectBuilder()
                .fromEntity(StudentExtractTypeToGroup.class, "groups")
                .column(property(StudentExtractTypeToGroup.type().fromAlias("groups")))
                .where(eq(property(StudentExtractTypeToGroup.group().code().fromAlias("groups")), value(StudentExtractGroupCodes.ENROLLMENT)));

        DQLSelectBuilder enrExtractByGroup = new DQLSelectBuilder()
                .top(1)
                .fromEntity(AbstractStudentExtract.class, "extracts")
                .column("extracts")
                .where(eq(property(AbstractStudentExtract.entity().fromAlias("extracts")), value(student)))
                .where(eq(property(AbstractStudentExtract.paragraph().order().state().code().fromAlias("extracts")), value(OrderStatesCodes.FINISHED)))
                .where(in(property(AbstractStudentExtract.type().fromAlias("extracts")), extractTypesFromGroups.buildQuery()))
                .order(property(AbstractStudentExtract.paragraph().order().commitDate().fromAlias("extracts")), OrderDirection.desc);

        AbstractStudentExtract extractByGroup = enrExtractByGroup.createStatement(session).uniqueResult();
        setEnrollmentExtractByGroup(extractByGroup);

        // третий вид выписок о зачислении
        DQLSelectBuilder enrExtract1Builder = new DQLSelectBuilder()
                .top(1)
                .fromEntity(AbstractStudentExtract.class, "e")
                .where(eq(property("e", AbstractStudentExtract.entity().id()), DQLExpressions.value(getData().getStudent().getId())))
                .where(eq(property(AbstractStudentExtract.paragraph().order().state().code().fromAlias("e")), value(OrderStatesCodes.FINISHED)))
                .where(eq(property(AbstractStudentExtract.type().title().fromAlias("e")), value("Приказы о зачислении")))
                .order(property(AbstractStudentExtract.paragraph().order().commitDate().fromAlias("e")), OrderDirection.desc);
        AbstractStudentExtract dirtyExtract = enrExtract1Builder.createStatement(session).uniqueResult();
        setDirtyEnrollmentExtract(dirtyExtract);
    }

    protected void prepareEnrollmentData()
    {
        // ВНИМАНИЕ КОСТЫЛЬ! Суть и причина костыля описана выше.
        if (getEnrollmentExtract() != null)
        {
            setEnrollmentCourse(getEnrollmentExtract().getCourse().getTitle());
            setEnrollmentOrderDate(getEnrollmentExtract().getOrder().getCommitDate());
            setEnrollmentOrderNumber(getEnrollmentExtract().getOrder().getNumber());
        }
        else if (getEnrollmentExtractByGroup() != null)
        {
            setEnrollmentCourse(getEnrollmentExtractByGroup().getCourseStr());
            setEnrollmentOrderDate(getEnrollmentExtractByGroup().getParagraph().getOrder().getCommitDate());
            setEnrollmentOrderNumber(getEnrollmentExtractByGroup().getParagraph().getOrder().getNumber());
        }
        else if (getDirtyEnrollmentExtract()!= null)
        {
            setEnrollmentCourse(getDirtyEnrollmentExtract().getCourseStr());
            setEnrollmentOrderDate(getDirtyEnrollmentExtract().getParagraph().getOrder().getCommitDate());
            setEnrollmentOrderNumber(getDirtyEnrollmentExtract().getParagraph().getOrder().getNumber());
        }
    }

    protected void modifyByStudent(RtfDocument document)
    {
        prepareEnrollmentData();
        Student student = getData().getStudent();
        RtfInjectModifier modifier = new RtfInjectModifier();
        EducationLevelsHighSchool educationLevelsHighSchool = student.getEducationOrgUnit().getEducationLevelHighSchool();
        StructureEducationLevels levelType = educationLevelsHighSchool.getEducationLevel().getLevelType();
        OrgUnit formativeOrgUnit = student.getEducationOrgUnit().getFormativeOrgUnit();
        DatabaseFile photo = student.getPerson().getIdentityCard().getPhoto();
        if (null != photo && null != photo.getContent() && photo.getContent().length > 0)
            RtfPictureUtil.insertPicture(document, photo.getContent(), "photo", 2000L, 1500L);
        else
            modifier.put("photo", "ФОТО");
        modifier.put("group", getProperty(student, "group.title"))
                .put("status", getProperty(student, "status.title"))
                .put("recBookNum", getProperty(student, "bookNumber"))
                .put("personalNum", StringUtils.isEmpty(student.getPerNumber()) ? "" : student.getPerNumber())
                .put("sex", student.getPerson().getIdentityCard().getSex().isMale() ? "муж." : "жен.")
                .put("lastName", getProperty(student, "person.identityCard.lastName"))
                .put("firstName", getProperty(student, "person.identityCard.firstName"))
                .put("middleName", getProperty(student, "person.identityCard.middleName"))
                .put("educationOrgUnit", getProperty(student, "educationOrgUnit.title"))
                .put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(getData().getStudent().getPerson().getIdentityCard().getBirthDate()))
                .put("birthPlace", getProperty(student, "person.identityCard.birthPlace"))
                .put("citizenship", getProperty(student, "person.identityCard.citizenship.title"))
                .put("nationality", getProperty(student, "person.identityCard.nationality.title"))
                .put("addressRegistration", getProperty(student, "person.identityCard.address.titleWithFlat"))
                .put("maritalStatus", getProperty(student, "person.familyStatus.title"))
                .put("addressTitle", getProperty(student, "person.address.titleWithFlat"))
                .put("homePhone", student.getPerson().getContactData().getPhoneFact())
                .put("mobilePhone", student.getPerson().getContactData().getPhoneMobile())
                .put("passportSeria", getProperty(student, "person.identityCard.seria"))
                .put("passportNum", getProperty(student, "person.identityCard.number"))
                .put("passportIssuanceDate", getDateProperty(student, "person.identityCard.issuanceDate"))
                .put("passportIssuancePlace", getProperty(student, "person.identityCard.issuancePlace"))
                .put("enrDevelopCondition", getProperty(student, Student.educationOrgUnit().developCondition().title().s()))
                .put("enrDevelopTech", getProperty(student, Student.educationOrgUnit().developTech().title().s()))
                .put("enrTargetAdmission", getBooleanProperty(student, Student.targetAdmission().getPath()))
                .put("enrCompensationType", getProperty(student, Student.compensationType().shortTitle().s()))
                .put("enrQualification", getProperty(student, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualificationTitle().s()))
                .put("enrDevelopForm", getProperty(student, Student.educationOrgUnit().developForm().title().s()))
                .put("enrDevelopPeriod", getProperty(student, Student.educationOrgUnit().developPeriod().title().s()))
                .put("eduLevel", educationLevelsHighSchool.getEducationLevel().getProgramSubjectTitleWithCode())
                .put("specOrProfile", educationLevelsHighSchool.getEducationLevel().getProgramSpecializationTitle())
                .put("formativeOrgUnit", formativeOrgUnit.getNominativeCaseTitle() == null ? formativeOrgUnit.getTitle() : formativeOrgUnit.getNominativeCaseTitle())
                .put("territorialOrgUnit", student.getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialTitle())
                .put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));


            modifier.put("enrCourse", getEnrollmentCourse() != null ? getEnrollmentCourse() : "");
            modifier.put("enrOrderNum", getEnrollmentOrderNumber());
            modifier.put("enrOrderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(getEnrollmentOrderDate()));


        if(null != student.getPerson().getPersonEduInstitution())
        {
            PersonEduInstitution personEduInstitution = student.getPerson().getPersonEduInstitution();
            String serial = personEduInstitution.getSeria() == null ? "" : personEduInstitution.getSeria();
            String delimiter = personEduInstitution.getSeria() == null ? "" : " ";
            String number = personEduInstitution.getNumber() == null ? "" : personEduInstitution.getNumber();
            modifier.put("educationDocNum", serial + delimiter + number);
            if(null != personEduInstitution.getEduInstitution())
                modifier.put("education", personEduInstitution.getEduInstitution().getTitle() + ", " + personEduInstitution.getYearEnd());
            else
                modifier.put("education", "");
        }
        else
        {
            modifier.put("educationDocNum", "");
            modifier.put("education", "");
        }

//        if(null != _personBenefits && !_personBenefits.isEmpty())
//        {
//            List<String> benefitTitles = CommonBaseEntityUtil.getPropertiesList(_personBenefits, PersonBenefit.benefit().title());
//            modifier.put("benefits", StringUtils.join(benefitTitles, ", "));
//        }
//        else modifier.put("benefits", "");
        modifier.put("snils", StringUtils.isEmpty(student.getPerson().getSnilsNumber()) ? "" : student.getPerson().getSnilsNumber());
        modifier.put("residenceBeforeEntrance", "");
        if(null != student.getPerson().getIdentityCard().getAddress())
        {
            modifier.put("regAddress", student.getPerson().getIdentityCard().getAddress().getTitleWithFlat());
        }
        else
        {
            modifier.put("regAddress", "");
        }

        appendNextOfKinData(modifier, getData().getFather(), "father");
        appendNextOfKinData(modifier, getData().getMother(), "mother");
        appendNextOfKinData(modifier, getData().getSpouse(), "spouse");
        List<String[]> children = new ArrayList<>();
        for (PersonNextOfKin ch : getData().getChildren())
        {
            children.add(new String[]{ch.getFio(), null, DateFormatter.DEFAULT_DATE_FORMATTER.format(ch.getBirthDate())});
        }
        if (children.size() == 0)
            children.add(new String[]{"", null, ""});
        new RtfTableModifier()
                // Регистрация прохождения учебы по основной образовательной программе T1
                .put("T1", getMainEduProgramTableData(_mainEduProgrammExtractsData))
                .put("T2", children.toArray(new String[children.size()][1]))
//                .put("T2", FefuOrderData.getOrphanPaymentsTableData(_orphanPaymentsData))
//                .put("T3", FefuOrderData.getPracticeExtractsTableData(_practiceExtractsData))
//                .put("T4", FefuOrderData.getPartComExtractsTableData(_partComExtractsData))
                .modify(document);

        modifier.modify(document);
    }

    private void appendNextOfKinData(RtfInjectModifier modifier, PersonNextOfKin relative, String prefix)
    {
        String fio = "";
        String birthDate = "";
        String workPlace = "";
        String residence = "";
        String phone = "";
        if (relative != null)
        {
            fio = relative.getFio();
            birthDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(relative.getBirthDate());
            workPlace = relative.getEmploymentPlace();
            residence = relative.getAddress() == null ? "" : relative.getAddress().getShortTitleWithSettlement();
            phone = relative.getPhones();
        }
        modifier.put(prefix + "Fio", fio);
        modifier.put(prefix + "BirthDate", birthDate);
        modifier.put(prefix + "WorkPlace", workPlace);
        modifier.put(prefix + "Residence", residence);
        modifier.put(prefix + "Phone", phone);
    }

    protected static String getBooleanProperty(Student student, String path)
    {
        Boolean value = (Boolean)student.getProperty(path);
        return  value != null ? (value ? "Да" : "Нет") : "";
    }

    protected static String getDateProperty(Student student, String path)
    {
        Date value = (Date) student.getProperty(path);
        return  value != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(value) : "";
    }

    protected static String getProperty(Student student, String path)
    {
        String value = (String)student.getProperty(path);
        return  value != null ? value : "";
    }


    public static String[][] getMainEduProgramTableData(List<Map<String, String>> extractsData)
    {
        if(extractsData.isEmpty()) return new String[1][];
        List<String[]> extractTableData = Lists.newArrayList();
        for(Map<String, String> extractData: extractsData)
        {
            List<String> tableData = Lists.newArrayList();
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_COURSE));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_EDU_OU));
            tableData.add(extractData.get(FefuStudentExtractData.BASE_PROP_ORDER_COMMIT_DATE));
            tableData.add(extractData.get(FefuStudentExtractData.BASE_PROP_ORDER_NUM));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_GEN_COMMENT));
            extractTableData.add(tableData.toArray(new String[0]));
        }
        return extractTableData.toArray(new String[0][0]);
    }

}
