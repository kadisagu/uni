/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.EditCourseEventRelation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.BBCourseDSHandler;

/**
 * @author Nikolay Fedorovskih
 * @since 24.04.2014
 */
@Configuration
public class BlackboardEditCourseEventRelation extends BusinessComponentManager
{
    public static final String COURSE_DS = "courseDS";
    public static final String BB_EVENT_DS = "bbEventDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return new BBCourseDSHandler(getName(), false);
    }

}