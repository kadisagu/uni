/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.settings.FefuStudentPaymentsAndBonusesSizePub;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unifefu.dao.student.FefuAcademicGrantSizeDAO;
import ru.tandemservice.unifefu.entity.FefuStudentPaymentsAndBonusesSizeSetting;

/**
 * @author Alexander Zhebko
 * @since 12.03.2013
 */
public class DAO extends UniDao<Model> implements IUniDao<Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        FefuStudentPaymentsAndBonusesSizeSetting setting = FefuAcademicGrantSizeDAO.getGrantSettings(this);
        if (setting != null)
        {
            model.setDefaultGrantSize(setting.getDefaultGrantSize());
            model.setDefaultGroupManagerBonusSize(setting.getDefaultGroupManagerBonusSize());
            model.setDefaultSocialGrantSize(setting.getDefaultSocialGrantSize());
        }
    }
}