package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь компетенции с ГОС (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuCompetence2EppStateEduStandardRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel";
    public static final String ENTITY_NAME = "fefuCompetence2EppStateEduStandardRel";
    public static final int VERSION_HASH = -1524578562;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_STANDARD = "eduStandard";
    public static final String L_FEFU_COMPETENCE = "fefuCompetence";
    public static final String P_NUMBER = "number";
    public static final String P_GROUP_NUMBER = "groupNumber";

    private EppStateEduStandard _eduStandard;     // ГОС
    private FefuCompetence _fefuCompetence;     // Компетенция (ДВФУ)
    private int _number;     // Порядковый номер компетенции
    private int _groupNumber;     // Порядковый номер компетенции в группе

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ГОС. Свойство не может быть null.
     */
    @NotNull
    public EppStateEduStandard getEduStandard()
    {
        return _eduStandard;
    }

    /**
     * @param eduStandard ГОС. Свойство не может быть null.
     */
    public void setEduStandard(EppStateEduStandard eduStandard)
    {
        dirty(_eduStandard, eduStandard);
        _eduStandard = eduStandard;
    }

    /**
     * @return Компетенция (ДВФУ). Свойство не может быть null.
     */
    @NotNull
    public FefuCompetence getFefuCompetence()
    {
        return _fefuCompetence;
    }

    /**
     * @param fefuCompetence Компетенция (ДВФУ). Свойство не может быть null.
     */
    public void setFefuCompetence(FefuCompetence fefuCompetence)
    {
        dirty(_fefuCompetence, fefuCompetence);
        _fefuCompetence = fefuCompetence;
    }

    /**
     * @return Порядковый номер компетенции. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Порядковый номер компетенции. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Порядковый номер компетенции в группе. Свойство не может быть null.
     */
    @NotNull
    public int getGroupNumber()
    {
        return _groupNumber;
    }

    /**
     * @param groupNumber Порядковый номер компетенции в группе. Свойство не может быть null.
     */
    public void setGroupNumber(int groupNumber)
    {
        dirty(_groupNumber, groupNumber);
        _groupNumber = groupNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuCompetence2EppStateEduStandardRelGen)
        {
            setEduStandard(((FefuCompetence2EppStateEduStandardRel)another).getEduStandard());
            setFefuCompetence(((FefuCompetence2EppStateEduStandardRel)another).getFefuCompetence());
            setNumber(((FefuCompetence2EppStateEduStandardRel)another).getNumber());
            setGroupNumber(((FefuCompetence2EppStateEduStandardRel)another).getGroupNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuCompetence2EppStateEduStandardRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuCompetence2EppStateEduStandardRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuCompetence2EppStateEduStandardRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduStandard":
                    return obj.getEduStandard();
                case "fefuCompetence":
                    return obj.getFefuCompetence();
                case "number":
                    return obj.getNumber();
                case "groupNumber":
                    return obj.getGroupNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduStandard":
                    obj.setEduStandard((EppStateEduStandard) value);
                    return;
                case "fefuCompetence":
                    obj.setFefuCompetence((FefuCompetence) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "groupNumber":
                    obj.setGroupNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduStandard":
                        return true;
                case "fefuCompetence":
                        return true;
                case "number":
                        return true;
                case "groupNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduStandard":
                    return true;
                case "fefuCompetence":
                    return true;
                case "number":
                    return true;
                case "groupNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduStandard":
                    return EppStateEduStandard.class;
                case "fefuCompetence":
                    return FefuCompetence.class;
                case "number":
                    return Integer.class;
                case "groupNumber":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuCompetence2EppStateEduStandardRel> _dslPath = new Path<FefuCompetence2EppStateEduStandardRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuCompetence2EppStateEduStandardRel");
    }
            

    /**
     * @return ГОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel#getEduStandard()
     */
    public static EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
    {
        return _dslPath.eduStandard();
    }

    /**
     * @return Компетенция (ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel#getFefuCompetence()
     */
    public static FefuCompetence.Path<FefuCompetence> fefuCompetence()
    {
        return _dslPath.fefuCompetence();
    }

    /**
     * @return Порядковый номер компетенции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Порядковый номер компетенции в группе. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel#getGroupNumber()
     */
    public static PropertyPath<Integer> groupNumber()
    {
        return _dslPath.groupNumber();
    }

    public static class Path<E extends FefuCompetence2EppStateEduStandardRel> extends EntityPath<E>
    {
        private EppStateEduStandard.Path<EppStateEduStandard> _eduStandard;
        private FefuCompetence.Path<FefuCompetence> _fefuCompetence;
        private PropertyPath<Integer> _number;
        private PropertyPath<Integer> _groupNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ГОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel#getEduStandard()
     */
        public EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
        {
            if(_eduStandard == null )
                _eduStandard = new EppStateEduStandard.Path<EppStateEduStandard>(L_EDU_STANDARD, this);
            return _eduStandard;
        }

    /**
     * @return Компетенция (ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel#getFefuCompetence()
     */
        public FefuCompetence.Path<FefuCompetence> fefuCompetence()
        {
            if(_fefuCompetence == null )
                _fefuCompetence = new FefuCompetence.Path<FefuCompetence>(L_FEFU_COMPETENCE, this);
            return _fefuCompetence;
        }

    /**
     * @return Порядковый номер компетенции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(FefuCompetence2EppStateEduStandardRelGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Порядковый номер компетенции в группе. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel#getGroupNumber()
     */
        public PropertyPath<Integer> groupNumber()
        {
            if(_groupNumber == null )
                _groupNumber = new PropertyPath<Integer>(FefuCompetence2EppStateEduStandardRelGen.P_GROUP_NUMBER, this);
            return _groupNumber;
        }

        public Class getEntityClass()
        {
            return FefuCompetence2EppStateEduStandardRel.class;
        }

        public String getEntityName()
        {
            return "fefuCompetence2EppStateEduStandardRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
