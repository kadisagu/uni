/* $Id$ */
package ru.tandemservice.unifefu.dao.mdbio;

import com.healthmarketscience.jackcess.Database;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.io.IOException;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 09.10.2012
 */
public interface IFefuStudentIOAddDataDao
{
    SpringBeanCache<IFefuStudentIOAddDataDao> instance = new SpringBeanCache<>(IFefuStudentIOAddDataDao.class.getName());

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Map<String, Long> doImport_FefuStudentAddData(Database mdb) throws IOException;
}