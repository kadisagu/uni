/* $Id$ */
package ru.tandemservice.unifefu;

import java.io.File;
import java.io.FileFilter;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 10.04.2013
 */
public class Test
{
    public static final Map<String, String> MONTH_NAME_REPLACEMENT_STRINGS = new HashMap<>();

    static
    {
        MONTH_NAME_REPLACEMENT_STRINGS.put("Jan", "01");
        MONTH_NAME_REPLACEMENT_STRINGS.put("Feb", "02");
        MONTH_NAME_REPLACEMENT_STRINGS.put("Mar", "03");
        MONTH_NAME_REPLACEMENT_STRINGS.put("Apr", "04");
        MONTH_NAME_REPLACEMENT_STRINGS.put("May", "05");
        MONTH_NAME_REPLACEMENT_STRINGS.put("Jun", "06");
        MONTH_NAME_REPLACEMENT_STRINGS.put("Jul", "07");
        MONTH_NAME_REPLACEMENT_STRINGS.put("Aug", "08");
        MONTH_NAME_REPLACEMENT_STRINGS.put("Sep", "09");
        MONTH_NAME_REPLACEMENT_STRINGS.put("Oct", "10");
        MONTH_NAME_REPLACEMENT_STRINGS.put("Nov", "11");
        MONTH_NAME_REPLACEMENT_STRINGS.put("Dec", "12");
    }

    public static void main(String[] args) throws Exception
    {
        File dir = new File("D:/!!!/!!! Update server logs");

        FileFilter filefilter = new FileFilter()
        {
            public boolean accept(File file)
            {
                return file.getName().endsWith(".txt") || file.getName().endsWith(".log") || file.getName().endsWith(".out");
            }
        };

        List<UpdateServerAccessAction> accessActionList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss Z");
        Set<String> actionTypes = new HashSet<>();
        Map<String, Set<String>> actionCommandTypesMap = new HashMap<>();

        for (File item : dir.listFiles(filefilter))
        {
            RandomAccessFile file = new RandomAccessFile(item.getAbsolutePath(), "r");

            if (item.getName().startsWith("localhost_access_log."))
            {   /*
                System.out.println("\n" + item.getName());

                while (file.getFilePointer() < file.length())
                {
                    String line = file.readLine().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "");
                    System.out.println(line);

                    String[] elems = line.split(" ");

                    for (Map.Entry<String, String> entry : MONTH_NAME_REPLACEMENT_STRINGS.entrySet())
                    {
                        elems[3] = elems[3].replace(entry.getKey(), entry.getValue());
                    }
                    Date date = sdf.parse(elems[3] + " " + elems[4]);

                    String[] protocol = elems[7].split("/");
                    int resultCode = Integer.valueOf(elems[8]);

                    Long recievedDataSize = null;
                    try
                    {
                        recievedDataSize = Long.valueOf(elems[9]);
                    } catch (NumberFormatException ex)
                    {
                    }

                    accessActionList.add(new UpdateServerAccessAction(elems[0], elems[1], elems[2], date, elems[4], elems[5], elems[6].replace("/", ""), protocol[0], protocol[1], resultCode, recievedDataSize));
                } */
            } else
            {
                System.out.println("\n" + item.getName());

                while (file.getFilePointer() < file.length())
                {
                    String line = file.readLine();
                    System.out.println(line);

                    String[] elems = line.split(" ");
                    if(elems.length > 4 && elems[0].length() == 10 && elems[1].length() == 12)
                    {
                        actionTypes.add(elems[3]);

                        if(line.contains("Action '"))
                        {
                            int commandIdx = line.indexOf("Action '") + 8;
                            String command = line.substring(commandIdx, line.indexOf("'", commandIdx + 1));
                            Set<String> commandSet = actionCommandTypesMap.get(elems[3]);
                            if(null == commandSet) commandSet = new HashSet<>();
                            commandSet.add(command);
                            actionCommandTypesMap.put(elems[3], commandSet);
                        }
                    }
                }
            }
            file.close();
        }
        System.out.println("Done.");
    }

    private static class UpdateServerAccessAction
    {
        private String _ip;
        private String _attr1;
        private String _attr2;
        private Date _date;
        private String _timeZone;
        private String _methodType;
        private String _actionType;
        private String _protocolType;
        private String _protocolVersion;
        private int _resultCode;
        private Long _recievedDataSize;

        private UpdateServerAccessAction(String ip, String attr1, String attr2, Date date, String timeZone, String methodType, String actionType, String protocolType, String protocolVersion, int resultCode, Long recievedDataSize)
        {
            _ip = ip;
            _attr1 = attr1;
            _attr2 = attr2;
            _date = date;
            _timeZone = timeZone;
            _methodType = methodType;
            _actionType = actionType;
            _protocolType = protocolType;
            _protocolVersion = protocolVersion;
            _resultCode = resultCode;
            _recievedDataSize = recievedDataSize;
        }

        public String getIp()
        {
            return _ip;
        }

        public String getAttr1()
        {
            return _attr1;
        }

        public String getAttr2()
        {
            return _attr2;
        }

        public Date getDate()
        {
            return _date;
        }

        public String getTimeZone()
        {
            return _timeZone;
        }

        public String getMethodType()
        {
            return _methodType;
        }

        public String getActionType()
        {
            return _actionType;
        }

        public String getProtocolType()
        {
            return _protocolType;
        }

        public String getProtocolVersion()
        {
            return _protocolVersion;
        }

        public int getResultCode()
        {
            return _resultCode;
        }

        public Long getRecievedDataSize()
        {
            return _recievedDataSize;
        }
    }

    private static class UpdateServerUpdateAction
    {
        // register
        private Date _date;


    }

    private static class UpdateServerUploadAction
    {
        private Date _date;


    }

    private static class UpdateServerInstallAction
    {
        private Date _date;


    }
}