/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e15;

import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 13.11.2013
 */
public class EduEnrAsTransferStuExtractDao extends ru.tandemservice.movestudent.component.modularextract.e15.EduEnrAsTransferStuExtractDao
{
    @Override
    public void doCommit(EduEnrAsTransferStuExtract extract, Map parameters)
    {
        super.doCommit(extract, parameters);

        StudentFefuExt studentFefuExt = getByNaturalId(new StudentFefuExt.NaturalId(extract.getEntity()));
        if (null == studentFefuExt)
        {
            studentFefuExt = new StudentFefuExt();
            studentFefuExt.setStudent(extract.getEntity());
        }
        if (null == extract.getPrevOrderEntryDate())
        {
            extract.setPrevOrderEntryDate(studentFefuExt.getEntranceDate());
        }
        studentFefuExt.setEntranceDate(null != extract.getEntryDate() ? extract.getEntryDate() : extract.getParagraph().getOrder().getCommitDate());
        getSession().saveOrUpdate(studentFefuExt);
    }

    @Override
    public void doRollback(EduEnrAsTransferStuExtract extract, Map parameters)
    {
        super.doRollback(extract, parameters);
        StudentFefuExt studentFefuExt = getByNaturalId(new StudentFefuExt.NaturalId(extract.getEntity()));
        studentFefuExt.setEntranceDate(extract.getPrevOrderEntryDate());
        getSession().saveOrUpdate(studentFefuExt);
    }
}
