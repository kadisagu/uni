/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleDaily.ui.OrgUnitPrintFormList;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.OrgUnitPrintFormList.SppScheduleDailyOrgUnitPrintFormList;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.OrgUnitPrintFormList.SppScheduleDailyOrgUnitPrintFormListUI;

/**
 * @author Igor Belanov
 * @since 05.09.2016
 */
public class SppScheduleDailyOrgUnitPrintFormListExtUI extends UIAddon
{
    public SppScheduleDailyOrgUnitPrintFormListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleDailyOrgUnitPrintFormList.PRINT_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(getParentPresenter().getSettings().getAsMap(true, "admin"));
        }
    }

    private SppScheduleDailyOrgUnitPrintFormListUI getParentPresenter()
    {
        return getPresenter();
    }
}
