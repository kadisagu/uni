/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrgUnitList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrgUnitAddEdit.FefuSettingsOrgUnitAddEdit;
import ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrgUnitAddEdit.FefuSettingsOrgUnitAddEditUI;

/**
 * @author Dmitry Seleznev
 * @since 30.07.2013
 */
public class FefuSettingsOrgUnitListUI extends UIPresenter
{
    public void onClickAdd()
    {
        _uiActivation.asRegionDialog(FefuSettingsOrgUnitAddEdit.class).activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(FefuSettingsOrgUnitAddEdit.class)
                .parameter(FefuSettingsOrgUnitAddEditUI.ORDER_SETTINGS_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}