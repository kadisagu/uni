
package ru.tandemservice.unifefu.ws.blackboard.course.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="adminCategory" type="{http://course.ws.blackboard/xsd}CategoryVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "adminCategory"
})
@XmlRootElement(name = "saveOrgCategory")
public class SaveOrgCategory {

    @XmlElementRef(name = "adminCategory", namespace = "http://course.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<CategoryVO> adminCategory;

    /**
     * Gets the value of the adminCategory property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CategoryVO }{@code >}
     *     
     */
    public JAXBElement<CategoryVO> getAdminCategory() {
        return adminCategory;
    }

    /**
     * Sets the value of the adminCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CategoryVO }{@code >}
     *     
     */
    public void setAdminCategory(JAXBElement<CategoryVO> value) {
        this.adminCategory = ((JAXBElement<CategoryVO> ) value);
    }

}
