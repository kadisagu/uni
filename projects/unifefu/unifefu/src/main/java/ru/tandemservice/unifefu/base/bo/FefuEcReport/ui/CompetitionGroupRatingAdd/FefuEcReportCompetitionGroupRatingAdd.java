/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.CompetitionGroupRatingAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantRequestData.EntrantRequestDataBlock;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.CompetitionGroupDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.EnrCampaignFormativeOrgUnitDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.EnrCampaignTerritorialOrgUnitDSHandler;

import java.util.Arrays;

/**
 * @author Nikolay Fedorovskih
 * @since 10.07.2013
 */
@Configuration
public class FefuEcReportCompetitionGroupRatingAdd extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_PARAM = EnrCampaignFormativeOrgUnitDSHandler.ENROLLMENT_CAMPAIGN_PROP;
    public static final String QUALIFICATIONS_PARAM = CompetitionGroupDSHandler.QUALIFICATIONS_PROP;
    public static final String FORMATIVE_ORGUNIT_PARAM = CompetitionGroupDSHandler.FORMATIVE_ORGUNIT_PROP;
    public static final String TERRITORIAL_ORGUNIT_PARAM = CompetitionGroupDSHandler.TERRITORIAL_ORGUNIT_PROP;

    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String COMPETITION_GROUP_DS = "competitionGroupDS";
    public static final String QUALIFICATION_DS = "qualificationDS";
    public static final String FORMATIVE_ORGUNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_ORGUNIT_DS = "territorialOrgUnitDS";
    public static final String BOOLEAN_DS = "booleanDS";

    public static final DataWrapper BOOLEAN_NO = new DataWrapper(0L, "Нет");
    public static final DataWrapper BOOLEAN_YES = new DataWrapper(1L, "Да");

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(selectDS(COMPENSATION_TYPE_DS, compensationTypeDSHandler()).addColumn(CompensationType.P_SHORT_TITLE))
                .addDataSource(selectDS(QUALIFICATION_DS, qualificationDSHandler()))
                .addDataSource(selectDS(COMPETITION_GROUP_DS, competitionGroupDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORGUNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(TERRITORIAL_ORGUNIT_DS, territorialOrgUnitDSHandler()))
                .addDataSource(selectDS(BOOLEAN_DS, boolDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> compensationTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), CompensationType.class).order(CompensationType.code());
    }

    @Bean
    public IDefaultComboDataSourceHandler qualificationDSHandler()
    {
        return EntrantRequestDataBlock.createQualificationDS(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> competitionGroupDSHandler()
    {
        return new CompetitionGroupDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new EnrCampaignFormativeOrgUnitDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> territorialOrgUnitDSHandler()
    {
        return new EnrCampaignTerritorialOrgUnitDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> boolDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(BOOLEAN_NO, BOOLEAN_YES));
    }
}