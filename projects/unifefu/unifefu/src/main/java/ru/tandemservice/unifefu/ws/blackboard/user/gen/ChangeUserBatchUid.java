
package ru.tandemservice.unifefu.ws.blackboard.user.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="originalBatchUid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="batchUid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "originalBatchUid",
    "batchUid"
})
@XmlRootElement(name = "changeUserBatchUid")
public class ChangeUserBatchUid {

    @XmlElementRef(name = "originalBatchUid", namespace = "http://user.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> originalBatchUid;
    @XmlElementRef(name = "batchUid", namespace = "http://user.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> batchUid;

    /**
     * Gets the value of the originalBatchUid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOriginalBatchUid() {
        return originalBatchUid;
    }

    /**
     * Sets the value of the originalBatchUid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOriginalBatchUid(JAXBElement<String> value) {
        this.originalBatchUid = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the batchUid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBatchUid() {
        return batchUid;
    }

    /**
     * Sets the value of the batchUid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBatchUid(JAXBElement<String> value) {
        this.batchUid = ((JAXBElement<String> ) value);
    }

}
