/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e51;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.entity.StudentCardLossPenaltyStuExtract;

/**
 * @author Ekaterina Zvereva
 * @since 30.12.2014
 */
public class StudentCardLossPenaltyStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e51.StudentCardLossPenaltyStuExtractPrint
{
   private static String WITH_REPRIMAND_TEXT = " объявить выговор за утерю студенческого билета.\n Дирекции ";
    @Override
    public void additionalModify(RtfInjectModifier modifier, StudentCardLossPenaltyStuExtract extract)
    {
        modifier.put("fefuReprimand", extract.isWithoutReprimand()? "": WITH_REPRIMAND_TEXT);
        if (extract.isWithoutReprimand())
            modifier.put("formativeOrgUnitStrWithTerritorial_G", "");
    }
}
