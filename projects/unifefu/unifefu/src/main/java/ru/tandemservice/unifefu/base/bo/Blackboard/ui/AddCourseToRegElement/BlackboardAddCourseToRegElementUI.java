/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.AddCourseToRegElement;

import com.google.common.base.Objects;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.base.bo.Blackboard.BlackboardManager;
import ru.tandemservice.unifefu.ws.blackboard.course.BBCourseUtils;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.CourseVO;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * @author Nikolay Fedorovskih
 * @since 07.03.2014
 */
@Input({
               @Bind(key = BlackboardAddCourseToRegElementUI.REGISTRY_ELEMENT_CONTEXT_PARAM, binding = "registryElement", required = true)
       })
public class BlackboardAddCourseToRegElementUI extends UIPresenter
{
    public static final String REGISTRY_ELEMENT_CONTEXT_PARAM = "regElement";

    private IEntity _action;
    private boolean _actionSelected;
    private EppRegistryElement _registryElement;
    private String _courseTitle;
    private String _courseDescription;
    private PpsEntry _ppsEntry;

    @Override
    public void onComponentRefresh()
    {
        setCourseTitle(Objects.firstNonNull(getRegistryElement().getFullTitle(), getRegistryElement().getTitle()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (getAction() != null)
        {
            dataSource.put(BlackboardAddCourseToRegElement.PPS_ENTRY_PARAM, getPpsEntry());
            dataSource.put(BlackboardAddCourseToRegElement.REG_ELEMENT_PARAM, getRegistryElement());
        }
    }

    public void onClickSave()
    {
        if (BlackboardAddCourseToRegElement.CREATE_NEW_COURSE_ITEM.getId().equals(getAction().getId()))
        {
            BBCourseUtils.createNewCourse(getCourseTitle(), Arrays.asList(getPpsEntry()), getRegistryElement(), getCourseDescription());
        }
        else if (BlackboardAddCourseToRegElement.LINK_COURSE_ITEM.getId().equals(getAction().getId()))
        {
            BlackboardManager.instance().dao().linkCoursesWithEppRegistryElement(getSelectedCourseIdsNotEmpty(), getRegistryElement(), Collections.singleton(getPpsEntry()));
        }
        else if (BlackboardAddCourseToRegElement.CLONE_COURSE_ITEM.getId().equals(getAction().getId()))
        {
            BlackboardManager.instance().dao().cloneCourse(getSelectedCourseIdsNotEmpty().iterator().next(), getRegistryElement(), Collections.singleton(getPpsEntry()));
        }
        else
        {
            throw new IllegalStateException();
        }

        deactivate();
    }

    public boolean isCheckboxDS()
    {
        return BlackboardAddCourseToRegElement.LINK_COURSE_ITEM.getId().equals(getAction().getId());
    }

    @SuppressWarnings("unchecked")
    private Collection<CourseVO> getSelectedCourseIdsNotEmpty()
    {
        String ds = BlackboardAddCourseToRegElement.CLONE_COURSE_ITEM.getId().equals(getAction().getId()) ? BlackboardAddCourseToRegElement.RADIO_COURSE_DS : BlackboardAddCourseToRegElement.CHECKBOX_COURSE_DS;
        Collection collection = _uiConfig.<BaseSearchListDataSource>getDataSource(ds).getOptionColumnSelectedObjects(BlackboardAddCourseToRegElement.CHECK_COLUMN_NAME);
        if (collection.isEmpty())
            throw new ApplicationException("Необходимо выбрать курс(ы).");

        return CommonBaseEntityUtil.getPropertiesList(collection, BlackboardAddCourseToRegElement.BB_COURSE_PARAM);
    }

    public void onClickNext()
    {
        _actionSelected = true;
    }

    public boolean isActionSelected()
    {
        return _actionSelected;
    }

    public IEntity getAction()
    {
        return _action;
    }

    public void setAction(IEntity action)
    {
        _action = action;
    }

    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    public void setRegistryElement(EppRegistryElement registryElement)
    {
        _registryElement = registryElement;
    }

    public boolean isCreateNew()
    {
        return _action != null && BlackboardAddCourseToRegElement.CREATE_NEW_COURSE_ITEM.getId().equals(_action.getId());
    }

    public String getCourseTitle()
    {
        return _courseTitle;
    }

    public void setCourseTitle(String courseTitle)
    {
        _courseTitle = courseTitle;
    }

    public PpsEntry getPpsEntry()
    {
        return _ppsEntry;
    }

    public void setPpsEntry(PpsEntry ppsEntry)
    {
        _ppsEntry = ppsEntry;
    }

    public String getCourseDescription()
    {
        return _courseDescription;
    }

    public void setCourseDescription(String courseDescription)
    {
        _courseDescription = courseDescription;
    }
}