package ru.tandemservice.unifefu.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списки поступающих
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEntrantIncomingListVar2ReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report";
    public static final String ENTITY_NAME = "fefuEntrantIncomingListVar2Report";
    public static final int VERSION_HASH = 816977122;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_STUDENT_CATEGORY_TITLE = "studentCategoryTitle";
    public static final String P_ORDER_BY_ORIGINALS = "orderByOriginals";
    public static final String P_WITHOUT_DOCUMENT_INFO = "withoutDocumentInfo";
    public static final String P_WITHOUT_DETAIL_SUM_MARK = "withoutDetailSumMark";
    public static final String P_SHOW_DISCIPLINE_TITLES = "showDisciplineTitles";
    public static final String P_SHOW_REQUESTED_DIRECTION_PRIORITY = "showRequestedDirectionPriority";
    public static final String P_SHOW_DATA_BY_ENROLLMENT_STAGE = "showDataByEnrollmentStage";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String P_NOT_PRINT_SPES_WITHOUT_REQUEST = "notPrintSpesWithoutRequest";
    public static final String P_NOT_PRINT_NUM_INFO = "notPrintNumInfo";
    public static final String P_INCLUDE_FOREIGN_PERSON = "includeForeignPerson";
    public static final String P_INCLUDE_ENTRANT_NO_MARK = "includeEntrantNoMark";
    public static final String P_INCLUDE_ENTRANT_WITH_BENEFIT = "includeEntrantWithBenefit";
    public static final String P_INCLUDE_ENTRANT_TARGET_ADMISSION = "includeEntrantTargetAdmission";
    public static final String P_ENTRANT_CUSTOM_STATE_TITLE = "entrantCustomStateTitle";
    public static final String P_NOT_SHOW_TAKEN_DIRECTION = "notShowTakenDirection";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _studentCategoryTitle;     // Категория поступающего
    private boolean _orderByOriginals;     // Выделить абитуриентов с оригиналами документов
    private boolean _withoutDocumentInfo;     // Без информации о документах об образовании
    private boolean _withoutDetailSumMark;     // Без расшифровки суммы баллов
    private boolean _showDisciplineTitles;     // Выводить названия дисциплин
    private boolean _showRequestedDirectionPriority;     // Выводить приоритеты
    private boolean _showDataByEnrollmentStage;     // Выводить данные по этапам зачисления
    private String _qualificationTitle;     // Квалификация
    private String _developFormTitle;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private boolean _notPrintSpesWithoutRequest;     // Не печатать направления/специальности, по которым нет заявлений
    private boolean _notPrintNumInfo;     // Не печатать информацию о цифрах приема
    private Boolean _includeForeignPerson;     // Учитывать иностранных граждан
    private Boolean _includeEntrantNoMark;     // Выводить абитуриентов без баллов
    private Boolean _includeEntrantWithBenefit;     // Выводить абитуриентов, имеющих льготы
    private Boolean _includeEntrantTargetAdmission;     // Выводить абитуриентов, поступающих по целевому приему
    private String _entrantCustomStateTitle;     // Дополнительные статусы абитуриентов
    private boolean _notShowTakenDirection;     // Не выводить направления по которым нет свободных мест

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryTitle()
    {
        return _studentCategoryTitle;
    }

    /**
     * @param studentCategoryTitle Категория поступающего.
     */
    public void setStudentCategoryTitle(String studentCategoryTitle)
    {
        dirty(_studentCategoryTitle, studentCategoryTitle);
        _studentCategoryTitle = studentCategoryTitle;
    }

    /**
     * @return Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isOrderByOriginals()
    {
        return _orderByOriginals;
    }

    /**
     * @param orderByOriginals Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     */
    public void setOrderByOriginals(boolean orderByOriginals)
    {
        dirty(_orderByOriginals, orderByOriginals);
        _orderByOriginals = orderByOriginals;
    }

    /**
     * @return Без информации о документах об образовании. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithoutDocumentInfo()
    {
        return _withoutDocumentInfo;
    }

    /**
     * @param withoutDocumentInfo Без информации о документах об образовании. Свойство не может быть null.
     */
    public void setWithoutDocumentInfo(boolean withoutDocumentInfo)
    {
        dirty(_withoutDocumentInfo, withoutDocumentInfo);
        _withoutDocumentInfo = withoutDocumentInfo;
    }

    /**
     * @return Без расшифровки суммы баллов. Свойство не может быть null.
     */
    @NotNull
    public boolean isWithoutDetailSumMark()
    {
        return _withoutDetailSumMark;
    }

    /**
     * @param withoutDetailSumMark Без расшифровки суммы баллов. Свойство не может быть null.
     */
    public void setWithoutDetailSumMark(boolean withoutDetailSumMark)
    {
        dirty(_withoutDetailSumMark, withoutDetailSumMark);
        _withoutDetailSumMark = withoutDetailSumMark;
    }

    /**
     * @return Выводить названия дисциплин. Свойство не может быть null.
     */
    @NotNull
    public boolean isShowDisciplineTitles()
    {
        return _showDisciplineTitles;
    }

    /**
     * @param showDisciplineTitles Выводить названия дисциплин. Свойство не может быть null.
     */
    public void setShowDisciplineTitles(boolean showDisciplineTitles)
    {
        dirty(_showDisciplineTitles, showDisciplineTitles);
        _showDisciplineTitles = showDisciplineTitles;
    }

    /**
     * @return Выводить приоритеты. Свойство не может быть null.
     */
    @NotNull
    public boolean isShowRequestedDirectionPriority()
    {
        return _showRequestedDirectionPriority;
    }

    /**
     * @param showRequestedDirectionPriority Выводить приоритеты. Свойство не может быть null.
     */
    public void setShowRequestedDirectionPriority(boolean showRequestedDirectionPriority)
    {
        dirty(_showRequestedDirectionPriority, showRequestedDirectionPriority);
        _showRequestedDirectionPriority = showRequestedDirectionPriority;
    }

    /**
     * @return Выводить данные по этапам зачисления. Свойство не может быть null.
     */
    @NotNull
    public boolean isShowDataByEnrollmentStage()
    {
        return _showDataByEnrollmentStage;
    }

    /**
     * @param showDataByEnrollmentStage Выводить данные по этапам зачисления. Свойство не может быть null.
     */
    public void setShowDataByEnrollmentStage(boolean showDataByEnrollmentStage)
    {
        dirty(_showDataByEnrollmentStage, showDataByEnrollmentStage);
        _showDataByEnrollmentStage = showDataByEnrollmentStage;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle Квалификация.
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormTitle()
    {
        return _developFormTitle;
    }

    /**
     * @param developFormTitle Форма освоения.
     */
    public void setDevelopFormTitle(String developFormTitle)
    {
        dirty(_developFormTitle, developFormTitle);
        _developFormTitle = developFormTitle;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Направление подготовки (специальность) приема.
     */
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotPrintSpesWithoutRequest()
    {
        return _notPrintSpesWithoutRequest;
    }

    /**
     * @param notPrintSpesWithoutRequest Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     */
    public void setNotPrintSpesWithoutRequest(boolean notPrintSpesWithoutRequest)
    {
        dirty(_notPrintSpesWithoutRequest, notPrintSpesWithoutRequest);
        _notPrintSpesWithoutRequest = notPrintSpesWithoutRequest;
    }

    /**
     * @return Не печатать информацию о цифрах приема. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotPrintNumInfo()
    {
        return _notPrintNumInfo;
    }

    /**
     * @param notPrintNumInfo Не печатать информацию о цифрах приема. Свойство не может быть null.
     */
    public void setNotPrintNumInfo(boolean notPrintNumInfo)
    {
        dirty(_notPrintNumInfo, notPrintNumInfo);
        _notPrintNumInfo = notPrintNumInfo;
    }

    /**
     * @return Учитывать иностранных граждан.
     */
    public Boolean getIncludeForeignPerson()
    {
        return _includeForeignPerson;
    }

    /**
     * @param includeForeignPerson Учитывать иностранных граждан.
     */
    public void setIncludeForeignPerson(Boolean includeForeignPerson)
    {
        dirty(_includeForeignPerson, includeForeignPerson);
        _includeForeignPerson = includeForeignPerson;
    }

    /**
     * @return Выводить абитуриентов без баллов.
     */
    public Boolean getIncludeEntrantNoMark()
    {
        return _includeEntrantNoMark;
    }

    /**
     * @param includeEntrantNoMark Выводить абитуриентов без баллов.
     */
    public void setIncludeEntrantNoMark(Boolean includeEntrantNoMark)
    {
        dirty(_includeEntrantNoMark, includeEntrantNoMark);
        _includeEntrantNoMark = includeEntrantNoMark;
    }

    /**
     * @return Выводить абитуриентов, имеющих льготы.
     */
    public Boolean getIncludeEntrantWithBenefit()
    {
        return _includeEntrantWithBenefit;
    }

    /**
     * @param includeEntrantWithBenefit Выводить абитуриентов, имеющих льготы.
     */
    public void setIncludeEntrantWithBenefit(Boolean includeEntrantWithBenefit)
    {
        dirty(_includeEntrantWithBenefit, includeEntrantWithBenefit);
        _includeEntrantWithBenefit = includeEntrantWithBenefit;
    }

    /**
     * @return Выводить абитуриентов, поступающих по целевому приему.
     */
    public Boolean getIncludeEntrantTargetAdmission()
    {
        return _includeEntrantTargetAdmission;
    }

    /**
     * @param includeEntrantTargetAdmission Выводить абитуриентов, поступающих по целевому приему.
     */
    public void setIncludeEntrantTargetAdmission(Boolean includeEntrantTargetAdmission)
    {
        dirty(_includeEntrantTargetAdmission, includeEntrantTargetAdmission);
        _includeEntrantTargetAdmission = includeEntrantTargetAdmission;
    }

    /**
     * @return Дополнительные статусы абитуриентов.
     */
    @Length(max=255)
    public String getEntrantCustomStateTitle()
    {
        return _entrantCustomStateTitle;
    }

    /**
     * @param entrantCustomStateTitle Дополнительные статусы абитуриентов.
     */
    public void setEntrantCustomStateTitle(String entrantCustomStateTitle)
    {
        dirty(_entrantCustomStateTitle, entrantCustomStateTitle);
        _entrantCustomStateTitle = entrantCustomStateTitle;
    }

    /**
     * @return Не выводить направления по которым нет свободных мест. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotShowTakenDirection()
    {
        return _notShowTakenDirection;
    }

    /**
     * @param notShowTakenDirection Не выводить направления по которым нет свободных мест. Свойство не может быть null.
     */
    public void setNotShowTakenDirection(boolean notShowTakenDirection)
    {
        dirty(_notShowTakenDirection, notShowTakenDirection);
        _notShowTakenDirection = notShowTakenDirection;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEntrantIncomingListVar2ReportGen)
        {
            setContent(((FefuEntrantIncomingListVar2Report)another).getContent());
            setFormingDate(((FefuEntrantIncomingListVar2Report)another).getFormingDate());
            setEnrollmentCampaign(((FefuEntrantIncomingListVar2Report)another).getEnrollmentCampaign());
            setDateFrom(((FefuEntrantIncomingListVar2Report)another).getDateFrom());
            setDateTo(((FefuEntrantIncomingListVar2Report)another).getDateTo());
            setCompensationType(((FefuEntrantIncomingListVar2Report)another).getCompensationType());
            setStudentCategoryTitle(((FefuEntrantIncomingListVar2Report)another).getStudentCategoryTitle());
            setOrderByOriginals(((FefuEntrantIncomingListVar2Report)another).isOrderByOriginals());
            setWithoutDocumentInfo(((FefuEntrantIncomingListVar2Report)another).isWithoutDocumentInfo());
            setWithoutDetailSumMark(((FefuEntrantIncomingListVar2Report)another).isWithoutDetailSumMark());
            setShowDisciplineTitles(((FefuEntrantIncomingListVar2Report)another).isShowDisciplineTitles());
            setShowRequestedDirectionPriority(((FefuEntrantIncomingListVar2Report)another).isShowRequestedDirectionPriority());
            setShowDataByEnrollmentStage(((FefuEntrantIncomingListVar2Report)another).isShowDataByEnrollmentStage());
            setQualificationTitle(((FefuEntrantIncomingListVar2Report)another).getQualificationTitle());
            setDevelopFormTitle(((FefuEntrantIncomingListVar2Report)another).getDevelopFormTitle());
            setDevelopConditionTitle(((FefuEntrantIncomingListVar2Report)another).getDevelopConditionTitle());
            setEnrollmentDirection(((FefuEntrantIncomingListVar2Report)another).getEnrollmentDirection());
            setNotPrintSpesWithoutRequest(((FefuEntrantIncomingListVar2Report)another).isNotPrintSpesWithoutRequest());
            setNotPrintNumInfo(((FefuEntrantIncomingListVar2Report)another).isNotPrintNumInfo());
            setIncludeForeignPerson(((FefuEntrantIncomingListVar2Report)another).getIncludeForeignPerson());
            setIncludeEntrantNoMark(((FefuEntrantIncomingListVar2Report)another).getIncludeEntrantNoMark());
            setIncludeEntrantWithBenefit(((FefuEntrantIncomingListVar2Report)another).getIncludeEntrantWithBenefit());
            setIncludeEntrantTargetAdmission(((FefuEntrantIncomingListVar2Report)another).getIncludeEntrantTargetAdmission());
            setEntrantCustomStateTitle(((FefuEntrantIncomingListVar2Report)another).getEntrantCustomStateTitle());
            setNotShowTakenDirection(((FefuEntrantIncomingListVar2Report)another).isNotShowTakenDirection());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEntrantIncomingListVar2ReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEntrantIncomingListVar2Report.class;
        }

        public T newInstance()
        {
            return (T) new FefuEntrantIncomingListVar2Report();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategoryTitle":
                    return obj.getStudentCategoryTitle();
                case "orderByOriginals":
                    return obj.isOrderByOriginals();
                case "withoutDocumentInfo":
                    return obj.isWithoutDocumentInfo();
                case "withoutDetailSumMark":
                    return obj.isWithoutDetailSumMark();
                case "showDisciplineTitles":
                    return obj.isShowDisciplineTitles();
                case "showRequestedDirectionPriority":
                    return obj.isShowRequestedDirectionPriority();
                case "showDataByEnrollmentStage":
                    return obj.isShowDataByEnrollmentStage();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "developFormTitle":
                    return obj.getDevelopFormTitle();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "notPrintSpesWithoutRequest":
                    return obj.isNotPrintSpesWithoutRequest();
                case "notPrintNumInfo":
                    return obj.isNotPrintNumInfo();
                case "includeForeignPerson":
                    return obj.getIncludeForeignPerson();
                case "includeEntrantNoMark":
                    return obj.getIncludeEntrantNoMark();
                case "includeEntrantWithBenefit":
                    return obj.getIncludeEntrantWithBenefit();
                case "includeEntrantTargetAdmission":
                    return obj.getIncludeEntrantTargetAdmission();
                case "entrantCustomStateTitle":
                    return obj.getEntrantCustomStateTitle();
                case "notShowTakenDirection":
                    return obj.isNotShowTakenDirection();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "studentCategoryTitle":
                    obj.setStudentCategoryTitle((String) value);
                    return;
                case "orderByOriginals":
                    obj.setOrderByOriginals((Boolean) value);
                    return;
                case "withoutDocumentInfo":
                    obj.setWithoutDocumentInfo((Boolean) value);
                    return;
                case "withoutDetailSumMark":
                    obj.setWithoutDetailSumMark((Boolean) value);
                    return;
                case "showDisciplineTitles":
                    obj.setShowDisciplineTitles((Boolean) value);
                    return;
                case "showRequestedDirectionPriority":
                    obj.setShowRequestedDirectionPriority((Boolean) value);
                    return;
                case "showDataByEnrollmentStage":
                    obj.setShowDataByEnrollmentStage((Boolean) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "developFormTitle":
                    obj.setDevelopFormTitle((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "notPrintSpesWithoutRequest":
                    obj.setNotPrintSpesWithoutRequest((Boolean) value);
                    return;
                case "notPrintNumInfo":
                    obj.setNotPrintNumInfo((Boolean) value);
                    return;
                case "includeForeignPerson":
                    obj.setIncludeForeignPerson((Boolean) value);
                    return;
                case "includeEntrantNoMark":
                    obj.setIncludeEntrantNoMark((Boolean) value);
                    return;
                case "includeEntrantWithBenefit":
                    obj.setIncludeEntrantWithBenefit((Boolean) value);
                    return;
                case "includeEntrantTargetAdmission":
                    obj.setIncludeEntrantTargetAdmission((Boolean) value);
                    return;
                case "entrantCustomStateTitle":
                    obj.setEntrantCustomStateTitle((String) value);
                    return;
                case "notShowTakenDirection":
                    obj.setNotShowTakenDirection((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategoryTitle":
                        return true;
                case "orderByOriginals":
                        return true;
                case "withoutDocumentInfo":
                        return true;
                case "withoutDetailSumMark":
                        return true;
                case "showDisciplineTitles":
                        return true;
                case "showRequestedDirectionPriority":
                        return true;
                case "showDataByEnrollmentStage":
                        return true;
                case "qualificationTitle":
                        return true;
                case "developFormTitle":
                        return true;
                case "developConditionTitle":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "notPrintSpesWithoutRequest":
                        return true;
                case "notPrintNumInfo":
                        return true;
                case "includeForeignPerson":
                        return true;
                case "includeEntrantNoMark":
                        return true;
                case "includeEntrantWithBenefit":
                        return true;
                case "includeEntrantTargetAdmission":
                        return true;
                case "entrantCustomStateTitle":
                        return true;
                case "notShowTakenDirection":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategoryTitle":
                    return true;
                case "orderByOriginals":
                    return true;
                case "withoutDocumentInfo":
                    return true;
                case "withoutDetailSumMark":
                    return true;
                case "showDisciplineTitles":
                    return true;
                case "showRequestedDirectionPriority":
                    return true;
                case "showDataByEnrollmentStage":
                    return true;
                case "qualificationTitle":
                    return true;
                case "developFormTitle":
                    return true;
                case "developConditionTitle":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "notPrintSpesWithoutRequest":
                    return true;
                case "notPrintNumInfo":
                    return true;
                case "includeForeignPerson":
                    return true;
                case "includeEntrantNoMark":
                    return true;
                case "includeEntrantWithBenefit":
                    return true;
                case "includeEntrantTargetAdmission":
                    return true;
                case "entrantCustomStateTitle":
                    return true;
                case "notShowTakenDirection":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "compensationType":
                    return CompensationType.class;
                case "studentCategoryTitle":
                    return String.class;
                case "orderByOriginals":
                    return Boolean.class;
                case "withoutDocumentInfo":
                    return Boolean.class;
                case "withoutDetailSumMark":
                    return Boolean.class;
                case "showDisciplineTitles":
                    return Boolean.class;
                case "showRequestedDirectionPriority":
                    return Boolean.class;
                case "showDataByEnrollmentStage":
                    return Boolean.class;
                case "qualificationTitle":
                    return String.class;
                case "developFormTitle":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "notPrintSpesWithoutRequest":
                    return Boolean.class;
                case "notPrintNumInfo":
                    return Boolean.class;
                case "includeForeignPerson":
                    return Boolean.class;
                case "includeEntrantNoMark":
                    return Boolean.class;
                case "includeEntrantWithBenefit":
                    return Boolean.class;
                case "includeEntrantTargetAdmission":
                    return Boolean.class;
                case "entrantCustomStateTitle":
                    return String.class;
                case "notShowTakenDirection":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEntrantIncomingListVar2Report> _dslPath = new Path<FefuEntrantIncomingListVar2Report>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEntrantIncomingListVar2Report");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getStudentCategoryTitle()
     */
    public static PropertyPath<String> studentCategoryTitle()
    {
        return _dslPath.studentCategoryTitle();
    }

    /**
     * @return Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isOrderByOriginals()
     */
    public static PropertyPath<Boolean> orderByOriginals()
    {
        return _dslPath.orderByOriginals();
    }

    /**
     * @return Без информации о документах об образовании. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isWithoutDocumentInfo()
     */
    public static PropertyPath<Boolean> withoutDocumentInfo()
    {
        return _dslPath.withoutDocumentInfo();
    }

    /**
     * @return Без расшифровки суммы баллов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isWithoutDetailSumMark()
     */
    public static PropertyPath<Boolean> withoutDetailSumMark()
    {
        return _dslPath.withoutDetailSumMark();
    }

    /**
     * @return Выводить названия дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isShowDisciplineTitles()
     */
    public static PropertyPath<Boolean> showDisciplineTitles()
    {
        return _dslPath.showDisciplineTitles();
    }

    /**
     * @return Выводить приоритеты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isShowRequestedDirectionPriority()
     */
    public static PropertyPath<Boolean> showRequestedDirectionPriority()
    {
        return _dslPath.showRequestedDirectionPriority();
    }

    /**
     * @return Выводить данные по этапам зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isShowDataByEnrollmentStage()
     */
    public static PropertyPath<Boolean> showDataByEnrollmentStage()
    {
        return _dslPath.showDataByEnrollmentStage();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getDevelopFormTitle()
     */
    public static PropertyPath<String> developFormTitle()
    {
        return _dslPath.developFormTitle();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isNotPrintSpesWithoutRequest()
     */
    public static PropertyPath<Boolean> notPrintSpesWithoutRequest()
    {
        return _dslPath.notPrintSpesWithoutRequest();
    }

    /**
     * @return Не печатать информацию о цифрах приема. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isNotPrintNumInfo()
     */
    public static PropertyPath<Boolean> notPrintNumInfo()
    {
        return _dslPath.notPrintNumInfo();
    }

    /**
     * @return Учитывать иностранных граждан.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getIncludeForeignPerson()
     */
    public static PropertyPath<Boolean> includeForeignPerson()
    {
        return _dslPath.includeForeignPerson();
    }

    /**
     * @return Выводить абитуриентов без баллов.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getIncludeEntrantNoMark()
     */
    public static PropertyPath<Boolean> includeEntrantNoMark()
    {
        return _dslPath.includeEntrantNoMark();
    }

    /**
     * @return Выводить абитуриентов, имеющих льготы.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getIncludeEntrantWithBenefit()
     */
    public static PropertyPath<Boolean> includeEntrantWithBenefit()
    {
        return _dslPath.includeEntrantWithBenefit();
    }

    /**
     * @return Выводить абитуриентов, поступающих по целевому приему.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getIncludeEntrantTargetAdmission()
     */
    public static PropertyPath<Boolean> includeEntrantTargetAdmission()
    {
        return _dslPath.includeEntrantTargetAdmission();
    }

    /**
     * @return Дополнительные статусы абитуриентов.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getEntrantCustomStateTitle()
     */
    public static PropertyPath<String> entrantCustomStateTitle()
    {
        return _dslPath.entrantCustomStateTitle();
    }

    /**
     * @return Не выводить направления по которым нет свободных мест. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isNotShowTakenDirection()
     */
    public static PropertyPath<Boolean> notShowTakenDirection()
    {
        return _dslPath.notShowTakenDirection();
    }

    public static class Path<E extends FefuEntrantIncomingListVar2Report> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _studentCategoryTitle;
        private PropertyPath<Boolean> _orderByOriginals;
        private PropertyPath<Boolean> _withoutDocumentInfo;
        private PropertyPath<Boolean> _withoutDetailSumMark;
        private PropertyPath<Boolean> _showDisciplineTitles;
        private PropertyPath<Boolean> _showRequestedDirectionPriority;
        private PropertyPath<Boolean> _showDataByEnrollmentStage;
        private PropertyPath<String> _qualificationTitle;
        private PropertyPath<String> _developFormTitle;
        private PropertyPath<String> _developConditionTitle;
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private PropertyPath<Boolean> _notPrintSpesWithoutRequest;
        private PropertyPath<Boolean> _notPrintNumInfo;
        private PropertyPath<Boolean> _includeForeignPerson;
        private PropertyPath<Boolean> _includeEntrantNoMark;
        private PropertyPath<Boolean> _includeEntrantWithBenefit;
        private PropertyPath<Boolean> _includeEntrantTargetAdmission;
        private PropertyPath<String> _entrantCustomStateTitle;
        private PropertyPath<Boolean> _notShowTakenDirection;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(FefuEntrantIncomingListVar2ReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(FefuEntrantIncomingListVar2ReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(FefuEntrantIncomingListVar2ReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getStudentCategoryTitle()
     */
        public PropertyPath<String> studentCategoryTitle()
        {
            if(_studentCategoryTitle == null )
                _studentCategoryTitle = new PropertyPath<String>(FefuEntrantIncomingListVar2ReportGen.P_STUDENT_CATEGORY_TITLE, this);
            return _studentCategoryTitle;
        }

    /**
     * @return Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isOrderByOriginals()
     */
        public PropertyPath<Boolean> orderByOriginals()
        {
            if(_orderByOriginals == null )
                _orderByOriginals = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_ORDER_BY_ORIGINALS, this);
            return _orderByOriginals;
        }

    /**
     * @return Без информации о документах об образовании. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isWithoutDocumentInfo()
     */
        public PropertyPath<Boolean> withoutDocumentInfo()
        {
            if(_withoutDocumentInfo == null )
                _withoutDocumentInfo = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_WITHOUT_DOCUMENT_INFO, this);
            return _withoutDocumentInfo;
        }

    /**
     * @return Без расшифровки суммы баллов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isWithoutDetailSumMark()
     */
        public PropertyPath<Boolean> withoutDetailSumMark()
        {
            if(_withoutDetailSumMark == null )
                _withoutDetailSumMark = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_WITHOUT_DETAIL_SUM_MARK, this);
            return _withoutDetailSumMark;
        }

    /**
     * @return Выводить названия дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isShowDisciplineTitles()
     */
        public PropertyPath<Boolean> showDisciplineTitles()
        {
            if(_showDisciplineTitles == null )
                _showDisciplineTitles = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_SHOW_DISCIPLINE_TITLES, this);
            return _showDisciplineTitles;
        }

    /**
     * @return Выводить приоритеты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isShowRequestedDirectionPriority()
     */
        public PropertyPath<Boolean> showRequestedDirectionPriority()
        {
            if(_showRequestedDirectionPriority == null )
                _showRequestedDirectionPriority = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_SHOW_REQUESTED_DIRECTION_PRIORITY, this);
            return _showRequestedDirectionPriority;
        }

    /**
     * @return Выводить данные по этапам зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isShowDataByEnrollmentStage()
     */
        public PropertyPath<Boolean> showDataByEnrollmentStage()
        {
            if(_showDataByEnrollmentStage == null )
                _showDataByEnrollmentStage = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_SHOW_DATA_BY_ENROLLMENT_STAGE, this);
            return _showDataByEnrollmentStage;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(FefuEntrantIncomingListVar2ReportGen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getDevelopFormTitle()
     */
        public PropertyPath<String> developFormTitle()
        {
            if(_developFormTitle == null )
                _developFormTitle = new PropertyPath<String>(FefuEntrantIncomingListVar2ReportGen.P_DEVELOP_FORM_TITLE, this);
            return _developFormTitle;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(FefuEntrantIncomingListVar2ReportGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isNotPrintSpesWithoutRequest()
     */
        public PropertyPath<Boolean> notPrintSpesWithoutRequest()
        {
            if(_notPrintSpesWithoutRequest == null )
                _notPrintSpesWithoutRequest = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_NOT_PRINT_SPES_WITHOUT_REQUEST, this);
            return _notPrintSpesWithoutRequest;
        }

    /**
     * @return Не печатать информацию о цифрах приема. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isNotPrintNumInfo()
     */
        public PropertyPath<Boolean> notPrintNumInfo()
        {
            if(_notPrintNumInfo == null )
                _notPrintNumInfo = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_NOT_PRINT_NUM_INFO, this);
            return _notPrintNumInfo;
        }

    /**
     * @return Учитывать иностранных граждан.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getIncludeForeignPerson()
     */
        public PropertyPath<Boolean> includeForeignPerson()
        {
            if(_includeForeignPerson == null )
                _includeForeignPerson = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_INCLUDE_FOREIGN_PERSON, this);
            return _includeForeignPerson;
        }

    /**
     * @return Выводить абитуриентов без баллов.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getIncludeEntrantNoMark()
     */
        public PropertyPath<Boolean> includeEntrantNoMark()
        {
            if(_includeEntrantNoMark == null )
                _includeEntrantNoMark = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_INCLUDE_ENTRANT_NO_MARK, this);
            return _includeEntrantNoMark;
        }

    /**
     * @return Выводить абитуриентов, имеющих льготы.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getIncludeEntrantWithBenefit()
     */
        public PropertyPath<Boolean> includeEntrantWithBenefit()
        {
            if(_includeEntrantWithBenefit == null )
                _includeEntrantWithBenefit = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_INCLUDE_ENTRANT_WITH_BENEFIT, this);
            return _includeEntrantWithBenefit;
        }

    /**
     * @return Выводить абитуриентов, поступающих по целевому приему.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getIncludeEntrantTargetAdmission()
     */
        public PropertyPath<Boolean> includeEntrantTargetAdmission()
        {
            if(_includeEntrantTargetAdmission == null )
                _includeEntrantTargetAdmission = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_INCLUDE_ENTRANT_TARGET_ADMISSION, this);
            return _includeEntrantTargetAdmission;
        }

    /**
     * @return Дополнительные статусы абитуриентов.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#getEntrantCustomStateTitle()
     */
        public PropertyPath<String> entrantCustomStateTitle()
        {
            if(_entrantCustomStateTitle == null )
                _entrantCustomStateTitle = new PropertyPath<String>(FefuEntrantIncomingListVar2ReportGen.P_ENTRANT_CUSTOM_STATE_TITLE, this);
            return _entrantCustomStateTitle;
        }

    /**
     * @return Не выводить направления по которым нет свободных мест. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report#isNotShowTakenDirection()
     */
        public PropertyPath<Boolean> notShowTakenDirection()
        {
            if(_notShowTakenDirection == null )
                _notShowTakenDirection = new PropertyPath<Boolean>(FefuEntrantIncomingListVar2ReportGen.P_NOT_SHOW_TAKEN_DIRECTION, this);
            return _notShowTakenDirection;
        }

        public Class getEntityClass()
        {
            return FefuEntrantIncomingListVar2Report.class;
        }

        public String getEntityName()
        {
            return "fefuEntrantIncomingListVar2Report";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
