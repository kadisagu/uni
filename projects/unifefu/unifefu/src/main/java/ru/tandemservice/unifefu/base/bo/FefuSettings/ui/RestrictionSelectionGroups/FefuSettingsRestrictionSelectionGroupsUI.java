/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.RestrictionSelectionGroups;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.tapsupport.component.matrix.CellCoordinates;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.unifefu.entity.FefuInactiveProgramSubjectIndexToCourse;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Ekaterina Zvereva
 * @since 18.08.2014
 */

public class FefuSettingsRestrictionSelectionGroupsUI extends UIPresenter
{

    private Map<CellCoordinates, Boolean> _matrixData;
    private List<Course> _coursesList;
    private List<EduProgramSubjectIndex> _eduProgramSubjectIndexes;

    @Override
    public void onComponentRefresh()
    {
        _coursesList = DevelopGridDAO.getCourseList();
        _eduProgramSubjectIndexes = DataAccessServices.dao().getList(EduProgramSubjectIndex.class, EduProgramSubjectIndex.P_PRIORITY);
        List<FefuInactiveProgramSubjectIndexToCourse> listRestriction = DataAccessServices.dao().getList(FefuInactiveProgramSubjectIndexToCourse.class);

        Set<CellCoordinates> inactiveCheckBoxes = Sets.newHashSetWithExpectedSize(listRestriction.size());

        for (FefuInactiveProgramSubjectIndexToCourse item : listRestriction)
        {
            inactiveCheckBoxes.add(new CellCoordinates(item.getEduProgramSubjectIndex().getId(), item.getCourse().getId()));
        }

        _matrixData = Maps.newHashMapWithExpectedSize(_coursesList.size() * _eduProgramSubjectIndexes.size());
        for (Course course : _coursesList)
        {
            for (EduProgramSubjectIndex progItem : _eduProgramSubjectIndexes)
            {
                CellCoordinates cellCoordinates = new CellCoordinates(progItem.getId(), course.getId());
                _matrixData.put(cellCoordinates, !inactiveCheckBoxes.contains(cellCoordinates));
            }
        }
    }

    public Map<CellCoordinates, Boolean> getMatrixData()
    {
        return _matrixData;
    }

    public void setMatrixData(Map<CellCoordinates, Boolean> matrixData)
    {
        _matrixData = matrixData;
    }

    public List<Course> getCoursesList()
    {
        return _coursesList;
    }

    public void setCoursesList(List<Course> coursesList)
    {
        _coursesList = coursesList;
    }

    public List<EduProgramSubjectIndex> getEduProgramSubjectIndexes()
    {
        return _eduProgramSubjectIndexes;
    }

    public void setEduProgramSubjectIndexes(List<EduProgramSubjectIndex> eduProgramSubjectIndexes)
    {
        _eduProgramSubjectIndexes = eduProgramSubjectIndexes;
    }

    public void onClickApply()
    {
        DataAccessServices.dao().doInTransaction(session -> {
            // Всё удаляем
            new DQLDeleteBuilder(FefuInactiveProgramSubjectIndexToCourse.class)
                    .createStatement(session).execute();

            // Создаем заново для снятых галочек
            for (CellCoordinates cell : _matrixData.keySet())
            {
                if (!_matrixData.get(cell))
                {
                    FefuInactiveProgramSubjectIndexToCourse temp = new FefuInactiveProgramSubjectIndexToCourse();
                    temp.setEduProgramSubjectIndex((EduProgramSubjectIndex) session.get(EduProgramSubjectIndex.class, cell.getRowId()));
                    temp.setCourse((Course) session.get(Course.class, cell.getColumnId()));
                    session.save(temp);
                }
            }

            return null;
        });
    }

    public void onClickSave()
    {
        onClickApply();
        deactivate();
    }
}