/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e85;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract;
import ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;

import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 25.12.2013
 */
public class GiveDiplomaStuExtractDao extends ru.tandemservice.movestudent.component.modularextract.e85.GiveDiplomaStuExtractDao
{
    @Override
    public void doCommit(GiveDiplomaStuExtract extract, Map parameters)
    {
        super.doCommit(extract, parameters);

        if (extract.isWithExcellent())
        {
            // создание доп. статуса: «Диплом с отличием»
            StudentCustomStateCI customStateCI = DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), UniFefuDefines.DIPLOMA_SUCCESS);
            StudentCustomState studentCustomState = new StudentCustomState();
            studentCustomState.setCustomState(customStateCI);
            studentCustomState.setStudent(extract.getEntity());
            UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(studentCustomState);
            StudentCustomStateToExtractRelation extractRelation = new StudentCustomStateToExtractRelation();
            extractRelation.setStudentCustomState(studentCustomState);
            extractRelation.setExtract(extract);
            save(extractRelation);
        }
        //Проверяем, является ли студент старостой в какой-нибудь группе и разрываем эту связь
        UnifefuDaoFacade.getFefuMoveStudentDAO().breakLinkCaptainWithGroups(extract);
    }

    @Override
    public void doRollback(GiveDiplomaStuExtract extract, Map parameters)
    {
        super.doRollback(extract, parameters);

        if (extract.isWithExcellent())
        {
            // удаление доп. статуса: «Диплом с отличием»
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateToExtractRelation.class, "r")
                    .column(property("r", StudentCustomStateToExtractRelation.studentCustomState().id()))
                    .where(eq(property("r", StudentCustomStateToExtractRelation.extract().id()), value(extract.getId())));

            DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(StudentCustomState.class).where(in(property(StudentCustomState.id()), builder.buildQuery()));

            deleteBuilder.createStatement(getSession()).execute();
        }
        //Восстанавливаем студента в должности старосты
        UnifefuDaoFacade.getFefuMoveStudentDAO().linkStudCaptainWithGroups(extract);
    }
}
