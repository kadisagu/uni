package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.OriginalsVerificationVar2List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.OriginalsVerificationVar2Add.FefuEcReportOriginalsVerificationVar2Add;

import java.util.List;

/**
 * User: amakarova
 * Date: 20.09.13
 */
public class FefuEcReportOriginalsVerificationVar2ListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        if (getSettings().get("enrollmentCampaign") == null)
        {
            List<EnrollmentCampaign> enrollmentCampaignList = DataAccessServices.dao().getList(EnrollmentCampaign.class, EnrollmentCampaign.id().s());
            getSettings().set("enrollmentCampaign", enrollmentCampaignList.isEmpty() ? null : enrollmentCampaignList.get(enrollmentCampaignList.size() - 1));
        }
    }

    // Listeners

    public void onClickAddReport()
    {
        getActivationBuilder().asDesktopRoot(FefuEcReportOriginalsVerificationVar2Add.class).activate();
    }

    public void onPrintReport()
    {
        getActivationBuilder().asRegion(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", getListenerParameterAsLong())
                .parameter("extension", "rtf")
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickShow()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(FefuEcReportOriginalsVerificationVar2List.REPORT_LIST_DS))
        {
            dataSource.putAll(getSettings().getAsMap(true, "enrollmentCampaign"));
        }
    }
}
