/* $Id$ */
package ru.tandemservice.unifefu.migration;

import com.google.common.collect.Maps;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuLoadTypeCodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 26.01.2015
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x1_10to11 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
                new ScriptDependency("org.tandemframework", "1.6.16"),
                new ScriptDependency("org.tandemframework.shared", "1.7.1"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();

        String aLoadSql = "select id from epp_c_loadtype_t where catalogcode_p = 'eppALoadType' and code_p = ";
        Long loadLecturesId = getId(tool, aLoadSql + "'" + EppALoadTypeCodes.TYPE_LECTURES + "'");
        Long loadPracticesId = getId(tool, aLoadSql + "'" + EppALoadTypeCodes.TYPE_PRACTICE + "'");
        Long loadLabsId = getId(tool, aLoadSql + "'" + EppALoadTypeCodes.TYPE_LABS + "'");

        PreparedStatement insertEpvRowTermLoad = tool.prepareStatement("insert into epp_epvrowterm_load_t (id, discriminator, rowterm_id, loadtype_id, hours_p, hoursi_p, hourse_p) values (?, ?, ?, ?, ?, ?, ?)");
        PreparedStatement updateEpvRowTermLoad = tool.prepareStatement("update epp_epvrowterm_load_t set hoursi_p = ? where id = ?");
        PreparedStatement updateEpvTermDistr = tool.prepareStatement("update epp_epvrow_distr_t set hourscontrol_p = ? where id = ?");
        PreparedStatement updateTotalEpvRow = tool.prepareStatement("update epp_epvrow_distr_load_t set hoursi_p = ? where row_id = ? and loadtype_id = ?");
        PreparedStatement updateEppRowTerm = tool.prepareStatement("update epp_epvrowterm_t set hoursControl_p = ? where id = ?");

        ResultSet src = stmt.executeQuery("select rowTerm.row_id, rowTerm.term_id, rowTerm.id, fload.code_p, fEpvLoad.load_p from fefuepvrowtermload_t fEpvLoad " +
                                                  "inner join epp_epvrowterm_t rowTerm on fEpvLoad.rowterm_id = rowTerm.id " +
                                                  "inner join (epp_epvrow_base_t rowBase inner join epp_epvrow_distr_t epvRow on epvRow.id=rowBase.id) on rowTerm.row_id = epvRow.id " +
                                                  "inner join fefuloadtype_t fload on fload.id=fEpvLoad.loadtype_id");

        short entityCode = tool.entityCodes().ensure("eppEpvRowTermLoad");
        // { eppEpvTermDistributedRow_id -> { fefuLoadTypeCode --> loadValue }}
        Map<Long, Map<String, Long>> eppTotalRowLoadMap = Maps.newHashMap();
        // { row_id -> controlValue }
        Map<Long, Long> eppTotalRowControlMap = Maps.newHashMap();

        while (src.next())
        {
            Long rowId = src.getLong(1);
            Long termId = src.getLong(2);
            Long rowTermId = src.getLong(3);
            String fefuLoadTypeCode = src.getString(4);
            Long loadInterValue = src.getLong(5);

            if (fefuLoadTypeCode.equals(FefuLoadTypeCodes.TYPE_EXAM_HOURS))
            {
                if (!eppTotalRowControlMap.containsKey(rowId)) eppTotalRowControlMap.put(rowId, 0L);
                eppTotalRowControlMap.put(rowId, eppTotalRowControlMap.get(rowId) + loadInterValue);

                updateEppRowTerm.setLong(1, loadInterValue);
                updateEppRowTerm.setLong(2, rowTermId);
                updateEppRowTerm.executeUpdate();
            }
            else
            {
                Long loadId = null;
                switch (fefuLoadTypeCode)
                {
                    case FefuLoadTypeCodes.TYPE_LECTURES_INTER: loadId = loadLecturesId; break;
                    case FefuLoadTypeCodes.TYPE_PRACTICE_INTER: loadId = loadPracticesId; break;
                    case FefuLoadTypeCodes.TYPE_LABS_INTER: loadId = loadLabsId; break;
                    default: throw new IllegalStateException();
                }

                Long epvLoadId = getEpvLoadId(tool, rowId, termId, loadId);
                if (null == epvLoadId)
                {
                    setEppTotalRowLoadMap(eppTotalRowLoadMap, rowId, fefuLoadTypeCode, loadInterValue);

                    insertEpvRowTermLoad.setLong(1, EntityIDGenerator.generateNewId(entityCode));
                    insertEpvRowTermLoad.setShort(2, entityCode);
                    insertEpvRowTermLoad.setLong(3, rowTermId);
                    insertEpvRowTermLoad.setLong(4, loadId);
                    insertEpvRowTermLoad.setLong(5, 0);
                    insertEpvRowTermLoad.setLong(6, loadInterValue);
                    insertEpvRowTermLoad.setLong(7, 0);
                    insertEpvRowTermLoad.execute();
                }
                else
                {
                    setEppTotalRowLoadMap(eppTotalRowLoadMap, rowId, fefuLoadTypeCode, loadInterValue);

                    updateEpvRowTermLoad.setLong(1, loadInterValue);
                    updateEpvRowTermLoad.setLong(2, epvLoadId);
                    updateEpvRowTermLoad.executeUpdate();
                }
            }
        }

        // Итоговая нагрузка строк УП
        for (Long rowId : eppTotalRowLoadMap.keySet())
        {
            for (Map.Entry<String, Long> entry : eppTotalRowLoadMap.get(rowId).entrySet())
            {
                Long loadId = null;
                String loadTypeCode = entry.getKey();
                Long totalLoadValue = entry.getValue();

                switch (loadTypeCode)
                {
                    case FefuLoadTypeCodes.TYPE_LECTURES_INTER: loadId = loadLecturesId; break;
                    case FefuLoadTypeCodes.TYPE_PRACTICE_INTER: loadId = loadPracticesId; break;
                    case FefuLoadTypeCodes.TYPE_LABS_INTER: loadId = loadLabsId; break;
                    default: throw new IllegalStateException();
                }

                updateTotalEpvRow.setLong(1, totalLoadValue);
                updateTotalEpvRow.setLong(2, rowId);
                updateTotalEpvRow.setLong(3, loadId);
                updateTotalEpvRow.executeUpdate();
            }
        }
        // Итоговый контроль строк УП
        for (Map.Entry<Long, Long> entry : eppTotalRowControlMap.entrySet())
        {
            updateEpvTermDistr.setLong(1, entry.getValue());
            updateEpvTermDistr.setLong(2, entry.getKey());
            updateEpvTermDistr.executeUpdate();
        }
    }

    private void setEppTotalRowLoadMap(Map<Long, Map<String, Long>> map, Long rowId, String code, Long value)
    {
        if (!map.containsKey(rowId)) map.put(rowId, Maps.<String, Long>newHashMap());
        Map<String, Long> loadTypeMap = map.get(rowId);

        if (!loadTypeMap.containsKey(code)) loadTypeMap.put(code, 0L);
        loadTypeMap.put(code, loadTypeMap.get(code)+ value);
    }

    private Long getEpvLoadId(DBTool tool, Long rowId, Long termId, Long loadTypeId) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();
        ResultSet src = stmt.executeQuery("select epvLoad.id from epp_epvrowterm_load_t epvLoad " +
                                                  "inner join epp_epvrowterm_t rowTerm on epvLoad.rowterm_id = rowTerm.id " +
                                                  "inner join epp_epvrow_distr_t epvRow on rowTerm.row_id = epvRow.id " +
                                                  "where rowTerm.row_id = '" + rowId + "' and rowTerm.term_id = '" + termId + "' and epvLoad.loadtype_id = '" + loadTypeId +"'");

        Long epvLoadId = null;
        while (src.next())
        {
            epvLoadId = src.getLong(1);
            if (src.next())
            {
                System.out.println("rowId = " + rowId + " termId = " + termId + " loadTypeId = " + loadTypeId);
                throw new IllegalStateException();
            }
        }
        return epvLoadId;
    }

    private Long getId(DBTool tool, String sql) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();
        ResultSet src = stmt.executeQuery(sql);
        while (src.next())
            return src.getLong(1);
        return null;
    }
}