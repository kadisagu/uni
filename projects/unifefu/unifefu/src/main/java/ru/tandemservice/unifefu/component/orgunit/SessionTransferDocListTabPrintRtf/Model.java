/* $Id $ */
package ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTabPrintRtf;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rostuncev Savva
 * @since 20.03.2014
 */

@State({@Bind(key = "sessionTransferDocumentId", binding = "sessionTransferDocument.id")})
public class Model
{
    private SessionTransferDocument _sessionTransferDocument = new SessionTransferDocument();
    private List<Integer> _captainRowList = new ArrayList<>();


    public SessionTransferDocument getSessionTransferDocument()
    {
        return _sessionTransferDocument;
    }

    public void setSessionTransferDocument(SessionTransferDocument _sessionTransferDocument)
    {
        this._sessionTransferDocument = _sessionTransferDocument;
    }

    public List<Integer> getCaptainRowList()
    {
        return _captainRowList;
    }

    public void setCaptainRowList(List<Integer> captainRowList)
    {
        _captainRowList = captainRowList;
    }
}
