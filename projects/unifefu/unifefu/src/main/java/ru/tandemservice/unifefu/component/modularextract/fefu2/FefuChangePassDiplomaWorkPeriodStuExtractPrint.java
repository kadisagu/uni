package ru.tandemservice.unifefu.component.modularextract.fefu2;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/20/12
 * Time: 1:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class FefuChangePassDiplomaWorkPeriodStuExtractPrint implements IPrintFormCreator<FefuChangePassDiplomaWorkPeriodStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuChangePassDiplomaWorkPeriodStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("period", extract.getSeason() != null ? extract.getSeason() : "______");
        modifier.put("year", String.valueOf(extract.getYear()));
        modifier.put("newDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getNewDate()));
        if (!extract.isAfterStateExam())
        {
            modifier.put("passed_D", "");
            modifier.put("stateExams", "");
        }
        else
        {
            modifier.put("passed_D", " и " + modifier.getStringValue("passed_D"));
            modifier.put("stateExams", extract.isPluralForStateExam() ? " государственные экзамены" : " государственный экзамен");
        }
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}
