/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu23.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 23.01.2015
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuTransfStuDPOExtract, Model>
{
}