/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;


/**
 * @author Ekaterina Zvereva
 * @since 15.05.2015
 */
public class DAO extends AbstractListExtractPubDAO<FefuTransfSpecialityStuListExtract, Model>
{
}
