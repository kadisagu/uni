/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuMobile.logic;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unifefu.entity.FefuChangedMobileEntity;
import ru.tandemservice.unifefu.entity.FefuStudentMarkHistory;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonExt;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unifefu.ws.mobile.entity.*;
import ru.tandemservice.unifefu.ws.nsi.NsiDatagramUtil;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsIRatingValueFormatter;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.*;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2014
 */
public class FefuMobileDao extends UniBaseDao implements IFefuMobileDao
{
    public static final String PERSON_MOBILE = "mobPerson";
    public static final String STUDENT_MOBILE = "student";
    public static final String SUBJECT_MOBILE = "subject";
    public static final String STUDENT_TO_SEMESTER_MOBILE = "student2Semester";
    public static final String PERIOD_MOBILE = "period";
    public static final String RESULT_MOBILE = "result";

    public static final Map<String, String> ENTITY_NAME_TO_MOBILE_ENTITY_NAME = new HashMap<>();
    public static final Set<String> ORIGINAL_ENTITY_NAME_SET = new HashSet<>();

    // public static final boolean WS_ENABLED = null != ApplicationRuntime.getProperty("fefu.mobile.ws.enabled") ? Boolean.parseBoolean(ApplicationRuntime.getProperty("fefu.mobile.ws.enabled")) : false;
    public static final boolean AUTO_SYNC_ENABLED = "true".equals(ApplicationRuntime.getProperty("fefu.mobile.autosync.enabled"));

    static
    {
        //ORIGINAL_ENTITY_NAME_SET.add(Principal.ENTITY_NAME);
        //ORIGINAL_ENTITY_NAME_SET.add(IdentityCard.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(Person.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(Student.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(EppRegistryElement.ENTITY_NAME);
        /*ORIGINAL_ENTITY_NAME_SET.add(EppRegistryElementPart.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(EppRegistryElementPartModule.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(EppRegistryElementPartFControlAction.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(EppRegistryElementLoad.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(EppRegistryModuleALoad.ENTITY_NAME);*/
        ORIGINAL_ENTITY_NAME_SET.add(EppStudentWorkPlanElement.ENTITY_NAME);
        /*ORIGINAL_ENTITY_NAME_SET.add(EppStudentWpeALoad.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(EppStudentWpeCAction.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(SppSchedule.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(SppScheduleWeekRow.ENTITY_NAME);*/
        ORIGINAL_ENTITY_NAME_SET.add(SppSchedulePeriod.ENTITY_NAME);
        /*ORIGINAL_ENTITY_NAME_SET.add(SppSchedulePeriodFix.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(SppScheduleSeason.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(TrEduGroupEvent.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(TrEduGroupEventStudent.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(TrJournal.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(TrJournalModule.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(TrJournalEvent.ENTITY_NAME);
        ORIGINAL_ENTITY_NAME_SET.add(TrJournalGroup.ENTITY_NAME);*/
        ORIGINAL_ENTITY_NAME_SET.add(TrJournalGroupStudent.ENTITY_NAME);
        //ORIGINAL_ENTITY_NAME_SET.add(SessionSlotLinkMark.ENTITY_NAME);

        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(Principal.ENTITY_NAME, PERSON_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(Person.ENTITY_NAME, PERSON_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(IdentityCard.ENTITY_NAME, PERSON_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(Student.ENTITY_NAME, STUDENT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryElement.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryElementPart.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryElementPartModule.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryElementPartFControlAction.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryElementLoad.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppRegistryModuleALoad.ENTITY_NAME, SUBJECT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppStudentWorkPlanElement.ENTITY_NAME, STUDENT_TO_SEMESTER_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppStudentWpeALoad.ENTITY_NAME, STUDENT_TO_SEMESTER_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(EppStudentWpeCAction.ENTITY_NAME, STUDENT_TO_SEMESTER_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SppSchedule.ENTITY_NAME, PERIOD_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SppScheduleWeekRow.ENTITY_NAME, PERIOD_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SppSchedulePeriod.ENTITY_NAME, PERIOD_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SppSchedulePeriodFix.ENTITY_NAME, PERIOD_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SppScheduleSeason.ENTITY_NAME, PERIOD_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrEduGroupEvent.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrEduGroupEventStudent.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrJournal.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrJournalModule.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrJournalEvent.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrJournalGroup.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(TrJournalGroupStudent.ENTITY_NAME, RESULT_MOBILE);
        ENTITY_NAME_TO_MOBILE_ENTITY_NAME.put(SessionSlotLinkMark.ENTITY_NAME, RESULT_MOBILE);
    }

    private Map<String, Set<Long>> PREPROCESSED_ENTITY_MAP = new HashMap<>();

    /**
     * По идентификатору объекта возвращает пару "Идентификатор объекта"-"Тип сущности"
     *
     * @param id - идентификатор объекта
     * @return - пара "Идентификатор объекта"-"Тип сущности"
     */
    private List<CoreCollectionUtils.Pair<Long, String>> getActualIEntityTypeAndIdentifierPairList(Long id)
    {
        IEntityMeta meta = EntityRuntime.getMeta(id);
        if (null == meta) return null;

        List<CoreCollectionUtils.Pair<Long, String>> resultIdList = new ArrayList<>();

        if (ORIGINAL_ENTITY_NAME_SET.contains(meta.getName()))
        {
            resultIdList.add(new CoreCollectionUtils.Pair<>(id, ENTITY_NAME_TO_MOBILE_ENTITY_NAME.get(meta.getName())));
        } else
        {
            IEntity entity = get(id);

            if (entity instanceof Principal)
            {
                Person2PrincipalRelation principalRelation = get(Person2PrincipalRelation.class, Person2PrincipalRelation.principal().id(), id);
                if (null != principalRelation)
                    resultIdList.add(new CoreCollectionUtils.Pair<>(principalRelation.getPerson().getId(), PERSON_MOBILE));
            } else if (entity instanceof IdentityCard)
            {
                IdentityCard iCard = (IdentityCard) entity;
                if (null != iCard.getPerson())
                    resultIdList.add(new CoreCollectionUtils.Pair<>(iCard.getPerson().getId(), PERSON_MOBILE));
            } else if (entity instanceof EppRegistryElementPart)
            {
                EppRegistryElementPart elementPart = (EppRegistryElementPart) entity;
                resultIdList.add(new CoreCollectionUtils.Pair<>(elementPart.getRegistryElement().getId(), SUBJECT_MOBILE));
            } else if (entity instanceof EppRegistryElementPartModule)
            {
                EppRegistryElementPartModule eppRegistryElementPartModule = (EppRegistryElementPartModule) entity;
                resultIdList.add(new CoreCollectionUtils.Pair<>(eppRegistryElementPartModule.getHierarhyParent().getRegistryElement().getId(), SUBJECT_MOBILE));
            } else if (entity instanceof EppRegistryElementPartFControlAction)
            {
                EppRegistryElementPartFControlAction action = (EppRegistryElementPartFControlAction) entity;
                resultIdList.add(new CoreCollectionUtils.Pair<>(action.getPart().getRegistryElement().getId(), SUBJECT_MOBILE));
            } else if (entity instanceof EppRegistryElementLoad)
            {
                EppRegistryElementLoad load = (EppRegistryElementLoad) entity;
                resultIdList.add(new CoreCollectionUtils.Pair<>(load.getRegistryElement().getId(), SUBJECT_MOBILE));
            } else if (entity instanceof EppRegistryModuleALoad)
            {
                EppRegistryModuleALoad load = (EppRegistryModuleALoad) entity;
                List<Long> regElIds = new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "e").distinct()
                        .column(property(EppRegistryElementPartModule.part().registryElement().id().fromAlias("e")))
                        .where(eq(property(EppRegistryElementPartModule.module().fromAlias("e")), value(load.getModule())))
                        .createStatement(getSession()).list();

                for (Long regElId : regElIds) resultIdList.add(new CoreCollectionUtils.Pair<>(regElId, SUBJECT_MOBILE));
            } else if (entity instanceof EppStudentWpeALoad)
            {
                EppStudentWpeALoad loadSlot = (EppStudentWpeALoad) entity;
                resultIdList.add(new CoreCollectionUtils.Pair<>(loadSlot.getStudentWpe().getId(), STUDENT_TO_SEMESTER_MOBILE));
            } else if (entity instanceof EppStudentWpeCAction)
            {
                EppStudentWpeCAction actionSlot = (EppStudentWpeCAction) entity;
                resultIdList.add(new CoreCollectionUtils.Pair<>(actionSlot.getStudentWpe().getId(), STUDENT_TO_SEMESTER_MOBILE));
            } else if (entity instanceof SppSchedule)
            {
                List<Long> periodIds = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e").distinct()
                        .column(property(SppSchedulePeriod.id().fromAlias("e")))
                        .where(eq(property(SppSchedulePeriod.weekRow().schedule().id().fromAlias("e")), value(id)))
                        .createStatement(getSession()).list();

                for (Long periodId : periodIds) resultIdList.add(new CoreCollectionUtils.Pair<>(periodId, PERIOD_MOBILE));
            } else if (entity instanceof SppScheduleWeekRow)
            {
                List<Long> periodIds = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e").distinct()
                        .column(property(SppSchedulePeriod.id().fromAlias("e")))
                        .where(eq(property(SppSchedulePeriod.weekRow().id().fromAlias("e")), value(id)))
                        .createStatement(getSession()).list();

                for (Long periodId : periodIds) resultIdList.add(new CoreCollectionUtils.Pair<>(periodId, PERIOD_MOBILE));
            } else if (entity instanceof SppSchedulePeriodFix)
            {
                //TODO что делать с фиксами? PERIOD_MOBILE);
            } else if (entity instanceof SppScheduleSeason)
            {
                List<Long> periodIds = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e").distinct()
                        .column(property(SppSchedulePeriod.id().fromAlias("e")))
                        .where(eq(property(SppSchedulePeriod.weekRow().schedule().season().id().fromAlias("e")), value(id)))
                        .createStatement(getSession()).list();

                for (Long periodId : periodIds) resultIdList.add(new CoreCollectionUtils.Pair<>(periodId, PERIOD_MOBILE));
            } else if (entity instanceof TrEduGroupEvent)
            {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "s")
                        .column(property(TrEduGroupEvent.group().fromAlias("s")))
                        .where(eq(property(TrEduGroupEvent.id().fromAlias("s")), value(id)));

                List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                        .column(property(TrJournalGroupStudent.id().fromAlias("e")))
                        .where(in(property(TrJournalGroupStudent.group().group().fromAlias("e")), subBuilder.buildQuery()))
                        .createStatement(getSession()).list();

                for (Long periodId : tjGrStudentIds)
                    resultIdList.add(new CoreCollectionUtils.Pair<>(periodId, RESULT_MOBILE));
            } else if (entity instanceof TrEduGroupEventStudent)
            {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "s")
                        .column(property(TrEduGroupEventStudent.event().group().fromAlias("s")))
                        .where(eq(property(TrEduGroupEventStudent.id().fromAlias("s")), value(id)));

                List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                        .column(property(TrJournalGroupStudent.id().fromAlias("e")))
                        .where(in(property(TrJournalGroupStudent.group().group().fromAlias("e")), subBuilder.buildQuery()))
                        .createStatement(getSession()).list();

                for (Long periodId : tjGrStudentIds)
                    resultIdList.add(new CoreCollectionUtils.Pair<>(periodId, RESULT_MOBILE));
            } else if (entity instanceof TrJournal)
            {
                List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                        .column(property(TrJournalGroupStudent.id().fromAlias("e")))
                        .where(eq(property(TrJournalGroupStudent.group().journal().id().fromAlias("e")), value(id)))
                        .createStatement(getSession()).list();

                for (Long tjGrStudentId : tjGrStudentIds)
                    resultIdList.add(new CoreCollectionUtils.Pair<>(tjGrStudentId, RESULT_MOBILE));
            } else if (entity instanceof TrJournalModule)
            {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(TrJournalModule.class, "s")
                        .column(property(TrJournalModule.journal().fromAlias("s")))
                        .where(eq(property(TrJournalModule.id().fromAlias("s")), value(id)));

                List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                        .column(property(TrJournalGroupStudent.id().fromAlias("e")))
                        .where(in(property(TrJournalGroupStudent.group().journal().fromAlias("e")), subBuilder.buildQuery()))
                        .createStatement(getSession()).list();

                for (Long periodId : tjGrStudentIds)
                    resultIdList.add(new CoreCollectionUtils.Pair<>(periodId, RESULT_MOBILE));
            } else if (entity instanceof TrJournalEvent)
            {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(TrJournalEvent.class, "s")
                        .column(property(TrJournalEvent.journalModule().journal().fromAlias("s")))
                        .where(eq(property(TrEduGroupEventStudent.id().fromAlias("s")), value(id)));

                List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                        .column(property(TrJournalGroupStudent.id().fromAlias("e")))
                        .where(in(property(TrJournalGroupStudent.group().journal().fromAlias("e")), subBuilder.buildQuery()))
                        .createStatement(getSession()).list();

                for (Long periodId : tjGrStudentIds)
                    resultIdList.add(new CoreCollectionUtils.Pair<>(periodId, RESULT_MOBILE));
            } else if (entity instanceof TrJournalGroup)
            {
                List<Long> tjGrStudentIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                        .column(property(TrJournalGroupStudent.id().fromAlias("e")))
                        .where(eq(property(TrJournalGroupStudent.group().id().fromAlias("e")), value(id)))
                        .createStatement(getSession()).list();

                for (Long tjGrStudentId : tjGrStudentIds)
                    resultIdList.add(new CoreCollectionUtils.Pair<>(tjGrStudentId, RESULT_MOBILE));
            } else if (entity instanceof SessionSlotLinkMark)
            {
                List<Long> sessSlotLinkMarkIds = new DQLSelectBuilder().fromEntity(SessionSlotLinkMark.class, "e").distinct()
                        .column(property(SessionSlotLinkMark.target().id().fromAlias("e")))
                        .where(eq(property(SessionSlotLinkMark.id().fromAlias("e")), value(id)))
                        .createStatement(getSession()).list();

                for (Long sessSlotLinkMarkId : sessSlotLinkMarkIds)
                    resultIdList.add(new CoreCollectionUtils.Pair<>(sessSlotLinkMarkId, RESULT_MOBILE));
            }

        }

        return resultIdList;
    }

    /**
     * По идентификатору объекта возвращает пару "Идентификатор объекта"-"Тип сущности"
     *
     * @param id - идентификатор объекта
     * @return - пара "Идентификатор объекта"-"Тип сущности"
     */
    private List<CoreCollectionUtils.Pair<Long, String>> getActualDeletedIEntityTypeAndIdentifierPairList(Long id)
    {
        IEntityMeta meta = EntityRuntime.getMeta(id);
        if (null == meta) return null;

        List<CoreCollectionUtils.Pair<Long, String>> resultIdList = new ArrayList<>();

        if (Student.ENTITY_NAME.equals(meta.getName()) || Person.ENTITY_NAME.equals(meta.getName()))
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                    .column(property(Student.id().fromAlias("s")))
                    .column(property(Student.person().id().fromAlias("s")));

            //Set<Long> studIdSet = new HashSet<>();
            if (Student.ENTITY_NAME.equals(meta.getName()))
                builder.where(eq(property(Student.id().fromAlias("s")), value(id)));
            else if (Person.ENTITY_NAME.equals(meta.getName()))
                builder.where(eq(property(Student.person().id().fromAlias("s")), value(id)));

            List<Object[]> stuPerIds = builder.createStatement(getSession()).list();

            for (Object[] item : stuPerIds)
            {
                resultIdList.add(new CoreCollectionUtils.Pair<>((Long)item[0], STUDENT_MOBILE));
                resultIdList.add(new CoreCollectionUtils.Pair<>((Long)item[1], PERSON_MOBILE));
                //studIdSet.add((Long) item[0]);
            }

            /*List<Long> stu2SemBuilder = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "e")
                    .column(property(EppStudentWorkPlanElement.id().fromAlias("e")))
                    .where(in(property(EppStudentWorkPlanElement.student().id().fromAlias("e")), studIdSet))
                    .createStatement(getSession()).list();

            for(Long s2s : stu2SemBuilder) resultIdList.add(new CoreCollectionUtils.Pair(s2s, STUDENT_TO_SEMESTER_MOBILE));

            List<Long> jgsBuilder = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e")
                    .column(property(TrJournalGroupStudent.id().fromAlias("e")))
                    .where(in(property(TrJournalGroupStudent.studentWpe().student().id().fromAlias("e")), studIdSet))
                    .createStatement(getSession()).list();

            for(Long jqs : jgsBuilder) resultIdList.add(new CoreCollectionUtils.Pair(jqs, RESULT_MOBILE));

            List<Long> sessBuilder = new DQLSelectBuilder().fromEntity(SessionSlotLinkMark.class, "e")
                    .column(property(SessionSlotLinkMark.target().id().fromAlias("e")))
                    .where(in(property(SessionSlotLinkMark.target().slot().studentWpeCAction().studentWpe().student().id().fromAlias("e")), studIdSet))
                    .createStatement(getSession()).list();

            for(Long sess : sessBuilder) resultIdList.add(new CoreCollectionUtils.Pair(sess, RESULT_MOBILE));*/
        } else if (EppStudentWorkPlanElement.ENTITY_NAME.equals(meta.getName()))
        {
            resultIdList.add(new CoreCollectionUtils.Pair<>(id, STUDENT_TO_SEMESTER_MOBILE));
        } else if (SppSchedulePeriod.ENTITY_NAME.equals(meta.getName()) || SppSchedulePeriodFix.ENTITY_NAME.equals(meta.getName()))
        {
            resultIdList.add(new CoreCollectionUtils.Pair<>(id, PERIOD_MOBILE));
        } else if (TrJournalGroupStudent.ENTITY_NAME.equals(meta.getName()))
        {
            resultIdList.add(new CoreCollectionUtils.Pair<>(id, RESULT_MOBILE));
        } else
        {
            resultIdList.addAll(getActualIEntityTypeAndIdentifierPairList(id));
        }

        return resultIdList;
    }

    /**
     * Регистрирует изменения сущностей для дальнейшей выдачи только добавленных, изменившихся и удалённых объектов серверу мобильного приложения
     *
     * @param entityIds - список идентификаторов объектов, по которым произошли изменения
     */
    @Override
    public void doRegisterChangedEntities(List<Long> entityIds, boolean delete)
    {
        /*
        // Проверяем наличие записей об изменениях в базе. Если сущность уже изменилась и есть среди записей, то нужно её убрать из этого списка.
        List<Long> existEntityId = new DQLSelectBuilder().fromEntity(FefuChangedMobileEntity.class, "e")
                .column(property(FefuChangedMobileEntity.entityId().fromAlias("e")))
                .where(in(property(FefuChangedMobileEntity.entityId().fromAlias("e")), entityIds))
                .createStatement(getSession()).list();

        entityIds.removeAll(existEntityId); */

        Map<String, Set<Long>> localEntityMap = new HashMap<>();
        Set<Long> correctedEntityIds = new HashSet<>();

        for (Long id : entityIds)
        {
            List<CoreCollectionUtils.Pair<Long, String>> actualIdToTypePairsList = delete ? getActualDeletedIEntityTypeAndIdentifierPairList(id) : getActualIEntityTypeAndIdentifierPairList(id);

            if (null != actualIdToTypePairsList)
            {
                for (CoreCollectionUtils.Pair<Long, String> pair : actualIdToTypePairsList)
                {
                    if (delete || !PREPROCESSED_ENTITY_MAP.containsKey(pair.getY()) || !PREPROCESSED_ENTITY_MAP.get(pair.getY()).contains(pair.getX()))
                    {
                        Set<Long> typedEntityIdsList = localEntityMap.get(pair.getY());
                        if (null == typedEntityIdsList) typedEntityIdsList = new HashSet<>();
                        typedEntityIdsList.add(pair.getX());
                        localEntityMap.put(pair.getY(), typedEntityIdsList);
                        correctedEntityIds.add(pair.getX());
                    }
                }
            }
        }

        // Проверяем наличие записей об изменениях в базе, с учетом связанных объектов. Если сущность уже изменилась и есть среди записей, то нужно её убрать из этого списка.
        DQLSelectBuilder deletedExistEntityBuilder = new DQLSelectBuilder().fromEntity(FefuChangedMobileEntity.class, "e")
                .column(property(FefuChangedMobileEntity.entityId().fromAlias("e")))
                .where(in(property(FefuChangedMobileEntity.entityId().fromAlias("e")), correctedEntityIds));

        if (delete)
            deletedExistEntityBuilder.where(eq(property(FefuChangedMobileEntity.deleted().fromAlias("e")), value(Boolean.TRUE)));

        List<Long> existEntityId = deletedExistEntityBuilder.createStatement(getSession()).list();

        if (delete)
        {
            List<FefuChangedMobileEntity> updatedEntityList = new DQLSelectBuilder().fromEntity(FefuChangedMobileEntity.class, "e").column(property("e"))
                    .where(in(property(FefuChangedMobileEntity.entityId().fromAlias("e")), correctedEntityIds))
                    .where(eq(property(FefuChangedMobileEntity.deleted().fromAlias("e")), value(Boolean.FALSE)))
                    .createStatement(getSession()).list();

            for (FefuChangedMobileEntity changedMobileEntity : updatedEntityList)
            {
                changedMobileEntity.setDeleted(true);
                existEntityId.add(changedMobileEntity.getEntityId());
            }
        }

        if (localEntityMap.entrySet().size() > 0)
        {
            int updCnt = 0;
            DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(FefuChangedMobileEntity.class);
            for (Map.Entry<String, Set<Long>> entry : localEntityMap.entrySet())
            {
                Set<Long> globalSet = PREPROCESSED_ENTITY_MAP.get(entry.getKey());
                if (null == globalSet) globalSet = new HashSet<>();

                for (Long id : entry.getValue())
                {
                    if (!existEntityId.contains(id))
                    {
                        insertBuilder.value(FefuChangedMobileEntity.entityId().s(), id);
                        insertBuilder.value(FefuChangedMobileEntity.entityType().s(), entry.getKey());
                        insertBuilder.value(FefuChangedMobileEntity.deleted().s(), delete);
                        insertBuilder.addBatch();
                        globalSet.add(id);
                        updCnt++;
                    }
                    if (updCnt == 300)
                    {
                        createStatement(insertBuilder).execute();
                        insertBuilder = new DQLInsertValuesBuilder(FefuChangedMobileEntity.class);
                        updCnt = 0;
                    }
                }
                PREPROCESSED_ENTITY_MAP.put(entry.getKey(), globalSet);
            }

            if (updCnt > 0) createStatement(insertBuilder).execute();
        }
    }

    public void doRegisterDeletedEntities(Long entityId)
    {
        IEntityMeta meta = EntityRuntime.getMeta(entityId);
        if (null == meta) return;

        // Проверяем наличие записей об изменениях в базе. Если сущность уже изменилась и есть среди записей, то нужно её убрать из этого списка.
        List<Long> existEntityId = new DQLSelectBuilder().fromEntity(FefuChangedMobileEntity.class, "e")
                .column(property(FefuChangedMobileEntity.entityId().fromAlias("e")))
                .where(eq(property(FefuChangedMobileEntity.entityId().fromAlias("e")), value(entityId)))
                .createStatement(getSession()).list();

        if (!existEntityId.isEmpty()) return;

        IEntity entity = get(entityId);
        System.out.println(meta);
    }

    /**
     * Возвращает по типу сущности список идентификаторов добавившихся, изменившихся и удалённых объектов, синхронизируемых с мобильным приложением
     *
     * @param entityName - тип сущности (наименование сущности в УНИ)
     * @return - список идентификаторов добавленных, измененных и удаённых объектов заданного типа
     */
    @Override
    public List<Long> getNewAndUpdatedEntityIdsList(String entityName)
    {
        return new DQLSelectBuilder().fromEntity(FefuChangedMobileEntity.class, "e")
                .column(property(FefuChangedMobileEntity.entityId().fromAlias("e")))
                .where(eq(property(FefuChangedMobileEntity.entityType().fromAlias("e")), value(entityName)))
                .where(eq(property(FefuChangedMobileEntity.deleted().fromAlias("e")), value(Boolean.FALSE)))
                .createStatement(getSession()).list();
    }

    /**
     * Возвращает по типу сущности список идентификаторов добавившихся, изменившихся и удалённых объектов, синхронизируемых с мобильным приложением
     *
     * @param entityName - тип сущности (наименование сущности в УНИ)
     * @return - список идентификаторов добавленных, измененных и удаённых объектов заданного типа
     */
    @Override
    public List<Long> getDeletedEntityIdsList(String entityName)
    {
        return new DQLSelectBuilder().fromEntity(FefuChangedMobileEntity.class, "e")
                .column(property(FefuChangedMobileEntity.entityId().fromAlias("e")))
                .where(eq(property(FefuChangedMobileEntity.entityType().fromAlias("e")), value(entityName)))
                .where(eq(property(FefuChangedMobileEntity.deleted().fromAlias("e")), value(Boolean.TRUE)))
                .createStatement(getSession()).list();
    }

    /**
     * Подтверждает обработку добавленных, измененных и удалённых объектов на стороне сервера мобильного приложения, дабы исключить повторную обработку этих объектов.
     * По сути, удаляет соответствующие идентификаторы из очереди на отправку на сервер мобильного приложения.
     *
     * @param entityIdsList - список идентификаторов объектов, по которым подтверждается успешная (или не очень успешная) обработка.
     */
    @Override
    public void doCommitEntityByIdList(List<Long> entityIdsList)
    {
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuChangedMobileEntity.class);
        deleteBuilder.where(in(property(FefuChangedMobileEntity.entityId()), entityIdsList));
        deleteBuilder.createStatement(getSession()).execute();

        for (Map.Entry<String, Set<Long>> entry : PREPROCESSED_ENTITY_MAP.entrySet())
        {
            entry.getValue().removeAll(entityIdsList);
        }
    }

    /**
     * Возвращает список троек ID студента, ID учебной группы, рабочий план для дальнейшего вычисления в каких учебных группах числится студент,
     * и того, какой семестр у студента является последним среди привязанных рабочих планов.
     *
     * @param studentIds - список идентификаторов студентов
     * @return - список троек ID студента, ID учебной группы, рабочий план
     */
    private List<Object[]> getStudentsEduGroupList(List<Long> studentIds)
    {
        if (null == studentIds || studentIds.isEmpty()) return new ArrayList<>();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, "eg")
                .column(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().id().fromAlias("eg")))
                .column(property(EppRealEduGroupRow.group().id().fromAlias("eg")))
                .column(property(EppRealEduGroupRow.group().workPlanRow().workPlan().fromAlias("eg")))
                        //.column(property(EppRealEduGroupRow.student().fromAlias("eg")));
                .where(in(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().id().fromAlias("eg")), studentIds));

        return builder.createStatement(getSession()).list();
    }

    /**
     * Возвращает список троек ID предмета (дисциплино-части), ID учебной группы, рабочий план для дальнейшего вычисления в каких учебных группах числится предмет.
     * Нужно для дальнейшего соотнесения студентов с предметами.
     *
     * @param subjectIds - список идентификаторов предметов (дисциплино-частей)
     * @return - список троек ID предмета (дисциплино-части), ID учебной группы, рабочий план
     */
    private List<Object[]> getSubjectEduGroupList(List<Long> subjectIds)
    {
        if (null == subjectIds || subjectIds.isEmpty()) return new ArrayList<>();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, "eg")
                .column(property(EppRealEduGroupRow.group().workPlanRow().registryElementPart().id().fromAlias("eg")))
                .column(property(EppRealEduGroupRow.group().id().fromAlias("eg")))
                .column(property(EppRealEduGroupRow.group().workPlanRow().workPlan().fromAlias("eg")))
                .where(in(property(EppRealEduGroupRow.group().workPlanRow().registryElementPart().registryElement().fromAlias("eg")), subjectIds));

        return builder.createStatement(getSession()).list();
    }


    // УЧЕБНЫЕ ГОДА

    /**
     * Возвращает список учебных годов, упорядоченных по названию
     *
     * @return - список учебных годов
     */
    @Override
    public List<EduYear> getEduYearsList()
    {
        List<EducationYear> educationYearList = new DQLSelectBuilder().fromEntity(EducationYear.class, "e")
                .column("e").order(property(EducationYear.title().fromAlias("e")))
                .createStatement(getSession()).list();

        List<EduYear> result = new ArrayList<>();

        for (EducationYear year : educationYearList)
        {
            EduYear eduYear = new EduYear();
            eduYear.setId(String.valueOf(year.getId()));
            eduYear.setStartYear(String.valueOf(year.getIntValue()));
            eduYear.setTitle(year.getTitle());
            result.add(eduYear);
        }

        return result;
    }

    /**
     * Возвращает текущий учебный год
     *
     * @return - текущиё учебный год
     */
    @Override
    public EduYear getCurrentEduYear()
    {
        List<EducationYear> educationYearList = new DQLSelectBuilder().fromEntity(EducationYear.class, "e")
                .column("e").order(property(EducationYear.title().fromAlias("e")), OrderDirection.desc)
                .where(eq(property(EducationYear.current().fromAlias("e")), value(Boolean.TRUE)))
                .createStatement(getSession()).list();

        if (!educationYearList.isEmpty())
        {
            EducationYear year = educationYearList.get(0);

            EduYear eduYear = new EduYear();
            eduYear.setId(String.valueOf(year.getId()));
            eduYear.setStartYear(String.valueOf(year.getIntValue()));
            eduYear.setTitle(year.getTitle());
            return eduYear;
        }

        throw new ApplicationException("Current education year is not specified.");
    }


    // ПЕРСОНЫ

    /**
     * Возвращает список идентификаторов не архивных студенческих персон
     *
     * @return - список идентификаторов студенческих персон
     */
    @Override
    public List<Long> getActiveStudentPersonsList()
    {
        Set<String> usedLogins = new HashSet<>();
        List<Long> personIds = new ArrayList<>();
        List<Object[]> persons = new DQLSelectBuilder().fromEntity(Student.class, "e").distinct()
                .column(property(Student.person().id().fromAlias("e")))
                .column(property(Student.principal().login().fromAlias("e")))
                .column(property(FefuNsiPersonExt.login().fromAlias("ex")))
                .joinEntity("e", DQLJoinType.left, FefuNsiPersonExt.class, "ex", eq(property(Student.person().id().fromAlias("e")), property(FefuNsiPersonExt.person().id().fromAlias("ex"))))
                .where(eq(property(Student.archival().fromAlias("e")), value(Boolean.FALSE)))
                .order(property(Student.person().id().fromAlias("e"))).createStatement(getSession()).list();

        for (Object[] obj : persons)
        {
            String login = (String) (null != obj[2] ? obj[2] : obj[1]);
            if (!usedLogins.contains(login)) personIds.add((Long) obj[0]);
            usedLogins.add(login);
        }

        return personIds;
    }

    /**
     * Возвращает список персон по списку идентификаторов
     *
     * @param personIdsList - список идентификаторов персон
     * @return - список персон
     */
    @Override
    public List<ru.tandemservice.unifefu.ws.mobile.entity.Person> getPersonsByIdList(List<Long> personIdsList)
    {
        List<Object[]> personList = new DQLSelectBuilder().fromEntity(Person.class, "e")
                .joinEntity("e", DQLJoinType.left, Person2PrincipalRelation.class, "r", eq(property(Person.id().fromAlias("e")), property(Person2PrincipalRelation.person().id().fromAlias("r"))))
                .joinEntity("e", DQLJoinType.left, FefuNsiPersonExt.class, "ex", eq(property(Person.id().fromAlias("e")), property(FefuNsiPersonExt.person().id().fromAlias("ex"))))
                .column(property(Person.id().fromAlias("e")))
                .column(property(Person2PrincipalRelation.principal().login().fromAlias("r")))
                .column(property(Person.identityCard().lastName().fromAlias("e")))
                .column(property(Person.identityCard().firstName().fromAlias("e")))
                .column(property(Person.identityCard().middleName().fromAlias("e")))
                .column(property(Person.identityCard().sex().code().fromAlias("e")))
                .column(property(Person.identityCard().birthDate().fromAlias("e")))
                .column(property(FefuNsiPersonExt.login().fromAlias("ex")))
                .where(in(property(Person.id().fromAlias("e")), personIdsList))
                .order(property(Person.id().fromAlias("e")))
                .createStatement(getSession()).list();

        List<ru.tandemservice.unifefu.ws.mobile.entity.Person> result = new ArrayList<>();
        for (Object[] personItem : personList)
        {
            Long personId = (Long) personItem[0];
            Date birthDate = (Date) personItem[6];
            String login = (String) (null != personItem[7] ? personItem[7] : personItem[1]);

            ru.tandemservice.unifefu.ws.mobile.entity.Person person = new ru.tandemservice.unifefu.ws.mobile.entity.Person();
            person.setId(String.valueOf(personId));
            person.setLastname((String) personItem[2]);
            person.setFirstname((String) personItem[3]);
            person.setMiddlename((String) personItem[4]);
            person.setSex(SexCodes.MALE.equals(personItem[5]) ? "0" : "1");
            person.setBirthday(NsiDatagramUtil.formatDate(birthDate));
            person.setUsername(login);

            result.add(person);
        }

        return result;
    }

    /**
     * Возвращает персону по идентификатору персоны
     *
     * @param personId - идентификатор персоны
     * @return - перосона
     */
    @Override
    public ru.tandemservice.unifefu.ws.mobile.entity.Person getPersonById(Long personId)
    {
        Person person = get(Person.class, personId);
        if (null == person)
        {
            throw new ApplicationException("Student with given identifier does not exists.");
        }

        Person2PrincipalRelation principalRelation = getByNaturalId(new Person2PrincipalRelation.NaturalId(person));
        FefuNsiPersonExt ext = getByNaturalId(new FefuNsiPersonExt.NaturalId(person));

        ru.tandemservice.unifefu.ws.mobile.entity.Person personWrapper = new ru.tandemservice.unifefu.ws.mobile.entity.Person();
        personWrapper.setId(String.valueOf(personId));
        personWrapper.setLastname(person.getIdentityCard().getLastName());
        personWrapper.setFirstname(person.getIdentityCard().getFirstName());
        personWrapper.setMiddlename(person.getIdentityCard().getMiddleName());
        personWrapper.setSex(SexCodes.MALE.equals(person.getIdentityCard().getSex().getCode()) ? "0" : "1");
        personWrapper.setBirthday(NsiDatagramUtil.formatDate(person.getIdentityCard().getBirthDate()));
        personWrapper.setUsername(null != ext ? ext.getLogin() : (null != principalRelation ? principalRelation.getPrincipal().getLogin() : null));

        return personWrapper;
    }


    // СТУДЕНТЫ

    /**
     * Возвращает список идентификаторов не архивных студентов.
     * Используется при первичном наполнении интегрируемой подсистемы.
     *
     * @return - список идентификаторов не архивных студентов
     */
    @Override
    public List<Long> getActiveStudentIdsList()
    {
        List<Long> studentIds = new DQLSelectBuilder().fromEntity(Student.class, "e").column(property(Student.id().fromAlias("e")))
                .where(eq(property(Student.archival().fromAlias("e")), value(Boolean.FALSE)))
                .order(property(Student.id().fromAlias("e"))).createStatement(getSession()).list();

        return studentIds;
    }

    /**
     * Возвращает список студентов по списку идентификаторов
     *
     * @param studentIdsList - список идентификаторов студентов
     * @return - список студентов
     */
    @Override
    public List<ru.tandemservice.unifefu.ws.mobile.entity.Student> getStudentsByIdList(List<Long> studentIdsList)
    {
        List<Object[]> studentList = new DQLSelectBuilder().fromEntity(Student.class, "e")
                .column(property(Student.id().fromAlias("e")))
                .column(property(Student.person().id().fromAlias("e")))
                .column(property(Student.group().id().fromAlias("e")))
                .column(property(Student.course().fromAlias("e")))
                .column(property(Student.educationOrgUnit().educationLevelHighSchool().title().fromAlias("e")))
                .column(property(Student.educationOrgUnit().educationLevelHighSchool().printTitle().fromAlias("e")))
                .column(property(Student.group().title().fromAlias("e")))
                .column(property(Student.group().educationOrgUnit().formativeOrgUnit().title().fromAlias("e")))
                .where(in(property(Student.id().fromAlias("e")), studentIdsList))
                .order(property(Student.id().fromAlias("e")))
                .createStatement(getSession()).list();

        List<ru.tandemservice.unifefu.ws.mobile.entity.Student> result = new ArrayList<>();
        for (Object[] studentItem : studentList)
        {
            ru.tandemservice.unifefu.ws.mobile.entity.Student student = new ru.tandemservice.unifefu.ws.mobile.entity.Student();
            student.setId(String.valueOf(studentItem[0]));
            student.setPersonId(String.valueOf(studentItem[1]));
            student.setAcademicGroupId(String.valueOf(studentItem[2]));
            student.setEduLevelTitle((String) studentItem[4]);
            student.setEduLevelShortTitle((String) studentItem[5]);
            student.setAcademicGroupTitle((String) studentItem[6]);
            student.setAcademicGroupOrgUnitTitle((String) studentItem[7]);

            result.add(student);
        }

        return result;
    }

    /**
     * Возвращает студента по идентификатору
     *
     * @param studentId - идетификатор студента
     * @return - студент
     */
    @Override
    public ru.tandemservice.unifefu.ws.mobile.entity.Student getStudentById(Long studentId)
    {
        Student student = get(Student.class, studentId);
        if (null == student)
        {
            throw new ApplicationException("Student with given identifier does not exists.");
        }

        ru.tandemservice.unifefu.ws.mobile.entity.Student studentWrapper = new ru.tandemservice.unifefu.ws.mobile.entity.Student();
        studentWrapper.setId(String.valueOf(studentId));
        studentWrapper.setPersonId(String.valueOf(student.getPerson().getId()));
        studentWrapper.setEduLevelTitle(student.getEducationOrgUnit().getEducationLevelHighSchool().getTitle());
        studentWrapper.setEduLevelShortTitle(student.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle());

        if (null != student.getGroup())
        {
            studentWrapper.setAcademicGroupId(String.valueOf(student.getGroup().getId()));
            studentWrapper.setAcademicGroupTitle(student.getGroup().getTitle());
            studentWrapper.setAcademicGroupOrgUnitTitle(null != student.getGroup().getEducationOrgUnit() ? student.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getTitle() : null);
        }

        return studentWrapper;
    }


    // ПРЕДМЕТЫ (дисцпилины)

    /**
     * Возвращает список идентификаторов дисциплин, на которые сформированы актуальные учебные группы студентов
     *
     * @return - сприсок идентификаторов дисциплин из учебных групп студентов
     */
    @Override
    public List<Long> getActiveSubjectIdsList()
    {
        List<Long> subjectIdsList = new ArrayList<>();

        subjectIdsList.addAll(new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "e").distinct()
                .column(property(EppStudentWorkPlanElement.registryElementPart().registryElement().id().fromAlias("e")))
                .where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("e"))))
                .order(property(EppStudentWorkPlanElement.registryElementPart().registryElement().id().fromAlias("e")))
                .createStatement(getSession()).<Long>list());

        List<Long> journalExploitedSubjectIds = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e").distinct()
                .column(property(TrJournalGroupStudent.group().journal().registryElementPart().registryElement().id().fromAlias("e")))
                .order(property(TrJournalGroupStudent.group().journal().registryElementPart().registryElement().id().fromAlias("e")))
                .createStatement(getSession()).list();

        journalExploitedSubjectIds.removeAll(subjectIdsList);
        subjectIdsList.addAll(journalExploitedSubjectIds);

        return subjectIdsList;
    }

    /**
     * Возвращает список дисциплин по списку идентификаторов дисциплин
     *
     * @param subjectIds - список идентификаторов дисциплин
     * @return - список предметов
     */
    @Override
    public List<Subject> getSubjectsByIdList(List<Long> subjectIds)
    {
        if (null == subjectIds || subjectIds.isEmpty()) return new ArrayList<>();

        List<Subject> subjectList = new ArrayList<>();
        Map<Long, IEppRegElWrapper> regElMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(subjectIds);
        EppELoadType selfWorkELoadType = getCatalogItem(EppELoadType.class, EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);

        for (Map.Entry<Long, IEppRegElWrapper> regElEntry : regElMap.entrySet())
        {
            IEppRegElWrapper regElementWrapper = regElMap.get(regElEntry.getKey());

            if (null != regElementWrapper)
            {
                List<EppALoadType> loadTypesList = getList(EppALoadType.class);
                List<EppFControlActionGroup> controlActionGroups = getList(EppFControlActionGroup.class);

                Subject subject = new Subject();
                subject.setId(regElementWrapper.getId().toString());
                subject.setFullname(null != regElementWrapper.getItem().getFullTitle() ? regElementWrapper.getItem().getFullTitle() : regElementWrapper.getItem().getTitle());
                subject.setShortname(null != regElementWrapper.getItem().getShortTitle() ? regElementWrapper.getItem().getShortTitle() : regElementWrapper.getItem().getTitle());

                for (Map.Entry<Integer, IEppRegElPartWrapper> partEntry : regElementWrapper.getPartMap().entrySet())
                {
                    IEppRegElPartWrapper loadWrapper = partEntry.getValue();
                    if (null != loadWrapper)
                    {
                        Semester semester = new Semester();
                        semester.setId(String.valueOf(loadWrapper.getId()));
                        semester.setNum(String.valueOf(loadWrapper.getNumber()));

                        //TODO milestones - шкала для отображения рейтинга.

                        semester.setSeminars(String.valueOf(Double.valueOf(loadWrapper.getLoadAsDouble(selfWorkELoadType.getFullCode())).intValue()));

                        for (EppALoadType loadType : loadTypesList)
                        {
                            String load = String.valueOf(Double.valueOf(loadWrapper.getLoadAsDouble(loadType.getFullCode())).intValue());
                            if (EppALoadTypeCodes.TYPE_LABS.equals(loadType.getCode())) semester.setLabs(load);
                            else if (EppALoadTypeCodes.TYPE_LECTURES.equals(loadType.getCode()))
                                semester.setLectures(load);
                            else if (EppALoadTypeCodes.TYPE_PRACTICE.equals(loadType.getCode()))
                                semester.setPractices(load);
                        }

                        for (EppFControlActionGroup ctrlActionGrp : controlActionGroups)
                        {
                            if (loadWrapper.hasActions4Groups(ctrlActionGrp.getCode()))
                            {
                                if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(ctrlActionGrp.getCode()))
                                    semester.setExam(String.valueOf(Boolean.TRUE));
                                else if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_SETOFF.equals(ctrlActionGrp.getCode()))
                                    semester.setCredit(String.valueOf(Boolean.TRUE));
                            }
                        }
                        subject.getSemesters().add(semester);
                    }
                }
                subjectList.add(subject);
            }
        }

        return subjectList;
    }


    // СВЯЗИ СТУДЕНТОВ С ПРЕДМЕТАИ (дисцпилинами)

    /**
     * Возвращает список актуальных связей студентов с предметами
     *
     * @return - сприсок идентификаторов дисциплин из учебных групп студентов
     */
    @Override
    public List<Long> getActiveStudent2SemestersIdsList()
    {
        return new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "e").distinct()
                .column(property(EppStudentWorkPlanElement.id().fromAlias("e")))
                .where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("e"))))
                .order(property(EppStudentWorkPlanElement.id().fromAlias("e")))
                .createStatement(getSession()).list();
    }

    /**
     * Возвращает список связей студентов с предметами по списку идентификаторов связей
     *
     * @param student2SemesterIds - список идентификаторов связей студентов с предметами
     * @return - список связей студентов с предметами
     */
    @Override
    public List<Student2Semester> getStudent2SemestersByIdList(List<Long> student2SemesterIds)
    {
        if (null == student2SemesterIds || student2SemesterIds.isEmpty()) return new ArrayList<>();

        List<Object[]> items = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "e")
                .column(property(EppStudentWorkPlanElement.id().fromAlias("e")))
                .column(property(EppStudentWorkPlanElement.student().id().fromAlias("e")))
                .column(property(EppStudentWorkPlanElement.registryElementPart().id().fromAlias("e")))
                .column(property(EppStudentWorkPlanElement.year().educationYear().id().fromAlias("e")))
                        //.column(property(EppStudentWorkPlanElement.registryElementPart().number().fromAlias("e"))) // Удобнее сравнивать по номеру семестра в РП
                .column(property(EppStudentWorkPlanElement.term().intValue().fromAlias("e")))
                .where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("e"))))
                .where(in(property(EppStudentWorkPlanElement.id().fromAlias("e")), student2SemesterIds))
                .order(property(EppStudentWorkPlanElement.student().id().fromAlias("e")))
                .order(property(EppStudentWorkPlanElement.registryElementPart().registryElement().id().fromAlias("e")))
                .order(property(EppStudentWorkPlanElement.id().fromAlias("e")))
                .createStatement(getSession()).list();

        List<Student2Semester> student2SemesterList = new ArrayList<>();

        for (Object[] item : items)
        {
            Student2Semester student2Subject = new Student2Semester();
            student2Subject.setId(String.valueOf(item[0]));
            student2Subject.setStudentId(String.valueOf(item[1]));
            student2Subject.setSemesterId(String.valueOf(item[2]));
            student2Subject.setEduYearId(String.valueOf(item[3]));
            student2Subject.setSemNumber(String.valueOf(item[4]));
            student2SemesterList.add(student2Subject);
        }

        return student2SemesterList;
    }


    // ПАРЫ

    /**
     * Возвращает список идентификаторов учебных пар на основе учебного расписания
     *
     * @return - список идентификаторов учебных пар
     */
    @Override
    public List<Long> getActualPeriodsIdsList()
    {
        List<Long> result = new ArrayList<>();

        result.addAll(new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e")
                .column(property(SppSchedulePeriod.id().fromAlias("e")))
                .where(ge(property(SppSchedulePeriod.weekRow().schedule().season().endDate().fromAlias("e")), valueDate(new Date())))
                .where(eq(property(SppSchedulePeriod.weekRow().schedule().approved().fromAlias("e")), value(Boolean.TRUE)))
                .where(eq(property(SppSchedulePeriod.empty().fromAlias("e")), value(Boolean.FALSE)))
                .createStatement(getSession()).<Long>list());

        result.addAll(new DQLSelectBuilder().fromEntity(SppSchedulePeriodFix.class, "e")
                .column(property(SppSchedulePeriodFix.id().fromAlias("e")))
                .where(ge(property(SppSchedulePeriodFix.sppSchedule().season().endDate().fromAlias("e")), valueDate(new Date())))
                .where(eq(property(SppSchedulePeriodFix.sppSchedule().approved().fromAlias("e")), value(Boolean.TRUE)))
                .createStatement(getSession()).<Long>list());

        return result;
    }

    /**
     * Возвращает список учебных пар по списку идентификаторов учебных пар
     *
     * @param periodIds - список идентификаторов учебных пар
     * @return - список учебных пар
     */
    @Override
    public List<Period> getPeriodsByIdList(List<Long> periodIds)
    {
        if (null == periodIds || periodIds.isEmpty()) return new ArrayList<>();

        List<SppSchedulePeriod> sppPeriodsList = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e").column("e")
                .where(eq(property(SppSchedulePeriod.empty().fromAlias("e")), value(Boolean.FALSE)))
                .where(in(property(SppSchedulePeriod.id().fromAlias("e")), periodIds))
                .createStatement(getSession()).list();

        //Set<Long> subjectIdsSet = new HashSet<>();
        Map<Long, Period> periodsMap = new HashMap<>();

        for (SppSchedulePeriod sppPeriod : sppPeriodsList)
        {
            Period period = new Period();
            period.setId(String.valueOf(sppPeriod.getId()));
            if (null != sppPeriod.getSubjectVal())
            {
                period.setSubjectId(String.valueOf(sppPeriod.getSubjectVal().getId()));
                period.setSubjectTitle(sppPeriod.getSubjectVal().getTitle());
                period.setSubjectShortTitle(sppPeriod.getSubjectVal().getShortTitle());
                if (null == period.getSubjectShortTitle())
                    period.setSubjectShortTitle(sppPeriod.getSubjectVal().getTitle());
                //subjectIdsSet.add(sppPeriod.getSubjectVal().getId());
            } else
            {
                period.setSubjectTitle(sppPeriod.getSubject());
                period.setSubjectShortTitle(sppPeriod.getSubject());
            }

            if (sppPeriod.getWeekRow().getSchedule().isDiffEven())
                period.setWeek(sppPeriod.getWeekRow().isEven() ? "2" : "1");
            else period.setWeek("0");

            period.setDay(String.valueOf(sppPeriod.getDayOfWeek() - 1));
            period.setAcademicGroupId(String.valueOf(sppPeriod.getWeekRow().getSchedule().getGroup().getId()));
            period.setType(null != sppPeriod.getSubjectLoadType() ? sppPeriod.getSubjectLoadType().getCode() : EppALoadTypeCodes.TYPE_LECTURES);
            period.setDateStart(NsiDatagramUtil.formatDate(sppPeriod.getWeekRow().getSchedule().getSeason().getStartDate()));
            period.setDateEnd(NsiDatagramUtil.formatDate(sppPeriod.getWeekRow().getSchedule().getSeason().getEndDate()));
            period.setPosition(String.valueOf(sppPeriod.getPeriodNum()));
            period.setTimeStart(String.valueOf(sppPeriod.getWeekRow().getBell().getStartHour() * 60 + sppPeriod.getWeekRow().getBell().getStartMin()));
            period.setTimeEnd(String.valueOf(sppPeriod.getWeekRow().getBell().getEndHour() * 60 + sppPeriod.getWeekRow().getBell().getEndMin()));
            period.setPlace(sppPeriod.getLectureRoom());
            period.setLecturer(sppPeriod.getTeacher());

            periodsMap.put(null != sppPeriod.getSubjectVal() ? sppPeriod.getSubjectVal().getId() : sppPeriod.getId(), period);
        }

        List<SppSchedulePeriodFix> fixPeriodsList = new DQLSelectBuilder().fromEntity(SppSchedulePeriodFix.class, "e").column("e")
                .where(in(property(SppSchedulePeriodFix.id().fromAlias("e")), periodIds))
                .createStatement(getSession()).list();

        for (SppSchedulePeriodFix sppPeriod : fixPeriodsList)
        {
            Period period = new Period();
            period.setId(String.valueOf(sppPeriod.getId()));
            if (null != sppPeriod.getSubjectVal())
            {
                period.setSubjectId(String.valueOf(sppPeriod.getSubjectVal().getId()));
                period.setSubjectTitle(sppPeriod.getSubjectVal().getTitle());
                period.setSubjectShortTitle(sppPeriod.getSubjectVal().getShortTitle());
                if (null == period.getSubjectShortTitle())
                    period.setSubjectShortTitle(sppPeriod.getSubjectVal().getTitle());
                //subjectIdsSet.add(sppPeriod.getSubjectVal().getId());
            } else
            {
                period.setSubjectTitle(sppPeriod.getSubject());
                period.setSubjectShortTitle(sppPeriod.getSubject());
            }

            period.setAcademicGroupId(String.valueOf(sppPeriod.getSppSchedule().getGroup().getId()));
            period.setType(null != sppPeriod.getSubjectLoadType() ? sppPeriod.getSubjectLoadType().getCode() : EppALoadTypeCodes.TYPE_LECTURES);
            period.setDateStart(NsiDatagramUtil.formatDate(sppPeriod.getSppSchedule().getSeason().getStartDate()));
            period.setDateEnd(NsiDatagramUtil.formatDate(sppPeriod.getSppSchedule().getSeason().getEndDate()));
            period.setPosition(String.valueOf(sppPeriod.getPeriodNum()));
            period.setTimeStart(String.valueOf(sppPeriod.getBell().getStartHour() * 60 + sppPeriod.getBell().getStartMin()));
            period.setTimeEnd(String.valueOf(sppPeriod.getBell().getEndHour() * 60 + sppPeriod.getBell().getEndMin()));
            period.setDate(NsiDatagramUtil.formatDate(sppPeriod.getDate()));
            period.setPlace(sppPeriod.getLectureRoom());
            period.setLecturer(sppPeriod.getTeacher());

            periodsMap.put(null != sppPeriod.getSubjectVal() ? sppPeriod.getSubjectVal().getId() : sppPeriod.getId(), period);
        }

        /*if(!subjectIdsSet.isEmpty())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, "eg")
                    .column(property(EppRealEduGroupRow.group().workPlanRow().registryElementPart().registryElement().id().fromAlias("eg")))
                    .column(property(EppRealEduGroupRow.student().slot().student().student().group().id().fromAlias("eg")))
                    .column(property(EppRealEduGroupRow.group().id().fromAlias("eg")))

                    .where(in(property(EppRealEduGroupRow.group().workPlanRow().registryElementPart().registryElement().fromAlias("eg")), subjectIds));
        }

        List<Object[]> eduGroupRowsList = getSubjectEduGroupList(new ArrayList(subjectIdsSet));
        for (Object[] eduGroupRow : eduGroupRowsList)
                {
                    Long subjectId = (Long) eduGroupRow[0];
                    Long eduGroupId = (Long) eduGroupRow[1];
                    periodsMap.get
                }                              */


        return new ArrayList<>(periodsMap.values());
    }


    // RESULTS

    @Override
    public List<Long> getActualResultsIdsList()
    {
        List<Long> result = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e")
                .column(property(TrJournalGroupStudent.id().fromAlias("e")))
                .where(eq(property(TrJournalGroupStudent.group().journal().yearPart().year().educationYear().current().fromAlias("e")), value(Boolean.TRUE)))
                .where(eq(property(TrJournalGroupStudent.studentWpe().student().archival().fromAlias("e")), value(Boolean.FALSE)))
                .order(property(TrJournalGroupStudent.id().fromAlias("e")))
                .createStatement(getSession()).list();

        result.addAll(new DQLSelectBuilder().fromEntity(SessionSlotLinkMark.class, "e")
                .column(property(SessionSlotLinkMark.target().id().fromAlias("e")))
                .where(in(property(SessionSlotLinkMark.target().slot().studentWpeCAction().studentWpe().student().archival().fromAlias("e")), value(Boolean.FALSE)))
                .order(property(SessionSlotLinkMark.target().slot().studentWpeCAction().studentWpe().student().id().fromAlias("e")))
                .createStatement(getSession()).<Long>list());

        return result;
    }

    @Override
    public List<Result> getResultsByIdList(List<Long> resultIds)
    {
        // Получаем список всех студентов УГС, включенных в журнал (в реализацию дисциплины)
        //List<TrJournalGroupStudent> studentSlotsList = new ArrayList<>();

        List<TrJournalGroupStudent> studentSlotList = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e")
                .fetchPath(DQLJoinType.inner, TrJournalGroupStudent.group().journal().fromAlias("e"))
                .where(eq(property(TrJournalGroupStudent.studentWpe().student().archival().fromAlias("e")), value(Boolean.FALSE)))
                .where(in(property(TrJournalGroupStudent.id().fromAlias("e")), resultIds))
                .createStatement(getSession()).list();

        Set<Long> journalIdsSet = new HashSet<>();
        for (TrJournalGroupStudent stud : studentSlotList)
            journalIdsSet.add(stud.getGroup().getJournal().getId());

        // Подготавливаем Мап событий студента. Ключ - EppStudentEpvSlot.id, событие студента
        Map<Long, List<TrEduGroupEventStudent>> studentToEventsMap = new HashMap<>();

        List<TrEduGroupEventStudent> events = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "event").column("event")
                .where(in(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().id().fromAlias("event")), journalIdsSet))
                .createStatement(getSession()).list();

        for (TrEduGroupEventStudent event : events)
        {
            Long studentSlotId = event.getStudentWpe().getId();
            List<TrEduGroupEventStudent> studEventsList = studentToEventsMap.get(studentSlotId);
            if (null == studEventsList) studEventsList = new ArrayList<>();
            studEventsList.add(event);
            studentToEventsMap.put(studentSlotId, studEventsList);
        }

        // Подготавливаем Мап оценок. Ключ - пара(EppStudentEpvSlot.id, TrEduGroupEvent.id), значение - пара(оценка, дата оценки)
        // Дата оценки берется из истории оценок, если дата мероприятия не указана! - из последней (по дате) записи в истории.
        Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Date>> markActualMap = new HashMap<>();

        DQLSelectBuilder histDQL = new DQLSelectBuilder().fromEntity(FefuStudentMarkHistory.class, "hist")
                .column(property("hist", FefuStudentMarkHistory.L_TR_EDU_GROUP_EVENT_STUDENT), "stud_id")
                .column(DQLFunctions.max(property("hist", FefuStudentMarkHistory.P_MARK_DATE)), "mark_date")
                .where(in(property(FefuStudentMarkHistory.trEduGroupEventStudent().event().journalEvent().journalModule().journal().id().fromAlias("hist")), journalIdsSet))
                .group(property("hist", FefuStudentMarkHistory.L_TR_EDU_GROUP_EVENT_STUDENT));

        DQLSelectColumnNumerator histDQLNumerator = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromDataSource(histDQL.buildQuery(), "h")
                        .joinEntity("h", DQLJoinType.inner, TrEduGroupEventStudent.class, "m",
                                eq(property("h.stud_id"), property("m.id")))
        );

        int eventColumnIdx = histDQLNumerator.column(property("m", TrEduGroupEventStudent.event().id()));
        int slotColumnIdx = histDQLNumerator.column(property("m", TrEduGroupEventStudent.studentWpe().id()));
        int markColumnIdx = histDQLNumerator.column(property("m", TrEduGroupEventStudent.P_GRADE_AS_LONG));
        int markDateColumnIdx = histDQLNumerator.column(property("m", TrEduGroupEventStudent.event().deadlineDate()));
        int markHistoryDateColumnIdx = histDQLNumerator.column("h.mark_date");

        Iterable<Object[]> rows = scrollRows(histDQLNumerator.getDql().createStatement(getSession()));
        for (Object[] item : rows)
        {
            Long eventId = (Long) item[eventColumnIdx];
            Long slotId = (Long) item[slotColumnIdx];
            Long markAsLong = (Long) item[markColumnIdx];
            Double mark = (markAsLong != null) ? (markAsLong * 0.01d) : null;
            Date markDate = (Date) item[markDateColumnIdx];
            Date markHistoryDate = (Date) item[markHistoryDateColumnIdx];

            CoreCollectionUtils.Pair<Long, Long> key = new CoreCollectionUtils.Pair<>(slotId, eventId);
            CoreCollectionUtils.Pair<Double, Date> value = new CoreCollectionUtils.Pair<>(mark, null != markDate ? markDate : markHistoryDate);

            markActualMap.put(key, value);
        }

        // Подготавливаем Мап коэффициентов БРС для КМ. Ключ - TrJournalEvent.id, значение - список коэффициентов БРС для КМ журнала
        Map<Long, List<TrBrsCoefficientValue>> brsCoeffsMap = new HashMap<>();

        List<TrBrsCoefficientValue> brsCoeffsList = new DQLSelectBuilder().fromEntity(TrBrsCoefficientValue.class, "e").column("e")
                .joinEntity("e", DQLJoinType.inner, TrJournalEvent.class, "ev", eq(property(TrBrsCoefficientValue.owner().id().fromAlias("e")), property(TrJournalEvent.id().fromAlias("ev"))))
                .where(in(property(TrJournalEvent.journalModule().journal().id().fromAlias("ev")), journalIdsSet))
                        //.where(eq(property(TrJournalEvent.journalModule().journal().yearPart().year().educationYear().current().fromAlias("ev")), value(Boolean.TRUE)))
                .createStatement(getSession()).list();

        for (TrBrsCoefficientValue brsCoefficientValue : brsCoeffsList)
        {
            Long ownerId = brsCoefficientValue.getOwner().getId();
            List<TrBrsCoefficientValue> ownerBrsCoeffsList = brsCoeffsMap.get(ownerId);
            if (null == ownerBrsCoeffsList) ownerBrsCoeffsList = new ArrayList<>();
            ownerBrsCoeffsList.add(brsCoefficientValue);
            brsCoeffsMap.put(ownerId, ownerBrsCoeffsList);
        }

        //Подготавливаем Мап коэффициентов БРС для журнала. Ключ - TrJournal.id, значение - список коэффициентов БРС для журнала
        Map<Long, List<TrBrsCoefficientValue>> journalCoeffsMap = new HashMap<>();

        List<TrBrsCoefficientValue> journalCoeffsList = new DQLSelectBuilder().fromEntity(TrBrsCoefficientValue.class, "e").column("e")
                .joinEntity("e", DQLJoinType.inner, TrJournal.class, "j", eq(property(TrBrsCoefficientValue.owner().id().fromAlias("e")), property(TrJournal.id().fromAlias("j"))))
                .where(in(property(TrJournal.id().fromAlias("j")), journalIdsSet))
                        //.where(eq(property(TrJournal.yearPart().year().educationYear().current().fromAlias("j")), value(Boolean.TRUE)))
                .createStatement(getSession()).list();

        for (TrBrsCoefficientValue brsCoefficientValue : journalCoeffsList)
        {
            Long ownerId = brsCoefficientValue.getOwner().getId();
            List<TrBrsCoefficientValue> ownerBrsCoeffsList = journalCoeffsMap.get(ownerId);
            if (null == ownerBrsCoeffsList) ownerBrsCoeffsList = new ArrayList<>();
            ownerBrsCoeffsList.add(brsCoefficientValue);
            journalCoeffsMap.put(ownerId, ownerBrsCoeffsList);
        }


        // Кэш для рейтингов. Ключ - TrJournal.id, значение - рейтинг студентов по журналу
        Map<Long, IBrsDao.ICurrentRatingCalc> ratingMap = new HashMap<>();

        // Кэш дополнительных атрибутов журнала. Ключ - TrJournal.id, значение - мап (ключ - код параметра) всех дополнительных атрибутов журнала
        Map<Long, Map<String, ISessionBrsDao.IRatingAdditParamDef>> additParamDefMap = new HashMap<>();

        // Мап для соотнесения результата с конкретным слотом студента в рабочем плане (нужно для вставки итоговых оценок в сессию)
        Map<String, Long> resultIdToWorkplanSlotIdMap = new HashMap<>();
        Set<Long> workPlanSlotIdsSet = new HashSet<>();

        Set<Long> regElIdSet = new HashSet<>();
        List<Result> resultsList = new ArrayList<>();

        for (TrJournalGroupStudent journalStudent : studentSlotList)
        {
            EppStudentWorkPlanElement slot = journalStudent.getStudentWpe();
            Student student = slot.getStudent();
            TrJournal journal = journalStudent.getGroup().getJournal();
            regElIdSet.add(slot.getRegistryElementPart().getRegistryElement().getId());
            // Получаем закэшированный набор дополнительных атрибутов журнала, либо поднимаем его из базы и кэшируем
            Map<String, ISessionBrsDao.IRatingAdditParamDef> journalAdditParamDefMap = additParamDefMap.get(journal.getId());
            if (null == journalAdditParamDefMap)
            {
                try
                {
                    journalAdditParamDefMap = TrBrsCoefficientManager.instance().brsDao().getRatingAdditionalParamDefinitions(journal);
                    additParamDefMap.put(journal.getId(), journalAdditParamDefMap);
                } catch (Exception e)
                {
                    e.printStackTrace(System.err);
                }
            }

            Result result = new Result();
            result.setId(String.valueOf(journalStudent.getId()));
            result.setStudentId(String.valueOf(student.getId()));
            result.setSubjectId(String.valueOf(slot.getRegistryElementPart().getRegistryElement().getId()));
            result.setSemester(String.valueOf(slot.getTerm().getIntValue()));
            //result.setSemester(String.valueOf(slot.getRegistryElementPart().getNumber())); //TODO реальный семестр по РП заменили на номер дисциплино-части, чтобы связать с semester
            result.setEduYearId(String.valueOf(slot.getYear().getEducationYear().getId()));

            resultIdToWorkplanSlotIdMap.put(result.getId(), slot.getId());
            workPlanSlotIdsSet.add(slot.getId());

            //String.valueOf(slot.getYear().getEducationYear().getTitle() + " / "  + slot.getSourceRow().getWorkPlan().getTerm().getIntValue() + " - " + slot.getGridTerm().getPartNumber() + " (" + slot.getGridTerm().getPartAmount() + ")");
            if (null != journalCoeffsMap.get(journal.getId()))
            {
                List<TrBrsCoefficientValue> coefficientValueList = journalCoeffsMap.get(journal.getId());
                if (null != coefficientValueList)
                {
                    for (TrBrsCoefficientValue brsCoefficientValue : coefficientValueList)
                    {
                        if (!"0".equals(brsCoefficientValue.getValueStr()))
                        {
                            Milestone milestone = new Milestone();
                            milestone.setValue(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(brsCoefficientValue.getValueAsDouble()));

                            String brsDefTitle = brsCoefficientValue.getDefinition().getShortTitle();
                            if (brsDefTitle.startsWith("01.")) milestone.setTitle("3");
                            else if (brsDefTitle.startsWith("02.")) milestone.setTitle("4");
                            else if (brsDefTitle.startsWith("03.")) milestone.setTitle("5");
                            else milestone.setTitle(brsDefTitle.replaceAll(">=", "").replaceAll("<=", "").trim());

                            result.getMilestones().add(milestone);
                        }
                    }
                }
            }


            // Получаем закэшированный рейтинг студентов по журналу, либо поднимаем его из базы и кэшируем
            IBrsDao.ICurrentRatingCalc rating = ratingMap.get(journal.getId());
            if (null == rating)
            {
                try
                {
                    rating = TrBrsCoefficientManager.instance().brsDao().getCalculatedRating(journal);
                } catch (Exception e)
                {
                    e.printStackTrace(System.err);
                }
            }

            ratingMap.put(journal.getId(), rating);

            if (null != rating)
            {
                IBrsDao.IStudentCurrentRatingData studentRating = rating.getCurrentRating(slot);
                ISessionBrsDao.IRatingValue ratValue = studentRating.getRatingValue();
                if (null != ratValue)
                {
                    result.setResult(null != ratValue.getMessage() ? ratValue.getMessage() : BrsIRatingValueFormatter.instance.format(ratValue));
                    result.setMaxResult("100");
                }
            }


            if (studentToEventsMap.containsKey(slot.getId()))
            {
                for (TrEduGroupEventStudent eventStud : studentToEventsMap.get(slot.getId()))
                {
                    WorkResult workResult = new WorkResult();
                    workResult.setTitle(eventStud.getEvent().getJournalEvent().getTitle());
                    workResult.setDate(NsiDatagramUtil.formatDate(eventStud.getEvent().getDeadlineDate()));
                    //System.out.println(eventStud.getEvent().getJournalEvent().getType().getTitle() + " - " + eventStud.getEvent().getJournalEvent().getType().getCode());
                    //EppALoadTypeCodes.

                    List<TrBrsCoefficientValue> coefficientValueList = brsCoeffsMap.get(eventStud.getEvent().getJournalEvent().getId());
                    if (null != coefficientValueList)
                    {
                        for (TrBrsCoefficientValue brsCoefficientValue : coefficientValueList)
                        {
                            if (FefuBrs.MAX_POINTS_CODE.equals(brsCoefficientValue.getDefinition().getUserCode()))
                                workResult.setMaxValue(brsCoefficientValue.getValueStr());
                        }
                    }

                    CoreCollectionUtils.Pair<Long, Long> key = new CoreCollectionUtils.Pair<>(slot.getId(), eventStud.getEvent().getId());
                    CoreCollectionUtils.Pair<Double, Date> actualMark = markActualMap.get(key);

                    if (actualMark != null && actualMark.getX() != null)
                        workResult.setValue(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(actualMark.getX()));

                    result.getWorkResults().add(workResult);
                }
            }
            resultsList.add(result);
        }


        // Оценки в сессию

        List<Object[]> sessResults = new DQLSelectBuilder().fromEntity(SessionSlotLinkMark.class, "e")
                .joinPath(DQLJoinType.inner, SessionSlotLinkMark.target().slot().studentWpeCAction().fromAlias("e"), "wpeCa")
                .joinEntity("wpeCa", DQLJoinType.inner, EppFControlActionType.class, "ca", eq(property("wpeCa", EppStudentWpeCAction.type()), property("ca", EppFControlActionType.eppGroupType())))
                .column(property(EppStudentWpeCAction.studentWpe().id().fromAlias("wpeCa")))
                .column(property(EppFControlActionType.code().fromAlias("ca")))
                .column(property(SessionSlotLinkMark.target().cachedMarkValue().title().fromAlias("e")))
                .where(in(property(EppStudentWpeCAction.studentWpe().id().fromAlias("wpeCa")), workPlanSlotIdsSet))
                .createStatement(getSession()).list();


        Map<Long, String> examMarkValue = new HashMap<>();
        Map<Long, String> diffCreditMarkValue = new HashMap<>();
        Map<Long, String> creditMarkValue = new HashMap<>();

        for (Object[] item : sessResults)
        {
            Long slotId = (Long) item[0];
            String actionCode = (String) item[1];

            if (EppFControlActionTypeCodes.CONTROL_ACTION_EXAM.equals(actionCode))
                examMarkValue.put(slotId, (String) item[2]);
            else if (EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM.equals(actionCode))
                examMarkValue.put(slotId, (String) item[2]);
            else if (EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF.equals(actionCode))
                diffCreditMarkValue.put(slotId, (String) item[2]);
            else if (EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF.equals(actionCode))
                creditMarkValue.put(slotId, (String) item[2]);
        }


        List<EppFControlActionGroup> controlActionGroups = getList(EppFControlActionGroup.class);
        Map<Long, IEppRegElWrapper> regElMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(regElIdSet);

        for (Result result : resultsList)
        {
            IEppRegElWrapper regElWrapper = regElMap.get(Long.valueOf(result.getSubjectId()));
            if (null != regElWrapper && null != regElWrapper.getPartMap())
            {
                IEppRegElPartWrapper regElPartWrapper = regElWrapper.getPartMap().get(Integer.valueOf(result.getSemester()));

                if (null != regElPartWrapper)
                {
                    for (EppFControlActionGroup ctrlActionGrp : controlActionGroups)
                    {
                        if (regElPartWrapper.hasActions4Groups(ctrlActionGrp.getCode()))
                        {
                            if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(ctrlActionGrp.getCode()))
                                result.setExam(String.valueOf(Boolean.TRUE));
                            else if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_SETOFF.equals(ctrlActionGrp.getCode()))
                                result.setCredit(String.valueOf(Boolean.TRUE));
                        }
                    }
                }
            }

            Long slotId = resultIdToWorkplanSlotIdMap.get(result.getId());
            if (null != slotId)
            {
                result.setExam(examMarkValue.get(slotId));
                result.setDiffCredit(diffCreditMarkValue.get(slotId));
                result.setCredit(creditMarkValue.get(slotId));
            }
        }


        // Добавляем оценки, не вошедшие в результаты журналов (оценка в сессию есть, а журнала нет)
        List<Object[]> markResults = new DQLSelectBuilder().fromEntity(SessionSlotLinkMark.class, "e")
                .joinPath(DQLJoinType.inner, SessionSlotLinkMark.target().slot().studentWpeCAction().fromAlias("e"), "wpeCa")
                .joinEntity("wpeCa", DQLJoinType.inner, EppFControlActionType.class, "ca", eq(property("wpe", EppStudentWpeCAction.type()), property("ca", EppFControlActionType.eppGroupType())))
                .column(property(SessionSlotLinkMark.target().id().fromAlias("e")))
                .column(property(EppStudentWpeCAction.studentWpe().id().fromAlias("wpeCa")))
                .column(property(EppStudentWpeCAction.studentWpe().student().id().fromAlias("wpeCa")))
                .column(property(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().id().fromAlias("wpeCa")))
                .column(property(EppStudentWpeCAction.studentWpe().year().educationYear().id().fromAlias("wpeCa")))
                .column(property(EppStudentWpeCAction.studentWpe().term().intValue().fromAlias("wpeCa")))
                        //.column(property(EppStudentWpeCAction.studentWpe().registryElementPart().number().fromAlias("wpeCa"))) //TODO реальный семестр по РП заменили на номер дисциплино-части, чтобы связать с semester
                .column(property(EppFControlActionType.code().fromAlias("ca")))
                .column(property(SessionSlotLinkMark.target().cachedMarkValue().title().fromAlias("e")))
                .where(in(property(SessionSlotLinkMark.target().id().fromAlias("e")), resultIds))
                .createStatement(getSession()).list();

        for (Object[] item : markResults)
        {
            Long slotId = (Long) item[1];
            if (!resultIdToWorkplanSlotIdMap.values().contains(slotId))
            {
                Result result = new Result();
                result.setId(String.valueOf(item[0]));
                result.setStudentId(String.valueOf(item[2]));
                result.setSubjectId(String.valueOf(item[3]));
                result.setEduYearId(String.valueOf(item[4]));
                result.setSemester(String.valueOf(item[5]));

                String actionCode = (String) item[6];

                if (EppFControlActionTypeCodes.CONTROL_ACTION_EXAM.equals(actionCode))
                    result.setExam((String) item[7]);
                else if (EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM.equals(actionCode))
                    result.setExam((String) item[7]);
                else if (EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF.equals(actionCode))
                    result.setDiffCredit((String) item[7]);
                else if (EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF.equals(actionCode))
                    result.setCredit((String) item[7]);

                resultsList.add(result);
            }
        }

        return resultsList;
    }
}