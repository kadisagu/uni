/**
 *$Id$
 */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.entity.EntityCodeCatalog;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uniload_demo.entity.*;
import ru.tandemservice.uniload_demo.entity.catalog.LoadTimeRule;

/**
 * @author Alexander Zhebko
 * @since 23.12.2013
 */
public class MS_unifefu_2x4x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1")
		};
    }

    @Override
    public ScriptDependency[] getAfterDependencies()
    {
        return new ScriptDependency[]{new ScriptDependency("uniload_demo", "2.4.1", 1)};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        EntityCodeCatalog entityCodeCatalog = tool.entityCodes();
        if (tool.tableExists("fefuedugroupppsdistr_t") && !tool.tableExists("loadedugroupppsdistr_t"))
        {
            entityCodeCatalog.rename("FefuEduGroupPpsDistr", LoadEduGroupPpsDistr.ENTITY_NAME);
            tool.renameTable("fefuedugroupppsdistr_t", "loadedugroupppsdistr_t");
        }

        if (tool.tableExists("fefufakeppsentry_t") && !tool.tableExists("loadfakeppsentry_t"))
        {
            entityCodeCatalog.rename("FefuFakePpsEntry", LoadFakePpsEntry.ENTITY_NAME);
            tool.renameTable("fefufakeppsentry_t", "loadfakeppsentry_t");
        }

        if (tool.tableExists("fefufirstcourseimedugroup_t") && !tool.tableExists("loadfirstcourseimedugroup_t"))
        {
            entityCodeCatalog.rename("FefuFirstCourseImEduGroup", LoadFirstCourseImEduGroup.ENTITY_NAME);
            tool.renameTable("fefufirstcourseimedugroup_t", "loadfirstcourseimedugroup_t");
        }

        if (tool.tableExists("fefuimedugrouptimerulerel_t") && !tool.tableExists("loadimedugrouptimerulerel_t"))
        {
            entityCodeCatalog.rename("FefuImEduGroupTimeRuleRel", LoadImEduGroupTimeRuleRel.ENTITY_NAME);
            tool.renameTable("fefuimedugrouptimerulerel_t", "loadimedugrouptimerulerel_t");
        }

        if (tool.tableExists("fefuloadmaxsize_t") && !tool.tableExists("loadmaxsize_t"))
        {
            entityCodeCatalog.rename("FefuLoadMaxSize", "loadMaxSize");
            tool.renameTable("fefuloadmaxsize_t", "loadmaxsize_t");
        }

        if (tool.tableExists("fefuloadrecordowner_t") && !tool.tableExists("loadrecordowner_t"))
        {
            entityCodeCatalog.rename("FefuLoadRecordOwner", LoadRecordOwner.ENTITY_NAME);
            tool.renameTable("fefuloadrecordowner_t", "loadrecordowner_t");
        }

        if (tool.tableExists("fefuloadtimerule_t") && !tool.tableExists("loadtimerule_t"))
        {
            entityCodeCatalog.rename("FefuLoadTimeRule", LoadTimeRule.ENTITY_NAME);
            tool.renameTable("fefuloadtimerule_t", "loadtimerule_t");
        }

        if (tool.tableExists("fefunoneduload_t") && !tool.tableExists("noneduload_t"))
        {
            entityCodeCatalog.rename("FefuNonEduLoad", "nonEduLoad");
            tool.renameTable("fefunoneduload_t", "noneduload_t");
        }

        if (tool.tableExists("fefuorgunitloadstate_t") && !tool.tableExists("orgunitloadstate_t"))
        {
            entityCodeCatalog.rename("FefuOrgUnitLoadState", OrgUnitLoadState.ENTITY_NAME);
            tool.renameTable("fefuorgunitloadstate_t", "orgunitloadstate_t");
        }

        if (tool.viewExists("ifefuimedugroup_v"))
        {
            tool.dropView("ifefuimedugroup_v");
        }
    }
}