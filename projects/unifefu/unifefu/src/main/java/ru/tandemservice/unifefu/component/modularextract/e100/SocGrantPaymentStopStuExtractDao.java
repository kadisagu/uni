/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.modularextract.e100;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.movestudent.entity.SocGrantPaymentStopStuExtract;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;


import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 21.02.2013
 */
public class SocGrantPaymentStopStuExtractDao extends ru.tandemservice.movestudent.component.modularextract.e100.SocGrantPaymentStopStuExtractDao
{
    public void doCommit(SocGrantPaymentStopStuExtract extract, Map parameters)
    {
        super.doCommit(extract, parameters);

        FefuStudentPayment payment = new FefuStudentPayment();
        payment.setExtract(extract);
        payment.setStopOrPauseOrder(true);
        payment.setStartDate(extract.getSocGrantPaymentStopDate());
        payment.setInfluencedExtract(extract.getSocGrantExtract());
        payment.setInfluencedOrderNum(extract.getSocGrantOrderNumber());
        payment.setInfluencedOrderDate(extract.getSocGrantOrderDate());
        if(!StringUtils.isEmpty(extract.getSocGrantOrderNumber()) && null != extract.getSocGrantOrderDate())
            payment.setComment("Прекращение выплаты социальной стипендии по приказу №" + extract.getSocGrantOrderNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getSocGrantOrderDate()));
        if(null != extract.getReason())
            payment.setReason(extract.getReason().getTitle());
        save(payment);
    }

    public void doRollback(SocGrantPaymentStopStuExtract extract, Map parameters)
    {
        super.doRollback(extract, parameters);

        // Удаляем выплату
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuStudentPayment.class);
        deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(FefuStudentPayment.extract()), DQLExpressions.value(extract)));
        deleteBuilder.createStatement(getSession()).execute();

    }
}
