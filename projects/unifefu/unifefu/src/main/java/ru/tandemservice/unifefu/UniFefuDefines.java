/* $Id$ */
package ru.tandemservice.unifefu;

import org.tandemframework.shared.commonbase.base.util.Zlo;

/**
 * @author nvankov
 * @since 3/27/13
 */
public interface UniFefuDefines
{
    // Коды дополнительных статусов студентов
    String ADMITTED_TO_STATE_EXAMES = "admittedToStateExams"; //Допущен к сдаче ГЭ
    String ADMITTED_TO_DIPLOMA = "admittedToDiploma"; //Допущен к защите ВКР
    String VACATION = "vacation"; //Каникулы
    String DIPLOMA_SUCCESS = "diplomaSuccess"; //Диплом с отличием
    String TARGET_RECEPTION = "targetReception"; //Целевой прием

    String ENTRANT_AUTH_CARD = "entrantAuthCard";
    String SCRIPT_ENROLLMENT_SIT_LIST = "enrollmentSitList";
    String SCRIPT_ENROLLMENT_SIT_EXAM_CARD_LIST = "enrollmentSitExamCardList";
    String SCRIPT_ENROLLMENT_CIPHER_LIST = "enrollmentCipherList";

    String TEMPLATE_STATISTIC_ENTRANT_REQUEST = "statisticEntrantRequest";
    String TEMPLATE_ENTRANT_SUBMITTED_DOCUMENTS = "fefuEntrantSubmittedDocuments";
    String TEMPLATE_ENTRANT_SUBMITTED_DOCUMENTS_EXT = "fefuEntrantSubmittedDocumentsExt";
    String TEMPLATE_ENTRANT_INCOMING_LIST = "fefuEntrantIncomingList";
    String TEMPLATE_ENTRANT_INCOMING_LIST_VAR2 = "fefuEntrantIncomingListVar2";
    String TEMPLATE_ENTRANT_RESIDENCE_DISTRIB = "fefuEntrantResidenceDistrib";
    String TEMPLATE_ENROLLED_LIST = "fefuEnrolledList";
    String TEMPLATE_STATISTIC_ENROLLED = "fefuStatisticEnrolled";

    String TEMPLATE_FEFU_ECG_DISTRIBUTION_PER_DIRECTION = "fefuEcgDistributionPerDirection";
    String TEMPLATE_FEFU_ECG_DISTRIBUTION_PER_COMPETITION_GROUP = "fefuEcgDistributionPerCompetitionGroup";

    // Коды приказов о зачислении
    String ORDER_TYPE_STUDENT_SPO_BUDGET = "fefu1";
    String ORDER_TYPE_STUDENT_SPO_CONTRACT = "fefu2";
    String ORDER_TYPE_STUDENT_TARGET_SPO_BUDGET = "fefu3";
    String ORDER_TYPE_STUDENT_OUT_OF_COMPETITION_BUDGET = "fefu4";
    String ORDER_TYPE_STUDENT_WITHOUT_EXAMS_BUDGET = "fefu5";
    String ORDER_TYPE_STUDENT_SPO_BUDGET_BY_CREATIVE = "fefu6";
    String ORDER_TYPE_STUDENT_SPO_BUDGET_BY_CERTIFICATE = "fefu7";
    String ORDER_TYPE_STUDENT_TARGET_SPO_BUDGET_BY_CERTIFICATE = "fefu8";
    String ORDER_TYPE_STUDENT_SPO_CONTRACT_BY_CERTIFICATE = "fefu9";
    String ORDER_TYPE_MASTER_TARGET_BUDGET = "fefu10";
    String ORDER_TYPE_FOREIGNERS_QUOTA_BUDGET = "fefu11";
    String ORDER_TYPE_MASTER_FOREIGNERS_QUOTA_BUDGET = "fefu12";

    // Этапы зачисления
    String ENROLLMENT_STEP_TARGET_ADMISSION = "1"; // целевики
    String ENROLLMENT_STEP_OUT_OF_COMPETITION = "2"; // внеконкурсники
    String ENROLLMENT_STEP_CONTRACT = "3"; // договорники

    /* Типы учебных недель ИМЦА */
    char IMTSA_EXAMINATION_SESSION = 'Э';
    char IMTSA_TEACH_PRACTICE = 'У';
    char IMTSA_RESEARCH_WORK = 'Н';
    char IMTSA_WORK_PRACTICE = 'П';
    char IMTSA_QUALIFICATION_WORK = 'Д';
    char IMTSA_STATE_EXAMINATION = 'Г';
    char IMTSA_HOLIDAYS = 'К';
    char IMTSA_UNUSED = '=';
    char IMTSA_THEORY = 'Т';
    char IMTSA_ATTESTATION = 'А';
    char IMTSA_IGNORE = '-';

    /* Формы текущего контроля */
    public static final String CONTROL_ACTION_GRAPH_WORK = "22";


    @Zlo("Завязка на код элемента пользовательского справочника")
    public static final String SCHOOL_ORGUNIT_TYPE_CODE = "43";
}
