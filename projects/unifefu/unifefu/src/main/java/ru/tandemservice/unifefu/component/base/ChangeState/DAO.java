/*$Id$*/
package ru.tandemservice.unifefu.component.base.ChangeState;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniepp.component.base.ChangeState.Model;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.FefuRegistryManager;
import ru.tandemservice.unifefu.entity.FefuEduProgramKind2MinLaborRel;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkPlanAgreementCheckingData;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 13.10.2014
 */
public class DAO extends ru.tandemservice.uniepp.component.base.ChangeState.DAO implements IDAO
{
	@Override
	public void changeState(Model model, String stateCode)
	{
		IEppStateObject stateObject = this.getNotNull(model.getId());
		if (stateObject instanceof EppWorkPlan && stateObject.getState().isFormative() && stateCode.equals(EppState.STATE_ACCEPTABLE))
		{
			checkWorkPlan((EppWorkPlan) stateObject);
		}
        else if (stateObject instanceof EppRegistryElement && stateCode.equals(EppState.STATE_ACCEPTABLE))
        {
            EppRegistryElement element = (EppRegistryElement) stateObject;
            EduProgramKind eduProgramKind = FefuRegistryManager.instance().dao().getEduProgramKind(element);
            Double laborAsDouble = element.getLaborAsDouble();

            if (element.getLabor() <= 0 || null == laborAsDouble || laborAsDouble % 1.0 != 0)
            {
                throw new ApplicationException("Трудоемкость дисциплины не является натуральным числом.");
            }
            else if (null != eduProgramKind)
            {
                FefuEduProgramKind2MinLaborRel rel = DataAccessServices.dao().get(FefuEduProgramKind2MinLaborRel.class, FefuEduProgramKind2MinLaborRel.eduProgramKind(), eduProgramKind);
                if (null != rel && laborAsDouble < rel.getLaborAsDouble())
                {
                    throw new ApplicationException("Общая трудоемкость дисциплины не соответствует установленным минимальным критериям (" + rel.getLaborAsDouble() +").");
                }
            }
            else
            {
                Long id = element.getId();
                IEppRegElWrapper wrapper = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(id)).get(id);
                Map<Integer, IEppRegElPartWrapper> partMap = wrapper.getPartMap();

                for (IEppRegElPartWrapper partWrapper : partMap.values())
                {
                    EppRegistryElementPart part = partWrapper.getItem();
                    if (part.getLabor() == 0 || part.getSize() == 0)
                    {
                        throw new ApplicationException("В дисциплине есть части с нулевой нагрузкой.");
                    }
                }
            }
        }
		super.changeState(model, stateCode);
	}

	private void checkWorkPlan(EppWorkPlan workPlan)
	{
		FefuWorkPlanAgreementCheckingData data = get(FefuWorkPlanAgreementCheckingData.class, FefuWorkPlanAgreementCheckingData.L_EPP_WORK_PLAN, workPlan);
		if (data == null)
		{
			data = new FefuWorkPlanAgreementCheckingData();
			data.setEppWorkPlan(workPlan);
		}
		data.setNoUnallocatedDiscipline(checkUnallocatedDiscipline(workPlan.getId(), workPlan.getParent().getId()));
		data.setCheckingDate(new Date());
		saveOrUpdate(data);
	}

	private boolean checkUnallocatedDiscipline(Long workPlanId, Long versionBlockId)
	{
		String eer = "eer";
		DQLSelectBuilder epvRowPlanBuilder = new DQLSelectBuilder().fromEntity(EppEpvGroupImRow.class, eer)
				.column(property(eer, EppEpvGroupImRow.storedIndex()))
				.column(property(eer, EppEpvGroupImRow.size()))
				.where(eq(property(eer, EppEpvGroupImRow.L_OWNER), value(versionBlockId)))
				.order(property(eer, EppEpvGroupImRow.P_STORED_INDEX));
		List<Object[]> eppEpvList = epvRowPlanBuilder.createStatement(getSession()).list();
		Map<String, Integer> eppEpvMap = new HashMap<>(eppEpvList.size());
		for (Object[] item : eppEpvList)
		{
			String index = (String) item[0];
			Integer number = (Integer) item[1];
			eppEpvMap.put(index, number);
		}

		String wpr = "ewpr";
		DQLSelectBuilder workPlanRowBuilder = new DQLSelectBuilder().fromEntity(EppWorkPlanRow.class, wpr)
				.column(property(wpr, EppWorkPlanRow.P_NUMBER))
				.where(eq(property(wpr, EppWorkPlanRow.L_WORK_PLAN), value(workPlanId)))
				.order(property(wpr, EppWorkPlanRow.P_NUMBER));
		List<String> eppWorkPlanList = workPlanRowBuilder.createStatement(getSession()).list();//EppWorkPlanRow

		for (String key : eppEpvMap.keySet())
		{
			Integer count = eppEpvMap.get(key);
			for (String s : eppWorkPlanList)
			{
				if (s.startsWith(key)) count--;
				if (count < 0) return false;
			}
			if (count != 0) return false;
		}

		return true;
	}
}
