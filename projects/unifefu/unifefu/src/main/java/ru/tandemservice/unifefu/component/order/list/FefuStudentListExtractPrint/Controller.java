/* $Id$ */
package ru.tandemservice.unifefu.component.order.list.FefuStudentListExtractPrint;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.order.list.StudentListExtractPrint.Model;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.unifefu.base.ext.MoveStudentPrintModifier.logic.FefuMoveStudentInjectModifier;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author dseleznev
 * @since 27.01.2012
 */
public class Controller extends ru.tandemservice.movestudent.component.order.list.StudentListExtractPrint.Controller
{
    @Override
    @SuppressWarnings("unchecked")
    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
    {
        Model model = getModel(component);

        deactivate(component);

        RtfDocument document;
        if (UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(model.getExtract().getState().getCode()))
        {
            document = new RtfReader().read(model.getData());
        }
        else
        {
            String printName = EntityRuntime.getMeta(model.getExtractId()).getName() + "_extractPrint";
            IPrintFormCreator componentPrint = (IPrintFormCreator) ApplicationRuntime.getBean(printName);
            document = componentPrint.createPrintForm(model.getData(), model.getExtract());
        }

        RtfTableModifier tableModifier = new RtfTableModifier();
        FefuMoveStudentInjectModifier.commonExtractSignerTableModifier(tableModifier);
        tableModifier.modify(document);

        if (null != model.getPrintPdf() && model.getPrintPdf())
            return UniReportUtils.createConvertingToPdfRenderer("Extract.pdf", document);
        else
            return new CommonBaseRenderer().rtf().fileName("Extract.rtf").document(document);
    }
}