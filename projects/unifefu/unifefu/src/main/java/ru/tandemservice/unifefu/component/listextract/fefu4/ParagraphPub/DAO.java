/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class DAO extends AbstractListParagraphPubDAO<FefuAcadGrantAssignStuEnrolmentListExtract, Model> implements IDAO
{
}