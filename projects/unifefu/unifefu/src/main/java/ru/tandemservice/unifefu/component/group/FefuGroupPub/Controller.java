/* $Id: Controller.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.group.FefuGroupPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import ru.tandemservice.uni.component.group.GroupPub.Model;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.utils.NsiIdWrapper;

import java.util.List;

/**
 * @author Viktor Nekrasov
 * @since 27.11.2013
 */
public class Controller extends ru.tandemservice.uni.component.group.GroupPub.Controller
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        super.onRefreshComponent(context);
        ((ru.tandemservice.unifefu.component.group.FefuGroupPub.Model) getModel(context)).setNsiIdWrapper(new NsiIdWrapper(getModel(context).getGroupId()));
    }

    @Override
    protected List<AbstractColumn> prepareColumnList(Model model)
    {
        List<AbstractColumn> columnList = super.prepareColumnList(model);
        int index = 0;
        for (AbstractColumn item : columnList)
        {
            index++;
            if (item.getKey().equals(Student.FIO_KEY)) break;
        }
        columnList.add(index, new SimpleColumn("Примечание", DAO.ORDER_NON_STATE_FINISHED).setClickable(false).setOrderable(false));
        return columnList;
    }
}