/**
 * SubmitStatusSOAP11SyncServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportal;

import org.tandemframework.core.runtime.ApplicationRuntime;

public class SubmitStatusSOAP11SyncServiceLocator extends org.apache.axis.client.Service implements ru.tandemservice.unifefu.ws.rateportal.SubmitStatusSOAP11SyncService {

    public static final String PORTAL_SUBMIT_STATUS_SERVICE_URL = "fefu.portal.service.status.url";

    public SubmitStatusSOAP11SyncServiceLocator() {
    }


    public SubmitStatusSOAP11SyncServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SubmitStatusSOAP11SyncServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SubmitStatusSOAP11SyncPort
    private java.lang.String SubmitStatusSOAP11SyncPort_address = ApplicationRuntime.getProperty(PORTAL_SUBMIT_STATUS_SERVICE_URL);
    //"https://srv-portal-04.dvfu.ru:9443/ip-adapter-mediationWeb/sca/SubmitStatus";
    //"http://77.239.225.71:19080/ip-adapter-mediationWeb/sca/SubmitStatus";

    public java.lang.String getSubmitStatusSOAP11SyncPortAddress() {
        return SubmitStatusSOAP11SyncPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SubmitStatusSOAP11SyncPortWSDDServiceName = "SubmitStatusSOAP11SyncPort";

    public java.lang.String getSubmitStatusSOAP11SyncPortWSDDServiceName() {
        return SubmitStatusSOAP11SyncPortWSDDServiceName;
    }

    public void setSubmitStatusSOAP11SyncPortWSDDServiceName(java.lang.String name) {
        SubmitStatusSOAP11SyncPortWSDDServiceName = name;
    }

    public ru.tandemservice.unifefu.ws.rateportal.SubmitStatus getSubmitStatusSOAP11SyncPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SubmitStatusSOAP11SyncPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSubmitStatusSOAP11SyncPort(endpoint);
    }

    public ru.tandemservice.unifefu.ws.rateportal.SubmitStatus getSubmitStatusSOAP11SyncPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ru.tandemservice.unifefu.ws.rateportal.SubmitStatusSOAP11SyncBindingStub _stub = new ru.tandemservice.unifefu.ws.rateportal.SubmitStatusSOAP11SyncBindingStub(portAddress, this);
            _stub.setPortName(getSubmitStatusSOAP11SyncPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSubmitStatusSOAP11SyncPortEndpointAddress(java.lang.String address) {
        SubmitStatusSOAP11SyncPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ru.tandemservice.unifefu.ws.rateportal.SubmitStatus.class.isAssignableFrom(serviceEndpointInterface)) {
                ru.tandemservice.unifefu.ws.rateportal.SubmitStatusSOAP11SyncBindingStub _stub = new ru.tandemservice.unifefu.ws.rateportal.SubmitStatusSOAP11SyncBindingStub(new java.net.URL(SubmitStatusSOAP11SyncPort_address), this);
                _stub.setPortName(getSubmitStatusSOAP11SyncPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SubmitStatusSOAP11SyncPort".equals(inputPortName)) {
            return getSubmitStatusSOAP11SyncPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/submit-status/service", "SubmitStatusSOAP11SyncService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/submit-status/service", "SubmitStatusSOAP11SyncPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SubmitStatusSOAP11SyncPort".equals(portName)) {
            setSubmitStatusSOAP11SyncPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
