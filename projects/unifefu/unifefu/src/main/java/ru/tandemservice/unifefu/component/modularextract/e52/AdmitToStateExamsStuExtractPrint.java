/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.e52;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AdmitToStateExamsStuExtract;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.Map;

/**
 * @author Igor Belanov
 * @since 14.06.2016
 */
public class AdmitToStateExamsStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e52.AdmitToStateExamsStuExtractPrint {
    @Override
    public void additionalModify(RtfInjectModifier modifier, AdmitToStateExamsStuExtract extract) {
        // конкретика для fefu
        // (метки fefuAdmited_to_GIA_A и fefuExecuted_content_of_academic_plan_A)
        boolean isMale = extract.getEntity().getPerson().getIdentityCard().getSex().isMale();
        Map<String,String[]> differentWordMap = isMale ? CommonExtractPrint.DIFFERENT_WORD_MALE_SEX_CASES_ARRAY : CommonExtractPrint.DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY;
        // винительный падеж в соответствии с полом
        String executed = differentWordMap.get("executed")[UniRtfUtil.CASE_POSTFIX.indexOf("_A")];
        String admited = differentWordMap.get("admited_past")[UniRtfUtil.CASE_POSTFIX.indexOf("_A")];
        if (extract.isNotNeedAdmissionToGIA()) {
            modifier.put("fefuAdmitedToGia_A", "");
            modifier.put("fefuEduPlanCompleted_A", ", " + executed + " содержание учебного плана");
        } else {
            modifier.put("fefuAdmitedToGia_A",  ", " + admited + " к государственной итоговой аттестации");
            modifier.put("fefuEduPlanCompleted_A", "");
        }
    }
}
