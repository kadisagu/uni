package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об условном переводе с курса на следующий курс (списочный)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuConditionalCourseTransferListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract";
    public static final String ENTITY_NAME = "fefuConditionalCourseTransferListExtract";
    public static final int VERSION_HASH = 1709217662;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_COURSE_NEW = "courseNew";
    public static final String L_GROUP_NEW = "groupNew";
    public static final String P_ELIMINATE_DATE = "eliminateDate";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Course _courseNew;     // Перевести на курс
    private Group _groupNew;     // Новая группа
    private Date _eliminateDate;     // Срок ликвидации академической задолженности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Перевести на курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourseNew()
    {
        return _courseNew;
    }

    /**
     * @param courseNew Перевести на курс. Свойство не может быть null.
     */
    public void setCourseNew(Course courseNew)
    {
        dirty(_courseNew, courseNew);
        _courseNew = courseNew;
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupNew()
    {
        return _groupNew;
    }

    /**
     * @param groupNew Новая группа. Свойство не может быть null.
     */
    public void setGroupNew(Group groupNew)
    {
        dirty(_groupNew, groupNew);
        _groupNew = groupNew;
    }

    /**
     * @return Срок ликвидации академической задолженности.
     */
    public Date getEliminateDate()
    {
        return _eliminateDate;
    }

    /**
     * @param eliminateDate Срок ликвидации академической задолженности.
     */
    public void setEliminateDate(Date eliminateDate)
    {
        dirty(_eliminateDate, eliminateDate);
        _eliminateDate = eliminateDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuConditionalCourseTransferListExtractGen)
        {
            setCourse(((FefuConditionalCourseTransferListExtract)another).getCourse());
            setGroup(((FefuConditionalCourseTransferListExtract)another).getGroup());
            setCompensationType(((FefuConditionalCourseTransferListExtract)another).getCompensationType());
            setCourseNew(((FefuConditionalCourseTransferListExtract)another).getCourseNew());
            setGroupNew(((FefuConditionalCourseTransferListExtract)another).getGroupNew());
            setEliminateDate(((FefuConditionalCourseTransferListExtract)another).getEliminateDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuConditionalCourseTransferListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuConditionalCourseTransferListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuConditionalCourseTransferListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "courseNew":
                    return obj.getCourseNew();
                case "groupNew":
                    return obj.getGroupNew();
                case "eliminateDate":
                    return obj.getEliminateDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "courseNew":
                    obj.setCourseNew((Course) value);
                    return;
                case "groupNew":
                    obj.setGroupNew((Group) value);
                    return;
                case "eliminateDate":
                    obj.setEliminateDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "courseNew":
                        return true;
                case "groupNew":
                        return true;
                case "eliminateDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "courseNew":
                    return true;
                case "groupNew":
                    return true;
                case "eliminateDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "courseNew":
                    return Course.class;
                case "groupNew":
                    return Group.class;
                case "eliminateDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuConditionalCourseTransferListExtract> _dslPath = new Path<FefuConditionalCourseTransferListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuConditionalCourseTransferListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Перевести на курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getCourseNew()
     */
    public static Course.Path<Course> courseNew()
    {
        return _dslPath.courseNew();
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getGroupNew()
     */
    public static Group.Path<Group> groupNew()
    {
        return _dslPath.groupNew();
    }

    /**
     * @return Срок ликвидации академической задолженности.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getEliminateDate()
     */
    public static PropertyPath<Date> eliminateDate()
    {
        return _dslPath.eliminateDate();
    }

    public static class Path<E extends FefuConditionalCourseTransferListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private Course.Path<Course> _courseNew;
        private Group.Path<Group> _groupNew;
        private PropertyPath<Date> _eliminateDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Перевести на курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getCourseNew()
     */
        public Course.Path<Course> courseNew()
        {
            if(_courseNew == null )
                _courseNew = new Course.Path<Course>(L_COURSE_NEW, this);
            return _courseNew;
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getGroupNew()
     */
        public Group.Path<Group> groupNew()
        {
            if(_groupNew == null )
                _groupNew = new Group.Path<Group>(L_GROUP_NEW, this);
            return _groupNew;
        }

    /**
     * @return Срок ликвидации академической задолженности.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract#getEliminateDate()
     */
        public PropertyPath<Date> eliminateDate()
        {
            if(_eliminateDate == null )
                _eliminateDate = new PropertyPath<Date>(FefuConditionalCourseTransferListExtractGen.P_ELIMINATE_DATE, this);
            return _eliminateDate;
        }

        public Class getEntityClass()
        {
            return FefuConditionalCourseTransferListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuConditionalCourseTransferListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
