/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsReport.logic;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;

/**
 * @author nvankov
 * @since 1/20/14
 */
public interface IFefuBrsReportDAO
{
    EppYearPart lastYearPart();

    /**
     * Получение родительского формирующего подразделения.
     * Если передано формирующее подразделение, то оно будет возвращено. Иначе будет поиск ближайшего родительского формирующего подразделения.
     *
     * @param orgUnit текущее подразделение (читающее)
     * @return родительское формирующее подразделение
     */
    OrgUnit getParentFormativeOrgUnit(OrgUnit orgUnit);

    Long getEppStateAcceptedId();
}
