/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu22.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Belanov
 * @since 05.07.2016
 * (по сути это обёртка для подпараграфа, в прочих приказах она называется ParagraphPartWrapper
 * а не SubParagraphWrapper (чтобы всех запутать, очевидно же))
 */
public class FefuPerformConditionCourseTransferListSubParagraphWrapper implements Comparable<FefuPerformConditionCourseTransferListSubParagraphWrapper>
{
    private final EducationLevelsHighSchool _educationLevelsHighSchool;
    private final ListStudentExtract _firstExtract;
    private final List<Student> _studentList = new ArrayList<>();

    public FefuPerformConditionCourseTransferListSubParagraphWrapper(EducationLevelsHighSchool educationLevelsHighSchool, ListStudentExtract firstExtract)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
        _firstExtract = firstExtract;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public List<Student> getStudentList()
    {
        return _studentList;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuPerformConditionCourseTransferListSubParagraphWrapper))
            return false;
        FefuPerformConditionCourseTransferListSubParagraphWrapper anotherSubParagraphWrapper = (FefuPerformConditionCourseTransferListSubParagraphWrapper) o;

        return _educationLevelsHighSchool.equals(anotherSubParagraphWrapper.getEducationLevelsHighSchool());
    }

    @Override
    public int hashCode()
    {
        return _educationLevelsHighSchool.hashCode();
    }

    @Override
    public int compareTo(FefuPerformConditionCourseTransferListSubParagraphWrapper anotherSubParagraphWrapper)
    {
        StructureEducationLevels levelType1 = _educationLevelsHighSchool.getEducationLevel().getLevelType();
        StructureEducationLevels levelType2 = anotherSubParagraphWrapper.getEducationLevelsHighSchool().getEducationLevel().getLevelType();

        // сначала идут подпараграфы без специализации и без профиля
        int compareResult = Boolean.compare(levelType1.isProfile() || levelType1.isSpecialization(), levelType2.isProfile() || levelType2.isSpecialization());
        // затем идут специализации и затем все остальные
        if (compareResult == 0)
            compareResult = Boolean.compare(!levelType1.isSpecialization(), !levelType2.isSpecialization());
        // внутри одинаковых сортируем по названию НПв
        if (compareResult == 0 && !_educationLevelsHighSchool.equals(anotherSubParagraphWrapper.getEducationLevelsHighSchool()))
            compareResult = _educationLevelsHighSchool.getTitle().compareTo(getEducationLevelsHighSchool().getTitle());

        return compareResult;
    }
}
