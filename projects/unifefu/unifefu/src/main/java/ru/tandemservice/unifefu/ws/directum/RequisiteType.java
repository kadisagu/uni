/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 16.07.2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Requisite", propOrder = {})
public class RequisiteType
{
    public static final String STRING_TYPE = "String";
    public static final String PICK_TYPE = "Pick";
    public static final String TOPIC_NAME = "Дополнение3";
    public static final String FIO_NAME = "String";
    public static final String EDU_FORM_NAME = "edu_form";
    public static final String NOTICE_NAME = "ISBEDocNote";

    @XmlValue
    protected String value;
    @XmlAttribute(name = "Name")
    protected String name;
    @XmlAttribute(name = "Type")
    protected String type;

    public static final List<RequisiteType> createSingleDocRequisitesList(String topic, String fio, String eduForm, String notice)
    {
        List<RequisiteType> result = new ArrayList<>();
        if (null != topic) result.add(createTopicRequisite(topic));
        if (null != fio) result.add(createFioRequisite(fio));
        if (null != eduForm) result.add(createEduFormRequisite(eduForm));
        if (null != notice) result.add(createNoticeRequisite(notice));
        return result;
    }

    public static final RequisiteType createTopicRequisite(String topic)
    {
        RequisiteType requisite = new RequisiteType();
        requisite.setType(STRING_TYPE);
        requisite.setName(TOPIC_NAME);
        requisite.setValue(topic);
        return requisite;
    }

    public static final RequisiteType createFioRequisite(String fio)
    {
        RequisiteType requisite = new RequisiteType();
        requisite.setType(STRING_TYPE);
        requisite.setName(FIO_NAME);
        requisite.setValue(fio);
        return requisite;
    }

    public static final RequisiteType createEduFormRequisite(String eduForm)
    {
        RequisiteType requisite = new RequisiteType();
        requisite.setType(PICK_TYPE);
        requisite.setName(EDU_FORM_NAME);
        requisite.setValue(eduForm);
        return requisite;
    }

    public static final RequisiteType createNoticeRequisite(String notice)
    {
        RequisiteType requisite = new RequisiteType();
        requisite.setType(STRING_TYPE);
        requisite.setName(NOTICE_NAME);
        requisite.setValue(notice);
        return requisite;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
}