/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSchedule.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.vo.FefuSchedulePrintDataVO;
import ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;
import ru.tandemservice.unispp.base.entity.SppSchedulePrintForm;
import ru.tandemservice.unispp.base.vo.SppScheduleGroupPrintVO;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author nvankov
 * @since 9/2/13
 */
@Transactional
public class FefuScheduleDAO extends BaseModifyAggregateDAO implements IFefuScheduleDAO
{
    @Override
    public SchedulePrintFormFefuExt savePrintForm(FefuSchedulePrintDataVO scheduleData)
    {
        SppSchedulePrintForm printForm = new SppSchedulePrintForm();
        SchedulePrintFormFefuExt fefuPrintForm = new SchedulePrintFormFefuExt();
        printForm.setCreateDate(new Date());

        printForm.setCourse(scheduleData.getCourse());
        printForm.setSeason(scheduleData.getSeason());
        printForm.setBells(scheduleData.getBells());

        printForm.setChief(scheduleData.getChief());

        printForm.setCreateOU(scheduleData.getFormativeOrgUnit());
        printForm.setFormativeOrgUnit(scheduleData.getFormativeOrgUnit());
        printForm.setGroups(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(scheduleData.getSchedules(), SppSchedule.group().title()), ", "));
        printForm.setEduYear(scheduleData.getEducationYear().getTitle());

        printForm.setCreateOU(scheduleData.getCurrentOrgUnit());

        List<SppScheduleGroupPrintVO> schedules = Lists.newArrayList();
        schedules.addAll(scheduleData.getSchedules().stream()
                .map(this::prepareScheduleGroupPrintVO).collect(Collectors.toList()));

        DatabaseFile content = new DatabaseFile();
        try
        {
            content.setContent(FefuSchedulePrintFormExcelContentBuilder.buildExcelContent(schedules, scheduleData));
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        baseCreate(content);
        printForm.setContent(content);
        baseCreate(printForm);
        fefuPrintForm.setSppSchedulePrintForm(printForm);
        fefuPrintForm.setTerritorialOrgUnit(scheduleData.getTerritorialOrgUnit());
        fefuPrintForm.setTerm(fefuPrintForm.getTerm());
        fefuPrintForm.setEduStartDate(scheduleData.getEduStartDate());
        fefuPrintForm.setChiefUMU(scheduleData.getChiefUMU());
        fefuPrintForm.setAdmin(scheduleData.getAdmin());
        fefuPrintForm.setAdminPhoneNum(scheduleData.getAdminPhoneNum());
        baseCreate(fefuPrintForm);
        return fefuPrintForm;
    }

    @Override
    public SppScheduleGroupPrintVO prepareScheduleGroupPrintVO(SppSchedule schedule)
    {
        SppScheduleGroupPrintVO groupPrintVO = new SppScheduleGroupPrintVO(schedule);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "p");
        builder.where(eq(property("p", SppSchedulePeriod.weekRow().schedule().id()), value(schedule.getId())));
        List<SppSchedulePeriod> periods = createStatement(builder).list();
        for (SppSchedulePeriod period : periods)
        {
            if (!period.isEmpty())
            {
                ScheduleBellEntry bell = period.getWeekRow().getBell();
                Boolean isEven = period.getWeekRow().isEven();
                if (SppScheduleGroupPrintVO.MONDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getMonday().containsKey(bell))
                        groupPrintVO.getMonday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getMonday().get(bell).containsKey(isEven))
                        groupPrintVO.getMonday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getMonday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getMonday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.TUESDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getTuesday().containsKey(bell))
                        groupPrintVO.getTuesday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getTuesday().get(bell).containsKey(isEven))
                        groupPrintVO.getTuesday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getTuesday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getTuesday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.WEDNESDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getWednesday().containsKey(bell))
                        groupPrintVO.getWednesday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getWednesday().get(bell).containsKey(isEven))
                        groupPrintVO.getWednesday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getWednesday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getWednesday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.THURSDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getThursday().containsKey(bell))
                        groupPrintVO.getThursday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getThursday().get(bell).containsKey(isEven))
                        groupPrintVO.getThursday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getThursday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getThursday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.FRIDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getFriday().containsKey(bell))
                        groupPrintVO.getFriday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getFriday().get(bell).containsKey(isEven))
                        groupPrintVO.getFriday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getFriday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getFriday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.SATURDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getSaturday().containsKey(bell))
                        groupPrintVO.getSaturday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getSaturday().get(bell).containsKey(isEven))
                        groupPrintVO.getSaturday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getSaturday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getSaturday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.SUNDAY == period.getDayOfWeek())
                {
                    groupPrintVO.setHasSunday(true);
                    if (!groupPrintVO.getSunday().containsKey(bell))
                        groupPrintVO.getSunday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getSunday().get(bell).containsKey(isEven))
                        groupPrintVO.getSunday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getSunday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getSunday().get(bell).get(isEven).add(period);
                }
            }
        }
        int columns = getColumns(groupPrintVO.getMonday(), 0);
        columns = getColumns(groupPrintVO.getTuesday(), columns);
        columns = getColumns(groupPrintVO.getWednesday(), columns);
        columns = getColumns(groupPrintVO.getThursday(), columns);
        columns = getColumns(groupPrintVO.getFriday(), columns);
        columns = getColumns(groupPrintVO.getSaturday(), columns);
        columns = getColumns(groupPrintVO.getSunday(), columns);
        groupPrintVO.setColumnNum(columns);

        DQLSelectBuilder studentCountBuilder = new DQLSelectBuilder().fromEntity(Student.class, "st");
        studentCountBuilder.column(count(property("st", Student.id())));
        studentCountBuilder.where(eq(property("st", Student.group().id()), value(schedule.getGroup().getId())));

        Number studentCount = createStatement(studentCountBuilder).uniqueResult();
        groupPrintVO.setStudents(studentCount.intValue());

        return groupPrintVO;
    }

    private int getColumns(Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> day, int i)
    {
        for (ScheduleBellEntry bell : day.keySet())
        {
            for (Boolean isEven : day.get(bell).keySet())
            {
                if (day.get(bell).get(isEven).size() > i) i = day.get(bell).get(isEven).size();
            }
        }
        return i;
    }
}
