/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

/**
 * @author Dmitry Seleznev
 * @since 18.04.2014
 */
public class FefuNsiIdWrapper
{
    private String _guid;
    private String _entityType;
    private boolean _addItemsToOBAllowed;
    private boolean _nsiIsMasterSystem;
    private Integer _obEntityHash;
    private Integer _nsiEntityHash;
    private String _nsiEntity;

    public FefuNsiIdWrapper(String guid, String entityType)
    {
        _guid = guid;
        _entityType = entityType;
    }

    public FefuNsiIdWrapper(String guid, String entityType, boolean addItemsToOBAllowed, boolean nsiIsMasterSystem)
    {
        _guid = guid;
        _entityType = entityType;
        _addItemsToOBAllowed = addItemsToOBAllowed;
        _nsiIsMasterSystem = nsiIsMasterSystem;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public String getEntityType()
    {
        return _entityType;
    }

    public void setEntityType(String entityType)
    {
        _entityType = entityType;
    }

    public boolean isAddItemsToOBAllowed()
    {
        return _addItemsToOBAllowed;
    }

    public void setAddItemsToOBAllowed(boolean addItemsToOBAllowed)
    {
        _addItemsToOBAllowed = addItemsToOBAllowed;
    }

    public boolean isNsiIsMasterSystem()
    {
        return _nsiIsMasterSystem;
    }

    public void setNsiIsMasterSystem(boolean nsiIsMasterSystem)
    {
        _nsiIsMasterSystem = nsiIsMasterSystem;
    }

    public Integer getNsiEntityHash()
    {
        return _nsiEntityHash;
    }

    public Integer getObEntityHash()
    {
        return _obEntityHash;
    }

    public void setObEntityHash(Integer obEntityHash)
    {
        _obEntityHash = obEntityHash;
    }

    public void setNsiEntityHash(Integer nsiEntityHash)
    {
        _nsiEntityHash = nsiEntityHash;
    }

    public String getNsiEntity()
    {
        return _nsiEntity;
    }

    public void setNsiEntity(String nsiEntity)
    {
        _nsiEntity = nsiEntity;
    }
}