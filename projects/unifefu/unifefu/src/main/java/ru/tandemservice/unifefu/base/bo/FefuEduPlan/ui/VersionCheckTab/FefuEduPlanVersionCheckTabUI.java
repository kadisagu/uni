/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.VersionCheckTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.VersionCheckAdd.FefuEduPlanVersionCheckAdd;
import ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 03.12.2014
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "version.id", required = true))
public class FefuEduPlanVersionCheckTabUI extends UIPresenter
{
    public static final String EDU_PLAN_VERSION_ID = "eduPlanVersionId";

    private EppEduPlanVersion _version = new EppEduPlanVersion();

    @Override
    public void onComponentRefresh()
    {
        _version = DataAccessServices.dao().getNotNull(_version.getId());
    }

    public boolean isEduStandardRelation()
    {
        return null != _version.getEduPlan().getParent();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuEduPlanVersionCheckTab.CHECK_RESULTS_DS.equals(dataSource.getName()))
        {
            dataSource.put(EDU_PLAN_VERSION_ID, _version.getId());
        }
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickCheckEpv()
    {
        EppStateEduStandard eduStandard = _version.getEduPlan().getParent();
        FefuEppStateEduStandardParameters parameters = DataAccessServices.dao().getByNaturalId(new FefuEppStateEduStandardParameters.NaturalId(eduStandard));
        List<FefuCompetence2EppStateEduStandardRel> competenceList = DataAccessServices.dao().getList(FefuCompetence2EppStateEduStandardRel.class, FefuCompetence2EppStateEduStandardRel.eduStandard(), eduStandard);
        List<FefuEppStateEduStandardLaborBlocks> laborBlockList = DataAccessServices.dao().getList(FefuEppStateEduStandardLaborBlocks.class, FefuEppStateEduStandardLaborBlocks.eduStandard(), eduStandard);

        if (null == parameters && competenceList.isEmpty() && laborBlockList.isEmpty())
        {
            InfoCollector info = ContextLocal.getInfoCollector();
            info.add("Для стандарта «" + eduStandard.getTitle() + "» не определены параметры проверки.");
        }
        else _uiActivation.asRegionDialog(FefuEduPlanVersionCheckAdd.class).parameter(UIPresenter.PUBLISHER_ID, _version.getId()).activate();
    }

    public EppEduPlanVersion getVersion()
    {
        return _version;
    }

    public void setVersion(EppEduPlanVersion version)
    {
        _version = version;
    }
}
