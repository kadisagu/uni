/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.server;

import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.datagram.*;
import ru.tandemservice.unifefu.ws.nsi.reactor.INsiEntityReactor;
import ru.tandemservice.unifefu.ws.nsi.reactor.NSIProcessingErrorException;
import ru.tandemservice.unifefu.ws.nsi.reactor.NSIProcessingWarnException;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;

import javax.xml.transform.TransformerException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 17.12.2013
 */
public class FefuNsiRequestsProcessor
{
    public static final String RESPONSE_CODE_TITLE_OK = "OK";
    public static final String RESPONSE_CODE_TITLE_ERR_PREFIX = "Could not process the incoming request: ";
    public static final String RESPONSE_CODE_TITLE_WARN_PREFIX = "Incoming request was processed with warnings: ";
    public static final BigInteger RESPONSE_CODE_NUMBER_INITIAL = BigInteger.valueOf(-1);
    public static final BigInteger RESPONSE_CODE_NUMBER_OK = BigInteger.valueOf(0);
    public static final BigInteger RESPONSE_CODE_NUMBER_WARN = BigInteger.valueOf(1);
    public static final BigInteger RESPONSE_CODE_NUMBER_ERROR = BigInteger.valueOf(2);
    public static final BigInteger RESPONSE_CODE_NUMBER_TRANSPORT_ERROR = BigInteger.valueOf(3);
    public static final BigInteger RESPONSE_CODE_NUMBER_OUTOFDATE_ERROR = BigInteger.valueOf(4);

    public static final String OPERATION_TYPE_RETRIEVE = "retrieve";
    public static final String OPERATION_TYPE_INSERT = "insert";
    public static final String OPERATION_TYPE_UPDATE = "update";
    public static final String OPERATION_TYPE_DELETE = "delete";

    private static Map<String, FefuNsiCatalogType> CATALOGS_MAP;
    private static List<FefuNsiCatalogType> CATALOGS_LIST;

    private static Set<Class> PROCESSABLE_NSI_ENTITTY_CLASSES_SET = new HashSet<>();
    private static Set<Class> MODIFIABLE_NSI_ENTITTY_CLASSES_SET = new HashSet<>();

    static
    {
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(AcademicRankType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(DegreeType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(CourseType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(StudentStatusType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(StudentCategoryType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(EducationFormType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(DevelopConditionType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(DevelopTechType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(DevelopPeriodType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(CompensationTypeType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(RelDegreeType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(LanguagesType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(LanguageLevelType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(EducationKindType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(OKSOType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(ContactKindType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(OksmType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(IdentityCardKindType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(OrganizationType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(DepartmentType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(EducationalProgramType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(AcademicGroupType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(HumanType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(IdentityCardType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(StudentType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(EmployeeType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(ContractorType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(AgreementType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(PostType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(ContactType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(HumanDegreeType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(HumanAcademicRankType.class);
        PROCESSABLE_NSI_ENTITTY_CLASSES_SET.add(HumanRoleType.class);

        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(AcademicRankType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(DegreeType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(CourseType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(StudentStatusType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(StudentCategoryType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(EducationFormType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(DevelopConditionType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(DevelopTechType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(DevelopPeriodType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(CompensationTypeType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(RelDegreeType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(LanguagesType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(LanguageLevelType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(EducationKindType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(OKSOType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(ContactKindType.class);
        /*MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(OksmType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(IdentityCardKindType.class);*/
        //MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(OrganizationType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(DepartmentType.class);
        /*MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(EducationalProgramType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(AcademicGroupType.class);*/
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(HumanType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(IdentityCardType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(StudentType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(EmployeeType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(ContractorType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(AgreementType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(PostType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(ContactType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(HumanDegreeType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(HumanAcademicRankType.class);
        MODIFIABLE_NSI_ENTITTY_CLASSES_SET.add(HumanRoleType.class);
    }

    private static List<FefuNsiCatalogType> getNsiCatalogList()
    {
        if (null == CATALOGS_MAP)
        {
            CATALOGS_MAP = new HashMap<>();
            CATALOGS_LIST = new ArrayList<>();
            for (FefuNsiCatalogType catalog : DataAccessServices.dao().getList(FefuNsiCatalogType.class, FefuNsiCatalogType.priority().s()))
            {
                CATALOGS_LIST.add(catalog);
                String[] nsiEntityNames = catalog.getNsiCode().split(",");
                for (String singleName : nsiEntityNames)
                    if (!CATALOGS_MAP.containsKey(singleName.trim()))
                        CATALOGS_MAP.put(singleName.trim(), catalog);
            }
        }
        return CATALOGS_LIST;
    }

    private static INsiEntityReactor getNsiEntityReactorByNsiEntityName(String nsiEntityName)
    {
        getNsiCatalogList();
        FefuNsiCatalogType catalog = CATALOGS_MAP.get(nsiEntityName);
        if (null != catalog)
        {
            INsiEntityReactor reactor = (INsiEntityReactor) ApplicationRuntime.getBean(catalog.getReactor());
            return reactor;
        }

        return null;
    }

    private static List<String> getEntityTypeSortedByPriority()
    {
        List<String> entityTypeSortedByPriority = new ArrayList<>();
        for (FefuNsiCatalogType catalog : getNsiCatalogList())
        {
            String[] nsiEntityNames = catalog.getNsiCode().split(",");
            for (String singleName : nsiEntityNames)
                if (!entityTypeSortedByPriority.contains(singleName))
                    entityTypeSortedByPriority.add(singleName);
        }
        return entityTypeSortedByPriority;
    }

    public static ServiceResponseType retrieve(ServiceRequestType nsiRequest)
    {
        ServiceResponseType response = new ServiceResponseType();
        RoutingHeaderType header = new RoutingHeaderType();
        response.setRoutingHeader(header);

        List<Object> datagramElements = null;
        if (null != nsiRequest.getDatagram()) datagramElements = getDatagramElements(nsiRequest);

        if (null == nsiRequest.getRoutingHeader().getOperationType())
            nsiRequest.getRoutingHeader().setOperationType(OPERATION_TYPE_RETRIEVE);

        Long rowId = IFefuNsiSyncDAO.instance.get().doLogNSIRequest(nsiRequest, datagramElements);

        validateIncomingResponseAndGetDatagramElements(nsiRequest, response, OPERATION_TYPE_RETRIEVE, datagramElements);
        if (null == datagramElements)
        {
            IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);
            return response;
        }

        header.setSourceId(nsiRequest.getRoutingHeader().getSourceId());
        //header.setCorrelationId(nsiRequest.getRoutingHeader().getMessageId());
        //header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        header.setMessageId(nsiRequest.getRoutingHeader().getMessageId());
        header.setOperationType(nsiRequest.getRoutingHeader().getOperationType());
        header.setDestinationId(nsiRequest.getRoutingHeader().getDestinationId());

        XDatagram xDatagram = NsiReactorUtils.NSI_OBJECT_FACTORY.createXDatagram();
        Map<String, List<Object>> nsiEntityTypeToEntityListMap = new HashMap<>();

        for (Object item : datagramElements)
        {
            if (!PROCESSABLE_NSI_ENTITTY_CLASSES_SET.contains(item.getClass()))
            {
                response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
                response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + "There are some unrecognized/unsupported elements in datagram.");
                IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);
                return response;
            }

            String nsiEntityType = item.getClass().getSimpleName();
            List<Object> entityList = nsiEntityTypeToEntityListMap.get(nsiEntityType);
            if (null == entityList) entityList = new ArrayList<>();
            entityList.add(item);
            nsiEntityTypeToEntityListMap.put(nsiEntityType, entityList);
        }

        for (Map.Entry<String, List<Object>> entry : nsiEntityTypeToEntityListMap.entrySet())
        {
            INsiEntityReactor reactor = getNsiEntityReactorByNsiEntityName(entry.getKey());
            if (null != reactor)
            {
                xDatagram.getEntityList().addAll(reactor.retrieve(entry.getValue()));
            }
        }

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram));
        MessageElement datagramOut = new SOAPBodyElement(inStream);

        List<Object> datagramContent = new ArrayList<>();
        datagramContent.add(datagramOut);
        response.setDatagram(new ServiceResponseType.Datagram(datagramContent));
        response.setCallCC(RESPONSE_CODE_NUMBER_OK);
        response.setCallRC(RESPONSE_CODE_TITLE_OK);

        IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);

        return response;
    }

    public static ServiceResponseType insert(ServiceRequestType nsiRequest)
    {
        ServiceResponseType response = new ServiceResponseType();
        RoutingHeaderType header = new RoutingHeaderType();
        response.setRoutingHeader(header);

        List<Object> datagramElements = null;
        if (null != nsiRequest.getDatagram()) datagramElements = getDatagramElements(nsiRequest);

        if (null == nsiRequest.getRoutingHeader().getOperationType())
            nsiRequest.getRoutingHeader().setOperationType(OPERATION_TYPE_INSERT);

        Long rowId = IFefuNsiSyncDAO.instance.get().doLogNSIRequest(nsiRequest, datagramElements);

        validateIncomingResponseAndGetDatagramElements(nsiRequest, response, OPERATION_TYPE_INSERT, datagramElements);
        if (null == datagramElements)
        {
            IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);
            return response;
        }

        header.setSourceId(nsiRequest.getRoutingHeader().getSourceId());
        //header.setCorrelationId(nsiRequest.getRoutingHeader().getMessageId());
        //header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        header.setMessageId(nsiRequest.getRoutingHeader().getMessageId());
        header.setOperationType(nsiRequest.getRoutingHeader().getOperationType());
        header.setDestinationId(nsiRequest.getRoutingHeader().getDestinationId());

        XDatagram xDatagram = NsiReactorUtils.NSI_OBJECT_FACTORY.createXDatagram();
        Map<String, List<Object>> nsiEntityTypeToEntityListMap = new HashMap<>();

        StringBuilder unSupportedBuilder = new StringBuilder();
        StringBuilder unProcessableBuilder = new StringBuilder();

        // Предварительно проверяем пакет на наличие неизвестных/необрабатываемых сущностей
        for (Object item : datagramElements)
        {
            if (!MODIFIABLE_NSI_ENTITTY_CLASSES_SET.contains(item.getClass()))
            {
                if (!PROCESSABLE_NSI_ENTITTY_CLASSES_SET.contains(item.getClass()))
                {
                    FefuNsiSyncDAO.getLog4jLogger().error(RESPONSE_CODE_TITLE_ERR_PREFIX + "There are some unrecognized/unsupported elements in datagram.");
                    unSupportedBuilder.append(unSupportedBuilder.length() > 0 ? ", " : "").append("'").append(item.getClass().getSimpleName()).append("'");
                } else
                {
                    FefuNsiSyncDAO.getLog4jLogger().error(RESPONSE_CODE_TITLE_ERR_PREFIX + "Insert does not supports for the given catalog in datagram.");
                    unProcessableBuilder.append(unProcessableBuilder.length() > 0 ? ", " : "").append("'").append(item.getClass().getSimpleName()).append("'");
                }

                continue;
            }

            String nsiEntityType = item.getClass().getSimpleName();
            List<Object> entityList = nsiEntityTypeToEntityListMap.get(nsiEntityType);
            if (null == entityList) entityList = new ArrayList<>();
            entityList.add(item);
            nsiEntityTypeToEntityListMap.put(nsiEntityType, entityList);
        }

        // Обрабатываем вложенные в пакет данные
        StringBuilder warnBuilder = new StringBuilder();
        for (String entityType : getEntityTypeSortedByPriority())
        {
            if (IdentityCardType.class.getSimpleName().equals(entityType)) continue;

            List<Object> objectList = nsiEntityTypeToEntityListMap.get(entityType);
            if (HumanType.class.getSimpleName().equals(entityType))
            {
                List<Object> icList = nsiEntityTypeToEntityListMap.get(IdentityCardType.class.getSimpleName());
                if (null != icList) objectList.addAll(icList);
            }

            if (null == objectList) continue;

            INsiEntityReactor reactor = getNsiEntityReactorByNsiEntityName(entityType);
            if (null != reactor)
            {
                try
                {
                    xDatagram.getEntityList().addAll(reactor.insert(objectList));
                    if (null == response.getCallCC())
                    {
                        response.setCallCC(RESPONSE_CODE_NUMBER_OK);
                        response.setCallRC(RESPONSE_CODE_TITLE_OK);
                    }
                } catch (NSIProcessingErrorException e)
                {
                    response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
                    response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + e.getMessage());
                    IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);
                    return response;
                } catch (NSIProcessingWarnException w)
                {
                    xDatagram.getEntityList().addAll(w.getDatagramObjects());
                    response.setCallCC(RESPONSE_CODE_NUMBER_WARN);
                    warnBuilder.append(warnBuilder.length() > 0 ? ";\n" : "").append(w.getMessage());
                    response.setCallRC(w.getMessage());
                }
            }
        }

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram));
        MessageElement datagramOut = new SOAPBodyElement(inStream);

        List<Object> datagramContent = new ArrayList<>();
        datagramContent.add(datagramOut);
        response.setDatagram(new ServiceResponseType.Datagram(datagramContent));

        correctResponseWithUnsupportedObjectsWarnings(response, nsiRequest.getRoutingHeader().getOperationType(), nsiEntityTypeToEntityListMap.values().size(), warnBuilder, unSupportedBuilder, unProcessableBuilder);

        IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);

        return response;
    }

    public static ServiceResponseType update(ServiceRequestType nsiRequest)
    {
        ServiceResponseType response = new ServiceResponseType();
        RoutingHeaderType header = new RoutingHeaderType();
        response.setRoutingHeader(header);

        List<Object> datagramElements = null;
        if (null != nsiRequest.getDatagram()) datagramElements = getDatagramElements(nsiRequest);

        if (null == nsiRequest.getRoutingHeader().getOperationType())
            nsiRequest.getRoutingHeader().setOperationType(OPERATION_TYPE_UPDATE);

        Long rowId = IFefuNsiSyncDAO.instance.get().doLogNSIRequest(nsiRequest, datagramElements);

        validateIncomingResponseAndGetDatagramElements(nsiRequest, response, OPERATION_TYPE_UPDATE, datagramElements);
        if (null == datagramElements)
        {
            IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);
            return response;
        }

        header.setSourceId(nsiRequest.getRoutingHeader().getSourceId());
        //header.setCorrelationId(nsiRequest.getRoutingHeader().getMessageId());
        //header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        header.setMessageId(nsiRequest.getRoutingHeader().getMessageId());
        header.setOperationType(nsiRequest.getRoutingHeader().getOperationType());
        header.setDestinationId(nsiRequest.getRoutingHeader().getDestinationId());

        XDatagram xDatagram = NsiReactorUtils.NSI_OBJECT_FACTORY.createXDatagram();
        Map<String, List<Object>> nsiEntityTypeToEntityListMap = new HashMap<>();

        StringBuilder unSupportedBuilder = new StringBuilder();
        StringBuilder unProcessableBuilder = new StringBuilder();

        // Предварительно проверяем пакет на наличие неизвестных/необрабатываемых сущностей
        for (Object item : datagramElements)
        {
            if (!MODIFIABLE_NSI_ENTITTY_CLASSES_SET.contains(item.getClass()))
            {
                if (!PROCESSABLE_NSI_ENTITTY_CLASSES_SET.contains(item.getClass()))
                {
                    FefuNsiSyncDAO.getLog4jLogger().error(RESPONSE_CODE_TITLE_ERR_PREFIX + "There are some unrecognized/unsupported elements in datagram.");
                    unSupportedBuilder.append(unSupportedBuilder.length() > 0 ? ", " : "").append("'").append(item.getClass().getSimpleName()).append("'");
                } else
                {
                    FefuNsiSyncDAO.getLog4jLogger().error(RESPONSE_CODE_TITLE_ERR_PREFIX + "Update does not supports for the given catalog in datagram.");
                    unProcessableBuilder.append(unProcessableBuilder.length() > 0 ? ", " : "").append("'").append(item.getClass().getSimpleName()).append("'");
                }

                continue;

                /*response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
                if (!PROCESSABLE_NSI_ENTITTY_CLASSES_SET.contains(item.getClass()))
                    response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + "There are some unrecognized/unsupported elements in datagram.");
                else
                    response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + "Update does not supports for the given catalog in datagram.");

                IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);
                return response;*/
            }

            String nsiEntityType = item.getClass().getSimpleName();
            List<Object> entityList = nsiEntityTypeToEntityListMap.get(nsiEntityType);
            if (null == entityList) entityList = new ArrayList<>();
            entityList.add(item);
            nsiEntityTypeToEntityListMap.put(nsiEntityType, entityList);
        }

        StringBuilder warnBuilder = new StringBuilder();

        for (String entityType : getEntityTypeSortedByPriority())
        {
            List<Object> objectList = nsiEntityTypeToEntityListMap.get(entityType);
            if (null == objectList) continue;

            INsiEntityReactor reactor = getNsiEntityReactorByNsiEntityName(entityType);
            if (null != reactor)
            {
                try
                {
                    xDatagram.getEntityList().addAll(reactor.update(objectList));
                    if (null == response.getCallCC())
                    {
                        response.setCallCC(RESPONSE_CODE_NUMBER_OK);
                        response.setCallRC(RESPONSE_CODE_TITLE_OK);
                    }
                } catch (NSIProcessingErrorException e)
                {
                    response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
                    response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + e.getMessage());
                    IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);
                    return response;
                } catch (NSIProcessingWarnException w)
                {
                    xDatagram.getEntityList().addAll(w.getDatagramObjects());
                    response.setCallCC(RESPONSE_CODE_NUMBER_WARN);
                    warnBuilder.append(warnBuilder.length() > 0 ? ";\n" : "").append(w.getMessage());
                    response.setCallRC(w.getMessage());
                }
            }
        }

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram));
        MessageElement datagramOut = new SOAPBodyElement(inStream);

        List<Object> datagramContent = new ArrayList<>();
        datagramContent.add(datagramOut);
        response.setDatagram(new ServiceResponseType.Datagram(datagramContent));

        correctResponseWithUnsupportedObjectsWarnings(response, nsiRequest.getRoutingHeader().getOperationType(), nsiEntityTypeToEntityListMap.values().size(), warnBuilder, unSupportedBuilder, unProcessableBuilder);

        IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);

        return response;
    }

    public static ServiceResponseType delete(ServiceRequestType nsiRequest)
    {
        ServiceResponseType response = new ServiceResponseType();
        RoutingHeaderType header = new RoutingHeaderType();
        response.setRoutingHeader(header);

        List<Object> datagramElements = null;
        if (null != nsiRequest.getDatagram()) datagramElements = getDatagramElements(nsiRequest);

        if (null == nsiRequest.getRoutingHeader().getOperationType())
            nsiRequest.getRoutingHeader().setOperationType(OPERATION_TYPE_DELETE);

        Long rowId = IFefuNsiSyncDAO.instance.get().doLogNSIRequest(nsiRequest, datagramElements);

        validateIncomingResponseAndGetDatagramElements(nsiRequest, response, OPERATION_TYPE_DELETE, datagramElements);
        if (null == datagramElements)
        {
            IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);
            return response;
        }

        header.setSourceId(nsiRequest.getRoutingHeader().getSourceId());
        //header.setCorrelationId(nsiRequest.getRoutingHeader().getMessageId());
        //header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        header.setMessageId(nsiRequest.getRoutingHeader().getMessageId());
        header.setOperationType(nsiRequest.getRoutingHeader().getOperationType());
        header.setDestinationId(nsiRequest.getRoutingHeader().getDestinationId());

        XDatagram xDatagram = NsiReactorUtils.NSI_OBJECT_FACTORY.createXDatagram();
        Map<String, List<Object>> nsiEntityTypeToEntityListMap = new HashMap<>();

        StringBuilder unSupportedBuilder = new StringBuilder();
        StringBuilder unProcessableBuilder = new StringBuilder();

        // Предварительно проверяем пакет на наличие неизвестных/необрабатываемых сущностей
        for (Object item : datagramElements)
        {
            if (!MODIFIABLE_NSI_ENTITTY_CLASSES_SET.contains(item.getClass()))
            {
                if (!PROCESSABLE_NSI_ENTITTY_CLASSES_SET.contains(item.getClass()))
                {
                    FefuNsiSyncDAO.getLog4jLogger().error(RESPONSE_CODE_TITLE_ERR_PREFIX + "There are some unrecognized/unsupported elements in datagram.");
                    unSupportedBuilder.append(unSupportedBuilder.length() > 0 ? ", " : "").append("'").append(item.getClass().getSimpleName()).append("'");
                } else
                {
                    FefuNsiSyncDAO.getLog4jLogger().error(RESPONSE_CODE_TITLE_ERR_PREFIX + "Delete does not supports for the given catalog in datagram.");
                    unProcessableBuilder.append(unProcessableBuilder.length() > 0 ? ", " : "").append("'").append(item.getClass().getSimpleName()).append("'");
                }

                continue;

                /*response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
                if (!PROCESSABLE_NSI_ENTITTY_CLASSES_SET.contains(item.getClass()))
                    response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + "There are some unrecognized/unsupported elements in datagram.");
                else
                    response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + "Delete does not supports for the given catalog in datagram.");

                IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);
                return response;*/
            } else if (item instanceof IdentityCardType)
            {
                response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
                response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + "Delete does not supports for the given catalog in datagram.");
                IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);
                return response;
            }

            String nsiEntityType = item.getClass().getSimpleName();
            List<Object> entityList = nsiEntityTypeToEntityListMap.get(nsiEntityType);
            if (null == entityList) entityList = new ArrayList<>();
            entityList.add(item);
            nsiEntityTypeToEntityListMap.put(nsiEntityType, entityList);
        }

        StringBuilder warnBuilder = new StringBuilder();
        List<String> entityTypeList = getEntityTypeSortedByPriority();
        Collections.reverse(entityTypeList);

        for (String entityType : entityTypeList)
        {
            List<Object> objectList = nsiEntityTypeToEntityListMap.get(entityType);
            if (null == objectList) continue;

            INsiEntityReactor reactor = getNsiEntityReactorByNsiEntityName(entityType);
            if (null != reactor)
            {
                try
                {
                    xDatagram.getEntityList().addAll(reactor.delete(objectList));
                    if (null == response.getCallCC())
                    {
                        response.setCallCC(RESPONSE_CODE_NUMBER_OK);
                        response.setCallRC(RESPONSE_CODE_TITLE_OK);
                    }
                } catch (NSIProcessingErrorException e)
                {
                    response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
                    response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + e.getMessage());
                    IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);
                    return response;
                } catch (NSIProcessingWarnException w)
                {
                    xDatagram.getEntityList().addAll(w.getDatagramObjects());
                    response.setCallCC(RESPONSE_CODE_NUMBER_WARN);
                    warnBuilder.append(warnBuilder.length() > 0 ? ";\n" : "").append(w.getMessage());
                    response.setCallRC(w.getMessage());
                }
            }
        }

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram));
        MessageElement datagramOut = new SOAPBodyElement(inStream);

        List<Object> datagramContent = new ArrayList<>();
        datagramContent.add(datagramOut);
        response.setDatagram(new ServiceResponseType.Datagram(datagramContent));

        correctResponseWithUnsupportedObjectsWarnings(response, nsiRequest.getRoutingHeader().getOperationType(), nsiEntityTypeToEntityListMap.values().size(), warnBuilder, unSupportedBuilder, unProcessableBuilder);

        IFefuNsiSyncDAO.instance.get().doUpdateLogNSIRequest(response, rowId);

        return response;
    }

    private static void correctResponseWithUnsupportedObjectsWarnings(ServiceResponseType response, String operationType, int processedObjAmount, StringBuilder warnBuilder, StringBuilder unSupportedBuilder, StringBuilder unProcessableBuilder)
    {
        if (warnBuilder.length() > 0)
        {
            response.setCallRC(RESPONSE_CODE_TITLE_WARN_PREFIX + "\n" + warnBuilder.toString());
        }

        if (null == response.getCallRC()) response.setCallRC(RESPONSE_CODE_TITLE_OK);

        if (unSupportedBuilder.length() > 0 || unProcessableBuilder.length() > 0)
        {
            StringBuilder resultRC = new StringBuilder();

            if (unSupportedBuilder.length() > 0)
            {
                resultRC.append("OB does not support next catalogs: ").append(unSupportedBuilder.toString()).append(".");
            }

            if (unProcessableBuilder.length() > 0)
            {
                resultRC.append(resultRC.length() > 0 ? " " : "").append("OB does not support ").append(operationType)
                        .append(" for catalogs ").append(unProcessableBuilder.toString()).append(".");
            }

            if (RESPONSE_CODE_TITLE_OK.equals(response.getCallRC()))
            {
                if (processedObjAmount == 0)
                {
                    response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
                    response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + resultRC.toString());
                } else
                {
                    response.setCallCC(RESPONSE_CODE_NUMBER_WARN);
                    response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + resultRC.toString());
                }
            } else if (RESPONSE_CODE_NUMBER_WARN.equals(response.getCallCC()))
            {
                response.setCallRC(RESPONSE_CODE_TITLE_WARN_PREFIX + "\n" + resultRC.toString() + "\n" + warnBuilder.toString());
            } else
            {
                if (null == response.getCallRC() || response.getCallRC().length() == 0)
                    response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + resultRC.toString());
                else
                    response.setCallRC(response.getCallRC() + "\n" + resultRC.toString());
            }
        }
    }

    public static ServiceResponseType dummyResponse(ServiceRequestType nsiRequest)
    {
        RoutingHeaderType header = new RoutingHeaderType();
        header.setSourceId(nsiRequest.getRoutingHeader().getSourceId());
        header.setMessageId(nsiRequest.getRoutingHeader().getMessageId());
        header.setOperationType(nsiRequest.getRoutingHeader().getOperationType());
        header.setDestinationId(nsiRequest.getRoutingHeader().getDestinationId());

        ServiceResponseType response = new ServiceResponseType();
        response.setRoutingHeader(header);
        response.setCallCC(BigInteger.valueOf(0));
        response.setCallRC("It's OK. Just test response.");
        System.out.println("************* Dummy response **************");

        return response;
    }

    public static ServiceResponseType2 dummyResponse2()
    {
        ServiceResponseType2 response = new ServiceResponseType2();
        response.setCallCC(BigInteger.valueOf(0));
        response.setCallRC("It's OK. Just test response.");
        System.out.println("************* Dummy response 2 **************");
        return response;
    }

    private static void validateIncomingResponseAndGetDatagramElements(ServiceRequestType nsiRequest, ServiceResponseType response, String operationType, List<Object> datagramElements)
    {
        RoutingHeaderType header = response.getRoutingHeader();
        if (null == nsiRequest.getRoutingHeader())
        {
            response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
            response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + "No request header was found.");
            return;
        }

        header.setSourceId(nsiRequest.getRoutingHeader().getSourceId());
        header.setMessageId(nsiRequest.getRoutingHeader().getMessageId());
        header.setOperationType(nsiRequest.getRoutingHeader().getOperationType());
        header.setDestinationId(nsiRequest.getRoutingHeader().getDestinationId());

        /*if (null == nsiRequest.getRoutingHeader().getOperationType())
        {
            response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
            response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + "Operation type is not specified.");
            return null;
        }*/

        if (null != nsiRequest.getRoutingHeader().getOperationType() && !operationType.equals(nsiRequest.getRoutingHeader().getOperationType()))
        {
            response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
            response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + "Operation type '" + nsiRequest.getRoutingHeader().getOperationType() + "' could not be processed by the '" + operationType + "' method.");
            return;
        }

        if (null == nsiRequest.getDatagram())
        {
            response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
            response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + "No datagram specified.");
            return;
        }

        if (null == datagramElements || 0 == datagramElements.size())
        {
            response.setCallCC(RESPONSE_CODE_NUMBER_ERROR);
            response.setCallRC(RESPONSE_CODE_TITLE_ERR_PREFIX + "There is no elements in datagram specified.");
            return;
        }

    }

    private static List<Object> getDatagramElements(ServiceRequestType nsiRequest)
    {
        ElementNSImpl respMsg = nsiRequest.getDatagram().getContent().size() > 0 ? (ElementNSImpl) nsiRequest.getDatagram().getContent().get(0) : null;
        if (null != respMsg)
        {
            try
            {
                XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, toXML(respMsg).getBytes());
                return respDatagram.getEntityList();
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String toXML(ElementNSImpl document) throws TransformerException, IOException
    {
        try
        {
            OutputFormat format = new OutputFormat();
            format.setLineWidth(65);
            format.setIndenting(true);
            format.setIndent(2);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);

            return out.toString();
        } catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}