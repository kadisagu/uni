/* $Id$ */
package ru.tandemservice.unifefu.component.menu.OnlineEntrantList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.unifefu.base.bo.OnlineEntrant.ui.EditComment.OnlineEntrantEditComment;
import ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt;

/**
 * @author Nikolay Fedorovskih
 * @since 01.07.2013
 */
public class Controller extends ru.tandemservice.uniec.component.menu.OnlineEntrantList.Controller
{
    public void prepareDataSource(IBusinessComponent component)
    {
        super.prepareDataSource(component);
        DynamicListDataSource<OnlineEntrant> ds = getModel(component).getDataSource();
        int position = ds.getColumn(ENTRANT_REG_DATE_COLUMN).getNumber();
        ds.addColumn(new SimpleColumn("e-mail", OnlineEntrant.email().s()), position + 1);

        ds.addColumn(new SimpleColumn("Комментарий", OnlineEntrantFefuExt.P_COMMENT).setWidth("160px"), position + 2);

        ds.addColumn(new ActionColumn("Редактировать комментарий", ActionColumn.EDIT, "onClickEditComment").setPermissionKey("editEntrantComment"), position + 3);

        // Всегда нужна колонка печати заявления
        if (ds.getColumn(PRINT_REQUEST_COLUMN) == null)
            addPrintColumn(ds, ds.getColumn(DELETE_COLUMN).getNumber());
    }

    public void onClickEditComment(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(OnlineEntrantEditComment.class.getSimpleName(),
                                                                                            new ParametersMap().add("entrantId", component.<Long>getListenerParameter())));
    }
}