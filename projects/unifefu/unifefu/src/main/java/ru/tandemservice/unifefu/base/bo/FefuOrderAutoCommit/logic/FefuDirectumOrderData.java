/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic;

import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 14.11.2012
 */
public class FefuDirectumOrderData
{
    private Long _id;
    private String _number;
    private Date _date;

    public FefuDirectumOrderData(Long id, String number, Date date)
    {
        _id = id;
        _number = number;
        _date = date;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public String getNumber()
    {
        return _number;
    }

    public void setNumber(String number)
    {
        _number = number;
    }

    public Date getDate()
    {
        return _date;
    }

    public void setDate(Date date)
    {
        _date = date;
    }
}