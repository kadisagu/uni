/* $Id: Model.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.workplan.WorkPlanPartSetEdit;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 08.02.14
 * Time: 21:45
 */
public class Model extends ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit.Model
{
    public boolean wizard = false;

    public boolean isWizard()
    {
        return wizard;
    }

    public void setWizard(boolean wizard)
    {
        this.wizard = wizard;
    }

    public boolean isNotWizard()
    {
        return !isWizard();
    }
}
