/* $Id$ */
package ru.tandemservice.unifefu.component.order.EnrollmentOrderAddEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.entity.catalog.FefuEnrollmentStep;
import ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt;

/**
 * @author Nikolay Fedorovskih
 * @since 31.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model, EnrollmentOrder order)
    {
        EnrollmentOrderFefuExt orderFefuExt = null;

        if (order.getId() != null)
            orderFefuExt = getByNaturalId(new EnrollmentOrderFefuExt.NaturalId(order));

        if (orderFefuExt == null)
            orderFefuExt = new EnrollmentOrderFefuExt();

        model.setOrderExt(orderFefuExt);

        model.setEnrollmentStepList(getList(FefuEnrollmentStep.class, FefuEnrollmentStep.P_CODE));
    }

    @Override
    public void update(Model model, EnrollmentOrder order)
    {
        model.getOrderExt().setEnrollmentOrder(order);
        saveOrUpdate(model.getOrderExt());
    }
}