/*$Id$*/
package ru.tandemservice.unifefu.component.group.FefuGroupPub;

import org.tandemframework.core.entity.OrderDirection;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Comparator;

/**
 * @author DMITRY KNYAZEV
 * @since 03.10.2014
 */
public final class FefuGroupStudentListCmp implements Comparator<Student>
{
	private final OrderDirection _orderDirection;

	public FefuGroupStudentListCmp(OrderDirection orderDirection)
	{
		_orderDirection = orderDirection;
	}

	public FefuGroupStudentListCmp()
	{
		this(OrderDirection.asc);
	}

	@Override
	public int compare(Student o1, Student o2)
	{
		int ret = o1.getCompensationType().getCode().compareTo(o2.getCompensationType().getCode());
		if (ret != 0)
		{
			if (_orderDirection == OrderDirection.desc)
				return -ret;
			return ret;
		}

		return Student.FULL_FIO_AND_ID_COMPARATOR.compare(o1, o2);
	}
}
