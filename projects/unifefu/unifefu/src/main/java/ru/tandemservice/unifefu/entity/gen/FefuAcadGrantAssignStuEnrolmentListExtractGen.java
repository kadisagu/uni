package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О назначении академической стипендии (вступительные испытания)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuAcadGrantAssignStuEnrolmentListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract";
    public static final String ENTITY_NAME = "fefuAcadGrantAssignStuEnrolmentListExtract";
    public static final int VERSION_HASH = -162693401;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_GRANT_SIZE = "grantSize";
    public static final String P_GROUP_MANAGER_BONUS_SIZE = "groupManagerBonusSize";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Date _beginDate;     // Дата начала выплаты стипендии
    private Date _endDate;     // Дата окончания выплаты стипендии
    private long _grantSize;     // Размер стипендии (в сотых долях копейки)
    private Long _groupManagerBonusSize;     // Размер выплаты старосте (в сотых долях копейки)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала выплаты стипендии. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания выплаты стипендии. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Размер стипендии (в сотых долях копейки). Свойство не может быть null.
     */
    @NotNull
    public long getGrantSize()
    {
        return _grantSize;
    }

    /**
     * @param grantSize Размер стипендии (в сотых долях копейки). Свойство не может быть null.
     */
    public void setGrantSize(long grantSize)
    {
        dirty(_grantSize, grantSize);
        _grantSize = grantSize;
    }

    /**
     * @return Размер выплаты старосте (в сотых долях копейки).
     */
    public Long getGroupManagerBonusSize()
    {
        return _groupManagerBonusSize;
    }

    /**
     * @param groupManagerBonusSize Размер выплаты старосте (в сотых долях копейки).
     */
    public void setGroupManagerBonusSize(Long groupManagerBonusSize)
    {
        dirty(_groupManagerBonusSize, groupManagerBonusSize);
        _groupManagerBonusSize = groupManagerBonusSize;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuAcadGrantAssignStuEnrolmentListExtractGen)
        {
            setCourse(((FefuAcadGrantAssignStuEnrolmentListExtract)another).getCourse());
            setGroup(((FefuAcadGrantAssignStuEnrolmentListExtract)another).getGroup());
            setCompensationType(((FefuAcadGrantAssignStuEnrolmentListExtract)another).getCompensationType());
            setBeginDate(((FefuAcadGrantAssignStuEnrolmentListExtract)another).getBeginDate());
            setEndDate(((FefuAcadGrantAssignStuEnrolmentListExtract)another).getEndDate());
            setGrantSize(((FefuAcadGrantAssignStuEnrolmentListExtract)another).getGrantSize());
            setGroupManagerBonusSize(((FefuAcadGrantAssignStuEnrolmentListExtract)another).getGroupManagerBonusSize());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuAcadGrantAssignStuEnrolmentListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuAcadGrantAssignStuEnrolmentListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuAcadGrantAssignStuEnrolmentListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "grantSize":
                    return obj.getGrantSize();
                case "groupManagerBonusSize":
                    return obj.getGroupManagerBonusSize();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "grantSize":
                    obj.setGrantSize((Long) value);
                    return;
                case "groupManagerBonusSize":
                    obj.setGroupManagerBonusSize((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "grantSize":
                        return true;
                case "groupManagerBonusSize":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "grantSize":
                    return true;
                case "groupManagerBonusSize":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "grantSize":
                    return Long.class;
                case "groupManagerBonusSize":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuAcadGrantAssignStuEnrolmentListExtract> _dslPath = new Path<FefuAcadGrantAssignStuEnrolmentListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuAcadGrantAssignStuEnrolmentListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Размер стипендии (в сотых долях копейки). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getGrantSize()
     */
    public static PropertyPath<Long> grantSize()
    {
        return _dslPath.grantSize();
    }

    /**
     * @return Размер выплаты старосте (в сотых долях копейки).
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getGroupManagerBonusSize()
     */
    public static PropertyPath<Long> groupManagerBonusSize()
    {
        return _dslPath.groupManagerBonusSize();
    }

    public static class Path<E extends FefuAcadGrantAssignStuEnrolmentListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Long> _grantSize;
        private PropertyPath<Long> _groupManagerBonusSize;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Дата начала выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(FefuAcadGrantAssignStuEnrolmentListExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания выплаты стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(FefuAcadGrantAssignStuEnrolmentListExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Размер стипендии (в сотых долях копейки). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getGrantSize()
     */
        public PropertyPath<Long> grantSize()
        {
            if(_grantSize == null )
                _grantSize = new PropertyPath<Long>(FefuAcadGrantAssignStuEnrolmentListExtractGen.P_GRANT_SIZE, this);
            return _grantSize;
        }

    /**
     * @return Размер выплаты старосте (в сотых долях копейки).
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract#getGroupManagerBonusSize()
     */
        public PropertyPath<Long> groupManagerBonusSize()
        {
            if(_groupManagerBonusSize == null )
                _groupManagerBonusSize = new PropertyPath<Long>(FefuAcadGrantAssignStuEnrolmentListExtractGen.P_GROUP_MANAGER_BONUS_SIZE, this);
            return _groupManagerBonusSize;
        }

        public Class getEntityClass()
        {
            return FefuAcadGrantAssignStuEnrolmentListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuAcadGrantAssignStuEnrolmentListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
