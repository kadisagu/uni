/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.DevelopTechType;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 07.05.2014
 */
public class DevelopTechUtil extends SimpleNsiCatalogUtil<DevelopTechType, DevelopTech>
{
    public static final String DEVELOP_TECH_GROUP_NSI_FIELD = "DeveloptechGroup";

    @Override
    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            super.getNsiFields();
            NSI_FIELDS.add(DEVELOP_TECH_GROUP_NSI_FIELD);
        }
        return NSI_FIELDS;
    }

    @Override
    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            super.getEntityFields();
            ENTITY_FIELDS.add(DevelopTech.group().s());
        }
        return ENTITY_FIELDS;
    }

    @Override
    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            super.getNsiToObFieldsMap();
            NSI_TO_OB_FIELDS_MAP.put(DEVELOP_TECH_GROUP_NSI_FIELD, DevelopTech.group().s());
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    @Override
    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            super.getObToNsiFieldsMap();
            OB_TO_NSI_FIELDS_MAP.put(DevelopTech.group().s(), DEVELOP_TECH_GROUP_NSI_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    public long getNsiDevelopTechGroupHash(DevelopTechType nsiEntity, boolean caseInsensitive)
    {
        if (null == nsiEntity.getDeveloptechGroup()) return 0;
        if (caseInsensitive) return MD5HashBuilder.getCheckSum(nsiEntity.getDeveloptechGroup().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(nsiEntity.getDeveloptechGroup());
    }

    @Override
    public boolean isFieldEmpty(DevelopTechType nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case DEVELOP_TECH_GROUP_NSI_FIELD:
                return null == nsiEntity.getDeveloptechGroup();
            default:
                return super.isFieldEmpty(nsiEntity, fieldName);
        }
    }

    @Override
    public long getNsiFieldHash(DevelopTechType nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case DEVELOP_TECH_GROUP_NSI_FIELD:
                return getNsiDevelopTechGroupHash(nsiEntity, caseInsensitive);
            default:
                return super.getNsiFieldHash(nsiEntity, fieldName, caseInsensitive);
        }
    }

    public long getDevelopTechGroupHash(DevelopTech entity, boolean caseInsensitive)
    {
        if (null == entity.getGroup()) return 0;
        if (caseInsensitive) return MD5HashBuilder.getCheckSum(entity.getGroup().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(entity.getGroup());
    }

    @Override
    public long getEntityFieldHash(DevelopTech entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case DevelopTech.P_GROUP:
                return getDevelopTechGroupHash(entity, caseInsensitive);
            default:
                return super.getEntityFieldHash(entity, nsiId, fieldName, caseInsensitive);
        }
    }
}