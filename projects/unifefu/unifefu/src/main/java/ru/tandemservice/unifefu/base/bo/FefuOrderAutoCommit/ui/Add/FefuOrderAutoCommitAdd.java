/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author Dmitry Seleznev
 * @since 13.11.2012
 */
@Configuration
public class FefuOrderAutoCommitAdd extends BusinessComponentManager
{
}