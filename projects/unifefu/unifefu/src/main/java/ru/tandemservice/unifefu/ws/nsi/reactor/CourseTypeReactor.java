/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.CourseType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class CourseTypeReactor extends SimpleCatalogEntityNewReactor<CourseType, Course>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<Course> getEntityClass()
    {
        return Course.class;
    }

    @Override
    public Class<CourseType> getNSIEntityClass()
    {
        return CourseType.class;
    }

    @Override
    public CourseType getCatalogElementRetrieveRequestObject(String guid)
    {
        CourseType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createCourseType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public CourseType createEntity(CoreCollectionUtils.Pair<Course, FefuNsiIds> entityPair)
    {
        CourseType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createCourseType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setCourseID(entityPair.getX().getCode());
        nsiEntity.setCourseNumber(String.valueOf(entityPair.getX().getIntValue()));
        nsiEntity.setCourseName(entityPair.getX().getTitle());
        return nsiEntity;
    }

    @Override
    public Course createEntity(CourseType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getCourseName()) return null;

        Course entity = new Course();
        entity.setTitle(nsiEntity.getCourseName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(Course.class));
        if (null != nsiEntity.getCourseNumber())
            entity.setIntValue(Integer.parseInt(nsiEntity.getCourseNumber()));
        else
            entity.setIntValue(-1);

        return entity;
    }

    @Override
    public Course updatePossibleDuplicateFields(CourseType nsiEntity, CoreCollectionUtils.Pair<Course, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), Course.title().s()))
                entityPair.getX().setTitle(nsiEntity.getCourseName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), Course.intValue().s()))
                entityPair.getX().setIntValue(null != nsiEntity.getCourseNumber() ? Integer.parseInt(nsiEntity.getCourseNumber()) : -1);
            return entityPair.getX();
        }
        return null;
    }

    @Override
    public CourseType updateNsiEntityFields(CourseType nsiEntity, Course entity)
    {
        nsiEntity.setCourseName(entity.getTitle());
        nsiEntity.setCourseID(entity.getCode());
        nsiEntity.setCourseNumber(String.valueOf(entity.getIntValue()));
        return nsiEntity;
    }
}