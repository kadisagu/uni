package ru.tandemservice.unifefu.base.ext.TrJournal.ui.AddEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.AddEvent.TrJournalAddEvent;

/**
 * User: newdev
 * Date: 25.11.13
 */
@Configuration
public class TrJournalAddEventExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrJournalAddEventExtUI.class.getSimpleName();

    @Autowired
    private TrJournalAddEvent _trJournalAddEvent;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trJournalAddEvent.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrJournalAddEventExtUI.class))
                .addAction(new TrJournalAddEventClickApplyAction("onClickApply"))
                .create();
    }
}