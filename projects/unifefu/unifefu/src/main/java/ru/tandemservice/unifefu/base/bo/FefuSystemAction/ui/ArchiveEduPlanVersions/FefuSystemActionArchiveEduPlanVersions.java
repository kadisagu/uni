package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.ArchiveEduPlanVersions;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;

/**
 * @author ilunin
 * @since 13.10.2014
 */
@Configuration
public class FefuSystemActionArchiveEduPlanVersions extends BusinessComponentManager
{
    public final static String EDU_PROGRAM_DURATION_DS = "eduProgramDurationDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDU_PROGRAM_DURATION_DS, developPeriodDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler developPeriodDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EduProgramDuration.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
            }
        };
    }
}