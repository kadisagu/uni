/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EppRegistry.logic.print;

import org.tandemframework.caf.report.ExcelListDataSourcePrinter;
import org.tandemframework.core.view.list.source.AbstractListDataSource;

/**
 * @author Alexey Lopatin
 * @since 20.11.2014
 */
public class FefuRegistryExcelListDataSourcePrinter extends ExcelListDataSourcePrinter
{
    public FefuRegistryExcelListDataSourcePrinter(String title, AbstractListDataSource source)
    {
        super(title, source);
    }

    @Override
    protected void init()
    {
        super.init();
        _headerRowsNum = 7;
    }
}
