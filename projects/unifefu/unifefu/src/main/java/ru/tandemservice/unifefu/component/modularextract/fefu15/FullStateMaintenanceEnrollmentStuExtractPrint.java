/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu15;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract;

import java.util.List;

/**
 * @author nvankov
 * @since 11/15/13
 */
public class FullStateMaintenanceEnrollmentStuExtractPrint implements IPrintFormCreator<FullStateMaintenanceEnrollmentStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FullStateMaintenanceEnrollmentStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        if(extract.isHasPayments())
        {
            List<FefuOrphanPayment> payments = UnifefuDaoFacade.getFefuMoveStudentDAO().getOrphanPayments(extract.getId());
            if(payments.isEmpty())
                modifier.put("payments", new RtfString().par());
            else
            {
                FefuOrphanPayment firstPayment = payments.get(0);
                List<String> paymentPrints = Lists.newArrayList();
                for(FefuOrphanPayment payment : payments)
                {
                    paymentPrints.add(payment.getPrint() + " в размере " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(payment.getPaymentSumInRuble()) + " руб.");
                }
                RtfString paymentStr = new RtfString();
                paymentStr.append("2. Выплатить " + modifier.getStringValue("fio_D") + " " +
                        StringUtils.join(paymentPrints, ", ") + " в " +
                        CommonBaseDateUtil.getMonthNameDeclined(firstPayment.getPaymentMonth(), GrammaCase.PREPOSITIONAL) +
                        " " + firstPayment.getPaymentYear() + " года.").par().par();
                modifier.put("payments", paymentStr);
            }
        }
        else
            modifier.put("payments", new RtfString().par());

        modifier.put("enrollDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEnrollDate()));

        CommonExtractPrint.initFefuGroup(modifier, "group", extract.getEntity().getGroup(), extract.getEntity().getEducationOrgUnit().getDevelopForm(), " группы ");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}
