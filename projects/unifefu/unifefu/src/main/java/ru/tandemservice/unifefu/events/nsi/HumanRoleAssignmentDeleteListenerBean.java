/* $Id:$ */
package ru.tandemservice.unifefu.events.nsi;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;
import org.tandemframework.shared.organization.sec.entity.IRoleAssignment;
import ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRole;
import ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRoleToContextRel;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 12.11.2015
 */
public class HumanRoleAssignmentDeleteListenerBean
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, FefuNsiHumanRole.class, DeleteHumanRoleAssignmentListener.INSTANCE);
    }

    public static class DeleteHumanRoleAssignmentListener extends TransactionCompleteAction<Void> implements IDSetEventListener
    {
        private static final DeleteHumanRoleAssignmentListener INSTANCE = new DeleteHumanRoleAssignmentListener();

        @Override
        public void onEvent(final DSetEvent event)
        {
            List<IRoleAssignment> humanRoleList = new DQLSelectBuilder().fromEntity(FefuNsiHumanRole.class, "r").column(property("ra"))
                    .joinEntity("r", DQLJoinType.inner, FefuNsiHumanRoleToContextRel.class, "rel", eq(property(FefuNsiHumanRole.id().fromAlias("r")), property(FefuNsiHumanRoleToContextRel.humanRole().id().fromAlias("rel"))))
                    .joinEntity("rel", DQLJoinType.left, IRoleAssignment.class, "ra", eq(property(FefuNsiHumanRoleToContextRel.roleAssignment().id().fromAlias("rel")), property("ra", IRoleAssignment.P_ID)))
                    .where(in(property(FefuNsiHumanRole.id().fromAlias("r")), event.getMultitude().getInExpression())).createStatement(event.getContext()).list();

            for (IRoleAssignment roleAssignment : humanRoleList)
            {
                if (null != roleAssignment) event.getContext().getSession().delete(roleAssignment);
            }
            this.register(event);
        }
    }
}