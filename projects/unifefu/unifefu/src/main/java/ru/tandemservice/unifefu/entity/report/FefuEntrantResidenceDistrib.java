package ru.tandemservice.unifefu.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.IFefuEntrantReport;
import ru.tandemservice.unifefu.entity.report.gen.FefuEntrantResidenceDistribGen;

/**
 * Распределение абитуриентов по месту жительства с учетом онлайн-абитуриентов
 */
public class FefuEntrantResidenceDistrib extends FefuEntrantResidenceDistribGen implements IFefuEntrantReport
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}