/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.EntrantRequestStep;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;
import ru.tandemservice.unifefu.dao.IFefuEntrantDAO;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.EntrantFefuExt;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 29.05.2013
 */
public class DAO extends ru.tandemservice.uniec.component.wizard.EntrantRequestStep.DAO
{
    @Override
    public void prepare(ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model m)
    {
        super.prepare(m);
        Model model = (Model) m;
        model.setOnlineEntrantSign(model.getOnlineEntrant() != null);

        // Технические комиссии
        List<FefuTechnicalCommission> commissions = UnifefuDaoFacade.getFefuEntrantDAO().getTechnicalCommissionsList(model.getEnrollmentCampaign());
        model.setTechnicalCommissionsSelectModel(new LazySimpleSelectModel<>(commissions).setSearchFromStart(false));
        model.setTechnicalCommission(UnifefuDaoFacade.getFefuEntrantDAO().getTechnicalCommission(model.getEntrantRequest()));
        model.setOldTechnicalCommission(model.getTechnicalCommission());
    }

    @Override
    public void update(ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model m)
    {
        super.update(m);
        Model model = (Model) m;
        if (model.getOnlineEntrant().getId() != null)
        {
            Entrant entrant = model.getEntrantRequest().getEntrant();
            EntrantFefuExt entrantFefuExt = getByNaturalId(new EntrantFefuExt.NaturalId(entrant));
            if (entrantFefuExt == null)
            {
                entrantFefuExt = new EntrantFefuExt();
                entrantFefuExt.setEntrant(entrant);
            }
            entrantFefuExt.setIsOnlineEntrant(model.getOnlineEntrantSign());
            saveOrUpdate(entrantFefuExt);
        }

        IFefuEntrantDAO entrantDAO = UnifefuDaoFacade.getFefuEntrantDAO();
        entrantDAO.setTechnicalCommission(m.getEntrantRequest(), model.getTechnicalCommission());

        getSession().flush(); // сохраняем заявление и связь с ТК

        // Если у нас единственное заявление и изменилась тех. комиссия, то перегенерируем номер абитуриента
        Entrant entrant = m.getEntrantRequest().getEntrant();
        if (entrantDAO.getEntrantRequestCount(entrant) == 1 &&
                (model.getOldTechnicalCommission() == null || !model.getOldTechnicalCommission().equals(model.getTechnicalCommission())))
        {
            entrant.setPersonalNumber(entrantDAO.getFefuEntrantNumber(m.getEntrantRequest(), getSession()));
            update(entrant);
            model.setOldTechnicalCommission(model.getTechnicalCommission());
        }
    }

    @Override
    protected void fixDirectionList(ru.tandemservice.uniec.component.wizard.EntrantRequestStep.Model model, List<OnlineRequestedEnrollmentDirection> onlineDirections)
    {
        // Нам надо восстановить порядок направлений в рамках конкурсных групп в соответствие с приоритетами,
        // расставленными онлайн-абитуриентом. Ох уж эти проектные кастомизации.

        // Группируем все направления по КГ, сохраняя порядок вставок
        Map<CompetitionGroup, CompetitionGroupPriorityFixer> map = new LinkedHashMap<>();
        CompetitionGroupPriorityFixer container;

        for (OnlineRequestedEnrollmentDirection onlineDir : onlineDirections)
        {
            CompetitionGroup cg = onlineDir.getEnrollmentDirection().getCompetitionGroup();
            if ((container = map.get(cg)) == null)
                map.put(cg, container = new CompetitionGroupPriorityFixer());
            container.onlineDirections.add(onlineDir);
        }

        List<RequestedEnrollmentDirection> selectedDirections = model.getSelectedRequestedEnrollmentDirectionList();
        for (RequestedEnrollmentDirection requestedDir : selectedDirections)
        {
            CompetitionGroup cg = requestedDir.getEnrollmentDirection().getCompetitionGroup();
            if ((container = map.get(cg)) == null)
                map.put(cg, container = new CompetitionGroupPriorityFixer());
            container.requestedDirections.add(requestedDir);
        }

        selectedDirections.clear();
        // Пересортируем направления и выгружаем обратно в нужном порядке
        for (CompetitionGroupPriorityFixer fixer : map.values())
        {
            selectedDirections.addAll(fixer.getFixedList());
        }
        model.setSelectedRequestedEnrollmentDirectionList(selectedDirections);
    }

    private class CompetitionGroupPriorityFixer
    {
        List<OnlineRequestedEnrollmentDirection> onlineDirections = new ArrayList<>();

        List<RequestedEnrollmentDirection> requestedDirections = new ArrayList<>();

        List<RequestedEnrollmentDirection> getFixedList()
        {
            List<RequestedEnrollmentDirection> fixedList = new ArrayList<>();
            int idx;
            for (OnlineRequestedEnrollmentDirection onlineDir : onlineDirections)
            {
                if ((idx = findRequestedDir(onlineDir)) >= 0)
                    fixedList.add(requestedDirections.remove(idx));
            }
            fixedList.addAll(requestedDirections);
            return fixedList;
        }

        boolean intCompare(Integer x, Integer y)
        {
            return (x == null && y == null) || (x != null && y != null && x.intValue() == y.intValue());
        }

        int findRequestedDir(OnlineRequestedEnrollmentDirection onlineDir)
        {
            for (int i = 0; i < requestedDirections.size(); i++)
            {
                RequestedEnrollmentDirection requestedDir = requestedDirections.get(i);
                if (onlineDir.getEnrollmentDirection() == requestedDir.getEnrollmentDirection() &&
                        onlineDir.getCompensationType() == requestedDir.getCompensationType() &&
                        onlineDir.getCompetitionKind() == requestedDir.getCompetitionKind() &&
                        onlineDir.getStudentCategory() == requestedDir.getStudentCategory() &&
                        onlineDir.isTargetAdmission() == requestedDir.isTargetAdmission() &&
                        intCompare(onlineDir.getProfileWorkExperienceDays(), requestedDir.getProfileWorkExperienceDays()) &&
                        intCompare(onlineDir.getProfileWorkExperienceMonths(), requestedDir.getProfileWorkExperienceMonths()) &&
                        intCompare(onlineDir.getProfileWorkExperienceYears(), requestedDir.getProfileWorkExperienceYears()))
                {
                    return i;
                }
            }
            // Соответствия не найдено из-за косяка, связанного с добавлением нескольких направлений из одной КГ.
            // Поэтому ищем соответствие просто по направлению и выставляем остальные параметры, как надо.
            // По сути это хак, для исправления косяка в продукте. Но Толя сказал с этим смириться.

            for (int i = 0; i < requestedDirections.size(); i++)
            {
                RequestedEnrollmentDirection requestedDir = requestedDirections.get(i);
                if (onlineDir.getEnrollmentDirection() == requestedDir.getEnrollmentDirection() &&
                        onlineDir.getCompensationType() == requestedDir.getCompensationType())
                {
                    requestedDir.setCompetitionKind(onlineDir.getCompetitionKind());
                    requestedDir.setStudentCategory(onlineDir.getStudentCategory());
                    requestedDir.setTargetAdmission(onlineDir.isTargetAdmission());
                    requestedDir.setProfileWorkExperienceDays(onlineDir.getProfileWorkExperienceDays());
                    requestedDir.setProfileWorkExperienceMonths(onlineDir.getProfileWorkExperienceMonths());
                    requestedDir.setProfileWorkExperienceYears(onlineDir.getProfileWorkExperienceYears());
                    return i;
                }
            }

            return -1;
        }
    }
}