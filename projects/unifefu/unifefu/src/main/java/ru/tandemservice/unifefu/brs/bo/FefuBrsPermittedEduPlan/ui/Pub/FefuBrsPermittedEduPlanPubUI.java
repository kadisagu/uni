/*$Id$*/
package ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.FefuBrsPermittedEduPlanManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.ui.Add.FefuBrsPermittedEduPlanAdd;

/**
 * @author DMITRY KNYAZEV
 * @since 22.01.2015
 */
public class FefuBrsPermittedEduPlanPubUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(FefuBrsPermittedEduPlanPub.PERMITTED_WORK_PLAN_DS))
            dataSource.put(FefuBrsPermittedEduPlanPub.PARAM_YEAR_PART, getEduYearPart());
    }

    //HANDLERS
    public void onClickAddEduPlan()
    {
        getActivationBuilder().asRegion(FefuBrsPermittedEduPlanAdd.class).activate();
    }
    
    public void onDeleteEntityFromList()
    {
        Long listenerParameter = getListenerParameterAsLong();
        FefuBrsPermittedEduPlanManager.instance().dao().delete(listenerParameter);
    }

    //getters/setters
    @SuppressWarnings("UnusedDeclaration")
    public boolean isDisableCreation()
    {
        return FefuBrsPermittedEduPlanManager.instance().dao().isDisableCreation();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setDisableCreation(boolean disableCreation)
    {
        FefuBrsPermittedEduPlanManager.instance().dao().saveDisableCreation(disableCreation);
    }

    public EppYearPart getEduYearPart()
    {
        return getSettings().get("eduYearPart");
    }
}
