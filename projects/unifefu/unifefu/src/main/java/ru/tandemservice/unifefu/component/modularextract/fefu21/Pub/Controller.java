/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu21.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubController;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.ui.Edit.FefuExtractPrintFormEdit;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public class Controller extends ModularStudentExtractPubController<FefuExcludeStuDPOExtract, IDAO, Model>
{
    public void onClickEditPrintForm(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getExtract().isIndividual())
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(FefuExtractPrintFormEdit.class.getSimpleName(),
                                                                                                new ParametersMap().add("objectId", model.getExtract().getParagraph().getOrder().getId())));
    }

}