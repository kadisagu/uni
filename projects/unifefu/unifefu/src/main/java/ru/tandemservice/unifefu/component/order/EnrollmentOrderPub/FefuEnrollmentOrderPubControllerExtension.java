/* $Id$ */
package ru.tandemservice.unifefu.component.order.EnrollmentOrderPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniec.component.order.EnrollmentOrderPub.IEnrollmentOrderPubControllerExtension;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.base.bo.Directum.ui.EnrOrderLogView.DirectumEnrOrderLogView;
import ru.tandemservice.unifefu.base.bo.Directum.ui.EnrSend.DirectumEnrSend;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;

/**
 * @author Dmitry Seleznev
 * @since 05.08.2013
 */
public class FefuEnrollmentOrderPubControllerExtension implements IEnrollmentOrderPubControllerExtension
{
    @Override
    public void doSendToCoordinationAdditionalAction(IBusinessComponent component, EnrollmentOrder order)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на согласование, так как он уже на согласовании.");

        if (!errCollector.hasErrors())
        {
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(DirectumEnrSend.class.getSimpleName(), new ParametersMap()
                    .add("orderId", order.getId()))
            );
        }
    }

    @Override
    public void doExecuteAdditionalAction(IBusinessComponent component, EnrollmentOrder order)
    {
        component.getController().activateInRoot(component, new ComponentActivator(DirectumEnrOrderLogView.class.getSimpleName(), new ParametersMap()
                .add("orderId", order.getId()))
        );
    }
}