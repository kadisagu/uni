/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu16.ParagraphAddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOListExtract;

import java.util.Date;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public class Model extends AbstractListParagraphAddEditModel<FefuExcludeStuDPOListExtract>
{

    private StudentStatus _studentStatusNew;
    private IUploadFile _uploadFileOrder;
    private Date _excludeDate;
    private FefuAdditionalProfessionalEducationProgram _dpoProgram;
    private ISelectModel _dpoProgramModel;
    private boolean _canAddTemplate;

    public boolean isCanAddTemplate()
    {
        return _canAddTemplate;
    }

    public void setCanAddTemplate(boolean canAddTemplate)
    {
        _canAddTemplate = canAddTemplate;
    }

    public ISelectModel getDpoProgramModel()
    {
        return _dpoProgramModel;
    }

    public void setDpoProgramModel(ISelectModel dpoProgramModel)
    {
        _dpoProgramModel = dpoProgramModel;
    }

    public FefuAdditionalProfessionalEducationProgram getDpoProgram()
    {
        return _dpoProgram;
    }

    public void setDpoProgram(FefuAdditionalProfessionalEducationProgram dpoProgram)
    {
        _dpoProgram = dpoProgram;
    }

    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    public void setExcludeDate(Date excludeDate)
    {
        _excludeDate = excludeDate;
    }

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }

    public IUploadFile getUploadFileOrder()
    {
        return _uploadFileOrder;
    }

    public void setUploadFileOrder(IUploadFile uploadFileOrder)
    {
        _uploadFileOrder = uploadFileOrder;
    }

}