package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.TransitCompensationStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. О компенсации проезда
 */
public class TransitCompensationStuExtract extends TransitCompensationStuExtractGen
{
    public double getCompensationSumAsDouble()
    {
        return getCompensationSum() / 100D;
    }
}