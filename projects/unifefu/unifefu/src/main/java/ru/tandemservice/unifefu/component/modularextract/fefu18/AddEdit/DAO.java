/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu18.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.StuffCompensationStuExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2014
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<StuffCompensationStuExtract, Model> implements IDAO
{
	@Override
	protected GrammaCase getStudentTitleCase()
	{
		return GrammaCase.ACCUSATIVE;
	}

	@Override
	protected StuffCompensationStuExtract createNewInstance()
	{
		return new StuffCompensationStuExtract();
	}

	@Override
	public void prepare(final Model model)
	{
		super.prepare(model);
		model.setCompensationSum(model.getExtract().getCompensationSum() / 100D);
		model.setImmediateSum(model.getExtract().getImmediateSum() / 100D);
	}

	@Override
	public void update(Model model)
	{

		StuffCompensationStuExtract extract = model.getExtract();
		extract.setCompensationSum(new Double((model.getCompensationSum()*100D)).longValue());
		extract.setImmediateSum(new Double((model.getImmediateSum() * 100D)).longValue());
		extract.setResponsiblePersonFio(extract.getResponsiblePerson().getPerson().getFio());
		extract.setMatchingPersonFio(extract.getMatchingPerson().getPerson().getFio());
		super.update(model);
	}
}
