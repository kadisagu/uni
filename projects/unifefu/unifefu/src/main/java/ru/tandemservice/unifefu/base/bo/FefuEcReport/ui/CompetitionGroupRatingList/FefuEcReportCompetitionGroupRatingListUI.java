/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.CompetitionGroupRatingList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.CompetitionGroupRatingAdd.FefuEcReportCompetitionGroupRatingAdd;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;

/**
 * @author Nikolay Fedorovskih
 * @since 10.07.2013
 */
public class FefuEcReportCompetitionGroupRatingListUI extends UIPresenter
{
    private static final String ENROLLMENT_CAMPAIGN_FILTER = "enrollmentCampaign";

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuEcReportCompetitionGroupRatingList.REPORTS_DS.equals(dataSource.getName()))
            dataSource.put(FefuEcReportCompetitionGroupRatingList.ENROLLMENT_CAMPAIGN_PARAM, getEnrollmentCampaign());
    }

    @Override
    public void onComponentRefresh()
    {
        initDefaults();
    }

    public void onPrintReport()
    {
        getActivationBuilder().asRegion(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", getListenerParameterAsLong())
                .parameter("extension", "rtf")
                .activate();
    }

    public void onClickAdd()
    {
        getActivationBuilder().asRegion(FefuEcReportCompetitionGroupRatingAdd.class).activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    private void initDefaults()
    {
        if (getEnrollmentCampaign() == null)
        {
            getSettings().set(ENROLLMENT_CAMPAIGN_FILTER, UnifefuDaoFacade.getFefuEntrantDAO().getLastEnrollmentCampaign());
            saveSettings();
        }
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getSettings().get(ENROLLMENT_CAMPAIGN_FILTER);
    }

}