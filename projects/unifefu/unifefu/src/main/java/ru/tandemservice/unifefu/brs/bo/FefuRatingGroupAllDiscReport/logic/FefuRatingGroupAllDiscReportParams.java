package ru.tandemservice.unifefu.brs.bo.FefuRatingGroupAllDiscReport.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unifefu.brs.base.BaseFefuBrsReportParams;

import java.util.Date;
import java.util.List;

/**
 * @author amakarova
 * @since 12/4/13
 */
public class FefuRatingGroupAllDiscReportParams extends BaseFefuBrsReportParams
{
    private OrgUnit _formativeOrgUnit;
    private PpsEntry _pps;
    private List<Group> _groupList;
    private DataWrapper _onlyFilledJournals;
    private DataWrapper _groupBy;
    private DataWrapper _attestation;
    private Date _checkDate;
    private List<EppIControlActionType> _excludeActionTypes;

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public PpsEntry getPps()
    {
        return _pps;
    }

    public void setPps(PpsEntry pps)
    {
        _pps = pps;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public DataWrapper getOnlyFilledJournals()
    {
        return _onlyFilledJournals;
    }

    public void setOnlyFilledJournals(DataWrapper onlyFilledJournals)
    {
        _onlyFilledJournals = onlyFilledJournals;
    }

    public DataWrapper getAttestation()
    {
        return _attestation;
    }

    public void setAttestation(DataWrapper attestation)
    {
        _attestation = attestation;
    }

    public Date getCheckDate()
    {
        return _checkDate;
    }

    public void setCheckDate(Date checkDate)
    {
        _checkDate = checkDate;
    }

    public DataWrapper getGroupBy()
    {
        return _groupBy;
    }

    public void setGroupBy(DataWrapper groupBy)
    {
        _groupBy = groupBy;
    }

    public List<EppIControlActionType> getExcludeActionTypes()
    {
        return _excludeActionTypes;
    }

    public void setExcludeActionTypes(List<EppIControlActionType> excludeActionTypes)
    {
        _excludeActionTypes = excludeActionTypes;
    }
}
