/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.validator;

/**
 * Валидатор строки: проверяет, что длина строки не превышает заданное значение.
 * @author Alexander Zhebko
 * @since 12.09.2013
 */
public class StringLengthValidator extends Validator<String>
{
    private int maxLength;

    public StringLengthValidator(int maxLength)
    {
        this.maxLength = maxLength;
    }

    @Override
    public boolean validateValue(String value)
    {
        return value.length() <= maxLength;
    }

    @Override
    public String getInvalidMessage()
    {
        return "Длина строки превышает " + maxLength + " символов.";
    }
}