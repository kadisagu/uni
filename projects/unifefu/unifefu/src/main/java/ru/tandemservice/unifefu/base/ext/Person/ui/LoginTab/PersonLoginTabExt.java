/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Person.ui.LoginTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.Person.ui.LoginTab.PersonLoginTab;

/**
 * @author Dmitry Seleznev
 * @since 01.12.2014
 */
@Configuration
public class PersonLoginTabExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + PersonLoginTabExtUI.class.getSimpleName();

    @Autowired
    private PersonLoginTab _loginTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_loginTab.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, PersonLoginTabExtUI.class))
                .create();
    }
}