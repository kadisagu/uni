/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu18.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.StuffCompensationStuExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2014
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<StuffCompensationStuExtract, Model>
{
}
