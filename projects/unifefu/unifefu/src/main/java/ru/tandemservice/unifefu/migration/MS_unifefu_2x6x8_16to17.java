package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x8_16to17 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.getStatement().executeUpdate("delete from ffrgstryelmnt2edprgrmkndrl_t");

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuRegistryElement2EduProgramKindRel

		// удалено свойство educationOrgUnit
		{
			// удалить колонку
			tool.dropColumn("ffrgstryelmnt2edprgrmkndrl_t", "educationorgunit_id");

		}

		// создано обязательное свойство eduProgramSubject
		{
			// создать колонку
			tool.createColumn("ffrgstryelmnt2edprgrmkndrl_t", new DBColumn("eduprogramsubject_id", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultEduProgramSubject = null;
			tool.executeUpdate("update ffrgstryelmnt2edprgrmkndrl_t set eduprogramsubject_id=? where eduprogramsubject_id is null", defaultEduProgramSubject);

			// сделать колонку NOT NULL
			tool.setColumnNullable("ffrgstryelmnt2edprgrmkndrl_t", "eduprogramsubject_id", false);
		}
    }
}