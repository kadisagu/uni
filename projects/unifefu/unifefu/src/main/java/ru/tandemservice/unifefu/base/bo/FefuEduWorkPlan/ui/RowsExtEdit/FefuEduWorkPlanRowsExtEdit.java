/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.ui.RowsExtEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author nvankov
 * @since 1/21/14
 */
@Configuration
public class FefuEduWorkPlanRowsExtEdit extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}



    