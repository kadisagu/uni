/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20.utils;

import com.google.common.collect.ComparisonChain;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;

/**
 * @author Andrey Andreev
 * @since 21.01.2016
 */
public class FefuAdmittedToGIAListParagraphWrapper implements Comparable<FefuAdmittedToGIAListParagraphWrapper>
{
    private final StudentListParagraph _paragraph;
    private final CompensationType _compensationType;
    private final StudentCategory _studentCategory;
    private final ListStudentExtract _firstExtract;

    public FefuAdmittedToGIAListParagraphWrapper(FefuAdmittedToGIAListExtract extract)
    {
        _paragraph = (StudentListParagraph) extract.getParagraph();
        _compensationType = extract.getEntity().getCompensationType();
        _studentCategory = extract.getEntity().getStudentCategory();
        _firstExtract = extract;
    }

    public StudentListParagraph getParagraph()
    {
        return _paragraph;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public int compareTo(FefuAdmittedToGIAListParagraphWrapper o)
    {
        return ComparisonChain.start()
                .compare(_paragraph.getNumber(), o.getParagraph().getNumber())
                .compare(_studentCategory.getId(), o.getStudentCategory().getId())
                .compare(_compensationType.getId(), o.getCompensationType().getId())
                .result();
    }
}