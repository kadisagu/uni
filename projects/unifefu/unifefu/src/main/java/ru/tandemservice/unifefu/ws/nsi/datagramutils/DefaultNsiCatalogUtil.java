/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

/**
 * @author Dmitry Seleznev
 * @since 07.05.2014
 */
public class DefaultNsiCatalogUtil extends SimpleNsiCatalogUtil<INsiEntity, IEntity>
{
}