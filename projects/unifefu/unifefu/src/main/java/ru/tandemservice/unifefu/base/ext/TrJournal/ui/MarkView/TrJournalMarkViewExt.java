package ru.tandemservice.unifefu.base.ext.TrJournal.ui.MarkView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkView.TrJournalMarkView;

/**
 * @author amakarova
 *
 */
@Configuration
public class TrJournalMarkViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrJournalMarkViewExtUI.class.getSimpleName();

    @Autowired
    private TrJournalMarkView _trJournalMarkView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trJournalMarkView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrJournalMarkViewExtUI.class))
                .addAction(new TrJournalMarkViewClickShowInfoAction("onClickShowInfo"))
                .create();
    }
}
