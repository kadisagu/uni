/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.unifefu.base.vo.FefuEntrantOriginalsVerificationReportVO;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 7/17/13
 */
public class FefuEntrantOriginalsVerificationReportBuilder implements IFefuEcReportBuilder
{
    private FefuEntrantOriginalsVerificationReportVO reportVO;
    private Session session;
    private Map<Person, List<PersonBenefit>> personBenefitMap = new HashMap<>();

    //rtf template columns
    private final int regNumColumnNum = 0;
    private final int fioColumnNum = 1;
    private final int enrDirColumnNum = 2;
    private final int categoryColumnNum = 3;
    private final int commentColumnNum = 4;
    private final int districtColumnNum = 5;
    private final int benefitTypeColumnNum = 6;
    private final int phoneNumColumnNum = 7;
    private final int originalColumnNum = 8;
    private final int takeAwayDocColumnNum = 9;

    public FefuEntrantOriginalsVerificationReportBuilder(FefuEntrantOriginalsVerificationReportVO reportVO)
    {
        this.reportVO = reportVO;
    }

    @Override
    public DatabaseFile getContent(Session session)
    {
        this.session = session;
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "fefuEntrantOriginalsVerification");

        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());


        List<IRtfElement> elementList = document.getElementList();
        RtfTable tableTemplate = (RtfTable) UniRtfUtil.findElement(elementList, "T1");
        int tableIndex = elementList.indexOf(tableTemplate);
        elementList.remove(tableIndex);
        RtfTableModifier tableModifier = new RtfTableModifier();
        injectTables(tableTemplate, elementList, tableModifier, tableIndex, getData());

        return RtfUtil.toByteArray(document);
    }

    private void injectTables(RtfTable tableTemplate, List<IRtfElement> elementList, RtfTableModifier tableModifier, int startIndex, Map<Character, List<EntrantData>> map)
    {
        List<Character> characters = new ArrayList<>(map.keySet());
        Collections.sort(characters);
        for (Character character : characters)
        {
            RtfTable table = tableTemplate.getClone();
            elementList.add(startIndex++, table);
            RtfString param = new RtfString().append(character.toString());
            new RtfInjectModifier().put("PARAM", param).modify(Collections.<IRtfElement>singletonList(table));
            tableModifier.put("T1", new TableRtfRowIntercepter()).put("T1", getTable(map.get(character)));
            tableModifier.modify(elementList);
        }
    }

    private String[][] getTable(List<EntrantData> entrantDatas)
    {
        List<String[]> rows = Lists.newArrayList();
        Collections.sort(entrantDatas, new EntrantDataComparator());
        for (EntrantData data : entrantDatas)
        {
            MainRow mainRow = new MainRow(data);
            rows.add(mainRow.toStringArray());
        }
        if (rows.isEmpty())
            rows.add(new String[]{""});
        return rows.toArray(new String[][]{});
    }


    @SuppressWarnings("unchecked")
    private Map<Character, List<EntrantData>> getData()
    {
        Map<Character, List<EntrantData>> result = new LinkedHashMap<>();

        //Все выбранные направления приема
        Map<Entrant, List<RequestedEnrollmentDirection>> entrantListAllRDirMap = Maps.newHashMap();
        DQLSelectBuilder subBuilder = getRequestedEnrollmentDirectionsBuilder(false);
        subBuilder.column(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().id()));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "rd");
        builder.where(in(property("rd", RequestedEnrollmentDirection.entrantRequest().entrant().id()), subBuilder.buildQuery()));
        List<RequestedEnrollmentDirection> allDirs = builder.createStatement(session).list();
        for(RequestedEnrollmentDirection direction: allDirs)
        {
            Entrant entrant = direction.getEntrantRequest().getEntrant();
            if(!entrantListAllRDirMap.containsKey(entrant))
                entrantListAllRDirMap.put(entrant, Lists.<RequestedEnrollmentDirection>newArrayList());
            if(!entrantListAllRDirMap.get(entrant).contains(direction))
                entrantListAllRDirMap.get(entrant).add(direction);
        }
        //

        List<RequestedEnrollmentDirection> requestedEnrollmentDirections = getRequestedEnrollmentDirectionsBuilder(false).createStatement(session).list();

        Map<Entrant, List<RequestedEnrollmentDirection>> entrantListMap = Maps.newHashMap();
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections)
        {
            Entrant entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
            if (!entrantListMap.containsKey(entrant))
                entrantListMap.put(entrant, Lists.<RequestedEnrollmentDirection>newArrayList());

            if (!entrantListMap.get(entrant).contains(requestedEnrollmentDirection))
                entrantListMap.get(entrant).add(requestedEnrollmentDirection);
        }
        if(reportVO.isIncludeEntrantWithBenefit()) initPersonBenefitMap();
        for (Entrant entrant : entrantListMap.keySet())
        {
            List<RequestedEnrollmentDirection> directions = entrantListMap.get(entrant);
            Collections.sort(directions, new RequestedEnrollmentDirectionComparator());
            RequestedEnrollmentDirection direction = directions.get(0);
            String comment = "";
            if(!direction.isOriginalDocumentHandedIn())
            {
                List<RequestedEnrollmentDirection> allEntrantDirections = entrantListAllRDirMap.get(entrant);
                Collections.sort(allEntrantDirections, new RequestedEnrollmentDirectionComparator());
                for(RequestedEnrollmentDirection enrDir : allEntrantDirections)
                {
                    if(enrDir.isOriginalDocumentHandedIn())
                    {
                        comment = enrDir.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle();
                    }
                }
            }
            EntrantData entrantData = new EntrantData(direction, comment);

            Character character = entrantData.getEntrant().getFio().charAt(0);
            if (!result.containsKey(character))
                result.put(character, Lists.<EntrantData>newArrayList());
            if (!result.get(character).contains(entrantData))
                result.get(character).add(entrantData);
        }
        return result;
    }

    private void initPersonBenefitMap()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonBenefit.class, "pb");
        builder.where(in(property("pb", PersonBenefit.person().id()), getRequestedEnrollmentDirectionsBuilder(true).buildQuery()));
        List<PersonBenefit> benefits = builder.createStatement(session).list();
        for(PersonBenefit personBenefit : benefits)
        {
            Person person = personBenefit.getPerson();
            if(!personBenefitMap.containsKey(person))
                personBenefitMap.put(person, Lists.<PersonBenefit>newArrayList());
            if(!personBenefitMap.get(person).contains(personBenefit))
                personBenefitMap.get(person).add(personBenefit);
        }
    }

    private DQLSelectBuilder getRequestedEnrollmentDirectionsBuilder(boolean onlyPersonIds)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red");
        if(onlyPersonIds)
            builder.column(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().person().id()));
        builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().id()), value(reportVO.getEnrollmentCampaign().getId())));
        builder.where(eq(property("red", RequestedEnrollmentDirection.compensationType().id()), value(reportVO.getCompensationType().getId())));
        builder.where(betweenDays(RequestedEnrollmentDirection.regDate().fromAlias("red"), reportVO.getFrom(), reportVO.getTo()));
        if (!reportVO.getStudentCategoryList().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.studentCategory().id()), EntityUtils.getIdsFromEntityList(reportVO.getStudentCategoryList())));
        builder.where(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().archival()), value(false)));
        if (reportVO.isOnlyWithOriginals())
            builder.where(eq(property("red", RequestedEnrollmentDirection.originalDocumentHandedIn()), value(true)));
        if (!reportVO.isAllEnrollmentDirections())
        {
            if (null != reportVO.getEnrollmentDirection())
                builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().id()), value(reportVO.getEnrollmentDirection().getId())));
            else
            {
                builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().id()), value(reportVO.getFormativeOrgUnit().getId())));
                builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().territorialOrgUnit().id()), value(reportVO.getTerritorialOrgUnit().getId())));
                if (null != reportVO.getEducationLevelsHighSchool())
                    builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().id()), value(reportVO.getEducationLevelsHighSchool().getId())));
                if (null != reportVO.getDevelopForm())
                    builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().id()), value(reportVO.getDevelopForm().getId())));
                if (null != reportVO.getDevelopCondition())
                    builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().id()), value(reportVO.getDevelopCondition().getId())));
            }
        }
        if (null == reportVO.getEnrollmentDirection())
        {
            if (null != reportVO.getDevelopTech())
                builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developTech().id()), value(reportVO.getDevelopTech().getId())));
            if (null != reportVO.getDevelopPeriod())
                builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developPeriod().id()), value(reportVO.getDevelopPeriod().getId())));
        }
        if (reportVO.isIncludeForeignPerson())
            builder.where(not(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE))));
        else
            builder.where(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
        if (reportVO.isIncludeEntrantTargetAdmission())
            builder.where(eq(property("red", RequestedEnrollmentDirection.targetAdmission()), value(true)));
        if (reportVO.isIncludeEntrantWithBenefit())
            builder.where(exists(new DQLSelectBuilder().fromEntity(PersonBenefit.class, "b").where(eq(
                    property("red", RequestedEnrollmentDirection.entrantRequest().entrant().person().id()),
                    property("b", PersonBenefit.person().id()))).buildQuery()));
        return builder;
    }

    private class EntrantData
    {
        private Entrant _entrant;
        private RequestedEnrollmentDirection _requestedEnrollmentDirection;
        private String _phoneNumbers;
        private String _comment;
        private String _category;
        private String _district;
        private String _benefits;

        public EntrantData(RequestedEnrollmentDirection requestedEnrollmentDirection, String comment)
        {
            _requestedEnrollmentDirection = requestedEnrollmentDirection;
            _entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
            _comment = comment;
            _category = getCategoryForDir(_requestedEnrollmentDirection);
            if(null != _entrant.getPerson().getAddress())
                _phoneNumbers = _entrant.getPerson().getContactData().getMainPhones("\n");
            AddressBase regAddress = _entrant.getPerson().getAddressRegistration();
            if(null != regAddress && regAddress instanceof AddressDetailed)
            {
                if(null != ((AddressDetailed)regAddress).getSettlement())
                    _district = ((AddressDetailed)regAddress).getSettlement().getTitleWithType();
            }
            if(reportVO.isIncludeEntrantWithBenefit())
            {
                if(null != personBenefitMap.get(_entrant.getPerson()))
                    _benefits = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(personBenefitMap.get(_entrant.getPerson()), PersonBenefit.benefit().shortTitle()), ", ");
            }
        }

        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _requestedEnrollmentDirection;
        }

        public Entrant getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Entrant entrant)
        {
            _entrant = entrant;
        }

        public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            _requestedEnrollmentDirection = requestedEnrollmentDirection;
        }

        private String getPhoneNumbers()
        {
            return _phoneNumbers;
        }

        private void setPhoneNumbers(String phoneNumbers)
        {
            _phoneNumbers = phoneNumbers;
        }

        private String getComment()
        {
            return _comment;
        }

        private void setComment(String comment)
        {
            _comment = comment;
        }

        private String getCategory()
        {
            return _category;
        }

        private void setCategory(String category)
        {
            _category = category;
        }

        private String getDistrict()
        {
            return _district;
        }

        private void setDistrict(String district)
        {
            _district = district;
        }

        private String getBenefits()
        {
            return _benefits;
        }

        private void setBenefits(String benefits)
        {
            _benefits = benefits;
        }
    }

    private class MainRow
    {
        private EntrantData _entrantData;

        public MainRow(EntrantData entrantData)
        {
            _entrantData = entrantData;
        }

        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _entrantData.getRequestedEnrollmentDirection();
        }

        String[] toStringArray()
        {
            int columns = 10;
            int diff = 0;
            if (!reportVO.isIncludeEntrantTargetAdmission()) diff += 1;
            if (!reportVO.isIncludeEntrantWithBenefit()) diff += 2;
            int i = 0;
            String[] result = new String[columns];
            result[regNumColumnNum] = _entrantData.getEntrant().getPersonalNumber();
            result[fioColumnNum] = _entrantData.getEntrant().getPerson().getFullFio();
            result[enrDirColumnNum] = _entrantData.getRequestedEnrollmentDirection().getEnrollmentDirection().getPrintTitle();
            result[categoryColumnNum] = _entrantData.getCategory();
            result[commentColumnNum] = _entrantData.getComment();
            if(reportVO.isIncludeEntrantTargetAdmission())
            {
                result[districtColumnNum] = _entrantData.getDistrict();
            }
            if(reportVO.isIncludeEntrantWithBenefit())
            {
                result[benefitTypeColumnNum - diff] = _entrantData.getBenefits();
                result[phoneNumColumnNum - diff] = _entrantData.getPhoneNumbers();
            }
            result[originalColumnNum - diff] = _entrantData.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn() ? "п" : "к";
            result[takeAwayDocColumnNum - diff] = _entrantData.getRequestedEnrollmentDirection().getEntrantRequest().isTakeAwayDocument() ? "з/д" : "";
            return result;
        }
    }

    class EntrantDataComparator implements Comparator<EntrantData>
    {
        @Override
        public int compare(EntrantData o1, EntrantData o2)
        {
            if (reportVO.isOrderByOriginals())
            {
                if (o1.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn() && !o2.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn())
                    return -1;
                else if (!o1.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn() && o2.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn())
                    return 1;
                else return o1.getEntrant().getPersonalNumber().compareTo(o2.getEntrant().getPersonalNumber());
            }
            else
            {
                return o1.getEntrant().getPersonalNumber().compareTo(o2.getEntrant().getPersonalNumber());
            }
        }
    }
    class RequestedEnrollmentDirectionComparator implements Comparator<RequestedEnrollmentDirection>
    {

        @Override
        public int compare(RequestedEnrollmentDirection o1, RequestedEnrollmentDirection o2)
        {
            if(o1.getEntrantRequest().equals(o2.getEntrantRequest()))
            {
                return o1.getPriority() - o2.getPriority();
            }
            else
            {
                return o1.getEntrantRequest().getRegNumber() - o2.getEntrantRequest().getRegNumber();
            }
        }
    }
    class TableRtfRowIntercepter extends RtfRowIntercepterBase
    {
        @Override
        public void beforeModify(RtfTable table, int currentRowIndex)
        {
            List<RtfCell> headerCellList = table.getRowList().get(currentRowIndex - 1).getCellList();
            List<RtfCell> cellList = table.getRowList().get(currentRowIndex).getCellList();
            int freeWidth = 0;
            List<RtfCell> forHeaderDelete = new ArrayList<>();
            List<RtfCell> forDelete = new ArrayList<>();
            if (!reportVO.isIncludeEntrantWithBenefit())
            {
                forHeaderDelete.add(headerCellList.get(benefitTypeColumnNum));
                forDelete.add(cellList.get(benefitTypeColumnNum));
                freeWidth += cellList.get(benefitTypeColumnNum).getWidth();
                forHeaderDelete.add(headerCellList.get(phoneNumColumnNum));
                forDelete.add(cellList.get(phoneNumColumnNum));
                freeWidth += cellList.get(phoneNumColumnNum).getWidth();
            }
            if (!reportVO.isIncludeEntrantTargetAdmission())
            {
                forHeaderDelete.add(headerCellList.get(districtColumnNum));
                forDelete.add(cellList.get(districtColumnNum));
                freeWidth += cellList.get(districtColumnNum).getWidth();
            }
            headerCellList.removeAll(forHeaderDelete);
            cellList.removeAll(forDelete);
            headerCellList.get(fioColumnNum).setWidth(headerCellList.get(fioColumnNum).getWidth() + freeWidth / 2);
            cellList.get(fioColumnNum).setWidth(cellList.get(fioColumnNum).getWidth() + freeWidth / 2);
            headerCellList.get(enrDirColumnNum).setWidth(headerCellList.get(enrDirColumnNum).getWidth() + freeWidth / 2);
            cellList.get(enrDirColumnNum).setWidth(cellList.get(enrDirColumnNum).getWidth() + freeWidth / 2);
            SharedRtfUtil.setCellAlignment(cellList.get(fioColumnNum), IRtfData.QC, IRtfData.QL);
            SharedRtfUtil.setCellAlignment(cellList.get(enrDirColumnNum), IRtfData.QC, IRtfData.QL);
        }
    }

    protected String getCategoryForDir(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        if (reportVO.isIncludeEntrantTargetAdmission())
        {
            ExternalOrgUnit orgUnit = requestedEnrollmentDirection.getExternalOrgUnit();
            if (orgUnit != null)
            {
                if(!StringUtils.isEmpty(orgUnit.getShortTitle()))
                    return orgUnit.getShortTitle();
                else
                    return orgUnit.getTitle();
            }
            else
                return "";
        } else {
            CompetitionKind competitionKind = requestedEnrollmentDirection.getCompetitionKind();
            if (competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION)
                    || competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
                return competitionKind.getShortTitle();
            else
                return "";
        }
    }
}
