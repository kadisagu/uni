/* $Id$ */

package ru.tandemservice.unifefu.component.listextract.fefu12.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative.AbstractParagraphAddEditAlternativeDAO;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public class DAO extends AbstractParagraphAddEditAlternativeDAO<FefuStuffCompensationStuListExtract, Model> implements IDAO
{
	@Override
	public void prepare(Model model)
	{
		super.prepare(model);

		model.setCourseList(DevelopGridDAO.getCourseList());
		model.setCompensationTypeList(getCatalogItemList(CompensationType.class));

		model.setGroupListModel(new GroupSelectModel(model, model.getOrgUnit())
		{
			@Override
			protected boolean isNeedRequest()
			{
				return true;
			}
		}.extModel(model).showChildLevels(true));

		model.setEduLevelHighSchoolListModel(new ExtEducationLevelsHighSchoolSelectModel(null, null, model.getOrgUnit()));


		if (model.getParagraphId() == null)
		{
			model.setCompensationType(get(CompensationType.class, CompensationType.P_CODE, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
		}

		FefuStuffCompensationStuListExtract extract = model.getFirstExtract();

		model.setEmployeePostModel(new OrderExecutorSelectModel());
		if (extract != null)
		{
			model.setMatchingDate(extract.getMatchingDate());
			model.setProtocolNumber(extract.getProtocolNumber());
			model.setProtocolDate(extract.getProtocolDate());
			model.setResponsibleForAgreement(extract.getMatchingPerson());
			model.setResponsibleForPayments(extract.getResponsiblePerson());
		}
	}

	@Override
	protected void patchSearchDataSource(DQLSelectBuilder builder, Model model)
	{
		if (model.getGroup() != null)
		{
			builder.where(eq(property(STUDENT_ALIAS, Student.group()), value(model.getGroup())));
		}
		else
		{
			if (model.getCourse() != null)
			{
				builder.where(eq(property(STUDENT_ALIAS, Student.course()), value(model.getCourse())));
			}
			if (model.getEducationLevelsHighSchool() != null)
			{
				builder.where(eq(property(STUDENT_ALIAS, Student.group().educationOrgUnit().educationLevelHighSchool()), value(model.getEducationLevelsHighSchool())));
			}
		}

		if (model.getCompensationType() != null)
		{
			builder.where(eq(property(STUDENT_ALIAS, Student.compensationType()), value(model.getCompensationType())));
		}

		if (model.getStudentCustomStateCIs() != null && !model.getStudentCustomStateCIs().isEmpty())
		{
			DQLSelectBuilder customState = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "cs")
					.column(property("cs", StudentCustomState.student()));
			customState.where(in(property("cs", StudentCustomState.customState()), model.getStudentCustomStateCIs()));

			List<Student> stud = customState.createStatement(getSession()).list();
			builder.where(in(property(STUDENT_ALIAS, Student.id()), stud));
		}
	}

	@Override
	protected FefuStuffCompensationStuListExtract createNewInstance(Model model)
	{
		return new FefuStuffCompensationStuListExtract();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void fillExtract(FefuStuffCompensationStuListExtract extract, Student student, Model model)
	{
		IValueMapHolder compensationSizeHolder = (IValueMapHolder) model.getDataSource().getColumn(Model.COMPENSATION_COLUMN_NAME);
		IValueMapHolder immediateSizeHolder = (IValueMapHolder) model.getDataSource().getColumn(Model.IMMEDIATE_COLUMN_NAME);
		Map<Long, Object> compensationSizeMap = (null == compensationSizeHolder ? Collections.emptyMap() : compensationSizeHolder.getValueMap());
		Map<Long, Object> immediateSizeMap = (null == immediateSizeHolder ? Collections.emptyMap() : immediateSizeHolder.getValueMap());

		Number compensationSize = (Number) compensationSizeMap.get(student.getId());
		Number immediateSize = (Number) immediateSizeMap.get(student.getId());

		extract.setCourse(student.getCourse());
		extract.setGroup(student.getGroup());
		extract.setCompensationType(student.getCompensationType());
		extract.setImmediateSumFromDouble(immediateSize.doubleValue());
		extract.setCompensationSumFromDouble(compensationSize.doubleValue());
		extract.setMatchingDate(model.getMatchingDate());
		extract.setProtocolDate(model.getProtocolDate());
		extract.setProtocolNumber(model.getProtocolNumber());
		extract.setMatchingPerson(model.getResponsibleForAgreement());
		extract.setMatchingPersonFio(model.getResponsibleForAgreement().getFio());
		extract.setResponsiblePerson(model.getResponsibleForPayments());
		extract.setResponsiblePersonFio(model.getResponsibleForPayments().getFio());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void update(Model model)
	{
		ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

		IValueMapHolder compensationSizeHolder = (IValueMapHolder) model.getDataSource().getColumn(Model.COMPENSATION_COLUMN_NAME);
		IValueMapHolder immediateSizeHolder = (IValueMapHolder) model.getDataSource().getColumn(Model.IMMEDIATE_COLUMN_NAME);
		Map<Long, Double> compensationSizeMap = (null == compensationSizeHolder ? Collections.emptyMap() : compensationSizeHolder.getValueMap());
		Map<Long, Double> immediateSizeMap = (null == immediateSizeHolder ? Collections.emptyMap() : immediateSizeHolder.getValueMap());

		List<ViewWrapper<Student>> viewlist = new ArrayList<ViewWrapper<Student>>((Collection) model.getDataSource().getEntityList());

		for (ViewWrapper<Student> wrapper : viewlist)
		{
			if (compensationSizeMap.get(wrapper.getEntity().getId()) == null)
				errorCollector.add("Поле «Компенсация» обязательно для заполнения.", "compensationSizeId_" + wrapper.getEntity().getId());
			if (immediateSizeMap.get(wrapper.getEntity().getId()) == null)
				errorCollector.add("Поле «Пособие» обязательно для заполнения.", "immediateSizeId_" + wrapper.getEntity().getId());
		}

		if (errorCollector.hasErrors())
			return;

		super.update(model);
	}
}