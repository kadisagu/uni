/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu9.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuSocGrantResumptionStuExtract, Model>
{
}