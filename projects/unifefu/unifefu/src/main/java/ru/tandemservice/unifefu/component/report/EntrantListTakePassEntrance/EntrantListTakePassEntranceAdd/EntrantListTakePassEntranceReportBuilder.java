package ru.tandemservice.unifefu.component.report.EntrantListTakePassEntrance.EntrantListTakePassEntranceAdd;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uniec.component.report.EntrantListTakePassEntrance.EntrantListTakePassEntranceAdd.Model;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;

import java.util.List;

/**
 * @author amakarova
 * @since 22.05.2013
 */
@Zlo
class EntrantListTakePassEntranceReportBuilder extends ru.tandemservice.uniec.component.report.EntrantListTakePassEntrance.EntrantListTakePassEntranceAdd.EntrantListTakePassEntranceReportBuilder
{
    public EntrantListTakePassEntranceReportBuilder(Model model, Session session) {
        super(model, session);
    }

    /**
     * Для кастомизации в проектах
     * @return Рег. №
     */
    @Override
    public String getRegNumber(List<EntrantRequest> entrantRequestList, Entrant entrant) {
        return entrant.getPersonalNumber();
    }
}
