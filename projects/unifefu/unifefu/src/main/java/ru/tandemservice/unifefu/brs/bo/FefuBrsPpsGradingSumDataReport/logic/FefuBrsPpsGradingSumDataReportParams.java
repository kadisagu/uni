/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unifefu.brs.base.BaseFefuBrsReportParams;

import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 12/16/13
 */
public class FefuBrsPpsGradingSumDataReportParams extends BaseFefuBrsReportParams
{
    private List<OrgUnit> _formativeOrgUnit;
    private List<OrgUnit> _tutorOrgUnit;
    private Date _checkDate;
    private DataWrapper _cutReport;

    public List<OrgUnit> getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(List<OrgUnit> formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public List<OrgUnit> getTutorOrgUnit()
    {
        return _tutorOrgUnit;
    }

    public void setTutorOrgUnit(List<OrgUnit> tutorOrgUnit)
    {
        _tutorOrgUnit = tutorOrgUnit;
    }

    public Date getCheckDate()
    {
        return _checkDate;
    }

    public void setCheckDate(Date checkDate)
    {
        _checkDate = checkDate;
    }

    public DataWrapper getCutReport()
    {
        return _cutReport;
    }

    public void setCutReport(DataWrapper cutReport)
    {
        _cutReport = cutReport;
    }
}
