package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unifefu.entity.FefuSendingRatingPackage;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пакет отправки рейтингов на портал ДВФУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuSendingRatingPackageGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuSendingRatingPackage";
    public static final String ENTITY_NAME = "fefuSendingRatingPackage";
    public static final int VERSION_HASH = -1358856553;
    private static IEntityMeta ENTITY_META;

    public static final String P_SENDING_TIME = "sendingTime";
    public static final String P_SENDING_DATA = "sendingData";
    public static final String P_EXTERNAL_ID = "externalId";
    public static final String P_SENDING_RESULT = "sendingResult";

    private Date _sendingTime;     // Время отправки
    private String _sendingData;     // Отправляемые данные
    private String _externalId;     // Внешний ID
    private String _sendingResult;     // Результат отправки сообщения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Время отправки. Свойство не может быть null.
     */
    @NotNull
    public Date getSendingTime()
    {
        return _sendingTime;
    }

    /**
     * @param sendingTime Время отправки. Свойство не может быть null.
     */
    public void setSendingTime(Date sendingTime)
    {
        dirty(_sendingTime, sendingTime);
        _sendingTime = sendingTime;
    }

    /**
     * @return Отправляемые данные. Свойство не может быть null.
     */
    @NotNull
    public String getSendingData()
    {
        initLazyForGet("sendingData");
        return _sendingData;
    }

    /**
     * @param sendingData Отправляемые данные. Свойство не может быть null.
     */
    public void setSendingData(String sendingData)
    {
        initLazyForSet("sendingData");
        dirty(_sendingData, sendingData);
        _sendingData = sendingData;
    }

    /**
     * @return Внешний ID.
     */
    @Length(max=255)
    public String getExternalId()
    {
        return _externalId;
    }

    /**
     * @param externalId Внешний ID.
     */
    public void setExternalId(String externalId)
    {
        dirty(_externalId, externalId);
        _externalId = externalId;
    }

    /**
     * @return Результат отправки сообщения. Свойство не может быть null.
     */
    @NotNull
    public String getSendingResult()
    {
        return _sendingResult;
    }

    /**
     * @param sendingResult Результат отправки сообщения. Свойство не может быть null.
     */
    public void setSendingResult(String sendingResult)
    {
        dirty(_sendingResult, sendingResult);
        _sendingResult = sendingResult;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuSendingRatingPackageGen)
        {
            setSendingTime(((FefuSendingRatingPackage)another).getSendingTime());
            setSendingData(((FefuSendingRatingPackage)another).getSendingData());
            setExternalId(((FefuSendingRatingPackage)another).getExternalId());
            setSendingResult(((FefuSendingRatingPackage)another).getSendingResult());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuSendingRatingPackageGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuSendingRatingPackage.class;
        }

        public T newInstance()
        {
            return (T) new FefuSendingRatingPackage();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sendingTime":
                    return obj.getSendingTime();
                case "sendingData":
                    return obj.getSendingData();
                case "externalId":
                    return obj.getExternalId();
                case "sendingResult":
                    return obj.getSendingResult();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sendingTime":
                    obj.setSendingTime((Date) value);
                    return;
                case "sendingData":
                    obj.setSendingData((String) value);
                    return;
                case "externalId":
                    obj.setExternalId((String) value);
                    return;
                case "sendingResult":
                    obj.setSendingResult((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sendingTime":
                        return true;
                case "sendingData":
                        return true;
                case "externalId":
                        return true;
                case "sendingResult":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sendingTime":
                    return true;
                case "sendingData":
                    return true;
                case "externalId":
                    return true;
                case "sendingResult":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sendingTime":
                    return Date.class;
                case "sendingData":
                    return String.class;
                case "externalId":
                    return String.class;
                case "sendingResult":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuSendingRatingPackage> _dslPath = new Path<FefuSendingRatingPackage>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuSendingRatingPackage");
    }
            

    /**
     * @return Время отправки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSendingRatingPackage#getSendingTime()
     */
    public static PropertyPath<Date> sendingTime()
    {
        return _dslPath.sendingTime();
    }

    /**
     * @return Отправляемые данные. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSendingRatingPackage#getSendingData()
     */
    public static PropertyPath<String> sendingData()
    {
        return _dslPath.sendingData();
    }

    /**
     * @return Внешний ID.
     * @see ru.tandemservice.unifefu.entity.FefuSendingRatingPackage#getExternalId()
     */
    public static PropertyPath<String> externalId()
    {
        return _dslPath.externalId();
    }

    /**
     * @return Результат отправки сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSendingRatingPackage#getSendingResult()
     */
    public static PropertyPath<String> sendingResult()
    {
        return _dslPath.sendingResult();
    }

    public static class Path<E extends FefuSendingRatingPackage> extends EntityPath<E>
    {
        private PropertyPath<Date> _sendingTime;
        private PropertyPath<String> _sendingData;
        private PropertyPath<String> _externalId;
        private PropertyPath<String> _sendingResult;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Время отправки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSendingRatingPackage#getSendingTime()
     */
        public PropertyPath<Date> sendingTime()
        {
            if(_sendingTime == null )
                _sendingTime = new PropertyPath<Date>(FefuSendingRatingPackageGen.P_SENDING_TIME, this);
            return _sendingTime;
        }

    /**
     * @return Отправляемые данные. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSendingRatingPackage#getSendingData()
     */
        public PropertyPath<String> sendingData()
        {
            if(_sendingData == null )
                _sendingData = new PropertyPath<String>(FefuSendingRatingPackageGen.P_SENDING_DATA, this);
            return _sendingData;
        }

    /**
     * @return Внешний ID.
     * @see ru.tandemservice.unifefu.entity.FefuSendingRatingPackage#getExternalId()
     */
        public PropertyPath<String> externalId()
        {
            if(_externalId == null )
                _externalId = new PropertyPath<String>(FefuSendingRatingPackageGen.P_EXTERNAL_ID, this);
            return _externalId;
        }

    /**
     * @return Результат отправки сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSendingRatingPackage#getSendingResult()
     */
        public PropertyPath<String> sendingResult()
        {
            if(_sendingResult == null )
                _sendingResult = new PropertyPath<String>(FefuSendingRatingPackageGen.P_SENDING_RESULT, this);
            return _sendingResult;
        }

        public Class getEntityClass()
        {
            return FefuSendingRatingPackage.class;
        }

        public String getEntityName()
        {
            return "fefuSendingRatingPackage";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
