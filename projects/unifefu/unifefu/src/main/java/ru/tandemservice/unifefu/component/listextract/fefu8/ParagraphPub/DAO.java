/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu8.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 23.10.2013
 */
public class DAO extends AbstractListParagraphPubDAO<FefuHolidayStuListExtract, Model> implements IDAO
{
}
