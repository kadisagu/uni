package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. «О переводе слушателей ДПО/ДО»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuTransfStuDPOExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract";
    public static final String ENTITY_NAME = "fefuTransfStuDPOExtract";
    public static final int VERSION_HASH = -1837867985;
    private static IEntityMeta ENTITY_META;

    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String P_INDIVIDUAL_PLAN = "individualPlan";
    public static final String L_DPO_PROGRAM_OLD = "dpoProgramOld";
    public static final String L_DPO_PROGRAM_NEW = "dpoProgramNew";
    public static final String L_COURSE_OLD = "courseOld";
    public static final String L_COURSE_NEW = "courseNew";

    private Date _transferDate;     // Дата перевода
    private boolean _individualPlan;     // Индивидуальный план
    private FefuAdditionalProfessionalEducationProgram _dpoProgramOld;     // Предыдущая программа ДПО/ДО
    private FefuAdditionalProfessionalEducationProgram _dpoProgramNew;     // Новая программа ДПО/ДО
    private Course _courseOld;     // Старый курс
    private Course _courseNew;     // Новый курс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата перевода. Свойство не может быть null.
     */
    @NotNull
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата перевода. Свойство не может быть null.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return Индивидуальный план. Свойство не может быть null.
     */
    @NotNull
    public boolean isIndividualPlan()
    {
        return _individualPlan;
    }

    /**
     * @param individualPlan Индивидуальный план. Свойство не может быть null.
     */
    public void setIndividualPlan(boolean individualPlan)
    {
        dirty(_individualPlan, individualPlan);
        _individualPlan = individualPlan;
    }

    /**
     * @return Предыдущая программа ДПО/ДО.
     */
    public FefuAdditionalProfessionalEducationProgram getDpoProgramOld()
    {
        return _dpoProgramOld;
    }

    /**
     * @param dpoProgramOld Предыдущая программа ДПО/ДО.
     */
    public void setDpoProgramOld(FefuAdditionalProfessionalEducationProgram dpoProgramOld)
    {
        dirty(_dpoProgramOld, dpoProgramOld);
        _dpoProgramOld = dpoProgramOld;
    }

    /**
     * @return Новая программа ДПО/ДО. Свойство не может быть null.
     */
    @NotNull
    public FefuAdditionalProfessionalEducationProgram getDpoProgramNew()
    {
        return _dpoProgramNew;
    }

    /**
     * @param dpoProgramNew Новая программа ДПО/ДО. Свойство не может быть null.
     */
    public void setDpoProgramNew(FefuAdditionalProfessionalEducationProgram dpoProgramNew)
    {
        dirty(_dpoProgramNew, dpoProgramNew);
        _dpoProgramNew = dpoProgramNew;
    }

    /**
     * @return Старый курс.
     */
    public Course getCourseOld()
    {
        return _courseOld;
    }

    /**
     * @param courseOld Старый курс.
     */
    public void setCourseOld(Course courseOld)
    {
        dirty(_courseOld, courseOld);
        _courseOld = courseOld;
    }

    /**
     * @return Новый курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourseNew()
    {
        return _courseNew;
    }

    /**
     * @param courseNew Новый курс. Свойство не может быть null.
     */
    public void setCourseNew(Course courseNew)
    {
        dirty(_courseNew, courseNew);
        _courseNew = courseNew;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuTransfStuDPOExtractGen)
        {
            setTransferDate(((FefuTransfStuDPOExtract)another).getTransferDate());
            setIndividualPlan(((FefuTransfStuDPOExtract)another).isIndividualPlan());
            setDpoProgramOld(((FefuTransfStuDPOExtract)another).getDpoProgramOld());
            setDpoProgramNew(((FefuTransfStuDPOExtract)another).getDpoProgramNew());
            setCourseOld(((FefuTransfStuDPOExtract)another).getCourseOld());
            setCourseNew(((FefuTransfStuDPOExtract)another).getCourseNew());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuTransfStuDPOExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuTransfStuDPOExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuTransfStuDPOExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return obj.getTransferDate();
                case "individualPlan":
                    return obj.isIndividualPlan();
                case "dpoProgramOld":
                    return obj.getDpoProgramOld();
                case "dpoProgramNew":
                    return obj.getDpoProgramNew();
                case "courseOld":
                    return obj.getCourseOld();
                case "courseNew":
                    return obj.getCourseNew();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "individualPlan":
                    obj.setIndividualPlan((Boolean) value);
                    return;
                case "dpoProgramOld":
                    obj.setDpoProgramOld((FefuAdditionalProfessionalEducationProgram) value);
                    return;
                case "dpoProgramNew":
                    obj.setDpoProgramNew((FefuAdditionalProfessionalEducationProgram) value);
                    return;
                case "courseOld":
                    obj.setCourseOld((Course) value);
                    return;
                case "courseNew":
                    obj.setCourseNew((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                        return true;
                case "individualPlan":
                        return true;
                case "dpoProgramOld":
                        return true;
                case "dpoProgramNew":
                        return true;
                case "courseOld":
                        return true;
                case "courseNew":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return true;
                case "individualPlan":
                    return true;
                case "dpoProgramOld":
                    return true;
                case "dpoProgramNew":
                    return true;
                case "courseOld":
                    return true;
                case "courseNew":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "transferDate":
                    return Date.class;
                case "individualPlan":
                    return Boolean.class;
                case "dpoProgramOld":
                    return FefuAdditionalProfessionalEducationProgram.class;
                case "dpoProgramNew":
                    return FefuAdditionalProfessionalEducationProgram.class;
                case "courseOld":
                    return Course.class;
                case "courseNew":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuTransfStuDPOExtract> _dslPath = new Path<FefuTransfStuDPOExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuTransfStuDPOExtract");
    }
            

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return Индивидуальный план. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#isIndividualPlan()
     */
    public static PropertyPath<Boolean> individualPlan()
    {
        return _dslPath.individualPlan();
    }

    /**
     * @return Предыдущая программа ДПО/ДО.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#getDpoProgramOld()
     */
    public static FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> dpoProgramOld()
    {
        return _dslPath.dpoProgramOld();
    }

    /**
     * @return Новая программа ДПО/ДО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#getDpoProgramNew()
     */
    public static FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> dpoProgramNew()
    {
        return _dslPath.dpoProgramNew();
    }

    /**
     * @return Старый курс.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#getCourseOld()
     */
    public static Course.Path<Course> courseOld()
    {
        return _dslPath.courseOld();
    }

    /**
     * @return Новый курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#getCourseNew()
     */
    public static Course.Path<Course> courseNew()
    {
        return _dslPath.courseNew();
    }

    public static class Path<E extends FefuTransfStuDPOExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _transferDate;
        private PropertyPath<Boolean> _individualPlan;
        private FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> _dpoProgramOld;
        private FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> _dpoProgramNew;
        private Course.Path<Course> _courseOld;
        private Course.Path<Course> _courseNew;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(FefuTransfStuDPOExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return Индивидуальный план. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#isIndividualPlan()
     */
        public PropertyPath<Boolean> individualPlan()
        {
            if(_individualPlan == null )
                _individualPlan = new PropertyPath<Boolean>(FefuTransfStuDPOExtractGen.P_INDIVIDUAL_PLAN, this);
            return _individualPlan;
        }

    /**
     * @return Предыдущая программа ДПО/ДО.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#getDpoProgramOld()
     */
        public FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> dpoProgramOld()
        {
            if(_dpoProgramOld == null )
                _dpoProgramOld = new FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram>(L_DPO_PROGRAM_OLD, this);
            return _dpoProgramOld;
        }

    /**
     * @return Новая программа ДПО/ДО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#getDpoProgramNew()
     */
        public FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> dpoProgramNew()
        {
            if(_dpoProgramNew == null )
                _dpoProgramNew = new FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram>(L_DPO_PROGRAM_NEW, this);
            return _dpoProgramNew;
        }

    /**
     * @return Старый курс.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#getCourseOld()
     */
        public Course.Path<Course> courseOld()
        {
            if(_courseOld == null )
                _courseOld = new Course.Path<Course>(L_COURSE_OLD, this);
            return _courseOld;
        }

    /**
     * @return Новый курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract#getCourseNew()
     */
        public Course.Path<Course> courseNew()
        {
            if(_courseNew == null )
                _courseNew = new Course.Path<Course>(L_COURSE_NEW, this);
            return _courseNew;
        }

        public Class getEntityClass()
        {
            return FefuTransfStuDPOExtract.class;
        }

        public String getEntityName()
        {
            return "fefuTransfStuDPOExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
