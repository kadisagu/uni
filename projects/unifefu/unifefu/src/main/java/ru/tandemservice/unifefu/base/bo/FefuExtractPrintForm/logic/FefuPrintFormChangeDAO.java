/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.logic;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public class FefuPrintFormChangeDAO extends UniBaseDao implements IFefuPrintFormChangeDAO
{


    public void saveOrUpdatePrintForm(AbstractStudentOrder order, IUploadFile uploadFile)
    {
        if (uploadFile == null) return;
        if (uploadFile.getSize() > 5 * 1024 * 1024)
            throw new ApplicationException("Размер загружаемого файла не должен превышать 5Mb.");
        FefuOrderToPrintFormRelation printForm = get(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.order(), order);
        try
        {
            byte[] content = IOUtils.toByteArray(uploadFile.getStream());
            if ((content == null) || (content.length == 0)) content = new byte[]{};

            if (null != printForm)
            {
                printForm.setFileType(CommonBaseUtil.getContentType(uploadFile));
                printForm.setFileName(uploadFile.getFileName());
                printForm.setContent(content);

            }
            else
            {
                printForm = new FefuOrderToPrintFormRelation();
                printForm.setOrder(order);
                printForm.setFileType(CommonBaseUtil.getContentType(uploadFile));
                printForm.setFileName(uploadFile.getFileName());
                printForm.setContent(content);

            }
            saveOrUpdate(printForm);
        }
        catch (final Throwable e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void deletePrintForm(AbstractStudentOrder order)
    {
        new DQLDeleteBuilder(FefuOrderToPrintFormRelation.class)
                .where(eq(property(FefuOrderToPrintFormRelation.order()), value(order)))
                .createStatement(getSession()).execute();
    }

}