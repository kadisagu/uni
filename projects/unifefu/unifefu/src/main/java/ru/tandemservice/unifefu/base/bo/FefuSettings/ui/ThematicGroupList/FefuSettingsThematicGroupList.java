/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.ThematicGroupList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unifefu.base.bo.FefuSettings.logic.FefuThemGroup2ExtTypesDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuSettings.logic.FefuThematicGroupsDSHandler;
import ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup;

/**
 * @author nvankov
 * @since 11/1/13
 */
@Configuration
public class FefuSettingsThematicGroupList extends BusinessComponentManager
{
    public final static String ORDER_TYPES_DS = "orderTypesDS";

    @Bean
    public ColumnListExtPoint orderTypesCL()
    {
        return columnListExtPointBuilder(ORDER_TYPES_DS)
                .addColumn(textColumn("title", FefuStudentExtractType2ThematicGroup.type().title()).required(true))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                        alert("ui.delete.alert", FefuStudentExtractType2ThematicGroup.type().title())))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ORDER_TYPES_DS, orderTypesCL(), orderTypesDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler orderTypesDSHandler()
    {
        return new FefuThemGroup2ExtTypesDSHandler(getName());
    }
}
