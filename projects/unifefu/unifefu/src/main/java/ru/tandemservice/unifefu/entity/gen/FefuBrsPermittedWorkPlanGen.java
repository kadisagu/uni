package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.unifefu.entity.FefuBrsPermittedWorkPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Разрешенные учебные планы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuBrsPermittedWorkPlanGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuBrsPermittedWorkPlan";
    public static final String ENTITY_NAME = "fefuBrsPermittedWorkPlan";
    public static final int VERSION_HASH = 1381497251;
    private static IEntityMeta ENTITY_META;

    public static final String L_WORK_PLAN = "workPlan";

    private EppWorkPlan _workPlan;     // Разрешенный рабочий учебный план

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Разрешенный рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppWorkPlan getWorkPlan()
    {
        return _workPlan;
    }

    /**
     * @param workPlan Разрешенный рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     */
    public void setWorkPlan(EppWorkPlan workPlan)
    {
        dirty(_workPlan, workPlan);
        _workPlan = workPlan;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuBrsPermittedWorkPlanGen)
        {
            setWorkPlan(((FefuBrsPermittedWorkPlan)another).getWorkPlan());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuBrsPermittedWorkPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuBrsPermittedWorkPlan.class;
        }

        public T newInstance()
        {
            return (T) new FefuBrsPermittedWorkPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "workPlan":
                    return obj.getWorkPlan();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "workPlan":
                    obj.setWorkPlan((EppWorkPlan) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "workPlan":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "workPlan":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "workPlan":
                    return EppWorkPlan.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuBrsPermittedWorkPlan> _dslPath = new Path<FefuBrsPermittedWorkPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuBrsPermittedWorkPlan");
    }
            

    /**
     * @return Разрешенный рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuBrsPermittedWorkPlan#getWorkPlan()
     */
    public static EppWorkPlan.Path<EppWorkPlan> workPlan()
    {
        return _dslPath.workPlan();
    }

    public static class Path<E extends FefuBrsPermittedWorkPlan> extends EntityPath<E>
    {
        private EppWorkPlan.Path<EppWorkPlan> _workPlan;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Разрешенный рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuBrsPermittedWorkPlan#getWorkPlan()
     */
        public EppWorkPlan.Path<EppWorkPlan> workPlan()
        {
            if(_workPlan == null )
                _workPlan = new EppWorkPlan.Path<EppWorkPlan>(L_WORK_PLAN, this);
            return _workPlan;
        }

        public Class getEntityClass()
        {
            return FefuBrsPermittedWorkPlan.class;
        }

        public String getEntityName()
        {
            return "fefuBrsPermittedWorkPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
