/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import ru.tandemservice.movestudent.component.modularextract.StudentModularOrderPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 02.02.2015
 */
public class FefuStudentModularOrderPrint extends StudentModularOrderPrint
{
    private static List<Short> getFefuDPODiscriminators()
    {
        final List<Short> dpoEntityCodes = new ArrayList<>();
        for (IEntityMeta clazz : IFefuDPOExtract.classes.get())
        {
            dpoEntityCodes.add(clazz.getEntityCode());
        }
        return dpoEntityCodes;
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, final StudentModularOrder order)
    {
        if (MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual(order))
        {

            final byte[] content = IUniBaseDao.instance.get().getCalculatedValue(session -> {
                // Для индивидуальных приказов ДПО, имплементирующих интерфейс IFefuDPOExtract, ищем печатную форму приказа, загруженную пользователем,
                return new DQLSelectBuilder().fromEntity(FefuOrderToPrintFormRelation.class, "rel")
                        .column(property("rel", FefuOrderToPrintFormRelation.content()))
                        .where(eq(property("rel", FefuOrderToPrintFormRelation.order()), value(order)))
                        .where(exists(
                                AbstractStudentExtract.class,
                                AbstractStudentExtract.paragraph().order().s(), order,
                                "class", getFefuDPODiscriminators()
                        ))
                        .createStatement(session).uniqueResult();
            });

            //если форма обнаружена - выводим ее на печать
            if (content != null)
            {
                return new RtfReader().read(content);
            }
        }

        return super.createPrintForm(template, order);
    }
}