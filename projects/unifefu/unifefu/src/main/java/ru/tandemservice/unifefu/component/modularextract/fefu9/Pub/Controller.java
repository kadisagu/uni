/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu9.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubController;
import ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class Controller extends ModularStudentExtractPubController<FefuSocGrantResumptionStuExtract, IDAO, Model>
{
}