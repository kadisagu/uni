/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSchedule.logic;

import ru.tandemservice.unispp.base.bo.SppSchedule.logic.ISppScheduleEventsDAO;

import java.util.List;

/**
 * @author nvankov
 * @since 9/11/13
 */
public interface IFefuScheduleEventsDAO extends ISppScheduleEventsDAO
{
    List<String> getCurrentNsiIds(Long groupId);
}
