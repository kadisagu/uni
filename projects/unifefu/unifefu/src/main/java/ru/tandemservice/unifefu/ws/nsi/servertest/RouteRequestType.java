/**
 * RouteRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.nsi.servertest;

public class RouteRequestType  implements java.io.Serializable {
    private RoutingHeaderType routingHeader;

    private RouteRequestTypeIdentifiers identifiers;

    public RouteRequestType() {
    }

    public RouteRequestType(
           RoutingHeaderType routingHeader,
           RouteRequestTypeIdentifiers identifiers) {
           this.routingHeader = routingHeader;
           this.identifiers = identifiers;
    }


    /**
     * Gets the routingHeader value for this RouteRequestType.
     * 
     * @return routingHeader
     */
    public RoutingHeaderType getRoutingHeader() {
        return routingHeader;
    }


    /**
     * Sets the routingHeader value for this RouteRequestType.
     * 
     * @param routingHeader
     */
    public void setRoutingHeader(RoutingHeaderType routingHeader) {
        this.routingHeader = routingHeader;
    }


    /**
     * Gets the identifiers value for this RouteRequestType.
     * 
     * @return identifiers
     */
    public RouteRequestTypeIdentifiers getIdentifiers() {
        return identifiers;
    }


    /**
     * Sets the identifiers value for this RouteRequestType.
     * 
     * @param identifiers
     */
    public void setIdentifiers(RouteRequestTypeIdentifiers identifiers) {
        this.identifiers = identifiers;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RouteRequestType)) return false;
        RouteRequestType other = (RouteRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.routingHeader==null && other.getRoutingHeader()==null) || 
             (this.routingHeader!=null &&
              this.routingHeader.equals(other.getRoutingHeader()))) &&
            ((this.identifiers==null && other.getIdentifiers()==null) || 
             (this.identifiers!=null &&
              this.identifiers.equals(other.getIdentifiers())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRoutingHeader() != null) {
            _hashCode += getRoutingHeader().hashCode();
        }
        if (getIdentifiers() != null) {
            _hashCode += getIdentifiers().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RouteRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "RouteRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("routingHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "routingHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "RoutingHeaderType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identifiers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "Identifiers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", ">RouteRequestType>Identifiers"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
