/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;

import ru.tandemservice.unifefu.base.bo.FefuRegistry.FefuRegistryManager;

/**
 * @author nvankov
 * @since 2/7/14
 */
public class FefuModuleLoadDSHandler extends DefaultSearchDataSourceHandler
{
    public FefuModuleLoadDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long moduleId = context.getNotNull("moduleId");
        return ListOutputBuilder.get(input, FefuRegistryManager.instance().dao().moduleLoadListReadOnly(moduleId)).pageable(false).build();
    }
}
