/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuPortalNotification;
import ru.tandemservice.unifefu.ws.portal.FefuPortalClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 14.06.2013
 */
public class FEFUPortalNotificationsDaemonDao extends UniBaseDao implements IFEFUPortalNotificationsDaemonDao
{
    public static final SyncDaemon DAEMON = new SyncDaemon(FEFUPortalNotificationsDaemonDao.class.getName(), 5, IFEFUPortalNotificationsDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            final IFEFUPortalNotificationsDaemonDao dao = IFEFUPortalNotificationsDaemonDao.instance.get();

            try
            {
                dao.doSendPortalNotifications();
            }
            catch (final Throwable t)
            {
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            }

        }
    };

    protected Session lock4update()
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, IFEFUPortalNotificationsDaemonDao.GLOBAL_DAEMON_LOCK);
        return session;
    }

    @Override
    public void doSendPortalNotifications()
    {
        List<FefuPortalNotification> notifications = new DQLSelectBuilder()
                .fromEntity(FefuPortalNotification.class, "r").column("r")
                .where(eq(property(FefuPortalNotification.sent().fromAlias("r")), value(Boolean.FALSE)))
                .createStatement(getSession()).list();

        List<FefuPortalNotification> resultNotifications = FefuPortalClient.sendNotifications(notifications);
        for (FefuPortalNotification notif : notifications)
        {
            update(notif);
        }
    }

    @Override
    public void doRegisterPortalNotifications(List<FefuPortalNotification> notifications)
    {
        for (FefuPortalNotification notif : notifications)
        {
            save(notif);
        }
    }

    @Override
    public List<CoreCollectionUtils.Pair<FefuNsiIds, Entrant>> getEntrantsList(EnrollmentCampaign campaign)
    {
        List<Object[]> preResult = new DQLSelectBuilder()
                .fromEntity(Entrant.class, "e").column("pg").column("e")
                .joinEntity("e", DQLJoinType.inner, FefuNsiIds.class, "pg", eq(property(Entrant.person().id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("pg"))))
                .where(eq(property(Entrant.enrollmentCampaign().id().fromAlias("e")), value(campaign)))
                .createStatement(getSession()).list();

        List<CoreCollectionUtils.Pair<FefuNsiIds, Entrant>> result = new ArrayList<>();
        for (Object[] item : preResult)
        {
            result.add(new CoreCollectionUtils.Pair<>((FefuNsiIds) item[0], (Entrant) item[1]));
        }

        return result;
    }

    @Override
    public List<CoreCollectionUtils.Pair<FefuNsiIds, Entrant>> getEntrantsFromOrders(EnrollmentCampaign campaign)
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(EnrollmentExtract.class, "ex").column(property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().id().fromAlias("ex")))
                .where(eq(property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().id().fromAlias("ex")), value(campaign)));

        List<FefuNsiIds> guidsList = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "g").column("g")
                .where(eq(property(FefuNsiIds.entityType().fromAlias("g")), value("person")))
                .where(in(property(FefuNsiIds.entityId().fromAlias("g")), subBuilder.buildQuery()))
                .createStatement(getSession()).list();

        Map<Long, FefuNsiIds> personGuidsMap = new HashMap<>();
        for (FefuNsiIds id : guidsList) personGuidsMap.put(id.getEntityId(), id);

        List<Entrant> preResult = new DQLSelectBuilder()
                .fromEntity(EnrollmentExtract.class, "ex").column(property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().fromAlias("ex")))
                .where(eq(property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().id().fromAlias("ex")), value(campaign)))
                .createStatement(getSession()).list();

        List<CoreCollectionUtils.Pair<FefuNsiIds, Entrant>> result = new ArrayList<>();
        for (Entrant entrant : preResult)
        {
            FefuNsiIds id = personGuidsMap.get(entrant.getPerson().getId());
            if (null != id) result.add(new CoreCollectionUtils.Pair<>(id, entrant));
        }

        return result;
    }

    @Override
    public List<CoreCollectionUtils.Pair<FefuNsiIds, PreliminaryEnrollmentStudent>> getPreliminaryEnrollmentStudents(EnrollmentCampaign campaign)
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(PreliminaryEnrollmentStudent.class, "ex").column(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().person().id().fromAlias("ex")))
                .where(eq(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().id().fromAlias("ex")), value(campaign)));

        List<FefuNsiIds> guidsList = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "g").column("g")
                .where(eq(property(FefuNsiIds.entityType().fromAlias("g")), value("person")))
                .where(in(property(FefuNsiIds.entityId().fromAlias("g")), subBuilder.buildQuery()))
                .createStatement(getSession()).list();

        Map<Long, FefuNsiIds> personGuidsMap = new HashMap<>();
        for (FefuNsiIds id : guidsList) personGuidsMap.put(id.getEntityId(), id);

        List<PreliminaryEnrollmentStudent> preResult = new DQLSelectBuilder()
                .fromEntity(PreliminaryEnrollmentStudent.class, "s").column("s")
                .where(eq(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("s")), value(campaign)))
                .createStatement(getSession()).list();

        List<CoreCollectionUtils.Pair<FefuNsiIds, PreliminaryEnrollmentStudent>> result = new ArrayList<>();
        for (PreliminaryEnrollmentStudent stud : preResult)
        {
            FefuNsiIds id = personGuidsMap.get(stud.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getId());
            if (null != id) result.add(new CoreCollectionUtils.Pair<>(id, stud));
        }

        return result;
    }

    @Override
    public List<CoreCollectionUtils.Pair<FefuNsiIds, EcgEntrantRecommended>> getEntrantsRecommended(EnrollmentCampaign campaign)
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(EcgEntrantRecommended.class, "ex").column(property(EcgEntrantRecommended.direction().entrantRequest().entrant().person().id().fromAlias("ex")))
                .where(eq(property(EcgEntrantRecommended.direction().entrantRequest().entrant().enrollmentCampaign().id().fromAlias("ex")), value(campaign)));

        List<FefuNsiIds> guidsList = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "g").column("g")
                .where(eq(property(FefuNsiIds.entityType().fromAlias("g")), value("person")))
                .where(in(property(FefuNsiIds.entityId().fromAlias("g")), subBuilder.buildQuery()))
                .createStatement(getSession()).list();

        Map<Long, FefuNsiIds> personGuidsMap = new HashMap<>();
        for (FefuNsiIds id : guidsList) personGuidsMap.put(id.getEntityId(), id);

        List<EcgEntrantRecommended> preResult = new DQLSelectBuilder()
                .fromEntity(EcgEntrantRecommended.class, "e").column("e")
                .where(eq(property(EcgEntrantRecommended.direction().entrantRequest().entrant().enrollmentCampaign().fromAlias("e")), value(campaign)))
                .createStatement(getSession()).list();

        List<CoreCollectionUtils.Pair<FefuNsiIds, EcgEntrantRecommended>> result = new ArrayList<>();
        for (EcgEntrantRecommended ent : preResult)
        {
            FefuNsiIds id = personGuidsMap.get(ent.getDirection().getEntrantRequest().getEntrant().getPerson().getId());
            if (null != id) result.add(new CoreCollectionUtils.Pair<>(id, ent));
        }

        return result;
    }
}