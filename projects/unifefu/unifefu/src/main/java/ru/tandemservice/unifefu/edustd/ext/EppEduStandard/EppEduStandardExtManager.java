/* $Id$ */
package ru.tandemservice.unifefu.edustd.ext.EppEduStandard;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexey Lopatin
 * @since 19.01.2015
 */
@Configuration
public class EppEduStandardExtManager extends BusinessObjectExtensionManager
{
}
