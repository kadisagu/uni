/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

/**
 * @author Nikolay Fedorovskih
 * @since 13.08.2013
 */
public interface IFefuStudentBookNumberDaemonBean
{
    public static final SpringBeanCache<IFefuStudentBookNumberDaemonBean> instance = new SpringBeanCache<>(IFefuStudentBookNumberDaemonBean.class.getName());

    /**
     * Заполняем номера зачетных книжек и личных дел студентов, у которых они не заполнены
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doFillStudentBookNumbers();
}