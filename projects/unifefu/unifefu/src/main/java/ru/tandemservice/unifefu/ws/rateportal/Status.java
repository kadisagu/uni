/**
 * Status.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportal;

public class Status  implements java.io.Serializable {
    private java.lang.String inputid;

    private java.lang.String systemcode;

    private java.lang.String functioncode;

    private java.lang.String contenttype;

    private ru.tandemservice.unifefu.ws.rateportal.RecipientList recipients;

    private ru.tandemservice.unifefu.ws.rateportal.ResourceList resources;

    private java.lang.String status;

    private java.lang.String detail;

    private java.util.Calendar actualdate;

    private java.lang.String comment;

    public Status() {
    }

    public Status(
           java.lang.String inputid,
           java.lang.String systemcode,
           java.lang.String functioncode,
           java.lang.String contenttype,
           ru.tandemservice.unifefu.ws.rateportal.RecipientList recipients,
           ru.tandemservice.unifefu.ws.rateportal.ResourceList resources,
           java.lang.String status,
           java.lang.String detail,
           java.util.Calendar actualdate,
           java.lang.String comment) {
           this.inputid = inputid;
           this.systemcode = systemcode;
           this.functioncode = functioncode;
           this.contenttype = contenttype;
           this.recipients = recipients;
           this.resources = resources;
           this.status = status;
           this.detail = detail;
           this.actualdate = actualdate;
           this.comment = comment;
    }


    /**
     * Gets the inputid value for this Status.
     * 
     * @return inputid
     */
    public java.lang.String getInputid() {
        return inputid;
    }


    /**
     * Sets the inputid value for this Status.
     * 
     * @param inputid
     */
    public void setInputid(java.lang.String inputid) {
        this.inputid = inputid;
    }


    /**
     * Gets the systemcode value for this Status.
     * 
     * @return systemcode
     */
    public java.lang.String getSystemcode() {
        return systemcode;
    }


    /**
     * Sets the systemcode value for this Status.
     * 
     * @param systemcode
     */
    public void setSystemcode(java.lang.String systemcode) {
        this.systemcode = systemcode;
    }


    /**
     * Gets the functioncode value for this Status.
     * 
     * @return functioncode
     */
    public java.lang.String getFunctioncode() {
        return functioncode;
    }


    /**
     * Sets the functioncode value for this Status.
     * 
     * @param functioncode
     */
    public void setFunctioncode(java.lang.String functioncode) {
        this.functioncode = functioncode;
    }


    /**
     * Gets the contenttype value for this Status.
     * 
     * @return contenttype
     */
    public java.lang.String getContenttype() {
        return contenttype;
    }


    /**
     * Sets the contenttype value for this Status.
     * 
     * @param contenttype
     */
    public void setContenttype(java.lang.String contenttype) {
        this.contenttype = contenttype;
    }


    /**
     * Gets the recipients value for this Status.
     * 
     * @return recipients
     */
    public ru.tandemservice.unifefu.ws.rateportal.RecipientList getRecipients() {
        return recipients;
    }


    /**
     * Sets the recipients value for this Status.
     * 
     * @param recipients
     */
    public void setRecipients(ru.tandemservice.unifefu.ws.rateportal.RecipientList recipients) {
        this.recipients = recipients;
    }


    /**
     * Gets the resources value for this Status.
     * 
     * @return resources
     */
    public ru.tandemservice.unifefu.ws.rateportal.ResourceList getResources() {
        return resources;
    }


    /**
     * Sets the resources value for this Status.
     * 
     * @param resources
     */
    public void setResources(ru.tandemservice.unifefu.ws.rateportal.ResourceList resources) {
        this.resources = resources;
    }


    /**
     * Gets the status value for this Status.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Status.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the detail value for this Status.
     * 
     * @return detail
     */
    public java.lang.String getDetail() {
        return detail;
    }


    /**
     * Sets the detail value for this Status.
     * 
     * @param detail
     */
    public void setDetail(java.lang.String detail) {
        this.detail = detail;
    }


    /**
     * Gets the actualdate value for this Status.
     * 
     * @return actualdate
     */
    public java.util.Calendar getActualdate() {
        return actualdate;
    }


    /**
     * Sets the actualdate value for this Status.
     * 
     * @param actualdate
     */
    public void setActualdate(java.util.Calendar actualdate) {
        this.actualdate = actualdate;
    }


    /**
     * Gets the comment value for this Status.
     * 
     * @return comment
     */
    public java.lang.String getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this Status.
     * 
     * @param comment
     */
    public void setComment(java.lang.String comment) {
        this.comment = comment;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Status)) return false;
        Status other = (Status) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inputid==null && other.getInputid()==null) || 
             (this.inputid!=null &&
              this.inputid.equals(other.getInputid()))) &&
            ((this.systemcode==null && other.getSystemcode()==null) || 
             (this.systemcode!=null &&
              this.systemcode.equals(other.getSystemcode()))) &&
            ((this.functioncode==null && other.getFunctioncode()==null) || 
             (this.functioncode!=null &&
              this.functioncode.equals(other.getFunctioncode()))) &&
            ((this.contenttype==null && other.getContenttype()==null) || 
             (this.contenttype!=null &&
              this.contenttype.equals(other.getContenttype()))) &&
            ((this.recipients==null && other.getRecipients()==null) || 
             (this.recipients!=null &&
              this.recipients.equals(other.getRecipients()))) &&
            ((this.resources==null && other.getResources()==null) || 
             (this.resources!=null &&
              this.resources.equals(other.getResources()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.detail==null && other.getDetail()==null) || 
             (this.detail!=null &&
              this.detail.equals(other.getDetail()))) &&
            ((this.actualdate==null && other.getActualdate()==null) || 
             (this.actualdate!=null &&
              this.actualdate.equals(other.getActualdate()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInputid() != null) {
            _hashCode += getInputid().hashCode();
        }
        if (getSystemcode() != null) {
            _hashCode += getSystemcode().hashCode();
        }
        if (getFunctioncode() != null) {
            _hashCode += getFunctioncode().hashCode();
        }
        if (getContenttype() != null) {
            _hashCode += getContenttype().hashCode();
        }
        if (getRecipients() != null) {
            _hashCode += getRecipients().hashCode();
        }
        if (getResources() != null) {
            _hashCode += getResources().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getDetail() != null) {
            _hashCode += getDetail().hashCode();
        }
        if (getActualdate() != null) {
            _hashCode += getActualdate().hashCode();
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Status.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/data", "Status"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inputid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/data", "inputid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/data", "systemcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("functioncode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/data", "functioncode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contenttype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/data", "contenttype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recipients");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipients"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "RecipientList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resources");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resources"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "ResourceList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/data", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/data", "detail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/data", "actualdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/data", "comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
