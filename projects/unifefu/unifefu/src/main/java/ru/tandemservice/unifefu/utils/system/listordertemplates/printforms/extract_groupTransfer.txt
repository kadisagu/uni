\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
О переводе из группы в группы\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_A}\cell\b0
{studentCategory_A} {course} курса{groupInternal_G}, {learned_A}{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStr}
 по {fefuEducationStrDirection_D} {orgUnitPrep} {formativeOrgUnitStrWithTerritorial_P} по {developForm_DF} форме обучения,
 перевести в группу {groupNew}.\par
\par
{basicsWord}{listBasics}{basicsPoint}\fi0\cell\row\pard