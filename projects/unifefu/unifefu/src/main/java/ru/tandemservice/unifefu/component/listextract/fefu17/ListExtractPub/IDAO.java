/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu17.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.01.2015
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuTransfStuDPOListExtract, Model>
{
}