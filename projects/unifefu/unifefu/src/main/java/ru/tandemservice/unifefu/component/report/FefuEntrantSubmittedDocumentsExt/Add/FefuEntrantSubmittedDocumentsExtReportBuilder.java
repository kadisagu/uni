package ru.tandemservice.unifefu.component.report.FefuEntrantSubmittedDocumentsExt.Add;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.base.entity.ecg.codes.EcgDistributionStateCodes;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.*;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.component.report.FefuReportUtil;
import ru.tandemservice.unifefu.component.report.ReportItem;
import ru.tandemservice.unifefu.entity.FefuEntrantContract;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: amakarova
 * Date: 18.06.13
 */
public class FefuEntrantSubmittedDocumentsExtReportBuilder
{

    private Model _model;
    private Session _session;
    /**
     * map(EnrollmentDirection Id, list(RequestedEnrollmentDirection) )
     */
    private Map<Long, List<RequestedEnrollmentDirection>> _requestedDirectionByDirection;
    /**
     * map(Entrant Id, list(FefuEntrantContract) )
     */
    private Map<Long, List<FefuEntrantContract>> _contractMap = new HashMap<>();
    /**
     * map(EntrantRequest Id, list(RequestedEnrollmentDirection) )
     */
    private Map<Long, List<RequestedEnrollmentDirection>> _requestedDirectionByRequest;
    protected RtfDocument _emptyTitleTemplate;
    protected RtfDocument _emptyTableTemplate;
    protected Map<EnrollmentDirection, EnrollmentDirectionExamSetData> _direction2examSetDetailMap;
    protected boolean isBudget;
    protected Map<Long, Set<EntrantIndividualProgress>> individualAchievementsMap;
    protected Map<Entrant, Map<Discipline2RealizationWayRelation, List<SubjectPassForm>>> _passFormByEntrantByDiscipline;
    /**
     * map(RequestedEnrollmentDirection Id, list(ChosenEntranceDiscipline) )
     */
    protected Map<Long, Set<ChosenEntranceDiscipline>> _chosenDisciplineByRequestedDirection;
    /**
     * map(Person Id, RequestedEnrollmentDirection)
     */
    protected Map<Long, List<PersonBenefit>> _personBenefits;
    /**
     * map(Entrant Id, list(EntrantEnrolmentRecommendation) )
     */
    protected Map<Long, List<EntrantEnrolmentRecommendation>> _entrantEnrolmentRecommendations;
    protected Map<CompetitionKind, Integer> _competitionKindPriorities;

    private Set<Long> _recommendeds;
    private Set<Long> _preStudents;
    /**
     * map(EnrollmentDirection Id, map[StudentCategoryId, map[Discipline2RealizationWayRelation Id, Priority) ) )
     */
    protected Map<Long, Map<Long, Map<Integer, List<Long>>>> _disciplinesByStudentCategoryByDirectionMap;

    // номера столбцов в шаблоне
    final int COL_NUM_INDEX = 0;
    final int COL_NUM_REG = 1;
    final int COL_NUM_FIO = 2;
    final int COL_NUM_CODE = 3;
    final int COL_NUM_CITY = 4;
    final int COL_NUM_MARKS = 5;
    final int COL_NUM_IND_ACH = 6;
    final int COL_NUM_SUM = 7;
    final int COL_NUM_CATEGORY = 8;
    final int COL_NUM_TARGET_ADMI = 9;
    final int COL_NUM_ORIGINAL_DOCUMENT = 10;
    final int COL_NUM_CONTRACT = 11;
    final int COL_NUM_DIRECTIONS = 12;
    final int COL_NUM_PRIORITY = 13;
    final int COL_NUM_PHONE = 14;
    final int COL_NUM_EGE = 15;
    final int COL_NUM_AGREEMNT = 16;
    final int COL_NUM_ORIGINAL = 17;

    public FefuEntrantSubmittedDocumentsExtReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    protected byte[] buildReport()
    {
        // получаем список направлений приема
        List<EnrollmentDirection> enrollmentDirections = FefuReportUtil.getEnrollmentDirectionList(_session, _model);

        _disciplinesByStudentCategoryByDirectionMap = FefuReportUtil.getDisciplinesByStudentCategoryByDirection(_session, enrollmentDirections);

        DQLSelectBuilder requestedDirectionsBuilder = FefuReportUtil.getRequestedEnrollmentDirections(_model, enrollmentDirections);
        List<RequestedEnrollmentDirection> requestedEnrollmentDirections = requestedDirectionsBuilder.createStatement(_session).list();

        // fetch data
        init(requestedEnrollmentDirections, requestedDirectionsBuilder);

        _requestedDirectionByDirection = requestedEnrollmentDirections.stream().collect(Collectors.groupingBy(
                red -> red.getEnrollmentDirection().getId(),
                Collectors.mapping(red -> red, Collectors.toList())
        ));

        RtfDocument document = prepareTemplates(requestedEnrollmentDirections.isEmpty() && !_model.isByAllEnrollmentDirections());

        TreeMap<String, List<EnrollmentDirection>> mainMap = enrollmentDirections.stream()
                // если нужно отфильтровываются направления без заявлений
                .filter(direction -> !(_model.getReport().isNotPrintSpesWithoutRequest()
                        && CollectionUtils.isEmpty(_requestedDirectionByDirection.get(direction.getId()))))

                .collect(Collectors.groupingBy(
                        direction -> {
                            EducationOrgUnit educationOrgUnit = direction.getEducationOrgUnit();
                            return educationOrgUnit.getFormativeOrgUnit().getPrintTitle() + " (" + educationOrgUnit.getTerritorialOrgUnit().getFullTitle() + ")";
                        },
                        TreeMap::new,
                        Collectors.mapping(direction -> direction, Collectors.toList())
                ));

        boolean[] notFirst = {false};
        mainMap.entrySet().forEach(ouEntry -> {
                                       if (notFirst[0])
                                           FefuReportUtil.insertPageBreak(document.getElementList());
                                       document.getElementList().addAll(getOuTable(ouEntry.getKey(), ouEntry.getValue()));
                                       notFirst[0] = true;
                                   }

        );

        return RtfUtil.toByteArray(document);
    }

    protected RtfDocument prepareTemplates(boolean empty)
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniFefuDefines.TEMPLATE_ENTRANT_SUBMITTED_DOCUMENTS_EXT);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        IRtfElement searchResultTitle = UniRtfUtil.findElement(template.getElementList(), "titleOu");
        _emptyTitleTemplate = RtfBean.getElementFactory().createRtfDocument();
        _emptyTitleTemplate.getElementList().add(searchResultTitle);
        _emptyTitleTemplate.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        _emptyTitleTemplate.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        IRtfElement searchResultTable = UniRtfUtil.findElement(template.getElementList(), "T");
        _emptyTableTemplate = RtfBean.getElementFactory().createRtfDocument();
        _emptyTableTemplate.getElementList().add(searchResultTable.getClone());
        if (!empty)
        {
            template.getElementList().remove(searchResultTitle);
            template.getElementList().remove(searchResultTable);
        }

        String titleParam = "";
        if (_model.isIncludeEntrantNoMark())
            titleParam = " без баллов";
        if (_model.isIncludeEntrantWithBenefit())
            titleParam = ", имеющих льготы";
        if (_model.isIncludeEntrantTargetAdmission())
            titleParam = ", поступающих по целевому набору";
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("titleParam", titleParam);

        if (empty)
        {
            String titleOu = _model.getFormativeOrgUnit().getPrintTitle() + " (" + _model.getTerritorialOrgUnit().getFullTitle() + ")";
            injectModifier.put("titleOu", titleOu);
            RtfTableModifier tm = new RtfTableModifier();
            tm.put("T1", new String[][]{new String[3]});
            tm.put("T", new String[][]{});
            tm.modify(template);
        }
        injectModifier.modify(template);

        return template;
    }

    protected void init(List<RequestedEnrollmentDirection> requestedDirections, DQLSelectBuilder requestedDirectionsBuilder)
    {
        _contractMap = getEntrantContractMap(requestedDirections);
        _requestedDirectionByRequest = FefuReportUtil.getOtherRequestedDirection(_session, requestedDirections);

        _personBenefits = FefuReportUtil.getPersonBenefitsMap(_session, requestedDirectionsBuilder);
        _competitionKindPriorities = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(_model.getEnrollmentCampaign());

        _direction2examSetDetailMap = ExamSetUtil.getDirectionExamSetDataMap(
                _session, _model.getReport().getEnrollmentCampaign(), _model.getStudentCategoryList(), _model.getReport().getCompensationType());
        _direction2examSetDetailMap.forEach((direction, examSetData) -> {
            if (!examSetData.isStructuresEqual())
                throw new ApplicationException("Для направления приема '" + direction.getTitle() + "' по категориям поступающих и виду затрат наборы экзаменов различные.");
        });

        individualAchievementsMap = FefuReportUtil.getIndividualAchievementsMap(_session, requestedDirectionsBuilder);
        isBudget = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_model.getReport().getCompensationType().getCode());
        _chosenDisciplineByRequestedDirection = FefuReportUtil.getChosenDisciplineByRequestedDirection(_session, requestedDirectionsBuilder);
        _passFormByEntrantByDiscipline = ExamSetUtil.getExamPassDisciplineByEntrantByDiscipline(_session, requestedDirections);

        _recommendeds = getRecommendedBuilder(requestedDirections);
        _preStudents = getPreliminaryBuilder(requestedDirections);

        _entrantEnrolmentRecommendations = EntrantDataUtil.getEntrantEnrolmentRecommendations(_session, requestedDirectionsBuilder);
    }

    protected List<IRtfElement> getOuTable(String orgUnitTitle, List<EnrollmentDirection> directions)
    {
        RtfDocument result = _emptyTitleTemplate.getClone();
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("titleOu", orgUnitTitle);
        injectModifier.modify(result);
        directions.sort((o1, o2) -> o1.getEducationOrgUnit().getEducationLevelHighSchool().getTitle()
                .compareTo(o2.getEducationOrgUnit().getEducationLevelHighSchool().getTitle()));

        Iterator<EnrollmentDirection> iterator = directions.iterator();
        while (iterator.hasNext())
        {
            result.getElementList().addAll(getDirectionTable(iterator.next()));

            if (iterator.hasNext())
                FefuReportUtil.insertPageBreak(result.getElementList());
        }

        return result.getElementList();
    }

    protected List<IRtfElement> getDirectionTable(EnrollmentDirection direction)
    {
        List<RequestedEnrollmentDirection> requestedDirections = _requestedDirectionByDirection.get(direction.getId());

        if (requestedDirections == null)
            requestedDirections = new ArrayList<>();

        RtfDocument table = _emptyTableTemplate.getClone();
        RtfTableModifier modifier = new RtfTableModifier();
        fillDirectionTableHead(modifier, direction);
        fillDirectionTableBody(modifier, direction, requestedDirections);
        modifier.modify(table);

        return table.getElementList();
    }

    protected void fillDirectionTableHead(RtfTableModifier modifier, EnrollmentDirection direction)
    {
        int targetPlan;
        if (isBudget)
            targetPlan = direction.getTargetAdmissionPlanBudget() != null ? direction.getTargetAdmissionPlanBudget() : 0;
        else
            targetPlan = direction.getTargetAdmissionPlanContract() != null ? direction.getTargetAdmissionPlanContract() : 0;

        int quotaPlan = direction.getSpecRightsQuota() != null ? direction.getSpecRightsQuota() : 0;

        int allPlan;
        if (isBudget)
            allPlan = direction.getMinisterialPlan() != null ? direction.getMinisterialPlan() : 0;
        else
            allPlan = direction.getContractPlan() != null ? direction.getContractPlan() : 0;

        String[][] headerRow = new String[1][4];
        headerRow[0][0] = direction.getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
        headerRow[0][1] = String.valueOf(allPlan - targetPlan - quotaPlan);
        headerRow[0][2] = String.valueOf(targetPlan);
        headerRow[0][3] = String.valueOf(quotaPlan);

        modifier.put("T1", headerRow);
    }

    protected void fillDirectionTableBody(RtfTableModifier modifier, EnrollmentDirection direction, List<RequestedEnrollmentDirection> requestedDirections)
    {
        // получаем структуру набора экзаменов текущего направления приема
        final EnrollmentDirectionExamSetData examSetData = _direction2examSetDetailMap.get(direction);

        Map<Long,  Map<Integer, List<Long>>> disciplinesByStudentCategoryMap =
                _disciplinesByStudentCategoryByDirectionMap.get(direction.getId());
        Map<Integer, List<Long>> disciplines = null;
        if (disciplinesByStudentCategoryMap != null)
            disciplines = disciplinesByStudentCategoryMap.get(direction.getId());

        int[] index = {1};
        String[][] data = requestedDirections.stream()
                .map(red -> getReportItem(red, examSetData))
                .sorted(new ReportItem.ItemComparator(_competitionKindPriorities, disciplines))
                .map(item -> getTableRow(index[0]++, item, examSetData))
                .toArray(String[][]::new);
        modifier.put("T", data);
        modifier.put("T", getRtfRowIntercepter4Table(examSetData));
    }

    protected ReportItem getReportItem(RequestedEnrollmentDirection requestedDirection, EnrollmentDirectionExamSetData examSetData)
    {
        ReportItem item = new ReportItem(requestedDirection);

        Entrant entrant = requestedDirection.getEntrantRequest().getEntrant();
        Person person = entrant.getPerson();
        ExamSetStructure examSetStructure = examSetData != null ? examSetData.getExamSetStructure() : null;
        List<ExamSetStructureItem> examSetStructureItems = examSetStructure != null ? examSetStructure.getItemList() : null;
        Set<ChosenEntranceDiscipline> chosenEntranceDisciplines = _chosenDisciplineByRequestedDirection.get(requestedDirection.getId());
        ChosenEntranceDiscipline[] chosenDistribution = MarkDistributionUtil.getChosenDistribution(
                chosenEntranceDisciplines != null ? chosenEntranceDisciplines : new HashSet<>(), examSetStructureItems);
        item.setChosenDistribution(chosenDistribution);

        Map<Discipline2RealizationWayRelation, List<SubjectPassForm>> passFormsByDiscipline = _passFormByEntrantByDiscipline.get(entrant);
        item.setChosenDisciplinesByWay(Arrays.asList(chosenDistribution).stream()
                                               .filter(cho -> cho != null)
                                               .peek(cho -> {
                                                   Discipline2RealizationWayRelation discipline = cho.getEnrollmentCampaignDiscipline();
                                                   List<SubjectPassForm> passForms = passFormsByDiscipline == null ? null : passFormsByDiscipline.get(discipline);
                                                   FefuReportUtil.addPassFormNSummMark(item, passForms, cho);
                                               })
                                               .collect(Collectors.toMap(cho -> cho.getEnrollmentCampaignDiscipline().getId(), cho -> cho)));
        item.setPassFormsByDiscipline(_passFormByEntrantByDiscipline.get(entrant));

        item.setContracts(_contractMap.get(entrant.getId()));

        List<RequestedEnrollmentDirection> otherRequestedDirections = _requestedDirectionByRequest.get(requestedDirection.getEntrantRequest().getId());
        item.setOtherRequestedDirection(otherRequestedDirections);

        EnrollmentCampaign campaign = requestedDirection.getEnrollmentDirection().getEnrollmentCampaign();
        boolean originalOnly = campaign.isUseCompetitionGroup() || campaign.isOriginalDocumentPerDirection();
        RequestedEnrollmentDirection original = otherRequestedDirections.stream()
                .filter(red -> red.isOriginalDocumentHandedIn() || !originalOnly)
                .sorted((o1, o2) -> Integer.compare(o1.getPriority(), o2.getPriority()))
                .findFirst().orElse(null);
        item.setOrigReqDir(original);

        item.setRecommended(_recommendeds.contains(requestedDirection.getId()));
        item.setPreliminary(_preStudents.contains(requestedDirection.getId()));

        item.setIndividualProgresses(individualAchievementsMap.get(requestedDirection.getEntrantRequest().getId()));
        item.setBenefits(_personBenefits.get(person.getId()));
        item.setEnrolmentRecommendations(_entrantEnrolmentRecommendations.get(entrant.getId()));

        return item;
    }

    protected String[] getTableRow(int index, ReportItem item, EnrollmentDirectionExamSetData examSetData)
    {
        List<String> result = new ArrayList<>();

        RequestedEnrollmentDirection requestedDirection = item.getRequestedDirection();
        Entrant entrant = requestedDirection.getEntrantRequest().getEntrant();
        Person person = entrant.getPerson();
        ExamSetStructure examSetStructure = examSetData != null ? examSetData.getExamSetStructure() : null;
        List<ExamSetStructureItem> examSetStructureItems = examSetStructure != null ? examSetStructure.getItemList() : null;

        EntrantState entrantState = entrant.getState();
        String first = Integer.toString(index);
        if (_model.isIncludeSymbols())
        {
            String symbol = "";
            if (entrantState.getCode().equals(EntrantStateCodes.ENROLED) || entrantState.getCode().equals(EntrantStateCodes.PRELIMENARY_ENROLLED))
            {
                if (item.isPreliminary())   // зачислен на это направление
                    symbol = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(requestedDirection.getCompensationType().getCode()) ? "!!" : "!";
                else                        // зачислен на другое направление
                    symbol = "?";
            }
            else if (item.getOrigReqDir() != null) symbol = "*";

            first = symbol + " " + first;
        }
        result.add(first);

        result.add(FefuReportUtil.getRegNumber(requestedDirection));
        result.add(person.getFullFio());

        String code = "";
        String disctrict = "";
        AddressBase address = person.getAddress();
        if (address != null && address instanceof AddressDetailed)
        {
            AddressItem settlement = ((AddressDetailed) address).getSettlement();
            if (settlement != null)
            {
                code = StringUtils.isEmpty(settlement.getInheritedRegionCode()) ? "" : settlement.getInheritedRegionCode();
                disctrict = settlement.getTitle();
            }
        }
        result.add(code);
        result.add(disctrict);

        if (examSetStructureItems == null || examSetStructureItems.isEmpty())
            result.add("");
        else
        {
            for (ChosenEntranceDiscipline chosenDiscipline : item.getChosenDistribution())
            {

                String markStr;
                if (chosenDiscipline == null) markStr = "x";
                else
                {
                    Double mark = chosenDiscipline.getFinalMark();
                    if (mark != null) markStr = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark);
                    else markStr = "";
                }
                if (!_model.getReport().isWithoutDetailSumMark())
                    result.add(markStr);
            }
        }

        if (_model.getEnrollmentCampaign().isUseIndividualProgressInAmountMark())
        {
            Collection<EntrantIndividualProgress> progressList = item.getIndividualProgresses();
            String progress = "";
            if (!CollectionUtils.isEmpty(progressList))
                progress = progressList.stream().map(ip -> ip.getIndividualProgressType().getShortTitle()).collect(Collectors.joining(", "));
            result.add(progress);
        }

        result.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getFinalSumMark()));
        result.add(FefuReportUtil.getCategory(requestedDirection, _model.isIncludeEntrantTargetAdmission(),
                                              !CollectionUtils.isEmpty(item.getBenefits()), !CollectionUtils.isEmpty(item.getEnrolmentRecommendations())));
        if (requestedDirection.isTargetAdmission()) result.add("ЦН");
        else result.add("");
        result.add(item.getPassForm("+", "-"));

        String contracts = "";
        if (item.getContracts() != null)
            contracts = item.getContracts().stream()
                    .map(c -> c.getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle())
                    .collect(Collectors.joining(", "));
        result.add(contracts);

        String otherDirection = "";
        if (item.getOtherRequestedDirection() != null)
            otherDirection = item.getOtherRequestedDirection().stream()
                    .filter(d -> !d.getId().equals(item.getRequestedDirection().getId()))
                    .map(d -> d.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle())
                    .collect(Collectors.joining(", "));
        result.add(otherDirection);

//        result.add(String.valueOf(requestedDirection.getPriority())); Колонка "Приор."

        result.add(person.getContactData().getAllPhones());

        if (_model.getReport().getIncludeEntrantWithBenefit())
        {
            List<PersonBenefit> benefits = _personBenefits.get(person.getId());
            String strBenefits = "";
            if (!CollectionUtils.isEmpty(benefits))
                strBenefits = benefits.stream().map(PersonBenefit::getTitle).collect(Collectors.joining("; "));
            result.add(strBenefits);
        }


        if (!_model.getReport().isWithoutDocumentInfo())
            result.add(requestedDirection.isOriginalDocumentHandedIn() ? "п" : "к");
        result.add(requestedDirection.isAgree4Enrollment() ? "+" : "-");

        //Колонка "Ориг"
        RequestedEnrollmentDirection origReqDir = item.getOrigReqDir();
        String origReqDirStr = "";
        if (origReqDir != null)
            origReqDirStr = origReqDir.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle();
        result.add(origReqDirStr);

        return result.toArray(new String[result.size()]);
    }

    protected RtfRowIntercepterBase getRtfRowIntercepter4Table(EnrollmentDirectionExamSetData examSetData)
    {
        return new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                RtfRow headRow = table.getRowList().get(currentRowIndex - 1);
                List<RtfCell> headerCellList = headRow.getCellList();
                RtfRow firstRow = table.getRowList().get(currentRowIndex);
                List<RtfCell> cellList = firstRow.getCellList();
                int freeWidth = 0;

                List<RtfCell> forHeaderDelete = new ArrayList<>();
                List<RtfCell> forDelete = new ArrayList<>();

                if (_model.getReport().isWithoutDocumentInfo())
                {
                    forHeaderDelete.add(headerCellList.get(COL_NUM_ORIGINAL_DOCUMENT));
                    forDelete.add(cellList.get(COL_NUM_ORIGINAL_DOCUMENT));
                    freeWidth += cellList.get(COL_NUM_ORIGINAL_DOCUMENT).getWidth();
                }

                if (!_model.getEnrollmentCampaign().isUseIndividualProgressInAmountMark())
                {
                    forHeaderDelete.add(headerCellList.get(COL_NUM_IND_ACH));
                    forDelete.add(cellList.get(COL_NUM_IND_ACH));
                    freeWidth += cellList.get(COL_NUM_IND_ACH).getWidth();
                }

                //if (!_model.getReport().isShowRequestedDirectionPriority()) Колонка "Приор." ненужна, но в шаблоне есть
                {
                    forHeaderDelete.add(headerCellList.get(COL_NUM_PRIORITY));
                    forDelete.add(cellList.get(COL_NUM_PRIORITY));
                    freeWidth += cellList.get(COL_NUM_PRIORITY).getWidth();
                }

                if (!_model.isWithOriginalColumn())
                {
                    forHeaderDelete.add(headerCellList.get(COL_NUM_ORIGINAL));
                    forDelete.add(cellList.get(COL_NUM_ORIGINAL));
                    freeWidth += cellList.get(COL_NUM_ORIGINAL).getWidth();
                }

                if (_model.getReport().isWithoutDetailSumMark())
                {
                    forHeaderDelete.add(headerCellList.get(COL_NUM_MARKS));
                    forDelete.add(cellList.get(COL_NUM_MARKS));
                    freeWidth += cellList.get(COL_NUM_MARKS).getWidth();
                }
                else
                {
                    List<StudentCategory> listCategory = _model.getStudentCategoryList();
                    if (listCategory == null || listCategory.isEmpty())
                        listCategory = IUniDao.instance.get().getCatalogItemList(StudentCategory.class);

                    FefuReportUtil.splitMarksColumns(headRow, firstRow, COL_NUM_MARKS, examSetData, listCategory, _model.getReport().isShowDisciplineTitles());
                }

                headerCellList.removeAll(forHeaderDelete);
                cellList.removeAll(forDelete);

                int newFioWidth = headerCellList.get(COL_NUM_FIO).getWidth() + freeWidth;
                headerCellList.get(COL_NUM_FIO).setWidth(newFioWidth);
                cellList.get(COL_NUM_FIO).setWidth(newFioWidth);
            }
        };
    }

    /**
     * Направления приема, по которым заключен контракт - по абитуриентам
     */
    private Map<Long, List<FefuEntrantContract>> getEntrantContractMap(List<RequestedEnrollmentDirection> requestedDirections)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(FefuEntrantContract.class, "contract")
                .column(property("contract"))
                .where(in(property(FefuEntrantContract.requestedEnrollmentDirection().fromAlias("contract")), requestedDirections))
                .where(eq(property(FefuEntrantContract.contractTerminated().fromAlias("contract")), value(Boolean.FALSE)));

        return builder.createStatement(_session).<FefuEntrantContract>list().stream()
                .collect(Collectors.groupingBy(
                        c -> c.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getId(),
                        Collectors.mapping(c -> c, Collectors.toList())
                ));
    }

    /**
     * Список Студент предварительного зачисления
     */
    private Set<Long> getPreliminaryBuilder(List<RequestedEnrollmentDirection> requestedDirections)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PreliminaryEnrollmentStudent.class, "p")
                .where(in(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().fromAlias("p")), requestedDirections))
                .column(property("p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().id()));
        return new HashSet<>(builder.createStatement(_session).list());
    }

    /**
     * Список Рекомендованный абитуриент распределения
     */
    private Set<Long> getRecommendedBuilder(List<RequestedEnrollmentDirection> requestedDirections)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EcgEntrantRecommended.class, "p")
                .where(in(property(EcgEntrantRecommended.direction().fromAlias("p")), requestedDirections))
                .where(eq(property("p." + EcgEntrantRecommended.distribution().state().code()), value(EcgDistributionStateCodes.APPROVED)))
                .column(property("p", EcgEntrantRecommended.direction().id()));
        return new HashSet<>(builder.createStatement(_session).list());
    }
}
