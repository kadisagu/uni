/* $Id$ */
package ru.tandemservice.unifefu.component.student.LogView;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

/**
 * @author Alexey Lopatin
 * @since 18.10.2013
 */
@State({@Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="entityId")})
public class Model extends ru.tandemservice.uni.component.log.EntityLogViewBase.Model
{
}
