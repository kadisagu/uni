package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Внешнее перезачтение (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionTransferOutsideOperationExtGen extends EntityBase
 implements INaturalIdentifiable<SessionTransferOutsideOperationExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt";
    public static final String ENTITY_NAME = "sessionTransferOutsideOperationExt";
    public static final int VERSION_HASH = -1726929191;
    private static IEntityMeta ENTITY_META;

    public static final String L_SESSION_TRANSFER_OUTSIDE_OPERATION = "sessionTransferOutsideOperation";
    public static final String P_WORK_TIME_DISC = "workTimeDisc";

    private SessionTransferOutsideOperation _sessionTransferOutsideOperation;     // Перезачтенное мероприятие из другого ОУ
    private Integer _workTimeDisc;     // Трудоемкость дисциплины

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Перезачтенное мероприятие из другого ОУ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SessionTransferOutsideOperation getSessionTransferOutsideOperation()
    {
        return _sessionTransferOutsideOperation;
    }

    /**
     * @param sessionTransferOutsideOperation Перезачтенное мероприятие из другого ОУ. Свойство не может быть null и должно быть уникальным.
     */
    public void setSessionTransferOutsideOperation(SessionTransferOutsideOperation sessionTransferOutsideOperation)
    {
        dirty(_sessionTransferOutsideOperation, sessionTransferOutsideOperation);
        _sessionTransferOutsideOperation = sessionTransferOutsideOperation;
    }

    /**
     * @return Трудоемкость дисциплины.
     */
    public Integer getWorkTimeDisc()
    {
        return _workTimeDisc;
    }

    /**
     * @param workTimeDisc Трудоемкость дисциплины.
     */
    public void setWorkTimeDisc(Integer workTimeDisc)
    {
        dirty(_workTimeDisc, workTimeDisc);
        _workTimeDisc = workTimeDisc;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionTransferOutsideOperationExtGen)
        {
            if (withNaturalIdProperties)
            {
                setSessionTransferOutsideOperation(((SessionTransferOutsideOperationExt)another).getSessionTransferOutsideOperation());
            }
            setWorkTimeDisc(((SessionTransferOutsideOperationExt)another).getWorkTimeDisc());
        }
    }

    public INaturalId<SessionTransferOutsideOperationExtGen> getNaturalId()
    {
        return new NaturalId(getSessionTransferOutsideOperation());
    }

    public static class NaturalId extends NaturalIdBase<SessionTransferOutsideOperationExtGen>
    {
        private static final String PROXY_NAME = "SessionTransferOutsideOperationExtNaturalProxy";

        private Long _sessionTransferOutsideOperation;

        public NaturalId()
        {}

        public NaturalId(SessionTransferOutsideOperation sessionTransferOutsideOperation)
        {
            _sessionTransferOutsideOperation = ((IEntity) sessionTransferOutsideOperation).getId();
        }

        public Long getSessionTransferOutsideOperation()
        {
            return _sessionTransferOutsideOperation;
        }

        public void setSessionTransferOutsideOperation(Long sessionTransferOutsideOperation)
        {
            _sessionTransferOutsideOperation = sessionTransferOutsideOperation;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionTransferOutsideOperationExtGen.NaturalId) ) return false;

            SessionTransferOutsideOperationExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getSessionTransferOutsideOperation(), that.getSessionTransferOutsideOperation()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getSessionTransferOutsideOperation());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getSessionTransferOutsideOperation());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionTransferOutsideOperationExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionTransferOutsideOperationExt.class;
        }

        public T newInstance()
        {
            return (T) new SessionTransferOutsideOperationExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sessionTransferOutsideOperation":
                    return obj.getSessionTransferOutsideOperation();
                case "workTimeDisc":
                    return obj.getWorkTimeDisc();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sessionTransferOutsideOperation":
                    obj.setSessionTransferOutsideOperation((SessionTransferOutsideOperation) value);
                    return;
                case "workTimeDisc":
                    obj.setWorkTimeDisc((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sessionTransferOutsideOperation":
                        return true;
                case "workTimeDisc":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sessionTransferOutsideOperation":
                    return true;
                case "workTimeDisc":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sessionTransferOutsideOperation":
                    return SessionTransferOutsideOperation.class;
                case "workTimeDisc":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionTransferOutsideOperationExt> _dslPath = new Path<SessionTransferOutsideOperationExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionTransferOutsideOperationExt");
    }
            

    /**
     * @return Перезачтенное мероприятие из другого ОУ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt#getSessionTransferOutsideOperation()
     */
    public static SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation> sessionTransferOutsideOperation()
    {
        return _dslPath.sessionTransferOutsideOperation();
    }

    /**
     * @return Трудоемкость дисциплины.
     * @see ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt#getWorkTimeDisc()
     */
    public static PropertyPath<Integer> workTimeDisc()
    {
        return _dslPath.workTimeDisc();
    }

    public static class Path<E extends SessionTransferOutsideOperationExt> extends EntityPath<E>
    {
        private SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation> _sessionTransferOutsideOperation;
        private PropertyPath<Integer> _workTimeDisc;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Перезачтенное мероприятие из другого ОУ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt#getSessionTransferOutsideOperation()
     */
        public SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation> sessionTransferOutsideOperation()
        {
            if(_sessionTransferOutsideOperation == null )
                _sessionTransferOutsideOperation = new SessionTransferOutsideOperation.Path<SessionTransferOutsideOperation>(L_SESSION_TRANSFER_OUTSIDE_OPERATION, this);
            return _sessionTransferOutsideOperation;
        }

    /**
     * @return Трудоемкость дисциплины.
     * @see ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt#getWorkTimeDisc()
     */
        public PropertyPath<Integer> workTimeDisc()
        {
            if(_workTimeDisc == null )
                _workTimeDisc = new PropertyPath<Integer>(SessionTransferOutsideOperationExtGen.P_WORK_TIME_DISC, this);
            return _workTimeDisc;
        }

        public Class getEntityClass()
        {
            return SessionTransferOutsideOperationExt.class;
        }

        public String getEntityName()
        {
            return "sessionTransferOutsideOperationExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
