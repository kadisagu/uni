/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu11.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author Alexey Lopatin
 * @since 22.11.2013
 */
public class FefuCompensationTypeTransferExtractWrapper implements Comparable<FefuCompensationTypeTransferExtractWrapper>
{
    private Person _person;
    private DevelopForm _developForm;
    private Course _course;
    private Group _group;

    public FefuCompensationTypeTransferExtractWrapper(Person person, DevelopForm developForm, Course course, Group group)
    {
        _person = person;
        _developForm = developForm;
        _course = course;
        _group = group;
    }

    public Person getPerson()
    {
        return _person;
    }

    public void setPerson(Person person)
    {
        _person = person;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    @Override
    public int compareTo(FefuCompensationTypeTransferExtractWrapper o)
    {
        return Person.FULL_FIO_AND_ID_COMPARATOR.compare(_person, o.getPerson());
    }
}
