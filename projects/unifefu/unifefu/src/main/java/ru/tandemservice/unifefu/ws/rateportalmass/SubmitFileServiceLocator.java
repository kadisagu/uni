/**
 * SubmitFileServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportalmass;

import org.tandemframework.core.runtime.ApplicationRuntime;

public class SubmitFileServiceLocator extends org.apache.axis.client.Service implements ru.tandemservice.unifefu.ws.rateportalmass.SubmitFileService {

	public static final String PORTAL_SUBMIT_STATUS_SERVICE_URL = "fefu.portal.service.status.url";

    public SubmitFileServiceLocator() {
    }


    public SubmitFileServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SubmitFileServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SubmitFileSOAP11Port
    private java.lang.String SubmitFileSOAP11Port_address = ApplicationRuntime.getProperty(PORTAL_SUBMIT_STATUS_SERVICE_URL);;
    //private java.lang.String SubmitFileSOAP11Port_address = "http://77.239.225.71:19080/ip-adapter-mediationWeb/sca/SubmitFile";

    public java.lang.String getSubmitFileSOAP11PortAddress() {
        return SubmitFileSOAP11Port_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SubmitFileSOAP11PortWSDDServiceName = "SubmitFileSOAP11Port";

    public java.lang.String getSubmitFileSOAP11PortWSDDServiceName() {
        return SubmitFileSOAP11PortWSDDServiceName;
    }

    public void setSubmitFileSOAP11PortWSDDServiceName(java.lang.String name) {
        SubmitFileSOAP11PortWSDDServiceName = name;
    }

    public ru.tandemservice.unifefu.ws.rateportalmass.SubmitFile getSubmitFileSOAP11Port() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SubmitFileSOAP11Port_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSubmitFileSOAP11Port(endpoint);
    }

    public ru.tandemservice.unifefu.ws.rateportalmass.SubmitFile getSubmitFileSOAP11Port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ru.tandemservice.unifefu.ws.rateportalmass.SubmitFileSOAP11BindingStub _stub = new ru.tandemservice.unifefu.ws.rateportalmass.SubmitFileSOAP11BindingStub(portAddress, this);
            _stub.setPortName(getSubmitFileSOAP11PortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSubmitFileSOAP11PortEndpointAddress(java.lang.String address) {
        SubmitFileSOAP11Port_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ru.tandemservice.unifefu.ws.rateportalmass.SubmitFile.class.isAssignableFrom(serviceEndpointInterface)) {
                ru.tandemservice.unifefu.ws.rateportalmass.SubmitFileSOAP11BindingStub _stub = new ru.tandemservice.unifefu.ws.rateportalmass.SubmitFileSOAP11BindingStub(new java.net.URL(SubmitFileSOAP11Port_address), this);
                _stub.setPortName(getSubmitFileSOAP11PortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SubmitFileSOAP11Port".equals(inputPortName)) {
            return getSubmitFileSOAP11Port();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/submit-file/service", "SubmitFileService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/submit-file/service", "SubmitFileSOAP11Port"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SubmitFileSOAP11Port".equals(portName)) {
            setSubmitFileSOAP11PortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
