package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuNsiPersonContact

		// создана новая сущность
        if(!tool.tableExists("fefunsipersoncontact_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefunsipersoncontact_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("person_id", DBType.LONG).setNullable(false), 
				new DBColumn("address_id", DBType.LONG), 
				new DBColumn("type_id", DBType.LONG), 
				new DBColumn("typestr_p", DBType.createVarchar(255)), 
				new DBColumn("description_p", DBType.createVarchar(255)), 
				new DBColumn("field1_p", DBType.createVarchar(255)), 
				new DBColumn("field2_p", DBType.createVarchar(255)), 
				new DBColumn("field3_p", DBType.createVarchar(255)), 
				new DBColumn("field4_p", DBType.createVarchar(255)), 
				new DBColumn("field5_p", DBType.createVarchar(255)), 
				new DBColumn("field6_p", DBType.createVarchar(255)), 
				new DBColumn("field7_p", DBType.createVarchar(255)), 
				new DBColumn("field8_p", DBType.createVarchar(255)), 
				new DBColumn("field9_p", DBType.createVarchar(255)), 
				new DBColumn("field10_p", DBType.createVarchar(255)), 
				new DBColumn("comment_p", DBType.createVarchar(255)), 
				new DBColumn("defaultaddr_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("field_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuNsiPersonContact");

		}


    }
}