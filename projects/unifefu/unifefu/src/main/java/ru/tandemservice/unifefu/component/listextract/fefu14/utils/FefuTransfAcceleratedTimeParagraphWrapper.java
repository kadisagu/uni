/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu14.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 13.11.2014
 */
public class FefuTransfAcceleratedTimeParagraphWrapper implements Comparable<FefuTransfAcceleratedTimeParagraphWrapper>
    {
        private final Course _course;
        private final StudentCategory _studentCategory;
        private final Group _groupOld;
        private final Group _groupNew;
        private final CompensationType _compensationTypeOld;
        private final EducationOrgUnit _educationOrgUnitOld;
        private final EducationOrgUnit _educationOrgUnitNew;
        private final DevelopForm _developForm;
        private String season;
        private Date plannedDate;
        private final ListStudentExtract _firstExtract;

        private final List<Student> _personList = new ArrayList<>();

        public FefuTransfAcceleratedTimeParagraphWrapper(Course course, StudentCategory studentCategory, Group groupOld, Group groupNew, CompensationType compensationTypeOld, EducationOrgUnit educationOrgUnitOld, EducationOrgUnit educationOrgUnitNew, DevelopForm developForm, ListStudentExtract firstExtract)
        {
            _course = course;
            _studentCategory = studentCategory;
            _groupOld = groupOld;
            _groupNew = groupNew;
            _compensationTypeOld = compensationTypeOld;
            _educationOrgUnitOld = educationOrgUnitOld;
            _educationOrgUnitNew = educationOrgUnitNew;
            _developForm = developForm;
            _firstExtract = firstExtract;
        }

        public Course getCourse()
        {
            return _course;
        }

        public StudentCategory getStudentCategory()
        {
            return _studentCategory;
        }

        public Group getGroupOld()
        {
            return _groupOld;
        }

        public Group getGroupNew()
        {
            return _groupNew;
        }

        public CompensationType getCompensationTypeOld()
        {
            return _compensationTypeOld;
        }

        public String getSeason()
        {
            return season;
        }

        public void setSeason(String season)
        {
            this.season = season;
        }

        public Date getPlannedDate()
        {
            return plannedDate;
        }

        public void setPlannedDate(Date plannedDate)
        {
            this.plannedDate = plannedDate;
        }

        public EducationOrgUnit getEducationOrgUnitOld()
        {
            return _educationOrgUnitOld;
        }

        public EducationOrgUnit getEducationOrgUnitNew()
        {
            return _educationOrgUnitNew;
        }

        public DevelopForm getDevelopForm()
        {
            return _developForm;
        }

        public List<Student> getPersonList()
        {
            return _personList;
        }

        public ListStudentExtract getFirstExtract()
        {
            return _firstExtract;
        }

        @Override
        public boolean equals(Object o)
        {
            if (!(o instanceof FefuTransfAcceleratedTimeParagraphWrapper))
                return false;

            FefuTransfAcceleratedTimeParagraphWrapper that = (FefuTransfAcceleratedTimeParagraphWrapper) o;

            return _course.equals(that.getCourse())
                    && _studentCategory.equals(that.getStudentCategory())
                    && _groupOld.equals(that.getGroupOld())
                    && _groupNew.equals(that.getGroupNew())
                    && _compensationTypeOld.equals(that.getCompensationTypeOld())
                    && _educationOrgUnitOld.equals(that.getEducationOrgUnitOld())
                    && _educationOrgUnitNew.equals(that.getEducationOrgUnitNew())
                    && _developForm.equals(that.getDevelopForm());
        }

        @Override
        public int compareTo(FefuTransfAcceleratedTimeParagraphWrapper o)
        {
            int result = 0;
            if (_course.getIntValue() > o.getCourse().getIntValue())
                return 1;
            else if (_course.getIntValue() < o.getCourse().getIntValue())
                return -1;
            else
            {
                if (StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(o.getStudentCategory().getCode()))
                    return -1;
                else if (StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(o.getStudentCategory().getCode()))
                    return 1;
                else
                {
                    boolean isThisBudgetOld = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_compensationTypeOld.getCode());
                    boolean isThisContractOld = CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(_compensationTypeOld.getCode());

                    boolean isThatBudgetOld = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(o.getCompensationTypeOld().getCode());
                    boolean isThatContractOld = CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(o.getCompensationTypeOld().getCode());

                    if (isThisBudgetOld && isThatContractOld) return -1;
                    else if (isThisContractOld && isThatBudgetOld) return 1;
                    else
                    {
                        if (isThisBudgetOld)
                        {
                               if (result == 0)
                                    result = _educationOrgUnitOld.getEducationLevelHighSchool().getTitle().compareTo(o.getEducationOrgUnitOld().getEducationLevelHighSchool().getTitle());
                                if (result == 0)
                                    result = _educationOrgUnitNew.getEducationLevelHighSchool().getTitle().compareTo(o.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());

                        }
                        else if (isThisContractOld)
                        {

                                if (result == 0)
                                    result = _educationOrgUnitOld.getEducationLevelHighSchool().getTitle().compareTo(o.getEducationOrgUnitOld().getEducationLevelHighSchool().getTitle());
                                if (result == 0)
                                    result = _educationOrgUnitNew.getEducationLevelHighSchool().getTitle().compareTo(o.getEducationOrgUnitNew().getEducationLevelHighSchool().getTitle());

                        }
                    }
                }
            }
            return result;
        }


}