/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu16;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e86.utils.EmployeePostDecl;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract;

import java.util.*;

/**
 * @author nvankov
 * @since 11/20/13
 */
public class FullStateMaintenanceStuExtractPrint implements IPrintFormCreator<FullStateMaintenanceStuExtract>
{
    public static final Comparator<FefuOrphanPayment> FEFU_ORPHAN_PAYMENT_COMPARATOR = new Comparator<FefuOrphanPayment>()
    {
        @Override
        public int compare(FefuOrphanPayment o1, FefuOrphanPayment o2)
        {
            if(!o1.getType().equals(o2.getType())) return o1.getType().getPriority() - o2.getType().getPriority();
            else if(!o1.getPaymentSum().equals(o2.getPaymentSum()))
            {
	            return ((o1.getPaymentSum() - o2.getPaymentSum()) < 0) ? -1 : 1;
            }
            else return 0;
        }
    };

    @Override
    public RtfDocument createPrintForm(byte[] template, FullStateMaintenanceStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("protocol", extract.getProtocolNumAndDate());



        List<FefuOrphanPayment> payments = UnifefuDaoFacade.getFefuMoveStudentDAO().getOrphanPayments(extract.getId());

        if (payments.isEmpty())
            modifier.put("payments", "");
        else
        {
            RtfString paymentStr = new RtfString().append("2. " + modifier.getStringValue("fio_D_initials"));



            if(extract.isPaymentsForPeriod())
            {
                paymentStr.append(" произвести cледующие выплаты по статье 262 «Пособия по социальной помощи населению»:").par();
                Map<PaymentPeriodDate, List<FefuOrphanPayment>> paymentsMap = getPeriodPaymentsMap(payments);
                List<PaymentPeriodDate> dates = Lists.newArrayList(paymentsMap.keySet());
                Collections.sort(dates);
                for(PaymentPeriodDate periodDate : dates)
                {
                    paymentStr.append("\\qj\\fi500", true);
                    paymentStr.append(" - за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(periodDate.getStartDate()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(periodDate.getEndDate()));


                    List<FefuOrphanPayment> orphanPayments = paymentsMap.get(periodDate);
                    Collections.sort(orphanPayments, FEFU_ORPHAN_PAYMENT_COMPARATOR);

                    List<String> paymentPrints = Lists.newArrayList();
                    for(FefuOrphanPayment payment : orphanPayments)
                    {
                        paymentPrints.add(payment.getPrint() + " в размере " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(payment.getPaymentSumInRuble()) + " руб.");
                    }
                    paymentStr.append(" " + StringUtils.join(paymentPrints, ", "));
                    if(dates.size() != dates.indexOf(periodDate) + 1)
                        paymentStr.append(";");
                    paymentStr.par();
                }
            }
            else
            {
                paymentStr.append(" произвести выплату по статье 262 «Пособия по социальной помощи населению»:").par();
                Map<PaymentDate, List<FefuOrphanPayment>> paymentsMap = getPaymentsMap(payments);
                List<PaymentDate> dates = Lists.newArrayList(paymentsMap.keySet());
                Collections.sort(dates);
                for(PaymentDate periodDate : dates)
                {
                    paymentStr.append("\\qj\\fi500", true);
                    paymentStr.append("- за " + periodDate + " года:").par();
                    List<FefuOrphanPayment> orphanPayments = paymentsMap.get(periodDate);
                    Collections.sort(orphanPayments, FEFU_ORPHAN_PAYMENT_COMPARATOR);
                    for(FefuOrphanPayment payment : orphanPayments)
                    {
//                        paymentStr.append("\\qj\\fi500", true);
                        paymentStr.append(payment.getPrint() + " в размере " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(payment.getPaymentSumInRuble()) + " (" + payment.getPaymentSumPrint() + ") рублей");
                        if((dates.size() == dates.indexOf(periodDate) + 1) && (orphanPayments.size() == orphanPayments.indexOf(payment) + 1))  paymentStr.append(".");
                        else if(orphanPayments.size() == orphanPayments.indexOf(payment) + 1) paymentStr.append(";");
                        else paymentStr.append(",");
                        paymentStr.par();
                    }
                }
            }
            modifier.put("payments", paymentStr);
        }

        RtfString responsibleForPaymentsStr = new RtfString();
        responsibleForPaymentsStr.append(payments.isEmpty() ? String.valueOf(2) : String.valueOf(3)).append(". ");
        if(extract.isAgreePayments())
        {
            responsibleForPaymentsStr.append(StringUtils.capitalize(EmployeePostDecl.getEmployeeStrDat(extract.getResponsibleForAgreement())))
                    .append(" согласовать с " + EmployeePostDecl.getEmployeeStrInst(extract.getResponsibleForPayments()))
                    .append(" начисление " + modifier.getStringValue("fio_D_initials") + " c " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStartPaymentPeriodDate()))
                    .append(" соответствующих выплат, предусмотренных в рамках полного государственного обеспечения.");
        }
        else
        {
            responsibleForPaymentsStr.append(StringUtils.capitalize(EmployeePostDecl.getEmployeeStrDat(extract.getResponsibleForPayments())))
                    .append(" произвести начисление " + modifier.getStringValue("fio_D_initials"))
                    .append(" соответствующих выплат, предусмотренных в рамках полного государственного обеспечения, за период с ")
                    .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStartPaymentPeriodDate()) + ".");
        }
        modifier.put("responsibleForPayments", responsibleForPaymentsStr.par());

        modifier.put("parNumControl", payments.isEmpty() ? String.valueOf(3) : String.valueOf(4));
        modifier.put("enrollDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEnrollDate()));

        CommonExtractPrint.initFefuGroup(modifier, "fefuGroup", extract.getEntity().getGroup(), extract.getEntity().getEducationOrgUnit().getDevelopForm(), " группы ");
        CommonExtractPrint.modifyEducationStr(modifier, extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(), new String[]{"fefuEducationStrDirection", "educationStrDirection"}, false);

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    private Map<PaymentPeriodDate, List<FefuOrphanPayment>> getPeriodPaymentsMap(List<FefuOrphanPayment> payments)
    {
        Map<PaymentPeriodDate, List<FefuOrphanPayment>> paymentsMap = Maps.newHashMap();
        for(FefuOrphanPayment payment : payments)
        {
            PaymentPeriodDate paymentDate = new PaymentPeriodDate(payment.getPeriodStartDate(), payment.getPeriodEndDate());
            if(!paymentsMap.containsKey(paymentDate))
                paymentsMap.put(paymentDate, Lists.<FefuOrphanPayment>newArrayList());
            if(!paymentsMap.get(paymentDate).contains(payment))
                paymentsMap.get(paymentDate).add(payment);
        }

        return paymentsMap;
    }

    private Map<PaymentDate, List<FefuOrphanPayment>> getPaymentsMap(List<FefuOrphanPayment> payments)
    {
        Map<PaymentDate, List<FefuOrphanPayment>> paymentsMap = Maps.newHashMap();
        for(FefuOrphanPayment payment : payments)
        {
            PaymentDate paymentDate = new PaymentDate(payment.getPaymentMonth(), payment.getPaymentYear());
            if(!paymentsMap.containsKey(paymentDate))
                paymentsMap.put(paymentDate, Lists.<FefuOrphanPayment>newArrayList());
            if(!paymentsMap.get(paymentDate).contains(payment))
                paymentsMap.get(paymentDate).add(payment);
        }

        return paymentsMap;
    }

    private class PaymentPeriodDate implements Comparable<PaymentPeriodDate>
    {
        private Date _startDate;
        private Date _endDate;

        private PaymentPeriodDate(Date startDate, Date endDate)
        {
            _startDate = startDate;
            _endDate = endDate;
        }

        private Date getStartDate()
        {
            return _startDate;
        }

        private void setStartDate(Date startDate)
        {
            _startDate = startDate;
        }

        private Date getEndDate()
        {
            return _endDate;
        }

        private void setEndDate(Date endDate)
        {
            _endDate = endDate;
        }

        @Override
        public int compareTo(PaymentPeriodDate o)
        {
            if(!_startDate.equals(o.getStartDate())) return _startDate.compareTo(o.getStartDate());
            else if(!_endDate.equals(o.getEndDate())) return _endDate.compareTo(o.getEndDate());
            else return 0;
        }

        @Override
        public boolean equals(Object obj)
        {
            if(!(obj instanceof PaymentPeriodDate))
                return false;
            PaymentPeriodDate periodDate = (PaymentPeriodDate) obj;
            return _startDate.equals(periodDate.getStartDate()) && _endDate.equals(periodDate.getEndDate());
        }

        @Override
        public int hashCode()
        {
            return _startDate.hashCode()+ _endDate.hashCode();
        }
    }

    private class PaymentDate implements Comparable<PaymentDate>
    {
        private int _month;
        private int _year;

        PaymentDate(int month, int year)
        {
            _month = month;
            _year = year;
        }

        private int getMonth()
        {
            return _month;
        }

        private void setMonth(int month)
        {
            _month = month;
        }

        private int getYear()
        {
            return _year;
        }

        private void setYear(int year)
        {
            _year = year;
        }

        @Override
        public boolean equals(Object obj)
        {
            if(!(obj instanceof PaymentDate))
                return false;
            PaymentDate date = (PaymentDate) obj;
            return _month == date.getMonth() && _year == date.getYear();
        }

        @Override
        public int compareTo(PaymentDate o)
        {
            if(_year != o.getYear()) return _year - o.getYear();
            else if(_month != o.getMonth()) return _month - o.getMonth();
            else return 0;
        }

        @Override
        public String toString()
        {
            return CommonBaseDateUtil.getMonthNameDeclined(_month, GrammaCase.NOMINATIVE) + " " + String.valueOf(_year);
        }

        @Override
        public int hashCode()
        {
            return _month + _year * 100;
        }
    }
}