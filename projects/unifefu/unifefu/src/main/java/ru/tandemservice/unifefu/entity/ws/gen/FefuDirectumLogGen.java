package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantOrder;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumLog;
import ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension;
import ru.tandemservice.unifefu.entity.ws.FefuStudentOrderExtension;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка лога обмена данными с Directum
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuDirectumLogGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuDirectumLog";
    public static final String ENTITY_NAME = "fefuDirectumLog";
    public static final int VERSION_HASH = 2009404381;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String L_ENR_ORDER = "enrOrder";
    public static final String L_ORDER_EXT = "orderExt";
    public static final String L_ENR_ORDER_EXT = "enrOrderExt";
    public static final String L_EXECUTOR = "executor";
    public static final String P_EXECUTOR_STR = "executorStr";
    public static final String P_OPERATION_DATE_TIME = "operationDateTime";
    public static final String P_OPERATION_TYPE = "operationType";
    public static final String P_OPERATION_RESULT = "operationResult";
    public static final String P_DIRECTUM_ID = "directumId";
    public static final String P_DATAGRAM = "datagram";
    public static final String P_DIRECTUM_TASK_ID = "directumTaskId";
    public static final String P_DIRECTUM_ORDER_ID = "directumOrderId";

    private AbstractStudentOrder _order;     // Приказ по движению студентов
    private AbstractEntrantOrder _enrOrder;     // Приказ о зачислении абитуриентов
    private FefuStudentOrderExtension _orderExt;     // Расширение с данными студенческого приказа в Directum
    private FefuEntrantOrderExtension _enrOrderExt;     // Расширение с данными абитуриентского приказа в Directum
    private EmployeePost _executor;     // Ссылка на исполнителя
    private String _executorStr;     // Исполнитель
    private Date _operationDateTime;     // Дата и время выполнения операции
    private String _operationType;     // Вид операции
    private String _operationResult;     // Результат выполнения
    private String _directumId;     // Идентификатор в Directum
    private String _datagram;     // Отправленная датаграмма
    private String _directumTaskId;     // Идентификатор задачи в Directum
    private String _directumOrderId;     // Идентификатор документа из Directum

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ по движению студентов.
     */
    public AbstractStudentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ по движению студентов.
     */
    public void setOrder(AbstractStudentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Приказ о зачислении абитуриентов.
     */
    public AbstractEntrantOrder getEnrOrder()
    {
        return _enrOrder;
    }

    /**
     * @param enrOrder Приказ о зачислении абитуриентов.
     */
    public void setEnrOrder(AbstractEntrantOrder enrOrder)
    {
        dirty(_enrOrder, enrOrder);
        _enrOrder = enrOrder;
    }

    /**
     * @return Расширение с данными студенческого приказа в Directum.
     */
    public FefuStudentOrderExtension getOrderExt()
    {
        return _orderExt;
    }

    /**
     * @param orderExt Расширение с данными студенческого приказа в Directum.
     */
    public void setOrderExt(FefuStudentOrderExtension orderExt)
    {
        dirty(_orderExt, orderExt);
        _orderExt = orderExt;
    }

    /**
     * @return Расширение с данными абитуриентского приказа в Directum.
     */
    public FefuEntrantOrderExtension getEnrOrderExt()
    {
        return _enrOrderExt;
    }

    /**
     * @param enrOrderExt Расширение с данными абитуриентского приказа в Directum.
     */
    public void setEnrOrderExt(FefuEntrantOrderExtension enrOrderExt)
    {
        dirty(_enrOrderExt, enrOrderExt);
        _enrOrderExt = enrOrderExt;
    }

    /**
     * @return Ссылка на исполнителя.
     */
    public EmployeePost getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Ссылка на исполнителя.
     */
    public void setExecutor(EmployeePost executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Исполнитель. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getExecutorStr()
    {
        return _executorStr;
    }

    /**
     * @param executorStr Исполнитель. Свойство не может быть null.
     */
    public void setExecutorStr(String executorStr)
    {
        dirty(_executorStr, executorStr);
        _executorStr = executorStr;
    }

    /**
     * @return Дата и время выполнения операции. Свойство не может быть null.
     */
    @NotNull
    public Date getOperationDateTime()
    {
        return _operationDateTime;
    }

    /**
     * @param operationDateTime Дата и время выполнения операции. Свойство не может быть null.
     */
    public void setOperationDateTime(Date operationDateTime)
    {
        dirty(_operationDateTime, operationDateTime);
        _operationDateTime = operationDateTime;
    }

    /**
     * @return Вид операции. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOperationType()
    {
        return _operationType;
    }

    /**
     * @param operationType Вид операции. Свойство не может быть null.
     */
    public void setOperationType(String operationType)
    {
        dirty(_operationType, operationType);
        _operationType = operationType;
    }

    /**
     * @return Результат выполнения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getOperationResult()
    {
        return _operationResult;
    }

    /**
     * @param operationResult Результат выполнения. Свойство не может быть null.
     */
    public void setOperationResult(String operationResult)
    {
        dirty(_operationResult, operationResult);
        _operationResult = operationResult;
    }

    /**
     * @return Идентификатор в Directum.
     */
    @Length(max=255)
    public String getDirectumId()
    {
        return _directumId;
    }

    /**
     * @param directumId Идентификатор в Directum.
     */
    public void setDirectumId(String directumId)
    {
        dirty(_directumId, directumId);
        _directumId = directumId;
    }

    /**
     * @return Отправленная датаграмма.
     */
    public String getDatagram()
    {
        return _datagram;
    }

    /**
     * @param datagram Отправленная датаграмма.
     */
    public void setDatagram(String datagram)
    {
        dirty(_datagram, datagram);
        _datagram = datagram;
    }

    /**
     * @return Идентификатор задачи в Directum.
     */
    @Length(max=255)
    public String getDirectumTaskId()
    {
        return _directumTaskId;
    }

    /**
     * @param directumTaskId Идентификатор задачи в Directum.
     */
    public void setDirectumTaskId(String directumTaskId)
    {
        dirty(_directumTaskId, directumTaskId);
        _directumTaskId = directumTaskId;
    }

    /**
     * @return Идентификатор документа из Directum.
     */
    @Length(max=255)
    public String getDirectumOrderId()
    {
        return _directumOrderId;
    }

    /**
     * @param directumOrderId Идентификатор документа из Directum.
     */
    public void setDirectumOrderId(String directumOrderId)
    {
        dirty(_directumOrderId, directumOrderId);
        _directumOrderId = directumOrderId;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuDirectumLogGen)
        {
            setOrder(((FefuDirectumLog)another).getOrder());
            setEnrOrder(((FefuDirectumLog)another).getEnrOrder());
            setOrderExt(((FefuDirectumLog)another).getOrderExt());
            setEnrOrderExt(((FefuDirectumLog)another).getEnrOrderExt());
            setExecutor(((FefuDirectumLog)another).getExecutor());
            setExecutorStr(((FefuDirectumLog)another).getExecutorStr());
            setOperationDateTime(((FefuDirectumLog)another).getOperationDateTime());
            setOperationType(((FefuDirectumLog)another).getOperationType());
            setOperationResult(((FefuDirectumLog)another).getOperationResult());
            setDirectumId(((FefuDirectumLog)another).getDirectumId());
            setDatagram(((FefuDirectumLog)another).getDatagram());
            setDirectumTaskId(((FefuDirectumLog)another).getDirectumTaskId());
            setDirectumOrderId(((FefuDirectumLog)another).getDirectumOrderId());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuDirectumLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuDirectumLog.class;
        }

        public T newInstance()
        {
            return (T) new FefuDirectumLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "enrOrder":
                    return obj.getEnrOrder();
                case "orderExt":
                    return obj.getOrderExt();
                case "enrOrderExt":
                    return obj.getEnrOrderExt();
                case "executor":
                    return obj.getExecutor();
                case "executorStr":
                    return obj.getExecutorStr();
                case "operationDateTime":
                    return obj.getOperationDateTime();
                case "operationType":
                    return obj.getOperationType();
                case "operationResult":
                    return obj.getOperationResult();
                case "directumId":
                    return obj.getDirectumId();
                case "datagram":
                    return obj.getDatagram();
                case "directumTaskId":
                    return obj.getDirectumTaskId();
                case "directumOrderId":
                    return obj.getDirectumOrderId();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((AbstractStudentOrder) value);
                    return;
                case "enrOrder":
                    obj.setEnrOrder((AbstractEntrantOrder) value);
                    return;
                case "orderExt":
                    obj.setOrderExt((FefuStudentOrderExtension) value);
                    return;
                case "enrOrderExt":
                    obj.setEnrOrderExt((FefuEntrantOrderExtension) value);
                    return;
                case "executor":
                    obj.setExecutor((EmployeePost) value);
                    return;
                case "executorStr":
                    obj.setExecutorStr((String) value);
                    return;
                case "operationDateTime":
                    obj.setOperationDateTime((Date) value);
                    return;
                case "operationType":
                    obj.setOperationType((String) value);
                    return;
                case "operationResult":
                    obj.setOperationResult((String) value);
                    return;
                case "directumId":
                    obj.setDirectumId((String) value);
                    return;
                case "datagram":
                    obj.setDatagram((String) value);
                    return;
                case "directumTaskId":
                    obj.setDirectumTaskId((String) value);
                    return;
                case "directumOrderId":
                    obj.setDirectumOrderId((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "enrOrder":
                        return true;
                case "orderExt":
                        return true;
                case "enrOrderExt":
                        return true;
                case "executor":
                        return true;
                case "executorStr":
                        return true;
                case "operationDateTime":
                        return true;
                case "operationType":
                        return true;
                case "operationResult":
                        return true;
                case "directumId":
                        return true;
                case "datagram":
                        return true;
                case "directumTaskId":
                        return true;
                case "directumOrderId":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "enrOrder":
                    return true;
                case "orderExt":
                    return true;
                case "enrOrderExt":
                    return true;
                case "executor":
                    return true;
                case "executorStr":
                    return true;
                case "operationDateTime":
                    return true;
                case "operationType":
                    return true;
                case "operationResult":
                    return true;
                case "directumId":
                    return true;
                case "datagram":
                    return true;
                case "directumTaskId":
                    return true;
                case "directumOrderId":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return AbstractStudentOrder.class;
                case "enrOrder":
                    return AbstractEntrantOrder.class;
                case "orderExt":
                    return FefuStudentOrderExtension.class;
                case "enrOrderExt":
                    return FefuEntrantOrderExtension.class;
                case "executor":
                    return EmployeePost.class;
                case "executorStr":
                    return String.class;
                case "operationDateTime":
                    return Date.class;
                case "operationType":
                    return String.class;
                case "operationResult":
                    return String.class;
                case "directumId":
                    return String.class;
                case "datagram":
                    return String.class;
                case "directumTaskId":
                    return String.class;
                case "directumOrderId":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuDirectumLog> _dslPath = new Path<FefuDirectumLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuDirectumLog");
    }
            

    /**
     * @return Приказ по движению студентов.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getOrder()
     */
    public static AbstractStudentOrder.Path<AbstractStudentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Приказ о зачислении абитуриентов.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getEnrOrder()
     */
    public static AbstractEntrantOrder.Path<AbstractEntrantOrder> enrOrder()
    {
        return _dslPath.enrOrder();
    }

    /**
     * @return Расширение с данными студенческого приказа в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getOrderExt()
     */
    public static FefuStudentOrderExtension.Path<FefuStudentOrderExtension> orderExt()
    {
        return _dslPath.orderExt();
    }

    /**
     * @return Расширение с данными абитуриентского приказа в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getEnrOrderExt()
     */
    public static FefuEntrantOrderExtension.Path<FefuEntrantOrderExtension> enrOrderExt()
    {
        return _dslPath.enrOrderExt();
    }

    /**
     * @return Ссылка на исполнителя.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getExecutor()
     */
    public static EmployeePost.Path<EmployeePost> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Исполнитель. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getExecutorStr()
     */
    public static PropertyPath<String> executorStr()
    {
        return _dslPath.executorStr();
    }

    /**
     * @return Дата и время выполнения операции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getOperationDateTime()
     */
    public static PropertyPath<Date> operationDateTime()
    {
        return _dslPath.operationDateTime();
    }

    /**
     * @return Вид операции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getOperationType()
     */
    public static PropertyPath<String> operationType()
    {
        return _dslPath.operationType();
    }

    /**
     * @return Результат выполнения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getOperationResult()
     */
    public static PropertyPath<String> operationResult()
    {
        return _dslPath.operationResult();
    }

    /**
     * @return Идентификатор в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getDirectumId()
     */
    public static PropertyPath<String> directumId()
    {
        return _dslPath.directumId();
    }

    /**
     * @return Отправленная датаграмма.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getDatagram()
     */
    public static PropertyPath<String> datagram()
    {
        return _dslPath.datagram();
    }

    /**
     * @return Идентификатор задачи в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getDirectumTaskId()
     */
    public static PropertyPath<String> directumTaskId()
    {
        return _dslPath.directumTaskId();
    }

    /**
     * @return Идентификатор документа из Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getDirectumOrderId()
     */
    public static PropertyPath<String> directumOrderId()
    {
        return _dslPath.directumOrderId();
    }

    public static class Path<E extends FefuDirectumLog> extends EntityPath<E>
    {
        private AbstractStudentOrder.Path<AbstractStudentOrder> _order;
        private AbstractEntrantOrder.Path<AbstractEntrantOrder> _enrOrder;
        private FefuStudentOrderExtension.Path<FefuStudentOrderExtension> _orderExt;
        private FefuEntrantOrderExtension.Path<FefuEntrantOrderExtension> _enrOrderExt;
        private EmployeePost.Path<EmployeePost> _executor;
        private PropertyPath<String> _executorStr;
        private PropertyPath<Date> _operationDateTime;
        private PropertyPath<String> _operationType;
        private PropertyPath<String> _operationResult;
        private PropertyPath<String> _directumId;
        private PropertyPath<String> _datagram;
        private PropertyPath<String> _directumTaskId;
        private PropertyPath<String> _directumOrderId;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ по движению студентов.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getOrder()
     */
        public AbstractStudentOrder.Path<AbstractStudentOrder> order()
        {
            if(_order == null )
                _order = new AbstractStudentOrder.Path<AbstractStudentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Приказ о зачислении абитуриентов.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getEnrOrder()
     */
        public AbstractEntrantOrder.Path<AbstractEntrantOrder> enrOrder()
        {
            if(_enrOrder == null )
                _enrOrder = new AbstractEntrantOrder.Path<AbstractEntrantOrder>(L_ENR_ORDER, this);
            return _enrOrder;
        }

    /**
     * @return Расширение с данными студенческого приказа в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getOrderExt()
     */
        public FefuStudentOrderExtension.Path<FefuStudentOrderExtension> orderExt()
        {
            if(_orderExt == null )
                _orderExt = new FefuStudentOrderExtension.Path<FefuStudentOrderExtension>(L_ORDER_EXT, this);
            return _orderExt;
        }

    /**
     * @return Расширение с данными абитуриентского приказа в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getEnrOrderExt()
     */
        public FefuEntrantOrderExtension.Path<FefuEntrantOrderExtension> enrOrderExt()
        {
            if(_enrOrderExt == null )
                _enrOrderExt = new FefuEntrantOrderExtension.Path<FefuEntrantOrderExtension>(L_ENR_ORDER_EXT, this);
            return _enrOrderExt;
        }

    /**
     * @return Ссылка на исполнителя.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getExecutor()
     */
        public EmployeePost.Path<EmployeePost> executor()
        {
            if(_executor == null )
                _executor = new EmployeePost.Path<EmployeePost>(L_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Исполнитель. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getExecutorStr()
     */
        public PropertyPath<String> executorStr()
        {
            if(_executorStr == null )
                _executorStr = new PropertyPath<String>(FefuDirectumLogGen.P_EXECUTOR_STR, this);
            return _executorStr;
        }

    /**
     * @return Дата и время выполнения операции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getOperationDateTime()
     */
        public PropertyPath<Date> operationDateTime()
        {
            if(_operationDateTime == null )
                _operationDateTime = new PropertyPath<Date>(FefuDirectumLogGen.P_OPERATION_DATE_TIME, this);
            return _operationDateTime;
        }

    /**
     * @return Вид операции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getOperationType()
     */
        public PropertyPath<String> operationType()
        {
            if(_operationType == null )
                _operationType = new PropertyPath<String>(FefuDirectumLogGen.P_OPERATION_TYPE, this);
            return _operationType;
        }

    /**
     * @return Результат выполнения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getOperationResult()
     */
        public PropertyPath<String> operationResult()
        {
            if(_operationResult == null )
                _operationResult = new PropertyPath<String>(FefuDirectumLogGen.P_OPERATION_RESULT, this);
            return _operationResult;
        }

    /**
     * @return Идентификатор в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getDirectumId()
     */
        public PropertyPath<String> directumId()
        {
            if(_directumId == null )
                _directumId = new PropertyPath<String>(FefuDirectumLogGen.P_DIRECTUM_ID, this);
            return _directumId;
        }

    /**
     * @return Отправленная датаграмма.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getDatagram()
     */
        public PropertyPath<String> datagram()
        {
            if(_datagram == null )
                _datagram = new PropertyPath<String>(FefuDirectumLogGen.P_DATAGRAM, this);
            return _datagram;
        }

    /**
     * @return Идентификатор задачи в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getDirectumTaskId()
     */
        public PropertyPath<String> directumTaskId()
        {
            if(_directumTaskId == null )
                _directumTaskId = new PropertyPath<String>(FefuDirectumLogGen.P_DIRECTUM_TASK_ID, this);
            return _directumTaskId;
        }

    /**
     * @return Идентификатор документа из Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumLog#getDirectumOrderId()
     */
        public PropertyPath<String> directumOrderId()
        {
            if(_directumOrderId == null )
                _directumOrderId = new PropertyPath<String>(FefuDirectumLogGen.P_DIRECTUM_ORDER_ID, this);
            return _directumOrderId;
        }

        public Class getEntityClass()
        {
            return FefuDirectumLog.class;
        }

        public String getEntityName()
        {
            return "fefuDirectumLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
