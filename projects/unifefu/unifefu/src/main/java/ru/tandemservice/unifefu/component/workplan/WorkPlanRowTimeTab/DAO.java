/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.workplan.WorkPlanRowTimeTab;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.component.workplan.WorkPlanRowTimeTab.Model;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.dao.year.IEppYearDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraph;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow2EduPlan;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRowWeek;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 15.11.2013
 */
public class DAO extends ru.tandemservice.uniepp.component.workplan.WorkPlanRowTimeTab.DAO
{
    @Override
    public void doFillFromWorkGraph(final Model model)
    {
        EppWorkPlanBase wp = model.getEppWorkPlan();
        Map<Integer, List<FefuWorkGraphRowWeek>> partWeeksMap = getWorkGraphRowWeeks(wp.getWorkPlan());
        if (partWeeksMap.isEmpty()) { return; }

        EppYearEducationWeek[] weeks = IEppYearDAO.instance.get().getYearEducationWeeks(wp.getYear().getId());
        Map<Integer, Date[]> partDateMap = new HashMap<>(partWeeksMap.size());
        for (Map.Entry<Integer, List<FefuWorkGraphRowWeek>> e: partWeeksMap.entrySet())
        {
            int start = Integer.MAX_VALUE, finish = Integer.MIN_VALUE;
            for (FefuWorkGraphRowWeek gweek: e.getValue())
            {
                int i = gweek.getWeek();
                start = Math.min(i, start);
                finish = Math.max(i, finish);
            }

            if ((1 <= start) && (start <= finish) && (finish <= weeks.length))
            {
                partDateMap.put(e.getKey(), new Date[] { weeks[start-1].getDate(), weeks[finish-1].getEndDate() });
            }
        }

        if (partDateMap.isEmpty()) { return;}

        IEppWorkPlanWrapper wpWrapper = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(Collections.singleton(wp.getId())).get(wp.getId());
        Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> rowPartLoadDataMap = IEppWorkPlanDataDAO.instance.get().getRowPartLoadDataMap(wpWrapper.getRowMap().keySet());
        for (Map<Integer, Map<String, EppWorkPlanRowPartLoad>> m0 : rowPartLoadDataMap.values()) {
            for (Map<String, EppWorkPlanRowPartLoad> m1: m0.values()) {
                for (EppWorkPlanRowPartLoad load: m1.values()) {
                    Date[] dates = partDateMap.get(load.getPart());
                    if (null == dates)
                    {
                        load.setStudyPeriodStartDate(null);
                        load.setStudyPeriodFinishDate(null);

                    } else
                    {
                        load.intersectStudyPeriod(dates[0], dates[1]);
                    }
                }
            }
        }

        IEppWorkPlanDataDAO.instance.get().doUpdateRowPartLoadPeriodsMap(rowPartLoadDataMap);
    }

    private Map<Integer, List<FefuWorkGraphRowWeek>> getWorkGraphRowWeeks(EppWorkPlan workPlan)
    {
        Map<Integer, EppWorkPlanPart> partMap = IEppWorkPlanDAO.instance.get().getWorkPlanPartMap(workPlan.getId());
        if (partMap.isEmpty()) { return Collections.emptyMap(); }

        EppEduPlan eduPlan = workPlan.getEduPlan();
        List<FefuWorkGraphRowWeek> weeks = new DQLSelectBuilder()
                .fromEntity(FefuWorkGraphRowWeek.class, "rw")
                .column(property("rw"))
                .where(in(
                        property("rw", FefuWorkGraphRowWeek.row().id()),
                        new DQLSelectBuilder()
                                .fromEntity(FefuWorkGraphRow2EduPlan.class, "rel")
                                .column(property("rel", FefuWorkGraphRow2EduPlan.row().id()))
                                .where(eq(property("rel", FefuWorkGraphRow2EduPlan.eduPlanVersion()), value(workPlan.getEduPlanVersion())))
                                .buildQuery()))

                .joinPath(DQLJoinType.inner, FefuWorkGraphRowWeek.row().graph().fromAlias("rw"), "g")
                .where(eq(property("g", FefuWorkGraph.year()), value(workPlan.getYear())))
                .where(eq(property("g", FefuWorkGraph.developForm().programForm()), value(eduPlan.getProgramForm())))
                .where(eq(property("g", FefuWorkGraph.developCondition()), value(eduPlan.getDevelopCondition())))
                .where(eduPlan.getProgramTrait() == null ? isNull(property("g", FefuWorkGraph.developTech().programTrait())) : eq(property("g", FefuWorkGraph.developTech()), value(eduPlan.getProgramTrait())))
                .where(eq(property("g", FefuWorkGraph.developGrid()), value(eduPlan.getDevelopGrid())))
                .where(eq(property("rw", FefuWorkGraphRowWeek.term()), value(workPlan.getTerm())))
                .createStatement(getSession()).list();

        if (weeks.isEmpty()) { return Collections.emptyMap(); }

        Map<Integer, List<FefuWorkGraphRowWeek>> result = new HashMap<>(partMap.size());
        Iterator<FefuWorkGraphRowWeek> it = weeks.iterator();
        for (Map.Entry<Integer, EppWorkPlanPart> e: partMap.entrySet())
        {
            List<FefuWorkGraphRowWeek> resultList = new ArrayList<>(1 + (weeks.size() / 2));
            for (int i = e.getValue().calculateTotalWeeks(); i > 0; i--)
            {
                if (it.hasNext()) { resultList.add(it.next()); }
            }

            result.put(e.getKey(), resultList);
        }

        return result;
    }
}