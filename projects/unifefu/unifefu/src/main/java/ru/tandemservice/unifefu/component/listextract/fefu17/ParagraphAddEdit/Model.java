/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu17.ParagraphAddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOListExtract;

import java.util.Date;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 27.01.2015
 */
public class Model extends AbstractListParagraphAddEditModel<FefuTransfStuDPOListExtract>
{
    private List<Course> _courseListNew;
    private Course _courseNew;
    private Course _courseOld;
    private ISelectModel _educationOrgUnitModel;
    private ISelectModel _dpoProgramModel;
    private ISelectModel _dpoProgramOldModel;
    private EducationOrgUnit _educationOrgUnitNew;
    private IUploadFile _uploadFileOrder;
    private Date _transferDate;
    private FefuAdditionalProfessionalEducationProgram _dpoProgramNew;
    private FefuAdditionalProfessionalEducationProgram _dpoProgramOld;
    private Boolean _individualPlan;
    private boolean _canAddTemplate;

    public boolean isCanAddTemplate()
    {
        return _canAddTemplate;
    }

    public void setCanAddTemplate(boolean canAddTemplate)
    {
        _canAddTemplate = canAddTemplate;
    }

    public List<Course> getCourseListNew()
    {
        return _courseListNew;
    }

    public void setCourseListNew(List<Course> courseListNew)
    {
        _courseListNew = courseListNew;
    }

    public Boolean getIndividualPlan()
    {
        return _individualPlan;
    }

    public void setIndividualPlan(Boolean individualPlan)
    {
        _individualPlan = individualPlan;
    }

    public FefuAdditionalProfessionalEducationProgram getDpoProgramOld()
    {
        return _dpoProgramOld;
    }

    public void setDpoProgramOld(FefuAdditionalProfessionalEducationProgram dpoProgramOld)
    {
        _dpoProgramOld = dpoProgramOld;
    }

    public Course getCourseNew()
    {
        return _courseNew;
    }

    public void setCourseNew(Course courseNew)
    {
        _courseNew = courseNew;
    }

    public ISelectModel getDpoProgramOldModel()
    {
        return _dpoProgramOldModel;
    }

    public void setDpoProgramOldModel(ISelectModel dpoProgramOldModel)
    {
        _dpoProgramOldModel = dpoProgramOldModel;
    }

    public Date getTransferDate()
    {
        return _transferDate;
    }

    public void setTransferDate(Date transferDate)
    {
        _transferDate = transferDate;
    }

    public FefuAdditionalProfessionalEducationProgram getDpoProgramNew()
    {
        return _dpoProgramNew;
    }

    public void setDpoProgramNew(FefuAdditionalProfessionalEducationProgram dpoProgramNew)
    {
        _dpoProgramNew = dpoProgramNew;
    }

    public ISelectModel getEducationOrgUnitModel()
    {
        return _educationOrgUnitModel;
    }

    public void setEducationOrgUnitModel(ISelectModel educationOrgUnitModel)
    {
        _educationOrgUnitModel = educationOrgUnitModel;
    }

    public ISelectModel getDpoProgramModel()
    {
        return _dpoProgramModel;
    }

    public void setDpoProgramModel(ISelectModel dpoProgramModel)
    {
        _dpoProgramModel = dpoProgramModel;
    }

    public Course getCourseOld()
    {
        return _courseOld;
    }

    public void setCourseOld(Course courseOld)
    {
        _courseOld = courseOld;
    }

    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    public IUploadFile getUploadFileOrder()
    {
        return _uploadFileOrder;
    }

    public void setUploadFileOrder(IUploadFile uploadFileOrder)
    {
        _uploadFileOrder = uploadFileOrder;
    }

}