/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu10.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract;

/**
 * @author nvankov
 * @since 11/21/13
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FullStateMaintenanceEnrollmentStuListExtract, Model>
{
}
