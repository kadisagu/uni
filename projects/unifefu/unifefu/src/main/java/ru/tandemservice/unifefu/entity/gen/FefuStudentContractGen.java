package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.entity.FefuStudentContract;
import ru.tandemservice.unifefu.entity.ws.FefuContractor;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Договор ДВФУ (Студент)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuStudentContractGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuStudentContract";
    public static final String ENTITY_NAME = "fefuStudentContract";
    public static final int VERSION_HASH = 994590570;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTRACTOR = "contractor";
    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_CONTRACT_I_D = "contractID";
    public static final String P_CONTRACT_NAME = "contractName";
    public static final String P_CONTRACT_NUMBER = "contractNumber";
    public static final String P_CONTRACT_DATE = "contractDate";
    public static final String P_PAYER = "payer";
    public static final String P_RESPONSIBILITIES = "responsibilities";
    public static final String P_SHORT_CONTENT = "shortContent";
    public static final String P_ADDITIONAL_INFO = "additionalInfo";
    public static final String P_DATE_BEGIN = "dateBegin";
    public static final String P_DATE_END = "dateEnd";
    public static final String P_BALANCE = "balance";
    public static final String P_TERMINATED = "terminated";

    private FefuContractor _contractor;     // Контрагент
    private EmployeePost _employeePost;     // Ответственный сотрудник
    private OrgUnit _orgUnit;     // Подразделение
    private String _contractID;     // ИД договора в НСИ
    private String _contractName;     // Наименование договора в НСИ
    private String _contractNumber;     // Номер договора
    private Date _contractDate;     // Дата договора
    private String _payer;     // Плательщик по договору
    private String _responsibilities;     // Вид обязательства
    private String _shortContent;     // Краткое содержание
    private String _additionalInfo;     // Дополнительная информация
    private Date _dateBegin;     // Период действия с
    private Date _dateEnd;     // Период действия по
    private String _balance;     // Сальдо по договору
    private boolean _terminated;     // Договор расторгнут

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Контрагент.
     */
    public FefuContractor getContractor()
    {
        return _contractor;
    }

    /**
     * @param contractor Контрагент.
     */
    public void setContractor(FefuContractor contractor)
    {
        dirty(_contractor, contractor);
        _contractor = contractor;
    }

    /**
     * @return Ответственный сотрудник.
     */
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Ответственный сотрудник.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return ИД договора в НСИ.
     */
    @Length(max=255)
    public String getContractID()
    {
        return _contractID;
    }

    /**
     * @param contractID ИД договора в НСИ.
     */
    public void setContractID(String contractID)
    {
        dirty(_contractID, contractID);
        _contractID = contractID;
    }

    /**
     * @return Наименование договора в НСИ.
     */
    @Length(max=255)
    public String getContractName()
    {
        return _contractName;
    }

    /**
     * @param contractName Наименование договора в НСИ.
     */
    public void setContractName(String contractName)
    {
        dirty(_contractName, contractName);
        _contractName = contractName;
    }

    /**
     * @return Номер договора.
     */
    @Length(max=255)
    public String getContractNumber()
    {
        return _contractNumber;
    }

    /**
     * @param contractNumber Номер договора.
     */
    public void setContractNumber(String contractNumber)
    {
        dirty(_contractNumber, contractNumber);
        _contractNumber = contractNumber;
    }

    /**
     * @return Дата договора.
     */
    public Date getContractDate()
    {
        return _contractDate;
    }

    /**
     * @param contractDate Дата договора.
     */
    public void setContractDate(Date contractDate)
    {
        dirty(_contractDate, contractDate);
        _contractDate = contractDate;
    }

    /**
     * @return Плательщик по договору.
     */
    public String getPayer()
    {
        return _payer;
    }

    /**
     * @param payer Плательщик по договору.
     */
    public void setPayer(String payer)
    {
        dirty(_payer, payer);
        _payer = payer;
    }

    /**
     * @return Вид обязательства.
     */
    public String getResponsibilities()
    {
        return _responsibilities;
    }

    /**
     * @param responsibilities Вид обязательства.
     */
    public void setResponsibilities(String responsibilities)
    {
        dirty(_responsibilities, responsibilities);
        _responsibilities = responsibilities;
    }

    /**
     * @return Краткое содержание.
     */
    public String getShortContent()
    {
        return _shortContent;
    }

    /**
     * @param shortContent Краткое содержание.
     */
    public void setShortContent(String shortContent)
    {
        dirty(_shortContent, shortContent);
        _shortContent = shortContent;
    }

    /**
     * @return Дополнительная информация.
     */
    public String getAdditionalInfo()
    {
        return _additionalInfo;
    }

    /**
     * @param additionalInfo Дополнительная информация.
     */
    public void setAdditionalInfo(String additionalInfo)
    {
        dirty(_additionalInfo, additionalInfo);
        _additionalInfo = additionalInfo;
    }

    /**
     * @return Период действия с.
     */
    public Date getDateBegin()
    {
        return _dateBegin;
    }

    /**
     * @param dateBegin Период действия с.
     */
    public void setDateBegin(Date dateBegin)
    {
        dirty(_dateBegin, dateBegin);
        _dateBegin = dateBegin;
    }

    /**
     * @return Период действия по.
     */
    public Date getDateEnd()
    {
        return _dateEnd;
    }

    /**
     * @param dateEnd Период действия по.
     */
    public void setDateEnd(Date dateEnd)
    {
        dirty(_dateEnd, dateEnd);
        _dateEnd = dateEnd;
    }

    /**
     * @return Сальдо по договору.
     */
    @Length(max=255)
    public String getBalance()
    {
        return _balance;
    }

    /**
     * @param balance Сальдо по договору.
     */
    public void setBalance(String balance)
    {
        dirty(_balance, balance);
        _balance = balance;
    }

    /**
     * @return Договор расторгнут. Свойство не может быть null.
     */
    @NotNull
    public boolean isTerminated()
    {
        return _terminated;
    }

    /**
     * @param terminated Договор расторгнут. Свойство не может быть null.
     */
    public void setTerminated(boolean terminated)
    {
        dirty(_terminated, terminated);
        _terminated = terminated;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuStudentContractGen)
        {
            setContractor(((FefuStudentContract)another).getContractor());
            setEmployeePost(((FefuStudentContract)another).getEmployeePost());
            setOrgUnit(((FefuStudentContract)another).getOrgUnit());
            setContractID(((FefuStudentContract)another).getContractID());
            setContractName(((FefuStudentContract)another).getContractName());
            setContractNumber(((FefuStudentContract)another).getContractNumber());
            setContractDate(((FefuStudentContract)another).getContractDate());
            setPayer(((FefuStudentContract)another).getPayer());
            setResponsibilities(((FefuStudentContract)another).getResponsibilities());
            setShortContent(((FefuStudentContract)another).getShortContent());
            setAdditionalInfo(((FefuStudentContract)another).getAdditionalInfo());
            setDateBegin(((FefuStudentContract)another).getDateBegin());
            setDateEnd(((FefuStudentContract)another).getDateEnd());
            setBalance(((FefuStudentContract)another).getBalance());
            setTerminated(((FefuStudentContract)another).isTerminated());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuStudentContractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuStudentContract.class;
        }

        public T newInstance()
        {
            return (T) new FefuStudentContract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "contractor":
                    return obj.getContractor();
                case "employeePost":
                    return obj.getEmployeePost();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "contractID":
                    return obj.getContractID();
                case "contractName":
                    return obj.getContractName();
                case "contractNumber":
                    return obj.getContractNumber();
                case "contractDate":
                    return obj.getContractDate();
                case "payer":
                    return obj.getPayer();
                case "responsibilities":
                    return obj.getResponsibilities();
                case "shortContent":
                    return obj.getShortContent();
                case "additionalInfo":
                    return obj.getAdditionalInfo();
                case "dateBegin":
                    return obj.getDateBegin();
                case "dateEnd":
                    return obj.getDateEnd();
                case "balance":
                    return obj.getBalance();
                case "terminated":
                    return obj.isTerminated();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "contractor":
                    obj.setContractor((FefuContractor) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "contractID":
                    obj.setContractID((String) value);
                    return;
                case "contractName":
                    obj.setContractName((String) value);
                    return;
                case "contractNumber":
                    obj.setContractNumber((String) value);
                    return;
                case "contractDate":
                    obj.setContractDate((Date) value);
                    return;
                case "payer":
                    obj.setPayer((String) value);
                    return;
                case "responsibilities":
                    obj.setResponsibilities((String) value);
                    return;
                case "shortContent":
                    obj.setShortContent((String) value);
                    return;
                case "additionalInfo":
                    obj.setAdditionalInfo((String) value);
                    return;
                case "dateBegin":
                    obj.setDateBegin((Date) value);
                    return;
                case "dateEnd":
                    obj.setDateEnd((Date) value);
                    return;
                case "balance":
                    obj.setBalance((String) value);
                    return;
                case "terminated":
                    obj.setTerminated((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "contractor":
                        return true;
                case "employeePost":
                        return true;
                case "orgUnit":
                        return true;
                case "contractID":
                        return true;
                case "contractName":
                        return true;
                case "contractNumber":
                        return true;
                case "contractDate":
                        return true;
                case "payer":
                        return true;
                case "responsibilities":
                        return true;
                case "shortContent":
                        return true;
                case "additionalInfo":
                        return true;
                case "dateBegin":
                        return true;
                case "dateEnd":
                        return true;
                case "balance":
                        return true;
                case "terminated":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "contractor":
                    return true;
                case "employeePost":
                    return true;
                case "orgUnit":
                    return true;
                case "contractID":
                    return true;
                case "contractName":
                    return true;
                case "contractNumber":
                    return true;
                case "contractDate":
                    return true;
                case "payer":
                    return true;
                case "responsibilities":
                    return true;
                case "shortContent":
                    return true;
                case "additionalInfo":
                    return true;
                case "dateBegin":
                    return true;
                case "dateEnd":
                    return true;
                case "balance":
                    return true;
                case "terminated":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "contractor":
                    return FefuContractor.class;
                case "employeePost":
                    return EmployeePost.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "contractID":
                    return String.class;
                case "contractName":
                    return String.class;
                case "contractNumber":
                    return String.class;
                case "contractDate":
                    return Date.class;
                case "payer":
                    return String.class;
                case "responsibilities":
                    return String.class;
                case "shortContent":
                    return String.class;
                case "additionalInfo":
                    return String.class;
                case "dateBegin":
                    return Date.class;
                case "dateEnd":
                    return Date.class;
                case "balance":
                    return String.class;
                case "terminated":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuStudentContract> _dslPath = new Path<FefuStudentContract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuStudentContract");
    }
            

    /**
     * @return Контрагент.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getContractor()
     */
    public static FefuContractor.Path<FefuContractor> contractor()
    {
        return _dslPath.contractor();
    }

    /**
     * @return Ответственный сотрудник.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return ИД договора в НСИ.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getContractID()
     */
    public static PropertyPath<String> contractID()
    {
        return _dslPath.contractID();
    }

    /**
     * @return Наименование договора в НСИ.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getContractName()
     */
    public static PropertyPath<String> contractName()
    {
        return _dslPath.contractName();
    }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getContractNumber()
     */
    public static PropertyPath<String> contractNumber()
    {
        return _dslPath.contractNumber();
    }

    /**
     * @return Дата договора.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getContractDate()
     */
    public static PropertyPath<Date> contractDate()
    {
        return _dslPath.contractDate();
    }

    /**
     * @return Плательщик по договору.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getPayer()
     */
    public static PropertyPath<String> payer()
    {
        return _dslPath.payer();
    }

    /**
     * @return Вид обязательства.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getResponsibilities()
     */
    public static PropertyPath<String> responsibilities()
    {
        return _dslPath.responsibilities();
    }

    /**
     * @return Краткое содержание.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getShortContent()
     */
    public static PropertyPath<String> shortContent()
    {
        return _dslPath.shortContent();
    }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getAdditionalInfo()
     */
    public static PropertyPath<String> additionalInfo()
    {
        return _dslPath.additionalInfo();
    }

    /**
     * @return Период действия с.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getDateBegin()
     */
    public static PropertyPath<Date> dateBegin()
    {
        return _dslPath.dateBegin();
    }

    /**
     * @return Период действия по.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getDateEnd()
     */
    public static PropertyPath<Date> dateEnd()
    {
        return _dslPath.dateEnd();
    }

    /**
     * @return Сальдо по договору.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getBalance()
     */
    public static PropertyPath<String> balance()
    {
        return _dslPath.balance();
    }

    /**
     * @return Договор расторгнут. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#isTerminated()
     */
    public static PropertyPath<Boolean> terminated()
    {
        return _dslPath.terminated();
    }

    public static class Path<E extends FefuStudentContract> extends EntityPath<E>
    {
        private FefuContractor.Path<FefuContractor> _contractor;
        private EmployeePost.Path<EmployeePost> _employeePost;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _contractID;
        private PropertyPath<String> _contractName;
        private PropertyPath<String> _contractNumber;
        private PropertyPath<Date> _contractDate;
        private PropertyPath<String> _payer;
        private PropertyPath<String> _responsibilities;
        private PropertyPath<String> _shortContent;
        private PropertyPath<String> _additionalInfo;
        private PropertyPath<Date> _dateBegin;
        private PropertyPath<Date> _dateEnd;
        private PropertyPath<String> _balance;
        private PropertyPath<Boolean> _terminated;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Контрагент.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getContractor()
     */
        public FefuContractor.Path<FefuContractor> contractor()
        {
            if(_contractor == null )
                _contractor = new FefuContractor.Path<FefuContractor>(L_CONTRACTOR, this);
            return _contractor;
        }

    /**
     * @return Ответственный сотрудник.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return ИД договора в НСИ.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getContractID()
     */
        public PropertyPath<String> contractID()
        {
            if(_contractID == null )
                _contractID = new PropertyPath<String>(FefuStudentContractGen.P_CONTRACT_I_D, this);
            return _contractID;
        }

    /**
     * @return Наименование договора в НСИ.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getContractName()
     */
        public PropertyPath<String> contractName()
        {
            if(_contractName == null )
                _contractName = new PropertyPath<String>(FefuStudentContractGen.P_CONTRACT_NAME, this);
            return _contractName;
        }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getContractNumber()
     */
        public PropertyPath<String> contractNumber()
        {
            if(_contractNumber == null )
                _contractNumber = new PropertyPath<String>(FefuStudentContractGen.P_CONTRACT_NUMBER, this);
            return _contractNumber;
        }

    /**
     * @return Дата договора.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getContractDate()
     */
        public PropertyPath<Date> contractDate()
        {
            if(_contractDate == null )
                _contractDate = new PropertyPath<Date>(FefuStudentContractGen.P_CONTRACT_DATE, this);
            return _contractDate;
        }

    /**
     * @return Плательщик по договору.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getPayer()
     */
        public PropertyPath<String> payer()
        {
            if(_payer == null )
                _payer = new PropertyPath<String>(FefuStudentContractGen.P_PAYER, this);
            return _payer;
        }

    /**
     * @return Вид обязательства.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getResponsibilities()
     */
        public PropertyPath<String> responsibilities()
        {
            if(_responsibilities == null )
                _responsibilities = new PropertyPath<String>(FefuStudentContractGen.P_RESPONSIBILITIES, this);
            return _responsibilities;
        }

    /**
     * @return Краткое содержание.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getShortContent()
     */
        public PropertyPath<String> shortContent()
        {
            if(_shortContent == null )
                _shortContent = new PropertyPath<String>(FefuStudentContractGen.P_SHORT_CONTENT, this);
            return _shortContent;
        }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getAdditionalInfo()
     */
        public PropertyPath<String> additionalInfo()
        {
            if(_additionalInfo == null )
                _additionalInfo = new PropertyPath<String>(FefuStudentContractGen.P_ADDITIONAL_INFO, this);
            return _additionalInfo;
        }

    /**
     * @return Период действия с.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getDateBegin()
     */
        public PropertyPath<Date> dateBegin()
        {
            if(_dateBegin == null )
                _dateBegin = new PropertyPath<Date>(FefuStudentContractGen.P_DATE_BEGIN, this);
            return _dateBegin;
        }

    /**
     * @return Период действия по.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getDateEnd()
     */
        public PropertyPath<Date> dateEnd()
        {
            if(_dateEnd == null )
                _dateEnd = new PropertyPath<Date>(FefuStudentContractGen.P_DATE_END, this);
            return _dateEnd;
        }

    /**
     * @return Сальдо по договору.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#getBalance()
     */
        public PropertyPath<String> balance()
        {
            if(_balance == null )
                _balance = new PropertyPath<String>(FefuStudentContractGen.P_BALANCE, this);
            return _balance;
        }

    /**
     * @return Договор расторгнут. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentContract#isTerminated()
     */
        public PropertyPath<Boolean> terminated()
        {
            if(_terminated == null )
                _terminated = new PropertyPath<Boolean>(FefuStudentContractGen.P_TERMINATED, this);
            return _terminated;
        }

        public Class getEntityClass()
        {
            return FefuStudentContract.class;
        }

        public String getEntityName()
        {
            return "fefuStudentContract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
