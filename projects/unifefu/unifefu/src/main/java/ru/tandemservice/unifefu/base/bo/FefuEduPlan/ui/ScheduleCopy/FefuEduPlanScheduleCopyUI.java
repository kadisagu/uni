/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.ScheduleCopy;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 23.10.2014
 */
@Input(@Bind(key = "version", binding = "version.id"))
public class FefuEduPlanScheduleCopyUI extends UIPresenter
{
    private EppEduPlanVersion _version = new EppEduPlanVersion();
    private EppEduPlanVersion _source;
    private Collection<Course> _courses;
    private Course _course;
    private Collection<Course> _selectedCourses;

    public EppEduPlanVersion getVersion() { return _version; }

    public EppEduPlanVersion getSource() { return _source; }
    public void setSource(EppEduPlanVersion source) { _source = source; }

    public Course getCourse() { return _course; }
    public void setCourse(Course course) { _course = course; }

    public Collection<Course> getCourses() { return _courses; }

    public boolean isCourseSelected()
    {
        return _selectedCourses.contains(getCourse());
    }

    public void setCourseSelected(boolean courseSelected)
    {
        if (courseSelected) _selectedCourses.add(getCourse());
        else  _selectedCourses.remove(getCourse());
    }

    public boolean isShowCourseBlock()
    {
        return !_courses.isEmpty();
    }

    @Override
    public void onComponentRefresh()
    {
        _version = IUniBaseDao.instance.get().get(EppEduPlanVersion.class, _version.getId());
        prepareEpvCourses();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(FefuEduPlanScheduleCopy.BIND_VERSION, _version.getId());
    }

    public void onChangeSourceEduPlanVersion()
    {
        prepareEpvCourses();
    }

    private void prepareEpvCourses()
    {
        _courses = new ArrayList<>();
        if (_source != null)
            _courses.addAll(IUniBaseDao.instance.get().<Course>getList(
                new DQLSelectBuilder()
                    .fromEntity(Course.class, "c")
                    .where(exists(
                        new DQLSelectBuilder()
                            .fromEntity(FefuEduPlanVersionWeek.class, "w")
                            .where(eq(property("w", FefuEduPlanVersionWeek.course()), property("c")))
                            .where(eq(property("w", FefuEduPlanVersionWeek.version()), value(_source)))
                            .buildQuery()))
                    .order(property("c", Course.intValue()))));

        _selectedCourses = new HashSet<>(_courses);
    }

    public void onClickApply()
    {
        if (_courses.isEmpty())
            throw new ApplicationException("У выбранной версии учебного плана не заполнен учебный график.");

        if (_selectedCourses.isEmpty())
            throw new ApplicationException("Выберите хотя бы один курс для копирования.");

        FefuEduPlanManager.instance().dao().doCopyEduPlanVersionSchedule(_source, _version, _selectedCourses);
        this.deactivate();
    }
}