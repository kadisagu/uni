package ru.tandemservice.unifefu.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport;
import ru.tandemservice.unifefu.entity.report.IFefuBrsReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Сводные данные балльно-рейтинговой системы»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuBrsSumDataReportGen extends EntityBase
 implements IFefuBrsReport{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport";
    public static final String ENTITY_NAME = "fefuBrsSumDataReport";
    public static final int VERSION_HASH = -2024004392;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_YEAR_PART = "yearPart";
    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_EDU_LEVEL = "eduLevel";
    public static final String P_GROUP = "group";
    public static final String P_FORMING_DATE_STR = "formingDateStr";

    private OrgUnit _orgUnit;     // Подразделение
    private String _yearPart;     // Часть учебного года
    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private String _formativeOrgUnit;     // Формирующее подразделение
    private String _developForm;     // Форма освоения
    private String _eduLevel;     // Направление подготовки
    private String _group;     // Группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Часть учебного года.
     */
    @Length(max=255)
    public String getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть учебного года.
     */
    public void setYearPart(String yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Формирующее подразделение.
     */
    @Length(max=1000)
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение.
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Направление подготовки.
     */
    @Length(max=3000)
    public String getEduLevel()
    {
        return _eduLevel;
    }

    /**
     * @param eduLevel Направление подготовки.
     */
    public void setEduLevel(String eduLevel)
    {
        dirty(_eduLevel, eduLevel);
        _eduLevel = eduLevel;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuBrsSumDataReportGen)
        {
            setOrgUnit(((FefuBrsSumDataReport)another).getOrgUnit());
            setYearPart(((FefuBrsSumDataReport)another).getYearPart());
            setContent(((FefuBrsSumDataReport)another).getContent());
            setFormingDate(((FefuBrsSumDataReport)another).getFormingDate());
            setFormativeOrgUnit(((FefuBrsSumDataReport)another).getFormativeOrgUnit());
            setDevelopForm(((FefuBrsSumDataReport)another).getDevelopForm());
            setEduLevel(((FefuBrsSumDataReport)another).getEduLevel());
            setGroup(((FefuBrsSumDataReport)another).getGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuBrsSumDataReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuBrsSumDataReport.class;
        }

        public T newInstance()
        {
            return (T) new FefuBrsSumDataReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "yearPart":
                    return obj.getYearPart();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "developForm":
                    return obj.getDevelopForm();
                case "eduLevel":
                    return obj.getEduLevel();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "yearPart":
                    obj.setYearPart((String) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "eduLevel":
                    obj.setEduLevel((String) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "yearPart":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "developForm":
                        return true;
                case "eduLevel":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "yearPart":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "developForm":
                    return true;
                case "eduLevel":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "yearPart":
                    return String.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "formativeOrgUnit":
                    return String.class;
                case "developForm":
                    return String.class;
                case "eduLevel":
                    return String.class;
                case "group":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuBrsSumDataReport> _dslPath = new Path<FefuBrsSumDataReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuBrsSumDataReport");
    }
            

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getYearPart()
     */
    public static PropertyPath<String> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Направление подготовки.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getEduLevel()
     */
    public static PropertyPath<String> eduLevel()
    {
        return _dslPath.eduLevel();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getFormingDateStr()
     */
    public static SupportedPropertyPath<String> formingDateStr()
    {
        return _dslPath.formingDateStr();
    }

    public static class Path<E extends FefuBrsSumDataReport> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _yearPart;
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _eduLevel;
        private PropertyPath<String> _group;
        private SupportedPropertyPath<String> _formingDateStr;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getYearPart()
     */
        public PropertyPath<String> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new PropertyPath<String>(FefuBrsSumDataReportGen.P_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(FefuBrsSumDataReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(FefuBrsSumDataReportGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(FefuBrsSumDataReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Направление подготовки.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getEduLevel()
     */
        public PropertyPath<String> eduLevel()
        {
            if(_eduLevel == null )
                _eduLevel = new PropertyPath<String>(FefuBrsSumDataReportGen.P_EDU_LEVEL, this);
            return _eduLevel;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(FefuBrsSumDataReportGen.P_GROUP, this);
            return _group;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport#getFormingDateStr()
     */
        public SupportedPropertyPath<String> formingDateStr()
        {
            if(_formingDateStr == null )
                _formingDateStr = new SupportedPropertyPath<String>(FefuBrsSumDataReportGen.P_FORMING_DATE_STR, this);
            return _formingDateStr;
        }

        public Class getEntityClass()
        {
            return FefuBrsSumDataReport.class;
        }

        public String getEntityName()
        {
            return "fefuBrsSumDataReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFormingDateStr();
}
