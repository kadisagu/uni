package ru.tandemservice.unifefu.component.catalog.fefuPromotedActivityType.FefuPromotedActivityTypePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuPromotedActivityType;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public class DAO extends DefaultCatalogPubDAO<FefuPromotedActivityType, Model> implements IDAO
{

}
