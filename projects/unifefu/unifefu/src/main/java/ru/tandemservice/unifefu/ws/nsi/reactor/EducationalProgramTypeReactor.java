/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.*;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 26.08.2013
 */
public class EducationalProgramTypeReactor extends SimpleCatalogEntityReactor<EducationalProgramType, EducationOrgUnit>
{
    private Map<String, DepartmentType> departmentsMap = new HashMap<>();
    private Map<String, EducationFormType> educationFormsMap = new HashMap<>();
    private Map<String, DevelopConditionType> developConditionsMap = new HashMap<>();
    private Map<String, DevelopTechType> developTechsMap = new HashMap<>();
    private Map<String, DevelopPeriodType> developPeriodsMap = new HashMap<>();

    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<EducationOrgUnit> getEntityClass()
    {
        return EducationOrgUnit.class;
    }

    @Override
    public Class<EducationalProgramType> getNSIEntityClass()
    {
        return EducationalProgramType.class;
    }

    @Override
    public String getGUID(EducationalProgramType nsiEntity)
    {
        return nsiEntity.getID();
    }

    @Override
    public EducationalProgramType getCatalogElementRetrieveRequestObject(String guid)
    {
        if (null == guid) return null;
        EducationalProgramType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createEducationalProgramType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    protected boolean useSmallPackage()
    {
        return true;
    }

    protected Map<String, EducationOrgUnit> getEntityMap(Class<EducationOrgUnit> entityClass)
    {
        return getEntityMap(entityClass, null);
    }

    @Override
    protected Map<String, EducationOrgUnit> getEntityMap(final Class<EducationOrgUnit> entityClass, Set<Long> entityIds)
    {
        Map<String, EducationOrgUnit> result = new HashMap<>();
        final List<EducationOrgUnit> entityList = new ArrayList<>();

        if (null == entityIds)
        {
            entityList.addAll(DataAccessServices.dao().<EducationOrgUnit>getList(
                    new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
            ));
        } else
        {
            BatchUtils.execute(entityIds, 100, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> entityIdsPortion)
                {
                    entityList.addAll(DataAccessServices.dao().<EducationOrgUnit>getList(
                            new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
                                    .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                    ));
                }
            });
        }

        for (EducationOrgUnit item : entityList/*.subList(0, 3)*/)
        {
            result.put(item.getId().toString(), item);
            result.put(EducationOrgUnit.title().s() + item.getTitle().trim().toUpperCase(), item);
        }

        return result;
    }

    @Override
    protected void prepareRelatedObjectsMap()
    {
        List<FefuNsiIds> depNsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(OrgUnit.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : depNsiIdsList)
        {
            DepartmentType depType = NsiReactorUtils.NSI_OBJECT_FACTORY.createDepartmentType();
            depType.setID(nsiId.getGuid());
            departmentsMap.put(nsiId.getGuid(), depType);
            departmentsMap.put(String.valueOf(nsiId.getEntityId()), depType);
        }


        List<FefuNsiIds> dfNsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(DevelopForm.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : dfNsiIdsList)
        {
            EducationFormType efType = NsiReactorUtils.NSI_OBJECT_FACTORY.createEducationFormType();
            efType.setID(nsiId.getGuid());
            educationFormsMap.put(nsiId.getGuid(), efType);
            educationFormsMap.put(String.valueOf(nsiId.getEntityId()), efType);
        }


        List<FefuNsiIds> dcNsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(DevelopCondition.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : dcNsiIdsList)
        {
            DevelopConditionType dcType = NsiReactorUtils.NSI_OBJECT_FACTORY.createDevelopConditionType();
            dcType.setID(nsiId.getGuid());
            developConditionsMap.put(nsiId.getGuid(), dcType);
            developConditionsMap.put(String.valueOf(nsiId.getEntityId()), dcType);
        }


        List<FefuNsiIds> dtNsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(DevelopTech.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : dtNsiIdsList)
        {
            DevelopTechType dtType = NsiReactorUtils.NSI_OBJECT_FACTORY.createDevelopTechType();
            dtType.setID(nsiId.getGuid());
            developTechsMap.put(nsiId.getGuid(), dtType);
            developTechsMap.put(String.valueOf(nsiId.getEntityId()), dtType);
        }


        List<FefuNsiIds> dpNsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(DevelopPeriod.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : dpNsiIdsList)
        {
            DevelopPeriodType dpType = NsiReactorUtils.NSI_OBJECT_FACTORY.createDevelopPeriodType();
            dpType.setID(nsiId.getGuid());
            developPeriodsMap.put(nsiId.getGuid(), dpType);
            developPeriodsMap.put(String.valueOf(nsiId.getEntityId()), dpType);
        }
    }

    @Override
    public EducationOrgUnit getPossibleDuplicate(EducationalProgramType nsiEntity, Map<String, FefuNsiIds> nsiIdsMap, Map<String, EducationOrgUnit> similarEntityMap)
    {
        if (null == nsiEntity.getID()) return null;

        // Проверяем, есть ли в базе аналогичные переданной сущности
        EducationOrgUnit possibleDuplicate = null;

        if (nsiIdsMap.containsKey(nsiEntity.getID()))
        {
            possibleDuplicate = similarEntityMap.get(String.valueOf(nsiIdsMap.get(nsiEntity.getID()).getEntityId()));
        }

        return possibleDuplicate;
    }

    @Override
    public boolean isAnythingChanged(EducationalProgramType nsiEntity, EducationOrgUnit entity, Map<String, EducationOrgUnit> similarEntityMap)
    {
        if (null == nsiEntity.getID()) return false;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getEducationalProgramID(), String.valueOf(entity.getCode())))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getEducationalProgramName(), entity.getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle()))
                return true;

            StructureEducationLevels lvl = entity.getEducationLevelHighSchool().getEducationLevel().getLevelType();
            while(null != lvl.getParent()) lvl = lvl.getParent();

            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getGenerationGos(), lvl.getShortTitle()))
                return true;

            DepartmentType formativeOrgUnitDep = departmentsMap.get(entity.getFormativeOrgUnit().getId().toString());
            if (null != formativeOrgUnitDep && (null == nsiEntity.getDepartmentID() || !formativeOrgUnitDep.getID().equals(nsiEntity.getDepartmentID().getDepartment().getID())))
                return true;

            DepartmentType territorialOrgUnitDep = departmentsMap.get(entity.getTerritorialOrgUnit().getId().toString());
            if (null != territorialOrgUnitDep && (null == nsiEntity.getTerritorialDepartmentID() || !territorialOrgUnitDep.getID().equals(nsiEntity.getTerritorialDepartmentID().getDepartment().getID())))
                return true;

            if (null != entity.getGroupOrgUnit())
            {
                DepartmentType deansOfficeDep = departmentsMap.get(entity.getGroupOrgUnit().getId().toString());
                if (null != deansOfficeDep && (null == nsiEntity.getDeansOfficeID() || !deansOfficeDep.getID().equals(nsiEntity.getDeansOfficeID().getDepartment().getID())))
                    return true;
            }

            if (null != entity.getOperationOrgUnit())
            {
                DepartmentType operationsOrgUnitDep = departmentsMap.get(entity.getOperationOrgUnit().getId().toString());
                if (null != operationsOrgUnitDep && (null == nsiEntity.getResponsibleDepartmentID() || !operationsOrgUnitDep.getID().equals(nsiEntity.getResponsibleDepartmentID().getDepartment().getID())))
                    return true;
            }

            EducationFormType eduFormType = educationFormsMap.get(entity.getDevelopForm().getId().toString());
            if (null != eduFormType && (null == nsiEntity.getEducationFormID() || !eduFormType.getID().equals(nsiEntity.getEducationFormID().getEducationForm().getID())))
                return true;

            DevelopConditionType develConditionType = developConditionsMap.get(entity.getDevelopCondition().getId().toString());
            if (null != develConditionType && (null == nsiEntity.getDevelopConditionID() || !develConditionType.getID().equals(nsiEntity.getDevelopConditionID().getDevelopCondition().getID())))
                return true;

            DevelopPeriodType develPeriodType = developPeriodsMap.get(entity.getDevelopPeriod().getId().toString());
            if (null != develPeriodType && (null == nsiEntity.getDevelopPeriodID() || !develPeriodType.getID().equals(nsiEntity.getDevelopPeriodID().getDevelopPeriod().getID())))
                return true;

            DevelopTechType develTechType = developTechsMap.get(entity.getDevelopTech().getId().toString());
            if (null != develTechType && (null == nsiEntity.getDevelopTechID() || !develTechType.getID().equals(nsiEntity.getDevelopTechID().getDevelopTech().getID())))
                return true;
        }

        return false;
    }

    @Override
    public EducationalProgramType createEntity(EducationOrgUnit entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        EducationalProgramType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createEducationalProgramType();
        nsiEntity.setID(getGUID(entity, nsiIdsMap));
        nsiEntity.setEducationalProgramID(String.valueOf(entity.getCode()));
        nsiEntity.setEducationalProgramName(entity.getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle());

        StructureEducationLevels lvl = entity.getEducationLevelHighSchool().getEducationLevel().getLevelType();
        while(null != lvl.getParent()) lvl = lvl.getParent();
        nsiEntity.setGenerationGos(lvl.getShortTitle());

        DepartmentType formativeOrgUnitDep = departmentsMap.get(entity.getFormativeOrgUnit().getId().toString());
        if (null != formativeOrgUnitDep)
        {
            EducationalProgramType.DepartmentID formativeOrgUnit = new EducationalProgramType.DepartmentID();
            formativeOrgUnit.setDepartment(formativeOrgUnitDep);
            nsiEntity.setDepartmentID(formativeOrgUnit);
        }

        DepartmentType territorialOrgUnitDep = departmentsMap.get(entity.getTerritorialOrgUnit().getId().toString());
        if (null != territorialOrgUnitDep)
        {
            EducationalProgramType.TerritorialDepartmentID territorialOrgUnit = new EducationalProgramType.TerritorialDepartmentID();
            territorialOrgUnit.setDepartment(territorialOrgUnitDep);
            nsiEntity.setTerritorialDepartmentID(territorialOrgUnit);
        }

        if (null != entity.getGroupOrgUnit())
        {
            DepartmentType deansOfficeDep = departmentsMap.get(entity.getGroupOrgUnit().getId().toString());
            if (null != deansOfficeDep)
            {
                EducationalProgramType.DeansOfficeID deansOffice = new EducationalProgramType.DeansOfficeID();
                deansOffice.setDepartment(deansOfficeDep);
                nsiEntity.setDeansOfficeID(deansOffice);
            }
        }

        if (null != entity.getOperationOrgUnit())
        {
            DepartmentType operationsOrgUnitDep = departmentsMap.get(entity.getOperationOrgUnit().getId().toString());
            if (null != operationsOrgUnitDep)
            {
                EducationalProgramType.ResponsibleDepartmentID responsibleOrgUnit = new EducationalProgramType.ResponsibleDepartmentID();
                responsibleOrgUnit.setDepartment(operationsOrgUnitDep);
                nsiEntity.setResponsibleDepartmentID(responsibleOrgUnit);
            }
        }

        EducationFormType eduFormType = educationFormsMap.get(entity.getDevelopForm().getId().toString());
        if (null != eduFormType)
        {
            EducationalProgramType.EducationFormID eduForm = new EducationalProgramType.EducationFormID();
            eduForm.setEducationForm(eduFormType);
            nsiEntity.setEducationFormID(eduForm);
        }

        DevelopConditionType develConditionType = developConditionsMap.get(entity.getDevelopCondition().getId().toString());
        if (null != develConditionType)
        {
            EducationalProgramType.DevelopConditionID develCondition = new EducationalProgramType.DevelopConditionID();
            develCondition.setDevelopCondition(develConditionType);
            nsiEntity.setDevelopConditionID(develCondition);
        }

        DevelopPeriodType develPeriodType = developPeriodsMap.get(entity.getDevelopPeriod().getId().toString());
        if (null != develPeriodType)
        {
            EducationalProgramType.DevelopPeriodID develPeriod = new EducationalProgramType.DevelopPeriodID();
            develPeriod.setDevelopPeriod(develPeriodType);
            nsiEntity.setDevelopPeriodID(develPeriod);
        }

        DevelopTechType develTechType = developTechsMap.get(entity.getDevelopTech().getId().toString());
        if (null != develTechType)
        {
            EducationalProgramType.DevelopTechID develTech = new EducationalProgramType.DevelopTechID();
            develTech.setDevelopTech(develTechType);
            nsiEntity.setDevelopTechID(develTech);
        }

        return nsiEntity;
    }

    @Override
    public EducationOrgUnit createEntity(EducationalProgramType nsiEntity, Map<String, EducationOrgUnit> similarEntityMap)
    {
        return null;
        //TODO: Как создавать у нас подразделения по данным НСИ?
    }

    @Override
    public EducationOrgUnit updatePossibleDuplicateFields(EducationalProgramType nsiEntity, EducationOrgUnit entity, Map<String, EducationOrgUnit> similarEntityMap)
    {
        return null;
    }

    @Override
    public EducationalProgramType updateNsiEntityFields(EducationalProgramType nsiEntity, EducationOrgUnit entity)
    {
        nsiEntity.setEducationalProgramID(String.valueOf(entity.getCode()));
        nsiEntity.setEducationalProgramName(entity.getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle());

        StructureEducationLevels lvl = entity.getEducationLevelHighSchool().getEducationLevel().getLevelType();
        while(null != lvl.getParent()) lvl = lvl.getParent();
        nsiEntity.setGenerationGos(lvl.getShortTitle());

        DepartmentType formativeOrgUnitDep = departmentsMap.get(entity.getFormativeOrgUnit().getId().toString());
        if (null != formativeOrgUnitDep)
        {
            EducationalProgramType.DepartmentID formativeOrgUnit = new EducationalProgramType.DepartmentID();
            formativeOrgUnit.setDepartment(formativeOrgUnitDep);
            nsiEntity.setDepartmentID(formativeOrgUnit);
        }

        DepartmentType territorialOrgUnitDep = departmentsMap.get(entity.getTerritorialOrgUnit().getId().toString());
        if (null != territorialOrgUnitDep)
        {
            EducationalProgramType.TerritorialDepartmentID territorialOrgUnit = new EducationalProgramType.TerritorialDepartmentID();
            territorialOrgUnit.setDepartment(territorialOrgUnitDep);
            nsiEntity.setTerritorialDepartmentID(territorialOrgUnit);
        }

        if (null != entity.getGroupOrgUnit())
        {
            DepartmentType deansOfficeDep = departmentsMap.get(entity.getGroupOrgUnit().getId().toString());
            if (null != deansOfficeDep)
            {
                EducationalProgramType.DeansOfficeID deansOffice = new EducationalProgramType.DeansOfficeID();
                deansOffice.setDepartment(deansOfficeDep);
                nsiEntity.setDeansOfficeID(deansOffice);
            }
        }

        if (null != entity.getOperationOrgUnit())
        {
            DepartmentType operationsOrgUnitDep = departmentsMap.get(entity.getOperationOrgUnit().getId().toString());
            if (null != operationsOrgUnitDep)
            {
                EducationalProgramType.ResponsibleDepartmentID responsibleOrgUnit = new EducationalProgramType.ResponsibleDepartmentID();
                responsibleOrgUnit.setDepartment(operationsOrgUnitDep);
                nsiEntity.setResponsibleDepartmentID(responsibleOrgUnit);
            }
        }

        EducationFormType eduFormType = educationFormsMap.get(entity.getDevelopForm().getId().toString());
        if (null != eduFormType)
        {
            EducationalProgramType.EducationFormID eduForm = new EducationalProgramType.EducationFormID();
            eduForm.setEducationForm(eduFormType);
            nsiEntity.setEducationFormID(eduForm);
        }

        DevelopConditionType develConditionType = developConditionsMap.get(entity.getDevelopCondition().getId().toString());
        if (null != develConditionType)
        {
            EducationalProgramType.DevelopConditionID develCondition = new EducationalProgramType.DevelopConditionID();
            develCondition.setDevelopCondition(develConditionType);
            nsiEntity.setDevelopConditionID(develCondition);
        }

        DevelopPeriodType develPeriodType = developPeriodsMap.get(entity.getDevelopPeriod().getId().toString());
        if (null != develPeriodType)
        {
            EducationalProgramType.DevelopPeriodID develPeriod = new EducationalProgramType.DevelopPeriodID();
            develPeriod.setDevelopPeriod(develPeriodType);
            nsiEntity.setDevelopPeriodID(develPeriod);
        }

        DevelopTechType develTechType = developTechsMap.get(entity.getDevelopTech().getId().toString());
        if (null != develTechType)
        {
            EducationalProgramType.DevelopTechID develTech = new EducationalProgramType.DevelopTechID();
            develTech.setDevelopTech(develTechType);
            nsiEntity.setDevelopTechID(develTech);
        }

        return nsiEntity;
    }
}