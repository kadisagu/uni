/**
 * SubmitStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportal;

public interface SubmitStatus extends java.rmi.Remote {
    public ru.tandemservice.unifefu.ws.rateportal.SubmitStatusResponse submitStatus(ru.tandemservice.unifefu.ws.rateportal.SubmitStatusRequest submitStatusRequestMsgPart) throws java.rmi.RemoteException, ru.tandemservice.unifefu.ws.rateportal.SubmitStatusFaultMsg;
}
