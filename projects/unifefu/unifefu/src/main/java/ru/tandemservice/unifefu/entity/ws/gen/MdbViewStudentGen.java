package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.ws.MdbViewPerson;
import ru.tandemservice.unifefu.entity.ws.MdbViewStudent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Студент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewStudentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.MdbViewStudent";
    public static final String ENTITY_NAME = "mdbViewStudent";
    public static final int VERSION_HASH = -1422263716;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_MDB_VIEW_PERSON = "mdbViewPerson";
    public static final String P_NUMBER = "number";
    public static final String P_ENTRANCE = "entrance";
    public static final String L_GROUP = "group";
    public static final String L_EDU_OU = "eduOu";
    public static final String P_COURSE = "course";
    public static final String P_COMPENSATION = "compensation";
    public static final String P_CATEGORY = "category";
    public static final String P_BOOK_NUMBER = "bookNumber";
    public static final String P_STATUS = "status";
    public static final String P_ARCHIVAL = "archival";
    public static final String P_FINISH_YEAR = "finishYear";
    public static final String P_PERSONAL_FILE_NUMBER = "personalFileNumber";
    public static final String P_SPECIAL_PURPOSE_RECRUIT = "specialPurposeRecruit";
    public static final String P_STUDENT_GUID = "studentGuid";
    public static final String P_FORMATIVE_ORG_UNIT_GUID = "formativeOrgUnitGuid";
    public static final String P_OUT_ORG_UNIT_GUID = "outOrgUnitGuid";
    public static final String P_EDUCATION_FORM_GUID = "educationFormGuid";
    public static final String P_EDUCATION_DIRECTION_GUID = "educationDirectionGuid";
    public static final String P_QUALIFICATION_GUID = "qualificationGuid";
    public static final String P_DEVELOP_TECH_GUID = "developTechGuid";
    public static final String P_DEVELOP_CONDITION_GUID = "developConditionGuid";
    public static final String P_COMPENSATION_TYPE_GUID = "compensationTypeGuid";

    private Student _student;     // Студент
    private MdbViewPerson _mdbViewPerson;     // Персона
    private String _number;     // Личный номер
    private int _entrance;     // Год приема
    private Group _group;     // Группа
    private EducationOrgUnit _eduOu;     // Параметры обучения студентов по направлению подготовки (НПП)
    private int _course;     // Курс
    private String _compensation;     // Вид возмещения затрат
    private String _category;     // Категория студента
    private String _bookNumber;     // Номер зачетной книжки
    private String _status;     // Статус студента
    private boolean _archival;     // Архивный
    private Integer _finishYear;     // Год выпуска
    private String _personalFileNumber;     // Номер личного дела
    private boolean _specialPurposeRecruit;     // Целевой прием
    private String _studentGuid;     // Идентификатор студента в НСИ
    private String _formativeOrgUnitGuid;     // Идентификатор формирующего подразделения в НСИ
    private String _outOrgUnitGuid;     // Идентификатор выпускающего подразделения в НСИ
    private String _educationFormGuid;     // Идентификатор формы обучения в НСИ
    private String _educationDirectionGuid;     // Идентификатор направления обучения (образовательной программы) в ОБ
    private String _qualificationGuid;     // Идентификатор уровня подготовки в НСИ
    private String _developTechGuid;     // Идентификатор технологии обучения в НСИ
    private String _developConditionGuid;     // Идентификатор условия освоения в НСИ
    private String _compensationTypeGuid;     // Идентификатор формы возмещения затрат в НСИ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Персона. Свойство не может быть null.
     */
    @NotNull
    public MdbViewPerson getMdbViewPerson()
    {
        return _mdbViewPerson;
    }

    /**
     * @param mdbViewPerson Персона. Свойство не может быть null.
     */
    public void setMdbViewPerson(MdbViewPerson mdbViewPerson)
    {
        dirty(_mdbViewPerson, mdbViewPerson);
        _mdbViewPerson = mdbViewPerson;
    }

    /**
     * @return Личный номер. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Личный номер. Свойство не может быть null и должно быть уникальным.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Год приема. Свойство не может быть null.
     */
    @NotNull
    public int getEntrance()
    {
        return _entrance;
    }

    /**
     * @param entrance Год приема. Свойство не может быть null.
     */
    public void setEntrance(int entrance)
    {
        dirty(_entrance, entrance);
        _entrance = entrance;
    }

    /**
     * @return Группа.
     */
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEduOu()
    {
        return _eduOu;
    }

    /**
     * @param eduOu Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEduOu(EducationOrgUnit eduOu)
    {
        dirty(_eduOu, eduOu);
        _eduOu = eduOu;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public int getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(int course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCompensation()
    {
        return _compensation;
    }

    /**
     * @param compensation Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensation(String compensation)
    {
        dirty(_compensation, compensation);
        _compensation = compensation;
    }

    /**
     * @return Категория студента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCategory()
    {
        return _category;
    }

    /**
     * @param category Категория студента. Свойство не может быть null.
     */
    public void setCategory(String category)
    {
        dirty(_category, category);
        _category = category;
    }

    /**
     * @return Номер зачетной книжки.
     */
    @Length(max=255)
    public String getBookNumber()
    {
        return _bookNumber;
    }

    /**
     * @param bookNumber Номер зачетной книжки.
     */
    public void setBookNumber(String bookNumber)
    {
        dirty(_bookNumber, bookNumber);
        _bookNumber = bookNumber;
    }

    /**
     * @return Статус студента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStatus()
    {
        return _status;
    }

    /**
     * @param status Статус студента. Свойство не может быть null.
     */
    public void setStatus(String status)
    {
        dirty(_status, status);
        _status = status;
    }

    /**
     * @return Архивный. Свойство не может быть null.
     */
    @NotNull
    public boolean isArchival()
    {
        return _archival;
    }

    /**
     * @param archival Архивный. Свойство не может быть null.
     */
    public void setArchival(boolean archival)
    {
        dirty(_archival, archival);
        _archival = archival;
    }

    /**
     * @return Год выпуска.
     */
    public Integer getFinishYear()
    {
        return _finishYear;
    }

    /**
     * @param finishYear Год выпуска.
     */
    public void setFinishYear(Integer finishYear)
    {
        dirty(_finishYear, finishYear);
        _finishYear = finishYear;
    }

    /**
     * @return Номер личного дела.
     */
    @Length(max=255)
    public String getPersonalFileNumber()
    {
        return _personalFileNumber;
    }

    /**
     * @param personalFileNumber Номер личного дела.
     */
    public void setPersonalFileNumber(String personalFileNumber)
    {
        dirty(_personalFileNumber, personalFileNumber);
        _personalFileNumber = personalFileNumber;
    }

    /**
     * @return Целевой прием. Свойство не может быть null.
     */
    @NotNull
    public boolean isSpecialPurposeRecruit()
    {
        return _specialPurposeRecruit;
    }

    /**
     * @param specialPurposeRecruit Целевой прием. Свойство не может быть null.
     */
    public void setSpecialPurposeRecruit(boolean specialPurposeRecruit)
    {
        dirty(_specialPurposeRecruit, specialPurposeRecruit);
        _specialPurposeRecruit = specialPurposeRecruit;
    }

    /**
     * @return Идентификатор студента в НСИ.
     */
    @Length(max=255)
    public String getStudentGuid()
    {
        return _studentGuid;
    }

    /**
     * @param studentGuid Идентификатор студента в НСИ.
     */
    public void setStudentGuid(String studentGuid)
    {
        dirty(_studentGuid, studentGuid);
        _studentGuid = studentGuid;
    }

    /**
     * @return Идентификатор формирующего подразделения в НСИ.
     */
    @Length(max=255)
    public String getFormativeOrgUnitGuid()
    {
        return _formativeOrgUnitGuid;
    }

    /**
     * @param formativeOrgUnitGuid Идентификатор формирующего подразделения в НСИ.
     */
    public void setFormativeOrgUnitGuid(String formativeOrgUnitGuid)
    {
        dirty(_formativeOrgUnitGuid, formativeOrgUnitGuid);
        _formativeOrgUnitGuid = formativeOrgUnitGuid;
    }

    /**
     * @return Идентификатор выпускающего подразделения в НСИ.
     */
    @Length(max=255)
    public String getOutOrgUnitGuid()
    {
        return _outOrgUnitGuid;
    }

    /**
     * @param outOrgUnitGuid Идентификатор выпускающего подразделения в НСИ.
     */
    public void setOutOrgUnitGuid(String outOrgUnitGuid)
    {
        dirty(_outOrgUnitGuid, outOrgUnitGuid);
        _outOrgUnitGuid = outOrgUnitGuid;
    }

    /**
     * @return Идентификатор формы обучения в НСИ.
     */
    @Length(max=255)
    public String getEducationFormGuid()
    {
        return _educationFormGuid;
    }

    /**
     * @param educationFormGuid Идентификатор формы обучения в НСИ.
     */
    public void setEducationFormGuid(String educationFormGuid)
    {
        dirty(_educationFormGuid, educationFormGuid);
        _educationFormGuid = educationFormGuid;
    }

    /**
     * @return Идентификатор направления обучения (образовательной программы) в ОБ.
     */
    @Length(max=255)
    public String getEducationDirectionGuid()
    {
        return _educationDirectionGuid;
    }

    /**
     * @param educationDirectionGuid Идентификатор направления обучения (образовательной программы) в ОБ.
     */
    public void setEducationDirectionGuid(String educationDirectionGuid)
    {
        dirty(_educationDirectionGuid, educationDirectionGuid);
        _educationDirectionGuid = educationDirectionGuid;
    }

    /**
     * @return Идентификатор уровня подготовки в НСИ.
     */
    @Length(max=255)
    public String getQualificationGuid()
    {
        return _qualificationGuid;
    }

    /**
     * @param qualificationGuid Идентификатор уровня подготовки в НСИ.
     */
    public void setQualificationGuid(String qualificationGuid)
    {
        dirty(_qualificationGuid, qualificationGuid);
        _qualificationGuid = qualificationGuid;
    }

    /**
     * @return Идентификатор технологии обучения в НСИ.
     */
    @Length(max=255)
    public String getDevelopTechGuid()
    {
        return _developTechGuid;
    }

    /**
     * @param developTechGuid Идентификатор технологии обучения в НСИ.
     */
    public void setDevelopTechGuid(String developTechGuid)
    {
        dirty(_developTechGuid, developTechGuid);
        _developTechGuid = developTechGuid;
    }

    /**
     * @return Идентификатор условия освоения в НСИ.
     */
    @Length(max=255)
    public String getDevelopConditionGuid()
    {
        return _developConditionGuid;
    }

    /**
     * @param developConditionGuid Идентификатор условия освоения в НСИ.
     */
    public void setDevelopConditionGuid(String developConditionGuid)
    {
        dirty(_developConditionGuid, developConditionGuid);
        _developConditionGuid = developConditionGuid;
    }

    /**
     * @return Идентификатор формы возмещения затрат в НСИ.
     */
    @Length(max=255)
    public String getCompensationTypeGuid()
    {
        return _compensationTypeGuid;
    }

    /**
     * @param compensationTypeGuid Идентификатор формы возмещения затрат в НСИ.
     */
    public void setCompensationTypeGuid(String compensationTypeGuid)
    {
        dirty(_compensationTypeGuid, compensationTypeGuid);
        _compensationTypeGuid = compensationTypeGuid;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewStudentGen)
        {
            setStudent(((MdbViewStudent)another).getStudent());
            setMdbViewPerson(((MdbViewStudent)another).getMdbViewPerson());
            setNumber(((MdbViewStudent)another).getNumber());
            setEntrance(((MdbViewStudent)another).getEntrance());
            setGroup(((MdbViewStudent)another).getGroup());
            setEduOu(((MdbViewStudent)another).getEduOu());
            setCourse(((MdbViewStudent)another).getCourse());
            setCompensation(((MdbViewStudent)another).getCompensation());
            setCategory(((MdbViewStudent)another).getCategory());
            setBookNumber(((MdbViewStudent)another).getBookNumber());
            setStatus(((MdbViewStudent)another).getStatus());
            setArchival(((MdbViewStudent)another).isArchival());
            setFinishYear(((MdbViewStudent)another).getFinishYear());
            setPersonalFileNumber(((MdbViewStudent)another).getPersonalFileNumber());
            setSpecialPurposeRecruit(((MdbViewStudent)another).isSpecialPurposeRecruit());
            setStudentGuid(((MdbViewStudent)another).getStudentGuid());
            setFormativeOrgUnitGuid(((MdbViewStudent)another).getFormativeOrgUnitGuid());
            setOutOrgUnitGuid(((MdbViewStudent)another).getOutOrgUnitGuid());
            setEducationFormGuid(((MdbViewStudent)another).getEducationFormGuid());
            setEducationDirectionGuid(((MdbViewStudent)another).getEducationDirectionGuid());
            setQualificationGuid(((MdbViewStudent)another).getQualificationGuid());
            setDevelopTechGuid(((MdbViewStudent)another).getDevelopTechGuid());
            setDevelopConditionGuid(((MdbViewStudent)another).getDevelopConditionGuid());
            setCompensationTypeGuid(((MdbViewStudent)another).getCompensationTypeGuid());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewStudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewStudent.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewStudent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "mdbViewPerson":
                    return obj.getMdbViewPerson();
                case "number":
                    return obj.getNumber();
                case "entrance":
                    return obj.getEntrance();
                case "group":
                    return obj.getGroup();
                case "eduOu":
                    return obj.getEduOu();
                case "course":
                    return obj.getCourse();
                case "compensation":
                    return obj.getCompensation();
                case "category":
                    return obj.getCategory();
                case "bookNumber":
                    return obj.getBookNumber();
                case "status":
                    return obj.getStatus();
                case "archival":
                    return obj.isArchival();
                case "finishYear":
                    return obj.getFinishYear();
                case "personalFileNumber":
                    return obj.getPersonalFileNumber();
                case "specialPurposeRecruit":
                    return obj.isSpecialPurposeRecruit();
                case "studentGuid":
                    return obj.getStudentGuid();
                case "formativeOrgUnitGuid":
                    return obj.getFormativeOrgUnitGuid();
                case "outOrgUnitGuid":
                    return obj.getOutOrgUnitGuid();
                case "educationFormGuid":
                    return obj.getEducationFormGuid();
                case "educationDirectionGuid":
                    return obj.getEducationDirectionGuid();
                case "qualificationGuid":
                    return obj.getQualificationGuid();
                case "developTechGuid":
                    return obj.getDevelopTechGuid();
                case "developConditionGuid":
                    return obj.getDevelopConditionGuid();
                case "compensationTypeGuid":
                    return obj.getCompensationTypeGuid();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "mdbViewPerson":
                    obj.setMdbViewPerson((MdbViewPerson) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "entrance":
                    obj.setEntrance((Integer) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "eduOu":
                    obj.setEduOu((EducationOrgUnit) value);
                    return;
                case "course":
                    obj.setCourse((Integer) value);
                    return;
                case "compensation":
                    obj.setCompensation((String) value);
                    return;
                case "category":
                    obj.setCategory((String) value);
                    return;
                case "bookNumber":
                    obj.setBookNumber((String) value);
                    return;
                case "status":
                    obj.setStatus((String) value);
                    return;
                case "archival":
                    obj.setArchival((Boolean) value);
                    return;
                case "finishYear":
                    obj.setFinishYear((Integer) value);
                    return;
                case "personalFileNumber":
                    obj.setPersonalFileNumber((String) value);
                    return;
                case "specialPurposeRecruit":
                    obj.setSpecialPurposeRecruit((Boolean) value);
                    return;
                case "studentGuid":
                    obj.setStudentGuid((String) value);
                    return;
                case "formativeOrgUnitGuid":
                    obj.setFormativeOrgUnitGuid((String) value);
                    return;
                case "outOrgUnitGuid":
                    obj.setOutOrgUnitGuid((String) value);
                    return;
                case "educationFormGuid":
                    obj.setEducationFormGuid((String) value);
                    return;
                case "educationDirectionGuid":
                    obj.setEducationDirectionGuid((String) value);
                    return;
                case "qualificationGuid":
                    obj.setQualificationGuid((String) value);
                    return;
                case "developTechGuid":
                    obj.setDevelopTechGuid((String) value);
                    return;
                case "developConditionGuid":
                    obj.setDevelopConditionGuid((String) value);
                    return;
                case "compensationTypeGuid":
                    obj.setCompensationTypeGuid((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "mdbViewPerson":
                        return true;
                case "number":
                        return true;
                case "entrance":
                        return true;
                case "group":
                        return true;
                case "eduOu":
                        return true;
                case "course":
                        return true;
                case "compensation":
                        return true;
                case "category":
                        return true;
                case "bookNumber":
                        return true;
                case "status":
                        return true;
                case "archival":
                        return true;
                case "finishYear":
                        return true;
                case "personalFileNumber":
                        return true;
                case "specialPurposeRecruit":
                        return true;
                case "studentGuid":
                        return true;
                case "formativeOrgUnitGuid":
                        return true;
                case "outOrgUnitGuid":
                        return true;
                case "educationFormGuid":
                        return true;
                case "educationDirectionGuid":
                        return true;
                case "qualificationGuid":
                        return true;
                case "developTechGuid":
                        return true;
                case "developConditionGuid":
                        return true;
                case "compensationTypeGuid":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "mdbViewPerson":
                    return true;
                case "number":
                    return true;
                case "entrance":
                    return true;
                case "group":
                    return true;
                case "eduOu":
                    return true;
                case "course":
                    return true;
                case "compensation":
                    return true;
                case "category":
                    return true;
                case "bookNumber":
                    return true;
                case "status":
                    return true;
                case "archival":
                    return true;
                case "finishYear":
                    return true;
                case "personalFileNumber":
                    return true;
                case "specialPurposeRecruit":
                    return true;
                case "studentGuid":
                    return true;
                case "formativeOrgUnitGuid":
                    return true;
                case "outOrgUnitGuid":
                    return true;
                case "educationFormGuid":
                    return true;
                case "educationDirectionGuid":
                    return true;
                case "qualificationGuid":
                    return true;
                case "developTechGuid":
                    return true;
                case "developConditionGuid":
                    return true;
                case "compensationTypeGuid":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "mdbViewPerson":
                    return MdbViewPerson.class;
                case "number":
                    return String.class;
                case "entrance":
                    return Integer.class;
                case "group":
                    return Group.class;
                case "eduOu":
                    return EducationOrgUnit.class;
                case "course":
                    return Integer.class;
                case "compensation":
                    return String.class;
                case "category":
                    return String.class;
                case "bookNumber":
                    return String.class;
                case "status":
                    return String.class;
                case "archival":
                    return Boolean.class;
                case "finishYear":
                    return Integer.class;
                case "personalFileNumber":
                    return String.class;
                case "specialPurposeRecruit":
                    return Boolean.class;
                case "studentGuid":
                    return String.class;
                case "formativeOrgUnitGuid":
                    return String.class;
                case "outOrgUnitGuid":
                    return String.class;
                case "educationFormGuid":
                    return String.class;
                case "educationDirectionGuid":
                    return String.class;
                case "qualificationGuid":
                    return String.class;
                case "developTechGuid":
                    return String.class;
                case "developConditionGuid":
                    return String.class;
                case "compensationTypeGuid":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewStudent> _dslPath = new Path<MdbViewStudent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewStudent");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getMdbViewPerson()
     */
    public static MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
    {
        return _dslPath.mdbViewPerson();
    }

    /**
     * @return Личный номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Год приема. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getEntrance()
     */
    public static PropertyPath<Integer> entrance()
    {
        return _dslPath.entrance();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getEduOu()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> eduOu()
    {
        return _dslPath.eduOu();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getCourse()
     */
    public static PropertyPath<Integer> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getCompensation()
     */
    public static PropertyPath<String> compensation()
    {
        return _dslPath.compensation();
    }

    /**
     * @return Категория студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getCategory()
     */
    public static PropertyPath<String> category()
    {
        return _dslPath.category();
    }

    /**
     * @return Номер зачетной книжки.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getBookNumber()
     */
    public static PropertyPath<String> bookNumber()
    {
        return _dslPath.bookNumber();
    }

    /**
     * @return Статус студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getStatus()
     */
    public static PropertyPath<String> status()
    {
        return _dslPath.status();
    }

    /**
     * @return Архивный. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#isArchival()
     */
    public static PropertyPath<Boolean> archival()
    {
        return _dslPath.archival();
    }

    /**
     * @return Год выпуска.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getFinishYear()
     */
    public static PropertyPath<Integer> finishYear()
    {
        return _dslPath.finishYear();
    }

    /**
     * @return Номер личного дела.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getPersonalFileNumber()
     */
    public static PropertyPath<String> personalFileNumber()
    {
        return _dslPath.personalFileNumber();
    }

    /**
     * @return Целевой прием. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#isSpecialPurposeRecruit()
     */
    public static PropertyPath<Boolean> specialPurposeRecruit()
    {
        return _dslPath.specialPurposeRecruit();
    }

    /**
     * @return Идентификатор студента в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getStudentGuid()
     */
    public static PropertyPath<String> studentGuid()
    {
        return _dslPath.studentGuid();
    }

    /**
     * @return Идентификатор формирующего подразделения в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getFormativeOrgUnitGuid()
     */
    public static PropertyPath<String> formativeOrgUnitGuid()
    {
        return _dslPath.formativeOrgUnitGuid();
    }

    /**
     * @return Идентификатор выпускающего подразделения в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getOutOrgUnitGuid()
     */
    public static PropertyPath<String> outOrgUnitGuid()
    {
        return _dslPath.outOrgUnitGuid();
    }

    /**
     * @return Идентификатор формы обучения в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getEducationFormGuid()
     */
    public static PropertyPath<String> educationFormGuid()
    {
        return _dslPath.educationFormGuid();
    }

    /**
     * @return Идентификатор направления обучения (образовательной программы) в ОБ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getEducationDirectionGuid()
     */
    public static PropertyPath<String> educationDirectionGuid()
    {
        return _dslPath.educationDirectionGuid();
    }

    /**
     * @return Идентификатор уровня подготовки в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getQualificationGuid()
     */
    public static PropertyPath<String> qualificationGuid()
    {
        return _dslPath.qualificationGuid();
    }

    /**
     * @return Идентификатор технологии обучения в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getDevelopTechGuid()
     */
    public static PropertyPath<String> developTechGuid()
    {
        return _dslPath.developTechGuid();
    }

    /**
     * @return Идентификатор условия освоения в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getDevelopConditionGuid()
     */
    public static PropertyPath<String> developConditionGuid()
    {
        return _dslPath.developConditionGuid();
    }

    /**
     * @return Идентификатор формы возмещения затрат в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getCompensationTypeGuid()
     */
    public static PropertyPath<String> compensationTypeGuid()
    {
        return _dslPath.compensationTypeGuid();
    }

    public static class Path<E extends MdbViewStudent> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private MdbViewPerson.Path<MdbViewPerson> _mdbViewPerson;
        private PropertyPath<String> _number;
        private PropertyPath<Integer> _entrance;
        private Group.Path<Group> _group;
        private EducationOrgUnit.Path<EducationOrgUnit> _eduOu;
        private PropertyPath<Integer> _course;
        private PropertyPath<String> _compensation;
        private PropertyPath<String> _category;
        private PropertyPath<String> _bookNumber;
        private PropertyPath<String> _status;
        private PropertyPath<Boolean> _archival;
        private PropertyPath<Integer> _finishYear;
        private PropertyPath<String> _personalFileNumber;
        private PropertyPath<Boolean> _specialPurposeRecruit;
        private PropertyPath<String> _studentGuid;
        private PropertyPath<String> _formativeOrgUnitGuid;
        private PropertyPath<String> _outOrgUnitGuid;
        private PropertyPath<String> _educationFormGuid;
        private PropertyPath<String> _educationDirectionGuid;
        private PropertyPath<String> _qualificationGuid;
        private PropertyPath<String> _developTechGuid;
        private PropertyPath<String> _developConditionGuid;
        private PropertyPath<String> _compensationTypeGuid;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getMdbViewPerson()
     */
        public MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
        {
            if(_mdbViewPerson == null )
                _mdbViewPerson = new MdbViewPerson.Path<MdbViewPerson>(L_MDB_VIEW_PERSON, this);
            return _mdbViewPerson;
        }

    /**
     * @return Личный номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(MdbViewStudentGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Год приема. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getEntrance()
     */
        public PropertyPath<Integer> entrance()
        {
            if(_entrance == null )
                _entrance = new PropertyPath<Integer>(MdbViewStudentGen.P_ENTRANCE, this);
            return _entrance;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getEduOu()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> eduOu()
        {
            if(_eduOu == null )
                _eduOu = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDU_OU, this);
            return _eduOu;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getCourse()
     */
        public PropertyPath<Integer> course()
        {
            if(_course == null )
                _course = new PropertyPath<Integer>(MdbViewStudentGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getCompensation()
     */
        public PropertyPath<String> compensation()
        {
            if(_compensation == null )
                _compensation = new PropertyPath<String>(MdbViewStudentGen.P_COMPENSATION, this);
            return _compensation;
        }

    /**
     * @return Категория студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getCategory()
     */
        public PropertyPath<String> category()
        {
            if(_category == null )
                _category = new PropertyPath<String>(MdbViewStudentGen.P_CATEGORY, this);
            return _category;
        }

    /**
     * @return Номер зачетной книжки.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getBookNumber()
     */
        public PropertyPath<String> bookNumber()
        {
            if(_bookNumber == null )
                _bookNumber = new PropertyPath<String>(MdbViewStudentGen.P_BOOK_NUMBER, this);
            return _bookNumber;
        }

    /**
     * @return Статус студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getStatus()
     */
        public PropertyPath<String> status()
        {
            if(_status == null )
                _status = new PropertyPath<String>(MdbViewStudentGen.P_STATUS, this);
            return _status;
        }

    /**
     * @return Архивный. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#isArchival()
     */
        public PropertyPath<Boolean> archival()
        {
            if(_archival == null )
                _archival = new PropertyPath<Boolean>(MdbViewStudentGen.P_ARCHIVAL, this);
            return _archival;
        }

    /**
     * @return Год выпуска.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getFinishYear()
     */
        public PropertyPath<Integer> finishYear()
        {
            if(_finishYear == null )
                _finishYear = new PropertyPath<Integer>(MdbViewStudentGen.P_FINISH_YEAR, this);
            return _finishYear;
        }

    /**
     * @return Номер личного дела.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getPersonalFileNumber()
     */
        public PropertyPath<String> personalFileNumber()
        {
            if(_personalFileNumber == null )
                _personalFileNumber = new PropertyPath<String>(MdbViewStudentGen.P_PERSONAL_FILE_NUMBER, this);
            return _personalFileNumber;
        }

    /**
     * @return Целевой прием. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#isSpecialPurposeRecruit()
     */
        public PropertyPath<Boolean> specialPurposeRecruit()
        {
            if(_specialPurposeRecruit == null )
                _specialPurposeRecruit = new PropertyPath<Boolean>(MdbViewStudentGen.P_SPECIAL_PURPOSE_RECRUIT, this);
            return _specialPurposeRecruit;
        }

    /**
     * @return Идентификатор студента в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getStudentGuid()
     */
        public PropertyPath<String> studentGuid()
        {
            if(_studentGuid == null )
                _studentGuid = new PropertyPath<String>(MdbViewStudentGen.P_STUDENT_GUID, this);
            return _studentGuid;
        }

    /**
     * @return Идентификатор формирующего подразделения в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getFormativeOrgUnitGuid()
     */
        public PropertyPath<String> formativeOrgUnitGuid()
        {
            if(_formativeOrgUnitGuid == null )
                _formativeOrgUnitGuid = new PropertyPath<String>(MdbViewStudentGen.P_FORMATIVE_ORG_UNIT_GUID, this);
            return _formativeOrgUnitGuid;
        }

    /**
     * @return Идентификатор выпускающего подразделения в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getOutOrgUnitGuid()
     */
        public PropertyPath<String> outOrgUnitGuid()
        {
            if(_outOrgUnitGuid == null )
                _outOrgUnitGuid = new PropertyPath<String>(MdbViewStudentGen.P_OUT_ORG_UNIT_GUID, this);
            return _outOrgUnitGuid;
        }

    /**
     * @return Идентификатор формы обучения в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getEducationFormGuid()
     */
        public PropertyPath<String> educationFormGuid()
        {
            if(_educationFormGuid == null )
                _educationFormGuid = new PropertyPath<String>(MdbViewStudentGen.P_EDUCATION_FORM_GUID, this);
            return _educationFormGuid;
        }

    /**
     * @return Идентификатор направления обучения (образовательной программы) в ОБ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getEducationDirectionGuid()
     */
        public PropertyPath<String> educationDirectionGuid()
        {
            if(_educationDirectionGuid == null )
                _educationDirectionGuid = new PropertyPath<String>(MdbViewStudentGen.P_EDUCATION_DIRECTION_GUID, this);
            return _educationDirectionGuid;
        }

    /**
     * @return Идентификатор уровня подготовки в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getQualificationGuid()
     */
        public PropertyPath<String> qualificationGuid()
        {
            if(_qualificationGuid == null )
                _qualificationGuid = new PropertyPath<String>(MdbViewStudentGen.P_QUALIFICATION_GUID, this);
            return _qualificationGuid;
        }

    /**
     * @return Идентификатор технологии обучения в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getDevelopTechGuid()
     */
        public PropertyPath<String> developTechGuid()
        {
            if(_developTechGuid == null )
                _developTechGuid = new PropertyPath<String>(MdbViewStudentGen.P_DEVELOP_TECH_GUID, this);
            return _developTechGuid;
        }

    /**
     * @return Идентификатор условия освоения в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getDevelopConditionGuid()
     */
        public PropertyPath<String> developConditionGuid()
        {
            if(_developConditionGuid == null )
                _developConditionGuid = new PropertyPath<String>(MdbViewStudentGen.P_DEVELOP_CONDITION_GUID, this);
            return _developConditionGuid;
        }

    /**
     * @return Идентификатор формы возмещения затрат в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudent#getCompensationTypeGuid()
     */
        public PropertyPath<String> compensationTypeGuid()
        {
            if(_compensationTypeGuid == null )
                _compensationTypeGuid = new PropertyPath<String>(MdbViewStudentGen.P_COMPENSATION_TYPE_GUID, this);
            return _compensationTypeGuid;
        }

        public Class getEntityClass()
        {
            return MdbViewStudent.class;
        }

        public String getEntityName()
        {
            return "mdbViewStudent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
