/* $Id: Controller.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.modularextract.fefu4.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubController;
import ru.tandemservice.unifefu.entity.FefuExcludeStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 27.08.2012
 */
public class Controller extends ModularStudentExtractPubController<FefuExcludeStuExtract, IDAO, Model>
{
}