/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.gradebook;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.process.*;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse2TrJournalRel;
import ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent;
import ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel;
import ru.tandemservice.unifefu.ws.blackboard.BBContextHelper;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.ColumnFilter;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.ColumnVO;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.ScoreFilter;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.ScoreVO;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 25.04.2014
 */
public class BBGradebookUtils
{
    /**
     * Получение списка колонок (мероприятий) курса.
     * Агрегирующие колонки типа TOTAL и WEIGHTED_TOTAL выкидываются, т.к. не нужны.
     *
     * @param coursePrimaryId идентификатор курса (примари)
     * @return список колонок
     */
    public static List<ColumnVO> getCourseColumns(final String coursePrimaryId)
    {
        List<ColumnVO> retList = BBContextHelper.getInstance().doGradebookRequest((gradebookWSPortType, of) -> {
            ColumnFilter columnFilter = of.createColumnFilter();
            columnFilter.setFilterType(GradebookWSConstants.GET_COLUMN_BY_COURSE_ID);
            columnFilter.setExternalGrade(false);
            return gradebookWSPortType.getGradebookColumns(coursePrimaryId, columnFilter);
        });

        List<ColumnVO> resultList = new ArrayList<>();
        if (retList != null)
        {
            for (ColumnVO column : retList)
            {
                // Были случаи, когда в списке был null
                if (column == null)
                    continue;

                if (GradebookWSConstants.NON_CALCULATED_CALC_TYPE.equals(column.getCalculationType().getValue()))
                    resultList.add(column);
            }
        }

        return resultList;
    }

    /**
     * Получение списка оценок по мероприятиям курса.
     *
     * @param coursePrimaryId идентификатор курса (примари)
     * @param columnIds       набор примари-идентификаторов колонок, для которых нужны оценки. Оценки по остальным колонкам выкидываются.
     * @return мапа: ключ - связка идентификатора колонки и идентификатора участника курса, значение - оценка
     */
    public static Map<CoreCollectionUtils.Pair<String, String>, Double> getGradesForColumns(final String coursePrimaryId, Set<String> columnIds)
    {
        List<ScoreVO> retList = BBContextHelper.getInstance().doGradebookRequest((gradebookWSPortType, of) -> {
            ScoreFilter filter = of.createScoreFilter();
            filter.setFilterType(GradebookWSConstants.GET_SCORE_BY_COURSE_ID);
            return gradebookWSPortType.getGrades(coursePrimaryId, filter);
        });

        Map<CoreCollectionUtils.Pair<String, String>, Double> resultMap = new HashMap<>();
        if (retList != null)
        {
            for (ScoreVO score : retList)
            {
                // Были случаи, когда в списке был null. Пустые оценки нам тоже не нужны
                if (score == null || StringUtils.isEmpty(score.getGrade().getValue()))
                    continue;

                if (columnIds.contains(score.getColumnId().getValue()) && NumberUtils.isNumber(score.getGrade().getValue()))
                {
                    // В качестве ключа используем связку id_колонки - id_участника_курса
                    CoreCollectionUtils.Pair<String, String> key = new CoreCollectionUtils.Pair<>(score.getColumnId().getValue(), score.getMemberId().getValue());
                    // В качестве значения - оценка в виде Double
                    Double value = Double.parseDouble(score.getGrade().getValue());

                    resultMap.put(key, value);
                }
            }
        }

        return resultMap;
    }

    /**
     * Получение оценок из ВВ для конкретной реализации. Запускается в виде прогресс-бара, а по завершении операции в него выводится сообщение о том,
     * сколько оценок получено.
     *
     * @param journal реализация дисциплины
     */
    public static void doSyncGradesForJournal(final TrJournal journal)
    {
        if (journal == null)
            return;

        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                final MutableInt counter = new MutableInt(0);

                // Получаем список связей мероприятий журнала с мероприятиями курса
                DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                        new DQLSelectBuilder().fromEntity(BbTrJournalEventRel.class, "rel")
                                .where(eq(property("rel", BbTrJournalEventRel.courseRelation().trJournal()), value(journal)))
                );
                int coursePrimaryIdCol = dql.column(property("rel", BbTrJournalEventRel.courseRelation().bbCourse().bbPrimaryId()));
                int entityCol = dql.column("rel");

                ListMultimap<String, BbTrJournalEventRel> courseMap = ArrayListMultimap.create();
                for (Object[] item : DataAccessServices.dao().<Object[]>getList(dql.getDql()))
                {
                    courseMap.put((String) item[coursePrimaryIdCol], (BbTrJournalEventRel) item[entityCol]);
                }

                // Получаем оценки для всех полученных курсов
                final Map<CoreCollectionUtils.Pair<String, String>, Double> gradeMap = new HashMap<>();
                for (String coursePrimaryId : courseMap.keySet())
                {
                    Set<String> columnIds = new HashSet<>();
                    for (BbTrJournalEventRel rel : courseMap.get(coursePrimaryId))
                    {
                        columnIds.add(rel.getBbCourseColumnPrimaryId());
                    }
                    gradeMap.putAll(getGradesForColumns(coursePrimaryId, columnIds));
                }

                String ret = DataAccessServices.dao().doInTransaction(session -> {
                    // Получаем все оценки журнала
                    DQLSelectColumnNumerator dql1 = new DQLSelectColumnNumerator(
                            new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "ge")

                                    // Джойним события курса ВВ
                                    .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.event().fromAlias("ge"), "gee")
                                    .joinEntity("ge", DQLJoinType.inner, BbTrJournalEventRel.class, "rel",
                                                eq(property("gee", TrEduGroupEvent.L_JOURNAL_EVENT), property("rel", BbTrJournalEventRel.event())))

//                                        .joinEntity("ge", DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.class, "gr", and(
//                                                eq(property(EppRealEduGroup4LoadTypeRow.group().fromAlias("gr")), property(TrEduGroupEvent.group().fromAlias("gee"))),
//                                                // для всех активных связей студентов УГС
//                                                isNull(property(EppRealEduGroup4LoadTypeRow.removalDate().fromAlias("gr")))
//                                        ))

                                            // Джойним участников курсов
                                    .joinPath(DQLJoinType.inner, BbTrJournalEventRel.courseRelation().fromAlias("rel"), "crel")
                                    .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.studentWpe().fromAlias("ge"), "slot")
                                    .joinEntity("ge", DQLJoinType.inner, BbCourseStudent.class, "s", and(
                                            eq(property("crel", BbCourse2TrJournalRel.L_BB_COURSE), property("s", BbCourseStudent.L_BB_COURSE)),
                                            eq(property("slot", EppStudentWorkPlanElement.L_STUDENT), property("s", BbCourseStudent.L_STUDENT))
                                    ))

                                    .where(eq(property("ge", TrEduGroupEventStudent.event().journalEvent().journalModule().journal()), value(journal)))
                    );

                    int columnIdCol = dql1.column(property("rel", BbTrJournalEventRel.P_BB_COURSE_COLUMN_PRIMARY_ID));
                    int memberPrimaryIdCol = dql1.column(property("s", BbCourseStudent.P_BB_PRIMARY_ID));
                    int markCol = dql1.column("ge");

                    List<Object[]> items = dql1.getDql().createStatement(session).list();
                    for (Object[] item : items)
                    {
                        CoreCollectionUtils.Pair<String, String> key = new CoreCollectionUtils.Pair<>((String) item[columnIdCol], (String) item[memberPrimaryIdCol]);
                        Double bbGrade = gradeMap.get(key);
                        if (bbGrade != null)
                        {
                            TrEduGroupEventStudent mark = (TrEduGroupEventStudent) item[markCol];
                            if (!bbGrade.equals(mark.getGrade()))
                            {
                                mark.setGrade(bbGrade);
                                session.update(mark);
                                counter.increment();
                            }
                            gradeMap.remove(key);
                        }
                    }

                    if (!gradeMap.isEmpty())
                    {
                        return "Для " + gradeMap.size() + " оценок, полученных из Blackboard, не найдено соответствующих событий, в которые полученные оценки можно проставить.\n" +
                                "Вероятно, для некоторых контрольных мероприятий данной реализации нет события в расписании, либо оно еще наступило.";
                    }

                    return null;
                });

                return new ProcessResult("Обновлено " + counter.getValue() + " оценок." + (ret == null ? "" : " " + ret));
            }
        };

        new BackgroundProcessHolder().start("Получение оценок студентов из системы ЭО (Blackboard)", process, ProcessDisplayMode.unknown);
    }
}