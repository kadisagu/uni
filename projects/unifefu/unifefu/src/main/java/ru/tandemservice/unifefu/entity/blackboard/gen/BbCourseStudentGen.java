package ru.tandemservice.unifefu.entity.blackboard.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;
import ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Студент на курсе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class BbCourseStudentGen extends EntityBase
 implements INaturalIdentifiable<BbCourseStudentGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent";
    public static final String ENTITY_NAME = "bbCourseStudent";
    public static final int VERSION_HASH = 138626543;
    private static IEntityMeta ENTITY_META;

    public static final String L_BB_COURSE = "bbCourse";
    public static final String L_STUDENT = "student";
    public static final String P_ACCESS = "access";
    public static final String P_BB_PRIMARY_ID = "bbPrimaryId";

    private BbCourse _bbCourse;     // Электронный учебный курс (ЭУК) в системе Blackboard
    private Student _student;     // Студент
    private boolean _access = true;     // Имеет доступ к курсу
    private String _bbPrimaryId;     // Идентификатор участника курса в Blackboard

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     */
    @NotNull
    public BbCourse getBbCourse()
    {
        return _bbCourse;
    }

    /**
     * @param bbCourse Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     */
    public void setBbCourse(BbCourse bbCourse)
    {
        dirty(_bbCourse, bbCourse);
        _bbCourse = bbCourse;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Имеет доступ к курсу. Свойство не может быть null.
     */
    @NotNull
    public boolean isAccess()
    {
        return _access;
    }

    /**
     * @param access Имеет доступ к курсу. Свойство не может быть null.
     */
    public void setAccess(boolean access)
    {
        dirty(_access, access);
        _access = access;
    }

    /**
     * @return Идентификатор участника курса в Blackboard. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=16)
    public String getBbPrimaryId()
    {
        return _bbPrimaryId;
    }

    /**
     * @param bbPrimaryId Идентификатор участника курса в Blackboard. Свойство не может быть null и должно быть уникальным.
     */
    public void setBbPrimaryId(String bbPrimaryId)
    {
        dirty(_bbPrimaryId, bbPrimaryId);
        _bbPrimaryId = bbPrimaryId;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof BbCourseStudentGen)
        {
            if (withNaturalIdProperties)
            {
                setBbCourse(((BbCourseStudent)another).getBbCourse());
                setStudent(((BbCourseStudent)another).getStudent());
            }
            setAccess(((BbCourseStudent)another).isAccess());
            setBbPrimaryId(((BbCourseStudent)another).getBbPrimaryId());
        }
    }

    public INaturalId<BbCourseStudentGen> getNaturalId()
    {
        return new NaturalId(getBbCourse(), getStudent());
    }

    public static class NaturalId extends NaturalIdBase<BbCourseStudentGen>
    {
        private static final String PROXY_NAME = "BbCourseStudentNaturalProxy";

        private Long _bbCourse;
        private Long _student;

        public NaturalId()
        {}

        public NaturalId(BbCourse bbCourse, Student student)
        {
            _bbCourse = ((IEntity) bbCourse).getId();
            _student = ((IEntity) student).getId();
        }

        public Long getBbCourse()
        {
            return _bbCourse;
        }

        public void setBbCourse(Long bbCourse)
        {
            _bbCourse = bbCourse;
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof BbCourseStudentGen.NaturalId) ) return false;

            BbCourseStudentGen.NaturalId that = (NaturalId) o;

            if( !equals(getBbCourse(), that.getBbCourse()) ) return false;
            if( !equals(getStudent(), that.getStudent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBbCourse());
            result = hashCode(result, getStudent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBbCourse());
            sb.append("/");
            sb.append(getStudent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends BbCourseStudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) BbCourseStudent.class;
        }

        public T newInstance()
        {
            return (T) new BbCourseStudent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "bbCourse":
                    return obj.getBbCourse();
                case "student":
                    return obj.getStudent();
                case "access":
                    return obj.isAccess();
                case "bbPrimaryId":
                    return obj.getBbPrimaryId();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "bbCourse":
                    obj.setBbCourse((BbCourse) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "access":
                    obj.setAccess((Boolean) value);
                    return;
                case "bbPrimaryId":
                    obj.setBbPrimaryId((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "bbCourse":
                        return true;
                case "student":
                        return true;
                case "access":
                        return true;
                case "bbPrimaryId":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "bbCourse":
                    return true;
                case "student":
                    return true;
                case "access":
                    return true;
                case "bbPrimaryId":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "bbCourse":
                    return BbCourse.class;
                case "student":
                    return Student.class;
                case "access":
                    return Boolean.class;
                case "bbPrimaryId":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<BbCourseStudent> _dslPath = new Path<BbCourseStudent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "BbCourseStudent");
    }
            

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent#getBbCourse()
     */
    public static BbCourse.Path<BbCourse> bbCourse()
    {
        return _dslPath.bbCourse();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Имеет доступ к курсу. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent#isAccess()
     */
    public static PropertyPath<Boolean> access()
    {
        return _dslPath.access();
    }

    /**
     * @return Идентификатор участника курса в Blackboard. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent#getBbPrimaryId()
     */
    public static PropertyPath<String> bbPrimaryId()
    {
        return _dslPath.bbPrimaryId();
    }

    public static class Path<E extends BbCourseStudent> extends EntityPath<E>
    {
        private BbCourse.Path<BbCourse> _bbCourse;
        private Student.Path<Student> _student;
        private PropertyPath<Boolean> _access;
        private PropertyPath<String> _bbPrimaryId;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent#getBbCourse()
     */
        public BbCourse.Path<BbCourse> bbCourse()
        {
            if(_bbCourse == null )
                _bbCourse = new BbCourse.Path<BbCourse>(L_BB_COURSE, this);
            return _bbCourse;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Имеет доступ к курсу. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent#isAccess()
     */
        public PropertyPath<Boolean> access()
        {
            if(_access == null )
                _access = new PropertyPath<Boolean>(BbCourseStudentGen.P_ACCESS, this);
            return _access;
        }

    /**
     * @return Идентификатор участника курса в Blackboard. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent#getBbPrimaryId()
     */
        public PropertyPath<String> bbPrimaryId()
        {
            if(_bbPrimaryId == null )
                _bbPrimaryId = new PropertyPath<String>(BbCourseStudentGen.P_BB_PRIMARY_ID, this);
            return _bbPrimaryId;
        }

        public Class getEntityClass()
        {
            return BbCourseStudent.class;
        }

        public String getEntityName()
        {
            return "bbCourseStudent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
