package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x10x6_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEntrantIncomingListVar2Report

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("ffentrntincmnglstvr2rprt_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_7f93cc95"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("content_id", DBType.LONG).setNullable(false), 
				new DBColumn("formingdate_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false), 
				new DBColumn("datefrom_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("dateto_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("compensationtype_id", DBType.LONG).setNullable(false), 
				new DBColumn("studentcategorytitle_p", DBType.createVarchar(255)), 
				new DBColumn("orderbyoriginals_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("withoutdocumentinfo_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("withoutdetailsummark_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("showdisciplinetitles_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("shwrqstddrctnprrty_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("showdatabyenrollmentstage_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("qualificationtitle_p", DBType.createVarchar(255)), 
				new DBColumn("developformtitle_p", DBType.createVarchar(255)), 
				new DBColumn("developconditiontitle_p", DBType.createVarchar(255)), 
				new DBColumn("enrollmentdirection_id", DBType.LONG), 
				new DBColumn("notprintspeswithoutrequest_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("notprintnuminfo_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("includeforeignperson_p", DBType.BOOLEAN), 
				new DBColumn("includeentrantnomark_p", DBType.BOOLEAN), 
				new DBColumn("includeentrantwithbenefit_p", DBType.BOOLEAN), 
				new DBColumn("ncldentrnttrgtadmssn_p", DBType.BOOLEAN), 
				new DBColumn("entrantcustomstatetitle_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuEntrantIncomingListVar2Report");

		}


    }
}