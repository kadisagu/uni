package ru.tandemservice.unifefu.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.IFefuEntrantReport;
import ru.tandemservice.unifefu.entity.report.gen.FefuEnrolledListReportGen;

/**
 * Списки зачисленных
 */
public class FefuEnrolledListReport extends FefuEnrolledListReportGen implements IFefuEntrantReport
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}