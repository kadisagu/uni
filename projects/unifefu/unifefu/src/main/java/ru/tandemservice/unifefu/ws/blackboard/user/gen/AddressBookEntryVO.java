
package ru.tandemservice.unifefu.ws.blackboard.user.gen;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressBookEntryVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressBookEntryVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addressBookEntry" type="{http://user.ws.blackboard/xsd}UserExtendedInfoVO" minOccurs="0"/>
 *         &lt;element name="expansionData" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressBookEntryVO", namespace = "http://user.ws.blackboard/xsd", propOrder = {
    "addressBookEntry",
    "expansionData",
    "id",
    "title",
    "userId"
})
public class AddressBookEntryVO {

    @XmlElementRef(name = "addressBookEntry", namespace = "http://user.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<UserExtendedInfoVO> addressBookEntry;
    @XmlElement(nillable = true)
    protected List<String> expansionData;
    @XmlElementRef(name = "id", namespace = "http://user.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> id;
    @XmlElementRef(name = "title", namespace = "http://user.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> title;
    @XmlElementRef(name = "userId", namespace = "http://user.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> userId;

    /**
     * Gets the value of the addressBookEntry property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UserExtendedInfoVO }{@code >}
     *     
     */
    public JAXBElement<UserExtendedInfoVO> getAddressBookEntry() {
        return addressBookEntry;
    }

    /**
     * Sets the value of the addressBookEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UserExtendedInfoVO }{@code >}
     *     
     */
    public void setAddressBookEntry(JAXBElement<UserExtendedInfoVO> value) {
        this.addressBookEntry = ((JAXBElement<UserExtendedInfoVO> ) value);
    }

    /**
     * Gets the value of the expansionData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expansionData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpansionData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getExpansionData() {
        if (expansionData == null) {
            expansionData = new ArrayList<>();
        }
        return this.expansionData;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setId(JAXBElement<String> value) {
        this.id = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitle(JAXBElement<String> value) {
        this.title = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserId(JAXBElement<String> value) {
        this.userId = ((JAXBElement<String> ) value);
    }

}
