/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu25;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentCustomStateCICodes;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public class FefuConditionalTransferCourseStuExtractDao extends UniBaseDao implements IExtractComponentDao<FefuConditionalTransferCourseStuExtract>
{
    public void doCommit(FefuConditionalTransferCourseStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        // выставим статус "условный"
        StudentCustomStateCI customStateCI = DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), StudentCustomStateCICodes.USLOVNYY_PEREVOD);
        StudentCustomState studentCustomState = new StudentCustomState();
        studentCustomState.setCustomState(customStateCI);
        studentCustomState.setStudent(extract.getEntity());
        studentCustomState.setBeginDate(extract.getBeginDate());
        UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(studentCustomState);
        StudentCustomStateToExtractRelation extractRelation = new StudentCustomStateToExtractRelation();
        extractRelation.setStudentCustomState(studentCustomState);
        extractRelation.setExtract(extract);
        save(extractRelation);

        MoveStudentDaoFacade.getCommonExtractUtil().doCommitWithoutChangeStatus(extract);
        doCaptainStudent(extract.getEntity(), extract.getGroupOld(), extract.getGroupNew());
    }

    public void doRollback(FefuConditionalTransferCourseStuExtract extract, Map parameters)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().doRollbackWithoutChangeStatus(extract);
        doCaptainStudent(extract.getEntity(), extract.getGroupNew(), extract.getGroupOld());

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateToExtractRelation.class, "rel").
                column(property("rel", StudentCustomStateToExtractRelation.studentCustomState().id())).
                where(eq(property("rel", StudentCustomStateToExtractRelation.extract().id()), value(extract.getId())));
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(StudentCustomState.class).
                where(in(property(StudentCustomState.id()), builder.buildQuery()));
        deleteBuilder.createStatement(getSession()).execute();
    }

    private void doCaptainStudent(Student student, Group groupOld, Group groupNew)
    {
        if ((UniDaoFacade.getGroupDao().isCaptainStudent(groupOld, student)) && !UniDaoFacade.getGroupDao().isCaptainStudent(groupNew, student))
        {
            UniDaoFacade.getGroupDao().deleteCaptainStudent(groupOld, student);
            UniDaoFacade.getGroupDao().addCaptainStudent(groupNew, student);
        }
    }
}
