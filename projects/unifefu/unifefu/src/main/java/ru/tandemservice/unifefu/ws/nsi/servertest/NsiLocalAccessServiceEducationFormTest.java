/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import ru.tandemservice.unifefu.ws.nsi.datagram.EducationFormType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import javax.xml.namespace.QName;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceEducationFormTest extends NsiLocalAccessBaseTest
{
    public static final String[][] RETRIEVE_WHOLE_PARAMS = new String[][]{{null}};
    public static final String[][] RETRIEVE_NON_EXISTS_PARAMS = new String[][]{{"aaaaaaaa-8d45-3209-be25-ec921dcdffa3"}};
    public static final String[][] RETRIEVE_SINGLE_PARAMS = new String[][]{{"f1e4828d-8d45-3209-be25-ec921dcdffa3"}};
    public static final String[][] RETRIEVE_FEW_PARAMS = new String[][]{{"f272ea5f-3fc6-11e1-85f3-001b245d68a8"}, {"f1e4828d-8d45-3209-be25-ec921dcdffa3"}, {"aaaaaaaa-8d45-3209-be25-ec921dcdffa3"}, {"69cb1a2c-ece1-3aae-b39b-6173796c6b75"}};

    public static final String[][] INSERT_NEW_PARAMS = new String[][]{{"69cb1a2c-ece1-3aae-b39b-xxxxxxxxxxxX", "Перезаочная", "переза", "7", "Г"}};
    public static final String[][] INSERT_NEW_ANALOG_PARAMS = new String[][]{{"69cb1a2c-ece1-3aae-b39b-xxxxxxxxxxxY", "Заочная", "заочн", "2", "З"}};
    public static final String[][] INSERT_NEW_GUID_EXISTS_PARAMS = new String[][]{{"69cb1a2c-ece1-3aae-b39b-xxxxxxxxxxxY", "Заочная1", "заочн1", "2", "З1"}};
    public static final String[][] INSERT_NEW_MERGE_GUID_EXISTS_PARAMS = new String[][]{{"69cb1a2c-ece1-3aae-b39b-xxxxxxxxxxxY", "Заочная2", "заочн2", "2", "З12", "69cb1a2c-ece1-3aae-b39b-xxxxxxxxxxxY; 69cb1a2c-ece1-3aae-b39b-xxxxxxxxxxxX; 90800349-619a-11e0-a335-xxxxxxxxxx38"}};
    public static final String[] INSERT_MASS_TEMPLATE_PARAMS = new String[]{"Тест", "Т"};

    public static final String[][] DELETE_EMPTY_PARAMS = new String[][]{{null}};
    public static final String[][] DELETE_NON_EXISTS_PARAMS = new String[][]{{"cb3c4af3-cdb9-304c-af85-51dc5936572X"}};
    public static final String[][] DELETE_SINGLE_PARAMS = new String[][]{{"cb3c4af3-cdb9-304c-af85-51dc5936572a"}};
    public static final String[][] DELETE_NON_DELETABLE_PARAMS = new String[][]{{"cb3c4af3-cdb9-304c-af85-51dc5936572a"}};
    public static final String[][] DELETE_MASS_PARAMS = new String[][]{{"cb3c4af3-cdb9-304c-af85-51dc5936572a"}};

    @Override
    protected INsiEntity createNsiEntity(String[] fieldValues)
    {
        EducationFormType entity = NsiLocalAccessServiceTestUtil.FACTORY.createEducationFormType();
        if (null != fieldValues)
        {
            if (fieldValues.length > 0) entity.setID(fieldValues[0]);
            if (fieldValues.length > 1) entity.setEducationFormName(fieldValues[1]);
            if (fieldValues.length > 2) entity.setEducationFormNameShort(fieldValues[2]);
            if (fieldValues.length > 3) entity.setEducationFormID(fieldValues[3]);
            if (fieldValues.length > 4) entity.setEducationFormGroup(fieldValues[4]);
            if (fieldValues.length > 5 && null != fieldValues[5])
                entity.getOtherAttributes().put(new QName("mergeDuplicates"), fieldValues[5]);
        }
        return entity;
    }
}