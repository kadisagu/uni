/**
 * FefuMobileDataProviderServiceImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.mobile.test;

public interface FefuMobileDataProviderServiceImpl extends javax.xml.rpc.Service {
    public java.lang.String getFefuMobileDataProviderServicePortAddress();

    public ru.tandemservice.unifefu.ws.mobile.test.FefuMobileDataProviderService getFefuMobileDataProviderServicePort() throws javax.xml.rpc.ServiceException;

    public ru.tandemservice.unifefu.ws.mobile.test.FefuMobileDataProviderService getFefuMobileDataProviderServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
