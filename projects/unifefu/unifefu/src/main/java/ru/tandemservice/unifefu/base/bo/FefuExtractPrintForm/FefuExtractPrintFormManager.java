/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.logic.FefuPrintFormChangeDAO;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.logic.IFefuPrintFormChangeDAO;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
@Configuration
public class FefuExtractPrintFormManager extends BusinessObjectManager
{
    public static FefuExtractPrintFormManager instance()
    {
        return instance(FefuExtractPrintFormManager.class);
    }

    @Bean
    public IFefuPrintFormChangeDAO dao()
    {
        return new FefuPrintFormChangeDAO();
    }
}