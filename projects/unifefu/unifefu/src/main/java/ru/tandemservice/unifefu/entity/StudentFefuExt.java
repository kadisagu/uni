package ru.tandemservice.unifefu.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.gen.StudentFefuExtGen;

/**
 * Студент (расширение ДВФУ)
 */
public class StudentFefuExt extends StudentFefuExtGen
{
    public StudentFefuExt()
    {

    }

    public StudentFefuExt(Student student)
    {
        setStudent(student);
    }

    public String getCheckLotusTitle()
    {
        if (getCheckedLotus() != null && getCheckedLotus())
            return "Да" + ", " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCheckedLotusDate());
           else
            return "Нет";
    }
}