package ru.tandemservice.unifefu.entity.ws;

import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuDirectumOrderTypeCodes;
import ru.tandemservice.unifefu.entity.ws.gen.*;

/**
 * Настройка параметров Directum для типа приказа по студентам
 */
public class FefuDirectumEnrSettings extends FefuDirectumEnrSettingsGen
{
    public FefuDirectumEnrSettings()
    {
    }

    public FefuDirectumEnrSettings(EntrantEnrollmentOrderType extractType, FefuDirectumOrderType orderType)
    {
        setEntrantEnrollmentOrderType(extractType);
        setFefuDirectumOrderType(orderType);
    }

    public Boolean getDismissFake()
    {
        return null;
    }

    public Boolean getRestoreFake()
    {
        return null;
    }

    public Boolean getGrantMatAidFake()
    {
        return null;
    }

    public Boolean getSocialFake()
    {
        return null;
    }

    public Boolean getPracticeFake()
    {
        return null;
    }

    public Boolean getGroupManagerFake()
    {
        return null;
    }

    public Boolean getPenaltyFake()
    {
        return null;
    }

    public Boolean getPaymentsFake()
    {
        return null;
    }

    public Boolean getStateFormularFake()
    {
        return null;
    }

    public Boolean getInternationalFake()
    {
        if (FefuDirectumOrderTypeCodes.ENR.equals(getFefuDirectumOrderType().getCode()))
            return super.isInternational();
        return null;
    }
}