/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.sec.entity.codes.AuthenticationTypeCodes;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.*;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRole;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Dmitry Seleznev
 * @since 02.07.2014
 */
public class NsiObjectsHolder
{
    /**
     * Наименование проперти, в которой должен быть указан guid подсистемы ОБ в НСИ
     */
    private static final String OB_SUBSYSTEM_GUID_PROPERTY = "fefu.nsi.subsystem.ob.guid";

    /**
     * GUID подсистемы ОБ в НСИ. Харнится в конфигах в проперте fefu.nsi.subsystem.ob.guid
     */
    private static String _obSubsystemGuid;

    /**
     * Мап полов. Ключ - название элемента в верхнем регистре, значение - элемент справчника
     */
    private static Map<String, Sex> _sexMap;

    /**
     * Мап типов УЛ. Ключ - GUID, ID, название (в верхнем регистре), или код элемента справочника (в верхнем регистре).
     * Значение - пара: элемент справочника "Типы удостоверений личнсоти" и ID объекта в НСИ.
     */
    private static Map<String, CoreCollectionUtils.Pair<IdentityCardType, FefuNsiIds>> _identityCardTypesMap;

    /**
     * Мап подразделений. Ключ - GUID, ID, или название (в верхнем регистре).
     * Значение - пара: Подразделение и ID объекта в НСИ.
     */
    protected static Map<String, CoreCollectionUtils.Pair<OrgUnit, FefuNsiIds>> _orgUnitIdToEntityMap;

    /**
     * Мап стран (для гражданства). Ключ - GUID, ID, название (в верхнем регистре), или код элемента справочника (в верхнем регистре).
     * Значение - пара: элемент справочника "Типы удостоверений личнсоти" и ID объекта в НСИ.
     */
    protected static Map<String, CoreCollectionUtils.Pair<AddressCountry, FefuNsiIds>> _countryIdToEntityMap;

    /**
     * Мап должностей. Ключ - GUID, ID, название (в верхнем регистре), или код элемента справочника (в верхнем регистре).
     * Значение - пара: элемент справочника "Должности, отнесенные к ПКГ и КУ" и ID объекта в НСИ.
     */
    private static Map<String, CoreCollectionUtils.Pair<PostBoundedWithQGandQL, FefuNsiIds>> _postBoundedMap;

    /**
     * Мап ученых степеней. Ключ - GUID, ID, название (в верхнем регистре), или код элемента справочника (в верхнем регистре).
     * Значение - пара: элемент справочника "Ученые степени" и ID объекта в НСИ.
     */
    private static Map<String, CoreCollectionUtils.Pair<ScienceDegree, FefuNsiIds>> _scienceDegreeMap;

    /**
     * Мап ученых званий. Ключ - GUID, ID, название (в верхнем регистре), или код элемента справочника (в верхнем регистре).
     * Значение - пара: элемент справочника "Ученые звания" и ID объекта в НСИ.
     */
    private static Map<String, CoreCollectionUtils.Pair<ScienceStatus, FefuNsiIds>> _scienceStatusMap;

    /**
     * Мап Отраслей науки. Ключ - ID, название (в верхнем регистре), или код элемента справочника (в верхнем регистре).
     * Значение - пара: элемент справочника "Отрасли науки" и ID объекта в НСИ (второй всегда пустой).
     */
    private static Map<String, CoreCollectionUtils.Pair<ScienceBranch, FefuNsiIds>> _scienceBranchMap;

    /**
     * Мап Специальностей по диплому. Ключ - ID, название (в верхнем регистре), или код элемента справочника (в верхнем регистре).
     * Значение - пара: элемент справочника "Специальности по диплому" и ID объекта в НСИ (второй всегда пустой).
     */
    private static Map<String, CoreCollectionUtils.Pair<EmployeeSpeciality, FefuNsiIds>> _employeeSpecialityMap;

    /**
     * Мап связей должностей с типами подразделений (Damn it). Ключ - пара: ID типа подразделения и ID должности.
     * Значение - связь типа подразеления и должности, на которыую ссылается сотрудник на должности.
     */
    private static Map<CoreCollectionUtils.Pair<Long, Long>, OrgUnitTypePostRelation> _relationsMap;

    /**
     * Мап кадровых ресурсов. Ключ - ID кадрового ресурса, или ID персоны. Значение - Кадровый ресурс.
     */
    private static Map<String, Employee> _employeeMap;

    /**
     * Мап персон. Ключ - GUID, или ID. Значение - пара: Персона и ID объекта в НСИ.
     */
    protected static Map<String, CoreCollectionUtils.Pair<Person, FefuNsiIds>> _personIdToEntityMap;
    //protected static Map<String, CoreCollectionUtils.Pair<IdentityCard, FefuNsiIds>> _identityCardIdToEntityMap;

    /**
     * Тип должности сотрудника ППС
     */
    private static EmployeeType _employeeTypePPS; // UniDaoFacade.getCoreDao().getCatalogItem(EmployeeType.class, EmployeeTypeCodes.EDU_STAFF);

    /**
     * Тип должности сотрудника РС
     */
    private static EmployeeType _employeeTypeRS; // UniDaoFacade.getCoreDao().getCatalogItem(EmployeeType.class, EmployeeTypeCodes.WORK_STAFF);

    /**
     * Тип должности сотрудника АУП
     */
    private static EmployeeType _employeeTypeAUP; // UniDaoFacade.getCoreDao().getCatalogItem(EmployeeType.class, EmployeeTypeCodes.MANAGEMENT_STAFF);

    /**
     * Квалификационный уровень сотрудника по умолчанию
     */
    private static QualificationLevel _defaultQualificationLevel; // DataAccessServices.dao().getList(QualificationLevel.class).get(0);

    /**
     * Профессионально-квалификационная группа сотрудника по умолчанию
     */
    private static ProfQualificationGroup _defaultProfQualificationGroup; // DataAccessServices.dao().getList(ProfQualificationGroup.class).get(0);

    /**
     * Тип аутентификации новых пользователей в системе по умолчанию
     */
    private static AuthenticationType _defaultPrincipalAuthenticationType; // DataAccessServices.dao().getByNaturalId(new AuthenticationType.NaturalId(AuthenticationTypeCodes.LDAP));

    /**
     * Мап сотрудников на должности. Ключ - GUID, или "Фамилия Имя Отчество Должность Подразделение" (в верхнем регистре).
     * Знаение - сотрудник на должности.
     */
    protected static Map<String, CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds>> _employeePostIdToEntityMap;

    /**
     * Мап ролей НСИ. Ключ - GUID.
     * Знаение - Роль НСИ.
     */
    protected static Map<String, CoreCollectionUtils.Pair<FefuNsiRole, FefuNsiIds>> _nsiRoleIdToEntityMap;

    /**
     * Мап категорий студентов. Ключ - GUID, ID, название (в верхнем регистре), или код элемента справочника (в верхнем регистре).
     * Значение - пара: элемент справочника "Категории студентов" и ID объекта в НСИ.
     */
    private static Map<String, CoreCollectionUtils.Pair<StudentCategory, FefuNsiIds>> _studentCategoryMap;

    /**
     * Мап статусов студентов. Ключ - GUID, ID, название (в верхнем регистре), или код элемента справочника (в верхнем регистре).
     * Значение - пара: элемент справочника "Состояние студента" и ID объекта в НСИ.
     */
    private static Map<String, CoreCollectionUtils.Pair<StudentStatus, FefuNsiIds>> _studentStatusMap;

    /**
     * Мап видов возмещения затрат на обучение. Ключ - GUID, ID, название (в верхнем регистре), или код элемента справочника (в верхнем регистре).
     * Значение - пара: элемент справочника "Виды возмещения затрат" и ID объекта в НСИ.
     */
    private static Map<String, CoreCollectionUtils.Pair<CompensationType, FefuNsiIds>> _compensationTypeMap;

    /**
     * Мап курсов. Ключ - GUID, ID, название (в верхнем регистре), или код элемента справочника (в верхнем регистре).
     * Значение - пара: элемент справочника "Курсы" и ID объекта в НСИ.
     */
    private static Map<String, CoreCollectionUtils.Pair<Course, FefuNsiIds>> _courseMap;

    // ГЕТТЕРЫ


    public static String getObSubsystemGuid()
    {
        if (null == _obSubsystemGuid)
        {
            _obSubsystemGuid = ApplicationRuntime.getProperty(OB_SUBSYSTEM_GUID_PROPERTY);
            if (null == StringUtils.trimToNull(_obSubsystemGuid))
                _obSubsystemGuid = "aab50d2f-0119-4438-83f7-e77e8705c264";
        }
        return _obSubsystemGuid;
    }

    public static Map<String, Sex> getSexMap()
    {
        if (null == _sexMap)
        {
            _sexMap = new HashMap<>();
            for (Sex sex : DataAccessServices.dao().getList(Sex.class)) _sexMap.put(sex.getTitle().toUpperCase(), sex);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole Sex list was received from database ----------");
        }
        return _sexMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<IdentityCardType, FefuNsiIds>> getIdentityCardTypesMap()
    {
        if (null == _identityCardTypesMap)
        {
            _identityCardTypesMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(IdentityCardType.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole IdentityCardTypes list was received from database ----------");
        }
        return _identityCardTypesMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<OrgUnit, FefuNsiIds>> getOrgUnitIdToEntityMap()
    {
        if (null == _orgUnitIdToEntityMap)
        {
            _orgUnitIdToEntityMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(OrgUnit.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole OrgUnits list was received from database ----------");
        }
        return _orgUnitIdToEntityMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<AddressCountry, FefuNsiIds>> getCountryIdToEntityMap()
    {
        if (null == _countryIdToEntityMap)
        {
            _countryIdToEntityMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(AddressCountry.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole Countries list was received from database ----------");
        }
        return _countryIdToEntityMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<PostBoundedWithQGandQL, FefuNsiIds>> getPostBoundedMap()
    {
        if (null == _postBoundedMap)
        {
            _postBoundedMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(PostBoundedWithQGandQL.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole PostBoudedWithQGandQL list was received from database ----------");
        }
        return _postBoundedMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<ScienceDegree, FefuNsiIds>> getScienceDegreeMap()
    {
        if (null == _scienceDegreeMap)
        {
            _scienceDegreeMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(ScienceDegree.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole ScienceDegree list was received from database ----------");
        }
        return _scienceDegreeMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<ScienceStatus, FefuNsiIds>> getScienceStatusMap()
    {
        if (null == _scienceStatusMap)
        {
            _scienceStatusMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(ScienceStatus.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole ScienceStatus list was received from database ----------");
        }
        return _scienceStatusMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<ScienceBranch, FefuNsiIds>> getScienceBranchMap()
    {
        if (null == _scienceBranchMap)
        {
            _scienceBranchMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(ScienceBranch.class, null, true);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole ScienceBranch list was received from database ----------");
        }
        return _scienceBranchMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<EmployeeSpeciality, FefuNsiIds>> getEmployeeSpecialityMap()
    {
        if (null == _employeeSpecialityMap)
        {
            _employeeSpecialityMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(EmployeeSpeciality.class, null, true);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole EmployeeSpeciality list was received from database ----------");
        }
        return _employeeSpecialityMap;
    }

    public static Map<CoreCollectionUtils.Pair<Long, Long>, OrgUnitTypePostRelation> getRelationsMap()
    {
        if (null == _relationsMap)
        {
            _relationsMap = new HashMap<>();
            for (OrgUnitTypePostRelation postRelation : DataAccessServices.dao().getList(OrgUnitTypePostRelation.class))
            {
                _relationsMap.put(new CoreCollectionUtils.Pair<>(postRelation.getOrgUnitType().getId(), postRelation.getPostBoundedWithQGandQL().getId()), postRelation);
            }
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole OrgUnitTypePostRelations list was received from database ----------");
        }
        return _relationsMap;
    }

    public static Map<String, Employee> getEmployeeMap()
    {
        if (null == _employeeMap)
        {
            _employeeMap = new HashMap<>();
            List<Object[]> objectsList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(Employee.class, "e")
                    .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "ei", eq(property(Employee.person().id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ei"))))
                    .column("e").column("ei"));

            for (Object[] row : objectsList)
            {
                Employee employee = (Employee) row[0];
                FefuNsiIds nsiId = (FefuNsiIds) row[1];
                _employeeMap.put(employee.getId().toString(), employee);
                _employeeMap.put(employee.getPerson().getId().toString(), employee);
                if (null != nsiId) _employeeMap.put(nsiId.getGuid(), employee);
            }
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole Employee list was received from database ----------");
        }

        return _employeeMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<Person, FefuNsiIds>> getPersonIdToEntityMap()
    {
        if (null == _personIdToEntityMap)
        {
            _personIdToEntityMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(Person.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole Persons list was received from database ----------");
        }
        return _personIdToEntityMap;
    }

    public static CoreCollectionUtils.Pair<Person, FefuNsiIds> getPersonPair(String key)
    {
        CoreCollectionUtils.Pair<Person, FefuNsiIds> pair = getPersonIdToEntityMap().get(key);

        if (null == pair)
        {
            Long personId = null;

            try
            {
                personId = Long.parseLong(key);
            } catch (Exception e)
            {
            }

            if (null == personId)
            {
                Set<Long> entityIdSet = IFefuNsiSyncDAO.instance.get().getEntityIdSetByNsiGuidsList(Collections.singletonList(key));
                if (!entityIdSet.isEmpty()) personId = entityIdSet.iterator().next();
            }

            if (null != personId)
            {
                getPersonIdToEntityMap().putAll(IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(Person.class, Collections.singleton(personId)));
                pair = getPersonIdToEntityMap().get(key);
            }
        }
        return pair;
    }

/*    public static Map<String, CoreCollectionUtils.Pair<IdentityCard, FefuNsiIds>> getIdentityCardIdToEntityMap()
    {
        if (null == _identityCardIdToEntityMap)
        {
            _identityCardIdToEntityMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(IdentityCard.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole IdentityCards list was received from database ----------");
        }
        return _identityCardIdToEntityMap;
    }*/

    public static Map<String, CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds>> getEmployeePostIdToEntityMap()
    {
        if (null == _employeePostIdToEntityMap)
        {
            _employeePostIdToEntityMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(EmployeePost.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole EmployeePosts list was received from database ----------");
        }
        return _employeePostIdToEntityMap;
    }

    public static CoreCollectionUtils.Pair<EmployeePost, FefuNsiIds> getEmployeePostEntityToGuidPair(String guid)
    {
        if (null == getEmployeePostIdToEntityMap().get(guid))
        {
            getEmployeePostIdToEntityMap().putAll(IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMapByGuids(EmployeePost.class, Collections.singleton(guid), true));
        }
        return getEmployeePostIdToEntityMap().get(guid);
    }

    public static void addPerson(Person person, FefuNsiIds nsiIds)
    {
        if (null != _personIdToEntityMap && null != person)
        {
            CoreCollectionUtils.Pair<Person, FefuNsiIds> pair = new CoreCollectionUtils.Pair<>(person, nsiIds);
            _personIdToEntityMap.put(person.getId().toString(), pair);
            if (null != nsiIds && null != nsiIds.getGuid()) _personIdToEntityMap.put(nsiIds.getGuid(), pair);
        }
    }

    public static void removePersonDuplicates(List<Person> personsList)
    {
        if (null != _personIdToEntityMap && null != personsList)
        {
            for (Person person : personsList)
            {
                CoreCollectionUtils.Pair<Person, FefuNsiIds> pair = _personIdToEntityMap.get(person.getId().toString());
                if (null != pair)
                {
                    if (null != pair.getX()) _personIdToEntityMap.remove(pair.getX().getId().toString());
                    if (null != pair.getY()) _personIdToEntityMap.remove(pair.getY().getGuid());
                }
            }
        }
    }

    public static EmployeeType getEmployeeTypePPS()
    {
        if (null == _employeeTypePPS)
            _employeeTypePPS = UniDaoFacade.getCoreDao().getCatalogItem(EmployeeType.class, EmployeeTypeCodes.EDU_STAFF);
        return _employeeTypePPS;
    }

    public static EmployeeType getEmployeeTypeRS()
    {
        if (null == _employeeTypeRS)
            _employeeTypeRS = UniDaoFacade.getCoreDao().getCatalogItem(EmployeeType.class, EmployeeTypeCodes.WORK_STAFF);
        return _employeeTypeRS;
    }

    public static EmployeeType getEmployeeTypeAUP()
    {
        if (null == _employeeTypeAUP)
            _employeeTypeAUP = UniDaoFacade.getCoreDao().getCatalogItem(EmployeeType.class, EmployeeTypeCodes.MANAGEMENT_STAFF);
        return _employeeTypeAUP;
    }

    public static QualificationLevel getDefaultQualificationLevel()
    {
        if (null == _defaultQualificationLevel)
            _defaultQualificationLevel = DataAccessServices.dao().getList(QualificationLevel.class).get(0);
        return _defaultQualificationLevel;
    }

    public static ProfQualificationGroup getDefaultProfQualificationGroup()
    {
        if (null == _defaultProfQualificationGroup)
            _defaultProfQualificationGroup = DataAccessServices.dao().getList(ProfQualificationGroup.class).get(0);
        return _defaultProfQualificationGroup;
    }

    public static AuthenticationType getDefaultPrincipalAuthenticationType()
    {
        if (null == _defaultPrincipalAuthenticationType)
            _defaultPrincipalAuthenticationType = DataAccessServices.dao().getByNaturalId(new AuthenticationType.NaturalId(AuthenticationTypeCodes.LDAP));
        return _defaultPrincipalAuthenticationType;
    }

    public static Map<String, CoreCollectionUtils.Pair<FefuNsiRole, FefuNsiIds>> getNsiRoleIdToEntityMap()
    {
        if (null == _nsiRoleIdToEntityMap)
        {
            _nsiRoleIdToEntityMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(FefuNsiRole.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole FefuNsiRoles list was received from database ----------");
        }
        return _nsiRoleIdToEntityMap;
    }

    public static CoreCollectionUtils.Pair<FefuNsiRole, FefuNsiIds> getRolePair(String key)
    {
        CoreCollectionUtils.Pair<FefuNsiRole, FefuNsiIds> pair = getNsiRoleIdToEntityMap().get(key);

        if (null == pair)
        {
            Long nsiRoleId = null;

            try
            {
                nsiRoleId = Long.parseLong(key);
            } catch (Exception e)
            {
            }

            if (null == nsiRoleId)
            {
                Set<Long> entityIdSet = IFefuNsiSyncDAO.instance.get().getEntityIdSetByNsiGuidsList(Collections.singletonList(key));
                if (!entityIdSet.isEmpty()) nsiRoleId = entityIdSet.iterator().next();
            }

            if (null != nsiRoleId)
            {
                getNsiRoleIdToEntityMap().putAll(IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(FefuNsiRole.class, Collections.singleton(nsiRoleId)));
                pair = getNsiRoleIdToEntityMap().get(key);
            }
        }
        return pair;
    }

    public static Map<String, CoreCollectionUtils.Pair<StudentCategory, FefuNsiIds>> getStudentCategoryMap()
    {
        if (null == _studentCategoryMap)
        {
            _studentCategoryMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(StudentCategory.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole StudentCategories list was received from database ----------");
        }
        return _studentCategoryMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<StudentStatus, FefuNsiIds>> getStudentStatusMap()
    {
        if (null == _studentStatusMap)
        {
            _studentStatusMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(StudentStatus.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole StudentStatuses list was received from database ----------");
        }
        return _studentStatusMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<CompensationType, FefuNsiIds>> getCompensationTypeMap()
    {
        if (null == _compensationTypeMap)
        {
            _compensationTypeMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(CompensationType.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole CompensationTypes list was received from database ----------");
        }
        return _compensationTypeMap;
    }

    public static Map<String, CoreCollectionUtils.Pair<Course, FefuNsiIds>> getCourseMap()
    {
        if (null == _courseMap)
        {
            _courseMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(Course.class, null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole Course list was received from database ----------");
        }
        return _courseMap;
    }
}