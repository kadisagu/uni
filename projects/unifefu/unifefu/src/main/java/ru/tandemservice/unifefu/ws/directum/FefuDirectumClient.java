/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import org.apache.axis.AxisFault;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.base.bo.Directum.DirectumManager;
import ru.tandemservice.unifefu.base.bo.Directum.logic.DirectumOperationResult;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;

/**
 * @author Dmitry Seleznev
 * @since 10.07.2013
 */
public class FefuDirectumClient
{
    private static final IntegrationServices SERVICE = new IntegrationServicesLocator();

    public static String[] registerEDocument(AbstractStudentOrder order, EnrollmentOrder enrOrder, FefuDirectumOrderType orderType, String documentName, String topic, String studentFio, String developForm, String notice, byte[] document)
    {
        String datagram = null;
        try
        {
            IWebServices port = SERVICE.getBasicHttpBinding_IWebServices();
            DataExchangePackage pack = new DataExchangePackage();
            pack.getObjects().add(new EDocumentType(orderType, documentName, new SectionType(RequisiteType.createSingleDocRequisitesList(topic, studentFio, developForm, notice))));
            datagram = new String(toXml(pack));
            String[] preResult = port.EDocumentsCreate(datagram, null != document ? new byte[][]{document} : new byte[][]{});
            DirectumOperationResult operationResult;

            String[] result = preResult;
            if (result.length > 0) result = result[0].split(";");
            if (result.length > 0 && "0".equals(result[0]))
            {
                if (null != order)
                    operationResult = new DirectumOperationResult(order, null, "Создание документа в Directum", preResult[0], result[1], null, datagram, null);
                else
                    operationResult = new DirectumOperationResult(null, enrOrder, "Создание документа в Directum", preResult[0], result[1], null, datagram, null);
            } else
            {
                operationResult = new DirectumOperationResult(order, enrOrder, "Создание документа в Directum", preResult[0], null, null, datagram, null);
            }

            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(operationResult);

            return result;
        } catch (Exception e)
        {
            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(new DirectumOperationResult(order, enrOrder, "Создание документа в Directum", null, null, null, datagram, null));
            e.printStackTrace(System.out);
        }
        return null;
    }

    public static Integer attachEDocumentPrintForm(String directumOrderId, AbstractStudentOrder order, EnrollmentOrder enrOrder, FefuDirectumOrderType orderType, byte[] document)
    {
        try
        {
            IWebServices port = SERVICE.getBasicHttpBinding_IWebServices();
            Integer docAttachResult = port.EDocumentVersionUpdate(Integer.valueOf(directumOrderId), 1, "ACROREAD", document);
            DirectumOperationResult operationResult;

            if (null != docAttachResult && 1 == docAttachResult)
            {
                if (null != order)
                    operationResult = new DirectumOperationResult(order, null, "Отправка печатной формы документа в Directum", String.valueOf(docAttachResult), directumOrderId, null, null, null);
                else
                    operationResult = new DirectumOperationResult(null, enrOrder, "Отправка печатной формы в Directum", String.valueOf(docAttachResult), directumOrderId, null, null, null);
            } else
            {
                operationResult = new DirectumOperationResult(order, enrOrder, "Отправка печатной формы в Directum", null != docAttachResult ? String.valueOf(docAttachResult) : null, directumOrderId, null, null, null);
            }

            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(operationResult);

            return docAttachResult;
        } catch (Exception e)
        {
            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(new DirectumOperationResult(order, enrOrder, "Отправка печатной формы в Directum", null, null, null, null, null));
            e.printStackTrace();
        }
        return null;
    }

    public static String registerTask(AbstractStudentOrder order, EnrollmentOrder enrOrder, String directumOrderId, RouteFormWrapper routeWrapper, byte[] document)
    {
        DataExchangePackage pack = null;

        try
        {
            IWebServices port = SERVICE.getBasicHttpBinding_IWebServices();

            pack = new DataExchangePackage();
            WorkflowParamsType workflowParams = new WorkflowParamsType(ParamType.createSingleTaskParamsList(routeWrapper));
            AttachmentsType attachments = new AttachmentsType(new AttachmentType(directumOrderId));
            pack.getObjects().add(new TaskType(routeWrapper.getRouteCode(), workflowParams, attachments));
            String result = port.createTask(new String(toXml(pack)), new byte[][]{document});
            // DEV-4728
            String directumTaskId = StringUtils.trimToNull(result.replaceAll(";", ""));
            directumTaskId = StringUtils.isNumeric(directumTaskId) ? directumTaskId : null;

            DirectumOperationResult operationResult = new DirectumOperationResult(order, enrOrder, "Создание задачи в Directum", result, directumOrderId, directumTaskId, new String(toXml(pack)), null);

            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(operationResult);

            return directumTaskId;
        } catch (Exception e)
        {
            // DEV-4728
            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(new DirectumOperationResult(order, enrOrder, "Создание задачи в Directum", e.getMessage(), directumOrderId, null, new String(toXml(pack)), null));
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] getOrderScan(String directumOrderId)
    {
        try
        {
            IWebServices port = SERVICE.getBasicHttpBinding_IWebServices();
            byte[] result = port.getEDocument(Integer.valueOf(directumOrderId), null);

            if (null == result || result.length == 0)
            {
                throw new ApplicationException("Скан-копия приказа не найдена в СЭД Directum. Возможно она была удалена. Проверить это можно, перейдя на карточку задачи в СЭД Directum.");
            }
            return result;
        } catch (Exception e)
        {
            if (e instanceof AxisFault && ((AxisFault) e).getFaultString().contains("не найден"))
                throw new ApplicationException("Скан-копия приказа не найдена в СЭД Directum. Возможно она была удалена. Проверить это можно, перейдя на карточку задачи в СЭД Directum.");
            e.printStackTrace();
            throw new ApplicationException("При попытке получения скан-копии приказа произошла ошибка. Обратитесь к администратору системы.");
        }
    }

    public static byte[] toXml(final Object root)
    {
        try
        {
            final JAXBContext jc = JAXBContext.newInstance(root.getClass());
            final Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            final ByteArrayOutputStream ba = new ByteArrayOutputStream(8192);
            m.marshal(root, ba);
            return ba.toByteArray();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }
}