package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Квалификация
 */
public class FefuQualification extends FefuQualificationGen
{
    public FefuQualification()
    {

    }

    public FefuQualification(EppEduPlanVersionBlock block)
    {
        this.setBlock(block);
    }
}