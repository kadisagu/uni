/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppSchedule.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.View.SppScheduleView;

/**
 * @author nvankov
 * @since 2/14/14
 */

@Configuration
public class SppScheduleViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "fefu" + SppScheduleViewExtUI.class.getSimpleName();

    @Autowired
    private SppScheduleView _sppScheduleView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_sppScheduleView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, SppScheduleViewExtUI.class))
                .create();
    }

    @Bean
    public ButtonListExtension actionsExtension()
    {
        return buttonListExtensionBuilder(_sppScheduleView.actionsButtonExtPoint())
                .addButton(submitButton("sentToPortal", ADDON_NAME + ":onClickSentToPortal").permissionKey("sppScheduleViewSendToPortal").visible("addon:" + ADDON_NAME + ".sentToPortalVisible")).create(); // Выгрузить расписание в портал

    }
}
