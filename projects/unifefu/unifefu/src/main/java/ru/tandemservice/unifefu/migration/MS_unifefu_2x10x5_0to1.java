package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Andrey Andreev
 * @since 15.07.2016
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x10x5_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.5"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEnrolledDealListReport переносится в uniec с переименованием в enrolledDealListReport
		tool.renameTable("fefuenrolleddeallistreport_t", "enrolleddeallistreport_t");
        tool.entityCodes().rename("fefuEnrolledDealListReport", "enrolledDealListReport");
        tool.executeUpdate("delete from scriptitem_t where code_p=?", "fefuEnrolledDealList");//удаление старого шаблона
    }
}
