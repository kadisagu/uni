package ru.tandemservice.unifefu.base.ext.Depersonalization.objects;

import com.google.common.collect.ImmutableList;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.application.IModuleMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.DepersonalizationUtils;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;
import ru.tandemservice.unifefu.entity.Fefu_1C_pack;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.nul;

/**
 * @author avedernikov
 * @since 05.10.2015
 */

public class UnifefuClearBlobsDO implements IDepersonalizationObject
{
	@Override
	public String getTitle()
	{
		return "Очистка содержимого документов (ДВФУ)";
	}

	@Override
	public Collection<Class<? extends IEntity>> getEntityClasses()
	{
		return ImmutableList.of(
				Fefu_1C_pack.class,
				FefuOrderToPrintFormRelation.class
		);
	}

	@Override
	public String getSettingsName()
	{
		return "unifefuClearBlobs";
	}

	@Override
	public Collection<String> getDescription()
	{
		return ImmutableList.of(
				"Очищается «Запрос» из «Пакет обмена данными с подсистемой БУ/УК (1С)».",
				"Удаляются экземпляры сущностей «Связь приказа с печатной формой(для приказов ДПО/ДО)»."
		);
	}

	@Override
	public IModuleMeta getModule()
	{
		return DepersonalizationUtils.getModuleMeta(Fefu_1C_pack.class);
	}

	@Override
	public void apply(Session session)
	{
		CommonDAO.executeAndClear(new DQLUpdateBuilder(Fefu_1C_pack.class).set(Fefu_1C_pack.request().s(), nul(PropertyType.BLOB)), session);
		new DQLDeleteBuilder(FefuOrderToPrintFormRelation.class).createStatement(session).execute();
	}
}