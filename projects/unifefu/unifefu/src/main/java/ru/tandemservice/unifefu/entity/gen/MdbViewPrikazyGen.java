package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.MdbViewPrikazy;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приказы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewPrikazyGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.MdbViewPrikazy";
    public static final String ENTITY_NAME = "mdbViewPrikazy";
    public static final int VERSION_HASH = -734867624;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_ORDER = "order";
    public static final String L_STUDENT = "student";
    public static final String P_ORDER_NUMBER = "orderNumber";
    public static final String P_ORDER_TYPE = "orderType";
    public static final String P_MODULAR_REASON = "modularReason";
    public static final String P_LIST_REASON = "listReason";
    public static final String P_LIST_BASICS = "listBasics";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_ORDER_DATE = "orderDate";
    public static final String P_LAST_NAME = "lastName";
    public static final String P_FIRST_NAME = "firstName";
    public static final String P_MIDDLE_NAME = "middleName";
    public static final String P_PERSON_BENEFITS = "personBenefits";
    public static final String P_DESCRIPTION = "description";

    private AbstractStudentExtract _extract;     // id выписки
    private AbstractStudentOrder _order;     // id приказа
    private Student _student;     // Студент
    private String _orderNumber;     // Номер приказа
    private String _orderType;     // Тип приказа
    private String _modularReason;     // Причина индивидуального приказа
    private String _listReason;     // Причина списочного приказа
    private String _listBasics;     // Основания
    private Date _createDate;     // Дата формирования
    private Date _orderDate;     // Дата приказа
    private String _lastName;     // Фамилия студента
    private String _firstName;     // Имя студента
    private String _middleName;     // Отчество студента
    private String _personBenefits;     // Наименование льготы
    private String _description;     // Примечание к приказу

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return id выписки. Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract id выписки. Свойство не может быть null.
     */
    public void setExtract(AbstractStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return id приказа. Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order id приказа. Свойство не может быть null.
     */
    public void setOrder(AbstractStudentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Номер приказа.
     */
    @Length(max=255)
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    /**
     * @param orderNumber Номер приказа.
     */
    public void setOrderNumber(String orderNumber)
    {
        dirty(_orderNumber, orderNumber);
        _orderNumber = orderNumber;
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOrderType()
    {
        return _orderType;
    }

    /**
     * @param orderType Тип приказа. Свойство не может быть null.
     */
    public void setOrderType(String orderType)
    {
        dirty(_orderType, orderType);
        _orderType = orderType;
    }

    /**
     * @return Причина индивидуального приказа.
     */
    @Length(max=255)
    public String getModularReason()
    {
        return _modularReason;
    }

    /**
     * @param modularReason Причина индивидуального приказа.
     */
    public void setModularReason(String modularReason)
    {
        dirty(_modularReason, modularReason);
        _modularReason = modularReason;
    }

    /**
     * @return Причина списочного приказа.
     */
    @Length(max=255)
    public String getListReason()
    {
        return _listReason;
    }

    /**
     * @param listReason Причина списочного приказа.
     */
    public void setListReason(String listReason)
    {
        dirty(_listReason, listReason);
        _listReason = listReason;
    }

    /**
     * @return Основания.
     */
    @Length(max=2048)
    public String getListBasics()
    {
        return _listBasics;
    }

    /**
     * @param listBasics Основания.
     */
    public void setListBasics(String listBasics)
    {
        dirty(_listBasics, listBasics);
        _listBasics = listBasics;
    }

    /**
     * @return Дата формирования.
     */
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Дата приказа.
     */
    public Date getOrderDate()
    {
        return _orderDate;
    }

    /**
     * @param orderDate Дата приказа.
     */
    public void setOrderDate(Date orderDate)
    {
        dirty(_orderDate, orderDate);
        _orderDate = orderDate;
    }

    /**
     * @return Фамилия студента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLastName()
    {
        return _lastName;
    }

    /**
     * @param lastName Фамилия студента. Свойство не может быть null.
     */
    public void setLastName(String lastName)
    {
        dirty(_lastName, lastName);
        _lastName = lastName;
    }

    /**
     * @return Имя студента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFirstName()
    {
        return _firstName;
    }

    /**
     * @param firstName Имя студента. Свойство не может быть null.
     */
    public void setFirstName(String firstName)
    {
        dirty(_firstName, firstName);
        _firstName = firstName;
    }

    /**
     * @return Отчество студента.
     */
    @Length(max=255)
    public String getMiddleName()
    {
        return _middleName;
    }

    /**
     * @param middleName Отчество студента.
     */
    public void setMiddleName(String middleName)
    {
        dirty(_middleName, middleName);
        _middleName = middleName;
    }

    /**
     * @return Наименование льготы.
     */
    @Length(max=255)
    public String getPersonBenefits()
    {
        return _personBenefits;
    }

    /**
     * @param personBenefits Наименование льготы.
     */
    public void setPersonBenefits(String personBenefits)
    {
        dirty(_personBenefits, personBenefits);
        _personBenefits = personBenefits;
    }

    /**
     * @return Примечание к приказу.
     */
    @Length(max=2048)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Примечание к приказу.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewPrikazyGen)
        {
            setExtract(((MdbViewPrikazy)another).getExtract());
            setOrder(((MdbViewPrikazy)another).getOrder());
            setStudent(((MdbViewPrikazy)another).getStudent());
            setOrderNumber(((MdbViewPrikazy)another).getOrderNumber());
            setOrderType(((MdbViewPrikazy)another).getOrderType());
            setModularReason(((MdbViewPrikazy)another).getModularReason());
            setListReason(((MdbViewPrikazy)another).getListReason());
            setListBasics(((MdbViewPrikazy)another).getListBasics());
            setCreateDate(((MdbViewPrikazy)another).getCreateDate());
            setOrderDate(((MdbViewPrikazy)another).getOrderDate());
            setLastName(((MdbViewPrikazy)another).getLastName());
            setFirstName(((MdbViewPrikazy)another).getFirstName());
            setMiddleName(((MdbViewPrikazy)another).getMiddleName());
            setPersonBenefits(((MdbViewPrikazy)another).getPersonBenefits());
            setDescription(((MdbViewPrikazy)another).getDescription());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewPrikazyGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewPrikazy.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewPrikazy();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "order":
                    return obj.getOrder();
                case "student":
                    return obj.getStudent();
                case "orderNumber":
                    return obj.getOrderNumber();
                case "orderType":
                    return obj.getOrderType();
                case "modularReason":
                    return obj.getModularReason();
                case "listReason":
                    return obj.getListReason();
                case "listBasics":
                    return obj.getListBasics();
                case "createDate":
                    return obj.getCreateDate();
                case "orderDate":
                    return obj.getOrderDate();
                case "lastName":
                    return obj.getLastName();
                case "firstName":
                    return obj.getFirstName();
                case "middleName":
                    return obj.getMiddleName();
                case "personBenefits":
                    return obj.getPersonBenefits();
                case "description":
                    return obj.getDescription();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((AbstractStudentExtract) value);
                    return;
                case "order":
                    obj.setOrder((AbstractStudentOrder) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "orderNumber":
                    obj.setOrderNumber((String) value);
                    return;
                case "orderType":
                    obj.setOrderType((String) value);
                    return;
                case "modularReason":
                    obj.setModularReason((String) value);
                    return;
                case "listReason":
                    obj.setListReason((String) value);
                    return;
                case "listBasics":
                    obj.setListBasics((String) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "orderDate":
                    obj.setOrderDate((Date) value);
                    return;
                case "lastName":
                    obj.setLastName((String) value);
                    return;
                case "firstName":
                    obj.setFirstName((String) value);
                    return;
                case "middleName":
                    obj.setMiddleName((String) value);
                    return;
                case "personBenefits":
                    obj.setPersonBenefits((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "order":
                        return true;
                case "student":
                        return true;
                case "orderNumber":
                        return true;
                case "orderType":
                        return true;
                case "modularReason":
                        return true;
                case "listReason":
                        return true;
                case "listBasics":
                        return true;
                case "createDate":
                        return true;
                case "orderDate":
                        return true;
                case "lastName":
                        return true;
                case "firstName":
                        return true;
                case "middleName":
                        return true;
                case "personBenefits":
                        return true;
                case "description":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "order":
                    return true;
                case "student":
                    return true;
                case "orderNumber":
                    return true;
                case "orderType":
                    return true;
                case "modularReason":
                    return true;
                case "listReason":
                    return true;
                case "listBasics":
                    return true;
                case "createDate":
                    return true;
                case "orderDate":
                    return true;
                case "lastName":
                    return true;
                case "firstName":
                    return true;
                case "middleName":
                    return true;
                case "personBenefits":
                    return true;
                case "description":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return AbstractStudentExtract.class;
                case "order":
                    return AbstractStudentOrder.class;
                case "student":
                    return Student.class;
                case "orderNumber":
                    return String.class;
                case "orderType":
                    return String.class;
                case "modularReason":
                    return String.class;
                case "listReason":
                    return String.class;
                case "listBasics":
                    return String.class;
                case "createDate":
                    return Date.class;
                case "orderDate":
                    return Date.class;
                case "lastName":
                    return String.class;
                case "firstName":
                    return String.class;
                case "middleName":
                    return String.class;
                case "personBenefits":
                    return String.class;
                case "description":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewPrikazy> _dslPath = new Path<MdbViewPrikazy>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewPrikazy");
    }
            

    /**
     * @return id выписки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return id приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getOrder()
     */
    public static AbstractStudentOrder.Path<AbstractStudentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getOrderNumber()
     */
    public static PropertyPath<String> orderNumber()
    {
        return _dslPath.orderNumber();
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getOrderType()
     */
    public static PropertyPath<String> orderType()
    {
        return _dslPath.orderType();
    }

    /**
     * @return Причина индивидуального приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getModularReason()
     */
    public static PropertyPath<String> modularReason()
    {
        return _dslPath.modularReason();
    }

    /**
     * @return Причина списочного приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getListReason()
     */
    public static PropertyPath<String> listReason()
    {
        return _dslPath.listReason();
    }

    /**
     * @return Основания.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getListBasics()
     */
    public static PropertyPath<String> listBasics()
    {
        return _dslPath.listBasics();
    }

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getOrderDate()
     */
    public static PropertyPath<Date> orderDate()
    {
        return _dslPath.orderDate();
    }

    /**
     * @return Фамилия студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getLastName()
     */
    public static PropertyPath<String> lastName()
    {
        return _dslPath.lastName();
    }

    /**
     * @return Имя студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getFirstName()
     */
    public static PropertyPath<String> firstName()
    {
        return _dslPath.firstName();
    }

    /**
     * @return Отчество студента.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getMiddleName()
     */
    public static PropertyPath<String> middleName()
    {
        return _dslPath.middleName();
    }

    /**
     * @return Наименование льготы.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getPersonBenefits()
     */
    public static PropertyPath<String> personBenefits()
    {
        return _dslPath.personBenefits();
    }

    /**
     * @return Примечание к приказу.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    public static class Path<E extends MdbViewPrikazy> extends EntityPath<E>
    {
        private AbstractStudentExtract.Path<AbstractStudentExtract> _extract;
        private AbstractStudentOrder.Path<AbstractStudentOrder> _order;
        private Student.Path<Student> _student;
        private PropertyPath<String> _orderNumber;
        private PropertyPath<String> _orderType;
        private PropertyPath<String> _modularReason;
        private PropertyPath<String> _listReason;
        private PropertyPath<String> _listBasics;
        private PropertyPath<Date> _createDate;
        private PropertyPath<Date> _orderDate;
        private PropertyPath<String> _lastName;
        private PropertyPath<String> _firstName;
        private PropertyPath<String> _middleName;
        private PropertyPath<String> _personBenefits;
        private PropertyPath<String> _description;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return id выписки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return id приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getOrder()
     */
        public AbstractStudentOrder.Path<AbstractStudentOrder> order()
        {
            if(_order == null )
                _order = new AbstractStudentOrder.Path<AbstractStudentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getOrderNumber()
     */
        public PropertyPath<String> orderNumber()
        {
            if(_orderNumber == null )
                _orderNumber = new PropertyPath<String>(MdbViewPrikazyGen.P_ORDER_NUMBER, this);
            return _orderNumber;
        }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getOrderType()
     */
        public PropertyPath<String> orderType()
        {
            if(_orderType == null )
                _orderType = new PropertyPath<String>(MdbViewPrikazyGen.P_ORDER_TYPE, this);
            return _orderType;
        }

    /**
     * @return Причина индивидуального приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getModularReason()
     */
        public PropertyPath<String> modularReason()
        {
            if(_modularReason == null )
                _modularReason = new PropertyPath<String>(MdbViewPrikazyGen.P_MODULAR_REASON, this);
            return _modularReason;
        }

    /**
     * @return Причина списочного приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getListReason()
     */
        public PropertyPath<String> listReason()
        {
            if(_listReason == null )
                _listReason = new PropertyPath<String>(MdbViewPrikazyGen.P_LIST_REASON, this);
            return _listReason;
        }

    /**
     * @return Основания.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getListBasics()
     */
        public PropertyPath<String> listBasics()
        {
            if(_listBasics == null )
                _listBasics = new PropertyPath<String>(MdbViewPrikazyGen.P_LIST_BASICS, this);
            return _listBasics;
        }

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(MdbViewPrikazyGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getOrderDate()
     */
        public PropertyPath<Date> orderDate()
        {
            if(_orderDate == null )
                _orderDate = new PropertyPath<Date>(MdbViewPrikazyGen.P_ORDER_DATE, this);
            return _orderDate;
        }

    /**
     * @return Фамилия студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getLastName()
     */
        public PropertyPath<String> lastName()
        {
            if(_lastName == null )
                _lastName = new PropertyPath<String>(MdbViewPrikazyGen.P_LAST_NAME, this);
            return _lastName;
        }

    /**
     * @return Имя студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getFirstName()
     */
        public PropertyPath<String> firstName()
        {
            if(_firstName == null )
                _firstName = new PropertyPath<String>(MdbViewPrikazyGen.P_FIRST_NAME, this);
            return _firstName;
        }

    /**
     * @return Отчество студента.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getMiddleName()
     */
        public PropertyPath<String> middleName()
        {
            if(_middleName == null )
                _middleName = new PropertyPath<String>(MdbViewPrikazyGen.P_MIDDLE_NAME, this);
            return _middleName;
        }

    /**
     * @return Наименование льготы.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getPersonBenefits()
     */
        public PropertyPath<String> personBenefits()
        {
            if(_personBenefits == null )
                _personBenefits = new PropertyPath<String>(MdbViewPrikazyGen.P_PERSON_BENEFITS, this);
            return _personBenefits;
        }

    /**
     * @return Примечание к приказу.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazy#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(MdbViewPrikazyGen.P_DESCRIPTION, this);
            return _description;
        }

        public Class getEntityClass()
        {
            return MdbViewPrikazy.class;
        }

        public String getEntityName()
        {
            return "mdbViewPrikazy";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
