package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.SessionAttSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Аттестационный лист
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttSheetDocumentGen extends SessionTransferDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.SessionAttSheetDocument";
    public static final String ENTITY_NAME = "sessionAttSheetDocument";
    public static final int VERSION_HASH = -156462326;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionAttSheetDocumentGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttSheetDocumentGen> extends SessionTransferDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttSheetDocument.class;
        }

        public T newInstance()
        {
            return (T) new SessionAttSheetDocument();
        }
    }
    private static final Path<SessionAttSheetDocument> _dslPath = new Path<SessionAttSheetDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttSheetDocument");
    }
            

    public static class Path<E extends SessionAttSheetDocument> extends SessionTransferDocument.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return SessionAttSheetDocument.class;
        }

        public String getEntityName()
        {
            return "sessionAttSheetDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
