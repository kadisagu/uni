/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import org.apache.commons.lang.StringUtils;

import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 22.01.2015
 */
public class ScienceDiplomaWrapper
{
    private String _seria;
    private String _number;
    private Date _date;

    public boolean isEmpty()
    {
        return null == _seria && null == _number && null == _date;
    }

    public String getSeria()
    {
        return _seria;
    }

    public void setSeria(String seria)
    {
        _seria = StringUtils.trimToNull(seria);
    }

    public String getNumber()
    {
        return _number;
    }

    public void setNumber(String number)
    {
        _number = StringUtils.trimToNull(number);
    }

    public Date getDate()
    {
        return _date;
    }

    public void setDate(Date date)
    {
        _date = date;
    }
}