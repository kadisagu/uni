/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu22.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.unifefu.entity.FefuPerformConditionCourseTransferListExtract;

/**
 * @author Igor Belanov
 * @since 30.06.2016
 */
public class Controller extends AbstractListParagraphAddEditController<FefuPerformConditionCourseTransferListExtract, IDAO, Model>
{
}
