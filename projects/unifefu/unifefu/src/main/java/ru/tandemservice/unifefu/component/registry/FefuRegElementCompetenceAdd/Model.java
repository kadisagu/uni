/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.registry.FefuRegElementCompetenceAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
@Input( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id")
} )
public class Model
{
    private Long _id;
    private ISelectModel _competenceModel;
    private List<FefuCompetence> _pickedCompetences = new ArrayList<>();

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public ISelectModel getCompetenceModel()
    {
        return _competenceModel;
    }

    public void setCompetenceModel(ISelectModel competenceModel)
    {
        _competenceModel = competenceModel;
    }

    public List<FefuCompetence> getPickedCompetences()
    {
        return _pickedCompetences;
    }

    public void setPickedCompetences(List<FefuCompetence> pickedCompetences)
    {
        _pickedCompetences = pickedCompetences;
    }
}