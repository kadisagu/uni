/* $Id$ */
package ru.tandemservice.unifefu.ws.portal;

import org.apache.axis.AxisFault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.w3c.dom.Element;
import ru.tandemservice.unifefu.entity.ws.FefuPortalNotification;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 12.06.2013
 */
public class FefuPortalClient
{
    public static final String PORTAL_SERVICE_URL = "fefu.portal.service.notifications.url";
    private static final String SUBSYSTEM_CODE = "PK";
    private static final String FUNCTION_CODE = "enrollment";
    private static final String DEFAULT_MIME_TYPE = "text";
    private static final int DEFAULT_PRIORITY = 1;

    private static final Logger LOGGER = LoggerFactory.getLogger(FefuPortalClient.class);

    public static List<FefuPortalNotification> sendNotifications(List<FefuPortalNotification> notifications)
    {
        if(null == ApplicationRuntime.getProperty(PORTAL_SERVICE_URL)) return null;

        try
        {
            List<Notification> list = new ArrayList<>();
            Map<String, FefuPortalNotification> notifMap = new HashMap<>();

            for (FefuPortalNotification item : notifications)
            {
                notifMap.put(item.getId().toString(), item);

                Notification notif = new Notification();
                notif.setInputid(item.getId().toString());
                notif.setSystemcode(SUBSYSTEM_CODE);
                notif.setFunctioncode(FUNCTION_CODE);
                notif.setPriority(DEFAULT_PRIORITY);

                Recipient rec = new Recipient();
                rec.setType(RecipientType.fromValue("user"));
                rec.setId(item.getPersonGuid().getGuid());

                List<Recipient> recList = new ArrayList<>();
                recList.add(rec);
                notif.setRecipients(recList.toArray(new Recipient[recList.size()]));
                notif.setMimeType(DEFAULT_MIME_TYPE);

                GregorianCalendar c = new GregorianCalendar();
                c.setTime(new Date());
                notif.setDatetime(c);
                notif.setText(item.getText());
                notif.setComment(item.getSubject());

                list.add(notif);
            }

	        SubmitNotificationsService service = new SubmitNotificationsServiceLocator();
	        SubmitNotifications port = service.getSubmitNotificationsPort();

	        SubmitNotificationsRequest request = new SubmitNotificationsRequest(list.toArray(new Notification[list.size()]));
            SubmitNotificationsResponse response = port.submitNotifications(request);

            Commit[] result = response.getCommits();
            List<FefuPortalNotification> resultNotifications = new ArrayList<>();

            for (Commit comm : result)
            {
                FefuPortalNotification notif = notifMap.get(comm.getInputid());
                if (null != notif)
                {
                    notif.setPortalId(comm.getOutputid());
                    notif.setSent(true);
                    resultNotifications.add(notif);
                }
           }

            return resultNotifications;

        } catch (Exception e)
        {
            if(e instanceof AxisFault && ((AxisFault)e).getFaultString().contains("java.net.ConnectException"))
            {
                LOGGER.warn("Connection to Portal webservice could not be established. It means no notifications could be sent to the FEFU Portal.");
            }
            else if (e instanceof AxisFault)
            {
                LOGGER.error("An AxisFault exception occurred", e);

                AxisFault fault = (AxisFault) e;
                System.out.println("FaultActor = " + fault.getFaultActor());
                System.out.println("FaultNode = " + fault.getFaultNode());
                System.out.println("FaultReason = " + fault.getFaultReason());
                System.out.println("FaultRole = " + fault.getFaultRole());
                System.out.println("FaultString = " + fault.getFaultString());
                System.out.println("FaultCode = " + fault.getFaultCode().toString());

                for(Element elem : fault.getFaultDetails())
                    System.out.println("FaultDetail = " + elem.toString());
            }
            else
            {
                LOGGER.error("A non AxisFault exception occurred", e);
            }
        }
        return null;
    }
}