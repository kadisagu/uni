/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.TestIDMStuff;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Dmitry Seleznev
 * @since 22.05.2013
 */
@Configuration
public class FefuSystemActionTestIDMStuff extends BusinessComponentManager
{
}