/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.settings.ImtsaCyclesConfiguration;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model, boolean gos2)
    {
        Model.GosGenerationBlock block = gos2 ? model.getBlockGos2() : model.getBlockGos3();
        IDataSettings dataSettings = block.getSettings();
        String imtsaCycle = dataSettings.get(Model.CYCLE_FILTER_NAME);

        List<FefuImtsaCyclePlanStructureRel> imtsaCyclePlanStructureRels = new DQLSelectBuilder()
                .fromEntity(FefuImtsaCyclePlanStructureRel.class, "ic")
                .where(likeUpper(property("ic", FefuImtsaCyclePlanStructureRel.imtsaCycle()), value(CoreStringUtils.escapeLike(imtsaCycle))))
                .where(eq(property("ic", FefuImtsaCyclePlanStructureRel.gos2()), value(gos2)))
                .order(property("ic", FefuImtsaCyclePlanStructureRel.imtsaCycle()))
                .createStatement(getSession())
                .list();

        UniBaseUtils.createPage(block.getDataSource(), imtsaCyclePlanStructureRels);
    }
}