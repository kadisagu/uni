/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuJournalReportRating.ui.History;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;

/**
 * @author DMITRY KNYAZEV
 * @since 28.04.2014
 */
public class FefuJournalReportRatingHistoryUI extends UIPresenter
{
	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		super.onBeforeDataSourceFetch(dataSource);
		if (dataSource.getName().equals(FefuJournalReportRatingHistory.HISTORY_DS))
		{
			dataSource.put(FefuJournalReportRatingHistory.FEFU_REQUEST_DATE_FROM, getSettings().get(FefuJournalReportRatingHistory.FEFU_REQUEST_DATE_FROM));
			dataSource.put(FefuJournalReportRatingHistory.FEFU_REQUEST_DATE_TO, getSettings().get(FefuJournalReportRatingHistory.FEFU_REQUEST_DATE_TO));
			dataSource.put(FefuJournalReportRatingHistory.FEFU_STUDENT_FIO, getSettings().get(FefuJournalReportRatingHistory.FEFU_STUDENT_FIO));
			dataSource.put(FefuJournalReportRatingHistory.EXTERNAL_ID, getSettings().get(FefuJournalReportRatingHistory.EXTERNAL_ID));
			dataSource.put(FefuJournalReportRatingHistory.XML_DATA_SEARCH, getSettings().get(FefuJournalReportRatingHistory.XML_DATA_SEARCH));
		}
	}

	public void onClickSearch()
	{
		getSettings().save();
	}

	public void onClickClear()
	{
		getSettings().clear();
		onClickSearch();
	}
}
