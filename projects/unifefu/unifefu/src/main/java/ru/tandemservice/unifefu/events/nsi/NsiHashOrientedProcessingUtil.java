/* $Id$ */
package ru.tandemservice.unifefu.events.nsi;

import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;

import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 23.12.2013
 */
public class NsiHashOrientedProcessingUtil
{
    public static final NsiEntityInfo createPersonNSIInfo(Person person)
    {
        NsiEntityInfo info = new NsiEntityInfo();
        info.setEntityID(person.getId());
        info.setChangeDate(new Date());
        info.setHash(getPersonHash(person));
        return info;
    }

    public static final Long getPersonHash(Person person)
    {
        Long hash = person.getId();
        if (null != person.getInnNumber()) hash = hash & person.getInnNumber().hashCode();
        if (null != person.getSnilsNumber()) hash = hash & person.getSnilsNumber().hashCode();
        if (null != person.getIdentityCard()) hash = hash & person.getIdentityCard().hashCode();
        return hash;
    }

    public static final NsiEntityInfo createIdentityCardNSIInfo(IdentityCard icard)
    {
        NsiEntityInfo info = new NsiEntityInfo();
        info.setEntityID(icard.getId());
        info.setChangeDate(new Date());
        info.setHash(getIdentityCardHash(icard));
        return info;
    }

    public static final Long getIdentityCardHash(IdentityCard icard)
    {
        Long hash = icard.getId();
        if (null != icard.getSeria()) hash = hash & icard.getSeria().hashCode();
        if (null != icard.getNumber()) hash = hash & icard.getNumber().hashCode();
        if (null != icard.getCardType()) hash = hash & icard.getCardType().hashCode();
        if (null != icard.getCitizenship()) hash = hash & icard.getCitizenship().hashCode();
        if (null != icard.getSex()) hash = hash & icard.getSex().hashCode();
        if (null != icard.getLastName()) hash = hash & icard.getLastName().hashCode();
        if (null != icard.getFirstName()) hash = hash & icard.getFirstName().hashCode();
        if (null != icard.getMiddleName()) hash = hash & icard.getMiddleName().hashCode();
        if (null != icard.getBirthDate()) hash = hash & icard.getBirthDate().hashCode();
        if (null != icard.getBirthPlace()) hash = hash & icard.getBirthPlace().hashCode();
        if (null != icard.getIssuanceDate()) hash = hash & icard.getIssuanceDate().hashCode();
        if (null != icard.getIssuancePlace()) hash = hash & icard.getIssuancePlace().hashCode();
        if (null != icard.getIssuanceCode()) hash = hash & icard.getIssuanceCode().hashCode();
        return hash;
    }
}