/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.SPOContractParAddEdit;

import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uniec.base.bo.EcOrder.util.BaseEcOrderParAddEditUI;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.entity.entrantOrder.EntrFefuSPOContractExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 29.07.2013
 */
public class FefuEcOrderSPOContractParAddEditUI extends BaseEcOrderParAddEditUI
{
    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        if (!isEditForm())
            setCourse(DevelopGridDAO.getCourseMap().get(1));
    }

    @Override
    public EnrollmentExtract createEnrollmentExtract(PreliminaryEnrollmentStudent preStudent)
    {
        return new EntrFefuSPOContractExtract();
    }
}