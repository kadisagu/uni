/* $Id$ */
package ru.tandemservice.unifefu.base.ext.TrJournal.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.TrJournalDao;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 15.04.2014
 */
public class FefuTrJournalDao extends TrJournalDao
{
    @Override
    public void doCopy(TrJournal sourceJournal, TrJournal targetJournal)
    {
        super.doCopy(sourceJournal, targetJournal);

        // То, что ниже - плохо. В том плане, что опирается на некоторые допущения. Основное допущение - в реализации может быть только одна УГС.

        getSession().flush();

        // Получаем УГС из исходной реализации
        TrJournalGroup srcGroup = getFefuJournalGroup(sourceJournal);

        // Получаем УГС в конечной реализации
        TrJournalGroup destGroup = getFefuJournalGroup(targetJournal);

        if ((srcGroup == null && destGroup == null) || destGroup == null)
            return;

        // Получаем события исходной реализации
        List<TrJournalEvent> srcEvents = getTrJournalEventList(srcGroup);

        // Получаем события итоговой реализации
        List<TrJournalEvent> destEvents = getTrJournalEventList(destGroup);

        // Копируем даты
        for (TrJournalEvent event : destEvents)
        {
            // Ищем среди исходных собитий событие с тем же номером
            TrJournalEvent srcEvent = null;
            for (TrJournalEvent e : srcEvents)
            {
                if (e.getNumber() == event.getNumber())
                {
                    srcEvent = e;
                    break;
                }
            }

            // Создаем событие УГС
            TrEduGroupEvent newGroupEvent = new TrEduGroupEvent(destGroup.getGroup(), event);

            // Копируем событие в расписании, если есть
            if (srcEvent != null && srcGroup != null)
            {
                TrEduGroupEvent srcGroupEvent = new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ge")
                        .column("ge").top(1)
                        .where(eq(property(TrEduGroupEvent.journalEvent().fromAlias("ge")), value(srcEvent)))
                        .createStatement(getSession()).uniqueResult();

                if (srcGroupEvent != null)
                {
                    if (srcGroupEvent.getScheduleEvent() != null)
                    {
                        ScheduleEvent scheduleEvent = new ScheduleEvent();
                        scheduleEvent.update(srcGroupEvent.getScheduleEvent(), false);
                        save(scheduleEvent);

                        newGroupEvent.setScheduleEvent(scheduleEvent);
                    }
                    newGroupEvent.setBellScheduleEntry(srcGroupEvent.getBellScheduleEntry());
                    newGroupEvent.setDeadlineDate(srcGroupEvent.getDeadlineDate());
                }
            }

            save(newGroupEvent);
        }
    }

    /**
     * Получение УГС для реализации.
     * Поскольку есть договоренность, что в ДВФУ всегда одна УГС в реализации, можно брать первую попавшуюся.
     */
    private TrJournalGroup getFefuJournalGroup(TrJournal journal)
    {
        return new DQLSelectBuilder().fromEntity(TrJournalGroup.class, "g")
                .column("g").top(1)
                .where(eq(property("g", TrJournalGroup.L_JOURNAL), value(journal)))
                .createStatement(getSession()).uniqueResult();
    }

    private List<TrJournalEvent> getTrJournalEventList(TrJournalGroup journalGroup)
    {
        if (journalGroup == null)
            return Collections.emptyList();

        return new DQLSelectBuilder().fromEntity(TrJournalEvent.class, "e")
                .column("e")
                .where(eq(property("e", TrJournalEvent.journalModule().journal()), value(journalGroup.getJournal())))
                .createStatement(getSession()).list();
    }
}