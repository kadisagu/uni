/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.FefuPpsSessionListDocumentManager;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.vo.FefuSessionListDocumentFilter;

/**
 * @author DMITRY KNYAZEV
 * @since 26.08.2014
 */
public class FefuPpsSessionListDocumentListUI extends UIPresenter
{
	private Person _person;

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(FefuPpsSessionListDocumentList.DOCUMENT_DS))
			dataSource.put(FefuPpsSessionListDocumentList.DOCUMENT_LIST, FefuPpsSessionListDocumentManager.instance().dao().getSessionListDocumentWrapperList(getPerson(), new FefuSessionListDocumentFilter(getSettings())));
	}

	//handlers
	public void onClickApply()
	{
		getSettings().save();
	}

	public void onClickCancel()
	{
		getSettings().clear();
	}

	//getters/setters
	public Person getPerson()
	{
		if (_person == null)
			_person = PersonManager.instance().dao().getPerson(getUserContext().getPrincipalContext());
		return _person;
	}
}
