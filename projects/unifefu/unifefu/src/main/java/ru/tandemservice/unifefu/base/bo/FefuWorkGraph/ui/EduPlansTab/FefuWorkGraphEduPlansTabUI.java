/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.EduPlansTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.FefuWorkGraphManager;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.EduPlanAdd.FefuWorkGraphEduPlanAdd;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraph;

/**
 * @author Alexander Zhebko
 * @since 09.10.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "workGraph.id", required = true))
public class FefuWorkGraphEduPlansTabUI extends UIPresenter
{
    private FefuWorkGraph _workGraph = new FefuWorkGraph();

    public FefuWorkGraph getWorkGraph(){ return _workGraph; }
    public void setWorkGraph(FefuWorkGraph workGraph){ _workGraph = workGraph; }

    public void onClickSelectEduPlanVersion()
    {
        _uiActivation.asRegionDialog(FefuWorkGraphEduPlanAdd.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, _workGraph.getId()).activate();
    }

    @Override
    public void onComponentRefresh()
    {
        _workGraph = FefuWorkGraphManager.instance().dao().getNotNull(FefuWorkGraph.class, _workGraph.getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(FefuWorkGraphEduPlansTab.EDU_PLAN_DS))
        {
            dataSource.put(FefuWorkGraphManager.BIND_WORK_GRAPH, _workGraph.getId());
        }
    }
}