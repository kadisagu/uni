package ru.tandemservice.unifefu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Часть учебного года"
 * Имя сущности : yearDistributionPart
 * Файл data.xml : unifefu-catalog.data.xml
 */
public interface YearDistributionPartCodes
{
    /** Константа кода (code) элемента : весь год (title) */
    String WHOLE_YEAR = "11";
    /** Константа кода (code) элемента : зимний семестр (title) */
    String WINTER_SEMESTER = "21";
    /** Константа кода (code) элемента : летний семестр (title) */
    String SUMMER_SEMESTER = "22";
    /** Константа кода (code) элемента : осенний триместр (title) */
    String AUTUMN_TERM = "31";
    /** Константа кода (code) элемента : зимний триместр (title) */
    String WINTER_TERM = "32";
    /** Константа кода (code) элемента : весенний триместр (title) */
    String SPRING_TERM = "33";
    /** Константа кода (code) элемента : I четверть (title) */
    String FIRST_SCHOOL_TERM = "41";
    /** Константа кода (code) элемента : II четверть (title) */
    String SECOND_SCHOOL_TERM = "42";
    /** Константа кода (code) элемента : III четверть (title) */
    String THIRD_SCHOOL_TERM = "43";
    /** Константа кода (code) элемента : IV четверть (title) */
    String FOURTH_SCHOOL_TERM = "44";

    Set<String> CODES = ImmutableSet.of(WHOLE_YEAR, WINTER_SEMESTER, SUMMER_SEMESTER, AUTUMN_TERM, WINTER_TERM, SPRING_TERM, FIRST_SCHOOL_TERM, SECOND_SCHOOL_TERM, THIRD_SCHOOL_TERM, FOURTH_SCHOOL_TERM);
}
