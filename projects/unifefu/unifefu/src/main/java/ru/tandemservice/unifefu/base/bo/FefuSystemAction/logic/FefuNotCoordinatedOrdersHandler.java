/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.DeleteNotCoordinatedOrders.FefuSystemActionDeleteNotCoordinatedOrders;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;


import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;

/**
 * @author Ekaterina Zvereva
 * @since 16.01.2015
 */
public class FefuNotCoordinatedOrdersHandler extends DefaultSearchDataSourceHandler
{
    public FefuNotCoordinatedOrdersHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Date dateFrom = context.get("ordersDateFrom");
        Date dateTo = context.get("ordersDateTo");
        Collection<IEntity> ordersTypesSet = context.get(FefuSystemActionDeleteNotCoordinatedOrders.ORDERS_TYPES_DS);

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(AbstractStudentOrder.class, "ord")
                .joinEntity("ord", DQLJoinType.left, AbstractStudentParagraph.class, "par", eq(property(AbstractStudentParagraph.order().fromAlias("par")), property("ord.id")))
                .joinEntity("par", DQLJoinType.left, AbstractStudentExtract.class, "ext", eq(property("par.id"), property(AbstractStudentExtract.paragraph().id().fromAlias("ext"))))
                .joinEntity("ext", DQLJoinType.left, StudentExtractType.class, "type", eq(property("ext", AbstractStudentExtract.type()), property("type.id")))
                .joinEntity("type", DQLJoinType.left, StudentExtractType.class, "ptype", eq(property("type", StudentExtractType.parent()), property("ptype.id")))
                .column(property("ord.id"), "orId")
                .column(
                        new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "ie")
                                .top(1)
                                .column(property("ie.id"))
                                .where(eq(property("ie", AbstractStudentExtract.paragraph().order()), property("ord.id")))
                                .order(property("ie", AbstractStudentExtract.paragraph().number()))
                                .order(property("ie", AbstractStudentExtract.number()))
                                .buildQuery(), "exId"
                )
                .where(ne(property(AbstractStudentOrder.state().code().fromAlias("ord")), value(OrderStatesCodes.FINISHED)))
                .group(property("ord.id"));

        if (ordersTypesSet != null && !ordersTypesSet.isEmpty())
            subBuilder.where(or((exists(new DQLSelectBuilder().fromEntity(StudentListOrder.class, "so")
                                                .where(eq(property("so", StudentListOrder.id()), property("ord.id")))
                                                .where(in(property("so", StudentListOrder.type()), ordersTypesSet))
                                                .buildQuery())),
                               in(property("type.id"), ordersTypesSet),
                             (in(property("ptype.id"), ordersTypesSet))

            ));

        if (dateFrom != null)
            subBuilder.where(ge(property(AbstractStudentOrder.createDate().fromAlias("ord")), value(dateFrom, PropertyType.DATE)));

        if (dateTo != null)
            subBuilder.where(le(property(AbstractStudentOrder.createDate().fromAlias("ord")), value(CoreDateUtils.getNextDayFirstTimeMoment(dateTo, 1), PropertyType.DATE)));


        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractStudentOrder.class, "o")
                .joinDataSource("o", DQLJoinType.inner, subBuilder.buildQuery(), "data", eq(property("o.id"), property("data.orId")))
                .column(property("o"))
                .column(property("data.exId"))
                .order(property(AbstractStudentOrder.createDate().fromAlias("o")), OrderDirection.asc)
                .order(property("o.id"), OrderDirection.asc);


        DQLExecutionContext dqlContext = new DQLExecutionContext(context.getSession());
        int totalSize = ((Long) builder.createCountStatement(dqlContext).uniqueResult()).intValue();
        int startRec = input.getStartRecord();
        int countRec = input.getCountRecord();

        if(-1 == startRec || totalSize < startRec)
        {
            startRec = totalSize - (0 != totalSize % countRec ? totalSize % countRec : countRec);
        }

        IDQLStatement statement = builder.createStatement(context.getSession());
        statement.setFirstResult(startRec);
        statement.setMaxResults(countRec);

        List<Object> resultList = new ArrayList<>();
        Map<Long, Long> relMap = new HashMap<>();

        for (Object[] item : statement.<Object[]>list())
        {
            AbstractStudentOrder order = (AbstractStudentOrder) item[0];
            DataWrapper dataWrapper = new DataWrapper(order);
            relMap.put(dataWrapper.getId(), (Long)item[1]);
            resultList.add(dataWrapper);
        }

        DSOutput output = new DSOutput(input);
        output.setRecordList(resultList);
        output.setTotalSize(totalSize);
        boolean admin = UserContext.getInstance().getPrincipalContext().isAdmin();

        List<DataWrapper> rows = DataWrapper.wrap(output);
        for(DataWrapper row:rows)
        {
            Object obj = row.getWrapped();
            boolean ignoreExtType = false;
            if(obj instanceof StudentListOrder)
            {
                row.setProperty(FefuSystemActionDeleteNotCoordinatedOrders.ORDER_TYPE, admin? ((StudentListOrder)obj).getType().getTitleWithCode(): (((StudentListOrder) obj).getType().getTitle()));
                ignoreExtType = true;
            }

            if (relMap.get(row.getId()) != null)
            {
                AbstractStudentExtract extract = DataAccessServices.dao().getNotNull(AbstractStudentExtract.class, relMap.get(row.getId()));
                if (!ignoreExtType)
                    row.setProperty(FefuSystemActionDeleteNotCoordinatedOrders.ORDER_TYPE, admin? extract.getType().getTitleWithCode() : extract.getType().getTitle());
                row.setProperty(FefuSystemActionDeleteNotCoordinatedOrders.STUDENT_FIO, extract.getEntity().getFio());
                row.setProperty(FefuSystemActionDeleteNotCoordinatedOrders.STUDENT_GROUP, extract.getEntity().getGroup() != null ? extract.getEntity().getGroup().getTitle() : "");

            }
        }


        return output;
      }
}