/**
 * GetEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class GetEntity  implements java.io.Serializable {
    private java.lang.String referenceName;

    private java.lang.Integer recordKey;

    private java.lang.String ISCode;

    public GetEntity() {
    }

    public GetEntity(
           java.lang.String referenceName,
           java.lang.Integer recordKey,
           java.lang.String ISCode) {
           this.referenceName = referenceName;
           this.recordKey = recordKey;
           this.ISCode = ISCode;
    }


    /**
     * Gets the referenceName value for this GetEntity.
     * 
     * @return referenceName
     */
    public java.lang.String getReferenceName() {
        return referenceName;
    }


    /**
     * Sets the referenceName value for this GetEntity.
     * 
     * @param referenceName
     */
    public void setReferenceName(java.lang.String referenceName) {
        this.referenceName = referenceName;
    }


    /**
     * Gets the recordKey value for this GetEntity.
     * 
     * @return recordKey
     */
    public java.lang.Integer getRecordKey() {
        return recordKey;
    }


    /**
     * Sets the recordKey value for this GetEntity.
     * 
     * @param recordKey
     */
    public void setRecordKey(java.lang.Integer recordKey) {
        this.recordKey = recordKey;
    }


    /**
     * Gets the ISCode value for this GetEntity.
     * 
     * @return ISCode
     */
    public java.lang.String getISCode() {
        return ISCode;
    }


    /**
     * Sets the ISCode value for this GetEntity.
     * 
     * @param ISCode
     */
    public void setISCode(java.lang.String ISCode) {
        this.ISCode = ISCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetEntity)) return false;
        GetEntity other = (GetEntity) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.referenceName==null && other.getReferenceName()==null) || 
             (this.referenceName!=null &&
              this.referenceName.equals(other.getReferenceName()))) &&
            ((this.recordKey==null && other.getRecordKey()==null) || 
             (this.recordKey!=null &&
              this.recordKey.equals(other.getRecordKey()))) &&
            ((this.ISCode==null && other.getISCode()==null) || 
             (this.ISCode!=null &&
              this.ISCode.equals(other.getISCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReferenceName() != null) {
            _hashCode += getReferenceName().hashCode();
        }
        if (getRecordKey() != null) {
            _hashCode += getRecordKey().hashCode();
        }
        if (getISCode() != null) {
            _hashCode += getISCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetEntity.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">GetEntity"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "ReferenceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "RecordKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "ISCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
