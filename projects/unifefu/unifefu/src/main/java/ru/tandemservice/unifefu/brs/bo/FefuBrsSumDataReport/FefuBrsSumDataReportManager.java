/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.brs.base.IBaseFefuBrsObjectManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.logic.FefuBrsSumDataReportDAO;
import ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.logic.IFefuBrsSumDataReportDAO;

/**
 * @author nvankov
 * @since 12/16/13
 */
@Configuration
public class FefuBrsSumDataReportManager extends BusinessObjectManager implements IBaseFefuBrsObjectManager
{
    @Override
    public String getPermissionKey()
    {
        return "fefuBrsSumDataReportPermissionKey";
    }

    public static FefuBrsSumDataReportManager instance()
    {
        return instance(FefuBrsSumDataReportManager.class);
    }

    @Bean
    public IFefuBrsSumDataReportDAO dao()
    {
        return new FefuBrsSumDataReportDAO();
    }
}



    