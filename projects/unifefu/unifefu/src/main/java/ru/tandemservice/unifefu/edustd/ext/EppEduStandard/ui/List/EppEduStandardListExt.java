/* $Id$ */
package ru.tandemservice.unifefu.edustd.ext.EppEduStandard.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.ui.List.EppEduStandardList;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.FefuEduStdManager;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.logic.FefuEduStandardHandler;

/**
 * @author Ekaterina Zvereva
 * @since 28.05.2015
 */
@Configuration
public class EppEduStandardListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "fefu" + EppEduStandardListExt.class.getSimpleName();
    @Autowired
    private EppEduStandardList _eppEduStandardList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eppEduStandardList.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EppEduStandardListExtUI.class))
                .replaceDataSource((searchListDS("eduStdDS", _eppEduStandardList.eduPlanDSColumns(), FefuEduStdManager.instance().fefuEppEduStandardHandler())))
                .create();
    }

    @Bean
    public ColumnListExtension eduPlanDSColumns()
    {
        return columnListExtensionBuilder(_eppEduStandardList.eduPlanDSColumns())
                .replaceColumn(textColumn("generation", FefuEduStandardHandler.GENERATION_EXT_COLUMN))
                .addAllBefore(EDIT_COLUMN_NAME)
                .addColumn(booleanColumn("customStandard", FefuEduStandardHandler.CUSTOM_STANDARD_COLUMN))
                .create();

    }
}
