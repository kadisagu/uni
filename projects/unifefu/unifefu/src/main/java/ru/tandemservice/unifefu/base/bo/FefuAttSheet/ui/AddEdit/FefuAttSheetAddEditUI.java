/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.AddEdit;

import com.google.common.collect.ImmutableList;
import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.form.validator.Required;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.FefuAttSheetManager;
import ru.tandemservice.unifefu.entity.SessionAttSheetDocument;
import ru.tandemservice.unifefu.entity.SessionAttSheetOperation;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.StudentByGroupOrgUnitComboDSHandler;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 01.07.2014
 */
@Input({@Bind(key = FefuAttSheetAddEditUI.STUDENT_ID, binding = "studentId"),
		@Bind(key = FefuAttSheetAddEditUI.DOCUMENT_ID, binding = "documentId"),
		@Bind(key = FefuAttSheetAddEditUI.ORG_UNIT_ID, binding = "orgUnitId")})
public class FefuAttSheetAddEditUI extends UIPresenter
{
	public static final String STUDENT_ID = "studentId";
	public static final String DOCUMENT_ID = "documentId";
	public static final String ORG_UNIT_ID = "orgUnitId";

	private static final String DVFU = "ДВФУ";

	private Long _documentId;
	private Long _orgUnitId;

	private Student _student;
	private SessionAttSheetDocument _document;
	private OrgUnit _orgUnit;
	private PpsEntrySelectBlockData ppsData;

	private List<FefuAttSheetWrapper> _rowList = new ArrayList<>();
	private Date _markDate;

	private FefuAttSheetWrapper _currentRow;
	private FefuAttSheetWrapper _editedRow;
	private Integer _editedRowIndex;
	boolean _rowAdded;
	boolean useCurrentRating;

	private ISelectModel _termModel;
	private ISelectModel _markModel;
	private ISelectModel _caModel = new LazySimpleSelectModel<>(EppFControlActionType.class);

	private Boolean _checkInstitutionDefault = false;

	@Override
	public void onComponentRender()
	{
		if (isAddForm())
			_uiSupport.setPageTitle("Добавление аттестационного листа");
		else
			_uiSupport.setPageTitle("Редактирование аттестационного листа");

		if (getDocumentId() == null && isCheckInstitutionDefault())
			getDocument().setEduInstitutionTitle(DVFU);
	}

	@Override
	public void onComponentRefresh()
	{
		ICommonDAO dao = DataAccessServices.dao();

		setOrgUnit(getOrgUnitId() == null ? null : dao.<OrgUnit>getNotNull(getOrgUnitId()));

		setEditedRow(null);
		setEditedRowIndex(null);
		setRowAdded(false);
		setEditedRowIndex(null);

		List<PpsEntry> ppsList = new ArrayList<>();

		if (isAddForm())
		{
			// форма добавления внешнего перезачтения
			if (getDocument() == null)
				setDocument(new SessionAttSheetDocument());
			getDocument().setFormingDate(new Date());

			if (getStudent() != null)
			{
				getDocument().setTargetStudent(getStudent());
				getDocument().setOrgUnit(getStudent().getEducationOrgUnit().getGroupOrgUnit());
			}
		}
		else
		{
			if (getDocument() == null)
				setDocument(dao.<SessionAttSheetDocument>getNotNull(getDocumentId()));
			// форма редактирования внешнего перезачтения
			setStudent(getDocument().getTargetStudent());
			List<SessionAttSheetOperation> operations = dao.getList(
					SessionAttSheetOperation.class,
					SessionAttSheetOperation.slot().document(), getDocument());

			List<FefuAttSheetWrapper> wrappers = new ArrayList<>();
			for (SessionAttSheetOperation operation : operations)
			{
				wrappers.add(new FefuAttSheetWrapper(operation));
				setMarkDate(operation.getPerformDate());
			}
			setRowList(wrappers);
			for (SessionComissionPps pps : FefuAttSheetManager.instance().dao().getCommissionPps(getDocument()))
			{
				ppsList.add(pps.getPps());
			}
		}

		setPpsData(new PpsEntrySelectBlockData(getOrgUnitId()).setMultiSelect(true).setInitiallySelectedPps(ppsList));

		if (getStudent() != null)
			setTermModel(SessionTermModel.createForStudent(getStudent()));

		useCurrentRating = ISessionBrsDao.instance.get().getSettings().isUseCurrentRatingForTransferDocument();
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (FefuAttSheetAddEdit.DS_EPP_SLOT.equals(dataSource.getName()))
		{
			dataSource.put(FefuAttSheetAddEdit.KEY_STUDENT, getStudent());
			if (null != getEditedRow() && null != getEditedRow().getTerm())
			{
				dataSource.put(FefuAttSheetAddEdit.KEY_EDU_YEAR, getEditedRow().getTerm().getYear());
				dataSource.put(FefuAttSheetAddEdit.KEY_YEAR_PART, getEditedRow().getTerm().getPart());
			}
			Collection<EppStudentWpeCAction> addedEpp = getAddedEpp();
			if (addedEpp != null && !addedEpp.isEmpty())
				dataSource.put(FefuAttSheetAddEdit.KEY_ADDED_EPP_ID, addedEpp);
		}
		else if (FefuAttSheetAddEdit.STUDENT_DS.equals(dataSource.getName()))
		{
			dataSource.put(StudentByGroupOrgUnitComboDSHandler.ORG_UNIT, getOrgUnit());
		}
	}

	// Listeners

	public void onSelectStudent()
	{
		// выбирать студента можно только на форме добавления и при не указанном студенте
		if (!isNeedSelectStudent())
			throw new IllegalStateException("Student changing is not allowed.");

		getDocument().setTargetStudent(getStudent());
		getDocument().setOrgUnit(getStudent().getEducationOrgUnit().getGroupOrgUnit());
		setTermModel(SessionTermModel.createForStudent(getStudent()));
		setRowList(new ArrayList<FefuAttSheetWrapper>());
		setEditedRow(null);
		setEditedRowIndex(null);
	}

	public void onClickApply()
	{
		if (getStudent().getEducationOrgUnit().getGroupOrgUnit() == null)
		{
			throw new ApplicationException("Нельзя добавить документ о перезачтении студенту, т.к. для его направления подготовки не задан ответственный деканат, который необходим для сохранения документа о перезачтении. Укажите деканат в настройке «Ответственные подразделения за направления подготовки (специальности)».");
		}

		FefuAttSheetManager.instance().dao().saveOrUpdateAttSheet(getDocument(), getMarkDate(), getRowList(), getPpsList());
		deactivate();
	}

	public void onClickAutoComplete()
	{
		String alias = "swca";
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, alias)
				.where(isNull(property(alias, EppStudentWpeCAction.P_REMOVAL_DATE)))
				.where(eq(property(alias, EppStudentWpeCAction.studentWpe().student()), value(getStudent())));
		Collection<EppStudentWpeCAction> addedEpp = getAllAddedEpp();
		if (addedEpp != null && !addedEpp.isEmpty())
		{
			dql.where(notIn(property(alias), addedEpp));
		}

		List<EppStudentWpeCAction> list = FefuAttSheetManager.instance().dao().getList(dql);
		for(EppStudentWpeCAction wpeCAction : list){
			FefuAttSheetWrapper attSheetWrapper = new FefuAttSheetWrapper();
			//required fields
			attSheetWrapper.setSourceDiscipline(wpeCAction.getStudentWpe().getRegistryElementPart().getRegistryElement().getTitle());
			attSheetWrapper.setSourceControlAction(wpeCAction.getActionType());
			//not required fields
			attSheetWrapper.setWorkTimeDiscDouble(wpeCAction.getStudentWpe().getRegistryElementPart().getLaborAsDouble());
			attSheetWrapper.setTerm(new SessionTermModel.TermWrapper(wpeCAction));
			attSheetWrapper.setEppSlot(wpeCAction);
			getRowList().add(attSheetWrapper);
		}
	}

	public void onClickAddRow()
	{
		if (getEditedRow() != null) return;
		final FefuAttSheetWrapper newRow = new FefuAttSheetWrapper();
		setEditedRow(newRow);
		getRowList().add(new FefuAttSheetWrapper());
		setEditedRowIndex(getRowList().size() - 1);
		setRowAdded(true);
	}

	public void onClickSaveRow()
	{
		if (getEditedRowIndex() != null && getRowList().size() > getEditedRowIndex() && getEditedRowIndex() >= 0)
			getRowList().set(getEditedRowIndex(), getEditedRow());
		setEditedRow(null);
		setRowAdded(false);
	}

	public void onClickCancelEdit()
	{
		if (isRowAdded() && !getRowList().isEmpty())
			getRowList().remove(getRowList().size() - 1);
		setEditedRowIndex(null);
		setEditedRow(null);
		setRowAdded(false);
	}

	public void onClickEditRow()
	{
		if (getEditedRow() != null) return;
		FefuAttSheetWrapper baseRow = findRow();
		setEditedRow(new FefuAttSheetWrapper(baseRow));
		setEditedRowIndex(getRowList().indexOf(baseRow));
		prepareTargetMarkModel();
		setRowAdded(false);
	}

	public void onClickDeleteRow()
	{
		getRowList().remove(findRow());
	}

	public void prepareTargetMarkModel()
	{
		setMarkModel(null == getEditedRow() ?
							 new LazySimpleSelectModel<>(ImmutableList.of()) :
							 FefuAttSheetManager.instance().dao().prepareMarkModel(getEditedRow().getEppSlot()));
	}

	// Utilities
	private Collection<EppStudentWpeCAction> getAddedEpp()
	{
		if (getRowList() == null || getRowList().isEmpty())
			return null;

		Collection<EppStudentWpeCAction> eppList = null;
		for (FefuAttSheetWrapper item : getRowList())
		{
			if (item.getEppSlot() != null)
			{
				if (item.getId() != getRowList().get(getEditedRowIndex()).getId())
				{
					if (eppList == null)
						eppList = new ArrayList<>();
					eppList.add(item.getEppSlot());
				}
			}
		}
		return eppList;
	}

	private Collection<EppStudentWpeCAction> getAllAddedEpp()
	{
		if (getRowList() == null || getRowList().isEmpty())
			return null;

		Collection<EppStudentWpeCAction> eppList = null;
		for (FefuAttSheetWrapper item : getRowList())
		{
			if (item.getEppSlot() != null)
			{
				if (eppList == null)
					eppList = new ArrayList<>();
				eppList.add(item.getEppSlot());
			}
		}
		return eppList;
	}

	public boolean isAddForm()
	{
		return getDocumentId() == null;
	}

	public boolean isNeedSelectStudent()
	{
		// если на форме добавления не указан студент, то его нужно выбрать
		return getDocumentId() == null;
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isHasStudent()
	{
		return getStudent() != null;
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isEditMode()
	{
		return getEditedRow() != null;
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isCurrentRowInEditMode()
	{
		return getEditedRow() != null && getEditedRowIndex().equals(getRowList().indexOf(getCurrentRow()));
	}

	public FefuAttSheetWrapper findRow()
	{
		Integer id = getListenerParameter();
		for (FefuAttSheetWrapper fefuAttSheetWrapper : getRowList())
		{
			if (fefuAttSheetWrapper.getId() == id)
				return fefuAttSheetWrapper;
		}
		return null;
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isRatingDisabled()
	{
		return !(getEditedRow() != null && getEditedRow().getMark() != null);
	}

	//Getters and setters
	public List<BaseValidator> getRatingValidators()
	{
		final List<BaseValidator> validators = new ArrayList<>(3);
		if (ISessionBrsDao.instance.get().getSettings().isCurrentRatingRequiredForTransferDocument())
			validators.add(new Required());
		validators.add(new Min("min=0"));
		validators.add(new Max("max=100"));
		return validators;
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isUseCurrentRating()
	{
		return useCurrentRating;
	}

	public Long getDocumentId()
	{
		return _documentId;
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setDocumentId(Long documentId)
	{
		_documentId = documentId;
	}

	public Long getOrgUnitId()
	{
		return _orgUnitId;
	}

	public void setOrgUnitId(Long orgUnitId)
	{
		_orgUnitId = orgUnitId;
	}

	public Student getStudent()
	{
		return _student;
	}

	public void setStudent(Student student)
	{
		_student = student;
	}

	public SessionAttSheetDocument getDocument()
	{
		return _document;
	}

	public void setDocument(SessionAttSheetDocument document)
	{
		_document = document;
	}

	public OrgUnit getOrgUnit()
	{
		return _orgUnit;
	}

	public void setOrgUnit(OrgUnit orgUnit)
	{
		_orgUnit = orgUnit;
	}

	public List<FefuAttSheetWrapper> getRowList()
	{
		return _rowList;
	}

	public void setRowList(List<FefuAttSheetWrapper> rowList)
	{
		_rowList = rowList;
	}

	public Date getMarkDate()
	{
		return _markDate;
	}

	public void setMarkDate(Date markDate)
	{
		_markDate = markDate;
	}

	public FefuAttSheetWrapper getCurrentRow()
	{
		return _currentRow;
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setCurrentRow(FefuAttSheetWrapper currentRow)
	{
		_currentRow = currentRow;
	}

	public FefuAttSheetWrapper getEditedRow()
	{
		return _editedRow;
	}

	public void setEditedRow(FefuAttSheetWrapper editedRow)
	{
		_editedRow = editedRow;
	}

	@SuppressWarnings("UnusedDeclaration")
	public ISelectModel getTermModel()
	{
		return _termModel;
	}

	public void setTermModel(ISelectModel termModel)
	{
		_termModel = termModel;
	}

	@SuppressWarnings("UnusedDeclaration")
	public ISelectModel getMarkModel()
	{
		return _markModel;
	}

	public void setMarkModel(ISelectModel markModel)
	{
		_markModel = markModel;
	}

	@SuppressWarnings("UnusedDeclaration")
	public ISelectModel getCaModel()
	{
		return _caModel;
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setCaModel(ISelectModel caModel)
	{
		_caModel = caModel;
	}

	public Integer getEditedRowIndex()
	{
		return _editedRowIndex;
	}

	public void setEditedRowIndex(Integer editedRowIndex)
	{
		this._editedRowIndex = editedRowIndex;
	}

	public boolean isRowAdded()
	{
		return _rowAdded;
	}

	public void setRowAdded(boolean rowAdded)
	{
		this._rowAdded = rowAdded;
	}

	public Boolean isCheckInstitutionDefault()
	{
		return _checkInstitutionDefault;
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setCheckInstitutionDefault(Boolean checkInstitutionDefault)
	{
		getSettings().set("eduInstitutionTitleCheck", checkInstitutionDefault);
		this._checkInstitutionDefault = checkInstitutionDefault;
	}

	public PpsEntrySelectBlockData getPpsData()
	{
		return ppsData;
	}

	public void setPpsData(PpsEntrySelectBlockData ppsData)
	{
		this.ppsData = ppsData;
	}

	public List<PpsEntry> getPpsList()
	{
		return getPpsData().getSelectedPpsList();
	}

	public void setPpsList(final List<PpsEntry> ppsList)
	{
		getPpsData().setSelectedPps(ppsList);
	}
}
