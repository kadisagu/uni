package ru.tandemservice.unifefu.events.dset;

import org.hibernate.Session;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unifefu.dao.daemon.FEFUMdbViewDaemonDAO;
import ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Created with IntelliJ IDEA.
 * User: Savva
 * Date: 27.02.14
 * Time: 10:38
 * To change this template use File | Settings | File Templates.
 */
public class FEFUMdbViewDaemonListener extends ParamTransactionCompleteListener<Boolean> {

    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppEduPlanVersion.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppEduPlanVersion.class, this);
    }

    @Override
    public Boolean beforeCompletion(Session session, Collection<Long> params) {

        new DQLDeleteBuilder(MdbViewEppEduPlanVersion.class)
                .where(in(property(MdbViewEppEduPlanVersion.eppEduPlanVersion().id()), params)).createStatement(session).execute();

        for (Long id : params)
                {
                    IEntityMeta meta = EntityRuntime.getMeta(id);
                    if (EppEduPlanVersion.ENTITY_NAME.equals(meta.getName()))
                    {
                        FEFUMdbViewDaemonDAO.DAEMON.registerAfterCompleteWakeUp(session);
                    }
                }
        return super.beforeCompletion(session, params);
    }

    @Override
    public void afterCompletion(Session session, int status, Collection<Long> params, Boolean beforeCompletionResult) {


}
}
