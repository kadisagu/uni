/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubModel;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;

/**
 * @author Andrey Andreev
 * @since 13.01.2016
 */
public class Model extends AbstractListParagraphPubModel<FefuAdmittedToGIAListExtract>
{

    private StudentCustomStateCI _studentCustomStateCI;

    public StudentCustomStateCI getStudentCustomStateCI()
    {
        return _studentCustomStateCI;
    }

    public void setStudentCustomStateCI(StudentCustomStateCI studentCustomStateCI)
    {
        _studentCustomStateCI = studentCustomStateCI;
    }
}
