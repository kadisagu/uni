package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x2_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPrikazyOther

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewprikazyother_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("extract_id", DBType.LONG).setNullable(false), 
				new DBColumn("order_id", DBType.LONG).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("ordernumber_p", DBType.createVarchar(255)), 
				new DBColumn("ordertype_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("modularreason_p", DBType.createVarchar(255)), 
				new DBColumn("listreason_p", DBType.createVarchar(255)), 
				new DBColumn("listbasics_p", DBType.createVarchar(2048)), 
				new DBColumn("createdate_p", DBType.TIMESTAMP), 
				new DBColumn("orderdate_p", DBType.DATE), 
				new DBColumn("lastname_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("firstname_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("middlename_p", DBType.createVarchar(255)), 
				new DBColumn("personbenefits_p", DBType.createVarchar(255)), 
				new DBColumn("description_p", DBType.createVarchar(2048))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewPrikazyOther");

		}


    }
}