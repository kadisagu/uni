package ru.tandemservice.unifefu.base.vo;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

/**
 * User: amakarova
 * Date: 20.09.13
 */
public class FefuEntrantOriginalsVerificationVar2ReportVO extends FefuEntrantOriginalsVerificationReportVO {

    private FefuTechnicalCommission _technicCommission;
    private ISelectModel _technicCommissionModel;

    public FefuTechnicalCommission getTechnicCommission()
    {
        return _technicCommission;
    }

    public void setTechnicCommission(FefuTechnicalCommission technicCommission)
    {
        _technicCommission = technicCommission;
    }
}