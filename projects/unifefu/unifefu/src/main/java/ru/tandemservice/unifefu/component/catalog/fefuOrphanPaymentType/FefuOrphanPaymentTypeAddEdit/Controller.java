/* $Id$ */
package ru.tandemservice.unifefu.component.catalog.fefuOrphanPaymentType.FefuOrphanPaymentTypeAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditController;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;

/**
 * @author nvankov
 * @since 11/18/13
 */
public class Controller extends DefaultCatalogAddEditController<FefuOrphanPaymentType, Model, IDAO>
{
}
