/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu10.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuEduEnrolmentToSecondAndNextCourseStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 17.04.2013
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuEduEnrolmentToSecondAndNextCourseStuExtract, Model>
{
}