/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.ChangeGuid;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.NSISync.NSISyncManager;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;

import java.util.List;
import java.util.UUID;

/**
 * @author Dmitry Seleznev
 * @since 08.06.2014
 */
@State({
        @Bind(key = NSISyncChangeGuidUI.ENTITY_ID, binding = NSISyncChangeGuidUI.ENTITY_ID)
})
public class NSISyncChangeGuidUI extends UIPresenter
{
    public static final String ENTITY_ID = "entityId";

    private Long _entityId;
    private FefuNsiIds _nsiId;
    private IEntity _entity;
    private String _guid;
    private boolean _newNsiId = false;

    @Override
    public void onComponentRefresh()
    {
        _entity = DataAccessServices.dao().getNotNull(_entityId);
        List<FefuNsiIds> nsiIdsList = DataAccessServices.dao().getList(FefuNsiIds.class, FefuNsiIds.entityId(), _entityId);
        if (!nsiIdsList.isEmpty())
        {
            Long syncDate = null;
            for (FefuNsiIds ids : nsiIdsList)
            {
                if (null == _nsiId)
                {
                    _nsiId = ids;
                    if (null != ids.getSyncTime()) syncDate = ids.getSyncTime().getTime();
                }

                if ((null == syncDate && null != ids.getSyncTime())
                        || (null != syncDate && null != ids.getSyncTime() && syncDate < ids.getSyncTime().getTime()))
                {
                    _nsiId = ids;
                    syncDate = ids.getSyncTime().getTime();
                }
            }
        }

        if (null == _nsiId)
        {
            _nsiId = new FefuNsiIds();
            _newNsiId = true;
        }
    }

    public void onClickGenerateGuid()
    {
        _guid = UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString();
    }

    public void onClickApply()
    {
        NSISyncManager.instance().dao().doChangeGuid(getEntityId(), getNsiId().getId(), getGuid(), _newNsiId);
        deactivate();
    }

    public Long getEntityId()
    {
        return _entityId;
    }

    public void setEntityId(Long entityId)
    {
        _entityId = entityId;
    }

    public FefuNsiIds getNsiId()
    {
        return _nsiId;
    }

    public void setNsiId(FefuNsiIds nsiId)
    {
        _nsiId = nsiId;
    }

    public IEntity getEntity()
    {
        return _entity;
    }

    public void setEntity(IEntity entity)
    {
        _entity = entity;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }
}
