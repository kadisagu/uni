package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.uni.dao.UniBaseDao;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class ApeProgramForStudentDao extends UniBaseDao implements IApeProgramForStudentDao
{
    private static final String ALIAS = "programForStudent";

    @Override
    public FefuAdditionalProfessionalEducationProgramForStudent getApeProgramForStudentByStudentId(Long studentId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuAdditionalProfessionalEducationProgramForStudent.class, ALIAS)
                .column(ALIAS);
        builder.where(eq(
                property(FefuAdditionalProfessionalEducationProgramForStudent.student().id().fromAlias(ALIAS)),
                value(studentId)));
        return builder.createStatement(getSession()).uniqueResult();

    }
}
