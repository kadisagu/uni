/* $Id: $ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Igor Belanov
 * @since 01.07.2016
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x10x4_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.4"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.4")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuConditionalCourseTransferListExtract

        // создана новая сущность
        if (!tool.tableExists("ffcndtnlcrstrnsfrlstextrct_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("ffcndtnlcrstrnsfrlstextrct_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_fb47e805"),
                    new DBColumn("course_id", DBType.LONG).setNullable(false),
                    new DBColumn("group_id", DBType.LONG).setNullable(false),
                    new DBColumn("compensationtype_id", DBType.LONG),
                    new DBColumn("coursenew_id", DBType.LONG).setNullable(false),
                    new DBColumn("groupnew_id", DBType.LONG).setNullable(false),
                    new DBColumn("eliminatedate_p", DBType.DATE)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuConditionalCourseTransferListExtract");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuPerformConditionCourseTransferListExtract

        // создана новая сущность
        {
            // у сущности нет своей таблицы - ничего делать не надо
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuPerformConditionCourseTransferListExtract");

        }


    }
}
