/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import java.io.OutputStream;
import java.util.Map;
import java.util.Set;

/**
 * @author Dmitry Seleznev
 * @since 23.09.2013
 */
public interface INSISyncDAO extends INeedPersistenceSupport, ICommonDAO
{
    void updateNsiCatalogAutosync(Long id);

    void updateNsiCatalogRealTimeSync(Long id);

    boolean isAnyIdInLoginSyncStack();

    void doFullFillHumanLoginSyncStack();

    void doGetFullOrgStructureFromNsi() throws Exception;

    // TODO: Do not forget to kill this method (after the first sync at FEFU)
    void prepareOrgstructDifferences(final OutputStream stream) throws Exception;

    void doCreateOrganizationsMappingReport() throws Exception;

    void getNsiCatalogLogRowPackFile(Long logRowId);

    Map<String, Long> getGuidToRealObjectIdMap(Set<String> guidsSet);

    boolean isOrgStructureInProcess();

    byte[] getOrgStructureFile();

    byte[] getOrgstructureDifferencesFile();

    void doDeleteOrgUnitsTree(Long orgUnitId);

    void doCorrectOrgstructSettings();

    Map<String, OrgUnit> getGuidToOrgUnitMap();

    void doChangeGuid(Long entityId, Long nsiId, String guid, boolean newGuid);
}