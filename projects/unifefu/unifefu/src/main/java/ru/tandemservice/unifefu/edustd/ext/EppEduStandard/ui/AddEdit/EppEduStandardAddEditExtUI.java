/* $Id$ */
package ru.tandemservice.unifefu.edustd.ext.EppEduStandard.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.ui.AddEdit.EppEduStandardAddEditUI;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt;
import ru.tandemservice.unifefu.entity.catalog.FefuEduStandartGeneration;
import ru.tandemservice.unifefu.entity.gen.FefuEppStateEduStandardExtGen;

import java.util.Collections;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 28.05.2015
 */
public class EppEduStandardAddEditExtUI extends UIAddon
{

    private FefuEppStateEduStandardExt _standardExt = new FefuEppStateEduStandardExt();
    private EppEduStandardAddEditUI _presenter;
    private List<FefuEduStandartGeneration> _generationList = Collections.emptyList();


    public EppEduStandardAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
        _presenter = (EppEduStandardAddEditUI)presenter;
        setGenerationList(DataAccessServices.dao().getList(FefuEduStandartGeneration.class));
    }

    public FefuEppStateEduStandardExt getStandardExt()
    {
        return _standardExt;
    }

    public void setStandardExt(FefuEppStateEduStandardExt standardExt)
    {
        _standardExt = standardExt;
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        FefuEppStateEduStandardExt standardExt = DataAccessServices.dao().getByNaturalId(new FefuEppStateEduStandardExtGen.NaturalId(((EppEduStandardAddEditUI) getPresenter()).getStateEduStandard()));
        if (standardExt != null)
        {
            setStandardExt(standardExt);
        }
        else
        {
            _standardExt.setEduStandard(((EppEduStandardAddEditUI) getPresenter()).getStateEduStandard());
        }
        if (_presenter.getGeneration()==null)
        {
            _presenter.setGeneration(EppGeneration.GENERATION_3);
        }
    }

    public List<FefuEduStandartGeneration> getGenerationList()
    {
        return _generationList;
    }

    public void setGenerationList(List<FefuEduStandartGeneration> generationList)
    {
        _generationList = generationList;
    }
}