/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d4.Add;

import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.form.validator.Validator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 17.09.2013
 */
public class Model extends ru.tandemservice.uni.component.documents.d4.Add.Model
{
    public List<Validator> getFefuValidatorYear()
    {
        List<Validator> list = new ArrayList<>();
        list.add(new Min("min=1990"));
        list.add(new Max("max=" + getCurrentYear()));
        return list;
    }
}
