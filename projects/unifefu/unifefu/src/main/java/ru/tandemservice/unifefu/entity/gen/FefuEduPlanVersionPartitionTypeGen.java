package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionPartitionType;
import ru.tandemservice.unifefu.entity.catalog.FefuSchedulePartitionType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип разбиения для УПв
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEduPlanVersionPartitionTypeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEduPlanVersionPartitionType";
    public static final String ENTITY_NAME = "fefuEduPlanVersionPartitionType";
    public static final int VERSION_HASH = 1763591908;
    private static IEntityMeta ENTITY_META;

    public static final String L_VERSION = "version";
    public static final String L_PARTITION_TYPE = "partitionType";

    private EppEduPlanVersion _version;     // УПв
    private FefuSchedulePartitionType _partitionType;     // Тип разбиения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return УПв. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEduPlanVersion getVersion()
    {
        return _version;
    }

    /**
     * @param version УПв. Свойство не может быть null и должно быть уникальным.
     */
    public void setVersion(EppEduPlanVersion version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Тип разбиения. Свойство не может быть null.
     */
    @NotNull
    public FefuSchedulePartitionType getPartitionType()
    {
        return _partitionType;
    }

    /**
     * @param partitionType Тип разбиения. Свойство не может быть null.
     */
    public void setPartitionType(FefuSchedulePartitionType partitionType)
    {
        dirty(_partitionType, partitionType);
        _partitionType = partitionType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEduPlanVersionPartitionTypeGen)
        {
            setVersion(((FefuEduPlanVersionPartitionType)another).getVersion());
            setPartitionType(((FefuEduPlanVersionPartitionType)another).getPartitionType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEduPlanVersionPartitionTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEduPlanVersionPartitionType.class;
        }

        public T newInstance()
        {
            return (T) new FefuEduPlanVersionPartitionType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "partitionType":
                    return obj.getPartitionType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((EppEduPlanVersion) value);
                    return;
                case "partitionType":
                    obj.setPartitionType((FefuSchedulePartitionType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "partitionType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "partitionType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return EppEduPlanVersion.class;
                case "partitionType":
                    return FefuSchedulePartitionType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEduPlanVersionPartitionType> _dslPath = new Path<FefuEduPlanVersionPartitionType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEduPlanVersionPartitionType");
    }
            

    /**
     * @return УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionPartitionType#getVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Тип разбиения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionPartitionType#getPartitionType()
     */
    public static FefuSchedulePartitionType.Path<FefuSchedulePartitionType> partitionType()
    {
        return _dslPath.partitionType();
    }

    public static class Path<E extends FefuEduPlanVersionPartitionType> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _version;
        private FefuSchedulePartitionType.Path<FefuSchedulePartitionType> _partitionType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionPartitionType#getVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> version()
        {
            if(_version == null )
                _version = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_VERSION, this);
            return _version;
        }

    /**
     * @return Тип разбиения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionPartitionType#getPartitionType()
     */
        public FefuSchedulePartitionType.Path<FefuSchedulePartitionType> partitionType()
        {
            if(_partitionType == null )
                _partitionType = new FefuSchedulePartitionType.Path<FefuSchedulePartitionType>(L_PARTITION_TYPE, this);
            return _partitionType;
        }

        public Class getEntityClass()
        {
            return FefuEduPlanVersionPartitionType.class;
        }

        public String getEntityName()
        {
            return "fefuEduPlanVersionPartitionType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
