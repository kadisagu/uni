/*$Id$*/
package ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad.logic;

import org.tandemframework.hibsupport.dao.ICommonDAO;

import java.io.InputStream;

/**
 * @author DMITRY KNYAZEV
 * @since 15.01.2015
 */
public interface IFefuFileLoadDao extends ICommonDAO
{
	/**
	 * Восстанавливает блок УП из XML
	 * @param stream         Содержимое файла XML
	 * @param baseBlockId    Id блока УП для восстановления записей
	 */
	public void saveEduBlockFromXML(InputStream stream, Long baseBlockId);

	/**
	 * Создает XML из блока УП
	 * @param blockId Id блока УП для экспорта записей
	 * @return строка формата XML
	 */
	public String createXMLFromEduBlock(Long blockId);
}
