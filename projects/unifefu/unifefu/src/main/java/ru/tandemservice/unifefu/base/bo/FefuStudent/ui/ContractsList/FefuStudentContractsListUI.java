/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.ui.ContractsList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuStudent.ui.ContractsEdit.FefuStudentContractsEdit;
import ru.tandemservice.unifefu.entity.FefuStudentContract;

/**
 * @author Alexey Lopatin
 * @since 20.12.2013
 */
@Input({@Bind(key = "studentId", binding = "studentId", required = true)})
public class FefuStudentContractsListUI extends UIPresenter
{
    private Long _studentId;
    private Student _student;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _student = DataAccessServices.dao().getNotNull(Student.class, _studentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuStudentContractsList.STUDENT_CONTRACTS_DS.equals(dataSource.getName()))
        {
            dataSource.put("studentId", _studentId);
            dataSource.put("personId", _student.getPerson().getId());
        }
    }

    // Listeners

    public void onChangeChecked()
    {
        FefuStudentContract contract = getContractDS().getRecordById(getListenerParameterAsLong());
        if(contract.isTerminated())
            contract.setTerminated(false);
        else
            contract.setTerminated(true);

        DataAccessServices.dao().update(contract);
    }

    public void onClickEditContract()
    {
        _uiActivation.asRegionDialog(FefuStudentContractsEdit.class).parameter("contractId", getListenerParameterAsLong()).activate();
    }

    public void onClickDeleteContract()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    // Getters & Setters

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public BaseSearchListDataSource getContractDS()
    {
        return _uiConfig.getDataSource(FefuStudentContractsList.STUDENT_CONTRACTS_DS);
    }
}
