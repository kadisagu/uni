/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.*;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.TravelPaymentOrderAdd.logic.FefuTravelPaymentOrderReportDAO;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.TravelPaymentOrderAdd.logic.IFefuTravelPaymentOrderReportDAO;

/**
 * @author Nikolay Fedorovskih
 * @since 10.07.2013
 */
@Configuration
public class FefuEcReportManager extends BusinessObjectManager
{
    public static FefuEcReportManager instance()
    {
        return instance(FefuEcReportManager.class);
    }

    @Bean
    public IFefuEcReportDAO dao()
    {
        return new FefuEcReportDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler fefuEntrantOriginalsVerificationReportVOListDSHandler()
    {
        return new FefuEntrantOriginalsVerificationReportListDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler fefuEntrantOriginalsVerificationVar2ReportVOListDSHandler()
    {
        return new FefuEntrantOriginalsVerificationVar2ReportListDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler fefuEntrantTAListsReportListDSHandler()
    {
        return new FefuEntrantTAListsReportListDSHandler(getName());
    }

    public static final Long YES = 0L;
    public static final Long NO = 1L;

    @Bean
    public ItemListExtPoint<DataWrapper> yesNoExtPoint()
    {
        return itemList(DataWrapper.class).
                add(YES.toString(), new DataWrapper(YES, "ui.yes")).
                add(NO.toString(), new DataWrapper(NO, "ui.no")).
                create();
    }

    @Bean
    public IDefaultComboDataSourceHandler yesNoComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(yesNoExtPoint());
    }

    @Bean
    public IDefaultComboDataSourceHandler enrollmentCampaignStageComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(enrollmentCampaignStageExtPoint());
    }

    public static final Long ENROLLMENT_CAMP_STAGE_DOCUMENTS = 0L;
    public static final Long ENROLLMENT_CAMP_STAGE_EXAMS = 1L;
    public static final Long ENROLLMENT_CAMP_STAGE_ENROLLMENT = 2L;

    @Bean
    public ItemListExtPoint<DataWrapper> enrollmentCampaignStageExtPoint()
    {
        return itemList(DataWrapper.class).
                add(ENROLLMENT_CAMP_STAGE_DOCUMENTS.toString(), new DataWrapper(ENROLLMENT_CAMP_STAGE_DOCUMENTS, "ui.enrollmentCampaignStage.documents")).
                add(ENROLLMENT_CAMP_STAGE_EXAMS.toString(), new DataWrapper(ENROLLMENT_CAMP_STAGE_EXAMS, "ui.enrollmentCampaignStage.exams")).
                add(ENROLLMENT_CAMP_STAGE_ENROLLMENT.toString(), new DataWrapper(ENROLLMENT_CAMP_STAGE_ENROLLMENT, "ui.enrollmentCampaignStage.enrollment")).
                create();
    }

    @Bean
    public IFefuTravelPaymentOrderReportDAO fefuTravelPaymentOrderReportDAO()
    {
        return new FefuTravelPaymentOrderReportDAO();
    }
}