package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuStuffCompensationStuListExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("ffstffcmpnstnstlstextrct_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("protocoldate_p", DBType.DATE).setNullable(false), 
				new DBColumn("protocolnumber_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("compensationsum_p", DBType.LONG).setNullable(false), 
				new DBColumn("immediatesum_p", DBType.LONG).setNullable(false), 
				new DBColumn("matchingpersonfio_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("responsiblepersonfio_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("matchingperson_id", DBType.LONG).setNullable(false), 
				new DBColumn("responsibleperson_id", DBType.LONG).setNullable(false), 
				new DBColumn("matchingdate_p", DBType.DATE).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuStuffCompensationStuListExtract");

		}


    }
}