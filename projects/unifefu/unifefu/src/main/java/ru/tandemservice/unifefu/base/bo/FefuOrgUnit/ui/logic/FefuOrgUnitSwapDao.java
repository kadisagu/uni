/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 28.03.2014
 */
public class FefuOrgUnitSwapDao extends CommonDAO implements IFefuOrgUnitSwapDao
{
    public void doSwapOrgUnits(Long orgUnitId, Long orgUnitToSwapId)
    {
        OrgUnit orgUnit = get(OrgUnit.class, orgUnitId);
        FefuNsiIds orgUnitNsiId = IFefuNsiSyncDAO.instance.get().getNsiId(orgUnitId);
        FefuNsiIds orgUnitToSwapNsiId = IFefuNsiSyncDAO.instance.get().getNsiId(orgUnitToSwapId);

        if (orgUnitId.equals(orgUnitToSwapId))
            throw new ApplicationException("Нельзя перепривязать guid подразделения к тому же подразделению");
        if (null == orgUnitToSwapNsiId) throw new ApplicationException("У выбранного подразделения не указан GUID");
        if (orgUnitToSwapNsiId.getGuid().contains("_"))
            throw new ApplicationException("У выбранного подразделения указан невалидный GUID");

        List<EmployeePost> employeePostList = new DQLSelectBuilder().fromEntity(EmployeePost.class, "e").column("e")
                .where(eq(property(EmployeePost.orgUnit().id().fromAlias("e")), value(orgUnitToSwapId)))
                .createStatement(getSession()).list();

        if (null == orgUnitNsiId)
        {
            orgUnitToSwapNsiId.setEntityId(orgUnit.getId());
            update(orgUnitToSwapNsiId);
        } else
        {
            FefuNsiIds newGuid = new FefuNsiIds();
            newGuid.update(orgUnitNsiId);
            newGuid.setGuid(orgUnitToSwapNsiId.getGuid());

            FefuNsiIds newToSwapGuid = new FefuNsiIds();
            newToSwapGuid.update(orgUnitToSwapNsiId);
            newToSwapGuid.setGuid(orgUnitToSwapNsiId.getGuid() + "_");

            delete(orgUnitNsiId);
            delete(orgUnitToSwapNsiId);
            getSession().flush();

            save(newGuid);
            save(newToSwapGuid);
        }

        for (EmployeePost post : employeePostList)
        {
            post.setOrgUnit(orgUnit);
            update(post);
        }
    }
}