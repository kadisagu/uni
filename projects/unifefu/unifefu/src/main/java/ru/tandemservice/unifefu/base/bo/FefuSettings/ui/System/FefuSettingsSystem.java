/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.System;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Dmitry Seleznev
 * @since 26.09.2014
 */
@Configuration
public class FefuSettingsSystem extends BusinessComponentManager
{
    public static final String YES_NO_DS = "yesNoDS";
    public static final long YES_ID = 0L;
    public static final long NO_ID = 1L;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(YES_NO_DS, yesNoComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler yesNoComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(YES_ID, "да")
                .addRecord(NO_ID, "нет");
    }
}