/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu25.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuConditionalTransferCourseStuExtract, IDAO, Model>
{
}
