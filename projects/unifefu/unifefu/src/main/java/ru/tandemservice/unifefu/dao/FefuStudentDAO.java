/* $Id$ */
package ru.tandemservice.unifefu.dao;

import ru.tandemservice.uni.dao.StudentDAO;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Nikolay Fedorovskih
 * @since 13.01.2014
 */
public class FefuStudentDAO extends StudentDAO
{
    @Override
    public String getFullTitleExtended(Student student)
    {
        return new StringBuilder("Студент: ")
                .append(student.getPerson().getFullFio())
                .append(" (").append(student.getStatus().getTitle()).append(", ")
                .append(student.getEducationOrgUnit().getDevelopForm().getTitle()).append(", ") // вид обучения
                .append(student.getCompensationType().getShortTitle()).append(")")              // вид возмещения затрат
                .append(" | Группа: ").append(((null != student.getGroup()) ? student.getGroup().getTitle() : "---"))
                .append(" | ").append(student.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle())
                .append(" (").append(student.getEducationOrgUnit().getTerritorialOrgUnit().getShortTitle()).append(") ")
                .append(" | ").append(student.getEducationOrgUnit().getEducationLevelHighSchool().getFullTitleExtended())
                .toString();
    }
}