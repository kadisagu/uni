/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPassDisciplineDates.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 14.06.2013
 */
public interface IFefuPassDisciplineDatesDAO extends INeedPersistenceSupport, ICommonDAO
{
    void savePassDates(List<DataWrapper> rows, SubjectPassForm subjectPassForm, DevelopForm developForm, FefuTechnicalCommission technicalCommission);
}