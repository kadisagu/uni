/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.SendPortalNotifications;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic.FefuEntrantsDSHandler;
import ru.tandemservice.unifefu.dao.daemon.IFEFUPersonGuidDaemonDao;
import ru.tandemservice.unifefu.dao.daemon.IFEFUPortalNotificationsDaemonDao;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuPortalNotification;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 13.06.2013
 */
public class FefuSystemActionSendPortalNotificationsUI extends UIPresenter
{
    private EnrollmentCampaign _enrollmentCampaign;
    private DataWrapper _notificationType;
    private Entrant _entrant;
    private String _guid;

    private Person _person;
    private FefuNsiIds _personGuid;

    private String _subject;
    private String _link;
    private String _text;

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public DataWrapper getNotificationType()
    {
        return _notificationType;
    }

    public void setNotificationType(DataWrapper notificationType)
    {
        _notificationType = notificationType;
    }

    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        _entrant = entrant;
    }

    public Person getPerson()
    {
        return _person;
    }

    public void setPerson(Person person)
    {
        _person = person;
    }

    public FefuNsiIds getPersonGuid()
    {
        return _personGuid;
    }

    public void setPersonGuid(FefuNsiIds personGuid)
    {
        _personGuid = personGuid;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public String getSubject()
    {
        return _subject;
    }

    public void setSubject(String subject)
    {
        _subject = subject;
    }

    public String getLink()
    {
        return _link;
    }

    public void setLink(String link)
    {
        _link = link;
    }

    public String getText()
    {
        return _text;
    }

    public void setText(String text)
    {
        _text = text;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuSystemActionSendPortalNotifications.NOTIFICATION_TYPE_DS.equals(dataSource.getName()))
        {
            String[] preItemList = StringUtils.split(_uiConfig.getProperty("ui.notificationTypeDS.itemslist"), ";");
            String[] codesList = StringUtils.split(_uiConfig.getProperty("ui.notificationTypeDS.itemcodes"), ";");
            List<DataWrapper> recordList = new ArrayList<>(preItemList.length);
            for (int i = 0; i < preItemList.length; i++)
                recordList.add(new DataWrapper(Long.parseLong(codesList[i]), preItemList[i], preItemList[i]));
            dataSource.put(UIDefines.COMBO_OBJECT_LIST, recordList);
        } else if (FefuSystemActionSendPortalNotifications.ENTRANTS_DS.equals(dataSource.getName()))
        {
            dataSource.put(FefuEntrantsDSHandler.ENROLLMENT_CAMPAIGN, _enrollmentCampaign);
        }
    }

    public void onChangeNotificationType()
    {
        if (null != getNotificationType())
        {
            setSubject(_uiConfig.getPropertySafe("ui.notification.subject" + getNotificationType().getId(), null));
            setLink(_uiConfig.getPropertySafe("ui.notification.link" + getNotificationType().getId(), null));
            setText(_uiConfig.getPropertySafe("ui.notification.text" + getNotificationType().getId(), null));
        } else
        {
            setSubject(null);
            setLink(null);
            setText(null);
        }
    }

    public void onChangeEntrant()
    {
        if (null != _entrant)
        {
            _person = _entrant.getPerson();
            _personGuid = IFefuNsiSyncDAO.instance.get().getNsiId(_entrant.getPerson().getId());
            _guid = null != _personGuid ? _personGuid.getGuid() : null;
        }
        else
        {
            _person = null;
            _personGuid = null;
            _guid = null;
        }
    }

    public void onClickApply()
    {
        if (null == getNotificationType()) return;
        List<FefuPortalNotification> notifications = new ArrayList<>();

        if (null != _guid)
        {
            if (null == _personGuid)
            {
                _personGuid = DataAccessServices.dao().get(FefuNsiIds.class, FefuNsiIds.guid(), _guid);
                if (null == _personGuid)
                    _personGuid = IFEFUPersonGuidDaemonDao.instance.get().doRegisterFictiveGuid(_guid);
                _person = DataAccessServices.dao().get(Person.class, Person.id(), _personGuid.getEntityId());
            }

            if (null == _personGuid) return;

            FefuPortalNotification notif = new FefuPortalNotification();
            notif.setSubject(getSubject());
            notif.setLink(getLink());
            notif.setPersonGuid(_personGuid);
            notif.setPerson(_person);
            notif.setText(getText());
            notifications.add(notif);

            System.out.println(notif.getPersonGuid().getGuid() + " - " + (null != notif.getPerson() ? notif.getPerson().getFullFio() : "Fictive Person"));
        } else
        {

            if (4L == getNotificationType().getId())
            {
                for (CoreCollectionUtils.Pair<FefuNsiIds, EcgEntrantRecommended> recEntrant : IFEFUPortalNotificationsDaemonDao.instance.get().getEntrantsRecommended(getEnrollmentCampaign()))
                {
                    Entrant entrant = recEntrant.getY().getDirection().getEntrantRequest().getEntrant();

                    FefuPortalNotification notif = new FefuPortalNotification();
                    notif.setSubject(getSubject());
                    notif.setLink(getLink());
                    notif.setPersonGuid(recEntrant.getX());
                    notif.setPerson(entrant.getPerson());
                    notif.setText(getText());
                    notifications.add(notif);

                    System.out.println(notif.getPersonGuid().getGuid() + " - " + notif.getPerson().getFullFio());
                }
            } else if (5L == getNotificationType().getId() || 6L == getNotificationType().getId() || 7L == getNotificationType().getId() || 8L == getNotificationType().getId())
            {
                for (CoreCollectionUtils.Pair<FefuNsiIds, Entrant> entrant : IFEFUPortalNotificationsDaemonDao.instance.get().getEntrantsFromOrders(getEnrollmentCampaign()))
                {
                    FefuPortalNotification notif = new FefuPortalNotification();
                    notif.setSubject(getSubject());
                    notif.setLink(getLink());
                    notif.setPersonGuid(entrant.getX());
                    notif.setPerson(entrant.getY().getPerson());
                    notif.setText(getText());
                    notifications.add(notif);

                    System.out.println(notif.getPersonGuid().getGuid() + " - " + notif.getPerson().getFullFio());
                }
            } else if (10 == getNotificationType().getId())
            {
                for (CoreCollectionUtils.Pair<FefuNsiIds, Entrant> entrant : IFEFUPortalNotificationsDaemonDao.instance.get().getEntrantsList(getEnrollmentCampaign()))
                {
                    FefuPortalNotification notif = new FefuPortalNotification();
                    notif.setSubject(getSubject());
                    notif.setLink(getLink());
                    notif.setPersonGuid(entrant.getX());
                    notif.setPerson(entrant.getY().getPerson());
                    notif.setText(getText());
                    notifications.add(notif);

                    System.out.println(notif.getPersonGuid().getGuid() + " - " + notif.getPerson().getFullFio());
                }
            } else if (11 == getNotificationType().getId())
            {
                for (CoreCollectionUtils.Pair<FefuNsiIds, PreliminaryEnrollmentStudent> stud : IFEFUPortalNotificationsDaemonDao.instance.get().getPreliminaryEnrollmentStudents(getEnrollmentCampaign()))
                {
                    Entrant entrant = stud.getY().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();
                    String text = getText();

                    String eduOrgUnit = stud.getY().getEducationOrgUnit().getTitleWithFormAndCondition();
                    text = text.replaceAll("\\@\\{educationOrgUnitWithFUTS\\}", eduOrgUnit);

                    FefuPortalNotification notif = new FefuPortalNotification();
                    notif.setSubject(getSubject());
                    notif.setLink(getLink());
                    notif.setPersonGuid(stud.getX());
                    notif.setPerson(entrant.getPerson());
                    notif.setText(getText());
                    notifications.add(notif);

                    System.out.println(notif.getPersonGuid().getGuid() + " - " + notif.getPerson().getFullFio() + " - " + eduOrgUnit);
                    System.out.println(text);
                }
            } else
            {
                for (CoreCollectionUtils.Pair<FefuNsiIds, Entrant> entrant : IFEFUPortalNotificationsDaemonDao.instance.get().getEntrantsList(getEnrollmentCampaign()))
                {
                    FefuPortalNotification notif = new FefuPortalNotification();
                    notif.setSubject(getSubject());
                    notif.setLink(getLink());
                    notif.setPersonGuid(entrant.getX());
                    notif.setPerson(entrant.getY().getPerson());
                    notif.setText(getText());
                    notifications.add(notif);

                    System.out.println(notif.getPersonGuid().getGuid() + " - " + notif.getPerson().getFullFio());
                }
            }
        }


        IFEFUPortalNotificationsDaemonDao.instance.get().doRegisterPortalNotifications(notifications);
        //IFEFUPortalNotificationsDaemonDao.instance.get().doSendPortalNotifications();
    }
}