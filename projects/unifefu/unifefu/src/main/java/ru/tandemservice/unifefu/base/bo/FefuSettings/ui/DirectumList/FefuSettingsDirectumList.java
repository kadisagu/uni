/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.DirectumList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 10.07.2013
 */
@Configuration
public class FefuSettingsDirectumList extends BusinessComponentManager
{
    public static final String SETTIGS_DIRECTUM_DS = "settingsSearchListDS";
    public static final String DATA_WRAPPERT_LIST = "dataWrappersList";
    public static final String SETTIGS_PARAM = "settingsParam";
    public static final String EDIT_DISABLED = "editDisabled";
    public static final String CLEAR_DISABLED = "clearDisabled";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SETTIGS_DIRECTUM_DS, getSettingsDirectumDS(), settingsDirectumDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getSettingsDirectumDS()
    {
        IMergeRowIdResolver merger = entity -> {
            if (null != ((TreeableDirectumSettingDataWrapper) entity).getExtractType())
                return ((TreeableDirectumSettingDataWrapper) entity).getExtractType().getId().toString();
            return entity.getId().toString();
        };

        IStyleResolver styleResolver = rowEntity -> null == ((TreeableDirectumSettingDataWrapper) rowEntity).getHierarhyParent() ? "font-weight:bold;" : null;

        return columnListExtPointBuilder(SETTIGS_DIRECTUM_DS)
                .addColumn(textColumn("title", TreeableDirectumSettingDataWrapper.EXTRACT_TYPE_PROPERTY + "." + StudentExtractType.title()).treeable(true).merger(merger).styleResolver(styleResolver))
                .addColumn(textColumn("description", TreeableDirectumSettingDataWrapper.EXTRACT_TYPE_DESCRIPTION_PROPERTY).merger(merger))
                .addColumn(textColumn("orderType", TreeableDirectumSettingDataWrapper.ORDER_TYPE_PROPERTY + "." + FefuDirectumOrderType.shortTitle()))
                .addColumn(textColumn("routeCode", TreeableDirectumSettingDataWrapper.ORDER_TYPE_PROPERTY + "." + FefuDirectumOrderType.routeCode()))
                .addColumn(textColumn("documentType", TreeableDirectumSettingDataWrapper.ORDER_TYPE_PROPERTY + "." + FefuDirectumOrderType.documentType()))
                .addColumn(booleanColumn("dismiss", SETTIGS_PARAM + "." + FefuDirectumSettings.P_DISMISS_FAKE).verticalHeader("true"))
                .addColumn(booleanColumn("restore", SETTIGS_PARAM + "." + FefuDirectumSettings.P_RESTORE_FAKE).verticalHeader("true"))
                .addColumn(booleanColumn("grantMatAid", SETTIGS_PARAM + "." + FefuDirectumSettings.P_GRANT_MAT_AID_FAKE).verticalHeader("true"))
                .addColumn(booleanColumn("social", SETTIGS_PARAM + "." + FefuDirectumSettings.P_SOCIAL_FAKE).verticalHeader("true"))
                .addColumn(booleanColumn("practice", SETTIGS_PARAM + "." + FefuDirectumSettings.P_PRACTICE_FAKE).verticalHeader("true"))
                .addColumn(booleanColumn("groupManager", SETTIGS_PARAM + "." + FefuDirectumSettings.P_GROUP_MANAGER_FAKE).verticalHeader("true"))
                .addColumn(booleanColumn("penalty", SETTIGS_PARAM + "." + FefuDirectumSettings.P_PENALTY_FAKE).verticalHeader("true"))
                .addColumn(booleanColumn("magister", SETTIGS_PARAM + "." + FefuDirectumSettings.P_MAGISTER_FAKE).verticalHeader("true"))
                .addColumn(booleanColumn("payments", SETTIGS_PARAM + "." + FefuDirectumSettings.P_PAYMENTS_FAKE).verticalHeader("true"))
                .addColumn(booleanColumn("stateFormular", SETTIGS_PARAM + "." + FefuDirectumSettings.P_STATE_FORMULAR_FAKE).verticalHeader("true"))
                .addColumn(booleanColumn("international", SETTIGS_PARAM + "." + FefuDirectumSettings.P_INTERNATIONAL_FAKE).verticalHeader("true"))
                .addColumn(booleanColumn("checkMedicalDocs", SETTIGS_PARAM + "." + FefuDirectumSettings.P_CHECK_MEDICAL_DOCS).verticalHeader("true"))
                .addColumn(textColumn("signer", SETTIGS_PARAM + "." + FefuDirectumSettings.signer().s()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled(EDIT_DISABLED))
                .addColumn(actionColumn("clear", new Icon("cancel", "Сбросить"), "onClickResetSettings", alert("settingsSearchListDS.clear.alert", StudentExtractType.title())).disabled(CLEAR_DISABLED))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> settingsDirectumDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<DataWrapper> recordList = context.get(DATA_WRAPPERT_LIST);
                return ListOutputBuilder.get(input, recordList).build();
            }
        };
    }
}