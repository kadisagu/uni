/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.PostType;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 01.07.2014
 */
public class PostTypeReactor extends SimpleCatalogEntityNewReactor<PostType, PostBoundedWithQGandQL>
{
    public static final String MANAGEMENT_STAFF_TITLE = "Руководители";
    public static final String WORK_STAFF_TITLE = "Рабочие";
    public static final String SERVICE_STAFF_TITLE = "Специалисты";
    public static final String SUPPORT_STAFF_TITLE = "Другие служащие";
    public static final String EDU_STAFF_TITLE = "Научные и научно-педагогические работники";

    private Map<String, Post> _postTitleLowerToPostMap = new HashMap<>();
    private QualificationLevel _defaultQualificationLevel;
    private ProfQualificationGroup _defaultQualificationGroup;
    private EmployeeType _employeeTypeManagementStaff;
    private EmployeeType _employeeTypeWorkStaff;
    private EmployeeType _employeeTypeServiceStaff;
    private EmployeeType _employeeTypeSupportStaff;
    private EmployeeType _employeeTypeEduStaff;

    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return false;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<PostBoundedWithQGandQL> getEntityClass()
    {
        return PostBoundedWithQGandQL.class;
    }

    @Override
    public Class<PostType> getNSIEntityClass()
    {
        return PostType.class;
    }

    @Override
    public PostType getCatalogElementRetrieveRequestObject(String guid)
    {
        PostType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createPostType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    protected void prepareRelatedObjectsMap(Set<Long> entityIds)
    {
        super.prepareRelatedObjectsMap(entityIds);
        List<Post> postList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(Post.class, "p").column("p"));
        for (Post post : postList)
        {
            Set<String> usedPostCodes = _usedCodes.get(Post.class);
            if(null == usedPostCodes) usedPostCodes = new HashSet<>();
            usedPostCodes.add(post.getUserCode());
            _usedCodes.put(Post.class, usedPostCodes);
            _postTitleLowerToPostMap.put(post.getTitle().toLowerCase(), post);
        }

        List<QualificationLevel> qualificationLevels = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(QualificationLevel.class, "l").column("l"));
        List<ProfQualificationGroup> profQualificationGroups = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(ProfQualificationGroup.class, "g").column("g"));
        _defaultQualificationLevel = !qualificationLevels.isEmpty() ? qualificationLevels.get(0) : null;
        _defaultQualificationGroup = !profQualificationGroups.isEmpty() ? profQualificationGroups.get(0) : null;

        _employeeTypeManagementStaff = IUniBaseDao.instance.get().getCatalogItem(EmployeeType.class, EmployeeTypeCodes.MANAGEMENT_STAFF);
        _employeeTypeWorkStaff = IUniBaseDao.instance.get().getCatalogItem(EmployeeType.class, EmployeeTypeCodes.WORK_STAFF);
        _employeeTypeServiceStaff = IUniBaseDao.instance.get().getCatalogItem(EmployeeType.class, EmployeeTypeCodes.SERVICE_STAFF);
        _employeeTypeSupportStaff = IUniBaseDao.instance.get().getCatalogItem(EmployeeType.class, EmployeeTypeCodes.SUPPORT_STAFF);
        _employeeTypeEduStaff = IUniBaseDao.instance.get().getCatalogItem(EmployeeType.class, EmployeeTypeCodes.EDU_STAFF);
    }

    @Override
    public PostType createEntity(CoreCollectionUtils.Pair<PostBoundedWithQGandQL, FefuNsiIds> entityPair)
    {
        PostType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createPostType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setPostID(entityPair.getX().getUserCode());
        nsiEntity.setPostName(entityPair.getX().getTitle());
        if (null != entityPair.getX().getPost() && null != entityPair.getX().getPost().getEmployeeType())
        {
            EmployeeType empType = entityPair.getX().getPost().getEmployeeType();
            if (EmployeeTypeCodes.MANAGEMENT_STAFF.equals(empType.getCode()))
            {
                nsiEntity.setPostAUP("1");
                nsiEntity.setPostStatisticalServiceCategory(MANAGEMENT_STAFF_TITLE);
            } else if (EmployeeTypeCodes.WORK_STAFF.equals(empType.getCode()))
                nsiEntity.setPostStatisticalServiceCategory(WORK_STAFF_TITLE);
            else if (EmployeeTypeCodes.SERVICE_STAFF.equals(empType.getCode()))
                nsiEntity.setPostStatisticalServiceCategory(SERVICE_STAFF_TITLE);
            else if (EmployeeTypeCodes.SUPPORT_STAFF.equals(empType.getCode()))
                nsiEntity.setPostStatisticalServiceCategory(SUPPORT_STAFF_TITLE);
            else if (EmployeeTypeCodes.EDU_STAFF.equals(empType.getCode()))
                nsiEntity.setPostStatisticalServiceCategory(EDU_STAFF_TITLE);
        }
        return nsiEntity;
    }

    @Override
    public PostBoundedWithQGandQL createEntity(PostType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getTitle()) return null;

        PostBoundedWithQGandQL entity = new PostBoundedWithQGandQL();
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(PostBoundedWithQGandQL.class));
        entity.setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getPostID()));
        entity.setTitle(nsiEntity.getPostName());
        entity.setEnabled(Boolean.TRUE);

        entity.setProfQualificationGroup(_defaultQualificationGroup);
        entity.setQualificationLevel(_defaultQualificationLevel);

        boolean aup = (null != nsiEntity.getPostAUP() && "1".equals(nsiEntity.getPostAUP())) ? Boolean.TRUE : Boolean.FALSE;
        Post post = _postTitleLowerToPostMap.get(nsiEntity.getPostName().toLowerCase());

        if (null == post)
        {
            post = new Post();
            post.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(Post.class));
            post.setUserCode(getUserCodeForOBChecked(Post.class, nsiEntity.getPostID()));
            post.setTitle(nsiEntity.getPostName());

            if (aup) post.setEmployeeType(_employeeTypeManagementStaff);
            if (null != nsiEntity.getPostStatisticalServiceCategory())
            {
                if (MANAGEMENT_STAFF_TITLE.equals(nsiEntity.getPostStatisticalServiceCategory()))
                    post.setEmployeeType(_employeeTypeManagementStaff);
                else if (WORK_STAFF_TITLE.equals(nsiEntity.getPostStatisticalServiceCategory()))
                    post.setEmployeeType(_employeeTypeWorkStaff);
                else if (SERVICE_STAFF_TITLE.equals(nsiEntity.getPostStatisticalServiceCategory()))
                    post.setEmployeeType(_employeeTypeServiceStaff);
                else if (SUPPORT_STAFF_TITLE.equals(nsiEntity.getPostStatisticalServiceCategory()))
                    post.setEmployeeType(_employeeTypeSupportStaff);
                else if (EDU_STAFF_TITLE.equals(nsiEntity.getPostStatisticalServiceCategory()))
                    post.setEmployeeType(_employeeTypeEduStaff);
            }
            if (null == post.getEmployeeType()) post.setEmployeeType(_employeeTypeWorkStaff);
            _postTitleLowerToPostMap.put(post.getTitle().toLowerCase(), post);
            _otherEntityToSaveOrUpdateSet.add(new CoreCollectionUtils.Pair<IEntity, FefuNsiIds>(post, null));
        }

        entity.setPost(post);
        return entity;
    }

    @Override
    public PostBoundedWithQGandQL updatePossibleDuplicateFields(PostType nsiEntity, CoreCollectionUtils.Pair<PostBoundedWithQGandQL, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), PostBoundedWithQGandQL.title().s()))
            {
                entityPair.getX().setTitle(nsiEntity.getPostName());
                entityPair.getX().getPost().setTitle(nsiEntity.getPostName());
                _otherEntityToSaveOrUpdateSet.add(new CoreCollectionUtils.Pair<IEntity, FefuNsiIds>(entityPair.getX().getPost(), null));
            }
        }
        return entityPair.getX();
    }

    @Override
    public PostType updateNsiEntityFields(PostType nsiEntity, PostBoundedWithQGandQL entity)
    {
        nsiEntity.setPostName(entity.getTitle());
        nsiEntity.setID(getUserCodeForNSIChecked(entity.getUserCode()));
        return nsiEntity;
    }
}