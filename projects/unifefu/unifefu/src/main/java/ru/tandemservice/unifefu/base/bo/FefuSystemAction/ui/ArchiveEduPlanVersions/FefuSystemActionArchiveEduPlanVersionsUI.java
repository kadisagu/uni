package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.ArchiveEduPlanVersions;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;

/**
 * @author ilunin
 * @since 13.10.2014
 */
public class FefuSystemActionArchiveEduPlanVersionsUI extends UIPresenter
{
    private Integer _eduEndYear;
    private EduProgramDuration _eduProgramDuration;
    private ISelectModel  _developPeriodList;

    public Integer getEduEndYear()
    {
        return _eduEndYear;
    }

    public void setEduEndYear(Integer eduEndYear)
    {
        this._eduEndYear = eduEndYear;
    }

    public EduProgramDuration getEduProgramDuration()
    {
        return this._eduProgramDuration;
    }

    public void setEduProgramDuration(EduProgramDuration eduProgramDuration)
    {
        this._eduProgramDuration = eduProgramDuration;
    }

    public ISelectModel getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(ISelectModel developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public void onClickApply()
    {
        FefuSystemActionManager.instance().dao().doArchiveEduPlanVersions(_eduEndYear, _eduProgramDuration);
        deactivate();
    }

}
