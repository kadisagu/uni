/* $Id$ */
package ru.tandemservice.unifefu.utils.fefuICal;

import java.util.List;

/**
 * @author nvankov
 * @since 9/16/13
 */
public class FefuScheduleICalXST
{
    private List<FefuScheduleVEventICalXST> _events;
    private List<String> _studentsNSIIds;

    public List<FefuScheduleVEventICalXST> getEvents()
    {
        return _events;
    }

    public void setEvents(List<FefuScheduleVEventICalXST> events)
    {
        _events = events;
    }

    public List<String> getStudentsNSIIds()
    {
        return _studentsNSIIds;
    }

    public void setStudentsNSIIds(List<String> studentsNSIIds)
    {
        _studentsNSIIds = studentsNSIIds;
    }
}
