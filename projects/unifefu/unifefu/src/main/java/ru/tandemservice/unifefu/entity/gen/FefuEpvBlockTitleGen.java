package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.FefuEpvBlockTitle;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Титул блока УПв
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEpvBlockTitleGen extends EntityBase
 implements INaturalIdentifiable<FefuEpvBlockTitleGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEpvBlockTitle";
    public static final String ENTITY_NAME = "fefuEpvBlockTitle";
    public static final int VERSION_HASH = -1193419178;
    private static IEntityMeta ENTITY_META;

    public static final String L_BLOCK = "block";
    public static final String P_PLAN_FULL_TITLE = "planFullTitle";
    public static final String P_PLAN_TITLE = "planTitle";
    public static final String P_USER_NUMBER = "userNumber";
    public static final String P_ACADEMY = "academy";
    public static final String P_ORG_UNIT = "orgUnit";
    public static final String P_HIGHER_ECHELON = "higherEchelon";
    public static final String P_PRODUCING_ORG_UNIT = "producingOrgUnit";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_EDUCATION_DIRECTION_CODE = "educationDirectionCode";
    public static final String P_START_YEAR = "startYear";
    public static final String P_INCLUDE_EXAMS_IN_HOURS_AMOUNT = "includeExamsInHoursAmount";
    public static final String P_DISSERTATION_AS_ATTESTATION = "dissertationAsAttestation";
    public static final String P_STATE_EXAM_AS_ATTESTATION = "stateExamAsAttestation";
    public static final String P_KSR_OR_INDIVIDUAL_LESSONS = "ksrOrIndividualLessons";
    public static final String P_ZET_IN_WEEK_ATT = "zetInWeekAtt";
    public static final String P_HOURS_AMOUNT_IN_Z_E_T_ATT = "hoursAmountInZETAtt";
    public static final String P_PROGRAM_LABOUR_UNITS = "programLabourUnits";
    public static final String P_INTERACTIVE_LESSONS_PERCENT = "interactiveLessonsPercent";
    public static final String P_LECTURES_PERCENT = "lecturesPercent";
    public static final String P_CHOICE_DISCIPLINES_PERCENT = "choiceDisciplinesPercent";
    public static final String P_MAX_SIZE = "maxSize";
    public static final String P_PLAN_KIND = "planKind";
    public static final String P_LEVEL_CODE = "levelCode";
    public static final String P_LEVEL = "level";
    public static final String P_TERMS_IN_COURSE = "termsInCourse";
    public static final String P_ELEMENTS_IN_WEEK = "elementsInWeek";
    public static final String P_STATE_EXAM_DATE = "stateExamDate";
    public static final String P_STATE_EXAM_DOC = "stateExamDoc";
    public static final String P_STATE_EXAM_TYPE = "stateExamType";
    public static final String P_APPLICATION = "application";
    public static final String P_APPLICATION_DATE = "applicationDate";
    public static final String P_APPLICATION_VERSION = "applicationVersion";
    public static final String P_ZET_IN_YEAR = "zetInYear";
    public static final String P_ZET_IN_WEEK = "zetInWeek";
    public static final String P_HOURS_AMOUNT_IN_Z_E_T = "hoursAmountInZET";
    public static final String P_TOTAL_Z_E_T = "totalZET";
    public static final String P_CERTIFICATE_DATE = "certificateDate";

    private EppEduPlanVersionBlock _block;     // Блок УПв
    private String _planFullTitle;     // Полное имя плана
    private String _planTitle;     // Имя плана
    private String _userNumber;     // Номер пользователя
    private String _academy;     // Образовательное учреждение
    private String _orgUnit;     // Структурное подразделение
    private String _higherEchelon;     // Вышестоящая организация
    private String _producingOrgUnit;     // Выпускающее подразделение
    private String _formativeOrgUnit;     // Формирующее подразделение
    private String _educationDirectionCode;     // Код направления подготовки (специальности)
    private Integer _startYear;     // Год начала подготовки
    private Boolean _includeExamsInHoursAmount;     // Включать экзамены в сумму часов
    private Boolean _dissertationAsAttestation;     // Относить Диссертации к разделу ИГА
    private Boolean _stateExamAsAttestation;     // Относить Гос. экзамены и защиту ВКР к разделу ИГА
    private String _ksrOrIndividualLessons;     // КСР-ИЗ
    private Double _zetInWeekAtt;     // ЗЕТ в неделе (ИГА)
    private Double _hoursAmountInZETAtt;     // Часов в ЗЕТ (ИГА)
    private Double _programLabourUnits;     // Единиц трудоемкости в основной образовательной программе
    private Double _interactiveLessonsPercent;     // Доля занятий в интерактивной форме, %
    private Double _lecturesPercent;     // Доля лекций, %
    private Double _choiceDisciplinesPercent;     // Доля дисциплин по выбору от вариативной части, %
    private Double _maxSize;     // Максимальная нагрузка
    private String _planKind;     // Вид плана
    private String _levelCode;     // Код уровня
    private String _level;     // Уровень
    private Integer _termsInCourse;     // Семестров на курсе
    private Integer _elementsInWeek;     // Элементов в неделе
    private Date _stateExamDate;     // Дата утверждения ГОСа
    private String _stateExamDoc;     // Номер ГОСа
    private String _stateExamType;     // Поколение ГОСа
    private String _application;     // Приложение ИМЦА
    private Date _applicationDate;     // Дата приложения ИМЦА
    private String _applicationVersion;     // Версия приложения ИМЦА
    private Double _zetInYear;     // ЗЕТ на год
    private Double _zetInWeek;     // ЗЕТ в неделе
    private Double _hoursAmountInZET;     // Часов в ЗЕТ
    private Double _totalZET;     // ЗЕТ на все
    private Date _certificateDate;     // Дата сертификата ИМЦА

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок УПв. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    /**
     * @param block Блок УПв. Свойство не может быть null и должно быть уникальным.
     */
    public void setBlock(EppEduPlanVersionBlock block)
    {
        dirty(_block, block);
        _block = block;
    }

    /**
     * @return Полное имя плана.
     */
    @Length(max=255)
    public String getPlanFullTitle()
    {
        return _planFullTitle;
    }

    /**
     * @param planFullTitle Полное имя плана.
     */
    public void setPlanFullTitle(String planFullTitle)
    {
        dirty(_planFullTitle, planFullTitle);
        _planFullTitle = planFullTitle;
    }

    /**
     * @return Имя плана.
     */
    @Length(max=255)
    public String getPlanTitle()
    {
        return _planTitle;
    }

    /**
     * @param planTitle Имя плана.
     */
    public void setPlanTitle(String planTitle)
    {
        dirty(_planTitle, planTitle);
        _planTitle = planTitle;
    }

    /**
     * @return Номер пользователя.
     */
    @Length(max=255)
    public String getUserNumber()
    {
        return _userNumber;
    }

    /**
     * @param userNumber Номер пользователя.
     */
    public void setUserNumber(String userNumber)
    {
        dirty(_userNumber, userNumber);
        _userNumber = userNumber;
    }

    /**
     * @return Образовательное учреждение.
     */
    @Length(max=255)
    public String getAcademy()
    {
        return _academy;
    }

    /**
     * @param academy Образовательное учреждение.
     */
    public void setAcademy(String academy)
    {
        dirty(_academy, academy);
        _academy = academy;
    }

    /**
     * @return Структурное подразделение.
     */
    @Length(max=255)
    public String getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Структурное подразделение.
     */
    public void setOrgUnit(String orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Вышестоящая организация.
     */
    @Length(max=255)
    public String getHigherEchelon()
    {
        return _higherEchelon;
    }

    /**
     * @param higherEchelon Вышестоящая организация.
     */
    public void setHigherEchelon(String higherEchelon)
    {
        dirty(_higherEchelon, higherEchelon);
        _higherEchelon = higherEchelon;
    }

    /**
     * @return Выпускающее подразделение.
     */
    @Length(max=255)
    public String getProducingOrgUnit()
    {
        return _producingOrgUnit;
    }

    /**
     * @param producingOrgUnit Выпускающее подразделение.
     */
    public void setProducingOrgUnit(String producingOrgUnit)
    {
        dirty(_producingOrgUnit, producingOrgUnit);
        _producingOrgUnit = producingOrgUnit;
    }

    /**
     * @return Формирующее подразделение.
     */
    @Length(max=255)
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение.
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Код направления подготовки (специальности).
     */
    @Length(max=255)
    public String getEducationDirectionCode()
    {
        return _educationDirectionCode;
    }

    /**
     * @param educationDirectionCode Код направления подготовки (специальности).
     */
    public void setEducationDirectionCode(String educationDirectionCode)
    {
        dirty(_educationDirectionCode, educationDirectionCode);
        _educationDirectionCode = educationDirectionCode;
    }

    /**
     * @return Год начала подготовки.
     */
    public Integer getStartYear()
    {
        return _startYear;
    }

    /**
     * @param startYear Год начала подготовки.
     */
    public void setStartYear(Integer startYear)
    {
        dirty(_startYear, startYear);
        _startYear = startYear;
    }

    /**
     * @return Включать экзамены в сумму часов.
     */
    public Boolean getIncludeExamsInHoursAmount()
    {
        return _includeExamsInHoursAmount;
    }

    /**
     * @param includeExamsInHoursAmount Включать экзамены в сумму часов.
     */
    public void setIncludeExamsInHoursAmount(Boolean includeExamsInHoursAmount)
    {
        dirty(_includeExamsInHoursAmount, includeExamsInHoursAmount);
        _includeExamsInHoursAmount = includeExamsInHoursAmount;
    }

    /**
     * @return Относить Диссертации к разделу ИГА.
     */
    public Boolean getDissertationAsAttestation()
    {
        return _dissertationAsAttestation;
    }

    /**
     * @param dissertationAsAttestation Относить Диссертации к разделу ИГА.
     */
    public void setDissertationAsAttestation(Boolean dissertationAsAttestation)
    {
        dirty(_dissertationAsAttestation, dissertationAsAttestation);
        _dissertationAsAttestation = dissertationAsAttestation;
    }

    /**
     * @return Относить Гос. экзамены и защиту ВКР к разделу ИГА.
     */
    public Boolean getStateExamAsAttestation()
    {
        return _stateExamAsAttestation;
    }

    /**
     * @param stateExamAsAttestation Относить Гос. экзамены и защиту ВКР к разделу ИГА.
     */
    public void setStateExamAsAttestation(Boolean stateExamAsAttestation)
    {
        dirty(_stateExamAsAttestation, stateExamAsAttestation);
        _stateExamAsAttestation = stateExamAsAttestation;
    }

    /**
     * @return КСР-ИЗ.
     */
    @Length(max=255)
    public String getKsrOrIndividualLessons()
    {
        return _ksrOrIndividualLessons;
    }

    /**
     * @param ksrOrIndividualLessons КСР-ИЗ.
     */
    public void setKsrOrIndividualLessons(String ksrOrIndividualLessons)
    {
        dirty(_ksrOrIndividualLessons, ksrOrIndividualLessons);
        _ksrOrIndividualLessons = ksrOrIndividualLessons;
    }

    /**
     * @return ЗЕТ в неделе (ИГА).
     */
    public Double getZetInWeekAtt()
    {
        return _zetInWeekAtt;
    }

    /**
     * @param zetInWeekAtt ЗЕТ в неделе (ИГА).
     */
    public void setZetInWeekAtt(Double zetInWeekAtt)
    {
        dirty(_zetInWeekAtt, zetInWeekAtt);
        _zetInWeekAtt = zetInWeekAtt;
    }

    /**
     * @return Часов в ЗЕТ (ИГА).
     */
    public Double getHoursAmountInZETAtt()
    {
        return _hoursAmountInZETAtt;
    }

    /**
     * @param hoursAmountInZETAtt Часов в ЗЕТ (ИГА).
     */
    public void setHoursAmountInZETAtt(Double hoursAmountInZETAtt)
    {
        dirty(_hoursAmountInZETAtt, hoursAmountInZETAtt);
        _hoursAmountInZETAtt = hoursAmountInZETAtt;
    }

    /**
     * @return Единиц трудоемкости в основной образовательной программе.
     */
    public Double getProgramLabourUnits()
    {
        return _programLabourUnits;
    }

    /**
     * @param programLabourUnits Единиц трудоемкости в основной образовательной программе.
     */
    public void setProgramLabourUnits(Double programLabourUnits)
    {
        dirty(_programLabourUnits, programLabourUnits);
        _programLabourUnits = programLabourUnits;
    }

    /**
     * @return Доля занятий в интерактивной форме, %.
     */
    public Double getInteractiveLessonsPercent()
    {
        return _interactiveLessonsPercent;
    }

    /**
     * @param interactiveLessonsPercent Доля занятий в интерактивной форме, %.
     */
    public void setInteractiveLessonsPercent(Double interactiveLessonsPercent)
    {
        dirty(_interactiveLessonsPercent, interactiveLessonsPercent);
        _interactiveLessonsPercent = interactiveLessonsPercent;
    }

    /**
     * @return Доля лекций, %.
     */
    public Double getLecturesPercent()
    {
        return _lecturesPercent;
    }

    /**
     * @param lecturesPercent Доля лекций, %.
     */
    public void setLecturesPercent(Double lecturesPercent)
    {
        dirty(_lecturesPercent, lecturesPercent);
        _lecturesPercent = lecturesPercent;
    }

    /**
     * @return Доля дисциплин по выбору от вариативной части, %.
     */
    public Double getChoiceDisciplinesPercent()
    {
        return _choiceDisciplinesPercent;
    }

    /**
     * @param choiceDisciplinesPercent Доля дисциплин по выбору от вариативной части, %.
     */
    public void setChoiceDisciplinesPercent(Double choiceDisciplinesPercent)
    {
        dirty(_choiceDisciplinesPercent, choiceDisciplinesPercent);
        _choiceDisciplinesPercent = choiceDisciplinesPercent;
    }

    /**
     * @return Максимальная нагрузка.
     */
    public Double getMaxSize()
    {
        return _maxSize;
    }

    /**
     * @param maxSize Максимальная нагрузка.
     */
    public void setMaxSize(Double maxSize)
    {
        dirty(_maxSize, maxSize);
        _maxSize = maxSize;
    }

    /**
     * @return Вид плана. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPlanKind()
    {
        return _planKind;
    }

    /**
     * @param planKind Вид плана. Свойство не может быть null.
     */
    public void setPlanKind(String planKind)
    {
        dirty(_planKind, planKind);
        _planKind = planKind;
    }

    /**
     * @return Код уровня.
     */
    @Length(max=255)
    public String getLevelCode()
    {
        return _levelCode;
    }

    /**
     * @param levelCode Код уровня.
     */
    public void setLevelCode(String levelCode)
    {
        dirty(_levelCode, levelCode);
        _levelCode = levelCode;
    }

    /**
     * @return Уровень.
     */
    @Length(max=255)
    public String getLevel()
    {
        return _level;
    }

    /**
     * @param level Уровень.
     */
    public void setLevel(String level)
    {
        dirty(_level, level);
        _level = level;
    }

    /**
     * @return Семестров на курсе.
     */
    public Integer getTermsInCourse()
    {
        return _termsInCourse;
    }

    /**
     * @param termsInCourse Семестров на курсе.
     */
    public void setTermsInCourse(Integer termsInCourse)
    {
        dirty(_termsInCourse, termsInCourse);
        _termsInCourse = termsInCourse;
    }

    /**
     * @return Элементов в неделе.
     */
    public Integer getElementsInWeek()
    {
        return _elementsInWeek;
    }

    /**
     * @param elementsInWeek Элементов в неделе.
     */
    public void setElementsInWeek(Integer elementsInWeek)
    {
        dirty(_elementsInWeek, elementsInWeek);
        _elementsInWeek = elementsInWeek;
    }

    /**
     * @return Дата утверждения ГОСа.
     */
    public Date getStateExamDate()
    {
        return _stateExamDate;
    }

    /**
     * @param stateExamDate Дата утверждения ГОСа.
     */
    public void setStateExamDate(Date stateExamDate)
    {
        dirty(_stateExamDate, stateExamDate);
        _stateExamDate = stateExamDate;
    }

    /**
     * @return Номер ГОСа.
     */
    @Length(max=255)
    public String getStateExamDoc()
    {
        return _stateExamDoc;
    }

    /**
     * @param stateExamDoc Номер ГОСа.
     */
    public void setStateExamDoc(String stateExamDoc)
    {
        dirty(_stateExamDoc, stateExamDoc);
        _stateExamDoc = stateExamDoc;
    }

    /**
     * @return Поколение ГОСа.
     */
    @Length(max=255)
    public String getStateExamType()
    {
        return _stateExamType;
    }

    /**
     * @param stateExamType Поколение ГОСа.
     */
    public void setStateExamType(String stateExamType)
    {
        dirty(_stateExamType, stateExamType);
        _stateExamType = stateExamType;
    }

    /**
     * @return Приложение ИМЦА.
     */
    @Length(max=255)
    public String getApplication()
    {
        return _application;
    }

    /**
     * @param application Приложение ИМЦА.
     */
    public void setApplication(String application)
    {
        dirty(_application, application);
        _application = application;
    }

    /**
     * @return Дата приложения ИМЦА.
     */
    public Date getApplicationDate()
    {
        return _applicationDate;
    }

    /**
     * @param applicationDate Дата приложения ИМЦА.
     */
    public void setApplicationDate(Date applicationDate)
    {
        dirty(_applicationDate, applicationDate);
        _applicationDate = applicationDate;
    }

    /**
     * @return Версия приложения ИМЦА.
     */
    @Length(max=255)
    public String getApplicationVersion()
    {
        return _applicationVersion;
    }

    /**
     * @param applicationVersion Версия приложения ИМЦА.
     */
    public void setApplicationVersion(String applicationVersion)
    {
        dirty(_applicationVersion, applicationVersion);
        _applicationVersion = applicationVersion;
    }

    /**
     * @return ЗЕТ на год.
     */
    public Double getZetInYear()
    {
        return _zetInYear;
    }

    /**
     * @param zetInYear ЗЕТ на год.
     */
    public void setZetInYear(Double zetInYear)
    {
        dirty(_zetInYear, zetInYear);
        _zetInYear = zetInYear;
    }

    /**
     * @return ЗЕТ в неделе.
     */
    public Double getZetInWeek()
    {
        return _zetInWeek;
    }

    /**
     * @param zetInWeek ЗЕТ в неделе.
     */
    public void setZetInWeek(Double zetInWeek)
    {
        dirty(_zetInWeek, zetInWeek);
        _zetInWeek = zetInWeek;
    }

    /**
     * @return Часов в ЗЕТ.
     */
    public Double getHoursAmountInZET()
    {
        return _hoursAmountInZET;
    }

    /**
     * @param hoursAmountInZET Часов в ЗЕТ.
     */
    public void setHoursAmountInZET(Double hoursAmountInZET)
    {
        dirty(_hoursAmountInZET, hoursAmountInZET);
        _hoursAmountInZET = hoursAmountInZET;
    }

    /**
     * @return ЗЕТ на все.
     */
    public Double getTotalZET()
    {
        return _totalZET;
    }

    /**
     * @param totalZET ЗЕТ на все.
     */
    public void setTotalZET(Double totalZET)
    {
        dirty(_totalZET, totalZET);
        _totalZET = totalZET;
    }

    /**
     * @return Дата сертификата ИМЦА.
     */
    public Date getCertificateDate()
    {
        return _certificateDate;
    }

    /**
     * @param certificateDate Дата сертификата ИМЦА.
     */
    public void setCertificateDate(Date certificateDate)
    {
        dirty(_certificateDate, certificateDate);
        _certificateDate = certificateDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEpvBlockTitleGen)
        {
            if (withNaturalIdProperties)
            {
                setBlock(((FefuEpvBlockTitle)another).getBlock());
            }
            setPlanFullTitle(((FefuEpvBlockTitle)another).getPlanFullTitle());
            setPlanTitle(((FefuEpvBlockTitle)another).getPlanTitle());
            setUserNumber(((FefuEpvBlockTitle)another).getUserNumber());
            setAcademy(((FefuEpvBlockTitle)another).getAcademy());
            setOrgUnit(((FefuEpvBlockTitle)another).getOrgUnit());
            setHigherEchelon(((FefuEpvBlockTitle)another).getHigherEchelon());
            setProducingOrgUnit(((FefuEpvBlockTitle)another).getProducingOrgUnit());
            setFormativeOrgUnit(((FefuEpvBlockTitle)another).getFormativeOrgUnit());
            setEducationDirectionCode(((FefuEpvBlockTitle)another).getEducationDirectionCode());
            setStartYear(((FefuEpvBlockTitle)another).getStartYear());
            setIncludeExamsInHoursAmount(((FefuEpvBlockTitle)another).getIncludeExamsInHoursAmount());
            setDissertationAsAttestation(((FefuEpvBlockTitle)another).getDissertationAsAttestation());
            setStateExamAsAttestation(((FefuEpvBlockTitle)another).getStateExamAsAttestation());
            setKsrOrIndividualLessons(((FefuEpvBlockTitle)another).getKsrOrIndividualLessons());
            setZetInWeekAtt(((FefuEpvBlockTitle)another).getZetInWeekAtt());
            setHoursAmountInZETAtt(((FefuEpvBlockTitle)another).getHoursAmountInZETAtt());
            setProgramLabourUnits(((FefuEpvBlockTitle)another).getProgramLabourUnits());
            setInteractiveLessonsPercent(((FefuEpvBlockTitle)another).getInteractiveLessonsPercent());
            setLecturesPercent(((FefuEpvBlockTitle)another).getLecturesPercent());
            setChoiceDisciplinesPercent(((FefuEpvBlockTitle)another).getChoiceDisciplinesPercent());
            setMaxSize(((FefuEpvBlockTitle)another).getMaxSize());
            setPlanKind(((FefuEpvBlockTitle)another).getPlanKind());
            setLevelCode(((FefuEpvBlockTitle)another).getLevelCode());
            setLevel(((FefuEpvBlockTitle)another).getLevel());
            setTermsInCourse(((FefuEpvBlockTitle)another).getTermsInCourse());
            setElementsInWeek(((FefuEpvBlockTitle)another).getElementsInWeek());
            setStateExamDate(((FefuEpvBlockTitle)another).getStateExamDate());
            setStateExamDoc(((FefuEpvBlockTitle)another).getStateExamDoc());
            setStateExamType(((FefuEpvBlockTitle)another).getStateExamType());
            setApplication(((FefuEpvBlockTitle)another).getApplication());
            setApplicationDate(((FefuEpvBlockTitle)another).getApplicationDate());
            setApplicationVersion(((FefuEpvBlockTitle)another).getApplicationVersion());
            setZetInYear(((FefuEpvBlockTitle)another).getZetInYear());
            setZetInWeek(((FefuEpvBlockTitle)another).getZetInWeek());
            setHoursAmountInZET(((FefuEpvBlockTitle)another).getHoursAmountInZET());
            setTotalZET(((FefuEpvBlockTitle)another).getTotalZET());
            setCertificateDate(((FefuEpvBlockTitle)another).getCertificateDate());
        }
    }

    public INaturalId<FefuEpvBlockTitleGen> getNaturalId()
    {
        return new NaturalId(getBlock());
    }

    public static class NaturalId extends NaturalIdBase<FefuEpvBlockTitleGen>
    {
        private static final String PROXY_NAME = "FefuEpvBlockTitleNaturalProxy";

        private Long _block;

        public NaturalId()
        {}

        public NaturalId(EppEduPlanVersionBlock block)
        {
            _block = ((IEntity) block).getId();
        }

        public Long getBlock()
        {
            return _block;
        }

        public void setBlock(Long block)
        {
            _block = block;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuEpvBlockTitleGen.NaturalId) ) return false;

            FefuEpvBlockTitleGen.NaturalId that = (NaturalId) o;

            if( !equals(getBlock(), that.getBlock()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBlock());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBlock());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEpvBlockTitleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEpvBlockTitle.class;
        }

        public T newInstance()
        {
            return (T) new FefuEpvBlockTitle();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "block":
                    return obj.getBlock();
                case "planFullTitle":
                    return obj.getPlanFullTitle();
                case "planTitle":
                    return obj.getPlanTitle();
                case "userNumber":
                    return obj.getUserNumber();
                case "academy":
                    return obj.getAcademy();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "higherEchelon":
                    return obj.getHigherEchelon();
                case "producingOrgUnit":
                    return obj.getProducingOrgUnit();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "educationDirectionCode":
                    return obj.getEducationDirectionCode();
                case "startYear":
                    return obj.getStartYear();
                case "includeExamsInHoursAmount":
                    return obj.getIncludeExamsInHoursAmount();
                case "dissertationAsAttestation":
                    return obj.getDissertationAsAttestation();
                case "stateExamAsAttestation":
                    return obj.getStateExamAsAttestation();
                case "ksrOrIndividualLessons":
                    return obj.getKsrOrIndividualLessons();
                case "zetInWeekAtt":
                    return obj.getZetInWeekAtt();
                case "hoursAmountInZETAtt":
                    return obj.getHoursAmountInZETAtt();
                case "programLabourUnits":
                    return obj.getProgramLabourUnits();
                case "interactiveLessonsPercent":
                    return obj.getInteractiveLessonsPercent();
                case "lecturesPercent":
                    return obj.getLecturesPercent();
                case "choiceDisciplinesPercent":
                    return obj.getChoiceDisciplinesPercent();
                case "maxSize":
                    return obj.getMaxSize();
                case "planKind":
                    return obj.getPlanKind();
                case "levelCode":
                    return obj.getLevelCode();
                case "level":
                    return obj.getLevel();
                case "termsInCourse":
                    return obj.getTermsInCourse();
                case "elementsInWeek":
                    return obj.getElementsInWeek();
                case "stateExamDate":
                    return obj.getStateExamDate();
                case "stateExamDoc":
                    return obj.getStateExamDoc();
                case "stateExamType":
                    return obj.getStateExamType();
                case "application":
                    return obj.getApplication();
                case "applicationDate":
                    return obj.getApplicationDate();
                case "applicationVersion":
                    return obj.getApplicationVersion();
                case "zetInYear":
                    return obj.getZetInYear();
                case "zetInWeek":
                    return obj.getZetInWeek();
                case "hoursAmountInZET":
                    return obj.getHoursAmountInZET();
                case "totalZET":
                    return obj.getTotalZET();
                case "certificateDate":
                    return obj.getCertificateDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "block":
                    obj.setBlock((EppEduPlanVersionBlock) value);
                    return;
                case "planFullTitle":
                    obj.setPlanFullTitle((String) value);
                    return;
                case "planTitle":
                    obj.setPlanTitle((String) value);
                    return;
                case "userNumber":
                    obj.setUserNumber((String) value);
                    return;
                case "academy":
                    obj.setAcademy((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((String) value);
                    return;
                case "higherEchelon":
                    obj.setHigherEchelon((String) value);
                    return;
                case "producingOrgUnit":
                    obj.setProducingOrgUnit((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "educationDirectionCode":
                    obj.setEducationDirectionCode((String) value);
                    return;
                case "startYear":
                    obj.setStartYear((Integer) value);
                    return;
                case "includeExamsInHoursAmount":
                    obj.setIncludeExamsInHoursAmount((Boolean) value);
                    return;
                case "dissertationAsAttestation":
                    obj.setDissertationAsAttestation((Boolean) value);
                    return;
                case "stateExamAsAttestation":
                    obj.setStateExamAsAttestation((Boolean) value);
                    return;
                case "ksrOrIndividualLessons":
                    obj.setKsrOrIndividualLessons((String) value);
                    return;
                case "zetInWeekAtt":
                    obj.setZetInWeekAtt((Double) value);
                    return;
                case "hoursAmountInZETAtt":
                    obj.setHoursAmountInZETAtt((Double) value);
                    return;
                case "programLabourUnits":
                    obj.setProgramLabourUnits((Double) value);
                    return;
                case "interactiveLessonsPercent":
                    obj.setInteractiveLessonsPercent((Double) value);
                    return;
                case "lecturesPercent":
                    obj.setLecturesPercent((Double) value);
                    return;
                case "choiceDisciplinesPercent":
                    obj.setChoiceDisciplinesPercent((Double) value);
                    return;
                case "maxSize":
                    obj.setMaxSize((Double) value);
                    return;
                case "planKind":
                    obj.setPlanKind((String) value);
                    return;
                case "levelCode":
                    obj.setLevelCode((String) value);
                    return;
                case "level":
                    obj.setLevel((String) value);
                    return;
                case "termsInCourse":
                    obj.setTermsInCourse((Integer) value);
                    return;
                case "elementsInWeek":
                    obj.setElementsInWeek((Integer) value);
                    return;
                case "stateExamDate":
                    obj.setStateExamDate((Date) value);
                    return;
                case "stateExamDoc":
                    obj.setStateExamDoc((String) value);
                    return;
                case "stateExamType":
                    obj.setStateExamType((String) value);
                    return;
                case "application":
                    obj.setApplication((String) value);
                    return;
                case "applicationDate":
                    obj.setApplicationDate((Date) value);
                    return;
                case "applicationVersion":
                    obj.setApplicationVersion((String) value);
                    return;
                case "zetInYear":
                    obj.setZetInYear((Double) value);
                    return;
                case "zetInWeek":
                    obj.setZetInWeek((Double) value);
                    return;
                case "hoursAmountInZET":
                    obj.setHoursAmountInZET((Double) value);
                    return;
                case "totalZET":
                    obj.setTotalZET((Double) value);
                    return;
                case "certificateDate":
                    obj.setCertificateDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "block":
                        return true;
                case "planFullTitle":
                        return true;
                case "planTitle":
                        return true;
                case "userNumber":
                        return true;
                case "academy":
                        return true;
                case "orgUnit":
                        return true;
                case "higherEchelon":
                        return true;
                case "producingOrgUnit":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "educationDirectionCode":
                        return true;
                case "startYear":
                        return true;
                case "includeExamsInHoursAmount":
                        return true;
                case "dissertationAsAttestation":
                        return true;
                case "stateExamAsAttestation":
                        return true;
                case "ksrOrIndividualLessons":
                        return true;
                case "zetInWeekAtt":
                        return true;
                case "hoursAmountInZETAtt":
                        return true;
                case "programLabourUnits":
                        return true;
                case "interactiveLessonsPercent":
                        return true;
                case "lecturesPercent":
                        return true;
                case "choiceDisciplinesPercent":
                        return true;
                case "maxSize":
                        return true;
                case "planKind":
                        return true;
                case "levelCode":
                        return true;
                case "level":
                        return true;
                case "termsInCourse":
                        return true;
                case "elementsInWeek":
                        return true;
                case "stateExamDate":
                        return true;
                case "stateExamDoc":
                        return true;
                case "stateExamType":
                        return true;
                case "application":
                        return true;
                case "applicationDate":
                        return true;
                case "applicationVersion":
                        return true;
                case "zetInYear":
                        return true;
                case "zetInWeek":
                        return true;
                case "hoursAmountInZET":
                        return true;
                case "totalZET":
                        return true;
                case "certificateDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "block":
                    return true;
                case "planFullTitle":
                    return true;
                case "planTitle":
                    return true;
                case "userNumber":
                    return true;
                case "academy":
                    return true;
                case "orgUnit":
                    return true;
                case "higherEchelon":
                    return true;
                case "producingOrgUnit":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "educationDirectionCode":
                    return true;
                case "startYear":
                    return true;
                case "includeExamsInHoursAmount":
                    return true;
                case "dissertationAsAttestation":
                    return true;
                case "stateExamAsAttestation":
                    return true;
                case "ksrOrIndividualLessons":
                    return true;
                case "zetInWeekAtt":
                    return true;
                case "hoursAmountInZETAtt":
                    return true;
                case "programLabourUnits":
                    return true;
                case "interactiveLessonsPercent":
                    return true;
                case "lecturesPercent":
                    return true;
                case "choiceDisciplinesPercent":
                    return true;
                case "maxSize":
                    return true;
                case "planKind":
                    return true;
                case "levelCode":
                    return true;
                case "level":
                    return true;
                case "termsInCourse":
                    return true;
                case "elementsInWeek":
                    return true;
                case "stateExamDate":
                    return true;
                case "stateExamDoc":
                    return true;
                case "stateExamType":
                    return true;
                case "application":
                    return true;
                case "applicationDate":
                    return true;
                case "applicationVersion":
                    return true;
                case "zetInYear":
                    return true;
                case "zetInWeek":
                    return true;
                case "hoursAmountInZET":
                    return true;
                case "totalZET":
                    return true;
                case "certificateDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "block":
                    return EppEduPlanVersionBlock.class;
                case "planFullTitle":
                    return String.class;
                case "planTitle":
                    return String.class;
                case "userNumber":
                    return String.class;
                case "academy":
                    return String.class;
                case "orgUnit":
                    return String.class;
                case "higherEchelon":
                    return String.class;
                case "producingOrgUnit":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "educationDirectionCode":
                    return String.class;
                case "startYear":
                    return Integer.class;
                case "includeExamsInHoursAmount":
                    return Boolean.class;
                case "dissertationAsAttestation":
                    return Boolean.class;
                case "stateExamAsAttestation":
                    return Boolean.class;
                case "ksrOrIndividualLessons":
                    return String.class;
                case "zetInWeekAtt":
                    return Double.class;
                case "hoursAmountInZETAtt":
                    return Double.class;
                case "programLabourUnits":
                    return Double.class;
                case "interactiveLessonsPercent":
                    return Double.class;
                case "lecturesPercent":
                    return Double.class;
                case "choiceDisciplinesPercent":
                    return Double.class;
                case "maxSize":
                    return Double.class;
                case "planKind":
                    return String.class;
                case "levelCode":
                    return String.class;
                case "level":
                    return String.class;
                case "termsInCourse":
                    return Integer.class;
                case "elementsInWeek":
                    return Integer.class;
                case "stateExamDate":
                    return Date.class;
                case "stateExamDoc":
                    return String.class;
                case "stateExamType":
                    return String.class;
                case "application":
                    return String.class;
                case "applicationDate":
                    return Date.class;
                case "applicationVersion":
                    return String.class;
                case "zetInYear":
                    return Double.class;
                case "zetInWeek":
                    return Double.class;
                case "hoursAmountInZET":
                    return Double.class;
                case "totalZET":
                    return Double.class;
                case "certificateDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEpvBlockTitle> _dslPath = new Path<FefuEpvBlockTitle>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEpvBlockTitle");
    }
            

    /**
     * @return Блок УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
    {
        return _dslPath.block();
    }

    /**
     * @return Полное имя плана.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getPlanFullTitle()
     */
    public static PropertyPath<String> planFullTitle()
    {
        return _dslPath.planFullTitle();
    }

    /**
     * @return Имя плана.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getPlanTitle()
     */
    public static PropertyPath<String> planTitle()
    {
        return _dslPath.planTitle();
    }

    /**
     * @return Номер пользователя.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getUserNumber()
     */
    public static PropertyPath<String> userNumber()
    {
        return _dslPath.userNumber();
    }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getAcademy()
     */
    public static PropertyPath<String> academy()
    {
        return _dslPath.academy();
    }

    /**
     * @return Структурное подразделение.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getOrgUnit()
     */
    public static PropertyPath<String> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Вышестоящая организация.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getHigherEchelon()
     */
    public static PropertyPath<String> higherEchelon()
    {
        return _dslPath.higherEchelon();
    }

    /**
     * @return Выпускающее подразделение.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getProducingOrgUnit()
     */
    public static PropertyPath<String> producingOrgUnit()
    {
        return _dslPath.producingOrgUnit();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Код направления подготовки (специальности).
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getEducationDirectionCode()
     */
    public static PropertyPath<String> educationDirectionCode()
    {
        return _dslPath.educationDirectionCode();
    }

    /**
     * @return Год начала подготовки.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getStartYear()
     */
    public static PropertyPath<Integer> startYear()
    {
        return _dslPath.startYear();
    }

    /**
     * @return Включать экзамены в сумму часов.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getIncludeExamsInHoursAmount()
     */
    public static PropertyPath<Boolean> includeExamsInHoursAmount()
    {
        return _dslPath.includeExamsInHoursAmount();
    }

    /**
     * @return Относить Диссертации к разделу ИГА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getDissertationAsAttestation()
     */
    public static PropertyPath<Boolean> dissertationAsAttestation()
    {
        return _dslPath.dissertationAsAttestation();
    }

    /**
     * @return Относить Гос. экзамены и защиту ВКР к разделу ИГА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getStateExamAsAttestation()
     */
    public static PropertyPath<Boolean> stateExamAsAttestation()
    {
        return _dslPath.stateExamAsAttestation();
    }

    /**
     * @return КСР-ИЗ.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getKsrOrIndividualLessons()
     */
    public static PropertyPath<String> ksrOrIndividualLessons()
    {
        return _dslPath.ksrOrIndividualLessons();
    }

    /**
     * @return ЗЕТ в неделе (ИГА).
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getZetInWeekAtt()
     */
    public static PropertyPath<Double> zetInWeekAtt()
    {
        return _dslPath.zetInWeekAtt();
    }

    /**
     * @return Часов в ЗЕТ (ИГА).
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getHoursAmountInZETAtt()
     */
    public static PropertyPath<Double> hoursAmountInZETAtt()
    {
        return _dslPath.hoursAmountInZETAtt();
    }

    /**
     * @return Единиц трудоемкости в основной образовательной программе.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getProgramLabourUnits()
     */
    public static PropertyPath<Double> programLabourUnits()
    {
        return _dslPath.programLabourUnits();
    }

    /**
     * @return Доля занятий в интерактивной форме, %.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getInteractiveLessonsPercent()
     */
    public static PropertyPath<Double> interactiveLessonsPercent()
    {
        return _dslPath.interactiveLessonsPercent();
    }

    /**
     * @return Доля лекций, %.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getLecturesPercent()
     */
    public static PropertyPath<Double> lecturesPercent()
    {
        return _dslPath.lecturesPercent();
    }

    /**
     * @return Доля дисциплин по выбору от вариативной части, %.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getChoiceDisciplinesPercent()
     */
    public static PropertyPath<Double> choiceDisciplinesPercent()
    {
        return _dslPath.choiceDisciplinesPercent();
    }

    /**
     * @return Максимальная нагрузка.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getMaxSize()
     */
    public static PropertyPath<Double> maxSize()
    {
        return _dslPath.maxSize();
    }

    /**
     * @return Вид плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getPlanKind()
     */
    public static PropertyPath<String> planKind()
    {
        return _dslPath.planKind();
    }

    /**
     * @return Код уровня.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getLevelCode()
     */
    public static PropertyPath<String> levelCode()
    {
        return _dslPath.levelCode();
    }

    /**
     * @return Уровень.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getLevel()
     */
    public static PropertyPath<String> level()
    {
        return _dslPath.level();
    }

    /**
     * @return Семестров на курсе.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getTermsInCourse()
     */
    public static PropertyPath<Integer> termsInCourse()
    {
        return _dslPath.termsInCourse();
    }

    /**
     * @return Элементов в неделе.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getElementsInWeek()
     */
    public static PropertyPath<Integer> elementsInWeek()
    {
        return _dslPath.elementsInWeek();
    }

    /**
     * @return Дата утверждения ГОСа.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getStateExamDate()
     */
    public static PropertyPath<Date> stateExamDate()
    {
        return _dslPath.stateExamDate();
    }

    /**
     * @return Номер ГОСа.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getStateExamDoc()
     */
    public static PropertyPath<String> stateExamDoc()
    {
        return _dslPath.stateExamDoc();
    }

    /**
     * @return Поколение ГОСа.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getStateExamType()
     */
    public static PropertyPath<String> stateExamType()
    {
        return _dslPath.stateExamType();
    }

    /**
     * @return Приложение ИМЦА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getApplication()
     */
    public static PropertyPath<String> application()
    {
        return _dslPath.application();
    }

    /**
     * @return Дата приложения ИМЦА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getApplicationDate()
     */
    public static PropertyPath<Date> applicationDate()
    {
        return _dslPath.applicationDate();
    }

    /**
     * @return Версия приложения ИМЦА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getApplicationVersion()
     */
    public static PropertyPath<String> applicationVersion()
    {
        return _dslPath.applicationVersion();
    }

    /**
     * @return ЗЕТ на год.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getZetInYear()
     */
    public static PropertyPath<Double> zetInYear()
    {
        return _dslPath.zetInYear();
    }

    /**
     * @return ЗЕТ в неделе.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getZetInWeek()
     */
    public static PropertyPath<Double> zetInWeek()
    {
        return _dslPath.zetInWeek();
    }

    /**
     * @return Часов в ЗЕТ.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getHoursAmountInZET()
     */
    public static PropertyPath<Double> hoursAmountInZET()
    {
        return _dslPath.hoursAmountInZET();
    }

    /**
     * @return ЗЕТ на все.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getTotalZET()
     */
    public static PropertyPath<Double> totalZET()
    {
        return _dslPath.totalZET();
    }

    /**
     * @return Дата сертификата ИМЦА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getCertificateDate()
     */
    public static PropertyPath<Date> certificateDate()
    {
        return _dslPath.certificateDate();
    }

    public static class Path<E extends FefuEpvBlockTitle> extends EntityPath<E>
    {
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _block;
        private PropertyPath<String> _planFullTitle;
        private PropertyPath<String> _planTitle;
        private PropertyPath<String> _userNumber;
        private PropertyPath<String> _academy;
        private PropertyPath<String> _orgUnit;
        private PropertyPath<String> _higherEchelon;
        private PropertyPath<String> _producingOrgUnit;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _educationDirectionCode;
        private PropertyPath<Integer> _startYear;
        private PropertyPath<Boolean> _includeExamsInHoursAmount;
        private PropertyPath<Boolean> _dissertationAsAttestation;
        private PropertyPath<Boolean> _stateExamAsAttestation;
        private PropertyPath<String> _ksrOrIndividualLessons;
        private PropertyPath<Double> _zetInWeekAtt;
        private PropertyPath<Double> _hoursAmountInZETAtt;
        private PropertyPath<Double> _programLabourUnits;
        private PropertyPath<Double> _interactiveLessonsPercent;
        private PropertyPath<Double> _lecturesPercent;
        private PropertyPath<Double> _choiceDisciplinesPercent;
        private PropertyPath<Double> _maxSize;
        private PropertyPath<String> _planKind;
        private PropertyPath<String> _levelCode;
        private PropertyPath<String> _level;
        private PropertyPath<Integer> _termsInCourse;
        private PropertyPath<Integer> _elementsInWeek;
        private PropertyPath<Date> _stateExamDate;
        private PropertyPath<String> _stateExamDoc;
        private PropertyPath<String> _stateExamType;
        private PropertyPath<String> _application;
        private PropertyPath<Date> _applicationDate;
        private PropertyPath<String> _applicationVersion;
        private PropertyPath<Double> _zetInYear;
        private PropertyPath<Double> _zetInWeek;
        private PropertyPath<Double> _hoursAmountInZET;
        private PropertyPath<Double> _totalZET;
        private PropertyPath<Date> _certificateDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок УПв. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
        {
            if(_block == null )
                _block = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK, this);
            return _block;
        }

    /**
     * @return Полное имя плана.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getPlanFullTitle()
     */
        public PropertyPath<String> planFullTitle()
        {
            if(_planFullTitle == null )
                _planFullTitle = new PropertyPath<String>(FefuEpvBlockTitleGen.P_PLAN_FULL_TITLE, this);
            return _planFullTitle;
        }

    /**
     * @return Имя плана.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getPlanTitle()
     */
        public PropertyPath<String> planTitle()
        {
            if(_planTitle == null )
                _planTitle = new PropertyPath<String>(FefuEpvBlockTitleGen.P_PLAN_TITLE, this);
            return _planTitle;
        }

    /**
     * @return Номер пользователя.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getUserNumber()
     */
        public PropertyPath<String> userNumber()
        {
            if(_userNumber == null )
                _userNumber = new PropertyPath<String>(FefuEpvBlockTitleGen.P_USER_NUMBER, this);
            return _userNumber;
        }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getAcademy()
     */
        public PropertyPath<String> academy()
        {
            if(_academy == null )
                _academy = new PropertyPath<String>(FefuEpvBlockTitleGen.P_ACADEMY, this);
            return _academy;
        }

    /**
     * @return Структурное подразделение.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getOrgUnit()
     */
        public PropertyPath<String> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new PropertyPath<String>(FefuEpvBlockTitleGen.P_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Вышестоящая организация.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getHigherEchelon()
     */
        public PropertyPath<String> higherEchelon()
        {
            if(_higherEchelon == null )
                _higherEchelon = new PropertyPath<String>(FefuEpvBlockTitleGen.P_HIGHER_ECHELON, this);
            return _higherEchelon;
        }

    /**
     * @return Выпускающее подразделение.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getProducingOrgUnit()
     */
        public PropertyPath<String> producingOrgUnit()
        {
            if(_producingOrgUnit == null )
                _producingOrgUnit = new PropertyPath<String>(FefuEpvBlockTitleGen.P_PRODUCING_ORG_UNIT, this);
            return _producingOrgUnit;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(FefuEpvBlockTitleGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Код направления подготовки (специальности).
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getEducationDirectionCode()
     */
        public PropertyPath<String> educationDirectionCode()
        {
            if(_educationDirectionCode == null )
                _educationDirectionCode = new PropertyPath<String>(FefuEpvBlockTitleGen.P_EDUCATION_DIRECTION_CODE, this);
            return _educationDirectionCode;
        }

    /**
     * @return Год начала подготовки.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getStartYear()
     */
        public PropertyPath<Integer> startYear()
        {
            if(_startYear == null )
                _startYear = new PropertyPath<Integer>(FefuEpvBlockTitleGen.P_START_YEAR, this);
            return _startYear;
        }

    /**
     * @return Включать экзамены в сумму часов.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getIncludeExamsInHoursAmount()
     */
        public PropertyPath<Boolean> includeExamsInHoursAmount()
        {
            if(_includeExamsInHoursAmount == null )
                _includeExamsInHoursAmount = new PropertyPath<Boolean>(FefuEpvBlockTitleGen.P_INCLUDE_EXAMS_IN_HOURS_AMOUNT, this);
            return _includeExamsInHoursAmount;
        }

    /**
     * @return Относить Диссертации к разделу ИГА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getDissertationAsAttestation()
     */
        public PropertyPath<Boolean> dissertationAsAttestation()
        {
            if(_dissertationAsAttestation == null )
                _dissertationAsAttestation = new PropertyPath<Boolean>(FefuEpvBlockTitleGen.P_DISSERTATION_AS_ATTESTATION, this);
            return _dissertationAsAttestation;
        }

    /**
     * @return Относить Гос. экзамены и защиту ВКР к разделу ИГА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getStateExamAsAttestation()
     */
        public PropertyPath<Boolean> stateExamAsAttestation()
        {
            if(_stateExamAsAttestation == null )
                _stateExamAsAttestation = new PropertyPath<Boolean>(FefuEpvBlockTitleGen.P_STATE_EXAM_AS_ATTESTATION, this);
            return _stateExamAsAttestation;
        }

    /**
     * @return КСР-ИЗ.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getKsrOrIndividualLessons()
     */
        public PropertyPath<String> ksrOrIndividualLessons()
        {
            if(_ksrOrIndividualLessons == null )
                _ksrOrIndividualLessons = new PropertyPath<String>(FefuEpvBlockTitleGen.P_KSR_OR_INDIVIDUAL_LESSONS, this);
            return _ksrOrIndividualLessons;
        }

    /**
     * @return ЗЕТ в неделе (ИГА).
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getZetInWeekAtt()
     */
        public PropertyPath<Double> zetInWeekAtt()
        {
            if(_zetInWeekAtt == null )
                _zetInWeekAtt = new PropertyPath<Double>(FefuEpvBlockTitleGen.P_ZET_IN_WEEK_ATT, this);
            return _zetInWeekAtt;
        }

    /**
     * @return Часов в ЗЕТ (ИГА).
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getHoursAmountInZETAtt()
     */
        public PropertyPath<Double> hoursAmountInZETAtt()
        {
            if(_hoursAmountInZETAtt == null )
                _hoursAmountInZETAtt = new PropertyPath<Double>(FefuEpvBlockTitleGen.P_HOURS_AMOUNT_IN_Z_E_T_ATT, this);
            return _hoursAmountInZETAtt;
        }

    /**
     * @return Единиц трудоемкости в основной образовательной программе.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getProgramLabourUnits()
     */
        public PropertyPath<Double> programLabourUnits()
        {
            if(_programLabourUnits == null )
                _programLabourUnits = new PropertyPath<Double>(FefuEpvBlockTitleGen.P_PROGRAM_LABOUR_UNITS, this);
            return _programLabourUnits;
        }

    /**
     * @return Доля занятий в интерактивной форме, %.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getInteractiveLessonsPercent()
     */
        public PropertyPath<Double> interactiveLessonsPercent()
        {
            if(_interactiveLessonsPercent == null )
                _interactiveLessonsPercent = new PropertyPath<Double>(FefuEpvBlockTitleGen.P_INTERACTIVE_LESSONS_PERCENT, this);
            return _interactiveLessonsPercent;
        }

    /**
     * @return Доля лекций, %.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getLecturesPercent()
     */
        public PropertyPath<Double> lecturesPercent()
        {
            if(_lecturesPercent == null )
                _lecturesPercent = new PropertyPath<Double>(FefuEpvBlockTitleGen.P_LECTURES_PERCENT, this);
            return _lecturesPercent;
        }

    /**
     * @return Доля дисциплин по выбору от вариативной части, %.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getChoiceDisciplinesPercent()
     */
        public PropertyPath<Double> choiceDisciplinesPercent()
        {
            if(_choiceDisciplinesPercent == null )
                _choiceDisciplinesPercent = new PropertyPath<Double>(FefuEpvBlockTitleGen.P_CHOICE_DISCIPLINES_PERCENT, this);
            return _choiceDisciplinesPercent;
        }

    /**
     * @return Максимальная нагрузка.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getMaxSize()
     */
        public PropertyPath<Double> maxSize()
        {
            if(_maxSize == null )
                _maxSize = new PropertyPath<Double>(FefuEpvBlockTitleGen.P_MAX_SIZE, this);
            return _maxSize;
        }

    /**
     * @return Вид плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getPlanKind()
     */
        public PropertyPath<String> planKind()
        {
            if(_planKind == null )
                _planKind = new PropertyPath<String>(FefuEpvBlockTitleGen.P_PLAN_KIND, this);
            return _planKind;
        }

    /**
     * @return Код уровня.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getLevelCode()
     */
        public PropertyPath<String> levelCode()
        {
            if(_levelCode == null )
                _levelCode = new PropertyPath<String>(FefuEpvBlockTitleGen.P_LEVEL_CODE, this);
            return _levelCode;
        }

    /**
     * @return Уровень.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getLevel()
     */
        public PropertyPath<String> level()
        {
            if(_level == null )
                _level = new PropertyPath<String>(FefuEpvBlockTitleGen.P_LEVEL, this);
            return _level;
        }

    /**
     * @return Семестров на курсе.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getTermsInCourse()
     */
        public PropertyPath<Integer> termsInCourse()
        {
            if(_termsInCourse == null )
                _termsInCourse = new PropertyPath<Integer>(FefuEpvBlockTitleGen.P_TERMS_IN_COURSE, this);
            return _termsInCourse;
        }

    /**
     * @return Элементов в неделе.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getElementsInWeek()
     */
        public PropertyPath<Integer> elementsInWeek()
        {
            if(_elementsInWeek == null )
                _elementsInWeek = new PropertyPath<Integer>(FefuEpvBlockTitleGen.P_ELEMENTS_IN_WEEK, this);
            return _elementsInWeek;
        }

    /**
     * @return Дата утверждения ГОСа.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getStateExamDate()
     */
        public PropertyPath<Date> stateExamDate()
        {
            if(_stateExamDate == null )
                _stateExamDate = new PropertyPath<Date>(FefuEpvBlockTitleGen.P_STATE_EXAM_DATE, this);
            return _stateExamDate;
        }

    /**
     * @return Номер ГОСа.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getStateExamDoc()
     */
        public PropertyPath<String> stateExamDoc()
        {
            if(_stateExamDoc == null )
                _stateExamDoc = new PropertyPath<String>(FefuEpvBlockTitleGen.P_STATE_EXAM_DOC, this);
            return _stateExamDoc;
        }

    /**
     * @return Поколение ГОСа.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getStateExamType()
     */
        public PropertyPath<String> stateExamType()
        {
            if(_stateExamType == null )
                _stateExamType = new PropertyPath<String>(FefuEpvBlockTitleGen.P_STATE_EXAM_TYPE, this);
            return _stateExamType;
        }

    /**
     * @return Приложение ИМЦА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getApplication()
     */
        public PropertyPath<String> application()
        {
            if(_application == null )
                _application = new PropertyPath<String>(FefuEpvBlockTitleGen.P_APPLICATION, this);
            return _application;
        }

    /**
     * @return Дата приложения ИМЦА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getApplicationDate()
     */
        public PropertyPath<Date> applicationDate()
        {
            if(_applicationDate == null )
                _applicationDate = new PropertyPath<Date>(FefuEpvBlockTitleGen.P_APPLICATION_DATE, this);
            return _applicationDate;
        }

    /**
     * @return Версия приложения ИМЦА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getApplicationVersion()
     */
        public PropertyPath<String> applicationVersion()
        {
            if(_applicationVersion == null )
                _applicationVersion = new PropertyPath<String>(FefuEpvBlockTitleGen.P_APPLICATION_VERSION, this);
            return _applicationVersion;
        }

    /**
     * @return ЗЕТ на год.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getZetInYear()
     */
        public PropertyPath<Double> zetInYear()
        {
            if(_zetInYear == null )
                _zetInYear = new PropertyPath<Double>(FefuEpvBlockTitleGen.P_ZET_IN_YEAR, this);
            return _zetInYear;
        }

    /**
     * @return ЗЕТ в неделе.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getZetInWeek()
     */
        public PropertyPath<Double> zetInWeek()
        {
            if(_zetInWeek == null )
                _zetInWeek = new PropertyPath<Double>(FefuEpvBlockTitleGen.P_ZET_IN_WEEK, this);
            return _zetInWeek;
        }

    /**
     * @return Часов в ЗЕТ.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getHoursAmountInZET()
     */
        public PropertyPath<Double> hoursAmountInZET()
        {
            if(_hoursAmountInZET == null )
                _hoursAmountInZET = new PropertyPath<Double>(FefuEpvBlockTitleGen.P_HOURS_AMOUNT_IN_Z_E_T, this);
            return _hoursAmountInZET;
        }

    /**
     * @return ЗЕТ на все.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getTotalZET()
     */
        public PropertyPath<Double> totalZET()
        {
            if(_totalZET == null )
                _totalZET = new PropertyPath<Double>(FefuEpvBlockTitleGen.P_TOTAL_Z_E_T, this);
            return _totalZET;
        }

    /**
     * @return Дата сертификата ИМЦА.
     * @see ru.tandemservice.unifefu.entity.FefuEpvBlockTitle#getCertificateDate()
     */
        public PropertyPath<Date> certificateDate()
        {
            if(_certificateDate == null )
                _certificateDate = new PropertyPath<Date>(FefuEpvBlockTitleGen.P_CERTIFICATE_DATE, this);
            return _certificateDate;
        }

        public Class getEntityClass()
        {
            return FefuEpvBlockTitle.class;
        }

        public String getEntityName()
        {
            return "fefuEpvBlockTitle";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
