package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

import java.util.Collection;

/**
 * Created by Savva on 10.02.14.
 */
public interface IFEFUMdbViewDaemonDAO {
    final String GLOBAL_DAEMON_LOCK = IFEFUMdbViewDaemonDAO.class.getName() + ".global-lock";
    final SpringBeanCache<IFEFUMdbViewDaemonDAO> instance = new SpringBeanCache<>(IFEFUMdbViewDaemonDAO.class.getName());

    @Transactional(readOnly = false)/*(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class}) */
    void doExportMdbView(Collection<Long> ipvIds);

    public Collection<Long> exist(Boolean b);
    public Collection<Long> epvIds2=null;
    public void clearEntity();
}
