/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ParametersTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.logic.FefuEduStdParametersCompetenceDSHandler;
import ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 01.12.2014
 */
@Configuration
public class FefuEduStdParametersTab extends BusinessComponentManager
{
    public static final String COMPETENCE_DS = "competenceDS";
    public static final String CONTROL_ACTION_DS = "controlActionDS";

    public static final String COMPETENCE_CODE = "competenceCode";
    public static final String COMPETENCE_TITLE = "competenceTitle";

    public static final String COMPETENCE_NUMBER_UP_NAME = "competenceNumberUp";
    public static final String COMPETENCE_NUMBER_DOWN_NAME = "competenceNumberDown";

    public static final String COMPETENCE_NUMBER_UP_LISTENER = "onClickCompetenceNumberUp";
    public static final String COMPETENCE_NUMBER_DOWN_LISTENER = "onClickCompetenceNumberDown";
    public static final String COMPETENCE_DELETE_LISTENER = "onClickDeleteCompetence";

    public static final String CONTROL_ACTION_EDIT_LISTENER = "onClickEditControlAction";
    public static final String CONTROL_ACTION_DELETE_LISTENER = "onClickDeleteControlAction";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(COMPETENCE_DS, competenceDS(), competenceDSHandler()))
                .addDataSource(searchListDS(CONTROL_ACTION_DS, controlActionDS(), controlActionDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint competenceDS()
    {
        return columnListExtPointBuilder(COMPETENCE_DS)
                .addColumn(textColumn(COMPETENCE_CODE, COMPETENCE_CODE))
                .addColumn(textColumn(COMPETENCE_TITLE, COMPETENCE_TITLE))
                .addColumn(actionColumn(COMPETENCE_NUMBER_UP_NAME, CommonDefines.ICON_UP, COMPETENCE_NUMBER_UP_LISTENER))
                .addColumn(actionColumn(COMPETENCE_NUMBER_DOWN_NAME, CommonDefines.ICON_DOWN, COMPETENCE_NUMBER_DOWN_LISTENER))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, COMPETENCE_DELETE_LISTENER).alert("message:competenceDS.delete.alert"))
                .create();
    }

    @Bean
    public ColumnListExtPoint controlActionDS()
    {
        return columnListExtPointBuilder(CONTROL_ACTION_DS)
                .addColumn(textColumn(FefuFControlActionType2EppStateEduStandardRel.L_CONTROL_ACTION, FefuFControlActionType2EppStateEduStandardRel.controlAction().title()))
                .addColumn(textColumn(FefuFControlActionType2EppStateEduStandardRel.P_MAX_PART_YEAR, FefuFControlActionType2EppStateEduStandardRel.maxPartYear()))
                .addColumn(textColumn(FefuFControlActionType2EppStateEduStandardRel.P_MAX_YEAR, FefuFControlActionType2EppStateEduStandardRel.maxYear()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, CONTROL_ACTION_EDIT_LISTENER))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, CONTROL_ACTION_DELETE_LISTENER).alert("message:controlActionDS.delete.alert"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> competenceDSHandler()
    {
        return new FefuEduStdParametersCompetenceDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> controlActionDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FefuFControlActionType2EppStateEduStandardRel.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                Long eduStandardId = context.get(FefuEduStdParametersTabUI.EDU_STANDARD_ID);
                dql.where(eq(property(alias, FefuFControlActionType2EppStateEduStandardRel.eduStandard().id()), value(eduStandardId)));
            }
        }
                .order(FefuFControlActionType2EppStateEduStandardRel.controlAction().title());
    }
}