/*$Id$*/
package ru.tandemservice.unifefu.component.orgunit.AttSheetPrintRtf;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import ru.tandemservice.unifefu.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;

/**
 * @author DMITRY KNYAZEV
 * @since 11.07.2014
 */
public class Controller extends ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTabPrintRtf.Controller
{

    @Override
    public RtfDocument createRtfDocument()
    {
        final UnisessionCommonTemplate template = DataAccessServices.dao().get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.FEFU_ATT_SHEET);
        return new RtfReader().read(template.getContent());
    }

    @Override
    protected String getFileName()
    {
        return "fefuAttSheet.rtf";
    }
}
