package ru.tandemservice.unifefu.component.listextract.fefu13.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative.AbstractParagraphAddEditAlternativeDAO;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uni.util.CustomStateUtil;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;
import ru.tandemservice.unifefu.entity.catalog.FefuPromotedActivityType;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public class DAO extends AbstractParagraphAddEditAlternativeDAO<FefuAdditionalAcademGrantStuListExtract, Model> implements IDAO
{
	@Override
	public void prepare(final Model model)
	{
		super.prepare(model);
		model.setCourseList(DevelopGridDAO.getCourseList());
		model.setCompensationTypeList(getCatalogItemList(CompensationType.class));

		model.setGroupListModel(new GroupSelectModel(model, model.getOrgUnit())
		{
			@Override
			protected boolean isNeedRequest()
			{
				return true;
			}
		}.extModel(model).showChildLevels(true));

		model.setEduLevelHighSchoolListModel(new ExtEducationLevelsHighSchoolSelectModel(null, null, model.getOrgUnit()));

        model.setPromotedActivityTypeList(getCatalogItemList(FefuPromotedActivityType.class));

		if (model.getParagraphId() == null)
		{
			model.setCompensationType(get(CompensationType.class, CompensationType.P_CODE, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
		}
        FefuAdditionalAcademGrantStuListExtract extract = model.getFirstExtract();
		if (model.isEditForm())
		{
            model.setPromotedActivityType(extract.getPromotedActivityType());
            model.setDateStart(extract.getDateStart());
            model.setDateEnd(extract.getDateEnd());
		}
		model.setEmployeePostModel(new OrderExecutorSelectModel());
		if (extract != null)
		{
			model.setMatchingDate(extract.getMatchingDate());
			model.setProtocolNumber(extract.getProtocolNumber());
			model.setResponsibleForAgreement(extract.getResponsibleForAgreement());
			model.setResponsibleForPayments(extract.getResponsibleForPayments());
		}
	}

	@Override
	protected FefuAdditionalAcademGrantStuListExtract createNewInstance(Model model)
	{
		return new FefuAdditionalAcademGrantStuListExtract();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void fillExtract(FefuAdditionalAcademGrantStuListExtract extract, Student student, Model model)
	{
		IValueMapHolder grantSizeHolder = (IValueMapHolder) model.getDataSource().getColumn("grantSize");
		Map<Long, Object> grantSizeMap = (null == grantSizeHolder ? Collections.emptyMap() : grantSizeHolder.getValueMap());

		Number grantSize = (Number) grantSizeMap.get(student.getId());

		extract.setCourse(student.getCourse());
		extract.setGroup(student.getGroup());
		extract.setCompensationType(student.getCompensationType());
        extract.setPromotedActivityType(model.getPromotedActivityType());
        extract.setDateStart(model.getDateStart());
		extract.setDateEnd(model.getDateEnd());
		extract.setAdditionalAcademGrantSize(grantSize.doubleValue());
		extract.setMatchingDate(model.getMatchingDate());
		extract.setProtocolNumber(model.getProtocolNumber());
		extract.setResponsibleForAgreement(model.getResponsibleForAgreement());
		extract.setResponsibleForPayments(model.getResponsibleForPayments());
	}

	@Override
	@SuppressWarnings("unchecked")
	public void update(Model model)
	{
		ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

		IValueMapHolder grantSizeHolder = (IValueMapHolder) model.getDataSource().getColumn("grantSize");
		Map<Long, Double> grantSizeMap = (null == grantSizeHolder ? Collections.emptyMap() : grantSizeHolder.getValueMap());

		List<ViewWrapper<Student>> viewlist = new ArrayList<ViewWrapper<Student>>((Collection) model.getDataSource().getEntityList());

		for (ViewWrapper<Student> wrapper : viewlist)
		{
			if (grantSizeMap.get(wrapper.getEntity().getId()) == null)
				errorCollector.add("Поле «Размер дополнительной стипендии» обязательно для заполнения.", "grantSizeId_" + wrapper.getEntity().getId());
		}

		if (errorCollector.hasErrors())
			return;

		super.update(model);
	}

	@Override
	protected void patchSearchDataSource(DQLSelectBuilder builder, Model model)
	{
		if (model.getGroup() != null)
		{
			builder.where(eq(property(STUDENT_ALIAS, Student.group()), value(model.getGroup())));
		}
		else
		{
			if (model.getCourse() != null)
			{
				builder.where(eq(property(STUDENT_ALIAS, Student.course()), value(model.getCourse())));
			}
			if (model.getEducationLevelsHighSchool() != null)
			{
				builder.where(eq(property(STUDENT_ALIAS, Student.group().educationOrgUnit().educationLevelHighSchool()), value(model.getEducationLevelsHighSchool())));
			}
		}

		if (model.getCompensationType() != null)
		{
			builder.where(eq(property(STUDENT_ALIAS, Student.compensationType()), value(model.getCompensationType())));
		}

		if (model.getStudentCustomStateCIs() != null && !model.getStudentCustomStateCIs().isEmpty())
		{
            CustomStateUtil.addCustomStatesFilter(builder, STUDENT_ALIAS, model.getStudentCustomStateCIs());
		}
	}
}