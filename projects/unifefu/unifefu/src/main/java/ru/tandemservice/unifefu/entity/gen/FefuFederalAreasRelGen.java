package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.unifefu.entity.FefuFederalAreasRel;
import ru.tandemservice.unifefu.entity.catalog.FefuFederalDistrict;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь федерального округа с административной единицей
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuFederalAreasRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuFederalAreasRel";
    public static final String ENTITY_NAME = "fefuFederalAreasRel";
    public static final int VERSION_HASH = -858619986;
    private static IEntityMeta ENTITY_META;

    public static final String L_FEFU_FEDERAL_DISTRICT = "fefuFederalDistrict";
    public static final String L_ADDRESS_ITEM = "addressItem";

    private FefuFederalDistrict _fefuFederalDistrict;     // Федеральный округ
    private AddressItem _addressItem;     // Административная единица

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Федеральный округ. Свойство не может быть null.
     */
    @NotNull
    public FefuFederalDistrict getFefuFederalDistrict()
    {
        return _fefuFederalDistrict;
    }

    /**
     * @param fefuFederalDistrict Федеральный округ. Свойство не может быть null.
     */
    public void setFefuFederalDistrict(FefuFederalDistrict fefuFederalDistrict)
    {
        dirty(_fefuFederalDistrict, fefuFederalDistrict);
        _fefuFederalDistrict = fefuFederalDistrict;
    }

    /**
     * @return Административная единица. Свойство не может быть null.
     */
    @NotNull
    public AddressItem getAddressItem()
    {
        return _addressItem;
    }

    /**
     * @param addressItem Административная единица. Свойство не может быть null.
     */
    public void setAddressItem(AddressItem addressItem)
    {
        dirty(_addressItem, addressItem);
        _addressItem = addressItem;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuFederalAreasRelGen)
        {
            setFefuFederalDistrict(((FefuFederalAreasRel)another).getFefuFederalDistrict());
            setAddressItem(((FefuFederalAreasRel)another).getAddressItem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuFederalAreasRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuFederalAreasRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuFederalAreasRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "fefuFederalDistrict":
                    return obj.getFefuFederalDistrict();
                case "addressItem":
                    return obj.getAddressItem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "fefuFederalDistrict":
                    obj.setFefuFederalDistrict((FefuFederalDistrict) value);
                    return;
                case "addressItem":
                    obj.setAddressItem((AddressItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "fefuFederalDistrict":
                        return true;
                case "addressItem":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "fefuFederalDistrict":
                    return true;
                case "addressItem":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "fefuFederalDistrict":
                    return FefuFederalDistrict.class;
                case "addressItem":
                    return AddressItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuFederalAreasRel> _dslPath = new Path<FefuFederalAreasRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuFederalAreasRel");
    }
            

    /**
     * @return Федеральный округ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFederalAreasRel#getFefuFederalDistrict()
     */
    public static FefuFederalDistrict.Path<FefuFederalDistrict> fefuFederalDistrict()
    {
        return _dslPath.fefuFederalDistrict();
    }

    /**
     * @return Административная единица. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFederalAreasRel#getAddressItem()
     */
    public static AddressItem.Path<AddressItem> addressItem()
    {
        return _dslPath.addressItem();
    }

    public static class Path<E extends FefuFederalAreasRel> extends EntityPath<E>
    {
        private FefuFederalDistrict.Path<FefuFederalDistrict> _fefuFederalDistrict;
        private AddressItem.Path<AddressItem> _addressItem;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Федеральный округ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFederalAreasRel#getFefuFederalDistrict()
     */
        public FefuFederalDistrict.Path<FefuFederalDistrict> fefuFederalDistrict()
        {
            if(_fefuFederalDistrict == null )
                _fefuFederalDistrict = new FefuFederalDistrict.Path<FefuFederalDistrict>(L_FEFU_FEDERAL_DISTRICT, this);
            return _fefuFederalDistrict;
        }

    /**
     * @return Административная единица. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFederalAreasRel#getAddressItem()
     */
        public AddressItem.Path<AddressItem> addressItem()
        {
            if(_addressItem == null )
                _addressItem = new AddressItem.Path<AddressItem>(L_ADDRESS_ITEM, this);
            return _addressItem;
        }

        public Class getEntityClass()
        {
            return FefuFederalAreasRel.class;
        }

        public String getEntityName()
        {
            return "fefuFederalAreasRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
