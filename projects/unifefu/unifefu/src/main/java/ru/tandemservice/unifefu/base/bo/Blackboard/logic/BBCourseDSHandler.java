/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse2EppRegElementRel;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse2PpsEntryRel;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse2TrJournalRel;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 18.03.2014
 */
public class BBCourseDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String REG_ELEMENT_PARAM = "regElement";
    public static final String EXCLUDE_REG_ELEMENT_PARAM = "excludeRegElement";
    public static final String TR_JOURNAL_PARAM = "trJournal";
    public static final String EXCLUDE_TR_JOURNAL_PARAM = "excludeTrJournal";
    public static final String PPS_ENTRY_LIST_FILTER_PARAM = "ppsEntryList";
    public static final String COURSE_TITLE_FILTER_PARAM = "courseTitle";
    public static final String COURSE_ID_FILTER_PARAM = "courseId";
    public static final String SHOW_ARCHIVE_FILTER_PARAM = "showArchive";

    public static final IdentifiableWrapper YES_ITEM = new IdentifiableWrapper(1L, "Да");

    private boolean _ppsRequired;

    public BBCourseDSHandler(String ownerId, boolean ppsRequired)
    {
        super(ownerId, BbCourse.class);
        _ppsRequired = ppsRequired;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EppRegistryElement registryElement = context.get(REG_ELEMENT_PARAM);
        EppRegistryElement excludeRegElement = context.get(EXCLUDE_REG_ELEMENT_PARAM);
        TrJournal trJournal = context.get(TR_JOURNAL_PARAM);
        TrJournal excludeTrJournal = context.get(EXCLUDE_TR_JOURNAL_PARAM);
        List<PpsEntry> ppsEntryList = context.get(PPS_ENTRY_LIST_FILTER_PARAM);

        if (_ppsRequired && (ppsEntryList == null || ppsEntryList.isEmpty()))
            return new DSOutput(input);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(BbCourse.class, "bb").column("bb");

        // Для выпадающих списков
        Set keys = input.getPrimaryKeys();
        if (keys != null && !keys.isEmpty())
        {
            if (keys.size() == 1)
                builder.where(eq(property("bb.id"), commonValue(keys.iterator().next())));
            else
                builder.where(in("bb.id", keys));
        }

        String idFilter = context.get(COURSE_ID_FILTER_PARAM);
        if (idFilter != null)
        {
            builder.where(likeUpper(property("bb", BbCourse.bbCourseId()), value(CoreStringUtils.escapeLike(idFilter))));
        }

        IdentifiableWrapper showArchive = context.get(SHOW_ARCHIVE_FILTER_PARAM);
        if (showArchive == null || !YES_ITEM.getId().equals(showArchive.getId()))
        {
            builder.where(eq(property("bb", BbCourse.archive()), value(Boolean.FALSE)));
        }

        if (registryElement != null || excludeRegElement != null)
        {
            DQLSelectBuilder relBuilder = new DQLSelectBuilder().fromEntity(BbCourse2EppRegElementRel.class, "rel").column("rel.id")
                    .where(eq(property("rel", BbCourse2EppRegElementRel.L_BB_COURSE), property("bb.id")));

            if (registryElement != null)
            {
                relBuilder.where(eq(property("rel", BbCourse2EppRegElementRel.L_EPP_REGISTRY_ELEMENT), value(registryElement)));
                builder.where(exists(relBuilder.buildQuery()));
            }

            if (excludeRegElement != null)
            {
                relBuilder.where(eq(property("rel", BbCourse2EppRegElementRel.L_EPP_REGISTRY_ELEMENT), value(excludeRegElement)));
                builder.where(notExists(relBuilder.buildQuery()));
            }
        }

        if (trJournal != null || excludeTrJournal != null)
        {
            DQLSelectBuilder relBuilder = new DQLSelectBuilder().fromEntity(BbCourse2TrJournalRel.class, "rel").column("rel.id")
                    .where(eq(property("rel", BbCourse2TrJournalRel.L_BB_COURSE), property("bb.id")));

            if (trJournal != null)
            {
                relBuilder.where(eq(property("rel", BbCourse2TrJournalRel.L_TR_JOURNAL), value(trJournal)));
                builder.where(exists(relBuilder.buildQuery()));
            }

            if (excludeTrJournal != null)
            {
                relBuilder.where(eq(property("rel", BbCourse2TrJournalRel.L_TR_JOURNAL), value(excludeTrJournal)));
                builder.where(notExists(relBuilder.buildQuery()));
            }
        }

        if (ppsEntryList != null && !ppsEntryList.isEmpty())
        {
            DQLSelectBuilder ppsBuilder = new DQLSelectBuilder().fromEntity(BbCourse2PpsEntryRel.class, "ppsRel")
                    .where(eq(property("ppsRel", BbCourse2PpsEntryRel.bbCourse()), property("bb.id")));

            if (ppsEntryList.size() == 1)
                ppsBuilder.where(eq(property("ppsRel", BbCourse2PpsEntryRel.ppsEntry()), value(ppsEntryList.get(0))));
            else
                ppsBuilder.where(in(property("ppsRel", BbCourse2PpsEntryRel.ppsEntry()), ppsEntryList));

            builder.where(exists(ppsBuilder.buildQuery()));
        }

        String filter = context.get(COURSE_TITLE_FILTER_PARAM);
        if (StringUtils.isNotEmpty(filter))
        {
            builder.where(likeUpper(property("bb", BbCourse.title()), value(CoreStringUtils.escapeLike(filter, true))));
        }

        // Для выпадающих списков
        filter = input.getComboFilterByValue();
        if (StringUtils.isNotEmpty(filter))
        {
            builder.where(likeUpper(property("bb", BbCourse.title()), value(CoreStringUtils.escapeLike(filter, true))));
        }

        if (input.getEntityOrder() != null)
        {
            builder.order(property("bb", input.getEntityOrder().getKeyString()), input.getEntityOrder().getDirection());
        }
        else
        {
            // Для выпадающих списков
            builder.order(property("bb", BbCourse.title()));
            builder.order(property("bb", BbCourse.bbCourseId()));
        }

        // Для выпадающих списков
        if (input.getCountRecord() == 0)
            input.setCountRecord(50);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }
}