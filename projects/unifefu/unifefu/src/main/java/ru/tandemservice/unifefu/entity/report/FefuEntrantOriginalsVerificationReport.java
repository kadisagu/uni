package ru.tandemservice.unifefu.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.IFefuEntrantReport;
import ru.tandemservice.unifefu.entity.report.gen.FefuEntrantOriginalsVerificationReportGen;

/**
 * Списки абитуриентов для сверки по подлинникам
 */
public class FefuEntrantOriginalsVerificationReport extends FefuEntrantOriginalsVerificationReportGen implements IFefuEntrantReport
{
    @EntityDSLSupport
    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}