/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu17.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.01.2015
 */
public class Model extends AbstractListExtractPubModel<FefuTransfStuDPOListExtract>
{
}