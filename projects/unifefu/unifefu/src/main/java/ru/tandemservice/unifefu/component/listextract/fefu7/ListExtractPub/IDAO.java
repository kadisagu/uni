/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu7.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaSuccessWithDismissStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuGiveDiplomaSuccessWithDismissStuListExtract, Model>
{
}
