package ru.tandemservice.unifefu.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifefu.entity.report.gen.*;

/**
 * Отчет «Результаты итоговой/промежуточной аттестации студентов по дисциплинам, участвующим в рейтинговой системе»
 */
public class FefuBrsAttestationResultsReport extends FefuBrsAttestationResultsReportGen
{
    @Override
    @EntityDSLSupport(parts = FefuBrsAttestationResultsReport.P_FORMING_DATE)
    public String getFormingDateStr()
    {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(getFormingDate());
    }
}