/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

/**
 * @author Nikolay Fedorovskih
 * @since 11.03.2014
 */
public class HeaderHandlerResolver implements HandlerResolver
{
    private String _sessionParamValue;
    private Set<IChangeSessionListener> _listeners;

    public HeaderHandlerResolver(String sessionParamValue)
    {
        _sessionParamValue = sessionParamValue;
        _listeners = new HashSet<>();
    }

    public void setSessionParamValue(String sessionParamValue)
    {
        _sessionParamValue = sessionParamValue;
        for (IChangeSessionListener listener : _listeners)
        {
            listener.sessionChanged();
        }
    }

    public String getSessionParamValue()
    {
        return _sessionParamValue;
    }

    public void addOnChangeListener(IChangeSessionListener listener)
    {
        _listeners.add(listener);
    }

    public List<Handler> getHandlerChain(PortInfo portInfo)
    {
        List<Handler> handlerChain = new ArrayList<>(1);
        handlerChain.add(new HeaderHandler(_sessionParamValue));
        return handlerChain;
    }
}