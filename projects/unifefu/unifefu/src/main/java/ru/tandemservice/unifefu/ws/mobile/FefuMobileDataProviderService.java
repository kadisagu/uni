/* $Id$ */
package ru.tandemservice.unifefu.ws.mobile;

import ru.tandemservice.unifefu.base.bo.FefuMobile.FefuMobileManager;
import ru.tandemservice.unifefu.ws.mobile.entity.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collections;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2014
 */

@WebService(serviceName = "fefuMobileDataProviderService")
public class FefuMobileDataProviderService
{
    @WebMethod(action = "commitEntityByIdList")
    public String commitEntityByIdList(@WebParam(name = "entityIdList", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") List<Long> entityIdList)
    {
        FefuMobileManager.instance().dao().doCommitEntityByIdList(entityIdList);
        return "0";
    }


    @WebMethod(action = "getCurrentEduYear")
    public EduYear getCurrentEduYear()
    {
        return FefuMobileManager.instance().dao().getCurrentEduYear();
    }

    @WebMethod(action = "getEduYearsList")
    public List<EduYear> getEduYearsList()
    {
        return FefuMobileManager.instance().dao().getEduYearsList();
    }


    @WebMethod(action = "getStudentPersonsIdsList")
    public List<Long> getStudentPersonsIdsList()
    {
        return FefuMobileManager.instance().dao().getActiveStudentPersonsList();
    }

    @WebMethod(action = "getNewAndUpdatedPersonsIdsList")
    public List<Long> getNewAndUpdatedPersonsIdsList()
    {
        return FefuMobileManager.instance().dao().getNewAndUpdatedEntityIdsList("mobPerson");
    }

    @WebMethod(action = "getDeletedPersonsIdsList")
    public List<Long> getDeletedPersonsIdsList()
    {
        return FefuMobileManager.instance().dao().getDeletedEntityIdsList("mobPerson");
    }

    @WebMethod(action = "getPerson")
    public Person getPerson(@WebParam(name = "personId", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") Long personId)
    {
        return FefuMobileManager.instance().dao().getPersonById(personId);
    }

    @WebMethod(action = "getPersonsByIdList")
    public List<Person> getPersonsByIdList(@WebParam(name = "personIdList", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") List<Long> personIdList)
    {
        return FefuMobileManager.instance().dao().getPersonsByIdList(personIdList);
    }


    @WebMethod(action = "getStudentsIdsList")
    public List<Long> getStudentsIdsList()
    {
        return FefuMobileManager.instance().dao().getActiveStudentIdsList();
    }

    @WebMethod(action = "getNewAndUpdatedStudentsIdsList")
    public List<Long> getNewAndUpdatedStudentsIdsList()
    {
        return FefuMobileManager.instance().dao().getNewAndUpdatedEntityIdsList("student");
    }

    @WebMethod(action = "getDeletedStudentsIdsList")
    public List<Long> getDeletedStudentsIdsList()
    {
        return FefuMobileManager.instance().dao().getDeletedEntityIdsList("student");
    }

    @WebMethod(action = "getStudent")
    public Student getStudent(@WebParam(name = "studentId", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") Long studentId)
    {
        return FefuMobileManager.instance().dao().getStudentById(studentId);
    }

    @WebMethod(action = "getStudentsByIdList")
    public List<Student> getStudentsByIdList(@WebParam(name = "studentIdList", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") List<Long> studentIdList)
    {
        return FefuMobileManager.instance().dao().getStudentsByIdList(studentIdList);
    }


    @WebMethod(action = "getSubjectsIdsList")
    public List<Long> getSubjectsIdsList()
    {
        return FefuMobileManager.instance().dao().getActiveSubjectIdsList();
    }

    @WebMethod(action = "getNewAndUpdatedSubjectIdsList")
    public List<Long> getNewAndUpdatedSubjectIdsList()
    {
        return FefuMobileManager.instance().dao().getNewAndUpdatedEntityIdsList("subject");
    }

    @WebMethod(action = "getDeletedSubjectIdsList")
    public List<Long> getDeletedSubjectIdsList()
    {
        return FefuMobileManager.instance().dao().getDeletedEntityIdsList("subject");
    }

    @WebMethod(action = "getSubject")
    public Subject getSubject(@WebParam(name = "subjectId", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") Long subjectId)
    {
        List<Subject> subjectsList = FefuMobileManager.instance().dao().getSubjectsByIdList(Collections.singletonList(subjectId));
        return (null != subjectsList && !subjectsList.isEmpty()) ? subjectsList.get(0) : null;
    }

    @WebMethod(action = "getSubjectsByIdList")
    public List<Subject> getSubjectsByIdList(@WebParam(name = "subjectIdList", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") List<Long> subjectIdList)
    {
        return FefuMobileManager.instance().dao().getSubjectsByIdList(subjectIdList);
    }


    @WebMethod(action = "getStudent2SemestersIdsList")
    public List<Long> getStudent2SemestersIdsList()
    {
        return FefuMobileManager.instance().dao().getActiveStudent2SemestersIdsList();
    }

    @WebMethod(action = "getNewAndUpdatedStudent2SemesterIdsList")
    public List<Long> getNewAndUpdatedStudent2SemesterIdsList()
    {
        return FefuMobileManager.instance().dao().getNewAndUpdatedEntityIdsList("student2Semester");
    }

    @WebMethod(action = "getDeletedStudent2SemesterIdsList")
    public List<Long> getDeletedStudent2SemesterIdsList()
    {
        return FefuMobileManager.instance().dao().getDeletedEntityIdsList("student2Semester");
    }

    @WebMethod(action = "getStudent2Semester")
    public Student2Semester getStudent2Semester(@WebParam(name = "studentToSemesterId", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") Long studentToSemesterId)
    {
        List<Student2Semester> student2SemestersList = FefuMobileManager.instance().dao().getStudent2SemestersByIdList(Collections.singletonList(studentToSemesterId));
        return (null != student2SemestersList && !student2SemestersList.isEmpty()) ? student2SemestersList.get(0) : null;
    }

    @WebMethod(action = "getStudent2SemestersByIdList")
    public List<Student2Semester> getStudent2SemestersByIdList(@WebParam(name = "student2SemesterIdList", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") List<Long> student2SemesterIdList)
    {
        return FefuMobileManager.instance().dao().getStudent2SemestersByIdList(student2SemesterIdList);
    }


    @WebMethod(action = "getPeriodsIdsList")
    public List<Long> getPeriodsIdsList()
    {
        return FefuMobileManager.instance().dao().getActualPeriodsIdsList();
    }

    @WebMethod(action = "getNewAndUpdatedPeriodIdsList")
    public List<Long> getNewAndUpdatedPeriodIdsList()
    {
        return FefuMobileManager.instance().dao().getNewAndUpdatedEntityIdsList("period");
    }

    @WebMethod(action = "getDeletedPeriodIdsList")
    public List<Long> getDeletedPeriodIdsList()
    {
        return FefuMobileManager.instance().dao().getDeletedEntityIdsList("period");
    }

    @WebMethod(action = "getPeriod")
    public Period getPeriod(@WebParam(name = "periodId", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") Long periodId)
    {
        List<Period> periodsList = FefuMobileManager.instance().dao().getPeriodsByIdList(Collections.singletonList(periodId));
        return (null != periodsList && !periodsList.isEmpty()) ? periodsList.get(0) : null;
    }

    @WebMethod(action = "getPeriodsByIdList")
    public List<Period> getPeriodsByIdList(@WebParam(name = "periodIdList", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") List<Long> periodIdList)
    {
        return FefuMobileManager.instance().dao().getPeriodsByIdList(periodIdList);
    }


    @WebMethod(action = "getResultsIdsList")
    public List<Long> getResultsIdsList()
    {
        return FefuMobileManager.instance().dao().getActualResultsIdsList();
    }

    @WebMethod(action = "getNewAndUpdatedResultIdsList")
    public List<Long> getNewAndUpdatedResultIdsList()
    {
        return FefuMobileManager.instance().dao().getNewAndUpdatedEntityIdsList("result");
    }

    @WebMethod(action = "getDeletedResultIdsList")
    public List<Long> getDeletedResultIdsList()
    {
        return FefuMobileManager.instance().dao().getDeletedEntityIdsList("result");
    }

    @WebMethod(action = "getResult")
    public Result getResult(@WebParam(name = "resultId", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") Long resultId)
    {
        List<Result> resultsList = FefuMobileManager.instance().dao().getResultsByIdList(Collections.singletonList(resultId));
        return (null != resultsList && !resultsList.isEmpty()) ? resultsList.get(0) : null;
    }

    @WebMethod(action = "getResultsByIdList")
    public List<Result> getResultsByIdList(@WebParam(name = "resultIdList", targetNamespace = "http://mobile.ws.unifefu.tandemservice.ru/") List<Long> resultIdList)
    {
        return FefuMobileManager.instance().dao().getResultsByIdList(resultIdList);
    }
}