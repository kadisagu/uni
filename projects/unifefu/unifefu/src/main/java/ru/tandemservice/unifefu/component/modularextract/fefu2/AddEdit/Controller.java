package ru.tandemservice.unifefu.component.modularextract.fefu2.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/20/12
 * Time: 1:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuChangePassDiplomaWorkPeriodStuExtract, IDAO, Model>
{
    public void onClickChangeYear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setYear(Integer.parseInt(DateFormatter.DATE_FORMATTER_JUST_YEAR.format(model.getExtract().getNewDate())));
    }
}

