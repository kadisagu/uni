/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import org.apache.axis.AxisFault;
import ru.tandemservice.unifefu.ws.nsi.datagram.*;

import javax.xml.rpc.ServiceException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 28.01.2015
 */
public class NsiLocalAccessServiceCustomTest
{
    public static void main(String[] args) throws Exception
    {
        try
        {
            List<Object> objects = new ArrayList<>();

            // Заведомо не обрабатываемый справочник
            EKKType ekk = new EKKType();
            objects.add(ekk);

            // Справочник по которомы мы - источник и не ожидаем входящих пакетов
            StudentType stud = new StudentType();
            //stud.setID("c6ecf056-ddf3-38bf-a371-099e7eda34d5");
            objects.add(stud);

            // Штатно обрабатываемый справочник
            HumanAcademicRankType hrank = new HumanAcademicRankType();
            hrank.setID("111a11c9-e054-3e16-80b8-b111ca65f798");
            hrank.setHumanAcademicRankDate("1999-06-10");
            hrank.setHumanAcademicDiplomaNumber("Диплом ПР № 001697 от 11.05.2003 г.");
            hrank.setHumanAcademicRankOrganization("Федеральная сслужба по надвздору в сфере образования и науки");

            HumanAcademicRankType.HumanID humanId = new HumanAcademicRankType.HumanID();
            HumanType human = new HumanType();
            human.setID("8c930c52-8b58-11e2-82ae-001b245d68a8");
            humanId.setHuman(human);
            hrank.setHumanID(humanId);

            HumanAcademicRankType.AcademicRankID rankID = new HumanAcademicRankType.AcademicRankID();
            AcademicRankType rank = new AcademicRankType();
            rank.setID("9080034e-619a-11e0-a335-001a4be8a71c");
            rankID.setAcademicRank(rank);
            hrank.setAcademicRankID(rankID);

            HumanAcademicRankType.HumanAcademicOKSOID oksoid = new HumanAcademicRankType.HumanAcademicOKSOID();
            OKSOType okso = new OKSOType();
            okso.setID("3864fad3-cdb9-3b4d-b4dc-efcf129d436c");
            oksoid.setOKSO(okso);
            hrank.setHumanAcademicOKSOID(oksoid);

            objects.add(hrank);

            NsiLocalAccessServiceTestUtil.testInsert(objects);
        } catch (Exception e)
        {
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            throw e;
        }
    }
}