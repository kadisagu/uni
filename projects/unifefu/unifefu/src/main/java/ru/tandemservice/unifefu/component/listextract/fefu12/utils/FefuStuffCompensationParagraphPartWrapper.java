/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu12.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.listextract.e34.utils.FinAidAssignParagraphPartWrapper;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public class FefuStuffCompensationParagraphPartWrapper extends FinAidAssignParagraphPartWrapper
{
	public FefuStuffCompensationParagraphPartWrapper(OrgUnit orgUnit, Group group, ListStudentExtract firstExtract)
	{
		super(orgUnit, group, firstExtract);
	}
}