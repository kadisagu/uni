/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Person.ui.IdentityCardTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardTab.PersonIdentityCardTabUI;
import ru.tandemservice.unifefu.base.bo.NSISync.ui.ChangeGuid.NSISyncChangeGuid;
import ru.tandemservice.unifefu.base.bo.NSISync.ui.ChangeGuid.NSISyncChangeGuidUI;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiDaemonDao;
import ru.tandemservice.unifefu.dao.daemon.IFefuNsiDaemonDao;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.utils.NsiIdWrapper;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 09.10.2012
 */
public class PersonIdentityCardTabExtUI extends UIAddon
{
    private StudentFefuExt _extension;
    private NsiIdWrapper _nsiIdWrapper;
    private NsiIdWrapper _icNsiIdWrapper;

    public PersonIdentityCardTabExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        List<StudentFefuExt> exts = DataAccessServices.dao().getList(StudentFefuExt.class, StudentFefuExt.student().person().id().s(), ((PersonIdentityCardTabUI) getPresenter()).getPerson().getId());
        if (!exts.isEmpty()) setExtension(exts.get(0));
        _nsiIdWrapper = new NsiIdWrapper(((PersonIdentityCardTabUI) getPresenter()).getPerson().getId());
        _icNsiIdWrapper = new NsiIdWrapper(((PersonIdentityCardTabUI) getPresenter()).getPerson().getIdentityCard().getId());
    }

    public void onClickEditGuid()
    {
        PersonIdentityCardTabUI presenter = getPresenter();
        getActivationBuilder().asRegionDialog(NSISyncChangeGuid.class)
                .parameter(NSISyncChangeGuidUI.ENTITY_ID, presenter.getPerson().getId())
                .activate();
    }

    public void onClickEditIcGuid()
    {
        PersonIdentityCardTabUI presenter = getPresenter();
        getActivationBuilder().asRegionDialog(NSISyncChangeGuid.class)
                .parameter(NSISyncChangeGuidUI.ENTITY_ID, presenter.getPerson().getIdentityCard().getId())
                .activate();
    }

    public void onClickReSync()
    {
        Long[] ids = new Long[]{((PersonIdentityCardTabUI) getPresenter()).getPerson().getId()};
        IFefuNsiDaemonDao.instance.get().doRegisterEntityAdd(ids);
        FefuNsiDaemonDao.DAEMON.wakeUpDaemon();
    }

    public StudentFefuExt getExtension()
    {
        return _extension;
    }

    public void setExtension(StudentFefuExt extension)
    {
        _extension = extension;
    }

    public NsiIdWrapper getNsiIdWrapper()
    {
        return _nsiIdWrapper;
    }

    public void setNsiIdWrapper(NsiIdWrapper nsiIdWrapper)
    {
        _nsiIdWrapper = nsiIdWrapper;
    }

    public NsiIdWrapper getIcNsiIdWrapper()
    {
        return _icNsiIdWrapper;
    }

    public void setIcNsiIdWrapper(NsiIdWrapper icNsiIdWrapper)
    {
        _icNsiIdWrapper = icNsiIdWrapper;
    }
}