package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Igor Belanov
 * @since 01.07.2016
 * (намеренно пустая миграция (миграция создавалась до выделения бранча,
 * а коммит был сделан после, поэтому код миграции перенесён в следующую версию))
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x10x3_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
    }
}
