package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x10x5_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.5"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность entrFefuForeignersQuotaBudgetExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("entrfefufrnsqtbudgextract_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_e6d6cb8b")
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("entrFefuForeignersQuotaBudgetExtract");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность entrFefuMasterForeignersQuotaBudgetExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("entrfefumstfrnsqtbudgextract_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_dee8774d")
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("entrFefuMasterForeignersQuotaBudgetExtract");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность entrFefuMasterTargetBudgetExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("entrfefumsttargetbudgextract_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_207333d4")
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("entrFefuMasterTargetBudgetExtract");

		}

		// ======================= ниже изменения внесённые в миграцию вручную =======================
		// entrantenrollmentordertype_t - таблица типов приказов,
		// enrollmentordertype_t - таблица связей типов приказов с приёмными кампаниями

		// так как нам нужны id новых приказов, а они добавятся уже после миграции, то придётся создать их вручную прямо здесь
		MigrationUtils.BatchInsertBuilder orderTypeInsertBuilder = new MigrationUtils.BatchInsertBuilder(tool.entityCodes()
				.get("entrantEnrollmentOrderType"), "code_p", "defaultpriority_p", "shorttitle_p", "targetadmission_p", "compensationtype_id", "studentcategory_id", "componentprefix_p", "title_p");

		List<Long> orderTypeIdList = new ArrayList<>();

		Long compensationTypeId = (Long)tool.getUniqueResult("select ct.id from compensationtype_t ct where code_p = ?", "1");
		Long studentCategoryId = (Long)tool.getUniqueResult("select sc.id from studentcategory_t sc where code_p = ?", "1");

		// (причём придётся прописывать хардкодом значения из data.xml)
		orderTypeIdList.add(orderTypeInsertBuilder.addRow("fefu10", 110, "Целевой бюджет (магистратура)", (byte)0, compensationTypeId, studentCategoryId, "FefuEcOrderMasterTargetBudget", "О зачислении в число магистрантов по целевому набору (бюджет)"));
		orderTypeIdList.add(orderTypeInsertBuilder.addRow("fefu11", 111, "Студенты (бюджет, по квоте для иностранцев)", (byte)0, compensationTypeId, studentCategoryId, "FefuEcOrderForeignersQuotaBudget", "О зачислении в число студентов (бюджет, по квоте для иностранцев)"));
		orderTypeIdList.add(orderTypeInsertBuilder.addRow("fefu12", 112, "Магистры (бюджет, по квоте для иностранцев)", (byte)0, compensationTypeId, studentCategoryId, "FefuEcOrderMasterForeignersQuotaBudget", "О зачислении в число магистрантов (бюджет, по квоте для иностранцев)"));

		orderTypeInsertBuilder.executeInsert(tool, "entrantenrollmentordertype_t");


		// так же нужно добавить связи приёмной кампании с новыми приказами
		MigrationUtils.BatchInsertBuilder capmaignToOrderTypeInsertBuilder = new MigrationUtils.BatchInsertBuilder(tool.entityCodes()
				.get("enrollmentOrderType"), "enrollmentcampaign_id", "entrantenrollmentordertype_id", "priority_p");

		// по дефолту сбросим все флаги
		// ! код зависит от базы (в базе все эти значения имеют тип tinyint)
		// (клиент юзает MS SQL, на MS SQL отработало)
		capmaignToOrderTypeInsertBuilder.setDefaultValue("used_p", ((byte) 0));
		capmaignToOrderTypeInsertBuilder.setDefaultValue("basic_p", ((byte) 0));
		capmaignToOrderTypeInsertBuilder.setDefaultValue("command_p", ((byte) 0));
		capmaignToOrderTypeInsertBuilder.setDefaultValue("group_p", ((byte) 0));
		capmaignToOrderTypeInsertBuilder.setDefaultValue("selectheadman_p", ((byte) 0));
		capmaignToOrderTypeInsertBuilder.setDefaultValue("reasonandbasic_p", ((byte) 0));
		capmaignToOrderTypeInsertBuilder.setDefaultValue("useenrollmentdate_p", ((byte) 0));

		// возьмём список ID всех существовавших до этого приёмных кампаний
		final List<Object[]> campaignIdList = tool.executeQuery(
				MigrationUtils.processor(1),
				"select ec.id from enrollmentcampaign_t ec"
		);

		// в таблице связей приоритеты нормализованы, поэтому для каждой приёмной кампании
		// нужно вычислить последний приоритет и инкрементировать от него

		// возьмём максимальные приоритеты (для каждой кампании)
		final List<Object[]> maxPriorityList = tool.executeQuery(
				MigrationUtils.processor(2),
				"select eot.enrollmentcampaign_id, max(eot.priority_p) from enrollmentordertype_t eot group by eot.enrollmentcampaign_id"
		);

		for (Object[] enrollmentCampaignId : campaignIdList)
		{
			Integer priority = 0;
			for (Object[] p : maxPriorityList)
			{
				// если в связях такая кампания есть, то возьмём максимальный
				// приоритет и будем инкрементировать от него
				if (enrollmentCampaignId[0].equals(p[0]))
				{
					priority = (Integer)p[1];
					break;
				}
			}
			// проставим все новые связи
			for (Long orderTypeId : orderTypeIdList)
				capmaignToOrderTypeInsertBuilder.addRow(enrollmentCampaignId[0], orderTypeId, ++priority);
		}

		capmaignToOrderTypeInsertBuilder.executeInsert(tool, "enrollmentordertype_t");
    }
}