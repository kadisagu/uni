/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSessionDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import ru.tandemservice.unisession.base.bo.SessionDocument.ui.logic.SessionDocumentDSHandler;

import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 12.05.2015
 */
@Configuration
public class FefuSessionDocumentManager extends BusinessObjectManager
{
    public static FefuSessionDocumentManager instance()
    {
        return instance(FefuSessionDocumentManager.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler fefuDocumentDS()
    {
        return new SessionDocumentDSHandler(getName())
        {
            @Override
            public Map<String, SessionDocumentWrapper> getDocumentMap()
            {
                Map<String, SessionDocumentWrapper> documentMap = super.getDocumentMap();
                documentMap.get(LIST_DOCUMENT_TAB).setTitle("Инд. ведомости");
                return documentMap;
            }
        };
    }
}
