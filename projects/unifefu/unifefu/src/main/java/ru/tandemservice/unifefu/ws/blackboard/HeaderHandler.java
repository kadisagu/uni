/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard;

import org.apache.ws.security.WSConstants;
import org.tandemframework.core.CoreDateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 * @author Nikolay Fedorovskih
 * @since 11.03.2014
 */
public class HeaderHandler implements SOAPHandler<SOAPMessageContext>
{
    private String sessionParamVal = "nosession";

    public HeaderHandler(String sessVal)
    {
        sessionParamVal = sessVal;
    }

    public boolean handleMessage(SOAPMessageContext smc)
    {

        Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        if (outboundProperty)
        {

            SOAPMessage message = smc.getMessage();

            try
            {
                SOAPHeader header = message.getSOAPPart().getEnvelope().getHeader();
                if (header == null)
                {
                    header = message.getSOAPPart().getEnvelope().addHeader();
                }

                SOAPElement security = header.addChildElement(WSConstants.WSSE_LN, WSConstants.WSSE_PREFIX, WSConstants.WSSE_NS);
                SOAPElement timeStampToken = security.addChildElement(WSConstants.TIMESTAMP_TOKEN_LN, WSConstants.WSU_PREFIX, WSConstants.WSU_NS);

                Date now = CoreDateUtils.add(new Date(), Calendar.DAY_OF_MONTH, 1);

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.add(Calendar.HOUR, 2);

                SOAPElement created = timeStampToken.addChildElement(WSConstants.CREATED_LN, WSConstants.WSU_PREFIX);
                created.addTextNode(format.format(now));

                SOAPElement Expires = timeStampToken.addChildElement(WSConstants.EXPIRES_LN, WSConstants.WSU_PREFIX);

                Expires.addTextNode(format.format(cal.getTime()));

                SOAPElement usernameToken = security.addChildElement(WSConstants.USERNAME_TOKEN_LN, WSConstants.WSSE_PREFIX);
                usernameToken.addAttribute(new QName("xmlns:wsu"), WSConstants.WSU_NS);

                SOAPElement username = usernameToken.addChildElement(WSConstants.USERNAME_LN, WSConstants.WSSE_PREFIX);
                username.addTextNode("session");

                SOAPElement password = usernameToken.addChildElement(WSConstants.PASSWORD_LN, WSConstants.WSSE_PREFIX);
                password.setAttribute(WSConstants.PASSWORD_TYPE_ATTR, WSConstants.PASSWORD_TEXT);
                password.addTextNode(sessionParamVal);

                // Print out the outbound SOAP message to System.out
                //message.writeTo(System.out);
                //System.out.println("");

            }
            catch (SOAPException e)
            {
                throw new RuntimeException(e);
                //e.printStackTrace();
            }

        }
/*
        else
        {
            try
            {

                // This handler does nothing with the response from the Web
                // Service so
                // we just print out the SOAP message.
                SOAPMessage message = smc.getMessage();
                message.writeTo(System.out);
                System.out.println("");

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }

         */
        return outboundProperty;

    }

    public Set<QName> getHeaders()
    {
        // throw new UnsupportedOperationException("Not supported yet.");

        //String s;

        return null;
    }

    public boolean handleFault(SOAPMessageContext context)
    {
        // throw new UnsupportedOperationException("Not supported yet.");
        return true;
    }

    public void close(MessageContext context)
    {
        // throw new UnsupportedOperationException("Not supported yet.");
    }
}