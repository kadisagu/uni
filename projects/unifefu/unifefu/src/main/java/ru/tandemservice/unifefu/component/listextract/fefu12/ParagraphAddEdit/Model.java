/* $Id$ */

package ru.tandemservice.unifefu.component.listextract.fefu12.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative.AbstractParagraphAddEditAlternativeModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.IExtEducationLevelModel;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;

import java.util.Date;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public class Model extends AbstractParagraphAddEditAlternativeModel<FefuStuffCompensationStuListExtract> implements IGroupModel, IExtEducationLevelModel
{
	public static final String COMPENSATION_COLUMN_NAME = "compensationSize";
	public static final String IMMEDIATE_COLUMN_NAME = "immediateSize";

    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
	private Date _matchingDate;
	private String _protocolNumber;
	private Date _protocolDate;
	private EmployeePost _responsibleForPayments;
	private EmployeePost _responsibleForAgreement;
	private EducationLevelsHighSchool _educationLevelsHighSchool;

	private ISelectModel _groupListModel;
	private ISelectModel _eduLevelHighSchoolListModel;
    private List<CompensationType> _compensationTypeList;
	private ISelectModel _employeePostModel;

    public String getCompensationSizeId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return COMPENSATION_COLUMN_NAME + "Id_" + dataSource.getCurrentEntity().getId();
    }
	public String getImmediateSizeId()
	{
		DynamicListDataSource dataSource = this.getDataSource();
		return IMMEDIATE_COLUMN_NAME + "Id_" + dataSource.getCurrentEntity().getId();
	}


    @Override
    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

	public ISelectModel getEmployeePostModel()
	{
		return _employeePostModel;
	}

	public void setEmployeePostModel(ISelectModel employeePostModel)
	{
		_employeePostModel = employeePostModel;
	}

	public Date getMatchingDate()
	{
		return _matchingDate;
	}

	public void setMatchingDate(Date matchingDate)
	{
		_matchingDate = matchingDate;
	}

	public String getProtocolNumber()
	{
		return _protocolNumber;
	}

	public void setProtocolNumber(String protocolNumber)
	{
		_protocolNumber = protocolNumber;
	}

	public Date getProtocolDate()
	{
		return _protocolDate;
	}

	public void setProtocolDate(Date protocolDate)
	{
		_protocolDate = protocolDate;
	}

	public EmployeePost getResponsibleForPayments()
	{
		return _responsibleForPayments;
	}

	public void setResponsibleForPayments(EmployeePost responsibleForPayments)
	{
		_responsibleForPayments = responsibleForPayments;
	}

	public EmployeePost getResponsibleForAgreement()
	{
		return _responsibleForAgreement;
	}

	public void setResponsibleForAgreement(EmployeePost responsibleForAgreement)
	{
		_responsibleForAgreement = responsibleForAgreement;
	}

	public EducationLevelsHighSchool getEducationLevelsHighSchool()
	{
		return _educationLevelsHighSchool;
	}

	public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
	{
		_educationLevelsHighSchool = educationLevelsHighSchool;
	}

	public ISelectModel getEduLevelHighSchoolListModel()
	{
		return _eduLevelHighSchoolListModel;
	}

	public void setEduLevelHighSchoolListModel(ISelectModel eduLevelHighSchoolListModel)
	{
		_eduLevelHighSchoolListModel = eduLevelHighSchoolListModel;
	}

	@Override
	public EducationLevels getParentEduLevel()
	{
		return getEducationLevelsHighSchool() != null ? getEducationLevelsHighSchool().getEducationLevel() : null;
	}
}
