/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.listextract.e34;

import org.joda.time.DateTime;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.movestudent.entity.FinAidAssignStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;

import java.util.Date;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 03.10.2012
 */
public class FinAidAssignStuListExtractDao extends ru.tandemservice.movestudent.component.listextract.e34.FinAidAssignStuListExtractDao
{
    @Override
    public void doCommit(FinAidAssignStuListExtract extract, Map parameters)
    {
        super.doCommit(extract, parameters);

        //выплата
        StudentListOrder listOrder = (StudentListOrder) extract.getParagraph().getOrder();

        FefuStudentPayment payment = new FefuStudentPayment();
        payment.setExtract(extract);

        int year = extract.getYear();
        int month = extract.getMonth();
        DateTime date = new DateTime().withMonthOfYear(month).withYear(year);
        Date firstDate = date.dayOfMonth().withMinimumValue().toDate();
        Date lastDate = date.dayOfMonth().withMaximumValue().toDate();

        payment.setStartDate(firstDate);
        payment.setStopDate(lastDate);
        payment.setPaymentSum(extract.getFinAidSize());
        if(null != listOrder.getReason())
            payment.setReason(listOrder.getReason().getTitle());
        save(payment);
    }

    @Override
    public void doRollback(FinAidAssignStuListExtract extract, Map parameters)
    {
        super.doRollback(extract, parameters);

        // Удаляем выплату
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuStudentPayment.class);
        deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(FefuStudentPayment.extract()), DQLExpressions.value(extract)));
        deleteBuilder.createStatement(getSession()).execute();
    }
}
