package ru.tandemservice.unifefu.component.report.FefuEntrantIncomingList.Add;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.DiplomaQualifications;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.component.report.FefuReportUtil;

import java.util.*;

/**
 * @author vip_delete
 * @since 03.07.2009
 */
@SuppressWarnings("deprecation")
public class FefuEntrantIncomingListReportBuilder
{
    private Model _model;
    private Session _session;

    private boolean isBudget;

    private int numColumn = 9;
    private int num = 0;
    private int numReg = 1;
    private int numFio = 2;
    private int numEdu = 3;
    private int numSpec = 4;
    private int numOrig = 5;
    private int numYearEdu = 6;
    private int numDate = 7;
    private int numPhone = 8;

    public FefuEntrantIncomingListReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        DateFormatter DATE_FORMATTER = new DateFormatter("dd.MM.yy");
        isBudget = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_model.getReport().getCompensationType().getCode());
        MQBuilder builder = getRequestedEnrollmentDirectionsBuilder();
        List<RequestedEnrollmentDirection> directionList = builder.getResultList(_session);
        if (directionList.size() == 0)
            throw new ApplicationException("Нет данных для построения отчета.");
        Map<Entrant, List<RequestedEnrollmentDirection>> entrantListMap = new HashMap<>();
        Map<String, Map<EducationLevelsHighSchool, List<RequestedEnrollmentDirection>>> ouMap = new HashMap<>();
        for (RequestedEnrollmentDirection direction : directionList) {
            EducationOrgUnit educationOrgUnit = direction.getEnrollmentDirection().getEducationOrgUnit();
            String titleOu = educationOrgUnit.getFormativeOrgUnit().getPrintTitle() + " (" + educationOrgUnit.getTerritorialOrgUnit().getFullTitle() + ")";
            SafeMap.safeGet(SafeMap.safeGet(ouMap, titleOu, HashMap.class), direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool(), ArrayList.class).add(direction);
            SafeMap.safeGet(entrantListMap, direction.getEntrantRequest().getEntrant(), ArrayList.class).add(direction);
        }

        List<String> formativeOuList = new ArrayList<>(ouMap.keySet());
        Collections.sort(formativeOuList);
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniFefuDefines.TEMPLATE_ENTRANT_INCOMING_LIST);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument result = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfSearchResult searchResultTitle = UniRtfUtil.findRtfMark(template, "titleOu");
        RtfElement emptyTitle = (RtfElement) searchResultTitle.getElementList().get(searchResultTitle.getIndex()).getClone();
        RtfSearchResult searchResultT = UniRtfUtil.findRtfTableMark(template, "T");
        RtfTable emptyTableT = (RtfTable) searchResultT.getElementList().get(searchResultT.getIndex()).getClone();

        // список строк
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        int rowIndex = 0;
        for (String ouTitle : formativeOuList) {
            Map<EducationLevelsHighSchool, List<RequestedEnrollmentDirection>> hsMap = ouMap.get(ouTitle);
            List<EducationLevelsHighSchool> hsList = new ArrayList<>(hsMap.keySet());
            Collections.sort(hsList, ITitled.TITLED_COMPARATOR);
            int indexOu = 0;
            for (EducationLevelsHighSchool hs : hsList) {
                final List<String[]> rows = new ArrayList<>();
                int indexRequest = 1;
                List<RequestedEnrollmentDirection> directions = hsMap.get(hs);
                Collections.sort(directions, ENTRANT_FIO_COMPARATOR);
                if (rowIndex > 0) {
                    FefuReportUtil.insertPageBreak(result.getElementList());
                    if (indexOu == 0) {
                        result.getElementList().add(emptyTitle.getClone());
                        IRtfElementFactory elementFactory = RtfBean.getElementFactory();
                        result.getElementList().add(elementFactory.createRtfControl(IRtfData.PAR));
                    }
                    result.getElementList().add(emptyTableT.getClone());
                }
                injectModifier.put("titleOu", ouTitle);
                injectModifier.modify(result);
                int plan = 0;
                for (RequestedEnrollmentDirection direction : directions)
                {
                    String[] data = new String[numColumn];
                    Entrant entrant = direction.getEntrantRequest().getEntrant();
                    Person person = entrant.getPerson();
                    PersonEduInstitution lastEduInstitution = person.getPersonEduInstitution();
                    DiplomaQualifications qual = lastEduInstitution != null ? lastEduInstitution.getDiplomaQualification() : null;
                    String strPhones = person.getContactData().getMainPhones();
                    data[num] = Integer.toString(indexRequest++);
                    data[numReg] = entrant.getPersonalNumber();
                    data[numFio] = person.getFullFio();
                    data[numEdu] = lastEduInstitution != null && lastEduInstitution.getEduInstitution() != null ? lastEduInstitution.getEduInstitution().getTitle() : " ";
                    data[numSpec] = qual != null ? qual.getTitle() : " ";
                    data[numOrig] = direction.isOriginalDocumentHandedIn() ? "п" : "к";
                    data[numYearEdu] = lastEduInstitution != null ? String.valueOf(lastEduInstitution.getYearEnd()) : " ";
                    data[numDate] = person.getIdentityCard().getBirthDate() == null ? "" : DATE_FORMATTER.format(person.getIdentityCard().getBirthDate());
                    data[numPhone] = strPhones;
                    rows.add(data);
                    if (isBudget)
                        plan = FefuReportUtil.getBudgetPlanWithoutTarget(direction.getEnrollmentDirection());
                    else
                        plan = FefuReportUtil.getContractPlanWithoutTarget(direction.getEnrollmentDirection());
                }
                rowIndex++;
                RtfTableModifier tableModifier = new RtfTableModifier();

                tableModifier.modify(result);
                String[] hsData = new String[2];
                hsData[0] = hs.getTitle();
                hsData[1] = String.valueOf(plan);
                tableModifier.put("T1", new String[][]{hsData});
                tableModifier.put("T1", new RtfRowIntercepterBase() {
                    @Override
                    public void beforeModify(RtfTable table, int currentRowIndex) {
                        RtfString string = new RtfString();
                        if (isBudget)
                            string.append("бюд");
                        else
                            string.append("дог");
                        RtfCell cell = table.getRowList().get(currentRowIndex - 1).getCellList().get(1);
                        cell.setElementList(string.toList());
                        SharedRtfUtil.setCellAlignment(cell, IRtfData.I);
                        SharedRtfUtil.setCellAlignment(cell, IRtfData.QC);
                    }
                });
                tableModifier.put("T", rows.toArray(new String[][]{}));
                tableModifier.put("T", new RtfRowIntercepterBase() {
                    @Override
                    public void beforeModify(RtfTable table, int currentRowIndex) {
                            List<RtfCell> cellList = table.getRowList().get(currentRowIndex).getCellList();
                        SharedRtfUtil.setCellAlignment(cellList.get(numFio), IRtfData.QC, IRtfData.QL);
                        SharedRtfUtil.setCellAlignment(cellList.get(numEdu), IRtfData.QL, IRtfData.QC);
                    }
                });

                tableModifier.modify(result);
                rowIndex++;
                indexOu++;
            }
        }
        String sumAll = String.valueOf(entrantListMap.size());
        injectModifier.put("sumAll", sumAll);
        injectModifier.modify(result);
        return RtfUtil.toByteArray(result);
    }

    private MQBuilder getRequestedEnrollmentDirectionsBuilder()
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "req");
        builder.addJoinFetch("req", RequestedEnrollmentDirection.enrollmentDirection().s(), "ed");
        builder.addJoinFetch("req", RequestedEnrollmentDirection.entrantRequest().s(), "request");
        builder.addJoinFetch("request", EntrantRequest.entrant().s(), "entrant");
        builder.addJoinFetch("entrant", Entrant.person().s(), "person");
        builder.addJoinFetch("person", Person.identityCard().s(), "card");
        builder.addJoin("ed", EnrollmentDirection.educationOrgUnit().s(), "ou");

        builder.add(MQExpression.eq("entrant", Entrant.archival().s(), Boolean.FALSE));
        builder.add(MQExpression.notEq("req", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(MQExpression.eq("request", EntrantRequest.entrant().enrollmentCampaign().s(), _model.getReport().getEnrollmentCampaign()));
        builder.add(UniMQExpression.betweenDate("request", EntrantRequest.regDate().s(), _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.eq("req", RequestedEnrollmentDirection.compensationType().s(), _model.getReport().getCompensationType()));

        if (_model.getReport().getEnrollmentCampaign().isExamSetDiff())
        {
            builder.add(MQExpression.eq("req", RequestedEnrollmentDirection.studentCategory().s(), _model.getStudentCategory()));
        } else if (_model.isStudentCategoryActive())
        {
            builder.add(MQExpression.in("req", RequestedEnrollmentDirection.studentCategory().s(), _model.getStudentCategoryList()));
        }

        if (_model.isQualificationActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), _model.getQualificationList()));
        }

        if (_model.isFormativeOrgUnitActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.formativeOrgUnit().s(), _model.getFormativeOrgUnitList()));
        }

        if (_model.isTerritorialOrgUnitActive())
        {
            builder.add(_model.getTerritorialOrgUnitList().isEmpty() ? MQExpression.isNull("ou", EducationOrgUnit.territorialOrgUnit().s()) : MQExpression.in("ou", EducationOrgUnit.territorialOrgUnit().s(), _model.getTerritorialOrgUnitList()));
        }

        if (_model.isEducationLevelHighSchoolActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.educationLevelHighSchool().s(), _model.getEducationLevelHighSchoolList()));
        }

        if (_model.isDevelopFormActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.developForm().s(), _model.getDevelopFormList()));
        }

        if (_model.isDevelopConditionActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.developCondition().s(), _model.getDevelopConditionList()));
        }

        if (_model.isDevelopTechActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.developTech().s(), _model.getDevelopTechList()));
        }

        if (_model.isDevelopPeriodActive())
        {
            builder.add(MQExpression.in("ou", EducationOrgUnit.developPeriod().s(), _model.getDevelopPeriodList()));
        }
        if (!_model.isIncludeForeignPerson())
        {
            builder.add(MQExpression.eq("card", IdentityCard.citizenship().code(), IKladrDefines.RUSSIA_COUNTRY_CODE));
        }
        return builder;
    }

//    private List<Discipline2RealizationWayRelation> getDisciplineList()
//    {
//        EnrollmentCampaign enrollmentCampaign = _model.getReport().getEnrollmentCampaign();
//
//        List<Group2DisciplineRelation> relList = new MQBuilder(Group2DisciplineRelation.ENTITY_CLASS, "r").add(MQExpression.eq("r", Group2DisciplineRelation.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign)).getResultList(_session);
//
//        Map<Long, Set<Discipline2RealizationWayRelation>> groupId2disciplineSet = new HashMap<>();
//        for (Group2DisciplineRelation rel : relList)
//        {
//            Set<Discipline2RealizationWayRelation> set = groupId2disciplineSet.get(rel.getGroup().getId());
//            if (set == null)
//                groupId2disciplineSet.put(rel.getGroup().getId(), set = new HashSet<>());
//            set.add(rel.getDiscipline());
//        }
//
//        List<EnrollmentDirection> enrollmentDirectionList = MultiEnrollmentDirectionUtil.getEnrollmentDirections(_model, _session);
//
//        StudentCategory studentCategory = enrollmentCampaign.isExamSetDiff() ? _model.getStudentCategory() : UniDaoFacade.getCoreDao().getCatalogItem(StudentCategory.class, UniDefines.STUDENT_CATEGORY_STUDENT);
//        Map<String, ExamSet> map = UniecDAOFacade.getExamSetDAO().getExamSetMap(enrollmentCampaign);
//
//        Set<Discipline2RealizationWayRelation> disciplineSet = new HashSet<>();
//
//        for (EnrollmentDirection direction : enrollmentDirectionList)
//        {
//            for (ExamSet examSet : map.values())
//            {
//                if (studentCategory.equals(examSet.getStudentCategory()) && examSet.getList().contains(direction))
//                {
//                    for (ExamSetItem examSetItem : examSet.getSetItemList())
//                    {
//                        if (examSetItem.getSubject() instanceof Discipline2RealizationWayRelation)
//                            disciplineSet.add((Discipline2RealizationWayRelation) examSetItem.getSubject());
//                        else
//                            disciplineSet.addAll(groupId2disciplineSet.get(examSetItem.getSubject().getId()));
//
//                        for (SetDiscipline choseItem : examSetItem.getChoice())
//                        {
//                            if (choseItem instanceof Discipline2RealizationWayRelation)
//                                disciplineSet.add((Discipline2RealizationWayRelation) choseItem);
//                            else
//                                disciplineSet.addAll(groupId2disciplineSet.get(choseItem.getId()));
//                        }
//                    }
//                }
//            }
//        }
//
//        CompensationType budget = UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET);
//        CompensationType contract = UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT);
//
//        // аналог UniecDAOFacade.getEntrantDAO().getRealizationFormSet, только с разделением на формы сдачи
//        boolean compensationTypeDiff = enrollmentCampaign.isEnrollmentPerCompTypeDiff();
//        MQBuilder builder = new MQBuilder(Discipline2RealizationFormRelation.ENTITY_CLASS, "r");
//        builder.add(MQExpression.eq("r", Discipline2RealizationFormRelation.L_DISCIPLINE + "." + Discipline2RealizationWayRelation.L_ENROLLMENT_CAMPAIGN, enrollmentCampaign));
//        builder.add(MQExpression.eq("r", Discipline2RealizationFormRelation.L_SUBJECT_PASS_FORM + "." + SubjectPassForm.P_INTERNAL, Boolean.TRUE));
//        if (!compensationTypeDiff)
//            builder.add(MQExpression.eq("r", Discipline2RealizationFormRelation.L_TYPE, budget));
//        List<Discipline2RealizationFormRelation> list = builder.getResultList(_session);
//
//        Set<MultiKey> realisationFormSet = new HashSet<>();
//        if (compensationTypeDiff)
//        {
//            for (Discipline2RealizationFormRelation item : list)
//                realisationFormSet.add(new MultiKey(item.getDiscipline(), item.getType()));
//        } else
//        {
//            for (Discipline2RealizationFormRelation item : list)
//                realisationFormSet.add(new MultiKey(new Object[]{item.getDiscipline()}));
//        }
//
//        List<Discipline2RealizationWayRelation> result = new ArrayList<>();
//
//        // берем только такие дисциплины, которые onlyStateExamInternal
//        for (Discipline2RealizationWayRelation discipline : disciplineSet)
//        {
//            if (compensationTypeDiff)
//            {
//                MultiKey key1 = new MultiKey(discipline, budget);
//                MultiKey key2 = new MultiKey(discipline, contract);
//                if (realisationFormSet.contains(key1) || realisationFormSet.contains(key2))
//                    result.add(discipline);
//            } else
//            {
//                MultiKey key = new MultiKey(new Object[]{discipline});
//                if (realisationFormSet.contains(key))
//                    result.add(discipline);
//            }
//        }
//
//        Collections.sort(result, ITitled.TITLED_COMPARATOR);
//        return result;
//    }

    private static final Comparator<RequestedEnrollmentDirection> ENTRANT_FIO_COMPARATOR = CommonCollator.comparing(red -> red.getEntrantRequest().getEntrant().getFullFio(), true);
}
