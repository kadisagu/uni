/* $Id$ */
package ru.tandemservice.unifefu.base.ext.UniStudent.logic.list;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniStudent.logic.list.StudentSearchListDSHandler;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.base.ext.UniStudent.UniStudentExtManager;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 22.10.2013
 */
public class FefuStudentSearchListDSHandler extends StudentSearchListDSHandler
{
    public FefuStudentSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static void addFefuFilters(DQLSelectBuilder builder, String alias, ExecutionContext context)
    {
        Collection<DevelopForm> developForms = context.get(UniStudentExtManager.PROP_DEVELOP_FORM);
        if (developForms != null && !developForms.isEmpty())
        {
            builder.where(DQLExpressions.in(DQLExpressions.property(alias, Student.educationOrgUnit().developForm()), developForms));
        }

        // базовое образование
        DataWrapper baseEdu = context.get(UniStudentExtManager.PROP_BASE_EDU);
        //Признаки обучения в УВЦ и ФВО
        DataWrapper isLearnUVC = context.get(FefuStudentManager.FEFU_LEARN_UVC);
        DataWrapper isLearnFVO = context.get(FefuStudentManager.FEFU_LEARN_FVO);

        if (isLearnFVO != null || isLearnUVC != null || baseEdu != null)
            builder.joinEntity(alias, DQLJoinType.left, StudentFefuExt.class, "ext", eq(property(alias), property("ext", StudentFefuExt.student())));

        if (null != isLearnUVC)
            builder.where(eq(property("ext", StudentFefuExt.uvcStudent()), value(isLearnUVC.getId().equals(FefuStudentManager.FEFU_IS_LEARNING_YES))));

        if (null != isLearnFVO)
            builder.where(eq(property("ext", StudentFefuExt.fvoStudent()), value(isLearnFVO.getId().equals(FefuStudentManager.FEFU_IS_LEARNING_YES))));

        if (baseEdu != null)
        {
            builder.where(eq(
                    property(StudentFefuExt.baseEdu().fromAlias("ext")),
                    value(baseEdu.getId())
            ));
        }
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);
        addFefuFilters(builder, alias, context);
    }
}