package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 24.01.2014
 */
public class MS_unifefu_2x4x1_15to16 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select ID, ORDER_ID, DIRECTUMNUMBER_P, DIRECTUMDATE_P " +
                "from FEFUSTUDENTORDEREXTENSION_T where ORDER_ID is not null and ORDER_ID in" +
                "(select ORDER_ID from FEFUSTUDENTORDEREXTENSION_T group by ORDER_ID having COUNT (ID) > 1)" +
                " order by ORDER_ID");
        ResultSet rs = stmt.getResultSet();

        Map<Long, Long> orderToActualExtIdMap = new HashMap<>();
        Map<Long, List<Long>> allTheOrdersList = new HashMap<>();

        while (rs.next())
        {
            Long id = rs.getLong(1);
            Long orderId = rs.getLong(2);
            String directumNumber = rs.getString(3);
            Date directumDate = rs.getDate(4);

            if ((null != directumNumber && null != directumDate) || !orderToActualExtIdMap.containsKey(orderId))
                orderToActualExtIdMap.put(orderId, id);

            List<Long> extList = allTheOrdersList.get(orderId);
            if (null == extList) extList = new ArrayList<>();
            extList.add(id);
            allTheOrdersList.put(orderId, extList);
        }

        PreparedStatement update = tool.prepareStatement("update FEFUDIRECTUMLOG_T set ORDEREXT_ID=? where ORDEREXT_ID=?");
        PreparedStatement delete = tool.prepareStatement("delete from FEFUSTUDENTORDEREXTENSION_T where id=?");

        for (Map.Entry<Long, List<Long>> entry : allTheOrdersList.entrySet())
        {
            Long orderId = entry.getKey();

            if (orderToActualExtIdMap.containsKey(orderId))
            {
                Long extId = orderToActualExtIdMap.get(orderId);
                for (Long extItemId : entry.getValue())
                {
                    if (!extItemId.equals(extId))
                    {
                        update.setLong(1, extId);
                        update.setLong(2, extItemId);
                        update.executeUpdate();

                        delete.setLong(1, extItemId);
                        delete.executeUpdate();
                    }
                }
            }
        }

        update.close();
        delete.close();
        stmt.close();
    }
}