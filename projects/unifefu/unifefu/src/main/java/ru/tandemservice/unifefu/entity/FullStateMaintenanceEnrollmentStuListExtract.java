package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAssignPaymentExtract;
import ru.tandemservice.unifefu.entity.gen.FullStateMaintenanceEnrollmentStuListExtractGen;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Проект приказа «О зачислении на полное государственное обеспечение»
 */
public class FullStateMaintenanceEnrollmentStuListExtract extends FullStateMaintenanceEnrollmentStuListExtractGen implements IAssignPaymentExtract
{
    @Override
    public Date getPaymentBeginDate()
    {
        return getStartPaymentPeriodDate();
    }

    @Override
    public BigDecimal getPaymentAmount()
    {
        // Сумма обязательна, но брать неоткуда. Пока передаем ноль.
        return new BigDecimal(0);
    }
}