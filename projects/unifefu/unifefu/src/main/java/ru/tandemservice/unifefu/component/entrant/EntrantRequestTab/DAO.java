/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.EntrantRequestTab;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.FefuEntrantRequest2TechnicalCommissionRelation;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 6/10/13
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantRequestTab.DAO
{
    @Override
    protected List<EntrantRequest> getEntrantRequestList(Entrant entrant)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "e")
                .column("e")
                .where(eq(
                        property(EntrantRequest.entrant().fromAlias("e")),
                        value(entrant)
                ))
                .order(property(EntrantRequest.regDate().fromAlias("e")));

        List<Long> tcLimitList = UnifefuDaoFacade.getFefuEntrantDAO().getAccessLimitationTCList(entrant.getEnrollmentCampaign());
        if (tcLimitList.size() > 0)
        {
            dql.joinEntity("e", DQLJoinType.inner, FefuEntrantRequest2TechnicalCommissionRelation.class, "rel",
                           eq(
                                   property(FefuEntrantRequest2TechnicalCommissionRelation.entrantRequest().fromAlias("rel")),
                                   property(EntrantRequest.id().fromAlias("e"))
                           )
            );

            dql.where(
                    in(
                            property(FefuEntrantRequest2TechnicalCommissionRelation.technicalCommission().id().fromAlias("rel")),
                            tcLimitList
                    )
            );
        }

        List<EntrantRequest> entrantRequestList = dql.createStatement(getSession()).list();

        Collections.sort(entrantRequestList, new Comparator<EntrantRequest>()
        {
            @Override
            public int compare(EntrantRequest o1, EntrantRequest o2)
            {
                if (!o1.isTakeAwayDocument() && o2.isTakeAwayDocument()) return -1;
                else if (o1.isTakeAwayDocument() && !o2.isTakeAwayDocument()) return 1;
                else if (o1.getRegDate().after(o2.getRegDate())) return -1;
                else if (o1.getRegDate().before(o2.getRegDate())) return 1;
                else return 0;
            }
        });

        return entrantRequestList;
    }

    private void checkAccess(Long requestId)
    {
        UnifefuDaoFacade.getFefuEntrantDAO().checkAccessForTCEmployee((EntrantRequest)proxy(requestId));
    }

    @Override
    public void updateDocumentsTakeAway(Long requestId)
    {
        checkAccess(requestId);
        super.updateDocumentsTakeAway(requestId);
    }

    @Override
    public void updateDocumentsReturn(Long requestId)
    {
        checkAccess(requestId);
        super.updateDocumentsReturn(requestId);
    }

    @Override
    public void executeIncludeIntoExamGroup(Long entrantRequestId)
    {
        checkAccess(entrantRequestId);
        super.executeIncludeIntoExamGroup(entrantRequestId);
    }
}
