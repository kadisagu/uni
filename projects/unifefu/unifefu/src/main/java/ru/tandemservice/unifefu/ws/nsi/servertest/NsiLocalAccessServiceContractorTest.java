/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import org.apache.axis.AxisFault;
import ru.tandemservice.unifefu.ws.nsi.datagram.ContractorType;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanType;

import javax.xml.rpc.ServiceException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceContractorTest
{
    public static void main(String[] args) throws Exception
    {
        try
        {
            //retrieveWholeCatalog();
            testInsertNewAnalog();
        } catch (Exception e)
        {
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            throw e;
        }
    }

    private static void retrieveWholeCatalog() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        objects.add(NsiLocalAccessServiceTestUtil.FACTORY.createContractorType());
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void testInsertNewAnalog() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        ContractorType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createContractorType();
        nsientity.setID("fc8ce410-19fb-4800-853e-757575757571");
        ContractorType.HumanID humanId = NsiLocalAccessServiceTestUtil.FACTORY.createContractorTypeHumanID();
        HumanType human = NsiLocalAccessServiceTestUtil.FACTORY.createHumanType();
        human.setID("fc8ce410-19fb-4800-853e-757575757573");
        humanId.setHuman(human);
        nsientity.setHumanID(humanId);
        nsientity.setContractorKPP("111114111");
        nsientity.setContractorNameFull("BlaBlaBla");
        objects.add(nsientity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }
}