/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu11.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 19.11.2013
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuCompensationTypeTransferStuListExtract, Model>
{
}
