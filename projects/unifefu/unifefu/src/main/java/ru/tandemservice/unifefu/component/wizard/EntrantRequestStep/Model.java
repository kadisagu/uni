/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.EntrantRequestStep;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

/**
 * @author Nikolay Fedorovskih
 * @since 04.06.2013
 */
public class Model extends ru.tandemservice.uniec.component.wizard.EntrantRequestStep.Model
{
    private boolean onlineEntrantSign;
    private FefuTechnicalCommission technicalCommission;
    private ISelectModel technicalCommissionsSelectModel;
    private FefuTechnicalCommission oldTechnicalCommission;

    public boolean getOnlineEntrantSign()
    {
        return onlineEntrantSign;
    }

    public void setOnlineEntrantSign(boolean isOnlineEntrant)
    {
        onlineEntrantSign = isOnlineEntrant;
    }

    public FefuTechnicalCommission getTechnicalCommission()
    {
        return technicalCommission;
    }

    public void setTechnicalCommission(FefuTechnicalCommission technicalCommission)
    {
        this.technicalCommission = technicalCommission;
    }

    public ISelectModel getTechnicalCommissionsSelectModel()
    {
        return technicalCommissionsSelectModel;
    }

    public void setTechnicalCommissionsSelectModel(ISelectModel technicalCommissionsSelectModel)
    {
        this.technicalCommissionsSelectModel = technicalCommissionsSelectModel;
    }

    public FefuTechnicalCommission getOldTechnicalCommission()
    {
        return oldTechnicalCommission;
    }

    public void setOldTechnicalCommission(FefuTechnicalCommission oldTechnicalCommission)
    {
        this.oldTechnicalCommission = oldTechnicalCommission;
    }
}