/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduplanVersionImtsaImportLogTab;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.FefuImtsaImportLog;

import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 05.09.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        List<EppEduPlanVersionBlock> blocks = new ArrayList<>();
        Map<EppEduPlanVersionBlock, String[]> logMap = new HashMap<>();
        List<FefuImtsaImportLog> logList = getList(FefuImtsaImportLog.class, FefuImtsaImportLog.block().eduPlanVersion().id(), model.getId());
        for (FefuImtsaImportLog log: logList)
        {
            blocks.add(log.getBlock());
            logMap.put(log.getBlock(), StringUtils.split(log.getMessage(), "\n"));
        }

        Collections.sort(blocks, EppEduPlanVersionBlock.COMPARATOR);

        model.setLogMap(logMap);
        model.setBlocks(blocks);
    }
}