/**
 * ServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.nsi.servertest;

public interface ServiceSoap extends java.rmi.Remote {
    public ServiceResponseType2 commit(CommitRequestType parameters) throws java.rmi.RemoteException;
    public ServiceResponseType2 route(RouteRequestType parameters) throws java.rmi.RemoteException;
    public ServiceResponseType insert(ServiceRequestType parameters) throws java.rmi.RemoteException;
    public ServiceResponseType delete(ServiceRequestType parameters) throws java.rmi.RemoteException;
    public ServiceResponseType2 deliver(DeliverRequestType parameters) throws java.rmi.RemoteException;
    public ServiceResponseType initialize(ServiceRequestType parameters) throws java.rmi.RemoteException;
    public ServiceResponseType update(ServiceRequestType parameters) throws java.rmi.RemoteException;
    public ServiceResponseType retrieve(ServiceRequestType parameters) throws java.rmi.RemoteException;
}
