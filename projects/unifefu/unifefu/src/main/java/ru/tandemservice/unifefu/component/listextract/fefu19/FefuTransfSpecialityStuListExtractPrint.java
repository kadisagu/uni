/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.entity.gen.StudentFefuExtGen;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 15.05.2015
 */
public class FefuTransfSpecialityStuListExtractPrint implements IPrintFormCreator<FefuTransfSpecialityStuListExtract>, IListParagraphPrintFormCreator<FefuTransfSpecialityStuListExtract>, IStudentListParagraphPrintFormatter
{
    public static final List<String> PROCESSED_QUALIFICATION_CODES = Arrays.asList(QualificationsCodes.BAKALAVR, QualificationsCodes.MAGISTR, QualificationsCodes.SPETSIALIST);
    public static final Map<String, String[]> PROFILE_STRING_CASES_ARRAY = new HashMap<>();
    public static final Map<String, String[]> PROFILE_STRING_PLURAL_CASES_ARRAY = new HashMap<>();

    static
    {
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.BAKALAVR, CommonExtractPrint.PROFILE_CASES);
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.MAGISTR, CommonExtractPrint.MASTER_PROGRAM_CASES);
        PROFILE_STRING_CASES_ARRAY.put(QualificationsCodes.SPETSIALIST, CommonExtractPrint.SPECIALIZATION_CASES);
        PROFILE_STRING_CASES_ARRAY.put(null, UniRtfUtil.EDU_DIRECTION_CASES);

        PROFILE_STRING_PLURAL_CASES_ARRAY.put(QualificationsCodes.BAKALAVR, CommonExtractPrint.PROFILE_PLURAL_CASES);
        PROFILE_STRING_PLURAL_CASES_ARRAY.put(QualificationsCodes.MAGISTR, CommonExtractPrint.MASTER_PROGRAM_PLURAL_CASES);
        PROFILE_STRING_PLURAL_CASES_ARRAY.put(QualificationsCodes.SPETSIALIST, CommonExtractPrint.SPECIALIZATION_PLURAL_CASES);
        PROFILE_STRING_PLURAL_CASES_ARRAY.put(null, UniRtfUtil.EDU_DIRECTION_CASES);
    }


    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuTransfSpecialityStuListExtract firstExtract)
    {
        CommonExtractPrint.initOrgUnit(modifier, firstExtract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");

        modifier.put("course", firstExtract.getEntity().getCourse().getTitle());
        modifier.put("group", firstExtract.getGroupOld().getTitle());

        CommonListExtractPrint.injectCompensationType(modifier, firstExtract.getEntity().getCompensationType(), "", firstExtract.getEntity().isTargetAdmission());
        UniRtfUtil.initEducationType(modifier, firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool(), "");
        modifier.put("educationOrgUnit", firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool().getPrintTitle());
        CommonExtractPrint.initDevelopForm(modifier, firstExtract.getEducationOrgUnitOld(), "");

    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuTransfSpecialityStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);

        CommonExtractPrint.initOrgUnit(injectModifier, firstExtract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");
        CommonExtractPrint.initFefuGroup(injectModifier, "groupInternal_G", firstExtract.getGroupOld(), firstExtract.getEducationOrgUnitOld().getDevelopForm(), " группы ");
        CommonExtractPrint.initFefuGroup(injectModifier, "groupExternal", firstExtract.getGroupNew(), firstExtract.getEducationOrgUnitNew().getDevelopForm(), " в группу ");

        EducationLevels specialityOld = firstExtract.getEducationOrgUnitOld().getEducationLevelHighSchool().getEducationLevel();
        CommonExtractPrint.modifyEducationStr(injectModifier, specialityOld, new String[]{"fefuEducationStrDirectionOld"}, true);

        EducationLevels specialityNew = EducationOrgUnitUtil.getParentLevel(firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool());
        CommonExtractPrint.modifyEducationStr(injectModifier, specialityNew, new String[]{"fefuEducationStrDirectionNew"}, true);

        boolean isNeedDevCondition = !firstExtract.getEducationOrgUnitOld().getDevelopCondition().getCode().equals(DevelopConditionCodes.FULL_PERIOD);

        StringBuilder developCondition = new StringBuilder(" по ").append(CommonExtractPrint.getDevelopConditionStr_G(firstExtract.getEducationOrgUnitOld().getDevelopCondition()))
                .append(" (").append(firstExtract.getEducationOrgUnitOld().getDevelopPeriod().getTitle()).append(") основной образовательной программе");
        injectModifier.put("fefuDevelopCondition", isNeedDevCondition? developCondition.toString():"");

        injectModifier.put("techRemote",
                                    DevelopTechCodes.DISTANCE.equals(firstExtract.getEducationOrgUnitOld().getDevelopTech().getCode())? CommonExtractPrint.WITH_REMOTE_EDU_TECH : "");

        //базовое образование
        StudentFefuExt studentExt = DataAccessServices.dao().getByNaturalId(new StudentFefuExtGen.NaturalId(firstExtract.getEntity()));
        String eduLevel = (studentExt != null && studentExt.getBaseEdu()!= null) ? (" на базе " + (studentExt.getBaseEdu().equals(FefuStudentManager.BASE_VPO) ? "высшего образования" : "среднего профессионального образования")):"";
        injectModifier.put("fefuBaseEduLevel", isNeedDevCondition? eduLevel:"");


        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuTransfSpecialityStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, FefuTransfSpecialityStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\\par ").append(extractNumber).append(".  ");
        buffer.append(student.getPerson().getFullFio());
        return buffer.toString();
    }
}