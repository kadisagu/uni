/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu19;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 17.11.2014
 */
public class FefuTransfAcceleratedTimeStuExtractDAO extends UniBaseDao implements IExtractComponentDao<FefuTransfAcceleratedTimeStuExtract>
{
    @Override
    public void doCommit(FefuTransfAcceleratedTimeStuExtract extract, Map parameters)
    {
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);
        MoveStudentDaoFacade.getCommonExtractUtil().doCommitWithoutChangeStatus(extract);

        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitNew());
        extract.getEntity().setGroup(extract.getGroupNew());


        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getTransferOrderDate());
            extract.setPrevOrderNumber(orderData.getTransferOrderNumber());
        }
        orderData.setTransferOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setTransferOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(FefuTransfAcceleratedTimeStuExtract extract, Map parameters)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().doRollback(extract);

        // возвращаем предыдущие номер и дату приказа
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitOld());
        extract.getEntity().setGroup(extract.getGroupOld());

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setTransferOrderDate(extract.getPrevOrderDate());
        orderData.setTransferOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}