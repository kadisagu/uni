/* $Id $ */
package ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTabPrintRtf;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideOperation;
import ru.tandemservice.unisession.entity.document.SessionTransferOperation;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Rostuncev Savva
 * @since 20.03.2014
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        SessionTransferDocument sessionTransferDocument = get(SessionTransferDocument.class, model.getSessionTransferDocument().getId());
        model.setSessionTransferDocument(sessionTransferDocument);
    }

    @SuppressWarnings("unchecked")
    public String[][] getSessionTransferDocument(Model model)
    {
	    String[] сurses = {"Первый курс", "Второй курс", "Третий курс", "Четвертый курс", "Пятый курс", "Шестой курс"};

        Integer courseLast =  model.getSessionTransferDocument().getTargetStudent().getEducationOrgUnit().getDevelopPeriod().getLastCourse();
        Integer countListSessionTransferOperation = new DQLSelectBuilder().fromEntity(SessionTransferOperation.class, "c")
                .where(eq(property("c", SessionTransferOperation.targetMark().slot().document().id()), value(model.getSessionTransferDocument().getId()))).createStatement(getSession()).list().size();
        List<Integer> caption = new ArrayList<>();
        String[][] sessionTransferOperationData = new String[courseLast + countListSessionTransferOperation][];
        int i = 0;
        for (int g = 0; g < courseLast; g++)
        {
            caption.add(i);
            sessionTransferOperationData[i++] = new String[]{"", сurses[g], "", "", "", "", ""};

            DQLSelectBuilder dqlSessionTransferOperation = new DQLSelectBuilder().fromEntity(SessionTransferOperation.class, "c")
                    .where(eq(property("c", SessionTransferOperation.targetMark().slot().document().id()), value(model.getSessionTransferDocument().getId())))
                    .where(eq(property(SessionTransferOperation.targetMark().slot().studentWpeCAction().studentWpe().course().intValue().fromAlias("c")), value(g + 1)));

            List<SessionTransferOperation> sessionTransferOperationList = dqlSessionTransferOperation.createStatement(getSession()).list();

            int countDisc = 0;
            for (SessionTransferOperation sessionTransferOperation : sessionTransferOperationList)
            {
                String number = Integer.toString(countDisc + 1);
                String workUch = sessionTransferOperation.getTargetMark().getSlot().getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getRegistryElement().getSizeAsDouble().toString();
                String registryElementTitleUch = StringUtils.uncapitalize(sessionTransferOperation.getTargetMark().getSlot().getStudentWpeCAction().getType().getTitle());
                String workMarkD = sessionTransferOperation.getTargetMark().getValueTitle();
                String comment = sessionTransferOperation.getComment();
                String disc =  sessionTransferOperation.getTargetMark().getSlot().getStudentWpeCAction().getStudentWpe().getSourceRow().getTitle();
                String workTimeDisc="";
                if (sessionTransferOperation instanceof SessionTransferOutsideOperation)
                {
                    SessionTransferOutsideOperationExt sessionTransferOutsideOperationExt = DataAccessServices.dao().get(SessionTransferOutsideOperationExt.class,SessionTransferOutsideOperationExt.sessionTransferOutsideOperation().id(),sessionTransferOperation.getId());
                    if(sessionTransferOutsideOperationExt!=null&&sessionTransferOutsideOperationExt.getWorkTimeDisc()!=null)
                    {
                    workTimeDisc = sessionTransferOutsideOperationExt.getWorkTimeDisc().toString();
                    }
                }
                if (sessionTransferOperation instanceof SessionTransferInsideOperation)
                {
                    workTimeDisc = ((SessionTransferInsideOperation) sessionTransferOperation).getSourceMark().getSlot().getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getRegistryElement().getSizeAsDouble().toString();
                }
                sessionTransferOperationData[i++] = new String[]{number, disc, workUch, registryElementTitleUch, workTimeDisc, workMarkD, comment};
                model.setCaptainRowList(caption);
                countDisc++;
            }
        }
        return sessionTransferOperationData;
    }


}
