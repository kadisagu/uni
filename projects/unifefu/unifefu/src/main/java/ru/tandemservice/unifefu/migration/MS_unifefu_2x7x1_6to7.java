/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x1_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
                new ScriptDependency("org.tandemframework", "1.6.16"),
                new ScriptDependency("org.tandemframework.shared", "1.7.1"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuFControlActionType2EppStateEduStandardRel

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fffcntrlactntyp2eppsttedstnd_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("edustandard_id", DBType.LONG).setNullable(false),
				new DBColumn("controlaction_id", DBType.LONG).setNullable(false),
				new DBColumn("maxpartyear_p", DBType.INTEGER).setNullable(false),
				new DBColumn("maxyear_p", DBType.INTEGER).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuFControlActionType2EppStateEduStandardRel");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuEppStateEduStandardAdditionalChecks

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("ffeppsttedstndrdaddtnlchcks_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("edustandard_id", DBType.LONG).setNullable(false),
				new DBColumn("altinchoicedisciplines_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("cntrlactnonttlrgelmnt_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("formingcompetenceregelement_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("integralitylaborpartyear_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("evenaudithours_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("multipleaudithourspartyear_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("bindeduplanandfixowner_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("dplncnfrmtyschdlinprctcs_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("selfworkforanydiscipline_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("duplicatedisciplinestitles_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuEppStateEduStandardAdditionalChecks");

            Statement stmt = tool.getConnection().createStatement();
            ResultSet src = stmt.executeQuery("select id from epp_stateedustd_t");

            while (src.next())
            {
                Long eduStandardId = src.getLong(1);

                PreparedStatement insert =
                        tool.prepareStatement("insert into ffeppsttedstndrdaddtnlchcks_t (id, discriminator, edustandard_id, " +
				                        "altinchoicedisciplines_p, cntrlactnonttlrgelmnt_p, formingcompetenceregelement_p, integralitylaborpartyear_p, " +
				                        "evenaudithours_p, multipleaudithourspartyear_p, bindeduplanandfixowner_p, dplncnfrmtyschdlinprctcs_p, " +
				                        "selfworkforanydiscipline_p, duplicatedisciplinestitles_p) values (?, ? ,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

                insert.setLong(1, EntityIDGenerator.generateNewId(entityCode));
                insert.setShort(2, entityCode);
                insert.setLong(3, eduStandardId);
                insert.setBoolean(4, true);
                insert.setBoolean(5, true);
                insert.setBoolean(6, true);
                insert.setBoolean(7, true);
                insert.setBoolean(8, true);
                insert.setBoolean(9, true);
                insert.setBoolean(10, true);
                insert.setBoolean(11, true);
                insert.setBoolean(12, true);
                insert.setBoolean(13, true);
                insert.execute();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuCompetence2EppStateEduStandardRel

        // создано свойство programSpecialization
        {
            // создать колонку
            tool.createColumn("ffcmptnc2eppsttedstndrdrl_t", new DBColumn("programspecialization_id", DBType.LONG));
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuEppStateEduStandardParameters

        // создано свойство theoryWeek
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("theoryweek_p", DBType.DOUBLE));
        }

        // создано свойство examSessionWeek
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("examsessionweek_p", DBType.DOUBLE));
        }

        // создано свойство teachPracticeWeek
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("teachpracticeweek_p", DBType.DOUBLE));
        }

        // создано свойство workPracticeWeek
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("workpracticeweek_p", DBType.DOUBLE));
        }

        // создано свойство resultAttestationWeek
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("resultattestationweek_p", DBType.DOUBLE));
        }

        // создано свойство holidaysWeek
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("holidaysweek_p", DBType.DOUBLE));
        }

        // создано свойство holidaysFirstPartYearFullTimeForm
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("hldysfrstprtyrflltmfrm_p", DBType.DOUBLE));
        }

        // создано свойство maxAmountAvgLoadTheory
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("maxamountavgloadtheory_p", DBType.INTEGER));
        }

        // создано свойство maxAmountAvgLoadExamSession
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("maxamountavgloadexamsession_p", DBType.INTEGER));
        }

        // создано свойство maxAmountAvgAuditLoadTheory
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("maxamountavgauditloadtheory_p", DBType.INTEGER));
        }

        // создано свойство hoursForOneExamFullTimeForm
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("hoursforoneexamfulltimeform_p", DBType.INTEGER));
        }

        // создано свойство minLaborOneDiscipline
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("minlaboronediscipline_p", DBType.INTEGER));
        }

        // создано свойство maxLecturesPercent
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("maxlecturespercent_p", DBType.INTEGER));
        }

        // создано свойство maxInteractiveLessonsPercent
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("maxinteractivelessonspercent_p", DBType.INTEGER));
        }

        // создано свойство maxChoiceDisciplinesPercent
        {
            // создать колонку
            tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("maxchoicedisciplinespercent_p", DBType.INTEGER));
        }
    }
}