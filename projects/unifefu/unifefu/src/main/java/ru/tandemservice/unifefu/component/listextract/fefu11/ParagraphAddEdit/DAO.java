/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu11.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 19.11.2013
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuCompensationTypeTransferStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        FefuCompensationTypeTransferStuListExtract firstExtract = model.getFirstExtract();
        if(null != firstExtract)
        {
            if(null!= model.getParagraphId())
            {
                model.setCourse(firstExtract.getEntity().getCourse());
                model.setGroup(firstExtract.getEntity().getGroup());
            }
            model.setProtocolDate(firstExtract.getProtocolDate());
            model.setTransferDate(firstExtract.getTransferDate());
            model.setProtocolNumbers(firstExtract.getProtocolNumbers());
            model.setDevelopForm(firstExtract.getDevelopForm());
        }

        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()).hideInactiveProgramSubject(true));
        model.setDevelopFormList(getCatalogItemList(DevelopForm.class));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setCompensationTypeNew(UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET));
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developForm(), model.getDevelopForm()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.compensationType().code(), CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT));
    }

    @Override
    protected FefuCompensationTypeTransferStuListExtract createNewInstance(Model model)
    {
        return new FefuCompensationTypeTransferStuListExtract();
    }

    @Override
    protected void fillExtract(FefuCompensationTypeTransferStuListExtract extract, Student student, Model model)
    {
        extract.setProtocolDate(model.getProtocolDate());
        extract.setTransferDate(model.getTransferDate());
        extract.setProtocolNumbers(model.getProtocolNumbers());
        extract.setDevelopForm(model.getDevelopForm());
        extract.setCompensationTypeOld(student.getCompensationType());
        extract.setCompensationTypeNew(model.getCompensationTypeNew());
    }
}