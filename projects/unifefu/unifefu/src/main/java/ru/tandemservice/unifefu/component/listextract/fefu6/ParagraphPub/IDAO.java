/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuGiveDiplomaWithDismissStuListExtract, Model>
{
}
