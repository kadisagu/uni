package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Трудоемкость ГОС по блокам (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEppStateEduStandardLaborBlocksGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks";
    public static final String ENTITY_NAME = "fefuEppStateEduStandardLaborBlocks";
    public static final int VERSION_HASH = -1540370304;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_STANDARD = "eduStandard";
    public static final String L_QUALIFICATION = "qualification";
    public static final String L_PARENT = "parent";
    public static final String L_BLOCK = "block";
    public static final String P_MIN_NUMBER = "minNumber";
    public static final String P_MAX_NUMBER = "maxNumber";

    private EppStateEduStandard _eduStandard;     // ГОС
    private EduProgramQualification _qualification;     // Квалификация
    private FefuEppStateEduStandardLaborBlocks _parent;     // Родительский элемент
    private EppPlanStructure _block;     // Элемент ГОС
    private int _minNumber;     // Минимальное число ЗЕТ
    private int _maxNumber;     // Максимальное число ЗЕТ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ГОС. Свойство не может быть null.
     */
    @NotNull
    public EppStateEduStandard getEduStandard()
    {
        return _eduStandard;
    }

    /**
     * @param eduStandard ГОС. Свойство не может быть null.
     */
    public void setEduStandard(EppStateEduStandard eduStandard)
    {
        dirty(_eduStandard, eduStandard);
        _eduStandard = eduStandard;
    }

    /**
     * @return Квалификация. Свойство не может быть null.
     */
    @NotNull
    public EduProgramQualification getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация. Свойство не может быть null.
     */
    public void setQualification(EduProgramQualification qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Родительский элемент.
     */
    public FefuEppStateEduStandardLaborBlocks getParent()
    {
        return _parent;
    }

    /**
     * @param parent Родительский элемент.
     */
    public void setParent(FefuEppStateEduStandardLaborBlocks parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Элемент ГОС. Свойство не может быть null.
     */
    @NotNull
    public EppPlanStructure getBlock()
    {
        return _block;
    }

    /**
     * @param block Элемент ГОС. Свойство не может быть null.
     */
    public void setBlock(EppPlanStructure block)
    {
        dirty(_block, block);
        _block = block;
    }

    /**
     * @return Минимальное число ЗЕТ. Свойство не может быть null.
     */
    @NotNull
    public int getMinNumber()
    {
        return _minNumber;
    }

    /**
     * @param minNumber Минимальное число ЗЕТ. Свойство не может быть null.
     */
    public void setMinNumber(int minNumber)
    {
        dirty(_minNumber, minNumber);
        _minNumber = minNumber;
    }

    /**
     * @return Максимальное число ЗЕТ. Свойство не может быть null.
     */
    @NotNull
    public int getMaxNumber()
    {
        return _maxNumber;
    }

    /**
     * @param maxNumber Максимальное число ЗЕТ. Свойство не может быть null.
     */
    public void setMaxNumber(int maxNumber)
    {
        dirty(_maxNumber, maxNumber);
        _maxNumber = maxNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEppStateEduStandardLaborBlocksGen)
        {
            setEduStandard(((FefuEppStateEduStandardLaborBlocks)another).getEduStandard());
            setQualification(((FefuEppStateEduStandardLaborBlocks)another).getQualification());
            setParent(((FefuEppStateEduStandardLaborBlocks)another).getParent());
            setBlock(((FefuEppStateEduStandardLaborBlocks)another).getBlock());
            setMinNumber(((FefuEppStateEduStandardLaborBlocks)another).getMinNumber());
            setMaxNumber(((FefuEppStateEduStandardLaborBlocks)another).getMaxNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEppStateEduStandardLaborBlocksGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEppStateEduStandardLaborBlocks.class;
        }

        public T newInstance()
        {
            return (T) new FefuEppStateEduStandardLaborBlocks();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduStandard":
                    return obj.getEduStandard();
                case "qualification":
                    return obj.getQualification();
                case "parent":
                    return obj.getParent();
                case "block":
                    return obj.getBlock();
                case "minNumber":
                    return obj.getMinNumber();
                case "maxNumber":
                    return obj.getMaxNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduStandard":
                    obj.setEduStandard((EppStateEduStandard) value);
                    return;
                case "qualification":
                    obj.setQualification((EduProgramQualification) value);
                    return;
                case "parent":
                    obj.setParent((FefuEppStateEduStandardLaborBlocks) value);
                    return;
                case "block":
                    obj.setBlock((EppPlanStructure) value);
                    return;
                case "minNumber":
                    obj.setMinNumber((Integer) value);
                    return;
                case "maxNumber":
                    obj.setMaxNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduStandard":
                        return true;
                case "qualification":
                        return true;
                case "parent":
                        return true;
                case "block":
                        return true;
                case "minNumber":
                        return true;
                case "maxNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduStandard":
                    return true;
                case "qualification":
                    return true;
                case "parent":
                    return true;
                case "block":
                    return true;
                case "minNumber":
                    return true;
                case "maxNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduStandard":
                    return EppStateEduStandard.class;
                case "qualification":
                    return EduProgramQualification.class;
                case "parent":
                    return FefuEppStateEduStandardLaborBlocks.class;
                case "block":
                    return EppPlanStructure.class;
                case "minNumber":
                    return Integer.class;
                case "maxNumber":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEppStateEduStandardLaborBlocks> _dslPath = new Path<FefuEppStateEduStandardLaborBlocks>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEppStateEduStandardLaborBlocks");
    }
            

    /**
     * @return ГОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getEduStandard()
     */
    public static EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
    {
        return _dslPath.eduStandard();
    }

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getQualification()
     */
    public static EduProgramQualification.Path<EduProgramQualification> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Родительский элемент.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getParent()
     */
    public static FefuEppStateEduStandardLaborBlocks.Path<FefuEppStateEduStandardLaborBlocks> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Элемент ГОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getBlock()
     */
    public static EppPlanStructure.Path<EppPlanStructure> block()
    {
        return _dslPath.block();
    }

    /**
     * @return Минимальное число ЗЕТ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getMinNumber()
     */
    public static PropertyPath<Integer> minNumber()
    {
        return _dslPath.minNumber();
    }

    /**
     * @return Максимальное число ЗЕТ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getMaxNumber()
     */
    public static PropertyPath<Integer> maxNumber()
    {
        return _dslPath.maxNumber();
    }

    public static class Path<E extends FefuEppStateEduStandardLaborBlocks> extends EntityPath<E>
    {
        private EppStateEduStandard.Path<EppStateEduStandard> _eduStandard;
        private EduProgramQualification.Path<EduProgramQualification> _qualification;
        private FefuEppStateEduStandardLaborBlocks.Path<FefuEppStateEduStandardLaborBlocks> _parent;
        private EppPlanStructure.Path<EppPlanStructure> _block;
        private PropertyPath<Integer> _minNumber;
        private PropertyPath<Integer> _maxNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ГОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getEduStandard()
     */
        public EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
        {
            if(_eduStandard == null )
                _eduStandard = new EppStateEduStandard.Path<EppStateEduStandard>(L_EDU_STANDARD, this);
            return _eduStandard;
        }

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getQualification()
     */
        public EduProgramQualification.Path<EduProgramQualification> qualification()
        {
            if(_qualification == null )
                _qualification = new EduProgramQualification.Path<EduProgramQualification>(L_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Родительский элемент.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getParent()
     */
        public FefuEppStateEduStandardLaborBlocks.Path<FefuEppStateEduStandardLaborBlocks> parent()
        {
            if(_parent == null )
                _parent = new FefuEppStateEduStandardLaborBlocks.Path<FefuEppStateEduStandardLaborBlocks>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Элемент ГОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getBlock()
     */
        public EppPlanStructure.Path<EppPlanStructure> block()
        {
            if(_block == null )
                _block = new EppPlanStructure.Path<EppPlanStructure>(L_BLOCK, this);
            return _block;
        }

    /**
     * @return Минимальное число ЗЕТ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getMinNumber()
     */
        public PropertyPath<Integer> minNumber()
        {
            if(_minNumber == null )
                _minNumber = new PropertyPath<Integer>(FefuEppStateEduStandardLaborBlocksGen.P_MIN_NUMBER, this);
            return _minNumber;
        }

    /**
     * @return Максимальное число ЗЕТ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks#getMaxNumber()
     */
        public PropertyPath<Integer> maxNumber()
        {
            if(_maxNumber == null )
                _maxNumber = new PropertyPath<Integer>(FefuEppStateEduStandardLaborBlocksGen.P_MAX_NUMBER, this);
            return _maxNumber;
        }

        public Class getEntityClass()
        {
            return FefuEppStateEduStandardLaborBlocks.class;
        }

        public String getEntityName()
        {
            return "fefuEppStateEduStandardLaborBlocks";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
