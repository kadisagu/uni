/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard;

import org.springframework.core.io.Resource;
import org.tandemframework.core.ResourceLocationEnum;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.spring.CoreSpringUtils;
import ru.tandemservice.unifefu.ws.blackboard.context.BBContextInitializer;
import ru.tandemservice.unifefu.ws.blackboard.context.ContextWSConstants;
import ru.tandemservice.unifefu.ws.blackboard.context.gen.ContextWSPortType;
import ru.tandemservice.unifefu.ws.blackboard.context.gen.InitializeResponse;
import ru.tandemservice.unifefu.ws.blackboard.course.BBCourseInitializer;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.CourseWSPortType;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.BBGradebookInitializer;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.GradebookWSPortType;
import ru.tandemservice.unifefu.ws.blackboard.membership.BBMembershipInitializer;
import ru.tandemservice.unifefu.ws.blackboard.membership.gen.CourseMembershipWSPortType;
import ru.tandemservice.unifefu.ws.blackboard.user.BBUserInitializer;
import ru.tandemservice.unifefu.ws.blackboard.user.gen.UserWSPortType;

import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Nikolay Fedorovskih
 * @since 27.02.2014
 */
public class BBContextHelper
{
    private static volatile BBContextHelper _contextHelper;
    private HeaderHandlerResolver _headerHandlerResolver;
    private BBContextInitializer _contextInitializer;
    private BBCourseInitializer _courseInitializer;
    private BBUserInitializer _userInitializer;
    private BBMembershipInitializer _membershipInitializer;
    private BBGradebookInitializer _gradebookInitializer;
    private AtomicBoolean reinitLocked = new AtomicBoolean(false);

    /**
     * Тут у нас синглтон
     */
    public static BBContextHelper getInstance()
    {
        if (_contextHelper == null)
        {
            synchronized (BBContextHelper.class)
            {
                if (_contextHelper == null)
                    _contextHelper = new BBContextHelper();
            }
        }
        return _contextHelper;
    }

    public static URL getWSDL_resource_URL(String wsdlName)
    {
        try
        {
            List<Resource> resources = CoreSpringUtils.getModuleResourcesByPattern(ApplicationRuntime.getModuleMeta("unifefu"), ResourceLocationEnum.resources, "**/blackboard/" + wsdlName);
            if (resources.isEmpty())
                throw new RuntimeException("WSDL file " + wsdlName + " is not found");

            if (resources.size() > 1)
                throw new IllegalStateException();

            return resources.get(0).getURL();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    private void init()
    {
        _contextInitializer = new BBContextInitializer(_headerHandlerResolver);
        _courseInitializer = new BBCourseInitializer(_headerHandlerResolver);
        _userInitializer = new BBUserInitializer(_headerHandlerResolver);
        _membershipInitializer = new BBMembershipInitializer(_headerHandlerResolver);
        _gradebookInitializer = new BBGradebookInitializer(_headerHandlerResolver);

        _headerHandlerResolver.setSessionParamValue("nosession");

        // Инициализируем новую сессию
        InitializeResponse contextInitResp;
        try
        {
            contextInitResp = _contextInitializer.getPortType().initialize();
            _headerHandlerResolver.setSessionParamValue(contextInitResp.getReturn().getValue());
        }
        catch (WebServiceException e)
        {
            throw new ApplicationException(BBConstants.BB_COULD_NOT_CONNECT + ": " + e.getMessage());
        }
    }

    private BBContextHelper()
    {
        _headerHandlerResolver = new HeaderHandlerResolver("");

        init();
    }

    /**
     * Запрос к вебсервису ContextWS
     */
    public <T> T doContextRequest(IBBRequest<ContextWSPortType, ru.tandemservice.unifefu.ws.blackboard.context.gen.ObjectFactory, T> request)
    {
        return doRequest(request, getContextInitializer());
    }

    /**
     * Запрос к вебсервису CourseWS
     */
    public <T> T doCourseRequest(IBBRequest<CourseWSPortType, ru.tandemservice.unifefu.ws.blackboard.course.gen.ObjectFactory, T> request)
    {
        return doRequest(request, getCourseInitializer());
    }

    /**
     * Запрос к вебсервису UserWS
     */
    public <T> T doUserRequest(IBBRequest<UserWSPortType, ru.tandemservice.unifefu.ws.blackboard.user.gen.ObjectFactory, T> request)
    {
        return doRequest(request, getUserInitializer());
    }

    /**
     * Запрос к вебсервису CourseMembershipWS
     */
    public <T> T doMembershipRequest(IBBRequest<CourseMembershipWSPortType, ru.tandemservice.unifefu.ws.blackboard.membership.gen.ObjectFactory, T> request)
    {
        return doRequest(request, getMembershipInitializer());
    }

    /**
     * Запрос к вебсервису GradebookWS
     */
    public <T> T doGradebookRequest(IBBRequest<GradebookWSPortType, ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.ObjectFactory, T> request)
    {
        return doRequest(request, getGradebookInitializer());
    }

    private <PT, OF, T> T doRequest(IBBRequest<PT, OF, T> request, PortTypeInitializer<PT, OF> portTypeInitializer)
    {
        try
        {
            return request.invoke(portTypeInitializer.getPortType(), portTypeInitializer.getObjectFactory());
        }
        catch (SOAPFaultException e)
        {
            // Если сессия устарела (ошибка WSFW001) или в ответ пришла неизвестная ошибка (WSFW000),
            // пробуем проинициализировать новую сессию, логинимся и повторяем запрос
            if (blackboardErrorEquals(e, ContextWSConstants.INVALID_SESSION, ContextWSConstants.UNKNOWN_ERROR) && reInit())
            {
                return request.invoke(portTypeInitializer.getPortType(), portTypeInitializer.getObjectFactory());
            }

            // Если явно говорит, что нет доступа (ошибка WSFW004), надо просто залогиниться
            if (blackboardErrorEquals(e, ContextWSConstants.ACCESS_DENIED) && loginProxyTool(BBSettingsVO.getCachedVO()))
            {
                return request.invoke(portTypeInitializer.getPortType(), portTypeInitializer.getObjectFactory());
            }

            // В любом другом случае просто падаем дальше
            throw e;
        }
        catch (WebServiceException e)
        {
            if (e.getCause() instanceof SocketTimeoutException)
                throw new ApplicationException(BBConstants.BB_COULD_NOT_CONNECT + ": истекло время ожидания ответа.");

            e.printStackTrace();
            throw new ApplicationException(BBConstants.BB_COULD_NOT_CONNECT + ": " + e.getMessage());
        }
    }

    /**
     * Идентификация ошибок, возвращаемых вебсервисами Blackboard.
     * Обычно приходит в виде "[КОД_ОШИБКИ]Текст сообщения"
     *
     * @param error      исключение cxf
     * @param errorCodes список ожидаемых кодов ошибок
     * @return является ли данная ошибка одной из тех, коды которых переданы в #errorCodes
     */
    public static boolean blackboardErrorEquals(SOAPFaultException error, String... errorCodes)
    {
        for (String code : errorCodes)
        {
            if (error.getMessage().startsWith("[" + code))
            {
                return true;
            }
        }
        return false;
    }

    public boolean reInit()
    {
        if (!reinitLocked.get())
        {
            synchronized (BBContextHelper.class)
            {
                if (!reinitLocked.get())
                {
                    reinitLocked.set(true);
                    try
                    {
                        init();
                    }
                    finally
                    {
                        reinitLocked.set(false);
                    }
                }
            }
        }

        if (getContextInitializer() == null)
            return false;

        try
        {
            return loginProxyTool(BBSettingsVO.getCachedVO());
        }
        catch (SOAPFaultException e)
        {
            // Даже если в ответ пришла одна из этих ошибок, значит сессия точно проинициализована
            return blackboardErrorEquals(e, ContextWSConstants.CTXWS001, ContextWSConstants.CTXWS002, ContextWSConstants.CTXWS003);
        }
    }

    public boolean loginProxyTool(BBSettingsVO settingsVO)
    {
        return getContextInitializer().getPortType().loginTool(settingsVO.getClientPassword(), settingsVO.getClientVendorId(), settingsVO.getClientProgramId(), "", BBConnector.SESSION_KEEP_ALIVE)
                && getContextInitializer().getPortType().emulateUser(settingsVO.getEmulationUser());
    }

    public BBContextInitializer getContextInitializer()
    {
        return _contextInitializer;
    }

    public BBCourseInitializer getCourseInitializer()
    {
        return _courseInitializer;
    }

    public BBUserInitializer getUserInitializer()
    {
        return _userInitializer;
    }

    public BBMembershipInitializer getMembershipInitializer()
    {
        return _membershipInitializer;
    }

    public BBGradebookInitializer getGradebookInitializer()
    {
        return _gradebookInitializer;
    }
}