/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.FefuForeignOnlineEntrantSearchStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;

import java.util.HashMap;
import java.util.Map;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickNext(IBusinessComponent component)
    {
        Model model = getModel(component);

        // деактивируем компонент
        deactivate(component);

        // ищем абитуриента
        Entrant entrant = getDao().findEntrant(model);
        if (entrant != null)
        {
            // если нашли, то переходим на его карточку
            Map<String, Object> params = new HashMap<>();
            params.put("selectedTab", "entrantTab");
            params.put("selectedDataTab", null);
            activateInRoot(component, new PublisherActivator(entrant, params));
            return;
        }
        FefuForeignOnlineEntrant foreignOnlineEntrant = model.getFefuForeignOnlineEntrant();

        // ищем персону с полным сходством
        IdentityCard identityCard = new IdentityCard();
        identityCard.setFirstName(foreignOnlineEntrant.getFirstNameRu());
        identityCard.setLastName(foreignOnlineEntrant.getLastNameRu());
        identityCard.setCardType(getDao().getCatalogItem(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT_NOT_RF));

        Map<String, Object> params = new HashMap<>();
        params.put("identityCard", identityCard);
        params.put("foreignOnlineEntrantId", foreignOnlineEntrant.getId());
        params.put("enrollmentCampaignId", model.getEnrollmentCampaign().getId());
        params.put("entrantMasterPermKey", Model.FOREIGN_ENTRANT_MASTER_PERM_KEY);
        component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(IEntrantWizardComponents.IDENTITY_CARD_STEP, params));
    }
}
