package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность, хранящая данные дат доп.статуса студента до проведения приказа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentCustomStateToExtractRollbackDataGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData";
    public static final String ENTITY_NAME = "studentCustomStateToExtractRollbackData";
    public static final int VERSION_HASH = 1688783266;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_STUDENT_CUSTOM_STATE = "studentCustomState";
    public static final String P_DATE_STATUS_BEGIN = "dateStatusBegin";
    public static final String P_DATE_STATUS_END = "dateStatusEnd";

    private ModularStudentExtract _extract;     // Выписка из приказа
    private StudentCustomState _studentCustomState;     // Доп.статус студента
    private Date _dateStatusBegin;     // Время начала действия статуса
    private Date _dateStatusEnd;     // Время окончания действия статуса

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из приказа. Свойство не может быть null.
     */
    @NotNull
    public ModularStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка из приказа. Свойство не может быть null.
     */
    public void setExtract(ModularStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Доп.статус студента. Свойство не может быть null.
     */
    @NotNull
    public StudentCustomState getStudentCustomState()
    {
        return _studentCustomState;
    }

    /**
     * @param studentCustomState Доп.статус студента. Свойство не может быть null.
     */
    public void setStudentCustomState(StudentCustomState studentCustomState)
    {
        dirty(_studentCustomState, studentCustomState);
        _studentCustomState = studentCustomState;
    }

    /**
     * @return Время начала действия статуса.
     */
    public Date getDateStatusBegin()
    {
        return _dateStatusBegin;
    }

    /**
     * @param dateStatusBegin Время начала действия статуса.
     */
    public void setDateStatusBegin(Date dateStatusBegin)
    {
        dirty(_dateStatusBegin, dateStatusBegin);
        _dateStatusBegin = dateStatusBegin;
    }

    /**
     * @return Время окончания действия статуса.
     */
    public Date getDateStatusEnd()
    {
        return _dateStatusEnd;
    }

    /**
     * @param dateStatusEnd Время окончания действия статуса.
     */
    public void setDateStatusEnd(Date dateStatusEnd)
    {
        dirty(_dateStatusEnd, dateStatusEnd);
        _dateStatusEnd = dateStatusEnd;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentCustomStateToExtractRollbackDataGen)
        {
            setExtract(((StudentCustomStateToExtractRollbackData)another).getExtract());
            setStudentCustomState(((StudentCustomStateToExtractRollbackData)another).getStudentCustomState());
            setDateStatusBegin(((StudentCustomStateToExtractRollbackData)another).getDateStatusBegin());
            setDateStatusEnd(((StudentCustomStateToExtractRollbackData)another).getDateStatusEnd());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentCustomStateToExtractRollbackDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentCustomStateToExtractRollbackData.class;
        }

        public T newInstance()
        {
            return (T) new StudentCustomStateToExtractRollbackData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "studentCustomState":
                    return obj.getStudentCustomState();
                case "dateStatusBegin":
                    return obj.getDateStatusBegin();
                case "dateStatusEnd":
                    return obj.getDateStatusEnd();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((ModularStudentExtract) value);
                    return;
                case "studentCustomState":
                    obj.setStudentCustomState((StudentCustomState) value);
                    return;
                case "dateStatusBegin":
                    obj.setDateStatusBegin((Date) value);
                    return;
                case "dateStatusEnd":
                    obj.setDateStatusEnd((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "studentCustomState":
                        return true;
                case "dateStatusBegin":
                        return true;
                case "dateStatusEnd":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "studentCustomState":
                    return true;
                case "dateStatusBegin":
                    return true;
                case "dateStatusEnd":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return ModularStudentExtract.class;
                case "studentCustomState":
                    return StudentCustomState.class;
                case "dateStatusBegin":
                    return Date.class;
                case "dateStatusEnd":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentCustomStateToExtractRollbackData> _dslPath = new Path<StudentCustomStateToExtractRollbackData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentCustomStateToExtractRollbackData");
    }
            

    /**
     * @return Выписка из приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData#getExtract()
     */
    public static ModularStudentExtract.Path<ModularStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Доп.статус студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData#getStudentCustomState()
     */
    public static StudentCustomState.Path<StudentCustomState> studentCustomState()
    {
        return _dslPath.studentCustomState();
    }

    /**
     * @return Время начала действия статуса.
     * @see ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData#getDateStatusBegin()
     */
    public static PropertyPath<Date> dateStatusBegin()
    {
        return _dslPath.dateStatusBegin();
    }

    /**
     * @return Время окончания действия статуса.
     * @see ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData#getDateStatusEnd()
     */
    public static PropertyPath<Date> dateStatusEnd()
    {
        return _dslPath.dateStatusEnd();
    }

    public static class Path<E extends StudentCustomStateToExtractRollbackData> extends EntityPath<E>
    {
        private ModularStudentExtract.Path<ModularStudentExtract> _extract;
        private StudentCustomState.Path<StudentCustomState> _studentCustomState;
        private PropertyPath<Date> _dateStatusBegin;
        private PropertyPath<Date> _dateStatusEnd;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData#getExtract()
     */
        public ModularStudentExtract.Path<ModularStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new ModularStudentExtract.Path<ModularStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Доп.статус студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData#getStudentCustomState()
     */
        public StudentCustomState.Path<StudentCustomState> studentCustomState()
        {
            if(_studentCustomState == null )
                _studentCustomState = new StudentCustomState.Path<StudentCustomState>(L_STUDENT_CUSTOM_STATE, this);
            return _studentCustomState;
        }

    /**
     * @return Время начала действия статуса.
     * @see ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData#getDateStatusBegin()
     */
        public PropertyPath<Date> dateStatusBegin()
        {
            if(_dateStatusBegin == null )
                _dateStatusBegin = new PropertyPath<Date>(StudentCustomStateToExtractRollbackDataGen.P_DATE_STATUS_BEGIN, this);
            return _dateStatusBegin;
        }

    /**
     * @return Время окончания действия статуса.
     * @see ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData#getDateStatusEnd()
     */
        public PropertyPath<Date> dateStatusEnd()
        {
            if(_dateStatusEnd == null )
                _dateStatusEnd = new PropertyPath<Date>(StudentCustomStateToExtractRollbackDataGen.P_DATE_STATUS_END, this);
            return _dateStatusEnd;
        }

        public Class getEntityClass()
        {
            return StudentCustomStateToExtractRollbackData.class;
        }

        public String getEntityName()
        {
            return "studentCustomStateToExtractRollbackData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
