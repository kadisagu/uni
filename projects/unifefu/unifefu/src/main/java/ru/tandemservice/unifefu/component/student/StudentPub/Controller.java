/**
 *$Id: Controller.java 38584 2014-10-08 11:05:36Z azhebko $
 */
package ru.tandemservice.unifefu.component.student.StudentPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuStudent.ui.VipDataEdit.FefuStudentVipDataEdit;
import ru.tandemservice.unifefu.base.bo.NSISync.ui.ChangeGuid.NSISyncChangeGuid;
import ru.tandemservice.unifefu.base.bo.NSISync.ui.ChangeGuid.NSISyncChangeGuidUI;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiDaemonDao;
import ru.tandemservice.unifefu.dao.daemon.IFefuNsiDaemonDao;

/**
 * @author Alexander Shaburov
 * @since 30.08.12
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        //сетим студента из расширяемого компонента
        ru.tandemservice.uni.component.student.StudentPub.Model baseModel = (ru.tandemservice.uni.component.student.StudentPub.Model) component.getModel(component.getName());
        Student student = baseModel.getStudent();
        model.setBaseModel(baseModel);
        model.setStudent(student);

        getDao().prepare(model);
    }

    public void onClickUnarchive(IBusinessComponent component)
    {
        ru.tandemservice.uni.component.student.StudentPub.Controller baseController = (ru.tandemservice.uni.component.student.StudentPub.Controller) component.getController(component.getName());
        baseController.getModel(component).getStudent().setFinishYear(null);
        baseController.getModel(component).getStudent().setArchivingDate(null);
        baseController.onClickUnarchive(component);
    }

    public void onClickStudentVipDataEdit(IBusinessComponent component)
    {
        component.createChildRegion("studentTabPanel", new ComponentActivator(FefuStudentVipDataEdit.class.getSimpleName(), new ParametersMap()
                .add("studentId", getModel(component).getStudent().getId())));
    }

    public void onClickForeignStudentDataEdit(IBusinessComponent component)
    {
        component.createChildRegion("studentTabPanel", new ComponentActivator(ru.tandemservice.unifefu.component.student.FefuForeignStudentDataEdit.Model.class.getPackage().getName(), new ParametersMap()
                .add("studentId", getModel(component).getStudent().getId())
        ));
    }

    public void onClickUvcFvoStudentDataEdit(IBusinessComponent component)
    {
        component.createChildRegion("studentTabPanel", new ComponentActivator(ru.tandemservice.unifefu.component.student.FefuUvcFvoStudentDataEdit.Model.class.getPackage().getName(), new ParametersMap()
                .add("studentId", getModel(component).getStudent().getId())
        ));
    }

    public void onClickLotusStudentDataEdit(IBusinessComponent component)
    {
        component.createChildRegion("studentTabPanel", new ComponentActivator(ru.tandemservice.unifefu.component.student.FefuLotusStudentDataEdit.Model.class.getPackage().getName(), new ParametersMap()
                .add("studentId", getModel(component).getStudent().getId())
        ));
    }


    public void onClickLotusStudentIDEdit(IBusinessComponent component)
    {
        component.createChildRegion("studentTabPanel", new ComponentActivator(ru.tandemservice.unifefu.component.student.FefuLotusStudentIDEdit.Model.class.getPackage().getName(), new ParametersMap()
                .add("studentId", getModel(component).getStudent().getId())
        ));
    }


    public void onClickSendToNsi(IBusinessComponent component)
    {
        Long[] ids = new Long[]{getModel(component).getStudent().getId()};
        IFefuNsiDaemonDao.instance.get().doRegisterEntityAdd(ids);
        FefuNsiDaemonDao.DAEMON.wakeUpDaemon();
    }

    public void onClickEditGuid(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(NSISyncChangeGuid.class.getSimpleName(),
                new ParametersMap().add(NSISyncChangeGuidUI.ENTITY_ID, getModel(component).getStudent().getId())));
    }
}