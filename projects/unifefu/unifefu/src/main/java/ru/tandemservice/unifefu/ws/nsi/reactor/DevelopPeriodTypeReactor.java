/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.DevelopPeriodType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class DevelopPeriodTypeReactor extends SimpleCatalogEntityNewReactor<DevelopPeriodType, DevelopPeriod>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<DevelopPeriod> getEntityClass()
    {
        return DevelopPeriod.class;
    }

    @Override
    public Class<DevelopPeriodType> getNSIEntityClass()
    {
        return DevelopPeriodType.class;
    }

    @Override
    public DevelopPeriodType getCatalogElementRetrieveRequestObject(String guid)
    {
        DevelopPeriodType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createDevelopPeriodType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public DevelopPeriodType createEntity(CoreCollectionUtils.Pair<DevelopPeriod, FefuNsiIds> entityPair)
    {
        DevelopPeriodType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createDevelopPeriodType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setDevelopPeriodID(entityPair.getX().getCode());
        nsiEntity.setDevelopPeriodName(entityPair.getX().getTitle());
        nsiEntity.setDevelopPeriodLastCourse(String.valueOf(entityPair.getX().getLastCourse()));
        nsiEntity.setDevelopPeriodPriority(String.valueOf(entityPair.getX().getPriority()));
        return nsiEntity;
    }

    @Override
    public DevelopPeriod createEntity(DevelopPeriodType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getDevelopPeriodName()) return null;

        DevelopPeriod entity = new DevelopPeriod();
        entity.setTitle(nsiEntity.getDevelopPeriodName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(DevelopPeriod.class));
        if (null != StringUtils.trimToNull(nsiEntity.getDevelopPeriodLastCourse()))
            entity.setLastCourse(Integer.parseInt(nsiEntity.getDevelopPeriodLastCourse()));
        else entity.setLastCourse(0);
        if (null != StringUtils.trimToNull(nsiEntity.getDevelopPeriodPriority()))
            entity.setPriority(Integer.parseInt(nsiEntity.getDevelopPeriodPriority()));
        else entity.setPriority(-1);

        return entity;
    }

    @Override
    public DevelopPeriod updatePossibleDuplicateFields(DevelopPeriodType nsiEntity, CoreCollectionUtils.Pair<DevelopPeriod, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), DevelopPeriod.title().s()))
                entityPair.getX().setTitle(nsiEntity.getDevelopPeriodName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), DevelopPeriod.lastCourse().s()))
                entityPair.getX().setLastCourse(null != StringUtils.trimToNull(nsiEntity.getDevelopPeriodLastCourse()) ? Integer.parseInt(nsiEntity.getDevelopPeriodLastCourse()) : 0);

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), DevelopPeriod.priority().s()))
                entityPair.getX().setPriority(null != StringUtils.trimToNull(nsiEntity.getDevelopPeriodPriority()) ? Integer.parseInt(nsiEntity.getDevelopPeriodPriority()) : -1);

        }
        return entityPair.getX();
    }

    @Override
    public DevelopPeriodType updateNsiEntityFields(DevelopPeriodType nsiEntity, DevelopPeriod entity)
    {
        nsiEntity.setDevelopPeriodID(entity.getCode());
        nsiEntity.setDevelopPeriodName(entity.getTitle());
        nsiEntity.setDevelopPeriodLastCourse(String.valueOf(entity.getLastCourse()));
        nsiEntity.setDevelopPeriodPriority(String.valueOf(entity.getPriority()));
        return nsiEntity;
    }
/*
    @Override
    public DevelopPeriod getPossibleDuplicate(DevelopPeriodType nsiEntity, Map<String, FefuNsiIds> nsiIdsMap, Map<String, DevelopPeriod> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getDevelopPeriodName()) return null;

        // Проверяем, есть ли в базе аналогичные переданной сущности
        DevelopPeriod possibleDuplicate = null;

        if (nsiIdsMap.containsKey(nsiEntity.getID()))
        {
            possibleDuplicate = similarEntityMap.get(String.valueOf(nsiIdsMap.get(nsiEntity.getID()).getEntityId()));
        }

        if (null == possibleDuplicate)
        {
            String titleIdx = nsiEntity.getDevelopPeriodName().trim().toUpperCase();
            if (similarEntityMap.containsKey(titleIdx)) possibleDuplicate = similarEntityMap.get(titleIdx);
        }

        return possibleDuplicate;
    }

    @Override
    public boolean isAnythingChanged(DevelopPeriodType nsiEntity, DevelopPeriod entity, Map<String, DevelopPeriod> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getDevelopPeriodName()) return false;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(similarEntityMap, nsiEntity.getDevelopPeriodName(), entity.getTitle()))
                return true;
        }
        return false;
    }

    @Override
    public DevelopPeriodType createEntity(DevelopPeriod entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        DevelopPeriodType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createDevelopPeriodType();
        nsiEntity.setID(getGUID(entity, nsiIdsMap));
        nsiEntity.setDevelopPeriodID(entity.getCode());
        nsiEntity.setDevelopPeriodName(entity.getTitle());
        nsiEntity.setDevelopPeriodLastCourse(String.valueOf(entity.getLastCourse()));
        nsiEntity.setDevelopPeriodPriority(String.valueOf(entity.getPriority()));
        return nsiEntity;
    }

    @Override
    public DevelopPeriod createEntity(DevelopPeriodType nsiEntity, Map<String, DevelopPeriod> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getDevelopPeriodName()) return null;

        DevelopPeriod entity = new DevelopPeriod();
        entity.setTitle(nsiEntity.getDevelopPeriodName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(DevelopPeriod.class));
        entity.setLastCourse(null != nsiEntity.getDevelopPeriodLastCourse() ? Integer.parseInt(nsiEntity.getDevelopPeriodLastCourse()) : 0);
        entity.setPriority(null != nsiEntity.getDevelopPeriodPriority() ? Integer.parseInt(nsiEntity.getDevelopPeriodPriority()) : 0);

        return entity;
    }

    @Override
    public DevelopPeriod updatePossibleDuplicateFields(DevelopPeriodType nsiEntity, DevelopPeriod entity, Map<String, DevelopPeriod> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getDevelopPeriodName()) return null;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(similarEntityMap, nsiEntity.getDevelopPeriodName(), entity.getTitle()))
                entity.setTitle(nsiEntity.getDevelopPeriodName());
        }
        return entity;
    }

    @Override
    public DevelopPeriodType updateNsiEntityFields(DevelopPeriodType nsiEntity, DevelopPeriod entity)
    {
        nsiEntity.setDevelopPeriodID(entity.getCode());
        nsiEntity.setDevelopPeriodName(entity.getTitle());
        nsiEntity.setDevelopPeriodLastCourse(String.valueOf(entity.getLastCourse()));
        nsiEntity.setDevelopPeriodPriority(String.valueOf(entity.getPriority()));
        return nsiEntity;
    } */
}