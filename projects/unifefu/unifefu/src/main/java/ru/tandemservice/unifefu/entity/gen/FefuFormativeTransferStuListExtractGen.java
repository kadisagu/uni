package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об изменении формирующего подразделения»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuFormativeTransferStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract";
    public static final String ENTITY_NAME = "fefuFormativeTransferStuListExtract";
    public static final int VERSION_HASH = -507511330;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP_NEW = "groupNew";
    public static final String L_GROUP_OLD = "groupOld";
    public static final String L_COMPENSATION_TYPE_NEW = "compensationTypeNew";
    public static final String L_COMPENSATION_TYPE_OLD = "compensationTypeOld";
    public static final String L_EDUCATION_ORG_UNIT_NEW = "educationOrgUnitNew";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";

    private Course _course;     // Курс
    private Group _groupNew;     // Новая группа
    private Group _groupOld;     // Старая группа
    private CompensationType _compensationTypeNew;     // Новый вид возмещения затрат
    private CompensationType _compensationTypeOld;     // Старый вид возмещения затрат
    private EducationOrgUnit _educationOrgUnitNew;     // Новое направление подготовки
    private EducationOrgUnit _educationOrgUnitOld;     // Старое направление подготовки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupNew()
    {
        return _groupNew;
    }

    /**
     * @param groupNew Новая группа. Свойство не может быть null.
     */
    public void setGroupNew(Group groupNew)
    {
        dirty(_groupNew, groupNew);
        _groupNew = groupNew;
    }

    /**
     * @return Старая группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupOld()
    {
        return _groupOld;
    }

    /**
     * @param groupOld Старая группа. Свойство не может быть null.
     */
    public void setGroupOld(Group groupOld)
    {
        dirty(_groupOld, groupOld);
        _groupOld = groupOld;
    }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationTypeNew()
    {
        return _compensationTypeNew;
    }

    /**
     * @param compensationTypeNew Новый вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeNew(CompensationType compensationTypeNew)
    {
        dirty(_compensationTypeNew, compensationTypeNew);
        _compensationTypeNew = compensationTypeNew;
    }

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationTypeOld()
    {
        return _compensationTypeOld;
    }

    /**
     * @param compensationTypeOld Старый вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeOld(CompensationType compensationTypeOld)
    {
        dirty(_compensationTypeOld, compensationTypeOld);
        _compensationTypeOld = compensationTypeOld;
    }

    /**
     * @return Новое направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    /**
     * @param educationOrgUnitNew Новое направление подготовки. Свойство не может быть null.
     */
    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        dirty(_educationOrgUnitNew, educationOrgUnitNew);
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    /**
     * @return Старое направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Старое направление подготовки. Свойство не может быть null.
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuFormativeTransferStuListExtractGen)
        {
            setCourse(((FefuFormativeTransferStuListExtract)another).getCourse());
            setGroupNew(((FefuFormativeTransferStuListExtract)another).getGroupNew());
            setGroupOld(((FefuFormativeTransferStuListExtract)another).getGroupOld());
            setCompensationTypeNew(((FefuFormativeTransferStuListExtract)another).getCompensationTypeNew());
            setCompensationTypeOld(((FefuFormativeTransferStuListExtract)another).getCompensationTypeOld());
            setEducationOrgUnitNew(((FefuFormativeTransferStuListExtract)another).getEducationOrgUnitNew());
            setEducationOrgUnitOld(((FefuFormativeTransferStuListExtract)another).getEducationOrgUnitOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuFormativeTransferStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuFormativeTransferStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuFormativeTransferStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "groupNew":
                    return obj.getGroupNew();
                case "groupOld":
                    return obj.getGroupOld();
                case "compensationTypeNew":
                    return obj.getCompensationTypeNew();
                case "compensationTypeOld":
                    return obj.getCompensationTypeOld();
                case "educationOrgUnitNew":
                    return obj.getEducationOrgUnitNew();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "groupNew":
                    obj.setGroupNew((Group) value);
                    return;
                case "groupOld":
                    obj.setGroupOld((Group) value);
                    return;
                case "compensationTypeNew":
                    obj.setCompensationTypeNew((CompensationType) value);
                    return;
                case "compensationTypeOld":
                    obj.setCompensationTypeOld((CompensationType) value);
                    return;
                case "educationOrgUnitNew":
                    obj.setEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "groupNew":
                        return true;
                case "groupOld":
                        return true;
                case "compensationTypeNew":
                        return true;
                case "compensationTypeOld":
                        return true;
                case "educationOrgUnitNew":
                        return true;
                case "educationOrgUnitOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "groupNew":
                    return true;
                case "groupOld":
                    return true;
                case "compensationTypeNew":
                    return true;
                case "compensationTypeOld":
                    return true;
                case "educationOrgUnitNew":
                    return true;
                case "educationOrgUnitOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "groupNew":
                    return Group.class;
                case "groupOld":
                    return Group.class;
                case "compensationTypeNew":
                    return CompensationType.class;
                case "compensationTypeOld":
                    return CompensationType.class;
                case "educationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuFormativeTransferStuListExtract> _dslPath = new Path<FefuFormativeTransferStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuFormativeTransferStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getGroupNew()
     */
    public static Group.Path<Group> groupNew()
    {
        return _dslPath.groupNew();
    }

    /**
     * @return Старая группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getGroupOld()
     */
    public static Group.Path<Group> groupOld()
    {
        return _dslPath.groupOld();
    }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getCompensationTypeNew()
     */
    public static CompensationType.Path<CompensationType> compensationTypeNew()
    {
        return _dslPath.compensationTypeNew();
    }

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getCompensationTypeOld()
     */
    public static CompensationType.Path<CompensationType> compensationTypeOld()
    {
        return _dslPath.compensationTypeOld();
    }

    /**
     * @return Новое направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
    {
        return _dslPath.educationOrgUnitNew();
    }

    /**
     * @return Старое направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    public static class Path<E extends FefuFormativeTransferStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _groupNew;
        private Group.Path<Group> _groupOld;
        private CompensationType.Path<CompensationType> _compensationTypeNew;
        private CompensationType.Path<CompensationType> _compensationTypeOld;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Новая группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getGroupNew()
     */
        public Group.Path<Group> groupNew()
        {
            if(_groupNew == null )
                _groupNew = new Group.Path<Group>(L_GROUP_NEW, this);
            return _groupNew;
        }

    /**
     * @return Старая группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getGroupOld()
     */
        public Group.Path<Group> groupOld()
        {
            if(_groupOld == null )
                _groupOld = new Group.Path<Group>(L_GROUP_OLD, this);
            return _groupOld;
        }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getCompensationTypeNew()
     */
        public CompensationType.Path<CompensationType> compensationTypeNew()
        {
            if(_compensationTypeNew == null )
                _compensationTypeNew = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE_NEW, this);
            return _compensationTypeNew;
        }

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getCompensationTypeOld()
     */
        public CompensationType.Path<CompensationType> compensationTypeOld()
        {
            if(_compensationTypeOld == null )
                _compensationTypeOld = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE_OLD, this);
            return _compensationTypeOld;
        }

    /**
     * @return Новое направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
        {
            if(_educationOrgUnitNew == null )
                _educationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_NEW, this);
            return _educationOrgUnitNew;
        }

    /**
     * @return Старое направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

        public Class getEntityClass()
        {
            return FefuFormativeTransferStuListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuFormativeTransferStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
