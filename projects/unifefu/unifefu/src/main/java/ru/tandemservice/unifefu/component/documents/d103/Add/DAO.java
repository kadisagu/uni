/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d103.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.entity.employee.OrderData;

/**
 * @author Alexey Lopatin
 * @since 29.09.2013
 */
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());

        if (null != orderData)
        {
            model.setEnrollmentOrderNumber(orderData.getEduEnrollmentOrderNumber());
            model.setEnrollmentOrderDate(orderData.getEduEnrollmentOrderDate());
        }
        int finishYear = model.getStudent().getEntranceYear() + model.getStudent().getEducationOrgUnit().getDevelopPeriod().getLastCourse();

        model.setFinishYear(finishYear);
    }
}
