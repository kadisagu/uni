/* $Id$ */
package ru.tandemservice.unifefu.base.ext.ExternalOrgUnit.ui.WizardStep1Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.WizardStep1Add.ExternalOrgUnitWizardStep1Add;

/**
 * @author nvankov
 * @since 8/13/13
 */
@Configuration
public class ExternalOrgUnitWizardStep1AddExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + ExternalOrgUnitWizardStep1AddExtUI.class.getSimpleName();

    @Autowired
    private ExternalOrgUnitWizardStep1Add _externalOrgUnitWizardStep1Add;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_externalOrgUnitWizardStep1Add.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, ExternalOrgUnitWizardStep1AddExtUI.class))
                .addAction(new ExternalOrgUnitWizardStep1AddClickApplyAction("onClickApply"))
                .addAction(new ExternalOrgUnitWizardStep1AddClickNextAction("onClickNext"))
                .create();
    }
}
