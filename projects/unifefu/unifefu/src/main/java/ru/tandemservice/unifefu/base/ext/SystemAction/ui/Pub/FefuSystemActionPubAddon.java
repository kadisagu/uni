/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.ext.SystemAction.ui.Pub;

import com.google.common.collect.Iterables;
import com.healthmarketscience.jackcess.Database;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.w3c.dom.Document;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.base.ext.SystemAction.ui.Pub.UniSystemActionPubAddon;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.ImtsaReImport.FefuEduPlanImtsaReImport;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.ArchiveEduPlanVersions.FefuSystemActionArchiveEduPlanVersions;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.CompetitionGroupFix.FefuSystemActionCompetitionGroupFix;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.DeleteNotCoordinatedOrders.FefuSystemActionDeleteNotCoordinatedOrders;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.GetLibraryPersonalData.FefuSystemActionGetLibraryPersonalData;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.GetPersonalData.FefuSystemActionGetPersonalData;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.IDMResponsesProcessing.FefuSystemActionIDMResponsesProcessing;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.ImportContracts.FefuSystemActionImportContracts;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.MergePersonsAndUpdateGuids.FefuSystemActionMergePersonsAndUpdateGuids;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.MoveOriginals.FefuSystemActionMoveOriginals;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.OnlineEntrantNotifications.FefuSystemActionOnlineEntrantNotifications;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.RegenGuids.FefuSystemActionRegenGuids;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.SendPortalNotifications.FefuSystemActionSendPortalNotifications;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.StudentsSendToArchive.FefuSystemActionStudentsSendToArchive;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.Test1C.FefuSystemActionTest1C;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.TestIDMStuff.FefuSystemActionTestIDMStuff;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.TransferDataAboutTargetReception.FefuSystemActionTransferDataAboutTargetReception;
import ru.tandemservice.unifefu.base.bo.NSISync.ui.PersonSync.NSISyncPersonSync;
import ru.tandemservice.unifefu.dao.daemon.*;
import ru.tandemservice.unifefu.dao.eppEduPlan.IImtsaImportDAO;
import ru.tandemservice.unifefu.dao.mdbio.IFefuEmployeeIODao;
import ru.tandemservice.unifefu.dao.mdbio.IFefuImtsaCompetenceIODao;
import ru.tandemservice.unifefu.dao.mdbio.IFefuOrdersIOAddDataDao;
import ru.tandemservice.unifefu.dao.mdbio.IFefuStudentIOAddDataDao;
import ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel;
import ru.tandemservice.unifefu.entity.FefuImtsaXml;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.node.ImtsaFile;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.node.planRow.ImtsaPlanRow;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.parser.ImtsaParser;
import ru.tandemservice.unifefu.utils.imtsa.ImtsaLoader;

import javax.annotation.Nullable;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
public class FefuSystemActionPubAddon extends UIAddon
{
    private Integer _tempMdbFileId;

    private void downloadTempFile(Integer temporaryFileId, String extension)
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.PRINT_REPORT)
                .parameters(new ParametersMap().add("id", temporaryFileId).add("zip", Boolean.FALSE).add("extension", extension))
                .activate();
    }

    @Override
    public void onComponentPrepareRender()
    {
        if (_tempMdbFileId != null)
        {
            try
            {
                downloadTempFile(_tempMdbFileId, "mdb");
            }
            finally
            {
                _tempMdbFileId = null;
            }
        }
    }

    public FefuSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickImportFefuExtendedStudents()
    {

        // пытаемся открыть файл
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {

                // предварительно
                String path = ApplicationRuntime.getAppInstallPath();
                final File file = new File(path, "data/importFefuStudentsAddData.mdb");
                if (!file.exists())
                {
                    throw new ApplicationException("Импорт данных провести не удалось: не найден файл «importFefuStudentsAddData.mdb» с исходными данными в каталоге «${app.install.path}/data/». Разместите файл для импорта с указанным названием на сервере приложения по указанному пути.");
                }
                if (!file.canRead())
                {
                    throw new ApplicationException("Импорт данных провести не удалось: файл «${app.install.path}/data/importFefuStudentsAddData.mdb» не доступен для чтения. Проверьте доступ к файлу на сервере приложения.");
                }

                // отключаем логирование в базу и прочие системные штуки
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                try
                {
                    BaseIODao.setupProcessState(state);
                    BaseIODao.doLoggedAction(() -> {
                        BaseIODao.getLog4jLogger().info("Import started.");
                        try
                        {
                            // открываем файл
                            try (Database mdb = Database.open(file))
                            {
                                // запускаем импорт
                                final Map<String, Long> value = IFefuStudentIOAddDataDao.instance.get().doImport_FefuStudentAddData(mdb);
                                BaseIODao.getLog4jLogger().info("Import finished. Done.");
                                return value;
                            }
                        }
                        catch (Exception t)
                        {
                            BaseIODao.getLog4jLogger().info("Import finished. Error.");
                            throw CoreExceptionUtils.getRuntimeException(t);
                        }
                    }, "importStudentsFefuAdditionlData");

                }
                catch (final Exception t)
                {

                    if (t instanceof ApplicationException)
                    {
                        // пробрасываем ApplicationException, как есть
                        throw (ApplicationException) t;
                    }

                    Debug.exception(t.getMessage(), t);
                    throw new ApplicationException("Импорт данных провести не удалось: произошла ошибка при обращении к файлу импорта. Обратитесь к логу импорта (файл importStudentsFefuAdditionlData.log) на сервере приложения для получения детальной информации об ошибке.", t);

                }
                finally
                {
                    // включаем штуки и логирование обратно
                    Debug.resumeLogging();
                    eventLock.release();
                }

                // если все ок, просто закрываем диалог
                return new ProcessResult("Импорт студентов завершен."); // закрываем диалог
            }
        };

        new BackgroundProcessHolder().start("Импорт данных студентов", process, ProcessDisplayMode.unknown);
    }

    public void onClickImportFefuOrdersFromLotus()
    {
        final String[][] filesForImport = new String[][]{
                {"Stud_prik_osn.mdb", "prik_osn", StudentExtractTypeCodes.FEFU_BASIC_EDU_PROGRAM_OTHER_ORDER},
                {"Stud_prik_dop.mdb", "prik_osn", StudentExtractTypeCodes.FEFU_ADDITIONAL_EDU_PROGRAM_OTHER_ORDER},
                {"Stud_prik_prak.mdb", "prik_osn", StudentExtractTypeCodes.FEFU_PRACTICE_OTHER_ORDER},
                {"Stud_prik_obs.mdb", "prik_osn", StudentExtractTypeCodes.FEFU_ENCOURAGE_OTHER_ORDER},
                {"Stud_prik_sir.mdb", "prik_osn", StudentExtractTypeCodes.FEFU_ORPHAN_GRANT_OTHER_ORDER},
        };

        // пытаемся открыть файл
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {

                // предварительно
                String path = ApplicationRuntime.getAppInstallPath();

                for (String[] dbFile : filesForImport)
                {
                    final String fileName = dbFile[0];
                    final String columnName = dbFile[1];
                    final String extractCategoryCode = dbFile[2];
                    final File file = new File(path, "data/" + fileName);
                    if (!file.exists())
                    {
                        BaseIODao.getLog4jLogger().info("DB IMPORT SKIPPED: Import for " + fileName + " was skipped, because there is no file with the name.");
                        //throw new ApplicationException("Импорт данных провести не удалось: не найден файл «Stud_prik_osn.mdb» с исходными данными в каталоге «${app.install.path}/data/». Разместите файл для импорта с указанным названием на сервере приложения по указанному пути.");
                        continue;
                    }
                    if (!file.canRead())
                    {
                        throw new ApplicationException("Импорт данных провести не удалось: файл «${app.install.path}/data/" + fileName + "» не доступен для чтения. Проверьте доступ к файлу на сервере приложения.");
                    }

                    // отключаем логирование в базу и прочие системные штуки
                    final IEventServiceLock eventLock = CoreServices.eventService().lock();
                    Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

                    try
                    {
                        BaseIODao.setupProcessState(state);
                        BaseIODao.doLoggedAction(() -> {
                            BaseIODao.getLog4jLogger().info("Import for " + fileName + " has been started.");
                            try
                            {
                                // открываем файл
                                try (Database mdb = Database.open(file))
                                {
                                    // запускаем импорт
                                    final Map<String, Long> value = IFefuOrdersIOAddDataDao.instance.get().doImport_LotusOrdersData(mdb, fileName, columnName, extractCategoryCode);
                                    BaseIODao.getLog4jLogger().info("Import for " + fileName + " was finished. Done.");
                                    return value;
                                }
                            }
                            catch (Exception t)
                            {
                                BaseIODao.getLog4jLogger().info("Import for " + fileName + " was finished. Error.");
                                throw CoreExceptionUtils.getRuntimeException(t);
                            }
                        }, "importOrdersFromLotus");

                    }
                    catch (final Exception t)
                    {

                        if (t instanceof ApplicationException)
                        {
                            // пробрасываем ApplicationException, как есть
                            throw (ApplicationException) t;
                        }

                        Debug.exception(t.getMessage(), t);
                        throw new ApplicationException("Импорт данных провести не удалось: произошла ошибка при обращении к файлу импорта. Обратитесь к логу импорта (файл importOrdersFromLotus.log) на сервере приложения для получения детальной информации об ошибке.", t);

                    }
                    finally
                    {
                        // включаем штуки и логирование обратно
                        Debug.resumeLogging();
                        eventLock.release();
                    }
                }

                // если все ок, просто закрываем диалог
                return new ProcessResult("Импорт приказов из Lotus завершен."); // закрываем диалог
            }
        };

        new BackgroundProcessHolder().start("Импорт данных о приказах студентов", process, ProcessDisplayMode.unknown);
    }

    public void onClickMakeArchivalEmptyGroupDuplicates()
    {
        FefuSystemActionManager.instance().dao().makeArchivalEmptyGroupDuplicates();
    }

    public void onClickMakeArchivalEmptyGroups()
    {
        FefuSystemActionManager.instance().dao().makeArchivalEmptyGroups();
    }

    public void onClickMakeArchivalGroupsWithNotActiveStudents()
    {
        FefuSystemActionManager.instance().dao().makeArchivalGroupsWithNotActiveStudents();
    }

    public void onClickArchiveEduPlanVersions()
    {
        getActivationBuilder().asRegionDialog(FefuSystemActionArchiveEduPlanVersions.class).activate();
    }

    public void onClickAssignForeignStudentsDeclinationSettings()
    {
        FefuSystemActionManager.instance().dao().doAssignForeignStudentsDeclinationSettings();
    }

    public void onClickChangeEntranceYearForArchiveStudents()
    {
        FefuSystemActionManager.instance().dao().doChangeEntranceYearForArchiveStudents();
    }

    public void onClickFillEduLevelQualifications() throws Exception
    {
        FefuSystemActionManager.instance().dao().doFillEduLevelQualifications();
    }

    public void onClickGetPersonsPersonalData()
    {
        getActivationBuilder().asRegionDialog(FefuSystemActionGetPersonalData.class).activate();
    }

    public void onClickGetPersonsPersonalDataLibrary()
    {
        getActivationBuilder().asRegionDialog(FefuSystemActionGetLibraryPersonalData.class).activate();
    }

    public void onClickImportContracts()
    {
        getActivationBuilder().asRegionDialog(FefuSystemActionImportContracts.class).activate();
    }

    public void onClickTestIdmPersonRegistrationService() throws Exception
    {
        //FefuSystemActionManager.instance().dao().doTestIdmPersonRegistrationService();
        getActivationBuilder().asRegionDialog(FefuSystemActionTestIDMStuff.class).activate();
    }

    public void onClickFixCompetitionGroups()
    {
        getActivationBuilder().asRegionDialog(FefuSystemActionCompetitionGroupFix.class).activate();
    }

    public void onClickMoveOriginals()
    {
        getActivationBuilder().asRegionDialog(FefuSystemActionMoveOriginals.class).activate();
    }

    public void onClickActualizePersonGuids()
    {
        IFEFUPersonGuidDaemonDao.instance.get().doActualizePersonGuidsTable();
    }

    public void onClickLoadDataFromImtsa()
    {
        ImtsaLoader.load();
    }

    public void onClickExportStudentsToTable()
    {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                FEFUAllStudExportDaemonDAO.DAEMON.waitForComplete();

                FEFUAllStudExportDaemonDAO.lockDaemon();

                try
                {
                    IFEFUAllStudExportDaemonDAO.instance.get().doDeleteAllEntities();

                    final Collection<Long> ids = IFEFUAllStudExportDaemonDAO.instance.get().getPersonIds(false);
                    if (ids.size() > 0)
                    {
                        for (List<Long> idsPart : Iterables.partition(ids, 128)) {
                            IFEFUAllStudExportDaemonDAO.instance.get().export_Person(idsPart);
                        }
                    }

                }
                catch (final Exception t)
                {
                    FEFUAllStudExportDaemonDAO.unlockDaemon();
                    Debug.exception(t.getMessage(), t);
                }

                FEFUAllStudExportDaemonDAO.unlockDaemon();
                return null;
            }
        };
        new BackgroundProcessHolder().start("Выгрузка студентов", process, ProcessDisplayMode.unknown);
    }

    public void onClickExportStudentOrdersToTable()
    {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                try
                {
                    IFEFUMdbViewOrdersDaemonDAO.instance.get().deleteAllEntities();

                    final Collection<Long> idsOrders = IFEFUMdbViewOrdersDaemonDAO.instance.get().getExtractIds(true);
                    final Collection<Long> idsOtherOrders = IFEFUMdbViewOrdersDaemonDAO.instance.get().getOtherExtractIds(true);

                    if (idsOtherOrders.size() != 0)
                    {
                        for (List<Long> idsPart : Iterables.partition(idsOtherOrders, 128)) {
                            IFEFUMdbViewOrdersDaemonDAO.instance.get().export_OtherOrders(idsPart);
                        }
                    }
                    if (idsOrders.size() != 0)
                    {
                        for (List<Long> idsPart : Iterables.partition(idsOrders, 128)) {
                            IFEFUMdbViewOrdersDaemonDAO.instance.get().export_Orders(idsPart);
                        }
                    }

                }
                catch (final Exception t)
                {
                    Debug.exception(t.getMessage(), t);
                }
                return null;
            }
        };
        new BackgroundProcessHolder().start("Выгрузка приказов по студентам", process, ProcessDisplayMode.unknown);
    }

    public void onClickExportStudentOrdersToMDB()
    {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                _tempMdbFileId = PrintReportTemporaryStorage.registerTemporaryPrintForm(getStudentOrdersMDB(), "studentOrders-" + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()) + ".mdb");
                return null;
            }
        };

        new BackgroundProcessHolder().start("Выгрузка приказов по студентам", process, ProcessDisplayMode.unknown);
    }

    private byte[] getStudentOrdersMDB()
    {
        try
        {
            final File file = File.createTempFile("mdb-io-", "mdb");
            try
            {
                final Database mdb = Database.create(Database.FileFormat.V2003, file, false);
                FefuSystemActionManager.instance().dao().export_StudentOrdersList(mdb);
                mdb.close();
                return FileUtils.readFileToByteArray(file);
            }
            finally
            {
                if (!file.delete())
                    System.out.println("Can't delete temp file: " + file.getPath());
            }
        }
        catch (final Exception t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onClickSendEntrantPortalNotifications()
    {
        getActivationBuilder().asRegionDialog(FefuSystemActionSendPortalNotifications.class).activate();
    }

    public void onClickOnlineEntrantSendNotifications()
    {
        getActivationBuilder().asDesktopRoot(FefuSystemActionOnlineEntrantNotifications.class).activate();
    }

    public void onClickProcessIdmRequests()
    {
        getActivationBuilder().asDesktopRoot(FefuSystemActionIDMResponsesProcessing.class).activate();
    }

    public void onClickSendStudentsToArchive()
    {
        getActivationBuilder().asDesktopRoot(FefuSystemActionStudentsSendToArchive.class).activate();
    }

    public void onClickFefuStudentsExport()
    {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                _tempMdbFileId = PrintReportTemporaryStorage.registerTemporaryPrintForm(UniSystemActionPubAddon.getTemplate4Student(true),
                                                                                        "all-students-" + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()) + ".mdb");
                return null;
            }
        };

        new BackgroundProcessHolder().start("Выгрузка студентов", process, ProcessDisplayMode.unknown);
    }

    public void onClickSyncStudentsWithNSI()
    {
        getActivationBuilder().asDesktopRoot(NSISyncPersonSync.class).activate();
    }

    private byte[] getPersonEduInstitutionsDump()
    {
        try
        {
            final File file = File.createTempFile("person-edu-institution-io-", null);
            try
            {
                FefuSystemActionManager.instance().dao().doExportEduInstitutions(file);
                return FileUtils.readFileToByteArray(file);
            }
            finally
            {
                if (!file.delete())
                    System.out.println("Can't delete temp file: " + file.getPath());
            }
        }
        catch (final Exception t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    private static final String EDU_INSTITUTION_RECOVERY_FILE_NAME = "person-edu-institution.csv";

    public void onExportEduInstitution()
    {
        downloadTempFile(PrintReportTemporaryStorage.registerTemporaryPrintForm(getPersonEduInstitutionsDump(), EDU_INSTITUTION_RECOVERY_FILE_NAME), "csv");
    }

    public void onImportEduInstitution()
    {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                String path = ApplicationRuntime.getAppInstallPath();
                File file = new File(path, "data/" + EDU_INSTITUTION_RECOVERY_FILE_NAME);
                if (!file.exists())
                {
                    throw new ApplicationException("Импорт провести не удалось: не найден файл «" + EDU_INSTITUTION_RECOVERY_FILE_NAME + "» с исходными данными в каталоге «${app.install.path}/data/». Разместите файл для импорта с указанным названием на сервере приложения по указанному пути.");
                }
                if (!file.canRead())
                {
                    throw new ApplicationException("Импорт провести не удалось: файл «${app.install.path}/data/" + EDU_INSTITUTION_RECOVERY_FILE_NAME + "» не доступен для чтения. Проверьте доступ к файлу на сервере приложения.");
                }
                String result = FefuSystemActionManager.instance().dao().doImportEduInstitutions(file);

                return new ProcessResult("Импорт завершен: восстановлено " + result + " записей.");
            }
        };

        new BackgroundProcessHolder().start("Импорт потерянной части данных об образовании", process, ProcessDisplayMode.unknown);
    }

    private static final String EMPLOYEES_MDB_FILE_NAME = "employees.mdb";

    public void onExportEmployees()
    {
        try
        {
            File file = File.createTempFile("mdb-io-employees-", "mdb");
            try
            {
                Database mdb = Database.create(Database.FileFormat.V2003, file, false);
                IFefuEmployeeIODao.instance.get().exportEmployees(mdb);
                mdb.close();

                downloadTempFile(PrintReportTemporaryStorage.registerTemporaryPrintForm(FileUtils.readFileToByteArray(file), EMPLOYEES_MDB_FILE_NAME), "mdb");
            }
            finally
            {
                if (!file.delete())
                    System.out.println("Can't delete temp file: " + file.getPath());
            }
        }
        catch (final Exception t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onImportEmployees()
    {
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                String path = ApplicationRuntime.getAppInstallPath();
                File file = new File(path, "data/" + EMPLOYEES_MDB_FILE_NAME);

                if (!file.exists())
                    throw new ApplicationException("Импорт провести не удалось: не найден файл «" + EMPLOYEES_MDB_FILE_NAME + "» с исходными данными в каталоге «${app.install.path}/data/». Разместите файл для импорта с указанным названием на сервере приложения по указанному пути.");

                if (!file.canRead())
                    throw new ApplicationException("Импорт провести не удалось: файл «${app.install.path}/data/" + EMPLOYEES_MDB_FILE_NAME + "» не доступен для чтения. Проверьте доступ к файлу на сервере приложения.");

                try (final Database mdb = Database.open(file))
                {
                    IFefuEmployeeIODao.instance.get().doImportEmployees(mdb);
                }
                catch (Exception t)
                {
                    BaseIODao.getLog4jLogger().info("Import finished with error.");
                    throw CoreExceptionUtils.getRuntimeException(t);
                }

                return new ProcessResult("Импорт успешно завершен.");
            }
        };

        new BackgroundProcessHolder().start("Импорт сотрудников", process, ProcessDisplayMode.unknown);
    }

    public void onArchivePracticeContract()
    {
        FefuSystemActionManager.instance().dao().doArchivePracticeContract();
    }


    public void onClickReImportImtsa()
    {
        getActivationBuilder().asDesktopRoot(FefuEduPlanImtsaReImport.class).activate();
    }

    private static void processCurrentVersionBlocks(List<Long> blockIds, boolean global, Map<Long, FefuImtsaXml> programSpecImtsaXmlMap, List<String> errorList)
    {
        // выключаем системное логирование, чтобы не тормозило
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        Debug.stopLogging();

        try
        {
            EppEduPlanVersionBlock root;
            List<EppEduPlanVersionBlock> blocks = DataAccessServices.dao().getList(EppEduPlanVersionBlock.class, blockIds);
            List<EppEduPlanVersionBlock> newBlocks = blocks;

            if (global)
            {
                root = CollectionUtils.find(blocks, block -> block instanceof EppEduPlanVersionRootBlock);

                blocks.remove(root);
                boolean rootFromImtsa = programSpecImtsaXmlMap.containsKey(rootBlockSpecializationId);
                if (!rootFromImtsa)
                {
                    // если основной блок не был импортирован из ИМЦА, не трогаем не импортированные
                    for (EppEduPlanVersionBlock block : new ArrayList<>(blocks))
                        if (!programSpecImtsaXmlMap.containsKey(((EppEduPlanVersionSpecializationBlock) block).getProgramSpecialization().getId()))
                            blocks.remove(block);
                }

                newBlocks = new ArrayList<>();
                if (rootFromImtsa)
                {
                    final Long rootId = root.getId();
                    EppEduPlanVersionRootBlock newRoot = new EppEduPlanVersionRootBlock();
                    newRoot.setEduPlanVersion(root.getEduPlanVersion());
                    newBlocks.add(newRoot);

                    DataAccessServices.dao().doInTransaction(
                            session -> new DQLDeleteBuilder(EppEduPlanVersionRootBlock.class)
                                    .where(eq(property(EppEduPlanVersionRootBlock.id()), value(rootId)))
                                    .createStatement(session).execute()
                    );
                }

                for (final EppEduPlanVersionBlock block : blocks)
                {
                    newBlocks.add(new EppEduPlanVersionSpecializationBlock(block.getEduPlanVersion(), ((EppEduPlanVersionSpecializationBlock) block).getProgramSpecialization()));
                    DataAccessServices.dao().doInTransaction(
                            session -> new DQLDeleteBuilder(EppEduPlanVersionBlock.class)
                                    .where(eq(property(EppEduPlanVersionBlock.id()), value(block.getId())))
                                    .createStatement(session).execute()
                    );
                }
            }

            for (final EppEduPlanVersionBlock block : newBlocks)
            {
                if (global)
                    DataAccessServices.dao().doInTransaction(session -> session.save(block));

                FefuImtsaXml xml = programSpecImtsaXmlMap.get(block instanceof EppEduPlanVersionRootBlock ? rootBlockSpecializationId : ((EppEduPlanVersionSpecializationBlock) block).getProgramSpecialization().getId());
                if (xml == null)
                {
                    continue;
                }

                if (global)
                    DataAccessServices.dao().save(new FefuImtsaXml(block, xml.getFileName(), xml.getEncoding(), xml.getXml(), xml.getNumber()));

                String xmlFileName = xml.getFileName();
                File file = null;
                boolean secondGosGeneration = block.getEduPlanVersion().getEduPlan().getGeneration().getNumber() == 2;
                try
                {
                    Document document = null;
                    if (xml.getEncoding() != null)
                    {
                        try
                        {
                            file = File.createTempFile("fefuTemp-", "xml");
                            FileUtils.writeStringToFile(file, new String(xml.getXml()), xml.getEncoding());
                            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                            DocumentBuilder db = dbf.newDocumentBuilder();
                            document = db.parse(file);
                            document.getDocumentElement().normalize();

                        }
                        catch (Exception e)
                        {
                            throw new ApplicationException("Неверная структура xml в файле».");
                        }

                    }
                    else
                    {
                        // костыль, нужно для повторной загрузки файлов ИМЦА
                        boolean successXmlParse = false;
                        for (String encoding : Arrays.asList("UTF-8", "windows-1251"))
                        {
                            if (successXmlParse)
                            {
                                break;
                            }
                            try
                            {
                                file = File.createTempFile("fefuTemp-", "xml");
                                FileUtils.writeStringToFile(file, new String(xml.getXml()), encoding);
                                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                                DocumentBuilder db = dbf.newDocumentBuilder();
                                document = db.parse(file);
                                document.getDocumentElement().normalize();
                                successXmlParse = true;

                            }
                            catch (Exception e)
                            {
                                // continue;
                            }
                        }

                        if (!successXmlParse)
                        {
                            throw new ApplicationException("Неверная структура xml в файле».");
                        }
                    }

                    ImtsaParser parser = ImtsaParser.get(document, secondGosGeneration);
                    parser.parse();


                    String blockError = null;
                    if (parser.hasErrors())
                    {
                        StringBuilder errorBuilder = new StringBuilder();
                        for (String error : parser.getErrors())
                        {
                            errorBuilder.append(error).append("\n");
                        }

                        blockError = errorBuilder.toString();
                    }

                    ImtsaFile imtsa = parser.getResult();
                    Map<String, EppPlanStructure> defaultPartsMap = new HashMap<>();
                    for (EppPlanStructure planStructure : FefuSystemActionManager.instance().dao().getParts(secondGosGeneration))
                    {
                        defaultPartsMap.put(planStructure.getShortTitle(), planStructure);
                    }

                    Map<String, EppPlanStructure> defaultCyclesMap = new HashMap<>();
                    for (FefuImtsaCyclePlanStructureRel rel : FefuSystemActionManager.instance().dao().getImtsaCycles(secondGosGeneration))
                    {
                        defaultCyclesMap.put(rel.getImtsaCycle(), rel.getPlanStructure());
                    }

                    Map<String, Set<String>> cycle2PartsMap = new TreeMap<>();
                    for (Map.Entry<String, ImtsaPlanRow> cycleEntry : imtsa.getRootPlanRow().getChildMap().entrySet())
                    {
                        String cycleId = cycleEntry.getKey();
                        Set<String> parts = new TreeSet<>();
                        ru.tandemservice.unifefu.component.eduplan.FefuImtsaImport.Controller.addParts(cycleEntry.getValue().getChildMap(), cycleId, parts);
                        cycle2PartsMap.put(cycleId, parts);
                    }


                    Map<String, String> abbreviation2TitleMap = imtsa.getTitle().getCycles().getAbbreviation2TitleMap();
                    Map<String, EppPlanStructure> cyclesMap = new LinkedHashMap<>();
                    Map<String, EppPlanStructure> partsMap = new LinkedHashMap<>();
                    for (Map.Entry<String, Set<String>> cycleEntry : cycle2PartsMap.entrySet())
                    {
                        String cycleAbbr = cycleEntry.getKey();
                        String cycleTitle = abbreviation2TitleMap.get(cycleAbbr);
                        EppPlanStructure cycleValue = defaultCyclesMap.get(cycleTitle);
                        if (cycleValue == null)
                        {
                            cycleValue = defaultCyclesMap.get(cycleAbbr);
                        }
                        cyclesMap.put(cycleAbbr, cycleValue);

                        for (String part : cycleEntry.getValue())
                        {
                            partsMap.put(part, defaultPartsMap.get(part.substring(part.lastIndexOf(".") + 1)));
                        }
                    }

                    List<String> errors = new ArrayList<>();
                    IImtsaImportDAO.instance.get().saveImtsa(block, imtsa, cyclesMap, partsMap, errors);
                    if (!errors.isEmpty())
                    {
                        String error = StringUtils.join(new TreeSet<>(errors), "\n");
                        blockError = blockError == null ? error : blockError + "\n" + error;

                        FefuSystemActionManager.instance().dao().saveOrRewriteImportLog(block, blockError);
                    }

                }
                catch (Exception e)
                {
                    errorList.add("Произошла ошибка при импорте файла " + xmlFileName + ".");

                }
                finally
                {
                    file.delete();
                }
            }
        }
        finally
        {
            // включаем системное логирование
            eventLock.release();
            Debug.resumeLogging();
        }
    }

    private static final long rootBlockSpecializationId = -1L; // для совместимости

    public static void doReImportImtsa(@Nullable Collection<Long> blockIds, final boolean global)
    {
        final List<String> errorList = new ArrayList<>();
        //  version -> specialization -> xml
        final Map<Long, Map<Long, FefuImtsaXml>> versionEduHSImtsaXmlMap = SafeMap.get(HashMap.class);
        for (FefuImtsaXml imtsaXml : FefuSystemActionManager.instance().dao().getXmlList(blockIds))
        {
            EppEduPlanVersionBlock block = imtsaXml.getBlock();
            versionEduHSImtsaXmlMap.get(block.getEduPlanVersion().getId()).put(block instanceof EppEduPlanVersionRootBlock ? rootBlockSpecializationId : ((EppEduPlanVersionSpecializationBlock) block).getProgramSpecialization().getId(), imtsaXml);
        }

        final Map<Long, String> versionStateCodeMap = new HashMap<>();
        DQLSelectBuilder versionStateCodeBuilder = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersionBlock.class, "b")
                .column(property("b", EppEduPlanVersionBlock.eduPlanVersion().id()))
                .column(property("b", EppEduPlanVersionBlock.eduPlanVersion().state().code()))
                .where(in(property("b", EppEduPlanVersionBlock.id()), blockIds));

        for (Object[] row : DataAccessServices.dao().<Object[]>getList(versionStateCodeBuilder))
        {
            versionStateCodeMap.put((Long) row[0], (String) row[1]);
        }

        final Map<EppEduPlanVersion, List<EppEduPlanVersionBlock>> versionBlockMap = SafeMap.get(ArrayList.class);
        for (EppEduPlanVersionBlock block : DataAccessServices.dao().getList(EppEduPlanVersionBlock.class, blockIds))
        {
            versionBlockMap.get(block.getEduPlanVersion()).add(block);
        }

        final int size = versionBlockMap.size();
        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                int i = 1;

                for (Map.Entry<EppEduPlanVersion, List<EppEduPlanVersionBlock>> versionEntry : versionBlockMap.entrySet())
                {
                    try
                    {
                        if (!versionStateCodeMap.get(versionEntry.getKey().getId()).equals(EppState.STATE_FORMATIVE))
                        {
                            errorList.add("Состояние версии УП " + versionEntry.getKey().getTitle() + " запрещает редактирование.");
                            state.setCurrentValue(Math.round(100 * (i++) / size));
                            continue;
                        }

                        List<Long> ids = new ArrayList<>(UniBaseDao.ids(versionEntry.getValue()));
                        processCurrentVersionBlocks(ids, global, versionEduHSImtsaXmlMap.get(versionEntry.getKey().getId()), errorList);

                    }
                    catch (RuntimeException e)
                    {
                        errorList.add("Произошла ошибка при импорте версии УП " + versionEntry.getKey().getTitle() + ".");

                    }
                    finally
                    {
                        state.setCurrentValue(Math.round(100 * (i++) / size));
                    }
                }

                for (String error : errorList)
                {
                    ContextLocal.getErrorCollector().add(error);
                }

                return new ProcessResult("Повторная загрузка файлов ИМЦА успешно проведена.");
            }
        };

        new BackgroundProcessHolder().start("Повторная загрузка файлов ИМЦА", process, ProcessDisplayMode.percent);
    }

    public void onClickDeleteAllFefuScheduleICalData()
    {
        FefuSystemActionManager.instance().dao().doDeleteAllFefuScheduleICalData();
    }

    public void onClickRemoveOtherOrderDuplicates()
    {
        Set<Long> ids = FefuSystemActionManager.instance().dao().getOtherOrdersDuplicates();

        for (List<Long> elements : Iterables.partition(ids, 64)) {
            FefuSystemActionManager.instance().dao().doDeleteOtherOrdersDuplicates(elements);
        }
    }

    public void onClickRegenExtractPrintFormsDEV5552()
    {
        /*
        Реализовать системное действие для перегенерации выписок данного типа, сформированных
        (см. дату формирования) с 01.05.2014 по 20.07.2014. (около 700 приказов в БД ДВФУ).
        Назвать "Перегенерировать выписки из списочных приказов о присвоении квалификации" в подсказке написать
        "Системное действие позволяет переформировать выписки из списочных приказов "О присвоении квалификации, выдаче диплома и отчислении в связи с окончанием университета"
        созданных в период с 01.05.2014 по 20.07.2014"
        */
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2014);

        calendar.set(Calendar.MONTH, Calendar.MAY);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date formingDateFrom = calendar.getTime();

        calendar.set(Calendar.MONTH, Calendar.JULY);
        calendar.set(Calendar.DAY_OF_MONTH, 20);
        Date formingDateTo = calendar.getTime();

        FefuSystemActionManager.instance().dao().regenerateExtractPrintForms(
				Arrays.asList(StudentExtractTypeCodes.FEFU_GIVE_GENERAL_DIPLOMA_WITH_DISMISS_LIST_EXTRACT, StudentExtractTypeCodes.FEFU_GIVE_DIPLOMA_WITH_HONOURS_WITH_DISMISS_LIST_EXTRACT),
				formingDateFrom, formingDateTo);
    }

    public void onClickRegenExtractPrintFormsDEV5570()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2014);

        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date formingDateFrom = calendar.getTime();

        calendar.set(Calendar.MONTH, Calendar.JULY);
        calendar.set(Calendar.DAY_OF_MONTH, 25);
        Date formingDateTo = calendar.getTime();

        FefuSystemActionManager.instance().dao().regenerateExtractPrintForms(
				Arrays.asList(StudentExtractTypeCodes.FEFU_ADMIT_TO_STATE_EXAMS_LIST_EXTRACT, StudentExtractTypeCodes.FEFU_ADMIT_TO_DIPLOMA_DEFEND_WORK_LIST_EXTRACT),
				formingDateFrom, formingDateTo);
    }

    public void onClickTest1C()
    {
        getActivationBuilder().asDesktopRoot(FefuSystemActionTest1C.class).activate();
    }

    public void onClickSendBlaBlaBlaRatingToPortal()
    {
        IFEFUPortalStatusesDaemonDao.instance.get().doSendPortalStatuses();
    }


    public void onClickImportMdbView()
    {
        // отключаем логирование в базу
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        Debug.stopLogging();
        try
        {

            IFEFUMdbViewDaemonDAO.instance.get().clearEntity();
            Collection<Long> collection = IFEFUMdbViewDaemonDAO.instance.get().exist(false);
            if (collection.size() != 0)
            {
                for (List<Long> ids : Iterables.partition(collection, 128)) {
                    IFEFUMdbViewDaemonDAO.instance.get().doExportMdbView(ids);

                }
            }
        }
        finally
        {
            // включаем штуки и логирование обратно
            Debug.resumeLogging();
            eventLock.release();
        }


    }

    public void onClickResetPersonSyncDateForFailedStudents()
    {
        FefuSystemActionManager.instance().dao().doResetPersonSyncDateForFailedStudents();
    }

    public void onClickMergePersonsAndUpdateTheirGuids()
    {
        getActivationBuilder().asRegionDialog(FefuSystemActionMergePersonsAndUpdateGuids.class).activate();
    }

    public void onClickRegenBadIdentityCardIds()
    {
        getActivationBuilder().asRegionDialog(FefuSystemActionRegenGuids.class).activate();
    }

    public void onClickTransferDataAboutTargetReception()
    {
        getActivationBuilder().asRegionDialog(FefuSystemActionTransferDataAboutTargetReception.class).activate();
    }

    public void onClickSendNotAcceptRegElementToArchive()
    {
        FefuSystemActionManager.instance().dao().doSendNotAcceptRegElementToArchive();
    }

    public void onClickLockAddressUpdateDaemon()
    {
        if (FefuNsiContactDaemonDao.isLocked()) FefuNsiContactDaemonDao.unlockDaemon();
        else FefuNsiContactDaemonDao.lockDaemon();
    }

    public void onClickDeleteNotCoordinatedOrders()
    {
        getActivationBuilder().asDesktopRoot(FefuSystemActionDeleteNotCoordinatedOrders.class).activate();
    }

    public void onClickImportImtsaCompetence()
    {
        //final StringBuilder resultMessage = new StringBuilder();
        String path = ApplicationRuntime.getAppInstallPath();
        final File importFile = new File(path, "data/competence.mdb");
        if (!importFile.exists())
        {
            throw new ApplicationException("Импорт данных провести не удалось: не найден файл «competence.mdb» с исходными данными в каталоге «${app.install.path}/data/». Разместите файл для импорта с указанным названием на сервере приложения по указанному пути.");
        }
        if (!importFile.canRead())
        {
            throw new ApplicationException("Импорт данных провести не удалось: файл «${app.install.path}/data/competence.mdb» не доступен для чтения. Проверьте доступ к файлу на сервере приложения.");
        }
        new BackgroundProcessHolder().start("Импорт компетенций из справочника ИМЦА", new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                String output;
                try (Database mdb = Database.open(importFile))
                {
                    // запускаем импорт
                    output = IFefuImtsaCompetenceIODao.instance.get().doImportImtsaCompetence(mdb);
                }
                catch (Exception e)
                {
                    throw new ApplicationException("Импорт данных провести не удалось. "+e.getMessage());
                }
                return new ProcessResult("Импорт компетенций из справочника ИМЦА: " + output);
            }
        }, ProcessDisplayMode.unknown);

    }

    public void onClickInitGuids()
    {
        FefuSystemActionManager.instance().dao().doGenerateGuids();
    }
}