package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x0_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        		// сущность fefuScheduleSessionPrintForm

        		// сущность была удалена
        		{

        			// удалить таблицу
        			tool.dropTable("fefuschedulesessionprintform_t", false /* - не удалять, если есть ссылающиеся таблицы */);

        			// удалить код сущности
        			tool.entityCodes().delete("fefuScheduleSessionPrintForm");

        		}

        ////////////////////////////////////////////////////////////////////////////////
        		// сущность fefuScheduleSessionEvent

        		// сущность была удалена
        		{
        			// удалить таблицу
        			tool.dropTable("fefuschedulesessionevent_t", false /* - не удалять, если есть ссылающиеся таблицы */);

        			// удалить код сущности
        			tool.entityCodes().delete("fefuScheduleSessionEvent");

        		}

        ////////////////////////////////////////////////////////////////////////////////

        // сущность fefuScheduleSession

        		// сущность была удалена
        		{


        			// удалить таблицу
        			tool.dropTable("fefuschedulesession_t", false /* - не удалять, если есть ссылающиеся таблицы */);

        			// удалить код сущности
        			tool.entityCodes().delete("fefuScheduleSession");

        		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuScheduleSessionSeason

		// сущность была удалена
		{


			// удалить таблицу
			tool.dropTable("fefuschedulesessionseason_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuScheduleSessionSeason");

		}
    }
}