/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.student.FefuForeignStudentDataEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 14.02.2013
 */
public interface IDAO extends IUniDao<Model>
{
}