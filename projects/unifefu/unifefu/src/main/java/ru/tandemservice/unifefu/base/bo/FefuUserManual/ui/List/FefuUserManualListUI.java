/**
 *$Id: FefuUserManualListUI.java 38584 2014-10-08 11:05:36Z azhebko $
 */
package ru.tandemservice.unifefu.base.bo.FefuUserManual.ui.List;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.data.ItemPolicy;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.UserManual.ui.AddEdit.UserManualAddEdit;
import org.tandemframework.shared.commonbase.base.bo.UserManual.ui.List.UserManualList;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.entity.UserManual;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.zip.ZipCompressionInterface;

/**
 * @author Rostuncev
 * @since 28.11.12
 */
public class FefuUserManualListUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (UserManualList.USER_MANUAL_SEARCH_DS.equals(dataSource.getName()))
            dataSource.put("title", getSettings().get("title"));
    }

    public void onClickAdd()
    {
        getActivationBuilder().asRegionDialog(UserManualAddEdit.class)
                .activate();
    }

    public void onClickDownload()
    {
        final UserManual userManual = DataAccessServices.dao().get(UserManual.class, getListenerParameterAsLong());
        final DatabaseFile file = userManual.getDatabaseFile();
        if (file == null || file.getContent() == null) {
            throw new ApplicationException("Инструкция не содержит файл.");
        }
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
                                                        .document(ZipCompressionInterface.INSTANCE.decompress(file.getContent()))
                                                        .fileName(userManual.getFileName()), true);
    }

    public void onClickEdit()
    {
        getActivationBuilder().asRegionDialog(UserManualAddEdit.class)
                .parameter("id", getListenerParameterAsLong())
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public boolean isDeleteDisabled()
    {
        UserManual current = getConfig().getDataSource(UserManualList.USER_MANUAL_SEARCH_DS).getCurrent();

        ItemPolicy itemPolicy = EntityDataRuntime.getEntityPolicy(UserManual.ENTITY_NAME).getItemPolicyById(current.getCode());

        return itemPolicy != null && !SynchronizeMeta.temp.equals(itemPolicy.getSynchronize());
    }
}
