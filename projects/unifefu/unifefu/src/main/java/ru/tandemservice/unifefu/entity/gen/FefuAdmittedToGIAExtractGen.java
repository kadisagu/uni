package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. «О допуске к государственной итоговой аттестации»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuAdmittedToGIAExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract";
    public static final String ENTITY_NAME = "fefuAdmittedToGIAExtract";
    public static final int VERSION_HASH = -1661377462;
    private static IEntityMeta ENTITY_META;

    public static final String P_SEASON = "season";
    public static final String P_YEAR = "year";

    private String _season;     // Семестр
    private int _year;     // Год проведения ГИА

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Семестр.
     */
    @Length(max=255)
    public String getSeason()
    {
        return _season;
    }

    /**
     * @param season Семестр.
     */
    public void setSeason(String season)
    {
        dirty(_season, season);
        _season = season;
    }

    /**
     * @return Год проведения ГИА. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год проведения ГИА. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuAdmittedToGIAExtractGen)
        {
            setSeason(((FefuAdmittedToGIAExtract)another).getSeason());
            setYear(((FefuAdmittedToGIAExtract)another).getYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuAdmittedToGIAExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuAdmittedToGIAExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuAdmittedToGIAExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "season":
                    return obj.getSeason();
                case "year":
                    return obj.getYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "season":
                    obj.setSeason((String) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "season":
                        return true;
                case "year":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "season":
                    return true;
                case "year":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "season":
                    return String.class;
                case "year":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuAdmittedToGIAExtract> _dslPath = new Path<FefuAdmittedToGIAExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuAdmittedToGIAExtract");
    }
            

    /**
     * @return Семестр.
     * @see ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract#getSeason()
     */
    public static PropertyPath<String> season()
    {
        return _dslPath.season();
    }

    /**
     * @return Год проведения ГИА. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    public static class Path<E extends FefuAdmittedToGIAExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<String> _season;
        private PropertyPath<Integer> _year;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Семестр.
     * @see ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract#getSeason()
     */
        public PropertyPath<String> season()
        {
            if(_season == null )
                _season = new PropertyPath<String>(FefuAdmittedToGIAExtractGen.P_SEASON, this);
            return _season;
        }

    /**
     * @return Год проведения ГИА. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(FefuAdmittedToGIAExtractGen.P_YEAR, this);
            return _year;
        }

        public Class getEntityClass()
        {
            return FefuAdmittedToGIAExtract.class;
        }

        public String getEntityName()
        {
            return "fefuAdmittedToGIAExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
