/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu16.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract;

/**
 * @author nvankov
 * @since 11/20/13
 */
public class Model extends ModularStudentExtractPubModel<FullStateMaintenanceStuExtract>
{
    private String _payments;
    private String _responsibleForPayments;
    private String _responsibleForAgreement;

    public String getPayments()
    {
        return _payments;
    }

    public void setPayments(String payments)
    {
        _payments = payments;
    }

    public String getResponsibleForPayments()
    {
        return _responsibleForPayments;
    }

    public void setResponsibleForPayments(String responsibleForPayments)
    {
        _responsibleForPayments = responsibleForPayments;
    }

    public String getResponsibleForAgreement()
    {
        return _responsibleForAgreement;
    }

    public void setResponsibleForAgreement(String responsibleForAgreement)
    {
        _responsibleForAgreement = responsibleForAgreement;
    }

    public boolean getHasPayments()
    {
        return !_payments.isEmpty();
    }
}
