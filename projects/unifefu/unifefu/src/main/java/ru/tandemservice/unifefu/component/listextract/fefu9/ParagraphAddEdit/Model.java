/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu9.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;
import ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class Model extends AbstractListParagraphAddEditModel<FefuFormativeTransferStuListExtract> implements IEducationLevelModel, IExtEducationLevelModel, IGroupModel
{
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _groupNewListModel;
    private ISelectModel _groupOldListModel;
    private ISelectModel _formativeOrgUnitsListModel;

    private Course _course;
    private Group _groupNew;
    private Group _groupOld;
    private OrgUnit _formativeOrgUnitNew;
    private OrgUnit _formativeOrgUnitOld;
    private CompensationType _compensationTypeOld;
    private CompensationType _compensationTypeNew;
    private String _formativeOrgUnitNewHint;

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getGroupNewListModel()
    {
        return _groupNewListModel;
    }

    public void setGroupNewListModel(ISelectModel groupNewListModel)
    {
        _groupNewListModel = groupNewListModel;
    }

    public ISelectModel getGroupOldListModel()
    {
        return _groupOldListModel;
    }

    public void setGroupOldListModel(ISelectModel groupOldListModel)
    {
        _groupOldListModel = groupOldListModel;
    }

    public ISelectModel getFormativeOrgUnitsListModel()
    {
        return _formativeOrgUnitsListModel;
    }

    public void setFormativeOrgUnitsListModel(ISelectModel formativeOrgUnitsListModel)
    {
        _formativeOrgUnitsListModel = formativeOrgUnitsListModel;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    public void setGroupNew(Group groupNew)
    {
        _groupNew = groupNew;
    }

    public Group getGroupOld()
    {
        return _groupOld;
    }

    public void setGroupOld(Group groupOld)
    {
        _groupOld = groupOld;
    }

    public OrgUnit getFormativeOrgUnitNew()
    {
        return _formativeOrgUnitNew;
    }

    public void setFormativeOrgUnitNew(OrgUnit formativeOrgUnitNew)
    {
        _formativeOrgUnitNew = formativeOrgUnitNew;
    }

    public OrgUnit getFormativeOrgUnitOld()
    {
        return _formativeOrgUnitOld;
    }

    public void setFormativeOrgUnitOld(OrgUnit formativeOrgUnitOld)
    {
        _formativeOrgUnitOld = formativeOrgUnitOld;
    }

    public CompensationType getCompensationTypeOld()
    {
        return _compensationTypeOld;
    }

    public void setCompensationTypeOld(CompensationType compensationTypeOld)
    {
        _compensationTypeOld = compensationTypeOld;
    }

    public CompensationType getCompensationTypeNew()
    {
        return _compensationTypeNew;
    }

    public void setCompensationTypeNew(CompensationType compensationTypeNew)
    {
        _compensationTypeNew = compensationTypeNew;
    }

    public String getFormativeOrgUnitNewHint()
    {
        return _formativeOrgUnitNewHint;
    }

    public void setFormativeOrgUnitNewHint(String formativeOrgUnitNewHint)
    {
        _formativeOrgUnitNewHint = formativeOrgUnitNewHint;
    }

    public boolean isAnyOrderParametersCantBeChanged()
    {
        return !isParagraphOnlyOneInTheOrder();
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return getFormativeOrgUnitNew();
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return getGroupOld() != null ? getGroupOld().getEducationOrgUnit().getTerritorialOrgUnit() : null;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return null;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return getGroupOld().getEducationOrgUnit().getDevelopForm();
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return getGroupOld().getEducationOrgUnit().getDevelopCondition();
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return getGroupOld().getEducationOrgUnit().getDevelopTech();
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return getGroupOld().getEducationOrgUnit().getDevelopPeriod();
    }

    @Override
    public EducationLevels getParentEduLevel()
    {
        return EducationOrgUnitUtil.getParentLevel(getGroupNew().getEducationOrgUnit().getEducationLevelHighSchool());
    }
}
