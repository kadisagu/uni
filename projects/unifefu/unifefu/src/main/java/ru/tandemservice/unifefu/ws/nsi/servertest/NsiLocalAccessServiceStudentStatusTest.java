/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import org.apache.axis.AxisFault;
import ru.tandemservice.unifefu.ws.nsi.datagram.StudentStatusType;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceStudentStatusTest
{
    public static void main(String[] args) throws Exception
    {
        try
        {
            retrieveWholeCatalog();
            //retrieveSingleNonExistElement();
            //retrieveSingleElement();
            //retrieveFewElements();

            //testInsertNew();
            //testInsertNewAnalog();
            //testInsertNewGuidExist();
            //testInsertNewMergeGuid();
            //testInsertMassElementsList();

            //testDeleteEmpty();
            //testDeleteNonExist();
            //testDeleteSingle();
            //testDeleteNonDeletable();
            //testDeleteMassDelete();
        } catch (Exception e)
        {
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            throw e;
        }
    }

    private static void retrieveWholeCatalog() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        objects.add(NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType());
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void retrieveSingleNonExistElement() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentStatusType studentStatusType = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        studentStatusType.setID("aaaaaaaa-db4e-3b3f-9a8e-3b8da4440b7d");
        objects.add(studentStatusType);
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void retrieveSingleElement() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentStatusType studentStatusType = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        studentStatusType.setID("f7656d2f-fecc-3e8a-b545-f96e10887b9d");
        objects.add(studentStatusType);
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void retrieveFewElements() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        StudentStatusType studentStatusType1 = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        studentStatusType1.setID("f7656d2f-fecc-3e8a-b545-f96e10887b9d");
        objects.add(studentStatusType1);

        StudentStatusType studentStatusType2 = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        studentStatusType2.setID("0b6a0f7f-8eb3-30b2-9701-8541ad0e8322");
        objects.add(studentStatusType2);

        StudentStatusType studentStatusType3 = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        studentStatusType3.setID("aaaaaaaa-db4e-3b3f-9a8e-3b8da4440b7d");
        objects.add(studentStatusType3);

        StudentStatusType studentStatusType4 = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        studentStatusType4.setID("ffb9a9d3-d006-37ac-82ca-03677bd48af2");
        objects.add(studentStatusType4);

        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void testInsertNew() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        StudentStatusType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        nsientity.setID("20e64b24-e15e-3d1d-91fd-xxxxxxxxxxxX");
        nsientity.setStudentStatusName("Пассивный");
        nsientity.setStudentStatusNameShort("пасс.");
        nsientity.setStudentStatusActive("1");
        nsientity.setStudentStatusUsedinSystem("1");
        nsientity.setStudentStatusPriority("-1");
        objects.add(nsientity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertNewAnalog() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        StudentStatusType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        nsientity.setID("a04cb766-d0ec-3010-a619-xxxxxxxxxxxY");
        nsientity.setStudentStatusName("Академ. отпуск");
        nsientity.setStudentStatusNameShort("Ак");
        nsientity.setStudentStatusActive("1");
        nsientity.setStudentStatusUsedinSystem("1");
        nsientity.setStudentStatusPriority("155");
        objects.add(nsientity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertNewGuidExist() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        StudentStatusType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        nsientity.setID("08a42e17-c837-3749-9967-e6887cccfa63");
        nsientity.setStudentStatusName("Академ. отпуск1");
        nsientity.setStudentStatusNameShort("Ак1");
        nsientity.setStudentStatusActive("1");
        nsientity.setStudentStatusUsedinSystem("1");
        nsientity.setStudentStatusPriority("155");
        objects.add(nsientity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertNewMergeGuid() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        StudentStatusType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        nsientity.setID("08a42e17-c837-3749-9967-e6887cccfa63");
        nsientity.setStudentStatusName("Академ. отпуск1");
        nsientity.setStudentStatusNameShort("Ак");
        nsientity.setStudentStatusActive("1");
        nsientity.setStudentStatusUsedinSystem("1");
        nsientity.setStudentStatusPriority("5");
        nsientity.getOtherAttributes().put(new QName("mergeDuplicates"), "08a42e17-c837-3749-9967-e6887cccfa63; a04cb766-d0ec-3010-a619-xxxxxxxxxxxY; 90800349-619a-11e0-a335-xxxxxxxxxx31");
        objects.add(nsientity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertMassElementsList() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        for (int i = 1; i < 1000; i++)
        {
            StudentStatusType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
            nsientity.setID(UUID.nameUUIDFromBytes(String.valueOf(i).getBytes()).toString());
            nsientity.setStudentStatusName("Тест" + i);
            nsientity.setStudentStatusNameShort("Т" + i);
            nsientity.setStudentStatusActive("1");
            nsientity.setStudentStatusUsedinSystem("1");
            nsientity.setStudentStatusPriority("-1");
            objects.add(nsientity);
        }

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testDeleteEmpty() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentStatusType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        objects.add(nsientity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteNonExist() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentStatusType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        nsientity.setID("08a42e17-c837-3749-9967-e6887cccfa6X");
        objects.add(nsientity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteSingle() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentStatusType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        nsientity.setID("08a42e17-c837-3749-9967-e6887cccfa63");
        objects.add(nsientity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteNonDeletable() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentStatusType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
        nsientity.setID("08a42e17-c837-3749-9967-e6887cccfa63");
        objects.add(nsientity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteMassDelete() throws Exception
    {
        String[] guidsToDelete = new String[]{
                "08a42e17-c837-3749-9967-e6887cccfa63"
        };

        List<Object> objects = new ArrayList<>();
        for (String guid : guidsToDelete)
        {
            StudentStatusType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentStatusType();
            nsientity.setID(guid);
            objects.add(nsientity);
        }

        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }
}