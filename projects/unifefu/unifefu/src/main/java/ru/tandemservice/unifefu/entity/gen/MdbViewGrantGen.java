package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unifefu.entity.MdbViewGrant;
import ru.tandemservice.unifefu.entity.MdbViewPrikazy;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выплаты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewGrantGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.MdbViewGrant";
    public static final String ENTITY_NAME = "mdbViewGrant";
    public static final int VERSION_HASH = -1313022321;
    private static IEntityMeta ENTITY_META;

    public static final String L_PRIKAZ = "prikaz";
    public static final String P_GRANT_SIZE_IND_AC = "grantSizeIndAc";
    public static final String P_GROUP_MANAGER_BONUS_SIZE_IND_AC = "groupManagerBonusSizeIndAc";
    public static final String P_GRANT_SIZE_IND_AC_ENR = "grantSizeIndAcEnr";
    public static final String P_GROUP_MANAGER_BONUS_SIZE_IND_AC_ENR = "groupManagerBonusSizeIndAcEnr";
    public static final String P_GRANT_SIZE_IND_ONCE = "grantSizeIndOnce";
    public static final String P_GRANT_SIZE_IND_BONUS = "grantSizeIndBonus";
    public static final String P_GRANT_SIZE_LIST_AC = "grantSizeListAc";
    public static final String P_GROUP_MANAGER_BONUS_SIZE_LIST_AC = "groupManagerBonusSizeListAc";
    public static final String P_GRANT_SIZE_LIST_AC_ENR = "grantSizeListAcEnr";
    public static final String P_GROUP_MANAGER_BONUS_SIZE_LIST_AC_ENR = "groupManagerBonusSizeListAcEnr";
    public static final String P_GRANT_SIZE_LIST_BONUS = "grantSizeListBonus";
    public static final String P_GRANT_SIZE_LIST_ONCE = "grantSizeListOnce";
    public static final String P_GRANT_SIZE_LIST_SOC = "grantSizeListSoc";
    public static final String P_GRANT_SIZE_LIST_FIN_AID = "grantSizeListFinAid";
    public static final String P_GRANT_START_DATE = "grantStartDate";
    public static final String P_GRANT_END_DATE = "grantEndDate";
    public static final String P_GROUP_MANAGER_BONUS_START_DATE = "groupManagerBonusStartDate";
    public static final String P_GROUP_MANAGER_BONUS_END_DATE = "groupManagerBonusEndDate";

    private MdbViewPrikazy _prikaz;     // Приказы
    private Double _grantSizeIndAc;     // Размер стипендии, инд., академ.
    private Double _groupManagerBonusSizeIndAc;     // Надбавка за старосту, инд., академ.
    private Double _grantSizeIndAcEnr;     // Размер стипендии, инд., академ. вступ
    private Double _groupManagerBonusSizeIndAcEnr;     // Надбавка за старосту, инд, академ. вступ
    private Double _grantSizeIndOnce;     // Размер стипендии, инд., однократно
    private Double _grantSizeIndBonus;     // Размер стипендии, инд., надбавка
    private Double _grantSizeListAc;     // Размер стипендии, спис., академ.
    private Double _groupManagerBonusSizeListAc;     // Надбавка за старосту, спис., академ.
    private Long _grantSizeListAcEnr;     // Размер стипендии, спис, академ. вступ.
    private Long _groupManagerBonusSizeListAcEnr;     // Надбавка за старосту, спис., академ. вступ.
    private Double _grantSizeListBonus;     // Размер стипендии, спис., надбавка
    private Double _grantSizeListOnce;     // Размер стипендии, спис., однократно
    private Double _grantSizeListSoc;     // Размер стипендии, спис., соц.
    private Double _grantSizeListFinAid;     // Размер стипендии, спис., мат. помощь
    private Date _grantStartDate;     // Дата начала выплат стипендии
    private Date _grantEndDate;     // Дата окончания выплат стипендии
    private Date _groupManagerBonusStartDate;     // Дата начала выплат надбавки за старосту
    private Date _groupManagerBonusEndDate;     // Дата окончания выплат надбавки за старосту

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказы. Свойство не может быть null.
     */
    @NotNull
    public MdbViewPrikazy getPrikaz()
    {
        return _prikaz;
    }

    /**
     * @param prikaz Приказы. Свойство не может быть null.
     */
    public void setPrikaz(MdbViewPrikazy prikaz)
    {
        dirty(_prikaz, prikaz);
        _prikaz = prikaz;
    }

    /**
     * @return Размер стипендии, инд., академ..
     */
    public Double getGrantSizeIndAc()
    {
        return _grantSizeIndAc;
    }

    /**
     * @param grantSizeIndAc Размер стипендии, инд., академ..
     */
    public void setGrantSizeIndAc(Double grantSizeIndAc)
    {
        dirty(_grantSizeIndAc, grantSizeIndAc);
        _grantSizeIndAc = grantSizeIndAc;
    }

    /**
     * @return Надбавка за старосту, инд., академ..
     */
    public Double getGroupManagerBonusSizeIndAc()
    {
        return _groupManagerBonusSizeIndAc;
    }

    /**
     * @param groupManagerBonusSizeIndAc Надбавка за старосту, инд., академ..
     */
    public void setGroupManagerBonusSizeIndAc(Double groupManagerBonusSizeIndAc)
    {
        dirty(_groupManagerBonusSizeIndAc, groupManagerBonusSizeIndAc);
        _groupManagerBonusSizeIndAc = groupManagerBonusSizeIndAc;
    }

    /**
     * @return Размер стипендии, инд., академ. вступ.
     */
    public Double getGrantSizeIndAcEnr()
    {
        return _grantSizeIndAcEnr;
    }

    /**
     * @param grantSizeIndAcEnr Размер стипендии, инд., академ. вступ.
     */
    public void setGrantSizeIndAcEnr(Double grantSizeIndAcEnr)
    {
        dirty(_grantSizeIndAcEnr, grantSizeIndAcEnr);
        _grantSizeIndAcEnr = grantSizeIndAcEnr;
    }

    /**
     * @return Надбавка за старосту, инд, академ. вступ.
     */
    public Double getGroupManagerBonusSizeIndAcEnr()
    {
        return _groupManagerBonusSizeIndAcEnr;
    }

    /**
     * @param groupManagerBonusSizeIndAcEnr Надбавка за старосту, инд, академ. вступ.
     */
    public void setGroupManagerBonusSizeIndAcEnr(Double groupManagerBonusSizeIndAcEnr)
    {
        dirty(_groupManagerBonusSizeIndAcEnr, groupManagerBonusSizeIndAcEnr);
        _groupManagerBonusSizeIndAcEnr = groupManagerBonusSizeIndAcEnr;
    }

    /**
     * @return Размер стипендии, инд., однократно.
     */
    public Double getGrantSizeIndOnce()
    {
        return _grantSizeIndOnce;
    }

    /**
     * @param grantSizeIndOnce Размер стипендии, инд., однократно.
     */
    public void setGrantSizeIndOnce(Double grantSizeIndOnce)
    {
        dirty(_grantSizeIndOnce, grantSizeIndOnce);
        _grantSizeIndOnce = grantSizeIndOnce;
    }

    /**
     * @return Размер стипендии, инд., надбавка.
     */
    public Double getGrantSizeIndBonus()
    {
        return _grantSizeIndBonus;
    }

    /**
     * @param grantSizeIndBonus Размер стипендии, инд., надбавка.
     */
    public void setGrantSizeIndBonus(Double grantSizeIndBonus)
    {
        dirty(_grantSizeIndBonus, grantSizeIndBonus);
        _grantSizeIndBonus = grantSizeIndBonus;
    }

    /**
     * @return Размер стипендии, спис., академ..
     */
    public Double getGrantSizeListAc()
    {
        return _grantSizeListAc;
    }

    /**
     * @param grantSizeListAc Размер стипендии, спис., академ..
     */
    public void setGrantSizeListAc(Double grantSizeListAc)
    {
        dirty(_grantSizeListAc, grantSizeListAc);
        _grantSizeListAc = grantSizeListAc;
    }

    /**
     * @return Надбавка за старосту, спис., академ..
     */
    public Double getGroupManagerBonusSizeListAc()
    {
        return _groupManagerBonusSizeListAc;
    }

    /**
     * @param groupManagerBonusSizeListAc Надбавка за старосту, спис., академ..
     */
    public void setGroupManagerBonusSizeListAc(Double groupManagerBonusSizeListAc)
    {
        dirty(_groupManagerBonusSizeListAc, groupManagerBonusSizeListAc);
        _groupManagerBonusSizeListAc = groupManagerBonusSizeListAc;
    }

    /**
     * @return Размер стипендии, спис, академ. вступ..
     */
    public Long getGrantSizeListAcEnr()
    {
        return _grantSizeListAcEnr;
    }

    /**
     * @param grantSizeListAcEnr Размер стипендии, спис, академ. вступ..
     */
    public void setGrantSizeListAcEnr(Long grantSizeListAcEnr)
    {
        dirty(_grantSizeListAcEnr, grantSizeListAcEnr);
        _grantSizeListAcEnr = grantSizeListAcEnr;
    }

    /**
     * @return Надбавка за старосту, спис., академ. вступ..
     */
    public Long getGroupManagerBonusSizeListAcEnr()
    {
        return _groupManagerBonusSizeListAcEnr;
    }

    /**
     * @param groupManagerBonusSizeListAcEnr Надбавка за старосту, спис., академ. вступ..
     */
    public void setGroupManagerBonusSizeListAcEnr(Long groupManagerBonusSizeListAcEnr)
    {
        dirty(_groupManagerBonusSizeListAcEnr, groupManagerBonusSizeListAcEnr);
        _groupManagerBonusSizeListAcEnr = groupManagerBonusSizeListAcEnr;
    }

    /**
     * @return Размер стипендии, спис., надбавка.
     */
    public Double getGrantSizeListBonus()
    {
        return _grantSizeListBonus;
    }

    /**
     * @param grantSizeListBonus Размер стипендии, спис., надбавка.
     */
    public void setGrantSizeListBonus(Double grantSizeListBonus)
    {
        dirty(_grantSizeListBonus, grantSizeListBonus);
        _grantSizeListBonus = grantSizeListBonus;
    }

    /**
     * @return Размер стипендии, спис., однократно.
     */
    public Double getGrantSizeListOnce()
    {
        return _grantSizeListOnce;
    }

    /**
     * @param grantSizeListOnce Размер стипендии, спис., однократно.
     */
    public void setGrantSizeListOnce(Double grantSizeListOnce)
    {
        dirty(_grantSizeListOnce, grantSizeListOnce);
        _grantSizeListOnce = grantSizeListOnce;
    }

    /**
     * @return Размер стипендии, спис., соц..
     */
    public Double getGrantSizeListSoc()
    {
        return _grantSizeListSoc;
    }

    /**
     * @param grantSizeListSoc Размер стипендии, спис., соц..
     */
    public void setGrantSizeListSoc(Double grantSizeListSoc)
    {
        dirty(_grantSizeListSoc, grantSizeListSoc);
        _grantSizeListSoc = grantSizeListSoc;
    }

    /**
     * @return Размер стипендии, спис., мат. помощь.
     */
    public Double getGrantSizeListFinAid()
    {
        return _grantSizeListFinAid;
    }

    /**
     * @param grantSizeListFinAid Размер стипендии, спис., мат. помощь.
     */
    public void setGrantSizeListFinAid(Double grantSizeListFinAid)
    {
        dirty(_grantSizeListFinAid, grantSizeListFinAid);
        _grantSizeListFinAid = grantSizeListFinAid;
    }

    /**
     * @return Дата начала выплат стипендии.
     */
    public Date getGrantStartDate()
    {
        return _grantStartDate;
    }

    /**
     * @param grantStartDate Дата начала выплат стипендии.
     */
    public void setGrantStartDate(Date grantStartDate)
    {
        dirty(_grantStartDate, grantStartDate);
        _grantStartDate = grantStartDate;
    }

    /**
     * @return Дата окончания выплат стипендии.
     */
    public Date getGrantEndDate()
    {
        return _grantEndDate;
    }

    /**
     * @param grantEndDate Дата окончания выплат стипендии.
     */
    public void setGrantEndDate(Date grantEndDate)
    {
        dirty(_grantEndDate, grantEndDate);
        _grantEndDate = grantEndDate;
    }

    /**
     * @return Дата начала выплат надбавки за старосту.
     */
    public Date getGroupManagerBonusStartDate()
    {
        return _groupManagerBonusStartDate;
    }

    /**
     * @param groupManagerBonusStartDate Дата начала выплат надбавки за старосту.
     */
    public void setGroupManagerBonusStartDate(Date groupManagerBonusStartDate)
    {
        dirty(_groupManagerBonusStartDate, groupManagerBonusStartDate);
        _groupManagerBonusStartDate = groupManagerBonusStartDate;
    }

    /**
     * @return Дата окончания выплат надбавки за старосту.
     */
    public Date getGroupManagerBonusEndDate()
    {
        return _groupManagerBonusEndDate;
    }

    /**
     * @param groupManagerBonusEndDate Дата окончания выплат надбавки за старосту.
     */
    public void setGroupManagerBonusEndDate(Date groupManagerBonusEndDate)
    {
        dirty(_groupManagerBonusEndDate, groupManagerBonusEndDate);
        _groupManagerBonusEndDate = groupManagerBonusEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewGrantGen)
        {
            setPrikaz(((MdbViewGrant)another).getPrikaz());
            setGrantSizeIndAc(((MdbViewGrant)another).getGrantSizeIndAc());
            setGroupManagerBonusSizeIndAc(((MdbViewGrant)another).getGroupManagerBonusSizeIndAc());
            setGrantSizeIndAcEnr(((MdbViewGrant)another).getGrantSizeIndAcEnr());
            setGroupManagerBonusSizeIndAcEnr(((MdbViewGrant)another).getGroupManagerBonusSizeIndAcEnr());
            setGrantSizeIndOnce(((MdbViewGrant)another).getGrantSizeIndOnce());
            setGrantSizeIndBonus(((MdbViewGrant)another).getGrantSizeIndBonus());
            setGrantSizeListAc(((MdbViewGrant)another).getGrantSizeListAc());
            setGroupManagerBonusSizeListAc(((MdbViewGrant)another).getGroupManagerBonusSizeListAc());
            setGrantSizeListAcEnr(((MdbViewGrant)another).getGrantSizeListAcEnr());
            setGroupManagerBonusSizeListAcEnr(((MdbViewGrant)another).getGroupManagerBonusSizeListAcEnr());
            setGrantSizeListBonus(((MdbViewGrant)another).getGrantSizeListBonus());
            setGrantSizeListOnce(((MdbViewGrant)another).getGrantSizeListOnce());
            setGrantSizeListSoc(((MdbViewGrant)another).getGrantSizeListSoc());
            setGrantSizeListFinAid(((MdbViewGrant)another).getGrantSizeListFinAid());
            setGrantStartDate(((MdbViewGrant)another).getGrantStartDate());
            setGrantEndDate(((MdbViewGrant)another).getGrantEndDate());
            setGroupManagerBonusStartDate(((MdbViewGrant)another).getGroupManagerBonusStartDate());
            setGroupManagerBonusEndDate(((MdbViewGrant)another).getGroupManagerBonusEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewGrantGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewGrant.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewGrant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "prikaz":
                    return obj.getPrikaz();
                case "grantSizeIndAc":
                    return obj.getGrantSizeIndAc();
                case "groupManagerBonusSizeIndAc":
                    return obj.getGroupManagerBonusSizeIndAc();
                case "grantSizeIndAcEnr":
                    return obj.getGrantSizeIndAcEnr();
                case "groupManagerBonusSizeIndAcEnr":
                    return obj.getGroupManagerBonusSizeIndAcEnr();
                case "grantSizeIndOnce":
                    return obj.getGrantSizeIndOnce();
                case "grantSizeIndBonus":
                    return obj.getGrantSizeIndBonus();
                case "grantSizeListAc":
                    return obj.getGrantSizeListAc();
                case "groupManagerBonusSizeListAc":
                    return obj.getGroupManagerBonusSizeListAc();
                case "grantSizeListAcEnr":
                    return obj.getGrantSizeListAcEnr();
                case "groupManagerBonusSizeListAcEnr":
                    return obj.getGroupManagerBonusSizeListAcEnr();
                case "grantSizeListBonus":
                    return obj.getGrantSizeListBonus();
                case "grantSizeListOnce":
                    return obj.getGrantSizeListOnce();
                case "grantSizeListSoc":
                    return obj.getGrantSizeListSoc();
                case "grantSizeListFinAid":
                    return obj.getGrantSizeListFinAid();
                case "grantStartDate":
                    return obj.getGrantStartDate();
                case "grantEndDate":
                    return obj.getGrantEndDate();
                case "groupManagerBonusStartDate":
                    return obj.getGroupManagerBonusStartDate();
                case "groupManagerBonusEndDate":
                    return obj.getGroupManagerBonusEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "prikaz":
                    obj.setPrikaz((MdbViewPrikazy) value);
                    return;
                case "grantSizeIndAc":
                    obj.setGrantSizeIndAc((Double) value);
                    return;
                case "groupManagerBonusSizeIndAc":
                    obj.setGroupManagerBonusSizeIndAc((Double) value);
                    return;
                case "grantSizeIndAcEnr":
                    obj.setGrantSizeIndAcEnr((Double) value);
                    return;
                case "groupManagerBonusSizeIndAcEnr":
                    obj.setGroupManagerBonusSizeIndAcEnr((Double) value);
                    return;
                case "grantSizeIndOnce":
                    obj.setGrantSizeIndOnce((Double) value);
                    return;
                case "grantSizeIndBonus":
                    obj.setGrantSizeIndBonus((Double) value);
                    return;
                case "grantSizeListAc":
                    obj.setGrantSizeListAc((Double) value);
                    return;
                case "groupManagerBonusSizeListAc":
                    obj.setGroupManagerBonusSizeListAc((Double) value);
                    return;
                case "grantSizeListAcEnr":
                    obj.setGrantSizeListAcEnr((Long) value);
                    return;
                case "groupManagerBonusSizeListAcEnr":
                    obj.setGroupManagerBonusSizeListAcEnr((Long) value);
                    return;
                case "grantSizeListBonus":
                    obj.setGrantSizeListBonus((Double) value);
                    return;
                case "grantSizeListOnce":
                    obj.setGrantSizeListOnce((Double) value);
                    return;
                case "grantSizeListSoc":
                    obj.setGrantSizeListSoc((Double) value);
                    return;
                case "grantSizeListFinAid":
                    obj.setGrantSizeListFinAid((Double) value);
                    return;
                case "grantStartDate":
                    obj.setGrantStartDate((Date) value);
                    return;
                case "grantEndDate":
                    obj.setGrantEndDate((Date) value);
                    return;
                case "groupManagerBonusStartDate":
                    obj.setGroupManagerBonusStartDate((Date) value);
                    return;
                case "groupManagerBonusEndDate":
                    obj.setGroupManagerBonusEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "prikaz":
                        return true;
                case "grantSizeIndAc":
                        return true;
                case "groupManagerBonusSizeIndAc":
                        return true;
                case "grantSizeIndAcEnr":
                        return true;
                case "groupManagerBonusSizeIndAcEnr":
                        return true;
                case "grantSizeIndOnce":
                        return true;
                case "grantSizeIndBonus":
                        return true;
                case "grantSizeListAc":
                        return true;
                case "groupManagerBonusSizeListAc":
                        return true;
                case "grantSizeListAcEnr":
                        return true;
                case "groupManagerBonusSizeListAcEnr":
                        return true;
                case "grantSizeListBonus":
                        return true;
                case "grantSizeListOnce":
                        return true;
                case "grantSizeListSoc":
                        return true;
                case "grantSizeListFinAid":
                        return true;
                case "grantStartDate":
                        return true;
                case "grantEndDate":
                        return true;
                case "groupManagerBonusStartDate":
                        return true;
                case "groupManagerBonusEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "prikaz":
                    return true;
                case "grantSizeIndAc":
                    return true;
                case "groupManagerBonusSizeIndAc":
                    return true;
                case "grantSizeIndAcEnr":
                    return true;
                case "groupManagerBonusSizeIndAcEnr":
                    return true;
                case "grantSizeIndOnce":
                    return true;
                case "grantSizeIndBonus":
                    return true;
                case "grantSizeListAc":
                    return true;
                case "groupManagerBonusSizeListAc":
                    return true;
                case "grantSizeListAcEnr":
                    return true;
                case "groupManagerBonusSizeListAcEnr":
                    return true;
                case "grantSizeListBonus":
                    return true;
                case "grantSizeListOnce":
                    return true;
                case "grantSizeListSoc":
                    return true;
                case "grantSizeListFinAid":
                    return true;
                case "grantStartDate":
                    return true;
                case "grantEndDate":
                    return true;
                case "groupManagerBonusStartDate":
                    return true;
                case "groupManagerBonusEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "prikaz":
                    return MdbViewPrikazy.class;
                case "grantSizeIndAc":
                    return Double.class;
                case "groupManagerBonusSizeIndAc":
                    return Double.class;
                case "grantSizeIndAcEnr":
                    return Double.class;
                case "groupManagerBonusSizeIndAcEnr":
                    return Double.class;
                case "grantSizeIndOnce":
                    return Double.class;
                case "grantSizeIndBonus":
                    return Double.class;
                case "grantSizeListAc":
                    return Double.class;
                case "groupManagerBonusSizeListAc":
                    return Double.class;
                case "grantSizeListAcEnr":
                    return Long.class;
                case "groupManagerBonusSizeListAcEnr":
                    return Long.class;
                case "grantSizeListBonus":
                    return Double.class;
                case "grantSizeListOnce":
                    return Double.class;
                case "grantSizeListSoc":
                    return Double.class;
                case "grantSizeListFinAid":
                    return Double.class;
                case "grantStartDate":
                    return Date.class;
                case "grantEndDate":
                    return Date.class;
                case "groupManagerBonusStartDate":
                    return Date.class;
                case "groupManagerBonusEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewGrant> _dslPath = new Path<MdbViewGrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewGrant");
    }
            

    /**
     * @return Приказы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getPrikaz()
     */
    public static MdbViewPrikazy.Path<MdbViewPrikazy> prikaz()
    {
        return _dslPath.prikaz();
    }

    /**
     * @return Размер стипендии, инд., академ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeIndAc()
     */
    public static PropertyPath<Double> grantSizeIndAc()
    {
        return _dslPath.grantSizeIndAc();
    }

    /**
     * @return Надбавка за старосту, инд., академ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusSizeIndAc()
     */
    public static PropertyPath<Double> groupManagerBonusSizeIndAc()
    {
        return _dslPath.groupManagerBonusSizeIndAc();
    }

    /**
     * @return Размер стипендии, инд., академ. вступ.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeIndAcEnr()
     */
    public static PropertyPath<Double> grantSizeIndAcEnr()
    {
        return _dslPath.grantSizeIndAcEnr();
    }

    /**
     * @return Надбавка за старосту, инд, академ. вступ.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusSizeIndAcEnr()
     */
    public static PropertyPath<Double> groupManagerBonusSizeIndAcEnr()
    {
        return _dslPath.groupManagerBonusSizeIndAcEnr();
    }

    /**
     * @return Размер стипендии, инд., однократно.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeIndOnce()
     */
    public static PropertyPath<Double> grantSizeIndOnce()
    {
        return _dslPath.grantSizeIndOnce();
    }

    /**
     * @return Размер стипендии, инд., надбавка.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeIndBonus()
     */
    public static PropertyPath<Double> grantSizeIndBonus()
    {
        return _dslPath.grantSizeIndBonus();
    }

    /**
     * @return Размер стипендии, спис., академ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListAc()
     */
    public static PropertyPath<Double> grantSizeListAc()
    {
        return _dslPath.grantSizeListAc();
    }

    /**
     * @return Надбавка за старосту, спис., академ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusSizeListAc()
     */
    public static PropertyPath<Double> groupManagerBonusSizeListAc()
    {
        return _dslPath.groupManagerBonusSizeListAc();
    }

    /**
     * @return Размер стипендии, спис, академ. вступ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListAcEnr()
     */
    public static PropertyPath<Long> grantSizeListAcEnr()
    {
        return _dslPath.grantSizeListAcEnr();
    }

    /**
     * @return Надбавка за старосту, спис., академ. вступ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusSizeListAcEnr()
     */
    public static PropertyPath<Long> groupManagerBonusSizeListAcEnr()
    {
        return _dslPath.groupManagerBonusSizeListAcEnr();
    }

    /**
     * @return Размер стипендии, спис., надбавка.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListBonus()
     */
    public static PropertyPath<Double> grantSizeListBonus()
    {
        return _dslPath.grantSizeListBonus();
    }

    /**
     * @return Размер стипендии, спис., однократно.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListOnce()
     */
    public static PropertyPath<Double> grantSizeListOnce()
    {
        return _dslPath.grantSizeListOnce();
    }

    /**
     * @return Размер стипендии, спис., соц..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListSoc()
     */
    public static PropertyPath<Double> grantSizeListSoc()
    {
        return _dslPath.grantSizeListSoc();
    }

    /**
     * @return Размер стипендии, спис., мат. помощь.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListFinAid()
     */
    public static PropertyPath<Double> grantSizeListFinAid()
    {
        return _dslPath.grantSizeListFinAid();
    }

    /**
     * @return Дата начала выплат стипендии.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantStartDate()
     */
    public static PropertyPath<Date> grantStartDate()
    {
        return _dslPath.grantStartDate();
    }

    /**
     * @return Дата окончания выплат стипендии.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantEndDate()
     */
    public static PropertyPath<Date> grantEndDate()
    {
        return _dslPath.grantEndDate();
    }

    /**
     * @return Дата начала выплат надбавки за старосту.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusStartDate()
     */
    public static PropertyPath<Date> groupManagerBonusStartDate()
    {
        return _dslPath.groupManagerBonusStartDate();
    }

    /**
     * @return Дата окончания выплат надбавки за старосту.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusEndDate()
     */
    public static PropertyPath<Date> groupManagerBonusEndDate()
    {
        return _dslPath.groupManagerBonusEndDate();
    }

    public static class Path<E extends MdbViewGrant> extends EntityPath<E>
    {
        private MdbViewPrikazy.Path<MdbViewPrikazy> _prikaz;
        private PropertyPath<Double> _grantSizeIndAc;
        private PropertyPath<Double> _groupManagerBonusSizeIndAc;
        private PropertyPath<Double> _grantSizeIndAcEnr;
        private PropertyPath<Double> _groupManagerBonusSizeIndAcEnr;
        private PropertyPath<Double> _grantSizeIndOnce;
        private PropertyPath<Double> _grantSizeIndBonus;
        private PropertyPath<Double> _grantSizeListAc;
        private PropertyPath<Double> _groupManagerBonusSizeListAc;
        private PropertyPath<Long> _grantSizeListAcEnr;
        private PropertyPath<Long> _groupManagerBonusSizeListAcEnr;
        private PropertyPath<Double> _grantSizeListBonus;
        private PropertyPath<Double> _grantSizeListOnce;
        private PropertyPath<Double> _grantSizeListSoc;
        private PropertyPath<Double> _grantSizeListFinAid;
        private PropertyPath<Date> _grantStartDate;
        private PropertyPath<Date> _grantEndDate;
        private PropertyPath<Date> _groupManagerBonusStartDate;
        private PropertyPath<Date> _groupManagerBonusEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getPrikaz()
     */
        public MdbViewPrikazy.Path<MdbViewPrikazy> prikaz()
        {
            if(_prikaz == null )
                _prikaz = new MdbViewPrikazy.Path<MdbViewPrikazy>(L_PRIKAZ, this);
            return _prikaz;
        }

    /**
     * @return Размер стипендии, инд., академ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeIndAc()
     */
        public PropertyPath<Double> grantSizeIndAc()
        {
            if(_grantSizeIndAc == null )
                _grantSizeIndAc = new PropertyPath<Double>(MdbViewGrantGen.P_GRANT_SIZE_IND_AC, this);
            return _grantSizeIndAc;
        }

    /**
     * @return Надбавка за старосту, инд., академ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusSizeIndAc()
     */
        public PropertyPath<Double> groupManagerBonusSizeIndAc()
        {
            if(_groupManagerBonusSizeIndAc == null )
                _groupManagerBonusSizeIndAc = new PropertyPath<Double>(MdbViewGrantGen.P_GROUP_MANAGER_BONUS_SIZE_IND_AC, this);
            return _groupManagerBonusSizeIndAc;
        }

    /**
     * @return Размер стипендии, инд., академ. вступ.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeIndAcEnr()
     */
        public PropertyPath<Double> grantSizeIndAcEnr()
        {
            if(_grantSizeIndAcEnr == null )
                _grantSizeIndAcEnr = new PropertyPath<Double>(MdbViewGrantGen.P_GRANT_SIZE_IND_AC_ENR, this);
            return _grantSizeIndAcEnr;
        }

    /**
     * @return Надбавка за старосту, инд, академ. вступ.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusSizeIndAcEnr()
     */
        public PropertyPath<Double> groupManagerBonusSizeIndAcEnr()
        {
            if(_groupManagerBonusSizeIndAcEnr == null )
                _groupManagerBonusSizeIndAcEnr = new PropertyPath<Double>(MdbViewGrantGen.P_GROUP_MANAGER_BONUS_SIZE_IND_AC_ENR, this);
            return _groupManagerBonusSizeIndAcEnr;
        }

    /**
     * @return Размер стипендии, инд., однократно.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeIndOnce()
     */
        public PropertyPath<Double> grantSizeIndOnce()
        {
            if(_grantSizeIndOnce == null )
                _grantSizeIndOnce = new PropertyPath<Double>(MdbViewGrantGen.P_GRANT_SIZE_IND_ONCE, this);
            return _grantSizeIndOnce;
        }

    /**
     * @return Размер стипендии, инд., надбавка.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeIndBonus()
     */
        public PropertyPath<Double> grantSizeIndBonus()
        {
            if(_grantSizeIndBonus == null )
                _grantSizeIndBonus = new PropertyPath<Double>(MdbViewGrantGen.P_GRANT_SIZE_IND_BONUS, this);
            return _grantSizeIndBonus;
        }

    /**
     * @return Размер стипендии, спис., академ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListAc()
     */
        public PropertyPath<Double> grantSizeListAc()
        {
            if(_grantSizeListAc == null )
                _grantSizeListAc = new PropertyPath<Double>(MdbViewGrantGen.P_GRANT_SIZE_LIST_AC, this);
            return _grantSizeListAc;
        }

    /**
     * @return Надбавка за старосту, спис., академ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusSizeListAc()
     */
        public PropertyPath<Double> groupManagerBonusSizeListAc()
        {
            if(_groupManagerBonusSizeListAc == null )
                _groupManagerBonusSizeListAc = new PropertyPath<Double>(MdbViewGrantGen.P_GROUP_MANAGER_BONUS_SIZE_LIST_AC, this);
            return _groupManagerBonusSizeListAc;
        }

    /**
     * @return Размер стипендии, спис, академ. вступ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListAcEnr()
     */
        public PropertyPath<Long> grantSizeListAcEnr()
        {
            if(_grantSizeListAcEnr == null )
                _grantSizeListAcEnr = new PropertyPath<Long>(MdbViewGrantGen.P_GRANT_SIZE_LIST_AC_ENR, this);
            return _grantSizeListAcEnr;
        }

    /**
     * @return Надбавка за старосту, спис., академ. вступ..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusSizeListAcEnr()
     */
        public PropertyPath<Long> groupManagerBonusSizeListAcEnr()
        {
            if(_groupManagerBonusSizeListAcEnr == null )
                _groupManagerBonusSizeListAcEnr = new PropertyPath<Long>(MdbViewGrantGen.P_GROUP_MANAGER_BONUS_SIZE_LIST_AC_ENR, this);
            return _groupManagerBonusSizeListAcEnr;
        }

    /**
     * @return Размер стипендии, спис., надбавка.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListBonus()
     */
        public PropertyPath<Double> grantSizeListBonus()
        {
            if(_grantSizeListBonus == null )
                _grantSizeListBonus = new PropertyPath<Double>(MdbViewGrantGen.P_GRANT_SIZE_LIST_BONUS, this);
            return _grantSizeListBonus;
        }

    /**
     * @return Размер стипендии, спис., однократно.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListOnce()
     */
        public PropertyPath<Double> grantSizeListOnce()
        {
            if(_grantSizeListOnce == null )
                _grantSizeListOnce = new PropertyPath<Double>(MdbViewGrantGen.P_GRANT_SIZE_LIST_ONCE, this);
            return _grantSizeListOnce;
        }

    /**
     * @return Размер стипендии, спис., соц..
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListSoc()
     */
        public PropertyPath<Double> grantSizeListSoc()
        {
            if(_grantSizeListSoc == null )
                _grantSizeListSoc = new PropertyPath<Double>(MdbViewGrantGen.P_GRANT_SIZE_LIST_SOC, this);
            return _grantSizeListSoc;
        }

    /**
     * @return Размер стипендии, спис., мат. помощь.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantSizeListFinAid()
     */
        public PropertyPath<Double> grantSizeListFinAid()
        {
            if(_grantSizeListFinAid == null )
                _grantSizeListFinAid = new PropertyPath<Double>(MdbViewGrantGen.P_GRANT_SIZE_LIST_FIN_AID, this);
            return _grantSizeListFinAid;
        }

    /**
     * @return Дата начала выплат стипендии.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantStartDate()
     */
        public PropertyPath<Date> grantStartDate()
        {
            if(_grantStartDate == null )
                _grantStartDate = new PropertyPath<Date>(MdbViewGrantGen.P_GRANT_START_DATE, this);
            return _grantStartDate;
        }

    /**
     * @return Дата окончания выплат стипендии.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGrantEndDate()
     */
        public PropertyPath<Date> grantEndDate()
        {
            if(_grantEndDate == null )
                _grantEndDate = new PropertyPath<Date>(MdbViewGrantGen.P_GRANT_END_DATE, this);
            return _grantEndDate;
        }

    /**
     * @return Дата начала выплат надбавки за старосту.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusStartDate()
     */
        public PropertyPath<Date> groupManagerBonusStartDate()
        {
            if(_groupManagerBonusStartDate == null )
                _groupManagerBonusStartDate = new PropertyPath<Date>(MdbViewGrantGen.P_GROUP_MANAGER_BONUS_START_DATE, this);
            return _groupManagerBonusStartDate;
        }

    /**
     * @return Дата окончания выплат надбавки за старосту.
     * @see ru.tandemservice.unifefu.entity.MdbViewGrant#getGroupManagerBonusEndDate()
     */
        public PropertyPath<Date> groupManagerBonusEndDate()
        {
            if(_groupManagerBonusEndDate == null )
                _groupManagerBonusEndDate = new PropertyPath<Date>(MdbViewGrantGen.P_GROUP_MANAGER_BONUS_END_DATE, this);
            return _groupManagerBonusEndDate;
        }

        public Class getEntityClass()
        {
            return MdbViewGrant.class;
        }

        public String getEntityName()
        {
            return "mdbViewGrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
