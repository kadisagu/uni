/* $Id$ */
package ru.tandemservice.unifefu.component.report;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 29.06.2016
 */
public abstract class FefuEntrantSubmittedDocumentsReportModel implements IEnrollmentCampaignModel, IEnrollmentCampaignSelectModel
{

    public static final long CUSTOM_STATE_CONTAINS = 0L;
    public static final long CUSTOM_STATE_NOT_CONTAINS = 1L;

    private boolean byAllEnrollmentDirections;

    private ISelectModel _formativeOrgUnitModel;
    private ISelectModel _territorialOrgUnitModel;
    private ISelectModel _educationLevelsHighSchoolModel;
    private ISelectModel _developFormModel;
    private ISelectModel _developTechModel;
    private ISelectModel _developConditionModel;
    private ISelectModel _developPeriodModel;
    private ISelectModel _qualificationListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _studentCategoryListModel;
    private ISelectModel _entrantCustomStateListModel;
    private List<CompensationType> _compensationTypes;
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private List<IdentifiableWrapper> _customStateRulesList = Arrays.asList(new IdentifiableWrapper(CUSTOM_STATE_CONTAINS, "Содержит"), new IdentifiableWrapper(CUSTOM_STATE_NOT_CONTAINS, "Не содержит"));

    private OrgUnit _formativeOrgUnit;
    private OrgUnit _territorialOrgUnit;
    private EducationLevelsHighSchool _educationLevelsHighSchool;
    private DevelopForm _developForm;
    private DevelopTech _developTech;
    private DevelopCondition _developCondition;
    private DevelopPeriod _developPeriod;
    private List<Qualifications> _qualificationList;
    private List<DevelopForm> _developFormList;
    private List<DevelopCondition> _developConditionList;
    private List<StudentCategory> _studentCategoryList;
    private List<EntrantCustomStateCI> _entrantCustomStateList;
    private IPrincipalContext _principalContext;

    private boolean _includeForeignPerson = true;
    private boolean _includeEntrantNoMark;
    private boolean _includeEntrantWithBenefit;
    private boolean _includeEntrantTargetAdmission;
    private boolean _withOriginalColumn = false;

    private IdentifiableWrapper customStateRule;


    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    // Getters & Setters

    public boolean isByAllEnrollmentDirections()
    {
        return byAllEnrollmentDirections;
    }

    public void setByAllEnrollmentDirections(boolean byAllEnrollmentDirections)
    {
        this.byAllEnrollmentDirections = byAllEnrollmentDirections;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return _formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        _formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return _territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        _territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return _educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        _educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return _developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        _developFormModel = developFormModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return _developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        _developTechModel = developTechModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return _developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        _developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return _developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        _developPeriodModel = developPeriodModel;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public List<CompensationType> getCompensationTypes()
    {
        return _compensationTypes;
    }

    public void setCompensationTypes(List<CompensationType> compensationTypes)
    {
        _compensationTypes = compensationTypes;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }
    public boolean isIncludeForeignPerson() {
        return _includeForeignPerson;
    }

    public void setIncludeForeignPerson(boolean includeForeignPerson) {
        _includeForeignPerson = includeForeignPerson;
    }

    public boolean isIncludeEntrantNoMark() {
        return _includeEntrantNoMark;
    }

    public void setIncludeEntrantNoMark(boolean includeEntrantNoMark) {
        _includeEntrantNoMark = includeEntrantNoMark;
    }

    public boolean isIncludeEntrantWithBenefit() {
        return _includeEntrantWithBenefit;
    }

    public void setIncludeEntrantWithBenefit(boolean includeEntrantWithBenefit) {
        _includeEntrantWithBenefit = includeEntrantWithBenefit;
    }

    public boolean isIncludeEntrantTargetAdmission() {
        return _includeEntrantTargetAdmission;
    }

    public void setIncludeEntrantTargetAdmission(boolean includeEntrantTargetAdmission) {
        _includeEntrantTargetAdmission = includeEntrantTargetAdmission;
    }

    public ISelectModel getEntrantCustomStateListModel()
    {
        return _entrantCustomStateListModel;
    }

    public void setEntrantCustomStateListModel(ISelectModel entrantCustomStateListModel)
    {
        _entrantCustomStateListModel = entrantCustomStateListModel;
    }

    public List<EntrantCustomStateCI> getEntrantCustomStateList()
    {
        return _entrantCustomStateList;
    }

    public void setEntrantCustomStateList(List<EntrantCustomStateCI> entrantCustomStateList)
    {
        _entrantCustomStateList = entrantCustomStateList;
    }

    public IdentifiableWrapper getCustomStateRule()
    {
        return customStateRule;
    }

    public void setCustomStateRule(IdentifiableWrapper customStateRule)
    {
        this.customStateRule = customStateRule;
    }

    public List<IdentifiableWrapper> getCustomStateRulesList()
    {
        return _customStateRulesList;
    }

    public void setCustomStateRulesList(List<IdentifiableWrapper> customStateRulesList)
    {
        _customStateRulesList = customStateRulesList;
    }

    public boolean isWithOriginalColumn()
    {
        return _withOriginalColumn;
    }

    public void setWithOriginalColumn(boolean withOriginalColumn)
    {
        _withOriginalColumn = withOriginalColumn;
    }

    abstract public Date getDateFrom();

    abstract public Date getDateTo();

    abstract public EnrollmentDirection getEnrollmentDirection();

    abstract public CompensationType getCompensationType();

    abstract public boolean isOnlyWithOriginals();
}