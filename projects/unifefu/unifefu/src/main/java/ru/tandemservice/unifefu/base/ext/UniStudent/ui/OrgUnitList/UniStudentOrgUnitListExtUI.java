/* $Id$ */
package ru.tandemservice.unifefu.base.ext.UniStudent.ui.OrgUnitList;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.unifefu.base.ext.UniStudent.UniStudentExtManager;

/**
 * @author Nikolay Fedorovskih
 * @since 22.10.2013
 */
public class UniStudentOrgUnitListExtUI extends UIAddon
{
    @Override
    public void onComponentRefresh()
    {
        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getPresenter().getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (util != null)
        {
            UniStudentExtManager.configEduUtil(util, getPresenter().getSettingsKey());
            util.onComponentRefresh();
        }
    }

    public UniStudentOrgUnitListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS))
        {
            dataSource.put(UniStudentExtManager.PROP_DEVELOP_FORM, getPresenter().getSettings().get(UniStudentExtManager.PROP_DEVELOP_FORM));
            dataSource.put(UniStudentExtManager.PROP_BASE_EDU, getPresenter().getSettings().get(UniStudentExtManager.PROP_BASE_EDU));
        }
    }
}