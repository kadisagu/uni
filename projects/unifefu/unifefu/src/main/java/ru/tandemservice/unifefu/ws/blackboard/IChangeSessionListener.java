/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard;

/**
 * @author Nikolay Fedorovskih
 * @since 16.03.2014
 */
public interface IChangeSessionListener
{
    void sessionChanged();
}