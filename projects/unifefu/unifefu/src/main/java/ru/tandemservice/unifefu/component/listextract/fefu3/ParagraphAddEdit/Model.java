/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu3.ParagraphAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 18.03.2013
 */
public class Model extends AbstractListParagraphAddEditModel<FefuAdmitToDiplomaStuListExtract> implements IGroupModel
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private ISelectModel _groupListModel;
    private List<CompensationType> _compensationTypeList;
    private StudentCustomStateCI _studentCustomStateCI;
    private Boolean _gosExamDefine = true;
    private Boolean _pluralForGosExam = false;
    private boolean _notNeedAdmissionToGIA = false;

    public Boolean getPluralForGosExam()
    {
        return _pluralForGosExam;
    }

    public void setPluralForGosExam(Boolean pluralForGosExam)
    {
        _pluralForGosExam = pluralForGosExam;
    }

    public Boolean getGosExamDefine()
    {
        return _gosExamDefine;
    }

    public void setGosExamDefine(Boolean gosExamDefine)
    {
        _gosExamDefine = gosExamDefine;
    }

    public StudentCustomStateCI getStudentCustomStateCI()
    {
        return _studentCustomStateCI;
    }

    public void setStudentCustomStateCI(StudentCustomStateCI studentCustomStateCI)
    {
        _studentCustomStateCI = studentCustomStateCI;
    }

    private boolean _courseAndGroupDisabled;

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public boolean isCourseAndGroupDisabled()
    {
        return _courseAndGroupDisabled;
    }

    public void setCourseAndGroupDisabled(boolean courseAndGroupDisabled)
    {
        _courseAndGroupDisabled = courseAndGroupDisabled;
    }

    public boolean getNotNeedAdmissionToGIA()
    {
        return _notNeedAdmissionToGIA;
    }

    public void setNotNeedAdmissionToGIA(boolean notNeedAdmissionToGIA)
    {
        _notNeedAdmissionToGIA = notNeedAdmissionToGIA;
    }
}