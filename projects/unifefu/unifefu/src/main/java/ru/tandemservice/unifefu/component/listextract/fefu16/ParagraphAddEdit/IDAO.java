/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu16.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuExcludeStuDPOListExtract, Model>
{
}