/* $Id $ */
package ru.tandemservice.unifefu.base.ext.SessionTransfer.ui.OutsideAddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutsideAddEdit;

/**
 * @author Rostuncev Savva
 * @since 26.03.2014
 */
@Configuration
public class SessionTransferOutsideAddEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "fefu" + SessionTransferOutsideAddEditExtUI.class.getSimpleName();

    @Autowired
    private SessionTransferOutsideAddEdit _sessionTransferInsidePub;


    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_sessionTransferInsidePub.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, SessionTransferOutsideAddEditExtUI.class))
                .create();
    }
}
