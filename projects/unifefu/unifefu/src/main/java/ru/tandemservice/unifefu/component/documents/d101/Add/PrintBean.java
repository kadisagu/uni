/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d101.Add;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 29.09.2013
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        TopOrgUnit academy = TopOrgUnit.getInstance();
        IdentityCard identityCard = model.getStudent().getPerson().getIdentityCard();
        boolean male = identityCard.getSex().isMale();
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());

        String lastName = PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), GrammaCase.DATIVE, male);
        String firstName = PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), GrammaCase.DATIVE, male);
        String middleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), GrammaCase.DATIVE, male);
        Date birthDate = model.getStudent().getPerson().getIdentityCard().getBirthDate();
        EducationLevels educationLevel = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
        StructureEducationLevels levelType = educationLevel.getLevelType();
        String levelTypeStr = "";

        if (levelType.isMiddle())
        {
            if (QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
            {
                levelTypeStr = "по специальности";
            }
            else if (QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(educationLevel.getQualification().getCode()))
            {
                levelTypeStr = "по специальности";
            }
        }
        else if (levelType.isSpecialty() || levelType.isSpecialization())
        {
            levelTypeStr = "по специальности";
        }
        else if (levelType.isMaster())
        {
            levelTypeStr = "по направлению";
        }
        else if (levelType.isBachelor())
        {
            levelTypeStr = "по направлению";
        }

        injectModifier
                .put("vuzTitle", academy.getTitle())
                .put("vuzTitle_D", academy.getPrepositionalCaseTitle() != null ? academy.getPrepositionalCaseTitle() : academy.getPrintTitle())
                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()))
                .put("number", Integer.toString(model.getNumber()))
                .put("addresseeName", model.getAddresseeName())
                .put("addresseeSettlement", model.getAddresseeSettlement())
                .put("addresseeStreet", model.getAddresseeStreet())
                .put("addresseeHouse", model.getAddresseeHouse())
                .put("addresseePostCode", model.getAddresseePostCode())
                .put("lastName_D", lastName)
                .put("firstName_D", firstName)
                .put("middleName_D", middleName == null ? "" : middleName)
                .put("birthDate", birthDate == null ? "__________" : DateFormatter.DEFAULT_DATE_FORMATTER.format(birthDate))
                .put("gender", male ? "он" : "она")
                .put("entranceYear", Integer.toString(model.getStudent().getEntranceYear()))
                .put("Accepted", male ? "Зачислен" : "Зачислена")
                .put("levelTypeStr", levelTypeStr)
                .put("levelHighSchool", educationLevel.isProfileOrSpecialization() ? "«" + educationLevel.getParentLevel().getTitle() + "»" : "«" + educationLevel.getTitle() + "»")
                .put("developForm_G", model.getStudent().getEducationOrgUnit().getDevelopForm().getGenCaseTitle())
                .put("enrollmentOrderNumber", model.getEnrollmentOrderNumber())
                .put("enrollmentOrderDate", new SimpleDateFormat("dd.MM.yyyy").format(model.getEnrollmentOrderDate()));

        injectModifier.put("orderList", orderListTitle(model.getOrderList()));

        return injectModifier;
    }

    private RtfString orderListTitle(List<AbstractStudentExtract> orderList)
    {
        RtfString str = new RtfString();
        for (AbstractStudentExtract extract : orderList)
        {
            str.append(extract.getGeneratedComment(true)).par();
        }
        return str;
    }

    @Override
    protected RtfTableModifier createTableModifier(Model model)
    {
        RtfTableModifier tableModifier = super.createTableModifier(model);

        String levelStageCode = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getRoot().getCode();
        String[][] fefuPrimaryVisa = FefuStudentManager.instance().printInjectModifierDao().getVisaSignerDocumentType(model.getStudentDocumentType().getCode(), levelStageCode);

        if (fefuPrimaryVisa.length != 0)
            tableModifier.put("FEFU_PRIMARY_VISA", fefuPrimaryVisa);
        else
        {
            OrgUnit orgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
            EmployeePost employeePost = (EmployeePost) orgUnit.getHead();

            if (null != employeePost)
            {
                Post post = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost();
                String postTitle = null != post.getNominativeCaseTitle() ? post.getNominativeCaseTitle() : post.getTitle();
                postTitle += " " + (null != employeePost.getOrgUnit().getGenitiveCaseTitle() ? employeePost.getOrgUnit().getGenitiveCaseTitle() : employeePost.getOrgUnit().getFullTitle());

                tableModifier.put("FEFU_PRIMARY_VISA", new String[][]{{postTitle, employeePost.getPerson().getIdentityCard().getIof()}});
            }
            else
            {
                tableModifier.put("FEFU_PRIMARY_VISA", new String[][]{});
            }
        }
        return tableModifier;
    }
}
