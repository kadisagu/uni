/* $Id$ */

package ru.tandemservice.unifefu.component.listextract.fefu12.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public class DAO extends AbstractListParagraphPubDAO<FefuStuffCompensationStuListExtract, Model> implements IDAO
{
}
