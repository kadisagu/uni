/* $Id: SHAUtils.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Dmitry Seleznev
 * @since 04.05.2013
 */
public class SHAUtils
{
    public static String getSHAChecksum(String shaKey, String... params) throws RuntimeException, NoSuchAlgorithmException
    {
        if (params.length == 0)
            throw new RuntimeException("For checksum calculating at least one parameter should be specified.");

        StringBuilder paramsLine = new StringBuilder();
        for (String param : params) paramsLine.append(param);
        if(null != shaKey) paramsLine.append(shaKey);

        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.reset();
        md.update(paramsLine.toString().getBytes());
        byte[] shaResultArray = md.digest();

        StringBuilder shaResultStr = new StringBuilder("");
        for (byte b : shaResultArray)
        {
            shaResultStr.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }

        return shaResultStr.toString();
    }
}
