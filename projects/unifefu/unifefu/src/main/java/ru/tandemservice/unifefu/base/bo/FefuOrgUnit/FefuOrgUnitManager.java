/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrgUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.logic.FefuOrgUnitSwapDao;
import ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.logic.IFefuOrgUnitSwapDao;
import ru.tandemservice.unifefu.base.ext.DipDocument.ui.Print.DipDocumentPrintExt;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 24.09.2013
 */
@Configuration
public class FefuOrgUnitManager extends BusinessObjectManager
{
    public static FefuOrgUnitManager instance()
    {
        return instance(FefuOrgUnitManager.class);
    }

    @Bean
    public IFefuOrgUnitSwapDao dao()
    {
        return new FefuOrgUnitSwapDao();
    }


    /* Хэндлер работников на подразделении
    *
    * */
    @Bean
    public IBusinessHandler<DSInput, DSOutput> formativeOrgUnitEmployeeHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EmployeePost.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                dql.where(eq(property(EmployeePost.orgUnit().fromAlias(alias)), commonValue(context.get(DipDocumentPrintExt.FORMATIVE_ORG_UNIT))))
                        .buildQuery();
            }

        }
                .filter(EmployeePost.employee().person().identityCard().fullFio())
                .filter(EmployeePost.postType().shortTitle())
                .filter(EmployeePost.postStatus().shortTitle())
                .filter(EmployeePost.postRelation().postBoundedWithQGandQL().post().title())
                .filter(EmployeePost.orgUnit().fullTitle());

    }
}
