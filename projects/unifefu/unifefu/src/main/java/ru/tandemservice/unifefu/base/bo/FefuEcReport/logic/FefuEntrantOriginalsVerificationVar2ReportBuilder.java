package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.unifefu.base.vo.FefuEntrantOriginalsVerificationVar2ReportVO;
import ru.tandemservice.unifefu.entity.FefuEntrantRequest2TechnicalCommissionRelation;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: amakarova
 * Date: 20.09.13
 */
public class FefuEntrantOriginalsVerificationVar2ReportBuilder implements IFefuEcReportBuilder
{
    private FefuEntrantOriginalsVerificationVar2ReportVO reportVO;
    private Session session;
    private Map<Person, List<PersonBenefit>> personBenefitMap = new HashMap<>();

    //rtf template columns
    private final int regNumColumnNum = 0;
    private final int fioColumnNum = 1;
    private final int enrDirColumnNum = 2;
    private final int categoryColumnNum = 3;
    private final int originalColumnNum = 4;
    private final int takeAwayDocColumnNum = 5;

    public FefuEntrantOriginalsVerificationVar2ReportBuilder(FefuEntrantOriginalsVerificationVar2ReportVO reportVO)
    {
        this.reportVO = reportVO;
    }

    @Override
    public DatabaseFile getContent(Session session)
    {
        this.session = session;
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "fefuEntrantOriginalsVerificationVar2");
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<EntrantData> entrantData = getData();
        tableModifier.put("T1", getTable(entrantData));
        tableModifier.put("T1", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (colIndex == enrDirColumnNum)
                    return entrantData.get(rowIndex).getOtherDirectionsString().toList();
                return null;
            }
        });
        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private String[][] getTable(List<EntrantData> entrantDatas)
    {
        List<String[]> rows = Lists.newArrayList();
        Collections.sort(entrantDatas, new EntrantDataComparator());
        for (EntrantData data : entrantDatas)
        {
            MainRow mainRow = new MainRow(data);
            rows.add(mainRow.toStringArray());
        }
        if (rows.isEmpty())
            rows.add(new String[]{""});
        return rows.toArray(new String[][]{});
    }

    @SuppressWarnings("unchecked")
    private List<EntrantData> getData()
    {
        List<EntrantData> result = new ArrayList<>();

        //Все выбранные направления приема
        Map<EntrantRequest, List<RequestedEnrollmentDirection>> entrantListAllRDirMap = Maps.newHashMap();
        DQLSelectBuilder subBuilder = getRequestedEnrollmentDirectionsBuilder(false);
        subBuilder.column(property("red", RequestedEnrollmentDirection.entrantRequest().id()));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "rd");
        builder.where(in(property("rd", RequestedEnrollmentDirection.entrantRequest().id()), subBuilder.buildQuery()));
        List<RequestedEnrollmentDirection> allDirs = builder.createStatement(session).list();
        for(RequestedEnrollmentDirection direction: allDirs)
        {
            EntrantRequest entrantRequest = direction.getEntrantRequest();
            if(!entrantListAllRDirMap.containsKey(entrantRequest))
                entrantListAllRDirMap.put(entrantRequest, Lists.<RequestedEnrollmentDirection>newArrayList());
            if(!entrantListAllRDirMap.get(entrantRequest).contains(direction))
                entrantListAllRDirMap.get(entrantRequest).add(direction);
        }
        //

        List<RequestedEnrollmentDirection> requestedEnrollmentDirections = getRequestedEnrollmentDirectionsBuilder(false).column("red").createStatement(session).list();

        Map<EntrantRequest, List<RequestedEnrollmentDirection>> entrantRequestListMap = Maps.newHashMap();
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections)
        {
            EntrantRequest entrantRequest = requestedEnrollmentDirection.getEntrantRequest();
            if (!entrantRequestListMap.containsKey(entrantRequest))
                entrantRequestListMap.put(entrantRequest, Lists.<RequestedEnrollmentDirection>newArrayList());

            if (!entrantRequestListMap.get(entrantRequest).contains(requestedEnrollmentDirection))
                entrantRequestListMap.get(entrantRequest).add(requestedEnrollmentDirection);
        }
        if(reportVO.isIncludeEntrantWithBenefit()) initPersonBenefitMap();
        for (EntrantRequest entrantRequest : entrantRequestListMap.keySet())
        {
            List<RequestedEnrollmentDirection> directions = entrantRequestListMap.get(entrantRequest);
            directions.sort(new RequestedEnrollmentDirectionComparator());
            EntrantData entrantData = new EntrantData(directions.get(0), entrantListAllRDirMap.get(entrantRequest));
            result.add(entrantData);

        }
        return result;
    }

    private void initPersonBenefitMap()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonBenefit.class, "pb");
        builder.where(in(property("pb", PersonBenefit.person().id()), getRequestedEnrollmentDirectionsBuilder(true).buildQuery()));
        List<PersonBenefit> benefits = builder.createStatement(session).list();
        for(PersonBenefit personBenefit : benefits)
        {
            Person person = personBenefit.getPerson();
            if(!personBenefitMap.containsKey(person))
                personBenefitMap.put(person, Lists.<PersonBenefit>newArrayList());
            if(!personBenefitMap.get(person).contains(personBenefit))
                personBenefitMap.get(person).add(personBenefit);
        }
    }

    private DQLSelectBuilder getRequestedEnrollmentDirectionsBuilder(boolean onlyPersonIds)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red");
        if(onlyPersonIds)
            builder.column(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().person().id()));
        builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().id()), value(reportVO.getEnrollmentCampaign().getId())));
        builder.where(eq(property("red", RequestedEnrollmentDirection.compensationType().id()), value(reportVO.getCompensationType().getId())));
        builder.where(betweenDays(RequestedEnrollmentDirection.regDate().fromAlias("red"), reportVO.getFrom(), reportVO.getTo()));
        if (!reportVO.getStudentCategoryList().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.studentCategory().id()), EntityUtils.getIdsFromEntityList(reportVO.getStudentCategoryList())));
        builder.where(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().archival()), value(false)));
        if (!reportVO.getQualifications().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification()), EntityUtils.getIdsFromEntityList(reportVO.getQualifications())));

        if (reportVO.isOnlyWithOriginals())
            builder.where(exists(RequestedEnrollmentDirection.class,
                                 RequestedEnrollmentDirection.entrantRequest().entrant().s(), property("red", RequestedEnrollmentDirection.entrantRequest().entrant()),
                                 RequestedEnrollmentDirection.originalDocumentHandedIn().s(), Boolean.TRUE));

        if (!reportVO.isAllEnrollmentDirections())
        {
            if (null != reportVO.getEnrollmentDirection())
                builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().id()), value(reportVO.getEnrollmentDirection().getId())));
            else
            {
                builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().id()), value(reportVO.getFormativeOrgUnit().getId())));
                builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().territorialOrgUnit().id()), value(reportVO.getTerritorialOrgUnit().getId())));
                if (null != reportVO.getEducationLevelsHighSchool())
                    builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().id()), value(reportVO.getEducationLevelsHighSchool().getId())));
                //форма освоения
                if (null != reportVO.getDevelopForm())
                    builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().id()), value(reportVO.getDevelopForm().getId())));
                // условие освоения
                if (null != reportVO.getDevelopCondition())
                    builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().id()), value(reportVO.getDevelopCondition().getId())));
            }
        }
        if (null == reportVO.getEnrollmentDirection())
        {
            // технология освоения
            if (null != reportVO.getDevelopTech())
                builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developTech().id()), value(reportVO.getDevelopTech().getId())));
            // срок освоения
            if (null != reportVO.getDevelopPeriod())
                builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developPeriod().id()), value(reportVO.getDevelopPeriod().getId())));
        }
        if (reportVO.isIncludeForeignPerson())
            builder.where(not(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE))));
        else
            builder.where(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
        if (reportVO.isIncludeEntrantTargetAdmission())
            builder.where(eq(property("red", RequestedEnrollmentDirection.targetAdmission()), value(true)));
        if (reportVO.isIncludeEntrantWithBenefit())
            builder.where(exists(new DQLSelectBuilder().fromEntity(PersonBenefit.class, "b").where(eq(
                    property("red", RequestedEnrollmentDirection.entrantRequest().entrant().person().id()),
                    property("b", PersonBenefit.person().id()))).buildQuery()));
        if (reportVO.getTechnicCommission() != null) {
            builder.joinEntity("red", DQLJoinType.left, FefuEntrantRequest2TechnicalCommissionRelation.class, "tc",
                    eq(
                            property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")),
                            property(FefuEntrantRequest2TechnicalCommissionRelation.entrantRequest().id().fromAlias("tc"))
                    ));
            builder.where(eq(property("tc", FefuEntrantRequest2TechnicalCommissionRelation.technicalCommission()), value(reportVO.getTechnicCommission())));
        }
        return builder;
    }

    private class EntrantData
    {
        private Entrant _entrant;
        private RequestedEnrollmentDirection _requestedEnrollmentDirection;
        private List<RequestedEnrollmentDirection> _otherDirections; // направления приема (в порядке их приоритета) из всех заявлений абитуриента (в рамках одного выбранного возмещения затрат)
        private String _category;
        private String _benefits;
        private boolean originalDocumentHandedIn = false;

        public EntrantData(RequestedEnrollmentDirection requestedEnrollmentDirection, List<RequestedEnrollmentDirection> otherDirections)
        {
            _requestedEnrollmentDirection = requestedEnrollmentDirection;
            _entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
            _category = getCategoryForDir(_requestedEnrollmentDirection);
            _otherDirections = otherDirections;
            if(reportVO.isIncludeEntrantWithBenefit())
            {
                if(null != personBenefitMap.get(_entrant.getPerson()))
                    _benefits = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(personBenefitMap.get(_entrant.getPerson()), PersonBenefit.benefit().shortTitle()), ", ");
            }
            for (RequestedEnrollmentDirection red : _otherDirections)
                if (red.isOriginalDocumentHandedIn()) {
                    originalDocumentHandedIn = true;
                    break;
                }
        }

        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _requestedEnrollmentDirection;
        }

        public Entrant getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Entrant entrant)
        {
            _entrant = entrant;
        }

        public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            _requestedEnrollmentDirection = requestedEnrollmentDirection;
        }

        private String getCategory()
        {
            return _category;
        }

        private void setCategory(String category)
        {
            _category = category;
        }

        private String getBenefits()
        {
            return _benefits;
        }

        private void setBenefits(String benefits)
        {
            _benefits = benefits;
        }

        public RtfString getOtherDirectionsString()
        {
            RtfString string = new RtfString();
            Iterator<RequestedEnrollmentDirection> iterator = _otherDirections.iterator();
            while (iterator.hasNext())
            {
                RequestedEnrollmentDirection direction = iterator.next();
                String shortTitle = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle();
                if (direction.isAgree4Enrollment())
                    string.boldBegin().append(shortTitle).boldEnd();
                else
                    string.append(shortTitle);
                if (iterator.hasNext())
                    string.append(", ");
            }

            return string;
        }

        public boolean isOriginalDocumentHandedIn()
        {
            return originalDocumentHandedIn;
        }
    }

    private class MainRow
    {
        private EntrantData _entrantData;

        public MainRow(EntrantData entrantData)
        {
            _entrantData = entrantData;
        }

        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _entrantData.getRequestedEnrollmentDirection();
        }

        String[] toStringArray()
        {
            int columns = 6;
            String[] result = new String[columns];
            result[regNumColumnNum] = _entrantData.getEntrant().getPersonalNumber();
            result[fioColumnNum] = _entrantData.getEntrant().getPerson().getFullFio();
            result[enrDirColumnNum] = "";//пишется в IRtfRowIntercepter
            result[categoryColumnNum] = _entrantData.getCategory();
            result[originalColumnNum] = _entrantData.isOriginalDocumentHandedIn() ? "п" : "к";
            result[takeAwayDocColumnNum] = _entrantData.getRequestedEnrollmentDirection().getEntrantRequest().isTakeAwayDocument() ? "з/д" : "";
            return result;
        }
    }

    class EntrantDataComparator implements Comparator<EntrantData>
    {
        @Override
        public int compare(EntrantData o1, EntrantData o2)
        {
            int result = 0;
            if (reportVO.isOrderByOriginals())
                result = Boolean.compare(o2.isOriginalDocumentHandedIn(), o1.isOriginalDocumentHandedIn());
            if (result == 0)
                result = o1.getEntrant().getPersonalNumber().compareTo(o2.getEntrant().getPersonalNumber());
            return result;
        }
    }

    class RequestedEnrollmentDirectionComparator implements Comparator<RequestedEnrollmentDirection>
    {

        @Override
        public int compare(RequestedEnrollmentDirection o1, RequestedEnrollmentDirection o2)
        {
            if(o1.getEntrantRequest().equals(o2.getEntrantRequest()))
            {
                return o1.getPriority() - o2.getPriority();
            }
            else
            {
                return o1.getEntrantRequest().getRegNumber() - o2.getEntrantRequest().getRegNumber();
            }
        }
    }

    protected String getCategoryForDir(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        if (reportVO.isIncludeEntrantTargetAdmission())
        {
            ExternalOrgUnit orgUnit = requestedEnrollmentDirection.getExternalOrgUnit();
            if (orgUnit != null)
            {
                if(!StringUtils.isEmpty(orgUnit.getShortTitle()))
                    return orgUnit.getShortTitle();
                else
                    return orgUnit.getTitle();
            }
            else
                return "";
        } else {
            CompetitionKind competitionKind = requestedEnrollmentDirection.getCompetitionKind();
            if (competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION)
                    || competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
                return competitionKind.getShortTitle();
            else
                return "";
        }
    }
}
