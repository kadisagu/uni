/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.TransferDataAboutTargetReception;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;

/**
 * @author Zvereva Ekaterina
 * @since 22.08.2014
 */
@Configuration
public class FefuSystemActionTransferDataAboutTargetReception extends BusinessComponentManager
{
    public final static String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .create();
    }


}