package ru.tandemservice.unifefu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип выплаты детям-сиротам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuOrphanPaymentTypeGen extends EntityBase
 implements INaturalIdentifiable<FefuOrphanPaymentTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType";
    public static final String ENTITY_NAME = "fefuOrphanPaymentType";
    public static final int VERSION_HASH = -624014325;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_PRIORITY = "priority";
    public static final String P_TITLE = "title";
    public static final String P_PRINT = "print";

    private String _code;     // Системный код
    private Integer _priority;     // Приоритет
    private String _title;     // Название
    private String _print;     // Вывод на печать

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Приоритет.
     */
    public Integer getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет.
     */
    public void setPriority(Integer priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Вывод на печать. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPrint()
    {
        return _print;
    }

    /**
     * @param print Вывод на печать. Свойство не может быть null.
     */
    public void setPrint(String print)
    {
        dirty(_print, print);
        _print = print;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuOrphanPaymentTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((FefuOrphanPaymentType)another).getCode());
            }
            setPriority(((FefuOrphanPaymentType)another).getPriority());
            setTitle(((FefuOrphanPaymentType)another).getTitle());
            setPrint(((FefuOrphanPaymentType)another).getPrint());
        }
    }

    public INaturalId<FefuOrphanPaymentTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<FefuOrphanPaymentTypeGen>
    {
        private static final String PROXY_NAME = "FefuOrphanPaymentTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuOrphanPaymentTypeGen.NaturalId) ) return false;

            FefuOrphanPaymentTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuOrphanPaymentTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuOrphanPaymentType.class;
        }

        public T newInstance()
        {
            return (T) new FefuOrphanPaymentType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "priority":
                    return obj.getPriority();
                case "title":
                    return obj.getTitle();
                case "print":
                    return obj.getPrint();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "print":
                    obj.setPrint((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "priority":
                        return true;
                case "title":
                        return true;
                case "print":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "priority":
                    return true;
                case "title":
                    return true;
                case "print":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "priority":
                    return Integer.class;
                case "title":
                    return String.class;
                case "print":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuOrphanPaymentType> _dslPath = new Path<FefuOrphanPaymentType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuOrphanPaymentType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Приоритет.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Вывод на печать. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType#getPrint()
     */
    public static PropertyPath<String> print()
    {
        return _dslPath.print();
    }

    public static class Path<E extends FefuOrphanPaymentType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Integer> _priority;
        private PropertyPath<String> _title;
        private PropertyPath<String> _print;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(FefuOrphanPaymentTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Приоритет.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(FefuOrphanPaymentTypeGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FefuOrphanPaymentTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Вывод на печать. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType#getPrint()
     */
        public PropertyPath<String> print()
        {
            if(_print == null )
                _print = new PropertyPath<String>(FefuOrphanPaymentTypeGen.P_PRINT, this);
            return _print;
        }

        public Class getEntityClass()
        {
            return FefuOrphanPaymentType.class;
        }

        public String getEntityName()
        {
            return "fefuOrphanPaymentType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
