/**
 * ServiceResponseType2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.nsi.servertest;

public class ServiceResponseType2  implements java.io.Serializable {
    private java.math.BigInteger callCC;

    private java.lang.String callRC;

    public ServiceResponseType2() {
    }

    public ServiceResponseType2(
           java.math.BigInteger callCC,
           java.lang.String callRC) {
           this.callCC = callCC;
           this.callRC = callRC;
    }


    /**
     * Gets the callCC value for this ServiceResponseType2.
     * 
     * @return callCC
     */
    public java.math.BigInteger getCallCC() {
        return callCC;
    }


    /**
     * Sets the callCC value for this ServiceResponseType2.
     * 
     * @param callCC
     */
    public void setCallCC(java.math.BigInteger callCC) {
        this.callCC = callCC;
    }


    /**
     * Gets the callRC value for this ServiceResponseType2.
     * 
     * @return callRC
     */
    public java.lang.String getCallRC() {
        return callRC;
    }


    /**
     * Sets the callRC value for this ServiceResponseType2.
     * 
     * @param callRC
     */
    public void setCallRC(java.lang.String callRC) {
        this.callRC = callRC;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceResponseType2)) return false;
        ServiceResponseType2 other = (ServiceResponseType2) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.callCC==null && other.getCallCC()==null) || 
             (this.callCC!=null &&
              this.callCC.equals(other.getCallCC()))) &&
            ((this.callRC==null && other.getCallRC()==null) || 
             (this.callRC!=null &&
              this.callRC.equals(other.getCallRC())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCallCC() != null) {
            _hashCode += getCallCC().hashCode();
        }
        if (getCallRC() != null) {
            _hashCode += getCallRC().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceResponseType2.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "ServiceResponseType2"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("callCC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "callCC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("callRC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "callRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
