/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.vo;

import org.tandemframework.caf.ui.support.IUISettings;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Date;

/**
 * @author DMITRY KNYAZEV
 * @since 26.08.2014
 */
public class FefuSessionListDocumentFilter
{
	private final EducationYear educationYear;
	private final YearDistributionPart yearPart;
	private final String documentNumber;
	private final String studentLastName;
	private final Date dateFrom;
	private final Date dateTo;

	public FefuSessionListDocumentFilter(IUISettings settings)
	{
		educationYear = settings.get("eduYear");
		yearPart = settings.get("yearPart");
		documentNumber = settings.get("docNumber");
		studentLastName = settings.get("studentLastName");
		dateFrom = settings.get("formedFrom");
		dateTo = settings.get("formedTo");
	}

	public EducationYear getEducationYear()
	{
		return educationYear;
	}

	public YearDistributionPart getYearPart()
	{
		return yearPart;
	}

	public String getDocumentNumber()
	{
		return documentNumber;
	}

	public String getStudentLastName()
	{
		return studentLastName;
	}

	public Date getDateFrom()
	{
		return dateFrom;
	}

	public Date getDateTo()
	{
		return dateTo;
	}
}
