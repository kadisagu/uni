/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 18.06.2013
 */
public class EntrantPersonComboDSHandler extends DefaultComboDataSourceHandler
{
    public EntrantPersonComboDSHandler(String ownerId)
    {
        super(ownerId, Person.class);
        setOrderByProperty(Person.identityCard().fullFio().s());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(Entrant.class, "en").column(property(Entrant.person().id().fromAlias("en")))
                .where(eq(property(Entrant.archival().fromAlias("en")), value(Boolean.FALSE)));

        ep.dqlBuilder.where(in(property(Person.id().fromAlias("e")), subBuilder.buildQuery()));

        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotEmpty(filter))
        {
            ep.dqlBuilder.joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("e"), "c");

            IDQLExpression expr = DQLFunctions.concat(DQLExpressions.property(IdentityCard.lastName().fromAlias("c")),
                    DQLExpressions.value(" "),
                    DQLExpressions.property(IdentityCard.firstName().fromAlias("c")),
                    DQLExpressions.caseExpr(
                            new IDQLExpression[]{DQLExpressions.isNull(DQLExpressions.property(IdentityCard.middleName().fromAlias("c")))},
                            new IDQLExpression[]{DQLExpressions.value("")},
                            DQLFunctions.concat(DQLExpressions.value(" "), DQLExpressions.property(IdentityCard.middleName().fromAlias("c"))))
            );

            ep.dqlBuilder.where(DQLExpressions.likeUpper(expr, DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
        }
    }
}