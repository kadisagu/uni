/* $Id$ */
package ru.tandemservice.unifefu.base.ext.DiplomaIssuance.ui.AddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.ui.AddEdit.DiplomaIssuanceAddEdit;

/**
 * @author Andrey Avetisov
 * @since 26.09.2014
 */
@Configuration
public class DiplomaIssuanceAddEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + DiplomaIssuanceAddEditExtUI.class.getSimpleName();

    @Autowired
    private DiplomaIssuanceAddEdit _diplomaIssuanceAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_diplomaIssuanceAddEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, DiplomaIssuanceAddEditExtUI.class))
                .addAction(new DiplomaIssuanceAddEditClickApply("onClickApply"))
                .create();
    }


}
