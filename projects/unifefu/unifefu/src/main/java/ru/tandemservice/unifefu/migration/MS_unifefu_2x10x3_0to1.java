package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x10x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.3"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuConditionalTransferCourseStuExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("ffcndtnltrnsfrcrsstextrct_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_a472e02b"), 
				new DBColumn("oldgroupnumber_p", DBType.createVarchar(255)), 
				new DBColumn("eliminatedate_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuConditionalTransferCourseStuExtract");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuPerformConditionTransferCourseStuExtract

		// создана новая сущность
		{
			// у сущности нет своей таблицы - ничего делать не надо
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuPerformConditionTransferCourseStuExtract");

		}


    }
}