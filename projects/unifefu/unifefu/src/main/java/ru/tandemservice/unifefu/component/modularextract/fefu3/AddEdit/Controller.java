/* $Id: Controller.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.modularextract.fefu3.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unifefu.entity.FefuReEducationStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 22.08.2012
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuReEducationStuExtract, IDAO, Model>
{
    public void onChangeAnnulAttestation(IBusinessComponent component)
    {
        Model model = getModel(component);
        if(model.getExtract().isAnnulAttestationResults())
        {
            model.getExtract().setPrevCourse(model.getExtract().getEntity().getCourse());
            model.getExtract().setEduYear(DataAccessServices.dao().<EducationYear>get(EducationYear.class, EducationYear.current().s(), Boolean.TRUE));
        }
    }
}