package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение подразделения для хранения данных Directum
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuOrgUnitDirectumExtensionGen extends EntityBase
 implements INaturalIdentifiable<FefuOrgUnitDirectumExtensionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension";
    public static final String ENTITY_NAME = "fefuOrgUnitDirectumExtension";
    public static final int VERSION_HASH = 711183725;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_FEFU_DIRECTUM_ORDER_TYPE = "fefuDirectumOrderType";
    public static final String P_ORG_UNIT_CODE = "orgUnitCode";
    public static final String P_ORG_UNIT_TITLE = "orgUnitTitle";

    private OrgUnit _orgUnit;     // Подразделение
    private FefuDirectumOrderType _fefuDirectumOrderType;     // Вид приказа в Directum
    private String _orgUnitCode;     // Код подразделения в Directum
    private String _orgUnitTitle;     // Наименование подразделения в Directum

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     */
    @NotNull
    public FefuDirectumOrderType getFefuDirectumOrderType()
    {
        return _fefuDirectumOrderType;
    }

    /**
     * @param fefuDirectumOrderType Вид приказа в Directum. Свойство не может быть null.
     */
    public void setFefuDirectumOrderType(FefuDirectumOrderType fefuDirectumOrderType)
    {
        dirty(_fefuDirectumOrderType, fefuDirectumOrderType);
        _fefuDirectumOrderType = fefuDirectumOrderType;
    }

    /**
     * @return Код подразделения в Directum. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOrgUnitCode()
    {
        return _orgUnitCode;
    }

    /**
     * @param orgUnitCode Код подразделения в Directum. Свойство не может быть null.
     */
    public void setOrgUnitCode(String orgUnitCode)
    {
        dirty(_orgUnitCode, orgUnitCode);
        _orgUnitCode = orgUnitCode;
    }

    /**
     * @return Наименование подразделения в Directum. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOrgUnitTitle()
    {
        return _orgUnitTitle;
    }

    /**
     * @param orgUnitTitle Наименование подразделения в Directum. Свойство не может быть null.
     */
    public void setOrgUnitTitle(String orgUnitTitle)
    {
        dirty(_orgUnitTitle, orgUnitTitle);
        _orgUnitTitle = orgUnitTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuOrgUnitDirectumExtensionGen)
        {
            if (withNaturalIdProperties)
            {
                setOrgUnit(((FefuOrgUnitDirectumExtension)another).getOrgUnit());
                setFefuDirectumOrderType(((FefuOrgUnitDirectumExtension)another).getFefuDirectumOrderType());
            }
            setOrgUnitCode(((FefuOrgUnitDirectumExtension)another).getOrgUnitCode());
            setOrgUnitTitle(((FefuOrgUnitDirectumExtension)another).getOrgUnitTitle());
        }
    }

    public INaturalId<FefuOrgUnitDirectumExtensionGen> getNaturalId()
    {
        return new NaturalId(getOrgUnit(), getFefuDirectumOrderType());
    }

    public static class NaturalId extends NaturalIdBase<FefuOrgUnitDirectumExtensionGen>
    {
        private static final String PROXY_NAME = "FefuOrgUnitDirectumExtensionNaturalProxy";

        private Long _orgUnit;
        private Long _fefuDirectumOrderType;

        public NaturalId()
        {}

        public NaturalId(OrgUnit orgUnit, FefuDirectumOrderType fefuDirectumOrderType)
        {
            _orgUnit = ((IEntity) orgUnit).getId();
            _fefuDirectumOrderType = ((IEntity) fefuDirectumOrderType).getId();
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public Long getFefuDirectumOrderType()
        {
            return _fefuDirectumOrderType;
        }

        public void setFefuDirectumOrderType(Long fefuDirectumOrderType)
        {
            _fefuDirectumOrderType = fefuDirectumOrderType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuOrgUnitDirectumExtensionGen.NaturalId) ) return false;

            FefuOrgUnitDirectumExtensionGen.NaturalId that = (NaturalId) o;

            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            if( !equals(getFefuDirectumOrderType(), that.getFefuDirectumOrderType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOrgUnit());
            result = hashCode(result, getFefuDirectumOrderType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOrgUnit());
            sb.append("/");
            sb.append(getFefuDirectumOrderType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuOrgUnitDirectumExtensionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuOrgUnitDirectumExtension.class;
        }

        public T newInstance()
        {
            return (T) new FefuOrgUnitDirectumExtension();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "fefuDirectumOrderType":
                    return obj.getFefuDirectumOrderType();
                case "orgUnitCode":
                    return obj.getOrgUnitCode();
                case "orgUnitTitle":
                    return obj.getOrgUnitTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "fefuDirectumOrderType":
                    obj.setFefuDirectumOrderType((FefuDirectumOrderType) value);
                    return;
                case "orgUnitCode":
                    obj.setOrgUnitCode((String) value);
                    return;
                case "orgUnitTitle":
                    obj.setOrgUnitTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "fefuDirectumOrderType":
                        return true;
                case "orgUnitCode":
                        return true;
                case "orgUnitTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "fefuDirectumOrderType":
                    return true;
                case "orgUnitCode":
                    return true;
                case "orgUnitTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "fefuDirectumOrderType":
                    return FefuDirectumOrderType.class;
                case "orgUnitCode":
                    return String.class;
                case "orgUnitTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuOrgUnitDirectumExtension> _dslPath = new Path<FefuOrgUnitDirectumExtension>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuOrgUnitDirectumExtension");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension#getFefuDirectumOrderType()
     */
    public static FefuDirectumOrderType.Path<FefuDirectumOrderType> fefuDirectumOrderType()
    {
        return _dslPath.fefuDirectumOrderType();
    }

    /**
     * @return Код подразделения в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension#getOrgUnitCode()
     */
    public static PropertyPath<String> orgUnitCode()
    {
        return _dslPath.orgUnitCode();
    }

    /**
     * @return Наименование подразделения в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension#getOrgUnitTitle()
     */
    public static PropertyPath<String> orgUnitTitle()
    {
        return _dslPath.orgUnitTitle();
    }

    public static class Path<E extends FefuOrgUnitDirectumExtension> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private FefuDirectumOrderType.Path<FefuDirectumOrderType> _fefuDirectumOrderType;
        private PropertyPath<String> _orgUnitCode;
        private PropertyPath<String> _orgUnitTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension#getFefuDirectumOrderType()
     */
        public FefuDirectumOrderType.Path<FefuDirectumOrderType> fefuDirectumOrderType()
        {
            if(_fefuDirectumOrderType == null )
                _fefuDirectumOrderType = new FefuDirectumOrderType.Path<FefuDirectumOrderType>(L_FEFU_DIRECTUM_ORDER_TYPE, this);
            return _fefuDirectumOrderType;
        }

    /**
     * @return Код подразделения в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension#getOrgUnitCode()
     */
        public PropertyPath<String> orgUnitCode()
        {
            if(_orgUnitCode == null )
                _orgUnitCode = new PropertyPath<String>(FefuOrgUnitDirectumExtensionGen.P_ORG_UNIT_CODE, this);
            return _orgUnitCode;
        }

    /**
     * @return Наименование подразделения в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension#getOrgUnitTitle()
     */
        public PropertyPath<String> orgUnitTitle()
        {
            if(_orgUnitTitle == null )
                _orgUnitTitle = new PropertyPath<String>(FefuOrgUnitDirectumExtensionGen.P_ORG_UNIT_TITLE, this);
            return _orgUnitTitle;
        }

        public Class getEntityClass()
        {
            return FefuOrgUnitDirectumExtension.class;
        }

        public String getEntityName()
        {
            return "fefuOrgUnitDirectumExtension";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
