/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.OrderLogView;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantOrder;
import ru.tandemservice.unifefu.base.bo.Directum.logic.DirectumDAO;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumLog;
import ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension;
import ru.tandemservice.unifefu.entity.ws.FefuStudentOrderExtension;
import ru.tandemservice.unimove.IAbstractOrder;

/**
 * @author Dmitry Seleznev
 * @since 23.07.2013
 */
@State({
        @Bind(key = "orderId", binding = "orderId")
})
public class DirectumOrderLogViewUI extends UIPresenter
{
    private Long _orderId;
    private FefuStudentOrderExtension _studentOrderExtension;
    private FefuEntrantOrderExtension _enrOrderExtension;
    private AbstractStudentExtract _firstExtract;
    private AbstractStudentOrder _studOrder;
    private AbstractEntrantOrder _enrOrder;

    @Override
    public void onComponentRefresh()
    {
        IAbstractOrder order = DataAccessServices.dao().getNotNull(_orderId);

        _studOrder = (order instanceof AbstractStudentOrder) ? (AbstractStudentOrder) order : null;
        _enrOrder = (order instanceof AbstractEntrantOrder) ? (AbstractEntrantOrder) order : null;

        if (_studOrder instanceof StudentModularOrder)
             _firstExtract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(_orderId);

        if (null != _studOrder)
            _studentOrderExtension = DataAccessServices.dao().get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.order().id(), _studOrder.getId());

        if (null != _enrOrder)
            _enrOrderExtension = DataAccessServices.dao().get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.order().id(), _enrOrder.getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DirectumOrderLogView.ORDER_LOG_DS.equals(dataSource.getName()))
        {
            dataSource.put(DirectumOrderLogView.ORDER_ID_PARAM, _orderId);
        }
    }

    public ISecured getSecurityObject()
    {
        if (null != _studOrder)
        {
            if (_studOrder instanceof StudentModularOrder) return _firstExtract;
            else if (_studOrder instanceof StudentListOrder) return _studOrder;
        } else
        {
            if (null != _enrOrder) return _enrOrder;
        }

        return _studOrder;
    }

    public String getSecurityKey()
    {
        if (null != _studOrder)
        {
            if (_studOrder instanceof StudentModularOrder) return "viewDirectumLogs_modularExtract";
            else if (_studOrder instanceof StudentListOrder) return "viewDirectumLogs_studentListOrder";
        } else if (null != _enrOrder)
        {
            return "viewDirectumLogs_enrollmentOrder";
        }
        return "";
    }

    public IEntity getExt()
    {
        if(null != _studentOrderExtension) return _studentOrderExtension;
        else return _enrOrderExtension;
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public AbstractStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public void setFirstExtract(AbstractStudentExtract firstExtract)
    {
        _firstExtract = firstExtract;
    }

    // DEV-4697
    public boolean isLastDirectumTaskIdAvailable()
    {
        return (null != _studentOrderExtension && null != _studentOrderExtension.getDirectumTaskId()) || (null != _enrOrderExtension && null != _enrOrderExtension.getDirectumTaskId());
    }
    // DEV-4697
    public String getLastDirectumTaskUrl()
    {
        String directumTask = (null != _studentOrderExtension && null != _studentOrderExtension.getDirectumTaskId()) ? _studentOrderExtension.getDirectumTaskId() :
                              ((null != _enrOrderExtension && null != _enrOrderExtension.getDirectumTaskId()) ? _enrOrderExtension.getDirectumTaskId() : "");
        return DirectumDAO.getDirectumBaseUrl() + directumTask;
    }

    // DEV-4697
    public void onClickGetLastDirectumTaskId()
    {
        String directumTaskId = (null != _studentOrderExtension && null != _studentOrderExtension.getDirectumTaskId()) ? _studentOrderExtension.getDirectumTaskId() :
                ((null != _enrOrderExtension && null != _enrOrderExtension.getDirectumTaskId()) ? _enrOrderExtension.getDirectumTaskId() : null);

        StringBuilder linkFile = new StringBuilder("Version=ISB7\nSystemCode=dvfu\nComponentType=9\nID=");
        linkFile.append(null != directumTaskId ? directumTaskId : "").append("\nViewCode=");

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
                .contentType(DatabaseFile.CONTENT_TYPE_SOME_DATA)
                .fileName((null != directumTaskId ? directumTaskId : "document") + ".isb")
                .document(linkFile.toString().getBytes()), true);
    }

    // DEV-4728
    public boolean isDirectumTaskIdAvailable()
    {
        FefuDirectumLog dlog = _uiConfig.getDataSource(DirectumOrderLogView.ORDER_LOG_DS).getCurrent();
        return (null != dlog && null != dlog.getDirectumTaskId());
    }
    // DEV-4728
    public String getDirectumTaskUrl()
    {
        FefuDirectumLog dlog = _uiConfig.getDataSource(DirectumOrderLogView.ORDER_LOG_DS).getCurrent();
        String directumTask = (null != dlog && null != dlog.getDirectumTaskId()) ? dlog.getDirectumTaskId() : "";
        return DirectumDAO.getDirectumBaseUrl() + directumTask;
    }

    // DEV-4728
    public String getDirectumTaskId()
    {
        FefuDirectumLog dlog = _uiConfig.getDataSource(DirectumOrderLogView.ORDER_LOG_DS).getCurrent();
        return (null != dlog && null != dlog.getDirectumTaskId()) ? dlog.getDirectumTaskId() : null;
    }

    // DEV-4728
    public void onClickGetDirectumTaskId()
    {
        String directumTaskId = getListenerParameter();
        StringBuilder linkFile = new StringBuilder("Version=ISB7\nSystemCode=dvfu\nComponentType=9\nID=");
        linkFile.append(null != directumTaskId ? directumTaskId : "").append("\nViewCode=");

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
                .contentType(DatabaseFile.CONTENT_TYPE_SOME_DATA)
                .fileName((null != directumTaskId ? directumTaskId : "document") + ".isb")
                .document(linkFile.toString().getBytes()), true);
    }
}