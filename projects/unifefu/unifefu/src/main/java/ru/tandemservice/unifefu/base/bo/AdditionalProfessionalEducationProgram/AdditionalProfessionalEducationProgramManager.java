package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.DiplomaQualifications;
import ru.tandemservice.unifefu.entity.catalog.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniOrgUnit.logic.OrgUnitByKindComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.EducationLevelAdditional;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.logic.Constants.FORMATIVE_ORG_UNIT;
import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.logic.Constants.TERRITORIAL_ORG_UNIT;
import static ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes.POVYSHENIE_KVALIFIKATSII;
import static ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes.PROFESSIONALNAYA_PEREPODGOTOVKA;

@Configuration
public class AdditionalProfessionalEducationProgramManager extends BusinessObjectManager
{
    public static AdditionalProfessionalEducationProgramManager instance()
    {
        return instance(AdditionalProfessionalEducationProgramManager.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler()
    {
        return new OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING);
    }

    @Bean
    public IDefaultComboDataSourceHandler territorialOrgUnitDSHandler()
    {
        return new OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationOrgUnitDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EducationOrgUnit.class)
        {

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                OrgUnit formativeOrgUnit = context.get(FORMATIVE_ORG_UNIT);
                if (formativeOrgUnit != null)
                {
                    dql.where(eq(property(EducationOrgUnit.formativeOrgUnit().fromAlias(alias)), value(formativeOrgUnit)));
                }

                OrgUnit territorialOrgUnit = context.get(TERRITORIAL_ORG_UNIT);
                if (territorialOrgUnit != null)
                {
                    dql.where(eq(property(EducationOrgUnit.territorialOrgUnit().fromAlias(alias)), value(territorialOrgUnit)));
                }
            }
        };
        handler.where(EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().additional(), true);
        return handler;
    }

    @Bean
    public IDefaultComboDataSourceHandler producingOrgUnitDSHandler()
    {
        return new OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> diplomaQualificationDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), DiplomaQualifications.class, DiplomaQualifications.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> diplomaTypeDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuDiplomaTypeApe.class, FefuDiplomaTypeApe.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> programStatusDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuProgramStatusApe.class, FefuProgramStatusApe.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> programPublicationStatusDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuProgramPublicationStatusApe.class, FefuProgramPublicationStatusApe.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationLevelDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EducationLevelAdditional.class)
        {

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                List<String> codes = Arrays.asList(POVYSHENIE_KVALIFIKATSII, PROFESSIONALNAYA_PEREPODGOTOVKA);
                dql.where(in(property(EducationLevelAdditional.qualification().code().fromAlias(alias)), codes));
            }
        };
        handler.order(EducationLevelAdditional.title());
        return handler;
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developFormDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuDevelopFormApe.class, FefuDevelopFormApe.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developConditionDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuDevelopConditionApe.class, FefuDevelopConditionApe.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developTechDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuDevelopTechApe.class, FefuDevelopTechApe.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developPeriodDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuDevelopPeriodApe.class, FefuDevelopPeriodApe.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> issuedDocumentDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuIssuedDocumentApe.class, FefuIssuedDocumentApe.title());
    }
}
