/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionTitleTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.*;

import java.util.Collection;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
    private Long _id;
    private ISelectModel _epvBlockModel;
    private EppEduPlanVersionBlock _selectedEpvBlock;
    private Map<Long, Block> _blockMap;
    private Collection<Block> _blocks;
    private Block _currentBlock;


    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public ISelectModel getEpvBlockModel()
    {
        return _epvBlockModel;
    }

    public void setEpvBlockModel(ISelectModel epvBlockModel)
    {
        _epvBlockModel = epvBlockModel;
    }

    public EppEduPlanVersionBlock getSelectedEpvBlock()
    {
        return _selectedEpvBlock;
    }

    public void setSelectedEpvBlock(EppEduPlanVersionBlock selectedEpvBlock)
    {
        _selectedEpvBlock = selectedEpvBlock;
    }

    public Map<Long, Block> getBlockMap()
    {
        return _blockMap;
    }

    public void setBlockMap(Map<Long, Block> blockMap)
    {
        _blockMap = blockMap;
    }

    public Collection<Block> getBlocks()
    {
        return _blocks;
    }

    public void setBlocks(Collection<Block> blocks)
    {
        _blocks = blocks;
    }

    public Block getCurrentBlock()
    {
        return _currentBlock;
    }

    public void setCurrentBlock(Block currentBlock)
    {
        _currentBlock = currentBlock;
    }

    public static class Block
    {
        private Long _id;
        private String _caption;
        private FefuEpvBlockTitle _blockTitle;
        private String _level;
        private StaticListDataSource<FefuDeveloper> _developerDataSource;
        private StaticListDataSource<FefuQualification> _qualificationDataSource;
        private StaticListDataSource<FefuSpeciality> _specialityDataSource;
        private StaticListDataSource<FefuActivityType> _activityTypeDataSource;

        public Block(Long id, String caption, FefuEpvBlockTitle blockTitle, String level, StaticListDataSource<FefuDeveloper> developerDataSource, StaticListDataSource<FefuQualification> qualificationDataSource, StaticListDataSource<FefuSpeciality> specialityDataSource, StaticListDataSource<FefuActivityType> activityTypeDataSource)
        {
            _id = id;
            _caption = caption;
            _blockTitle = blockTitle;
            _level = level;
            _developerDataSource = developerDataSource;
            _qualificationDataSource = qualificationDataSource;
            _specialityDataSource = specialityDataSource;
            _activityTypeDataSource = activityTypeDataSource;
        }


        public Long getId()
        {
            return _id;
        }

        public String getCaption()
        {
            return _caption;
        }

        public FefuEpvBlockTitle getBlockTitle()
        {
            return _blockTitle;
        }

        public String getLevel()
        {
            return _level;
        }

        public StaticListDataSource<FefuDeveloper> getDeveloperDataSource()
        {
            return _developerDataSource;
        }

        public StaticListDataSource<FefuQualification> getQualificationDataSource()
        {
            return _qualificationDataSource;
        }

        public StaticListDataSource<FefuSpeciality> getSpecialityDataSource()
        {
            return _specialityDataSource;
        }

        public StaticListDataSource<FefuActivityType> getActivityTypeDataSource()
        {
            return _activityTypeDataSource;
        }
    }
}
