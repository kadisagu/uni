package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение персоны для НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuNsiPersonExtGen extends EntityBase
 implements INaturalIdentifiable<FefuNsiPersonExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuNsiPersonExt";
    public static final String ENTITY_NAME = "fefuNsiPersonExt";
    public static final int VERSION_HASH = -1249755967;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON = "person";
    public static final String P_LOGIN = "login";

    private Person _person;     // Физ. лицо
    private String _login;     // Логин

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Физ. лицо. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Физ. лицо. Свойство не может быть null и должно быть уникальным.
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Логин. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLogin()
    {
        return _login;
    }

    /**
     * @param login Логин. Свойство не может быть null.
     */
    public void setLogin(String login)
    {
        dirty(_login, login);
        _login = login;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuNsiPersonExtGen)
        {
            if (withNaturalIdProperties)
            {
                setPerson(((FefuNsiPersonExt)another).getPerson());
            }
            setLogin(((FefuNsiPersonExt)another).getLogin());
        }
    }

    public INaturalId<FefuNsiPersonExtGen> getNaturalId()
    {
        return new NaturalId(getPerson());
    }

    public static class NaturalId extends NaturalIdBase<FefuNsiPersonExtGen>
    {
        private static final String PROXY_NAME = "FefuNsiPersonExtNaturalProxy";

        private Long _person;

        public NaturalId()
        {}

        public NaturalId(Person person)
        {
            _person = ((IEntity) person).getId();
        }

        public Long getPerson()
        {
            return _person;
        }

        public void setPerson(Long person)
        {
            _person = person;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuNsiPersonExtGen.NaturalId) ) return false;

            FefuNsiPersonExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getPerson(), that.getPerson()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPerson());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPerson());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuNsiPersonExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuNsiPersonExt.class;
        }

        public T newInstance()
        {
            return (T) new FefuNsiPersonExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "person":
                    return obj.getPerson();
                case "login":
                    return obj.getLogin();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "login":
                    obj.setLogin((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "person":
                        return true;
                case "login":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "person":
                    return true;
                case "login":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "person":
                    return Person.class;
                case "login":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuNsiPersonExt> _dslPath = new Path<FefuNsiPersonExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuNsiPersonExt");
    }
            

    /**
     * @return Физ. лицо. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonExt#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Логин. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonExt#getLogin()
     */
    public static PropertyPath<String> login()
    {
        return _dslPath.login();
    }

    public static class Path<E extends FefuNsiPersonExt> extends EntityPath<E>
    {
        private Person.Path<Person> _person;
        private PropertyPath<String> _login;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Физ. лицо. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonExt#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Логин. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonExt#getLogin()
     */
        public PropertyPath<String> login()
        {
            if(_login == null )
                _login = new PropertyPath<String>(FefuNsiPersonExtGen.P_LOGIN, this);
            return _login;
        }

        public Class getEntityClass()
        {
            return FefuNsiPersonExt.class;
        }

        public String getEntityName()
        {
            return "fefuNsiPersonExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
