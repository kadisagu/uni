package ru.tandemservice.unifefu.component.entrant.EntrantRequestTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.unifefu.UniFefuDefines;

/**
 * @author amakarova
 * @since 14/05/2013
 */
public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Controller
{
    @Override
    @SuppressWarnings("unchecked")
    public void onClickPrintAuthCard(IBusinessComponent component)
    {
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniFefuDefines.ENTRANT_AUTH_CARD), component.<Long>getListenerParameter());
    }
}
