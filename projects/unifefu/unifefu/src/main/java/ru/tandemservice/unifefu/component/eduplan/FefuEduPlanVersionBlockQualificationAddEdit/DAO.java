/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionBlockQualificationAddEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.FefuQualification;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getQualificationId() == null)
        {
            FefuQualification qualification = new FefuQualification();
            qualification.setBlock(get(EppEduPlanVersionBlock.class, model.getBlockId()));
            model.setQualification(qualification);

        } else
        {
            model.setQualification(get(FefuQualification.class, model.getQualificationId()));
        }
    }
}