/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unifefu.entity.FefuStudentMarkHistory;
import ru.tandemservice.unifefu.entity.catalog.FefuBrsDocTemplate;
import ru.tandemservice.unifefu.entity.report.FefuBrsDelayGradingReport;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAction;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 12/10/13
 */
public class FefuBrsDelayGradingReportDAO extends BaseModifyAggregateDAO implements IFefuBrsDelayGradingReportDAO
{
    @Override
    public FefuBrsDelayGradingReport createReport(FefuBrsDelayGradingReportParams reportParams)
    {
        FefuBrsDelayGradingReport report = new FefuBrsDelayGradingReport();
        report.setFormingDate(new Date());
        report.setFormativeOrgUnit(reportParams.getFormativeOrgUnit() != null ? reportParams.getFormativeOrgUnit().getTitle() : null);
        report.setResponsibilityOrgUnit(reportParams.getResponsibilityOrgUnit() != null ? reportParams.getResponsibilityOrgUnit().getTitle() : null);
        report.setOrgUnit(reportParams.getOrgUnit());
        report.setYearPart(reportParams.getYearPart().getTitle());
        if (null != reportParams.getPpsList() && !reportParams.getPpsList().isEmpty())
            report.setTeacher(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getPpsList(), PpsEntry.title()), ", "));
        if (null != reportParams.getGroupList() && !reportParams.getGroupList().isEmpty())
            report.setGroup(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getGroupList(), Group.title()), ", "));
        report.setCheckDate(reportParams.getCheckDate());

        DatabaseFile content = new DatabaseFile();
        content.setContent(print(reportParams));
        content.setFilename("fefuBrsDelayGradingReport");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        baseCreate(content);
        report.setContent(content);
        baseCreate(report);

        return report;
    }

    private byte[] print(FefuBrsDelayGradingReportParams reportParams)
    {
        FefuBrsDocTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(FefuBrsDocTemplate.class, "fefuBrsDelayGradingReport");
        RtfDocument document = new RtfReader().read(templateDocument.getContent());
        RtfInjectModifier modifier = new RtfInjectModifier();
        String orgUnit = reportParams.getFormativeOrgUnit() != null ? reportParams.getFormativeOrgUnit().getPrintTitle() : "";
        if (reportParams.getResponsibilityOrgUnit() != null)
        {
            if (!orgUnit.isEmpty())
                orgUnit += ", ";
            orgUnit += reportParams.getResponsibilityOrgUnit().getPrintTitle();
        }
        String eduYearPart = reportParams.getYearPart().getTitle();

        modifier.put("reportParams", orgUnit + ", " + eduYearPart + ", дата проверки - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(reportParams.getCheckDate()));
        modifier.modify(document);
        fillTable(document, reportParams);
        return RtfUtil.toByteArray(document);
    }

    private void fillTable(RtfDocument document, final FefuBrsDelayGradingReportParams reportParams)
    {
        final Map<TrJournal, Map<EppRealEduGroup, List<TrEduGroupEvent>>> eduGroupEventMap = Maps.newHashMap();
        final Map<TrJournal, Map<EppRealEduGroup, Map<TrEduGroupEvent, Map<EppStudentWorkPlanElement, StudentEventMark>>>> marksMap = Maps.newHashMap();
        final Map<PpsEntryByEmployeePost, Map<TrJournal, List<EppRealEduGroup>>> ppsJournalMap = Maps.newHashMap();
        final Map<EppRealEduGroup, Map<Group, List<EppStudentWorkPlanElement>>> eduGroupStudentMap = Maps.newHashMap();

        // Все запланированые события в журнале по КМ и оценки
        DQLSelectBuilder eventBuilder = getEventBuilder(reportParams);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "eg")
                .where(eq(property("ge", TrEduGroupEvent.group().id()), property("eg", EppRealEduGroup4LoadTypeRow.group().id())))
                .where(isNull(property("eg", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().removalDate())));

        if (null != reportParams.getGroupList() && !reportParams.getGroupList().isEmpty())
        {
            dql.where(in(property("eg", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group()), reportParams.getGroupList()));
        }
        else if (reportParams.getFormativeOrgUnit() != null)
        {
            dql.where(eqValue(property("eg", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().educationOrgUnit().formativeOrgUnit()), reportParams.getFormativeOrgUnit()));
        }

        eventBuilder.where(exists(dql.buildQuery()));

        for (TrEduGroupEvent event : createStatement(eventBuilder).<TrEduGroupEvent>list())
        {
            if (event.getJournalEvent() instanceof TrEventAction)
            {
                EppRealEduGroup eduGroup = event.getGroup();
                TrJournal journal = event.getJournalEvent().getJournalModule().getJournal();

                if (!eduGroupEventMap.containsKey(journal))
                    eduGroupEventMap.put(journal, Maps.<EppRealEduGroup, List<TrEduGroupEvent>>newHashMap());
                if (!eduGroupEventMap.get(journal).containsKey(eduGroup))
                    eduGroupEventMap.get(journal).put(eduGroup, Lists.<TrEduGroupEvent>newArrayList());
                if (!eduGroupEventMap.get(journal).get(eduGroup).contains(event))
                    eduGroupEventMap.get(journal).get(eduGroup).add(event);
            }
        }

        // оценки
        DQLSelectBuilder marksSubBuilder = getEventBuilder(reportParams);
        DQLSelectBuilder existsDql = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "eg")
                .where(eq(property("ge", TrEduGroupEvent.group().id()), property("eg", EppRealEduGroup4LoadTypeRow.group().id())))
                .where(isNull(property("eg", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().removalDate())));

        if (reportParams.getGroupList() != null && !reportParams.getGroupList().isEmpty())
        {
            existsDql.where(in(property("eg", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group()), reportParams.getGroupList()));
        }
        else if (reportParams.getFormativeOrgUnit() != null)
        {
            existsDql.where(eqValue(property("eg", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().educationOrgUnit().formativeOrgUnit()), reportParams.getFormativeOrgUnit()));
        }

        marksSubBuilder.where(exists(existsDql.buildQuery()));

        DQLSelectBuilder marksBuilder = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "m");
        marksBuilder.where(in(property("m", TrEduGroupEventStudent.event()), marksSubBuilder.buildQuery()));
        marksBuilder.where(isNull(property("m", TrEduGroupEventStudent.transferDate())));


        DQLSelectBuilder fefuMarksSubBuilder = new DQLSelectBuilder().fromEntity(FefuStudentMarkHistory.class, "fm");
        fefuMarksSubBuilder.column(property("fm", FefuStudentMarkHistory.trEduGroupEventStudent().id()), "eventId");
        fefuMarksSubBuilder.column(DQLFunctions.min(property("fm", FefuStudentMarkHistory.markDate())), "minDate");
        fefuMarksSubBuilder.group(property("fm", FefuStudentMarkHistory.trEduGroupEventStudent()));
        fefuMarksSubBuilder.where(le(property("fm", FefuStudentMarkHistory.markDate()), valueTimestamp(reportParams.getCheckDate())));
        fefuMarksSubBuilder.where(in(property("fm", FefuStudentMarkHistory.trEduGroupEventStudent().event()), marksSubBuilder.buildQuery()));
        fefuMarksSubBuilder.where(isNull(property("fm", FefuStudentMarkHistory.trEduGroupEventStudent().transferDate())));


        List<FefuStudentMarkHistory> studentMarks = createStatement(
                new DQLSelectBuilder().
                        fromEntity(FefuStudentMarkHistory.class, "f").column("f").
                        fromDataSource(fefuMarksSubBuilder.buildQuery(), "marks").
                        where(eq(property("f", FefuStudentMarkHistory.trEduGroupEventStudent().id()), property("marks.eventId"))).
                        where(eq(property("f", FefuStudentMarkHistory.markDate()), property("marks.minDate")))).list();

        for (TrEduGroupEventStudent eventStudent : createStatement(marksBuilder).<TrEduGroupEventStudent>list())
        {
            TrJournal journal = eventStudent.getJournal();
            EppRealEduGroup eduGroup = eventStudent.getEvent().getGroup();
            TrEduGroupEvent event = eventStudent.getEvent();
            EppStudentWorkPlanElement student = eventStudent.getStudentWpe();
            if (!marksMap.containsKey(journal))
                marksMap.put(journal, Maps.<EppRealEduGroup, Map<TrEduGroupEvent, Map<EppStudentWorkPlanElement, StudentEventMark>>>newHashMap());
            if (!marksMap.get(journal).containsKey(eduGroup))
                marksMap.get(journal).put(eduGroup, Maps.<TrEduGroupEvent, Map<EppStudentWorkPlanElement, StudentEventMark>>newHashMap());
            if (!marksMap.get(journal).get(eduGroup).containsKey(event))
                marksMap.get(journal).get(eduGroup).put(event, Maps.<EppStudentWorkPlanElement, StudentEventMark>newHashMap());
            if (!marksMap.get(journal).get(eduGroup).get(event).containsKey(student))
            {
                FefuStudentMarkHistory eventMark = null;
                for (FefuStudentMarkHistory mark : studentMarks)
                {
                    if (mark.getTrEduGroupEventStudent().getId().equals(eventStudent.getId()))
                    {
                        eventMark = mark;
                        break;
                    }

                }
                StudentEventMark studentEventMark = new StudentEventMark(eventStudent, eventMark, reportParams.getCheckDate());

                marksMap.get(journal).get(eduGroup).get(event).put(student, studentEventMark);


            }
        }

        // студенты
        DQLSelectBuilder studentsBuilder = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "st");

        if (reportParams.getGroupList() != null && !reportParams.getGroupList().isEmpty())
        {
            studentsBuilder.where(in(property("st", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group()), reportParams.getGroupList()));
        }
        else if (reportParams.getFormativeOrgUnit() != null)
        {
            studentsBuilder.where(eqValue(property("st", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().educationOrgUnit().formativeOrgUnit()), reportParams.getFormativeOrgUnit()));
        }


        for (EppRealEduGroup4LoadTypeRow row : createStatement(studentsBuilder).<EppRealEduGroup4LoadTypeRow>list())
        {
            EppStudentWorkPlanElement student = row.getStudentWpePart().getStudentWpe();
            Group group = row.getStudentWpePart().getStudentWpe().getStudent().getGroup();
            EppRealEduGroup eduGroup = row.getGroup();

            if (!eduGroupStudentMap.containsKey(eduGroup))
                eduGroupStudentMap.put(eduGroup, Maps.<Group, List<EppStudentWorkPlanElement>>newHashMap());
            if (!eduGroupStudentMap.get(eduGroup).containsKey(group))
                eduGroupStudentMap.get(eduGroup).put(group, Lists.<EppStudentWorkPlanElement>newArrayList());
            if (!eduGroupStudentMap.get(eduGroup).get(group).contains(student))
                eduGroupStudentMap.get(eduGroup).get(group).add(student);
        }


        // преподаватели
        DQLSelectBuilder ppsSubBuilder = getEventBuilder(reportParams);
        ppsSubBuilder.column(property("ge", TrEduGroupEvent.journalEvent().journalModule().journal()));
        ppsSubBuilder.where(exists(
                new DQLSelectBuilder().fromEntity(EppPpsCollectionItem.class, "item").
                        where(eq(property("item", EppPpsCollectionItem.pps().id()), property("pps", PpsEntryByEmployeePost.id()))).
                        where(eq(property("item", EppPpsCollectionItem.list().id()), property("ge", TrEduGroupEvent.group().id()))).
                        buildQuery()));
        ppsSubBuilder.distinct();

        DQLSelectBuilder ppsBuilder = new DQLSelectBuilder().fromEntity(TrJournalGroup.class, "jg");
        ppsBuilder.joinEntity("jg", DQLJoinType.inner, PpsEntryByEmployeePost.class, "pps",
                              exists(new DQLSelectBuilder()
                                             .fromEntity(EppPpsCollectionItem.class, "item").
                                              where(eq(property("item", EppPpsCollectionItem.pps().id()), property("pps", PpsEntryByEmployeePost.id()))).
                                              where(eq(property("item", EppPpsCollectionItem.list().id()), property("jg", TrJournalGroup.group().id()))).
                                              buildQuery()));
        ppsBuilder.column("jg");
        ppsBuilder.column("pps");
        ppsBuilder.fetchPath(DQLJoinType.inner, TrJournalGroup.journal().fromAlias("jg"), "journal");
        ppsBuilder.fetchPath(DQLJoinType.inner, TrJournalGroup.group().type().fromAlias("jg"), "grouptype");
        ppsBuilder.where(in(property("jg", TrJournalGroup.journal()), ppsSubBuilder.buildQuery()));
        if (CollectionUtils.isNotEmpty(reportParams.getPpsList()))
            ppsBuilder.where(in(property("pps"), reportParams.getPpsList()));

        for (Object row : createStatement(ppsBuilder).list())
        {
            TrJournalGroup journalGroup = (TrJournalGroup) ((Object[]) row)[0];
            TrJournal journal = journalGroup.getJournal();
            EppRealEduGroup eduGroup = journalGroup.getGroup();
            PpsEntryByEmployeePost pps = (PpsEntryByEmployeePost) ((Object[]) row)[1];
            if (!ppsJournalMap.containsKey(pps))
                ppsJournalMap.put(pps, Maps.<TrJournal, List<EppRealEduGroup>>newHashMap());
            if (!ppsJournalMap.get(pps).containsKey(journal))
                ppsJournalMap.get(pps).put(journal, Lists.<EppRealEduGroup>newArrayList());
            if (!ppsJournalMap.get(pps).get(journal).contains(eduGroup))
                ppsJournalMap.get(pps).get(journal).add(eduGroup);
        }

        List<GroupTrJournalDataForPps> journalDataList = Lists.newArrayList();

        for (Map.Entry<PpsEntryByEmployeePost, Map<TrJournal, List<EppRealEduGroup>>> ppsEntry : ppsJournalMap.entrySet())
        {
            if (null != reportParams.getPpsList() && !reportParams.getPpsList().isEmpty() && !reportParams.getPpsList().contains(ppsEntry.getKey()))
                continue;

            Map<TrJournal, List<EppRealEduGroup>> journalMap = ppsEntry.getValue();
            PpsEntryByEmployeePost pps = ppsEntry.getKey();
            Map<TrJournal, Map<Group, Map<TrEduGroupEvent, Map<EppStudentWorkPlanElement, StudentEventMark>>>> ppsDataMap = Maps.newHashMap();

            for (Map.Entry<TrJournal, List<EppRealEduGroup>> journalEntry : journalMap.entrySet())
            {
                TrJournal journal = journalEntry.getKey();
                if (null != journalEntry.getValue() && !journalEntry.getValue().isEmpty())
                {
                    Map<EppRealEduGroup, List<TrEduGroupEvent>> journalEventMap = eduGroupEventMap.get(journal);
                    if (journalEventMap == null)
                        continue;

                    for (EppRealEduGroup eduGroup : journalEntry.getValue())
                    {
                        List<TrEduGroupEvent> events = journalEventMap.get(eduGroup);
                        if (events == null || events.isEmpty())
                            continue;

                        Map<Group, List<EppStudentWorkPlanElement>> groupStudentMap = eduGroupStudentMap.get(eduGroup);

                        if (null != groupStudentMap && !groupStudentMap.isEmpty() && !events.isEmpty())
                        {
                            Map<EppRealEduGroup, Map<TrEduGroupEvent, Map<EppStudentWorkPlanElement, StudentEventMark>>> loadTypeMapMap = marksMap.get(journal);
                            Map<TrEduGroupEvent, Map<EppStudentWorkPlanElement, StudentEventMark>> journalMarksMap = (loadTypeMapMap != null) ? loadTypeMapMap.get(eduGroup) : null;

                            for (Map.Entry<Group, List<EppStudentWorkPlanElement>> groupEntry : groupStudentMap.entrySet())
                            {
                                Map<Group, Map<TrEduGroupEvent, Map<EppStudentWorkPlanElement, StudentEventMark>>> groupMap = ppsDataMap.get(journal);
                                if (groupMap == null)
                                    ppsDataMap.put(journal, groupMap = new HashMap<>());

                                Map<TrEduGroupEvent, Map<EppStudentWorkPlanElement, StudentEventMark>> eventMap = groupMap.get(groupEntry.getKey());
                                if (eventMap == null)
                                    groupMap.put(groupEntry.getKey(), eventMap = new HashMap<>());

                                for (TrEduGroupEvent eduGroupEvent : events)
                                {
                                    Map<EppStudentWorkPlanElement, StudentEventMark> eventSlotMap = eventMap.get(eduGroupEvent);
                                    if (eventSlotMap == null)
                                        eventMap.put(eduGroupEvent, eventSlotMap = new HashMap<>());

                                    Map<EppStudentWorkPlanElement, StudentEventMark> slotMap = journalMarksMap != null ? journalMarksMap.get(eduGroupEvent) : null;

                                    for (EppStudentWorkPlanElement student : groupEntry.getValue())
                                    {
                                        if (!eventSlotMap.containsKey(student))
                                        {
                                            eventSlotMap.put(student, slotMap != null ? slotMap.get(student) : null);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }

            for (Map.Entry<TrJournal, Map<Group, Map<TrEduGroupEvent, Map<EppStudentWorkPlanElement, StudentEventMark>>>> journalEntry : ppsDataMap.entrySet())
            {
                TrJournal journal = journalEntry.getKey();
                for (Map.Entry<Group, Map<TrEduGroupEvent, Map<EppStudentWorkPlanElement, StudentEventMark>>> groupEntry : journalEntry.getValue().entrySet())
                {
                    Group group = groupEntry.getKey();
                    int maxDelay = 0;
                    int withoutMarks = 0;
                    int delay = 0;
                    for (Map.Entry<TrEduGroupEvent, Map<EppStudentWorkPlanElement, StudentEventMark>> jGroupMarkEntry : groupEntry.getValue().entrySet())
                    {
                        if (jGroupMarkEntry.getKey().getJournalEvent() instanceof TrEventAction)
                        {
                            int eventMaxDelay = 0;
                            int withoutDelay = 0;
                            int eventWithoutMarks = 0;
                            for (Map.Entry<EppStudentWorkPlanElement, StudentEventMark> entry : jGroupMarkEntry.getValue().entrySet())
                            {
                                if (null == entry.getValue())
                                {
                                    withoutMarks++;
                                    eventWithoutMarks++;
                                }
                                else
                                {
                                    StudentEventMark mark = entry.getValue();
                                    if (mark._hasMark)
                                    {
                                        if (0 == mark.getDelayDays()) withoutDelay++;
                                        else if (mark.getDelayDays() > maxDelay)
                                            eventMaxDelay = mark.getDelayDays();
                                    }
                                    else
                                    {
                                        withoutMarks++;
                                        eventWithoutMarks++;
                                    }
                                }
                            }
                            if (withoutDelay < jGroupMarkEntry.getValue().size() && eventMaxDelay > maxDelay)
                                maxDelay = eventMaxDelay;
                            if (eventWithoutMarks == jGroupMarkEntry.getValue().size() && jGroupMarkEntry.getKey().getScheduleEvent().getDurationEnd().before(reportParams.getCheckDate()))
                                delay++;
                        }
                    }
                    journalDataList.add(new GroupTrJournalDataForPps(pps, group, journal, maxDelay, withoutMarks, delay));
                }
            }
        }

        List<String[]> tableRows = Lists.newArrayList();
        if (!journalDataList.isEmpty())
        {
            Collections.sort(journalDataList, (o1, o2) -> {
                if (o1.getMaxDelay() != o2.getMaxDelay())
                    return o2.getMaxDelay() - o1.getMaxDelay();
                if (!o1.getPps().getFullFio().equals(o2.getPps().getFullFio()))
                    return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getPps().getFullFio(), o2.getPps().getFullFio());
                if (!o1.getGroup().getTitle().equals(o2.getGroup().getTitle()))
                    return o1.getGroup().getTitle().compareTo(o2.getGroup().getTitle());
                if (!o1.getJournal().getRegistryElementPart().getTitle().equals(o2.getJournal().getRegistryElementPart().getTitle()))
                    return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getJournal().getRegistryElementPart().getTitle(), o2.getJournal().getRegistryElementPart().getTitle());
                if (o1.getWithoutMarks() != o2.getWithoutMarks())
                    return o1.getWithoutMarks() - o2.getWithoutMarks();
                return 0;
            });

            int rowNum = 1;
            for (GroupTrJournalDataForPps data : journalDataList)
            {
                tableRows.add(data.getRow(rowNum++));
            }

        }

        if (tableRows.isEmpty())
            throw new ApplicationException("Нет данных для отчета");

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", tableRows.toArray(new String[0][0]));
        tableModifier.modify(document);

    }

    private DQLSelectBuilder getEventBuilder(FefuBrsDelayGradingReportParams reportParams)
    {
        DQLSelectBuilder eventBuilder = new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ge");
        eventBuilder.where(isNotNull(property("ge", TrEduGroupEvent.scheduleEvent())));
        eventBuilder.where(eq(property("ge", TrEduGroupEvent.journalEvent().journalModule().journal().yearPart().id()), value(reportParams.getYearPart().getId())));
        eventBuilder.where(le(property("ge", TrEduGroupEvent.scheduleEvent().durationEnd()), valueTimestamp(reportParams.getCheckDate())));
        if (null != reportParams.getPpsList() && !reportParams.getPpsList().isEmpty())
        {
            eventBuilder.where(exists(new DQLSelectBuilder().fromEntity(EppPpsCollectionItem.class, "item")
                                              .where(in(property("item", EppPpsCollectionItem.pps()), reportParams.getPpsList()))
                                              .where(eq(property("item", EppPpsCollectionItem.list().id()), property("ge", TrEduGroupEvent.group().id())))
                                              .buildQuery()));
        }

        if (reportParams.getResponsibilityOrgUnit() != null)
        {
            eventBuilder.where(eqValue(property("ge", TrEduGroupEvent.journalEvent().journalModule().journal().registryElementPart().registryElement().owner()), reportParams.getResponsibilityOrgUnit()));
        }

        return eventBuilder;
    }

    class GroupTrJournalDataForPps
    {
        private PpsEntryByEmployeePost _pps;
        private Group _group;
        private TrJournal _journal;
        private int _maxDelay = 0;
        private int _withoutMarks = 0;
        private int _delay = 0;

        GroupTrJournalDataForPps(PpsEntryByEmployeePost pps, Group group, TrJournal journal, int maxDelay, int withoutMarks, int delay)
        {
            _pps = pps;
            _group = group;
            _journal = journal;
            _maxDelay = maxDelay;
            _withoutMarks = withoutMarks;
            _delay = delay;
        }

        public PpsEntryByEmployeePost getPps()
        {
            return _pps;
        }

        public void setPps(PpsEntryByEmployeePost pps)
        {
            _pps = pps;
        }

        public Group getGroup()
        {
            return _group;
        }

        public void setGroup(Group group)
        {
            _group = group;
        }

        public TrJournal getJournal()
        {
            return _journal;
        }

        public void setJournal(TrJournal journal)
        {
            _journal = journal;
        }

        public int getMaxDelay()
        {
            return _maxDelay;
        }

        public int getWithoutMarks()
        {
            return _withoutMarks;
        }

        public int getDelay()
        {
            return _delay;
        }

        public String[] getRow(int rowNum)
        {
            List<String> cells = Lists.newArrayList();
            cells.add(String.valueOf(rowNum));
            cells.add(_pps.getFullFio());
            cells.add(_journal.getRegistryElementPart().getRegistryElement().getTitle());
            cells.add(_group.getTitle());
            cells.add(String.valueOf(_maxDelay));
            cells.add(String.valueOf(_withoutMarks));
            cells.add(String.valueOf(_delay));
            return cells.toArray(new String[0]);
        }
    }

    class StudentEventMark
    {
        private TrEduGroupEventStudent _event;
        private FefuStudentMarkHistory _mark;
        private Date _checkDate;

        private int _delayDays = 0;
        private boolean _hasMark;

        StudentEventMark(TrEduGroupEventStudent event, FefuStudentMarkHistory mark, Date checkDate)
        {
            _event = event;
            _mark = mark;
            _checkDate = checkDate;

            fillMark();
        }

        public TrEduGroupEventStudent getEvent()
        {
            return _event;
        }

        public void setEvent(TrEduGroupEventStudent event)
        {
            _event = event;
        }

        public FefuStudentMarkHistory getMark()
        {
            return _mark;
        }

        public void setMark(FefuStudentMarkHistory mark)
        {
            _mark = mark;
        }

        public Date getCheckDate()
        {
            return _checkDate;
        }

        public void setCheckDate(Date checkDate)
        {
            _checkDate = checkDate;
        }

        public int getDelayDays()
        {
            return _delayDays;
        }

        public void setDelayDays(int delayDays)
        {
            _delayDays = delayDays;
        }

        public boolean isHasMark()
        {
            return _hasMark;
        }

        public void setHasMark(boolean hasMark)
        {
            _hasMark = hasMark;
        }

        private void fillMark()
        {
            if (null == _event)
                _hasMark = false;
            else
            {
                if (null == _mark)
                    _hasMark = false;
                else
                {
                    if (null != _event.getGrade())
                    {
                        _hasMark = true;
                        Date endDate = _event.getEvent().getScheduleEvent().getDurationEnd();
                        if (endDate.before(_mark.getMarkDate()) && _mark.getMarkDate().before(_checkDate))
                        {
                            LocalDate start = new LocalDate(endDate);
                            LocalDate end = new LocalDate(_mark.getMarkDate());
                            _delayDays = Days.daysBetween(start, end).getDays();
                        }
                    }
                    else _hasMark = false;
                }
            }
        }


    }
}
