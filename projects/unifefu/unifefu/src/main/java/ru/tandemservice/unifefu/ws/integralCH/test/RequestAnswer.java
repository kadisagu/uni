/**
 * RequestAnswer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.integralCH.test;

public class RequestAnswer  implements java.io.Serializable {
    private java.lang.String eduLevel;

    private java.lang.String course;

    private java.lang.String term;

    private java.lang.String school;

    private java.lang.String cathedra;

    private java.lang.String specialty;

    private java.lang.String discipline;

    private java.lang.String lecturer;

    public RequestAnswer() {
    }

    public RequestAnswer(
           java.lang.String eduLevel,
           java.lang.String course,
           java.lang.String term,
           java.lang.String school,
           java.lang.String cathedra,
           java.lang.String specialty,
           java.lang.String discipline,
           java.lang.String lecturer) {
           this.eduLevel = eduLevel;
           this.course = course;
           this.term = term;
           this.school = school;
           this.cathedra = cathedra;
           this.specialty = specialty;
           this.discipline = discipline;
           this.lecturer = lecturer;
    }


    /**
     * Gets the eduLevel value for this RequestAnswer.
     * 
     * @return eduLevel
     */
    public java.lang.String getEduLevel() {
        return eduLevel;
    }


    /**
     * Sets the eduLevel value for this RequestAnswer.
     * 
     * @param eduLevel
     */
    public void setEduLevel(java.lang.String eduLevel) {
        this.eduLevel = eduLevel;
    }


    /**
     * Gets the course value for this RequestAnswer.
     * 
     * @return course
     */
    public java.lang.String getCourse() {
        return course;
    }


    /**
     * Sets the course value for this RequestAnswer.
     * 
     * @param course
     */
    public void setCourse(java.lang.String course) {
        this.course = course;
    }


    /**
     * Gets the term value for this RequestAnswer.
     * 
     * @return term
     */
    public java.lang.String getTerm() {
        return term;
    }


    /**
     * Sets the term value for this RequestAnswer.
     * 
     * @param term
     */
    public void setTerm(java.lang.String term) {
        this.term = term;
    }


    /**
     * Gets the school value for this RequestAnswer.
     * 
     * @return school
     */
    public java.lang.String getSchool() {
        return school;
    }


    /**
     * Sets the school value for this RequestAnswer.
     * 
     * @param school
     */
    public void setSchool(java.lang.String school) {
        this.school = school;
    }


    /**
     * Gets the cathedra value for this RequestAnswer.
     * 
     * @return cathedra
     */
    public java.lang.String getCathedra() {
        return cathedra;
    }


    /**
     * Sets the cathedra value for this RequestAnswer.
     * 
     * @param cathedra
     */
    public void setCathedra(java.lang.String cathedra) {
        this.cathedra = cathedra;
    }


    /**
     * Gets the specialty value for this RequestAnswer.
     * 
     * @return specialty
     */
    public java.lang.String getSpecialty() {
        return specialty;
    }


    /**
     * Sets the specialty value for this RequestAnswer.
     * 
     * @param specialty
     */
    public void setSpecialty(java.lang.String specialty) {
        this.specialty = specialty;
    }


    /**
     * Gets the discipline value for this RequestAnswer.
     * 
     * @return discipline
     */
    public java.lang.String getDiscipline() {
        return discipline;
    }


    /**
     * Sets the discipline value for this RequestAnswer.
     * 
     * @param discipline
     */
    public void setDiscipline(java.lang.String discipline) {
        this.discipline = discipline;
    }


    /**
     * Gets the lecturer value for this RequestAnswer.
     * 
     * @return lecturer
     */
    public java.lang.String getLecturer() {
        return lecturer;
    }


    /**
     * Sets the lecturer value for this RequestAnswer.
     * 
     * @param lecturer
     */
    public void setLecturer(java.lang.String lecturer) {
        this.lecturer = lecturer;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RequestAnswer)) return false;
        RequestAnswer other = (RequestAnswer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.eduLevel==null && other.getEduLevel()==null) || 
             (this.eduLevel!=null &&
              this.eduLevel.equals(other.getEduLevel()))) &&
            ((this.course==null && other.getCourse()==null) || 
             (this.course!=null &&
              this.course.equals(other.getCourse()))) &&
            ((this.term==null && other.getTerm()==null) || 
             (this.term!=null &&
              this.term.equals(other.getTerm()))) &&
            ((this.school==null && other.getSchool()==null) || 
             (this.school!=null &&
              this.school.equals(other.getSchool()))) &&
            ((this.cathedra==null && other.getCathedra()==null) || 
             (this.cathedra!=null &&
              this.cathedra.equals(other.getCathedra()))) &&
            ((this.specialty==null && other.getSpecialty()==null) || 
             (this.specialty!=null &&
              this.specialty.equals(other.getSpecialty()))) &&
            ((this.discipline==null && other.getDiscipline()==null) || 
             (this.discipline!=null &&
              this.discipline.equals(other.getDiscipline()))) &&
            ((this.lecturer==null && other.getLecturer()==null) || 
             (this.lecturer!=null &&
              this.lecturer.equals(other.getLecturer())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEduLevel() != null) {
            _hashCode += getEduLevel().hashCode();
        }
        if (getCourse() != null) {
            _hashCode += getCourse().hashCode();
        }
        if (getTerm() != null) {
            _hashCode += getTerm().hashCode();
        }
        if (getSchool() != null) {
            _hashCode += getSchool().hashCode();
        }
        if (getCathedra() != null) {
            _hashCode += getCathedra().hashCode();
        }
        if (getSpecialty() != null) {
            _hashCode += getSpecialty().hashCode();
        }
        if (getDiscipline() != null) {
            _hashCode += getDiscipline().hashCode();
        }
        if (getLecturer() != null) {
            _hashCode += getLecturer().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestAnswer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://integralCH.ws.unifefu.tandemservice.ru/", "RequestAnswer"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eduLevel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eduLevel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("course");
        elemField.setXmlName(new javax.xml.namespace.QName("", "course"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("term");
        elemField.setXmlName(new javax.xml.namespace.QName("", "term"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("school");
        elemField.setXmlName(new javax.xml.namespace.QName("", "school"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cathedra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cathedra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("specialty");
        elemField.setXmlName(new javax.xml.namespace.QName("", "specialty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discipline");
        elemField.setXmlName(new javax.xml.namespace.QName("", "discipline"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lecturer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lecturer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
