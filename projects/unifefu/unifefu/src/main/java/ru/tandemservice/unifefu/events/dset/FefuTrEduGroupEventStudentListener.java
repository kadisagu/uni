package ru.tandemservice.unifefu.events.dset;

import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.unifefu.entity.FefuStudentMarkHistory;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: amakarova
 * Date: 07.11.13
 */
public class FefuTrEduGroupEventStudentListener extends ParamTransactionCompleteListener<Boolean>
{
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, TrEduGroupEventStudent.class, this);
    }

    @Override
    public Collection<Long> getIds(DSetEvent event)
    {
        return new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "a").column("a.id").createStatement(event.getContext()).list();
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        final Date currentDate = new Date();

        BatchUtils.execute(params, 128, elements ->
        {
            List<TrEduGroupEventStudent> events = new DQLSelectBuilder()
                    .fromEntity(TrEduGroupEventStudent.class, "event")
                    .where(in(property(TrEduGroupEventStudent.id().fromAlias("event")), elements))
                    .createStatement(session)
                    .list();

            for (TrEduGroupEventStudent event : events) {
                    FefuStudentMarkHistory markHistory = new DQLSelectBuilder().fromEntity(FefuStudentMarkHistory.class, "h")
                            .where(inDay(FefuStudentMarkHistory.markDate().fromAlias("h"), currentDate))
                            .where(eq(property(FefuStudentMarkHistory.trEduGroupEventStudent().fromAlias("h")), value(event)))
                            .createStatement(session)
                            .uniqueResult();

                    if (markHistory == null) {
                        session.save(new FefuStudentMarkHistory(event, currentDate, event.getGradeAsLong()));
                    } else {
                        markHistory.setGradeAsLong(event.getGradeAsLong());
                        session.save(markHistory);
                    }
                    session.flush();
            }
        });
        return super.beforeCompletion(session, params);
    }
}