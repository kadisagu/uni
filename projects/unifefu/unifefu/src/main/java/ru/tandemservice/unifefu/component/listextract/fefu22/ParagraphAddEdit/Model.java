/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu22.ParagraphAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuPerformConditionCourseTransferListExtract;

import java.util.List;

/**
 * @author Igor Belanov
 * @since 30.06.2016
 */
public class Model extends AbstractListParagraphAddEditModel<FefuPerformConditionCourseTransferListExtract> implements IGroupModel
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;

    private List<CompensationType> _compensationTypeList;
    private ISelectModel _groupListModel;

    private boolean _courseAndGroupDisabled;

    @Override
    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public boolean isCourseAndGroupDisabled()
    {
        return _courseAndGroupDisabled;
    }

    public void setCourseAndGroupDisabled(boolean courseAndGroupDisabled)
    {
        _courseAndGroupDisabled = courseAndGroupDisabled;
    }
}
