/* $Id $ */
package ru.tandemservice.unifefu.base.ext.SessionTransfer.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.base.ext.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutOpExtWrapper;
import ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutOpWrapper;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Rostuncev Savva
 * @since 26.03.2014
 */

public class SessionTransferExtDao extends UniBaseDao
{
    public static SessionTransferExtDao instance()
    {
        return new SessionTransferExtDao();
    }

    public void saveOrUpdateOutsideTransfer(List<? extends SessionTransferOutOpExtWrapper> rowListExt, List<? extends SessionTransferOutOpWrapper> rowList)
    {
        int i = 0;
        for (final SessionTransferOutOpWrapper item : rowList)
        {
            if (rowListExt.get(i).getWorkTimeDisc() != null)
            {
                SessionTransferOutsideOperation sessionTransferOutsideOperation = (SessionTransferOutsideOperation) DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(SessionTransferOutsideOperation.class, "a")
                        .where(eq(property(SessionTransferOutsideOperation.discipline().fromAlias("a")), value(item.getSourceDiscipline())))
                        .where(eq(property(SessionTransferOutsideOperation.controlAction().id().fromAlias("a")), value(item.getSourceControlAction())))
                        .where(eq(property(SessionTransferOutsideOperation.mark().fromAlias("a")), value(item.getSourceMark().toString())))
                        .where(like(property("a", SessionTransferOutsideOperation.comment().s()), value("%" + item.getComment() + "%")))
                ).get(0);

                SessionTransferOutsideOperationExt sessionTransferOutsideOperationExt = new SessionTransferOutsideOperationExt();
                sessionTransferOutsideOperationExt.setSessionTransferOutsideOperation(sessionTransferOutsideOperation);

                sessionTransferOutsideOperationExt.setWorkTimeDisc(rowListExt.get(i).getWorkTimeDisc());
                DataAccessServices.dao().save(sessionTransferOutsideOperationExt);
            }
            i++;
        }
    }
}
