package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.MdbViewEppDevelopGridMeta;
import ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебная сетка
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewEppDevelopGridMetaGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.MdbViewEppDevelopGridMeta";
    public static final String ENTITY_NAME = "mdbViewEppDevelopGridMeta";
    public static final int VERSION_HASH = 1361531346;
    private static IEntityMeta ENTITY_META;

    public static final String L_MDB_VIEW_EPP_EDU_PLAN_VERSION = "mdbViewEppEduPlanVersion";
    public static final String P_DEVELOP_GRID_META = "developGridMeta";

    private MdbViewEppEduPlanVersion _mdbViewEppEduPlanVersion;     // Версия учебного плана
    private String _developGridMeta; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     */
    @NotNull
    public MdbViewEppEduPlanVersion getMdbViewEppEduPlanVersion()
    {
        return _mdbViewEppEduPlanVersion;
    }

    /**
     * @param mdbViewEppEduPlanVersion Версия учебного плана. Свойство не может быть null.
     */
    public void setMdbViewEppEduPlanVersion(MdbViewEppEduPlanVersion mdbViewEppEduPlanVersion)
    {
        dirty(_mdbViewEppEduPlanVersion, mdbViewEppEduPlanVersion);
        _mdbViewEppEduPlanVersion = mdbViewEppEduPlanVersion;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getDevelopGridMeta()
    {
        return _developGridMeta;
    }

    /**
     * @param developGridMeta 
     */
    public void setDevelopGridMeta(String developGridMeta)
    {
        dirty(_developGridMeta, developGridMeta);
        _developGridMeta = developGridMeta;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewEppDevelopGridMetaGen)
        {
            setMdbViewEppEduPlanVersion(((MdbViewEppDevelopGridMeta)another).getMdbViewEppEduPlanVersion());
            setDevelopGridMeta(((MdbViewEppDevelopGridMeta)another).getDevelopGridMeta());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewEppDevelopGridMetaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewEppDevelopGridMeta.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewEppDevelopGridMeta();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mdbViewEppEduPlanVersion":
                    return obj.getMdbViewEppEduPlanVersion();
                case "developGridMeta":
                    return obj.getDevelopGridMeta();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mdbViewEppEduPlanVersion":
                    obj.setMdbViewEppEduPlanVersion((MdbViewEppEduPlanVersion) value);
                    return;
                case "developGridMeta":
                    obj.setDevelopGridMeta((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mdbViewEppEduPlanVersion":
                        return true;
                case "developGridMeta":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mdbViewEppEduPlanVersion":
                    return true;
                case "developGridMeta":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mdbViewEppEduPlanVersion":
                    return MdbViewEppEduPlanVersion.class;
                case "developGridMeta":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewEppDevelopGridMeta> _dslPath = new Path<MdbViewEppDevelopGridMeta>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewEppDevelopGridMeta");
    }
            

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppDevelopGridMeta#getMdbViewEppEduPlanVersion()
     */
    public static MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> mdbViewEppEduPlanVersion()
    {
        return _dslPath.mdbViewEppEduPlanVersion();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEppDevelopGridMeta#getDevelopGridMeta()
     */
    public static PropertyPath<String> developGridMeta()
    {
        return _dslPath.developGridMeta();
    }

    public static class Path<E extends MdbViewEppDevelopGridMeta> extends EntityPath<E>
    {
        private MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> _mdbViewEppEduPlanVersion;
        private PropertyPath<String> _developGridMeta;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppDevelopGridMeta#getMdbViewEppEduPlanVersion()
     */
        public MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> mdbViewEppEduPlanVersion()
        {
            if(_mdbViewEppEduPlanVersion == null )
                _mdbViewEppEduPlanVersion = new MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion>(L_MDB_VIEW_EPP_EDU_PLAN_VERSION, this);
            return _mdbViewEppEduPlanVersion;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEppDevelopGridMeta#getDevelopGridMeta()
     */
        public PropertyPath<String> developGridMeta()
        {
            if(_developGridMeta == null )
                _developGridMeta = new PropertyPath<String>(MdbViewEppDevelopGridMetaGen.P_DEVELOP_GRID_META, this);
            return _developGridMeta;
        }

        public Class getEntityClass()
        {
            return MdbViewEppDevelopGridMeta.class;
        }

        public String getEntityName()
        {
            return "mdbViewEppDevelopGridMeta";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
