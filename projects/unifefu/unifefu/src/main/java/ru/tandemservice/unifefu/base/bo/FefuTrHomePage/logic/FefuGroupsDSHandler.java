package ru.tandemservice.unifefu.base.bo.FefuTrHomePage.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unifefu.base.bo.FefuTrHomePage.FefuTrHomePageManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author amakarova
 */
public class FefuGroupsDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String COLUMN_FCA = "fca";
    public static final String COLUMN_UGS = "groups";
    public static final String COLUMN_YEAR_PART = "year_part";
    public static final String COLUMN_STATE = "state";
    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(TrJournalGroup.class, "e");
    }

    public FefuGroupsDSHandler(final String ownerId) {
        super(ownerId);
    }

    public static final String PARAM_YEAR_PART = "yearPart";

    @Override
    @SuppressWarnings("unchecked")
    protected DSOutput execute(final DSInput input, final ExecutionContext context) {
        final Session session = context.getSession();

        DQLSelectBuilder jDQL = registry.buildDQLSelectBuilder();

        jDQL = this.applyFilters(jDQL, input, context);

        DSOutput output = new DSOutput(input);
        if (input.getEntityOrder().getKeyString().equals("number"))
        {
            jDQL.column(property("e.id"));
            jDQL.column(property(TrJournalGroup.journal().number().fromAlias("e")));
            Comparator<Object[]> comparator = output.getEntityOrder().getDirection() == OrderDirection.asc ? UniBaseDao.ID_VALUE_PAIR__NUMBER_AS_STRING__COMPARATOR_DIRECT : UniBaseDao.ID_VALUE_PAIR__NUMBER_AS_STRING__COMPARATOR_INDIRECT;

            List<Object[]> rows = jDQL.createStatement(session).shallowList();
            Collections.sort(rows, comparator);

            final List<Long> ids = UniBaseUtils.getColumn(rows, 0);

            final int count = ids.size();
            output.setTotalSize(count);

            final List<Long> subIds = ids.subList(output.getStartRecord(), Math.min(output.getStartRecord() + output.getCountRecord(), count));
            output.getRecordList().addAll(CommonDAO.sort(IUniBaseDao.instance.get().getList(TrJournalGroup.class, "id", subIds), subIds));
        }
        else
        {
            jDQL.column("e");
            registry.applyOrder(jDQL, input.getEntityOrder());
            output = DQLSelectOutputBuilder.get(input, jDQL, session).build();
        }

        final List<DataWrapper> wrappers = DataWrapper.wrap(output, "id", "title");
        for (final DataWrapper wrapper: wrappers) {
            wrapper.put(FefuGroupsDSHandler.COLUMN_FCA, StringUtils.EMPTY);
        }

        BatchUtils.execute(wrappers, 256, new BatchUtils.Action<DataWrapper>() {
            @Override
            public void execute(final Collection<DataWrapper> wrappers) {
                // fca
                {
                    for (final DataWrapper wrapper : wrappers) {
                        TrJournalGroup jrg = DataAccessServices.dao().get(TrJournalGroup.class, wrapper.getId());
                        EppFControlActionGroup fControlActionGroup = TrBrsCoefficientManager.instance().coefDao().getFcaGroup(jrg.getJournal());
                        wrapper.put(FefuGroupsDSHandler.COLUMN_FCA, null == fControlActionGroup ? StringUtils.EMPTY : fControlActionGroup.getTitle());
                    }
                }
            }
        });
        return output;
    }


    protected DQLSelectBuilder applyFilters(final DQLSelectBuilder dql, final DSInput input, final ExecutionContext context) {

        final Person personKey = context.get(FefuTrHomePageManager.PERSON_KEY);
        if (personKey != null) {
            dql.joinEntity("e", DQLJoinType.inner, EppPpsCollectionItem.class, "pps", eq(property(EppPpsCollectionItem.list().fromAlias("pps")), property(TrJournalGroup.group().fromAlias("e"))));
            dql.where(eq(property(EppPpsCollectionItem.pps().person().fromAlias("pps")), value(personKey)));
        }
        final EppYearPart yearPart = context.get(PARAM_YEAR_PART);
        if (null != yearPart)
            dql.where(eq(property(TrJournalGroup.journal().yearPart().fromAlias("e")), value(yearPart)));

        return dql;
    }
}
