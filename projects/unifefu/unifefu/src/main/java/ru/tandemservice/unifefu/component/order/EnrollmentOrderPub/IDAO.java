/* $Id$ */
package ru.tandemservice.unifefu.component.order.EnrollmentOrderPub;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;

/**
 * @author Nikolay Fedorovskih
 * @since 31.07.2013
 */
public interface IDAO extends IUniDao<Model>
{
    void prepare(Model model, EnrollmentOrder order);
}