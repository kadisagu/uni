package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параметры ГОС (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEppStateEduStandardParametersGen extends EntityBase
 implements INaturalIdentifiable<FefuEppStateEduStandardParametersGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters";
    public static final String ENTITY_NAME = "fefuEppStateEduStandardParameters";
    public static final int VERSION_HASH = -339089650;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_STANDARD = "eduStandard";
    public static final String L_DEVELOP_GRID_FOR_FULL_TIME_FORM = "developGridForFullTimeForm";
    public static final String P_LABOR_FULL_TIME = "laborFullTime";
    public static final String P_LABOR_FULL_TIME_ONE_YEAR = "laborFullTimeOneYear";
    public static final String P_LABOR_PART_TIME_AND_EXTRAMURAL_ONE_YEAR = "laborPartTimeAndExtramuralOneYear";
    public static final String P_MAX_HOURS_LECTURE_TYPE_PERCENT = "maxHoursLectureTypePercent";
    public static final String P_MIN_UNITS_CHOICE_DISCIPLINES = "minUnitsChoiceDisciplines";
    public static final String L_BLOCK_CHECK_NUMBER_LECTURES = "blockCheckNumberLectures";
    public static final String L_BLOCK_CHECK_Z_E_T_VARIABLE_PART = "blockCheckZETVariablePart";
    public static final String P_THEORY_WEEK = "theoryWeek";
    public static final String P_EXAM_SESSION_WEEK = "examSessionWeek";
    public static final String P_TEACH_PRACTICE_WEEK = "teachPracticeWeek";
    public static final String P_WORK_PRACTICE_WEEK = "workPracticeWeek";
    public static final String P_RESULT_ATTESTATION_WEEK = "resultAttestationWeek";
    public static final String P_HOLIDAYS_WEEK = "holidaysWeek";
    public static final String P_HOLIDAYS_FIRST_PART_YEAR_FULL_TIME_FORM = "holidaysFirstPartYearFullTimeForm";
    public static final String P_MAX_AMOUNT_AVG_LOAD_THEORY = "maxAmountAvgLoadTheory";
    public static final String P_MAX_AMOUNT_AVG_LOAD_EXAM_SESSION = "maxAmountAvgLoadExamSession";
    public static final String P_MAX_AMOUNT_AVG_AUDIT_LOAD_THEORY = "maxAmountAvgAuditLoadTheory";
    public static final String P_HOURS_FOR_ONE_EXAM_FULL_TIME_FORM = "hoursForOneExamFullTimeForm";
    public static final String P_MIN_LABOR_ONE_DISCIPLINE = "minLaborOneDiscipline";
    public static final String P_MAX_LECTURES_PERCENT = "maxLecturesPercent";
    public static final String P_MAX_INTERACTIVE_LESSONS_PERCENT = "maxInteractiveLessonsPercent";
    public static final String P_MIN_CHOICE_DISCIPLINES_PERCENT = "minChoiceDisciplinesPercent";

    private EppStateEduStandard _eduStandard;     // Государственный образовательный стандарт
    private DevelopGrid _developGridForFullTimeForm;     // Нормативный срок освоения образовательной программы для очной формы
    private int _laborFullTime;     // Общая трудоемкость обр. программы (очная форма, в ЗЕТ)
    private int _laborFullTimeOneYear;     // Общая трудоемкость обр. программы (очная формы, за один уч. год, в ЗЕТ)
    private int _laborPartTimeAndExtramuralOneYear;     // Общая трудоемкость обр. программы (очно-заочная и заочн. формы, за один уч. год, в ЗЕТ)
    private int _maxHoursLectureTypePercent;     // Макс. кол-во часов, отводимых на занятия лекц. типа в целом по блокам, %
    private int _minUnitsChoiceDisciplines;     // Мин. трудоемкость дисц. по выбору, в ЗЕТ
    private EppPlanStructure _blockCheckNumberLectures;     // Блок для проверки количества лекционных занятий
    private EppPlanStructure _blockCheckZETVariablePart;     // Блок для проверки количества ЗЕТ вариативной части
    private Double _theoryWeek;     // Теоретическое обучение, в неделях
    private Double _examSessionWeek;     // Экзаменационные сессии, в неделях
    private Double _teachPracticeWeek;     // Практика учебная, в неделях
    private Double _workPracticeWeek;     // Практика производственная, в неделях
    private Double _resultAttestationWeek;     // Итоговая аттестация, в неделях
    private Double _holidaysWeek;     // Каникулы в учебный год, в неделях
    private Double _holidaysFirstPartYearFullTimeForm;     // Каникулы в первую часть учебного года для очной формы, в неделях
    private Integer _maxAmountAvgLoadTheory;     // Макс. объем ср. нагр. в нед. (аудит. и самост. работы) в период теор. обучения, в часах
    private Integer _maxAmountAvgLoadExamSession;     // Макс. объем ср. нагр. в нед. (аудит. и самост. работы) в период экз. сессии, в часах
    private Integer _maxAmountAvgAuditLoadTheory;     // Макс. объем ср. аудит. нагр. в нед. в период теор. обучения, в часах
    private Integer _hoursForOneExamFullTimeForm;     // Часы, отводимые на подготовку к одному экзамену для очной формы
    private Integer _minLaborOneDiscipline;     // Минимальная трудоемкость одной дисциплины, в ЗЕТ
    private Integer _maxLecturesPercent;     // Макс. доля лекционных занятий, %
    private Integer _maxInteractiveLessonsPercent;     // Макс. доля занятий в интерактивной форме, %
    private Integer _minChoiceDisciplinesPercent;     // Мин. доля дисциплин по выбору во всех вариативных частях, %

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppStateEduStandard getEduStandard()
    {
        return _eduStandard;
    }

    /**
     * @param eduStandard Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     */
    public void setEduStandard(EppStateEduStandard eduStandard)
    {
        dirty(_eduStandard, eduStandard);
        _eduStandard = eduStandard;
    }

    /**
     * @return Нормативный срок освоения образовательной программы для очной формы. Свойство не может быть null.
     */
    @NotNull
    public DevelopGrid getDevelopGridForFullTimeForm()
    {
        return _developGridForFullTimeForm;
    }

    /**
     * @param developGridForFullTimeForm Нормативный срок освоения образовательной программы для очной формы. Свойство не может быть null.
     */
    public void setDevelopGridForFullTimeForm(DevelopGrid developGridForFullTimeForm)
    {
        dirty(_developGridForFullTimeForm, developGridForFullTimeForm);
        _developGridForFullTimeForm = developGridForFullTimeForm;
    }

    /**
     * @return Общая трудоемкость обр. программы (очная форма, в ЗЕТ). Свойство не может быть null.
     */
    @NotNull
    public int getLaborFullTime()
    {
        return _laborFullTime;
    }

    /**
     * @param laborFullTime Общая трудоемкость обр. программы (очная форма, в ЗЕТ). Свойство не может быть null.
     */
    public void setLaborFullTime(int laborFullTime)
    {
        dirty(_laborFullTime, laborFullTime);
        _laborFullTime = laborFullTime;
    }

    /**
     * @return Общая трудоемкость обр. программы (очная формы, за один уч. год, в ЗЕТ). Свойство не может быть null.
     */
    @NotNull
    public int getLaborFullTimeOneYear()
    {
        return _laborFullTimeOneYear;
    }

    /**
     * @param laborFullTimeOneYear Общая трудоемкость обр. программы (очная формы, за один уч. год, в ЗЕТ). Свойство не может быть null.
     */
    public void setLaborFullTimeOneYear(int laborFullTimeOneYear)
    {
        dirty(_laborFullTimeOneYear, laborFullTimeOneYear);
        _laborFullTimeOneYear = laborFullTimeOneYear;
    }

    /**
     * @return Общая трудоемкость обр. программы (очно-заочная и заочн. формы, за один уч. год, в ЗЕТ). Свойство не может быть null.
     */
    @NotNull
    public int getLaborPartTimeAndExtramuralOneYear()
    {
        return _laborPartTimeAndExtramuralOneYear;
    }

    /**
     * @param laborPartTimeAndExtramuralOneYear Общая трудоемкость обр. программы (очно-заочная и заочн. формы, за один уч. год, в ЗЕТ). Свойство не может быть null.
     */
    public void setLaborPartTimeAndExtramuralOneYear(int laborPartTimeAndExtramuralOneYear)
    {
        dirty(_laborPartTimeAndExtramuralOneYear, laborPartTimeAndExtramuralOneYear);
        _laborPartTimeAndExtramuralOneYear = laborPartTimeAndExtramuralOneYear;
    }

    /**
     * @return Макс. кол-во часов, отводимых на занятия лекц. типа в целом по блокам, %. Свойство не может быть null.
     */
    @NotNull
    public int getMaxHoursLectureTypePercent()
    {
        return _maxHoursLectureTypePercent;
    }

    /**
     * @param maxHoursLectureTypePercent Макс. кол-во часов, отводимых на занятия лекц. типа в целом по блокам, %. Свойство не может быть null.
     */
    public void setMaxHoursLectureTypePercent(int maxHoursLectureTypePercent)
    {
        dirty(_maxHoursLectureTypePercent, maxHoursLectureTypePercent);
        _maxHoursLectureTypePercent = maxHoursLectureTypePercent;
    }

    /**
     * @return Мин. трудоемкость дисц. по выбору, в ЗЕТ. Свойство не может быть null.
     */
    @NotNull
    public int getMinUnitsChoiceDisciplines()
    {
        return _minUnitsChoiceDisciplines;
    }

    /**
     * @param minUnitsChoiceDisciplines Мин. трудоемкость дисц. по выбору, в ЗЕТ. Свойство не может быть null.
     */
    public void setMinUnitsChoiceDisciplines(int minUnitsChoiceDisciplines)
    {
        dirty(_minUnitsChoiceDisciplines, minUnitsChoiceDisciplines);
        _minUnitsChoiceDisciplines = minUnitsChoiceDisciplines;
    }

    /**
     * @return Блок для проверки количества лекционных занятий. Свойство не может быть null.
     */
    @NotNull
    public EppPlanStructure getBlockCheckNumberLectures()
    {
        return _blockCheckNumberLectures;
    }

    /**
     * @param blockCheckNumberLectures Блок для проверки количества лекционных занятий. Свойство не может быть null.
     */
    public void setBlockCheckNumberLectures(EppPlanStructure blockCheckNumberLectures)
    {
        dirty(_blockCheckNumberLectures, blockCheckNumberLectures);
        _blockCheckNumberLectures = blockCheckNumberLectures;
    }

    /**
     * @return Блок для проверки количества ЗЕТ вариативной части. Свойство не может быть null.
     */
    @NotNull
    public EppPlanStructure getBlockCheckZETVariablePart()
    {
        return _blockCheckZETVariablePart;
    }

    /**
     * @param blockCheckZETVariablePart Блок для проверки количества ЗЕТ вариативной части. Свойство не может быть null.
     */
    public void setBlockCheckZETVariablePart(EppPlanStructure blockCheckZETVariablePart)
    {
        dirty(_blockCheckZETVariablePart, blockCheckZETVariablePart);
        _blockCheckZETVariablePart = blockCheckZETVariablePart;
    }

    /**
     * @return Теоретическое обучение, в неделях.
     */
    public Double getTheoryWeek()
    {
        return _theoryWeek;
    }

    /**
     * @param theoryWeek Теоретическое обучение, в неделях.
     */
    public void setTheoryWeek(Double theoryWeek)
    {
        dirty(_theoryWeek, theoryWeek);
        _theoryWeek = theoryWeek;
    }

    /**
     * @return Экзаменационные сессии, в неделях.
     */
    public Double getExamSessionWeek()
    {
        return _examSessionWeek;
    }

    /**
     * @param examSessionWeek Экзаменационные сессии, в неделях.
     */
    public void setExamSessionWeek(Double examSessionWeek)
    {
        dirty(_examSessionWeek, examSessionWeek);
        _examSessionWeek = examSessionWeek;
    }

    /**
     * @return Практика учебная, в неделях.
     */
    public Double getTeachPracticeWeek()
    {
        return _teachPracticeWeek;
    }

    /**
     * @param teachPracticeWeek Практика учебная, в неделях.
     */
    public void setTeachPracticeWeek(Double teachPracticeWeek)
    {
        dirty(_teachPracticeWeek, teachPracticeWeek);
        _teachPracticeWeek = teachPracticeWeek;
    }

    /**
     * @return Практика производственная, в неделях.
     */
    public Double getWorkPracticeWeek()
    {
        return _workPracticeWeek;
    }

    /**
     * @param workPracticeWeek Практика производственная, в неделях.
     */
    public void setWorkPracticeWeek(Double workPracticeWeek)
    {
        dirty(_workPracticeWeek, workPracticeWeek);
        _workPracticeWeek = workPracticeWeek;
    }

    /**
     * @return Итоговая аттестация, в неделях.
     */
    public Double getResultAttestationWeek()
    {
        return _resultAttestationWeek;
    }

    /**
     * @param resultAttestationWeek Итоговая аттестация, в неделях.
     */
    public void setResultAttestationWeek(Double resultAttestationWeek)
    {
        dirty(_resultAttestationWeek, resultAttestationWeek);
        _resultAttestationWeek = resultAttestationWeek;
    }

    /**
     * @return Каникулы в учебный год, в неделях.
     */
    public Double getHolidaysWeek()
    {
        return _holidaysWeek;
    }

    /**
     * @param holidaysWeek Каникулы в учебный год, в неделях.
     */
    public void setHolidaysWeek(Double holidaysWeek)
    {
        dirty(_holidaysWeek, holidaysWeek);
        _holidaysWeek = holidaysWeek;
    }

    /**
     * @return Каникулы в первую часть учебного года для очной формы, в неделях.
     */
    public Double getHolidaysFirstPartYearFullTimeForm()
    {
        return _holidaysFirstPartYearFullTimeForm;
    }

    /**
     * @param holidaysFirstPartYearFullTimeForm Каникулы в первую часть учебного года для очной формы, в неделях.
     */
    public void setHolidaysFirstPartYearFullTimeForm(Double holidaysFirstPartYearFullTimeForm)
    {
        dirty(_holidaysFirstPartYearFullTimeForm, holidaysFirstPartYearFullTimeForm);
        _holidaysFirstPartYearFullTimeForm = holidaysFirstPartYearFullTimeForm;
    }

    /**
     * @return Макс. объем ср. нагр. в нед. (аудит. и самост. работы) в период теор. обучения, в часах.
     */
    public Integer getMaxAmountAvgLoadTheory()
    {
        return _maxAmountAvgLoadTheory;
    }

    /**
     * @param maxAmountAvgLoadTheory Макс. объем ср. нагр. в нед. (аудит. и самост. работы) в период теор. обучения, в часах.
     */
    public void setMaxAmountAvgLoadTheory(Integer maxAmountAvgLoadTheory)
    {
        dirty(_maxAmountAvgLoadTheory, maxAmountAvgLoadTheory);
        _maxAmountAvgLoadTheory = maxAmountAvgLoadTheory;
    }

    /**
     * @return Макс. объем ср. нагр. в нед. (аудит. и самост. работы) в период экз. сессии, в часах.
     */
    public Integer getMaxAmountAvgLoadExamSession()
    {
        return _maxAmountAvgLoadExamSession;
    }

    /**
     * @param maxAmountAvgLoadExamSession Макс. объем ср. нагр. в нед. (аудит. и самост. работы) в период экз. сессии, в часах.
     */
    public void setMaxAmountAvgLoadExamSession(Integer maxAmountAvgLoadExamSession)
    {
        dirty(_maxAmountAvgLoadExamSession, maxAmountAvgLoadExamSession);
        _maxAmountAvgLoadExamSession = maxAmountAvgLoadExamSession;
    }

    /**
     * @return Макс. объем ср. аудит. нагр. в нед. в период теор. обучения, в часах.
     */
    public Integer getMaxAmountAvgAuditLoadTheory()
    {
        return _maxAmountAvgAuditLoadTheory;
    }

    /**
     * @param maxAmountAvgAuditLoadTheory Макс. объем ср. аудит. нагр. в нед. в период теор. обучения, в часах.
     */
    public void setMaxAmountAvgAuditLoadTheory(Integer maxAmountAvgAuditLoadTheory)
    {
        dirty(_maxAmountAvgAuditLoadTheory, maxAmountAvgAuditLoadTheory);
        _maxAmountAvgAuditLoadTheory = maxAmountAvgAuditLoadTheory;
    }

    /**
     * @return Часы, отводимые на подготовку к одному экзамену для очной формы.
     */
    public Integer getHoursForOneExamFullTimeForm()
    {
        return _hoursForOneExamFullTimeForm;
    }

    /**
     * @param hoursForOneExamFullTimeForm Часы, отводимые на подготовку к одному экзамену для очной формы.
     */
    public void setHoursForOneExamFullTimeForm(Integer hoursForOneExamFullTimeForm)
    {
        dirty(_hoursForOneExamFullTimeForm, hoursForOneExamFullTimeForm);
        _hoursForOneExamFullTimeForm = hoursForOneExamFullTimeForm;
    }

    /**
     * @return Минимальная трудоемкость одной дисциплины, в ЗЕТ.
     */
    public Integer getMinLaborOneDiscipline()
    {
        return _minLaborOneDiscipline;
    }

    /**
     * @param minLaborOneDiscipline Минимальная трудоемкость одной дисциплины, в ЗЕТ.
     */
    public void setMinLaborOneDiscipline(Integer minLaborOneDiscipline)
    {
        dirty(_minLaborOneDiscipline, minLaborOneDiscipline);
        _minLaborOneDiscipline = minLaborOneDiscipline;
    }

    /**
     * @return Макс. доля лекционных занятий, %.
     */
    public Integer getMaxLecturesPercent()
    {
        return _maxLecturesPercent;
    }

    /**
     * @param maxLecturesPercent Макс. доля лекционных занятий, %.
     */
    public void setMaxLecturesPercent(Integer maxLecturesPercent)
    {
        dirty(_maxLecturesPercent, maxLecturesPercent);
        _maxLecturesPercent = maxLecturesPercent;
    }

    /**
     * @return Макс. доля занятий в интерактивной форме, %.
     */
    public Integer getMaxInteractiveLessonsPercent()
    {
        return _maxInteractiveLessonsPercent;
    }

    /**
     * @param maxInteractiveLessonsPercent Макс. доля занятий в интерактивной форме, %.
     */
    public void setMaxInteractiveLessonsPercent(Integer maxInteractiveLessonsPercent)
    {
        dirty(_maxInteractiveLessonsPercent, maxInteractiveLessonsPercent);
        _maxInteractiveLessonsPercent = maxInteractiveLessonsPercent;
    }

    /**
     * @return Мин. доля дисциплин по выбору во всех вариативных частях, %.
     */
    public Integer getMinChoiceDisciplinesPercent()
    {
        return _minChoiceDisciplinesPercent;
    }

    /**
     * @param minChoiceDisciplinesPercent Мин. доля дисциплин по выбору во всех вариативных частях, %.
     */
    public void setMinChoiceDisciplinesPercent(Integer minChoiceDisciplinesPercent)
    {
        dirty(_minChoiceDisciplinesPercent, minChoiceDisciplinesPercent);
        _minChoiceDisciplinesPercent = minChoiceDisciplinesPercent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEppStateEduStandardParametersGen)
        {
            if (withNaturalIdProperties)
            {
                setEduStandard(((FefuEppStateEduStandardParameters)another).getEduStandard());
            }
            setDevelopGridForFullTimeForm(((FefuEppStateEduStandardParameters)another).getDevelopGridForFullTimeForm());
            setLaborFullTime(((FefuEppStateEduStandardParameters)another).getLaborFullTime());
            setLaborFullTimeOneYear(((FefuEppStateEduStandardParameters)another).getLaborFullTimeOneYear());
            setLaborPartTimeAndExtramuralOneYear(((FefuEppStateEduStandardParameters)another).getLaborPartTimeAndExtramuralOneYear());
            setMaxHoursLectureTypePercent(((FefuEppStateEduStandardParameters)another).getMaxHoursLectureTypePercent());
            setMinUnitsChoiceDisciplines(((FefuEppStateEduStandardParameters)another).getMinUnitsChoiceDisciplines());
            setBlockCheckNumberLectures(((FefuEppStateEduStandardParameters)another).getBlockCheckNumberLectures());
            setBlockCheckZETVariablePart(((FefuEppStateEduStandardParameters)another).getBlockCheckZETVariablePart());
            setTheoryWeek(((FefuEppStateEduStandardParameters)another).getTheoryWeek());
            setExamSessionWeek(((FefuEppStateEduStandardParameters)another).getExamSessionWeek());
            setTeachPracticeWeek(((FefuEppStateEduStandardParameters)another).getTeachPracticeWeek());
            setWorkPracticeWeek(((FefuEppStateEduStandardParameters)another).getWorkPracticeWeek());
            setResultAttestationWeek(((FefuEppStateEduStandardParameters)another).getResultAttestationWeek());
            setHolidaysWeek(((FefuEppStateEduStandardParameters)another).getHolidaysWeek());
            setHolidaysFirstPartYearFullTimeForm(((FefuEppStateEduStandardParameters)another).getHolidaysFirstPartYearFullTimeForm());
            setMaxAmountAvgLoadTheory(((FefuEppStateEduStandardParameters)another).getMaxAmountAvgLoadTheory());
            setMaxAmountAvgLoadExamSession(((FefuEppStateEduStandardParameters)another).getMaxAmountAvgLoadExamSession());
            setMaxAmountAvgAuditLoadTheory(((FefuEppStateEduStandardParameters)another).getMaxAmountAvgAuditLoadTheory());
            setHoursForOneExamFullTimeForm(((FefuEppStateEduStandardParameters)another).getHoursForOneExamFullTimeForm());
            setMinLaborOneDiscipline(((FefuEppStateEduStandardParameters)another).getMinLaborOneDiscipline());
            setMaxLecturesPercent(((FefuEppStateEduStandardParameters)another).getMaxLecturesPercent());
            setMaxInteractiveLessonsPercent(((FefuEppStateEduStandardParameters)another).getMaxInteractiveLessonsPercent());
            setMinChoiceDisciplinesPercent(((FefuEppStateEduStandardParameters)another).getMinChoiceDisciplinesPercent());
        }
    }

    public INaturalId<FefuEppStateEduStandardParametersGen> getNaturalId()
    {
        return new NaturalId(getEduStandard());
    }

    public static class NaturalId extends NaturalIdBase<FefuEppStateEduStandardParametersGen>
    {
        private static final String PROXY_NAME = "FefuEppStateEduStandardParametersNaturalProxy";

        private Long _eduStandard;

        public NaturalId()
        {}

        public NaturalId(EppStateEduStandard eduStandard)
        {
            _eduStandard = ((IEntity) eduStandard).getId();
        }

        public Long getEduStandard()
        {
            return _eduStandard;
        }

        public void setEduStandard(Long eduStandard)
        {
            _eduStandard = eduStandard;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuEppStateEduStandardParametersGen.NaturalId) ) return false;

            FefuEppStateEduStandardParametersGen.NaturalId that = (NaturalId) o;

            if( !equals(getEduStandard(), that.getEduStandard()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEduStandard());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEduStandard());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEppStateEduStandardParametersGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEppStateEduStandardParameters.class;
        }

        public T newInstance()
        {
            return (T) new FefuEppStateEduStandardParameters();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduStandard":
                    return obj.getEduStandard();
                case "developGridForFullTimeForm":
                    return obj.getDevelopGridForFullTimeForm();
                case "laborFullTime":
                    return obj.getLaborFullTime();
                case "laborFullTimeOneYear":
                    return obj.getLaborFullTimeOneYear();
                case "laborPartTimeAndExtramuralOneYear":
                    return obj.getLaborPartTimeAndExtramuralOneYear();
                case "maxHoursLectureTypePercent":
                    return obj.getMaxHoursLectureTypePercent();
                case "minUnitsChoiceDisciplines":
                    return obj.getMinUnitsChoiceDisciplines();
                case "blockCheckNumberLectures":
                    return obj.getBlockCheckNumberLectures();
                case "blockCheckZETVariablePart":
                    return obj.getBlockCheckZETVariablePart();
                case "theoryWeek":
                    return obj.getTheoryWeek();
                case "examSessionWeek":
                    return obj.getExamSessionWeek();
                case "teachPracticeWeek":
                    return obj.getTeachPracticeWeek();
                case "workPracticeWeek":
                    return obj.getWorkPracticeWeek();
                case "resultAttestationWeek":
                    return obj.getResultAttestationWeek();
                case "holidaysWeek":
                    return obj.getHolidaysWeek();
                case "holidaysFirstPartYearFullTimeForm":
                    return obj.getHolidaysFirstPartYearFullTimeForm();
                case "maxAmountAvgLoadTheory":
                    return obj.getMaxAmountAvgLoadTheory();
                case "maxAmountAvgLoadExamSession":
                    return obj.getMaxAmountAvgLoadExamSession();
                case "maxAmountAvgAuditLoadTheory":
                    return obj.getMaxAmountAvgAuditLoadTheory();
                case "hoursForOneExamFullTimeForm":
                    return obj.getHoursForOneExamFullTimeForm();
                case "minLaborOneDiscipline":
                    return obj.getMinLaborOneDiscipline();
                case "maxLecturesPercent":
                    return obj.getMaxLecturesPercent();
                case "maxInteractiveLessonsPercent":
                    return obj.getMaxInteractiveLessonsPercent();
                case "minChoiceDisciplinesPercent":
                    return obj.getMinChoiceDisciplinesPercent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduStandard":
                    obj.setEduStandard((EppStateEduStandard) value);
                    return;
                case "developGridForFullTimeForm":
                    obj.setDevelopGridForFullTimeForm((DevelopGrid) value);
                    return;
                case "laborFullTime":
                    obj.setLaborFullTime((Integer) value);
                    return;
                case "laborFullTimeOneYear":
                    obj.setLaborFullTimeOneYear((Integer) value);
                    return;
                case "laborPartTimeAndExtramuralOneYear":
                    obj.setLaborPartTimeAndExtramuralOneYear((Integer) value);
                    return;
                case "maxHoursLectureTypePercent":
                    obj.setMaxHoursLectureTypePercent((Integer) value);
                    return;
                case "minUnitsChoiceDisciplines":
                    obj.setMinUnitsChoiceDisciplines((Integer) value);
                    return;
                case "blockCheckNumberLectures":
                    obj.setBlockCheckNumberLectures((EppPlanStructure) value);
                    return;
                case "blockCheckZETVariablePart":
                    obj.setBlockCheckZETVariablePart((EppPlanStructure) value);
                    return;
                case "theoryWeek":
                    obj.setTheoryWeek((Double) value);
                    return;
                case "examSessionWeek":
                    obj.setExamSessionWeek((Double) value);
                    return;
                case "teachPracticeWeek":
                    obj.setTeachPracticeWeek((Double) value);
                    return;
                case "workPracticeWeek":
                    obj.setWorkPracticeWeek((Double) value);
                    return;
                case "resultAttestationWeek":
                    obj.setResultAttestationWeek((Double) value);
                    return;
                case "holidaysWeek":
                    obj.setHolidaysWeek((Double) value);
                    return;
                case "holidaysFirstPartYearFullTimeForm":
                    obj.setHolidaysFirstPartYearFullTimeForm((Double) value);
                    return;
                case "maxAmountAvgLoadTheory":
                    obj.setMaxAmountAvgLoadTheory((Integer) value);
                    return;
                case "maxAmountAvgLoadExamSession":
                    obj.setMaxAmountAvgLoadExamSession((Integer) value);
                    return;
                case "maxAmountAvgAuditLoadTheory":
                    obj.setMaxAmountAvgAuditLoadTheory((Integer) value);
                    return;
                case "hoursForOneExamFullTimeForm":
                    obj.setHoursForOneExamFullTimeForm((Integer) value);
                    return;
                case "minLaborOneDiscipline":
                    obj.setMinLaborOneDiscipline((Integer) value);
                    return;
                case "maxLecturesPercent":
                    obj.setMaxLecturesPercent((Integer) value);
                    return;
                case "maxInteractiveLessonsPercent":
                    obj.setMaxInteractiveLessonsPercent((Integer) value);
                    return;
                case "minChoiceDisciplinesPercent":
                    obj.setMinChoiceDisciplinesPercent((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduStandard":
                        return true;
                case "developGridForFullTimeForm":
                        return true;
                case "laborFullTime":
                        return true;
                case "laborFullTimeOneYear":
                        return true;
                case "laborPartTimeAndExtramuralOneYear":
                        return true;
                case "maxHoursLectureTypePercent":
                        return true;
                case "minUnitsChoiceDisciplines":
                        return true;
                case "blockCheckNumberLectures":
                        return true;
                case "blockCheckZETVariablePart":
                        return true;
                case "theoryWeek":
                        return true;
                case "examSessionWeek":
                        return true;
                case "teachPracticeWeek":
                        return true;
                case "workPracticeWeek":
                        return true;
                case "resultAttestationWeek":
                        return true;
                case "holidaysWeek":
                        return true;
                case "holidaysFirstPartYearFullTimeForm":
                        return true;
                case "maxAmountAvgLoadTheory":
                        return true;
                case "maxAmountAvgLoadExamSession":
                        return true;
                case "maxAmountAvgAuditLoadTheory":
                        return true;
                case "hoursForOneExamFullTimeForm":
                        return true;
                case "minLaborOneDiscipline":
                        return true;
                case "maxLecturesPercent":
                        return true;
                case "maxInteractiveLessonsPercent":
                        return true;
                case "minChoiceDisciplinesPercent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduStandard":
                    return true;
                case "developGridForFullTimeForm":
                    return true;
                case "laborFullTime":
                    return true;
                case "laborFullTimeOneYear":
                    return true;
                case "laborPartTimeAndExtramuralOneYear":
                    return true;
                case "maxHoursLectureTypePercent":
                    return true;
                case "minUnitsChoiceDisciplines":
                    return true;
                case "blockCheckNumberLectures":
                    return true;
                case "blockCheckZETVariablePart":
                    return true;
                case "theoryWeek":
                    return true;
                case "examSessionWeek":
                    return true;
                case "teachPracticeWeek":
                    return true;
                case "workPracticeWeek":
                    return true;
                case "resultAttestationWeek":
                    return true;
                case "holidaysWeek":
                    return true;
                case "holidaysFirstPartYearFullTimeForm":
                    return true;
                case "maxAmountAvgLoadTheory":
                    return true;
                case "maxAmountAvgLoadExamSession":
                    return true;
                case "maxAmountAvgAuditLoadTheory":
                    return true;
                case "hoursForOneExamFullTimeForm":
                    return true;
                case "minLaborOneDiscipline":
                    return true;
                case "maxLecturesPercent":
                    return true;
                case "maxInteractiveLessonsPercent":
                    return true;
                case "minChoiceDisciplinesPercent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduStandard":
                    return EppStateEduStandard.class;
                case "developGridForFullTimeForm":
                    return DevelopGrid.class;
                case "laborFullTime":
                    return Integer.class;
                case "laborFullTimeOneYear":
                    return Integer.class;
                case "laborPartTimeAndExtramuralOneYear":
                    return Integer.class;
                case "maxHoursLectureTypePercent":
                    return Integer.class;
                case "minUnitsChoiceDisciplines":
                    return Integer.class;
                case "blockCheckNumberLectures":
                    return EppPlanStructure.class;
                case "blockCheckZETVariablePart":
                    return EppPlanStructure.class;
                case "theoryWeek":
                    return Double.class;
                case "examSessionWeek":
                    return Double.class;
                case "teachPracticeWeek":
                    return Double.class;
                case "workPracticeWeek":
                    return Double.class;
                case "resultAttestationWeek":
                    return Double.class;
                case "holidaysWeek":
                    return Double.class;
                case "holidaysFirstPartYearFullTimeForm":
                    return Double.class;
                case "maxAmountAvgLoadTheory":
                    return Integer.class;
                case "maxAmountAvgLoadExamSession":
                    return Integer.class;
                case "maxAmountAvgAuditLoadTheory":
                    return Integer.class;
                case "hoursForOneExamFullTimeForm":
                    return Integer.class;
                case "minLaborOneDiscipline":
                    return Integer.class;
                case "maxLecturesPercent":
                    return Integer.class;
                case "maxInteractiveLessonsPercent":
                    return Integer.class;
                case "minChoiceDisciplinesPercent":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEppStateEduStandardParameters> _dslPath = new Path<FefuEppStateEduStandardParameters>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEppStateEduStandardParameters");
    }
            

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getEduStandard()
     */
    public static EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
    {
        return _dslPath.eduStandard();
    }

    /**
     * @return Нормативный срок освоения образовательной программы для очной формы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getDevelopGridForFullTimeForm()
     */
    public static DevelopGrid.Path<DevelopGrid> developGridForFullTimeForm()
    {
        return _dslPath.developGridForFullTimeForm();
    }

    /**
     * @return Общая трудоемкость обр. программы (очная форма, в ЗЕТ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getLaborFullTime()
     */
    public static PropertyPath<Integer> laborFullTime()
    {
        return _dslPath.laborFullTime();
    }

    /**
     * @return Общая трудоемкость обр. программы (очная формы, за один уч. год, в ЗЕТ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getLaborFullTimeOneYear()
     */
    public static PropertyPath<Integer> laborFullTimeOneYear()
    {
        return _dslPath.laborFullTimeOneYear();
    }

    /**
     * @return Общая трудоемкость обр. программы (очно-заочная и заочн. формы, за один уч. год, в ЗЕТ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getLaborPartTimeAndExtramuralOneYear()
     */
    public static PropertyPath<Integer> laborPartTimeAndExtramuralOneYear()
    {
        return _dslPath.laborPartTimeAndExtramuralOneYear();
    }

    /**
     * @return Макс. кол-во часов, отводимых на занятия лекц. типа в целом по блокам, %. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxHoursLectureTypePercent()
     */
    public static PropertyPath<Integer> maxHoursLectureTypePercent()
    {
        return _dslPath.maxHoursLectureTypePercent();
    }

    /**
     * @return Мин. трудоемкость дисц. по выбору, в ЗЕТ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMinUnitsChoiceDisciplines()
     */
    public static PropertyPath<Integer> minUnitsChoiceDisciplines()
    {
        return _dslPath.minUnitsChoiceDisciplines();
    }

    /**
     * @return Блок для проверки количества лекционных занятий. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getBlockCheckNumberLectures()
     */
    public static EppPlanStructure.Path<EppPlanStructure> blockCheckNumberLectures()
    {
        return _dslPath.blockCheckNumberLectures();
    }

    /**
     * @return Блок для проверки количества ЗЕТ вариативной части. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getBlockCheckZETVariablePart()
     */
    public static EppPlanStructure.Path<EppPlanStructure> blockCheckZETVariablePart()
    {
        return _dslPath.blockCheckZETVariablePart();
    }

    /**
     * @return Теоретическое обучение, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getTheoryWeek()
     */
    public static PropertyPath<Double> theoryWeek()
    {
        return _dslPath.theoryWeek();
    }

    /**
     * @return Экзаменационные сессии, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getExamSessionWeek()
     */
    public static PropertyPath<Double> examSessionWeek()
    {
        return _dslPath.examSessionWeek();
    }

    /**
     * @return Практика учебная, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getTeachPracticeWeek()
     */
    public static PropertyPath<Double> teachPracticeWeek()
    {
        return _dslPath.teachPracticeWeek();
    }

    /**
     * @return Практика производственная, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getWorkPracticeWeek()
     */
    public static PropertyPath<Double> workPracticeWeek()
    {
        return _dslPath.workPracticeWeek();
    }

    /**
     * @return Итоговая аттестация, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getResultAttestationWeek()
     */
    public static PropertyPath<Double> resultAttestationWeek()
    {
        return _dslPath.resultAttestationWeek();
    }

    /**
     * @return Каникулы в учебный год, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getHolidaysWeek()
     */
    public static PropertyPath<Double> holidaysWeek()
    {
        return _dslPath.holidaysWeek();
    }

    /**
     * @return Каникулы в первую часть учебного года для очной формы, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getHolidaysFirstPartYearFullTimeForm()
     */
    public static PropertyPath<Double> holidaysFirstPartYearFullTimeForm()
    {
        return _dslPath.holidaysFirstPartYearFullTimeForm();
    }

    /**
     * @return Макс. объем ср. нагр. в нед. (аудит. и самост. работы) в период теор. обучения, в часах.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxAmountAvgLoadTheory()
     */
    public static PropertyPath<Integer> maxAmountAvgLoadTheory()
    {
        return _dslPath.maxAmountAvgLoadTheory();
    }

    /**
     * @return Макс. объем ср. нагр. в нед. (аудит. и самост. работы) в период экз. сессии, в часах.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxAmountAvgLoadExamSession()
     */
    public static PropertyPath<Integer> maxAmountAvgLoadExamSession()
    {
        return _dslPath.maxAmountAvgLoadExamSession();
    }

    /**
     * @return Макс. объем ср. аудит. нагр. в нед. в период теор. обучения, в часах.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxAmountAvgAuditLoadTheory()
     */
    public static PropertyPath<Integer> maxAmountAvgAuditLoadTheory()
    {
        return _dslPath.maxAmountAvgAuditLoadTheory();
    }

    /**
     * @return Часы, отводимые на подготовку к одному экзамену для очной формы.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getHoursForOneExamFullTimeForm()
     */
    public static PropertyPath<Integer> hoursForOneExamFullTimeForm()
    {
        return _dslPath.hoursForOneExamFullTimeForm();
    }

    /**
     * @return Минимальная трудоемкость одной дисциплины, в ЗЕТ.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMinLaborOneDiscipline()
     */
    public static PropertyPath<Integer> minLaborOneDiscipline()
    {
        return _dslPath.minLaborOneDiscipline();
    }

    /**
     * @return Макс. доля лекционных занятий, %.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxLecturesPercent()
     */
    public static PropertyPath<Integer> maxLecturesPercent()
    {
        return _dslPath.maxLecturesPercent();
    }

    /**
     * @return Макс. доля занятий в интерактивной форме, %.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxInteractiveLessonsPercent()
     */
    public static PropertyPath<Integer> maxInteractiveLessonsPercent()
    {
        return _dslPath.maxInteractiveLessonsPercent();
    }

    /**
     * @return Мин. доля дисциплин по выбору во всех вариативных частях, %.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMinChoiceDisciplinesPercent()
     */
    public static PropertyPath<Integer> minChoiceDisciplinesPercent()
    {
        return _dslPath.minChoiceDisciplinesPercent();
    }

    public static class Path<E extends FefuEppStateEduStandardParameters> extends EntityPath<E>
    {
        private EppStateEduStandard.Path<EppStateEduStandard> _eduStandard;
        private DevelopGrid.Path<DevelopGrid> _developGridForFullTimeForm;
        private PropertyPath<Integer> _laborFullTime;
        private PropertyPath<Integer> _laborFullTimeOneYear;
        private PropertyPath<Integer> _laborPartTimeAndExtramuralOneYear;
        private PropertyPath<Integer> _maxHoursLectureTypePercent;
        private PropertyPath<Integer> _minUnitsChoiceDisciplines;
        private EppPlanStructure.Path<EppPlanStructure> _blockCheckNumberLectures;
        private EppPlanStructure.Path<EppPlanStructure> _blockCheckZETVariablePart;
        private PropertyPath<Double> _theoryWeek;
        private PropertyPath<Double> _examSessionWeek;
        private PropertyPath<Double> _teachPracticeWeek;
        private PropertyPath<Double> _workPracticeWeek;
        private PropertyPath<Double> _resultAttestationWeek;
        private PropertyPath<Double> _holidaysWeek;
        private PropertyPath<Double> _holidaysFirstPartYearFullTimeForm;
        private PropertyPath<Integer> _maxAmountAvgLoadTheory;
        private PropertyPath<Integer> _maxAmountAvgLoadExamSession;
        private PropertyPath<Integer> _maxAmountAvgAuditLoadTheory;
        private PropertyPath<Integer> _hoursForOneExamFullTimeForm;
        private PropertyPath<Integer> _minLaborOneDiscipline;
        private PropertyPath<Integer> _maxLecturesPercent;
        private PropertyPath<Integer> _maxInteractiveLessonsPercent;
        private PropertyPath<Integer> _minChoiceDisciplinesPercent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getEduStandard()
     */
        public EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
        {
            if(_eduStandard == null )
                _eduStandard = new EppStateEduStandard.Path<EppStateEduStandard>(L_EDU_STANDARD, this);
            return _eduStandard;
        }

    /**
     * @return Нормативный срок освоения образовательной программы для очной формы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getDevelopGridForFullTimeForm()
     */
        public DevelopGrid.Path<DevelopGrid> developGridForFullTimeForm()
        {
            if(_developGridForFullTimeForm == null )
                _developGridForFullTimeForm = new DevelopGrid.Path<DevelopGrid>(L_DEVELOP_GRID_FOR_FULL_TIME_FORM, this);
            return _developGridForFullTimeForm;
        }

    /**
     * @return Общая трудоемкость обр. программы (очная форма, в ЗЕТ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getLaborFullTime()
     */
        public PropertyPath<Integer> laborFullTime()
        {
            if(_laborFullTime == null )
                _laborFullTime = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_LABOR_FULL_TIME, this);
            return _laborFullTime;
        }

    /**
     * @return Общая трудоемкость обр. программы (очная формы, за один уч. год, в ЗЕТ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getLaborFullTimeOneYear()
     */
        public PropertyPath<Integer> laborFullTimeOneYear()
        {
            if(_laborFullTimeOneYear == null )
                _laborFullTimeOneYear = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_LABOR_FULL_TIME_ONE_YEAR, this);
            return _laborFullTimeOneYear;
        }

    /**
     * @return Общая трудоемкость обр. программы (очно-заочная и заочн. формы, за один уч. год, в ЗЕТ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getLaborPartTimeAndExtramuralOneYear()
     */
        public PropertyPath<Integer> laborPartTimeAndExtramuralOneYear()
        {
            if(_laborPartTimeAndExtramuralOneYear == null )
                _laborPartTimeAndExtramuralOneYear = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_LABOR_PART_TIME_AND_EXTRAMURAL_ONE_YEAR, this);
            return _laborPartTimeAndExtramuralOneYear;
        }

    /**
     * @return Макс. кол-во часов, отводимых на занятия лекц. типа в целом по блокам, %. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxHoursLectureTypePercent()
     */
        public PropertyPath<Integer> maxHoursLectureTypePercent()
        {
            if(_maxHoursLectureTypePercent == null )
                _maxHoursLectureTypePercent = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_MAX_HOURS_LECTURE_TYPE_PERCENT, this);
            return _maxHoursLectureTypePercent;
        }

    /**
     * @return Мин. трудоемкость дисц. по выбору, в ЗЕТ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMinUnitsChoiceDisciplines()
     */
        public PropertyPath<Integer> minUnitsChoiceDisciplines()
        {
            if(_minUnitsChoiceDisciplines == null )
                _minUnitsChoiceDisciplines = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_MIN_UNITS_CHOICE_DISCIPLINES, this);
            return _minUnitsChoiceDisciplines;
        }

    /**
     * @return Блок для проверки количества лекционных занятий. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getBlockCheckNumberLectures()
     */
        public EppPlanStructure.Path<EppPlanStructure> blockCheckNumberLectures()
        {
            if(_blockCheckNumberLectures == null )
                _blockCheckNumberLectures = new EppPlanStructure.Path<EppPlanStructure>(L_BLOCK_CHECK_NUMBER_LECTURES, this);
            return _blockCheckNumberLectures;
        }

    /**
     * @return Блок для проверки количества ЗЕТ вариативной части. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getBlockCheckZETVariablePart()
     */
        public EppPlanStructure.Path<EppPlanStructure> blockCheckZETVariablePart()
        {
            if(_blockCheckZETVariablePart == null )
                _blockCheckZETVariablePart = new EppPlanStructure.Path<EppPlanStructure>(L_BLOCK_CHECK_Z_E_T_VARIABLE_PART, this);
            return _blockCheckZETVariablePart;
        }

    /**
     * @return Теоретическое обучение, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getTheoryWeek()
     */
        public PropertyPath<Double> theoryWeek()
        {
            if(_theoryWeek == null )
                _theoryWeek = new PropertyPath<Double>(FefuEppStateEduStandardParametersGen.P_THEORY_WEEK, this);
            return _theoryWeek;
        }

    /**
     * @return Экзаменационные сессии, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getExamSessionWeek()
     */
        public PropertyPath<Double> examSessionWeek()
        {
            if(_examSessionWeek == null )
                _examSessionWeek = new PropertyPath<Double>(FefuEppStateEduStandardParametersGen.P_EXAM_SESSION_WEEK, this);
            return _examSessionWeek;
        }

    /**
     * @return Практика учебная, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getTeachPracticeWeek()
     */
        public PropertyPath<Double> teachPracticeWeek()
        {
            if(_teachPracticeWeek == null )
                _teachPracticeWeek = new PropertyPath<Double>(FefuEppStateEduStandardParametersGen.P_TEACH_PRACTICE_WEEK, this);
            return _teachPracticeWeek;
        }

    /**
     * @return Практика производственная, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getWorkPracticeWeek()
     */
        public PropertyPath<Double> workPracticeWeek()
        {
            if(_workPracticeWeek == null )
                _workPracticeWeek = new PropertyPath<Double>(FefuEppStateEduStandardParametersGen.P_WORK_PRACTICE_WEEK, this);
            return _workPracticeWeek;
        }

    /**
     * @return Итоговая аттестация, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getResultAttestationWeek()
     */
        public PropertyPath<Double> resultAttestationWeek()
        {
            if(_resultAttestationWeek == null )
                _resultAttestationWeek = new PropertyPath<Double>(FefuEppStateEduStandardParametersGen.P_RESULT_ATTESTATION_WEEK, this);
            return _resultAttestationWeek;
        }

    /**
     * @return Каникулы в учебный год, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getHolidaysWeek()
     */
        public PropertyPath<Double> holidaysWeek()
        {
            if(_holidaysWeek == null )
                _holidaysWeek = new PropertyPath<Double>(FefuEppStateEduStandardParametersGen.P_HOLIDAYS_WEEK, this);
            return _holidaysWeek;
        }

    /**
     * @return Каникулы в первую часть учебного года для очной формы, в неделях.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getHolidaysFirstPartYearFullTimeForm()
     */
        public PropertyPath<Double> holidaysFirstPartYearFullTimeForm()
        {
            if(_holidaysFirstPartYearFullTimeForm == null )
                _holidaysFirstPartYearFullTimeForm = new PropertyPath<Double>(FefuEppStateEduStandardParametersGen.P_HOLIDAYS_FIRST_PART_YEAR_FULL_TIME_FORM, this);
            return _holidaysFirstPartYearFullTimeForm;
        }

    /**
     * @return Макс. объем ср. нагр. в нед. (аудит. и самост. работы) в период теор. обучения, в часах.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxAmountAvgLoadTheory()
     */
        public PropertyPath<Integer> maxAmountAvgLoadTheory()
        {
            if(_maxAmountAvgLoadTheory == null )
                _maxAmountAvgLoadTheory = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_MAX_AMOUNT_AVG_LOAD_THEORY, this);
            return _maxAmountAvgLoadTheory;
        }

    /**
     * @return Макс. объем ср. нагр. в нед. (аудит. и самост. работы) в период экз. сессии, в часах.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxAmountAvgLoadExamSession()
     */
        public PropertyPath<Integer> maxAmountAvgLoadExamSession()
        {
            if(_maxAmountAvgLoadExamSession == null )
                _maxAmountAvgLoadExamSession = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_MAX_AMOUNT_AVG_LOAD_EXAM_SESSION, this);
            return _maxAmountAvgLoadExamSession;
        }

    /**
     * @return Макс. объем ср. аудит. нагр. в нед. в период теор. обучения, в часах.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxAmountAvgAuditLoadTheory()
     */
        public PropertyPath<Integer> maxAmountAvgAuditLoadTheory()
        {
            if(_maxAmountAvgAuditLoadTheory == null )
                _maxAmountAvgAuditLoadTheory = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_MAX_AMOUNT_AVG_AUDIT_LOAD_THEORY, this);
            return _maxAmountAvgAuditLoadTheory;
        }

    /**
     * @return Часы, отводимые на подготовку к одному экзамену для очной формы.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getHoursForOneExamFullTimeForm()
     */
        public PropertyPath<Integer> hoursForOneExamFullTimeForm()
        {
            if(_hoursForOneExamFullTimeForm == null )
                _hoursForOneExamFullTimeForm = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_HOURS_FOR_ONE_EXAM_FULL_TIME_FORM, this);
            return _hoursForOneExamFullTimeForm;
        }

    /**
     * @return Минимальная трудоемкость одной дисциплины, в ЗЕТ.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMinLaborOneDiscipline()
     */
        public PropertyPath<Integer> minLaborOneDiscipline()
        {
            if(_minLaborOneDiscipline == null )
                _minLaborOneDiscipline = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_MIN_LABOR_ONE_DISCIPLINE, this);
            return _minLaborOneDiscipline;
        }

    /**
     * @return Макс. доля лекционных занятий, %.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxLecturesPercent()
     */
        public PropertyPath<Integer> maxLecturesPercent()
        {
            if(_maxLecturesPercent == null )
                _maxLecturesPercent = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_MAX_LECTURES_PERCENT, this);
            return _maxLecturesPercent;
        }

    /**
     * @return Макс. доля занятий в интерактивной форме, %.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMaxInteractiveLessonsPercent()
     */
        public PropertyPath<Integer> maxInteractiveLessonsPercent()
        {
            if(_maxInteractiveLessonsPercent == null )
                _maxInteractiveLessonsPercent = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_MAX_INTERACTIVE_LESSONS_PERCENT, this);
            return _maxInteractiveLessonsPercent;
        }

    /**
     * @return Мин. доля дисциплин по выбору во всех вариативных частях, %.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters#getMinChoiceDisciplinesPercent()
     */
        public PropertyPath<Integer> minChoiceDisciplinesPercent()
        {
            if(_minChoiceDisciplinesPercent == null )
                _minChoiceDisciplinesPercent = new PropertyPath<Integer>(FefuEppStateEduStandardParametersGen.P_MIN_CHOICE_DISCIPLINES_PERCENT, this);
            return _minChoiceDisciplinesPercent;
        }

        public Class getEntityClass()
        {
            return FefuEppStateEduStandardParameters.class;
        }

        public String getEntityName()
        {
            return "fefuEppStateEduStandardParameters";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
