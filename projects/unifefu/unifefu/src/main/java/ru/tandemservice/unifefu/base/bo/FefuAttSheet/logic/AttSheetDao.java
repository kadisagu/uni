/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuAttSheet.logic;

import com.google.common.collect.ImmutableList;
import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.AddEdit.FefuAttSheetWrapper;
import ru.tandemservice.unifefu.entity.SessionAttSheetDocument;
import ru.tandemservice.unifefu.entity.SessionAttSheetOperation;
import ru.tandemservice.unifefu.entity.SessionAttSheetSlot;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 07.07.2014
 */
public class AttSheetDao extends UniBaseDao implements IAttSheetDao
{
	@Override
	public void saveOrUpdateAttSheet(SessionAttSheetDocument document, final Date markDate, Collection<FefuAttSheetWrapper> rowList, Collection<PpsEntry> membersList)
	{
		final Session session = getSession();
		// удаляем предыдущее содержимое, чтобы не возиться со сверкой
		if (document.getId() != null)
		{
			deleteAttSheetDocument(document, false, session);
			session.flush();
		}

		saveOrUpdate(document);

		//сохраняяем комиссию и ее членов если они есть
		SessionComission commission = null;
		if (membersList != null && !membersList.isEmpty())
		{
			commission = new SessionComission();
			session.save(commission);

			for (PpsEntry entry : membersList)
			{
				SessionComissionPps commissionPps = new SessionComissionPps();
				commissionPps.setCommission(commission);
				commissionPps.setPps(entry);
				session.save(commissionPps);
			}
		}

		// сохраняем заново все мероприятия
		for (final FefuAttSheetWrapper item : rowList)
		{
			SessionMark mark = null;
			SessionDocumentSlot sessionDocumentSlot;
			if(item.getEppSlot() != null && item.getMark()!= null)
			{
				SessionComission slotCommission = new SessionComission();
				session.save(slotCommission);

				sessionDocumentSlot = new SessionDocumentSlot();
				//required
				sessionDocumentSlot.setDocument(document);
				sessionDocumentSlot.setStudentWpeCAction(item.getEppSlot());
				sessionDocumentSlot.setInSession(true);
				//not required
				sessionDocumentSlot.setCommission(slotCommission);

				save(sessionDocumentSlot);
				mark = SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark( sessionDocumentSlot , new ISessionMarkDAO.MarkData()
				{
					@Override
					public Date getPerformDate()
					{
						return markDate;
					}

					@Override
					public Double getPoints()
					{
						return item.getRating();
					}

					@Override
					public SessionMarkCatalogItem getMarkValue()
					{
						return item.getMark();
					}

					@Override
					public String getComment()
					{
						return null;
					}
				});
			}

			SessionAttSheetSlot slot = new SessionAttSheetSlot();
			slot.setDocument(document);
			slot.setStudent(item.getEppSlot());
			slot.setCommission(commission);
			session.save(slot);

			SessionAttSheetOperation operation = new SessionAttSheetOperation();
			operation.setSlot(slot);
			operation.setDiscipline(item.getSourceDiscipline());
			operation.setControlAction(item.getSourceControlAction());
			operation.setMark(item.getSourceMark());
			operation.setPerformDate(markDate);

			operation.setTargetMark((SessionSlotRegularMark) mark);
			operation.setValue(item.getMark());
			operation.setComment(item.getComment());
			operation.setWorkTimeDisc(item.getWorkTimeDisc());
			session.save(operation);
		}
	}

	@Override
	public void deleteAttSheetDocument(SessionAttSheetDocument document)
	{
		this.deleteAttSheetDocument(document, true, null);
	}

	private void deleteAttSheetDocument(SessionAttSheetDocument document, boolean toDeleteDocument, Session session)
	{
		if(session == null) session = getSession();
		deleteMarks(document.getId(),session);
		Set<SessionComission> commissionList = new HashSet<>();
		for (SessionDocumentSlot sessionDocumentSlot: getList(SessionDocumentSlot.class, SessionDocumentSlot.document().s(), document)){
			session.delete(sessionDocumentSlot);
		}
		for (SessionAttSheetSlot slot : getList(SessionAttSheetSlot.class, SessionAttSheetSlot.document().s(), document))
		{
			if (slot.getCommission() != null)
				commissionList.add(slot.getCommission());
			session.delete(slot);
		}

		for (SessionComissionPps member : getCommissionPps(document))
		{
			session.delete(member);
		}

		for (SessionDocumentSlot sessionDocumentSlot: getList(SessionDocumentSlot.class, SessionDocumentSlot.document().s(), document)){
			session.delete(sessionDocumentSlot);
		}

		for (SessionComission commission : commissionList)
		{
			session.delete(commission);
		}

		if (toDeleteDocument)
		{
			session.delete(document);
		}
	}

	@Override
	public ISelectModel prepareMarkModel(EppStudentWpeCAction slot)
	{
		if (null == slot || null == slot.getType())
		{
			return new LazySimpleSelectModel<>(ImmutableList.of());
		}
		// достаем все положительные оценки (из шкалы слота)
		EppGradeScale scale = ISessionDocumentBaseDAO.instance.get().getGradeScale(slot);
		List<SessionMarkGradeValueCatalogItem> scaleMarks = new DQLSelectBuilder().fromEntity(SessionMarkGradeValueCatalogItem.class, "m")
				.column(property("m"))
				.where(eq(property("m", SessionMarkGradeValueCatalogItem.scale()), value(scale)))
				.where(eq(property("m", SessionMarkGradeValueCatalogItem.positive()), value(Boolean.TRUE)))
				.createStatement(getSession()).list();
		return new LazySimpleSelectModel<>(scaleMarks);
	}

	@Override
	public Collection<SessionComissionPps> getCommissionPps(SessionAttSheetDocument document)
	{
		return new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "scp")
				.column("scp")
				.where(exists(SessionAttSheetSlot.class,
				              SessionAttSheetSlot.L_DOCUMENT, document,
				              SessionAttSheetSlot.L_COMMISSION, property("scp", SessionComissionPps.L_COMMISSION)))
				.createStatement(getSession()).list();
	}
	@Override
	public void deleteMarks(Long documentId, Session session)
	{
		SessionTransferDocument document = getNotNull(documentId);

		// удаляем все оценки - они удалятся вместе с атт. листами мероприятиями
		new DQLDeleteBuilder(SessionAttSheetOperation.class)
				.where(eq(property(SessionAttSheetOperation.targetMark().slot().document()), value(document)))
				.createStatement(session).execute();
	}
}
