/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;

/**
 * @author Alexey Lopatin
 * @since 11.12.2013
 */
@SuppressWarnings({"unused"})
public class MS_unifefu_2x3x3_15to16 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
                new ScriptDependency("org.tandemframework", "1.6.13"),
                new ScriptDependency("org.tandemframework.shared", "1.3.3"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.3.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        String[] badParagraphTitles = {
                "Выписка из списочного приказа. О допуске к сдаче государственных экзаменов. Допустить к сдаче государственных экзаменов",
                "Выписка из списочного приказа. О назначении академической стипендии (вступительные испытания). Назначить государственную академическую стипендию"
        };

        String[] goodParagraphTitles = {
                "Шаблон подпараграфа списочного приказа. О допуске к сдаче государственных экзаменов. Допустить к сдаче государственных экзаменов",
                "Шаблон подпараграфа списочного приказа. О назначении академической стипендии (вступительные испытания). Назначить государственную академическую стипендию"
        };

        PreparedStatement st3 = tool.prepareStatement("update movestudenttemplate_t set title_p=? where title_p=? and index_p=3");

        int counter = 0;
        for (int i = 0; i < badParagraphTitles.length; i++)
        {
            st3.setString(1, goodParagraphTitles[i]);
            st3.setString(2, badParagraphTitles[i]);
            counter += st3.executeUpdate();
        }

        System.out.println(getClass().getSimpleName() + ": fixed " + counter + " from " + badParagraphTitles.length);
    }
}