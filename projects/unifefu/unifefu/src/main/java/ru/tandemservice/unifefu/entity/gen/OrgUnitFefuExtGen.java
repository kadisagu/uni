package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.entity.OrgUnitFefuExt;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Подразделение (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitFefuExtGen extends EntityBase
 implements INaturalIdentifiable<OrgUnitFefuExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.OrgUnitFefuExt";
    public static final String ENTITY_NAME = "orgUnitFefuExt";
    public static final int VERSION_HASH = 1517870982;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_EMPLOYEE_POST_POSSIBLE_VISA = "employeePostPossibleVisa";

    private OrgUnit _orgUnit;     // Подразделение
    private EmployeePostPossibleVisa _employeePostPossibleVisa;     // Виза сотрудника

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Виза сотрудника.
     */
    public EmployeePostPossibleVisa getEmployeePostPossibleVisa()
    {
        return _employeePostPossibleVisa;
    }

    /**
     * @param employeePostPossibleVisa Виза сотрудника.
     */
    public void setEmployeePostPossibleVisa(EmployeePostPossibleVisa employeePostPossibleVisa)
    {
        dirty(_employeePostPossibleVisa, employeePostPossibleVisa);
        _employeePostPossibleVisa = employeePostPossibleVisa;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitFefuExtGen)
        {
            if (withNaturalIdProperties)
            {
                setOrgUnit(((OrgUnitFefuExt)another).getOrgUnit());
            }
            setEmployeePostPossibleVisa(((OrgUnitFefuExt)another).getEmployeePostPossibleVisa());
        }
    }

    public INaturalId<OrgUnitFefuExtGen> getNaturalId()
    {
        return new NaturalId(getOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<OrgUnitFefuExtGen>
    {
        private static final String PROXY_NAME = "OrgUnitFefuExtNaturalProxy";

        private Long _orgUnit;

        public NaturalId()
        {}

        public NaturalId(OrgUnit orgUnit)
        {
            _orgUnit = ((IEntity) orgUnit).getId();
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OrgUnitFefuExtGen.NaturalId) ) return false;

            OrgUnitFefuExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitFefuExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitFefuExt.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitFefuExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "employeePostPossibleVisa":
                    return obj.getEmployeePostPossibleVisa();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "employeePostPossibleVisa":
                    obj.setEmployeePostPossibleVisa((EmployeePostPossibleVisa) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "employeePostPossibleVisa":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "employeePostPossibleVisa":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "employeePostPossibleVisa":
                    return EmployeePostPossibleVisa.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitFefuExt> _dslPath = new Path<OrgUnitFefuExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitFefuExt");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.OrgUnitFefuExt#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Виза сотрудника.
     * @see ru.tandemservice.unifefu.entity.OrgUnitFefuExt#getEmployeePostPossibleVisa()
     */
    public static EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa> employeePostPossibleVisa()
    {
        return _dslPath.employeePostPossibleVisa();
    }

    public static class Path<E extends OrgUnitFefuExt> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa> _employeePostPossibleVisa;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.OrgUnitFefuExt#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Виза сотрудника.
     * @see ru.tandemservice.unifefu.entity.OrgUnitFefuExt#getEmployeePostPossibleVisa()
     */
        public EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa> employeePostPossibleVisa()
        {
            if(_employeePostPossibleVisa == null )
                _employeePostPossibleVisa = new EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa>(L_EMPLOYEE_POST_POSSIBLE_VISA, this);
            return _employeePostPossibleVisa;
        }

        public Class getEntityClass()
        {
            return OrgUnitFefuExt.class;
        }

        public String getEntityName()
        {
            return "orgUnitFefuExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
