package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.gen.*;

/** @see ru.tandemservice.unifefu.entity.gen.FefuActivityTypeGen */
public class FefuActivityType extends FefuActivityTypeGen
{

    public FefuActivityType()
    {

    }

    public FefuActivityType(EppEduPlanVersionBlock block)
    {
        this.setBlock(block);
    }
}