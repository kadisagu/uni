package ru.tandemservice.unifefu.base.ext.TrOrgUnit.ui.JournalAdd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalAdd.TrOrgUnitJournalAdd;

/**
 * User: newdev
 */
@Configuration
public class TrOrgUnitJournalAddExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrOrgUnitJournalAddExtUI.class.getSimpleName();

    @Autowired
    private TrOrgUnitJournalAdd _trOrgUnitJournalAdd;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trOrgUnitJournalAdd.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrOrgUnitJournalAddExtUI.class))
                .addAction(new TrOrgUnitJournalAddClickApply("onClickApply"))
                .create();
    }
}

