<?xml version="1.0" encoding="UTF-8"?>
<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">

    <xs:element name="student" type="studentType"/>

    <xs:complexType name="studentType">
        <xs:annotation>
            <xs:documentation>Данные студента - корневой элемент датаграммы</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element type="xs:string" name="ratingtitle" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Дата формирования рейтинга</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="name" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>ФИО студента</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="lotus_id" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>Идентификатор студента в Lotus</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="Guid" name="person_guid" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>GUID физ. лица из НСИ</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="groupsType" name="groups" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Группы студента</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="groupsType">
        <xs:annotation>
            <xs:documentation>Академические группы студента</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element type="groupType" name="group" maxOccurs="unbounded" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Академические группы студента (всегда будет одна)</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="groupType">
        <xs:annotation>
            <xs:documentation>Академическая группа студента</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element type="xs:string" name="name" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Наименование академической группы студента</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="courseType" name="course" maxOccurs="unbounded" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Данные реализаций дисциплины</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="courseType">
        <xs:annotation>
            <xs:documentation>Данные реализации дисциплины</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element type="xs:string" name="name" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Наименование дисциплины из реализации</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="teacher_name" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>ФИО преподавателей из УГС</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="criteria" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Коэффициенты БРС для реализации</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="rating_final" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Итоговый рейтинг</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="rating_current" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Текущий рейтинг</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="has_required_points" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Факт сдачи всех КМ из реализации с заданным минимальным баллом (да/нет)</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:enumeration value="1"/>
                        <xs:enumeration value="0"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="has_required_points_current" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Факт сдачи на текущую дату всех КМ из реализации с заданным минимальным баллом (да/нет)</xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:enumeration value="1"/>
                        <xs:enumeration value="0"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element type="xs:string" name="result" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Текстовая оценка</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="result_current" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Текстовая оценка на текущую дату</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="url" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>URL страницы с рейтингом (не используется)</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="taskType" name="task" maxOccurs="unbounded" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>Данные контрольных мероприятий из реализации дисциплины</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="taskType">
        <xs:annotation>
            <xs:documentation>Данные контрольного мероприятия из реализации дисциплины</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element type="xs:string" name="name" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Тема контрольнго мероприятия из структуры чтения в реализации дисциплины</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="due" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Дата проведения контрольнго мероприятия</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="weight" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Вес контрольнго мероприятия</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="max_points" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Максимальный балл для контрольнго мероприятия</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="type_name" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Тип контрольнго мероприятия из структуры чтения в реализации дисциплины</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="required_points" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Минимальный балл для контрольнго мероприятия</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="mark_due" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Конечная дата выставления оценки</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="markType" name="mark" maxOccurs="unbounded" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>Данные об оценках по контрольному мероприятию</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="markType">
        <xs:annotation>
            <xs:documentation>Данные об оценке по контрольному мероприятию</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element type="xs:string" name="mdate" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Дата выставления оценки</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element type="xs:string" name="value" minOccurs="1">
                <xs:annotation>
                    <xs:documentation>Балл</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:simpleType name="Guid">
        <xs:annotation>
            <xs:documentation>GUID по типу того, что используется в НСИ</xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:pattern value="[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}"/>
        </xs:restriction>
    </xs:simpleType>

</xs:schema>