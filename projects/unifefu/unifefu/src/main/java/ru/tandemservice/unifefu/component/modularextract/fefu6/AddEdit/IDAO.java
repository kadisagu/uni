/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu6.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuChangeFioStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 03.09.2012
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuChangeFioStuExtract, Model>
{
}