/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.CompetenceAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 01.12.2014
 */
@Configuration
public class FefuEduStdCompetenceAdd extends BusinessComponentManager
{
    public static final String EPP_SKILL_GROUP_DS = "eppSkillGroupDS";
    public static final String FEFU_COMPETENCE_DS = "fefuCompetenceDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EPP_SKILL_GROUP_DS, eppSkillGroupDSHandler()))
                .addDataSource(selectDS(FEFU_COMPETENCE_DS, fefuCompetenceDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eppSkillGroupDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppSkillGroup.class)
                .filter(EppSkillGroup.title())
                .order(EppSkillGroup.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> fefuCompetenceDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FefuCompetence.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                Long eduStandardId = context.get(FefuEduStdCompetenceAddUI.EDU_STANDARD_ID);
                EppSkillGroup eppSkillGroup = context.get(FefuEduStdCompetenceAddUI.EPP_SKILL_GROUP);

                if (null == eppSkillGroup) dql.where(nothing());
                else
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(FefuCompetence2EppStateEduStandardRel.class, "r")
                            .column(property("r", FefuCompetence2EppStateEduStandardRel.fefuCompetence().id()))
                            .where(eq(property("r", FefuCompetence2EppStateEduStandardRel.eduStandard().id()), value(eduStandardId)));

                    dql.where(eq(property(alias, FefuCompetence.eppSkillGroup().id()), value(eppSkillGroup.getId())));
                    dql.where(notIn(property(alias, FefuCompetence.id()), subBuilder.buildQuery()));
                }
            }
        }
                .filter(FefuCompetence.title())
                .order(FefuCompetence.title());
    }
}
