/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSchedule.ui.PrintFormAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.FefuScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

/**
 * @author nvankov
 * @since 9/25/13
 */
@Configuration
public class FefuSchedulePrintFormAdd extends BusinessComponentManager
{
    public static final String FORMATIVE_OU_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_OU_DS = "territorialOrgUnitDS";
    public static final String EDU_LEVEL_DS = "eduLevelDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String COURSE_DS = "courseDS";
    public static final String SEASON_DS = "seasonDS";
    public static final String BELLS_DS = "bellsDS";
    public static final String SCHEDULES_DS = "schedulesDS";
    public static final String TERM_DS = "termDS";
    public static final String ADMIN_DS = "adminDS";
    public static final String EDUCATION_YEAR = "educationYearDS";
    public static final String EMPLOYEE_POST_DS = "employeePostDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(FORMATIVE_OU_DS, FefuScheduleManager.instance().fefuGroupComboDSHandler()))
                .addDataSource(selectDS(TERRITORIAL_OU_DS, FefuScheduleManager.instance().fefuGroupComboDSHandler()).addColumn("title", OrgUnit.territorialFullTitle().s()))
                .addDataSource(selectDS(EDU_LEVEL_DS, FefuScheduleManager.instance().fefuGroupComboDSHandler()).addColumn("title", EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, FefuScheduleManager.instance().fefuGroupComboDSHandler()))
                .addDataSource(selectDS(COURSE_DS, FefuScheduleManager.instance().fefuGroupComboDSHandler()))
                .addDataSource(selectDS(SEASON_DS, FefuScheduleManager.instance().schedulesComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleSeason>()
                {
                    @Override
                    public String format(SppScheduleSeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .addDataSource(selectDS(BELLS_DS, FefuScheduleManager.instance().schedulesComboDSHandler()))
                .addDataSource(selectDS(SCHEDULES_DS, FefuScheduleManager.instance().schedulesComboDSHandler()).addColumn("group", SppSchedule.group().title().s()).addColumn("schedule", SppSchedule.title().s()))
                .addDataSource(selectDS(TERM_DS, FefuScheduleManager.instance().termComboDSHandler()))
                .addDataSource(selectDS(EDUCATION_YEAR, educationYearComboDSHandler()))
                .addDataSource(selectDS(ADMIN_DS, SppScheduleManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s()))
                .addDataSource(selectDS(EMPLOYEE_POST_DS, SppScheduleManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler educationYearComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EducationYear.class);
    }
}
