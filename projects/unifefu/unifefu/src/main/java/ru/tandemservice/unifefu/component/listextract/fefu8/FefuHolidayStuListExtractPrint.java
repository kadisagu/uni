/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu8;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Alexey Lopatin
 * @since 21.10.2013
 */
public class FefuHolidayStuListExtractPrint implements IPrintFormCreator<FefuHolidayStuListExtract>, IListParagraphPrintFormCreator<FefuHolidayStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuHolidayStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuHolidayStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);

        Student student = firstExtract.getEntity();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();

        CommonExtractPrint.initOrgUnit(injectModifier, educationOrgUnit, "formativeOrgUnitStr", "");
        CommonExtractPrint.modifyEducationStr(injectModifier, educationOrgUnit);

        injectModifier.put("beginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getBeginDate()));
        injectModifier.put("endDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getEndDate()));

        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuHolidayStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuHolidayStuListExtract firstExtract)
    {
    }
}