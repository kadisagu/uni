/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;
import ru.tandemservice.unifefu.base.vo.FefuEduWorkPlanSumData;
import ru.tandemservice.unifefu.base.vo.FefuLoadTypeVO;
import ru.tandemservice.unifefu.entity.catalog.FefuLoadType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuLoadTypeCodes;
import ru.tandemservice.unifefu.entity.eduPlan.FefuChangedWorkPlan;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkPlanAgreementCheckingData;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 1/23/14
 */
public class FefuEduWorkPlanDAO extends BaseModifyAggregateDAO implements IFefuEduWorkPlanDAO
{
    @Override
    public StaticListDataSource<IEppWorkPlanRowWrapper> workPlanRowExtDataSource(Long wpId)
    {
        return new StaticListDataSource<>();
    }

    @Transactional(readOnly = true)
    @Override
    public Map<Long, Map<Integer, Map<String, Number>>> getInteractiveLoadMap(Long wpId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuEppWorkPlanRowPartLoad.class, "l");
        builder.where(eq(property("l", FefuEppWorkPlanRowPartLoad.row().workPlan().id()), value(wpId)));

        Map<Long, Map<Integer, Map<String, Number>>> interactiveLoadMap = Maps.newHashMap();
        for (FefuEppWorkPlanRowPartLoad rowPartLoad : createStatement(builder).<FefuEppWorkPlanRowPartLoad>list())
        {
            Long rowId = rowPartLoad.getRow().getId();
            Integer part = rowPartLoad.getPart();
            String code = rowPartLoad.getLoadType().getCode();

            if (!interactiveLoadMap.containsKey(rowId))
                interactiveLoadMap.put(rowId, Maps.<Integer, Map<String, Number>>newHashMap());
            if (!interactiveLoadMap.get(rowId).containsKey(part))
                interactiveLoadMap.get(rowId).put(part, Maps.<String, Number>newHashMap());
            if (!interactiveLoadMap.get(rowId).get(part).containsKey(code))
                interactiveLoadMap.get(rowId).get(part).put(code, rowPartLoad.getLoadAsDouble());
        }

        return interactiveLoadMap;
    }

    @Transactional(readOnly = true)
    @Override
    public List<FefuLoadTypeVO> getLoadTypeList()
    {
        Map<String, String> loadTypesMap = Maps.newHashMap();
        loadTypesMap.put(EppALoadTypeCodes.TYPE_LECTURES, FefuLoadTypeCodes.TYPE_LECTURES_INTER);
        loadTypesMap.put(EppALoadTypeCodes.TYPE_PRACTICE, FefuLoadTypeCodes.TYPE_PRACTICE_INTER);
        loadTypesMap.put(EppALoadTypeCodes.TYPE_LABS, FefuLoadTypeCodes.TYPE_LABS_INTER);

        Map<String, FefuLoadType> interactiveLoadTypeMap = fefuLoadTypeMap();

        List<FefuLoadTypeVO> loadTypeVOs = Lists.newArrayList();

        List<EppALoadType> loadTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppALoadType.class);

        for (EppALoadType loadType : loadTypes)
        {
            loadTypeVOs.add(new FefuLoadTypeVO(loadType));
            loadTypeVOs.add(new FefuLoadTypeVO(interactiveLoadTypeMap.get(loadTypesMap.get(loadType.getCode()))));
        }
        loadTypeVOs.add(new FefuLoadTypeVO(interactiveLoadTypeMap.get(FefuLoadTypeCodes.TYPE_EXAM_HOURS)));
        return loadTypeVOs;
    }

    @Override
    public List<EppWorkPlanPart> getPartsForRow(Long workPlanId)
    {
        DQLSelectBuilder workPlanPartsBuilder = new DQLSelectBuilder().fromEntity(EppWorkPlanPart.class, "wpp");
        workPlanPartsBuilder.where(eq(property("wpp", EppWorkPlanPart.workPlan().id()), value(workPlanId)));
        workPlanPartsBuilder.order(property("wpp", EppWorkPlanPart.number()));

        return createStatement(workPlanPartsBuilder).list();
    }

    @Override
    public Map<Integer, Map<String, FefuEppWorkPlanRowPartLoad>> getFefuPartLoadMapForRow(Long rowId)
    {
        DQLSelectBuilder fefuPartLoadBuilder = new DQLSelectBuilder().fromEntity(FefuEppWorkPlanRowPartLoad.class, "pl");
        fefuPartLoadBuilder.where(eq(property("pl", FefuEppWorkPlanRowPartLoad.row().id()), value(rowId)));

        Map<Integer, Map<String, FefuEppWorkPlanRowPartLoad>> fefuPartLoadMap = Maps.newHashMap();
        for (FefuEppWorkPlanRowPartLoad partLoad : createStatement(fefuPartLoadBuilder).<FefuEppWorkPlanRowPartLoad>list())
        {
            if (!fefuPartLoadMap.containsKey(partLoad.getPart()))
                fefuPartLoadMap.put(partLoad.getPart(), Maps.<String, FefuEppWorkPlanRowPartLoad>newHashMap());
            if (!fefuPartLoadMap.get(partLoad.getPart()).containsKey(partLoad.getLoadType().getCode()))
                fefuPartLoadMap.get(partLoad.getPart()).put(partLoad.getLoadType().getCode(), partLoad);
        }

        return fefuPartLoadMap;
    }

    @Override
    public Map<Integer, Map<String, EppWorkPlanRowPartLoad>> getEppPartLoadMapForRow(Long rowId)
    {
        DQLSelectBuilder eppPartLoadBuilder = new DQLSelectBuilder().fromEntity(EppWorkPlanRowPartLoad.class, "pl");
        eppPartLoadBuilder.where(eq(property("pl", EppWorkPlanRowPartLoad.row().id()), value(rowId)));

        Map<Integer, Map<String, EppWorkPlanRowPartLoad>> eppPartLoadMap = Maps.newHashMap();
        for (EppWorkPlanRowPartLoad partLoad : createStatement(eppPartLoadBuilder).<EppWorkPlanRowPartLoad>list())
        {
            if (!eppPartLoadMap.containsKey(partLoad.getPart()))
                eppPartLoadMap.put(partLoad.getPart(), Maps.<String, EppWorkPlanRowPartLoad>newHashMap());
            if (!eppPartLoadMap.get(partLoad.getPart()).containsKey(partLoad.getLoadType().getFullCode()))
                eppPartLoadMap.get(partLoad.getPart()).put(partLoad.getLoadType().getFullCode(), partLoad);
        }

        return eppPartLoadMap;
    }

    @Override
    public void updateLoadData(List<FefuEppWorkPlanRowPartLoad> createOrUpdatePartLoads, List<FefuEppWorkPlanRowPartLoad> deletePartLoads)
    {
        for (FefuEppWorkPlanRowPartLoad partLoad : createOrUpdatePartLoads)
        {
            baseCreateOrUpdate(partLoad);
        }
        for (FefuEppWorkPlanRowPartLoad partLoad : deletePartLoads)
        {
            baseDelete(partLoad);
        }
    }

    @Override
    public Map<String, FefuLoadType> fefuLoadTypeMap()
    {
        Map<String, FefuLoadType> interactiveLoadTypeMap = Maps.newHashMap();

        for (FefuLoadType loadType : DataAccessServices.dao().getList(FefuLoadType.class))
        {
            interactiveLoadTypeMap.put(loadType.getCode(), loadType);
        }

        return interactiveLoadTypeMap;
    }

    @Transactional(readOnly = true)
    @Override
    public FefuEduWorkPlanSumData getWorkPlanSumData(Long eppWorkPlanId)
    {
        FefuEduWorkPlanSumData sumData = new FefuEduWorkPlanSumData();

        IEppWorkPlanWrapper wrapper = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(Collections.singleton(eppWorkPlanId)).get(eppWorkPlanId);

        EppELoadType selfWorkLoadType = DataAccessServices.dao().get(EppELoadType.class, EppELoadType.code(), EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);

        List<EppWorkPlanPart> parts = getPartsForRow(eppWorkPlanId);

        List<EppControlActionType> actionTypes = IEppWorkPlanDAO.instance.get().getActiveControlActionTypes(null);

        EppWorkPlan workPlan = (EppWorkPlan) getNotNull(eppWorkPlanId);
        EppEduPlanVersionBlock planVersionBlock = workPlan.getBlock();
        Term term = workPlan.getTerm();

        IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(planVersionBlock.getId(), true);
        final int workPlanWeeks /*недели*/ = blockWrapper.getTermSize(IEppEpvBlockWrapper.ELOAD_FULL_CODE_ALL, term.getIntValue());

        Double totalLabor = 0d;
        Double aLoad = 0d;
        Double examHours = 0d;
        Double selfWork = 0d;

        int actionCount = 0;

        if (null != wrapper && null != wrapper.getRowMap())
        {
            for (Map.Entry<Long, IEppWorkPlanRowWrapper> entry : wrapper.getRowMap().entrySet())
            {

                if (null != entry.getValue())
                {
                    IEppWorkPlanRowWrapper rowWrapper = entry.getValue();
                    if (null != rowWrapper.getRegistryElementPart() && null != rowWrapper.getRegistryElementPart().getItem())
                    {
                        if (null != rowWrapper.getRegistryElementPart().getItem().getLaborAsDouble())
                            totalLabor += rowWrapper.getRegistryElementPart().getItem().getLaborAsDouble();
                        if (null != rowWrapper.getRegistryElementPart().getItem().getSizeAsDouble())
                            aLoad += rowWrapper.getRegistryElementPart().getItem().getSizeAsDouble();
                    }
                    // самостоятельная нагрузка
                    for (EppWorkPlanPart part : parts)
                    {
                        selfWork += rowWrapper.getTotalPartLoad(part.getNumber(), selfWorkLoadType.getFullCode());
                    }

                    // промежуточный контроль
                    for (EppControlActionType actionType : actionTypes)
                    {
                        if (actionType instanceof EppFControlActionType)
                        {
                            actionCount += rowWrapper.getActionSize(actionType.getFullCode());
                        }
                    }
                }
            }

            // часы за экз.
            DQLSelectBuilder examHBuilder = new DQLSelectBuilder().fromEntity(FefuEppWorkPlanRowPartLoad.class, "l");
            examHBuilder.where(eq(property("l", FefuEppWorkPlanRowPartLoad.row().workPlan().id()), value(eppWorkPlanId)));
            examHBuilder.where(eq(property("l", FefuEppWorkPlanRowPartLoad.loadType().code()), value(FefuLoadTypeCodes.TYPE_EXAM_HOURS)));
            for (FefuEppWorkPlanRowPartLoad load : createStatement(examHBuilder).<FefuEppWorkPlanRowPartLoad>list())
            {
                if (null != load.getLoadAsDouble())
                    examHours += load.getLoadAsDouble();
            }

            if (totalLabor > 0)
                sumData.setTotalLabor(UniEppUtils.formatLoad(totalLabor, false));
            if (aLoad > 0)
                sumData.setaLoad(UniEppUtils.formatLoad(aLoad, false));
            if (selfWork > 0)
                sumData.setSelfWork(UniEppUtils.formatLoad(selfWork, false));
            if (actionCount > 0)
                sumData.setActionCount(String.valueOf(actionCount));
            if (examHours > 0)
                sumData.setExamHours(UniEppUtils.formatLoad(examHours, false));

            if (workPlanWeeks > 0)
            {
                Double averageALoad = aLoad / workPlanWeeks;
                Double averageTotalLoad = (examHours + selfWork + aLoad) / workPlanWeeks;

                sumData.setAverageALoad(UniEppUtils.formatLoad(averageALoad, false));
                sumData.setAverageTotalLoad(UniEppUtils.formatLoad(averageTotalLoad, false));
            }
        }

        Boolean checkedData = new DQLSelectBuilder()
                .fromEntity(FefuWorkPlanAgreementCheckingData.class, "d")
                .column(property("d", FefuWorkPlanAgreementCheckingData.P_NO_UNALLOCATED_DISCIPLINE))
                .where(eq(property("d", FefuWorkPlanAgreementCheckingData.L_EPP_WORK_PLAN), value(eppWorkPlanId)))
                .createStatement(getSession()).uniqueResult();
        if (checkedData != null)
            sumData.setExistUnallocatedDiscipline(checkedData ? "Нет" : "Да");

        Boolean changed = new DQLSelectBuilder()
                .fromEntity(FefuChangedWorkPlan.class, "c")
                .column(property("c", FefuChangedWorkPlan.P_CHANGED))
                .where(eq(property("c", FefuChangedWorkPlan.L_EPP_WORK_PLAN), value(eppWorkPlanId)))
                .createStatement(getSession()).uniqueResult();
        if (changed != null)
            sumData.setChanged(changed ? "Да" : "Нет");

        return sumData;
    }
}