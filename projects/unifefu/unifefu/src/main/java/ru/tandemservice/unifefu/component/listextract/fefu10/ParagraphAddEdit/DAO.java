/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu10.ParagraphAddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract;

import java.util.List;

/**
 * @author nvankov
 * @since 11/21/13
 */
public class DAO extends AbstractListParagraphAddEditDAO<FullStateMaintenanceEnrollmentStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));

        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        model.setEmployeePostModel(new OrderExecutorSelectModel());

        // заполняем поля сохраненными значениями
        FullStateMaintenanceEnrollmentStuListExtract extract = model.getFirstExtract();
        if (null != extract)
        {
            if (model.getParagraphId() != null)
            {
                model.setGroup(extract.getGroup());
                model.setCompensationType(extract.getCompensationType());
            }
            model.setCourse(extract.getCourse());
            model.setEnrollDate(extract.getEnrollDate());
            model.setProtocol(extract.getProtocolNumAndDate());
            model.setStartPaymentDate(extract.getStartPaymentPeriodDate());
            model.setPaymentEndDate(extract.getPaymentEndDate());
            model.setResponsibleForPayments(extract.getResponsibleForPayments());
            model.setResponsibleForAgreement(extract.getResponsibleForAgreement());
        }
        model.setEditCourseDisabled(!model.isParagraphOnlyOneInTheOrder());
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP,  model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourse()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected FullStateMaintenanceEnrollmentStuListExtract createNewInstance(Model model)
    {
        return new FullStateMaintenanceEnrollmentStuListExtract();
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if ( (model.getPaymentEndDate() != null) && (model.getPaymentEndDate().getTime() < model.getStartPaymentDate().getTime()))
            errors.add("Дата окончания выплат не может быть меньше даты начала.", "startPaymentPeriodDate", "paymentEndDate");
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FullStateMaintenanceEnrollmentStuListExtract.class, "e");
        builder.where(DQLExpressions.eq(DQLExpressions.property("e", FullStateMaintenanceEnrollmentStuListExtract.paragraph().order().id()), DQLExpressions.value(model.getParagraph().getOrder().getId())));
        builder.where(DQLExpressions.not(DQLExpressions.eq(DQLExpressions.property("e", FullStateMaintenanceEnrollmentStuListExtract.paragraph().id()), DQLExpressions.value(model.getParagraphId()))));
        List<FullStateMaintenanceEnrollmentStuListExtract> extracts = builder.createStatement(getSession()).list();
        for(FullStateMaintenanceEnrollmentStuListExtract extract : extracts)
        {
            extract.setEnrollDate(model.getEnrollDate());
            extract.setProtocolNumAndDate(model.getProtocol());
            extract.setStartPaymentPeriodDate(model.getStartPaymentDate());
            extract.setResponsibleForPayments(model.getResponsibleForPayments());
            extract.setResponsibleForAgreement(model.getResponsibleForAgreement());
            update(extract);
        }
    }

    @Override
    protected void fillExtract(FullStateMaintenanceEnrollmentStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
        extract.setEnrollDate(model.getEnrollDate());
        extract.setProtocolNumAndDate(model.getProtocol());
        extract.setStartPaymentPeriodDate(model.getStartPaymentDate());
        extract.setPaymentEndDate(model.getPaymentEndDate());
        extract.setResponsibleForPayments(model.getResponsibleForPayments());
        extract.setResponsibleForAgreement(model.getResponsibleForAgreement());
    }
}
