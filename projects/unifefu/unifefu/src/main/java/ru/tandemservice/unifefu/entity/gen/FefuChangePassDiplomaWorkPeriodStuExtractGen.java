package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О переносе защиты выпускной квалификационной работы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuChangePassDiplomaWorkPeriodStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract";
    public static final String ENTITY_NAME = "fefuChangePassDiplomaWorkPeriodStuExtract";
    public static final int VERSION_HASH = 630258664;
    private static IEntityMeta ENTITY_META;

    public static final String P_NEW_DATE = "newDate";
    public static final String P_YEAR = "year";
    public static final String P_SEASON = "season";
    public static final String P_AFTER_STATE_EXAM = "afterStateExam";
    public static final String P_PLURAL_FOR_STATE_EXAM = "pluralForStateExam";

    private Date _newDate;     // Новая дата
    private int _year;     // Год
    private String _season;     // Время года
    private boolean _afterStateExam;     // После сдачи ГЭ
    private boolean _pluralForStateExam;     // Множественное число для ГЭ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Новая дата. Свойство не может быть null.
     */
    @NotNull
    public Date getNewDate()
    {
        return _newDate;
    }

    /**
     * @param newDate Новая дата. Свойство не может быть null.
     */
    public void setNewDate(Date newDate)
    {
        dirty(_newDate, newDate);
        _newDate = newDate;
    }

    /**
     * @return Год. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Время года.
     */
    @Length(max=255)
    public String getSeason()
    {
        return _season;
    }

    /**
     * @param season Время года.
     */
    public void setSeason(String season)
    {
        dirty(_season, season);
        _season = season;
    }

    /**
     * @return После сдачи ГЭ. Свойство не может быть null.
     */
    @NotNull
    public boolean isAfterStateExam()
    {
        return _afterStateExam;
    }

    /**
     * @param afterStateExam После сдачи ГЭ. Свойство не может быть null.
     */
    public void setAfterStateExam(boolean afterStateExam)
    {
        dirty(_afterStateExam, afterStateExam);
        _afterStateExam = afterStateExam;
    }

    /**
     * @return Множественное число для ГЭ. Свойство не может быть null.
     */
    @NotNull
    public boolean isPluralForStateExam()
    {
        return _pluralForStateExam;
    }

    /**
     * @param pluralForStateExam Множественное число для ГЭ. Свойство не может быть null.
     */
    public void setPluralForStateExam(boolean pluralForStateExam)
    {
        dirty(_pluralForStateExam, pluralForStateExam);
        _pluralForStateExam = pluralForStateExam;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuChangePassDiplomaWorkPeriodStuExtractGen)
        {
            setNewDate(((FefuChangePassDiplomaWorkPeriodStuExtract)another).getNewDate());
            setYear(((FefuChangePassDiplomaWorkPeriodStuExtract)another).getYear());
            setSeason(((FefuChangePassDiplomaWorkPeriodStuExtract)another).getSeason());
            setAfterStateExam(((FefuChangePassDiplomaWorkPeriodStuExtract)another).isAfterStateExam());
            setPluralForStateExam(((FefuChangePassDiplomaWorkPeriodStuExtract)another).isPluralForStateExam());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuChangePassDiplomaWorkPeriodStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuChangePassDiplomaWorkPeriodStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuChangePassDiplomaWorkPeriodStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                    return obj.getNewDate();
                case "year":
                    return obj.getYear();
                case "season":
                    return obj.getSeason();
                case "afterStateExam":
                    return obj.isAfterStateExam();
                case "pluralForStateExam":
                    return obj.isPluralForStateExam();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "newDate":
                    obj.setNewDate((Date) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "season":
                    obj.setSeason((String) value);
                    return;
                case "afterStateExam":
                    obj.setAfterStateExam((Boolean) value);
                    return;
                case "pluralForStateExam":
                    obj.setPluralForStateExam((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                        return true;
                case "year":
                        return true;
                case "season":
                        return true;
                case "afterStateExam":
                        return true;
                case "pluralForStateExam":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                    return true;
                case "year":
                    return true;
                case "season":
                    return true;
                case "afterStateExam":
                    return true;
                case "pluralForStateExam":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                    return Date.class;
                case "year":
                    return Integer.class;
                case "season":
                    return String.class;
                case "afterStateExam":
                    return Boolean.class;
                case "pluralForStateExam":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuChangePassDiplomaWorkPeriodStuExtract> _dslPath = new Path<FefuChangePassDiplomaWorkPeriodStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuChangePassDiplomaWorkPeriodStuExtract");
    }
            

    /**
     * @return Новая дата. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract#getNewDate()
     */
    public static PropertyPath<Date> newDate()
    {
        return _dslPath.newDate();
    }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Время года.
     * @see ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract#getSeason()
     */
    public static PropertyPath<String> season()
    {
        return _dslPath.season();
    }

    /**
     * @return После сдачи ГЭ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract#isAfterStateExam()
     */
    public static PropertyPath<Boolean> afterStateExam()
    {
        return _dslPath.afterStateExam();
    }

    /**
     * @return Множественное число для ГЭ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract#isPluralForStateExam()
     */
    public static PropertyPath<Boolean> pluralForStateExam()
    {
        return _dslPath.pluralForStateExam();
    }

    public static class Path<E extends FefuChangePassDiplomaWorkPeriodStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _newDate;
        private PropertyPath<Integer> _year;
        private PropertyPath<String> _season;
        private PropertyPath<Boolean> _afterStateExam;
        private PropertyPath<Boolean> _pluralForStateExam;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Новая дата. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract#getNewDate()
     */
        public PropertyPath<Date> newDate()
        {
            if(_newDate == null )
                _newDate = new PropertyPath<Date>(FefuChangePassDiplomaWorkPeriodStuExtractGen.P_NEW_DATE, this);
            return _newDate;
        }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(FefuChangePassDiplomaWorkPeriodStuExtractGen.P_YEAR, this);
            return _year;
        }

    /**
     * @return Время года.
     * @see ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract#getSeason()
     */
        public PropertyPath<String> season()
        {
            if(_season == null )
                _season = new PropertyPath<String>(FefuChangePassDiplomaWorkPeriodStuExtractGen.P_SEASON, this);
            return _season;
        }

    /**
     * @return После сдачи ГЭ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract#isAfterStateExam()
     */
        public PropertyPath<Boolean> afterStateExam()
        {
            if(_afterStateExam == null )
                _afterStateExam = new PropertyPath<Boolean>(FefuChangePassDiplomaWorkPeriodStuExtractGen.P_AFTER_STATE_EXAM, this);
            return _afterStateExam;
        }

    /**
     * @return Множественное число для ГЭ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract#isPluralForStateExam()
     */
        public PropertyPath<Boolean> pluralForStateExam()
        {
            if(_pluralForStateExam == null )
                _pluralForStateExam = new PropertyPath<Boolean>(FefuChangePassDiplomaWorkPeriodStuExtractGen.P_PLURAL_FOR_STATE_EXAM, this);
            return _pluralForStateExam;
        }

        public Class getEntityClass()
        {
            return FefuChangePassDiplomaWorkPeriodStuExtract.class;
        }

        public String getEntityName()
        {
            return "fefuChangePassDiplomaWorkPeriodStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
