/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.Fias.FiasManager;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.NsiDatagramUtil;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.datagram.*;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.FefuNsiIdWrapper;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import javax.xml.namespace.QName;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 27.08.2013
 */
public class HumanTypeReactor extends SimpleCatalogEntityReactor<HumanType, Person>
{
    private Map<String, OksmType> oksmMap = new HashMap<>();
    private Map<String, AddressCountry> countryMap = new HashMap<>();
    private Map<String, IdentityCardType> identityCardsMap = new HashMap<>();
    private Map<String, IdentityCardKindType> identityCardTypesMap = new HashMap<>();
    private Map<String, FefuNsiIds> identityCardIdsMap = new HashMap<>();
    private Map<String, org.tandemframework.shared.person.catalog.entity.IdentityCardType> icardTypesMap = new HashMap<>();
    private Set<String> nonProcessableIncomingPersonGuids = new HashSet<>();
    private Map<String, Sex> sexMap = new HashMap<>();

    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<Person> getEntityClass()
    {
        return Person.class;
    }

    @Override
    public Class<HumanType> getNSIEntityClass()
    {
        return HumanType.class;
    }

    @Override
    public String getGUID(HumanType nsiEntity)
    {
        return nsiEntity.getID();
    }

    @Override
    public HumanType getCatalogElementRetrieveRequestObject(String guid)
    {
        HumanType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    protected Map<String, Person> getEntityMap(Class<Person> entityClass)
    {
        return getEntityMap(entityClass, null);
    }

    @Override
    protected Map<String, Person> getEntityMap(final Class<Person> entityClass, Set<Long> entityIds)
    {
        Map<String, Person> result = new HashMap<>();
        final List<Person> entityList = new ArrayList<>();
        final List<IdentityCard> icardsList = new ArrayList<>();

        if (null == entityIds)
        {
            entityList.addAll(DataAccessServices.dao().<Person>getList(
                    new DQLSelectBuilder().fromEntity(Student.class, "s").column("e")
                            .joinEntity("s", DQLJoinType.inner, entityClass, "e", eq(property(Student.person().id().fromAlias("s")), property(Person.id().fromAlias("e"))))
                    //.where(eq(property(Person.id().fromAlias("e")), value(1418978644924573557l)))

            ));
        } else
        {
            BatchUtils.execute(entityIds, 100, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> entityIdsPortion)
                {
                    entityList.addAll(DataAccessServices.dao().<Person>getList(
                            new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
                                    .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                    ));
                    icardsList.addAll(DataAccessServices.dao().<IdentityCard>getList(
                            new DQLSelectBuilder().fromEntity(IdentityCard.class, "e").column("e")
                                    .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                    ));
                }
            });
        }

        for (Person item : entityList/*.subList(0, 2000)*/)
        {
            result.put(item.getId().toString(), item);
        }

        for (IdentityCard icard : icardsList)
        {
            result.put(icard.getPerson().getId().toString(), icard.getPerson());
        }

        return result;
    }

    protected Map<String, FefuNsiIds> fillMaps(Set<Long> entityIds, Map<String, Person> personsMap, Map<String, IdentityCard> icardsMap)
    {
        Set<Long> secondWaveIds = new HashSet<>();
        Map<String, FefuNsiIds> result = new HashMap<>();
        final List<Object[]> objectsList = new ArrayList<>();
        final List<Object[]> secondObjectsList = new ArrayList<>();

        if (null == entityIds) return null;
        BatchUtils.execute(entityIds, 100, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(final Collection<Long> entityIdsPortion)
            {
                objectsList.addAll(DataAccessServices.dao().<Object[]>getList(
                        new DQLSelectBuilder().fromEntity(Person.class, "e").column("e").column("ids")
                                .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "ids", eq(property(Person.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                                .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                ));
                objectsList.addAll(DataAccessServices.dao().<Object[]>getList(
                        new DQLSelectBuilder().fromEntity(IdentityCard.class, "e").column("e").column("ids")
                                .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "ids", eq(property(IdentityCard.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                                .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                ));
            }
        });

        for (Object[] item : objectsList)
        {
            if (null != item[1])
            {
                FefuNsiIds nsiId = (FefuNsiIds) item[1];
                result.put(String.valueOf(nsiId.getEntityId()), nsiId);
                result.put(nsiId.getGuid(), nsiId);
            }

            if (item[0] instanceof Person)
            {
                secondWaveIds.add(((Person) item[0]).getIdentityCard().getId());
                personsMap.put(((Person) item[0]).getId().toString(), (Person) item[0]);
            }
            if (item[0] instanceof IdentityCard)
            {
                secondWaveIds.add(((IdentityCard) item[0]).getPerson().getId());
                icardsMap.put(((IdentityCard) item[0]).getId().toString(), (IdentityCard) item[0]);
            }
        }

        BatchUtils.execute(secondWaveIds, 100, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(final Collection<Long> entityIdsPortion)
            {
                secondObjectsList.addAll(DataAccessServices.dao().<Object[]>getList(
                        new DQLSelectBuilder().fromEntity(Person.class, "e").column("e").column("ids")
                                .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "ids", eq(property(Person.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                                .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                ));
                secondObjectsList.addAll(DataAccessServices.dao().<Object[]>getList(
                        new DQLSelectBuilder().fromEntity(IdentityCard.class, "e").column("e").column("ids")
                                .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "ids", eq(property(IdentityCard.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                                .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                ));
            }
        });

        for (Object[] item : secondObjectsList)
        {
            if (null != item[1])
            {
                FefuNsiIds nsiId = (FefuNsiIds) item[1];
                result.put(String.valueOf(nsiId.getEntityId()), nsiId);
                result.put(nsiId.getGuid(), nsiId);
            }

            if (item[0] instanceof Person) personsMap.put(((Person) item[0]).getId().toString(), (Person) item[0]);
            if (item[0] instanceof IdentityCard)
                icardsMap.put(((IdentityCard) item[0]).getId().toString(), (IdentityCard) item[0]);
        }

        return result;
    }

    @Override
    protected void prepareRelatedObjectsMap()
    {
        if (identityCardTypesMap.values().size() == 0)
        {
            List<FefuNsiIds> icardTypeIdsList = DataAccessServices.dao().getList(
                    new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                            .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(org.tandemframework.shared.person.catalog.entity.IdentityCardType.ENTITY_NAME)))
            );


            for (FefuNsiIds nsiId : icardTypeIdsList)
            {
                IdentityCardKindType icardKindType = NsiReactorUtils.NSI_OBJECT_FACTORY.createIdentityCardKindType();
                icardKindType.setID(nsiId.getGuid());
                identityCardTypesMap.put(nsiId.getGuid(), icardKindType);
                identityCardTypesMap.put(String.valueOf(nsiId.getEntityId()), icardKindType);
                identityCardIdsMap.put(nsiId.getGuid(), nsiId);
                identityCardIdsMap.put(String.valueOf(nsiId.getEntityId()), nsiId);
            }

            icardTypesMap.put(null, UniDaoFacade.getCoreDao().getCatalogItem(org.tandemframework.shared.person.catalog.entity.IdentityCardType.class, IdentityCardTypeCodes.DRUGOY_DOKUMENT));

            List<org.tandemframework.shared.person.catalog.entity.IdentityCardType> icardTypesList =
                    DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(org.tandemframework.shared.person.catalog.entity.IdentityCardType.class, "e").column("e"));

            for (org.tandemframework.shared.person.catalog.entity.IdentityCardType cardType : icardTypesList)
            {
                IdentityCardKindType kindType = identityCardTypesMap.get(cardType.getId().toString());
                if (null != kindType) icardTypesMap.put(kindType.getID(), cardType);
            }
        }

        if (sexMap.values().size() == 0)
        {
            sexMap.put(null, UniDaoFacade.getCoreDao().getCatalogItem(Sex.class, SexCodes.MALE));
            sexMap.put("МУЖСКОЙ", UniDaoFacade.getCoreDao().getCatalogItem(Sex.class, SexCodes.MALE));
            sexMap.put("ЖЕНСКИЙ", UniDaoFacade.getCoreDao().getCatalogItem(Sex.class, SexCodes.FEMALE));
        }


        if (oksmMap.values().size() == 0)
        {
            List<FefuNsiIds> oksmIdsList = DataAccessServices.dao().getList(
                    new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                            .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(AddressCountry.ENTITY_NAME)))
            );

            for (FefuNsiIds nsiId : oksmIdsList)
            {
                OksmType oksmType = NsiReactorUtils.NSI_OBJECT_FACTORY.createOksmType();
                oksmType.setID(nsiId.getGuid());
                oksmMap.put(nsiId.getGuid(), oksmType);
                oksmMap.put(String.valueOf(nsiId.getEntityId()), oksmType);
            }

            countryMap.put(null, FiasManager.instance().kladrDao().getCountry(IKladrDefines.RUSSIA_COUNTRY_CODE));

            List<AddressCountry> countriesList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(AddressCountry.class, "e").column("e"));
            for (AddressCountry country : countriesList)
            {
                OksmType type = oksmMap.get(country.getId().toString());
                if (null != type) countryMap.put(type.getID(), country);
            }
        }

        List<FefuNsiIds> identityCardIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(IdentityCard.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : identityCardIdsList)
        {
            IdentityCardType icType = NsiReactorUtils.NSI_OBJECT_FACTORY.createIdentityCardType();
            icType.setID(nsiId.getGuid());
            identityCardsMap.put(nsiId.getGuid(), icType);
            identityCardsMap.put(String.valueOf(nsiId.getEntityId()), icType);
        }

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou").column(property(EducationOrgUnit.id().fromAlias("eou")))
                .where(eq(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind().code().fromAlias("eou")), value(EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_)));

        List<String> studentPersonGuidsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column(property(FefuNsiIds.guid().fromAlias("e")))
                        .joinEntity("e", DQLJoinType.inner, Student.class, "s", eq(property(Student.person().id().fromAlias("s")), property(FefuNsiIds.entityId().fromAlias("e"))))
                        .where(notIn(property(Student.educationOrgUnit().fromAlias("s")), subBuilder.buildQuery()))
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(Person.ENTITY_NAME)))
                        .where(ne(property(Student.archival().fromAlias("s")), value(Boolean.TRUE))));

        List<String> entrantPersonGuidsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column(property(FefuNsiIds.guid().fromAlias("e")))
                        .joinEntity("e", DQLJoinType.inner, Entrant.class, "s", eq(property(Entrant.person().id().fromAlias("s")), property(FefuNsiIds.entityId().fromAlias("e"))))
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(Person.ENTITY_NAME)))
                        .where(ne(property(Entrant.archival().fromAlias("s")), value(Boolean.TRUE))));

        nonProcessableIncomingPersonGuids.clear();
        nonProcessableIncomingPersonGuids.addAll(studentPersonGuidsList);
        nonProcessableIncomingPersonGuids.addAll(entrantPersonGuidsList);
    }

    @Override
    public Person getPossibleDuplicate(HumanType nsiEntity, Map<String, FefuNsiIds> nsiIdsMap, Map<String, Person> similarEntityMap)
    {
        if (null == nsiEntity.getID()) return null;

        // Проверяем, есть ли в базе аналогичные переданной сущности
        Person possibleDuplicate = null;

        if (nsiIdsMap.containsKey(nsiEntity.getID()))
        {
            possibleDuplicate = similarEntityMap.get(String.valueOf(nsiIdsMap.get(nsiEntity.getID()).getEntityId()));
            if (null == possibleDuplicate) possibleDuplicate = similarEntityMap.get(nsiEntity.getID());
        }

        return possibleDuplicate;
    }

    @Override
    public boolean isAnythingChanged(HumanType nsiEntity, Person entity, Map<String, Person> similarEntityMap)
    {
        if (null == nsiEntity.getID()) return false;

        if (null != entity)
        {
            /*if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanID(), String.valueOf(entity.getId().toString()), isNsiMasterSystem()))
                return true;*/
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanLastNameShort(), entity.getIdentityCard().getLastName().substring(0, 1).toUpperCase(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanLastName(), String.valueOf(entity.getIdentityCard().getLastName()), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanFirstName(), entity.getIdentityCard().getFirstName(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanMiddleName(), entity.getIdentityCard().getMiddleName(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanBirthdate(), NsiDatagramUtil.formatDate(entity.getIdentityCard().getBirthDate()), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanBirthPlace(), entity.getIdentityCard().getBirthPlace(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanINN(), entity.getInnNumber(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanPFR(), entity.getSnilsNumber(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanSex(), SexCodes.MALE.equals(entity.getIdentityCard().getSex().getCode()) ? "Мужской" : "Женский", isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanBasicEmail(), entity.getEmail(), isNsiMasterSystem()))
                return true;

            OksmType oksmType = oksmMap.get(entity.getIdentityCard().getCitizenship().getId().toString());
            if (null != oksmType && (null == nsiEntity.getHumanCitizenship() || !oksmType.getID().equals(nsiEntity.getHumanCitizenship().getOksm().getID())))
                return true;
        }

        return false;
    }

    public boolean isAnythingChanged(IdentityCardType nsiEntity, IdentityCard entity)
    {
        if (null == nsiEntity.getID()) return false;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getIdentityCardNumber(), entity.getNumber(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getIdentityCardSeries(), entity.getSeria(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getIdentityCardWhoGive(), entity.getIssuancePlace(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getIdentityCardDepartmentCode(), entity.getIssuanceCode(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getIdentityCardDateGive(), NsiDatagramUtil.formatDate(entity.getIssuanceDate()), isNsiMasterSystem()))
                return true;

            String regPeriod = DateFormatter.DEFAULT_DATE_FORMATTER.format(entity.getRegistrationPeriodFrom());
            if (regPeriod.length() > 0 && null != entity.getRegistrationPeriodTo())
                regPeriod = regPeriod + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(entity.getRegistrationPeriodTo());

            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getIdentityCardDayOfEntry(), regPeriod.length() > 0 ? regPeriod : null, isNsiMasterSystem()))
                return true;

            IdentityCardKindType icardKindType = identityCardTypesMap.get(entity.getCardType().getId().toString());
            if (null != icardKindType && (null == nsiEntity.getIdentityCardKindID() || !icardKindType.getID().equals(nsiEntity.getIdentityCardKindID().getIdentityCardKind().getID())))
                return true;
        }

        return false;
    }

    @Override
    public HumanType createEntity(Person entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        HumanType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanType();
        nsiEntity.setID(getGUID(entity, nsiIdsMap));
        //nsiEntity.setHumanID(entity.getId().toString());
        nsiEntity.setHumanLastNameShort(entity.getIdentityCard().getLastName().substring(0, 1).toUpperCase());
        nsiEntity.setHumanLastName(entity.getIdentityCard().getLastName());
        nsiEntity.setHumanFirstName(entity.getIdentityCard().getFirstName());
        nsiEntity.setHumanMiddleName(entity.getIdentityCard().getMiddleName());
        nsiEntity.setHumanBirthdate(NsiDatagramUtil.formatDate(entity.getIdentityCard().getBirthDate()));
        nsiEntity.setHumanBirthPlace(entity.getIdentityCard().getBirthPlace());
        nsiEntity.setHumanINN(entity.getInnNumber());
        nsiEntity.setHumanPFR(entity.getSnilsNumber());
        nsiEntity.setHumanSex(SexCodes.MALE.equals(entity.getIdentityCard().getSex().getCode()) ? "Мужской" : "Женский");
        nsiEntity.setHumanBasicEmail(entity.getEmail());

        OksmType oksmType = oksmMap.get(entity.getIdentityCard().getCitizenship().getId().toString());
        if (null != oksmType)
        {
            HumanType.HumanCitizenship citizenship = new HumanType.HumanCitizenship();
            citizenship.setOksm(oksmType);
            nsiEntity.setHumanCitizenship(citizenship);
        }

        return nsiEntity;
    }

    public IdentityCardType createEntity(IdentityCard entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        IdentityCardType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createIdentityCardType();
        String guid = null != identityCardIdsMap.get(entity.getId().toString()) ? identityCardIdsMap.get(entity.getId().toString()).getGuid() : null;
        if (null == guid) guid = NsiReactorUtils.generateGUID(entity);

        nsiEntity.setID(guid);
        nsiEntity.setIdentityCardNumber(entity.getNumber());
        nsiEntity.setIdentityCardSeries(entity.getSeria());
        nsiEntity.setIdentityCardWhoGive(entity.getIssuancePlace());
        nsiEntity.setIdentityCardDepartmentCode(entity.getIssuanceCode());
        nsiEntity.setIdentityCardDateGive(NsiDatagramUtil.formatDate(entity.getIssuanceDate()));

        String regPeriod = DateFormatter.DEFAULT_DATE_FORMATTER.format(entity.getRegistrationPeriodFrom());
        if (regPeriod.length() > 0 && null != entity.getRegistrationPeriodTo())
            regPeriod = regPeriod + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(entity.getRegistrationPeriodTo());
        regPeriod = StringUtils.trimToNull(regPeriod);
        nsiEntity.setIdentityCardDayOfEntry(regPeriod);

        IdentityCardKindType icardKindType = identityCardTypesMap.get(entity.getCardType().getId().toString());
        if (null != icardKindType)
        {
            IdentityCardType.IdentityCardKindID cardKind = new IdentityCardType.IdentityCardKindID();
            cardKind.setIdentityCardKind(icardKindType);
            nsiEntity.setIdentityCardKindID(cardKind);
        }

        String personGuid = getGUID(entity.getPerson(), nsiIdsMap);
        if (null != personGuid)
        {
            IdentityCardType.HumanID humanId = new IdentityCardType.HumanID();
            HumanType human = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanType();
            human.setID(personGuid);
            humanId.setHuman(human);
            nsiEntity.setHumanID(humanId);
        }

        return nsiEntity;
    }

    @Override
    public Person createEntity(HumanType nsiEntity, Map<String, Person> similarEntityMap)
    {
        return null;
        //TODO: Как создавать у нас персон по данным НСИ?
    }

    @Override
    public Person updatePossibleDuplicateFields(HumanType nsiEntity, Person entity, Map<String, Person> similarEntityMap)
    {
        return null;
    }

    public Person updateEntityFields(Person entity, HumanType nsiEntity, IdentityCardType nsiICard)
    {
        boolean needNewIdentityCard = false;
        IdentityCard icard = entity.getIdentityCard();

        if (null == entity.getContactData()) entity.setContactData(new PersonContactData());

        if (!nonProcessableIncomingPersonGuids.contains(nsiEntity.getID()) && null != icard)
        {
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanLastName(), icard.getLastName(), true)
                    || NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanFirstName(), icard.getFirstName(), true)
                    || NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanMiddleName(), icard.getMiddleName(), true))
                needNewIdentityCard = true;
        }

        needNewIdentityCard = needNewIdentityCard || (null != nsiICard && (NsiReactorUtils.isShouldBeUpdated(nsiICard.getIdentityCardNumber(), icard.getNumber(), true)
                || NsiReactorUtils.isShouldBeUpdated(nsiICard.getIdentityCardSeries(), icard.getSeria(), true)));

        if (null == icard) needNewIdentityCard = true;
        else if (null == icard.getSeria() && null == icard.getNumber() && null == icard.getIssuanceDate() && IdentityCardTypeCodes.DRUGOY_DOKUMENT.equals(icard.getCardType().getCode()))
            needNewIdentityCard = false;

        if (needNewIdentityCard)
        {
            icard = new IdentityCard();

            if (null != entity.getIdentityCard())
            {
                icard.setSex(entity.getIdentityCard().getSex());
                icard.setLastName(entity.getIdentityCard().getLastName());
                icard.setFirstName(entity.getIdentityCard().getFirstName());
                icard.setMiddleName(entity.getIdentityCard().getMiddleName());
                icard.setBirthDate(entity.getIdentityCard().getBirthDate());
                icard.setBirthPlace(entity.getIdentityCard().getBirthPlace());
                icard.setCitizenship(entity.getIdentityCard().getCitizenship());
                icard.setCardType(entity.getIdentityCard().getCardType());
                icard.setNationality(entity.getIdentityCard().getNationality());

                if (null != entity.getIdentityCard().getAddress())
                {
                    icard.setAddress(AddressBaseUtils.getSameAddress(entity.getIdentityCard().getAddress()));
                }

                if (null != entity.getIdentityCard().getPhoto())
                {
                    DatabaseFile photo = new DatabaseFile();
                    photo.update(entity.getIdentityCard().getPhoto());
                    icard.setPhoto(photo);
                }
            }

            entity.setIdentityCard(icard);
        }

        if (!nonProcessableIncomingPersonGuids.contains(nsiEntity.getID()))
        {
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanLastName(), icard.getLastName(), true))
                icard.setLastName(nsiEntity.getHumanLastName());
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanFirstName(), icard.getFirstName(), true))
                icard.setFirstName(nsiEntity.getHumanFirstName());
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanMiddleName(), icard.getMiddleName(), true))
                icard.setMiddleName(nsiEntity.getHumanMiddleName());
        }

        if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanBirthdate(), NsiDatagramUtil.formatDate(icard.getBirthDate()), true))
            icard.setBirthDate(NsiDatagramUtil.parseDate(nsiEntity.getHumanBirthdate()));
        if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanBirthPlace(), icard.getBirthPlace(), true))
            icard.setBirthPlace(nsiEntity.getHumanBirthPlace());
        if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanINN(), entity.getInnNumber(), true))
            entity.setInnNumber(nsiEntity.getHumanINN());
        if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanPFR(), entity.getSnilsNumber(), true))
            entity.setSnilsNumber(nsiEntity.getHumanPFR());
        if (null == icard.getSex() || NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanSex(), SexCodes.MALE.equals(icard.getSex().getCode()) ? "Мужской" : "Женский", true))
        {
            Sex sex = null != nsiEntity.getHumanSex() ? sexMap.get(nsiEntity.getHumanSex().trim().toUpperCase()) : null;
            if (null == sex && null != icard.getSex()) sex = icard.getSex();
            if (null == sex) sex = sexMap.get(null);
            if (null != sex) icard.setSex(sex);
        }

        if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getHumanBasicEmail(), entity.getEmail(), true) && null != entity.getContactData())
            entity.getContactData().setEmail(nsiEntity.getHumanBasicEmail());

        if (null != nsiEntity.getHumanCitizenship() && null != nsiEntity.getHumanCitizenship().getOksm())
        {
            AddressCountry country = null != nsiEntity.getHumanCitizenship() ? countryMap.get(nsiEntity.getHumanCitizenship().getOksm().getID()) : null;
            if (null == country) country = countryMap.get(null);
            if (null != country) icard.setCitizenship(country);
        }

        if (null != nsiICard && !NsiReactorUtils.isIdentityCardIgnore())
        {
            if (NsiReactorUtils.isShouldBeUpdated(nsiICard.getIdentityCardNumber(), icard.getNumber(), true))
                icard.setNumber(nsiICard.getIdentityCardNumber());
            if (NsiReactorUtils.isShouldBeUpdated(nsiICard.getIdentityCardSeries(), icard.getSeria(), true))
                icard.setSeria(nsiICard.getIdentityCardSeries());
            if (NsiReactorUtils.isShouldBeUpdated(nsiICard.getIdentityCardWhoGive(), icard.getIssuancePlace(), true))
                icard.setIssuancePlace(nsiICard.getIdentityCardWhoGive());
            if (NsiReactorUtils.isShouldBeUpdated(nsiICard.getIdentityCardDepartmentCode(), icard.getIssuanceCode(), true))
                icard.setIssuanceCode(nsiICard.getIdentityCardDepartmentCode());
            if (NsiReactorUtils.isShouldBeUpdated(nsiICard.getIdentityCardDateGive(), NsiDatagramUtil.formatDate(icard.getIssuanceDate()), true))
                icard.setIssuanceDate(NsiDatagramUtil.parseDate(nsiICard.getIdentityCardDateGive()));

            /*String regPeriod = DateFormatter.DEFAULT_DATE_FORMATTER.format(icard.getRegistrationPeriodFrom());
            if (regPeriod.length() > 0 && null != icard.getRegistrationPeriodTo())
                regPeriod = regPeriod + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(entity.getRegistrationPeriodTo());*/

            /*if (NsiReactorUtils.isShouldBeUpdated(nsiICard.getIdentityCardDayOfEntry(), regPeriod.length() > 0 ? regPeriod : null, isNsiMasterSystem()))
                return true;*/

            if (null != nsiICard.getIdentityCardKindID() && !NsiReactorUtils.isIdentityCardIgnore())
            {
                org.tandemframework.shared.person.catalog.entity.IdentityCardType cardType = icardTypesMap.get(nsiICard.getIdentityCardKindID().getIdentityCardKind().getID());
                if (null == cardType) cardType = icardTypesMap.get(null);
                if (null != cardType) icard.setCardType(cardType);
            }
        }

        if (null == entity.getIdentityCard().getLastName() || 0 == entity.getIdentityCard().getLastName().trim().length())
            entity.getIdentityCard().setLastName("-");
        if (null == entity.getIdentityCard().getFirstName() || 0 == entity.getIdentityCard().getFirstName().trim().length())
            entity.getIdentityCard().setFirstName("-");
        if (null == entity.getIdentityCard().getCitizenship())
            entity.getIdentityCard().setCitizenship(countryMap.get(null));
        if (null == entity.getIdentityCard().getSex()) entity.getIdentityCard().setSex(sexMap.get(null));
        if (null == entity.getIdentityCard().getCardType())
            entity.getIdentityCard().setCardType(icardTypesMap.get(null));

        return entity;
    }

    @Override
    public HumanType updateNsiEntityFields(HumanType nsiEntity, Person entity)
    {
        //nsiEntity.setHumanID(entity.getId().toString());
        nsiEntity.setHumanLastNameShort(entity.getIdentityCard().getLastName().substring(0, 1).toUpperCase());
        nsiEntity.setHumanLastName(entity.getIdentityCard().getLastName());
        nsiEntity.setHumanFirstName(entity.getIdentityCard().getFirstName());
        nsiEntity.setHumanMiddleName(entity.getIdentityCard().getMiddleName());
        nsiEntity.setHumanBirthdate(NsiDatagramUtil.formatDate(entity.getIdentityCard().getBirthDate()));
        nsiEntity.setHumanBirthPlace(entity.getIdentityCard().getBirthPlace());
        nsiEntity.setHumanINN(entity.getInnNumber());
        nsiEntity.setHumanPFR(entity.getSnilsNumber());
        nsiEntity.setHumanSex(SexCodes.MALE.equals(entity.getIdentityCard().getSex().getCode()) ? "Мужской" : "Женский");
        nsiEntity.setHumanBasicEmail(entity.getEmail());

        OksmType oksmType = oksmMap.get(entity.getIdentityCard().getCitizenship().getId().toString());
        if (null != oksmType)
        {
            HumanType.HumanCitizenship citizenship = new HumanType.HumanCitizenship();
            citizenship.setOksm(oksmType);
            nsiEntity.setHumanCitizenship(citizenship);
        }

        return nsiEntity;
    }

    public IdentityCardType updateNsiEntityFields(IdentityCardType nsiEntity, IdentityCard entity)
    {
        if (null == entity.getNumber()) return null;

        nsiEntity.setIdentityCardNumber(entity.getNumber());
        nsiEntity.setIdentityCardSeries(entity.getSeria());
        nsiEntity.setIdentityCardWhoGive(entity.getIssuancePlace());
        nsiEntity.setIdentityCardDepartmentCode(entity.getIssuanceCode());
        nsiEntity.setIdentityCardDateGive(NsiDatagramUtil.formatDate(entity.getIssuanceDate()));

        String regPeriod = DateFormatter.DEFAULT_DATE_FORMATTER.format(entity.getRegistrationPeriodFrom());
        if (regPeriod.length() > 0 && null != entity.getRegistrationPeriodTo())
            regPeriod = regPeriod + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(entity.getRegistrationPeriodTo());
        regPeriod = StringUtils.trimToNull(regPeriod);
        nsiEntity.setIdentityCardDayOfEntry(regPeriod);

        IdentityCardKindType icardKindType = identityCardTypesMap.get(entity.getCardType().getId().toString());
        if (null != icardKindType)
        {
            IdentityCardType.IdentityCardKindID cardKind = new IdentityCardType.IdentityCardKindID();
            cardKind.setIdentityCardKind(icardKindType);
            nsiEntity.setIdentityCardKindID(cardKind);
        }

        if (null != nsiEntity.getHumanID())
        {
            IdentityCardType.HumanID humanId = new IdentityCardType.HumanID();
            HumanType human = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanType();
            human.setID(nsiEntity.getHumanID().getHuman().getID());
            humanId.setHuman(human);
            nsiEntity.setHumanID(humanId);
        }

        return nsiEntity;
    }

    @Override
    public void synchronizeFullCatalogWithNsi()
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG HAVE STARTED ==========");

        // Формируем мап сущностей ОБ, где ключ - идентификатор сущности ОБ, либо Название (ITitled), или пользовательский код (IActiveCatalogItem)
        final Map<String, Person> entityMap = getEntityMap(getEntityClass());
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");
        // Формируем мап идентификаторов НСИ из числа сохраненных в ОБ, где ключ - идентификатор ОБ, либо GUID
        final Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMap(EntityRuntime.getMeta(getEntityClass()).getName());
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        // Если для синхронизации нужны дополнительные данные, то инициализируем их
        prepareRelatedObjectsMap();

        List<Object> nsiEntityToUpdate = new ArrayList<>(); // Сюда сваливаем все сущности НСИ, которые требуется обновить данными из ОБ
        List<Object> nsiICardsToUpdate = new ArrayList<>();
        List<Object> nsiICardsToCreate = new ArrayList<>();

        // Если разрешено добавление новых элементов в НСИ
        if (isAddItemsToNSIAllowed())
        {
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start adding new " + entityMap.values().size() + " elements to NSI for " + getEntityClass().getSimpleName() + " catalog ----------");

            List<Object> nsiInsertEntityList = new ArrayList<>();
            Set<Long> alreadyPreparedForSendingToNSIEntitySet = new HashSet<>();

            // Бежим по всем сущностям ОБ и выискиваем те, которых нет в НСИ
            for (Person entity : entityMap.values())
            {
                FefuNsiIds nsiId = nsiIdsMap.get(entity.getId().toString()); // Вытаскиваем GUID ОБ, если есть такой
                if (null != nsiId && nsiId.isDeletedFromNsi())
                    continue; // Проверяем, чтобы соответствующий элемент не был удалён ранее из НСИ

                // Если ранее ещё не обрабатывали данную сущность и не отправляли на правки в НСИ
                if (!alreadyPreparedForSendingToNSIEntitySet.contains(entity.getId()))
                {
                    HumanType human = createEntity(entity, nsiIdsMap);
                    nsiInsertEntityList.add(human);
                    alreadyPreparedForSendingToNSIEntitySet.add(entity.getId());
                    FefuNsiIds newIds = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(entity, nsiId, new FefuNsiIdWrapper(null, getEntityName()));
                    CoreCollectionUtils.Pair<Person, FefuNsiIds> pair = new CoreCollectionUtils.Pair<>(entity, newIds);
                    NsiObjectsHolder.getPersonIdToEntityMap().put(entity.getId().toString(), pair);
                    if (null != newIds && null != newIds.getGuid())
                        NsiObjectsHolder.getPersonIdToEntityMap().put(newIds.getGuid(), pair);

                    IdentityCardType nsiCardType = createEntity(entity.getIdentityCard(), nsiIdsMap);
                    if (null != nsiCardType)
                    {
                        nsiICardsToCreate.add(nsiCardType);
                        FefuNsiIds icId = identityCardIdsMap.get(nsiCardType.getID());
                        IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(entity.getIdentityCard(), icId, new FefuNsiIdWrapper(nsiCardType.getID(), IdentityCard.ENTITY_NAME));
                    }
                }
            }
            // Собственно, производим отправку свежедобавляемых элементов в НСИ
            NsiReactorUtils.executeNSIAction(nsiInsertEntityList, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT, true);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Adding new " + entityMap.values().size() + " elements to NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
        }

        // Если НСИ не является мастер-системой по данному справочнику и есть некие отличия элементов с ОБ, то производим их обновление в НСИ
        if (!isNsiMasterSystem() && nsiEntityToUpdate.size() > 0)
        {
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start update " + nsiEntityToUpdate.size() + " elements at NSI for " + getEntityClass().getSimpleName() + " catalog ----------");
            NsiReactorUtils.executeNSIAction(nsiEntityToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE, true);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Update " + nsiEntityToUpdate.size() + " elements at NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
        }

        // Если НСИ не является мастер-системой по данному справочнику и есть некие отличия элементов с ОБ, то производим их обновление в НСИ
        if (!isNsiMasterSystem() && nsiICardsToCreate.size() > 0)
        {
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start insert new " + nsiICardsToCreate.size() + " elements at NSI for " + IdentityCard.ENTITY_NAME + " catalog ----------");
            NsiReactorUtils.executeNSIAction(nsiICardsToCreate, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT, true);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Insert new " + nsiICardsToCreate.size() + " elements at NSI for " + IdentityCard.ENTITY_NAME + " catalog was finished ----------");
        }

        // Если НСИ не является мастер-системой по данному справочнику и есть некие отличия элементов с ОБ, то производим их обновление в НСИ
        if (!isNsiMasterSystem() && nsiICardsToUpdate.size() > 0)
        {
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start update " + nsiICardsToUpdate.size() + " elements at NSI for " + IdentityCard.ENTITY_NAME + " catalog ----------");
            NsiReactorUtils.executeNSIAction(nsiICardsToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE, true);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Update " + nsiICardsToUpdate.size() + " elements at NSI for " + IdentityCard.ENTITY_NAME + " catalog was finished ----------");
        }

        // Изменяем время последней синхронизации для справочника в целом
        IFefuNsiSyncDAO.instance.get().updateNsiCatalogSyncTime(getNSIEntityClass().getSimpleName());
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG WAS FINISHED ==========");
    }

    @Override
    public void insertOrUpdateNewItemsAtNSI(Set<Long> entityToAddIdsSet)
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== INSERT/UPDATE ITEMS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG HAVE STARTED ==========");

        final Map<String, Person> personMap = new HashMap<>();
        final Map<String, IdentityCard> icardsMap = new HashMap<>();
        final Map<String, FefuNsiIds> nsiIdsMap = fillMaps(entityToAddIdsSet, personMap, icardsMap);

        // Если для синхронизации нужны дополнительные данные, то инициализируем их
        prepareRelatedObjectsMap();

        BatchUtils.execute(personMap.values(), 200, new BatchUtils.Action<Person>()
        {
            @Override
            public void execute(final Collection<Person> entityPortion)
            {
                List<Object> nsiEntityToUpdate = new ArrayList<>();
                List<Object> nsiEntityToCreate = new ArrayList<>();
                List<Object> nsiICardsToUpdate = new ArrayList<>();
                List<Object> nsiICardsToCreate = new ArrayList<>();
                List<CoreCollectionUtils.Pair<IEntity, FefuNsiIds>> updatedEntityPairsList = new ArrayList();

                for (Person person : entityPortion)
                {
                    IdentityCard icard = person.getIdentityCard();
                    FefuNsiIds personIds = nsiIdsMap.get(person.getId().toString());
                    FefuNsiIds icardIds = nsiIdsMap.get(icard.getId().toString());
                    HumanType humanType = createEntity(person, nsiIdsMap);
                    IdentityCardType cardType = createEntity(icard, nsiIdsMap);
                    if (null != personIds) nsiEntityToUpdate.add(humanType);
                    else nsiEntityToCreate.add(humanType);
                    if (null != icardIds) nsiICardsToUpdate.add(cardType);
                    else nsiICardsToCreate.add(cardType);

                    updatedEntityPairsList.add(new CoreCollectionUtils.Pair(person, personIds));
                    updatedEntityPairsList.add(new CoreCollectionUtils.Pair(icard, icardIds));
                }

                if (!nsiEntityToCreate.isEmpty())
                {
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start insert elements at NSI for " + getEntityClass().getSimpleName() + " catalog ----------");
                    NsiReactorUtils.prepareNSIAction(nsiEntityToCreate, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT, true);
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Insert elements at NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
                }

                if (!nsiICardsToCreate.isEmpty())
                {
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start insert elements at NSI for IdentityCard catalog ----------");
                    NsiReactorUtils.prepareNSIAction(nsiICardsToCreate, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT, true);
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Insert elements at NSI for IdentityCard catalog was finished ----------");
                }

                if (!nsiEntityToUpdate.isEmpty())
                {
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start updating elements at NSI for " + getEntityClass().getSimpleName() + " catalog ----------");
                    NsiReactorUtils.prepareNSIAction(nsiEntityToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE, true);
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Updating elements at NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
                }

                if (!nsiICardsToUpdate.isEmpty())
                {
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start updating elements at NSI for IdentityCard catalog ----------");
                    NsiReactorUtils.prepareNSIAction(nsiICardsToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE, true);
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Updating elements at NSI for IdentityCard catalog was finished ----------");
                }

                for (CoreCollectionUtils.Pair<IEntity, FefuNsiIds> pair : updatedEntityPairsList)
                {
                    FefuNsiIds newIds = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(pair.getX(), pair.getY(), new FefuNsiIdWrapper(null, pair.getX() instanceof IdentityCard ? IdentityCard.ENTITY_NAME : getEntityName()));
                    if (pair.getX() instanceof Person)
                    {
                        CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = new CoreCollectionUtils.Pair<>((Person) pair.getX(), newIds);
                        NsiObjectsHolder.getPersonIdToEntityMap().put(personPair.getX().getId().toString(), personPair);
                        if (null != newIds && null != newIds.getGuid())
                            NsiObjectsHolder.getPersonIdToEntityMap().put(newIds.getGuid(), personPair);
                    }
                }
            }
        });

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== INSERT/UPDATE ITEMS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG WAS FINISHED ==========");
    }

    @Override
    public List<Object> insert(List<INsiEntity> itemsToInsert) throws NSIProcessingErrorException
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== INSERT ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB HAVE STARTED ==========");

        Set<String> guidsToInsert = new HashSet<>();
        List<HumanType> humansToInsert = new ArrayList<>();
        Map<String, IdentityCardType> icardsToUpdate = new HashMap<>();
        List<Object> insertedObjects = new ArrayList<>();

        // Сет guid'ов физлиц, которые будут ведущими при дедубликации
        Set<String> mergeMasterPersonGuidSet = new HashSet<>();

        // Мап предпочтительных УЛ для физлиц. Ключ- guid ФЛ, значение - список УЛ, которые надо перебросить на ФЛ.
        Map<String, Set<String>> preferableIdentityCardForPersonMap = new HashMap<>();

        // Мап подмены GUID'ов персон. Если мёрджим персон студента и сотрудника, причём за основу берем сотрудника, то нужно взять саму персону от студента, а GUID подставить от сотрудника
        Map<String, String> personGuidChangeMap = new HashMap<>();
        Map<String, String> personGuidChangeRevMap = new HashMap<>();

        if (null != itemsToInsert)
        {
            for (Object nsiEntity : itemsToInsert)
            {
                if (nsiEntity instanceof HumanType)
                {
                    HumanType human = (HumanType) nsiEntity;
                    if (null == human.getIsNotConsistent() || 1 != human.getIsNotConsistent())
                    {
                        String guid = getGUID(human);
                        if (null != guid) guidsToInsert.add(guid);
                        humansToInsert.add(human);

                        if (null != human.getIdentityCard())
                        {
                            icardsToUpdate.put(human.getID(), human.getIdentityCard().getIdentityCard());
                        }

                        String mergeDuplicates = null != human.getMergeDublicates() ? human.getMergeDublicates() : human.getOtherAttributes().get(new QName("mergeDuplicates"));

                        if (null != mergeDuplicates)
                        {
                            mergeMasterPersonGuidSet.add(guid);
                            String[] duplicates = mergeDuplicates.split(";");
                            for (String duplicate : duplicates)
                            {
                                duplicate = duplicate.trim();
                                if(nonProcessableIncomingPersonGuids.contains(duplicate) && !nonProcessableIncomingPersonGuids.contains(guid))
                                {
                                    mergeMasterPersonGuidSet.remove(guid);
                                    mergeMasterPersonGuidSet.add(duplicate);
                                    personGuidChangeMap.put(duplicate, guid);
                                    personGuidChangeRevMap.put(guid, duplicate);
                                    guidsToInsert.add(guid);
                                    guid = duplicate;
                                }
                                else guidsToInsert.add(duplicate);
                            }
                        }
                    }
                }
                if (nsiEntity instanceof IdentityCardType)
                {
                    IdentityCardType icard = (IdentityCardType) nsiEntity;

                    if (null != icard.getHumanID())
                    {
                        HumanType ihuman = icard.getHumanID().getHuman();
                        String guid = getGUID(ihuman);

                        if (null != ihuman && (null == ihuman.getIsNotConsistent() || 1 != ihuman.getIsNotConsistent()))
                        {
                            if (mergeMasterPersonGuidSet.contains(guid))
                            {
                                Set<String> preferableICList = preferableIdentityCardForPersonMap.get(guid);
                                if (null == preferableICList) preferableICList = new HashSet<>();
                                preferableICList.add(icard.getID());
                                preferableIdentityCardForPersonMap.put(guid, preferableICList);
                            }

                            if (null != guid) guidsToInsert.add(guid);
                            humansToInsert.add(icard.getHumanID().getHuman());
                        }
                        icardsToUpdate.put(guid, icard);
                    }
                }
            }
        }

        if (guidsToInsert.isEmpty())
            throw new NSIProcessingErrorException("There is no any entity guid to insert in datagram.");

        prepareRelatedObjectsMap();

        Map<String, Person> entityMap = guidsToInsert.isEmpty() ? getNsiSynchrinizedEntityMap(getEntityClass(), null, false) : getNsiSynchrinizedEntityMap(getEntityClass(), guidsToInsert, false);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        // Формируем мап идентификаторов НСИ из числа сохраненных в ОБ, где ключ - идентификатор ОБ, либо GUID
        /*Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMapByGuids(EntityRuntime.getMeta(getEntityClass()).getName(), guidsToInsert);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");*/

        Set<Long> entityIds = new HashSet<>();
        for (Person person : entityMap.values())
        {
            entityIds.add(person.getId());
            entityIds.add(person.getIdentityCard().getId());
        }

        Map<String, Person> personMap = new HashMap<>();
        Map<String, IdentityCard> icardsMap = new HashMap<>();
        Map<String, FefuNsiIds> nsiIdsMap = fillMaps(entityIds, personMap, icardsMap);

        Set<String> alreadyProcessedEntitySet = new HashSet<>();
        Map<String, Person> studentPersonMap = new HashMap<>();

        for (HumanType item : humansToInsert)
        {
            String guid = getGUID(item);
            String srcGuid = getGUID(item);
            if (null != personGuidChangeRevMap.get(guid))
                guid = personGuidChangeRevMap.get(guid);

            if (null != guid)
            {
                if (!alreadyProcessedEntitySet.contains(guid))
                {
                    String newGuid = guid;
                    alreadyProcessedEntitySet.add(guid);
                    List<Person> personDuplicates = new ArrayList<>();

                    HumanType replacedHuman = null != srcGuid ? getCatalogElementRetrieveRequestObject(srcGuid) : null;
                    Person entity = getPossibleDuplicate(null != replacedHuman ? replacedHuman : item, nsiIdsMap, entityMap);
                    if (null != entity) personDuplicates.add(entity);

                    String mergeDuplicates = null != item.getMergeDublicates() ? item.getMergeDublicates() : item.getOtherAttributes().get(new QName("mergeDuplicates"));
                    if (null != mergeDuplicates)
                    {
                        String[] duplicates = mergeDuplicates.split(";");
                        for (String duplicateGuid : duplicates)
                        {
                            duplicateGuid = duplicateGuid.trim();
                            Person duplicatePerson = entityMap.get(duplicateGuid);

                            if (null == entity && null != duplicatePerson)
                            {
                                entity = duplicatePerson;
                                newGuid = duplicateGuid;
                            }

                            if (null != duplicatePerson && !personDuplicates.contains(duplicatePerson))
                                personDuplicates.add(duplicatePerson);

                            if (nonProcessableIncomingPersonGuids.contains(duplicateGuid) && null != duplicatePerson && !studentPersonMap.containsKey(newGuid))
                                studentPersonMap.put(newGuid, duplicatePerson);
                        }

                        if (null != personGuidChangeRevMap.get(guid)) newGuid = personGuidChangeRevMap.get(guid);
                    }

                    insertedObjects.add(item);

                    // Поднимаем список всех УЛ, которые в процессе дедубликации должны быть перевешаны на итоговое ФЛ
                    Map<FefuNsiIds, IdentityCard> prefICMap = new HashMap<>();
                    Set<String> preferableICGuids = preferableIdentityCardForPersonMap.get(guid);
                    if (null != preferableICGuids)
                    {
                        for (String prefICGuid : preferableICGuids)
                        {
                            FefuNsiIds icIds = nsiIdsMap.get(prefICGuid);
                            if (null != icIds)
                            {
                                IdentityCard prefICard = icardsMap.get(String.valueOf(icIds.getEntityId()));
                                prefICMap.put(icIds, prefICard);
                            }
                        }
                    }

                    IdentityCardType nsiICard = icardsToUpdate.get(guid);
                    if (null != entity)
                    {
                        personDuplicates.remove(entity);
                        FefuNsiIds entityNsiId = nsiIdsMap.get(entity.getId().toString());
                        if (null != entityNsiId && nonProcessableIncomingPersonGuids.contains(entityNsiId.getGuid()))
                        {
                            nonProcessableIncomingPersonGuids.add(item.getID());
                            if(NsiReactorUtils.isIdentityCardIgnore()) continue;
                        }
                        Person updatedEntity = updateEntityFields(entity, item, nsiICard);
                        FefuNsiIds nsiId = nsiIdsMap.get(null == newGuid ? guid : newGuid);
                        String icardGuid = null != nsiICard ? nsiICard.getID() : (null != entity.getIdentityCard() && null != entity.getIdentityCard().getId() ? NsiReactorUtils.generateGUID(entity.getIdentityCard().getId()) : null);
                        FefuNsiIds icardNsiId = nsiIdsMap.get(icardGuid);
                        Person studentPerson = studentPersonMap.get(null == newGuid ? guid : newGuid);
                        FefuNsiIds newIds = IFefuNsiSyncDAO.instance.get().saveOrUpdateCrazyPersonWithNsiId(updatedEntity, nsiId, guid, getEntityName(), icardNsiId, icardGuid, personDuplicates, prefICMap, item.getHumanLogin(), studentPerson);
                        NsiObjectsHolder.removePersonDuplicates(personDuplicates);
                        NsiObjectsHolder.addPerson(entity, newIds);
                    } else
                    {
                        entity = new Person();
                        FefuNsiIds nsiId = nsiIdsMap.get(null == newGuid ? guid : newGuid);
                        Person updatedEntity = updateEntityFields(entity, item, nsiICard);
                        String icardGuid = null != nsiICard ? nsiICard.getID() : (null != entity.getIdentityCard() && null != entity.getIdentityCard().getId() ? NsiReactorUtils.generateGUID(entity.getIdentityCard().getId()) : null);
                        FefuNsiIds icardNsiId = nsiIdsMap.get(icardGuid);
                        Person studentPerson = studentPersonMap.get(null == newGuid ? guid : newGuid);
                        FefuNsiIds newIds = IFefuNsiSyncDAO.instance.get().saveOrUpdateCrazyPersonWithNsiId(updatedEntity, nsiId, guid, getEntityName(), icardNsiId, icardGuid, personDuplicates, prefICMap, item.getHumanLogin(), studentPerson);
                        NsiObjectsHolder.removePersonDuplicates(personDuplicates);
                        NsiObjectsHolder.addPerson(entity, newIds);
                    }

                    insertedObjects.add(nsiICard);
                }
            } else
            {
                throw new NSIProcessingErrorException("There is no GUID for one, or more entity in datagram.");
            }
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== INSERT ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
        return insertedObjects;
    }

    @Override
    public List<Object> update(List<INsiEntity> itemsToUpdate) throws NSIProcessingErrorException
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== UPDATE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB HAVE STARTED ==========");

        Set<String> guidsToUpdate = new HashSet<>();
        List<HumanType> humansToUpdate = new ArrayList<>();
        Map<String, IdentityCardType> icardsToUpdate = new HashMap<>();
        List<Object> updatedObjects = new ArrayList<>();

        // Сет guid'ов физлиц, которые будут ведущими при дедубликации
        Set<String> mergeMasterPersonGuidSet = new HashSet<>();

        // Мап предпочтительных УЛ для физлиц. Ключ- guid ФЛ, значение - список УЛ, которые надо перебросить на ФЛ.
        Map<String, Set<String>> preferableIdentityCardForPersonMap = new HashMap<>();

        // Мап подмены GUID'ов персон. Если мёрджим персон студента и сотрудника, причём за основу берем сотрудника, то нужно взять саму персону от студента, а GUID подставить от сотрудника
        Map<String, String> personGuidChangeMap = new HashMap<>();
        Map<String, String> personGuidChangeRevMap = new HashMap<>();

        if (null != itemsToUpdate)
        {
            for (Object nsiEntity : itemsToUpdate)
            {
                if (nsiEntity instanceof HumanType)
                {
                    HumanType human = (HumanType) nsiEntity;
                    if (null == human.getIsNotConsistent() || 1 != human.getIsNotConsistent())
                    {
                        String guid = getGUID(human);
                        if (null != guid) guidsToUpdate.add(guid);
                        humansToUpdate.add(human);

                        if (null != human.getIdentityCard())
                        {
                            icardsToUpdate.put(human.getID(), human.getIdentityCard().getIdentityCard());
                        }

                        String mergeDuplicates = null != human.getMergeDublicates() ? human.getMergeDublicates() : human.getOtherAttributes().get(new QName("mergeDuplicates"));
                        if (null != mergeDuplicates)
                        {
                            mergeMasterPersonGuidSet.add(guid);
                            String[] duplicates = mergeDuplicates.split(";");
                            for (String duplicate : duplicates)
                            {
                                duplicate = duplicate.trim();
                                if(nonProcessableIncomingPersonGuids.contains(duplicate) && !nonProcessableIncomingPersonGuids.contains(guid))
                                {
                                    mergeMasterPersonGuidSet.remove(guid);
                                    mergeMasterPersonGuidSet.add(duplicate);
                                    personGuidChangeMap.put(duplicate, guid);
                                    personGuidChangeRevMap.put(guid, duplicate);
                                    guidsToUpdate.add(guid);
                                    guid = duplicate;
                                }
                                else guidsToUpdate.add(duplicate);
                            }
                        }
                    }
                }
                if (nsiEntity instanceof IdentityCardType)
                {
                    IdentityCardType icard = (IdentityCardType) nsiEntity;

                    if (null != icard.getHumanID())
                    {
                        HumanType ihuman = icard.getHumanID().getHuman();
                        String guid = getGUID(ihuman);

                        if (null != ihuman && (null == ihuman.getIsNotConsistent() || 1 != ihuman.getIsNotConsistent()))
                        {
                            if (mergeMasterPersonGuidSet.contains(guid))
                            {
                                Set<String> preferableICList = preferableIdentityCardForPersonMap.get(guid);
                                if (null == preferableICList) preferableICList = new HashSet<>();
                                preferableICList.add(icard.getID());
                                preferableIdentityCardForPersonMap.put(guid, preferableICList);
                            }

                            if (null != guid) guidsToUpdate.add(guid);
                            humansToUpdate.add(icard.getHumanID().getHuman());
                        }
                        icardsToUpdate.put(guid, icard);
                    }
                }
            }
        }

        if (guidsToUpdate.isEmpty())
            throw new NSIProcessingErrorException("There is no any entity guid to update in datagram.");

        prepareRelatedObjectsMap();

        Map<String, Person> entityMap = guidsToUpdate.isEmpty() ? getNsiSynchrinizedEntityMap(getEntityClass(), null, false) : getNsiSynchrinizedEntityMap(getEntityClass(), guidsToUpdate, false);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        // Формируем мап идентификаторов НСИ из числа сохраненных в ОБ, где ключ - идентификатор ОБ, либо GUID
        /*Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMapByGuids(EntityRuntime.getMeta(getEntityClass()).getName(), guidsToUpdate);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");*/

        Set<Long> entityIds = new HashSet<>();
        for (Person person : entityMap.values())
        {
            entityIds.add(person.getId());
            entityIds.add(person.getIdentityCard().getId());
        }

        Map<String, Person> personMap = new HashMap<>();
        Map<String, IdentityCard> icardsMap = new HashMap<>();
        Map<String, FefuNsiIds> nsiIdsMap = fillMaps(entityIds, personMap, icardsMap);

        Set<String> alreadyProcessedEntitySet = new HashSet<>();
        Map<String, Person> studentPersonMap = new HashMap<>();

        for (HumanType item : humansToUpdate)
        {
            String guid = getGUID(item);
            String srcGuid = getGUID(item);
            if (null != personGuidChangeRevMap.get(guid))
                guid = personGuidChangeRevMap.get(guid);

            if (null != guid)
            {
                if (!alreadyProcessedEntitySet.contains(guid))
                {
                    String newGuid = guid;
                    alreadyProcessedEntitySet.add(guid);
                    List<Person> personDuplicates = new ArrayList<>();
                    HumanType replacedHuman = null != srcGuid ? getCatalogElementRetrieveRequestObject(srcGuid) : null;
                    Person entity = getPossibleDuplicate(null != replacedHuman ? replacedHuman : item, nsiIdsMap, entityMap);
                    if (null != entity) personDuplicates.add(entity);

                    String mergeDuplicates = null != item.getMergeDublicates() ? item.getMergeDublicates() : item.getOtherAttributes().get(new QName("mergeDuplicates"));

                    if (null != mergeDuplicates)
                    {
                        String[] duplicates = mergeDuplicates.split(";");
                        for (String duplicateGuid : duplicates)
                        {
                            duplicateGuid = duplicateGuid.trim();
                            Person duplicatePerson = entityMap.get(duplicateGuid);

                            if (null == entity && null != duplicatePerson)
                            {
                                entity = duplicatePerson;
                                newGuid = duplicateGuid;
                            }

                            if (null != duplicatePerson && !personDuplicates.contains(duplicatePerson))
                                personDuplicates.add(duplicatePerson);

                            if (nonProcessableIncomingPersonGuids.contains(duplicateGuid) && null != duplicatePerson && !studentPersonMap.containsKey(newGuid))
                                studentPersonMap.put(newGuid, duplicatePerson);
                        }

                        if (null != personGuidChangeRevMap.get(guid)) newGuid = personGuidChangeRevMap.get(guid);
                    }

                    updatedObjects.add(item);

                    // Поднимаем список всех УЛ, которые в процессе дедубликации должны быть перевешаны на итоговое ФЛ
                    Map<FefuNsiIds, IdentityCard> prefICMap = new HashMap<>();
                    Set<String> preferableICGuids = preferableIdentityCardForPersonMap.get(guid);
                    if (null != preferableICGuids)
                    {
                        for (String prefICGuid : preferableICGuids)
                        {
                            FefuNsiIds icIds = nsiIdsMap.get(prefICGuid);
                            if (null != icIds)
                            {
                                IdentityCard prefICard = icardsMap.get(String.valueOf(icIds.getEntityId()));
                                prefICMap.put(icIds, prefICard);
                            }
                        }
                    }

                    IdentityCardType nsiICard = icardsToUpdate.get(guid);
                    if (null != entity)
                    {
                        FefuNsiIds entityNsiId = nsiIdsMap.get(entity.getId().toString());
                        if (null != entityNsiId && nonProcessableIncomingPersonGuids.contains(entityNsiId.getGuid()))
                        {
                            nonProcessableIncomingPersonGuids.add(item.getID());
                            if(NsiReactorUtils.isIdentityCardIgnore()) continue;
                        }
                        personDuplicates.remove(entity);
                        FefuNsiIds nsiId = nsiIdsMap.get(null == newGuid ? guid : newGuid);
                        Person updatedEntity = updateEntityFields(entity, item, nsiICard);
                        String icardGuid = null != nsiICard ? nsiICard.getID() : (null != entity.getIdentityCard().getId() ? NsiReactorUtils.generateGUID(entity.getIdentityCard().getId()) : null);
                        FefuNsiIds icardNsiId = nsiIdsMap.get(icardGuid);
                        Person studentPerson = studentPersonMap.get(null == newGuid ? guid : newGuid);
                        FefuNsiIds newIds = IFefuNsiSyncDAO.instance.get().saveOrUpdateCrazyPersonWithNsiId(updatedEntity, nsiId, guid, getEntityName(), icardNsiId, icardGuid, personDuplicates, prefICMap, item.getHumanLogin(), studentPerson);
                        NsiObjectsHolder.removePersonDuplicates(personDuplicates);
                        NsiObjectsHolder.addPerson(entity, newIds);
                    } else
                    {
                        throw new NSIProcessingErrorException("There is no mapped entity in OB for the given entity GUID or the object has different entity type, than the requested.");
                    }

                    updatedObjects.add(nsiICard);
                }
            } else
            {
                throw new NSIProcessingErrorException("There is no GUID for one, or more entity in datagram.");
            }
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== UPDATE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
        return updatedObjects;
    }

    @Override
    public List<Object> delete(List<INsiEntity> itemsToDelete) throws NSIProcessingErrorException
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB HAVE STARTED ==========");

        Set<String> guidsToDelete = new HashSet<>();
        List<HumanType> humansToDelete = new ArrayList<>();
        List<Object> deletedObjects = new ArrayList<>();

        if (null != itemsToDelete)
        {
            for (Object nsiEntity : itemsToDelete)
            {
                if (nsiEntity instanceof HumanType)
                {
                    HumanType human = (HumanType) nsiEntity;
                    String guid = getGUID(human);
                    if (null != guid) guidsToDelete.add(guid);
                    humansToDelete.add(human);
                }
            }
        }

        if (guidsToDelete.isEmpty())
            throw new NSIProcessingErrorException("There is no any entity guid to delete in datagram.");

        //prepareRelatedObjectsMap();

        Map<String, Person> entityMap = guidsToDelete.isEmpty() ? getNsiSynchrinizedEntityMap(getEntityClass(), null, false) : getNsiSynchrinizedEntityMap(getEntityClass(), guidsToDelete, false);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        // Формируем мап идентификаторов НСИ из числа сохраненных в ОБ, где ключ - идентификатор ОБ, либо GUID
        Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMapByGuids(EntityRuntime.getMeta(getEntityClass()).getName(), guidsToDelete);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        Set<String> alreadyDeletedPersons = new HashSet<>();
        for (HumanType item : humansToDelete)
        {
            String guid = getGUID(item);

            if (null != guid)
            {
                if (!alreadyDeletedPersons.contains(guid))
                {
                    alreadyDeletedPersons.add(guid);
                    Person entity = entityMap.get(guid);
                    if (null != entity)
                    {
                        FefuNsiIds nsiId = nsiIdsMap.get(guid);
                        try
                        {
                            IFefuNsiSyncDAO.instance.get().deletePersonWithNsiId(entity, nsiId);
                        } catch (Exception e)
                        {
                            throw new NSIProcessingErrorException("There are one or more entities, linked to the item requested for delete. You should delete them before.");
                        }
                    } else
                    {
                        throw new NSIProcessingErrorException("There is no mapped entity in OB for the given entity GUID or the object has different entity type, than the requested.");
                    }

                    deletedObjects.add(item);
                }
            } else
            {
                throw new NSIProcessingErrorException("There is no GUID for one, or more entity in datagram.");
            }
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");

        return deletedObjects;
    }
}