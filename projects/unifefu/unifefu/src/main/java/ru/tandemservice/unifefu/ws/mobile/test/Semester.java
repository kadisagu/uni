/**
 * Semester.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.mobile.test;

public class Semester  implements java.io.Serializable {
    private java.lang.String id;

    private java.lang.String num;

    private java.lang.String seminars;

    private java.lang.String labs;

    private java.lang.String practices;

    private java.lang.String lectures;

    private java.lang.String diff_credit;

    private java.lang.String exam;

    private java.lang.String credit;

    public Semester() {
    }

    public Semester(
           java.lang.String id,
           java.lang.String num,
           java.lang.String seminars,
           java.lang.String labs,
           java.lang.String practices,
           java.lang.String lectures,
           java.lang.String diff_credit,
           java.lang.String exam,
           java.lang.String credit) {
           this.id = id;
           this.num = num;
           this.seminars = seminars;
           this.labs = labs;
           this.practices = practices;
           this.lectures = lectures;
           this.diff_credit = diff_credit;
           this.exam = exam;
           this.credit = credit;
    }


    /**
     * Gets the id value for this Semester.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this Semester.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the num value for this Semester.
     * 
     * @return num
     */
    public java.lang.String getNum() {
        return num;
    }


    /**
     * Sets the num value for this Semester.
     * 
     * @param num
     */
    public void setNum(java.lang.String num) {
        this.num = num;
    }


    /**
     * Gets the seminars value for this Semester.
     * 
     * @return seminars
     */
    public java.lang.String getSeminars() {
        return seminars;
    }


    /**
     * Sets the seminars value for this Semester.
     * 
     * @param seminars
     */
    public void setSeminars(java.lang.String seminars) {
        this.seminars = seminars;
    }


    /**
     * Gets the labs value for this Semester.
     * 
     * @return labs
     */
    public java.lang.String getLabs() {
        return labs;
    }


    /**
     * Sets the labs value for this Semester.
     * 
     * @param labs
     */
    public void setLabs(java.lang.String labs) {
        this.labs = labs;
    }


    /**
     * Gets the practices value for this Semester.
     * 
     * @return practices
     */
    public java.lang.String getPractices() {
        return practices;
    }


    /**
     * Sets the practices value for this Semester.
     * 
     * @param practices
     */
    public void setPractices(java.lang.String practices) {
        this.practices = practices;
    }


    /**
     * Gets the lectures value for this Semester.
     * 
     * @return lectures
     */
    public java.lang.String getLectures() {
        return lectures;
    }


    /**
     * Sets the lectures value for this Semester.
     * 
     * @param lectures
     */
    public void setLectures(java.lang.String lectures) {
        this.lectures = lectures;
    }


    /**
     * Gets the diff_credit value for this Semester.
     * 
     * @return diff_credit
     */
    public java.lang.String getDiff_credit() {
        return diff_credit;
    }


    /**
     * Sets the diff_credit value for this Semester.
     * 
     * @param diff_credit
     */
    public void setDiff_credit(java.lang.String diff_credit) {
        this.diff_credit = diff_credit;
    }


    /**
     * Gets the exam value for this Semester.
     * 
     * @return exam
     */
    public java.lang.String getExam() {
        return exam;
    }


    /**
     * Sets the exam value for this Semester.
     * 
     * @param exam
     */
    public void setExam(java.lang.String exam) {
        this.exam = exam;
    }


    /**
     * Gets the credit value for this Semester.
     * 
     * @return credit
     */
    public java.lang.String getCredit() {
        return credit;
    }


    /**
     * Sets the credit value for this Semester.
     * 
     * @param credit
     */
    public void setCredit(java.lang.String credit) {
        this.credit = credit;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Semester)) return false;
        Semester other = (Semester) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.num==null && other.getNum()==null) || 
             (this.num!=null &&
              this.num.equals(other.getNum()))) &&
            ((this.seminars==null && other.getSeminars()==null) || 
             (this.seminars!=null &&
              this.seminars.equals(other.getSeminars()))) &&
            ((this.labs==null && other.getLabs()==null) || 
             (this.labs!=null &&
              this.labs.equals(other.getLabs()))) &&
            ((this.practices==null && other.getPractices()==null) || 
             (this.practices!=null &&
              this.practices.equals(other.getPractices()))) &&
            ((this.lectures==null && other.getLectures()==null) || 
             (this.lectures!=null &&
              this.lectures.equals(other.getLectures()))) &&
            ((this.diff_credit==null && other.getDiff_credit()==null) || 
             (this.diff_credit!=null &&
              this.diff_credit.equals(other.getDiff_credit()))) &&
            ((this.exam==null && other.getExam()==null) || 
             (this.exam!=null &&
              this.exam.equals(other.getExam()))) &&
            ((this.credit==null && other.getCredit()==null) || 
             (this.credit!=null &&
              this.credit.equals(other.getCredit())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getNum() != null) {
            _hashCode += getNum().hashCode();
        }
        if (getSeminars() != null) {
            _hashCode += getSeminars().hashCode();
        }
        if (getLabs() != null) {
            _hashCode += getLabs().hashCode();
        }
        if (getPractices() != null) {
            _hashCode += getPractices().hashCode();
        }
        if (getLectures() != null) {
            _hashCode += getLectures().hashCode();
        }
        if (getDiff_credit() != null) {
            _hashCode += getDiff_credit().hashCode();
        }
        if (getExam() != null) {
            _hashCode += getExam().hashCode();
        }
        if (getCredit() != null) {
            _hashCode += getCredit().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Semester.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "Semester"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("num");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "num"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seminars");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "seminars"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("labs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "labs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("practices");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "practices"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lectures");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "lectures"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diff_credit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "diff_credit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exam");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "exam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("credit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "credit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
