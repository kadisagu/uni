/* $Id$ */
package ru.tandemservice.unifefu.dao;

import ru.tandemservice.movestudent.dao.IMoveStudentDao;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;

import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 11/25/13
 */
public interface IFefuMoveStudentDAO extends IMoveStudentDao
{
    List<FefuOrphanPayment> getOrphanPayments(Long extractId);

    /**
     * Разрывает связь студента-старосты с группами
     * @param extract - выписка из приказа о присвоении квалификации
     */
    void breakLinkCaptainWithGroups(AbstractStudentExtract extract);


    /**
     * Возвращает студента в старосты групп
     * @param extract - выписка из приказа о присвоении квалификации
     */
    void linkStudCaptainWithGroups(AbstractStudentExtract extract);

    /**
     * Установка даты окончания действия статуса
     */
    void setStatusEndDateByCode(ModularStudentExtract extract, String statusCode, Date endDate);

    /**
     * Восстановление значений дат для доп статуса
     */
    void revertCustomStateDates(ModularStudentExtract extract);

}
