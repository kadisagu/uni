/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu21.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public class Model extends ModularStudentExtractPubModel<FefuExcludeStuDPOExtract>
{
    private StudentStatus _studentStatusNew;
    private String _printFormFileName;
    private FefuAdditionalProfessionalEducationProgram _dpoProgramOld;

    public FefuAdditionalProfessionalEducationProgram getDpoProgramOld()
    {
        return _dpoProgramOld;
    }

    public void setDpoProgramOld(FefuAdditionalProfessionalEducationProgram dpoProgramOld)
    {
        _dpoProgramOld = dpoProgramOld;
    }

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }

    public String getPrintFormFileName()
    {
        return _printFormFileName;
    }

    public void setPrintFormFileName(String printFormFileName)
    {
        _printFormFileName = printFormFileName;
    }

    public boolean isShowPrintForm()
    {
        return isIndividualOrder() && getExtract().getParagraph().getOrder().getState().getCode().equals("1");

    }
}