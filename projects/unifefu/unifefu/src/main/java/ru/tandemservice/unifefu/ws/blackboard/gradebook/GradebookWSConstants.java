/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.gradebook;

/**
 * @author Nikolay Fedorovskih
 * @since 28.01.2014
 */
public class GradebookWSConstants
{
    public static final String ABANDONED = "Abandoned";
    public static final String AVERAGE_AGGREGATION_MODEL = "Average";
    public static final String AVERAGE_CALC_TYPE = "AVERAGE";
    public static final String CANCELED = "Canceled";
    public static final String COMPLETE = "C";
    public static final String COMPLETED = "Completed";
    public static final String FIRST_AGGREGATION_MODEL = "First";

    public static final int COLUMN_ATTRIBUTE_COLUMN_DISPLAY_NAME = 2;
    public static final int COLUMN_ATTRIBUTE_COLUMN_NAME = 1; // Supported column attributes that can be updated by calling updateColumnAttribute} of GradebookWS.
    public static final int COLUMN_ATTRIBUTE_MAX_VALUE = 3;
    public static final int COLUMN_ATTRIBUTE_STATS_VISIBLE = 5;
    protected static final int COLUMN_ATTRIBUTE_VALID_HIGH_LIMIT = 6; // COLUMN_ATTRIBUTE_VALID_HIGH_LIMIT is not a supported column attribute value.
    public static final int COLUMN_ATTRIBUTE_VISIBLE_TO_STUDENT_IND = 4;

    public static final int GET_ATTEMPT_BY_GRADE_ID = 1; // Supported filter type values which must be specified for loading attempts
    public static final int GET_ATTEMPT_BY_GRADE_ID_AND_LAST_ATTEMPT = 2;
    public static final int GET_ATTEMPT_BY_IDS = 3;
    public static final int GET_COLUMN_BY_COURSE_ID = 1; // For GET_BY_COURSE_ID, framework will pass in the course id.
    public static final int GET_COLUMN_BY_COURSE_ID_AND_COLUMN_NAME = 2; // For GET_BY_COURSE_ID_AND_COLUMN_NAME, courseId and name must be populated
    public static final int GET_COLUMN_BY_EXTERNAL_GRADE_FLAG = 4; // For GET_BY_EXTERNAL_GRADE_FLAG, courseId must be populated
    public static final int GET_COLUMN_BY_IDS = 3; // For GET_BY_ID, id(s) must be populated
    public static final int GET_GRADEBOOK_TYPE_BY_COURSE_ID = 1; // Supported filter type values which must be specified for loading gradebook type
    public static final int GET_GRADEBOOK_TYPE_BY_COURSE_ID_AND_TITLE = 2;
    public static final int GET_GRADEBOOK_TYPE_BY_ID = 3;
    public static final int GET_GRADING_SCHEMA_BY_COURSE_ID = 1; // Supported filter type values which must be specified for loading grading schema
    public static final int GET_GRADING_SCHEMA_BY_COURSE_ID_AND_TITLE = 2;
    public static final int GET_GRADING_SCHEMA_BY_ID = 3;
    public static final int GET_SCORE_BY_COLUMN_ID = 3;
    public static final int GET_SCORE_BY_COLUMN_ID_AND_USER_ID = 2;
    public static final int GET_SCORE_BY_COURSE_ID = 1; // Supported filter type values which must be specified for loading grades
    public static final int GET_SCORE_BY_COURSE_ID_AND_FINAL_GRADE = 4;
    public static final int GET_SCORE_BY_ID = 7;
    public static final int GET_SCORE_BY_MEMBER_ID = 6;
    public static final int GET_SCORE_BY_MEMBER_ID_AND_COLUMN_ID = 5;
    public static final int GET_SCORE_BY_MEMBER_IDS_AND_COLUMN_ID = 10;
    public static final int GET_SCORE_BY_USER_ID = 8;
    public static final int GET_SCORE_BY_USER_ID_AND_FINAL_GRADE = 9;

    public static final String HIGHEST_AGGREGATION_MODEL = "Highest";
    public static final String HTML_TEXT_TYPE = "HTML";
    public static final String IN_PROGRESS = "In Progress";
    public static final String INSTRUCTOR_COMMENTS_DISPLAY_VALUE = "instructorCommentsDisplayValue"; // Used in ScoreVO and AttemptVO expansionData to render the full instructor comments with internal URL references expanded to include appropriate paths.
    public static final String INSTRUCTOR_COMMENTS_TEXT_TYPE = "instructorCommentsTextType"; // Used in ScoreVO and AttemptVO expansionData to indicate the type of instructor comments (aka instructor notes).
    public static final String LAST_AGGREGATION_MODEL = "LAST";
    public static final String LOWEST_AGGREGATION_MODEL = "Lowest";
    public static final String MINMAX_CALC_TYPE = "MINMAX";
    public static final String NEEDS_GRADING = "Needs Grading";
    public static final String NON_CALCULATED_CALC_TYPE = "NON_CALCULATED";
    public static final String NOT_ATTEMPTED = "Not Attempted";
    public static final String PERCENT = "P";
    public static final String PLAIN_TEXT_TEXT_TYPE = "PLAIN_TEXT";
    public static final String RESOLVE_FULL_COMMENTS = "resolve.full.comments"; // set in the ScoreFilter.expansiondata if you want the feedback and instructor notes resolved so that references to internal files are replaced with fully qualified URLs and set in the expansionData.
    public static final String SCORE = "S";

    public static final int SCORE_STATUS_GRADED = 1;
    public static final int SCORE_STATUS_NEEDS_GRADING = 2;

    public static final String STUDENT_COMMENTS_DISPLAY_VALUE = "studentCommentsDisplayValue"; // Used in ScoreVO and AttemptVO expansionData to render the full feedback with internal URL references expanded to include appropriate paths.
    public static final String STUDENT_COMMENTS_TEXT_TYPE = "studentCommentsTextType"; // Used in ScoreVO and AttemptVO expansionData to indicate the type of feedback (aka student comments).
    public static final String SUSPENDED = "Suspended";
    public static final String TABULAR = "T";
    public static final String TEXT = "X";
    public static final String TOTAL_CALC_TYPE = "TOTAL";
    public static final String WEIGHTED_TOTAL_CALC_TYPE = "WEIGHTED_TOTAL";


    public static final String GBWS001 = "Gradebook.WS001"; // Failed to create a column
    public static final String GBWS002 = "Gradebook.WS002"; // Failed to save score
    public static final String GBWS003 = "Gradebook.WS003"; // Failed to load requested columns
    public static final String GBWS004 = "Gradebook.WS004"; // Failed to delete a column
    public static final String GBWS005 = "Gradebook.WS005"; // Failed to delete a grading schema
    public static final String GBWS006 = "Gradebook.WS006"; // Failed to save a grading schema
    public static final String GBWS007 = "Gradebook.WS007"; // Failed to load the grading schema
    public static final String GBWS008 = "Gradebook.WS008"; // Failed to delete a gradebook type
    public static final String GBWS009 = "Gradebook.WS009"; // Failed to save a gradebook type
    public static final String GBWS010 = "Gradebook.WS010"; // Failed to load the gradebook type
    public static final String GBWS011 = "Gradebook.WS011"; // Failed to load grades
    public static final String GBWS012 = "Gradebook.WS012"; // Failed to load attempts
    public static final String GBWS013 = "Gradebook.WS013"; // Failed to update column attribute
    public static final String GBWS014 = "Gradebook.WS014"; // Failed to get the user in session
    public static final String GBWS015 = "Gradebook.WS015"; // Failed to get the course in session
}