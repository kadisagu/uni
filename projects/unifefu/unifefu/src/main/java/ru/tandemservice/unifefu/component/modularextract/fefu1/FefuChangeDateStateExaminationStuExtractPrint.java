package ru.tandemservice.unifefu.component.modularextract.fefu1;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/17/12
 * Time: 4:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class FefuChangeDateStateExaminationStuExtractPrint implements IPrintFormCreator<FefuChangeDateStateExaminationStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuChangeDateStateExaminationStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("period", extract.getSeason() != null ? extract.getSeason() : "______");
        modifier.put("year", String.valueOf(extract.getYear()));
        modifier.put("newDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getNewDate()));
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}
