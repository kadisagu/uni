package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О возобновлении выплаты социальной стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuSocGrantResumptionStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract";
    public static final String ENTITY_NAME = "fefuSocGrantResumptionStuExtract";
    public static final int VERSION_HASH = 256411504;
    private static IEntityMeta ENTITY_META;

    public static final String L_SOC_GRANT_EXTRACT = "socGrantExtract";
    public static final String P_SOC_GRANT_ORDER = "socGrantOrder";
    public static final String P_SOC_GRANT_ORDER_DATE = "socGrantOrderDate";
    public static final String P_PAY_RESUME_DATE = "payResumeDate";
    public static final String P_PAYMENT_END_DATE = "paymentEndDate";

    private AbstractStudentExtract _socGrantExtract;     // Выписка о назначении социальной стипендии
    private String _socGrantOrder;     // Номер приказа о назначении социальной стипендии
    private Date _socGrantOrderDate;     // Дата приказа о назначении социальной стипендии
    private Date _payResumeDate;     // Дата возобновления выплаты социальной стипендии
    private Date _paymentEndDate;     // Дата окончания выплат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка о назначении социальной стипендии.
     */
    public AbstractStudentExtract getSocGrantExtract()
    {
        return _socGrantExtract;
    }

    /**
     * @param socGrantExtract Выписка о назначении социальной стипендии.
     */
    public void setSocGrantExtract(AbstractStudentExtract socGrantExtract)
    {
        dirty(_socGrantExtract, socGrantExtract);
        _socGrantExtract = socGrantExtract;
    }

    /**
     * @return Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSocGrantOrder()
    {
        return _socGrantOrder;
    }

    /**
     * @param socGrantOrder Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    public void setSocGrantOrder(String socGrantOrder)
    {
        dirty(_socGrantOrder, socGrantOrder);
        _socGrantOrder = socGrantOrder;
    }

    /**
     * @return Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getSocGrantOrderDate()
    {
        return _socGrantOrderDate;
    }

    /**
     * @param socGrantOrderDate Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    public void setSocGrantOrderDate(Date socGrantOrderDate)
    {
        dirty(_socGrantOrderDate, socGrantOrderDate);
        _socGrantOrderDate = socGrantOrderDate;
    }

    /**
     * @return Дата возобновления выплаты социальной стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getPayResumeDate()
    {
        return _payResumeDate;
    }

    /**
     * @param payResumeDate Дата возобновления выплаты социальной стипендии. Свойство не может быть null.
     */
    public void setPayResumeDate(Date payResumeDate)
    {
        dirty(_payResumeDate, payResumeDate);
        _payResumeDate = payResumeDate;
    }

    /**
     * @return Дата окончания выплат.
     */
    public Date getPaymentEndDate()
    {
        return _paymentEndDate;
    }

    /**
     * @param paymentEndDate Дата окончания выплат.
     */
    public void setPaymentEndDate(Date paymentEndDate)
    {
        dirty(_paymentEndDate, paymentEndDate);
        _paymentEndDate = paymentEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuSocGrantResumptionStuExtractGen)
        {
            setSocGrantExtract(((FefuSocGrantResumptionStuExtract)another).getSocGrantExtract());
            setSocGrantOrder(((FefuSocGrantResumptionStuExtract)another).getSocGrantOrder());
            setSocGrantOrderDate(((FefuSocGrantResumptionStuExtract)another).getSocGrantOrderDate());
            setPayResumeDate(((FefuSocGrantResumptionStuExtract)another).getPayResumeDate());
            setPaymentEndDate(((FefuSocGrantResumptionStuExtract)another).getPaymentEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuSocGrantResumptionStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuSocGrantResumptionStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuSocGrantResumptionStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    return obj.getSocGrantExtract();
                case "socGrantOrder":
                    return obj.getSocGrantOrder();
                case "socGrantOrderDate":
                    return obj.getSocGrantOrderDate();
                case "payResumeDate":
                    return obj.getPayResumeDate();
                case "paymentEndDate":
                    return obj.getPaymentEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    obj.setSocGrantExtract((AbstractStudentExtract) value);
                    return;
                case "socGrantOrder":
                    obj.setSocGrantOrder((String) value);
                    return;
                case "socGrantOrderDate":
                    obj.setSocGrantOrderDate((Date) value);
                    return;
                case "payResumeDate":
                    obj.setPayResumeDate((Date) value);
                    return;
                case "paymentEndDate":
                    obj.setPaymentEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                        return true;
                case "socGrantOrder":
                        return true;
                case "socGrantOrderDate":
                        return true;
                case "payResumeDate":
                        return true;
                case "paymentEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    return true;
                case "socGrantOrder":
                    return true;
                case "socGrantOrderDate":
                    return true;
                case "payResumeDate":
                    return true;
                case "paymentEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    return AbstractStudentExtract.class;
                case "socGrantOrder":
                    return String.class;
                case "socGrantOrderDate":
                    return Date.class;
                case "payResumeDate":
                    return Date.class;
                case "paymentEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuSocGrantResumptionStuExtract> _dslPath = new Path<FefuSocGrantResumptionStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuSocGrantResumptionStuExtract");
    }
            

    /**
     * @return Выписка о назначении социальной стипендии.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract#getSocGrantExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> socGrantExtract()
    {
        return _dslPath.socGrantExtract();
    }

    /**
     * @return Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract#getSocGrantOrder()
     */
    public static PropertyPath<String> socGrantOrder()
    {
        return _dslPath.socGrantOrder();
    }

    /**
     * @return Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract#getSocGrantOrderDate()
     */
    public static PropertyPath<Date> socGrantOrderDate()
    {
        return _dslPath.socGrantOrderDate();
    }

    /**
     * @return Дата возобновления выплаты социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract#getPayResumeDate()
     */
    public static PropertyPath<Date> payResumeDate()
    {
        return _dslPath.payResumeDate();
    }

    /**
     * @return Дата окончания выплат.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract#getPaymentEndDate()
     */
    public static PropertyPath<Date> paymentEndDate()
    {
        return _dslPath.paymentEndDate();
    }

    public static class Path<E extends FefuSocGrantResumptionStuExtract> extends ModularStudentExtract.Path<E>
    {
        private AbstractStudentExtract.Path<AbstractStudentExtract> _socGrantExtract;
        private PropertyPath<String> _socGrantOrder;
        private PropertyPath<Date> _socGrantOrderDate;
        private PropertyPath<Date> _payResumeDate;
        private PropertyPath<Date> _paymentEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка о назначении социальной стипендии.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract#getSocGrantExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> socGrantExtract()
        {
            if(_socGrantExtract == null )
                _socGrantExtract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_SOC_GRANT_EXTRACT, this);
            return _socGrantExtract;
        }

    /**
     * @return Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract#getSocGrantOrder()
     */
        public PropertyPath<String> socGrantOrder()
        {
            if(_socGrantOrder == null )
                _socGrantOrder = new PropertyPath<String>(FefuSocGrantResumptionStuExtractGen.P_SOC_GRANT_ORDER, this);
            return _socGrantOrder;
        }

    /**
     * @return Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract#getSocGrantOrderDate()
     */
        public PropertyPath<Date> socGrantOrderDate()
        {
            if(_socGrantOrderDate == null )
                _socGrantOrderDate = new PropertyPath<Date>(FefuSocGrantResumptionStuExtractGen.P_SOC_GRANT_ORDER_DATE, this);
            return _socGrantOrderDate;
        }

    /**
     * @return Дата возобновления выплаты социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract#getPayResumeDate()
     */
        public PropertyPath<Date> payResumeDate()
        {
            if(_payResumeDate == null )
                _payResumeDate = new PropertyPath<Date>(FefuSocGrantResumptionStuExtractGen.P_PAY_RESUME_DATE, this);
            return _payResumeDate;
        }

    /**
     * @return Дата окончания выплат.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract#getPaymentEndDate()
     */
        public PropertyPath<Date> paymentEndDate()
        {
            if(_paymentEndDate == null )
                _paymentEndDate = new PropertyPath<Date>(FefuSocGrantResumptionStuExtractGen.P_PAYMENT_END_DATE, this);
            return _paymentEndDate;
        }

        public Class getEntityClass()
        {
            return FefuSocGrantResumptionStuExtract.class;
        }

        public String getEntityName()
        {
            return "fefuSocGrantResumptionStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
