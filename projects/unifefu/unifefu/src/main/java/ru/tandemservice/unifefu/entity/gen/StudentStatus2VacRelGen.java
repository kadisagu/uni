package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.StudentStatus2VacRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь состояний студентов и типов отпусков
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentStatus2VacRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.StudentStatus2VacRel";
    public static final String ENTITY_NAME = "studentStatus2VacRel";
    public static final int VERSION_HASH = -1987758459;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATUS = "status";
    public static final String P_CONSIDER = "consider";

    private StudentStatus _status;     // Состояние студента
    private boolean _consider; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Состояние студента. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StudentStatus getStatus()
    {
        return _status;
    }

    /**
     * @param status Состояние студента. Свойство не может быть null и должно быть уникальным.
     */
    public void setStatus(StudentStatus status)
    {
        dirty(_status, status);
        _status = status;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isConsider()
    {
        return _consider;
    }

    /**
     * @param consider  Свойство не может быть null.
     */
    public void setConsider(boolean consider)
    {
        dirty(_consider, consider);
        _consider = consider;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentStatus2VacRelGen)
        {
            setStatus(((StudentStatus2VacRel)another).getStatus());
            setConsider(((StudentStatus2VacRel)another).isConsider());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentStatus2VacRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentStatus2VacRel.class;
        }

        public T newInstance()
        {
            return (T) new StudentStatus2VacRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "status":
                    return obj.getStatus();
                case "consider":
                    return obj.isConsider();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "status":
                    obj.setStatus((StudentStatus) value);
                    return;
                case "consider":
                    obj.setConsider((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "status":
                        return true;
                case "consider":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "status":
                    return true;
                case "consider":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "status":
                    return StudentStatus.class;
                case "consider":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentStatus2VacRel> _dslPath = new Path<StudentStatus2VacRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentStatus2VacRel");
    }
            

    /**
     * @return Состояние студента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.StudentStatus2VacRel#getStatus()
     */
    public static StudentStatus.Path<StudentStatus> status()
    {
        return _dslPath.status();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentStatus2VacRel#isConsider()
     */
    public static PropertyPath<Boolean> consider()
    {
        return _dslPath.consider();
    }

    public static class Path<E extends StudentStatus2VacRel> extends EntityPath<E>
    {
        private StudentStatus.Path<StudentStatus> _status;
        private PropertyPath<Boolean> _consider;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Состояние студента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.StudentStatus2VacRel#getStatus()
     */
        public StudentStatus.Path<StudentStatus> status()
        {
            if(_status == null )
                _status = new StudentStatus.Path<StudentStatus>(L_STATUS, this);
            return _status;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentStatus2VacRel#isConsider()
     */
        public PropertyPath<Boolean> consider()
        {
            if(_consider == null )
                _consider = new PropertyPath<Boolean>(StudentStatus2VacRelGen.P_CONSIDER, this);
            return _consider;
        }

        public Class getEntityClass()
        {
            return StudentStatus2VacRel.class;
        }

        public String getEntityName()
        {
            return "studentStatus2VacRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
