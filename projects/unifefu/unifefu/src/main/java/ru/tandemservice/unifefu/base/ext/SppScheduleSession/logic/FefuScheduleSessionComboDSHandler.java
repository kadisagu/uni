/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleSession.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionPrintParams;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Igor Belanov
 * @since 22.09.2016
 */
public class FefuScheduleSessionComboDSHandler extends SppScheduleSessionComboDSHandler
{
    public FefuScheduleSessionComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalFilters(DQLSelectBuilder builder, SppScheduleSessionPrintParams scheduleData)
    {
        builder.where(eq(property("s", SppScheduleSession.group().educationOrgUnit().territorialOrgUnit()), value(scheduleData.getTerritorialOrgUnit())));
    }
}
