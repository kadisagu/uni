/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EcCampaign;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uniec.base.bo.EcCampaign.logic.IEcCampaignDao;
import ru.tandemservice.unifefu.base.ext.EcCampaign.logic.FefuEcCampaignDao;

/**
 * @author Nikolay Fedorovskih
 * @since 21.06.2013
 */
@Configuration
public class EcCampaignExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEcCampaignDao dao()
    {
        return new FefuEcCampaignDao();
    }
}