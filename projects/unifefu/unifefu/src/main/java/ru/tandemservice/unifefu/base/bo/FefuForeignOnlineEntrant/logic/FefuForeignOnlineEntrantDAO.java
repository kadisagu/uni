/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;
import ru.tandemservice.unifefu.ws.foreignOnlineEntrants.FefuForeignOnlineEntrantsService;

import javax.xml.bind.annotation.XmlAttribute;
import java.util.Date;

/**
 * @author nvankov
 * @since 4/2/13
 */
@Transactional
public class FefuForeignOnlineEntrantDAO extends BaseModifyAggregateDAO implements IFefuForeignOnlineEntrantDAO
{
    public static final int FEFU_INFO_CODE_INTERNET = 1;
    public static final int FEFU_INFO_CODE_ADVERTISING = 2;
    public static final int FEFU_INFO_CODE_OTHER = 3;

    @Override
    public void createOrUpdate(FefuForeignOnlineEntrant foreignOnlineEntrant)
    {
        baseCreateOrUpdate(foreignOnlineEntrant);
    }

    @Override
    public void webServiceCreateOrUpdate(FefuForeignOnlineEntrantsService.ForeignOnlineEntrant foreignOnlineEntrantWS)
    {
        boolean create = false;
        FefuForeignOnlineEntrant fefuForeignOnlineEntrant = getFefuForeignOnlineEntrant(foreignOnlineEntrantWS.userId);

        if(null == fefuForeignOnlineEntrant)
        {
            create = true;
            fefuForeignOnlineEntrant = new FefuForeignOnlineEntrant();
        }

        fefuForeignOnlineEntrant.setUserId(foreignOnlineEntrantWS.userId);

        if(create)
        {

            EnrollmentCampaign enrollmentCampaign = new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "e").top(1)
                    .column(DQLExpressions.property("e"))
                    .order(DQLExpressions.property(EnrollmentCampaign.id().fromAlias("e")), OrderDirection.desc)
                    .createStatement(getSession()).uniqueResult();
            if(null == enrollmentCampaign)
                throw new ApplicationException("Нету приемной компании");
            fefuForeignOnlineEntrant.setEnrollmentCampaign(enrollmentCampaign);
            fefuForeignOnlineEntrant.setPersonalNumber(getMaxEntrantPersonalNumber(enrollmentCampaign) + 1);
            fefuForeignOnlineEntrant.setRegistrationDate(new Date());
        }

        fefuForeignOnlineEntrant.setLastNameRu(foreignOnlineEntrantWS.lastNameRu);
        fefuForeignOnlineEntrant.setFirstNameRu(foreignOnlineEntrantWS.firstNameRu);
        fefuForeignOnlineEntrant.setMiddleNameRu(foreignOnlineEntrantWS.middleNameRu);
        fefuForeignOnlineEntrant.setFirstNameEn(foreignOnlineEntrantWS.firstNameEn);
        fefuForeignOnlineEntrant.setLastNameEn(foreignOnlineEntrantWS.lastNameEn);
        fefuForeignOnlineEntrant.setFioNativeEscaped(foreignOnlineEntrantWS.fioNative);
        fefuForeignOnlineEntrant.setCountry(foreignOnlineEntrantWS.country);
        fefuForeignOnlineEntrant.setBirthDate(foreignOnlineEntrantWS.birthDate);
        fefuForeignOnlineEntrant.setBirthPlace(foreignOnlineEntrantWS.birthPlace);
        fefuForeignOnlineEntrant.setMaritalStatus(foreignOnlineEntrantWS.maritalStatus);
        fefuForeignOnlineEntrant.setAddressNativeEscaped(foreignOnlineEntrantWS.addressNative);
        fefuForeignOnlineEntrant.setAddressEn(foreignOnlineEntrantWS.addressEn);
        fefuForeignOnlineEntrant.setPhoneHome(foreignOnlineEntrantWS.phoneHome);
        fefuForeignOnlineEntrant.setAddressVlad(foreignOnlineEntrantWS.addressVlad);
        fefuForeignOnlineEntrant.setPhoneVlad(foreignOnlineEntrantWS.phoneVlad);
        fefuForeignOnlineEntrant.setEmail(foreignOnlineEntrantWS.email);
        fefuForeignOnlineEntrant.setSchoolWithLocation(foreignOnlineEntrantWS.schoolWithLocation);
        fefuForeignOnlineEntrant.setEnteringDateStr(foreignOnlineEntrantWS.enteringDateStr);
        fefuForeignOnlineEntrant.setGraduationDateStr(foreignOnlineEntrantWS.graduationDateStr);
        fefuForeignOnlineEntrant.setForeignLang(foreignOnlineEntrantWS.foreignLang);
        fefuForeignOnlineEntrant.setWorkPlace(foreignOnlineEntrantWS.workPlace);
        fefuForeignOnlineEntrant.setOccupation(foreignOnlineEntrantWS.occupation);
        fefuForeignOnlineEntrant.setWorkDateFrom(foreignOnlineEntrantWS.workDateFrom);
        fefuForeignOnlineEntrant.setWorkDateTo(foreignOnlineEntrantWS.workDateTo);
        fefuForeignOnlineEntrant.setContactPerson(foreignOnlineEntrantWS.contactPerson);
        fefuForeignOnlineEntrant.setFefuInfo(foreignOnlineEntrantWS.fefuInfo);
        fefuForeignOnlineEntrant.setNeedDormitory(foreignOnlineEntrantWS.needDormitory);
        fefuForeignOnlineEntrant.setEduProgrammAndSpec(foreignOnlineEntrantWS.eduProgrammAndSpec);

        createOrUpdate(fefuForeignOnlineEntrant);
    }

    @Override
    public FefuForeignOnlineEntrantsService.ForeignOnlineEntrant webServiceGetForeignOnlineEntrant(Long userId)
    {
        FefuForeignOnlineEntrant fefuForeignOnlineEntrant = getFefuForeignOnlineEntrant(userId);
        if(null == fefuForeignOnlineEntrant) return null;

        FefuForeignOnlineEntrantsService.ForeignOnlineEntrant foreignOnlineEntrantWS = new FefuForeignOnlineEntrantsService.ForeignOnlineEntrant();
        foreignOnlineEntrantWS.userId = fefuForeignOnlineEntrant.getUserId();
        foreignOnlineEntrantWS.lastNameRu = fefuForeignOnlineEntrant.getLastNameRu();
        foreignOnlineEntrantWS.firstNameRu = fefuForeignOnlineEntrant.getFirstNameRu();
        foreignOnlineEntrantWS.middleNameRu = fefuForeignOnlineEntrant.getMiddleNameRu();
        foreignOnlineEntrantWS.firstNameEn = fefuForeignOnlineEntrant.getFirstNameEn();
        foreignOnlineEntrantWS.lastNameEn = fefuForeignOnlineEntrant.getLastNameEn();
        foreignOnlineEntrantWS.fioNative = fefuForeignOnlineEntrant.getFioNativeEscaped();
        foreignOnlineEntrantWS.country = fefuForeignOnlineEntrant.getCountry();
        foreignOnlineEntrantWS.birthDate = fefuForeignOnlineEntrant.getBirthDate();
        foreignOnlineEntrantWS.birthPlace = fefuForeignOnlineEntrant.getBirthPlace();
        foreignOnlineEntrantWS.maritalStatus = fefuForeignOnlineEntrant.getMaritalStatus();
        foreignOnlineEntrantWS.addressNative = fefuForeignOnlineEntrant.getAddressNativeEscaped();
        foreignOnlineEntrantWS.addressEn = fefuForeignOnlineEntrant.getAddressEn();
        foreignOnlineEntrantWS.phoneHome = fefuForeignOnlineEntrant.getPhoneHome();
        foreignOnlineEntrantWS.addressVlad = fefuForeignOnlineEntrant.getAddressVlad();
        foreignOnlineEntrantWS.phoneVlad = fefuForeignOnlineEntrant.getPhoneVlad();
        foreignOnlineEntrantWS.email = fefuForeignOnlineEntrant.getEmail();
        foreignOnlineEntrantWS.schoolWithLocation = fefuForeignOnlineEntrant.getSchoolWithLocation();
        foreignOnlineEntrantWS.enteringDateStr = fefuForeignOnlineEntrant.getEnteringDateStr();
        foreignOnlineEntrantWS.graduationDateStr = fefuForeignOnlineEntrant.getGraduationDateStr();
        foreignOnlineEntrantWS.foreignLang = fefuForeignOnlineEntrant.getForeignLang();
        foreignOnlineEntrantWS.workPlace = fefuForeignOnlineEntrant.getWorkPlace();
        foreignOnlineEntrantWS.occupation = fefuForeignOnlineEntrant.getOccupation();
        foreignOnlineEntrantWS.workDateFrom = fefuForeignOnlineEntrant.getWorkDateFrom();
        foreignOnlineEntrantWS.workDateTo = fefuForeignOnlineEntrant.getWorkDateTo();
        foreignOnlineEntrantWS.contactPerson = fefuForeignOnlineEntrant.getContactPerson();
        foreignOnlineEntrantWS.fefuInfo = fefuForeignOnlineEntrant.getFefuInfo();
        foreignOnlineEntrantWS.needDormitory = fefuForeignOnlineEntrant.getNeedDormitory();
        foreignOnlineEntrantWS.eduProgrammAndSpec = fefuForeignOnlineEntrant.getEduProgrammAndSpec();

        return foreignOnlineEntrantWS;
    }

    @Override
    public FefuForeignOnlineEntrant getFefuForeignOnlineEntrant(long userId)
    {
        return DataAccessServices.dao().get(FefuForeignOnlineEntrant.class, FefuForeignOnlineEntrant.userId(), userId);
    }

    @Override
    public boolean canDeleteEntrant(long userId)
    {
        FefuForeignOnlineEntrant entrant = getFefuForeignOnlineEntrant(userId);
        if(null != entrant)
            return null == entrant.getEntrant();
        else
            return true;
    }

    @Override
    public void deleteForeignOnlineEntrant(Long foreignOnlineEntrantId)
    {
        FefuForeignOnlineEntrant entrant = (FefuForeignOnlineEntrant) get(foreignOnlineEntrantId);
        if(null != entrant.getEntrant())
            throw new ApplicationException("Заявление иностранного онлайн-абитуриента принято. Удаление невозможно.");
        baseDelete(foreignOnlineEntrantId);
    }

    @Override
    public boolean deleteForeignOnlineEntrantWS(long userId)
    {
        FefuForeignOnlineEntrant entrant = getFefuForeignOnlineEntrant(userId);
        if(null == entrant) return true;
        if(!canDeleteEntrant(entrant.getUserId())) return false;
        else
        {
            deleteForeignOnlineEntrant(entrant.getId());
            return true;
        }
    }

    @Override
    public boolean isForeignOnlineEntrantAccepted(long userId)
    {
        FefuForeignOnlineEntrant entrant = getFefuForeignOnlineEntrant(userId);
        if(null == entrant) return false;
        return null != entrant.getEntrant();
    }

    @Override
    public String getForeignDocumentCopiesName(long userId)
    {
        FefuForeignOnlineEntrant entrant = getFefuForeignOnlineEntrant(userId);
        if(null == entrant) return null;

        return entrant.getDocumentCopiesName();
    }

    @Override
    public byte[] getForeignDocumentCopies(long userId)
    {
        FefuForeignOnlineEntrant entrant = getFefuForeignOnlineEntrant(userId);
        if(null == entrant) return null;
        if(null == entrant.getDocumentCopies()) return null;

        return entrant.getDocumentCopies().getContent();
    }

    @Override
    public void deleteForeignDocumentCopies(long userId)
    {
        FefuForeignOnlineEntrant onlineEntrant = getFefuForeignOnlineEntrant(userId);

        if (onlineEntrant == null) return;

        DatabaseFile databaseFile = onlineEntrant.getDocumentCopies();
        onlineEntrant.setDocumentCopies(null);
        onlineEntrant.setDocumentCopiesName(null);
        baseUpdate(onlineEntrant);

        if (databaseFile != null)
            baseDelete(databaseFile);
    }

    @Override
    public void saveForeignDocumentCopies(long userId, byte[] zipArchive, String documentCopiesName)
    {
        FefuForeignOnlineEntrant onlineEntrant = getFefuForeignOnlineEntrant(userId);

        if (onlineEntrant == null || zipArchive == null || StringUtils.isEmpty(documentCopiesName)) return;

        // если файл уже загружен, то удаляем его
        deleteForeignDocumentCopies(userId);

        // сохраняем файл
        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(zipArchive);
        baseCreate(databaseFile);

        onlineEntrant.setDocumentCopies(databaseFile);
        onlineEntrant.setDocumentCopiesName(documentCopiesName);
        createOrUpdate(onlineEntrant);
    }


    private int getMaxEntrantPersonalNumber(EnrollmentCampaign enrollmentCampaign)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "FefuForeignOnlineEntrantPersonalNumberSync-" + enrollmentCampaign.getId());

        Criteria foreignEntrantCriteria = getSession().createCriteria(FefuForeignOnlineEntrant.class);
        foreignEntrantCriteria .add(Restrictions.eq(FefuForeignOnlineEntrant.enrollmentCampaign().s(), enrollmentCampaign));
        foreignEntrantCriteria.setProjection(Projections.max(FefuForeignOnlineEntrant.personalNumber().s()));
        Number result = (Number) foreignEntrantCriteria.uniqueResult();

        return (result != null) ? result.intValue() : 0;
    }

}
