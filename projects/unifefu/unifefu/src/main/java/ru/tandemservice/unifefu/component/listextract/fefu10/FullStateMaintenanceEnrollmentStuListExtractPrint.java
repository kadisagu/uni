/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu10;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.Calendar;

/**
 * @author nvankov
 * @since 11/21/13
 */
public class FullStateMaintenanceEnrollmentStuListExtractPrint implements IPrintFormCreator<FullStateMaintenanceEnrollmentStuListExtract>, IListParagraphPrintFormCreator<FullStateMaintenanceEnrollmentStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FullStateMaintenanceEnrollmentStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.put("course", extract.getCourse().getTitle());

        if(DevelopFormCodes.FULL_TIME_FORM.equals(extract.getGroup().getEducationOrgUnit().getDevelopForm().getCode()))
            modifier.put("fefuGroup", " группы " + extract.getGroup().getTitle());
        else
            modifier.put("fefuGroup", "");


        String educationStrDirection;

        EducationLevelsHighSchool educationLevelsHighSchool = extract.getGroup().getEducationOrgUnit().getEducationLevelHighSchool();
        StructureEducationLevels levelType = educationLevelsHighSchool.getEducationLevel().getLevelType();

        if (levelType.isSpecialization()) // специализация
        {
            educationStrDirection = "специальности ";
        }
        else if (levelType.isSpecialty()) // специальность
        {
            educationStrDirection = "специальности ";
        }
        else if (levelType.isBachelor() && levelType.isProfile()) // бакалаврский профиль
        {
            educationStrDirection = "направлению ";
        }
        else if (levelType.isMaster() && levelType.isProfile()) // магистерский профиль
        {
            educationStrDirection = "направлению ";
        }
        else // направление подготовки
        {
            educationStrDirection = "направлению подготовки ";
        }

        String direction = CommonExtractPrint.getFefuHighEduLevelStr(educationLevelsHighSchool.getEducationLevel());

        modifier.put("fefuEducationStrDirection_D", educationStrDirection + direction);


        EducationLevels educationLevels = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
        CommonExtractPrint.modifyEducationStr(modifier, educationLevels, "");

        CommonExtractPrint.initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(), "orgUnit", "");
        CommonExtractPrint.initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, extract.getGroup().getEducationOrgUnit(), CommonListOrderPrint.getEducationBaseText(extract.getGroup()), "fefuShortFastExtendedOptionalText");

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(extract.getEnrollDate());
        modifier.put("enrollDate", calendar.get(Calendar.DAY_OF_MONTH) + " " + CommonBaseDateUtil.getMonthNameDeclined(calendar.get(Calendar.MONTH) + 1, GrammaCase.GENITIVE) + " " + calendar.get(Calendar.YEAR) + " года");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FullStateMaintenanceEnrollmentStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);

        Student student = firstExtract.getEntity();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();

        CommonExtractPrint.initOrgUnit(injectModifier, educationOrgUnit, "formativeOrgUnitStr", "");
        CommonExtractPrint.modifyEducationStr(injectModifier, educationOrgUnit);

        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FullStateMaintenanceEnrollmentStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FullStateMaintenanceEnrollmentStuListExtract firstExtract)
    {
    }
}
