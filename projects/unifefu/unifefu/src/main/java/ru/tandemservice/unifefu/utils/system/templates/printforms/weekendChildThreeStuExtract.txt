\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
О предоставлении отпуска по уходу за {reasonPrint} до 3 лет\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_D}\cell\b0
{studentCategory_D} {course} курса{groupInternal_G}, {custom_learned_D}{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStr} по {fefuEducationStr_D} {orgUnitPrep} {formativeOrgUnitStrWithTerritorial_P} по {developForm_DF} форме обучения, предоставить отпуск по уходу за {reasonPrint} до 3 лет c {beginDate} по {endDate}{freeAttendance}.\par
\par{payBenefit}\par
\par{payOnetimeBenefit}\par
\fi0\cell\row\pard

\keepn\nowidctlpar\qj
Основание: {listBasics}.