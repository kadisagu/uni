/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu13.AddEdit;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressInter;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.e41.utils.EmployeePostVO;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.utils.CachedSingleSelectTextModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 6/20/13
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<SendPracticOutStuExtract, Model>
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected SendPracticOutStuExtract createNewInstance()
    {
        return new SendPracticOutStuExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setPracticeTypeList(MoveStudentDaoFacade.getMoveStudentDao().getStudentPracticeTypes());
        model.setPracticeKindModel(new CachedSingleSelectTextModel(MoveStudentDaoFacade.getMoveStudentDao().getStudentPracticeKinds()));
        model.setEmployeePostVOModel(new UniSimpleAutocompleteModel()
        {
            {
                setColumnTitles(new String[]{"ФИО", "Должность", "Подразделение", "Ученая степень"});
                setLabelProperties(new String[]{"a", "b", "c", "d"});
//                setLabelProperties(new String[] {"employeePost.person.fio", "employee.postRelation.postBoundedWithQGandQL.title", "employeePost.orgUnit.titleWithType", "academicDegree.fullTitle"});
            }

            @Override
            public ListResult findValues(String filter)
            {
                List<EmployeePost> employeePosts = getMQBuilder(filter).getResultList(getSession(), 0, 50);

                List<EmployeePostVO> employeePostVOs = Lists.newArrayList();

                Map<Long, PersonAcademicDegree> personsDegrees = getAcademicDegreesMap(employeePosts);

                for (EmployeePost employeePost : employeePosts)
                {
                    employeePostVOs.add(new EmployeePostVO(employeePost, personsDegrees.get(employeePost.getPerson().getId())));
                }

                return new ListResult<>(employeePostVOs, getMQBuilder(filter).getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                EmployeePost empl = get((Long) primaryKey);
                if (null != empl)
                {
                    if (getMQBuilder(null).getResultList(getSession()).contains(empl))
                        return new EmployeePostVO(empl, getAcademicDegree(empl));
                }
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                if (0 == columnIndex)
                {
                    return ((EmployeePostVO) value).getEmployeePost().getPerson().getFio();
                }
                else if (1 == columnIndex)
                {
                    return ((EmployeePostVO) value).getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getTitle();
                }
                else if (2 == columnIndex)
                {
                    return ((EmployeePostVO) value).getEmployeePost().getOrgUnit().getTitleWithType();
                }
                else if (3 == columnIndex)
                {
                    PersonAcademicDegree academicDegree = ((EmployeePostVO) value).getAcademicDegree();
                    return null != academicDegree ? academicDegree.getAcademicDegree().getTitle() : null;
                }
                else
                    return super.getLabelFor(value, columnIndex);
            }

            private MQBuilder getMQBuilder(String filter)
            {
                MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "e");

                builder.add(MQExpression.eq("e", EmployeePost.L_POST_STATUS + "." + EmployeePostStatus.P_ACTIVE, Boolean.TRUE));
                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like("e", EmployeePost.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FULL_FIO, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("e", EmployeePost.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FULL_FIO, OrderDirection.asc);

                return builder;
            }

        });
        model.setExternalOrgUnitModel(new UniSimpleAutocompleteModel()
        {

            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(getMQBuilder(filter).getResultList(getSession(), 0, 50), getMQBuilder(filter).getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                ExternalOrgUnit ou = get((Long) primaryKey);
                if (null != ou)
                {
                    if (getMQBuilder(null).getResultList(getSession()).contains(ou))
                        return ou;
                }
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                ExternalOrgUnit extOu = (ExternalOrgUnit) value;
                String legalForm = StringUtils.isEmpty(extOu.getLegalForm().getShortTitle()) ? "" : extOu.getLegalForm().getShortTitle();
                StringBuilder extOuStr = new StringBuilder(legalForm).append(StringUtils.isEmpty(legalForm) ? "" : " ").append(extOu.getShortTitle());

                AddressDetailed address =  extOu.getLegalAddress();

                if (null != address && null != address.getSettlement())
                {
                    extOuStr.append(" - ").append(address.getSettlement().getTitleWithType());
                    if (address instanceof AddressRu && null != ((AddressRu) address).getStreet())

                    {
                        extOuStr.append(", ").append(((AddressRu) address).getStreet().getTitleWithType());
                        if (!StringUtils.isEmpty(((AddressRu) address).getHouseNumber()))
                        {
                            extOuStr.append(", д.").append(((AddressRu) address).getHouseNumber());
                            if (!StringUtils.isEmpty(((AddressRu) address).getHouseUnitNumber()))
                            {
                                extOuStr.append("-").append(((AddressRu) address).getHouseUnitNumber());
                            }
                        }
                    }
                    else if (address instanceof AddressInter)
                    {
                        extOuStr.append(", ").append(((AddressInter) address).getAddressLocation());
                    }
                }

                if (!StringUtils.isEmpty(extOu.getPhone()))
                    extOuStr.append(", тел. ").append(extOu.getPhone());

                if (!StringUtils.isEmpty(extOu.getEmail()))
                    extOuStr.append(", ").append(extOu.getEmail());

                return extOuStr.toString();
            }

            private MQBuilder getMQBuilder(String filter)
            {
                MQBuilder builder = new MQBuilder(ExternalOrgUnit.ENTITY_CLASS, "ou");

                builder.add(MQExpression.eq("ou", ExternalOrgUnit.P_ACTIVE, Boolean.TRUE));
                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like("ou", ExternalOrgUnit.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("ou", ExternalOrgUnit.P_TITLE, OrderDirection.asc);

                return builder;
            }

        });
        model.setPracticeContractModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {

                ExternalOrgUnit externalOrgUnit = model.getExtract().getPracticeExtOrgUnit();
                if (null == externalOrgUnit)
                    return ListResult.getEmpty();

                return new ListResult<>(getMQBuilder(filter).getResultList(getSession(), 0, 50), getMQBuilder(filter).getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                FefuPracticeContractWithExtOu pc = get((Long) primaryKey);
                if (null != pc)
                {
                    if (getMQBuilder(null).getResultList(getSession()).contains(pc))
                        return pc;
                }
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((FefuPracticeContractWithExtOu) value).getContractNumWithDate();
            }

            private MQBuilder getMQBuilder(String filter)
            {
                ExternalOrgUnit externalOrgUnit = model.getExtract().getPracticeExtOrgUnit();

                MQBuilder builder = new MQBuilder(FefuPracticeContractWithExtOu.ENTITY_CLASS, "pc");
                builder.add(MQExpression.eq("pc", FefuPracticeContractWithExtOu.P_ARCHIVAL, false));
                if (null != externalOrgUnit)
                    builder.add(MQExpression.eq("pc", FefuPracticeContractWithExtOu.L_EXTERNAL_ORG_UNIT + "." + ExternalOrgUnit.P_ID, externalOrgUnit.getId()));
                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like("pc", FefuPracticeContractWithExtOu.P_CONTRACT_NUM, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("pc", FefuPracticeContractWithExtOu.P_CONTRACT_NUM, OrderDirection.asc);

                return builder;
            }
        });

        if (model.isEditForm())
        {
            model.setPracticeType(MoveStudentDaoFacade.getMoveStudentDao().extractPracticeType(model.getExtract().getPracticeKind()));
            model.setPracticeKind(MoveStudentDaoFacade.getMoveStudentDao().extractPracticeKind(model.getExtract().getPracticeKind()));

            if(null != model.getExtract().getPreventAccidentsIC())
            {
                model.setPreventAccidentsIC(new EmployeePostVO(model.getExtract().getPreventAccidentsIC(), model.getExtract().getPreventAccidentsICDegree()));
            }

            if(null != model.getExtract().getResponsForRecieveCash())
            {
                model.setResponsForRecieveCash(new EmployeePostVO(model.getExtract().getResponsForRecieveCash(), model.getExtract().getResponsForRecieveCashDegree()));
            }


            if(null != model.getExtract().getPracticeHeaderInner())
            {
                model.setPracticeHeaderInner(new EmployeePostVO(model.getExtract().getPracticeHeaderInner(), model.getExtract().getPracticeHeaderInnerDegree()));
            }
        }
        else
        {
            model.getExtract().setPracticeCourse(model.getExtract().getEntity().getCourse());
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if (model.getPracticeHeaderInner() == null && StringUtils.isEmpty(model.getExtract().getPracticeHeaderInnerStr()))
            errors.add("Поле «Руководитель практики от ОУ» или поле «Руководитель практики от ОУ(печать)» обязательно для заполнения.", "practiceHeaderInner", "practiceHeaderInnerStr");


        if (null == model.getPreventAccidentsIC() && StringUtils.isEmpty(model.getExtract().getPreventAccidentsICStr()))
            errors.add("Поле «Ответственный за соблюдение техники безопасности» или поле «Ответственный за соблюдение техники безопасности(печать)» обязательно для заполнения.", "preventAccidentsIC", "preventAccidentsICStr");

        if(model.getExtract().isProvideFundsAccordingToEstimates())
        {
            if (null == model.getResponsForRecieveCash() && StringUtils.isEmpty(model.getExtract().getResponsForRecieveCashStr()))
                errors.add("Поле «Ответственный за получение денежных средств» или поле «Ответственный за получение денежных средств(печать)» обязательно для заполнения.", "responsForRecieveCash", "responsForRecieveCashStr");
        }
        if (!model.getExtract().getPracticeBeginDate().before(model.getExtract().getPracticeEndDate()))
            errors.add("Дата окончания должна быть позже даты начала", "practiceBeginDate", "practiceEndDate");
        if (model.getExtract().getAttestationDate().before(model.getExtract().getPracticeEndDate()))
            errors.add("Дата аттестации должна быть позже даты окончания практики", "attestationDate", "practiceEndDate");
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setPracticeKind(MoveStudentDaoFacade.getMoveStudentDao().getFullPracticeKind(model.getPracticeType(), model.getPracticeKind()));

        super.update(model);

        if(null != model.getResponsForRecieveCash())
        {
            model.getExtract().setResponsForRecieveCash(model.getResponsForRecieveCash().getEmployeePost());
            model.getExtract().setResponsForRecieveCashDegree(model.getResponsForRecieveCash().getAcademicDegree());
        }
        else
        {
            model.getExtract().setResponsForRecieveCash(null);
            model.getExtract().setResponsForRecieveCashDegree(null);
        }

        if(null != model.getPreventAccidentsIC())
        {
            model.getExtract().setPreventAccidentsIC(model.getPreventAccidentsIC().getEmployeePost());
            model.getExtract().setPreventAccidentsICDegree(model.getPreventAccidentsIC().getAcademicDegree());
        }
        else
        {
            model.getExtract().setPreventAccidentsIC(null);
            model.getExtract().setPreventAccidentsICDegree(null);
        }

        if(null != model.getPracticeHeaderInner())
        {
            model.getExtract().setPracticeHeaderInner(model.getPracticeHeaderInner().getEmployeePost());
            model.getExtract().setPracticeHeaderInnerDegree(model.getPracticeHeaderInner().getAcademicDegree());
        }
        else
        {
            model.getExtract().setPreventAccidentsIC(null);
            model.getExtract().setPreventAccidentsICDegree(null);
        }
    }


    private Map<Long, PersonAcademicDegree> getAcademicDegreesMap(List<EmployeePost> employeePosts)
    {
        Map<Long, PersonAcademicDegree> academicDegreesMap = Maps.newHashMap();

        Map<Long, List<PersonAcademicDegree>> academicDegreesListMap = Maps.newHashMap();

        List<Long> personsIds = Lists.newArrayList();

        for (EmployeePost employeePost : employeePosts)
        {
            personsIds.add(employeePost.getPerson().getId());
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "d");
        builder.where(DQLExpressions.in(DQLExpressions.property("d", PersonAcademicDegree.person().id()), personsIds));

        List<PersonAcademicDegree> academicDegreeList = builder.createStatement(getSession()).list();

        for (PersonAcademicDegree degree : academicDegreeList)
        {
            if (null == academicDegreesListMap.get(degree.getPerson().getId()))
            {
                List<PersonAcademicDegree> degrees = Lists.newArrayList();
                degrees.add(degree);
                academicDegreesListMap.put(degree.getPerson().getId(), degrees);
            }
            else
            {
                List<PersonAcademicDegree> degrees = academicDegreesListMap.get(degree.getPerson().getId());
                List<Long> degreesIds = EntityUtils.getIdsFromEntityList(degrees);
                if (!degreesIds.contains(degree.getId()))
                {
                    degrees.add(degree);
                }
            }
        }

        for (Long key : academicDegreesListMap.keySet())
        {
            PersonAcademicDegree personAcademicDegree = null;

            List<PersonAcademicDegree> degrees = academicDegreesListMap.get(key);
            if (!degrees.isEmpty())
            {
                personAcademicDegree = degrees.get(degrees.size() - 1);

                boolean containsWithoutDate = true;
                for (PersonAcademicDegree academicDegree : degrees)
                {
                    if (null == academicDegree.getIssuanceDate())
                    {
                        containsWithoutDate = false;
                        break;
                    }
                }

                if (!containsWithoutDate)
                {
                    EntityComparator<PersonAcademicDegree> comparator = new EntityComparator<>(new EntityOrder(PersonAcademicDegree.P_ISSUANCE_DATE));
                    Collections.sort(degrees, comparator);
                    personAcademicDegree = degrees.get(degrees.size() - 1);
                }
            }
            if (null != personAcademicDegree)
            {
                academicDegreesMap.put(key, personAcademicDegree);
            }
        }
        return academicDegreesMap;
    }

    public PersonAcademicDegree getAcademicDegree(EmployeePost employeePost)
    {
        DQLSelectBuilder degreeBuilder = new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "d");
        degreeBuilder.where(DQLExpressions.eq(DQLExpressions.property("d", PersonAcademicDegree.person().id()), DQLExpressions.value(employeePost.getPerson().getId())));
        degreeBuilder.order(DQLExpressions.property("d", PersonAcademicDegree.id()));

        List<PersonAcademicDegree> academicDegrees = degreeBuilder.createStatement(getSession()).list();

        PersonAcademicDegree personAcademicDegree = null;

        if (!academicDegrees.isEmpty())
        {
            personAcademicDegree = academicDegrees.get(academicDegrees.size() - 1);

            boolean containsWithoutDate = true;
            for (PersonAcademicDegree academicDegree : academicDegrees)
            {
                if (null == academicDegree.getIssuanceDate())
                {
                    containsWithoutDate = false;
                    break;
                }
            }

            if (!containsWithoutDate)
            {
                EntityComparator<PersonAcademicDegree> comparator = new EntityComparator<>(new EntityOrder(PersonAcademicDegree.P_ISSUANCE_DATE));
                Collections.sort(academicDegrees, comparator);
                personAcademicDegree = academicDegrees.get(academicDegrees.size() - 1);
            }
        }
        return personAcademicDegree;
    }
}
