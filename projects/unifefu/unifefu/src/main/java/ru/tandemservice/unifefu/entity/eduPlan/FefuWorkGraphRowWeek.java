package ru.tandemservice.unifefu.entity.eduPlan;

import ru.tandemservice.unifefu.entity.eduPlan.gen.*;

/**
 * ГУП (Неделя строки курса)
 */
public class FefuWorkGraphRowWeek extends FefuWorkGraphRowWeekGen
{
    public FefuWorkGraphRowWeek()
    {

    }

    public FefuWorkGraphRowWeek(FefuWorkGraphRow row, int week)
    {
        this.setRow(row);
        this.setWeek(week);
    }
}