package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x8_14to15 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuAdditionalProfessionalEducationProgramForStudentIndividualParameters

		// создана новая сущность
        if (!tool.tableExists("fefu_ape_program_ind_params_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_ape_program_ind_params_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("individualdevelopform_id", DBType.LONG).setNullable(false), 
				new DBColumn("individualdevelopcondition_id", DBType.LONG).setNullable(false), 
				new DBColumn("individualdeveloptech_id", DBType.LONG).setNullable(false), 
				new DBColumn("reason_p", DBType.createVarchar(255)), 
				new DBColumn("subject_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuAdditionalProfessionalEducationProgramForStudentIndividualParameters");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuAdditionalProfessionalEducationProgramForStudent

		// удалено свойство reason
        if (tool.columnExists("fefu_ape_program_for_student_t", "reason_p"))
		{
			// удалить колонку
			tool.dropColumn("fefu_ape_program_for_student_t", "reason_p");

		}

		// удалено свойство individualDevelopForm
        if (tool.columnExists("fefu_ape_program_for_student_t", "individualdevelopform_id"))
		{
			// удалить колонку
			tool.dropColumn("fefu_ape_program_for_student_t", "individualdevelopform_id");

		}

		// удалено свойство individualDevelopCondition
        if (tool.columnExists("fefu_ape_program_for_student_t", "individualdevelopcondition_id"))
		{
			// удалить колонку
			tool.dropColumn("fefu_ape_program_for_student_t", "individualdevelopcondition_id");

		}

		// удалено свойство individualDevelopTech
        if (tool.columnExists("fefu_ape_program_for_student_t", "individualdeveloptech_id"))
		{
			// удалить колонку
			tool.dropColumn("fefu_ape_program_for_student_t", "individualdeveloptech_id");

		}

		// создано свойство individualParameters
        if (!tool.columnExists("fefu_ape_program_for_student_t", "individualparameters_id"))
		{
			// создать колонку
			tool.createColumn("fefu_ape_program_for_student_t", new DBColumn("individualparameters_id", DBType.LONG));

		}
    }
}