/**
 * ReferencesUpdateAsync.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class ReferencesUpdateAsync  implements java.io.Serializable {
    private java.lang.String XMLPackage;

    private java.lang.String ISCode;

    private java.lang.Boolean fullSync;

    public ReferencesUpdateAsync() {
    }

    public ReferencesUpdateAsync(
           java.lang.String XMLPackage,
           java.lang.String ISCode,
           java.lang.Boolean fullSync) {
           this.XMLPackage = XMLPackage;
           this.ISCode = ISCode;
           this.fullSync = fullSync;
    }


    /**
     * Gets the XMLPackage value for this ReferencesUpdateAsync.
     * 
     * @return XMLPackage
     */
    public java.lang.String getXMLPackage() {
        return XMLPackage;
    }


    /**
     * Sets the XMLPackage value for this ReferencesUpdateAsync.
     * 
     * @param XMLPackage
     */
    public void setXMLPackage(java.lang.String XMLPackage) {
        this.XMLPackage = XMLPackage;
    }


    /**
     * Gets the ISCode value for this ReferencesUpdateAsync.
     * 
     * @return ISCode
     */
    public java.lang.String getISCode() {
        return ISCode;
    }


    /**
     * Sets the ISCode value for this ReferencesUpdateAsync.
     * 
     * @param ISCode
     */
    public void setISCode(java.lang.String ISCode) {
        this.ISCode = ISCode;
    }


    /**
     * Gets the fullSync value for this ReferencesUpdateAsync.
     * 
     * @return fullSync
     */
    public java.lang.Boolean getFullSync() {
        return fullSync;
    }


    /**
     * Sets the fullSync value for this ReferencesUpdateAsync.
     * 
     * @param fullSync
     */
    public void setFullSync(java.lang.Boolean fullSync) {
        this.fullSync = fullSync;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReferencesUpdateAsync)) return false;
        ReferencesUpdateAsync other = (ReferencesUpdateAsync) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.XMLPackage==null && other.getXMLPackage()==null) || 
             (this.XMLPackage!=null &&
              this.XMLPackage.equals(other.getXMLPackage()))) &&
            ((this.ISCode==null && other.getISCode()==null) || 
             (this.ISCode!=null &&
              this.ISCode.equals(other.getISCode()))) &&
            ((this.fullSync==null && other.getFullSync()==null) || 
             (this.fullSync!=null &&
              this.fullSync.equals(other.getFullSync())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getXMLPackage() != null) {
            _hashCode += getXMLPackage().hashCode();
        }
        if (getISCode() != null) {
            _hashCode += getISCode().hashCode();
        }
        if (getFullSync() != null) {
            _hashCode += getFullSync().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReferencesUpdateAsync.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">ReferencesUpdateAsync"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("XMLPackage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "XMLPackage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "ISCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fullSync");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "FullSync"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
