/* $Id: EppExtViewProvider4RegModule.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.report.externalView;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.uniepp.entity.registry.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Victor Nekrasov
 * @since 24.04.2014
 */

public class EppExtViewProvider4RegModule extends SimpleDQLExternalViewConfig
{
	@Override
	protected DQLSelectBuilder buildDqlQuery()
	{
		DQLSelectBuilder dql = new DQLSelectBuilder()
				.fromEntity(EppRegistryElementPartModule.class, "regPartModule")
				.joinPath(DQLJoinType.left, EppRegistryElementPartModule.part().fromAlias("regPartModule"), "elementPart")
				.joinPath(DQLJoinType.left, EppRegistryElementPart.registryElement().fromAlias("elementPart"), "regElement")
				.joinPath(DQLJoinType.left, EppRegistryElementPartModule.module().fromAlias("regPartModule"), "regModule")

				.joinEntity("regModule", DQLJoinType.left, EppRegistryModuleALoad.class, "moduleLoad",
				            eq(property("regModule"), property(EppRegistryModuleALoad.module().fromAlias("moduleLoad"))));

		column(dql, property("regModule", EppRegistryModule.id()), "moduleId").comment("ID модуля");
		column(dql, property("regModule", EppRegistryModule.number()), "moduleNumber").comment("Порядковый номер модуля");
		column(dql, property("elementPart", EppRegistryElementPart.id()), "partId").comment("ID части");
		column(dql, property("regElement", EppRegistryElement.id()), "elementId").comment("ID реестровой записи");

		column(dql, property("moduleLoad", EppRegistryModuleALoad.loadType().code()), "loadTypeCode").comment("код типа нагрузки");
		column(dql, property("moduleLoad", EppRegistryModuleALoad.loadType().title()), "loadTypeTitle").comment("наименование типа нагрузки");
		column(dql, property("moduleLoad", EppRegistryModuleALoad.load()), "loadValue").comment("часов нагрузки (*100)");

		return dql;
	}
}

