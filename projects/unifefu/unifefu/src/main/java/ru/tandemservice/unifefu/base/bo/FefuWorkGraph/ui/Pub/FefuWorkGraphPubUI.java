/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.SimpleStyleResolver;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.RichDataSourceModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.year.IEppYearDAO;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.util.WeekTypeLegendRow;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.FefuWorkGraphManager;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraph;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow;
import ru.tandemservice.unifefu.tapestry.richTableList.FefuRangeSelectionWeekTypeListDataSource;
import ru.tandemservice.unifefu.tapestry.richTableList.FefuWeekTypeBlockColumn;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "workGraph.id", required = true),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class FefuWorkGraphPubUI extends UIPresenter
{
    private FefuWorkGraph _workGraph = new FefuWorkGraph();
    private String _selectedTab = FefuWorkGraphPub.WORK_GRAPH_TAB;

    public FefuWorkGraph getWorkGraph(){ return _workGraph; }
    public void setWorkGraph(FefuWorkGraph workGraph){ _workGraph = workGraph; }

    public String getSelectedTab(){ return _selectedTab; }
    public void setSelectedTab(String selectedTab){ _selectedTab = selectedTab; }

    /* legacy */
    private Map<EduProgramSubject, FefuRangeSelectionWeekTypeListDataSource> _id2dataSource;
    private RichDataSourceModel<IEntity> _headerDataSource;
    private EduProgramSubject _currentProgramSubject;
    private List<WeekTypeLegendRow> _weekTypeLegendList;
    private WeekTypeLegendRow _weekTypeLegendItem;
    private EppYearEducationWeek[] _weekData;

    public Map<EduProgramSubject, FefuRangeSelectionWeekTypeListDataSource> getId2dataSource(){ return _id2dataSource; }
    public void setId2dataSource(Map<EduProgramSubject, FefuRangeSelectionWeekTypeListDataSource> id2dataSource){ _id2dataSource = id2dataSource; }

    public RichDataSourceModel<IEntity> getHeaderDataSource(){ return _headerDataSource; }
    public void setHeaderDataSource(RichDataSourceModel<IEntity> headerDataSource){ _headerDataSource = headerDataSource; }

    public EduProgramSubject getCurrentProgramSubject(){ return _currentProgramSubject; }
    public void setCurrentProgramSubject(EduProgramSubject currentProgramSubject){ _currentProgramSubject = currentProgramSubject; }

    public List<WeekTypeLegendRow> getWeekTypeLegendList(){ return _weekTypeLegendList; }
    public void setWeekTypeLegendList(List<WeekTypeLegendRow> weekTypeLegendList){ _weekTypeLegendList = weekTypeLegendList; }

    public WeekTypeLegendRow getWeekTypeLegendItem(){ return _weekTypeLegendItem; }
    public void setWeekTypeLegendItem(WeekTypeLegendRow weekTypeLegendItem){ _weekTypeLegendItem = weekTypeLegendItem; }

    public EppYearEducationWeek[] getWeekData(){ return _weekData; }
    public void setWeekData(EppYearEducationWeek[] weekData){ _weekData = weekData; }


    public String getDeleteAlertMessage()
    {
        return getConfig().getProperty("ui.delete.alert", _workGraph.getTitle());
    }



    @Override
    public void onComponentRefresh()
    {
        _workGraph = FefuWorkGraphManager.instance().dao().get(FefuWorkGraph.class, _workGraph.getId());
        prepareDSList();
    }

    private void prepareDSList()
    {
        _weekTypeLegendList = IEppEduPlanDAO.instance.get().getWeekTypeLegendRowList(null);
        _weekData = IEppYearDAO.instance.get().getYearEducationWeeks(_workGraph.getYear().getId());
        final AbstractColumn courseColumn = new SimpleColumn("Курс", FefuWorkGraphRow.course().intValue().s()).setClickable(false).setOrderable(false);
        courseColumn.setStyleResolver(new SimpleStyleResolver("min-width:50px;"));

        List<Course> courses = getSettings().get("course");
        _id2dataSource = FefuWorkGraphManager.instance().dao().getWorkGraphEduLevelDataSourceMap(_workGraph, courses, getSettings().<EduProgramSubject>get("programSubject"));

        // создаем шапку всех таблиц ГУП
        if (!_id2dataSource.isEmpty())
        {
            final AbstractListDataSource<IEntity> dataSource = new SimpleListDataSource<>(Collections.<IEntity> emptyList());
            dataSource.setCountRow(0);

            dataSource.addColumn(courseColumn);
            for (final EppYearEducationWeek week : _weekData)
            {
                final HeadColumn weekColumn = new HeadColumn(Integer.toString(week.getNumber()), week.getTitle());
                weekColumn.setVerticalHeader(true);

                final SimpleColumn column = new SimpleColumn(Integer.toString(week.getNumber()), null);
                column.setHeaderStyle("padding-left:0;padding-right:0;text-align:center;width:20.2px;font-size:11px;");
                column.setStyleResolver(new SimpleStyleResolver("padding-left:0;padding-right:0;text-align:center;width:20.2px;font-size:11px;"));

                weekColumn.addColumn(column);
                dataSource.addColumn(weekColumn);
            }

            _headerDataSource = new RichDataSourceModel<>(dataSource);
        }

        // создаем колонки для всех RangeSelectionListDataSource
        for (final FefuRangeSelectionWeekTypeListDataSource rangeModel : _id2dataSource.values())
        {
            // создаем колонки
            final AbstractListDataSource dataSource = rangeModel.getDataSource();
            dataSource.addColumn(courseColumn);
            for (final EppYearEducationWeek week : _weekData)
            {
                FefuWeekTypeBlockColumn column = new FefuWeekTypeBlockColumn(week.getId(), week.getNumber() - 1, Integer.toString(week.getNumber()));
                column.setHeaderStyle("padding-left:0;padding-right:0;text-align:center;width:20px;font-size:11px;");
                column.setStyleResolver(new SimpleStyleResolver("padding-left:0;padding-right:0;text-align:center;width:20px;font-size:11px;"));
                dataSource.addColumn(column);
            }
        }
    }


    public void onClickSearch()
    {
        _uiSettings.save();
        prepareDSList();
    }

    public void onClickClear()
    {
        _uiSettings.clear();
        onClickSearch();
    }

    public void onClickDelete()
    {
        FefuWorkGraphManager.instance().dao().delete(_workGraph.getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(FefuWorkGraphManager.BIND_WORK_GRAPH, _workGraph.getId());
    }
}