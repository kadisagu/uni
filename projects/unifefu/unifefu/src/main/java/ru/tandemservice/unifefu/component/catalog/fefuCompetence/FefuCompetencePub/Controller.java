/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.catalog.fefuCompetence.FefuCompetencePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class Controller extends DefaultCatalogPubController<FefuCompetence, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<FefuCompetence> createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<FefuCompetence> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", FefuCompetence.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Группа компетенций", FefuCompetence.L_EPP_SKILL_GROUP + "." + EppSkillGroup.P_TITLE).setClickable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", FefuCompetence.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        return dataSource;
    }
}