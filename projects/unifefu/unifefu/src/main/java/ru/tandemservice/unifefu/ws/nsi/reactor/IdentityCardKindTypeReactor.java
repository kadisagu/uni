/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.datagram.IdentityCardKindType;

import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 25.08.2013
 */
public class IdentityCardKindTypeReactor extends SimpleCatalogEntityReactor<IdentityCardKindType, IdentityCardType>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<IdentityCardType> getEntityClass()
    {
        return IdentityCardType.class;
    }

    @Override
    public Class<IdentityCardKindType> getNSIEntityClass()
    {
        return IdentityCardKindType.class;
    }

    @Override
    public String getGUID(IdentityCardKindType nsiEntity)
    {
        return nsiEntity.getID();
    }

    @Override
    public IdentityCardKindType getCatalogElementRetrieveRequestObject(String guid)
    {
        IdentityCardKindType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createIdentityCardKindType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public IdentityCardType getPossibleDuplicate(IdentityCardKindType nsiEntity, Map<String, FefuNsiIds> nsiIdsMap, Map<String, IdentityCardType> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getIdentityCardTypeName()) return null;

        // Проверяем, есть ли в базе аналогичные переданной сущности
        IdentityCardType possibleDuplicate = null;

        if (nsiIdsMap.containsKey(nsiEntity.getID()))
        {
            possibleDuplicate = similarEntityMap.get(String.valueOf(nsiIdsMap.get(nsiEntity.getID()).getEntityId()));
        }

        if (null == possibleDuplicate)
        {
            String titleIdx = nsiEntity.getIdentityCardTypeName().trim().toUpperCase();
            if (similarEntityMap.containsKey(titleIdx)) possibleDuplicate = similarEntityMap.get(titleIdx);
        }

        return possibleDuplicate;
    }

    @Override
    public boolean isAnythingChanged(IdentityCardKindType nsiEntity, IdentityCardType entity, Map<String, IdentityCardType> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getIdentityCardTypeName()) return false;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(similarEntityMap, nsiEntity.getIdentityCardTypeName(), entity.getTitle()))
                return true;
        }
        return false;
    }

    @Override
    public IdentityCardKindType createEntity(IdentityCardType entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        IdentityCardKindType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createIdentityCardKindType();
        nsiEntity.setID(getGUID(entity, nsiIdsMap));
        nsiEntity.setIdentityCardTypeName(entity.getTitle());
        nsiEntity.setIdentityCardKindID(entity.getUserCode());
        return nsiEntity;
    }

    @Override
    public IdentityCardType createEntity(IdentityCardKindType nsiEntity, Map<String, IdentityCardType> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getIdentityCardTypeName()) return null;

        IdentityCardType entity = new IdentityCardType();
        entity.setTitle(nsiEntity.getIdentityCardTypeName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(IdentityCardType.class));

        if (!similarEntityMap.containsKey(FefuNsiSyncDAO.USER_CODE_PREFIX + nsiEntity.getIdentityCardKindID()))
            entity.setUserCode(nsiEntity.getIdentityCardKindID());

        return entity;
    }

    @Override
    public IdentityCardType updatePossibleDuplicateFields(IdentityCardKindType nsiEntity, IdentityCardType entity, Map<String, IdentityCardType> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getIdentityCardTypeName()) return null;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(similarEntityMap, nsiEntity.getIdentityCardTypeName(), entity.getTitle()))
                entity.setTitle(nsiEntity.getIdentityCardTypeName());

            if (NsiReactorUtils.isShouldBeUpdated(similarEntityMap, nsiEntity.getIdentityCardKindID(), entity.getUserCode()))
                entity.setUserCode(nsiEntity.getIdentityCardKindID());
        }
        return entity;
    }

    @Override
    public IdentityCardKindType updateNsiEntityFields(IdentityCardKindType nsiEntity, IdentityCardType entity)
    {
        nsiEntity.setIdentityCardTypeName(entity.getTitle());
        nsiEntity.setIdentityCardKindID(entity.getUserCode());
        return nsiEntity;
    }
}