/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSchedule.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.unifefu.base.vo.FefuSchedulePrintDataVO;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 7/17/13
 */
public class GroupComboDSHandler extends SimpleTitledComboDataSourceHandler
{
    public GroupComboDSHandler(String ownerId)
    {
        super(ownerId);
        _filtered = true;
    }

    public enum Columns
    {
        FORMATIVE_ORG_UNIT(OrgUnit.class, Group.educationOrgUnit().formativeOrgUnit(), Group.educationOrgUnit().formativeOrgUnit().id(), Group.educationOrgUnit().formativeOrgUnit().title(), OrgUnit.title()),
        TERRITORIAL_ORG_UNIT(OrgUnit.class, Group.educationOrgUnit().territorialOrgUnit(), Group.educationOrgUnit().territorialOrgUnit().id(), Group.educationOrgUnit().territorialOrgUnit().territorialFullTitle(), OrgUnit.territorialFullTitle()),
        EDUCATION_LEVEL(EducationLevelsHighSchool.class, Group.educationOrgUnit().educationLevelHighSchool(), Group.educationOrgUnit().educationLevelHighSchool().id(), Group.educationOrgUnit().educationLevelHighSchool().title(), EducationLevelsHighSchool.title()),
        DEVELOP_FORM(DevelopForm.class, Group.educationOrgUnit().developForm(), Group.educationOrgUnit().developForm().id(), Group.educationOrgUnit().developForm().title(), DevelopForm.title()),
        COURSE(Course.class, Group.course(), Group.course().id(), Group.course().title(), Course.intValue());

        Columns(Class entityClass, MetaDSLPath column, MetaDSLPath idColumn, MetaDSLPath orderPath, MetaDSLPath filterPath)
        {
            _entityClass = entityClass;
            _column = column;
            _orderPath = orderPath;
            _filterPath = filterPath;
            _idColumn = idColumn;
        }

        private Class _entityClass;
        private MetaDSLPath _column;
        private MetaDSLPath _idColumn;
        private MetaDSLPath _orderPath;
        private MetaDSLPath _filterPath;

        public MetaDSLPath getColumn()
        {
            return _column;
        }

        public MetaDSLPath getOrderPath()
        {
            return _orderPath;
        }

        public void setOrderPath(MetaDSLPath orderPath)
        {
            _orderPath = orderPath;
        }

        public void setColumn(MetaDSLPath column)
        {
            _column = column;
        }

        public MetaDSLPath getFilterPath()
        {
            return _filterPath;
        }

        public void setFilterPath(MetaDSLPath filterPath)
        {
            _filterPath = filterPath;
        }

        public MetaDSLPath getIdColumn()
        {
            return _idColumn;
        }

        public void setIdColumn(MetaDSLPath idColumn)
        {
            _idColumn = idColumn;
        }

        public Class getEntityClass()
        {
            return _entityClass;
        }

        public void setEntityClass(Class entityClass)
        {
            _entityClass = entityClass;
        }
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Columns column = context.get("column");
        FefuSchedulePrintDataVO scheduleData = context.get("scheduleData");

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
        subBuilder.distinct();
        subBuilder.column(property("g", column.getIdColumn()));

        if (null != scheduleData.getCurrentOrgUnit())
        {
            Long orgUnitId = scheduleData.getCurrentOrgUnit().getId();
            DQLSelectBuilder kindRelationsBuilder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                    .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                    .where(eq(property("rel", OrgUnitToKindRelation.orgUnit().id()), value(orgUnitId)));
            List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

            Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                    .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

            IDQLExpression condition = nothing(); // false
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                condition = or(condition, eq(property("g", Group.educationOrgUnit().educationLevelHighSchool().orgUnit().id()), value(orgUnitId)));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                condition = or(condition, eq(property("g", Group.educationOrgUnit().formativeOrgUnit().id()), value(orgUnitId)));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                condition = or(condition, eq(property("g", Group.educationOrgUnit().territorialOrgUnit().id()), value(orgUnitId)));

            subBuilder.where(condition);
        }

        if (!Columns.FORMATIVE_ORG_UNIT.equals(column))
        {
            if (null == scheduleData.getFormativeOrgUnit())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            if (null != scheduleData.getFormativeOrgUnit())
                subBuilder.where(eq(property("g", Group.educationOrgUnit().formativeOrgUnit()), value(scheduleData.getFormativeOrgUnit())));
        }
        if (!Columns.FORMATIVE_ORG_UNIT.equals(column) && !Columns.TERRITORIAL_ORG_UNIT.equals(column))
        {
            if (null == scheduleData.getFormativeOrgUnit() && null == scheduleData.getTerritorialOrgUnit())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            if (null != scheduleData.getTerritorialOrgUnit())
                subBuilder.where(eq(property("g", Group.educationOrgUnit().territorialOrgUnit()), value(scheduleData.getTerritorialOrgUnit())));
        }
        if (!Columns.FORMATIVE_ORG_UNIT.equals(column) && !Columns.TERRITORIAL_ORG_UNIT.equals(column) && !Columns.EDUCATION_LEVEL.equals(column))
        {
            if (null == scheduleData.getFormativeOrgUnit() && null == scheduleData.getTerritorialOrgUnit())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            if (null != scheduleData.getEduLevels() && !scheduleData.getEduLevels().isEmpty())
                subBuilder.where(in(property("g", Group.educationOrgUnit().educationLevelHighSchool()), scheduleData.getEduLevels()));
        }
        if (!Columns.FORMATIVE_ORG_UNIT.equals(column) && !Columns.TERRITORIAL_ORG_UNIT.equals(column) && !Columns.EDUCATION_LEVEL.equals(column) && !Columns.DEVELOP_FORM.equals(column))
        {
            if (null == scheduleData.getFormativeOrgUnit() && null == scheduleData.getTerritorialOrgUnit())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            if (null != scheduleData.getDevelopForm())
                subBuilder.where(eq(property("g", Group.educationOrgUnit().developForm()), value(scheduleData.getDevelopForm())));
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(column.getEntityClass(), "e");
//        if (null != column.getColumn())
//            builder.column(property("gr", column.getColumn()));
        builder.where(in(property("e.id"), subBuilder.buildQuery()));

        setOrderByProperty(column.getOrderPath().s());
        setFilterByProperty(column.getFilterPath().s());
        context.put(UIDefines.COMBO_OBJECT_LIST, createStatement(builder).list());
        return super.execute(input, context);
    }
}
