/**
 *$Id: FefuStudentVipDataDao.java 38584 2014-10-08 11:05:36Z azhebko $
 */
package ru.tandemservice.unifefu.base.bo.FefuStudent.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.PersonFefuExt;
import ru.tandemservice.unifefu.entity.gen.PersonFefuExtGen;

import java.util.Date;

/**
 * @author Alexander Shaburov
 * @since 30.08.12
 */
public class FefuStudentVipDataDao extends CommonDAO implements IFefuStudentVipDataDao
{
    @Override
    public PersonFefuExt preparePersonFefuExt(Person person)
    {
        PersonFefuExt personFefuExt = getByNaturalId(new PersonFefuExtGen.NaturalId(person));
        if (personFefuExt == null)
            personFefuExt = new PersonFefuExt(person);

        return personFefuExt;
    }

    @Override
    public void update(PersonFefuExt personFefuExt, boolean checkVip)
    {
        if (!checkVip)
        {
            personFefuExt.setVipDate(null);
            personFefuExt.setCommentVip(null);
            personFefuExt.setFioCuratorVip(null);
        }
        else
        {
            if (personFefuExt.getVipDate() == null)
                personFefuExt.setVipDate(new Date());
        }

        saveOrUpdate(personFefuExt);
    }
}
