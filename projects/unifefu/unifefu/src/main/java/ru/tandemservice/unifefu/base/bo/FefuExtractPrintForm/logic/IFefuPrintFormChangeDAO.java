/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.logic;

import org.apache.tapestry.request.IUploadFile;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public interface IFefuPrintFormChangeDAO
{
    void saveOrUpdatePrintForm(AbstractStudentOrder order, IUploadFile uploadFile);

    void deletePrintForm(AbstractStudentOrder order);
}