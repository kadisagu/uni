/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.imtsa;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alexander Zhebko
 * @since 17.05.2013
 */
public class ImtsaFile extends ImtsaLogable implements Iterable<ImtsaFile.ImtsaNode>
{
    private static boolean mergeSelfWorkFromanotherSources = Boolean.valueOf(System.getProperty("mergeSelfWork", "true"));

    public static final String KIND_CODE_DISCIPLINE = "eppRegistryDiscipline";
    public static final String KIND_CODE_PRACTICE = "eppRegistryPractice";
    public static final String KIND_CODE_PRACTICE_TEACH = "eppRegistryPractice.1";

    public static final String TYPE_LOAD_WEEKS = "ld.weeks";
    public static final String TYPE_LOAD_A_LABORATORY = "ld.aload.lab";
    public static final String TYPE_LOAD_A_PRACTICE = "ld.aload.pra";
    public static final String TYPE_LOAD_A_LECTURES = "ld.aload.lec";
    public static final String TYPE_LOAD_SELFWORK = "ld.selfw";
    public static final String TYPE_LOAD_LABOR = "ld.labor";
    public static final String TYPE_LOAD_TOTAL = "ld.total";

    public static final String TYPE_CONTROL_COURSE_PROJECT = "ca.cproj";
    public static final String TYPE_CONTROL_COURSE_WORK = "ca.cwork";
    public static final String TYPE_CONTROL_TEST = "ca.test";
    public static final String TYPE_CONTROL_EXAM = "ca.exam";

    public static final String VARIANT_INDEX_PART_PREFIX = "ДВ";
    public static final Pattern VARIANT_INDEX_PART_MASK = Pattern.compile("^" + VARIANT_INDEX_PART_PREFIX + "(\\d+)$");
    public static final Pattern NUMBER_INDEX_PART_MASK = Pattern.compile("^(\\d+)$");


    public static class NodeCodeMapHolder
    {
        private static Map<String, String> nodeCodeMapOriginal;
        private static Map<String, String> getNodeCodeMapOriginal()
        {
            if (nodeCodeMapOriginal == null)
            {
                nodeCodeMapOriginal = new TreeMap<String, String>()
                {
                    {
                        log4j_logger.info("---- codes from properties -----------------------------------------");
                        try
                        {
                            PropertiesConfiguration.setDefaultListDelimiter('\0'); // не должен разбивать значения свойств

                            // читаем файл сопоставлений (глобальный)
                            {
                                final URL url = ImtsaFile.class.getResource("imtsa.codes.properties");
                                final PropertiesConfiguration config = new PropertiesConfiguration();
                                config.setEncoding("UTF-8");
                                config.load(url);
                                this.fillGlobalNodeCodeMap(config);
                            }

                            // читаем файл сопоставлений (локальный - локальный переопределяет существующий в коде)
                            final File localFile = ImtsaLoader.getLocalConfig();
                            if (localFile.exists())
                            {
                                final PropertiesConfiguration config = new PropertiesConfiguration();
                                config.setEncoding("UTF-8");
                                config.load(localFile);
                                this.fillGlobalNodeCodeMap(config);
                            }

                        } catch (final Throwable t)
                        {
                            throw new RuntimeException(t.getMessage(), t);
                        }
                        log4j_logger.info("");
                    }

                    /* заполняет справочник кодов из конфигурационного файла */
                    private void fillGlobalNodeCodeMap(final PropertiesConfiguration config)
                    {
                        // заполняем сопоставления (все ключи файла кодов)
                        for (final Iterator<String> it = config.getKeys(); it.hasNext(); )
                        {
                            final String k = it.next();
                            final String value = StringUtils.trimToNull(config.getString(k));
                            if ((null == value) || NO_CODE.equals(value))
                            {
                                continue;
                            }

                            final String key = StringUtils.trimToEmpty(k).toLowerCase();
                            final String old = this.put(key, value);
                            if (!value.equals(old))
                            {
                                if (null == old)
                                {
                                    log4j_logger.info("КОД " + key + " = " + value);
                                } else
                                {
                                    log4j_logger.warn("КОД " + key + " = " + value + " (было " + old + ")");
                                }
                            }
                        }
                    }
                };
            }

            return nodeCodeMapOriginal;
        }
    }

    public static final List<String> errorList = new ArrayList<>();


    private static Map<String, String> nodeCodeMap;
    public static Map<String, String> getNodeCodeMap()
    {
        if (nodeCodeMap == null)
        {
            nodeCodeMap = NodeCodeMapHolder.getNodeCodeMapOriginal();
        }

        return nodeCodeMap;
    }

    public static void setDefaultNodeCodeMap()
    {
        nodeCodeMap = null;
    }

    public static void updateNodeCodeMap()
    {
        NodeCodeMapHolder.getNodeCodeMapOriginal().putAll(nodeCodeMap);
    }


/*
    @SuppressWarnings({"serial", "unchecked"})
    private static final Map<String, String> NODE_CODE_MAP = new TreeMap<String, String>()
    {
        {
            log4j_logger.info("---- codes from properties -----------------------------------------");
            try
            {
                PropertiesConfiguration.setDefaultListDelimiter('\0'); // не должен разбивать значения свойств

                // читаем файл сопоставлений (глобальный)
                {
                    final URL url = ImtsaFile.class.getResource("imtsa.codes.properties");
                    final PropertiesConfiguration config = new PropertiesConfiguration();
                    config.setEncoding("UTF-8");
                    config.load(url);
                    this.fillGlobalNodeCodeMap(config);
                }

                // читаем файл сопоставлений (локальный - локальный переопределяет существующий в коде)
                final File localFile = new File("codes.properties");
                if (localFile.exists())
                {
                    final PropertiesConfiguration config = new PropertiesConfiguration();
                    config.setEncoding("UTF-8");
                    config.load(localFile);
                    this.fillGlobalNodeCodeMap(config);
                }

            } catch (final Throwable t)
            {
                throw new RuntimeException(t.getMessage(), t);
            }
            log4j_logger.info("");
        }

        */
/* заполняет справочник кодов из конфигурационного файла *//*

        private void fillGlobalNodeCodeMap(final PropertiesConfiguration config)
        {
            // заполняем сопоставления (все ключи файла кодов)
            for (final Iterator<String> it = config.getKeys(); it.hasNext(); )
            {
                final String k = it.next();
                final String value = StringUtils.trimToNull(config.getString(k));
                if ((null == value) || NO_CODE.equals(value))
                {
                    continue;
                }

                final String key = StringUtils.trimToEmpty(k).toLowerCase();
                final String old = this.put(key, value);
                if (!value.equals(old))
                {
                    if (null == old)
                    {
                        log4j_logger.info("КОД " + key + " = " + value);
                    } else
                    {
                        log4j_logger.warn("КОД " + key + " = " + value + " (было " + old + ")");
                    }
                }

            }
        }
    };
*/

    /** */
    public static void saveGlobalNodeCodeMap()
    {
        try
        {
/*
            File xmlDir = new File(ImtsaLoader.XML_PATH);
            if (!xmlDir.isDirectory())
            {
                if (!xmlDir.mkdir())
                {
                    throw new IllegalStateException("No «xml» folder found.");
                }
            }
            final File localFile = new File(xmlDir, "codes.properties");
            if (!localFile.exists())
            {
                localFile.createNewFile();
            }
*/

            File localFile = ImtsaLoader.getLocalConfig();

            PropertiesConfiguration.setDefaultListDelimiter('\0'); // не должен разбивать значения свойств
            final PropertiesConfiguration config = new PropertiesConfiguration();
            config.setEncoding("UTF-8");

            for (final Map.Entry<String, String> titleEntry : getNodeCodeMap().entrySet())
            {
                config.setProperty(titleEntry.getKey(), titleEntry.getValue());
            }

            config.save(localFile);

        } catch (final Throwable t)
        {
            throw new RuntimeException(t.getMessage(), t);
        }
    }

    // если код не найден - рисуем это
    public static final String NO_CODE = "?";

    /**
     * @param path путь в формате [название].[название].[название]
     * @return последовательность одов справочника «структура элементов ГОС и УП»
     */
    public static String getNodeCode(String path)
    {
        path = StringUtils.trimToEmpty(path).toLowerCase();
        String code = getNodeCodeMap().get(path);
        if (null == code)
        {
            getNodeCodeMap().put(path, code = NO_CODE);
        }
        return code;
    }

    /**
     * выводит содержимое справочника кодов
     */
    public static void printNodeCodeMap()
    {
        // это нужно вставить в global.property - файл, и заполнить кодами (по названию циклов можно определить какого поколения ГОС)
        log4j_logger.info("");
        log4j_logger.info("---- global.code.properties ----------------------------------------");
        for (final Map.Entry<String, String> titleEntry : getNodeCodeMap().entrySet())
        {
            log4j_logger.info(titleEntry.getKey().replaceAll("[ ]", "\\\\ ") + " = " + titleEntry.getValue());
        }
        log4j_logger.info("");
    }

    /**
     * выводит содержимое справочника кодов (только ошибки)
     */
    public static boolean printNodeCodeMapErrors()
    {
        boolean error = false;

        // это те, по которым надо принимать решение
        @SuppressWarnings("unchecked")
        final Collection<Map.Entry<String, String>> errorEntries = CollectionUtils.select(getNodeCodeMap().entrySet(), entry -> NO_CODE.equals(((Map.Entry<String, String>) entry).getValue()));
        if (errorEntries.size() > 0)
        {
            log4j_logger.error("");
            log4j_logger.error("---- отсутствующие коды циклов -------------------------------------");
            for (final Map.Entry<String, String> titleEntry : errorEntries)
            {
                log4j_logger.error(titleEntry.getKey().replaceAll("[ ]", "\\\\ ") + " = ");
                error = true;
            }
            log4j_logger.error("");
        }
        return error;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    private ImtsaFile()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * любая строка, в том числе подразумеваемая (если для строки явно указан идентификатор - то это ImtsaDiscipline)
     */
    public static class ImtsaNode
    {

        protected ImtsaNode parent;

        public ImtsaNode getParent()
        {
            return this.parent;
        }

        protected String title;

        public String getTitle()
        {
            return this.title;
        }

        private String indexPart;

        public String getIndexPart()
        {
            return this.indexPart;
        }

        private String number;

        public String getNumber()
        {
            return this.number;
        }

        private String hierarchyIndex;

        public String getHierarchyIndex()
        {
            return this.hierarchyIndex;
        }

        private String imtsaIndex;

        public String getImtsaIndex()
        {
            return this.imtsaIndex;
        }

        private final Map<String, ImtsaNode> childMap = new LinkedHashMap<>();

        public Collection<ImtsaNode> getChildNodes()
        {
            return this.childMap.values();
        }

        public String getTypeChar()
        {
            return "n";
        }

        private ImtsaNode add(final String pathItem, final Class<? extends ImtsaNode> nodeKlass)
        {
            ImtsaNode e = this.childMap.get(pathItem);
            if (null != e)
            {
                if (!nodeKlass.isAssignableFrom(e.getClass()))
                {
                    // ожидаемый класс не подходит (ожидали дисциплину - а взяли Node)
                    throw new ClassCastException(e.getClass().getSimpleName() + " != " + nodeKlass.getSimpleName());
                }
                return e;
            }

            if (!this.allowChild(nodeKlass))
            {
                throw new ClassCastException(nodeKlass.getSimpleName() + " is not correct child for " + this.getClass().getSimpleName());
            }

            // если узла еще нет - то создаем его (заданного класса)
            try
            {
                this.childMap.put(pathItem, e = nodeKlass.newInstance());
            } catch (final Throwable t)
            {
                throw new RuntimeException(t.getMessage(), t);
            }

            // выставляем номер
            e.parent = this;
            e.number = StringUtils.leftPad(String.valueOf(this.childMap.size()), 2, '0');
            e.indexPart = pathItem;
            e.imtsaIndex = buildIndex(this.getImtsaIndex(), e.indexPart);
            e.hierarchyIndex = buildIndex(this.getHierarchyIndex(), e.number);
            return e;
        }

        protected boolean allowChild(final Class<? extends ImtsaNode> nodeKlass)
        {
            return true;
        }

        protected static String buildIndex(String parentIndex, final String selfIndexPart)
        {
            parentIndex = StringUtils.trimToNull(parentIndex);
            return (null == parentIndex ? selfIndexPart : (parentIndex + "." + selfIndexPart));
        }
    }

    /**
     * выбор
     */
    public static class ImtsaVariant extends ImtsaNode
    {
        @Override
        public String getTypeChar()
        {
            return "v";
        }

        @Override
        protected boolean allowChild(final Class<? extends ImtsaNode> nodeKlass)
        {
            return ImtsaDiscipline.class.isAssignableFrom(nodeKlass);
        }
    }

    /**
     * группа (строка)
     */
    public static class ImtsaGroup extends ImtsaNode
    {
        @Override
        public String getTypeChar()
        {
            return "g";
        }

        @Override
        protected boolean allowChild(final Class<? extends ImtsaNode> nodeKlass)
        {
            return ImtsaDiscipline.class.isAssignableFrom(nodeKlass);
        }
    }

    /**
     * дисциплина (строка)
     */
    public static class ImtsaDiscipline extends ImtsaNode
    {
        private final Map<Integer, Map<String, Number>> term2loadMap = new TreeMap<>();

        public Map<Integer, Map<String, Number>> getTerm2loadMap()
        {
            return this.term2loadMap;
        }

        public Map<String, Number> getTermLoadMap(final Integer term)
        {
            Map<String, Number> map = this.term2loadMap.get(term);
            if (null == map)
            {
                this.term2loadMap.put(term, map = new LinkedHashMap<>());
            }
            return map;
        }

        protected String kind;

        public String getKind()
        {
            return this.kind;
        }

        protected String orgUnitCode;

        public String getOrgUnitCode()
        {
            return this.orgUnitCode;
        }

        @Override
        public String getTypeChar()
        {
            return "d";
        }

        @Override
        protected boolean allowChild(final Class<? extends ImtsaNode> nodeKlass)
        {
            return false;
        }

        private List<PairKey<String, Integer>> competences = new ArrayList<>();

        public List<PairKey<String, Integer>> getCompetences()
        {
            return this.competences;
        }
    }

    /**
     * практика
     */
    public static class ImtsaPractice extends ImtsaDiscipline
    {
        @Override
        public String getTypeChar()
        {
            return "p";
        }
    }


    /**
     * компетенция
     */
    public static class ImtsaCompetence
    {
        private String skillGroup;

        public String getSkillGroup() { return skillGroup; }

        private int number;

        public int getNumber() { return number; }

        private String content;

        public String getContent() { return content; }

        public ImtsaCompetence(String skillGroup, int number, String content)
        {
            this.skillGroup = skillGroup;
            this.number = number;
            this.content = content;
        }
    }

    public static class ImtsaSchedule
    {
        private int course;

        public int getCourse() { return course; }

        private int term;

        public int getTerm() { return term; }

        private int part;

        public int getPart() { return part; }

        private String schedule;

        public String getSchedule() { return schedule;}

        public ImtsaSchedule(int course, int term, int part, String schedule)
        {
            this.course = course;
            this.term = term;
            this.part = part;
            this.schedule = schedule;
        }
    }

    public static class ImtsaTitle
    {
        private String planFullTitle;
        private String planTitle;
        private String userNumber;
        private String academy;
        private String orgUnit;
        private String higherEchelon;
        private String producingOrgUnit;
        private String formativeOrgUnit;
        private String educationDirectionCode;
        private Integer startYear;
        private Boolean includeExamsInHoursAmount;
        private Boolean dissertationAsAttestation;
        private Boolean stateExamAsAttestation;
        private String ksrOrIndividualLessons;
        private Double zetInWeekAtt;
        private Integer hoursAmountInZetAtt;
        private Double programLabourUnits;
        private Double interactiveLessonsPercent;
        private Double lecturesPercent;
        private Double choiceDisciplinePercent;
        private Double maxSize;
        private String planKind;
        private String levelCode;
        private Integer termsInCourse;
        private Integer elementsInWeek;
        private Date stateExamDate;
        private String stateExamDoc;
        private String stateExamType;
        private String application;
        private Date applicationDate;
        private String applicationVersion;
        private Double zetInYear;
        private Double zetInWeek;
        private Integer hoursAmountInZET;
        private Double totalZET;
        private Date certificateDate;

        public String getPlanFullTitle()
        {
            return planFullTitle;
        }

        public void setPlanFullTitle(String planFullTitle)
        {
            this.planFullTitle = planFullTitle;
        }

        public String getPlanTitle()
        {
            return planTitle;
        }

        public void setPlanTitle(String planTitle)
        {
            this.planTitle = planTitle;
        }

        public String getUserNumber()
        {
            return userNumber;
        }

        public void setUserNumber(String userNumber)
        {
            this.userNumber = userNumber;
        }

        public String getAcademy()
        {
            return academy;
        }

        public void setAcademy(String academy)
        {
            this.academy = academy;
        }

        public String getOrgUnit()
        {
            return orgUnit;
        }

        public void setOrgUnit(String orgUnit)
        {
            this.orgUnit = orgUnit;
        }

        public String getHigherEchelon()
        {
            return higherEchelon;
        }

        public void setHigherEchelon(String higherEchelon)
        {
            this.higherEchelon = higherEchelon;
        }

        public String getProducingOrgUnit()
        {
            return producingOrgUnit;
        }

        public void setProducingOrgUnit(String producingOrgUnit)
        {
            this.producingOrgUnit = producingOrgUnit;
        }

        public String getFormativeOrgUnit()
        {
            return formativeOrgUnit;
        }

        public void setFormativeOrgUnit(String formativeOrgUnit)
        {
            this.formativeOrgUnit = formativeOrgUnit;
        }

        public String getEducationDirectionCode()
        {
            return educationDirectionCode;
        }

        public void setEducationDirectionCode(String educationDirectionCode)
        {
            this.educationDirectionCode = educationDirectionCode;
        }

        public Integer getStartYear()
        {
            return startYear;
        }

        public void setStartYear(Integer startYear)
        {
            this.startYear = startYear;
        }

        public Boolean isIncludeExamsInHoursAmount()
        {
            return includeExamsInHoursAmount;
        }

        public void setIncludeExamsInHoursAmount(Boolean includeExamsInHoursAmount)
        {
            this.includeExamsInHoursAmount = includeExamsInHoursAmount;
        }

        public Boolean isDissertationAsAttestation()
        {
            return dissertationAsAttestation;
        }

        public void setDissertationAsAttestation(Boolean dissertationAsAttestation)
        {
            this.dissertationAsAttestation = dissertationAsAttestation;
        }

        public Boolean isStateExamAsAttestation()
        {
            return stateExamAsAttestation;
        }

        public void setStateExamAsAttestation(Boolean stateExamAsAttestation)
        {
            this.stateExamAsAttestation = stateExamAsAttestation;
        }

        public String getKsrOrIndividualLessons()
        {
            return ksrOrIndividualLessons;
        }

        public void setKsrOrIndividualLessons(String ksrOrIndividualLessons)
        {
            this.ksrOrIndividualLessons = ksrOrIndividualLessons;
        }

        public Double getZetInWeekAtt()
        {
            return zetInWeekAtt;
        }

        public void setZetInWeekAtt(Double zetInWeekAtt)
        {
            this.zetInWeekAtt = zetInWeekAtt;
        }

        public Integer getHoursAmountInZetAtt()
        {
            return hoursAmountInZetAtt;
        }

        public void setHoursAmountInZetAtt(Integer hoursAmountInZetAtt)
        {
            this.hoursAmountInZetAtt = hoursAmountInZetAtt;
        }

        public Double getProgramLabourUnits()
        {
            return programLabourUnits;
        }

        public void setProgramLabourUnits(Double programLabourUnits)
        {
            this.programLabourUnits = programLabourUnits;
        }

        public Double getInteractiveLessonsPercent()
        {
            return interactiveLessonsPercent;
        }

        public void setInteractiveLessonsPercent(Double interactiveLessonsPercent)
        {
            this.interactiveLessonsPercent = interactiveLessonsPercent;
        }

        public Double getLecturesPercent()
        {
            return lecturesPercent;
        }

        public void setLecturesPercent(Double lecturesPercent)
        {
            this.lecturesPercent = lecturesPercent;
        }

        public Double getChoiceDisciplinePercent()
        {
            return choiceDisciplinePercent;
        }

        public void setChoiceDisciplinePercent(Double choiceDisciplinePercent)
        {
            this.choiceDisciplinePercent = choiceDisciplinePercent;
        }

        public Double getMaxSize()
        {
            return maxSize;
        }

        public void setMaxSize(Double maxSize)
        {
            this.maxSize = maxSize;
        }

        public String getPlanKind()
        {
            return planKind;
        }

        public void setPlanKind(String planKind)
        {
            this.planKind = planKind;
        }

        public String getLevelCode()
        {
            return levelCode;
        }

        public void setLevelCode(String levelCode)
        {
            this.levelCode = levelCode;
        }

        public Integer getTermsInCourse()
        {
            return termsInCourse;
        }

        public void setTermsInCourse(Integer termsInCourse)
        {
            this.termsInCourse = termsInCourse;
        }

        public Integer getElementsInWeek()
        {
            return elementsInWeek;
        }

        public void setElementsInWeek(Integer elementsInWeek)
        {
            this.elementsInWeek = elementsInWeek;
        }

        public Date getStateExamDate()
        {
            return stateExamDate;
        }

        public void setStateExamDate(Date stateExamDate)
        {
            this.stateExamDate = stateExamDate;
        }

        public String getStateExamDoc()
        {
            return stateExamDoc;
        }

        public void setStateExamDoc(String stateExamDoc)
        {
            this.stateExamDoc = stateExamDoc;
        }

        public String getStateExamType()
        {
            return stateExamType;
        }

        public void setStateExamType(String stateExamType)
        {
            this.stateExamType = stateExamType;
        }

        public String getApplication()
        {
            return application;
        }

        public void setApplication(String application)
        {
            this.application = application;
        }

        public Date getApplicationDate()
        {
            return applicationDate;
        }

        public void setApplicationDate(Date applicationDate)
        {
            this.applicationDate = applicationDate;
        }

        public String getApplicationVersion()
        {
            return applicationVersion;
        }

        public void setApplicationVersion(String applicationVersion)
        {
            this.applicationVersion = applicationVersion;
        }

        public Double getZetInYear()
        {
            return zetInYear;
        }

        public void setZetInYear(Double zetInYear)
        {
            this.zetInYear = zetInYear;
        }

        public Double getZetInWeek()
        {
            return zetInWeek;
        }

        public void setZetInWeek(Double zetInWeek)
        {
            this.zetInWeek = zetInWeek;
        }

        public Integer getHoursAmountInZET()
        {
            return hoursAmountInZET;
        }

        public void setHoursAmountInZET(Integer hoursAmountInZET)
        {
            this.hoursAmountInZET = hoursAmountInZET;
        }

        public Double getTotalZET()
        {
            return totalZET;
        }

        public void setTotalZET(Double totalZET)
        {
            this.totalZET = totalZET;
        }

        public Date getCertificateDate()
        {
            return certificateDate;
        }

        public void setCertificateDate(Date certificateDate)
        {
            this.certificateDate = certificateDate;
        }
    }

    public static class ImtsaDeveloper
    {
        private int number;
        private String fio;
        private String post;

        public ImtsaDeveloper(int number, String fio, String post)
        {
            this.number = number;
            this.fio = fio;
            this.post = post;
        }

        public int getNumber(){ return number; }
        public String getFio(){ return fio; }
        public String getPost(){ return post; }
    }

    public static class ImtsaQualification
    {
        private int number;
        private String title;
        private String developPeriod;

        public ImtsaQualification(int number, String title, String developPeriod)
        {
            this.number = number;
            this.title = title;
            this.developPeriod = developPeriod;
        }

        public int getNumber(){ return number; }
        public String getTitle(){ return title; }
        public String getDevelopPeriod(){ return developPeriod; }
    }

    public static class ImtsaSpeciality
    {
        private int number;
        private String title;

        public ImtsaSpeciality(int number, String title)
        {
            this.number = number;
            this.title = title;
        }

        public int getNumber(){ return number; }
        public String getTitle(){ return title; }
    }

    public List<ImtsaCompetence> competences = new ArrayList<>();

    public List<ImtsaSchedule> schedules = new ArrayList<>();


    public ImtsaTitle blockTitle;
    public List<ImtsaDeveloper> developers = new ArrayList<>();
    public List<ImtsaQualification> qualifications = new ArrayList<>();
    public List<ImtsaSpeciality> specialities = new ArrayList<>();


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* фиктивный корень иерархии */
    private final ImtsaNode root = new ImtsaNode()
    {
        @Override
        public String getHierarchyIndex()
        {
            return "";
        }

        @Override
        public String getTypeChar()
        {
            return "r";
        }

        @Override
        protected boolean allowChild(final Class<? extends ImtsaNode> nodeKlass)
        {
            return true;
        }
    };


    /**
     * возвращает элемент, по идентификатору ИМЦА
     *
     * @param idPath идентификатор (путь, разделенный '.')
     * @param klass  класс элемента, который хотим получить
     * @return элемент
     */
    @SuppressWarnings("unchecked")
    public <T extends ImtsaNode> T get(final String idPath, final Class<T> klass)
    {
        try
        {
            final Iterator<String> pathIterator = Arrays.asList(StringUtils.trimToEmpty(idPath).split("[.]")).iterator();
            ImtsaNode node = this.root;
            while (pathIterator.hasNext())
            {
                final String pathItem = pathIterator.next();
                if (pathIterator.hasNext())
                {
                    // промежуточный
                    if (VARIANT_INDEX_PART_MASK.matcher(pathItem).matches())
                    {
                        // дисциплина по выбору
                        node = node.add(pathItem, ImtsaVariant.class);
                    }
//                    else if (NUMBER_INDEX_PART_MASK.matcher(pathItem).matches())
//                    {
//                        // группа (wtf?)
//                        node = node.add(pathItem, ImtsaGroup.class);
//                    }
                    else
                    {
                        // узел
                        node = node.add(pathItem, ImtsaNode.class);
                    }
                } else
                {
                    // последний
                    node = node.add(pathItem, klass);
                }
            }
            return (T) node;
        } catch (final Throwable t)
        {
            throw new RuntimeException("ИдетификаторДисциплины=\"" + idPath + "\": \n" + t.getMessage(), t);
        }
    }

    /* пробигается по всем элементам иерархии (исключая фиктивный корень) */
    private static class ImtsaNodeIterator implements Iterator<ImtsaNode>
    {
        private Stack<Iterator<ImtsaNode>> stack = new Stack<>();

        public ImtsaNodeIterator(final ImtsaNode root)
        {
            this.stack.add(root.getChildNodes().iterator());
        }

        private Iterator<ImtsaNode> it()
        {
            return this.stack.peek();
        }

        @Override
        public boolean hasNext()
        {
            while (this.stack.size() > 0)
            {
                if (this.it().hasNext())
                {
                    return true;
                }
                this.stack.pop();
            }
            return false;
        }

        @Override
        public ImtsaNode next()
        {
            while (this.stack.size() > 0)
            {
                if (this.it().hasNext())
                {
                    final ImtsaNode next = this.it().next();
                    final Collection<ImtsaNode> childNodes = next.getChildNodes();
                    if (childNodes.size() > 0)
                    {
                        this.stack.push(childNodes.iterator());
                    }
                    return next;
                }
                this.stack.pop();
            }
            throw new NoSuchElementException();
        }

        @Override
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * @return все элементы иерархии
     */
    @Override
    public Iterator<ImtsaNode> iterator()
    {
        return new ImtsaNodeIterator(this.root);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * обрабатывает xml-файл ИМЦА (ВПО2/3 очники)
     *
     * @param file файл
     * @return обработанный файл, null - если произошла ошибка
     */
    public static ImtsaFile parse(final File file, final boolean importSchedule)
    {
        // только один поток может работать с логгером
        synchronized (log4j_logger)
        {
            try
            {
                // регистрируем обработчик для файла
                final String logFileName = file.getAbsoluteFile() + ".log";
                try
                {
                    new File(logFileName).delete();
                } catch (final Throwable t)
                { /**/ }

                final FileAppender appender = new FileAppender(new PatternLayout(LOG4J_PATTERN), logFileName);
                final Level level = log4j_logger.getLevel();
                appender.setName("imtsaLogFileAppender");
                appender.setThreshold(Level.INFO);
                log4j_logger.addAppender(appender);
                log4j_logger.setLevel(Level.INFO);

                PropertiesConfiguration.setDefaultListDelimiter('\0'); // не должен разбивать значения свойств
                final PropertiesConfiguration config = new PropertiesConfiguration();
                config.setEncoding("UTF-8");

                final File configFile = new File(file.getAbsoluteFile() + ".properties");
                if (!configFile.exists())
                {
                    configFile.createNewFile();
                }
                config.load(configFile);

                try
                {
                    log4j_logger.info("Начало обработки файла\n" + file.getAbsolutePath() + "\n====================================================================");

                    final Document doc = parseXml(file);
                    return parse(doc, config, importSchedule);

                } catch (final Throwable t)
                {
                    log4j_logger.error(t.getMessage(), t);
                    return null;

                } finally
                {

                    // сохраняем
                    config.save(configFile);

                    // отцепляем дополнительное логирование
                    log4j_logger.info("Завершение обработки файла\n====================================================================\n\n");
                    log4j_logger.removeAppender(appender);
                    log4j_logger.setLevel(level);
                }

            } catch (final Throwable t)
            {
                log4j_logger.error(t.getMessage(), t);
                return null;
            }
        }
    }

    public static ImtsaFile parse(InputStream imtsaInputStream, boolean importSchedule) throws IOException, ConfigurationException
    {
        // только один поток может работать с логгером
        synchronized (log4j_logger)
        {
            PropertiesConfiguration.setDefaultListDelimiter('\0'); // не должен разбивать значения свойств
            final PropertiesConfiguration config = new PropertiesConfiguration();
            config.setEncoding("UTF-8");

            final File configFile = new File(ImtsaLoader.PATH ,"codes.properties");
            if (!configFile.exists())
            {
                configFile.createNewFile();
            }
            try
            {
                final Document doc = parseXml(imtsaInputStream);
                return parse(doc, config, importSchedule);

            } finally
            {
                // сохраняем
                config.save(configFile);
            }
        }
    }

    private static ImtsaFile parse(Document doc, PropertiesConfiguration config, boolean importSchedule)
    {

        final ImtsaFile result = new ImtsaFile();
        boolean hasError = false;


        /* титул */
        {
            log4j_logger.info("---- титул ---------------------------------------------------------");

            final NodeList titleNodeList = doc.getElementsByTagName("Титул");
            if (null == titleNodeList)
            {
                throw new IllegalStateException("В документе отсутствует тег «Титул».");
            }
            if (titleNodeList.getLength() > 1)
            {
                throw new IllegalStateException("В документе присутствует более одного тега «Тиул».");
            }

            final Node node = titleNodeList.item(0);
            ImtsaTitle title = new ImtsaTitle();
            result.blockTitle = title;

            final String planFullTitle = toStringNull(node, "ПолноеИмяПлана");
            title.setPlanFullTitle(planFullTitle);

            final String planTitle = toStringNull(node, "ИмяПлана");
            title.setPlanTitle(planTitle);

            final String userNumber = toStringNull(node, "НомерПользователя");
            title.setUserNumber(userNumber);

            final String academy = toStringNull(node, "ИмяВуза");
            title.setAcademy(academy);

            final String orgUnit = toStringNull(node, "ИмяВуза2");
            title.setOrgUnit(orgUnit);

            final String higherEchelon = toStringNull(node, "Головная");
            title.setHigherEchelon(higherEchelon);

            final String producingOrgUnit = toStringNull(node, "КодКафедры");
            title.setProducingOrgUnit(producingOrgUnit);

            String formativeOrgUnit = toStringNull(node, "Факультет");
            title.setFormativeOrgUnit(formativeOrgUnit);

            final String educationDirectionCode = toStringNull(node, "ПоследнийШифр");
            title.setEducationDirectionCode(educationDirectionCode);

            final Integer startYear = toIntegerNull(node, "ГодНачалаПодготовки");
            if (startYear != null && (startYear > 2100 || startYear < 1900)){ throw new IllegalStateException("Некорректно задан атрибут «ГодНачалаПодготовки»."); }
            title.setStartYear(startYear);

            final String includeExamsInHoursAmount = toStringNull(node, "ВключатьЭкВСуммуЧасов");
            title.setIncludeExamsInHoursAmount(includeExamsInHoursAmount == null ? null : includeExamsInHoursAmount.equals("1"));

            final String dissertationAsAttestation = toStringNull(node, "ДвИГА");
            title.setDissertationAsAttestation(dissertationAsAttestation == null ? null : dissertationAsAttestation.equals("1"));

            final String stateExamAsAttestation = toStringNull(node, "ГвИГА");
            title.setStateExamAsAttestation(stateExamAsAttestation == null ? null : stateExamAsAttestation.equals("1"));

            final String ksrOrIndividualLessons = toStringNull(node, "КСР_ИЗ").toUpperCase();
            if (ksrOrIndividualLessons != null && !ksrOrIndividualLessons.equals("КСР") && !ksrOrIndividualLessons.equals("ИЗ")){ throw new IllegalStateException("неизвестное значение атрибута «КСР_ИЗ»."); }
            title.setKsrOrIndividualLessons(ksrOrIndividualLessons);

            final Double zetInWeekAtt = toDoubleNull(node, "ИГА_ЗЕТвНеделе");
            title.setZetInWeekAtt(zetInWeekAtt);

            final Integer hoursAmountInZetAtt = toIntegerNull(node, "ИГА_ЧасовВЗЕТ");
            title.setHoursAmountInZetAtt(hoursAmountInZetAtt);

            final Double programLabourUnits = toDoubleNull(node, "ООПет");
            title.setProgramLabourUnits(programLabourUnits);

            final Double interactiveLessonsPercent = toDoubleNull(node, "Интер");
            title.setInteractiveLessonsPercent(interactiveLessonsPercent);

            final Double lecturesPercent = toDoubleNull(node, "Лекц");
            title.setLecturesPercent(lecturesPercent);

            final Double choiceDisciplinesPercent = toDoubleNull(node, "ДВВ");
            title.setChoiceDisciplinePercent(choiceDisciplinesPercent);

            final Double maxSize = toDoubleNull(node, "МаксНагр");
            title.setMaxSize(maxSize);

            final String planKind = toString(node, "ВидПлана");
            if (planKind.isEmpty()){ throw new IllegalStateException("В титуле отстутствует атрибут «ВидПлана»."); }
            title.setPlanKind(planKind);

            final String levelCode = toStringNull(node, "КодУровня");
            if (levelCode != null && !levelCode.equals("B") && !levelCode.equals("M") && !levelCode.equals("S")){ throw new IllegalStateException("Неизвестное значение атрибута «КодУровня».");}
            title.setLevelCode(levelCode == null ? null : levelCode.toUpperCase());

            final Integer termsInCourse = toIntegerNull(node, "СеместровНаКурсе");
            title.setTermsInCourse(termsInCourse);

            final Integer elementsInWeek = toIntegerNull(node, "ЭлементовВНеделе");
            title.setElementsInWeek(elementsInWeek);

            Date stateExamDate = null;
            String stateExamDateStr = toStringNull(node, "ДатаГОСа");
            String imtsaSEDatePattern = "dd.MM.yyyy h:mm:ss";
            String[] datePatterns = {imtsaSEDatePattern, DateFormatter.PATTERN_DEFAULT, DateFormatter.PATTERN_WITH_TIME};
            if (stateExamDateStr != null)
            {
                try
                {
                    stateExamDate = DateUtils.parseDate(stateExamDateStr, datePatterns);

                } catch (ParseException e)
                {
                    //throw new IllegalStateException("Неизвестный формат атрибута «ДатаГОСа».");

                }
            }
            title.setStateExamDate(stateExamDate);

            final String stateExamDoc = toStringNull(node, "ДокументГОСа");
            title.setStateExamDoc(stateExamDoc);

            final String stateExamType = toStringNull(node, "ТипГОСа");
            title.setStateExamType(stateExamType);

            final String application = toStringNull(node, "Приложение");
            title.setApplication(application);

            Date applicationDate = null;
            String applicationDateStr = toString(node, "ДатаПриложения");
            if (applicationDateStr != null)
            {
                try
                {
                    applicationDate = DateUtils.parseDate(applicationDateStr, datePatterns);

                } catch (ParseException e)
                {
                    //throw new IllegalStateException("Неизвестный формат атрибута «ДатаПриложения».");
                }
            }
            title.setApplicationDate(applicationDate);

            final String applicationVersion = toStringNull(node, "ВерсияПриложения");
            title.setApplicationVersion(applicationVersion);

            final Double zetInYear = toDoubleNull(node, "ЗЕТнаГОД");
            title.setZetInYear(zetInYear);

            final Double zetInWeek = toDoubleNull(node, "ЗЕТвНеделе");
            title.setZetInWeek(zetInWeek);

            final Integer hoursAmountInZET = toIntegerNull(node, "ЧасовВЗЕТ");
            title.setHoursAmountInZET(hoursAmountInZET);

            final Double totalZET = toDoubleNull(node, "ЗЕТнаВСЕ");
            title.setTotalZET(totalZET);

            Date certificateDate = null;
            String certificateDateStr = toStringNull(node, "ДатаСертификатаИМЦА");
            if (certificateDateStr != null)
            {
                try
                {
                    certificateDate = DateUtils.parseDate(certificateDateStr, datePatterns);

                } catch (ParseException e)
                {
                    //throw new IllegalStateException("Неизвестный формат атрибута «ДатаСертификатаИМЦА».");
                }
            }
            title.setCertificateDate(certificateDate);

        }

        {
            /* разработчики */
            log4j_logger.info("---- разработчики ---------------------------------------------------------");

            final NodeList developerNodeList = doc.getElementsByTagName("Разработчики");
            if (null == developerNodeList)
            {
                throw new IllegalStateException("В документе отсутствует тег «Разработчики».");
            }
            if (developerNodeList.getLength() > 1)
            {
                throw new IllegalStateException("В документе присутствует более одного тега «Разработчики».");
            }

            final NodeList developerNodes = developerNodeList.item(0).getChildNodes();
            for (int i = 0; i < developerNodes.getLength(); i++)
            {
                // берем нод, если не подходит - пропускаем
                final Node node = developerNodes.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE){ continue; }

                // печатаем название
                log4j_logger.info(str(node));
                if (!node.getNodeName().equals("Разработчик"))
                {
                    log4j_logger.warn(" ! Пропущен - не «Разработчик».");
                    continue;
                }

                Integer number = toInteger(node, "Ном");
                if (number == 0){ throw new IllegalStateException("Не задан номер разработчика."); }

                String fio = toString(node, "ФИО");
                if (fio.isEmpty()){ throw new IllegalStateException("Не указаны ФИО " + number + " разработчика."); }

                String post = toString(node, "Должность");
                if (post.isEmpty()){ throw new IllegalStateException("Не указана должность " + number + " разработчика."); }

                result.developers.add(new ImtsaDeveloper(number, fio, post));
            }
        }

        {
            /* квалификации */
            log4j_logger.info("---- квалификации ---------------------------------------------------------");

            final NodeList qualificationNodeList = doc.getElementsByTagName("Квалификации");
            if (null == qualificationNodeList)
            {
                throw new IllegalStateException("В документе отсутствует тег «Квалификации».");
            }
            if (qualificationNodeList.getLength() > 1)
            {
                throw new IllegalStateException("В документе присутствует более одного тега «Квалификации».");
            }

            final NodeList qualificationNodes = qualificationNodeList.item(0).getChildNodes();
            for (int i = 0; i < qualificationNodes.getLength(); i++)
            {
                // берем нод, если не подходит - пропускаем
                final Node node = qualificationNodes.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE){ continue; }

                // печатаем название
                log4j_logger.info(str(node));
                if (!node.getNodeName().equals("Квалификация"))
                {
                log4j_logger.warn(" ! Пропущен - не «Квалификация».");
                    continue;
                }

                Integer number = toInteger(node, "Ном");
                if (number == 0){ throw new IllegalStateException("Не задан номер квалификации."); }

                String title = toString(node, "Название");
                if (title.isEmpty()){ throw new IllegalStateException("Не указано название " + number + " квалификации."); }

                String developPeriod = toString(node, "СрокОбучения");
                if (developPeriod.isEmpty()){ throw new IllegalStateException("Не указан срок обучения " + number + " квалификации."); }

                result.qualifications.add(new ImtsaQualification(number, title, developPeriod));
            }
        }

        {
            /* специальности */
            log4j_logger.info("---- специальности ---------------------------------------------------------");

            final NodeList specialityNodeList = doc.getElementsByTagName("Специальности");
            if (null == specialityNodeList)
            {
                throw new IllegalStateException("В документе отсутствует тег «Специальности».");
            }
            if (specialityNodeList.getLength() > 1)
            {
                throw new IllegalStateException("В документе присутствует более одного тега «Специальности».");
            }

            final NodeList specialityNodes = specialityNodeList.item(0).getChildNodes();
            for (int i = 0; i < specialityNodes.getLength(); i++)
            {
                // берем нод, если не подходит - пропускаем
                final Node node = specialityNodes.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE){ continue; }

                // печатаем название
                log4j_logger.info(str(node));
                if (!node.getNodeName().equals("Специальность"))
                {
                log4j_logger.warn(" ! Пропущен - не «Специальность».");
                    continue;
                }

                Integer number = toInteger(node, "Ном");
                if (number == 0){ throw new IllegalStateException("Не задан номер специальности."); }

                String title = toString(node, "Название");
                if (title.isEmpty()){ throw new IllegalStateException("Не указано название " + number + " специальности."); }

                result.specialities.add(new ImtsaSpeciality(number, title));
            }
        }

        /* учебный график */
        if (importSchedule)
        {
            log4j_logger.info("---- учебный график ---------------------------------------------------------");

            final NodeList scheduleNodeList = doc.getElementsByTagName("ГрафикУчПроцесса");
            if (null == scheduleNodeList)
            {
                throw new IllegalStateException("В документе отсутствует тег «ГрафикУчПроцесса».");
            }
            if (scheduleNodeList.getLength() > 1)
            {
                throw new IllegalStateException("В документе присутствует более одного тега «ГрафикУчПроцесса».");
            }

            final NodeList courseNodes = scheduleNodeList.item(0).getChildNodes();

            // шаблон учебного графика
            String globalTemplate = "";
            for (int i = 0; i < courseNodes.getLength(); i++)
            {
                // берем нод, если не подходит - пропускаем
                final Node node = courseNodes.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE)
                {
                    continue;
                }

                // печатаем название
                log4j_logger.info(str(node));
                if (!node.getNodeName().equals("Курс"))
                {
                    log4j_logger.warn(" ! Пропущен - не «Курс».");
                    continue;
                }

                final Integer courseNum = toInteger(node, "Ном");
                if (courseNum == 0)
                {
                    globalTemplate = toString(node, "График");
                    break;
                }
            }

            if (globalTemplate.isEmpty())
            {
                throw new IllegalStateException("Не корректно задан шаблон учебного графика.");
            }

            // "разбиение" учебного графика - кол-во атрибутов "График" в семестрах (либо "разбиение", либо единица)
            int partition = 1;

            Set<Integer> courseNums = new HashSet<>();
            for (int i = 0; i < courseNodes.getLength(); i++)
            {
                // берем нод, если не подходит - пропускаем
                final Node node = courseNodes.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE)
                {
                    continue;
                }

                // печатаем название
                log4j_logger.info(str(node));
                if (!node.getNodeName().equals("Курс"))
                {
                    log4j_logger.warn(" ! Пропущен - не «Курс».");
                    continue;
                }

                final Integer courseNum = toInteger(node, "Ном");
                if (courseNum == 0)
                {
                    continue;
                }

                if (!courseNums.add(courseNum))
                {
                    throw new IllegalStateException("Номер курса " + courseNum + " встречается больше одного раза.");
                }

                Map<Integer, String> courseTemplateMap = new HashMap<>();
                String courseSample = toString(node, "График").toUpperCase();
                if (courseSample.isEmpty() || courseSample.length() != globalTemplate.length())
                {
                    throw new IllegalStateException("Некорректно задан график " + courseNum + " курса.");
                }
                courseTemplateMap.put(1, courseSample);
                boolean matchTermTemplates = !courseSample.contains("=");
                if (!matchTermTemplates)
                {
                    continue;
                }

                for (int j = 2; j <= 6; j++)
                {
                    String schedule = "График" + j;
                    String template = toString(node, schedule).toUpperCase();
                    if (!template.isEmpty())
                    {
                        if (template.length() != globalTemplate.length())
                            throw new IllegalStateException("«" + schedule + "»" + " " + courseNum + "  курса не соответствует шаблону " + '"' + globalTemplate + '"');

                        courseTemplateMap.put(j, template);
                    }
                }

                /* семестры текущего курса */
                Map<Integer, Integer> firstWeekNumMap = new HashMap<>(); // НОМЕР СЕМЕСТРА - номер первой недели в семестре
                Map<Integer, Integer> termScheduleLengthMap = new HashMap<>(); // НОМЕР СЕМЕСТРА - длина учебного графика в семестре
                Map<Integer, Map<Integer, String>> termTemplateMap = new TreeMap<>();
                Set<Integer> termNums = new TreeSet<>();
                NodeList termNodeList = node.getChildNodes();
                for (int k = 0; k < termNodeList.getLength(); k++)
                {
                    // берем нод, если не подходит - пропускаем
                    final Node termNode = termNodeList.item(k);
                    if (termNode.getNodeType() != Node.ELEMENT_NODE)
                    {
                        continue;
                    }

                    // печатаем название
                    log4j_logger.info(str(termNode));
                    if (!termNode.getNodeName().equals("Семестр"))
                    {
                        log4j_logger.warn(" ! Пропущен - не «Семестр».");
                        continue;
                    }

                    final Integer termNum = toInteger(termNode, "Ном");
                    if (termNum == 0)
                    {
                        throw new IllegalStateException("У семестра " + courseNum + " курса отсутствует атрибут «Ном».");
                    }

                    if (!termNums.add(termNum))
                    {
                        throw new IllegalStateException("В " + courseNum + " курсе " + termNum + " семестр встречается больше одного раза.");
                    }

                    final Integer firstWeekNum = toInteger(termNode, "НомерПервойНедели");
                    if (firstWeekNum == 0)
                    {
                        throw new IllegalStateException("У " + termNum + " семестра " + courseNum + " курса отсутствует атрибут «НомерПервойНедели».");
                    }

                    firstWeekNumMap.put(termNum, firstWeekNum);

/*
                                if (!matchTermTemplates)
                                {
                                    continue;
                                }
*/

                    String baseTemplate = toString(termNode, "График");
                    if (baseTemplate.isEmpty())
                    {
                        throw new IllegalStateException("У " + termNum + " семестра " + courseNum + " курса отсутствует атрибут «График».");
                    }
                    SafeMap.safeGet(termTemplateMap, termNum, HashMap.class).put(1, baseTemplate);

                    // количество атрибутов "График" (частей разбиения) в семестре - кандидат на "разбиение"
                    int candidate = 0;
                    for (int part = 2; part <= 6; part++)
                    {
                        String schedule = "График" + part;
                        String template = toString(termNode, schedule).toUpperCase();

                        if (template.isEmpty())
                        {
                            if (candidate == 0)
                            {
                                // нет такого атрибута, предыдущий был последним
                                candidate = part - 1;
                            }

                        } else if (candidate != 0)
                        {
                            // такой атрибут есть, однако предыдущего не было -> дырка
                            throw new IllegalStateException("У " + termNum + " семестра " + courseNum + " курса пропущен атрибут «График" + (candidate + 1) + "».");

                        } else
                        {
                            Integer length = termScheduleLengthMap.get(termNum);
                            if (length == null)
                            {
                                termScheduleLengthMap.put(termNum, template.length());

                            } else if (template.length() != length)
                            {
                                // среди графиков одного семестра есть два с разной длиной
                                throw new IllegalStateException("Графики " + termNum + " семестра " + courseNum + " курса имеют разную длину.");
                            }

                            SafeMap.safeGet(termTemplateMap, termNum, HashMap.class).put(part, template);
                        }
                    }

                    if (candidate != 1 && candidate != partition)
                    {
                        // полученное на данном семестре "разбиение" отличается от ранее полученного (единицу не расматриваем: она означает, что в семестре недели не разбиты)
                        if (partition == 1)
                        {
                            // "разбиение" по умолчанию, заменяем на полученное
                            partition = candidate;

                        } else
                        {
                            // получили разбиение, отличное от ранее полученного (не единица)
                            throw new IllegalStateException("Невозможно определить тип разбиения недели в учебном графике.");
                        }
                    }
                }

                int termSize = termNums.size();
                if (termSize != termNums.toArray(new Integer[termSize])[termSize - 1])
                {
                    throw new IllegalStateException("Пропущен семестр в " + courseNum + " курсе.");
                }

/*
                            if (!matchTermTemplates)
                            {
                                continue;
                            }
*/


                for (int j = 1, size = firstWeekNumMap.size(); j < size - 1; j++)
                {
                    int weeksPerTerm = firstWeekNumMap.get(j + 1) - firstWeekNumMap.get(j);
                    if (weeksPerTerm != termScheduleLengthMap.get(j))
                    {
                        throw new IllegalStateException("Учебный график " + j + " семестра " + courseNum + " курса не соответствует длительности семестра.");
                    }
                }

                // проверяем, что график в семестрах соответствует курсовому шаблону
                for (Map.Entry<Integer, String> courseTemplateEntry : courseTemplateMap.entrySet())
                {
                    Integer partNum = courseTemplateEntry.getKey();

                    StringBuilder scheduleBuilder = new StringBuilder();
                    for (Integer termNum : termTemplateMap.keySet())
                    {
                        Map<Integer, String> partMap = termTemplateMap.get(termNum);
                        String schedulePart = partMap.size() == 1 ? partMap.get(1) : partMap.get(partNum);

                        scheduleBuilder.append(schedulePart);
                    }

                    String schedule = scheduleBuilder.toString();
                    if (partNum == 1)
                    {
                        if (!schedule.equals(courseSample))
                        {
                            throw new IllegalStateException("«График» не соответствует шаблону " + courseNum + " курса.");
                        }

                    } else
                    {
                        String mask = courseTemplateEntry.getValue();
                        for (int l = 0; l < globalTemplate.length(); l++)
                        {
                            char ch = mask.charAt(l);
                            if (ch == '0')
                            {
                                ch = courseSample.charAt(l);
                            }
                            if (ch != schedule.charAt(l))
                            {
                                throw new IllegalStateException("«График" + partNum + "»" + " не соответствует шаблону " + courseNum + " курса.");
                            }
                        }
                    }
                }

                for (Map.Entry<Integer, Map<Integer, String>> termEntry : termTemplateMap.entrySet())
                {
                    int termNum = termEntry.getKey();
                    for (Map.Entry<Integer, String> partEntry : termEntry.getValue().entrySet())
                    {
                        int part = partEntry.getKey();
                        String schedule = partEntry.getValue();

                        result.schedules.add(new ImtsaSchedule(courseNum, termNum, part, schedule));
                    }
                }
            }
        }


        // используется для проверки уникальности индексов компетенции
        final Map<String, Set<Integer>> compNumberMap = SafeMap.get(TreeSet.class);

        // используется совместно с compNumberMap для проверки распределенности компетенций в блоке
        final Map<String, Integer> compMaxNumber = new HashMap<>();

        // используется при сопоставлении кодов и индексов компетенций в строкахПлана
        final Map<String, String> codeIndexMap = new HashMap<>();

        // отображение индекса компетенции на состоявлющие его сокр. название группы и номер компетенции
        final Map<String, PairKey<String, Integer>> indexMap = new HashMap<>();

        // хранится информация об импортируемых компетенциях (только распределенные)
        final Map<PairKey<String, Integer>, String> index2ContentMap = new HashMap<>();

        // используется для проверки уникальности кодов компетенции
        final Set<String> competenceCodeSet = new HashSet<>();

        /* компетенции */
        {
            // отображение импортируемых компетенций в логгере
            final Set<String> competenceIndexSet = new HashSet<>();

            // используется для проверки уникальности содержаний компетенции
            final Set<String> competenceContentSet = new HashSet<>();


            log4j_logger.info("---- компетенции ---------------------------------------------------------");

            final NodeList competenceNodeList = doc.getElementsByTagName("Компетенции");
            if (null == competenceNodeList)
            {
                throw new IllegalStateException("В документе отсутствует тег «Компетенции».");
            }
            if (competenceNodeList.getLength() > 1)
            {
                throw new IllegalStateException("В документе присутствует более одного тега «Компетенции».");
            }

            final NodeList competenceNodes = competenceNodeList.item(0).getChildNodes();

            for (int i = 0; i < competenceNodes.getLength(); i++)
            {
                // берем нод, если не подходит - пропускаем
                final Node node = competenceNodes.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE)
                {
                    continue;
                }

                // печатаем название
                log4j_logger.info(str(node));
                if (!node.getNodeName().equals("Строка"))
                {
                    log4j_logger.warn(" ! Пропущен - не «Строка».");
                    continue;
                }

                final String competeceCode = toString(node, "Код");
                if (competeceCode.isEmpty())
                {
                    log4j_logger.error(" ! Пропущен - отсутствует аттрибут «Код».");
                    continue;
                }

                final String competenceIndex = toString(node, "Индекс").toUpperCase();
                if (competenceIndex.isEmpty())
                {
                    log4j_logger.error(" ! Пропущен - отсутствует аттрибут «Индекс».");
                    continue;
                }

                final String competenceContent = toString(node, "Содержание");
                if (competenceContent.isEmpty())
                {
                    log4j_logger.warn(" ! Пропущен - отсутствует аттрибут «Содержание».");
                    continue;
                }

                // сохраняем компетенции

                if (!competenceCodeSet.add(competeceCode))
                    throw new IllegalStateException("Повторяющийся код компетенции: «" + competeceCode + "».");

                if (!competenceIndexSet.add(competenceIndex))
                    throw new IllegalStateException("Повторяющийся индекс компетенции: «" + competenceIndex + "».");

                if (!competenceContentSet.add(competenceContent.toUpperCase().replaceAll(" ", "")))
                    throw new IllegalStateException("Повторяющееся содержание компетенции: «" + competenceContent + "».");


                PairKey<String, Integer> key;
                try
                {
                    key = getCompetenceGroupAndNumber(competenceIndex);

                } catch (IllegalArgumentException e)
                {
                    throw new IllegalStateException("Неверный формат индекса компетенции:" + " «" + competenceIndex + "».");
                }

                if (!compNumberMap.get(key.getFirst()).add(key.getSecond()))
                    throw new IllegalStateException("Повторяющийся индекс компетенции: «" + competenceIndex + "».");

                indexMap.put(competenceIndex, key);

                index2ContentMap.put(key, competenceContent);

                codeIndexMap.put(competeceCode, competenceIndex);
            }

            for (Map.Entry<String, Set<Integer>> entry : compNumberMap.entrySet())
            {
                int size = entry.getValue().size();

                if (size != (Integer) entry.getValue().toArray()[size - 1])
                {
                    throw new IllegalStateException("Пропущен номер компетенции в группе:" + " «" + entry.getKey() + "».");
                }
            }

            for (Map.Entry<String, Set<Integer>> entry : compNumberMap.entrySet())
            {
                compMaxNumber.put(entry.getKey(), entry.getValue().size());
            }

            log4j_logger.info("   Компетенции: " + competenceIndexSet);

        }


        // 'абревиатура-цикла' (upper) -> 'название цикла' (lower, название обработано - произведено сопоставление с config)
        final Map<String, String> cycleAbvr2TitleMap = new LinkedHashMap<>();

        /* циклы */
        {
            log4j_logger.info("---- циклы ---------------------------------------------------------");

            final NodeList cycleAttrsNodeList = doc.getElementsByTagName("АтрибутыЦиклов");
            if (null == cycleAttrsNodeList)
            {
                throw new IllegalStateException("В документе отсутствует тег «АтрибутыЦиклов».");
            }
            if (cycleAttrsNodeList.getLength() > 1)
            {
                throw new IllegalStateException("В документе присутствует более одного тега «АтрибутыЦиклов».");
            }

            final NodeList cycleNodes = cycleAttrsNodeList.item(0).getChildNodes();
            for (int i = 0; i < cycleNodes.getLength(); i++)

            {
                // берем нод, если не подходит - пропускаем
                final Node node = cycleNodes.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE)
                {
                    continue;
                }

                // печатаем название
                log4j_logger.info(str(node));
                if (!node.getNodeName().equals("Цикл"))
                {
                    log4j_logger.warn(" ! Пропущен - не «Цикл».");
                    continue;
                }

                final String cycleAbvr = toString(node, "Абревиатура");
                if (cycleAbvr.isEmpty())
                {
                    log4j_logger.error(" ! Пропущен - отсутствует аттрибут «Абревиатура».");
                    continue;
                }

                final String cycleTitle = toString(node, "Название");
                if (cycleTitle.isEmpty())
                {
                    log4j_logger.warn(" ! Пропущен - отсутствует аттрибут «Название».");
                    continue;
                }

                // сохраняем циклы (изменяем название через конфигурации, если требуется)
                final String configuredCycleTitle = config.getString(cycleTitle, cycleTitle);
                config.setProperty(cycleTitle, configuredCycleTitle);

                if (null != cycleAbvr2TitleMap.put(cycleAbvr.toUpperCase(), configuredCycleTitle.toLowerCase()))
                {
                    throw new IllegalStateException("Повторяющаяся аббревиатура цикла: «" + cycleAbvr + "».");
                }
            }

            log4j_logger.info("   Циклы: " + cycleAbvr2TitleMap.toString());
        }

        /* дисциплины */
        {
            log4j_logger.info("---- строки --------------------------------------------------------");
            final NodeList nodeList = doc.getElementsByTagName("Строка");
            for (int i = 0; i < nodeList.getLength(); i++)
            {
                // берем нод, если не подходит - пропускаем
                final Node node = nodeList.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE)
                {
                    continue;
                }

                // печатаем тэг
                log4j_logger.info(str(node));

                // проверяем, что он в требуемом нам месте
                if (!node.getParentNode().getNodeName().equals("СтрокиПлана"))
                {
                    log4j_logger.warn(" ! Пропущен - не находится в «СтрокиПлана».");
                    continue;
                }

                try
                {
                    // проверяем, что заполнен аттрибут "ИдентификаторДисциплины"
                    final String disciplineId = toString(node, "ИдетификаторДисциплины");
                    if (disciplineId.isEmpty())
                    {
                        log4j_logger.warn(" ! Пропущен - отсутствует аттрибут «ИдетификаторДисциплины».");
                        continue;
                    }

                    // проверяем, что заполнен аттрибут "Цикл"
                    final String disciplineCycle = toString(node, "Цикл");
                    if (disciplineCycle.isEmpty())
                    {
                        log4j_logger.warn(" ! Пропущен - отсутствует аттрибут «Цикл».");
                        continue;
                    }

                    // проверяем, что заполнен аттрибут "Дис"
                    final String disciplineTitle = toString(node, "Дис");
                    if (disciplineTitle.isEmpty())
                    {
                        log4j_logger.warn(" ! Пропущен - отсутствует аттрибут «Дис».");
                        continue;
                    }

                    log4j_logger.info("   [" + disciplineCycle + ", " + disciplineId + "] = \"" + disciplineTitle + "\"");
                    if (!disciplineId.startsWith(disciplineCycle))
                    {
                        // ?wtf?
                    }

                    // дисциплины для разделов (объявляется группа)
                    if (toInteger(node, "ДисцплинаДляРазделов").intValue() > 0)
                    {
                        // это группа: создаем группу
                        log4j_logger.info("     Группа");
                        final ImtsaGroup group = result.get(disciplineId, ImtsaGroup.class);
                        group.title = disciplineTitle;
                        continue;
                    }

                    // это истинная дисциплина
                    // создаем дисциплину в структуре (далее - если возникает ошибка, то весь файл ИМЦы признается ошибочным)
                    log4j_logger.info("     Дисциплина");
                    final String correctedDisciplineId = disciplineId.replaceAll("\\.В(\\d+)\\.", "\\.В\\." + VARIANT_INDEX_PART_PREFIX + "$1\\.").replaceAll("(\\d+)\\." + VARIANT_INDEX_PART_PREFIX + "(\\d+)", "$1\\.В\\." + VARIANT_INDEX_PART_PREFIX + "$2"); // в ГОС2 .В#n. - это должно быть .В.ДВ#n. // [MBS]#m.ДВ#n.#k -> [MBS]#m.В.ДВ#n.#k
                    final ImtsaDiscipline discipline = result.get(correctedDisciplineId, ImtsaDiscipline.class);
                    discipline.title = disciplineTitle;
                    discipline.kind = KIND_CODE_DISCIPLINE;

                    // кафедра
                    final String disciplineOwner = toString(node, "Кафедра");
                    if (null != disciplineOwner)
                    {
                        discipline.orgUnitCode = disciplineOwner;
                    }

                    // компетенции
                    String discCodesStr = toString(node, "КомпетенцииКоды");
                    String discIndexesStr = toString(node, "Компетенции");

                    if (discCodesStr.isEmpty() != discIndexesStr.isEmpty())
                        throw new IllegalStateException("Несоответсвие между кодами компетенций и компетенциями в строке: «" + disciplineId + "».");

                    if (!discCodesStr.isEmpty())
                    {
                        final List<String> discCodes = Arrays.asList(discCodesStr.split("&"));
                        if (discCodes.size() != new HashSet<>(discCodes).size())
                            throw new IllegalStateException("Повторяющиеся коды компетенций в строке: «" + disciplineId + "».");

                        final List<String> discIndexes = Arrays.asList(discIndexesStr.replaceAll(" ", "").split(","));
                        if (discIndexes.size() != new HashSet<>(discIndexes).size())
                            throw new IllegalStateException("Повторяющиеся компетенции в строке: «" + disciplineId + "».");

                        if (discCodes.size() != discIndexes.size())
                            throw new IllegalStateException("Несоответствие между кодами компетенций и компетенциями в строке: «" + disciplineId + "».");

                        for (String code : discCodes)
                        {
                            if (!discIndexes.contains(codeIndexMap.get(code)))
                                throw new IllegalStateException("Несоответствие между кодами компетенций и компетенциями в строке: «" + disciplineId + "».");
                        }

                        for (String index : discIndexes)
                        {
                            PairKey<String, Integer> indexKey = indexMap.get(index);

                            compNumberMap.get(indexKey.getFirst()).remove(indexKey.getSecond());

                            discipline.getCompetences().add(indexKey);
                        }

                        if (!competenceCodeSet.containsAll(discCodes))
                        {
                            throw new IllegalStateException("Некорректная компетенция в строке: «" + disciplineId + "».");
                        }
                    }


                    // данные по самой дисциплине
                    {
                        process_disciplineNode_controlAction_termsAttribute(node, discipline, "СемЭкз", TYPE_CONTROL_EXAM);
                        process_disciplineNode_controlAction_termsAttribute(node, discipline, "СемЗач", TYPE_CONTROL_TEST);
                        process_disciplineNode_controlAction_termsAttribute(node, discipline, "СемКР", TYPE_CONTROL_COURSE_WORK);
                        process_disciplineNode_controlAction_termsAttribute(node, discipline, "СемКП", TYPE_CONTROL_COURSE_PROJECT);

                        process_disciplineNode_loadType_loadAttribute(node, discipline, 0, "ПодлежитИзучению", TYPE_LOAD_TOTAL);
                        process_disciplineNode_loadType_loadAttribute(node, discipline, 0, "КредитовНаДисциплину", TYPE_LOAD_LABOR);

                        // не надо - грузим всегда из нагрузки по семестрам
                        // process_disciplineNode_loadType_loadAttribute(node, discipline, 0, "СР", TYPE_LOAD_SELFWORK);
                    }

                    // нагрузка по семестрам
                    {
                        final NodeList termChildNodes = node.getChildNodes();
                        for (int n = 0; n < termChildNodes.getLength(); n++)
                        {
                            final Node termNode = termChildNodes.item(n);

                            // игнорируем левые теги и свякий прочий мусор
                            if (termNode.getNodeType() != Node.ELEMENT_NODE)
                            {
                                continue;
                            }
                            if (!termNode.getNodeName().equals("Сем"))
                            {
                                continue;
                            }

                            // номер семестра
                            final Integer number = toInteger(termNode, "Ном");
                            if ((null == number) || (number.intValue() <= 0))
                            {
                                log4j_logger.warn(" ! Пропущен «" + str(termNode) + "» - отсутствует аттрибут «Ном».");
                                continue;
                            }

                            // данные по части дисциплины
                            {
                                process_disciplineNode_loadType_loadAttribute(termNode, discipline, number, "Лек", TYPE_LOAD_A_LECTURES);
                                process_disciplineNode_loadType_loadAttribute(termNode, discipline, number, "Пр", TYPE_LOAD_A_PRACTICE);
                                process_disciplineNode_loadType_loadAttribute(termNode, discipline, number, "Лаб", TYPE_LOAD_A_LABORATORY);

                                process_disciplineNode_loadType_loadAttribute(termNode, discipline, number, "СРС", TYPE_LOAD_SELFWORK);

                                if (mergeSelfWorkFromanotherSources)
                                {
                                    process_disciplineNode_loadType_loadAttribute(termNode, discipline, number, "КСР", TYPE_LOAD_SELFWORK);
                                    process_disciplineNode_loadType_loadAttribute(termNode, discipline, number, "ЧасЭкз", TYPE_LOAD_SELFWORK);
                                }
                            }
                        }
                    }

                } catch (final Throwable t)
                {
                    errorList.add(" ! Ошибка при обработке " + str(node) + ":\n" + t.getMessage());
                    log4j_logger.error(" ! Ошибка при обработке " + str(node) + ":\n" + t.getMessage(), t);
                    hasError = true;
                }
            }

        }

        /* практики (учебные) */
        {
            log4j_logger.info("---- учебные практики ----------------------------------------------");
            final NodeList nodeList = doc.getElementsByTagName("УчебПрактика");
            for (int i = 0; i < nodeList.getLength(); i++)
            {
                // берем нод, если не подходит - пропускаем
                final Node node = nodeList.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE)
                {
                    continue;
                }

                // печатаем тэг
                log4j_logger.info(str(node));

                // проверяем, что он в требуемом нам месте
                final String parentNodeName = node.getParentNode().getNodeName();
                if (!parentNodeName.equals("УчебПрактики"))
                {
                    log4j_logger.warn(" ! Пропущен - не находится в «УчебПрактики».");
                    continue;
                }

                try
                {
                    // проверяем, что заполнен аттрибут "Вид"
                    final String practiceKind = toString(node, "Вид");
                    if (practiceKind.isEmpty())
                    {
                        log4j_logger.warn(" ! Пропущен - отсутствует аттрибут «Вид».");
                        continue;
                    }

                    // номер семестра
                    final Integer number = toInteger(node, "Сем");
                    if ((null == number) || (number.intValue() <= 0))
                    {
                        log4j_logger.warn(" ! Пропущен - отсутсвует аттрибут «Сем».");
                        continue;
                    }

                    log4j_logger.info("   [" + number + ", " + practiceKind + "]");

                    // создаем практику в структуре (далее - если возникает ошибка, то весь файл ИМЦы признается ошибочным)
                    final String idPath = (parentNodeName + ":" + practiceKind).replace('.', ' ').replaceAll("[ ]", "-").trim();
                    final ImtsaPractice tPractice = result.get(idPath, ImtsaPractice.class);
                    tPractice.title = practiceKind;
                    tPractice.kind = KIND_CODE_PRACTICE_TEACH;

                    // кафедра
                    final String disciplineOwner = toString(node, "Кафедра");
                    if (null != disciplineOwner)
                    {
                        tPractice.orgUnitCode = disciplineOwner;
                    }

                    process_disciplineNode_loadType_loadAttribute(node, tPractice, number, "Нед", TYPE_LOAD_WEEKS);

                } catch (final Throwable t)
                {
                    errorList.add(" ! Ошибка при обработке " + str(node) + ":\n" + t.getMessage());
                    log4j_logger.error(" ! Ошибка при обработке " + str(node) + ":\n" + t.getMessage(), t);
                    hasError = true;
                }
            }
        }

        /* практики (прочие) */
        {
            log4j_logger.info("---- прочие практики -----------------------------------------------");
            final NodeList nodeList = doc.getElementsByTagName("ПрочаяПрактика");
            for (int i = 0; i < nodeList.getLength(); i++)
            {
                // берем нод, если не подходит - пропускаем
                final Node node = nodeList.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE)
                {
                    continue;
                }

                // печатаем тэг
                log4j_logger.info(str(node));

                // проверяем, что он в требуемом нам месте
                final String parentNodeName = node.getParentNode().getNodeName();
                if (!(parentNodeName.equals("ПрочиеПрактики") || parentNodeName.equals("ПреддипломныеПрактики")))
                {
                    log4j_logger.warn(" ! Пропущен - не находится в «ПрочиеПрактики», «ПреддипломныеПрактики».");
                    continue;
                }

                try
                {
                    // проверяем, что заполнен аттрибут "Вид"
                    final String practiceKind = toString(node, "Вид");
                    if (practiceKind.isEmpty())
                    {
                        log4j_logger.warn(" ! Пропущен - отсутствует аттрибут «Вид».");
                        continue;
                    }

                    // номер семестра
                    final Integer number = toInteger(node, "Сем");
                    if ((null == number) || (number.intValue() <= 0))
                    {
                        log4j_logger.warn(" ! Пропущен - отсутсвует аттрибут «Сем».");
                        continue;
                    }

                    log4j_logger.info("   [" + number + ", " + practiceKind + "]");

                    // создаем практику в структуре (далее - если возникает ошибка, то весь файл ИМЦы признается ошибочным)
                    final String idPath = (parentNodeName + ":" + practiceKind).replace('.', ' ').trim().replaceAll("[ ]+", "_").trim();
                    final ImtsaPractice xPractice = result.get(idPath, ImtsaPractice.class);
                    xPractice.title = practiceKind;
                    xPractice.kind = KIND_CODE_PRACTICE;

                    // кафедра
                    final String disciplineOwner = toString(node, "Кафедра");
                    if (null != disciplineOwner)
                    {
                        xPractice.orgUnitCode = disciplineOwner;
                    }

                    process_disciplineNode_loadType_loadAttribute(node, xPractice, number, "Нед", TYPE_LOAD_WEEKS);

                    // TODO: fillme
                } catch (final Throwable t)
                {
                    errorList.add(" ! Ошибка при обработке «" + str(node) + "»:\n" + t.getMessage());
                    log4j_logger.error(" ! Ошибка при обработке «" + str(node) + "»:\n" + t.getMessage(), t);
                    hasError = true;
                }
            }
        }

        // проверяем распределенность компетенций:
        // в каждой группе могут оставаться только последовательные номера, оканчивающиеся на максимальный номер в группе (флеш-рояль)
        boolean hasCompetenceErrors = false;
        for (Map.Entry<String, Set<Integer>> entry : compNumberMap.entrySet())
        {
            int prevNumber = -1;
            for (Integer number : entry.getValue())
            {
                if (prevNumber != -1)
                {
                    if (number - prevNumber > 1)
                    {
                        hasCompetenceErrors = true;
                        break;
                    }

                    index2ContentMap.remove(new PairKey<>(entry.getKey(), number));
                }

                prevNumber = number;
            }

            if (prevNumber != -1 && prevNumber != compMaxNumber.get(entry.getKey()))
            {
                hasCompetenceErrors = true;
            }

            if (hasCompetenceErrors)
            {
                errorList.add(" ! В файле содержатся нераспределенные компетенции: " + " «" + entry.getKey() + "».");
                log4j_logger.error(" ! В файле содержатся нераспределенные компетенции: " + " «" + entry.getKey() + "».");
                hasError = true;
            }
        }


        if (hasError)
        {
            throw new IllegalStateException("Документ содержит ошибки.");
        }

        // записываем импортируемые компетенции
        for (Map.Entry<PairKey<String, Integer>, String> entry : index2ContentMap.entrySet())
        {
            result.competences.add(new ImtsaCompetence(entry.getKey().getFirst(), entry.getKey().getSecond(), entry.getValue()));
        }

        // выводим сообщения
        // формируем индексы
        {
            log4j_logger.info("---- итого ---------------------------------------------------------");
            final Set<String> codePrefixSet = new HashSet<>();

            for (final ImtsaNode node : result)
            {
                final String prefix = node.getHierarchyIndex() + " " + node.getTypeChar() + "[" + node.getIndexPart() + "]";
                if (node instanceof ImtsaDiscipline)
                {
                    // дисциплина (здесь делать ничего не надо)
                    log4j_logger.info(prefix + " «" + node.getTitle() + "» " + ((ImtsaDiscipline) node).getTerm2loadMap().toString());
                } else if (node instanceof ImtsaGroup)
                {
                    // группа (здесь делать ничего не надо)
                    log4j_logger.info(prefix + " «" + node.getTitle() + "»");
                } else if (node instanceof ImtsaVariant)
                {
                    // дисциплина по выбору (нужно укзать название дисциплины по выбору)
                    final Matcher matcher = VARIANT_INDEX_PART_MASK.matcher(node.getIndexPart());
                    if (!matcher.matches())
                    {
                        throw new IllegalStateException("wtf?");
                    }

                    // указывем название нода - номер дисциплины по выбору
                    node.title = "Выбор " + matcher.replaceAll("$1");

                    log4j_logger.info(prefix + " «" + node.getTitle() + "»");
                } else
                {
                    // узел (цикл, компонент, индекс, часть)
                    // указываем путь

                    String path = "";
                    ImtsaNode tmp = node;
                    while (null != tmp)
                    {
                        if (tmp.getHierarchyIndex().indexOf('.') < 0)
                        {
                            // дошли до цикла
                            final String c = cycleAbvr2TitleMap.get(tmp.getIndexPart().toUpperCase());
                            if (null == c)
                            {
                                errorList.add("Для цикла \\\"\" + tmp.getIndexPart() + \"\\\" не определено название.");
                                log4j_logger.error("Для цикла \"" + tmp.getIndexPart() + "\" не определено название.");
                                hasError = true;
                            }
                            path = ("[" + c + "]" + (path.length() > 0 ? ("." + path) : ""));
                            break;
                        }
                        final String c = tmp.getIndexPart();
                        path = ("[" + c + "]" + (path.length() > 0 ? ("." + path) : ""));
                        tmp = tmp.getParent();
                    }

                    // !!! сохраняем код элемента справочника !!!
                    // для узла пытаемся найти код элемента справочника структуры

                    final String nodeCode = getNodeCode(path);
                    if (NO_CODE.equals(node.title = (nodeCode /*+"; "+path*/)))
                    {
                        hasError = true;
                        errorList.add("Для «" + path + "» отсутствует последовательность кодов справочника.");
                        log4j_logger.error("Для «" + path + "» отсутствует последовательность кодов справочника.");
                    }

                    codePrefixSet.add(StringUtils.substringBefore(nodeCode, "."));
                    log4j_logger.info(prefix + ": " + node.getTitle() + "; " + path);
                }
            }

            if (codePrefixSet.size() > 1)
            {
                hasError = true;
                log4j_logger.error("Используются элементы справочника структуры из разных поколений: " + codePrefixSet);
                errorList.add("Используются элементы справочника структуры из разных поколений: " + codePrefixSet);
            }
        }

        if (hasError)
        {
            throw new IllegalStateException("Документ содержит ошибки.");
        }

        return result;

    }

    /**
     * сохраняет в дисциплине данные нагрузки по строке в семестр
     *
     * @param node          нод
     * @param discipline    дисциплина
     * @param termNumber    номер семестра (0 - вся дисциплина)
     * @param attribureName аттрибут со значением (будет превращен в double)
     * @param loadKey       код нагрузки во внешней системе
     */
    private static void process_disciplineNode_loadType_loadAttribute(final Node node, final ImtsaDiscipline discipline, final int termNumber, final String attribureName, final String loadKey)
    {
        final Double load = toDouble(node, attribureName);
        if (load.doubleValue() > 0d)
        {
            final Map<String, Number> map = discipline.getTermLoadMap(termNumber);
            final Number n = map.get(loadKey);
            map.put(loadKey, load.doubleValue() + (null == n ? 0d : n.doubleValue()));
        }
    }

    /**
     * сохраняет в дисциплине формы контроля по семестрам
     *
     * @param node          нод
     * @param discipline    дисциплина
     * @param attribureName аттрибут со списокм кодов семестров (в формате ИМЦА)
     * @param loadKey       код нагрузки во внешней системе
     */
    private static void process_disciplineNode_controlAction_termsAttribute(final Node node, final ImtsaDiscipline discipline, final String attribureName, final String loadKey)
    {
        final Collection<Integer> exams = toTermNumbers(node, attribureName);
        for (final Integer term : exams)
        {
            final Map<String, Number> map = discipline.getTermLoadMap(term);
            final Number n = map.get(loadKey);
            map.put(loadKey, 1 + (null == n ? 0 : n.intValue()));
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* утильный метод, разбивает индекс компетенции на сокр. название группы и номер */
    private static PairKey<String, Integer> getCompetenceGroupAndNumber(String index) throws IllegalArgumentException
    {
        int hyphenPos = index.indexOf("-");
        if (hyphenPos != index.lastIndexOf("-") || hyphenPos < 1 || hyphenPos > index.length() - 2)
            throw new NumberFormatException();

        String groupShortTitle = index.substring(0, hyphenPos);
        int number = Integer.valueOf(index.substring(hyphenPos + 1));

        if (number <= 0)
            throw new IllegalArgumentException();

        return new PairKey<>(groupShortTitle, number);
    }

    /* преобразует аттрибут нода в строку (отсутствие значения = пустая строка) */
    private static String toString(final Node node, final String attributeName)
    {
        final Node attr = node.getAttributes().getNamedItem(attributeName);
        if (null == attr)
        {
            return "";
        }

        return StringUtils.trimToEmpty(attr.getNodeValue());
    }

    /* преобразует аттрибут нода в целое число (отсутствие значения = 0) */
    private static Integer toInteger(final Node node, final String attributeName)
    {
        final Node attr = node.getAttributes().getNamedItem(attributeName);
        if (null == attr)
        {
            return 0;
        }

        return toInteger(StringUtils.trimToEmpty(attr.getNodeValue()));
    }

    /* преобразует строку в целое число */
    private static Integer toInteger(final String str)
    {
        try
        {
            return Integer.valueOf(str);
        } catch (final NumberFormatException e)
        {
            throw new RuntimeException("Некорректный формат записи числа «" + str + "».\n" + e.getMessage(), e);
        }
    }

    /* преобразует значение аттрибута нода в вещественное число (отсутствие значения = 0) */
    private static Double toDouble(final Node node, final String attributeName)
    {
        final Node attr = node.getAttributes().getNamedItem(attributeName);
        if (null == attr)
        {
            return 0d;
        }

        return toDouble(StringUtils.trimToEmpty(attr.getNodeValue()));
    }

    /* преобразует строку в вещественное число (в любой локали) */
    public static Double toDouble(final String str)
    {
        final DecimalFormat decimalFormat = new DecimalFormat();
        final char decimalSeparator = decimalFormat.getDecimalFormatSymbols().getDecimalSeparator();
        try
        {
            return decimalFormat.parse(str.replace('.', decimalSeparator).replace(',', decimalSeparator)).doubleValue();
        } catch (final ParseException e)
        {
            throw new RuntimeException("Некорректный формат записи числа «" + str + "».\n" + e.getMessage(), e);
        }
    }

    public static String toStringNull(Node node, String attributeName)
    {
        Node attr = node.getAttributes().getNamedItem(attributeName);
        return attr == null ? null : StringUtils.trimToEmpty(attr.getNodeValue());
    }

    public static Integer toIntegerNull(Node node, String attributeName)
    {
        Node attr = node.getAttributes().getNamedItem(attributeName);
        return attr == null ? null : toInteger(StringUtils.trimToEmpty(attr.getNodeValue()));
    }

    public static Double toDoubleNull(Node node, String attributeName)
    {
        Node attr = node.getAttributes().getNamedItem(attributeName);
        return attr == null ? null : toDouble(StringUtils.trimToEmpty(attr.getNodeValue()));
    }

    /* превращает аттрибут нода, содержащий коды номеров семестров, в список номеров семестров (отсутствие значения = пустой список) */
    private static Collection<Integer> toTermNumbers(final Node node, final String attributeName)
    {
        final Node attr = node.getAttributes().getNamedItem(attributeName);
        if (null == attr)
        {
            return Collections.emptyList();
        }

        final String textContent = StringUtils.trimToEmpty(attr.getNodeValue()).toLowerCase();
        final Set<Integer> terms = new TreeSet<>();
        for (final char c : textContent.toCharArray())
        {
            if (!terms.add(toTermNumber(c)))
            {
                throw new IllegalStateException("Некорректное значение «" + textContent + "», повторяющиеся номера семестров.");
            }
        }
        return new ArrayList<>(terms);
    }

    /* правила пересчет кодов симвовлов в номера семестров, согласно правилам ИМЦА */
    private static final Map<Character, Integer> TERM_NUMBERS = new HashMap<>();

    static
    {
        final char[] numbers = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
        for (int i = 0; i < numbers.length; i++)
        {
            if (null != TERM_NUMBERS.put(Character.toLowerCase(numbers[i]), i))
            {
                throw new IllegalStateException("Initialization error.");
            }
        }
    }

    /* превращает меместр из символа в номер, согласно правилам ИМЦА */
    private static Integer toTermNumber(final char c)
    {
        final Integer n = TERM_NUMBERS.get(Character.toLowerCase(c));
        if (null == n)
        {
            throw new IllegalStateException("Некорректный номер семестра: «" + c + "».");
        }
        return n;
    }

    /* аналог node.toString() - рисует node со всеми аттрибутами */
    public static String str(final Node node)
    {
        final StringBuilder sb = new StringBuilder("<").append(node.getNodeName());
        final NamedNodeMap attrs = node.getAttributes();
        for (int i = 0; i < attrs.getLength(); i++)
        {
            final Node item = attrs.item(i);
            sb.append(" ").append(item.getNodeName()).append("=\"").append(item.getNodeValue()).append("\"");
        }
        return sb.append(">").toString();
    }

    /* утильный метод, создает xml-документ из файла */
    private static Document parseXml(final File file) throws IOException, SAXException, ParserConfigurationException
    {
        try
        {
            final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            final DocumentBuilder db = dbf.newDocumentBuilder();
            final Document doc = db.parse(file);
            doc.getDocumentElement().normalize();
            return doc;
        } catch (final Exception t)
        {
            log4j_logger.error("Неверная структура xml в файле «" + file.getName() + "».\n" + t.getMessage(), t);
            throw t;
        }
    }

    /* утильный метод, создает xml-документ из потока */
    private static Document parseXml(final InputStream inputStream)
    {
        try
        {
            final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            final DocumentBuilder db = dbf.newDocumentBuilder();
            final Document doc = db.parse(inputStream);
            doc.getDocumentElement().normalize();
            return doc;

        } catch (final Throwable t)
        {
            log4j_logger.error("Неверная структура xml в файле»." + t.getMessage(), t);
            throw new IllegalStateException("Неверная структура xml в файле».");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* test only */
/*    public static void main(final String[] args)
    {
        final File dir = new File("xml");
        if (!dir.isDirectory())
        {
            throw new IllegalStateException("No «xml» folder found.");
        }

        final FilenameFilter filter = new FilenameFilter()
        {
            @Override
            public boolean accept(final File dir, final String name)
            {
                return StringUtils.trimToEmpty(name).endsWith(".xml");
            }
        };

        final List<String> errorFiles = new ArrayList<String>();
        try
        {
            for (final File file : dir.listFiles(filter))
            {
                final ImtsaFile imtsa = parse(file);
                if (null == imtsa)
                {
                    errorFiles.add(file.getAbsolutePath());
                    continue;
                }

                for (final ImtsaNode node : imtsa)
                {
                    if (ImtsaNode.class.equals(node.getClass()))
                    {

                        @SuppressWarnings("unused")
                        final String codes = node.getTitle().split(";")[0];
                        // todo: use code in mdb
                    }
                }
            }
        } finally
        {
            ImtsaFile.saveGlobalNodeCodeMap();
        }

        // глобальная ошибка
        boolean error = false;

        // выводим содержимое справочника кодов (все)
        printNodeCodeMap();

        // это файлы с ошибками
        if (errorFiles.size() > 0)
        {
            log4j_logger.error("");
            log4j_logger.error("---- файлы, содержащие ошибки --------------------------------------");
            for (final String f : errorFiles)
            {
                log4j_logger.error(f);
                error = true;
            }
            log4j_logger.error("");
        }

        // выводим содержимое справочника кодов (ошибки)
        error |= printNodeCodeMapErrors();

        // если были ошибки - возвращаем код, отличный от 0
        if (error)
        {
            System.exit(1);
        }
    }*/
}