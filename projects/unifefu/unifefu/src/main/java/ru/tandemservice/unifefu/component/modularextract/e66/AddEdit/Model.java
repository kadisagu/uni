/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e66.AddEdit;

import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;

/**
 * @author Alexey Lopatin
 * @since 22.11.2013
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e66.AddEdit.Model implements IExtEducationLevelModel, IGroupModel
{
    @Override
    public EducationLevels getParentEduLevel()
    {
        return EducationOrgUnitUtil.getParentLevel(getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
    }

    @Override
    public Course getCourse()
    {
        return getEduModel().getCourse();
    }
}
