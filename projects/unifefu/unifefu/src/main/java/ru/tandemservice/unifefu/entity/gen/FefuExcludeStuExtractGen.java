package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuExcludeStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. Об отчислении (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuExcludeStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuExcludeStuExtract";
    public static final String ENTITY_NAME = "fefuExcludeStuExtract";
    public static final int VERSION_HASH = -2141933650;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String P_TWO_DIPLOMA_INTERNATIONAL_PROGRAM = "twoDiplomaInternationalProgram";

    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private boolean _twoDiplomaInternationalProgram;     // Обучение по двухдипломной программе

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Обучение по двухдипломной программе. Свойство не может быть null.
     */
    @NotNull
    public boolean isTwoDiplomaInternationalProgram()
    {
        return _twoDiplomaInternationalProgram;
    }

    /**
     * @param twoDiplomaInternationalProgram Обучение по двухдипломной программе. Свойство не может быть null.
     */
    public void setTwoDiplomaInternationalProgram(boolean twoDiplomaInternationalProgram)
    {
        dirty(_twoDiplomaInternationalProgram, twoDiplomaInternationalProgram);
        _twoDiplomaInternationalProgram = twoDiplomaInternationalProgram;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuExcludeStuExtractGen)
        {
            setStudentStatusOld(((FefuExcludeStuExtract)another).getStudentStatusOld());
            setTwoDiplomaInternationalProgram(((FefuExcludeStuExtract)another).isTwoDiplomaInternationalProgram());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuExcludeStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuExcludeStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuExcludeStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "twoDiplomaInternationalProgram":
                    return obj.isTwoDiplomaInternationalProgram();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "twoDiplomaInternationalProgram":
                    obj.setTwoDiplomaInternationalProgram((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                        return true;
                case "twoDiplomaInternationalProgram":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return true;
                case "twoDiplomaInternationalProgram":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return StudentStatus.class;
                case "twoDiplomaInternationalProgram":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuExcludeStuExtract> _dslPath = new Path<FefuExcludeStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuExcludeStuExtract");
    }
            

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuExcludeStuExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Обучение по двухдипломной программе. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuExcludeStuExtract#isTwoDiplomaInternationalProgram()
     */
    public static PropertyPath<Boolean> twoDiplomaInternationalProgram()
    {
        return _dslPath.twoDiplomaInternationalProgram();
    }

    public static class Path<E extends FefuExcludeStuExtract> extends ModularStudentExtract.Path<E>
    {
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private PropertyPath<Boolean> _twoDiplomaInternationalProgram;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Предыдущее состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuExcludeStuExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Обучение по двухдипломной программе. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuExcludeStuExtract#isTwoDiplomaInternationalProgram()
     */
        public PropertyPath<Boolean> twoDiplomaInternationalProgram()
        {
            if(_twoDiplomaInternationalProgram == null )
                _twoDiplomaInternationalProgram = new PropertyPath<Boolean>(FefuExcludeStuExtractGen.P_TWO_DIPLOMA_INTERNATIONAL_PROGRAM, this);
            return _twoDiplomaInternationalProgram;
        }

        public Class getEntityClass()
        {
            return FefuExcludeStuExtract.class;
        }

        public String getEntityName()
        {
            return "fefuExcludeStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
