/* $Id$ */
package ru.tandemservice.unifefu.base.ext.UniStudent.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.FefuLevelTypeFilter;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 17.10.2013
 */
public class FefuEduProgramEducationOrgUnitAddon extends UniEduProgramEducationOrgUnitAddon
{
    public static final FefuLevelTypeFilter FEFU_LEVEL_TYPE = new FefuLevelTypeFilter();
    public static final Filters FEFU_FORMATIVE_ORG_UNIT = new Filters("FORMATIVE_ORG_UNIT", EducationOrgUnit.formativeOrgUnit(), "Формирующее подразделение", Arrays.asList("TERRITORIAL_ORG_UNIT", "PRODUCING_ORG_UNIT", "DEVELOP_FORM", "DEVELOP_CONDITION", "DEVELOP_TECH", "DEVELOP_PERIOD"), EducationOrgUnit.formativeOrgUnit().fullTitle(), EducationOrgUnit.formativeOrgUnit().id(), OrgUnit.title(), OrgUnit.fullTitle(), OrgUnit.class);
    public static final Filters FEFU_TERRITORIAL_ORG_UNIT = new Filters("TERRITORIAL_ORG_UNIT", EducationOrgUnit.territorialOrgUnit(), "Территориальное подразделение", Arrays.asList("PRODUCING_ORG_UNIT", "DEVELOP_FORM", "DEVELOP_CONDITION", "DEVELOP_TECH", "DEVELOP_PERIOD"), EducationOrgUnit.territorialOrgUnit().territorialFullTitle(), EducationOrgUnit.territorialOrgUnit().id(), OrgUnit.territorialFullTitle(), OrgUnit.territorialFullTitle(), OrgUnit.class);
    public static final Filters FEFU_PRODUCING_ORG_UNIT = new Filters("PRODUCING_ORG_UNIT", EducationOrgUnit.educationLevelHighSchool().orgUnit(), "Выпускающее подразделение", Arrays.asList("DEVELOP_FORM", "DEVELOP_CONDITION", "DEVELOP_TECH", "DEVELOP_PERIOD"), EducationOrgUnit.educationLevelHighSchool().orgUnit().fullTitle(), EducationOrgUnit.educationLevelHighSchool().orgUnit().id(), OrgUnit.title(), OrgUnit.fullTitle(), OrgUnit.class);

    public static final Filters FEFU_INCLUDE_SPECIALIZATIONS_AND_PROFILES =
            new Filters("INCLUDE_PROFILES", null, "Учитывать дочерние направления подготовки", Arrays.asList("EDUCATION_LEVEL_HIGH_SCHOOL"), null, null, null, null, null)
            {
                @Override
                public void applyFilter(DQLSelectBuilder builder, Object value, String alias, UniEduProgramEducationOrgUnitAddon addon) { /* nop */ }
            };

    public static final Filters FEFU_EDUCATION_LEVEL_HIGH_SCHOOL = new Filters
            (
                    Filters.EDUCATION_LEVEL_HIGH_SCHOOL.getSettingsName(),
                    Filters.EDUCATION_LEVEL_HIGH_SCHOOL.getEntityPath(),
                    Filters.EDUCATION_LEVEL_HIGH_SCHOOL.getFieldName(),
                    Arrays.asList("FORMATIVE_ORG_UNIT", "TERRITORIAL_ORG_UNIT", "PRODUCING_ORG_UNIT", "DEVELOP_FORM", "DEVELOP_CONDITION", "DEVELOP_TECH", "DEVELOP_PERIOD"),
                    Filters.EDUCATION_LEVEL_HIGH_SCHOOL.getTitleFilterPath(),
                    Filters.EDUCATION_LEVEL_HIGH_SCHOOL.getPrimaryKeyPath(),
                    Filters.EDUCATION_LEVEL_HIGH_SCHOOL.getOrderFilterPath(),
                    Filters.EDUCATION_LEVEL_HIGH_SCHOOL.getTitlePath(),
                    Filters.EDUCATION_LEVEL_HIGH_SCHOOL.getClazz()
            )
    {
        @Override
        public void applyFilter(DQLSelectBuilder builder, Object value, String alias, UniEduProgramEducationOrgUnitAddon addon)
        {
            if (!YES_ITEM.equals(addon.getFilterValue(FEFU_INCLUDE_SPECIALIZATIONS_AND_PROFILES)))
                super.applyFilter(builder, value, alias, addon);
            else
            {
                if (value instanceof Collection)
                {
                    Collection<EducationLevels> parentLevels = new ArrayList<>(((Collection) value).size());
                    for (Object o : (Collection) value)
                    {
                        parentLevels.add(((EducationLevelsHighSchool)o).getEducationLevel());
                    }
                    builder.where(or(
                        in(property(EducationOrgUnit.educationLevelHighSchool().fromAlias(alias)), (Collection) value),
                        in(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().parentLevel().fromAlias(alias)), parentLevels)
                    ));
                }
                else
                {
                    EducationLevelsHighSchool hs = (EducationLevelsHighSchool) value;
                    builder.where(or(
                        eq(property(EducationOrgUnit.educationLevelHighSchool().fromAlias(alias)), value(hs)),
                        eq(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().parentLevel().fromAlias(alias)), value(hs.getEducationLevel()))
                    ));
                }
            }
        }
    };

    public static final IdentifiableWrapper YES_ITEM = new IdentifiableWrapper(1L, "Да");

    public static final Filters[] FEFU_FILTERS = {
            FEFU_LEVEL_TYPE,
            Filters.QUALIFICATION,
            FEFU_EDUCATION_LEVEL_HIGH_SCHOOL,
            FEFU_INCLUDE_SPECIALIZATIONS_AND_PROFILES,
            FEFU_FORMATIVE_ORG_UNIT,
            FEFU_TERRITORIAL_ORG_UNIT,
            FEFU_PRODUCING_ORG_UNIT,
            Filters.DEVELOP_CONDITION,
            Filters.DEVELOP_PERIOD,
            Filters.DEVELOP_TECH
    };
    public static final Collection<Filters> FEFU_FILTER_SET = Arrays.asList(FEFU_FILTERS);

    public FefuEduProgramEducationOrgUnitAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        getModelsMap().put(FEFU_LEVEL_TYPE, new CommonFilterSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StructureEducationLevels.class, "level")
                        .where(isNull(property("level", StructureEducationLevels.parent())))
                        .order(property("level", StructureEducationLevels.priority()));

                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("level", StructureEducationLevels.shortTitle()), value(CoreStringUtils.escapeLike(filter, true))));

                if (o != null)
                {
                    if (o instanceof Set)
                        builder.where(in(property("level.id"), (Collection) o));
                    else
                        builder.where(eq(property("level.id"), commonValue(o, PropertyType.LONG)));
                }

                return new DQLListResultBuilder(builder, 50);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return (String) FastBeanUtils.getValue(value, FEFU_LEVEL_TYPE.getTitlePath());
            }
        });

        getModelsMap().put(FEFU_INCLUDE_SPECIALIZATIONS_AND_PROFILES, new CommonFilterSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IdentifiableWrapper> result = new ArrayList<>(1);
                if (primaryKeys != null && !primaryKeys.isEmpty() && primaryKeys.contains(YES_ITEM.getId()))
                    result.add(YES_ITEM);
                return result;
            }

            public ListResult findValues(String filter)
            {
                if (filter == null || filter.isEmpty() || StringUtils.containsIgnoreCase(YES_ITEM.getTitle(), filter))
                    return new ListResult<>(YES_ITEM);
                return ListResult.getEmpty();
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return YES_ITEM.getId().equals(primaryKey) ? YES_ITEM : null;
            }

            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                return null;
            }
        }
        );
    }

    @Override
    protected Collection<Filters> getPossibleFilters()
    {
        return FEFU_FILTER_SET;
    }
}