/**
 * GetReferenceChangedFromResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class GetReferenceChangedFromResponse  implements java.io.Serializable {
    private java.lang.String getReferenceChangedFromResult;

    public GetReferenceChangedFromResponse() {
    }

    public GetReferenceChangedFromResponse(
           java.lang.String getReferenceChangedFromResult) {
           this.getReferenceChangedFromResult = getReferenceChangedFromResult;
    }


    /**
     * Gets the getReferenceChangedFromResult value for this GetReferenceChangedFromResponse.
     * 
     * @return getReferenceChangedFromResult
     */
    public java.lang.String getGetReferenceChangedFromResult() {
        return getReferenceChangedFromResult;
    }


    /**
     * Sets the getReferenceChangedFromResult value for this GetReferenceChangedFromResponse.
     * 
     * @param getReferenceChangedFromResult
     */
    public void setGetReferenceChangedFromResult(java.lang.String getReferenceChangedFromResult) {
        this.getReferenceChangedFromResult = getReferenceChangedFromResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetReferenceChangedFromResponse)) return false;
        GetReferenceChangedFromResponse other = (GetReferenceChangedFromResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getReferenceChangedFromResult==null && other.getGetReferenceChangedFromResult()==null) || 
             (this.getReferenceChangedFromResult!=null &&
              this.getReferenceChangedFromResult.equals(other.getGetReferenceChangedFromResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetReferenceChangedFromResult() != null) {
            _hashCode += getGetReferenceChangedFromResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetReferenceChangedFromResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">GetReferenceChangedFromResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getReferenceChangedFromResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "GetReferenceChangedFromResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
