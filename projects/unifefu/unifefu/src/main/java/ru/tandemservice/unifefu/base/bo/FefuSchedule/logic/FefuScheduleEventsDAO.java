/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSchedule.logic;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.*;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.MutableDateTime;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.FefuScheduleManager;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.utils.fefuICal.FefuScheduleICalXST;
import ru.tandemservice.unifefu.utils.fefuICal.FefuScheduleVEventICalXST;
import ru.tandemservice.unifefu.utils.fefuICal.FefuTeacherScheduleICalXST;
import ru.tandemservice.unifefu.utils.fefuICal.FefuTeacherScheduleVEventICalXST;
import ru.tandemservice.unifefu.ws.eventsAxis2.FefuScheduleICalCommits;
import ru.tandemservice.unifefu.ws.eventsAxis2.FefuSubmitEventsClient;
import ru.tandemservice.unifefu.ws.eventsAxis2.FefuTeacherScheduleICalCommits;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleEventsDAO;
import ru.tandemservice.unispp.base.entity.*;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 9/11/13
 */
@Transactional
public class FefuScheduleEventsDAO extends SppScheduleEventsDAO implements IFefuScheduleEventsDAO
{
    @Override
    public synchronized void sendICalendars()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppSchedule.class, "s");
        builder.where(eq(property("s", SppSchedule.approved()), value(true)));
        builder.where(eq(property("s", SppSchedule.group().archival()), value(false)));
        Date currentDate = new org.joda.time.DateTime().withTime(0, 0, 0, 1).toDate();
        builder.where(and(
                le(property("s", SppSchedule.season().startDate()), valueDate(currentDate)),
                ge(property("s", SppSchedule.season().endDate()), valueDate(currentDate))
        ));

        List<SppSchedule> fefuSchedules = builder.createStatement(getSession()).list();
        fefuSchedules.forEach(this::sendICalendar);

        //Удаляются данные ICalendar которые не привязаны к расписаниям занятий и сессий
        DQLSelectBuilder iCalDataBuilder = new DQLSelectBuilder().fromEntity(SppScheduleICal.class, "ical");
        iCalDataBuilder.where(or(
                notExists(new DQLSelectBuilder().fromEntity(SppSchedule.class, "sch")
                        .where(eq(property("ical", SppScheduleICal.id()), property("sch", SppSchedule.icalData().id()))).buildQuery()),
                notExists(new DQLSelectBuilder().fromEntity(SppScheduleSession.class, "schs")
                        .where(eq(property("ical", SppScheduleICal.id()), property("schs", SppScheduleSession.icalData().id()))).buildQuery()),
                notExists(new DQLSelectBuilder().fromEntity(SppSchedule.class, "sch")
                        .where(eq(property("ical", SppScheduleICal.id()), property("sch", SppSchedule.icalTeacherData().id()))).buildQuery()),
                notExists(new DQLSelectBuilder().fromEntity(SppScheduleSession.class, "schs")
                        .where(eq(property("ical", SppScheduleICal.id()), property("schs", SppScheduleSession.icalTeacherData().id()))).buildQuery()
                )));
        cancelICalendar(iCalDataBuilder.createStatement(getSession()).<SppScheduleICal>list());
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void cancelICalendar(List<SppScheduleICal> iCalDataList)
    {
        if (iCalDataList.isEmpty()) return;
        if (null == ApplicationRuntime.getProperty(FefuSubmitEventsClient.PORTAL_SERVICE_EVENTS_URL))
        {
            throw new ApplicationException("Приложение не настроено для работы с веб-сервисом.");
        }

        List<SppScheduleICal> iCalDataListForDelete = Lists.newArrayList();
        for (SppScheduleICal iCalData : iCalDataList)
        {
            // TODO: add teacher

            Object obj = FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml());
            if (obj instanceof FefuScheduleICalXST)
            {
                FefuScheduleICalXST scheduleICalXST = (FefuScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml());
                if (scheduleICalXST.getEvents().isEmpty() || scheduleICalXST.getStudentsNSIIds().isEmpty())
                {
                    iCalDataListForDelete.add(iCalData);
                    continue;
                }
                int sequence = iCalData.getSequence();
                String iCalendar = getICalendar(scheduleICalXST.getEvents(), null, sequence, true);
                FefuScheduleICalCommits request = FefuSubmitEventsClient.sendEvents(iCalData.getId().toString(), null, scheduleICalXST.getStudentsNSIIds(), iCalendar, getVEventsMap(scheduleICalXST.getEvents()));
                if (!request.isRequestFailed())
                    iCalDataListForDelete.add(iCalData);
            }
            else if (obj instanceof FefuTeacherScheduleICalXST)
            {
                FefuTeacherScheduleICalXST scheduleICalXST = (FefuTeacherScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml());
                if (scheduleICalXST.getEvents().isEmpty())
                {
                    iCalDataListForDelete.add(iCalData);
                    continue;
                }

                int sequence = iCalData.getSequence() + 1;

                Map<String, List<FefuTeacherScheduleVEventICalXST>> cancelEventsMap = Maps.newHashMap();
                for (FefuTeacherScheduleVEventICalXST event : scheduleICalXST.getEvents())
                {
                    SafeMap.safeGet(cancelEventsMap, event.getTeacherNSIId(), ArrayList.class).add(event);
                }

                List<FefuTeacherScheduleVEventICalXST> failedEvents = Lists.newArrayList();

                for (Map.Entry<String, List<FefuTeacherScheduleVEventICalXST>> entry : cancelEventsMap.entrySet())
                {
                    String iCalendar = getTeacherICalendar(entry.getValue(), null, sequence, true);
                    FefuTeacherScheduleICalCommits request = FefuSubmitEventsClient.sendEvents(iCalData.getId().toString(), null, Lists.newArrayList(entry.getKey()), iCalendar, getTeacherVEventsMap(scheduleICalXST.getEvents()));
                    if (!request.getFailedVEvents().isEmpty())
                    {
                        failedEvents.addAll(request.getFailedVEvents());
                    }
                }

                if (!failedEvents.isEmpty())
                {
                    iCalData.setSequence(sequence);
                    scheduleICalXST.setEvents(failedEvents);
                    iCalData.setXml(FefuScheduleManager.instance().xStream().toXML(scheduleICalXST));
                    update(iCalData);
                }
                else
                    iCalDataListForDelete.add(iCalData);
            }
        }
        iCalDataListForDelete.forEach(this::delete);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void sendICalendar(SppSchedule schedule)
    {
        NamedSyncInTransactionCheckLocker.register(getSession().getTransaction(), "fefu.schedule.ws.export.lock_" + schedule.getId(), 300);
        if (null == ApplicationRuntime.getProperty(FefuSubmitEventsClient.PORTAL_SERVICE_EVENTS_URL))
        {
            throw new ApplicationException("Приложение не настроено для работы с веб-сервисом.", true);
        }

        sendStudentsICalendar(schedule);
        sendTeacherICalendar(schedule);
    }

    private void sendTeacherICalendar(SppSchedule schedule)
    {
        if (schedule.getIcalTeacherData() != null && !schedule.getIcalTeacherData().isChanged()) return;

        DQLSelectBuilder periodBuilder = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "p");
        periodBuilder.where(eq(property("p", SppSchedulePeriod.weekRow().schedule().id()), value(schedule.getId())));

        List<SppSchedulePeriod> periods = periodBuilder.createStatement(getSession()).list();

        DQLSelectBuilder periodFixesBuilder = new DQLSelectBuilder().fromEntity(SppSchedulePeriodFix.class, "f");
        periodFixesBuilder.where(eq(property("f", SppSchedulePeriodFix.sppSchedule().id()), value(schedule.getId())));
        List<SppSchedulePeriodFix> periodFixes = periodFixesBuilder.createStatement(getSession()).list();

        Date endDate = schedule.getSeason().getEndDate();
        Date startDate = schedule.getSeason().getStartDate();

        Map<String, LocalDate> datesMap = getDatesMap(startDate, endDate);
        Map<LocalDate, Boolean> oddOrEvenDatesMap = getOddOrEvenDatesMap(datesMap, schedule.getSeason().isStartFromEven());

        List<FefuTeacherScheduleVEventICalXST> currentPeriodVEvents = Lists.newArrayList();

        // находим guid преподавателей
        List<Long> personIds = Lists.newArrayList();

        personIds.addAll(periods.stream().filter(period -> period.getTeacherVal() != null)
                .map(period -> period.getTeacherVal().getPerson().getId()).collect(Collectors.toList()));

        // personId -> guid
        final Map<Long, String> teacherGuids = Maps.newHashMap();

        // test
//        for(Long emplId : personIds)
//        {
//            teacherGuids.put(emplId, getCurrentTestNsiIds().get(0));
//        }
        // test

        BatchUtils.execute(personIds, 100, elements -> {
            List<FefuNsiIds> nsiIdses = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "fn")
                    .where(in(property("fn", FefuNsiIds.entityId()), elements))
                    .createStatement(getSession()).list();
            for (FefuNsiIds nsiIds : nsiIdses)
            {
                teacherGuids.put(nsiIds.getEntityId(), nsiIds.getGuid());
            }
        });


        periods.stream().filter(period -> period.getTeacherVal() != null && !StringUtils.isEmpty(teacherGuids.get(period.getTeacherVal().getPerson().getId())))
                .forEach(period -> {
            List<LocalDate> datesForPeriod = getDatesForPeriod(period, datesMap, oddOrEvenDatesMap);
            if (!period.isEmpty())
                currentPeriodVEvents.addAll(getTeacherPeriodVEvents(period, datesForPeriod, teacherGuids.get(period.getTeacherVal().getPerson().getId())));
        });

        Map<String, FefuTeacherScheduleVEventICalXST> currentFixVEvents = getTeacherFixVEvents(periodFixes, datesMap, teacherGuids);

        List<String> canceledPeriods = Lists.newArrayList();

        for (SppSchedulePeriodFix fix : periodFixes)
        {
            LocalDate date = datesMap.get(getStrValueForDate(fix.getDate()));

            // Если дата исправления не укладывается в период расписания, то пропускаем такое исправление
            if (null == date) continue;

            if (fix.isCanceled())
            {
                canceledPeriods.add(fix.getPeriodNum() + "_" + date.getDayOfMonth() + "_" + date.getMonthOfYear() + "_" + date.getYear() + "_" + fix.getBell().getId());
            }
        }

        List<FefuTeacherScheduleVEventICalXST> currentVEvents = Lists.newArrayList();

        for (FefuTeacherScheduleVEventICalXST currentVEvent : currentPeriodVEvents)
        {
            if (canceledPeriods.contains(currentVEvent.getUidPrefix())) continue;
            if (currentFixVEvents.get(currentVEvent.getUidPrefix()) != null)
            {
                currentVEvents.add(currentFixVEvents.get(currentVEvent.getUidPrefix()));
            }
            else
            {
                currentVEvents.add(currentVEvent);
            }
        }

        // находим события для отмены
        List<FefuTeacherScheduleVEventICalXST> cancelVEvents = Lists.newArrayList();

        SppScheduleICal iCalData = schedule.getIcalTeacherData();

        FefuTeacherScheduleICalXST scheduleICalXST = iCalData != null ? (FefuTeacherScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml()) : new FefuTeacherScheduleICalXST();

        if (iCalData != null)
        {
            scheduleICalXST = (FefuTeacherScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml());

            if (scheduleICalXST.getEvents() != null && !scheduleICalXST.getEvents().isEmpty())
            {
                for (FefuTeacherScheduleVEventICalXST event : scheduleICalXST.getEvents())
                {
                    if (!containsTeacherVEvents(event, currentVEvents))
                        cancelVEvents.add(event);
                }
            }
        }
        else
        {
            iCalData = new SppScheduleICal();
            iCalData.setSequence(0);
        }
        // nsiId -> list events
        Map<String, List<FefuTeacherScheduleVEventICalXST>> currentEventsTeacherMap = Maps.newHashMap();

        // nsiId -> list events
        Map<String, List<FefuTeacherScheduleVEventICalXST>> cancelEventsTeacherMap = Maps.newHashMap();

        for (FefuTeacherScheduleVEventICalXST event : currentVEvents)
        {
            SafeMap.safeGet(currentEventsTeacherMap, event.getTeacherNSIId(), ArrayList.class).add(event);
        }

        for (FefuTeacherScheduleVEventICalXST event : cancelVEvents)
        {
            SafeMap.safeGet(cancelEventsTeacherMap, event.getTeacherNSIId(), ArrayList.class).add(event);
        }

        int sequence = iCalData.getSequence() + 1;

        List<FefuTeacherScheduleVEventICalXST> commitedVEvents = Lists.newArrayList();

        boolean iCalDataChanged = false;

        for (Map.Entry<String, List<FefuTeacherScheduleVEventICalXST>> entry : cancelEventsTeacherMap.entrySet())
        {

            String iCalendar = getTeacherICalendar(entry.getValue(), schedule, sequence, true);

            List<String> guids = Lists.newArrayList(entry.getKey());
            if ("37b24159-4bbd-47df-9883-932dd9d6c6c7".equals(entry.getKey()))
                guids.add("00000000-0000-0000-5555-000000000000");

            FefuTeacherScheduleICalCommits publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), null, guids, iCalendar, getTeacherVEventsMap(entry.getValue()));

            // если фейл, то события необходимо оставить.
            if (!publishRequest.getFailedVEvents().isEmpty())
            {
                commitedVEvents.addAll(publishRequest.getFailedVEvents());
                iCalDataChanged = true;
            }
        }

        for (Map.Entry<String, List<FefuTeacherScheduleVEventICalXST>> entry : currentEventsTeacherMap.entrySet())
        {

            String iCalendar = getTeacherICalendar(entry.getValue(), schedule, sequence, false);

            List<String> guids = Lists.newArrayList(entry.getKey());
            if ("37b24159-4bbd-47df-9883-932dd9d6c6c7".equals(entry.getKey()))
                guids.add("00000000-0000-0000-5555-000000000000");

            FefuTeacherScheduleICalCommits publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), "Расписание преподавателя", guids, iCalendar, getTeacherVEventsMap(entry.getValue()));

            if (publishRequest.getFailedVEvents().isEmpty())
            {
                commitedVEvents.addAll(entry.getValue());
            }
            else
            {
                if (!publishRequest.getCommitedVEvents().isEmpty())
                    commitedVEvents.addAll(publishRequest.getCommitedVEvents());
                iCalDataChanged = true;
            }
        }

        if (!commitedVEvents.isEmpty())
        {
            iCalData.setChanged(iCalDataChanged);
            iCalData.setSequence(sequence);
            scheduleICalXST.setEvents(commitedVEvents);
            iCalData.setXml(FefuScheduleManager.instance().xStream().toXML(scheduleICalXST));
            save(iCalData);
            schedule.setIcalTeacherData(iCalData);
            update(schedule);
        }
    }

    private Map<String, FefuTeacherScheduleVEventICalXST> getTeacherFixVEvents(List<SppSchedulePeriodFix> periodFixes, Map<String, LocalDate> datesMap, Map<Long, String> teacherGuids)
    {
        Map<String, FefuTeacherScheduleVEventICalXST> vEvents = Maps.newHashMap();
        for (SppSchedulePeriodFix fix : periodFixes)
        {
            if (fix.getTeacherVal() != null && !StringUtils.isEmpty(teacherGuids.get(fix.getTeacherVal().getPerson().getId())) && fix.isCanceled())
            {
                LocalDate date = datesMap.get(getStrValueForDate(fix.getDate()));

                // Если дата исправления не укладывается в период расписания, то пропускаем такое исправление
                if (null == date) continue;

                FefuTeacherScheduleVEventICalXST vEvent = new FefuTeacherScheduleVEventICalXST();
                vEvent.setPairId(fix.getId());
                vEvent.setUidPrefix(fix.getPeriodNum() + "_" + date.getDayOfMonth() + "_" + date.getMonthOfYear() + "_" + date.getYear() + "_" + fix.getBell().getId());
                vEvent.setStartDate(getStartDate(date, fix.getBell()));
                vEvent.setEndDate(getEndDate(date, fix.getBell()));
                if (!StringUtils.isEmpty(fix.getPeriodGroup()))
                    vEvent.setPeriodGroup(fix.getPeriodGroup());
                vEvent.setSubject(fix.getSubject());
                vEvent.setLectureRoom(fix.getLectureRoom());
                vEvent.setTeacher(fix.getTeacher());
                vEvent.setTeacherNSIId(teacherGuids.get(fix.getTeacherVal().getPerson().getId()));
                vEvents.put(vEvent.getUidPrefix(), vEvent);
            }
        }
        return vEvents;
    }

    private List<FefuTeacherScheduleVEventICalXST> getTeacherPeriodVEvents(SppSchedulePeriod period, List<LocalDate> datesForPeriod, String teacherGuid)
    {
        if (period.getTeacherVal() == null) return Lists.newArrayList();

        List<FefuTeacherScheduleVEventICalXST> vEvents = Lists.newArrayList();
        for (LocalDate date : datesForPeriod)
        {
            FefuTeacherScheduleVEventICalXST vEvent = new FefuTeacherScheduleVEventICalXST();
            vEvent.setPairId(period.getId());
            vEvent.setUidPrefix(period.getPeriodNum() + "_" + date.getDayOfMonth() + "_" + date.getMonthOfYear() + "_" + date.getYear() + "_" + period.getWeekRow().getBell().getId());
            vEvent.setStartDate(getStartDate(date, period.getWeekRow().getBell()));
            vEvent.setEndDate(getEndDate(date, period.getWeekRow().getBell()));
            if (!StringUtils.isEmpty(period.getPeriodGroup()))
                vEvent.setPeriodGroup(period.getPeriodGroup());
            vEvent.setSubject(period.getSubject());
            vEvent.setLectureRoom(period.getLectureRoom());
            vEvent.setTeacher(period.getTeacher());
            vEvent.setTeacherNSIId(teacherGuid);
            vEvents.add(vEvent);
        }
        return vEvents;
    }

    private void sendStudentsICalendar(SppSchedule schedule)
    {
        // Проверяем изменился ли список получателей для данного расписания
        List<String> guidsForAdd = Lists.newArrayList();
        List<String> guidsForCancel = Lists.newArrayList();
        List<String> currentNsiIds =
                getCurrentNsiIds(schedule.getGroup().getId());
//                getCurrentTestNsiIds();

        if (null != schedule.getIcalData())
        {
            SppScheduleICal iCalData = schedule.getIcalData();
            FefuScheduleICalXST scheduleICalXST = (FefuScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml());

            List<String> previousNsiIds = scheduleICalXST.getStudentsNSIIds();

            for (String guid : previousNsiIds)
            {
                if (!currentNsiIds.contains(guid))
                    guidsForCancel.add(guid);
            }

            for (String guid : currentNsiIds)
            {
                if (!previousNsiIds.contains(guid))
                    guidsForAdd.add(guid);
            }
        }

        boolean recipientsNotChanged = guidsForAdd.isEmpty() && guidsForCancel.isEmpty();

        if (null != schedule.getIcalData() && !schedule.getIcalData().isChanged() && recipientsNotChanged) return;

        if (!recipientsNotChanged && ((schedule.getIcalData() != null && !schedule.getIcalData().isChanged()) || currentNsiIds.isEmpty()))
        {
            sendICalendarWithChangesRecipientList(schedule, guidsForAdd, guidsForCancel, currentNsiIds);
            return;
        }
        if (currentNsiIds.isEmpty()) return;

        DQLSelectBuilder periodBuilder = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "p");
        periodBuilder.where(eq(property("p", SppSchedulePeriod.weekRow().schedule().id()), value(schedule.getId())));

        List<SppSchedulePeriod> periods = periodBuilder.createStatement(getSession()).list();

        DQLSelectBuilder periodFixesBuilder = new DQLSelectBuilder().fromEntity(SppSchedulePeriodFix.class, "f");
        periodFixesBuilder.where(eq(property("f", SppSchedulePeriodFix.sppSchedule().id()), value(schedule.getId())));
        List<SppSchedulePeriodFix> periodFixes = periodFixesBuilder.createStatement(getSession()).list();

        Date endDate = schedule.getSeason().getEndDate();
        Date startDate = schedule.getSeason().getStartDate();

        Map<String, LocalDate> datesMap = getDatesMap(startDate, endDate);
        Map<LocalDate, Boolean> oddOrEvenDatesMap = getOddOrEvenDatesMap(datesMap, schedule.getSeason().isStartFromEven());

        Map<String, FefuScheduleVEventICalXST> currentPeriodVEvents = Maps.newHashMap();
        for (SppSchedulePeriod period : periods)
        {
            List<LocalDate> datesForPeriod = getDatesForPeriod(period, datesMap, oddOrEvenDatesMap);
            if (!period.isEmpty())
                currentPeriodVEvents.putAll(getPeriodVEvents(period, datesForPeriod));
        }
        Map<String, FefuScheduleVEventICalXST> currentFixVEvents = getFixVEvents(periodFixes, datesMap);

        List<FefuScheduleVEventICalXST> currentVEvents = getCurrentVEvents(currentPeriodVEvents, currentFixVEvents);


        if (null == schedule.getIcalData())
        {
            if (currentVEvents.isEmpty()) return;
            sendICalendarWithNullICallData(schedule, currentVEvents, currentNsiIds);
        }
        else
        {
            sendICalendarWithChangedICallData(schedule, currentVEvents, guidsForAdd, guidsForCancel, currentNsiIds);
        }
    }

    @Override
    public boolean getScheduleDataOrRecipientsChanged(Long scheduleId)
    {
        SppSchedule schedule = DataAccessServices.dao().get(scheduleId);
        if (null == schedule) return true;
        if (null == schedule.getIcalData()) return true;
        if (null == schedule.getIcalTeacherData()) return true;
        if (schedule.getIcalData().isChanged()) return true;
        if (schedule.getIcalTeacherData().isChanged()) return true;

        boolean recipientsChanged = false;
        List<String> currentNsiIds =
                getCurrentNsiIds(schedule.getGroup().getId());
//                getCurrentTestNsiIds();

        FefuScheduleICalXST scheduleICalXST = (FefuScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(schedule.getIcalData().getXml());
        List<String> oldNsiIds = scheduleICalXST.getStudentsNSIIds();

        for (String guid : currentNsiIds)
        {
            if (!oldNsiIds.contains(guid))
            {
                recipientsChanged = true;
                break;
            }
        }
        if (recipientsChanged) return true;

        for (String guid : oldNsiIds)
        {
            if (!currentNsiIds.contains(guid))
            {
                recipientsChanged = true;
                break;
            }
        }
        return recipientsChanged;
    }

    private void sendICalendarWithChangedICallData(SppSchedule schedule, List<FefuScheduleVEventICalXST> currentVEvents, List<String> guidsForAdd, List<String> guidsForCancel, List<String> currentNsiIds)
    {

        SppScheduleICal iCalData = schedule.getIcalData();
        FefuScheduleICalXST scheduleICalXST = (FefuScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml());

        List<FefuScheduleVEventICalXST> oldVEvents = scheduleICalXST.getEvents();

        List<FefuScheduleVEventICalXST> vEventsForCancel = Lists.newArrayList();
        List<FefuScheduleVEventICalXST> vEventsForAdd = Lists.newArrayList();

        Map<String, FefuScheduleVEventICalXST> currentVEventsMap = Maps.newHashMap();
        Map<String, FefuScheduleVEventICalXST> oldVEventsMap = Maps.newHashMap();
        for (FefuScheduleVEventICalXST vEvent : currentVEvents)
        {
            currentVEventsMap.put(vEvent.getUidPrefix() + "@" + vEvent.getPairId(), vEvent);
        }

        for (FefuScheduleVEventICalXST vEvent : oldVEvents)
        {
            oldVEventsMap.put(vEvent.getUidPrefix() + "@" + vEvent.getPairId(), vEvent);
        }

        vEventsForCancel.addAll(oldVEventsMap.keySet().stream()
                .filter(vEventUid -> !currentVEventsMap.containsKey(vEventUid))
                .map(oldVEventsMap::get).collect(Collectors.toList()));
        vEventsForAdd.addAll(currentVEventsMap.keySet().stream()
                .filter(vEventUid -> !oldVEventsMap.containsKey(vEventUid))
                .map(currentVEventsMap::get).collect(Collectors.toList()));
        int sequence = iCalData.getSequence();

        FefuScheduleICalCommits cancelRecipRequest = getVEventsMap(oldVEvents);
        if (!guidsForCancel.isEmpty())
        {
            String iCalendar = getICalendar(oldVEvents, schedule, ++sequence, true);
            cancelRecipRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), "Расписание для группы " + schedule.getGroup().getTitle(), guidsForCancel, iCalendar, cancelRecipRequest);

        }
        else
        {
            cancelRecipRequest.setRequestFailed(false);
        }

        FefuScheduleICalCommits cancelRequest = getVEventsMap(vEventsForCancel);
        if (!vEventsForCancel.isEmpty())
        {
            String iCalendar = getICalendar(vEventsForCancel, schedule, ++sequence, true);
            List<String> nsiIdsForCancel = Lists.newArrayList(currentNsiIds);
            nsiIdsForCancel.remove(guidsForCancel);
            cancelRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), "Расписание для группы " + schedule.getGroup().getTitle(), nsiIdsForCancel, iCalendar, cancelRequest);
        }
        else cancelRequest.setRequestFailed(false);


        if (currentVEvents.isEmpty())
        {
            scheduleICalXST.setEvents(currentVEvents);
            scheduleICalXST.setStudentsNSIIds(currentNsiIds);
            iCalData.setSequence(sequence);
            iCalData.setXml(FefuScheduleManager.instance().xStream().toXML(scheduleICalXST));
            iCalData.setChanged(false);
            update(iCalData);
            return;
        }
        String iCalendar = getICalendar(currentVEvents, schedule, ++sequence, false);
        FefuScheduleICalCommits publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), "Расписание для группы " + schedule.getGroup().getTitle(), currentNsiIds, iCalendar, getVEventsMap(currentVEvents));


        if (!cancelRecipRequest.isRequestFailed() && !cancelRequest.isRequestFailed() && !publishRequest.isRequestFailed())
        {
            scheduleICalXST.setEvents(publishRequest.getCommitedVEvents());
            scheduleICalXST.setStudentsNSIIds(currentNsiIds);
            iCalData.setSequence(sequence);
            iCalData.setXml(FefuScheduleManager.instance().xStream().toXML(scheduleICalXST));
            iCalData.setChanged(false);
            update(iCalData);
        }
    }

    private void sendICalendarWithNullICallData(SppSchedule schedule, List<FefuScheduleVEventICalXST> currentVEvents, List<String> currentNsiIds)
    {
        FefuScheduleICalXST scheduleICalXST = new FefuScheduleICalXST();
        scheduleICalXST.setStudentsNSIIds(currentNsiIds);
        int sequence = 1;
        String iCalendar = getICalendar(currentVEvents, schedule, sequence, false);
        FefuScheduleICalCommits publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), "Расписание для группы " + schedule.getGroup().getTitle(), currentNsiIds, iCalendar, getVEventsMap(currentVEvents));
        if (!publishRequest.isRequestFailed())
        {
            SppScheduleICal iCalData = new SppScheduleICal();
            iCalData.setChanged(false);
            iCalData.setSequence(sequence);
            scheduleICalXST.setEvents(publishRequest.getCommitedVEvents());
            iCalData.setXml(FefuScheduleManager.instance().xStream().toXML(scheduleICalXST));
            save(iCalData);
            schedule.setIcalData(iCalData);
            update(schedule);
        }
    }

    private void sendICalendarWithChangesRecipientList(SppSchedule schedule, List<String> guidsForAdd, List<String> guidsForCancel, List<String> currentNsiIds)
    {
        SppScheduleICal iCalData = schedule.getIcalData();
        FefuScheduleICalXST scheduleICalXST = (FefuScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml());
        FefuScheduleICalCommits cancelRequest = getVEventsMap(scheduleICalXST.getEvents());

        if (!guidsForCancel.isEmpty())
        {
            int sequence = iCalData.getSequence();
            String iCalendar = getICalendar(scheduleICalXST.getEvents(), schedule, ++sequence, true);
            cancelRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), "Расписание для группы " + schedule.getGroup().getTitle(), guidsForCancel, iCalendar, cancelRequest);
            if (!cancelRequest.isRequestFailed())
            {
                iCalData.setSequence(sequence);
                update(iCalData);
            }
        }
        else
        {
            cancelRequest.setRequestFailed(false);
        }

        if (cancelRequest.isRequestFailed()) return;


        FefuScheduleICalCommits publishRequest = getVEventsMap(scheduleICalXST.getEvents());
        if (!guidsForAdd.isEmpty())
        {
            int sequence = iCalData.getSequence();
            String iCalendar = getICalendar(scheduleICalXST.getEvents(), schedule, ++sequence, false);
            publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), "Расписание для группы " + schedule.getGroup().getTitle(), currentNsiIds, iCalendar, publishRequest);
            if (!publishRequest.isRequestFailed())
            {
                iCalData.setSequence(sequence);
                update(iCalData);
            }
            else return;
        }
        scheduleICalXST.setStudentsNSIIds(currentNsiIds);
        scheduleICalXST.setEvents(publishRequest.getCommitedVEvents());
        iCalData.setXml(FefuScheduleManager.instance().xStream().toXML(scheduleICalXST));
        update(iCalData);
    }

    private FefuScheduleICalCommits getVEventsMap(List<FefuScheduleVEventICalXST> events)
    {
        FefuScheduleICalCommits iCalCommits = new FefuScheduleICalCommits();
        Map<String, FefuScheduleVEventICalXST> vEventsMap = Maps.newHashMap();
        for (FefuScheduleVEventICalXST vEvent : events)
        {
            vEventsMap.put(vEvent.getUidPrefix() + "@" + vEvent.getPairId(), vEvent);
        }
        iCalCommits.setvEventsMap(vEventsMap);
        iCalCommits.setCommitedVEvents(Lists.<FefuScheduleVEventICalXST>newArrayList());
        iCalCommits.setFailedVEvents(Lists.<FefuScheduleVEventICalXST>newArrayList());
        return iCalCommits;
    }

    private FefuTeacherScheduleICalCommits getTeacherVEventsMap(List<FefuTeacherScheduleVEventICalXST> events)
    {
        FefuTeacherScheduleICalCommits iCalCommits = new FefuTeacherScheduleICalCommits();
        Map<String, FefuTeacherScheduleVEventICalXST> vEventsMap = Maps.newHashMap();
        for (FefuTeacherScheduleVEventICalXST vEvent : events)
        {
            vEventsMap.put(vEvent.getUidPrefix() + "@" + vEvent.getPairId(), vEvent);
        }
        iCalCommits.setvEventsMap(vEventsMap);
        iCalCommits.setCommitedVEvents(Lists.<FefuTeacherScheduleVEventICalXST>newArrayList());
        iCalCommits.setFailedVEvents(Lists.<FefuTeacherScheduleVEventICalXST>newArrayList());
        return iCalCommits;
    }

    private String getICalendar(List<FefuScheduleVEventICalXST> vEvents, SppSchedule schedule, int sequence, boolean cancel)
    {
//        List<FefuScheduleVEventICalXST> vEvents = scheduleICalXST.getEvents();

        // Create a calendar
        net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
        icsCalendar.getProperties().add(Version.VERSION_2_0);
        icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
        Method method = cancel ? Method.CANCEL : Method.PUBLISH;
        icsCalendar.getProperties().add(CalScale.GREGORIAN);
        icsCalendar.getProperties().add(method);

        TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
        net.fortuna.ical4j.model.TimeZone timezone = registry.getTimeZone(java.util.Calendar.getInstance().getTimeZone().getID());
        VTimeZone tz = timezone.getVTimeZone();

        for (FefuScheduleVEventICalXST vEvent : vEvents)
        {
            // Create the event
            String eventName = vEvent.getSubject();
            DateTime start = new DateTime(vEvent.getStartDate());
            DateTime end = new DateTime(vEvent.getEndDate());
            VEvent meeting = new VEvent(start, end, eventName);
            StringBuilder description = new StringBuilder();
            if (!StringUtils.isEmpty(vEvent.getPeriodGroup()))
                description.append("Подгруппа - ").append(vEvent.getPeriodGroup());
            if (!StringUtils.isEmpty(vEvent.getLectureRoom()))
                description.append(StringUtils.isEmpty(description.toString()) ? "" : " ").append("Аудитория - ").append(vEvent.getLectureRoom());
            if (!StringUtils.isEmpty(vEvent.getTeacher()))
                description.append(StringUtils.isEmpty(description.toString()) ? "" : " ").append("Преподаватель - ").append(vEvent.getTeacher());

            Description desc = new Description(description.toString());
            if (null != schedule)
            {
                Categories cat = new Categories("Расписание для группы " + schedule.getGroup().getTitle());
                meeting.getProperties().add(cat);
            }
            else
            {
                Categories cat = new Categories("Отмена расписания");
                meeting.getProperties().add(cat);
            }
            meeting.getProperties().add(tz.getTimeZoneId());

            if (!StringUtils.isEmpty(vEvent.getLectureRoom()))
            {
                Location location = new Location(vEvent.getLectureRoom());
                meeting.getProperties().add(location);
            }

            meeting.getProperties().add(desc);
            Uid uid = new Uid(vEvent.getUidPrefix() + "@" + vEvent.getPairId());
            Sequence seq = new Sequence(sequence);
            meeting.getProperties().add(uid);
            meeting.getProperties().add(seq);
            icsCalendar.getComponents().add(meeting);
        }

        return icsCalendar.toString();
    }

    private String getTeacherICalendar(List<FefuTeacherScheduleVEventICalXST> vEvents, SppSchedule schedule, int sequence, boolean cancel)
    {
//        List<FefuScheduleVEventICalXST> vEvents = scheduleICalXST.getEvents();

        // Create a calendar
        net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
        icsCalendar.getProperties().add(Version.VERSION_2_0);
        icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
        Method method = cancel ? Method.CANCEL : Method.PUBLISH;
        icsCalendar.getProperties().add(CalScale.GREGORIAN);
        icsCalendar.getProperties().add(method);

        TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
        net.fortuna.ical4j.model.TimeZone timezone = registry.getTimeZone(java.util.Calendar.getInstance().getTimeZone().getID());
        VTimeZone tz = timezone.getVTimeZone();

        for (FefuTeacherScheduleVEventICalXST vEvent : vEvents)
        {
            // Create the event
            String eventName = vEvent.getSubject();
            DateTime start = new DateTime(vEvent.getStartDate());
            DateTime end = new DateTime(vEvent.getEndDate());
            VEvent meeting = new VEvent(start, end, eventName);
            StringBuilder description = new StringBuilder();
            if (!StringUtils.isEmpty(vEvent.getPeriodGroup()))
                description.append("Подгруппа - ").append(vEvent.getPeriodGroup());

            description.append(StringUtils.isEmpty(description.toString()) ? "" : " ").append("Группа - ").append(schedule.getGroup().getTitle());

            if (!StringUtils.isEmpty(vEvent.getLectureRoom()))
                description.append(StringUtils.isEmpty(description.toString()) ? "" : " ").append("Аудитория - ").append(vEvent.getLectureRoom());
            if (!StringUtils.isEmpty(vEvent.getTeacher()))
                description.append(StringUtils.isEmpty(description.toString()) ? "" : " ").append("Преподаватель - ").append(vEvent.getTeacher());


            Description desc = new Description(description.toString());
            if (null != schedule)
            {
                Categories cat = new Categories("Расписание преподавателя");
                meeting.getProperties().add(cat);
            }
            else
            {
                Categories cat = new Categories("Отмена расписания");
                meeting.getProperties().add(cat);
            }
            meeting.getProperties().add(tz.getTimeZoneId());

            if (!StringUtils.isEmpty(vEvent.getLectureRoom()))
            {
                Location location = new Location(vEvent.getLectureRoom());
                meeting.getProperties().add(location);
            }

            meeting.getProperties().add(desc);
            Uid uid = new Uid(vEvent.getUidPrefix() + "@" + vEvent.getPairId());
            Sequence seq = new Sequence(sequence);
            meeting.getProperties().add(uid);
            meeting.getProperties().add(seq);
            icsCalendar.getComponents().add(meeting);
        }

        return icsCalendar.toString();
    }


    private List<String> getCurrentTestNsiIds()
    {
        List<String> testIds = Lists.newArrayList();
        testIds.add("00000000-0000-0000-4444-000000000000");
        return testIds;
    }

    @Override
    public List<String> getCurrentNsiIds(Long groupId)
    {
        DQLSelectBuilder studentBuilder = new DQLSelectBuilder().fromEntity(Student.class, "s");
        studentBuilder.column(property("s", Student.person().id()));
        studentBuilder.where(eq(property("s", Student.group().id()), value(groupId)));

        DQLSelectBuilder nsiIdsBuilder = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "nsi");
        nsiIdsBuilder.column(property("nsi", FefuNsiIds.guid()));
        nsiIdsBuilder.where(eq(property("nsi", FefuNsiIds.entityType()), value(Person.ENTITY_NAME)));
        nsiIdsBuilder.where(in(property("nsi", FefuNsiIds.entityId()), studentBuilder.buildQuery()));

        List<String> currentNsiIds = nsiIdsBuilder.createStatement(getSession()).list();
        if (currentNsiIds.contains("1300d0f6-f558-3d57-aae9-fa2d2ce67c0b"))
            currentNsiIds.add("00000000-0000-0000-4444-000000000000");

        return currentNsiIds;
    }

    private List<FefuScheduleVEventICalXST> getCurrentVEvents(Map<String, FefuScheduleVEventICalXST> currentPeriodVEvents, Map<String, FefuScheduleVEventICalXST> currentFixVEvents)
    {
        List<FefuScheduleVEventICalXST> currentVEvents = Lists.newArrayList();
        for (String key : currentPeriodVEvents.keySet())
        {
            if (currentFixVEvents.containsKey(key))
            {
                if (!StringUtils.isEmpty(currentFixVEvents.get(key).getSubject()))
                    currentVEvents.add(currentFixVEvents.get(key));
            }
            else
                currentVEvents.add(currentPeriodVEvents.get(key));
        }
        currentVEvents.addAll(currentFixVEvents.keySet().stream()
                .filter(key -> !currentPeriodVEvents.containsKey(key))
                .filter(key -> !StringUtils.isEmpty(currentFixVEvents.get(key).getSubject()))
                .map(currentFixVEvents::get).collect(Collectors.toList()));

        return currentVEvents;
    }

    private Map<String, FefuScheduleVEventICalXST> getFixVEvents(List<SppSchedulePeriodFix> periodFixes, Map<String, LocalDate> datesMap)
    {
        List<LocalDate> dates = Lists.newArrayList(datesMap.values());

        Map<String, FefuScheduleVEventICalXST> vEvents = Maps.newHashMap();
        for (SppSchedulePeriodFix fix : periodFixes)
        {
            LocalDate date = datesMap.get(getStrValueForDate(fix.getDate()));

            // Если дата исправления не укладывается в период расписания, то пропускаем такое исправление
            if (null == date) continue;

            FefuScheduleVEventICalXST vEvent = new FefuScheduleVEventICalXST();
            vEvent.setPairId(fix.getId());
            vEvent.setUidPrefix(fix.getPeriodNum() + "_" + date.getDayOfMonth() + "_" + date.getMonthOfYear() + "_" + date.getYear() + "_" + fix.getBell().getId());
            vEvent.setStartDate(getStartDate(date, fix.getBell()));
            vEvent.setEndDate(getEndDate(date, fix.getBell()));
            if (!StringUtils.isEmpty(fix.getPeriodGroup()))
                vEvent.setPeriodGroup(fix.getPeriodGroup());
            vEvent.setSubject(fix.getSubject());
            vEvent.setLectureRoom(fix.getLectureRoom());
            vEvent.setTeacher(fix.getTeacher());
            vEvents.put(vEvent.getUidPrefix(), vEvent);

        }
        return vEvents;
    }

    private Date getStartDate(LocalDate date, ScheduleBellEntry bell)
    {
        org.joda.time.DateTime dateTime = date.toDateTimeAtCurrentTime();
        return dateTime.withHourOfDay(bell.getStartHour()).withMinuteOfHour(bell.getStartMin()).withSecondOfMinute(0).withMillisOfSecond(0).toDate();
    }

    private Date getEndDate(LocalDate date, ScheduleBellEntry bell)
    {
        org.joda.time.DateTime dateTime = date.toDateTimeAtCurrentTime();
        return dateTime.withHourOfDay(bell.getEndHour()).withMinuteOfHour(bell.getEndMin()).withSecondOfMinute(0).withMillisOfSecond(0).toDate();
    }

    private Map<String, FefuScheduleVEventICalXST> getPeriodVEvents(SppSchedulePeriod period, List<LocalDate> datesForPeriod)
    {
        Map<String, FefuScheduleVEventICalXST> vEvents = Maps.newHashMap();
        for (LocalDate date : datesForPeriod)
        {
            FefuScheduleVEventICalXST vEvent = new FefuScheduleVEventICalXST();
            vEvent.setPairId(period.getId());
            vEvent.setUidPrefix(period.getPeriodNum() + "_" + date.getDayOfMonth() + "_" + date.getMonthOfYear() + "_" + date.getYear() + "_" + period.getWeekRow().getBell().getId());
            vEvent.setStartDate(getStartDate(date, period.getWeekRow().getBell()));
            vEvent.setEndDate(getEndDate(date, period.getWeekRow().getBell()));
            if (!StringUtils.isEmpty(period.getPeriodGroup()))
                vEvent.setPeriodGroup(period.getPeriodGroup());
            vEvent.setSubject(period.getSubject());
            vEvent.setLectureRoom(period.getLectureRoom());
            vEvent.setTeacher(period.getTeacher());
            vEvents.put(vEvent.getUidPrefix(), vEvent);
        }
        return vEvents;
    }


    private List<LocalDate> getDatesForPeriod(SppSchedulePeriod period, Map<String, LocalDate> datesMap, Map<LocalDate, Boolean> oddOrEvenDatesMap)
    {
        SppSchedule schedule = period.getWeekRow().getSchedule();
        // у занятия могут быть свои даты проведения
        Collection<LocalDate> periodDates = null;
        if (period.getStartDate() != null || period.getEndDate() != null)
        {
            Date startDate = period.getStartDate() != null ? period.getStartDate() : schedule.getSeason().getStartDate();
            Date endDate = period.getEndDate() != null ? period.getEndDate() : schedule.getSeason().getEndDate();
            periodDates = getDatesMap(startDate, endDate).values();
        }
        List<LocalDate> dates = Lists.newArrayList(periodDates != null ? periodDates : datesMap.values());

        int dayOfWeek = period.getDayOfWeek();
        Boolean evenPeriod = schedule.isDiffEven() ? period.getWeekRow().isEven() : null;

        List<LocalDate> datesForPeriod = Lists.newArrayList();
        for (LocalDate date : dates)
        {
            if (dayOfWeek == date.getDayOfWeek())
            {
                if (null == evenPeriod)
                    datesForPeriod.add(date);
                else
                {
                    Boolean isDateEven = oddOrEvenDatesMap.get(date);

                    // isDateEven can't be null
                    if (null == isDateEven) throw new NullPointerException("Here can't be null value in map for date");

                    if (evenPeriod)
                    {
                        if (isDateEven) datesForPeriod.add(date);
                    }
                    else
                    {
                        if (!isDateEven) datesForPeriod.add(date);
                    }
                }
            }
        }
        return datesForPeriod;
    }

    private Map<LocalDate, Boolean> getOddOrEvenDatesMap(Map<String, LocalDate> datesMap, boolean startFromEven)
    {
        Map<LocalDate, Boolean> oddOrEvenDatesMap = Maps.newHashMap();
        List<LocalDate> dates = Lists.newArrayList(datesMap.values());
        Collections.sort(dates);
        Map<Integer, List<LocalDate>> weekMap = Maps.newHashMap();

        for (LocalDate date : dates)
        {
            Integer week = date.getWeekOfWeekyear();
            if (!weekMap.containsKey(week))
                weekMap.put(week, Lists.<LocalDate>newArrayList());
            weekMap.get(week).add(date);
        }
        List<Integer> weeks = Lists.newArrayList(weekMap.keySet());
        Collections.sort(weeks);
        for (Integer week : weeks)
        {
            Boolean oddWeek;
            if ((weeks.indexOf(week) + 1) % 2 == 1)
            {
                if (startFromEven)
                {
                    oddWeek = Boolean.FALSE;
                }
                else
                {
                    oddWeek = Boolean.TRUE;
                }
            }
            else
            {
                if (startFromEven)
                {
                    oddWeek = Boolean.TRUE;
                }
                else
                {
                    oddWeek = Boolean.FALSE;
                }
            }

            List<LocalDate> localDates = weekMap.get(week);
            for (LocalDate localDate : localDates)
            {
                oddOrEvenDatesMap.put(localDate, oddWeek);
            }
        }
        return oddOrEvenDatesMap;
    }

    @Override
    public Map<String, LocalDate> getDatesMap(Date startDate, Date endDate)
    {
        Map<String, LocalDate> datesMap = Maps.newHashMap();

        MutableDateTime startMDate = getMutableDate(startDate);
        while (!startMDate.toDate().after(endDate))
        {
            datesMap.put(getStrValueForDate(startMDate.toDate()), new LocalDate(startMDate));
            startMDate.addDays(1);
        }
        datesMap.put(getStrValueForDate(endDate), new LocalDate(getLocalDate(endDate)));

        return datesMap;
    }

    private MutableDateTime getMutableDate(Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        MutableDateTime mutableDateTime = new MutableDateTime();
        mutableDateTime.setYear(year);
        mutableDateTime.setMonthOfYear(month);
        mutableDateTime.setDayOfMonth(day);
        return mutableDateTime;
    }

    private String getStrValueForDate(Date date)
    {
        LocalDate localDate = getLocalDate(date);
        return Integer.toString(localDate.getYear()) + Integer.toString(localDate.getMonthOfYear()) + Integer.toString(localDate.getDayOfMonth());
    }

    private LocalDate getLocalDate(Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        MutableDateTime mutableDateTime = new MutableDateTime();
        mutableDateTime.setYear(year);
        mutableDateTime.setMonthOfYear(month);
        mutableDateTime.setDayOfMonth(day);
        return new LocalDate(mutableDateTime);
    }

    public boolean containsTeacherVEvents(FefuTeacherScheduleVEventICalXST o, List<FefuTeacherScheduleVEventICalXST> list)
    {
        boolean contains = false;
        for (FefuTeacherScheduleVEventICalXST event : list)
        {
            if (equalsTeacherVEvents(event, o))
            {
                contains = true;
                break;
            }
        }
        return contains;
    }

    public boolean equalsTeacherVEvents(FefuTeacherScheduleVEventICalXST o1, FefuTeacherScheduleVEventICalXST o2)
    {
        if (o1 == null && o2 == null) return true;
        if (o1 == null || o2 == null) return false;

        return Objects.equal(o1.getPairId(), o2.getPairId()) &&
                Objects.equal(o1.getUidPrefix(), o2.getUidPrefix()) &&
                Objects.equal(o1.getStartDate(), o2.getStartDate()) &&
                Objects.equal(o1.getEndDate(), o2.getEndDate()) &&
                Objects.equal(o1.getPeriodGroup(), o2.getPeriodGroup()) &&
                Objects.equal(o1.getSubject(), o2.getSubject()) &&
                Objects.equal(o1.getTeacherNSIId(), o2.getTeacherNSIId()) &&
                Objects.equal(o1.getLectureRoom(), o2.getLectureRoom());
    }
}
