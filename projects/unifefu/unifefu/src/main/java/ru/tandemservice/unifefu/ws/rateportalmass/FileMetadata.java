/**
 * FileMetadata.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportalmass;

public class FileMetadata  implements java.io.Serializable {
    private java.lang.String inputid;

    private java.lang.String systemcode;

    private java.lang.String functioncode;

    private java.lang.String documentcode;

    private java.lang.String contenttype;

    private ru.tandemservice.unifefu.ws.rateportalmass.Recipient[] recipients;

    private ru.tandemservice.unifefu.ws.rateportalmass.Resource[] resources;

    private java.lang.String filename;

    private java.util.Calendar createdate;

    private java.util.Calendar modifydate;

    private java.lang.String comment;

    public FileMetadata() {
    }

    public FileMetadata(
           java.lang.String inputid,
           java.lang.String systemcode,
           java.lang.String functioncode,
           java.lang.String documentcode,
           java.lang.String contenttype,
           ru.tandemservice.unifefu.ws.rateportalmass.Recipient[] recipients,
           ru.tandemservice.unifefu.ws.rateportalmass.Resource[] resources,
           java.lang.String filename,
           java.util.Calendar createdate,
           java.util.Calendar modifydate,
           java.lang.String comment) {
           this.inputid = inputid;
           this.systemcode = systemcode;
           this.functioncode = functioncode;
           this.documentcode = documentcode;
           this.contenttype = contenttype;
           this.recipients = recipients;
           this.resources = resources;
           this.filename = filename;
           this.createdate = createdate;
           this.modifydate = modifydate;
           this.comment = comment;
    }


    /**
     * Gets the inputid value for this FileMetadata.
     * 
     * @return inputid
     */
    public java.lang.String getInputid() {
        return inputid;
    }


    /**
     * Sets the inputid value for this FileMetadata.
     * 
     * @param inputid
     */
    public void setInputid(java.lang.String inputid) {
        this.inputid = inputid;
    }


    /**
     * Gets the systemcode value for this FileMetadata.
     * 
     * @return systemcode
     */
    public java.lang.String getSystemcode() {
        return systemcode;
    }


    /**
     * Sets the systemcode value for this FileMetadata.
     * 
     * @param systemcode
     */
    public void setSystemcode(java.lang.String systemcode) {
        this.systemcode = systemcode;
    }


    /**
     * Gets the functioncode value for this FileMetadata.
     * 
     * @return functioncode
     */
    public java.lang.String getFunctioncode() {
        return functioncode;
    }


    /**
     * Sets the functioncode value for this FileMetadata.
     * 
     * @param functioncode
     */
    public void setFunctioncode(java.lang.String functioncode) {
        this.functioncode = functioncode;
    }


    /**
     * Gets the documentcode value for this FileMetadata.
     * 
     * @return documentcode
     */
    public java.lang.String getDocumentcode() {
        return documentcode;
    }


    /**
     * Sets the documentcode value for this FileMetadata.
     * 
     * @param documentcode
     */
    public void setDocumentcode(java.lang.String documentcode) {
        this.documentcode = documentcode;
    }


    /**
     * Gets the contenttype value for this FileMetadata.
     * 
     * @return contenttype
     */
    public java.lang.String getContenttype() {
        return contenttype;
    }


    /**
     * Sets the contenttype value for this FileMetadata.
     * 
     * @param contenttype
     */
    public void setContenttype(java.lang.String contenttype) {
        this.contenttype = contenttype;
    }


    /**
     * Gets the recipients value for this FileMetadata.
     * 
     * @return recipients
     */
    public ru.tandemservice.unifefu.ws.rateportalmass.Recipient[] getRecipients() {
        return recipients;
    }


    /**
     * Sets the recipients value for this FileMetadata.
     * 
     * @param recipients
     */
    public void setRecipients(ru.tandemservice.unifefu.ws.rateportalmass.Recipient[] recipients) {
        this.recipients = recipients;
    }


    /**
     * Gets the resources value for this FileMetadata.
     * 
     * @return resources
     */
    public ru.tandemservice.unifefu.ws.rateportalmass.Resource[] getResources() {
        return resources;
    }


    /**
     * Sets the resources value for this FileMetadata.
     * 
     * @param resources
     */
    public void setResources(ru.tandemservice.unifefu.ws.rateportalmass.Resource[] resources) {
        this.resources = resources;
    }


    /**
     * Gets the filename value for this FileMetadata.
     * 
     * @return filename
     */
    public java.lang.String getFilename() {
        return filename;
    }


    /**
     * Sets the filename value for this FileMetadata.
     * 
     * @param filename
     */
    public void setFilename(java.lang.String filename) {
        this.filename = filename;
    }


    /**
     * Gets the createdate value for this FileMetadata.
     * 
     * @return createdate
     */
    public java.util.Calendar getCreatedate() {
        return createdate;
    }


    /**
     * Sets the createdate value for this FileMetadata.
     * 
     * @param createdate
     */
    public void setCreatedate(java.util.Calendar createdate) {
        this.createdate = createdate;
    }


    /**
     * Gets the modifydate value for this FileMetadata.
     * 
     * @return modifydate
     */
    public java.util.Calendar getModifydate() {
        return modifydate;
    }


    /**
     * Sets the modifydate value for this FileMetadata.
     * 
     * @param modifydate
     */
    public void setModifydate(java.util.Calendar modifydate) {
        this.modifydate = modifydate;
    }


    /**
     * Gets the comment value for this FileMetadata.
     * 
     * @return comment
     */
    public java.lang.String getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this FileMetadata.
     * 
     * @param comment
     */
    public void setComment(java.lang.String comment) {
        this.comment = comment;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FileMetadata)) return false;
        FileMetadata other = (FileMetadata) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inputid==null && other.getInputid()==null) || 
             (this.inputid!=null &&
              this.inputid.equals(other.getInputid()))) &&
            ((this.systemcode==null && other.getSystemcode()==null) || 
             (this.systemcode!=null &&
              this.systemcode.equals(other.getSystemcode()))) &&
            ((this.functioncode==null && other.getFunctioncode()==null) || 
             (this.functioncode!=null &&
              this.functioncode.equals(other.getFunctioncode()))) &&
            ((this.documentcode==null && other.getDocumentcode()==null) || 
             (this.documentcode!=null &&
              this.documentcode.equals(other.getDocumentcode()))) &&
            ((this.contenttype==null && other.getContenttype()==null) || 
             (this.contenttype!=null &&
              this.contenttype.equals(other.getContenttype()))) &&
            ((this.recipients==null && other.getRecipients()==null) || 
             (this.recipients!=null &&
              java.util.Arrays.equals(this.recipients, other.getRecipients()))) &&
            ((this.resources==null && other.getResources()==null) || 
             (this.resources!=null &&
              java.util.Arrays.equals(this.resources, other.getResources()))) &&
            ((this.filename==null && other.getFilename()==null) || 
             (this.filename!=null &&
              this.filename.equals(other.getFilename()))) &&
            ((this.createdate==null && other.getCreatedate()==null) || 
             (this.createdate!=null &&
              this.createdate.equals(other.getCreatedate()))) &&
            ((this.modifydate==null && other.getModifydate()==null) || 
             (this.modifydate!=null &&
              this.modifydate.equals(other.getModifydate()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInputid() != null) {
            _hashCode += getInputid().hashCode();
        }
        if (getSystemcode() != null) {
            _hashCode += getSystemcode().hashCode();
        }
        if (getFunctioncode() != null) {
            _hashCode += getFunctioncode().hashCode();
        }
        if (getDocumentcode() != null) {
            _hashCode += getDocumentcode().hashCode();
        }
        if (getContenttype() != null) {
            _hashCode += getContenttype().hashCode();
        }
        if (getRecipients() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRecipients());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRecipients(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResources() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResources());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResources(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFilename() != null) {
            _hashCode += getFilename().hashCode();
        }
        if (getCreatedate() != null) {
            _hashCode += getCreatedate().hashCode();
        }
        if (getModifydate() != null) {
            _hashCode += getModifydate().hashCode();
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FileMetadata.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/data", "FileMetadata"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inputid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/data", "inputid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/data", "systemcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("functioncode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/data", "functioncode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/data", "documentcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contenttype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/data", "contenttype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recipients");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipients"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "RecipientList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resources");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resources"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "ResourceList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filename");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/data", "filename"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/data", "createdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modifydate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/data", "modifydate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/files/data", "comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
