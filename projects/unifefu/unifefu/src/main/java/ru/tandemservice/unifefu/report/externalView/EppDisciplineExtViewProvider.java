/* $Id$ */
package ru.tandemservice.unifefu.report.externalView;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Avetisov
 * @since 17.04.2014
 */
public class EppDisciplineExtViewProvider extends SimpleDQLExternalViewConfig
{
    @Override
    protected DQLSelectBuilder buildDqlQuery()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, "regElem")
                .joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().fromAlias("regElem"), "regDiscipline");

        column(dql, property("regElem", EppWorkPlanRegistryElementRow.L_WORK_PLAN), "workPlanId").comment("id_РУП");
        column(dql, property("regDiscipline", EppRegistryElement.P_ID), "disciplineId").comment("id_дисциплины");
        column(dql, property("regDiscipline", EppRegistryElement.P_TITLE), "title").comment("название");
        column(dql, property("regDiscipline", EppRegistryElement.P_SIZE), "loadInHours").comment("нагрузка_в_часах");
        column(dql, property("regDiscipline", EppRegistryElement.P_NUMBER), "RegNumber").comment("номер_в_реестре");

        return dql;
    }
}
