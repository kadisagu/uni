/* $Id$ */
package ru.tandemservice.unifefu.component.catalog.fefuOrphanPaymentType.FefuOrphanPaymentTypeItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubModel;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;

/**
 * @author nvankov
 * @since 11/18/13
 */
public class Model extends DefaultCatalogItemPubModel<FefuOrphanPaymentType>
{
}
