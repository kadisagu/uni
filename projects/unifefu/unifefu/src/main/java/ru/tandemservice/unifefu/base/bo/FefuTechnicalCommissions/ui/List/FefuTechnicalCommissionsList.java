/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

/**
 * @author Nikolay Fedorovskih
 * @since 11.06.2013
 */
@Configuration
public class FefuTechnicalCommissionsList extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_PARAM = "enrollmentCampaign";
    public static final String TECHNICAL_COMMISSIONS_DS = "technicalCommissionsDS";
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(searchListDS(TECHNICAL_COMMISSIONS_DS, getTechnicalCommissionsDS(), technicalCommissionsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getTechnicalCommissionsDS()
    {
        return columnListExtPointBuilder(TECHNICAL_COMMISSIONS_DS)
                .addColumn(actionColumn(FefuTechnicalCommission.title().s()).path(FefuTechnicalCommission.title()).listener("onClickView").order())
                .addColumn(textColumn(FefuTechnicalCommission.number().s(), FefuTechnicalCommission.number()).order())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                                        alert("technicalCommissionsDS.delete.alert", FefuTechnicalCommission.title())))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> technicalCommissionsDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                EnrollmentCampaign enrollmentCampaign = context.get(ENROLLMENT_CAMPAIGN_PARAM);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuTechnicalCommission.class, "e").column("e")
                        .where(DQLExpressions.eq(DQLExpressions.property(FefuTechnicalCommission.enrollmentCampaign().fromAlias("e")),
                                                 DQLExpressions.value(enrollmentCampaign)))
                        .order(DQLExpressions.property("e", input.getEntityOrder().getColumnName()), input.getEntityOrder().getDirection());

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).build();
            }
        };
    }
}