/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EcReportPerson;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexey Lopatin
 * @since 27.11.2014
 */
@Configuration
public class EcReportPersonExrManager extends BusinessObjectExtensionManager
{
}
