package ru.tandemservice.unifefu.component.orgunit.OrgUnitStudentPersonCards;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards.IOrgUnitStudentPersonCardPrintFactory;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.component.student.StudentPersonCard.StudentPersonCardPrintFactory;

import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 04.08.2009
 */
public class OrgUnitStudentPersonCardPrintFactory extends StudentPersonCardPrintFactory implements IOrgUnitStudentPersonCardPrintFactory
{
    private Map<Long, List<EnrollmentExtract>> _enrollmentExtractMap;
    private Map<Long, List<PersonBenefit>> _personBenefitsMap;

    private Map<Long, Map<String, List<AbstractStudentExtract>>> students2extractsMap = Maps.newHashMap();


    public Map<Long, List<EnrollmentExtract>> getEnrollmentExtractMap()
    {
        return _enrollmentExtractMap;
    }

    public void setEnrollmentExtractMap(Map<Long, List<EnrollmentExtract>> enrollmentExtractMap)
    {
        _enrollmentExtractMap = enrollmentExtractMap;
    }

    public Map<Long, List<PersonBenefit>> getPersonBenefitsMap()
    {
        return _personBenefitsMap;
    }

    public void setPersonBenefitsMap(Map<Long, List<PersonBenefit>> personBenefitsMap)
    {
        _personBenefitsMap = personBenefitsMap;
    }

    public Map<Long, Map<String, List<AbstractStudentExtract>>> getStudents2extractsMap()
    {
        return students2extractsMap;
    }

    public void setStudents2extractsMap(Map<Long, Map<String, List<AbstractStudentExtract>>> students2extractsMap)
    {
        this.students2extractsMap = students2extractsMap;
    }

    @Override
    public void addCard(RtfDocument document)
    {
        modifyByStudent(document);
    }
}