/**
 * SubmitStatusFaultMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportal;

public class SubmitStatusFaultMsg extends org.apache.axis.AxisFault {
    public java.lang.String submitStatusFaultMsgPart;
    public java.lang.String getSubmitStatusFaultMsgPart() {
        return this.submitStatusFaultMsgPart;
    }

    public SubmitStatusFaultMsg() {
    }

    public SubmitStatusFaultMsg(java.lang.Exception target) {
        super(target);
    }

    public SubmitStatusFaultMsg(java.lang.String message, java.lang.Throwable t) {
        super(message, t);
    }

      public SubmitStatusFaultMsg(java.lang.String submitStatusFaultMsgPart) {
        this.submitStatusFaultMsgPart = submitStatusFaultMsgPart;
    }

    /**
     * Writes the exception data to the faultDetails
     */
    public void writeDetails(javax.xml.namespace.QName qname, org.apache.axis.encoding.SerializationContext context) throws java.io.IOException {
        context.serialize(qname, null, submitStatusFaultMsgPart);
    }
}
