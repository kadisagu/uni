/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class FefuAcadGrantAssignParagraphPartWrapper implements Comparable<FefuAcadGrantAssignParagraphPartWrapper>
{
    private final Course _course;
    private final Group _group;
    private final ListStudentExtract _firstExtract;

    public FefuAcadGrantAssignParagraphPartWrapper(Course course, Group group, ListStudentExtract firstExtract)
    {
        _course = course;
        _group = group;
        _firstExtract = firstExtract;
    }

    private final List<FefuAcadGrantAssignExtractWrapper> _extractWrapperList = new ArrayList<>();

    public Course getCourse()
    {
        return _course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public List<FefuAcadGrantAssignExtractWrapper> getExtractWrapperList()
    {
        return _extractWrapperList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuAcadGrantAssignParagraphPartWrapper))
            return false;

        FefuAcadGrantAssignParagraphPartWrapper that = (FefuAcadGrantAssignParagraphPartWrapper) o;

        return _course.equals(that.getCourse()) && _group.equals(that.getGroup());
    }

    @Override
    public int hashCode()
    {
        return _course.hashCode() & _group.hashCode();
    }

    @Override
    public int compareTo(FefuAcadGrantAssignParagraphPartWrapper o)
    {
        int result = ((Integer) _course.getIntValue()).compareTo(o.getCourse().getIntValue());
        if (result == 0)
            result = _group.getTitle().compareTo(o.getGroup().getTitle());

        return result;
    }
}