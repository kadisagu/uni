/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.brs.base.IBaseFefuBrsObjectManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.logic.FefuBrsPpsGradingSumDataReportDAO;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.logic.IFefuBrsPpsGradingSumDataReportDAO;

/**
 * @author nvankov
 * @since 12/16/13
 */
@Configuration
public class FefuBrsPpsGradingSumDataReportManager extends BusinessObjectManager implements IBaseFefuBrsObjectManager
{
    @Override
    public String getPermissionKey()
    {
        return "fefuBrsPpsGradingSumDataReportPermissionKey";
    }

    public static FefuBrsPpsGradingSumDataReportManager instance()
    {
        return instance(FefuBrsPpsGradingSumDataReportManager.class);
    }

    @Bean
    public IFefuBrsPpsGradingSumDataReportDAO dao()
    {
        return new FefuBrsPpsGradingSumDataReportDAO();
    }
}



    