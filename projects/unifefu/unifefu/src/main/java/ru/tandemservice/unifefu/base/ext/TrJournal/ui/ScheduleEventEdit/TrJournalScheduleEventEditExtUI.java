package ru.tandemservice.unifefu.base.ext.TrJournal.ui.ScheduleEventEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.ScheduleEventEdit.TrJournalScheduleEventEditUI;

import java.util.Date;


/**
 * @author amakarova
 */
public class TrJournalScheduleEventEditExtUI extends UIAddon
{
    public TrJournalScheduleEventEditExtUI(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentActivate()
    {
        TrJournalScheduleEventEditUI presenter = getPresenter();

        presenter.getSchEvent().setDurationBegin(new Date());
        presenter.getSchEvent().setDurationEnd(new Date());
        presenter.onComponentActivate();
    }
}
