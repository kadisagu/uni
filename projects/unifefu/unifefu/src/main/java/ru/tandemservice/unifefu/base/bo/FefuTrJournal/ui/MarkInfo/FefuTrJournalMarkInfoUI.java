package ru.tandemservice.unifefu.base.bo.FefuTrJournal.ui.MarkInfo;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSearchListUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unifefu.entity.FefuStudentMarkHistory;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkInfo.TrJournalMarkInfoUI;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author amakarova
 */
public class FefuTrJournalMarkInfoUI extends TrJournalMarkInfoUI
{
    private DynamicListDataSource<FefuStudentMarkHistory> _dataSource;

    @Override
    public void onComponentRefresh()
    {
        prepareDataSource();
        super.onComponentRefresh();
    }

    private void prepareDataSource()
    {
        DynamicListDataSource<FefuStudentMarkHistory> dataSource = new DynamicListDataSource<>(getConfig().getBusinessComponent(), component -> {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuStudentMarkHistory.class, "h")
                    .where(eq(property(FefuStudentMarkHistory.trEduGroupEventStudent().fromAlias("h")), value(getMark())));
            CommonBaseSearchListUtil.createPage(FefuTrJournalMarkInfoUI.this._dataSource, builder);
        });
        dataSource.addColumn(new SimpleColumn("Дата отметки", FefuStudentMarkHistory.P_MARK_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Отметка", FefuStudentMarkHistory.P_GRADE_AS_LONG).setFormatter(new LongAsDoubleFormatter(2)));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete").setPermissionKey("fefuDeleteMarkHistory"));

        setDataSource(dataSource);
    }

    public DynamicListDataSource<FefuStudentMarkHistory> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<FefuStudentMarkHistory> dataSource)
    {
        _dataSource = dataSource;
    }

    public void onClickDelete()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }
}
