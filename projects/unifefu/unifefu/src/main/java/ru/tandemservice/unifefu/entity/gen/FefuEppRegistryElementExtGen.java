package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.FefuEppRegistryElementExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение элемента реестра
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEppRegistryElementExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEppRegistryElementExt";
    public static final String ENTITY_NAME = "fefuEppRegistryElementExt";
    public static final int VERSION_HASH = -185273307;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPP_REGISTRY_ELEMENT = "eppRegistryElement";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_SEND_TO_U_M_U = "sendToUMU";

    private EppRegistryElement _eppRegistryElement;     // Элемент реестра
    private OrgUnit _orgUnit;     // Ведущая кафедра
    private boolean _sendToUMU = false; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppRegistryElement getEppRegistryElement()
    {
        return _eppRegistryElement;
    }

    /**
     * @param eppRegistryElement Элемент реестра. Свойство не может быть null и должно быть уникальным.
     */
    public void setEppRegistryElement(EppRegistryElement eppRegistryElement)
    {
        dirty(_eppRegistryElement, eppRegistryElement);
        _eppRegistryElement = eppRegistryElement;
    }

    /**
     * @return Ведущая кафедра.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Ведущая кафедра.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isSendToUMU()
    {
        return _sendToUMU;
    }

    /**
     * @param sendToUMU  Свойство не может быть null.
     */
    public void setSendToUMU(boolean sendToUMU)
    {
        dirty(_sendToUMU, sendToUMU);
        _sendToUMU = sendToUMU;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEppRegistryElementExtGen)
        {
            setEppRegistryElement(((FefuEppRegistryElementExt)another).getEppRegistryElement());
            setOrgUnit(((FefuEppRegistryElementExt)another).getOrgUnit());
            setSendToUMU(((FefuEppRegistryElementExt)another).isSendToUMU());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEppRegistryElementExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEppRegistryElementExt.class;
        }

        public T newInstance()
        {
            return (T) new FefuEppRegistryElementExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eppRegistryElement":
                    return obj.getEppRegistryElement();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "sendToUMU":
                    return obj.isSendToUMU();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eppRegistryElement":
                    obj.setEppRegistryElement((EppRegistryElement) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "sendToUMU":
                    obj.setSendToUMU((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eppRegistryElement":
                        return true;
                case "orgUnit":
                        return true;
                case "sendToUMU":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eppRegistryElement":
                    return true;
                case "orgUnit":
                    return true;
                case "sendToUMU":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eppRegistryElement":
                    return EppRegistryElement.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "sendToUMU":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEppRegistryElementExt> _dslPath = new Path<FefuEppRegistryElementExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEppRegistryElementExt");
    }
            

    /**
     * @return Элемент реестра. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEppRegistryElementExt#getEppRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> eppRegistryElement()
    {
        return _dslPath.eppRegistryElement();
    }

    /**
     * @return Ведущая кафедра.
     * @see ru.tandemservice.unifefu.entity.FefuEppRegistryElementExt#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppRegistryElementExt#isSendToUMU()
     */
    public static PropertyPath<Boolean> sendToUMU()
    {
        return _dslPath.sendToUMU();
    }

    public static class Path<E extends FefuEppRegistryElementExt> extends EntityPath<E>
    {
        private EppRegistryElement.Path<EppRegistryElement> _eppRegistryElement;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Boolean> _sendToUMU;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент реестра. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEppRegistryElementExt#getEppRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> eppRegistryElement()
        {
            if(_eppRegistryElement == null )
                _eppRegistryElement = new EppRegistryElement.Path<EppRegistryElement>(L_EPP_REGISTRY_ELEMENT, this);
            return _eppRegistryElement;
        }

    /**
     * @return Ведущая кафедра.
     * @see ru.tandemservice.unifefu.entity.FefuEppRegistryElementExt#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppRegistryElementExt#isSendToUMU()
     */
        public PropertyPath<Boolean> sendToUMU()
        {
            if(_sendToUMU == null )
                _sendToUMU = new PropertyPath<Boolean>(FefuEppRegistryElementExtGen.P_SEND_TO_U_M_U, this);
            return _sendToUMU;
        }

        public Class getEntityClass()
        {
            return FefuEppRegistryElementExt.class;
        }

        public String getEntityName()
        {
            return "fefuEppRegistryElementExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
