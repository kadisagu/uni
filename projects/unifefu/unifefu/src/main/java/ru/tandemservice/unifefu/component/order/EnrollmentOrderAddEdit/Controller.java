/* $Id$ */
package ru.tandemservice.unifefu.component.order.EnrollmentOrderAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.uniec.IUniecComponents;

/**
 * @author Nikolay Fedorovskih
 * @since 31.07.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.Model baseModel = component.getModel(IUniecComponents.ENROLLMENT_ORDER_ADD_EDIT);
        Model model = getModel(component);

        getDao().prepare(model, baseModel.getOrder());
    }

    public void onClickApply(IBusinessComponent component)
    {
        ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.Model baseModel = component.getModel(IUniecComponents.ENROLLMENT_ORDER_ADD_EDIT);
        ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.Controller baseController = (ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.Controller) component.getController(IUniecComponents.ENROLLMENT_ORDER_ADD_EDIT);
        baseController.getDao().update(baseModel, component.getUserContext().getErrorCollector());

        if (component.getUserContext().getErrorCollector().hasErrors()) return;

        getDao().update(getModel(component), baseModel.getOrder());

        deactivate(component);
        if (baseModel.isAddForm())
            activateInRoot(component, new PublisherActivator(baseModel.getOrder()));
    }
}