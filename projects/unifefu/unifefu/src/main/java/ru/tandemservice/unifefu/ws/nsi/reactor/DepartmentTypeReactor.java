/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.log4j.Level;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unifefu.base.bo.NSISync.NSISyncManager;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuTopOrgUnit;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.datagram.DepartmentType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;
import ru.tandemservice.unifefu.ws.nsi.datagram.OrganizationType;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.FefuNsiIdWrapper;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 25.08.2013
 */
public class DepartmentTypeReactor extends SimpleCatalogEntityReactor<DepartmentType, OrgUnit>
{
    private static final Set<String> DEPARTMENTS_TO_IGNORE = new HashSet<>();
    private static Map<String, OrgUnitType> ORG_UNIT_TYPES_MAP = new LinkedHashMap<>();
    private static OrgUnitType DEPARTMENT_ORG_UNIT_TYPE = UniDaoFacade.getCoreDao().getCatalogItem(OrgUnitType.class, OrgUnitTypeCodes.DIVISION);

    static
    {
        DEPARTMENTS_TO_IGNORE.add("УЧЕБНЫЕ ПОДРАЗДЕЛЕНИЯ");
        DEPARTMENTS_TO_IGNORE.add("НЕУЧЕБНЫЕ ПОДРАЗДЕЛЕНИЯ");
        DEPARTMENTS_TO_IGNORE.add("Школы");

        OrgUnitType groupType = DataAccessServices.dao().get(OrgUnitType.class, OrgUnitType.title(), "Группа");
        ORG_UNIT_TYPES_MAP.put("Административно-управленческий персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Инженерно-технический персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Обслуживающий персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Профессорско-преподавательский персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Прочий обслуживающий персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Учебно-вспомогательный персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Административно-управленческий состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Обслуживающий состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Педагогический и воспитательный состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Педагогический состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Преподавательский состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Профессорско-преподавательский состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Учебно-вспомогательный состав".toUpperCase(), groupType);

        for (OrgUnitType ouType : DataAccessServices.dao().getList(OrgUnitType.class))
        {
            ORG_UNIT_TYPES_MAP.put(ouType.getTitle().toUpperCase(), ouType);
        }
    }

    private Map<String, OrgUnit> guidToOrgUnitMap = new HashMap<>();
    private Map<String, DepartmentType> departmentsMap = new HashMap<>();
    private Map<String, OrganizationType> organizationsMap = new HashMap<>();

    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return false;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<OrgUnit> getEntityClass()
    {
        return OrgUnit.class;
    }

    @Override
    public Class<DepartmentType> getNSIEntityClass()
    {
        return DepartmentType.class;
    }

    @Override
    public String getGUID(DepartmentType nsiEntity)
    {
        return nsiEntity.getID();
    }

    @Override
    public DepartmentType getCatalogElementRetrieveRequestObject(String guid)
    {
        DepartmentType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createDepartmentType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    protected Map<String, OrgUnit> getEntityMap(Class<OrgUnit> entityClass)
    {
        return getEntityMap(entityClass, null);
    }

    @Override
    protected Map<String, OrgUnit> getEntityMap(final Class<OrgUnit> entityClass, Set<Long> entityIds)
    {
        Map<String, OrgUnit> result = new HashMap<>();
        final List<OrgUnit> entityList = new ArrayList<>();

        if (null == entityIds)
        {
            entityList.addAll(DataAccessServices.dao().<OrgUnit>getList(
                    new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
            ));
        } else
        {
            BatchUtils.execute(entityIds, 100, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> entityIdsPortion)
                {
                    entityList.addAll(DataAccessServices.dao().<OrgUnit>getList(
                            new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
                                    .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                    ));
                }
            });
        }

        for (OrgUnit item : entityList)
        {
            result.put(item.getId().toString(), item);
            result.put(OrgUnit.title().s() + item.getTitle().trim().toUpperCase(), item);
        }

        return result;
    }

    @Override
    protected void prepareRelatedObjectsMap()
    {
        List<FefuTopOrgUnit> fefuTopOrgUnits = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuTopOrgUnit.class, "e").column("e")
        );

        Map<Long, Long> topOrgUnitToOrgUnitIdsMap = new HashMap<>();
        for (FefuTopOrgUnit topOrgUnit : fefuTopOrgUnits)
        {
            topOrgUnitToOrgUnitIdsMap.put(topOrgUnit.getId(), topOrgUnit.getOrgUnit().getId());
        }

        List<FefuNsiIds> topNsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(FefuTopOrgUnit.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : topNsiIdsList)
        {
            OrganizationType orgType = NsiReactorUtils.NSI_OBJECT_FACTORY.createOrganizationType();
            orgType.setID(nsiId.getGuid());
            organizationsMap.put(nsiId.getGuid(), orgType);
            organizationsMap.put(String.valueOf(nsiId.getEntityId()), orgType);
            Long orgUnitId = topOrgUnitToOrgUnitIdsMap.get(nsiId.getEntityId());
            if (null != orgUnitId) organizationsMap.put(orgUnitId.toString(), orgType);
        }

        List<FefuNsiIds> nsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(OrgUnit.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : nsiIdsList)
        {
            DepartmentType depType = getCatalogElementRetrieveRequestObject(nsiId.getGuid());
            departmentsMap.put(nsiId.getGuid(), depType);
            departmentsMap.put(String.valueOf(nsiId.getEntityId()), depType);
            OrganizationType orgType = NsiReactorUtils.NSI_OBJECT_FACTORY.createOrganizationType();
            orgType.setID(nsiId.getGuid());
            organizationsMap.put(nsiId.getGuid(), orgType);
            organizationsMap.put(String.valueOf(nsiId.getEntityId()), orgType);
        }
    }

    @Override
    public OrgUnit getPossibleDuplicate(DepartmentType nsiEntity, Map<String, FefuNsiIds> nsiIdsMap, Map<String, OrgUnit> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getDepartmentName()) return null;

        // Проверяем, есть ли в базе аналогичные переданной сущности
        OrgUnit possibleDuplicate = null;

        if (nsiIdsMap.containsKey(nsiEntity.getID()))
        {
            possibleDuplicate = similarEntityMap.get(String.valueOf(nsiIdsMap.get(nsiEntity.getID()).getEntityId()));
        }

        /*if (null == possibleDuplicate)
        {
            String titleIdx = nsiEntity.getDepartmentName().trim().toUpperCase();
            if (similarEntityMap.containsKey(titleIdx)) possibleDuplicate = similarEntityMap.get(titleIdx);
        }  */

        return possibleDuplicate;
    }

    @Override
    public boolean isAnythingChanged(DepartmentType nsiEntity, OrgUnit entity, Map<String, OrgUnit> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getDepartmentName()) return false;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getDepartmentID(), entity.getDivisionCode()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getDepartmentName(), entity.getTitle()))
                return true;

            if (null != entity.getParent())
            {
                DepartmentType parentDep = departmentsMap.get(entity.getParent().getId().toString());
                if (null != parentDep && (null == nsiEntity.getDepartmentUpper() || !parentDep.getID().equals(nsiEntity.getDepartmentUpper().getDepartment().getID())))
                    return true;
/*
                OrgUnit parentOrganization = entity.getParent();
                while (null != parentOrganization.getParent() && !(OrgUnitTypeCodes.BRANCH.equals(parentOrganization.getOrgUnitType().getCode())))
                    parentOrganization = parentOrganization.getParent();

                OrganizationType parent = organizationsMap.get(parentOrganization.getId().toString());
                if (null != parent)
                {
                    DepartmentType.OrganizationID parentOrg = new DepartmentType.OrganizationID();
                    parentOrg.setOrganization(parent);
                    nsiEntity.setOrganizationID(parentOrg);
                } */
            }
        }
        return false;
    }

    @Override
    public DepartmentType createEntity(OrgUnit entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        DepartmentType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createDepartmentType();
        nsiEntity.setID(getGUID(entity, nsiIdsMap));
        nsiEntity.setDepartmentID(entity.getDivisionCode());
        nsiEntity.setDepartmentName(entity.getTitle());

        if (null != entity.getParent())
        {
            DepartmentType parent = departmentsMap.get(entity.getParent().getId().toString());
            if (null != parent)
            {
                DepartmentType.DepartmentUpper upperDep = new DepartmentType.DepartmentUpper();
                upperDep.setDepartment(parent);
                nsiEntity.setDepartmentUpper(upperDep);
            }
        }

        if (null != entity.getParent())
        {
            OrgUnit parentOrganization = entity.getParent();
            while (null != parentOrganization.getParent() && !(OrgUnitTypeCodes.BRANCH.equals(parentOrganization.getOrgUnitType().getCode())))
                parentOrganization = parentOrganization.getParent();

            OrganizationType parent = organizationsMap.get(parentOrganization.getId().toString());
            if (null != parent)
            {
                DepartmentType.OrganizationID parentOrg = new DepartmentType.OrganizationID();
                parentOrg.setOrganization(parent);
                nsiEntity.setOrganizationID(parentOrg);
            }
        }

        departmentsMap.put(entity.getId().toString(), nsiEntity);
        departmentsMap.put(nsiEntity.getID(), nsiEntity);

        return nsiEntity;
    }

    @Override
    public OrgUnit createEntity(DepartmentType nsiEntity, Map<String, OrgUnit> similarEntityMap)
    {
        String parentDepartmentGuid = null != nsiEntity.getDepartmentUpper() ? nsiEntity.getDepartmentUpper().getDepartment().getID() : null;
        OrgUnit parentOrgUnit = guidToOrgUnitMap.get(parentDepartmentGuid);
        if (null == parentOrgUnit) parentOrgUnit = TopOrgUnit.getInstance();
        String titleUpper = nsiEntity.getDepartmentName().toUpperCase();

        OrgUnitType orgUnitType = null;
        for (Map.Entry<String, OrgUnitType> entry : ORG_UNIT_TYPES_MAP.entrySet())
        {
            if (titleUpper.contains(entry.getKey()))
            {
                orgUnitType = entry.getValue();
                break;
            }
        }
        if (null == orgUnitType) orgUnitType = DEPARTMENT_ORG_UNIT_TYPE;
        String departmentTitle = nsiEntity.getDepartmentName().trim();

        OrgUnit entity = new OrgUnit();
        entity.setParent(parentOrgUnit);
        entity.setArchival(Boolean.FALSE);
        entity.setOrgUnitType(orgUnitType);
        entity.setTitle(departmentTitle);
        entity.setShortTitle(departmentTitle);
        entity.setFullTitle(departmentTitle);
        entity.setTerritorialTitle(departmentTitle);
        entity.setTerritorialShortTitle(departmentTitle);
        entity.setTerritorialFullTitle(departmentTitle);
        entity.setDivisionCode(nsiEntity.getDepartmentID());

        return entity;
    }

    @Override
    public OrgUnit updatePossibleDuplicateFields(DepartmentType nsiEntity, OrgUnit entity, Map<String, OrgUnit> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getDepartmentName()) return null;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getDepartmentID(), entity.getDivisionCode()))
                entity.setDivisionCode(nsiEntity.getDepartmentID().trim());

            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getDepartmentName(), entity.getTitle()))
                entity.setTitle(nsiEntity.getDepartmentName().trim());

            //TODO Как быть с печатными названиями подразделения?

            if (null != nsiEntity.getDepartmentUpper() && null != nsiEntity.getDepartmentUpper().getDepartment())
            {
                OrgUnit parentOrgUnit = guidToOrgUnitMap.get(nsiEntity.getDepartmentUpper().getDepartment().getID());
                if (null == parentOrgUnit) parentOrgUnit = TopOrgUnit.getInstance();
                entity.setParent(parentOrgUnit);
            }
        }
        return entity;
    }

    @Override
    public DepartmentType updateNsiEntityFields(DepartmentType nsiEntity, OrgUnit entity)
    {
        nsiEntity.setDepartmentName(entity.getTitle());
        if (null != entity.getDivisionCode()) nsiEntity.setDepartmentID(entity.getDivisionCode());
        if (null != entity.getParent())
        {
            DepartmentType parent = departmentsMap.get(entity.getParent().getId().toString());
            if (null != parent)
            {
                DepartmentType.DepartmentUpper upperDep = new DepartmentType.DepartmentUpper();
                upperDep.setDepartment(parent);
                nsiEntity.setDepartmentUpper(upperDep);
            }
        }

        if (null != entity.getParent())
        {
            OrgUnit parentOrganization = entity.getParent();
            while (null != parentOrganization.getParent() && !(OrgUnitTypeCodes.BRANCH.equals(parentOrganization.getOrgUnitType().getCode())))
                parentOrganization = parentOrganization.getParent();

            OrganizationType parent = organizationsMap.get(parentOrganization.getId().toString());
            if (null != parent)
            {
                DepartmentType.OrganizationID parentOrg = new DepartmentType.OrganizationID();
                parentOrg.setOrganization(parent);
                nsiEntity.setOrganizationID(parentOrg);
            }
        }

        return nsiEntity;
    }

    public List<Object> commitPreparedOrgStructure(NsiReactorUtils.OrgStructureWrapper orgStructureWrapper)
    {
        // Подготовливаем мапы с подразделениями ОБ
        Map<String, OrgUnit> entityMap = getEntityMap(getEntityClass());

        // Формируем мап идентификаторов НСИ из числа сохраненных в ОБ, где ключ - идентификатор ОБ, либо GUID
        final Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMap(EntityRuntime.getMeta(getEntityClass()).getName());
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        // Если для синхронизации нужны дополнительные данные, то инициализируем их
        prepareRelatedObjectsMap();

        // Используется для отсылки новых дочерних подразделений на только что добавленных родителей
        Map<String, OrgUnit> justAddedOrgUnitsToGuidMap = new HashMap<>();
        List<Object> processedDepartmentsList = new ArrayList<>();

        // Проводим сравнение актуализированного списка подразделений из НСИ со списком элементов оргструктуры ОБ.
        // Добавлять новые элементы в НСИ мы не можем, поэтому связываем только имеющиеся, плюс создаем в ОБ
        // недостающие подразделения.
        for (String departmentGuid : orgStructureWrapper.resultDepartmentGuidsOrdered/*departmentsSerializedList*/)
        {
            DepartmentType nsiEntity = orgStructureWrapper.guidToDepartmentMap.get(departmentGuid);
            FefuNsiIds nsiId = nsiIdsMap.get(departmentGuid);
            OrgUnit mappedOrgUnit = null;

            // Если соответствующий GUID уже ассоциирован с подразделением в ОБ, то просто обновляем, при необходимости.
            if (null != nsiId) mappedOrgUnit = entityMap.get(nsiId.getEntityId());

            // Иначе, пытаемся ассоциировать с имеющимися элементами в ОБ.
            if (null == mappedOrgUnit)
            {
                // Пытаемся найти соответствующее подразделение, связанное по наименованию
                Long mappedByTitleOrgUnitId = orgStructureWrapper.resultGuidToOrgUnitMap.get(departmentGuid);
                if (null != mappedByTitleOrgUnitId)
                    mappedOrgUnit = entityMap.get(String.valueOf(mappedByTitleOrgUnitId));
            }

            // Если подразделение найдено по соответствию guid или по совпадению наименования, то обновляем существующее подарзделение
            // Ассоциируем его с соответствующим guid, меняем наименование, ссылку на родительское подразделение. Тип не меняем. Это неправильно.
            if (null != mappedOrgUnit)
            {
                if (isAnythingChanged(nsiEntity, mappedOrgUnit, entityMap))
                {
                    OrgUnit parentOrgUnit = getParentOrgUnit(nsiEntity);
                    String uniqueKey = null != parentOrgUnit ? String.valueOf(parentOrgUnit.getId()) : "";
                    uniqueKey += nsiEntity.getDepartmentName().toLowerCase();

                    if (!orgStructureWrapper.uniqueKeySet.contains(uniqueKey))
                    {
                        OrgUnit modifiedEntity = updatePossibleDuplicateFields(nsiEntity, mappedOrgUnit, entityMap);
                        orgStructureWrapper.uniqueKeySet.add(uniqueKey);
                        IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiEntities(modifiedEntity, nsiId, new FefuNsiIdWrapper(departmentGuid, getEntityName(), isAddItemsToOBAllowed(), isNsiMasterSystem()), null, null);
                        guidToOrgUnitMap.put(departmentGuid, modifiedEntity);
                        entityMap.put(String.valueOf(mappedOrgUnit.getId()), mappedOrgUnit);
                        entityMap.put(departmentGuid, mappedOrgUnit);
                        processedDepartmentsList.add(nsiEntity);

                        if (null != modifiedEntity.getParent())
                        {
                            System.out.println("+++++++++ " + modifiedEntity.getParent().getId() + " - " + modifiedEntity.getParent().getTitle() + " |||||| " + modifiedEntity.getTitle() + " - " + departmentGuid);
                        }
                    } else
                    {
                        FefuNsiSyncDAO.logEvent(Level.WARN, "---------- Department with guid '" + departmentGuid + "', title '" + nsiEntity.getDepartmentName() + "' was ignored, because there is another department with the same title inside the parent orgUnit (" + (null != parentOrgUnit ? parentOrgUnit.getId() : "unknownParenId") + ") ----------");
                    }
                } else
                {
                    guidToOrgUnitMap.put(departmentGuid, mappedOrgUnit);
                    IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(mappedOrgUnit, nsiId, new FefuNsiIdWrapper(departmentGuid, getEntityName()));
                }
                justAddedOrgUnitsToGuidMap.put(departmentGuid, mappedOrgUnit);
            }

            //Если подразделение не связано ни по guid, ни по наименованию, то создаём новое
            if (null == mappedOrgUnit)
            {
                OrgUnit entity = createEntity(nsiEntity, justAddedOrgUnitsToGuidMap);

                String uniqueKey = null != entity.getParent() ? String.valueOf(entity.getParent().getId()) : "";
                uniqueKey += entity.getTitle().toLowerCase();

                System.out.println(entity.getId() + " - " + nsiEntity.getDepartmentID() + " - " + nsiEntity.getDepartmentName());

                if (!orgStructureWrapper.uniqueKeySet.contains(uniqueKey))
                {
                    orgStructureWrapper.uniqueKeySet.add(uniqueKey);
                    IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiEntities(entity, nsiId, new FefuNsiIdWrapper(departmentGuid, getEntityName(), isAddItemsToOBAllowed(), isNsiMasterSystem()), null, null);
                    justAddedOrgUnitsToGuidMap.put(departmentGuid, entity);
                    guidToOrgUnitMap.put(departmentGuid, entity);
                    entityMap.put(String.valueOf(entity.getId()), entity);
                    processedDepartmentsList.add(nsiEntity);
                    entityMap.put(departmentGuid, entity);
                } else
                {
                    FefuNsiSyncDAO.logEvent(Level.WARN, "------+--- Department with guid '" + departmentGuid + "', title '" + entity.getTitle() + "' was ignored, because there is another department with the same title inside the parent orgUnit ----------");
                    entity = null;
                }
            }
        }

        // Корректируем настройку вложенности типов подразделений, чтобы соответствующие вкладки с подарзедлениями стали доступны на подразделении
        NSISyncManager.instance().dao().doCorrectOrgstructSettings();

        return processedDepartmentsList;
    }

    @Override
    public void synchronizeFullCatalogWithNsi()
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG HAVE STARTED ==========");

        // Забираем порцию элементов справочника из НСИ
        List<INsiEntity> departmentsList = new ArrayList<>();

        // TODO Раскомментировать, если нужно загружать список подразделений из файла
        /*byte[] orgstructFile = NsiReactorUtils.getEmulatedResponseDatagramElementsFromFile("d:/NsiDepartments17.12.2014 21_30.xml");
        List<DepartmentType> nsiEntityList = NsiReactorUtils.getObjectsFromNSIByFile(DepartmentType.class, orgstructFile);/**/

        // TODO Закомментировать, если нужно загружать список подразделений из файла
        List<DepartmentType> nsiEntityList = NsiReactorUtils.retrieveObjectsFromNsiBySingleElement(getNSIEntityClass(), getFullCatalogRetrieveRequestObject());
        for (DepartmentType departmentType : nsiEntityList) departmentsList.add(departmentType);

        // Подбор соответствия Подразделений НСИ подразделениям ОБ
        NsiReactorUtils.OrgStructureWrapper orgStructureWrapper = new NsiReactorUtils.OrgStructureWrapper();
        NsiReactorUtils.prepareActualDepartmentsStructure(departmentsList, orgStructureWrapper);
        NsiReactorUtils.prepareOBOrgStructure(orgStructureWrapper);
        NsiReactorUtils.printMappedOrgStructureRecursive(null, null, orgStructureWrapper, 0);

        departmentsMap.putAll(orgStructureWrapper.guidToDepartmentMap);

        // TODO: Если надо просто вывести маппинг подразделений в консоль, то раскомментарить
/*
        orgStructureWrapper.outputBuilder.append("\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.guidToDepartmentMap.values().size() + " departments received.\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.idToOrgUnitMap.values().size() + " orgUnits loaded from database.\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.mappedByGuidCnt + " mapped by guid\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.mappedByTitleCnt + " mapped by title\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.alreadyMappedOrgUnitsSet.size() + " mapped\n");

        orgStructureWrapper.outputBuilder.append("\n---------------------- Unmapped OB org units: \n");
        List<OrgUnit> unmappedOrgUnitList = new ArrayList<>();
        for (Long unusedId : orgStructureWrapper.unmappedOrgUnitIds)
        {
            OrgUnit orgUnit = orgStructureWrapper.idToOrgUnitMap.get(unusedId);
            if (null != orgUnit) unmappedOrgUnitList.add(orgUnit);
        }

        Collections.sort(unmappedOrgUnitList, new Comparator<OrgUnit>()
        {
            @Override
            public int compare(OrgUnit o1, OrgUnit o2)
            {
                return o1.getTitleWithParent().compareTo(o2.getTitleWithParent());
            }
        });

        for (OrgUnit orgUnit : unmappedOrgUnitList)
        {
            orgStructureWrapper.outputBuilder.append(orgUnit.getId());
            orgStructureWrapper.outputBuilder.append(". ").append(orgStructureWrapper.useSeparators ? "|" : "");
            orgStructureWrapper.outputBuilder.append(orgUnit.getTitleWithParent());
            orgStructureWrapper.outputBuilder.append("\n");
        }

        System.out.println(orgStructureWrapper.outputBuilder.toString());
        if(null != orgStructureWrapper.outputBuilder) return;/**/

        commitPreparedOrgStructure(orgStructureWrapper);

        // Изменяем время последней синхронизации для справочника в целом
        IFefuNsiSyncDAO.instance.get().updateNsiCatalogSyncTime(getNSIEntityClass().getSimpleName());
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG WAS FINISHED ==========");
    }

    @Override
    public void insertOrUpdateNewItemsAtNSI(Set<Long> entityToAddIdsSet)
    {
        // Ничего не делаем, ибо писать в НСИ нам нельзя.
    }

    private OrgUnit getParentOrgUnit(DepartmentType nsiEntity)
    {
        if (null != nsiEntity.getDepartmentUpper() && null != nsiEntity.getDepartmentUpper().getDepartment())
        {
            OrgUnit parentOrgUnit = guidToOrgUnitMap.get(nsiEntity.getDepartmentUpper().getDepartment().getID());
            if (null == parentOrgUnit) parentOrgUnit = TopOrgUnit.getInstance();
            return parentOrgUnit;
        }
        return null;
    }

    public List<Object> insertOrUpdateDepartments(List<INsiEntity> itemsToUpdate, String operationType) throws NSIProcessingErrorException
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB HAVE STARTED ==========");

        // Подбор соответствия Подразделений НСИ подразделениям ОБ
        NsiReactorUtils.OrgStructureWrapper orgStructureWrapper = new NsiReactorUtils.OrgStructureWrapper();
        NsiReactorUtils.prepareActualDepartmentsStructure(itemsToUpdate, orgStructureWrapper);
        NsiReactorUtils.prepareOBOrgStructure(orgStructureWrapper);
        for (String parentGuid : NsiReactorUtils.getRootDepartmentsForPartialOrgstructure(orgStructureWrapper))
            NsiReactorUtils.printMappedOrgStructureRecursive(parentGuid, null, orgStructureWrapper, 0);

        departmentsMap.putAll(orgStructureWrapper.guidToDepartmentMap);

        // TODO: Если надо просто вывести маппинг подразделений в консоль, то раскомментарить
/*
        orgStructureWrapper.outputBuilder.append("\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.guidToDepartmentMap.values().size() + " departments received.\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.idToOrgUnitMap.values().size() + " orgUnits loaded from database.\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.mappedByGuidCnt + " mapped by guid\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.mappedByTitleCnt + " mapped by title\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.alreadyMappedOrgUnitsSet.size() + " mapped\n");

        orgStructureWrapper.outputBuilder.append("\n---------------------- Unmapped OB org units: \n");


        System.out.println(orgStructureWrapper.outputBuilder.toString());
        if(null != orgStructureWrapper.outputBuilder) return new ArrayList<>();/**/


        guidToOrgUnitMap = NSISyncManager.instance().dao().getGuidToOrgUnitMap();
        List<Object> propcessedDepartmentsList = commitPreparedOrgStructure(orgStructureWrapper);

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
        return propcessedDepartmentsList;
    }

    @Override
    public List<Object> insert(List<INsiEntity> itemsToUpdate) throws NSIProcessingErrorException
    {
        return insertOrUpdateDepartments(itemsToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT);
    }

    @Override
    public List<Object> update(List<INsiEntity> itemsToUpdate) throws NSIProcessingErrorException
    {
        return insertOrUpdateDepartments(itemsToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE);
    }

    @Override
    public List<Object> delete(List<INsiEntity> itemsToDelete) throws NSIProcessingErrorException
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB HAVE STARTED ==========");

        Set<String> guidsToDelete = new HashSet<>();
        List<DepartmentType> departmentsToDelete = new ArrayList<>();
        List<Object> deletedObjects = new ArrayList<>();

        if (null != itemsToDelete)
        {
            for (Object nsiEntity : itemsToDelete)
            {
                if (nsiEntity instanceof DepartmentType)
                {
                    DepartmentType department = (DepartmentType) nsiEntity;
                    String guid = getGUID(department);
                    if (null != guid) guidsToDelete.add(guid);
                    departmentsToDelete.add(department);
                }
            }
        }

        if (guidsToDelete.isEmpty())
            throw new NSIProcessingErrorException("There is no any entity guid to delete in datagram.");

        //prepareRelatedObjectsMap();

        Map<String, OrgUnit> entityMap = guidsToDelete.isEmpty() ? getNsiSynchrinizedEntityMap(getEntityClass(), null, false) : getNsiSynchrinizedEntityMap(getEntityClass(), guidsToDelete, false);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        // Формируем мап идентификаторов НСИ из числа сохраненных в ОБ, где ключ - идентификатор ОБ, либо GUID
        Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMapByGuids(EntityRuntime.getMeta(getEntityClass()).getName(), guidsToDelete);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        Set<String> alreadyDeletedPersons = new HashSet<>();
        for (DepartmentType item : departmentsToDelete)
        {
            String guid = getGUID(item);

            if (null != guid)
            {
                if (!alreadyDeletedPersons.contains(guid))
                {
                    alreadyDeletedPersons.add(guid);
                    OrgUnit entity = entityMap.get(guid);
                    if (null != entity)
                    {
                        FefuNsiIds nsiId = nsiIdsMap.get(guid);
                        try
                        {
                            IFefuNsiSyncDAO.instance.get().deleteOrgUnitWithNsiId(entity, nsiId);
                        } catch (Exception e)
                        {
                            throw new NSIProcessingErrorException("There are one or more entities, linked to the item requested for delete. You should delete them before.");
                        }
                    } else
                    {
                        throw new NSIProcessingErrorException("There is no mapped entity in OB for the given entity GUID or the object has different entity type, than the requested.");
                    }

                    deletedObjects.add(item);
                }
            } else
            {
                throw new NSIProcessingErrorException("There is no GUID for one, or more entity in datagram.");
            }
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");

        return deletedObjects;
    }
}