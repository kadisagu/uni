/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unifefu.base.vo.FefuEntrantOriginalsVerificationReportVO;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 10.07.2013
 */
@Transactional
public class FefuEcReportDAO extends BaseModifyAggregateDAO implements IFefuEcReportDAO
{
    @Override
    public Long createReport(IFefuEcReportBuilder reportBuilder, IFefuEntrantReport report)
    {
        DatabaseFile reportFile = reportBuilder.getContent(getSession());
        baseCreate(reportFile);

        report.setContent(reportFile);
        baseCreate(report);

        return report.getId();
    }

    @Override
    public EnrollmentDirection getSelectedEnrollmentDirection(FefuEntrantOriginalsVerificationReportVO reportVO)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "ed");
        builder.where(eq(property("ed", EnrollmentDirection.enrollmentCampaign().id()), value(reportVO.getEnrollmentCampaign().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().formativeOrgUnit().id()), value(reportVO.getFormativeOrgUnit().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().territorialOrgUnit().id()), value(reportVO.getTerritorialOrgUnit().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().id()), value(reportVO.getEducationLevelsHighSchool().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().developForm().id()), value(reportVO.getDevelopForm().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().developCondition().id()), value(reportVO.getDevelopCondition().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().developTech().id()), value(reportVO.getDevelopTech().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().developPeriod().id()), value(reportVO.getDevelopPeriod().getId())));

        List<EnrollmentDirection> enrollmentDirections = createStatement(builder).list();
        if(enrollmentDirections.isEmpty()) return null;
        return enrollmentDirections.get(0);
    }
}