/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu21.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOExtract;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public class DAO  extends ModularStudentExtractPubDAO<FefuExcludeStuDPOExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));

        if (model.getExtract().isIndividual())
            model.setPrintFormFileName((String) getProperty(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.P_FILE_NAME,
                                                            FefuOrderToPrintFormRelation.L_ORDER, model.getExtract().getParagraph().getOrder()));
        model.setDpoProgramOld((FefuAdditionalProfessionalEducationProgram)getProperty(FefuAdditionalProfessionalEducationProgramForStudent.class, FefuAdditionalProfessionalEducationProgramForStudent.L_PROGRAM,
                                           FefuAdditionalProfessionalEducationProgramForStudent.L_STUDENT, model.getExtract().getEntity()));
    }

}