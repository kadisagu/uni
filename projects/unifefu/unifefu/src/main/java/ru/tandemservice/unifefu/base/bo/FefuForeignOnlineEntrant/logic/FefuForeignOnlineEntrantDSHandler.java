/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 4/2/13
 */
public class FefuForeignOnlineEntrantDSHandler extends DefaultSearchDataSourceHandler
{
    public FefuForeignOnlineEntrantDSHandler(String ownerId)
    {
        super(ownerId);
    }


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Number personalNum = context.get("personalNum");
        String lastName = context.get("lastName");
        String firstName = context.get("firstName");
        Date birthDate = context.get("birthDate");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuForeignOnlineEntrant.class, "oe");
        builder.fetchPath(DQLJoinType.left, FefuForeignOnlineEntrant.entrant().fromAlias("oe"), "entrant");

        if(null != personalNum)
        {
            builder.where(eq(property("oe", FefuForeignOnlineEntrant.personalNumber()), commonValue(personalNum)));
        }

        if(!StringUtils.isEmpty(lastName))
        {
            builder.where(or(
                    like(DQLFunctions.upper(property("oe", FefuForeignOnlineEntrant.lastNameRu())), value(CoreStringUtils.escapeLike(lastName, true))),
                    like(DQLFunctions.upper(property("oe", FefuForeignOnlineEntrant.lastNameEn())), value(CoreStringUtils.escapeLike(lastName, true)))
            ));
        }

        if(!StringUtils.isEmpty(firstName))
        {
            builder.where(or(
                    like(DQLFunctions.upper(property("oe", FefuForeignOnlineEntrant.firstNameRu())), value(CoreStringUtils.escapeLike(firstName, true))),
                    like(DQLFunctions.upper(property("oe", FefuForeignOnlineEntrant.firstNameEn())), value(CoreStringUtils.escapeLike(firstName, true)))
            ));
        }

        if(null != birthDate)
            builder.where(eq(property("oe", FefuForeignOnlineEntrant.birthDate()), valueDate(birthDate)));

        builder.order(property("oe", FefuForeignOnlineEntrant.personalNumber()));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).build();
    }
}
