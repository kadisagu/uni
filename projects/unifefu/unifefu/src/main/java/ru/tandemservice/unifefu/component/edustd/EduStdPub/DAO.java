/* $Id$ */
package ru.tandemservice.unifefu.component.edustd.EduStdPub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt;
import ru.tandemservice.unifefu.entity.gen.FefuEppStateEduStandardExtGen;

/**
 * @author Ekaterina Zvereva
 * @since 29.05.2015
 */
public class DAO extends ru.tandemservice.uniepp.component.edustd.EduStdPub.DAO
{
    @Override
    public void prepare(final ru.tandemservice.uniepp.component.edustd.EduStdPub.Model model)
    {
        super.prepare(model);
        Model modelExt = (Model) model;
        modelExt.setEduStandardExt((FefuEppStateEduStandardExt)getByNaturalId(new FefuEppStateEduStandardExtGen.NaturalId(model.getEppStateEduStandard())));
    }
}
