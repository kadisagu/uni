package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Неделя учебного графика (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEduPlanVersionWeekGen extends EntityBase
 implements INaturalIdentifiable<FefuEduPlanVersionWeekGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek";
    public static final String ENTITY_NAME = "fefuEduPlanVersionWeek";
    public static final int VERSION_HASH = 1609369151;
    private static IEntityMeta ENTITY_META;

    public static final String L_VERSION = "version";
    public static final String L_COURSE = "course";
    public static final String L_WEEK = "week";
    public static final String L_TERM = "term";
    public static final String L_WEEK_TYPE = "weekType";

    private EppEduPlanVersion _version;     // УПв
    private Course _course;     // Курс
    private EppWeek _week;     // Учебная неделя
    private Term _term;     // Семестр
    private EppWeekType _weekType;     // Тип недели

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return УПв. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getVersion()
    {
        return _version;
    }

    /**
     * @param version УПв. Свойство не может быть null.
     */
    public void setVersion(EppEduPlanVersion version)
    {
        dirty(_version, version);
        _version = version;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Учебная неделя. Свойство не может быть null.
     */
    @NotNull
    public EppWeek getWeek()
    {
        return _week;
    }

    /**
     * @param week Учебная неделя. Свойство не может быть null.
     */
    public void setWeek(EppWeek week)
    {
        dirty(_week, week);
        _week = week;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Тип недели.
     */
    public EppWeekType getWeekType()
    {
        return _weekType;
    }

    /**
     * @param weekType Тип недели.
     */
    public void setWeekType(EppWeekType weekType)
    {
        dirty(_weekType, weekType);
        _weekType = weekType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEduPlanVersionWeekGen)
        {
            if (withNaturalIdProperties)
            {
                setVersion(((FefuEduPlanVersionWeek)another).getVersion());
                setCourse(((FefuEduPlanVersionWeek)another).getCourse());
                setWeek(((FefuEduPlanVersionWeek)another).getWeek());
            }
            setTerm(((FefuEduPlanVersionWeek)another).getTerm());
            setWeekType(((FefuEduPlanVersionWeek)another).getWeekType());
        }
    }

    public INaturalId<FefuEduPlanVersionWeekGen> getNaturalId()
    {
        return new NaturalId(getVersion(), getCourse(), getWeek());
    }

    public static class NaturalId extends NaturalIdBase<FefuEduPlanVersionWeekGen>
    {
        private static final String PROXY_NAME = "FefuEduPlanVersionWeekNaturalProxy";

        private Long _version;
        private Long _course;
        private Long _week;

        public NaturalId()
        {}

        public NaturalId(EppEduPlanVersion version, Course course, EppWeek week)
        {
            _version = ((IEntity) version).getId();
            _course = ((IEntity) course).getId();
            _week = ((IEntity) week).getId();
        }

        public Long getVersion()
        {
            return _version;
        }

        public void setVersion(Long version)
        {
            _version = version;
        }

        public Long getCourse()
        {
            return _course;
        }

        public void setCourse(Long course)
        {
            _course = course;
        }

        public Long getWeek()
        {
            return _week;
        }

        public void setWeek(Long week)
        {
            _week = week;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuEduPlanVersionWeekGen.NaturalId) ) return false;

            FefuEduPlanVersionWeekGen.NaturalId that = (NaturalId) o;

            if( !equals(getVersion(), that.getVersion()) ) return false;
            if( !equals(getCourse(), that.getCourse()) ) return false;
            if( !equals(getWeek(), that.getWeek()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getVersion());
            result = hashCode(result, getCourse());
            result = hashCode(result, getWeek());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getVersion());
            sb.append("/");
            sb.append(getCourse());
            sb.append("/");
            sb.append(getWeek());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEduPlanVersionWeekGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEduPlanVersionWeek.class;
        }

        public T newInstance()
        {
            return (T) new FefuEduPlanVersionWeek();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "version":
                    return obj.getVersion();
                case "course":
                    return obj.getCourse();
                case "week":
                    return obj.getWeek();
                case "term":
                    return obj.getTerm();
                case "weekType":
                    return obj.getWeekType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "version":
                    obj.setVersion((EppEduPlanVersion) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "week":
                    obj.setWeek((EppWeek) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "weekType":
                    obj.setWeekType((EppWeekType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "version":
                        return true;
                case "course":
                        return true;
                case "week":
                        return true;
                case "term":
                        return true;
                case "weekType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "version":
                    return true;
                case "course":
                    return true;
                case "week":
                    return true;
                case "term":
                    return true;
                case "weekType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "version":
                    return EppEduPlanVersion.class;
                case "course":
                    return Course.class;
                case "week":
                    return EppWeek.class;
                case "term":
                    return Term.class;
                case "weekType":
                    return EppWeekType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEduPlanVersionWeek> _dslPath = new Path<FefuEduPlanVersionWeek>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEduPlanVersionWeek");
    }
            

    /**
     * @return УПв. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek#getVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> version()
    {
        return _dslPath.version();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Учебная неделя. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek#getWeek()
     */
    public static EppWeek.Path<EppWeek> week()
    {
        return _dslPath.week();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Тип недели.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek#getWeekType()
     */
    public static EppWeekType.Path<EppWeekType> weekType()
    {
        return _dslPath.weekType();
    }

    public static class Path<E extends FefuEduPlanVersionWeek> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _version;
        private Course.Path<Course> _course;
        private EppWeek.Path<EppWeek> _week;
        private Term.Path<Term> _term;
        private EppWeekType.Path<EppWeekType> _weekType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return УПв. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek#getVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> version()
        {
            if(_version == null )
                _version = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_VERSION, this);
            return _version;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Учебная неделя. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek#getWeek()
     */
        public EppWeek.Path<EppWeek> week()
        {
            if(_week == null )
                _week = new EppWeek.Path<EppWeek>(L_WEEK, this);
            return _week;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Тип недели.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek#getWeekType()
     */
        public EppWeekType.Path<EppWeekType> weekType()
        {
            if(_weekType == null )
                _weekType = new EppWeekType.Path<EppWeekType>(L_WEEK_TYPE, this);
            return _weekType;
        }

        public Class getEntityClass()
        {
            return FefuEduPlanVersionWeek.class;
        }

        public String getEntityName()
        {
            return "fefuEduPlanVersionWeek";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
