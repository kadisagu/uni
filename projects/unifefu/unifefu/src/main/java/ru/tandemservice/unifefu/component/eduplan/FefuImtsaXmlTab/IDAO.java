/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuImtsaXmlTab;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 07.09.2013
 */
public interface IDAO extends IUniDao<Model>
{
}