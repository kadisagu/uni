/* $Id: StudExtViewProvider4StudMainInfo.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.report.externalView;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.catalog.entity.EducationalInstitutionTypeKind;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLFunctions.year;

/**
 * @author Victor Nekrasov
 * @since 18.03.2014
 */

public class StudExtViewProvider4StudMainInfo  extends SimpleDQLExternalViewConfig
{
    @Override
        protected DQLSelectBuilder buildDqlQuery() {

            DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(Student.class, "std")

            .joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("std"), "eduOu")
            .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().fromAlias("eduOu"), "eduHS")
            .joinPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().fromAlias("eduHS"), "eduLevel")
            .joinPath(DQLJoinType.inner, EducationLevels.levelType().fromAlias("eduLevel"), "levelType")
            .joinPath(DQLJoinType.left, EducationLevels.qualification().fromAlias("eduLevel"), "eduQualification")
            .joinPath(DQLJoinType.left, Student.group().fromAlias("std"), "group")
            .joinPath(DQLJoinType.left, Student.compensationType().fromAlias("std"), "compType")
            .joinPath(DQLJoinType.left, Student.status().fromAlias("std"), "status")
            .joinPath(DQLJoinType.left, Student.course().fromAlias("std"), "course")

            .joinPath(DQLJoinType.inner, Student.person().fromAlias("std"), "person")
            .joinPath(DQLJoinType.left, Person.personEduInstitution().fromAlias("person"), "eduInst")
            .joinPath(DQLJoinType.left, PersonEduInstitution.eduInstitutionKind().fromAlias("eduInst"), "eduInstKind")
            .joinPath(DQLJoinType.left, EducationalInstitutionTypeKind.parent().fromAlias("eduInstKind"), "eduInstKindParent")
            .joinPath(DQLJoinType.left, PersonEduInstitution.educationLevelStage().fromAlias("eduInst"), "eduInstStage")
            .joinPath(DQLJoinType.left, EducationLevelStage.parent().fromAlias("eduInstStage"), "eduInstStageParent")
            .joinPath(DQLJoinType.left, PersonEduInstitution.graduationHonour().fromAlias("eduInst"), "gradHonour")
            .joinPath(DQLJoinType.left, PersonEduInstitution.addressItem().fromAlias("eduInst"), "eduInstAddressItem")
            .joinPath(DQLJoinType.left, AddressItem.country().fromAlias("eduInstAddressItem"), "eduInstAddressCountry")

//            .joinPath(DQLJoinType.left, EducationOrgUnit.territorialOrgUnit().head().fromAlias("eduOu"), "terrOuHead")
//            .joinPath(DQLJoinType.left, IPersistentEmployeePostGen.employee().person().identityCard().fromAlias("terrOuHead"), "terrHeadIdc")
            .joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "idc")
            .joinEntity("idc", DQLJoinType.left, AddressCountry.class, "citizenshipCountry", eq(property("idc", IdentityCard.citizenship().id()), property("citizenshipCountry", AddressCountry.id())));

            column(dql, property(Student.id().fromAlias("std")), "studentId").comment("ИД студента");
            booleanIntColumn(dql, property(Student.archival().fromAlias("std")), "studentArchival").comment("архивный, целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            column(dql, property(Student.bookNumber().fromAlias("std")), "bookNumber").comment("№ зачетной книжки");
            column(dql, property(Student.personalNumber().fromAlias("std")), "studentNumber").comment("личный номер студента");
            column(dql, property(Student.entranceYear().fromAlias("std")), "entranceYear").comment(" год поступления");
            column(dql, property(Student.finishYear().fromAlias("std")), "finishYear").comment("год выпуска");
            column(dql, property(CompensationType.shortTitle().fromAlias("compType")), "compensationType").comment("сокращенное название вида возмещения затрат");
            column(dql, property(CompensationType.code().fromAlias("compType")), "compensationTypeCode").comment("код справочника для вида возмещения затрат");

            column(dql, property(StudentStatus.shortTitle().fromAlias("status")), "statusShortTitle").comment("сокращенное название статуса");
            column(dql, property(StudentStatus.title().fromAlias("status")), "statusTitle").comment("полное название статуса");
            column(dql, property(StudentStatus.code().fromAlias("status")), "statusCode").comment("код статуса");
            booleanIntColumn(dql, property(StudentStatus.active().fromAlias("status")), "statusActive").comment("активный , целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");

            column(dql, property(Group.title().fromAlias("group")), "groupTitle").comment("название группы");

            column(dql, property(Course.title().fromAlias("course")), "courseTitle").comment("название курса");
            column(dql, property(Course.intValue().fromAlias("course")), "courseIntValue").comment("номер курса");

            column(dql, property(IdentityCard.birthDate().fromAlias("idc")), "birthDate").comment("дата рождения");
            column(dql, year(property(IdentityCard.birthDate().fromAlias("idc"))), "birthDateYear").comment("год рождения");
            column(dql, property(AddressCountry.title().fromAlias("citizenshipCountry")), "citizenship").comment("название страны");
            column(dql, property(AddressCountry.digitalCode().fromAlias("citizenshipCountry")), "citizenshipCode").comment("цифровой код страны");
            column(dql, property(IdentityCard.firstName().fromAlias("idc")), "firstName").comment("имя студента");
            column(dql, property(IdentityCard.lastName().fromAlias("idc")), "lastName").comment("фамилия студента");
            column(dql, property(IdentityCard.middleName().fromAlias("idc")), "middleName").comment("отчество студента");
            column(dql, property(IdentityCard.fullFio().fromAlias("idc")), "fullFio").comment("ФИО студента");
            column(dql, property(IdentityCard.sex().title().fromAlias("idc")), "sex").comment("пол");
            column(dql, property(IdentityCard.sex().code().fromAlias("idc")), "sexCode").comment("код справочника для пола");

            column(dql, property(EducationOrgUnit.developCondition().shortTitle().fromAlias("eduOu")), "developConditionShort").comment("сокр. название условия освоения");
            column(dql, property(EducationOrgUnit.developCondition().title().fromAlias("eduOu")), "developConditionTitle").comment("название условия освоения");
            column(dql, property(EducationOrgUnit.developCondition().code().fromAlias("eduOu")), "developConditionCode").comment("код справочника для условия освоения");
            column(dql, property(EducationOrgUnit.developForm().title().fromAlias("eduOu")), "developForm").comment("название формы освоения");
            column(dql, property(EducationOrgUnit.developForm().code().fromAlias("eduOu")), "developFormCode").comment("код справочника для формы освоения");
            column(dql, property(EducationOrgUnit.developPeriod().title().fromAlias("eduOu")), "developPeriod").comment(" название срока освоения");
            column(dql, property(EducationOrgUnit.developPeriod().lastCourse().fromAlias("eduOu")), "lastCourse").comment("последний курс срока освоения");
            column(dql, property(EducationOrgUnit.developTech().title().fromAlias("eduOu")), "developTech").comment("название технологии освоения");
            column(dql, property(EducationOrgUnit.developTech().code().fromAlias("eduOu")), "developTechCode").comment("код справочника для технологии освоения");
            column(dql, property(EducationOrgUnit.formativeOrgUnit().fullTitle().fromAlias("eduOu")), "formativeOrgUnit").comment("название формирующего подразделения");
            column(dql, property(EducationOrgUnit.territorialOrgUnit().territorialFullTitle().fromAlias("eduOu")), "territorialOrgUnit").comment("название территориального подразделения");
            column(dql, property(EducationLevelsHighSchool.printTitle().fromAlias("eduHS")), "educationLevelsHighSchool").comment("шифр и наименование  направления  подготовки (специальности)");
            column(dql, property(EducationLevelsHighSchool.orgUnit().title().fromAlias("eduHS")), "orgUnit").comment("название выпускающего подразделения");
            column(dql, property(Qualifications.title().fromAlias("eduQualification")), "qualification").comment("название квалификации");
            column(dql, property(Qualifications.code().fromAlias("eduQualification")), "qualificationCode").comment("код квалификации");
            column(dql, property(EducationLevels.inheritedOkso().fromAlias("eduLevel")), "okso").comment("код направления  подготовки (специальности)");
            column(dql, property(EducationLevels.title().fromAlias("eduLevel")), "educationLevel").comment("наименование  направления  подготовки (специальности)");

            booleanIntColumn(dql, StructureEducationLevels.additional().fromAlias("levelType"), "additional").comment("целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            booleanIntColumn(dql, StructureEducationLevels.bachelor().fromAlias("levelType"), "bachelor").comment("бакалавр, целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            booleanIntColumn(dql, StructureEducationLevels.basic().fromAlias("levelType"), "basic").comment("целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            booleanIntColumn(dql, StructureEducationLevels.gos2().fromAlias("levelType"), "gos2").comment("целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            booleanIntColumn(dql, StructureEducationLevels.gos3().fromAlias("levelType"), "gos3").comment("целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            booleanIntColumn(dql, StructureEducationLevels.high().fromAlias("levelType"), "high").comment("целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            booleanIntColumn(dql, StructureEducationLevels.master().fromAlias("levelType"), "master").comment("целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            booleanIntColumn(dql, StructureEducationLevels.middle().fromAlias("levelType"), "middle").comment("целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            booleanIntColumn(dql, StructureEducationLevels.profile().fromAlias("levelType"), "profile").comment("профиль, целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            booleanIntColumn(dql, StructureEducationLevels.specialization().fromAlias("levelType"), "specialization").comment("специализация,целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            booleanIntColumn(dql, StructureEducationLevels.specialty().fromAlias("levelType"), "specialty").comment("специальность, целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
            column(dql, property(StructureEducationLevels.title().fromAlias("levelType")), "levelType").comment("наименование структуры направления  подготовки (специальности)");

            /*
            column(dql, property(EducationalInstitutionTypeKind.shortTitle().fromAlias("eduInstKind")), "eduInstitutionKind");
            column(dql, property(EducationalInstitutionTypeKind.code().fromAlias("eduInstKind")), "eduInstitutionKindCode");
            column(dql, property(EducationalInstitutionTypeKind.shortTitle().fromAlias("eduInstKindParent")), "eduInstitutionKindParent");
            column(dql, property(EducationalInstitutionTypeKind.code().fromAlias("eduInstKindParent")), "eduInstitutionKindParentCode");
            column(dql, property(AddressCountry.title().fromAlias("eduInstAddressCountry")), "eduInstCountry");
            column(dql, property(AddressCountry.digitalCode().fromAlias("eduInstAddressCountry")), "eduInstCountryCode");


            column(dql, property(EducationLevelStage.shortTitle().fromAlias("eduInstStage")), "educationLevelStage");
            column(dql, property(EducationLevelStage.code().fromAlias("eduInstStage")), "educationLevelStageCode");
            column(dql, property(EducationLevelStage.shortTitle().fromAlias("eduInstStageParent")), "educationLevelStageParent");
            column(dql, property(EducationLevelStage.code().fromAlias("eduInstStageParent")), "educationLevelStageParentCode");
            column(dql, property(GraduationHonour.title().fromAlias("gradHonour")), "graduationHonour");

            column(
                dql,
                new DQLCaseExpressionBuilder()
                .when(gt(DQL.parseExpression("eduInst.mark3+eduInst.mark4+eduInst.mark5"), value(0)), DQL.parseExpression("(3*eduInst.mark3+4*eduInst.mark4+5*eduInst.mark5)/(eduInst.mark3+eduInst.mark4+eduInst.mark5)"))
                .otherwise(value(null))
                .build(),
                "eduInstitutionAverageMark"
            );
            column(dql, property(PersonEduInstitution.issuanceDate().fromAlias("eduInst")), "eduInstitutionIssuanceDate");
            column(dql, property(PersonEduInstitution.regionCode().fromAlias("eduInst")), "eduInstitutionRegionCode");
            column(dql, property(PersonEduInstitution.yearEnd().fromAlias("eduInst")), "eduInstitutionYearEnd");
            */
            return dql;
        }
}

