/* $Id $ */
package ru.tandemservice.unifefu.base.ext.SessionTransfer.ui.OutsideAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unifefu.base.ext.SessionTransfer.logic.SessionTransferExtDao;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutOpWrapper;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutsideAddEditUI;

import java.util.ArrayList;
import java.util.List;

import static ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTabPrintRtf.Controller.DVFU;

/**
 * @author Rostuncev Savva
 * @since 26.03.2014
 */

public class SessionTransferOutsideAddEditExtUI extends UIAddon
{
    public Boolean _checkInstitutionDefault = false;
    private List<SessionTransferOutOpExtWrapper> _rowList;

    private SessionTransferOutOpExtWrapper _currentRowTimeDisc;
    private SessionTransferOutOpExtWrapper _editedRowTimeDisc;

    public SessionTransferOutsideAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);

    }

    public Boolean getCheckInstitutionDefault()
    {
        return _checkInstitutionDefault;
    }

    public Boolean isCheckInstitutionDefault()
    {
        return _checkInstitutionDefault;
    }

    public void setCheckInstitutionDefault(Boolean _checkInstitutionDefault)
    {
        getPresenter().getSettings().set("eduInstitutionTitleCheck", _checkInstitutionDefault);
        this._checkInstitutionDefault = _checkInstitutionDefault;
    }


    public void onClickApply()
    {
        ((SessionTransferOutsideAddEditUI) getPresenter()).onClickApply();
        SessionTransferExtDao.instance().saveOrUpdateOutsideTransfer(getRowList(), ((SessionTransferOutsideAddEditUI) getPresenter()).getRowList());
    }

    public void onClickSaveRow()
    {
        if (((SessionTransferOutsideAddEditUI) getPresenter()).getEditedRowIndex() != null && ((SessionTransferOutsideAddEditUI) getPresenter()).getRowList().size() > ((SessionTransferOutsideAddEditUI) getPresenter()).getEditedRowIndex() && ((SessionTransferOutsideAddEditUI) getPresenter()).getEditedRowIndex() >= 0)
        {
            ((SessionTransferOutsideAddEditUI) getPresenter()).getRowList().set(((SessionTransferOutsideAddEditUI) getPresenter()).getEditedRowIndex(), ((SessionTransferOutsideAddEditUI) getPresenter()).getEditedRow());
            getEditedRow().setId(((SessionTransferOutsideAddEditUI) getPresenter()).getRowList().get(((SessionTransferOutsideAddEditUI) getPresenter()).getEditedRowIndex()).getId());
            getRowList().set(((SessionTransferOutsideAddEditUI) getPresenter()).getEditedRowIndex(), getEditedRow());
        }
        setEditedRow(null);
        ((SessionTransferOutsideAddEditUI) getPresenter()).setEditedRow(null);
        ((SessionTransferOutsideAddEditUI) getPresenter()).setRowAdded(false);
    }

    @Override
    public void onComponentRender()
    {
        super.onComponentRender();
        if (((SessionTransferOutsideAddEditUI) getPresenter()).getDocumentId() == null)
        {
            if (isCheckInstitutionDefault())
            {
                ((SessionTransferOutsideAddEditUI) getPresenter()).getDocument().setEduInstitutionTitle(DVFU);
            }
        }
    }

    @Override
    public void onComponentRefresh()
    {
        setRowList(new ArrayList<SessionTransferOutOpExtWrapper>());
        ((SessionTransferOutsideAddEditUI) getPresenter()).onComponentRefresh();
        if (((SessionTransferOutsideAddEditUI) getPresenter()).getDocumentId() != null)
        {
            for (SessionTransferOutOpWrapper operation : ((SessionTransferOutsideAddEditUI) getPresenter()).getRowList())
            {
                getRowList().add(new SessionTransferOutOpExtWrapper(operation));
            }
        }
    }

    public void onClickEditRow()
    {
        if (((SessionTransferOutsideAddEditUI) getPresenter()).getEditedRow() != null) return;
        SessionTransferOutOpWrapper baseRow = ((SessionTransferOutsideAddEditUI) getPresenter()).findRow();
        SessionTransferOutOpExtWrapper baseRowExt = findRow();
        ((SessionTransferOutsideAddEditUI) getPresenter()).setEditedRow(new SessionTransferOutOpWrapper(baseRow));
        setEditedRow(new SessionTransferOutOpExtWrapper(baseRowExt));
        ((SessionTransferOutsideAddEditUI) getPresenter()).setEditedRowIndex(((SessionTransferOutsideAddEditUI) getPresenter()).getRowList().indexOf(baseRow));
        ((SessionTransferOutsideAddEditUI) getPresenter()).prepareTargetMarkModel();
        ((SessionTransferOutsideAddEditUI) getPresenter()).setRowAdded(false);
    }

    private SessionTransferOutOpExtWrapper findRow()
    {
        Integer id = getListenerParameter();
        SessionTransferOutOpExtWrapper row = null;
        for (SessionTransferOutOpExtWrapper r : getRowList())
            if (r.getId() == id)
                row = r;
        return row;
    }

    public List<SessionTransferOutOpExtWrapper> getRowList()
    {
        return _rowList;
    }

    public void setRowList(List<SessionTransferOutOpExtWrapper> _rowList)
    {
        this._rowList = _rowList;
    }

    public SessionTransferOutOpExtWrapper getCurrentRow()
    {
        return _currentRowTimeDisc;
    }

    public void setCurrentRow(SessionTransferOutOpExtWrapper _currentRow)
    {
        this._currentRowTimeDisc = _currentRow;
    }

    public SessionTransferOutOpExtWrapper getEditedRow()
    {
        return _editedRowTimeDisc;
    }

    public void setEditedRow(SessionTransferOutOpExtWrapper _editedRow)
    {
        this._editedRowTimeDisc = _editedRow;
    }

    public void onClickAddRow()
    {
        if (((SessionTransferOutsideAddEditUI) getPresenter()).getEditedRow() != null) return;
        final SessionTransferOutOpWrapper newRow = new SessionTransferOutOpWrapper();
        ((SessionTransferOutsideAddEditUI) getPresenter()).setEditedRow(newRow);
        setEditedRow(new SessionTransferOutOpExtWrapper());
        ((SessionTransferOutsideAddEditUI) getPresenter()).getRowList().add(new SessionTransferOutOpWrapper());
        getRowList().add(new SessionTransferOutOpExtWrapper());
        ((SessionTransferOutsideAddEditUI) getPresenter()).setEditedRowIndex(((SessionTransferOutsideAddEditUI) getPresenter()).getRowList().size() - 1);
        ((SessionTransferOutsideAddEditUI) getPresenter()).setRowAdded(true);
    }

    public void onClickDeleteRow()
    {
        ((SessionTransferOutsideAddEditUI) getPresenter()).onClickDeleteRow();
        SessionTransferOutOpExtWrapper row = findRow();
        if (null != row)
            getRowList().remove(row);
    }

    public void onClickCancelEdit()
    {
        ((SessionTransferOutsideAddEditUI) getPresenter()).onClickCancelEdit();
        if (((SessionTransferOutsideAddEditUI) getPresenter()).isRowAdded() && !getRowList().isEmpty())
            getRowList().remove(getRowList().size() - 1);
        setEditedRow(null);
    }


    //    public void onClickEditRow()
//    {
//        ((SessionTransferOutsideAddEditUI)getPresenter()).onClickEditRow();
//        List<SessionTransferOutsideOperationExt> sessionTransferOutsideOperationExts= DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt.class,"a")
//                .where(DQLExpressions.eq(DQLExpressions.property(ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt.sessionTransferOutsideOperation().targetMark().slot().document().id().fromAlias("a")), DQLExpressions.value(((SessionTransferOutsideAddEditUI)getPresenter()).getDocumentId())))
//                .where(DQLExpressions.eq(DQLExpressions.property(ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt.sessionTransferOutsideOperation().discipline().fromAlias("a")),DQLExpressions.value(((SessionTransferOutsideAddEditUI)getPresenter()).getRowList().get(((SessionTransferOutsideAddEditUI)getPresenter()).getEditedRowIndex()).getSourceDiscipline())))
//                .where(DQLExpressions.eq(DQLExpressions.property(ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt.sessionTransferOutsideOperation().targetMark().slot().student().slot().sourceRow().id().fromAlias("a")),DQLExpressions.value(((SessionTransferOutsideAddEditUI)getPresenter()).getRowList().get(((SessionTransferOutsideAddEditUI)getPresenter()).getEditedRowIndex()).getEppSlot().getSlot().getSourceRow().getId()))));
//
//    }
}
