/* $Id$ */
package ru.tandemservice.unifefu.base.ext.JuridicalContactor.ui.Add;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

/**
 * @author nvankov
 * @since 8/20/13
 */
public class JuridicalContactorAddExtUI extends UIAddon
{
    private boolean _editShortTitle = false;

    public JuridicalContactorAddExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public boolean isEditShortTitle()
    {
        return _editShortTitle;
    }

    public void setEditShortTitle(boolean editShortTitle)
    {
        _editShortTitle = editShortTitle;
    }
}
