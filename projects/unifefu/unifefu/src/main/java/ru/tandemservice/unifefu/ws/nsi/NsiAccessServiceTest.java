/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi;

import org.apache.axis.AxisFault;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import ru.tandemservice.uniec_fis.util.IFisService;
import ru.tandemservice.unifefu.ws.nsi.datagram.ObjectFactory;
import ru.tandemservice.unifefu.ws.nsi.datagram.OrganizationType;
import ru.tandemservice.unifefu.ws.nsi.datagram.XDatagram;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.HandlerRegistry;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.*;


/**
 * @author Dmitry Seleznev
 * @since 25.06.2013
 */
public class NsiAccessServiceTest
{
    private static final ObjectFactory FACTORY = new ObjectFactory();

    public static void main(String[] args) throws Exception
    {

        try
        {
            ServiceResponseType response = retrieveOrganizations();//retrieveDepartments(); //retrievePostTypes(); //retrieveAgreementTypes(); //retrieveOrganizations();
            System.out.println(getXmlFormatted(response.getDatagram().get_any()[0].toString()));
            FileWriter out = new FileWriter("d:/retrieved" + DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date()).replaceAll(":", "_") + ".xml");
            out.write(getXmlFormatted(response.getDatagram().get_any()[0].toString()/*.replaceAll(";", "").replaceAll("&#x", "\\\\u0")*/));
            out.close();

            MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

            if (null != respMsg)
            {
                XDatagram respDatagram = fromXml(XDatagram.class, respMsg.getAsString().getBytes());

                List<String> rootOrgs = new ArrayList<>();
                Map<String, List<String>> result = new HashMap<>();

                for (Object retrievedObject : respDatagram.getEntityList())
                {
                    if (retrievedObject instanceof OrganizationType)
                    {
                        OrganizationType org = (OrganizationType) retrievedObject;
                        OrganizationType parentOrg = null != org.getOrganizationUpper() ? org.getOrganizationUpper().getOrganization() : null;
                        String resultLine = org.getOrganizationName() + (null != parentOrg ? (" (" + parentOrg.getOrganizationName() + ")") : "");
                        if (null == parentOrg) rootOrgs.add(resultLine);
                        else
                        {
                            String parentTitle = parentOrg.getOrganizationName();
                            List<String> orgList = result.get(parentTitle);
                            if (null == orgList) orgList = new ArrayList<>();
                            orgList.add(resultLine);
                            result.put(parentTitle, orgList);
                        }
                    }
                }

                Collections.sort(rootOrgs);
                for (Map.Entry<String, List<String>> entry : result.entrySet()) Collections.sort(entry.getValue());

                for (String line : rootOrgs)
                {
                    System.out.println(line);
                    List<String> childs = result.get(line);
                    if (null != childs)
                    {
                        for (String child : childs)
                            System.out.println("\t" + child);
                    }
                }

                /*Set<String> postGuids = new HashSet<>();
                List<EmployeeType> employeeTypes = new ArrayList<>();*/
/*                for (Object retrievedObject : respDatagram.getEntityList())
                {
                    if (retrievedObject instanceof EmployeeType)
                    {
                        EmployeeType empl = (EmployeeType) retrievedObject;
                        employeeTypes.add(empl);
                        if (null != empl.getEmployeePostID())
                            postGuids.add(empl.getEmployeePostID().getPost().getID());

                        System.out.print(empl.getID() + " - ");
                        if (null != empl.getEmployeePostID())
                            System.out.print(empl.getEmployeePostID().getPost().getID() + " - ");
                        else System.out.print("                                           - ");
                        if (null != empl.getHumanID())
                        {
                            HumanType human = empl.getHumanID().getHuman();
                            System.out.print(human.getHumanLastName() + " " + human.getHumanFirstName() + " " + human.getHumanMiddleName());
                        }
                        System.out.println();
                    }
                }

                System.out.println("PostTypes unique count = " + postGuids.size());

                /*List<PostType> postsList = new ArrayList<>();
                for (Object retrievedObject : respDatagram.getEntityList())
                {
                    if (retrievedObject instanceof PostType) postsList.add((PostType) retrievedObject);
                }

                Collections.sort(postsList, new Comparator<PostType>()
                {
                    @Override
                    public int compare(PostType o1, PostType o2)
                    {
                        if(null == o1.getPostName() && null == o2.getPostName()) return 0;
                        if(null == o1.getPostName() && null != o2.getPostName()) return 1;
                        if(null != o1.getPostName() && null == o2.getPostName()) return -1;
                        return o1.getPostName().compareTo(o2.getPostName());
                    }
                });

                for(PostType post : postsList)
                {
                    System.out.println(post.getPostName());
                } */

                System.out.println("--------------------------");
            }
        } catch (
                Exception e
                )

        {
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            throw e;
        }

    }

    private static ServiceResponseType retrieveDepartments() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        objects.add(FACTORY.createDepartmentType());
        return retrieveSomething(objects);
    }

    private static ServiceResponseType retrieveOrganizations() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        objects.add(FACTORY.createOrganizationType());
        return retrieveSomething(objects);
    }

    private static ServiceResponseType retrievePostTypes() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        objects.add(FACTORY.createPostType());
        return retrieveSomething(objects);
    }

    private static ServiceResponseType retrieveAgreementTypes() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        objects.add(FACTORY.createAgreementType());
        return retrieveSomething(objects);
    }

    private static ServiceResponseType retrieveSomething(List<Object> entityToRetrieveList) throws Exception
    {
        ServiceSoap_Service service = new ServiceSoap_ServiceLocator_del();
        ServiceSoap_PortType port = service.getServiceSoap12();

        HandlerRegistry registry = service.getHandlerRegistry();
        QName servicePort = new QName("http://www.qa.com", "ServiceSoap12");
        List handlerChain = registry.getHandlerChain(servicePort);
        HandlerInfo info = new HandlerInfo();
        info.setHandlerClass(NsiSecurityHandler.class);
        handlerChain.add(info);

        //service.getHandlerRegistry().getHandlerChain().

        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
        XDatagram xDatagram = FACTORY.createXDatagram();
        xDatagram.getEntityList().addAll(entityToRetrieveList);

        ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
        MessageElement datagramOut = new SOAPBodyElement(inStream);

        System.out.println(datagramOut.toString()); // TODO comment it

        header.setSourceId("OB");
        header.setOperationType(RoutingHeaderTypeOperationType.fromValue("retrieve"));
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        datagram.set_any(new MessageElement[]{datagramOut});
        request.setDatagram(datagram);

        return port.retrieve(request);
    }

    public static <T> T fromXml(final Class<T> clazz, final byte[] xml)
    {
        try
        {
            final JAXBContext jc = JAXBContext.newInstance(clazz.getPackage().getName());
            final Unmarshaller u = jc.createUnmarshaller();
            final Object object = u.unmarshal(new ByteArrayInputStream(xml));

            if (object instanceof JAXBElement)
            {
                JAXBElement element = ((JAXBElement) object);
                if ("error".equalsIgnoreCase(element.getName().getLocalPart()))
                {
                    throw new IFisService.FisError(element, element.getValue().toString()); // корневой тег с ошибкой
                }
            }

            return clazz.cast(object);
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static byte[] toXml(final Object root)
    {
        try
        {
            final JAXBContext jc = JAXBContext.newInstance(root.getClass());
            final Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            final ByteArrayOutputStream ba = new ByteArrayOutputStream(8192);
            m.marshal(root, ba);
            return ba.toByteArray();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static String getXmlFormatted(String src) throws Exception
    {
        if (null == src || src.trim().length() == 0) return null;

        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(src));
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(is);

        DOMSource domSource = new DOMSource(doc);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "Utf8");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);
        String result = sw.toString();//FefuCrazyXmlFormatter.INSTANCE.format(sw.toString());
        return result;
    }

/*
    final List<Object> objects = new ArrayList<>();

    for (String guid : ORGUNIT_GUIDS)
    {
        EmployeeType emplType = FACTORY.createEmployeeType();
        DepartmentType dep = FACTORY.createDepartmentType();
        dep.setID(guid);
        EmployeeType.EmployeeDepartmentID depId = FACTORY.createEmployeeTypeEmployeeDepartmentID();
        depId.setDepartment(dep);
        emplType.setEmployeeDepartmentID(depId);
        objects.add(emplType);
    }

    final Set<String> postGuids = new HashSet<>();
    final List<EmployeeType> employeeTypes = new ArrayList<>();
    final FileWriter out = new FileWriter("d:/retrieved" + DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date()).replaceAll(":", "_") + ".xml");
    final List<String> statCategory = new ArrayList<>();

    BatchUtils.execute(objects, 20, new BatchUtils.Action<Object>()
    {
        @Override
        public void execute(Collection<Object> elements)
        {
            try
            {
                List<Object> objectsPortion = new ArrayList<>();
                objectsPortion.addAll(elements);

                ServiceResponseType response = retrieveSomething(objectsPortion);
                out.write(response.getDatagram().get_any()[0].toString().replaceAll(";", "").replaceAll("&#x", "\\\\u0"));

                MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

                if (null != respMsg)
                {

                    XDatagram respDatagram = fromXml(XDatagram.class, respMsg.getAsString().getBytes());


                    for (Object retrievedObject : respDatagram.getEntityList())
                    {
                        if (retrievedObject instanceof EmployeeType)
                        {
                            EmployeeType empl = (EmployeeType) retrievedObject;
                            employeeTypes.add(empl);
                            if (null != empl.getEmployeePostID())
                            {
                                postGuids.add(empl.getEmployeePostID().getPost().getID());
                                if (!statCategory.contains(empl.getEmployeePostID().getPost().getPostStatisticalServiceCategory()))
                                    statCategory.add(empl.getEmployeePostID().getPost().getPostStatisticalServiceCategory());
                            }

                            System.out.print(empl.getID() + " - ");
                            if (null != empl.getEmployeePostID())
                                System.out.print(empl.getEmployeePostID().getPost().getID() + " - ");
                            else System.out.print("                                           - ");
                            if (null != empl.getHumanID())
                            {
                                HumanType human = empl.getHumanID().getHuman();
                                System.out.print(human.getHumanLastName() + " " + human.getHumanFirstName() + " " + human.getHumanMiddleName());
                            }
                            System.out.println();
                        }
                    }


                }
            } catch (Exception e)
            {
                if (e instanceof ServiceException)
                    System.out.println("!!!!! Malformed ULR Exception has occured!");
                if (e instanceof AxisFault)
                    System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
                //throw e;
            }
        }
    });

    out.close();

    System.out.println("=============================");
    Collections.sort(statCategory, new Comparator<String>()
    {
        @Override
        public int compare(String o1, String o2)
        {
            if (null == o1 && null == o2) return 0;
            if (null != o1 && null == o2) return -1;
            if (null == o1 && null != o2) return 1;
            return o1.compareTo(o2);
        }
    });
    for (String category : statCategory) System.out.println(category);
    System.out.println("=============================");


    System.out.println("PostTypes unique count = " + postGuids.size());

    /*List<PostType> postsList = new ArrayList<>();
    for (Object retrievedObject : respDatagram.getEntityList())
    {
        if (retrievedObject instanceof PostType) postsList.add((PostType) retrievedObject);
    }

    Collections.sort(postsList, new Comparator<PostType>()
    {
        @Override
        public int compare(PostType o1, PostType o2)
        {
            if(null == o1.getPostName() && null == o2.getPostName()) return 0;
            if(null == o1.getPostName() && null != o2.getPostName()) return 1;
            if(null != o1.getPostName() && null == o2.getPostName()) return -1;
            return o1.getPostName().compareTo(o2.getPostName());
        }
    });

    for(PostType post : postsList)
    {
        System.out.println(post.getPostName());
    } */


}