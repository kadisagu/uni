/* $Id$ */
package ru.tandemservice.unifefu.brs.base;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.*;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;

import java.util.Arrays;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 30.05.2014
 */
@Input({
               @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = BaseBrsReportPresenter.ORG_UNIT_ID_BIND_PARAM),
               @Bind(key = BaseBrsReportPresenter.ORG_UNIT_ID_BIND_PARAM, binding = BaseBrsReportPresenter.ORG_UNIT_ID_BIND_PARAM)
       })
@Output({
                @Bind(key = BaseBrsReportPresenter.ORG_UNIT_ID_BIND_PARAM, binding = BaseBrsReportPresenter.ORG_UNIT_ID_BIND_PARAM)
        })
public abstract class BaseBrsReportPresenter extends UIPresenter
{
    public static final String ORG_UNIT_ID_BIND_PARAM = "orgUnitId";

    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private OrgUnitSecModel _secModel;

    public OrgUnitSecModel getSecModel()
    {
        if (_secModel == null && getOrgUnitId() != null)
        {
            _secModel = new OrgUnitSecModel(getOrgUnit());
        }
        return _secModel;
    }

    public String getViewKey()
    {
        String permissionKey = ((IBaseFefuBrsObjectManager) getConfig().getBusinessObjectManager()).getPermissionKey();
        return getOrgUnitId() == null ? permissionKey : getSecModel().getPermission("orgUnit_" + permissionKey);
    }

    public ISecured getSecuredObject()
    {
        return null == getOrgUnitId() ? super.getSecuredObject() : getOrgUnit();
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        if (_orgUnitId == null)
            return null;

        if (_orgUnit == null)
            _orgUnit = DataAccessServices.dao().get(OrgUnit.class, _orgUnitId);

        return _orgUnit;
    }
}