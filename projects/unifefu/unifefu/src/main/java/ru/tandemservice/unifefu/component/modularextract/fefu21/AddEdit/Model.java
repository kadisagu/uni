/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu21.AddEdit;

import org.apache.tapestry.request.IUploadFile;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuExcludeStuDPOExtract>
{
    private StudentStatus _studentStatusNew;
    private IUploadFile _uploadFile;

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

}