/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.RegOrderListLogView;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.IPublisherColumnBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLStatement;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.Directum.ui.CardLineLogView.DirectumCardLineLogView;
import ru.tandemservice.unifefu.base.bo.FefuStudent.logic.FefuStudentFIOListDSHandler;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumLog;
import ru.tandemservice.unifefu.entity.ws.FefuStudentOrderExtension;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 28.11.2013
 */
@Configuration
public class DirectumRegOrderListLogView extends BusinessComponentManager
{
    public static final String REG_ORDER_LIST_LOG_DS = "regOrderListLogDS";
    public static final String FEFU_STUDENT_FIO_DS = "fefuStudentListDS";

    public static final String FEFU_REQUEST_DATE_FROM = "requestDateFrom";
    public static final String FEFU_REQUEST_DATE_TO = "requestDateTo";
    public static final String FEFU_STUDENT_FIO = "fefuStudentFio";
    public static final String FEFU_DIRECTUM_DATE = "directumDate";
    public static final String FEFU_DIRECTUM_NUMBER = "directumNumber";
    public static final String FEFU_DIRECTUM_ORDER_ID = "directumOrderId";
    public static final String FEFU_DIRECTUM_TASK_ID = "directumTaskId";

    public static final String FEFU_DATA_BOUND = "fefuDataBound";
    public static final String FEFU_NUMBER_BOUND = "fefuNumberBound";
    public static final String FEFU_EXTRACT_ID = "fefuExtractId";

    public static final String FEFU_OPERATION_TYPE = "operationType";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REG_ORDER_LIST_LOG_DS, getRegOrderListLogDS(), regOrderListLogDSHandler()))
                .addDataSource(selectDS(FEFU_STUDENT_FIO_DS, fefuStudentListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getRegOrderListLogDS()
    {
        IPublisherColumnBuilder linkResolverCreateDate = publisherColumn(FefuDirectumLog.order().createDate().s(), FefuDirectumLog.order().createDate()).publisherLinkResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Map<String, Object> params = new HashMap<>();
                FefuDirectumLog dlog = ((DataWrapper) entity).getWrapped();
                if (null != dlog.getOrder())
                    params.put(UIPresenter.PUBLISHER_ID, dlog.getOrder().getId());
                if (null != dlog.getEnrOrder())
                    params.put(UIPresenter.PUBLISHER_ID, dlog.getEnrOrder().getId());
                return params;
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return null;
            }
        });

        IPublisherColumnBuilder linkResolverDateTime = publisherColumn(FefuDirectumLog.operationDateTime().s(), FefuDirectumLog.operationDateTime()).publisherLinkResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                FefuDirectumLog dlog = ((DataWrapper) entity).getWrapped();
                Long extractId = (Long) entity.getProperty(FEFU_EXTRACT_ID);

                Map<String, Object> params = new HashMap<>();
                params.put(UIPresenter.PUBLISHER_ID, dlog.getId());
                params.put(FEFU_EXTRACT_ID, extractId);
                return params;
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return DirectumCardLineLogView.class.getSimpleName();
            }
        });

        IStyleResolver styleResolverDate = entity -> !((boolean) entity.getProperty(FEFU_DATA_BOUND)) ? "background-color:#ffe8f3" : null;
        IStyleResolver styleResolverNumber = entity -> !((boolean) entity.getProperty(FEFU_NUMBER_BOUND)) ? "background-color:#ffe8f3" : null;

        // DEV-4592, DEV-4307, DEV-4697, DEV-4725, DEV-4728
        return columnListExtPointBuilder(REG_ORDER_LIST_LOG_DS)
                .addColumn(linkResolverDateTime.formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn(FefuDirectumLog.P_DIRECTUM_ORDER_ID, FefuDirectumLog.directumOrderId()).order())
                .addColumn(blockColumn(FefuDirectumLog.P_DIRECTUM_TASK_ID, "directumTaskBlockColumn").order())
                .addColumn(textColumn(FefuDirectumLog.P_OPERATION_TYPE, FefuDirectumLog.operationType()).order())
                .addColumn(textColumn(FefuDirectumLog.L_EXECUTOR, FefuDirectumLog.executorStr()).order())
                .addColumn(linkResolverCreateDate.formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn(FefuDirectumLog.orderExt().directumDate().s(), FefuDirectumLog.orderExt().directumDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).styleResolver(styleResolverDate).order())
                .addColumn(textColumn(FefuDirectumLog.orderExt().directumNumber().s(), FefuDirectumLog.orderExt().directumNumber()).styleResolver(styleResolverNumber).order())
                .addColumn(textColumn(FefuDirectumLog.P_OPERATION_RESULT, FefuDirectumLog.operationResult()).order())
                .addColumn(actionColumn("orderExt.directumScan", new Icon("printer_pdf", "directumScanPdfPrint"), "onClickDirectumScanPdfPrint").disabled("ui:directumScanPdfPrintAvailable"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> regOrderListLogDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Date requestDateFrom = context.get(FEFU_REQUEST_DATE_FROM);
                Date requestDateTo = context.get(FEFU_REQUEST_DATE_TO);
                // DEV-4307
                List<Long> studentIdList = context.get(FEFU_STUDENT_FIO);
                Date directumDate = context.get(FEFU_DIRECTUM_DATE);
                String directumNumber = context.get(FEFU_DIRECTUM_NUMBER);
                String directumOrderId = context.get(FEFU_DIRECTUM_ORDER_ID);
                // DEV-4728
                String operationType = context.get(FEFU_OPERATION_TYPE);

                // DEV-4728
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuDirectumLog.class, "e").column("e")
                        .joinEntity("e", DQLJoinType.left, FefuStudentOrderExtension.class, "soe", eq(property("e", FefuDirectumLog.orderExt().id()), property("soe", FefuStudentOrderExtension.id())))
                        .joinEntity("e", DQLJoinType.left, AbstractStudentOrder.class, "aso", eq(property("e", FefuDirectumLog.order().id()), property("aso", AbstractStudentOrder.id())))
                        .order(property("e", input.getEntityOrder().getColumnName()), input.getEntityOrder().getDirection());

                if (null != requestDateFrom)
                {
	                requestDateFrom = CoreDateUtils.getDayFirstTimeMoment(requestDateFrom);
                    builder.where(ge(property("e", FefuDirectumLog.operationDateTime()), valueTimestamp(requestDateFrom)));
                }

                if (null != requestDateTo)
                {
	                requestDateTo = CoreDateUtils.getNextDayFirstTimeMoment(requestDateTo, 1);
                    builder.where(lt(property("e", FefuDirectumLog.operationDateTime()), valueTimestamp(requestDateTo)));
                }

                if (null != studentIdList && !studentIdList.isEmpty())
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                            .joinPath(DQLJoinType.inner, AbstractStudentExtract.paragraph().fromAlias("e"), "p")
                            .column("p." + IAbstractParagraph.L_ORDER)
                            .where(in(property("e", AbstractStudentExtract.entity()), studentIdList));

                    builder.where(in(property(AbstractStudentOrder.id().fromAlias("aso")), subBuilder.buildQuery()));
                    //TODO
                }

                if (null != directumDate)
                {
                    builder.where(or(
                            eq(property("soe", FefuStudentOrderExtension.directumDate()), valueDate(directumDate)),
                            eq(property("aso", AbstractStudentOrder.commitDate()), valueDate(directumDate))
                    ));
                }

                if (null != directumNumber)
                {
                    builder.where(or(
                            eq(property("soe", FefuStudentOrderExtension.directumNumber()), value(directumNumber)),
                            eq(property("aso", AbstractStudentOrder.number()), value(directumNumber))
                    ));
                }

                if (null != directumOrderId)
                {
                    builder.where(or(
                            eq(property("soe", FefuStudentOrderExtension.directumOrderId()), value(directumOrderId)),
                            eq(property("e", FefuDirectumLog.directumOrderId()), value(directumOrderId)),
                            // DEV-4697
                            eq(property("soe", FefuStudentOrderExtension.directumTaskId()), value(directumOrderId)),
                            eq(property("e", FefuDirectumLog.directumTaskId()), value(directumOrderId)),
                            // DEV-4307
                            like(property("e", FefuDirectumLog.operationResult()), value("%"+directumOrderId+"%"))
                    ));
                }

                // DEV-4728
                if (null != operationType)
                {
                    builder.where(or(
                            like(property("e", FefuDirectumLog.operationType()), value("%"+operationType+"%"))
                    ));
                }

                DQLExecutionContext dqlContext = new DQLExecutionContext(context.getSession());
                int totalSize = ((Long) builder.createCountStatement(dqlContext).uniqueResult()).intValue();
                int startRec = input.getStartRecord();
                int countRec = input.getCountRecord();

                if (-1 == startRec || totalSize < startRec)
                {
                    startRec = totalSize - (0 != totalSize % countRec ? totalSize % countRec : countRec);
                }

                IDQLStatement statement = builder.createStatement(context.getSession());
                statement.setFirstResult(startRec);
                statement.setMaxResults(countRec);
                List<FefuDirectumLog> directumList = statement.list();

                List<Object> recordList = new ArrayList<>();
                for (FefuDirectumLog dlog : directumList)
                {
                    String orderNumber = null != dlog.getOrder() ? dlog.getOrder().getNumber() : null;
                    Date orderDate = null != dlog.getOrder() ? dlog.getOrder().getCommitDate() : null;

                    String orderDateStr = null != orderDate ? DateFormatter.DEFAULT_DATE_FORMATTER.format(orderDate) : null;

                    String extOrderNumber = null != dlog.getOrderExt() ? dlog.getOrderExt().getDirectumNumber() : null;
                    Date extOrderDate = null != dlog.getOrderExt() ? dlog.getOrderExt().getDirectumDate() : null;
                    String extOrderDateStr = null != extOrderDate ? DateFormatter.DEFAULT_DATE_FORMATTER.format(extOrderDate) : null;

                    boolean numberError = true;
                    boolean dateError = true;

                    if (null != orderNumber || null != extOrderNumber)
                    {
                        numberError = null != orderNumber && null != extOrderNumber && orderNumber.equals(extOrderNumber);
                    }

                    if (null != orderDateStr || null != extOrderDateStr)
                    {
                        dateError = null != orderDateStr && null != extOrderDateStr && orderDateStr.equals(extOrderDateStr);
                    }

                    DataWrapper record = new DataWrapper(dlog.getId(), String.valueOf(dlog.getId()), dlog);
                    record.setProperty(FEFU_NUMBER_BOUND, numberError);
                    record.setProperty(FEFU_DATA_BOUND, dateError);
                    recordList.add(record);
                }

                DSOutput output = new DSOutput(input);
                output.setRecordList(recordList);
                output.setTotalSize(totalSize);
                return output;
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> fefuStudentListDSHandler()
    {
        EntityComboDataSourceHandler handler = new FefuStudentFIOListDSHandler(getName())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = super.query(alias, filter);
                builder.where(eq(property(Student.archival().fromAlias(alias)), value(Boolean.FALSE)));
                return builder;
            }
        };
        return handler.order(Student.person().identityCard().fullFio()).pageable(true);
    }
}
