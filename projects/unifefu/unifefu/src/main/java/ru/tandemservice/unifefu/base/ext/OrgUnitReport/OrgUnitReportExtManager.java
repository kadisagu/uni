/* $Id:$ */
package ru.tandemservice.unifefu.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.uni.component.reports.FormativeAndTerritorialOrgUnitReportVisibleResolver;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.logic.FefuBrsOrgUnitReportVisibleResolver;

/**
 * @author rsizonenko
 * @since 06.11.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager {

    public static final String UNI_FEFU_ORG_UNIT_BRS_REPORT_BLOCK = "unifefuOrgUnitBrsReportBlock";

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;

    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {
        return itemListExtension(_orgUnitReportManager.blockListExtPoint())
                .add(UNI_FEFU_ORG_UNIT_BRS_REPORT_BLOCK, new OrgUnitReportBlockDefinition("Рейтинговая Система", UNI_FEFU_ORG_UNIT_BRS_REPORT_BLOCK, new FefuBrsOrgUnitReportVisibleResolver()))
                .replace("uniecOrgUnitReportBlock", new OrgUnitReportBlockDefinition("Отчеты модуля «Абитуриенты»", "uniecOrgUnitReportBlock", new FormativeAndTerritorialOrgUnitReportVisibleResolver()))
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {
        return itemListExtension(_orgUnitReportManager.reportListExtPoint())
                .add("fefuBrsAttestationResultsReport", new OrgUnitReportDefinition("Результаты текущей/промежуточной аттестации студентов по дисциплинам, участвующим в рейтинговой системе", "fefuBrsAttestationResultsReport", UNI_FEFU_ORG_UNIT_BRS_REPORT_BLOCK, "FefuBrsAttestationResultsReportList", "orgUnit_fefuBrsReportAttestationResultsPermissionKey"))
                .add("fefuBrsPercentGradingReport", new OrgUnitReportDefinition("Процент проставленных оценок", "fefuBrsPercentGradingReport", UNI_FEFU_ORG_UNIT_BRS_REPORT_BLOCK, "FefuBrsPercentGradingReportList", "orgUnit_fefuBrsPercentGradingReportPermissionKey"))
                .add("fefuBrsDelayGradingReport", new OrgUnitReportDefinition("Опоздание в простановке оценок", "fefuBrsDelayGradingReport", UNI_FEFU_ORG_UNIT_BRS_REPORT_BLOCK, "FefuBrsDelayGradingReportList", "orgUnit_fefuBrsDelayGradingReportPermissionKey"))
                .add("fefuBrsPpsGradingSumDataReport", new OrgUnitReportDefinition("Сводные данные выставления оценок преподавателями", "fefuBrsPpsGradingSumDataReport", UNI_FEFU_ORG_UNIT_BRS_REPORT_BLOCK, "FefuBrsPpsGradingSumDataReportList", "orgUnit_fefuBrsPpsGradingSumDataReportPermissionKey"))
                .add("fefuBrsStudentDisciplinesReport", new OrgUnitReportDefinition("Рейтинг студента по всем дисциплинам", "fefuBrsStudentDisciplinesReport", UNI_FEFU_ORG_UNIT_BRS_REPORT_BLOCK, "FefuBrsStudentDisciplinesReportList", "orgUnit_fefuBrsStudentDisciplinesReportPermissionKey"))
                .add("fefuBrsSumDataReport", new OrgUnitReportDefinition("Сводные данные балльно-рейтинговой системы", "fefuBrsSumDataReport", UNI_FEFU_ORG_UNIT_BRS_REPORT_BLOCK, "FefuBrsSumDataReportList", "orgUnit_fefuBrsSumDataReportPermissionKey"))
                .add("fefuListTrJournalDisciplinesReport", new OrgUnitReportDefinition("Перечень реализаций (рейтинг-планов) дисциплин", "fefuListTrJournalDisciplinesReport", UNI_FEFU_ORG_UNIT_BRS_REPORT_BLOCK, "FefuListTrJournalDisciplinesReportList", "orgUnit_fefuListTrJournalDisciplinesReportPermissionKey"))
                .add("fefuRatingGroupAllDiscReport", new OrgUnitReportDefinition("Рейтинг группы по всем дисциплинам", "fefuRatingGroupAllDiscReport", UNI_FEFU_ORG_UNIT_BRS_REPORT_BLOCK, "FefuRatingGroupAllDiscReportList", "orgUnit_fefuRatingGroupAllDiscPermissionKey"))
                .create();
    }
}
