/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.FefuForeignOnlineEntrantSearchStep;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;

import java.util.List;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    public static final String FOREIGN_ENTRANT_MASTER_PERM_KEY = "addForeignEntrantMaster";
    private FefuForeignOnlineEntrant _fefuForeignOnlineEntrant;
    private ISelectModel _fefuForeignOnlineEntrantModel;

    private EnrollmentCampaign _enrollmentCampaign;
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public FefuForeignOnlineEntrant getFefuForeignOnlineEntrant()
    {
        return _fefuForeignOnlineEntrant;
    }

    public void setFefuForeignOnlineEntrant(FefuForeignOnlineEntrant fefuForeignOnlineEntrant)
    {
        _fefuForeignOnlineEntrant = fefuForeignOnlineEntrant;
    }

    public ISelectModel getFefuForeignOnlineEntrantModel()
    {
        return _fefuForeignOnlineEntrantModel;
    }

    public void setFefuForeignOnlineEntrantModel(ISelectModel fefuForeignOnlineEntrantModel)
    {
        _fefuForeignOnlineEntrantModel = fefuForeignOnlineEntrantModel;
    }
}