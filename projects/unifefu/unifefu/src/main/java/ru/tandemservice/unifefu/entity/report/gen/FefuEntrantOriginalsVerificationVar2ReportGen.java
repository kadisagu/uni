package ru.tandemservice.unifefu.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списки абитуриентов для сверки по подлинникам (вариант 2)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEntrantOriginalsVerificationVar2ReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report";
    public static final String ENTITY_NAME = "fefuEntrantOriginalsVerificationVar2Report";
    public static final int VERSION_HASH = -621600209;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_TECHNIC_COMMISSION_TITLE = "technicCommissionTitle";
    public static final String P_STUDENT_CATEGORY_TITLE = "studentCategoryTitle";
    public static final String P_ORDER_BY_ORIGINALS = "orderByOriginals";
    public static final String P_ONLY_WITH_ORIGINALS = "onlyWithOriginals";
    public static final String P_ALL_ENROLLMENT_DIRECTIONS = "allEnrollmentDirections";
    public static final String P_FORMATIVE_ORG_UNIT_TITLE = "formativeOrgUnitTitle";
    public static final String P_TERRITORIAL_ORG_UNIT_TITLE = "territorialOrgUnitTitle";
    public static final String P_EDUCATION_LEVELS_HIGH_SCHOOL_TITLE = "educationLevelsHighSchoolTitle";
    public static final String P_DEVELOP_TECH_TITLE = "developTechTitle";
    public static final String P_DEVELOP_PERIOD_TITLE = "developPeriodTitle";
    public static final String P_DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String P_INCLUDE_FOREIGN_PERSON = "includeForeignPerson";
    public static final String P_INCLUDE_ENTRANT_WITH_BENEFIT = "includeEntrantWithBenefit";
    public static final String P_INCLUDE_ENTRANT_TARGET_ADMISSION = "includeEntrantTargetAdmission";
    public static final String P_PERIOD_TITLE = "periodTitle";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _technicCommissionTitle;     // Техническая комиссия
    private String _studentCategoryTitle;     // Категория поступающего
    private boolean _orderByOriginals;     // Выделить абитуриентов с оригиналами документов
    private boolean _onlyWithOriginals;     // Не включать абитуриентов без оригиналов документов
    private boolean _allEnrollmentDirections = false;     // По всем направлениям/специальностям
    private String _formativeOrgUnitTitle;     // Формирующее подразделение
    private String _territorialOrgUnitTitle;     // Территориальное подразделение
    private String _educationLevelsHighSchoolTitle;     // Направление подготовки (специальности) ОУ
    private String _developTechTitle;     // Технология освоения
    private String _developPeriodTitle;     // Срок освоения
    private String _developFormTitle;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private String _qualificationTitle;     // Квалификация
    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private Boolean _includeForeignPerson;     // Учитывать иностранных граждан
    private Boolean _includeEntrantWithBenefit;     // Выводить абитуриентов, имеющих льготы
    private Boolean _includeEntrantTargetAdmission;     // Выводить абитуриентов, поступающих по целевому приему

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Техническая комиссия.
     */
    @Length(max=255)
    public String getTechnicCommissionTitle()
    {
        return _technicCommissionTitle;
    }

    /**
     * @param technicCommissionTitle Техническая комиссия.
     */
    public void setTechnicCommissionTitle(String technicCommissionTitle)
    {
        dirty(_technicCommissionTitle, technicCommissionTitle);
        _technicCommissionTitle = technicCommissionTitle;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryTitle()
    {
        return _studentCategoryTitle;
    }

    /**
     * @param studentCategoryTitle Категория поступающего.
     */
    public void setStudentCategoryTitle(String studentCategoryTitle)
    {
        dirty(_studentCategoryTitle, studentCategoryTitle);
        _studentCategoryTitle = studentCategoryTitle;
    }

    /**
     * @return Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isOrderByOriginals()
    {
        return _orderByOriginals;
    }

    /**
     * @param orderByOriginals Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     */
    public void setOrderByOriginals(boolean orderByOriginals)
    {
        dirty(_orderByOriginals, orderByOriginals);
        _orderByOriginals = orderByOriginals;
    }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     */
    @NotNull
    public boolean isOnlyWithOriginals()
    {
        return _onlyWithOriginals;
    }

    /**
     * @param onlyWithOriginals Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     */
    public void setOnlyWithOriginals(boolean onlyWithOriginals)
    {
        dirty(_onlyWithOriginals, onlyWithOriginals);
        _onlyWithOriginals = onlyWithOriginals;
    }

    /**
     * @return По всем направлениям/специальностям. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllEnrollmentDirections()
    {
        return _allEnrollmentDirections;
    }

    /**
     * @param allEnrollmentDirections По всем направлениям/специальностям. Свойство не может быть null.
     */
    public void setAllEnrollmentDirections(boolean allEnrollmentDirections)
    {
        dirty(_allEnrollmentDirections, allEnrollmentDirections);
        _allEnrollmentDirections = allEnrollmentDirections;
    }

    /**
     * @return Формирующее подразделение.
     */
    @Length(max=255)
    public String getFormativeOrgUnitTitle()
    {
        return _formativeOrgUnitTitle;
    }

    /**
     * @param formativeOrgUnitTitle Формирующее подразделение.
     */
    public void setFormativeOrgUnitTitle(String formativeOrgUnitTitle)
    {
        dirty(_formativeOrgUnitTitle, formativeOrgUnitTitle);
        _formativeOrgUnitTitle = formativeOrgUnitTitle;
    }

    /**
     * @return Территориальное подразделение.
     */
    @Length(max=255)
    public String getTerritorialOrgUnitTitle()
    {
        return _territorialOrgUnitTitle;
    }

    /**
     * @param territorialOrgUnitTitle Территориальное подразделение.
     */
    public void setTerritorialOrgUnitTitle(String territorialOrgUnitTitle)
    {
        dirty(_territorialOrgUnitTitle, territorialOrgUnitTitle);
        _territorialOrgUnitTitle = territorialOrgUnitTitle;
    }

    /**
     * @return Направление подготовки (специальности) ОУ.
     */
    @Length(max=255)
    public String getEducationLevelsHighSchoolTitle()
    {
        return _educationLevelsHighSchoolTitle;
    }

    /**
     * @param educationLevelsHighSchoolTitle Направление подготовки (специальности) ОУ.
     */
    public void setEducationLevelsHighSchoolTitle(String educationLevelsHighSchoolTitle)
    {
        dirty(_educationLevelsHighSchoolTitle, educationLevelsHighSchoolTitle);
        _educationLevelsHighSchoolTitle = educationLevelsHighSchoolTitle;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTechTitle()
    {
        return _developTechTitle;
    }

    /**
     * @param developTechTitle Технология освоения.
     */
    public void setDevelopTechTitle(String developTechTitle)
    {
        dirty(_developTechTitle, developTechTitle);
        _developTechTitle = developTechTitle;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriodTitle()
    {
        return _developPeriodTitle;
    }

    /**
     * @param developPeriodTitle Срок освоения.
     */
    public void setDevelopPeriodTitle(String developPeriodTitle)
    {
        dirty(_developPeriodTitle, developPeriodTitle);
        _developPeriodTitle = developPeriodTitle;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormTitle()
    {
        return _developFormTitle;
    }

    /**
     * @param developFormTitle Форма освоения.
     */
    public void setDevelopFormTitle(String developFormTitle)
    {
        dirty(_developFormTitle, developFormTitle);
        _developFormTitle = developFormTitle;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle Квалификация.
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return Направление подготовки (специальность) приема.
     */
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return Учитывать иностранных граждан.
     */
    public Boolean getIncludeForeignPerson()
    {
        return _includeForeignPerson;
    }

    /**
     * @param includeForeignPerson Учитывать иностранных граждан.
     */
    public void setIncludeForeignPerson(Boolean includeForeignPerson)
    {
        dirty(_includeForeignPerson, includeForeignPerson);
        _includeForeignPerson = includeForeignPerson;
    }

    /**
     * @return Выводить абитуриентов, имеющих льготы.
     */
    public Boolean getIncludeEntrantWithBenefit()
    {
        return _includeEntrantWithBenefit;
    }

    /**
     * @param includeEntrantWithBenefit Выводить абитуриентов, имеющих льготы.
     */
    public void setIncludeEntrantWithBenefit(Boolean includeEntrantWithBenefit)
    {
        dirty(_includeEntrantWithBenefit, includeEntrantWithBenefit);
        _includeEntrantWithBenefit = includeEntrantWithBenefit;
    }

    /**
     * @return Выводить абитуриентов, поступающих по целевому приему.
     */
    public Boolean getIncludeEntrantTargetAdmission()
    {
        return _includeEntrantTargetAdmission;
    }

    /**
     * @param includeEntrantTargetAdmission Выводить абитуриентов, поступающих по целевому приему.
     */
    public void setIncludeEntrantTargetAdmission(Boolean includeEntrantTargetAdmission)
    {
        dirty(_includeEntrantTargetAdmission, includeEntrantTargetAdmission);
        _includeEntrantTargetAdmission = includeEntrantTargetAdmission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEntrantOriginalsVerificationVar2ReportGen)
        {
            setContent(((FefuEntrantOriginalsVerificationVar2Report)another).getContent());
            setFormingDate(((FefuEntrantOriginalsVerificationVar2Report)another).getFormingDate());
            setEnrollmentCampaign(((FefuEntrantOriginalsVerificationVar2Report)another).getEnrollmentCampaign());
            setDateFrom(((FefuEntrantOriginalsVerificationVar2Report)another).getDateFrom());
            setDateTo(((FefuEntrantOriginalsVerificationVar2Report)another).getDateTo());
            setCompensationType(((FefuEntrantOriginalsVerificationVar2Report)another).getCompensationType());
            setTechnicCommissionTitle(((FefuEntrantOriginalsVerificationVar2Report)another).getTechnicCommissionTitle());
            setStudentCategoryTitle(((FefuEntrantOriginalsVerificationVar2Report)another).getStudentCategoryTitle());
            setOrderByOriginals(((FefuEntrantOriginalsVerificationVar2Report)another).isOrderByOriginals());
            setOnlyWithOriginals(((FefuEntrantOriginalsVerificationVar2Report)another).isOnlyWithOriginals());
            setAllEnrollmentDirections(((FefuEntrantOriginalsVerificationVar2Report)another).isAllEnrollmentDirections());
            setFormativeOrgUnitTitle(((FefuEntrantOriginalsVerificationVar2Report)another).getFormativeOrgUnitTitle());
            setTerritorialOrgUnitTitle(((FefuEntrantOriginalsVerificationVar2Report)another).getTerritorialOrgUnitTitle());
            setEducationLevelsHighSchoolTitle(((FefuEntrantOriginalsVerificationVar2Report)another).getEducationLevelsHighSchoolTitle());
            setDevelopTechTitle(((FefuEntrantOriginalsVerificationVar2Report)another).getDevelopTechTitle());
            setDevelopPeriodTitle(((FefuEntrantOriginalsVerificationVar2Report)another).getDevelopPeriodTitle());
            setDevelopFormTitle(((FefuEntrantOriginalsVerificationVar2Report)another).getDevelopFormTitle());
            setDevelopConditionTitle(((FefuEntrantOriginalsVerificationVar2Report)another).getDevelopConditionTitle());
            setQualificationTitle(((FefuEntrantOriginalsVerificationVar2Report)another).getQualificationTitle());
            setEnrollmentDirection(((FefuEntrantOriginalsVerificationVar2Report)another).getEnrollmentDirection());
            setIncludeForeignPerson(((FefuEntrantOriginalsVerificationVar2Report)another).getIncludeForeignPerson());
            setIncludeEntrantWithBenefit(((FefuEntrantOriginalsVerificationVar2Report)another).getIncludeEntrantWithBenefit());
            setIncludeEntrantTargetAdmission(((FefuEntrantOriginalsVerificationVar2Report)another).getIncludeEntrantTargetAdmission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEntrantOriginalsVerificationVar2ReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEntrantOriginalsVerificationVar2Report.class;
        }

        public T newInstance()
        {
            return (T) new FefuEntrantOriginalsVerificationVar2Report();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "compensationType":
                    return obj.getCompensationType();
                case "technicCommissionTitle":
                    return obj.getTechnicCommissionTitle();
                case "studentCategoryTitle":
                    return obj.getStudentCategoryTitle();
                case "orderByOriginals":
                    return obj.isOrderByOriginals();
                case "onlyWithOriginals":
                    return obj.isOnlyWithOriginals();
                case "allEnrollmentDirections":
                    return obj.isAllEnrollmentDirections();
                case "formativeOrgUnitTitle":
                    return obj.getFormativeOrgUnitTitle();
                case "territorialOrgUnitTitle":
                    return obj.getTerritorialOrgUnitTitle();
                case "educationLevelsHighSchoolTitle":
                    return obj.getEducationLevelsHighSchoolTitle();
                case "developTechTitle":
                    return obj.getDevelopTechTitle();
                case "developPeriodTitle":
                    return obj.getDevelopPeriodTitle();
                case "developFormTitle":
                    return obj.getDevelopFormTitle();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "includeForeignPerson":
                    return obj.getIncludeForeignPerson();
                case "includeEntrantWithBenefit":
                    return obj.getIncludeEntrantWithBenefit();
                case "includeEntrantTargetAdmission":
                    return obj.getIncludeEntrantTargetAdmission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "technicCommissionTitle":
                    obj.setTechnicCommissionTitle((String) value);
                    return;
                case "studentCategoryTitle":
                    obj.setStudentCategoryTitle((String) value);
                    return;
                case "orderByOriginals":
                    obj.setOrderByOriginals((Boolean) value);
                    return;
                case "onlyWithOriginals":
                    obj.setOnlyWithOriginals((Boolean) value);
                    return;
                case "allEnrollmentDirections":
                    obj.setAllEnrollmentDirections((Boolean) value);
                    return;
                case "formativeOrgUnitTitle":
                    obj.setFormativeOrgUnitTitle((String) value);
                    return;
                case "territorialOrgUnitTitle":
                    obj.setTerritorialOrgUnitTitle((String) value);
                    return;
                case "educationLevelsHighSchoolTitle":
                    obj.setEducationLevelsHighSchoolTitle((String) value);
                    return;
                case "developTechTitle":
                    obj.setDevelopTechTitle((String) value);
                    return;
                case "developPeriodTitle":
                    obj.setDevelopPeriodTitle((String) value);
                    return;
                case "developFormTitle":
                    obj.setDevelopFormTitle((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "includeForeignPerson":
                    obj.setIncludeForeignPerson((Boolean) value);
                    return;
                case "includeEntrantWithBenefit":
                    obj.setIncludeEntrantWithBenefit((Boolean) value);
                    return;
                case "includeEntrantTargetAdmission":
                    obj.setIncludeEntrantTargetAdmission((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "compensationType":
                        return true;
                case "technicCommissionTitle":
                        return true;
                case "studentCategoryTitle":
                        return true;
                case "orderByOriginals":
                        return true;
                case "onlyWithOriginals":
                        return true;
                case "allEnrollmentDirections":
                        return true;
                case "formativeOrgUnitTitle":
                        return true;
                case "territorialOrgUnitTitle":
                        return true;
                case "educationLevelsHighSchoolTitle":
                        return true;
                case "developTechTitle":
                        return true;
                case "developPeriodTitle":
                        return true;
                case "developFormTitle":
                        return true;
                case "developConditionTitle":
                        return true;
                case "qualificationTitle":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "includeForeignPerson":
                        return true;
                case "includeEntrantWithBenefit":
                        return true;
                case "includeEntrantTargetAdmission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "compensationType":
                    return true;
                case "technicCommissionTitle":
                    return true;
                case "studentCategoryTitle":
                    return true;
                case "orderByOriginals":
                    return true;
                case "onlyWithOriginals":
                    return true;
                case "allEnrollmentDirections":
                    return true;
                case "formativeOrgUnitTitle":
                    return true;
                case "territorialOrgUnitTitle":
                    return true;
                case "educationLevelsHighSchoolTitle":
                    return true;
                case "developTechTitle":
                    return true;
                case "developPeriodTitle":
                    return true;
                case "developFormTitle":
                    return true;
                case "developConditionTitle":
                    return true;
                case "qualificationTitle":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "includeForeignPerson":
                    return true;
                case "includeEntrantWithBenefit":
                    return true;
                case "includeEntrantTargetAdmission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "compensationType":
                    return CompensationType.class;
                case "technicCommissionTitle":
                    return String.class;
                case "studentCategoryTitle":
                    return String.class;
                case "orderByOriginals":
                    return Boolean.class;
                case "onlyWithOriginals":
                    return Boolean.class;
                case "allEnrollmentDirections":
                    return Boolean.class;
                case "formativeOrgUnitTitle":
                    return String.class;
                case "territorialOrgUnitTitle":
                    return String.class;
                case "educationLevelsHighSchoolTitle":
                    return String.class;
                case "developTechTitle":
                    return String.class;
                case "developPeriodTitle":
                    return String.class;
                case "developFormTitle":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
                case "qualificationTitle":
                    return String.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "includeForeignPerson":
                    return Boolean.class;
                case "includeEntrantWithBenefit":
                    return Boolean.class;
                case "includeEntrantTargetAdmission":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEntrantOriginalsVerificationVar2Report> _dslPath = new Path<FefuEntrantOriginalsVerificationVar2Report>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEntrantOriginalsVerificationVar2Report");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Техническая комиссия.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getTechnicCommissionTitle()
     */
    public static PropertyPath<String> technicCommissionTitle()
    {
        return _dslPath.technicCommissionTitle();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getStudentCategoryTitle()
     */
    public static PropertyPath<String> studentCategoryTitle()
    {
        return _dslPath.studentCategoryTitle();
    }

    /**
     * @return Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#isOrderByOriginals()
     */
    public static PropertyPath<Boolean> orderByOriginals()
    {
        return _dslPath.orderByOriginals();
    }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#isOnlyWithOriginals()
     */
    public static PropertyPath<Boolean> onlyWithOriginals()
    {
        return _dslPath.onlyWithOriginals();
    }

    /**
     * @return По всем направлениям/специальностям. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#isAllEnrollmentDirections()
     */
    public static PropertyPath<Boolean> allEnrollmentDirections()
    {
        return _dslPath.allEnrollmentDirections();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getFormativeOrgUnitTitle()
     */
    public static PropertyPath<String> formativeOrgUnitTitle()
    {
        return _dslPath.formativeOrgUnitTitle();
    }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getTerritorialOrgUnitTitle()
     */
    public static PropertyPath<String> territorialOrgUnitTitle()
    {
        return _dslPath.territorialOrgUnitTitle();
    }

    /**
     * @return Направление подготовки (специальности) ОУ.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getEducationLevelsHighSchoolTitle()
     */
    public static PropertyPath<String> educationLevelsHighSchoolTitle()
    {
        return _dslPath.educationLevelsHighSchoolTitle();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDevelopTechTitle()
     */
    public static PropertyPath<String> developTechTitle()
    {
        return _dslPath.developTechTitle();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDevelopPeriodTitle()
     */
    public static PropertyPath<String> developPeriodTitle()
    {
        return _dslPath.developPeriodTitle();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDevelopFormTitle()
     */
    public static PropertyPath<String> developFormTitle()
    {
        return _dslPath.developFormTitle();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return Учитывать иностранных граждан.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getIncludeForeignPerson()
     */
    public static PropertyPath<Boolean> includeForeignPerson()
    {
        return _dslPath.includeForeignPerson();
    }

    /**
     * @return Выводить абитуриентов, имеющих льготы.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getIncludeEntrantWithBenefit()
     */
    public static PropertyPath<Boolean> includeEntrantWithBenefit()
    {
        return _dslPath.includeEntrantWithBenefit();
    }

    /**
     * @return Выводить абитуриентов, поступающих по целевому приему.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getIncludeEntrantTargetAdmission()
     */
    public static PropertyPath<Boolean> includeEntrantTargetAdmission()
    {
        return _dslPath.includeEntrantTargetAdmission();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getPeriodTitle()
     */
    public static SupportedPropertyPath<String> periodTitle()
    {
        return _dslPath.periodTitle();
    }

    public static class Path<E extends FefuEntrantOriginalsVerificationVar2Report> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _technicCommissionTitle;
        private PropertyPath<String> _studentCategoryTitle;
        private PropertyPath<Boolean> _orderByOriginals;
        private PropertyPath<Boolean> _onlyWithOriginals;
        private PropertyPath<Boolean> _allEnrollmentDirections;
        private PropertyPath<String> _formativeOrgUnitTitle;
        private PropertyPath<String> _territorialOrgUnitTitle;
        private PropertyPath<String> _educationLevelsHighSchoolTitle;
        private PropertyPath<String> _developTechTitle;
        private PropertyPath<String> _developPeriodTitle;
        private PropertyPath<String> _developFormTitle;
        private PropertyPath<String> _developConditionTitle;
        private PropertyPath<String> _qualificationTitle;
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private PropertyPath<Boolean> _includeForeignPerson;
        private PropertyPath<Boolean> _includeEntrantWithBenefit;
        private PropertyPath<Boolean> _includeEntrantTargetAdmission;
        private SupportedPropertyPath<String> _periodTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(FefuEntrantOriginalsVerificationVar2ReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(FefuEntrantOriginalsVerificationVar2ReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(FefuEntrantOriginalsVerificationVar2ReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Техническая комиссия.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getTechnicCommissionTitle()
     */
        public PropertyPath<String> technicCommissionTitle()
        {
            if(_technicCommissionTitle == null )
                _technicCommissionTitle = new PropertyPath<String>(FefuEntrantOriginalsVerificationVar2ReportGen.P_TECHNIC_COMMISSION_TITLE, this);
            return _technicCommissionTitle;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getStudentCategoryTitle()
     */
        public PropertyPath<String> studentCategoryTitle()
        {
            if(_studentCategoryTitle == null )
                _studentCategoryTitle = new PropertyPath<String>(FefuEntrantOriginalsVerificationVar2ReportGen.P_STUDENT_CATEGORY_TITLE, this);
            return _studentCategoryTitle;
        }

    /**
     * @return Выделить абитуриентов с оригиналами документов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#isOrderByOriginals()
     */
        public PropertyPath<Boolean> orderByOriginals()
        {
            if(_orderByOriginals == null )
                _orderByOriginals = new PropertyPath<Boolean>(FefuEntrantOriginalsVerificationVar2ReportGen.P_ORDER_BY_ORIGINALS, this);
            return _orderByOriginals;
        }

    /**
     * @return Не включать абитуриентов без оригиналов документов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#isOnlyWithOriginals()
     */
        public PropertyPath<Boolean> onlyWithOriginals()
        {
            if(_onlyWithOriginals == null )
                _onlyWithOriginals = new PropertyPath<Boolean>(FefuEntrantOriginalsVerificationVar2ReportGen.P_ONLY_WITH_ORIGINALS, this);
            return _onlyWithOriginals;
        }

    /**
     * @return По всем направлениям/специальностям. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#isAllEnrollmentDirections()
     */
        public PropertyPath<Boolean> allEnrollmentDirections()
        {
            if(_allEnrollmentDirections == null )
                _allEnrollmentDirections = new PropertyPath<Boolean>(FefuEntrantOriginalsVerificationVar2ReportGen.P_ALL_ENROLLMENT_DIRECTIONS, this);
            return _allEnrollmentDirections;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getFormativeOrgUnitTitle()
     */
        public PropertyPath<String> formativeOrgUnitTitle()
        {
            if(_formativeOrgUnitTitle == null )
                _formativeOrgUnitTitle = new PropertyPath<String>(FefuEntrantOriginalsVerificationVar2ReportGen.P_FORMATIVE_ORG_UNIT_TITLE, this);
            return _formativeOrgUnitTitle;
        }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getTerritorialOrgUnitTitle()
     */
        public PropertyPath<String> territorialOrgUnitTitle()
        {
            if(_territorialOrgUnitTitle == null )
                _territorialOrgUnitTitle = new PropertyPath<String>(FefuEntrantOriginalsVerificationVar2ReportGen.P_TERRITORIAL_ORG_UNIT_TITLE, this);
            return _territorialOrgUnitTitle;
        }

    /**
     * @return Направление подготовки (специальности) ОУ.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getEducationLevelsHighSchoolTitle()
     */
        public PropertyPath<String> educationLevelsHighSchoolTitle()
        {
            if(_educationLevelsHighSchoolTitle == null )
                _educationLevelsHighSchoolTitle = new PropertyPath<String>(FefuEntrantOriginalsVerificationVar2ReportGen.P_EDUCATION_LEVELS_HIGH_SCHOOL_TITLE, this);
            return _educationLevelsHighSchoolTitle;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDevelopTechTitle()
     */
        public PropertyPath<String> developTechTitle()
        {
            if(_developTechTitle == null )
                _developTechTitle = new PropertyPath<String>(FefuEntrantOriginalsVerificationVar2ReportGen.P_DEVELOP_TECH_TITLE, this);
            return _developTechTitle;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDevelopPeriodTitle()
     */
        public PropertyPath<String> developPeriodTitle()
        {
            if(_developPeriodTitle == null )
                _developPeriodTitle = new PropertyPath<String>(FefuEntrantOriginalsVerificationVar2ReportGen.P_DEVELOP_PERIOD_TITLE, this);
            return _developPeriodTitle;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDevelopFormTitle()
     */
        public PropertyPath<String> developFormTitle()
        {
            if(_developFormTitle == null )
                _developFormTitle = new PropertyPath<String>(FefuEntrantOriginalsVerificationVar2ReportGen.P_DEVELOP_FORM_TITLE, this);
            return _developFormTitle;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(FefuEntrantOriginalsVerificationVar2ReportGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(FefuEntrantOriginalsVerificationVar2ReportGen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return Учитывать иностранных граждан.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getIncludeForeignPerson()
     */
        public PropertyPath<Boolean> includeForeignPerson()
        {
            if(_includeForeignPerson == null )
                _includeForeignPerson = new PropertyPath<Boolean>(FefuEntrantOriginalsVerificationVar2ReportGen.P_INCLUDE_FOREIGN_PERSON, this);
            return _includeForeignPerson;
        }

    /**
     * @return Выводить абитуриентов, имеющих льготы.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getIncludeEntrantWithBenefit()
     */
        public PropertyPath<Boolean> includeEntrantWithBenefit()
        {
            if(_includeEntrantWithBenefit == null )
                _includeEntrantWithBenefit = new PropertyPath<Boolean>(FefuEntrantOriginalsVerificationVar2ReportGen.P_INCLUDE_ENTRANT_WITH_BENEFIT, this);
            return _includeEntrantWithBenefit;
        }

    /**
     * @return Выводить абитуриентов, поступающих по целевому приему.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getIncludeEntrantTargetAdmission()
     */
        public PropertyPath<Boolean> includeEntrantTargetAdmission()
        {
            if(_includeEntrantTargetAdmission == null )
                _includeEntrantTargetAdmission = new PropertyPath<Boolean>(FefuEntrantOriginalsVerificationVar2ReportGen.P_INCLUDE_ENTRANT_TARGET_ADMISSION, this);
            return _includeEntrantTargetAdmission;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report#getPeriodTitle()
     */
        public SupportedPropertyPath<String> periodTitle()
        {
            if(_periodTitle == null )
                _periodTitle = new SupportedPropertyPath<String>(FefuEntrantOriginalsVerificationVar2ReportGen.P_PERIOD_TITLE, this);
            return _periodTitle;
        }

        public Class getEntityClass()
        {
            return FefuEntrantOriginalsVerificationVar2Report.class;
        }

        public String getEntityName()
        {
            return "fefuEntrantOriginalsVerificationVar2Report";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getPeriodTitle();
}
