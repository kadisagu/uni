/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18.ParagraphAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.CustomStateUtil;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.FefuExtractPrintFormManager;
import ru.tandemservice.unifefu.entity.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuOrderContingentStuDPOListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setDpoProgramModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult<FefuAdditionalProfessionalEducationProgram> findValues(String filter)
            {
                DQLSelectBuilder builder = createSelectBuilder(filter, null);

                List<FefuAdditionalProfessionalEducationProgram> list = builder.createStatement(getSession()).list();
                return new ListResult<>(list, list.size());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return get(FefuAdditionalProfessionalEducationProgram.class, (Long)primaryKey);
            }

            private DQLSelectBuilder createSelectBuilder(String filter, Object o)
            {
                OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(FefuAdditionalProfessionalEducationProgram.class, "ape")
                        .predicate(DQLPredicateType.distinct)
                        .where(or(eq(property("ape", FefuAdditionalProfessionalEducationProgram.formativeOrgUnit()), value(orgUnit)),
                                  eq(property("ape", FefuAdditionalProfessionalEducationProgram.territorialOrgUnit()), value(orgUnit))))
                        .column(property("ape"));


                if (o != null)
                {
                    if (o instanceof Long)
                        builder.where(eq(property("ape", FefuAdditionalProfessionalEducationProgram.id()), commonValue(o, PropertyType.LONG)));
                    else if (o instanceof Collection)
                        builder.where(in(property("ape", FefuAdditionalProfessionalEducationProgram.id()), (Collection) o));
                }

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("ape", FefuAdditionalProfessionalEducationProgram.title()), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property("ape", FefuAdditionalProfessionalEducationProgram.title()), OrderDirection.asc);

                return builder;
            }


        });

        model.setStudentStatusModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                return get(StudentStatus.class, (Long) primaryKey);
            }

            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(getCatalogItemList(StudentStatus.class));
            }
        });

        model.setNewStudentCustomStateCIModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                return get(StudentCustomStateCI.class, (Long) primaryKey);
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = createBuilder(filter);
                return new ListResult<>(builder.createStatement(getSession()).list(), 50);
            }

            private DQLSelectBuilder createBuilder(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateCI.class, "st").column(property("st"))
                        .order(property(StudentCustomStateCI.title().fromAlias("st")));

                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("st", StudentCustomStateCI.P_TITLE), value(CoreStringUtils.escapeLike(filter, true))));

                return builder;
            }
        });

        final FefuOrderContingentStuDPOListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            FefuAdditionalProfessionalEducationProgram program = new DQLSelectBuilder().fromEntity(FefuAdditionalProfessionalEducationProgramForStudent.class, "apes")
                    .column(property(FefuAdditionalProfessionalEducationProgramForStudent.program().fromAlias("apes")))
                    .where(eq(property("apes", FefuAdditionalProfessionalEducationProgramForStudent.student()), value(firstExtract.getEntity())))
                    .createStatement(getSession()).uniqueResult();

            model.setDpoProgram(program);
            model.setCourse(firstExtract.getEntity().getCourse());
            model.setStudentStatusNew(firstExtract.getStudentStatusNew());
            model.setStudentCustomStateNew(firstExtract.getStudentCustomStateNew());
        }
        model.setCanAddTemplate(!model.isEditForm() && model.isParagraphOnlyOneInTheOrder());


    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();
        builder.add(MQExpression.or(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().formativeOrgUnit(), orgUnit), MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().territorialOrgUnit(), orgUnit)));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().additional(), true));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.studentCategory().code(), StudentCategoryCodes.STUDENT_CATEGORY_DPP));

        List<Long> studIds = new DQLSelectBuilder().fromEntity(FefuAdditionalProfessionalEducationProgramForStudent.class, "apes")
                .column(property("apes", FefuAdditionalProfessionalEducationProgramForStudent.student().id()))
                .where(eq(property("apes",FefuAdditionalProfessionalEducationProgramForStudent.program()), value(model.getDpoProgram()))).createStatement(getSession()).list();
        builder.add(MQExpression.in(STUDENT_ALIAS, Student.id(), studIds));

        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.course(), model.getCourse()));

        if (model.getStudentCustomStateCIs() != null && !model.getStudentCustomStateCIs().isEmpty())
        {
            CustomStateUtil.addCustomStatesFilter(builder, STUDENT_ALIAS, model.getStudentCustomStateCIs());
        }
    }

    @Override
    protected FefuOrderContingentStuDPOListExtract createNewInstance(Model model)
    {
        return new FefuOrderContingentStuDPOListExtract();
    }

    @Override
    protected void fillExtract(FefuOrderContingentStuDPOListExtract extract, Student student, Model model)
    {
        extract.setStudentStatusNew(model.getStudentStatusNew());
        extract.setStudentCustomStateNew(model.getStudentCustomStateNew());
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        AbstractStudentOrder order = model.getParagraph().getOrder();
        FefuExtractPrintFormManager.instance().dao().saveOrUpdatePrintForm(order, model.getUploadFileOrder());
    }

    @Override
    protected void appendStudentsWithGroup(MQBuilder builder)
    {
        /* Нужны студенты и без группы*/
    }

}