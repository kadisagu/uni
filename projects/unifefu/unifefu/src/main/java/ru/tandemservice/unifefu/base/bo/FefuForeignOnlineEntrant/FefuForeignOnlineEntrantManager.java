/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.logic.FefuForeignOnlineEntrantDAO;
import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.logic.FefuForeignOnlineEntrantDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.logic.IFefuForeignOnlineEntrantDAO;

/**
 * @author nvankov
 * @since 4/2/13
 */
@Configuration
public class FefuForeignOnlineEntrantManager extends BusinessObjectManager
{
    // UniecScriptItem
    public static final String TEMPLATE_PRINT_REQUEST_RU = "fefuForeignOnlineEntrantRequestRu";
    public static final String TEMPLATE_PRINT_REQUEST_EN = "fefuForeignOnlineEntrantRequestEn";

    public static FefuForeignOnlineEntrantManager instance()
    {
        return instance(FefuForeignOnlineEntrantManager.class);
    }

    @Bean
    public IFefuForeignOnlineEntrantDAO dao()
    {
        return new FefuForeignOnlineEntrantDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler fefuForeignOnlineEntrantDSHandler()
    {
        return new FefuForeignOnlineEntrantDSHandler(getName());
    }
}