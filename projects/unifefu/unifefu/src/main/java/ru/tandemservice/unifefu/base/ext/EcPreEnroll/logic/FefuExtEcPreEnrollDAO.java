/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EcPreEnroll.logic;

import ru.tandemservice.uniec.base.bo.EcPreEnroll.logic.EcPreEnrollDao;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.Collection;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 02.06.2015
 */
public class FefuExtEcPreEnrollDAO extends EcPreEnrollDao
{

    @Override
    public Map<Long, Integer> fillIndividualProgressMap(EnrollmentCampaign campaign, Collection<RequestedEnrollmentDirection> directions)
    {
        if (!campaign.isUseIndividualProgressInAmountMark())
            return super.fillIndividualProgressMap(campaign, directions);

        return getIndividualProgressByDirectionMap(directions);
    }
}