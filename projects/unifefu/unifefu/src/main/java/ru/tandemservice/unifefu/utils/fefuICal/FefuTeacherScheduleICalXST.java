/* $Id$ */
package ru.tandemservice.unifefu.utils.fefuICal;

import java.util.List;

/**
 * @author nvankov
 * @since 9/16/13
 */
public class FefuTeacherScheduleICalXST
{
    private List<FefuTeacherScheduleVEventICalXST> _events;

    public List<FefuTeacherScheduleVEventICalXST> getEvents()
    {
        return _events;
    }

    public void setEvents(List<FefuTeacherScheduleVEventICalXST> events)
    {
        _events = events;
    }
}
