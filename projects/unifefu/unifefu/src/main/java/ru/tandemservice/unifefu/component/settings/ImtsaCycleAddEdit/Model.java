/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.settings.ImtsaCycleAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"),
    @Bind(key = "gos2", binding = "gos2")
})
public class Model
{
    private Long _id;
    private boolean _gos2;
    private FefuImtsaCyclePlanStructureRel _cyclePlanStructureRel;
    private ISelectModel _cycleModel;

    public Long getId(){ return _id; }
    public void setId(Long id){ _id = id; }

    public boolean isGos2(){ return _gos2; }
    public void setGos2(boolean gos2g){ _gos2 = gos2g; }

    public FefuImtsaCyclePlanStructureRel getCyclePlanStructureRel(){ return _cyclePlanStructureRel; }
    public void setCyclePlanStructureRel(FefuImtsaCyclePlanStructureRel cyclePlanStructureRel){ _cyclePlanStructureRel = cyclePlanStructureRel; }

    public ISelectModel getCycleModel(){ return _cycleModel; }
    public void setCycleModel(ISelectModel cycleModel){ _cycleModel = cycleModel; }


    public boolean isEditForm()
    {
        return _id != null;
    }
}