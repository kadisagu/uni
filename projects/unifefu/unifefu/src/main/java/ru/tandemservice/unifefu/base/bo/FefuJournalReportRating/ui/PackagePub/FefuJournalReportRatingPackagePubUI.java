/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuJournalReportRating.ui.PackagePub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.FefuSendingRatingPackage;
import ru.tandemservice.unifefu.tapestry.formatter.FefuCrazyXmlFormatter;

/**
 * @author DMITRY KNYAZEV
 * @since 29.04.2014
 */
@State
		({
				 @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "packageId", required = true)
		 })
public class FefuJournalReportRatingPackagePubUI extends UIPresenter
{
	private Long _packageId;

	private FefuSendingRatingPackage _packageLog;

	private String _xmlData;

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		super.onBeforeDataSourceFetch(dataSource);
		if (dataSource.getName().equals(FefuJournalReportRatingPackagePub.PACKAGE_PUB_DS))
		{
			dataSource.put(FefuJournalReportRatingPackagePub.FEFU_PACKAGE_ID, getPackageId());
		}
	}

	@Override
	public void onComponentRefresh()
	{
		setPackageLog(DataAccessServices.dao().getNotNull(FefuSendingRatingPackage.class, getPackageId()));
		setXmlData(FefuCrazyXmlFormatter.INSTANCE.format(getPackageLog().getSendingData()));
	}

	public Long getPackageId()
	{
		return _packageId;
	}

	public void setPackageId(Long packageId)
	{
		_packageId = packageId;
	}

	public String getXmlData()
	{
		return _xmlData;
	}

	public void setXmlData(String xmlData)
	{
		_xmlData = xmlData;
	}

	public FefuSendingRatingPackage getPackageLog()
	{
		return _packageLog;
	}

	public void setPackageLog(FefuSendingRatingPackage packageLog)
	{
		_packageLog = packageLog;
	}
}
