package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.unifefu.entity.FefuPerformConditionTransferCourseStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. «О выполнении условий перевода с курса на следующий курс»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuPerformConditionTransferCourseStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuPerformConditionTransferCourseStuExtract";
    public static final String ENTITY_NAME = "fefuPerformConditionTransferCourseStuExtract";
    public static final int VERSION_HASH = 1772408341;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuPerformConditionTransferCourseStuExtractGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuPerformConditionTransferCourseStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuPerformConditionTransferCourseStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuPerformConditionTransferCourseStuExtract();
        }
    }
    private static final Path<FefuPerformConditionTransferCourseStuExtract> _dslPath = new Path<FefuPerformConditionTransferCourseStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuPerformConditionTransferCourseStuExtract");
    }
            

    public static class Path<E extends FefuPerformConditionTransferCourseStuExtract> extends ModularStudentExtract.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return FefuPerformConditionTransferCourseStuExtract.class;
        }

        public String getEntityName()
        {
            return "fefuPerformConditionTransferCourseStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
