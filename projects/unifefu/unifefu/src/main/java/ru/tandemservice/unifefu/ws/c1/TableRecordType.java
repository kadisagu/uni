/**
 * TableRecordType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.c1;

public class TableRecordType  implements java.io.Serializable {
    private java.lang.String studentID;

    private java.lang.String departmentID;

    private java.lang.String postID;

    private java.util.Date studentDate;

    private java.math.BigDecimal scholarshipAmount;

    private java.lang.String calcKind;

    private java.lang.String departmentOldID;

    private java.lang.String studentNumber;

    public TableRecordType() {
    }

    public TableRecordType(
           java.lang.String studentID,
           java.lang.String departmentID,
           java.lang.String postID,
           java.util.Date studentDate,
           java.math.BigDecimal scholarshipAmount,
           java.lang.String calcKind,
           java.lang.String departmentOldID,
           java.lang.String studentNumber) {
           this.studentID = studentID;
           this.departmentID = departmentID;
           this.postID = postID;
           this.studentDate = studentDate;
           this.scholarshipAmount = scholarshipAmount;
           this.calcKind = calcKind;
           this.departmentOldID = departmentOldID;
           this.studentNumber = studentNumber;
    }


    /**
     * Gets the studentID value for this TableRecordType.
     * 
     * @return studentID
     */
    public java.lang.String getStudentID() {
        return studentID;
    }


    /**
     * Sets the studentID value for this TableRecordType.
     * 
     * @param studentID
     */
    public void setStudentID(java.lang.String studentID) {
        this.studentID = studentID;
    }


    /**
     * Gets the departmentID value for this TableRecordType.
     * 
     * @return departmentID
     */
    public java.lang.String getDepartmentID() {
        return departmentID;
    }


    /**
     * Sets the departmentID value for this TableRecordType.
     * 
     * @param departmentID
     */
    public void setDepartmentID(java.lang.String departmentID) {
        this.departmentID = departmentID;
    }


    /**
     * Gets the postID value for this TableRecordType.
     * 
     * @return postID
     */
    public java.lang.String getPostID() {
        return postID;
    }


    /**
     * Sets the postID value for this TableRecordType.
     * 
     * @param postID
     */
    public void setPostID(java.lang.String postID) {
        this.postID = postID;
    }


    /**
     * Gets the studentDate value for this TableRecordType.
     * 
     * @return studentDate
     */
    public java.util.Date getStudentDate() {
        return studentDate;
    }


    /**
     * Sets the studentDate value for this TableRecordType.
     * 
     * @param studentDate
     */
    public void setStudentDate(java.util.Date studentDate) {
        this.studentDate = studentDate;
    }


    /**
     * Gets the scholarshipAmount value for this TableRecordType.
     * 
     * @return scholarshipAmount
     */
    public java.math.BigDecimal getScholarshipAmount() {
        return scholarshipAmount;
    }


    /**
     * Sets the scholarshipAmount value for this TableRecordType.
     * 
     * @param scholarshipAmount
     */
    public void setScholarshipAmount(java.math.BigDecimal scholarshipAmount) {
        this.scholarshipAmount = scholarshipAmount;
    }


    /**
     * Gets the calcKind value for this TableRecordType.
     * 
     * @return calcKind
     */
    public java.lang.String getCalcKind() {
        return calcKind;
    }


    /**
     * Sets the calcKind value for this TableRecordType.
     * 
     * @param calcKind
     */
    public void setCalcKind(java.lang.String calcKind) {
        this.calcKind = calcKind;
    }


    /**
     * Gets the departmentOldID value for this TableRecordType.
     * 
     * @return departmentOldID
     */
    public java.lang.String getDepartmentOldID() {
        return departmentOldID;
    }


    /**
     * Sets the departmentOldID value for this TableRecordType.
     * 
     * @param departmentOldID
     */
    public void setDepartmentOldID(java.lang.String departmentOldID) {
        this.departmentOldID = departmentOldID;
    }


    /**
     * Gets the studentNumber value for this TableRecordType.
     * 
     * @return studentNumber
     */
    public java.lang.String getStudentNumber() {
        return studentNumber;
    }


    /**
     * Sets the studentNumber value for this TableRecordType.
     * 
     * @param studentNumber
     */
    public void setStudentNumber(java.lang.String studentNumber) {
        this.studentNumber = studentNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TableRecordType)) return false;
        TableRecordType other = (TableRecordType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.studentID==null && other.getStudentID()==null) || 
             (this.studentID!=null &&
              this.studentID.equals(other.getStudentID()))) &&
            ((this.departmentID==null && other.getDepartmentID()==null) || 
             (this.departmentID!=null &&
              this.departmentID.equals(other.getDepartmentID()))) &&
            ((this.postID==null && other.getPostID()==null) || 
             (this.postID!=null &&
              this.postID.equals(other.getPostID()))) &&
            ((this.studentDate==null && other.getStudentDate()==null) || 
             (this.studentDate!=null &&
              this.studentDate.equals(other.getStudentDate()))) &&
            ((this.scholarshipAmount==null && other.getScholarshipAmount()==null) || 
             (this.scholarshipAmount!=null &&
              this.scholarshipAmount.equals(other.getScholarshipAmount()))) &&
            ((this.calcKind==null && other.getCalcKind()==null) || 
             (this.calcKind!=null &&
              this.calcKind.equals(other.getCalcKind()))) &&
            ((this.departmentOldID==null && other.getDepartmentOldID()==null) || 
             (this.departmentOldID!=null &&
              this.departmentOldID.equals(other.getDepartmentOldID()))) &&
            ((this.studentNumber==null && other.getStudentNumber()==null) || 
             (this.studentNumber!=null &&
              this.studentNumber.equals(other.getStudentNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStudentID() != null) {
            _hashCode += getStudentID().hashCode();
        }
        if (getDepartmentID() != null) {
            _hashCode += getDepartmentID().hashCode();
        }
        if (getPostID() != null) {
            _hashCode += getPostID().hashCode();
        }
        if (getStudentDate() != null) {
            _hashCode += getStudentDate().hashCode();
        }
        if (getScholarshipAmount() != null) {
            _hashCode += getScholarshipAmount().hashCode();
        }
        if (getCalcKind() != null) {
            _hashCode += getCalcKind().hashCode();
        }
        if (getDepartmentOldID() != null) {
            _hashCode += getDepartmentOldID().hashCode();
        }
        if (getStudentNumber() != null) {
            _hashCode += getStudentNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TableRecordType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "TableRecordType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("studentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "StudentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departmentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "DepartmentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "PostID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("studentDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "StudentDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scholarshipAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "ScholarshipAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calcKind");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "CalcKind"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departmentOldID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "DepartmentOldID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("studentNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "StudentNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
