/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;
import ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRequest;
import ru.tandemservice.unifefu.entity.ws.FefuPortalNotification;
import ru.tandemservice.unifefu.utils.FefuMailSender;
import ru.tandemservice.unifefu.utils.SHAUtils;
import ru.tandemservice.unifefu.utils.SymmetricCrypto;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 31.05.2013
 */
public class FEFUPasswordRegistratorDaemonDao extends UniBaseDao implements IFEFUPasswordRegistratorDaemonDao
{
    private String NSI_DESTINATION_URL = ApplicationRuntime.getProperty("fefu.idm.service.connectionstr");
    private String NSI_SHA_KEY = ApplicationRuntime.getProperty("fefu.idm.service.shakey1");

    public static final SyncDaemon DAEMON = new SyncDaemon(FEFUPasswordRegistratorDaemonDao.class.getName(), 3, IFEFUPasswordRegistratorDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            final IFEFUPasswordRegistratorDaemonDao dao = IFEFUPasswordRegistratorDaemonDao.instance.get();

            try
            {
                dao.doSendRequestsToNSI();
            } catch (final Throwable t)
            {
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            }

        }
    };

    protected Session lock4update()
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, IFEFUPasswordRegistratorDaemonDao.GLOBAL_DAEMON_LOCK);
        return session;
    }

    @Override
    public void updateOnlineEntrantExtPassword(Long userId, String passwordEnrypted)
    {
        OnlineEntrant olEntrant = new DQLSelectBuilder().fromEntity(OnlineEntrant.class, "e")
                .where(eq(property(OnlineEntrant.userId().fromAlias("e")), value(userId)))
                .where(eq(property(OnlineEntrant.enrollmentCampaign().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "c")
                        .column(DQLFunctions.max(property(EnrollmentCampaign.id().fromAlias("c"))))
                        .buildQuery()))
                .createStatement(getSession()).uniqueResult();

        if (null != olEntrant)
        {
            OnlineEntrantFefuExt olExt = get(OnlineEntrantFefuExt.class, OnlineEntrantFefuExt.onlineEntrant().id(), olEntrant.getId());
            if (null == olExt)
            {
                olExt = new OnlineEntrantFefuExt();
                olExt.setOnlineEntrant(olEntrant);
            }
            olExt.setPasswordEncrypted(passwordEnrypted);
            getSession().saveOrUpdate(olExt);
        } else
        {
            FefuForeignOnlineEntrant ffolEntrant = new DQLSelectBuilder().fromEntity(FefuForeignOnlineEntrant.class, "e")
                    .where(eq(property(FefuForeignOnlineEntrant.userId().fromAlias("e")), value(userId)))
                    .where(eq(property(FefuForeignOnlineEntrant.enrollmentCampaign().id().fromAlias("e")), new DQLSelectBuilder().fromEntity(EnrollmentCampaign.class, "c")
                            .column(DQLFunctions.max(property(EnrollmentCampaign.id().fromAlias("c"))))
                            .buildQuery()))
                    .createStatement(getSession()).uniqueResult();

            if (null != ffolEntrant)
            {
                ffolEntrant.setPasswordEncrypted(passwordEnrypted);
                update(ffolEntrant);
            }
        }
    }

    @Override
    public void doRegisterNSIRequest(FefuNsiIds nsiId, Person person, String password)
    {
        String passwordEncrypted = new SymmetricCrypto().crypt(password);
        FefuNsiRequest request = new FefuNsiRequest();
        request.setPasswordEncrypted(passwordEncrypted);
        request.setNsiGuid(nsiId);
        request.setPerson(person);
        request.setSent(false);
        save(request);
    }

    @Override
    public void doSendRequestsToNSI()
    {
        if (null == NSI_DESTINATION_URL || null == NSI_SHA_KEY) return;

        List<FefuNsiRequest> requests = new DQLSelectBuilder()
                .fromEntity(FefuNsiRequest.class, "r").column("r")
                .where(eq(property(FefuNsiRequest.sent().fromAlias("r")), value(Boolean.FALSE)))
                .createStatement(getSession()).list();

        for (FefuNsiRequest request : requests)
        {
            doSendSingleRequest(request);
        }
    }

    @Override
    public void updateNSIRequest(String guid, String nsiLogin, String errorMsg)
    {
        FefuNsiIds nsiId = get(FefuNsiIds.class, FefuNsiIds.guid(), guid);
        if (null != nsiId)
        {
            List<FefuPortalNotification> notifications = new ArrayList<>();

            List<FefuNsiRequest> requests = new DQLSelectBuilder()
                    .fromEntity(FefuNsiRequest.class, "r").column("r")
                    .where(eq(property(FefuNsiRequest.nsiGuid().guid().fromAlias("r")), value(guid)))
                    .where(isNull(property(FefuNsiRequest.loginFromNSI().fromAlias("r"))))
                    .createStatement(getSession()).list();

            for (FefuNsiRequest request : requests)
            {
                if (null == nsiLogin) request.setSent(false);
                request.setLoginFromNSI(nsiLogin);
                request.setResponse(errorMsg);
                update(request);

                if (null == errorMsg && !StringUtils.isEmpty(nsiLogin))
                {
                    PersonManager.instance().dao().updatePersonLogin(request.getPerson().getId(), nsiLogin, null);

                    FefuPortalNotification notif = new FefuPortalNotification();
                    notif.setSubject("Добро пожаловать в Ваш Личный кабинет");
                    notif.setPersonGuid(request.getNsiGuid());
                    notif.setPerson(request.getPerson());
                    notif.setText("Уважаемый абитуриент! Добро пожаловать в Ваш Личный кабинет, в котором будут размещаться сообщения от Приемной комиссии ДВФУ, ежедневный рейтинг в отборе по направлению, расписание экзаменов и собеседований, результаты зачисления.");
                    notifications.add(notif);

                    System.out.println(notif.getPersonGuid().getGuid() + " - " + (null != notif.getPerson() ? notif.getPerson().getFullFio() : "Fictive Person"));

                    PersonContactData contactData = request.getPerson().getContactData();
                    if (null != contactData.getEmail())
                    {
                        String text = "Уважаемый Абитуриент!\n" +
                                "\n" +
                                "Сообщаем Вам, что в среде представительства ДВФУ в Интернете для Вас подготовлен Личный кабинет, в котором Вы сможете воспользоваться информационными и коммуникационными сервисами по вопросам набора в ДВФУ.\n" +
                                "Для входа в Личный кабинет необходимо вызвать страницу с адресом «https://ip.dvfu.ru/wps/portal» и ввести следующие реквизиты:\n" +
                                "-\t«ИД пользователя» - " + nsiLogin + ";\n" +
                                "-\t«Пароль»:\n" +
                                "\t\tа) если вы подали заявление гражданина РФ на поступление через сервис ДВФУ по приему заявлений абитуриентов через Интернет, то необходимо ввести пароль, указанный Вами при регистрации;\n" +
                                "\t\tб) если Вы подали заявление на пункте приема заявлений абитуриентов приемной комиссии, либо Вы заполнили анкету иностранного гражданина через Сервис ДВФУ по приему заявлений абитуриентов через Интернет, то пароль будет сформирован для вас автоматически и будет состоять из вашей транслитерированной фамилии (мягкий и твердый знак опущены) + номер вашего документа, удостоверяющего личность + дата Вашего рождения без разделителей в формате 'ддммгггг' (в случае, если первые два параметра суммарно по длине меньше восьми символов), например, Ivanov1234, или Yan123411121992.\n" +
                                "Надеемся, что использование Личного кабинета в среде представительства ДВФУ в сети Интернет будет для Вас полезным.\n" +
                                "\n" +
                                "По всем вопросам, связанным с работой Личного кабинета, Вы можете обратиться в Приемную комиссию ДВФУ по ее контактам, указанным на сайте.\n" +
                                "\n" +
                                "Пожалуйста, не отвечайте на данное письмо.\n" +
                                "\n" +
                                "Приемная комиссия ДВФУ.";

                        try
                        {
                            FefuMailSender.sendEmail(contactData.getEmail(), "Доступ в личный кабинет абитуриента", text);
                        } catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                    }
                }
            }

            IFEFUPortalNotificationsDaemonDao.instance.get().doRegisterPortalNotifications(notifications);
        }
    }

    @Override
    public void doResendRequest(FefuNsiRequest idmRequest)
    {
        doSendSingleRequest(idmRequest);
    }

    @Override
    public void doResendRequests(List<Long> requestIds)
    {
        if (null == NSI_DESTINATION_URL || null == NSI_SHA_KEY) return;

        List<FefuNsiRequest> requests = new DQLSelectBuilder()
                .fromEntity(FefuNsiRequest.class, "r").column("r")
                .where(in(property(FefuNsiRequest.id().fromAlias("r")), requestIds))
                .createStatement(getSession()).list();

        for (FefuNsiRequest request : requests)
            doSendSingleRequest(request);
    }

    private void doSendSingleRequest(FefuNsiRequest request)
    {
        if (null == NSI_DESTINATION_URL || null == NSI_SHA_KEY) return;

        System.out.println(DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date()) + " System action for IDM integration testing has started...");

        AddressBase address = request.getPerson().getAddress();
        String email = request.getPerson().getContactData().getEmail();
        String password = new SymmetricCrypto().decrypt(request.getPasswordEncrypted());
        String lastName = request.getPerson().getIdentityCard().getLastName();
        String firstName = request.getPerson().getIdentityCard().getFirstName();
        String middleName = request.getPerson().getIdentityCard().getMiddleName();
        String guid = request.getNsiGuid().getGuid();
        if (null == middleName) middleName = "";
        String hashStr = null;

        // Если мыло не указано, то генерится случайное мыло по общему шаблону, поскольку для AD ДВФУ этот параметр обязателен
        if (StringUtils.isEmpty(email))
        {
            email = "";
        }

        try
        {
            hashStr = SHAUtils.getSHAChecksum(NSI_SHA_KEY, email, password, lastName, firstName, middleName, guid);
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }

        if (null != hashStr)
        {
            System.out.println("email\t= " + email);
            System.out.println("password\t= " + password);
            System.out.println("lastName\t= " + lastName);
            System.out.println("firstName\t= " + firstName);
            System.out.println("secondName\t= " + middleName);
            System.out.println("guidPerson\t= " + guid);
            System.out.println("shaKey\t= " + NSI_SHA_KEY);
            System.out.println("hashStr\t= " + new String(hashStr));

            try
            {
                HttpClient httpClient = new HttpClient();
                PostMethod post = new PostMethod();
                post.addRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                post.setPath(NSI_DESTINATION_URL);

                post.setParameter("email", email);
                post.setParameter("password", password);
                post.setParameter("firstName", firstName);
                post.setParameter("secondName", middleName);
                post.setParameter("lastName", lastName);
                post.setParameter("guidPerson", guid);
                post.setParameter("hashStr", new String(hashStr));

                httpClient.executeMethod(post);
                System.out.println("System action for IDM integration testing was finished. The response from IDM is:");
                System.out.println(post.getResponseBodyAsString());

                if (null != post.getResponseBodyAsString() && post.getResponseBodyAsString().length() > 10)
                    return;

                request.setSent(true);
                update(request);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        } else
        {
            System.out.println("Can not calculate SHA-1 checksum.");
        }
    }
}