/* $Id$ */
package ru.tandemservice.unifefu.component.report.FefuEntrantIncomingListVar2.Pub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report;

/**
 * @author Igor Belanov
 * @since 25.07.2016
 */
@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    private FefuEntrantIncomingListVar2Report _report = new FefuEntrantIncomingListVar2Report();

    public FefuEntrantIncomingListVar2Report getReport()
    {
        return _report;
    }

    public void setReport(FefuEntrantIncomingListVar2Report report)
    {
        _report = report;
    }
}
