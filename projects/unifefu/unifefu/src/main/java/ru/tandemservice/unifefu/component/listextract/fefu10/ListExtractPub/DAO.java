/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu10.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract;

/**
 * @author nvankov
 * @since 11/21/13
 */
public class DAO extends AbstractListExtractPubDAO<FullStateMaintenanceEnrollmentStuListExtract, Model> implements IDAO
{
}
