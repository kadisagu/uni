/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.catalog.educationLevelsHighSchool.EducationLevelsHighSchoolPub;

/**
 * @author Dmitry Seleznev
 * @since 21.02.2014
 */
public class Controller extends ru.tandemservice.uni.component.catalog.educationLevelsHighSchool.EducationLevelsHighSchoolPub.Controller
{
//убрал изменения в соответствии с DEV-4844
//    @Override
//    public void onClickEditItem(IBusinessComponent context)
//    {
//        context.createDefaultChildRegion(new ComponentActivator(CommonManager.catalogComponentProvider().getAddEditComponent(getModel(context).getItemClass()), new ParametersMap()
//                .add(DefaultCatalogAddEditModel.CATALOG_ITEM_ID, context.getListenerParameter())
//        ));
//    }
}