package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.gen.FefuImtsaXmlGen;

import java.io.InputStream;

/**
 * XML файл ИМЦА
 */
public class FefuImtsaXml extends FefuImtsaXmlGen
{
    public FefuImtsaXml()
    {

    }

    public FefuImtsaXml(EppEduPlanVersionBlock block, String fileName, String encoding, byte[] xml)
    {
        this(block, fileName, encoding, xml, 1);
    }

    public FefuImtsaXml(EppEduPlanVersionBlock block, String fileName, String encoding, byte[] xml, int number)
    {
        this.setBlock(block);
        this.setFileName(fileName);
        this.setEncoding(encoding);
        this.setXml(xml);
        this.setNumber(number);
    }
}