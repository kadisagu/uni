/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d104.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;

import java.util.Calendar;

/**
 * @author Alexey Lopatin
 * @since 29.09.2013
 */
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        Calendar oldBookDate = Calendar.getInstance();
        oldBookDate.set(model.getStudent().getEntranceYear(), Calendar.SEPTEMBER, 1);

        model.setOldBookNumber(model.getStudent().getBookNumber());
        model.setOldBookDate(oldBookDate.getTime());
    }
}
