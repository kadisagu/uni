/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.student.FefuForeignStudentDataEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 14.02.2013
 */
@Input(keys = "studentId", bindings = "studentId")
public class Model
{
    private Long _studentId;
    private StudentFefuExt _studentFefuExt;
    private DataWrapper _baseEdu;             // Базовое образование
    private List<DataWrapper> _baseEduList;

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public StudentFefuExt getStudentFefuExt()
    {
        return _studentFefuExt;
    }

    public void setStudentFefuExt(StudentFefuExt studentFefuExt)
    {
        _studentFefuExt = studentFefuExt;
    }

    public DataWrapper getBaseEdu() {
        return _baseEdu;
    }

    public void setBaseEdu(DataWrapper baseEdu) {
        _baseEdu = baseEdu;
    }

    public List<DataWrapper> getBaseEduList() {
        return _baseEduList;
    }

    public void setBaseEduList(List<DataWrapper> baseEduList) {
        _baseEduList = baseEduList;
    }
}