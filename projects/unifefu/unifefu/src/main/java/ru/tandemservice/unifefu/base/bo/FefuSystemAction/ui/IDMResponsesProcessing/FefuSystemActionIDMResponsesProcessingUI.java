/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.IDMResponsesProcessing;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.Person.ui.View.PersonView;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic.IdmRequestDSHandler;
import ru.tandemservice.unifefu.dao.daemon.IFEFUPasswordRegistratorDaemonDao;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRequest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 18.06.2013
 */
public class FefuSystemActionIDMResponsesProcessingUI extends UIPresenter
{
    private Person _person;
    private String _guid;

    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuSystemActionIDMResponsesProcessing.IDM_REQUEST_DS.equals(dataSource.getName()))
        {
            dataSource.put(IdmRequestDSHandler.PERSON_FILTER, _person);
            dataSource.put(IdmRequestDSHandler.GUID_FILTER, _guid);
        }
    }

    // Getters & Setters

    public Person getPerson()
    {
        return _person;
    }

    public void setPerson(Person person)
    {
        _person = person;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }


    // Listeners

    public void onClickView()
    {
        FefuNsiRequest request = DataAccessServices.dao().get(FefuNsiRequest.class, getListenerParameterAsLong());
        if(null != request && null != request.getPerson())
            _uiActivation.asDesktopRoot(PersonView.class)
                    .parameter(UIPresenter.PUBLISHER_ID, request.getPerson().getId())
                    .activate();
    }

    public void onClickRetryRegistration()
    {
        FefuNsiRequest request = DataAccessServices.dao().getNotNull(FefuNsiRequest.class, getListenerParameterAsLong());
        IFEFUPasswordRegistratorDaemonDao.instance.get().doResendRequest(request);
    }

    public void onClickRetryRegistrations()
    {
        Collection<IEntity> selected = ((CheckboxColumn) ((PageableSearchListDataSource) _uiConfig.getDataSource(FefuSystemActionIDMResponsesProcessing.IDM_REQUEST_DS)).getLegacyDataSource().getColumn("checkbox")).getSelectedObjects();
        if (selected.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы один запрос к IDM.");

        List<Long> ids = new ArrayList<>();
        for (IEntity item : selected)
            ids.add(item.getId());

        IFEFUPasswordRegistratorDaemonDao.instance.get().doResendRequests(ids);
    }

    @Override
    public void clearSettings()
    {
        super.clearSettings();
        _person = null;
        _guid = null;
    }
}