/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d103.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;

import java.util.Date;

/**
 * @author Alexey Lopatin
 * @since 29.09.2013
 */
public class Model extends DocumentAddBaseModel
{
    private String _enrollmentOrderNumber;
    private Date _enrollmentOrderDate;
    private Integer _finishYear;

    public String getEnrollmentOrderNumber()
    {
        return _enrollmentOrderNumber;
    }

    public void setEnrollmentOrderNumber(String enrollmentOrderNumber)
    {
        _enrollmentOrderNumber = enrollmentOrderNumber;
    }

    public Date getEnrollmentOrderDate()
    {
        return _enrollmentOrderDate;
    }

    public void setEnrollmentOrderDate(Date enrollmentOrderDate)
    {
        _enrollmentOrderDate = enrollmentOrderDate;
    }

    public Integer getFinishYear()
    {
        return _finishYear;
    }

    public void setFinishYear(Integer finishYear)
    {
        _finishYear = finishYear;
    }
}
