/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.FefuPpsSessionListDocumentManager;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.logic.FefuSessionDocumentSlotWrapper;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.ui.Edit.FefuPpsSessionListDocumentEdit;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.ui.Edit.FefuPpsSessionListDocumentEditUI;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

import java.util.HashMap;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 26.08.2014
 */
@State({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "documentId", required = true)})
public class FefuPpsSessionListDocumentPubUI extends UIPresenter
{
	private Long _documentId;
	private Person _person;
	private Student _student;
	private SessionListDocument _card;
	private boolean _useRating; // TODO it not used?

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (dataSource.getName().equals(FefuPpsSessionListDocumentPub.DOCUMENT_SLOT_DS))
		{
			List<FefuSessionDocumentSlotWrapper> list = FefuPpsSessionListDocumentManager.instance().dao().getSessionDocumentSlotWrapperList(getPerson(), getCard());
			dataSource.put(FefuPpsSessionListDocumentPub.DOCUMENT_SLOT_LIST, list);
		}
	}

	@Override
	public void onComponentRefresh()
	{
		_useRating = ISessionBrsDao.instance.get().getSettings().isUseCurrentRatingForListDocument();
	}

	//handlers
	public void onClickMark()
	{
		_uiActivation.asRegionDialog(FefuPpsSessionListDocumentEdit.class)
				.parameters(new HashMap<String, Object>()
				{{
						put(PublisherActivator.PUBLISHER_ID_KEY, getCard());
						put(FefuPpsSessionListDocumentEditUI.STUDENT_BIND, getStudent());
					}})
				.activate();
	}

	public boolean isUseRating()
	{
		return _useRating;
	}

	//getters/setters
	public Long getDocumentId()
	{
		return _documentId;
	}

	public void setDocumentId(Long documentId)
	{
		_documentId = documentId;
	}

	public SessionListDocument getCard()
	{
		if (_card == null)
			_card = DataAccessServices.dao().getNotNull(getDocumentId());
		return _card;
	}

	public String getTitle()
	{
		return "Индивидуальная ведомость №" + getCard().getNumber();
	}

	public String getStudentTitle()
	{
		return getStudent().getPerson().getFullFio();
	}

	public Person getPerson()
	{
		if (_person == null)
			_person = PersonManager.instance().dao().getPerson(getUserContext().getPrincipalContext());
		return _person;
	}

	public Student getStudent()
	{
		if (_student == null)
			_student = FefuPpsSessionListDocumentManager.instance().dao().getStudent(getCard());
		return _student;
	}

	public Long getStudentId()
	{
		return getStudent().getId();
	}
}
