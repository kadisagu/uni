package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup;
import ru.tandemservice.unifefu.entity.catalog.FefuThematicGroupStuExtractTypes;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь типа приказа с тематической группой
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuStudentExtractType2ThematicGroupGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup";
    public static final String ENTITY_NAME = "fefuStudentExtractType2ThematicGroup";
    public static final int VERSION_HASH = -1912362565;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String L_THEMATIC_GROUP = "thematicGroup";

    private StudentExtractType _type;     // Тип приказа
    private FefuThematicGroupStuExtractTypes _thematicGroup;     // Тематическая группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     */
    @NotNull
    public StudentExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип приказа. Свойство не может быть null.
     */
    public void setType(StudentExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Тематическая группа. Свойство не может быть null.
     */
    @NotNull
    public FefuThematicGroupStuExtractTypes getThematicGroup()
    {
        return _thematicGroup;
    }

    /**
     * @param thematicGroup Тематическая группа. Свойство не может быть null.
     */
    public void setThematicGroup(FefuThematicGroupStuExtractTypes thematicGroup)
    {
        dirty(_thematicGroup, thematicGroup);
        _thematicGroup = thematicGroup;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuStudentExtractType2ThematicGroupGen)
        {
            setType(((FefuStudentExtractType2ThematicGroup)another).getType());
            setThematicGroup(((FefuStudentExtractType2ThematicGroup)another).getThematicGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuStudentExtractType2ThematicGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuStudentExtractType2ThematicGroup.class;
        }

        public T newInstance()
        {
            return (T) new FefuStudentExtractType2ThematicGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "thematicGroup":
                    return obj.getThematicGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((StudentExtractType) value);
                    return;
                case "thematicGroup":
                    obj.setThematicGroup((FefuThematicGroupStuExtractTypes) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "thematicGroup":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "thematicGroup":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return StudentExtractType.class;
                case "thematicGroup":
                    return FefuThematicGroupStuExtractTypes.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuStudentExtractType2ThematicGroup> _dslPath = new Path<FefuStudentExtractType2ThematicGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuStudentExtractType2ThematicGroup");
    }
            

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup#getType()
     */
    public static StudentExtractType.Path<StudentExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Тематическая группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup#getThematicGroup()
     */
    public static FefuThematicGroupStuExtractTypes.Path<FefuThematicGroupStuExtractTypes> thematicGroup()
    {
        return _dslPath.thematicGroup();
    }

    public static class Path<E extends FefuStudentExtractType2ThematicGroup> extends EntityPath<E>
    {
        private StudentExtractType.Path<StudentExtractType> _type;
        private FefuThematicGroupStuExtractTypes.Path<FefuThematicGroupStuExtractTypes> _thematicGroup;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup#getType()
     */
        public StudentExtractType.Path<StudentExtractType> type()
        {
            if(_type == null )
                _type = new StudentExtractType.Path<StudentExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Тематическая группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup#getThematicGroup()
     */
        public FefuThematicGroupStuExtractTypes.Path<FefuThematicGroupStuExtractTypes> thematicGroup()
        {
            if(_thematicGroup == null )
                _thematicGroup = new FefuThematicGroupStuExtractTypes.Path<FefuThematicGroupStuExtractTypes>(L_THEMATIC_GROUP, this);
            return _thematicGroup;
        }

        public Class getEntityClass()
        {
            return FefuStudentExtractType2ThematicGroup.class;
        }

        public String getEntityName()
        {
            return "fefuStudentExtractType2ThematicGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
