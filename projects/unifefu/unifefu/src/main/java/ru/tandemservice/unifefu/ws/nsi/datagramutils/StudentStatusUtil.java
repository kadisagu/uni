/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.StudentStatusType;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 07.05.2014
 */
public class StudentStatusUtil extends SimpleNsiCatalogUtil<StudentStatusType, StudentStatus>
{
    public static final String STUDENT_STATUS_ACTIVE_NSI_FIELD = "StudentStatusActive";
    public static final String STUDENT_STATUS_USED_NSI_FIELD = "StudentStatusUsedinSystem";
    public static final String STUDENT_STATUS_PRIORITY_NSI_FIELD = "StudentStatusPriority";

    @Override
    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            super.getNsiFields();
            NSI_FIELDS.add(STUDENT_STATUS_ACTIVE_NSI_FIELD);
            NSI_FIELDS.add(STUDENT_STATUS_USED_NSI_FIELD);
            NSI_FIELDS.add(STUDENT_STATUS_PRIORITY_NSI_FIELD);
        }
        return NSI_FIELDS;
    }

    @Override
    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            super.getEntityFields();
            ENTITY_FIELDS.add(StudentStatus.active().s());
            ENTITY_FIELDS.add(StudentStatus.usedInSystem().s());
            ENTITY_FIELDS.add(StudentStatus.priority().s());
        }
        return ENTITY_FIELDS;
    }

    @Override
    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            super.getNsiToObFieldsMap();
            NSI_TO_OB_FIELDS_MAP.put(STUDENT_STATUS_ACTIVE_NSI_FIELD, StudentStatus.active().s());
            NSI_TO_OB_FIELDS_MAP.put(STUDENT_STATUS_USED_NSI_FIELD, StudentStatus.usedInSystem().s());
            NSI_TO_OB_FIELDS_MAP.put(STUDENT_STATUS_PRIORITY_NSI_FIELD, StudentStatus.priority().s());
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    @Override
    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            super.getObToNsiFieldsMap();
            OB_TO_NSI_FIELDS_MAP.put(StudentStatus.active().s(), STUDENT_STATUS_ACTIVE_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(StudentStatus.usedInSystem().s(), STUDENT_STATUS_USED_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(StudentStatus.priority().s(), STUDENT_STATUS_PRIORITY_NSI_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    public long getNsiStudentStatusActiveHash(StudentStatusType nsiEntity)
    {
        return 0; //null == nsiEntity.getStudentStatusActive() ? 0 : ("1".equals(nsiEntity.getStudentStatusActive()) ? Boolean.TRUE : Boolean.FALSE).hashCode();
    }

    public long getNsiStudentStatusUsedHash(StudentStatusType nsiEntity)
    {
        return 0; //null == nsiEntity.getStudentStatusUsedinSystem() ? 0 : ("1".equals(nsiEntity.getStudentStatusUsedinSystem()) ? Boolean.TRUE : Boolean.FALSE).hashCode();
    }

    public long getNsiStudentStatusPriorityHash(StudentStatusType nsiEntity)
    {
        return 0; //null == nsiEntity.getStudentStatusPriority() ? 0 : (nsiEntity.getStudentStatusPriority()).hashCode();
    }

    @Override
    public boolean isFieldEmpty(StudentStatusType nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case STUDENT_STATUS_ACTIVE_NSI_FIELD:
                return false;
            case STUDENT_STATUS_USED_NSI_FIELD:
                return false;
            case STUDENT_STATUS_PRIORITY_NSI_FIELD:
                return false;
            default:
                return false;
        }
    }

    @Override
    public long getNsiFieldHash(StudentStatusType nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case STUDENT_STATUS_ACTIVE_NSI_FIELD:
                return getNsiStudentStatusActiveHash(nsiEntity);
            case STUDENT_STATUS_USED_NSI_FIELD:
                return getNsiStudentStatusUsedHash(nsiEntity);
            case STUDENT_STATUS_PRIORITY_NSI_FIELD:
                return getNsiStudentStatusPriorityHash(nsiEntity);
            default:
                return super.getNsiFieldHash(nsiEntity, fieldName, caseInsensitive);
        }
    }

    public long getStudentStatusActiveHash(StudentStatus entity)
    {
        return 0; //(entity.isActive() ? Boolean.TRUE : Boolean.FALSE).hashCode();
    }

    public long getStudentStatusUsedHash(StudentStatus entity)
    {
        return 0; //(entity.isUsedInSystem() ? Boolean.TRUE : Boolean.FALSE).hashCode();
    }

    public long getStudentStatusPriorityHash(StudentStatus entity)
    {
        return 0; //String.valueOf(entity.getPriority()).hashCode();
    }

    @Override
    public long getEntityFieldHash(StudentStatus entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case StudentStatus.P_ACTIVE:
                return getStudentStatusActiveHash(entity);
            case StudentStatus.P_USED_IN_SYSTEM:
                return getStudentStatusUsedHash(entity);
            case StudentStatus.P_PRIORITY:
                return getStudentStatusPriorityHash(entity);
            default:
                return super.getEntityFieldHash(entity, nsiId, fieldName, caseInsensitive);
        }
    }
}