package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x2_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPrikazyOther

		// создано свойство reason
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("reason_id", DBType.LONG));

		}

		// создано свойство reasonComment
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("reasoncomment_p", DBType.createVarchar(255)));

		}

		// создано свойство course
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("course_id", DBType.LONG));

		}

		// создано свойство term
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("term_id", DBType.LONG));

		}

		// создано свойство institute
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("institute_p", DBType.createVarchar(255)));

		}

		// создано свойство faculty
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("faculty_p", DBType.createVarchar(255)));

		}

		// создано свойство speciality
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("speciality_p", DBType.createVarchar(255)));

		}

		// создано свойство profile
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("profile_p", DBType.createVarchar(255)));

		}

		// создано свойство dopEduLevel
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("dopedulevel_p", DBType.createVarchar(255)));

		}

		// создано свойство encouragelty
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("encouragelty_p", DBType.createVarchar(255)));

		}

		// создано свойство encourageltyEndDate
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("encourageltyenddate_p", DBType.DATE));

		}

		// создано свойство grantSize
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("grantsize_p", DBType.createVarchar(255)));

		}

		// создано свойство grantCategory
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("grantcategory_p", DBType.createVarchar(255)));

		}

		// создано свойство practiceType
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("practicetype_p", DBType.createVarchar(255)));

		}

		// создано свойство practicePlace
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("practiceplace_p", DBType.createVarchar(255)));

		}

		// создано свойство practiceOwnershipType
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("practiceownershiptype_p", DBType.createVarchar(255)));

		}

		// создано свойство practiceSettlement
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("practicesettlement_p", DBType.createVarchar(255)));

		}

		// создано свойство practicePeriod
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("practiceperiod_p", DBType.createVarchar(255)));

		}

		// создано свойство practiceSupervisor
		{
			// создать колонку
			tool.createColumn("mdbviewprikazyother_t", new DBColumn("practicesupervisor_p", DBType.createVarchar(255)));

		}
        {

            tool.createColumn("mdbviewprikazyother_t", new DBColumn("comment_p", DBType.createVarchar(1024)));
        }


    }
}