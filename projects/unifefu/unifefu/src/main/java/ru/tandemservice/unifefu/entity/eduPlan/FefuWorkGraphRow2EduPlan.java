package ru.tandemservice.unifefu.entity.eduPlan;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unifefu.entity.eduPlan.gen.*;

/**
 * ГУП (Связь строки с учебным планом)
 */
public class FefuWorkGraphRow2EduPlan extends FefuWorkGraphRow2EduPlanGen
{
    public FefuWorkGraphRow2EduPlan()
    {

    }

    public FefuWorkGraphRow2EduPlan(FefuWorkGraphRow workGraphRow, EppEduPlanVersion version)
    {
        this.setRow(workGraphRow);
        this.setEduPlanVersion(version);
    }
}