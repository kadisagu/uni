/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.user;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unifefu.ws.blackboard.BBContextHelper;
import ru.tandemservice.unifefu.ws.blackboard.user.gen.UserFilter;
import ru.tandemservice.unifefu.ws.blackboard.user.gen.UserVO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 13.03.2014
 */
public class BBUserUtils
{
    /**
     * Получение юзера по гуиду.
     *
     * @param guid GUID персоны
     * @return юзер или null, если не найден
     */
    @Nullable
    public static UserVO findUser(@NotNull final String guid)
    {
        List<UserVO> list = BBContextHelper.getInstance().doUserRequest((userWSPortType, of) -> {
            UserFilter userFilter = of.createUserFilter();
            userFilter.setFilterType(UserWSConstants.GET_USER_BY_BATCH_ID_WITH_AVAILABILITY);
            userFilter.getBatchId().add(guid);
            return userWSPortType.getUser(userFilter);
        });

        if (list != null && !list.isEmpty())
        {
            if (list.size() > 1)
                throw new ApplicationException("В Blackboard найдено более одного пользователя с GUID = " + guid + ".");

            return list.get(0);
        }

        return null;
    }

    /**
     * Получение юзера по имени пользователя в BB
     *
     * @param bbUserName имя пользователя в BB
     * @return объект {@link ru.tandemservice.unifefu.ws.blackboard.user.gen.UserVO} или {@code null}, если юзер не найден
     */
    @Nullable
    public static UserVO findUserByUserName(@NotNull final String bbUserName)
    {
        List<UserVO> list = BBContextHelper.getInstance().doUserRequest((userWSPortType, of) -> {
            UserFilter userFilter = of.createUserFilter();
            userFilter.setFilterType(UserWSConstants.GET_USER_BY_NAME_WITH_AVAILABILITY);
            userFilter.getName().add(bbUserName);
            return userWSPortType.getUser(userFilter);
        });

        if (list != null && !list.isEmpty())
        {
            if (list.size() > 1)
                throw new ApplicationException("В Blackboard найдено более одного пользователя с userId = " + bbUserName + ".");

            return list.get(0);
        }

        return null;
    }

    /**
     * Получение пользователя Blackboard по идентификатору персоны
     *
     * @param personId идентификатор персоны
     * @return юзер ВВ, если найден, или null
     */
    @Nullable
    public static UserVO findUser(@NotNull Long personId)
    {
        String guid = IFefuNsiSyncDAO.instance.get().getNsiIdGuid(personId);
        return guid != null ? BBUserUtils.findUser(guid) : null;
    }

    /**
     * Получение пользователей ВВ для набора преподавателей
     *
     * @param ppsEntryList набор преподавателей
     * @return мапа [препод - юзер ВВ]
     */
    @NotNull
    public static Map<PpsEntry, UserVO> findUsersNotNull(@NotNull Collection<PpsEntry> ppsEntryList)
    {
        Map<PpsEntry, UserVO> resultMap = new HashMap<>(ppsEntryList.size());
        for (PpsEntry ppsEntry : ppsEntryList)
        {
            String ppsPersonGuid = IFefuNsiSyncDAO.instance.get().getNsiIdGuid(ppsEntry.getPerson().getId());

            if (ppsPersonGuid == null)
                throw new ApplicationException("GUID (идентификатор НСИ) для " + ppsEntry.getPerson().getFullFio() + " не найден.");

            // Получаем юзера для преподавателя
            UserVO userVO = BBUserUtils.findUser(ppsPersonGuid);
            if (userVO == null)
                throw new ApplicationException("Пользователь " + ppsEntry.getFullFio() + " (" + ppsPersonGuid + ") в системе Blackboard не зарегистрирован.");

            resultMap.put(ppsEntry, userVO);
        }
        return resultMap;
    }
}