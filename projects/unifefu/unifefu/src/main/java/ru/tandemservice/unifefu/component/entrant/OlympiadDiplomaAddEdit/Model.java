/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.OlympiadDiplomaAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unifefu.entity.OlympiadDiplomaFefuExt;

/**
 * @author Nikolay Fedorovskih
 * @since 03.06.2013
 */
public class Model extends ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.Model
{
    private OlympiadDiplomaFefuExt olympiadDiplomaFefuExt;
    private ISelectModel olymdiadModel;

    public OlympiadDiplomaFefuExt getOlympiadDiplomaFefuExt()
    {
        return olympiadDiplomaFefuExt;
    }

    public void setOlympiadDiplomaFefuExt(OlympiadDiplomaFefuExt olympiadDiplomaFefuExt)
    {
        this.olympiadDiplomaFefuExt = olympiadDiplomaFefuExt;
    }

    public ISelectModel getOlymdiadModel()
    {
        return olymdiadModel;
    }

    public void setOlymdiadModel(ISelectModel olymdiadModel)
    {
        this.olymdiadModel = olymdiadModel;
    }
}