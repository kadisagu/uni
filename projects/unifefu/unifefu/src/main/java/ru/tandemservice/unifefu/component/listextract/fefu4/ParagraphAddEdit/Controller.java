/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractListParagraphAddEditController<FefuAcadGrantAssignStuEnrolmentListExtract, IDAO, Model>
{
    public static final String GRANT_COLUMN = "grantSize";
    public static final String GROUP_MANAGER_BONUS_COLUMN = "groupManagerBonusSize";

    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn(GRANT_COLUMN, "Размер стипендии, руб.").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn(GROUP_MANAGER_BONUS_COLUMN, "Размер выплаты старосте, руб.").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Заполнить по образцу", "clone", "onClickCopyGrantSize"));
        dataSource.setOrder("status", OrderDirection.asc);

        Model model = getModel(component);
        if (model.isEditForm())
        {
            IValueMapHolder grantHolder = (IValueMapHolder) dataSource.getColumn(GRANT_COLUMN);
            Map<Long, Long> grantMap = (null == grantHolder ? Collections.emptyMap() : grantHolder.getValueMap());

            IValueMapHolder groupManagerBonusHolder = (IValueMapHolder) dataSource.getColumn(GROUP_MANAGER_BONUS_COLUMN);
            Map<Long, Long> groupManagerBonusMap = (null == grantHolder ? Collections.emptyMap() : groupManagerBonusHolder.getValueMap());

            for (FefuAcadGrantAssignStuEnrolmentListExtract grantExtract : (List<FefuAcadGrantAssignStuEnrolmentListExtract>) model.getParagraph().getExtractList())
            {
                grantMap.put(grantExtract.getEntity().getId(), grantExtract.getGrantSizeInRuble());
                groupManagerBonusMap.put(grantExtract.getEntity().getId(), grantExtract.getGroupManagerBonusSizeInRuble());
            }
        }
    }

    public void onClickCopyGrantSize(IBusinessComponent component)
    {
        Model model = getModel(component);
        List<Long> groupManagerIds = model.getGroupManagerIds();
        DynamicListDataSource dataSource = model.getDataSource();

        IValueMapHolder grantHolder = (IValueMapHolder) model.getDataSource().getColumn(GRANT_COLUMN);
        Map<Long, Long> grantMap = (null == grantHolder ? Collections.emptyMap() : grantHolder.getValueMap());

        IValueMapHolder groupManagerBonusHolder = (IValueMapHolder) model.getDataSource().getColumn(GROUP_MANAGER_BONUS_COLUMN);
        Map<Long, Long> groupManagerBonusMap = (null == grantHolder ? Collections.emptyMap() : groupManagerBonusHolder.getValueMap());

        Long currentGrant = grantMap.get((Long)component.getListenerParameter());
        Long currentGroupManagerBonus = groupManagerBonusMap.get(component.getListenerParameter());

        for (ViewWrapper<Student> wrapper : (List<ViewWrapper<Student>>) dataSource.getSelectedEntities())
        {
            Long id = wrapper.getId();

            if (grantMap.get(id) == null)
                grantMap.put(id, currentGrant);

            if (groupManagerBonusMap.get(id) == null && groupManagerIds.contains(id))
                groupManagerBonusMap.put(id, currentGroupManagerBonus);
        }

        model.getDataSource().refresh();
    }
}