package ru.tandemservice.unifefu.entity.ws;

import ru.tandemservice.unifefu.entity.ws.gen.*;

/**
 * Расширение сотрудника для ДВФУ
 */
public class FefuEmployeePostExt extends FefuEmployeePostExtGen
{
    public Double getRateAsDouble()
    {
        return null == getRate() ? null : getRate() / 10000.0;
    }
}