/* $Id$ */
package ru.tandemservice.unifefu.brs.base;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 30.05.2014
 */
public abstract class BaseFefuBrsReportParams
{
    private EppYearPart _yearPart;
    private OrgUnit _orgUnit;
    private OrgUnit _responsibilityOrgUnit;

    public EppYearPart getYearPart()
    {
        return _yearPart;
    }

    public void setYearPart(EppYearPart yearPart)
    {
        _yearPart = yearPart;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public OrgUnit getResponsibilityOrgUnit()
    {
        return _responsibilityOrgUnit;
    }

    public void setResponsibilityOrgUnit(OrgUnit responsibilityOrgUnit)
    {
        _responsibilityOrgUnit = responsibilityOrgUnit;
    }
}