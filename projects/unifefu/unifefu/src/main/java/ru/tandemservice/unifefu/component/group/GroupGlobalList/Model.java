/*$Id$*/
package ru.tandemservice.unifefu.component.group.GroupGlobalList;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 08.10.2014
 */
public class Model extends ru.tandemservice.uni.component.group.GroupGlobalList.Model
{
	public static final String P_VACATION_STUDENT_COUNT = "vacationStudentCount";

    public static final String FEFU_EDU_LEVEL_TYPE_FILTER_NAME = "educationLevelTypeList";

	private List<YesNoWrapper> _archivalModel;
    private ISelectModel _educationLevelTypeListModel;

    public ISelectModel getEducationLevelTypeListModel()
    {
        return _educationLevelTypeListModel;
    }

    public void setEducationLevelTypeListModel(ISelectModel educationLevelTypeListModel)
    {
        _educationLevelTypeListModel = educationLevelTypeListModel;
    }

    public List<YesNoWrapper> getArchivalModel()
	{
		return _archivalModel;
	}

	public void setArchivalModel(List<YesNoWrapper> archivalModel)
	{
		_archivalModel = archivalModel;
	}
}
