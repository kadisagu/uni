/* $Id:$ */
package ru.tandemservice.unifefu.events.nsi;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiContactType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact;
import ru.tandemservice.unifefu.ws.nsi.reactor.ContactTypeReactor;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry V. Seleznev
 * @since 09.01.2015
 */
public class AddressListener extends ParamTransactionCompleteListener<Boolean>
{
    public static Set<Long> JUST_CHANGED_CONTACT_ID_SET = new HashSet<>();
    public static Map<Long, String> NEW_ADDR_GUID_INHERITANCE_MAP = new HashMap<>();

    @SuppressWarnings("unchecked")
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, AddressBase.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, AddressBase.class, this);
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        Set<Long> preProcessedContactIdList = new HashSet<>();
        for (Long id : params)
        {
            if (!JUST_CHANGED_CONTACT_ID_SET.contains(id)) preProcessedContactIdList.add(id);
            JUST_CHANGED_CONTACT_ID_SET.remove(id);
        }

        if (preProcessedContactIdList.isEmpty()) return true;


        Map<Long, Person> addrIdToPersonMap = new HashMap<>();

        List<Object[]> icardAddrList = new DQLSelectBuilder().fromEntity(IdentityCard.class, "e")
                .column(property(IdentityCard.address().id().fromAlias("e"))).column(property(IdentityCard.person().fromAlias("e")))
                .where(in(property(IdentityCard.address().id().fromAlias("e")), preProcessedContactIdList)).createStatement(session).list();

        Set<Long> icardAddrIdSet = new HashSet<>();

        for (Object[] item : icardAddrList)
        {
            icardAddrIdSet.add((Long) item[0]);
            addrIdToPersonMap.put((Long) item[0], (Person) item[1]);
        }

        List<Object[]> personAddrList = new DQLSelectBuilder().fromEntity(Person.class, "e")
                .column(property(Person.address().id().fromAlias("e"))).column(property("e"))
                .where(in(property(Person.address().id().fromAlias("e")), preProcessedContactIdList)).createStatement(session).list();

        for (Object[] item : personAddrList)
        {
            addrIdToPersonMap.put((Long) item[0], (Person) item[1]);
        }

        List<FefuNsiPersonContact> contactDataList = new DQLSelectBuilder().fromEntity(FefuNsiPersonContact.class, "e").column(property("e"))
                .where(in(property(FefuNsiPersonContact.address().id().fromAlias("e")), preProcessedContactIdList)).createStatement(session).list();

        Map<Long, List<FefuNsiPersonContact>> contactsMap = new HashMap<>();
        for (FefuNsiPersonContact contact : contactDataList)
        {
            List<FefuNsiPersonContact> contactsList = contactsMap.get(contact.getAddress().getId());
            if (null == contactsList) contactsList = new ArrayList<>();
            contactsList.add(contact);
            contactsMap.put(contact.getAddress().getId(), contactsList);
        }

        List<AddressBase> addrList = new DQLSelectBuilder().fromEntity(AddressBase.class, "e").column(property("e"))
                .where(in(property(AddressBase.id().fromAlias("e")), preProcessedContactIdList)).createStatement(session).list();

        for (AddressBase addr : addrList)
        {
            Person person = addrIdToPersonMap.get(addr.getId());
            if (null != person)
            {
                List<FefuNsiPersonContact> contactList = contactsMap.get(addr.getId());
                if (null == contactList) contactList = Collections.singletonList(new FefuNsiPersonContact());

                for (FefuNsiPersonContact contact : contactList)
                {

                    FefuNsiContactType conType = DataAccessServices.dao().get(FefuNsiContactType.class, FefuNsiContactType.userCode(), icardAddrIdSet.contains(addr.getId()) ? ContactTypeReactor.CONTACT_KIND_CODE_REG_ADDR : ContactTypeReactor.CONTACT_KIND_CODE_FACT_ADDR);
                    if (null != conType)
                    {
                        contact.setPerson(person);
                        contact.setType(conType);
                        contact.setAddress(addr);
                        contact.setTypeStr("Адрес");

                        if (addr instanceof AddressRu)
                        {
                            AddressRu addrRu = (AddressRu) addr;
                            contact.setField1(addrRu.getInheritedPostCode());

                            if (null != addrRu.getSettlement())
                            {
                                List<AddressItem> addressItemList = new ArrayList<>();
                                AddressItem parentAddrItem = addrRu.getSettlement();
                                while (null != parentAddrItem)
                                {
                                    addressItemList.add(parentAddrItem);
                                    parentAddrItem = parentAddrItem.getParent();
                                }

                                if (addressItemList.get(0).getAddressType().isCountryside())
                                {
                                    contact.setField5(addressItemList.get(0).getTitle() + " " + addressItemList.get(0).getAddressType().getShortTitle());
                                    if (addressItemList.size() == 2)
                                    {
                                        contact.setField2(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                                    } else if (addressItemList.size() == 3)
                                    {
                                        contact.setField4(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                                        contact.setField2(addressItemList.get(2).getTitle() + " " + addressItemList.get(2).getAddressType().getShortTitle());
                                    } else if (addressItemList.size() == 4)
                                    {
                                        contact.setField4(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                                        contact.setField2(addressItemList.get(3).getTitle() + " " + addressItemList.get(3).getAddressType().getShortTitle());
                                    }
                                } else
                                {
                                    contact.setField4(addressItemList.get(0).getTitle() + " " + addressItemList.get(0).getAddressType().getShortTitle());
                                    if (addressItemList.size() == 2)
                                    {
                                        contact.setField2(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                                    } else if (addressItemList.size() == 3)
                                    {
                                        contact.setField2(addressItemList.get(2).getTitle() + " " + addressItemList.get(2).getAddressType().getShortTitle());
                                        contact.setField3(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                                    } else if (addressItemList.size() == 4)
                                    {
                                        contact.setField2(addressItemList.get(3).getTitle() + " " + addressItemList.get(3).getAddressType().getShortTitle());
                                        contact.setField3(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                                    }
                                }
                            }

                            if (null != addrRu.getStreet())
                            {
                                contact.setField6(addrRu.getStreet().getTitle() + " " + addrRu.getStreet().getAddressType().getShortTitle());
                            }

                            contact.setField7(addrRu.getHouseNumber());
                            contact.setField8(addrRu.getHouseUnitNumber());
                            contact.setField9(addrRu.getFlatNumber());
                        } else if (addr instanceof AddressInter)
                        {
                            AddressInter addrInt = (AddressInter) addr;
                            contact.setField1(null != addrInt.getCountry().getFullTitle() ? addrInt.getCountry().getFullTitle() : addrInt.getCountry().getTitle());
                            contact.setField2(addrInt.getRegion());
                            contact.setField3(addrInt.getDistrict());

                            if (null != addrInt.getSettlement())
                            {
                                contact.setField4(addrInt.getSettlement().getTitle() + " " + addrInt.getSettlement().getAddressType().getShortTitle());
                            }

                            String[] addrArr = StringUtils.split(addrInt.getAddressLocation(), ",");
                            if (null != addrArr)
                            {
                                if (addrArr.length > 0) contact.setField6(addrArr[0]);
                                if (addrArr.length > 1) contact.setField7(addrArr[1]);
                                if (addrArr.length > 2) contact.setField8(addrArr[2]);
                                if (addrArr.length > 3) contact.setField9(addrArr[3]);
                                if (addrArr.length > 4) contact.setField10(addrArr[4]);
                            }
                        } else if (addr instanceof AddressString)
                        {
                            AddressString addrStr = (AddressString) addr;
                            contact.setDescription(addrStr.getAddress());
                            String[] addrArr = StringUtils.split(addrStr.getAddress(), ",");
                            if (addrArr.length > 0) contact.setField1(addrArr[0]);
                            if (addrArr.length > 1) contact.setField2(addrArr[1]);
                            if (addrArr.length > 2) contact.setField3(addrArr[2]);
                            if (addrArr.length > 3) contact.setField4(addrArr[3]);
                            if (addrArr.length > 4) contact.setField5(addrArr[4]);
                            if (addrArr.length > 5) contact.setField6(addrArr[5]);
                            if (addrArr.length > 6) contact.setField7(addrArr[6]);
                            if (addrArr.length > 7) contact.setField8(addrArr[7]);
                            if (addrArr.length > 8) contact.setField9(addrArr[8]);
                            if (addrArr.length > 9) contact.setField10(addrArr[9]);
                        }

                        if (addr instanceof AddressRu || addr instanceof AddressInter)
                        {
                            StringBuilder descBuilder = new StringBuilder();
                            if (null != contact.getField1()) descBuilder.append(contact.getField1());
                            if (null != contact.getField2())
                                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField2());
                            if (null != contact.getField3())
                                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField3());
                            if (null != contact.getField4())
                                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField4());
                            if (null != contact.getField5())
                                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField5());
                            if (null != contact.getField6())
                                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField6());
                            if (null != contact.getField7())
                                descBuilder.append(descBuilder.length() > 0 ? ", дом № " : "дом № ").append(contact.getField7());
                            if (null != contact.getField8())
                                descBuilder.append(descBuilder.length() > 0 ? ", корп. " : "корп. ").append(contact.getField8());
                            if (null != contact.getField9())
                                descBuilder.append(descBuilder.length() > 0 ? ", кв. " : "кв. ").append(contact.getField9());
                            if (null != contact.getField10())
                                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField10());
                            contact.setDescription(descBuilder.toString());
                        }

                        if (null == StringUtils.trimToNull(contact.getDescription()))
                        {
                            contact = null;
                            continue;
                        }

                        session.saveOrUpdate(contact);

                        if (null != contact.getAddress() && NEW_ADDR_GUID_INHERITANCE_MAP.containsKey(contact.getAddress().getId()))
                        {
                            String guid = NEW_ADDR_GUID_INHERITANCE_MAP.get(contact.getAddress().getId());
                            if (null != guid)
                            {
                                new DQLUpdateBuilder(FefuNsiIds.class).set(FefuNsiIds.P_ENTITY_ID, value(contact.getId()))
                                        .where(eq(property(FefuNsiIds.guid()), value(guid)))
                                        .createStatement(session).execute();

                                NEW_ADDR_GUID_INHERITANCE_MAP.remove(contact.getAddress().getId());
                            }
                        }

                        // Вносим идентификатор сущности в сет для листенера, обрабатывающего изменения контактов НСИ, дабы избежать зацикливания листенеров
                        NsiPersonContactListener.JUST_CHANGED_CONTACT_ID_SET.add(contact.getId());
                    } else contact = null;
                }
            }
        }

        session.flush();

        return true;
    }
}