/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu15.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public class Controller  extends AbstractListParagraphAddEditController<FefuEnrollStuDPOListExtract, IDAO, Model>
{

    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа", Student.group().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", Student.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", Student.educationOrgUnit().developTech().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", Student.educationOrgUnit().developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", Student.educationOrgUnit().developPeriod().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", new String[]{Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(EDU_LVL_HS_COLUMN_TITLE, Student.educationOrgUnit().educationLevelHighSchool().configurableTitle().s()).setClickable(false));
        dataSource.setOrder("status", OrderDirection.asc);
    }
}