/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 02.12.2014
 */
public class FefuEduStdDAO extends SharedBaseDao implements IFefuEduStdDAO
{
    @Override
    public List<FefuEppStateEduStandardLaborBlocks> getEduStandardLaborBlockRows(EppStateEduStandard eduStandard, EduProgramQualification qualification)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuEppStateEduStandardLaborBlocks.class, "b")
                .where(eq(property("b", FefuEppStateEduStandardLaborBlocks.eduStandard().id()), value(eduStandard.getId())))
                .where(eq(property("b", FefuEppStateEduStandardLaborBlocks.qualification().id()), value(qualification.getId())))
                .order(property("b", FefuEppStateEduStandardLaborBlocks.block().title()));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public void recalculateBlockMinAndMaxNumbers(FefuEppStateEduStandardLaborBlocks block)
    {
        List<FefuEppStateEduStandardLaborBlocks> rootBlockList = Lists.newArrayList();
        FefuEppStateEduStandardLaborBlocks parentBlock = block.getParent();

        while (null != parentBlock)
        {
            rootBlockList.add(parentBlock);
            parentBlock = parentBlock.getParent();
        }
        if (!rootBlockList.isEmpty())
        {
            List<FefuEppStateEduStandardLaborBlocks> childBlockList = DataAccessServices.dao().getList(FefuEppStateEduStandardLaborBlocks.class, FefuEppStateEduStandardLaborBlocks.parent(), rootBlockList);

            if (!childBlockList.isEmpty())
            {
                Map<FefuEppStateEduStandardLaborBlocks, List<FefuEppStateEduStandardLaborBlocks>> childMap = Maps.newHashMap();
                parentBlock = block.getParent();

                for (FefuEppStateEduStandardLaborBlocks child : childBlockList)
                {
                    FefuEppStateEduStandardLaborBlocks parent = child.getParent();

                    if (!childMap.containsKey(parent))
                        childMap.put(parent, Lists.<FefuEppStateEduStandardLaborBlocks>newArrayList());
                    childMap.get(parent).add(child);
                }
                while (null != parentBlock)
                {
                    int minNumber = 0;
                    int maxNumber = 0;

                    for (FefuEppStateEduStandardLaborBlocks child : childMap.get(parentBlock))
                    {
                        minNumber += child.getMinNumber();
                        maxNumber += child.getMaxNumber();
                    }
                    parentBlock.setMinNumber(minNumber);
                    parentBlock.setMaxNumber(maxNumber);
                    DataAccessServices.dao().saveOrUpdate(parentBlock);

                    parentBlock = parentBlock.getParent();
                }
            }
        }
    }

    @Override
    public void saveFefuCompetence(EppStateEduStandard eduStandard, Long skillGroupId, List<FefuCompetence> fefuCompetenceList)
    {
        Integer competencesNumber = new DQLSelectBuilder().fromEntity(FefuCompetence2EppStateEduStandardRel.class, "r")
                .column(DQLFunctions.max(property("r", FefuCompetence2EppStateEduStandardRel.number())))
                .where(eq(property("r", FefuCompetence2EppStateEduStandardRel.eduStandard().id()), value(eduStandard.getId())))
                .createStatement(getSession())
                .uniqueResult();

        Integer groupNumber = new DQLSelectBuilder().fromEntity(FefuCompetence2EppStateEduStandardRel.class, "r")
                .column(DQLFunctions.max(property("r", FefuCompetence2EppStateEduStandardRel.groupNumber())))
                .where(eq(property("r", FefuCompetence2EppStateEduStandardRel.eduStandard().id()), value(eduStandard.getId())))
                .where(eq(property("r", FefuCompetence2EppStateEduStandardRel.fefuCompetence().eppSkillGroup().id()), value(skillGroupId)))
                .createStatement(getSession())
                .uniqueResult();

        if (competencesNumber == null) competencesNumber = 0;
        if (groupNumber == null) groupNumber = 0;

        for (FefuCompetence competence : fefuCompetenceList)
        {
            FefuCompetence2EppStateEduStandardRel rel = new FefuCompetence2EppStateEduStandardRel();
            rel.setEduStandard(eduStandard);
            rel.setFefuCompetence(competence);
            rel.setNumber(++competencesNumber);
            rel.setGroupNumber(++groupNumber);

            saveOrUpdate(rel);
        }
    }

    @Override
    public void doChangeCompetencePriorityDown(Long entityId)
    {
        FefuCompetence2EppStateEduStandardRel item = getNotNull(FefuCompetence2EppStateEduStandardRel.class, entityId);
        CommonManager.instance().commonPriorityDao().doChangePriorityDown(item, item.getClass(), FefuCompetence2EppStateEduStandardRel.number().s(),
                                                                          FefuCompetence2EppStateEduStandardRel.eduStandard());
        normalizeCompetenceRels(item.getEduStandard(), item.getFefuCompetence().getEppSkillGroup());
    }

    @Override
    public void doChangeCompetencePriorityUp(Long entityId)
    {
        FefuCompetence2EppStateEduStandardRel item = getNotNull(FefuCompetence2EppStateEduStandardRel.class, entityId);
        CommonManager.instance().commonPriorityDao().doChangePriorityUp(item, item.getClass(), FefuCompetence2EppStateEduStandardRel.number().s(),
                                                                        FefuCompetence2EppStateEduStandardRel.eduStandard());
        normalizeCompetenceRels(item.getEduStandard(), item.getFefuCompetence().getEppSkillGroup());
    }

    private void normalizeCompetenceRels(EppStateEduStandard eduStandard, EppSkillGroup skillGroup) {
        List<FefuCompetence2EppStateEduStandardRel> rels = new DQLSelectBuilder().fromEntity(FefuCompetence2EppStateEduStandardRel.class, "rel")
                .column(property("rel"))
                .where(eq(property("rel", FefuCompetence2EppStateEduStandardRel.eduStandard()), value(eduStandard)))
                .where(eq(property("rel", FefuCompetence2EppStateEduStandardRel.fefuCompetence().eppSkillGroup()), value(skillGroup)))
                .order(property("rel", FefuCompetence2EppStateEduStandardRel.number()))
                .createStatement(getSession()).list();

        int groupNumber = 1;
        for (FefuCompetence2EppStateEduStandardRel rel : rels) {
            rel.setGroupNumber(groupNumber);
            update(rel);
            groupNumber++;
        }
    }

    @Override
    public void doReMoveFefuCompetence(EppStateEduStandard eduStandard, EduProgramSpecialization programSpecialization)
    {
        List<FefuCompetence2EppStateEduStandardRel> relList = new DQLSelectBuilder().fromEntity(FefuCompetence2EppStateEduStandardRel.class, "r")
                .where(eq(property("r", FefuCompetence2EppStateEduStandardRel.eduStandard()), value(eduStandard)))
                .order(property("r", FefuCompetence2EppStateEduStandardRel.number()))
                .createStatement(getSession()).list();

        for (int number = 0; number < relList.size(); number++)
        {
            FefuCompetence2EppStateEduStandardRel rel = relList.get(number);
            rel.setNumber(number + 1);
            update(rel);
        }
    }

    @Override
    public Map<Long, Integer> getCompetenceNumbersMap(Long stateEduStandardId)
    {
        String alias = "rel";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(FefuCompetence2EppStateEduStandardRel.class, alias)
                .where(eq(property(alias, FefuCompetence2EppStateEduStandardRel.eduStandard().id()), value(stateEduStandardId)))
                .order(property(alias, FefuCompetence2EppStateEduStandardRel.fefuCompetence().eppSkillGroup().shortTitle()))
                .order(property(alias, FefuCompetence2EppStateEduStandardRel.number()));

        Map<Long, Integer> result = new HashMap<>();
        dql.createStatement(getSession()).<FefuCompetence2EppStateEduStandardRel>list()
                .stream()
                .collect(Collectors.groupingBy(
                        rel -> rel.getFefuCompetence().getEppSkillGroup(),
                        Collectors.mapping(rel -> rel, Collectors.toList())
                ))
                .entrySet()
                .forEach(entry -> {
                    List<FefuCompetence2EppStateEduStandardRel> rels = entry.getValue();
                    rels.sort((r1, r2) -> Integer.compare(r1.getNumber(), r2.getNumber()));
                    int i = 1;
                    for (FefuCompetence2EppStateEduStandardRel rel : rels)
                        result.put(rel.getFefuCompetence().getId(), i++);
                });

        return result;
    }
}
