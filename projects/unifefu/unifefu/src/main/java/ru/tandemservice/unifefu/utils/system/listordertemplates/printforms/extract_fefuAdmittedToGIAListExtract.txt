\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
О допуске к государственной итоговой аттестации\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_A}\cell\b0
{studentCategory_A} {course} курса{groupInternal_G} {formativeOrgUnitStrWithTerritorial_G},
 {learned_past_A}{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStr},
 не {having} академической задолженности
 и в полном объеме {executed_A} учебный план по {fefuEducationStr_D}
 по {developForm_GF} форме обучения,
 допустить к государственной итоговой аттестации в {seasonTerm_P} в соответствии
 с графиком проведения государственной итоговой аттестации {year}\~года.
\fi0\cell\row\pard

Основание: {listBasics}.