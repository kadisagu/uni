/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu2.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubController;
import ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 15.03.2013
 */
public class Controller extends AbstractListParagraphPubController<FefuAdmitToStateExamsStuListExtract, IDAO, Model>
{
}