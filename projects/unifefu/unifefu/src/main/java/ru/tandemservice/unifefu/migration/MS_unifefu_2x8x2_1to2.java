package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x8x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				new ScriptDependency("org.tandemframework", "1.6.17"),
				new ScriptDependency("org.tandemframework.shared", "1.8.2"),
				new ScriptDependency("ru.tandemservice.uni.product", "2.8.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionSheetMarkBy

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("sessionsheetmarkby_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessionsheetmarkby"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("sessionsheetdoc_id", DBType.LONG), 
				new DBColumn("principalcontext_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sessionSheetMarkBy");

		}


    }
}