package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x2_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionAttSheetOperation

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("sessionattsheetoperation_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("slot_id", DBType.LONG).setNullable(false), 
				new DBColumn("discipline_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("controlaction_id", DBType.LONG).setNullable(false), 
				new DBColumn("mark_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("performdate_p", DBType.DATE).setNullable(false), 
				new DBColumn("worktimedisc_p", DBType.INTEGER), 
				new DBColumn("value_id", DBType.LONG), 
				new DBColumn("comment_p", DBType.TEXT)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sessionAttSheetOperation");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionAttSheetSlot

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("sessionattsheetslot_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("document_id", DBType.LONG).setNullable(false), 
				new DBColumn("student_id", DBType.LONG), 
				new DBColumn("commission_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sessionAttSheetSlot");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionAttSheetDocument

		// создана новая сущность
		{
			// у сущности нет своей таблицы - ничего делать не надо
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sessionAttSheetDocument");

		}


    }
}