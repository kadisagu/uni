/* $Id$ */
package ru.tandemservice.unifefu.events.nsi;

import org.hibernate.Session;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.dao.daemon.IFefuNsiDaemonDao;

import java.util.Collection;

/**
 * @author Dmitry Seleznev
 * @since 12.09.2013
 */
public class NsiEntityInsertListener extends ParamTransactionCompleteListener<Boolean>
{
    public void init()
    {
        for (Class clazz : INsiEntityToBeTracked.ENTITY_TYPES_TO_BE_TRACKED)
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, clazz, this);
    }

    @Override
    public void afterCompletion(Session session, int status, Collection<Long> params, Boolean beforeCompletionResult)
    {
        IFefuNsiDaemonDao.instance.get().doRegisterEntityAdd(params.toArray(new Long[]{}));

        for (Long id : params)
        {
            IEntityMeta meta = EntityRuntime.getMeta(id);
            if (OrgUnit.ENTITY_NAME.equals(meta.getName()))
                IFefuNsiDaemonDao.instance.get().doCreateFefuTopOrgUnit(id);
        }
    }
}