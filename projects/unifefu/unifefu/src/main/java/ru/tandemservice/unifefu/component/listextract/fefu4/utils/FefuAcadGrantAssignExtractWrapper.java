/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.utils;

import org.tandemframework.shared.person.base.entity.Person;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class FefuAcadGrantAssignExtractWrapper implements Comparable<FefuAcadGrantAssignExtractWrapper>
{
    private final Person _person;
    private final String _grantSize;

    public FefuAcadGrantAssignExtractWrapper(Person person, String grantSize)
    {
        _person = person;
        _grantSize = grantSize;
    }

    public Person getPerson()
    {
        return _person;
    }

    public String getGrantSize()
    {
        return _grantSize;
    }

    @Override
    public int compareTo(FefuAcadGrantAssignExtractWrapper o)
    {
        return Person.FULL_FIO_AND_ID_COMPARATOR.compare(_person, o.getPerson());
    }
}