package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Идентификаторы сущностей в НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuNsiIdsGen extends EntityBase
 implements INaturalIdentifiable<FefuNsiIdsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuNsiIds";
    public static final String ENTITY_NAME = "fefuNsiIds";
    public static final int VERSION_HASH = -760961489;
    private static IEntityMeta ENTITY_META;

    public static final String P_GUID = "guid";
    public static final String P_ENTITY_ID = "entityId";
    public static final String P_ENTITY_TYPE = "entityType";
    public static final String P_HAS_FORM_FILLED = "hasFormFilled";
    public static final String P_SYNC_TIME = "syncTime";
    public static final String P_DELETED_FROM_NSI = "deletedFromNsi";
    public static final String P_DELETED_FROM_O_B = "deletedFromOB";
    public static final String P_ENTITY_HASH = "entityHash";

    private String _guid;     // Идентификатор объекта в НСИ
    private long _entityId;     // Идентификатор сущности УНИ
    private String _entityType;     // Тип сущности УНИ
    private boolean _hasFormFilled;     // Анкета выпускника заполнена
    private Date _syncTime;     // Дата и время синхронизации
    private boolean _deletedFromNsi;     // Удалён в НСИ
    private boolean _deletedFromOB;     // Удалён в УНИ
    private Integer _entityHash;     // Hash сущности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор объекта в НСИ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getGuid()
    {
        return _guid;
    }

    /**
     * @param guid Идентификатор объекта в НСИ. Свойство не может быть null и должно быть уникальным.
     */
    public void setGuid(String guid)
    {
        dirty(_guid, guid);
        _guid = guid;
    }

    /**
     * @return Идентификатор сущности УНИ. Свойство не может быть null.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор сущности УНИ. Свойство не может быть null.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Тип сущности УНИ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEntityType()
    {
        return _entityType;
    }

    /**
     * @param entityType Тип сущности УНИ. Свойство не может быть null.
     */
    public void setEntityType(String entityType)
    {
        dirty(_entityType, entityType);
        _entityType = entityType;
    }

    /**
     * @return Анкета выпускника заполнена. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasFormFilled()
    {
        return _hasFormFilled;
    }

    /**
     * @param hasFormFilled Анкета выпускника заполнена. Свойство не может быть null.
     */
    public void setHasFormFilled(boolean hasFormFilled)
    {
        dirty(_hasFormFilled, hasFormFilled);
        _hasFormFilled = hasFormFilled;
    }

    /**
     * @return Дата и время синхронизации.
     */
    public Date getSyncTime()
    {
        return _syncTime;
    }

    /**
     * @param syncTime Дата и время синхронизации.
     */
    public void setSyncTime(Date syncTime)
    {
        dirty(_syncTime, syncTime);
        _syncTime = syncTime;
    }

    /**
     * @return Удалён в НСИ. Свойство не может быть null.
     */
    @NotNull
    public boolean isDeletedFromNsi()
    {
        return _deletedFromNsi;
    }

    /**
     * @param deletedFromNsi Удалён в НСИ. Свойство не может быть null.
     */
    public void setDeletedFromNsi(boolean deletedFromNsi)
    {
        dirty(_deletedFromNsi, deletedFromNsi);
        _deletedFromNsi = deletedFromNsi;
    }

    /**
     * @return Удалён в УНИ. Свойство не может быть null.
     */
    @NotNull
    public boolean isDeletedFromOB()
    {
        return _deletedFromOB;
    }

    /**
     * @param deletedFromOB Удалён в УНИ. Свойство не может быть null.
     */
    public void setDeletedFromOB(boolean deletedFromOB)
    {
        dirty(_deletedFromOB, deletedFromOB);
        _deletedFromOB = deletedFromOB;
    }

    /**
     * @return Hash сущности.
     */
    public Integer getEntityHash()
    {
        return _entityHash;
    }

    /**
     * @param entityHash Hash сущности.
     */
    public void setEntityHash(Integer entityHash)
    {
        dirty(_entityHash, entityHash);
        _entityHash = entityHash;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuNsiIdsGen)
        {
            if (withNaturalIdProperties)
            {
                setGuid(((FefuNsiIds)another).getGuid());
            }
            setEntityId(((FefuNsiIds)another).getEntityId());
            setEntityType(((FefuNsiIds)another).getEntityType());
            setHasFormFilled(((FefuNsiIds)another).isHasFormFilled());
            setSyncTime(((FefuNsiIds)another).getSyncTime());
            setDeletedFromNsi(((FefuNsiIds)another).isDeletedFromNsi());
            setDeletedFromOB(((FefuNsiIds)another).isDeletedFromOB());
            setEntityHash(((FefuNsiIds)another).getEntityHash());
        }
    }

    public INaturalId<FefuNsiIdsGen> getNaturalId()
    {
        return new NaturalId(getGuid());
    }

    public static class NaturalId extends NaturalIdBase<FefuNsiIdsGen>
    {
        private static final String PROXY_NAME = "FefuNsiIdsNaturalProxy";

        private String _guid;

        public NaturalId()
        {}

        public NaturalId(String guid)
        {
            _guid = guid;
        }

        public String getGuid()
        {
            return _guid;
        }

        public void setGuid(String guid)
        {
            _guid = guid;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuNsiIdsGen.NaturalId) ) return false;

            FefuNsiIdsGen.NaturalId that = (NaturalId) o;

            if( !equals(getGuid(), that.getGuid()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getGuid());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getGuid());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuNsiIdsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuNsiIds.class;
        }

        public T newInstance()
        {
            return (T) new FefuNsiIds();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "guid":
                    return obj.getGuid();
                case "entityId":
                    return obj.getEntityId();
                case "entityType":
                    return obj.getEntityType();
                case "hasFormFilled":
                    return obj.isHasFormFilled();
                case "syncTime":
                    return obj.getSyncTime();
                case "deletedFromNsi":
                    return obj.isDeletedFromNsi();
                case "deletedFromOB":
                    return obj.isDeletedFromOB();
                case "entityHash":
                    return obj.getEntityHash();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "guid":
                    obj.setGuid((String) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "entityType":
                    obj.setEntityType((String) value);
                    return;
                case "hasFormFilled":
                    obj.setHasFormFilled((Boolean) value);
                    return;
                case "syncTime":
                    obj.setSyncTime((Date) value);
                    return;
                case "deletedFromNsi":
                    obj.setDeletedFromNsi((Boolean) value);
                    return;
                case "deletedFromOB":
                    obj.setDeletedFromOB((Boolean) value);
                    return;
                case "entityHash":
                    obj.setEntityHash((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "guid":
                        return true;
                case "entityId":
                        return true;
                case "entityType":
                        return true;
                case "hasFormFilled":
                        return true;
                case "syncTime":
                        return true;
                case "deletedFromNsi":
                        return true;
                case "deletedFromOB":
                        return true;
                case "entityHash":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "guid":
                    return true;
                case "entityId":
                    return true;
                case "entityType":
                    return true;
                case "hasFormFilled":
                    return true;
                case "syncTime":
                    return true;
                case "deletedFromNsi":
                    return true;
                case "deletedFromOB":
                    return true;
                case "entityHash":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "guid":
                    return String.class;
                case "entityId":
                    return Long.class;
                case "entityType":
                    return String.class;
                case "hasFormFilled":
                    return Boolean.class;
                case "syncTime":
                    return Date.class;
                case "deletedFromNsi":
                    return Boolean.class;
                case "deletedFromOB":
                    return Boolean.class;
                case "entityHash":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuNsiIds> _dslPath = new Path<FefuNsiIds>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuNsiIds");
    }
            

    /**
     * @return Идентификатор объекта в НСИ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#getGuid()
     */
    public static PropertyPath<String> guid()
    {
        return _dslPath.guid();
    }

    /**
     * @return Идентификатор сущности УНИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Тип сущности УНИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#getEntityType()
     */
    public static PropertyPath<String> entityType()
    {
        return _dslPath.entityType();
    }

    /**
     * @return Анкета выпускника заполнена. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#isHasFormFilled()
     */
    public static PropertyPath<Boolean> hasFormFilled()
    {
        return _dslPath.hasFormFilled();
    }

    /**
     * @return Дата и время синхронизации.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#getSyncTime()
     */
    public static PropertyPath<Date> syncTime()
    {
        return _dslPath.syncTime();
    }

    /**
     * @return Удалён в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#isDeletedFromNsi()
     */
    public static PropertyPath<Boolean> deletedFromNsi()
    {
        return _dslPath.deletedFromNsi();
    }

    /**
     * @return Удалён в УНИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#isDeletedFromOB()
     */
    public static PropertyPath<Boolean> deletedFromOB()
    {
        return _dslPath.deletedFromOB();
    }

    /**
     * @return Hash сущности.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#getEntityHash()
     */
    public static PropertyPath<Integer> entityHash()
    {
        return _dslPath.entityHash();
    }

    public static class Path<E extends FefuNsiIds> extends EntityPath<E>
    {
        private PropertyPath<String> _guid;
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _entityType;
        private PropertyPath<Boolean> _hasFormFilled;
        private PropertyPath<Date> _syncTime;
        private PropertyPath<Boolean> _deletedFromNsi;
        private PropertyPath<Boolean> _deletedFromOB;
        private PropertyPath<Integer> _entityHash;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор объекта в НСИ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#getGuid()
     */
        public PropertyPath<String> guid()
        {
            if(_guid == null )
                _guid = new PropertyPath<String>(FefuNsiIdsGen.P_GUID, this);
            return _guid;
        }

    /**
     * @return Идентификатор сущности УНИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(FefuNsiIdsGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Тип сущности УНИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#getEntityType()
     */
        public PropertyPath<String> entityType()
        {
            if(_entityType == null )
                _entityType = new PropertyPath<String>(FefuNsiIdsGen.P_ENTITY_TYPE, this);
            return _entityType;
        }

    /**
     * @return Анкета выпускника заполнена. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#isHasFormFilled()
     */
        public PropertyPath<Boolean> hasFormFilled()
        {
            if(_hasFormFilled == null )
                _hasFormFilled = new PropertyPath<Boolean>(FefuNsiIdsGen.P_HAS_FORM_FILLED, this);
            return _hasFormFilled;
        }

    /**
     * @return Дата и время синхронизации.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#getSyncTime()
     */
        public PropertyPath<Date> syncTime()
        {
            if(_syncTime == null )
                _syncTime = new PropertyPath<Date>(FefuNsiIdsGen.P_SYNC_TIME, this);
            return _syncTime;
        }

    /**
     * @return Удалён в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#isDeletedFromNsi()
     */
        public PropertyPath<Boolean> deletedFromNsi()
        {
            if(_deletedFromNsi == null )
                _deletedFromNsi = new PropertyPath<Boolean>(FefuNsiIdsGen.P_DELETED_FROM_NSI, this);
            return _deletedFromNsi;
        }

    /**
     * @return Удалён в УНИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#isDeletedFromOB()
     */
        public PropertyPath<Boolean> deletedFromOB()
        {
            if(_deletedFromOB == null )
                _deletedFromOB = new PropertyPath<Boolean>(FefuNsiIdsGen.P_DELETED_FROM_O_B, this);
            return _deletedFromOB;
        }

    /**
     * @return Hash сущности.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiIds#getEntityHash()
     */
        public PropertyPath<Integer> entityHash()
        {
            if(_entityHash == null )
                _entityHash = new PropertyPath<Integer>(FefuNsiIdsGen.P_ENTITY_HASH, this);
            return _entityHash;
        }

        public Class getEntityClass()
        {
            return FefuNsiIds.class;
        }

        public String getEntityName()
        {
            return "fefuNsiIds";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
