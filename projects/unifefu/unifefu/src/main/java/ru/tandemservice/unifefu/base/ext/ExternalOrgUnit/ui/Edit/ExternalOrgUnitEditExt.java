/* $Id$ */
package ru.tandemservice.unifefu.base.ext.ExternalOrgUnit.ui.Edit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.Edit.ExternalOrgUnitEdit;

/**
 * @author nvankov
 * @since 8/13/13
 */
@Configuration
public class ExternalOrgUnitEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + ExternalOrgUnitEditExtUI.class.getSimpleName();

    @Autowired
    private ExternalOrgUnitEdit _externalOrgUnitEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_externalOrgUnitEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, ExternalOrgUnitEditExtUI.class))
                .addAction(new ExternalOrgUnitEditClickApplyAction("onClickApply"))
                .create();
    }
}
