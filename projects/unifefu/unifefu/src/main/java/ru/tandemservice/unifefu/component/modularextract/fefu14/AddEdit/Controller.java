/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu14.AddEdit;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressInter;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.e41.utils.EmployeePostVO;
import ru.tandemservice.movestudent.component.listextract.e41.utils.OrgUnitVO;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;
import ru.tandemservice.unifefu.entity.SendPracticInnerStuExtract;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 6/20/13
 */
public class Controller extends CommonModularStudentExtractAddEditController<SendPracticInnerStuExtract, IDAO, Model>
{
    public void onOrgUnitChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        // Структурное подразделение
        OrgUnit currentOrgUnit = model.getExtract().getPracticeOrgUnit();

        if (null != currentOrgUnit)
        {
            StringBuilder orgUnitStr = new StringBuilder(currentOrgUnit.getTitle());

            AddressDetailed address = currentOrgUnit.getAddress();

            if (null != address)
            {
                if (null != address.getSettlement())
                {
                    orgUnitStr.append(", ").append(address.getSettlement().getTitleWithType());
                    if (address instanceof AddressRu && null != ((AddressRu)address).getStreet())
                    {
                        orgUnitStr.append(", ").append(((AddressRu)address).getStreet().getTitleWithType());
                        if (!StringUtils.isEmpty(((AddressRu)address).getHouseNumber()))
                        {
                            orgUnitStr.append(", д.").append(((AddressRu)address).getHouseNumber());
                            if (!StringUtils.isEmpty(((AddressRu)address).getHouseUnitNumber()))
                            {
                                orgUnitStr.append("-").append(((AddressRu)address).getHouseUnitNumber());
                            }
                        }
                    }
                    else if(address instanceof AddressInter && !StringUtils.isEmpty(address.getShortTitle()))
                    {
                        orgUnitStr.append(", ").append(address.getShortTitle());
                    }
                }

            }

            if (!StringUtils.isEmpty(currentOrgUnit.getPhone()))
                orgUnitStr.append(", тел. ").append(currentOrgUnit.getPhone());

            if (!StringUtils.isEmpty(currentOrgUnit.getEmail()))
                orgUnitStr.append(", ").append(currentOrgUnit.getEmail());

            model.getExtract().setPracticeOrgUnitStr(orgUnitStr.toString());
        }
    }


    public void onPreventAccidentsICChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        if(null != model.getPreventAccidentsIC())
        {
            model.getExtract().setPreventAccidentsICStr(getPreventAccidentsICStr(model.getPreventAccidentsIC()));
        }
    }

    public void onResponsForRecieveCashChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        if(null != model.getResponsForRecieveCash())
        {
            model.getExtract().setResponsForRecieveCashStr(getFioAccustiveStr(model.getResponsForRecieveCash()));
        }
    }

    public void onPracticeHeaderChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        // Руководитель практики
        if(null != model.getPracticeHeader())
        {
            model.getExtract().setPracticeHeaderStr(getPracticeHeaderStr(model.getPracticeHeader()));
        }

    }

    private String getPreventAccidentsICStr(EmployeePostVO employeePostVO)
    {
        StringBuilder preventAccidentsICStrBuilder = new StringBuilder();

        preventAccidentsICStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));

        String preventPostAT = employeePostVO.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
        String preventAccidentsICAccustive = !StringUtils.isEmpty(preventPostAT) ? preventPostAT :
                employeePostVO.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(preventAccidentsICAccustive))
            preventAccidentsICStrBuilder.append(StringUtils.isEmpty(preventAccidentsICStrBuilder.toString()) ? "" : " ").append(preventAccidentsICAccustive.toLowerCase());

        preventAccidentsICStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedIofInitials(employeePostVO.getEmployeePost().getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));

        return preventAccidentsICStrBuilder.toString();
    }


    private String getFioAccustiveStr(EmployeePostVO employeePostVO)
    {
        EmployeePost practiceHeader = employeePostVO.getEmployeePost();

        StringBuilder fioStrBuilder = new StringBuilder();

        fioStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));

        String practiceHeaderPostAT = practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
        String practiceHeaderAccustive = !StringUtils.isEmpty(practiceHeaderPostAT) ? practiceHeaderPostAT :
                practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(practiceHeaderAccustive))
            fioStrBuilder.append(StringUtils.isEmpty(fioStrBuilder.toString()) ? "" : " ").append(practiceHeaderAccustive.toLowerCase());

        fioStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(practiceHeader.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));

        return fioStrBuilder.toString();
    }

    private String getPracticeHeaderStr(EmployeePostVO employeePostVO)
    {
        EmployeePost practiceHeader = employeePostVO.getEmployeePost();

        StringBuilder practiceHeaderStrBuilder = new StringBuilder();

        practiceHeaderStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));

        String practiceHeaderPostNT = practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        String practiceHeaderNominative = !StringUtils.isEmpty(practiceHeaderPostNT) ? practiceHeaderPostNT :
                practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(practiceHeaderNominative))
            practiceHeaderStrBuilder.append(StringUtils.isEmpty(practiceHeaderStrBuilder.toString()) ? "" : " ").append(practiceHeaderNominative.toLowerCase());

        practiceHeaderStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(practiceHeader.getPerson().getIdentityCard(), GrammaCase.NOMINATIVE));

        return practiceHeaderStrBuilder.toString();
    }

    private String getDegreeShortTitleWithDots(PersonAcademicDegree prevDegree)
    {
        String degreeShortTitleWithDots = "";
        if (null != prevDegree)
        {
            String[] degreeTitle = prevDegree.getAcademicDegree().getTitle().toLowerCase().split(" ");
            StringBuilder shortDegreeTitle = new StringBuilder();
            for (String deg : Lists.newArrayList(degreeTitle))
            {
                shortDegreeTitle.append(deg.charAt(0)).append(".");
            }
            degreeShortTitleWithDots += shortDegreeTitle;
        }
        return degreeShortTitleWithDots;
    }


}
