package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.5.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.5.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность bbStudent

		// сущность была удалена
        if (tool.tableExists("bbstudent_t"))
		{
			// удалить таблицу
			tool.dropTable("bbstudent_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("bbStudent");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbCourseStudent

		// создана новая сущность
        if (!tool.tableExists("bbcoursestudent_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("bbcoursestudent_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("bbcourse_id", DBType.LONG).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("access_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("bbprimaryid_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("bbCourseStudent");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbGroupMembership

		// создана новая сущность
        if (!tool.tableExists("bbgroupmembership_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("bbgroupmembership_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("bbgroup_id", DBType.LONG).setNullable(false), 
				new DBColumn("bbcoursestudent_id", DBType.LONG).setNullable(false), 
				new DBColumn("bbprimaryid_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("bbGroupMembership");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbCourse

        if (tool.columnExists("bbcourse_t", "blackboardid_p"))
		{
			tool.renameColumn("bbcourse_t", "blackboardid_p", "bbcourseid_p");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbGroup

		// удалено свойство bbCourse
        if (tool.columnExists("bbgroup_t", "bbcourse_id"))
		{
			// удалить колонку
			tool.dropColumn("bbgroup_t", "bbcourse_id");
		}

		// удалено свойство trJournalGroup
        if (tool.columnExists("bbgroup_t", "trjournalgroup_id"))
		{
			// удалить колонку
			tool.dropColumn("bbgroup_t", "trjournalgroup_id");

		}

		// создано обязательное свойство bbCourse2TrJournalRel
        if (!tool.columnExists("bbgroup_t", "bbcourse2trjournalrel_id"))
		{
			// создать колонку
			tool.createColumn("bbgroup_t", new DBColumn("bbcourse2trjournalrel_id", DBType.LONG));

			// сделать колонку NOT NULL
			tool.setColumnNullable("bbgroup_t", "bbcourse2trjournalrel_id", false);
		}

		// создано свойство group
        if (!tool.columnExists("bbgroup_t", "group_id"))
		{
			// создать колонку
			tool.createColumn("bbgroup_t", new DBColumn("group_id", DBType.LONG));
		}
    }
}