package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuCaptainGroupToExtractRelation;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь приказа о переводе со старостой группы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuCaptainGroupToExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuCaptainGroupToExtractRelation";
    public static final String ENTITY_NAME = "fefuCaptainGroupToExtractRelation";
    public static final int VERSION_HASH = 2352318;
    private static IEntityMeta ENTITY_META;

    public static final String L_CAPTAIN = "captain";
    public static final String L_EXTRACT = "extract";

    private Student _captain;     // Староста
    private FefuTransfSpecialityStuListExtract _extract;     // Выписка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Староста. Свойство не может быть null.
     */
    @NotNull
    public Student getCaptain()
    {
        return _captain;
    }

    /**
     * @param captain Староста. Свойство не может быть null.
     */
    public void setCaptain(Student captain)
    {
        dirty(_captain, captain);
        _captain = captain;
    }

    /**
     * @return Выписка. Свойство не может быть null.
     */
    @NotNull
    public FefuTransfSpecialityStuListExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка. Свойство не может быть null.
     */
    public void setExtract(FefuTransfSpecialityStuListExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuCaptainGroupToExtractRelationGen)
        {
            setCaptain(((FefuCaptainGroupToExtractRelation)another).getCaptain());
            setExtract(((FefuCaptainGroupToExtractRelation)another).getExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuCaptainGroupToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuCaptainGroupToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new FefuCaptainGroupToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "captain":
                    return obj.getCaptain();
                case "extract":
                    return obj.getExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "captain":
                    obj.setCaptain((Student) value);
                    return;
                case "extract":
                    obj.setExtract((FefuTransfSpecialityStuListExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "captain":
                        return true;
                case "extract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "captain":
                    return true;
                case "extract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "captain":
                    return Student.class;
                case "extract":
                    return FefuTransfSpecialityStuListExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuCaptainGroupToExtractRelation> _dslPath = new Path<FefuCaptainGroupToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuCaptainGroupToExtractRelation");
    }
            

    /**
     * @return Староста. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCaptainGroupToExtractRelation#getCaptain()
     */
    public static Student.Path<Student> captain()
    {
        return _dslPath.captain();
    }

    /**
     * @return Выписка. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCaptainGroupToExtractRelation#getExtract()
     */
    public static FefuTransfSpecialityStuListExtract.Path<FefuTransfSpecialityStuListExtract> extract()
    {
        return _dslPath.extract();
    }

    public static class Path<E extends FefuCaptainGroupToExtractRelation> extends EntityPath<E>
    {
        private Student.Path<Student> _captain;
        private FefuTransfSpecialityStuListExtract.Path<FefuTransfSpecialityStuListExtract> _extract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Староста. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCaptainGroupToExtractRelation#getCaptain()
     */
        public Student.Path<Student> captain()
        {
            if(_captain == null )
                _captain = new Student.Path<Student>(L_CAPTAIN, this);
            return _captain;
        }

    /**
     * @return Выписка. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCaptainGroupToExtractRelation#getExtract()
     */
        public FefuTransfSpecialityStuListExtract.Path<FefuTransfSpecialityStuListExtract> extract()
        {
            if(_extract == null )
                _extract = new FefuTransfSpecialityStuListExtract.Path<FefuTransfSpecialityStuListExtract>(L_EXTRACT, this);
            return _extract;
        }

        public Class getEntityClass()
        {
            return FefuCaptainGroupToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "fefuCaptainGroupToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
