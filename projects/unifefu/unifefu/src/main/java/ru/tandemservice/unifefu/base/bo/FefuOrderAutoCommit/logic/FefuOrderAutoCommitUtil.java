/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.BackgroundProcessBase;
import org.tandemframework.core.process.IBackgroundProcess;
import org.tandemframework.core.process.ProcessResult;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.FefuOrderAutoCommitManager;
import ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuOrderCommitResultCodes;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 16.11.2012
 */
public class FefuOrderAutoCommitUtil
{
    public static List<FefuDirectumOrderData> validateDirectumSyncFileStructureAndPrepareData(IUploadFile file, EmployeePost executor) throws Exception
    {
        // TODO: админ не должен запускать это действие, нужно проводить валидацию.
        if (null == executor) addFileError("Администратор не может выполнять синхронизацию с СЭД Directum.");

        //50Б требуется для хранения данных одного приказа. Предполагаем, что загружаемый файл содержит не более 1000 приказов.
        if (file.getSize() > 512 * 1024)
            addFileError("Размер загружаемого из СЭД Directum файла не может превышать 512кБ.");

        String[] orderRecords = IOUtils.toString(file.getStream(), "UTF-8").trim().split("\r\n");
        if (orderRecords.length == 0) addFileError("Файл не должен быть пустым.");
        List<FefuDirectumOrderData> orderDataList = new ArrayList<>();
        Set<String> addedOrderNumbers = new HashSet<>();
        Set<Long> addedOrderIds = new HashSet<>();

        for (String singleRecord : orderRecords)
        {
            int splitterCount = 0;
            for (int i = 0; i < singleRecord.length(); i++)
                if (singleRecord.charAt(i) == ';')
                    splitterCount++;

            if (splitterCount != 2)
                addFileError("Одна, или несколько записей в загружаемом файле имеет некорректный формат.");

            String[] params = singleRecord.split(";");
            if (params.length != 3)
                addFileError("Одна, или несколько записей в загружаемом файле имеет некорректный формат.");

            Long id = null;
            try
            {
                id = Long.valueOf(params[0]);
            } catch (Exception e)
            {
                addFileError("Одна, или несколько записей в загружаемом файле содержит некорректный идентификатор приказа.");
            }

            String number = params[1];
            if (number.trim().length() == 0)
                addFileError("Одна, или несколько записей в загружаемом файле не содержит номера приказа.");
            //TODO: Order number correctness

            Date date = null;
            if (params[2].trim().length() == 0)
                addFileError("Одна, или несколько записей в загружаемом файле не содержит даты приказа.");
            String firstDate = params[2].split(" ")[0];

            if (firstDate.length() != 10)
                addFileError("Одна, или несколько записей в загружаемом файле содержит дату приказа в некорректном формате. Формат должен соответствовать шаблону \"дд.мм.гггг\".");

            try
            {
                date = DateUtils.parseDate(firstDate, new String[]{"dd.MM.yyyy"});
            } catch (Exception e)
            {
                addFileError("Одна, или несколько записей в загружаемом файле содержит дату приказа в некорректном формате. Формат должен соответствовать шаблону \"дд.мм.гггг\".");
            }

            if (addedOrderIds.contains(id))
                addFileError("Две, или более записей в загружаемом файле ссылаются на один и тот же идентификатор приказа (" + id + ").");

            if (addedOrderNumbers.contains(number))
                addFileError("Две, или более записей в загружаемом файле содержат один и тот же номер приказа (" + number + ").");

            addedOrderIds.add(id);
            addedOrderNumbers.add(number);
            orderDataList.add(new FefuDirectumOrderData(id, number, date));
        }

        FefuOrderAutoCommitManager.instance().dao().validateFefuDirectumOrderData(orderDataList);

        return orderDataList;
    }

    public static void synchronizeOrders(final List<FefuDirectumOrderData> orderDataList, final EmployeePost executor, final Date formingDate, final boolean commit)
    {
        final Long transactionId = System.currentTimeMillis();
        final String executorStr = OrderExecutorSelectModel.getExecutor(executor);

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {

            if (orderDataList.size() > 0)
            {
                final IBackgroundProcess process = new BackgroundProcessBase()
                {
                    @Override
                    public ProcessResult run(final ProcessState state)
                    {
                        int currentValue = 0;
                        state.setMaxValue(100);

                        for (FefuDirectumOrderData data : orderDataList)
                        {
                            // Синхронизация номера и даты
                            FefuOrderAutoCommitLogRow row = FefuOrderAutoCommitManager.instance().dao().doSynchronizeSingleOrderData(data, transactionId, executor, executorStr, formingDate, commit, state, orderDataList.size(), currentValue);
                            Thread.yield();

                            if (commit && FefuOrderCommitResultCodes.SUCCESS.equals(row.getCommitResult().getCode()))
                            {
                                // Согласование и поведение приказа
                                try
                                {
                                    FefuOrderAutoCommitManager.instance().dao().doCommitOrders(row, data.getId(), executor, state, orderDataList.size(), currentValue);
                                } catch (FefuOrderCommitError error)
                                {
                                    row.setCommentShort(error.getShortErrText());
                                    row.setComment(error.getErrText());
                                    row.setCommitResult(error.getResult());
                                    DataAccessServices.dao().update(row);
                                }
                            }
                            Thread.yield();
                        }
                        return null; // закрываем диалог
                    }
                };

                new BackgroundProcessHolder().start("Синхронизация с СЭД Directum", process);
                //model.setDataSource(null); // дабы на время показа дочерней формы не грузить список студентов еще раз
            }
        } finally
        {
            eventLock.release();
        }
    }

    private static void addFileError(String message)
    {
        UserContext.getInstance().getErrorCollector().add(message, "file");
        throw new ApplicationException(message);
    }
}