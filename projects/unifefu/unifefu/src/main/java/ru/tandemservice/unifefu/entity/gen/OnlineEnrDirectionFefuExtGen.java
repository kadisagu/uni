package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;
import ru.tandemservice.unifefu.entity.OnlineEnrDirectionFefuExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выбранное онлайн направление приема (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OnlineEnrDirectionFefuExtGen extends EntityBase
 implements INaturalIdentifiable<OnlineEnrDirectionFefuExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.OnlineEnrDirectionFefuExt";
    public static final String ENTITY_NAME = "onlineEnrDirectionFefuExt";
    public static final int VERSION_HASH = -1819252809;
    private static IEntityMeta ENTITY_META;

    public static final String L_ONLINE_ENR_DIRECTION = "onlineEnrDirection";
    public static final String P_PRIORITY = "priority";

    private OnlineRequestedEnrollmentDirection _onlineEnrDirection;     // Выбранное онлайн направление приема
    private Integer _priority;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выбранное онлайн направление приема. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public OnlineRequestedEnrollmentDirection getOnlineEnrDirection()
    {
        return _onlineEnrDirection;
    }

    /**
     * @param onlineEnrDirection Выбранное онлайн направление приема. Свойство не может быть null и должно быть уникальным.
     */
    public void setOnlineEnrDirection(OnlineRequestedEnrollmentDirection onlineEnrDirection)
    {
        dirty(_onlineEnrDirection, onlineEnrDirection);
        _onlineEnrDirection = onlineEnrDirection;
    }

    /**
     * @return Приоритет.
     */
    public Integer getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет.
     */
    public void setPriority(Integer priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OnlineEnrDirectionFefuExtGen)
        {
            if (withNaturalIdProperties)
            {
                setOnlineEnrDirection(((OnlineEnrDirectionFefuExt)another).getOnlineEnrDirection());
            }
            setPriority(((OnlineEnrDirectionFefuExt)another).getPriority());
        }
    }

    public INaturalId<OnlineEnrDirectionFefuExtGen> getNaturalId()
    {
        return new NaturalId(getOnlineEnrDirection());
    }

    public static class NaturalId extends NaturalIdBase<OnlineEnrDirectionFefuExtGen>
    {
        private static final String PROXY_NAME = "OnlineEnrDirectionFefuExtNaturalProxy";

        private Long _onlineEnrDirection;

        public NaturalId()
        {}

        public NaturalId(OnlineRequestedEnrollmentDirection onlineEnrDirection)
        {
            _onlineEnrDirection = ((IEntity) onlineEnrDirection).getId();
        }

        public Long getOnlineEnrDirection()
        {
            return _onlineEnrDirection;
        }

        public void setOnlineEnrDirection(Long onlineEnrDirection)
        {
            _onlineEnrDirection = onlineEnrDirection;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OnlineEnrDirectionFefuExtGen.NaturalId) ) return false;

            OnlineEnrDirectionFefuExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getOnlineEnrDirection(), that.getOnlineEnrDirection()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOnlineEnrDirection());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOnlineEnrDirection());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OnlineEnrDirectionFefuExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OnlineEnrDirectionFefuExt.class;
        }

        public T newInstance()
        {
            return (T) new OnlineEnrDirectionFefuExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "onlineEnrDirection":
                    return obj.getOnlineEnrDirection();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "onlineEnrDirection":
                    obj.setOnlineEnrDirection((OnlineRequestedEnrollmentDirection) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "onlineEnrDirection":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "onlineEnrDirection":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "onlineEnrDirection":
                    return OnlineRequestedEnrollmentDirection.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OnlineEnrDirectionFefuExt> _dslPath = new Path<OnlineEnrDirectionFefuExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OnlineEnrDirectionFefuExt");
    }
            

    /**
     * @return Выбранное онлайн направление приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.OnlineEnrDirectionFefuExt#getOnlineEnrDirection()
     */
    public static OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection> onlineEnrDirection()
    {
        return _dslPath.onlineEnrDirection();
    }

    /**
     * @return Приоритет.
     * @see ru.tandemservice.unifefu.entity.OnlineEnrDirectionFefuExt#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends OnlineEnrDirectionFefuExt> extends EntityPath<E>
    {
        private OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection> _onlineEnrDirection;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выбранное онлайн направление приема. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.OnlineEnrDirectionFefuExt#getOnlineEnrDirection()
     */
        public OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection> onlineEnrDirection()
        {
            if(_onlineEnrDirection == null )
                _onlineEnrDirection = new OnlineRequestedEnrollmentDirection.Path<OnlineRequestedEnrollmentDirection>(L_ONLINE_ENR_DIRECTION, this);
            return _onlineEnrDirection;
        }

    /**
     * @return Приоритет.
     * @see ru.tandemservice.unifefu.entity.OnlineEnrDirectionFefuExt#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(OnlineEnrDirectionFefuExtGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return OnlineEnrDirectionFefuExt.class;
        }

        public String getEntityName()
        {
            return "onlineEnrDirectionFefuExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
