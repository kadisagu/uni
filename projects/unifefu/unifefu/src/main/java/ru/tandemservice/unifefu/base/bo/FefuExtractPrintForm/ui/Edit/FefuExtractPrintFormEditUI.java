/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.ui.Edit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.FefuExtractPrintFormManager;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;


/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
@Input({@Bind(key = "objectId", binding = "orderId", required = true)})
public class FefuExtractPrintFormEditUI extends UIPresenter
{

    private Long _orderId;

    private IUploadFile _uploadFile;

    private String _printFormName;

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

    public String getPrintFormName()
    {
        return _printFormName;
    }

    public void setPrintFormName(String printFormName)
    {
        _printFormName = printFormName;
    }

    public boolean isPrintFormExist()
    {
        return UniDaoFacade.getCoreDao().existsEntity(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.order().id().s(), _orderId);
    }

    @Override
    public void onComponentRefresh()
    {
        _printFormName = UniDaoFacade.getCoreDao().getProperty(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.P_FILE_NAME, FefuOrderToPrintFormRelation.order().id().s(), _orderId);
    }

    public void onClickApply()
    {
        FefuExtractPrintFormManager.instance().dao().saveOrUpdatePrintForm(DataAccessServices.dao().get(AbstractStudentOrder.class, _orderId), getUploadFile());
        deactivate();
    }

    public void onClickDelete()
    {
        FefuExtractPrintFormManager.instance().dao().deletePrintForm(DataAccessServices.dao().get(AbstractStudentOrder.class, _orderId));
        deactivate();
    }
}