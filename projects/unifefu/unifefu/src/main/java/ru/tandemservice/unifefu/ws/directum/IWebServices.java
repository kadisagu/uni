/**
 * IWebServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public interface IWebServices extends java.rmi.Remote {
    public java.lang.String[] referencesUpdate(java.lang.String XMLPackage, java.lang.String ISCode) throws java.rmi.RemoteException;
    public java.lang.String referencesUpdateAsync(java.lang.String XMLPackage, java.lang.String ISCode, java.lang.Boolean fullSync) throws java.rmi.RemoteException;
    public java.lang.String getReferenceChangedFrom(java.lang.String referenceName, java.lang.String dateFrom, java.lang.String ISCode) throws java.rmi.RemoteException;
    public java.lang.String getEntity(java.lang.String referenceName, java.lang.Integer recordKey, java.lang.String ISCode) throws java.rmi.RemoteException;
    public java.lang.String getRegCard(java.lang.Integer EDocumentID, java.lang.String ISCode) throws java.rmi.RemoteException;
    public java.lang.String getEDocumentVersionList(java.lang.Integer EDocumentID) throws java.rmi.RemoteException;
    public byte[] getEDocument(java.lang.Integer EDocumentID, java.lang.Integer verNum) throws java.rmi.RemoteException;
    public java.lang.String getEDocumentCard(java.lang.Integer EDocumentID) throws java.rmi.RemoteException;
    public void EDocumentCardUpdate(java.lang.String XMLPackage, java.lang.Integer EDocumentID) throws java.rmi.RemoteException;
    public java.lang.String[] EDocumentsCreate(java.lang.String XMLPackage, byte[][] documents) throws java.rmi.RemoteException;
    public java.lang.String createTask(java.lang.String XMLPackage, byte[][] documents) throws java.rmi.RemoteException;
    public java.lang.String createTaskAsync(java.lang.String XMLPackage, byte[][] documents) throws java.rmi.RemoteException;
    public java.lang.String getEDocumentRequisite(java.lang.String TKED) throws java.rmi.RemoteException;
    public java.lang.String getReferenceFormInfo(java.lang.String referenceName) throws java.rmi.RemoteException;
    public java.lang.String getReferencesFiltered(java.lang.String referenceName, java.lang.String filter, java.lang.String ISCode) throws java.rmi.RemoteException;
    public java.lang.String getEDocumentFormInfo(java.lang.Integer EDocumentID) throws java.rmi.RemoteException;
    public java.lang.String getReferenceRequisite(java.lang.String referenceName) throws java.rmi.RemoteException;
    public java.lang.Integer EDocumentVersionUpdate(java.lang.Integer EDocumentID, java.lang.Integer verNum, java.lang.String editor, byte[] document) throws java.rmi.RemoteException;
    public java.lang.String getBindEDocumentsList(java.lang.String objectType, java.lang.String objectKey, java.lang.String ISCode) throws java.rmi.RemoteException;
    public java.lang.String[] EDocumentUpdateWithBind(java.lang.String XMLPackage, byte[][] documents, java.lang.String objectType, java.lang.String objectKey, java.lang.String ISCode) throws java.rmi.RemoteException;
    public java.lang.String transformXMLPackage(java.lang.String XMLPackage, java.lang.String ISCode) throws java.rmi.RemoteException;
    public void objectBindToEDocuments(int[] EDocumentIDs, java.lang.String objectType, java.lang.String objectKey, java.lang.String ISCode) throws java.rmi.RemoteException;
}
