/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionBlockSpecialityAddEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.FefuSpeciality;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getSpecialityId() == null)
        {
            FefuSpeciality speciality = new FefuSpeciality();
            speciality.setBlock(get(EppEduPlanVersionBlock.class, model.getBlockId()));
            model.setSpeciality(speciality);

        } else
        {
            model.setSpeciality(get(FefuSpeciality.class, model.getSpecialityId()));
        }
    }
}