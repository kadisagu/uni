/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleSession;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unifefu.base.ext.SppScheduleSession.logic.FefuScheduleSessionDAO;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.ISppScheduleSessionDAO;

/**
 * @author nvankov
 * @since 12/23/14
 */
@Configuration
public class SppScheduleSessionExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public ISppScheduleSessionDAO dao()
    {
        return new FefuScheduleSessionDAO();
    }
}
