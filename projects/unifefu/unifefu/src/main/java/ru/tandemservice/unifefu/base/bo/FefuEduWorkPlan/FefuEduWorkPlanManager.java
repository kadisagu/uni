/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.logic.FefuEduWorkPlanDAO;
import ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.logic.IFefuEduWorkPlanDAO;

/**
 * @author nvankov
 * @since 1/21/14
 */
@Configuration
public class FefuEduWorkPlanManager extends BusinessObjectManager
{
    public static FefuEduWorkPlanManager instance()
    {
        return instance(FefuEduWorkPlanManager.class);
    }

    @Bean
    public IFefuEduWorkPlanDAO dao()
    {
        return new FefuEduWorkPlanDAO();
    }
}



    