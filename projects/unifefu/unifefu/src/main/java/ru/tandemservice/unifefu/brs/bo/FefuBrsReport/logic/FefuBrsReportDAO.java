/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsReport.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author nvankov
 * @since 1/20/14
 */
@Transactional
public class FefuBrsReportDAO extends UniBaseDao implements IFefuBrsReportDAO
{
    @Override
    public EppYearPart lastYearPart()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppYearPart.class, "yp")
                .top(1).column("yp")
                .order(property("yp", EppYearPart.year().educationYear().intValue()), OrderDirection.desc)
                .order(property("yp", EppYearPart.part().yearDistribution().amount()), OrderDirection.desc)
                .order(property("yp", EppYearPart.part().number()), OrderDirection.desc);

        return createStatement(builder).uniqueResult();
    }

    @Override
    public OrgUnit getParentFormativeOrgUnit(OrgUnit orgUnit)
    {
        while (orgUnit != null)
        {
            if (UniDaoFacade.getOrgstructDao().isOrgUnitHasKind(orgUnit, UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                return orgUnit;

            orgUnit = orgUnit.getParent();
        }
        return null;
    }

    private static Long _eppStateAccepted = null;

    @Override
    public Long getEppStateAcceptedId()
    {
        if (_eppStateAccepted == null)
            _eppStateAccepted = getId(EppState.class, EppState.P_CODE, EppState.STATE_ACCEPTED);

        return _eppStateAccepted;
    }
}
