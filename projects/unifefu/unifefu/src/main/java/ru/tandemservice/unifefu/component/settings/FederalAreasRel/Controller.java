package ru.tandemservice.unifefu.component.settings.FederalAreasRel;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unifefu.entity.FefuFederalAreasRel;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author amakarova
 * @since 25.10.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        prepareDataSource(component);

        model.setSettings(component.getSettings());

        getDao().prepare(model);
    }

    protected void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }
        DynamicListDataSource<FefuFederalAreasRel> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Административная единица", FefuFederalAreasRel.addressItem().fullTitle()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Федеральный округ", FefuFederalAreasRel.fefuFederalDistrict().title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Код", FefuFederalAreasRel.addressItem().inheritedRegionCode()).setClickable(false).setOrderable(false));
        model.setDataSource(dataSource);
    }

    public void onClickFillFederalAreasRelation(IBusinessComponent component)
    {
        getDao().fillData(getDocument());
    }


    public void onClickUpdateFederalAreasRelation(IBusinessComponent component)
    {
        getDao().updateData(getDocument());
    }

    private Document getDocument()
    {
        String path = ApplicationRuntime.getAppInstallPath(); //${app.install.path}
        final File file = new File(path, "data/addFederal.xml");
        InputStream inputStream;
        if (!file.exists() || !file.canRead())
        {
            inputStream = getClass().getClassLoader().getResourceAsStream("unifefu/addFederal.xml");
        }
        else
        {
            try
            {
                inputStream = new FileInputStream(file);
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }

        try
        {
            return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
        }
        catch (SAXException | IOException | ParserConfigurationException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void onClickDeleteFederalAreasRelation(IBusinessComponent component)
    {
        getDao().clearData();
    }
}
