/* $Id$ */
package ru.tandemservice.unifefu.dao.mdbio;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

import java.io.IOException;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 09.10.2012
 */
public class FefuStudentIOAddDataDao extends BaseIODao implements IFefuStudentIOAddDataDao
{
    @Override
    public Map<String, Long> doImport_FefuStudentAddData(Database mdb) throws IOException
    {

        Table all_stud = mdb.getTable("all_stud");
        if (all_stud == null) throw new ApplicationException("Таблица «all_stud» отсутствует в базе данных.");

        final Session session = this.getSession();

        final Map<String, StudentFefuExt> extensionsMap = new HashMap<>();
        final Map<String, List<StudentFefuExt>> duplicatesMap = new HashMap<>();
        List<StudentFefuExt> extensions = new DQLSelectBuilder().fromEntity(StudentFefuExt.class, "fe").column("fe")
                .createStatement(session).list();

        for (StudentFefuExt ext : extensions)
        {
            if (!extensionsMap.containsKey(ext.getIntegrationId()))
                extensionsMap.put(ext.getIntegrationId(), ext);
            else
            {
                List<StudentFefuExt> duplicatesList = duplicatesMap.get(ext.getIntegrationId());
                if (null == duplicatesList) duplicatesList = new ArrayList<>();
                duplicatesList.add(ext);
                duplicatesMap.put(ext.getIntegrationId(), duplicatesList);
            }
        }

        final Set<String> studentExtIdList = new HashSet<>();
        BatchUtils.execute(all_stud, 32, rows -> {
            for (final Map<String, Object> row : rows)
            {
                final Object id = row.get("stud_uid");
                if(studentExtIdList.contains(id)) continue;
                studentExtIdList.add((String)id);

                try
                {
                    final StudentFefuExt studentFefuExt = extensionsMap.get(id);
                    List<StudentFefuExt> extList = duplicatesMap.get(id);
                    if (null == extList) extList = new ArrayList<>();
                    if(null != studentFefuExt) extList.add(studentFefuExt);

                    for (StudentFefuExt singleExt : extList)
                    {
                        singleExt = (StudentFefuExt)session.load(StudentFefuExt.class, singleExt.getId());
                        fillStudentFefuExt(singleExt, row);
                        session.saveOrUpdate(singleExt);
                    }

                } catch (final Throwable t)
                {
                    log4j_logger.fatal("Error with processing: all_stud [stud_uid = " + id + "]");
                    log4j_logger.fatal(t.getMessage(), t);
                    throw new IllegalArgumentException("Студент stud_uid=" + id + ": Произошла ошибка обработки дополнительных данных.");
                }
            }

            System.err.print(".");
            session.flush();
            session.clear();
        });

        return new HashMap<>();
    }

    private void fillStudentFefuExt(StudentFefuExt studentFefuExt, Map<String, Object> row)
    {
        studentFefuExt.setGroupNumber(StringUtils.trimToNull((String) row.get("group_number")));
        studentFefuExt.setOrderDate(StringUtils.trimToNull((String) row.get("prikaz_date")));
        studentFefuExt.setOrderNumber(StringUtils.trimToNull((String) row.get("prikaz")));
        studentFefuExt.setOksoMajorCode(StringUtils.trimToNull((String) row.get("OKSO_major_code")));
        studentFefuExt.setOksoMajor(StringUtils.trimToNull((String) row.get("OKSO_major")));
        studentFefuExt.setMajorCode(StringUtils.trimToNull((String) row.get("major_code")));
        studentFefuExt.setMajor(StringUtils.trimToNull((String) row.get("major")));
        studentFefuExt.setSubMajor(StringUtils.trimToNull((String) row.get("submajor")));
        studentFefuExt.setSrokObu(StringUtils.trimToNull((String) row.get("srokobu")));
        studentFefuExt.setEduForm(StringUtils.trimToNull((String) row.get("form_education")));
        studentFefuExt.setInstitute(StringUtils.trimToNull((String) row.get("institute")));
        studentFefuExt.setSchool(StringUtils.trimToNull((String) row.get("school")));
        studentFefuExt.setOldAddress1Str(StringUtils.trimToNull((String) row.get("addres1")));
        studentFefuExt.setOldAddress2Str(StringUtils.trimToNull((String) row.get("addres2")));
    }
}