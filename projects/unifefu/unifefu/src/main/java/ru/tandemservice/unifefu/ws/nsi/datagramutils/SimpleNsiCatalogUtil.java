/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import org.apache.commons.collections.map.HashedMap;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import javax.xml.namespace.QName;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 08.04.2014
 */
public abstract class SimpleNsiCatalogUtil<T extends INsiEntity, E extends IEntity> implements INsiEntityUtil<T, E>
{
    public static final String ID_NSI_FIELD = "ID";
    public static final String CODE_NSI_FIELD = "CODE";
    public static final String TITLE_NSI_FIELD = "TITLE";
    public static final String SHORT_TITLE_NSI_FIELD = "SHORT_TITLE";
    public static final String GUID_FIELD = "GUID";
    public static final String SHORT_TITLE_FIELD = "shortTitle";

    protected List<String> NSI_FIELDS;
    protected List<String> ENTITY_FIELDS;

    protected Map<String, String> NSI_TO_OB_FIELDS_MAP;
    protected Map<String, String> OB_TO_NSI_FIELDS_MAP;

    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            NSI_FIELDS = new ArrayList<>();
            NSI_FIELDS.add(ID_NSI_FIELD);
            NSI_FIELDS.add(CODE_NSI_FIELD);
            NSI_FIELDS.add(TITLE_NSI_FIELD);
            NSI_FIELDS.add(SHORT_TITLE_NSI_FIELD);
        }

        return NSI_FIELDS;
    }

    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            ENTITY_FIELDS = new ArrayList<>();
            ENTITY_FIELDS.add(GUID_FIELD);
            ENTITY_FIELDS.add(ICatalogItem.CATALOG_ITEM_USER_CODE);
            ENTITY_FIELDS.add(ICatalogItem.CATALOG_ITEM_TITLE);
            ENTITY_FIELDS.add(SHORT_TITLE_FIELD);
        }

        return ENTITY_FIELDS;
    }

    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            NSI_TO_OB_FIELDS_MAP = new HashedMap();
            NSI_TO_OB_FIELDS_MAP.put(ID_NSI_FIELD, GUID_FIELD);
            NSI_TO_OB_FIELDS_MAP.put(CODE_NSI_FIELD, ICatalogItem.CATALOG_ITEM_USER_CODE);
            NSI_TO_OB_FIELDS_MAP.put(TITLE_NSI_FIELD, ICatalogItem.CATALOG_ITEM_TITLE);
            NSI_TO_OB_FIELDS_MAP.put(SHORT_TITLE_NSI_FIELD, SHORT_TITLE_FIELD);
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            OB_TO_NSI_FIELDS_MAP = new HashedMap();
            OB_TO_NSI_FIELDS_MAP.put(GUID_FIELD, ID_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(ICatalogItem.CATALOG_ITEM_USER_CODE, CODE_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(ICatalogItem.CATALOG_ITEM_TITLE, TITLE_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(SHORT_TITLE_FIELD, SHORT_TITLE_NSI_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    @Override
    public long getNsiIdHash(T nsiEntity)
    {
        return MD5HashBuilder.getCheckSum(nsiEntity.getID().toLowerCase());
    }

    @Override
    public long getNsiCodeHash(T nsiEntity)
    {
        return MD5HashBuilder.getCheckSum(nsiEntity.getCode());
    }

    @Override
    public long getNsiTitleHash(T nsiEntity, boolean caseInsensitive)
    {
        if (null == nsiEntity.getTitle()) return 0;
        if (caseInsensitive) return MD5HashBuilder.getCheckSum(nsiEntity.getTitle().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(nsiEntity.getTitle());
    }

    @Override
    public long getNsiShortTitleHash(T nsiEntity, boolean caseInsensitive)
    {
        if (null == nsiEntity.getShortTitle()) return 0;
        if (caseInsensitive) return MD5HashBuilder.getCheckSum(nsiEntity.getShortTitle().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(nsiEntity.getShortTitle());
    }

    @Override
    public long getNsiFieldHash(T nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case ID_NSI_FIELD:
                return getNsiIdHash(nsiEntity);
            case CODE_NSI_FIELD:
                return getNsiCodeHash(nsiEntity);
            case TITLE_NSI_FIELD:
                return getNsiTitleHash(nsiEntity, caseInsensitive);
            case SHORT_TITLE_NSI_FIELD:
                return getNsiShortTitleHash(nsiEntity, caseInsensitive);
            default:
                throw new UnsupportedOperationException("Field '" + fieldName + "' not found for '" + nsiEntity.getClass().getSimpleName() + "' entity type.");
        }
    }

    @Override
    public boolean isFieldEmpty(T nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case ID_NSI_FIELD:
                return null == nsiEntity.getID();
            case CODE_NSI_FIELD:
                return null == nsiEntity.getCode();
            case TITLE_NSI_FIELD:
                return null == nsiEntity.getTitle();
            case SHORT_TITLE_NSI_FIELD:
                return null == nsiEntity.getShortTitle();
            default:
                throw new UnsupportedOperationException("Field '" + fieldName + "' not found for '" + nsiEntity.getClass().getSimpleName() + "' entity type.");
        }
    }

    @Override
    public long getNsiEntityHash(T nsiEntity, boolean takeId, boolean takeCode, boolean caseInsensitive)
    {
        Set<String> excludeFieldsSet = new HashSet();
        if (!takeId) excludeFieldsSet.add(ID_NSI_FIELD);
        if (!takeCode) excludeFieldsSet.add(CODE_NSI_FIELD);

        MD5HashBuilder builder = new MD5HashBuilder();
        for (String fieldName : getNsiFields())
        {
            if (!excludeFieldsSet.contains(fieldName))
                builder.add(getNsiFieldHash(nsiEntity, fieldName, caseInsensitive));
        }
        return builder.getCheckSum();
    }

    @Override
    public long getNsiEntityHash(T nsiEntity, boolean caseInsensitive)
    {
        return getNsiEntityHash(nsiEntity, true, true, caseInsensitive);
    }

    @Override
    public long getGuidHash(FefuNsiIds nsiId)
    {
        if (null == nsiId || null == nsiId.getGuid()) return 0;
        return MD5HashBuilder.getCheckSum(nsiId.getGuid().toLowerCase());
    }

    @Override
    public long getCodeHash(E entity)
    {
        if (entity instanceof ICatalogItem)
        {
            IEntityMeta meta = EntityRuntime.getMeta(entity);
            if (null != meta.getProperty(ICatalogItem.CATALOG_ITEM_USER_CODE))
            {
                String userCode = (String) entity.getProperty(ICatalogItem.CATALOG_ITEM_USER_CODE);
                return null != userCode ? MD5HashBuilder.getCheckSum(userCode.replaceAll("nsi.", "")) : 0;
            }

            return MD5HashBuilder.getCheckSum(((ICatalogItem) entity).getCode());
        }
        throw new UnsupportedOperationException("Could not operate with non ICatalogItem entities.");
    }

    @Override
    public long getTitleHash(E entity, boolean caseInsensitive)
    {
        if (entity instanceof ICatalogItem)
        {
            if (caseInsensitive)
                return MD5HashBuilder.getCheckSum(((ICatalogItem) entity).getTitle().trim().toUpperCase());
            return MD5HashBuilder.getCheckSum(((ICatalogItem) entity).getTitle());
        }
        throw new UnsupportedOperationException("Could not operate with non ICatalogItem entities.");
    }

    @Override
    public long getShortTitleHash(E entity, boolean caseInsensitive)
    {
        IEntityMeta meta = EntityRuntime.getMeta(entity);
        if (null != meta.getProperty(SHORT_TITLE_FIELD))
        {
            String shortTitle = (String) entity.getProperty(SHORT_TITLE_FIELD);
            return null != shortTitle ? MD5HashBuilder.getCheckSum(shortTitle.toUpperCase()) : 0;
        }

        return 0;
    }

    @Override
    public long getEntityFieldHash(E entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case GUID_FIELD:
                return getGuidHash(nsiId);
            case ICatalogItem.CATALOG_ITEM_USER_CODE:
                return getCodeHash(entity);
            case ICatalogItem.CATALOG_ITEM_CODE:
                return getCodeHash(entity);
            case ICatalogItem.CATALOG_ITEM_TITLE:
                return getTitleHash(entity, caseInsensitive);
            case SHORT_TITLE_FIELD:
                return getShortTitleHash(entity, caseInsensitive);
            default:
                throw new UnsupportedOperationException("Field '" + fieldName + "' not found for '" + entity.getClass().getSimpleName() + "' entity type.");
        }
    }

    @Override
    public long getEntityHash(E entity, FefuNsiIds nsiIds, boolean takeId, boolean takeCode, boolean caseInsensitive)
    {
        Set<String> excludeFieldsSet = new HashSet();
        if (!takeId) excludeFieldsSet.add(GUID_FIELD);
        if (!takeCode)
        {
            excludeFieldsSet.add(ICatalogItem.CATALOG_ITEM_USER_CODE);
            excludeFieldsSet.add(ICatalogItem.CATALOG_ITEM_CODE);
        }

        MD5HashBuilder builder = new MD5HashBuilder();
        for (String fieldName : getEntityFields())
        {
            if (!excludeFieldsSet.contains(fieldName))
                builder.add(getEntityFieldHash(entity, nsiIds, fieldName, caseInsensitive));
        }
        return builder.getCheckSum();
    }

    @Override
    public long getEntityHash(E entity, FefuNsiIds nsiIds, boolean caseInsensitive)
    {
        return getEntityHash(entity, nsiIds, true, true, caseInsensitive);
    }

    private String getEntityFieldNameByNsiEntityFieldName(String nsiFieldName)
    {
        if (!getNsiToObFieldsMap().containsKey(nsiFieldName))
            throw new UnsupportedOperationException("Field '" + nsiFieldName + "' not found.");
        return getNsiToObFieldsMap().get(nsiFieldName);
    }

    private String getNsiEntityFieldNameByEntityFieldName(String fieldName)
    {
        if (!getObToNsiFieldsMap().containsKey(fieldName))
            throw new UnsupportedOperationException("Field '" + fieldName + "' not found.");
        return getObToNsiFieldsMap().get(fieldName);
    }

    @Override
    public boolean isTwoObjectsIdenticalInGeneral(T nsiEntity, E entity, FefuNsiIds nsiIds)
    {
        return getEntityHash(entity, nsiIds, false, false, true) == getNsiEntityHash(nsiEntity, false, false, true);
    }

    @Override
    public boolean isTwoObjectsIdenticalFieldByField(T nsiEntity, E entity, FefuNsiIds nsiIds)
    {
        // Поскольку идентичность примитивных справочников сравнивается исключительно по названию элемента,
        // то hash сравнения поле-в-поле совпадает с hash'ем всего объекта
        return isTwoObjectsIdenticalInGeneral(nsiEntity, entity, nsiIds);
    }

    @Override
    public boolean isFieldChanged(T nsiEntity, E entity, FefuNsiIds nsiIds, String nsiFieldName)
    {
        if (isFieldEmpty(nsiEntity, nsiFieldName))
            return false; // Поля, в которых сидит null мы не имеем права обновлять, поскольку из НСИ приходят только изменившиеся поля
        return getNsiFieldHash(nsiEntity, nsiFieldName, false) != getEntityFieldHash(entity, nsiIds, getEntityFieldNameByNsiEntityFieldName(nsiFieldName), false);
    }

    @Override
    public boolean isFieldChanged(E entity, T nsiEntity, FefuNsiIds nsiIds, String fieldName)
    {
        if (isFieldEmpty(nsiEntity, getNsiEntityFieldNameByEntityFieldName(fieldName)))
            return false; // Поля, в которых сидит null мы не имеем права обновлять, поскольку из НСИ приходят только изменившиеся поля
        return getEntityFieldHash(entity, nsiIds, fieldName, false) != getNsiFieldHash(nsiEntity, getNsiEntityFieldNameByEntityFieldName(fieldName), false);
    }

    @Override
    public boolean isEntityChanged(T nsiEntity, E entity, FefuNsiIds nsiIds)
    {
        return getEntityHash(entity, nsiIds, false, false, false) != getNsiEntityHash(nsiEntity, false, false, false);
    }

    @Override
    public List<String> getMergeDuplicatesList(T nsiEntity)
    {
        List<String> result = new ArrayList<>();
        String mergeDuplicates = null != nsiEntity.getMergeDublicates() ? nsiEntity.getMergeDublicates() : nsiEntity.getOtherAttributes().get(new QName("mergeDuplicates"));

        if (null != mergeDuplicates)
        {
            String[] duplicates = mergeDuplicates.split(";");
            for (String duplicate : duplicates) result.add(duplicate.trim());
        }

        if (result.size() < 2) return null;
        return result;
    }
}