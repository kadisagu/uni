/* $Id$ */
package ru.tandemservice.unifefu.eduplan.ext.EppEduPlanVersion.ui.BlockPub;

import org.apache.commons.io.FileUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.w3c.dom.Document;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockPub.EppEduPlanVersionBlockPubUI;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.util.FefuEpvRowWrapper;
import ru.tandemservice.unifefu.base.ext.SystemAction.ui.Pub.FefuSystemActionPubAddon;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.dao.eppEduPlan.IImtsaImportDAO;
import ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad.FefuFileLoadManager;
import ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad.ui.Pub.FefuFileLoadPub;
import ru.tandemservice.unifefu.entity.*;
import ru.tandemservice.unifefu.entity.catalog.codes.EppRegistryStructureCodes;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 03.10.2014
 */
public class FefuEduPlanVersionBlockPubAddon extends UIAddon
{

    public static final String EDUPLAN_ID = "eduPlanId";
    private Map<IEppEpvRowWrapper, FefuEpvRowWrapper> _fefuWrapperMap;

    private EppEduPlanVersionBlockPubUI presenter;

    @Override
    public void onComponentRefresh()
    {
        if (getPresenter() instanceof EppEduPlanVersionBlockPubUI)
        {
            presenter = getPresenter();
            Collection<IEppEpvRowWrapper> rows = presenter.getBlockWrapper().getRowMap().values();
            _fefuWrapperMap = getFefuCompetenceWrapperMap(rows);
        }
    }

    public FefuEduPlanVersionBlockPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private EppEduPlanVersionBlock getBlock()
    {
        return ((EppEduPlanVersionBlockPubUI) this.getPresenter()).getBlock();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onClickLoadImtsa()
    {
        this.getActivationBuilder().asRegionDialog("ru.tandemservice.unifefu.component.eduplan.FefuImtsaImport").parameter(PublisherActivator.PUBLISHER_ID_KEY, getBlock().getId()).activate();
    }


    public void onClickReImportImtsa()
    {
        FefuSystemActionPubAddon.doReImportImtsa(Collections.singletonList(getBlock().getId()), false);
        this.getPresenter().getSupport().doRefresh();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onClickLoadFile()
    {
        EppEduPlanVersionBlock block = getBlock();
        if (!(block.getEduPlanVersion().getEduPlan() instanceof EppEduPlanHigherProf))
            throw new ApplicationException("Выгрузка учебных планов, отличных от учебных планов ВПО, запрещена.");

        FefuImtsaXml xml = UnifefuDaoFacade.getUnifefuDAO().getXml(block.getId());
        if (xml != null)
        {
            String encoding = xml.getEncoding();
            if (encoding == null)
            {
                // костыль, нужно для повторной загрузки файлов ИМЦА
                Document document;
                File file;
                for (String pEncoding : Arrays.asList("UTF-8", "windows-1251"))
                {
                    if (encoding != null)
                    {
                        break;
                    }
                    try
                    {
                        file = File.createTempFile("fefuTemp-", "xml");
                        FileUtils.writeStringToFile(file, new String(xml.getXml()), pEncoding);
                        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                        DocumentBuilder db = dbf.newDocumentBuilder();
                        document = db.parse(file);
                        document.getDocumentElement().normalize();
                        encoding = pEncoding;

                    }
                    catch (Exception e)
                    {
                        // continue;
                    }
                }

                if (encoding == null)
                {
                    encoding = "UTF-8";
                }
            }

            String fileName = xml.getFileName();
            if (block instanceof EppEduPlanVersionSpecializationBlock)
            {
                EppEduPlanHigherProf eduPlan = ((EppEduPlanHigherProf) block.getEduPlanVersion().getEduPlan());
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EducationLevelsHighSchool.class, "lhs")
                        .column(property("lhs", EducationLevelsHighSchool.id()))
                        .where(eq(property("lhs", EducationLevelsHighSchool.assignedQualification()), value(eduPlan.getProgramQualification())))
                        .where(eq(property("lhs", EducationLevelsHighSchool.programOrientation()), value(eduPlan.getProgramOrientation())))
                        .where(eq(property("lhs", EducationLevelsHighSchool.educationLevel().eduProgramSpecialization()), value(((EppEduPlanVersionSpecializationBlock) block).getProgramSpecialization())));

                Collection<Long> eduLevels = IUniBaseDao.instance.get().getList(builder);
                if (eduLevels.size() != 1)
                    throw new ApplicationException("Невозможно найти учебный план. Проверьте корректность параметров обучения студентов.");

                Long eduLevelId = eduLevels.iterator().next();
                Long versionId = block.getEduPlanVersion().getId();

                fileName = Long.toHexString(versionId) + "-" + Long.toHexString(eduLevelId) + "-" + String.valueOf(xml.getNumber()) + "-" + fileName;
            }

            byte[] xmlContent = xml.getXml();
            try
            {
                xmlContent = new String(xmlContent, "UTF-8").getBytes(encoding);

            }
            catch (UnsupportedEncodingException e)
            {
                //

            }
            finally
            {
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().fileName(fileName).document(xmlContent), false);
            }
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onClickImportXml()
    {
        this.getActivationBuilder().asRegionDialog(FefuFileLoadPub.class).parameter(UIPresenter.PUBLISHER_ID, getBlock().getId()).activate();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onClickExportXml()
    {

        try
        {
            byte[] result = FefuFileLoadManager.instance().dao().createXMLFromEduBlock(getBlock().getId()).getBytes("UTF-8");
            String fileName = getBlock().getTitle();
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().fileName(fileName + ".xml").document(result), false);
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
    }

    public void onClickEditFefuCompetence()
    {
        getActivationBuilder().asCurrent("ru.tandemservice.unifefu.component.eduplan.row.FefuCompetenceAddEdit")
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong())
                .parameter(EDUPLAN_ID, getBlock().getEduPlanVersion().getEduPlan().getId()).activate();
    }

    @SuppressWarnings("UnusedDeclaration")
    public boolean isImtsaButtonsVisible()
    {
        return getBlock().getEduPlanVersion().getEduPlan() instanceof EppEduPlanHigherProf;
    }

    private Map<IEppEpvRowWrapper, FefuEpvRowWrapper> getFefuCompetenceWrapperMap(Collection<IEppEpvRowWrapper> wrappers)
    {
        Map<Long, List<String>> compByRowMap = getCompetencesByRegisterRow(wrappers);

        return wrappers.stream().collect(Collectors.toMap(
                wrapper -> wrapper,
                wrapper -> {
                    EppEpvRow row = (EppEpvRow) wrapper.getRow();
                    List<String> competences = compByRowMap.get(row.getId());
                    if (competences == null) competences = new ArrayList<>();
                    return new FefuEpvRowWrapper(row, competences)
                    {
                        @Override
                        public IHierarchyItem getHierarhyParent()
                        {
                            return null;
                        }
                    };
                }
        ));
    }

    private Map<Long, List<String>> getCompetencesByRegisterRow(Collection<IEppEpvRowWrapper> wrappers)
    {
        List<Long> rowIds = wrappers.stream()
                .map(IEppEpvRowWrapper::getRow)
                .filter(row -> row instanceof EppEpvRegistryRow)
                .map(IIdentifiable::getId)
                .collect(Collectors.toList());

        String cm2row_alias = "row";
        List<FefuAbstractCompetence2EpvRegistryRowRel> competence2EpvRegistryRowRels =
                new DQLSelectBuilder()
                        .fromEntity(FefuAbstractCompetence2EpvRegistryRowRel.class, cm2row_alias)
                        .column(property(cm2row_alias))
                        .where(in(property(cm2row_alias, FefuAbstractCompetence2EpvRegistryRowRel.registryRow()), rowIds))
                        .createStatement(getSession()).list();

        return competence2EpvRegistryRowRels.stream()
                .collect(Collectors.groupingBy(
                        rel->rel.getRegistryRow().getId(),
                        Collectors.mapping(FefuAbstractCompetence2EpvRegistryRowRel::getGroupCode, Collectors.toList())
                ));
    }

    public boolean isFefuCompetenceEditEnabled()
    {
        EppEduPlanVersionBlockPubUI pr = getPresenter();
        IEppEpvRowWrapper wrapper = pr.getRowWrapper();
        return (wrapper.getRow() instanceof EppEpvRegistryRow) && getBlock().equals(wrapper.getRow().getOwner());
    }

    public String getFefuCompetenceDisplayableTitle()
    {
        EppEduPlanVersionBlockPubUI pr = getPresenter();
        IEppEpvRowWrapper rowWrapper = pr.getRowWrapper();

        FefuEpvRowWrapper fefuWrapper = _fefuWrapperMap.get(rowWrapper);
        return fefuWrapper == null ? "" : fefuWrapper.getCompetences();
    }


    public void onClickCreateRegistryElements()
    {
        List<Long> registryRowIds = getRegistryRowIds();
        IImtsaImportDAO.instance.get().createRegistryElements(registryRowIds, false);
    }

    private List<Long> getRegistryRowIds()
    {
        List<String> codes = Arrays.asList(EppRegistryStructureCodes.REGISTRY_PRACTICE, EppRegistryStructureCodes.REGISTRY_ATTESTATION);
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "r")
                .column(property("r", EppEpvRegistryRow.id()))
                .joinPath(DQLJoinType.inner, EppEpvRegistryRow.owner().fromAlias("r"), "b")
                .where(eq(property("b.id"), value(getBlock().getId())))
                .where(isNull(property("r", EppEpvRegistryRow.registryElement().id())))
                .where(or(in(property("r", EppEpvRegistryRow.registryElementType().code()), codes),
                          in(property("r", EppEpvRegistryRow.registryElementType().parent().code()), codes)));
        return builder.createStatement(getSession()).list();
    }

}