/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nvankov
 * @since 7/2/13
 */
public class FefuGiveDiplomaWithDismissParagraphWrapper implements Comparable<FefuGiveDiplomaWithDismissParagraphWrapper>
{
    private final StudentCategory _studentCategory;
    private final CompensationType _compensationType;
    private final Course _course;
    private final DevelopForm _developForm;
    private final DevelopCondition _developCondition;
    private final DevelopTech _developTech;
    private final DevelopPeriod _developPeriod;
    private final EducationLevels _educationLevels;
    private final EduProgramQualification _qualification;
    private final OrgUnit _formativeOrgUnit;
    private final OrgUnit _territorialOrgUnit;
    private final String _eduBaseText;
    private final boolean _printDiplomaQualification;
    private final ListStudentExtract _firstExtract;


    public FefuGiveDiplomaWithDismissParagraphWrapper(StudentCategory studentCategory, CompensationType compensationType, Course course, DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod, EducationLevels educationLevels, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit, String eduBaseText, boolean printDiplomaQualification, EduProgramQualification qualification, ListStudentExtract firstExtract)
    {
        _studentCategory = studentCategory;
        _compensationType = compensationType;
        _course = course;
        _developForm = developForm;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
        _educationLevels = educationLevels;
        _formativeOrgUnit = formativeOrgUnit;
        _territorialOrgUnit = territorialOrgUnit;
        _eduBaseText = eduBaseText;
        _printDiplomaQualification = printDiplomaQualification;
        _qualification = qualification;
        _firstExtract = firstExtract;
    }

    private final List<FefuGiveDiplomaWithDismissParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<>();


    public List<FefuGiveDiplomaWithDismissParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }


    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public Course getCourse()
    {
        return _course;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public EducationLevels getEducationLevels()
    {
        return _educationLevels;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public String getEduBaseText()
    {
        return _eduBaseText;
    }

    public boolean isPrintDiplomaQualification()
    {
        return _printDiplomaQualification;
    }

    public EduProgramQualification getQualification()
    {
        return _qualification;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuGiveDiplomaWithDismissParagraphWrapper))
            return false;

        FefuGiveDiplomaWithDismissParagraphWrapper that = (FefuGiveDiplomaWithDismissParagraphWrapper) o;


        return _compensationType.equals(that.getCompensationType()) && _studentCategory.equals(that.getStudentCategory());
    }

    @Override
    public int hashCode()
    {
        return _compensationType.hashCode() & _studentCategory.hashCode();
    }

    @Override
    public int compareTo(FefuGiveDiplomaWithDismissParagraphWrapper o)
    {
        if(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(o.getStudentCategory().getCode()))
            return -1;
        else if(StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(o.getStudentCategory().getCode()))
            return 1;
        else
        {
            if(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(o.getCompensationType().getCode())) return -1;
            else if(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(o.getCompensationType().getCode())) return 1;
            else return 0;
        }

    }
}
