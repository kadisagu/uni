/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu3;

import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.unifefu.entity.FefuReEducationStuExtract;

import java.util.Collections;

/**
 * @author Dmitry Seleznev
 * @since 22.08.2012
 */
public class FefuReEducationStuExtractPrint implements IPrintFormCreator<FefuReEducationStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuReEducationStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        CommonExtractPrint.initOrgUnit(modifier, extract.getEducationOrgUnitOld(), "formativeOrgUnitStrOld", "");

        if (extract.isAnnulAttestationResults())
        {
            StringBuilder builder = new StringBuilder();
            builder.append("Аннулировать аттестации по всем дисциплинам, прослушанным на ");
            builder.append(extract.getPrevCourse().getTitle()).append(" курсе в ");
            builder.append(extract.getEduYear().getTitle()).append(" уч. году.");
            modifier.put("annulAttestation", builder.toString());
        }
        else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("annulAttestation"), true, false);
        }
        CommonExtractPrint.initFefuGroup(modifier, "inGroupNew", extract.getGroupNew(), extract.getEducationOrgUnitNew().getDevelopForm(), " в группе ");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}