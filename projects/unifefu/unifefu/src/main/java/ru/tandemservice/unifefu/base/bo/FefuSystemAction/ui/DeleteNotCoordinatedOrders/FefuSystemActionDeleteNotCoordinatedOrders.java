/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.DeleteNotCoordinatedOrders;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic.FefuNotCoordinatedOrdersHandler;

/**
 * @author Ekaterina Zvereva
 * @since 16.01.2015
 */
@Configuration
public class FefuSystemActionDeleteNotCoordinatedOrders extends BusinessComponentManager
{

    public static final String ORDERS_DS = "ordersListDS";
    public static final String ORDERS_TYPES_DS = "ordersTypes";
    public static final String DATE_FROM = "ordersDateFrom";
    public static final String DATE_TO = "ordersDateTo";

    public static final String STUDENT_FIO = "studentFIO";
    public static final String STUDENT_GROUP = "studentGroup";

    public static final String ORDER_TYPE = "orderType";



    @Bean
    public ColumnListExtPoint ordersDSColumnList()
    {
        return columnListExtPointBuilder(ORDERS_DS).

                addColumn(checkboxColumn("select").required(true)).
                addColumn(textColumn("number", AbstractStudentOrder.number()).width("100px")).
                addColumn(publisherColumn("createDate", AbstractStudentOrder.createDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).required(true)).
                addColumn(textColumn("state", AbstractStudentOrder.state().title()).width("100px")).
                addColumn(textColumn("type", ORDER_TYPE)).
                addColumn(textColumn(STUDENT_FIO, STUDENT_FIO))
                .addColumn(textColumn(STUDENT_GROUP, STUDENT_GROUP))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder().
                addDataSource(searchListDS(ORDERS_DS, ordersDSColumnList(), fefuOrdersNotCoordinatesDSHandler()))
        .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> fefuOrdersNotCoordinatesDSHandler()
    {
        return new FefuNotCoordinatedOrdersHandler(getName());
    }

}