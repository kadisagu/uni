package ru.tandemservice.unifefu.entity.blackboard.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse2EppRegElementRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь ЭУК с записью реестра
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class BbCourse2EppRegElementRelGen extends EntityBase
 implements INaturalIdentifiable<BbCourse2EppRegElementRelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.blackboard.BbCourse2EppRegElementRel";
    public static final String ENTITY_NAME = "bbCourse2EppRegElementRel";
    public static final int VERSION_HASH = -1823332379;
    private static IEntityMeta ENTITY_META;

    public static final String L_BB_COURSE = "bbCourse";
    public static final String L_EPP_REGISTRY_ELEMENT = "eppRegistryElement";

    private BbCourse _bbCourse;     // Электронный учебный курс (ЭУК) в системе Blackboard
    private EppRegistryElement _eppRegistryElement;     // Элемент реестра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     */
    @NotNull
    public BbCourse getBbCourse()
    {
        return _bbCourse;
    }

    /**
     * @param bbCourse Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     */
    public void setBbCourse(BbCourse bbCourse)
    {
        dirty(_bbCourse, bbCourse);
        _bbCourse = bbCourse;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getEppRegistryElement()
    {
        return _eppRegistryElement;
    }

    /**
     * @param eppRegistryElement Элемент реестра. Свойство не может быть null.
     */
    public void setEppRegistryElement(EppRegistryElement eppRegistryElement)
    {
        dirty(_eppRegistryElement, eppRegistryElement);
        _eppRegistryElement = eppRegistryElement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof BbCourse2EppRegElementRelGen)
        {
            if (withNaturalIdProperties)
            {
                setBbCourse(((BbCourse2EppRegElementRel)another).getBbCourse());
                setEppRegistryElement(((BbCourse2EppRegElementRel)another).getEppRegistryElement());
            }
        }
    }

    public INaturalId<BbCourse2EppRegElementRelGen> getNaturalId()
    {
        return new NaturalId(getBbCourse(), getEppRegistryElement());
    }

    public static class NaturalId extends NaturalIdBase<BbCourse2EppRegElementRelGen>
    {
        private static final String PROXY_NAME = "BbCourse2EppRegElementRelNaturalProxy";

        private Long _bbCourse;
        private Long _eppRegistryElement;

        public NaturalId()
        {}

        public NaturalId(BbCourse bbCourse, EppRegistryElement eppRegistryElement)
        {
            _bbCourse = ((IEntity) bbCourse).getId();
            _eppRegistryElement = ((IEntity) eppRegistryElement).getId();
        }

        public Long getBbCourse()
        {
            return _bbCourse;
        }

        public void setBbCourse(Long bbCourse)
        {
            _bbCourse = bbCourse;
        }

        public Long getEppRegistryElement()
        {
            return _eppRegistryElement;
        }

        public void setEppRegistryElement(Long eppRegistryElement)
        {
            _eppRegistryElement = eppRegistryElement;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof BbCourse2EppRegElementRelGen.NaturalId) ) return false;

            BbCourse2EppRegElementRelGen.NaturalId that = (NaturalId) o;

            if( !equals(getBbCourse(), that.getBbCourse()) ) return false;
            if( !equals(getEppRegistryElement(), that.getEppRegistryElement()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBbCourse());
            result = hashCode(result, getEppRegistryElement());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBbCourse());
            sb.append("/");
            sb.append(getEppRegistryElement());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends BbCourse2EppRegElementRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) BbCourse2EppRegElementRel.class;
        }

        public T newInstance()
        {
            return (T) new BbCourse2EppRegElementRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "bbCourse":
                    return obj.getBbCourse();
                case "eppRegistryElement":
                    return obj.getEppRegistryElement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "bbCourse":
                    obj.setBbCourse((BbCourse) value);
                    return;
                case "eppRegistryElement":
                    obj.setEppRegistryElement((EppRegistryElement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "bbCourse":
                        return true;
                case "eppRegistryElement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "bbCourse":
                    return true;
                case "eppRegistryElement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "bbCourse":
                    return BbCourse.class;
                case "eppRegistryElement":
                    return EppRegistryElement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<BbCourse2EppRegElementRel> _dslPath = new Path<BbCourse2EppRegElementRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "BbCourse2EppRegElementRel");
    }
            

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2EppRegElementRel#getBbCourse()
     */
    public static BbCourse.Path<BbCourse> bbCourse()
    {
        return _dslPath.bbCourse();
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2EppRegElementRel#getEppRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> eppRegistryElement()
    {
        return _dslPath.eppRegistryElement();
    }

    public static class Path<E extends BbCourse2EppRegElementRel> extends EntityPath<E>
    {
        private BbCourse.Path<BbCourse> _bbCourse;
        private EppRegistryElement.Path<EppRegistryElement> _eppRegistryElement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2EppRegElementRel#getBbCourse()
     */
        public BbCourse.Path<BbCourse> bbCourse()
        {
            if(_bbCourse == null )
                _bbCourse = new BbCourse.Path<BbCourse>(L_BB_COURSE, this);
            return _bbCourse;
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2EppRegElementRel#getEppRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> eppRegistryElement()
        {
            if(_eppRegistryElement == null )
                _eppRegistryElement = new EppRegistryElement.Path<EppRegistryElement>(L_EPP_REGISTRY_ELEMENT, this);
            return _eppRegistryElement;
        }

        public Class getEntityClass()
        {
            return BbCourse2EppRegElementRel.class;
        }

        public String getEntityName()
        {
            return "bbCourse2EppRegElementRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
