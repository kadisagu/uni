/* $Id$ */
package ru.tandemservice.unifefu.report.externalView;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Avetisov
 * @since 17.04.2014
 */
public class EppStudIDsExtViewProvider extends SimpleDQLExternalViewConfig
{
    @Override
    protected DQLSelectBuilder buildDqlQuery()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, "stdEduWrkPlan");

        column(dql, property("stdEduWrkPlan", EppStudent2WorkPlan.studentEduPlanVersion().student().id()), "studentId").comment("id_студента");
        column(dql, property("stdEduWrkPlan", EppStudent2WorkPlan.L_WORK_PLAN), "workPlanId").comment("id_РУП");

        return dql;
    }
}
