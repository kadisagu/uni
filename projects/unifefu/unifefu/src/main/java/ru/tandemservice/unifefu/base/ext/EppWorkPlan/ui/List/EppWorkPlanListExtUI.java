/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EppWorkPlan.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.WorkPlanAutoCreate.FefuWorkGraphWorkPlanAutoCreate;

/**
 * @author Denis Katkov
 * @since 10.03.2016
 */
public class EppWorkPlanListExtUI extends UIAddon
{
    public EppWorkPlanListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickAddByGUPFefu()
    {
        getActivationBuilder().asRegionDialog(FefuWorkGraphWorkPlanAutoCreate.class).activate();
    }
}