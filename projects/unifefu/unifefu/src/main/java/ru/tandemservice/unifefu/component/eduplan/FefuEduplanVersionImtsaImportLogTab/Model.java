/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduplanVersionImtsaImportLogTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 05.09.2013
 */
@Input(
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id", required = true)
)
public class Model
{
    private long _id;
    private Map<EppEduPlanVersionBlock, String[]> _logMap;
    private List<EppEduPlanVersionBlock> _blocks;
    private EppEduPlanVersionBlock _currentBlock;
    private String _currentLogMessage;

    public long getId(){ return _id; }
    public void setId(long id){ _id = id; }

    public Map<EppEduPlanVersionBlock, String[]> getLogMap(){ return _logMap; }
    public void setLogMap(Map<EppEduPlanVersionBlock, String[]> logMap){ _logMap = logMap; }

    public List<EppEduPlanVersionBlock> getBlocks(){ return _blocks; }
    public void setBlocks(List<EppEduPlanVersionBlock> blocks){ _blocks = blocks; }

    public EppEduPlanVersionBlock getCurrentBlock(){ return _currentBlock; }
    public void setCurrentBlock(EppEduPlanVersionBlock currentBlock){ _currentBlock = currentBlock; }

    public String getCurrentLogMessage(){ return _currentLogMessage; }
    public void setCurrentLogMessage(String currentLogMessage){ _currentLogMessage = currentLogMessage; }

    public String[] getMessages(){ return _logMap.get(_currentBlock); }
}