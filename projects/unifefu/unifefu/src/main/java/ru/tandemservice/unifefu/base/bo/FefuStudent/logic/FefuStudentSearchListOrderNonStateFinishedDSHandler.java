/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.list.FefuStudentSearchListDSHandler;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class FefuStudentSearchListOrderNonStateFinishedDSHandler extends FefuStudentSearchListDSHandler
{
    public static final String ORDER_NON_STATE_FINISHED = "orderNonStateFinished";

    public FefuStudentSearchListOrderNonStateFinishedDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = prepareDQL(context);

        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "e")
                .joinPath(DQLJoinType.left, AbstractStudentExtract.paragraph().fromAlias("e"), "p")
                .joinPath(DQLJoinType.left, AbstractStudentParagraph.order().fromAlias("p"), "o")
                .where(or(
                        isNull(property(AbstractStudentExtract.paragraph().id().fromAlias("e"))),
                        ne(property(AbstractStudentOrder.state().code().fromAlias("o")), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED))))
                .where(eq(property(AbstractStudentExtract.entity().id().fromAlias("e")), property("s.id")))
                .column(DQLFunctions.count(property("e.id")));

        builder.column(subBuilder.buildQuery());

        _orderDescriptionRegistry.applyOrder(builder, input.getEntityOrder());
        addAdditionalRestrictions(builder, "s", input, context);

        DQLExecutionContext dqlContext = new DQLExecutionContext(context.getSession());
        int totalSize = ((Long) builder.createCountStatement(dqlContext).uniqueResult()).intValue();
        int startRec = input.getStartRecord();
        int countRec = input.getCountRecord();

        if(-1 == startRec || totalSize < startRec)
        {
            startRec = totalSize - (0 != totalSize % countRec ? totalSize % countRec : countRec);
        }

        IDQLStatement statement = builder.createStatement(context.getSession());
        statement.setFirstResult(startRec);
        statement.setMaxResults(countRec);

        List<Long> studentIds = new ArrayList<>();
        List<Object> recordList = new ArrayList<>();

        for (Object[] item : statement.<Object[]>list())
        {
            Student student = (Student) item[0];
            Long orderNonStateFinished = (Long) item[1];

            DataWrapper dataWrapper = new DataWrapper(student);
            dataWrapper.put(ORDER_NON_STATE_FINISHED, orderNonStateFinished > 0);

            studentIds.add(student.getId());
            recordList.add(dataWrapper);
        }

        prepareWrappData(studentIds, input, context);

        DSOutput output = new DSOutput(input);
        output.setRecordList(recordList);
        output.setTotalSize(totalSize);

        for (DataWrapper student : DataWrapper.wrap(output))
            wrap(student, context);

        return output;
    }
}
