/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.ErrorPage;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationProperties;

/**
 * @author Dmitry Seleznev
 * @since 05.03.2014
 */
public class DirectumErrorPageUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        //throw new ApplicationException("Страница не найдена.");
    }

    public boolean isAdrenalineDisabled()
    {
        return !ApplicationProperties.ADRENALINE_ENABLED;
    }
}