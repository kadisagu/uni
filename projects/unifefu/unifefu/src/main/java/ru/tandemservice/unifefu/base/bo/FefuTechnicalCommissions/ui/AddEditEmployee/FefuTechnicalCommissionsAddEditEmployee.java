/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.AddEditEmployee;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.View.FefuTechnicalCommissionsView;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 11.06.2013
 */
@Configuration
public class FefuTechnicalCommissionsAddEditEmployee extends BusinessComponentManager
{
    public static final String EMPLOYEE_COMBO_DS = "employeeComboDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(EMPLOYEE_COMBO_DS, employeeComboDSHandler()).addColumn(EmployeePost.fullTitle().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> employeeComboDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EmployeePost.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                FefuTechnicalCommission technicalCommission = context.get(FefuTechnicalCommissionsView.TECHNICAL_COMMISSION_PARAM);

                // Выводим всех сотрудников, кроме уже добавленных
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuTechnicalCommissionEmployee.class, "x")
                        .column(property(FefuTechnicalCommissionEmployee.employeePost().fromAlias("x")))
                        .where(eq(property(FefuTechnicalCommissionEmployee.technicalCommission().fromAlias("x")), value(technicalCommission)));

                dql.where(
                        notIn(property(EmployeePost.id().fromAlias(alias)), builder.buildQuery())
                );
            }
        };
        handler.order(EmployeePost.person().identityCard().fullFio());
        handler.filter(EmployeePost.person().identityCard().fullFio());
        handler.setPageable(true);
        return handler;
    }
}