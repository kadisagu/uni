/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 15.05.2015
 */
public class Model extends AbstractListParagraphAddEditModel<FefuTransfSpecialityStuListExtract> implements IEducationLevelModel, IExtEducationLevelModel, IGroupModel
{
    private Course _course;
    private Group _group;
    private ISelectModel _groupListModel;

    private ISelectModel _specializationsListModel;
    private EducationLevelsHighSchool _oldEduLevelHighSchool;
    private EducationLevelsHighSchool _educationLevelsHighSchool;

    private List<CompensationType> _compensationTypeList;
    private CompensationType _compensationType;

    private ISelectModel _groupNewListModel;
    private Group _groupNew;

    private EducationLevels _parentEduLevel;

    private DevelopForm _developForm;
    private DevelopTech _developTech;
    private DevelopCondition _developCondition;
    private DevelopPeriod _developPeriod;

    private boolean captainTransfer;

    public boolean isCaptainTransfer()
    {
        return captainTransfer;
    }

    public void setCaptainTransfer(boolean captainTransfer)
    {
        this.captainTransfer = captainTransfer;
    }

    public ISelectModel getGroupNewListModel()
    {
        return _groupNewListModel;
    }

    public void setGroupNewListModel(ISelectModel groupNewListModel)
    {
        _groupNewListModel = groupNewListModel;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    public void setGroupNew(Group groupNew)
    {
        _groupNew = groupNew;
    }

    public EducationLevelsHighSchool getOldEduLevelHighSchool()
    {
        return _oldEduLevelHighSchool;
    }

    public void setOldEduLevelHighSchool(EducationLevelsHighSchool oldEduLevelHighSchool)
    {
        _oldEduLevelHighSchool = oldEduLevelHighSchool;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public ISelectModel getSpecializationsListModel()
    {
        return _specializationsListModel;
    }

    public void setSpecializationsListModel(ISelectModel specializationsListModel)
    {
        _specializationsListModel = specializationsListModel;
    }


    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public EducationLevels getParentEduLevel()
    {
        return (getGroupNew() != null && getGroupNew().getEducationOrgUnit() != null)? EducationOrgUnitUtil.getParentLevel(getGroupNew().getEducationOrgUnit().getEducationLevelHighSchool()) : null;
    }

    public void setParentEduLevel(EducationLevels parentEduLevel)
    {
        _parentEduLevel = parentEduLevel;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getFormativeOrgUnit() : null;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getTerritorialOrgUnit() : null;
    }

    public boolean isAnyOrderParametersCantBeChanged()
    {
        return !isParagraphOnlyOneInTheOrder();
    }
}