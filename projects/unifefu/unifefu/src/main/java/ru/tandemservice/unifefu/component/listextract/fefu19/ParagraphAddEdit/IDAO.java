/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 15.05.2015
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuTransfSpecialityStuListExtract, Model>
{
}
