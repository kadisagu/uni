/* $Id$ */
package ru.tandemservice.unifefu.component.eduplan.EduPlanVersionAddRowFromOther;

import com.google.common.base.Preconditions;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuAbstractCompetence2EpvRegistryRowRel;
import ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel;
import ru.tandemservice.unifefu.entity.FefuEpvRowCompetence2EppStateEduStandardRel;
import ru.tandemservice.unifefu.entity.FefuImtsaCompetence2EpvRegistryRowRel;
import ru.tandemservice.unifefu.entity.eduPlan.FefuPracticeDispersion;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 24.10.2014
 */
public class DAO extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddRowFromOther.DAO
{
    @Override
    protected void updateCopiedRows(Map<Long, Long> rowMap)
    {
        super.updateCopiedRows(rowMap);

        if (rowMap.isEmpty())
            return;

        // при сохранении строк необходимо также скопировать
        //  компетенции
        //  рассредоточенность практик

        updateEpvPracticeDispersion(rowMap);
        updateEpvRowCompetences(rowMap);
    }

    /** Сохраняет данные о рассредоточенности практик УП. */
    private void updateEpvPracticeDispersion(@NotNull Map<Long, Long> rowMap)
    {
        Preconditions.checkNotNull(rowMap);
        new DQLDeleteBuilder(FefuPracticeDispersion.class).where(in(property(FefuPracticeDispersion.practice()), rowMap.values())).createStatement(this.getSession()).execute();

        Collection<Long> sourceRowDispersedPractices = this.getList(
            new DQLSelectBuilder()
                .fromEntity(FefuPracticeDispersion.class, "d")
                .column(property("d", FefuPracticeDispersion.practice().id()))
                .where(eq(property("d", FefuPracticeDispersion.dispersed()), value(Boolean.TRUE)))
                .where(in(property("d", FefuPracticeDispersion.practice()), rowMap.keySet())));

        if (sourceRowDispersedPractices.isEmpty())
            return;

        short entityCode = EntityRuntime.getMeta(FefuPracticeDispersion.class).getEntityCode();
        DQLInsertValuesBuilder builder = new DQLInsertValuesBuilder(FefuPracticeDispersion.class);

        for (Long sourceRow: sourceRowDispersedPractices)
        {
            builder.value(FefuPracticeDispersion.P_ID, EntityIDGenerator.generateNewId(entityCode));
            builder.value(FefuPracticeDispersion.L_PRACTICE, rowMap.get(sourceRow));
            builder.value(FefuPracticeDispersion.P_DISPERSED, Boolean.TRUE);
            builder.addBatch();
        }

        builder.createStatement(this.getSession()).execute();
    }

    /** Сохраняет компетенции строк УП. */
    private void updateEpvRowCompetences(@NotNull Map<Long, Long> rowMap)
    {
        Preconditions.checkNotNull(rowMap);
        new DQLDeleteBuilder(FefuAbstractCompetence2EpvRegistryRowRel.class).where(in(property(FefuAbstractCompetence2EpvRegistryRowRel.registryRow()), rowMap.values())).createStatement(this.getSession()).execute();

        List<FefuAbstractCompetence2EpvRegistryRowRel> sourceRels = getList(
                new DQLSelectBuilder()
                        .fromEntity(FefuAbstractCompetence2EpvRegistryRowRel.class, "rel")
                        .column(property("rel"))
                        .where(in(property("rel", FefuAbstractCompetence2EpvRegistryRowRel.registryRow()), rowMap.keySet())));

        if (sourceRels.isEmpty())
            return;

        String row_alias = "row";
        String srel_alias = "srel";
        DQLSelectBuilder stdBuilder = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, row_alias)
                .column(property(row_alias, EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().parent().id()))
                .where(in(property(row_alias, EppEpvRegistryRow.id()), rowMap.values()));
        DQLSelectBuilder stdRelsBuilder = new DQLSelectBuilder()
                .fromEntity(FefuCompetence2EppStateEduStandardRel.class, srel_alias)
                .column(property(srel_alias))
                .where(in(property(srel_alias, FefuCompetence2EppStateEduStandardRel.eduStandard().id()), stdBuilder.buildQuery()));
        List<FefuCompetence2EppStateEduStandardRel> stdRels = getList(stdRelsBuilder);
        Map<CoreCollectionUtils.Pair<Long, Long>, FefuCompetence2EppStateEduStandardRel> relByCmpAndStd = stdRels.stream()
                .collect(Collectors.toMap(
                                 rel -> new CoreCollectionUtils.Pair<Long, Long>(rel.getFefuCompetence().getId(), rel.getEduStandard().getId()),
                                 rel -> rel)
                );

        for (FefuAbstractCompetence2EpvRegistryRowRel sourceRel: sourceRels)
        {
            Long sourceRow = sourceRel.getRegistryRow().getId();

            Long newRowId = rowMap.get(sourceRow);
            if (newRowId == null) continue;

            EppEpvRegistryRow newRow = get(EppEpvRegistryRow.class, newRowId);
            if (newRow == null) continue;

            if (sourceRel instanceof FefuImtsaCompetence2EpvRegistryRowRel)
            {
                FefuImtsaCompetence2EpvRegistryRowRel newRel = new FefuImtsaCompetence2EpvRegistryRowRel(
                        newRow, sourceRel.getFefuCompetence(), sourceRel.getGroupNumber());
                save(newRel);
            }
            else if (sourceRel instanceof FefuEpvRowCompetence2EppStateEduStandardRel)
            {
                EppStateEduStandard eduStandard = newRow.getOwner().getEduPlanVersion().getEduPlan().getParent();
                if(eduStandard == null) continue;

                FefuCompetence2EppStateEduStandardRel stdRel = relByCmpAndStd.get(new CoreCollectionUtils.Pair<>(sourceRel.getFefuCompetence().getId(), eduStandard.getId()));
                if (stdRel == null) continue;

                FefuEpvRowCompetence2EppStateEduStandardRel newRel = new FefuEpvRowCompetence2EppStateEduStandardRel(stdRel, newRow);
                save(newRel);
            }
        }
    }
}