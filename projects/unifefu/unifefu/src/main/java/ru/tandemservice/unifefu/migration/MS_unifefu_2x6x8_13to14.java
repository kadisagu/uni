package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x8_13to14 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuAdditionalProfessionalEducationProgram

		// удалено свойство ownerOrgUnit
        if (tool.columnExists("fefu_ape_program_t", "ownerorgunit_id"))
		{
			// удалить колонку
			tool.dropColumn("fefu_ape_program_t", "ownerorgunit_id");
		}

		// создано обязательное свойство producingOrgUnit
        if (!tool.columnExists("fefu_ape_program_t", "producingorgunit_id"))
		{
			// создать колонку
			tool.createColumn("fefu_ape_program_t", new DBColumn("producingorgunit_id", DBType.LONG));
			// сделать колонку NOT NULL
			tool.setColumnNullable("fefu_ape_program_t", "producingorgunit_id", false);

		}
    }
}