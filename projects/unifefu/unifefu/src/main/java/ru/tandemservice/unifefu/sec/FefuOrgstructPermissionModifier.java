/* $Id$ */
package ru.tandemservice.unifefu.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.unisession.entity.document.SessionListDocument;


import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 13.05.2013
 */
public class FefuOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unifefu");
        config.setName("unifefu-sec-config");
        config.setTitle("");

//
//        // права на сам объект «Расписание ДВФУ»
//
//        PermissionGroupMeta pg = CtrContractVersionUtil.registerPermissionGroup(config, FefuSchedule.class);
//        PermissionMetaUtil.createGroupRelation(config, pg.getName(), "fefuScheduleCG");
//        PermissionMetaUtil.createGroupRelation(config, pg.getName(), "fefuScheduleLC");
	    final ModuleLocalGroupMeta mlgSession = PermissionMetaUtil.createModuleLocalGroup(config, "unisessionLocal", "Модуль «Сессия»");
	    final ClassGroupMeta lcgListDocument = PermissionMetaUtil.createClassGroup(mlgSession, "listDocumentLocalClass", "Объект «Индивидуальная ведомость»", SessionListDocument.class.getName());
	    PermissionMetaUtil.createGroupRelation(config, "unisessionListDocumentPG", lcgListDocument.getName());

        ModuleGlobalGroupMeta moduleGlobalGroupMeta = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        ModuleLocalGroupMeta moduleLocalGroupMeta = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        // для каждого типа подразделения добавляем права на вкладку «студенты / договоры» (вкладка непосредственно подразделения)
        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();

            ClassGroupMeta globalClassGroup = PermissionMetaUtil.createClassGroup(moduleGlobalGroupMeta, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            ClassGroupMeta localClassGroup = PermissionMetaUtil.createClassGroup(moduleLocalGroupMeta, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            // "Вкладка «Студенты»"
            final PermissionGroupMeta permissionGroupStudentsTab = PermissionMetaUtil.createPermissionGroup(config, code + "StudentsPermissionGroup", "Вкладка «Студенты»");

            PermissionGroupMeta pgEntrantTab = PermissionMetaUtil.createPermissionGroup(config, code + "EntrantsTabPermissionGroup", "Вкладка «Абитуриенты»");
            PermissionMetaUtil.createPermission(pgEntrantTab, "orgUnit_fefuAddEntrant_" + code, "Добавление абитуриента");
            PermissionMetaUtil.createPermission(pgEntrantTab, "orgUnit_fefuAddEntrantWizard_" + code, "Мастер добавления абитуриента");

            // Вкладка «Расписание»
            PermissionGroupMeta pgSppScheduleTab = PermissionMetaUtil.createPermissionGroup(config, code + "SppScheduleOrgUnitTabPermissionGroup", "Вкладка «Расписание»");
//            PermissionGroupMeta pgFefuScheduleTab = PermissionMetaUtil.createPermissionGroup(config, code + "FefuScheduleOrgUnitTabPermissionGroup", "Вкладка «Расписание»");




            // добавляем права на кнопку «Изменение реквизитов лица, подписывающего приказы по студентам»
            final PermissionGroupMeta permissionOrgUnitSignatoryPublisher = PermissionMetaUtil.createPermissionGroup(config, code + "PublisherPermissionGroup", "Вкладка «" + description.getTitle() + "»");
            PermissionMetaUtil.createPermission(permissionOrgUnitSignatoryPublisher, "unifefuOrgUnitSignatoryEdit_" + code, "Изменение реквизитов лица, подписывающего приказы по студентам");

            // "Вкладка «Учебный процесс»"
            final PermissionGroupMeta pgEppTab = PermissionMetaUtil.createPermissionGroup(config, code + "EppPG", "Вкладка «Учебный процесс»");
            final PermissionGroupMeta pgEpvAcceptionTab = PermissionMetaUtil.createPermissionGroup(pgEppTab, code + "EppEpvAttestationPG", "Вкладка «Согласование УП»");

            PermissionMetaUtil.createPermission(pgEpvAcceptionTab, "createRegistry_eppEpvAcception_list_" + code, "Создание мероприятий реестра пустым строкам");

            // Вкладка "Журналы"
            final PermissionGroupMeta pgTrainingTab = PermissionMetaUtil.createPermissionGroup(config, code + "TrainingTabPermissionGroup", "Вкладка «Журналы»");
            final PermissionGroupMeta pgJournalGroupTab = PermissionMetaUtil.createPermissionGroup(pgTrainingTab, code + "TrainingEduGroupTabPG", "Вкладка «Учебные группы»");
            PermissionMetaUtil.createPermission(pgJournalGroupTab, "groupsJournalGenFefu_" + code, "Формирование реализации на учебные группы (ДВФУ)");

            // Отчеты БРС на табе «Отчеты»
            PermissionGroupMeta permissionGroupReports = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            PermissionGroupMeta pgBrsReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReports, code + "FefuBrsReports", "Отчеты по Рейтинговой Системе");
            PermissionMetaUtil.createPermission(pgBrsReports, "orgUnit_fefuBrsReportAttestationResultsPermissionKey_" + code, "Просмотр отчета «Результаты текущей/промежуточной аттестации студентов по дисциплинам, участвующим в рейтинговой системе»");
            PermissionMetaUtil.createPermission(pgBrsReports, "orgUnit_fefuBrsPercentGradingReportPermissionKey_" + code, "Просмотр отчета «Процент проставленных оценок»");
            PermissionMetaUtil.createPermission(pgBrsReports, "orgUnit_fefuListTrJournalDisciplinesReportPermissionKey_" + code, "Просмотр отчета «Перечень реализаций (рейтинг-планов) дисциплин»");
            PermissionMetaUtil.createPermission(pgBrsReports, "orgUnit_fefuBrsDelayGradingReportPermissionKey_" + code, "Просмотр отчета «Опоздание в простановке оценок»");
            PermissionMetaUtil.createPermission(pgBrsReports, "orgUnit_fefuBrsSumDataReportPermissionKey_" + code, "Просмотр отчета «Сводные данные балльно-рейтинговой системы»");
            PermissionMetaUtil.createPermission(pgBrsReports, "orgUnit_fefuBrsPpsGradingSumDataReportPermissionKey_" + code, "Просмотр отчета «Сводные данные выставления оценок преподавателями»");
            PermissionMetaUtil.createPermission(pgBrsReports, "orgUnit_fefuBrsStudentDisciplinesReportPermissionKey_" + code, "Просмотр отчета «Рейтинг студента по всем дисциплинам»");
            PermissionMetaUtil.createPermission(pgBrsReports, "orgUnit_fefuRatingGroupAllDiscPermissionKey_" + code, "Просмотр отчета «Рейтинг группы по всем дисциплинам»");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}