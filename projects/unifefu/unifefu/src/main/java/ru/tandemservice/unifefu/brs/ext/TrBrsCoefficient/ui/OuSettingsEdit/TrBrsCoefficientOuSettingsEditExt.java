/* $Id$ */
package ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.OuSettingsEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuSettingsEdit.TrBrsCoefficientOuSettingsEdit;

/**
 * @author Nikolay Fedorovskih
 * @since 14.07.2014
 */
@Configuration
public class TrBrsCoefficientOuSettingsEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrBrsCoefficientOuSettingsEditExtUI.class.getSimpleName();

    @Autowired
    private TrBrsCoefficientOuSettingsEdit _trBrsCoefficientOuSettingsEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trBrsCoefficientOuSettingsEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrBrsCoefficientOuSettingsEditExtUI.class))
                .addAction(new TrBrsCoefficientOuSettingsEditClickApply("onClickApply"))
                .create();
    }
}