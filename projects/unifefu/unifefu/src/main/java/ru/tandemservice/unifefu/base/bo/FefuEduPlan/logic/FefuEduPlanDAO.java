/**
 * $Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.logic;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvTotalRow;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartModuleWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.util.FefuEpvRowWrapper;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.FefuEduStdManager;
import ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionScheduleTab.FefuSummaryBudgetRowWrapper;
import ru.tandemservice.unifefu.dao.eppEduPlan.IImtsaImportDAO;
import ru.tandemservice.unifefu.entity.*;
import ru.tandemservice.unifefu.entity.catalog.FefuLoadType;
import ru.tandemservice.unifefu.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuLoadTypeCodes;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEppRegistryModuleELoad;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEpvCheckState;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEpvRowTermLoad;
import ru.tandemservice.unifefu.entity.eduPlan.FefuPracticeDispersion;
import ru.tandemservice.unifefu.tapestry.richTableList.FefuRangeSelectionWeekTypeListDataSource;
import ru.tandemservice.unifefu.utils.FefuEduPlanVersionScheduleUtils;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
public class FefuEduPlanDAO extends SharedBaseDao implements IFefuEduPlanDAO
{
    @Override
    public Map<Term, Map<ICatalogItem, Double>> getEpvRowExtLoadDataMap(Long rowId)
    {
        Map<Term, Map<ICatalogItem, Double>> result = new LinkedHashMap<>();

        List<ICatalogItem> loadTypes = new ArrayList<>();
        Map<String, ICatalogItem> interactiveLoadTypeMap = new HashMap<>();
        Map<String, ICatalogItem> baseLoadTypeMap = new HashMap<>();

        for (FefuLoadType loadType : getList(FefuLoadType.class))
        {
            interactiveLoadTypeMap.put(loadType.getCode(), loadType);
        }

        for (EppALoadType loadType : getList(EppALoadType.class))
        {
            baseLoadTypeMap.put(loadType.getCode(), loadType);
        }

        {
            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_LECTURES));
            loadTypes.add(interactiveLoadTypeMap.get(FefuLoadTypeCodes.TYPE_LECTURES_INTER));

            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_PRACTICE));
            loadTypes.add(interactiveLoadTypeMap.get(FefuLoadTypeCodes.TYPE_PRACTICE_INTER));

            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_LABS));
            loadTypes.add(interactiveLoadTypeMap.get(FefuLoadTypeCodes.TYPE_LABS_INTER));

            loadTypes.add(interactiveLoadTypeMap.get(FefuLoadTypeCodes.TYPE_EXAM_HOURS));
        }

        List<Term> terms = new DQLSelectBuilder()
                .fromEntity(DevelopGridTerm.class, "dgt")
                .column(property("dgt", DevelopGridTerm.term()))
                .where(eq(property("dgt", DevelopGridTerm.developGrid().id()),
                          new DQLSelectBuilder()
                                  .fromEntity(EppEpvRegistryRow.class, "rr")
                                  .column(property("rr", EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().developGrid().id()))
                                  .where(eq(property("rr", EppEpvRegistryRow.id()), value(rowId)))
                                  .buildQuery()))

                .order(property("dgt", DevelopGridTerm.term().intValue()))
                .createStatement(getSession())
                .list();

        for (ICatalogItem loadType : loadTypes)
        {
            for (Term term : terms)
            {
                SafeMap.safeGet(result, term, LinkedHashMap.class).put(loadType, null);
            }
        }

        for (FefuEpvRowTermLoad termLoad : getList(FefuEpvRowTermLoad.class, FefuEpvRowTermLoad.rowTerm().row().id(), rowId))
        {
            SafeMap.safeGet(result, termLoad.getRowTerm().getTerm(), LinkedHashMap.class).put(termLoad.getLoadType(), UniEppUtils.wrap(termLoad.getLoad()));
        }

        for (EppEpvRowTermLoad rowTermLoad : getList(EppEpvRowTermLoad.class, EppEpvRowTermLoad.rowTerm().row().id(), rowId))
        {
            if (rowTermLoad.getLoadType() != null)
                SafeMap.safeGet(result, rowTermLoad.getRowTerm().getTerm(), LinkedHashMap.class).put(rowTermLoad.getLoadType(), UniEppUtils.wrap(rowTermLoad.getHours()));
        }

        return result;
    }

    @Override
    public void updateEpvRowExtLoad(Long rowId, Map<Term, Map<ICatalogItem, Double>> dataMap, ErrorCollector errorCollector)
    {
        final EppEpvTermDistributedRow termDistributedRow = get(rowId);

        Map<String, EppALoadType> loadTypeMap = new HashMap<>();
        for (EppALoadType loadType : getList(EppALoadType.class))
        {
            loadTypeMap.put(loadType.getCode(), loadType);
        }

        Map<String, String> fefuLoad2baseLoadMap = new HashMap<>();
        {
            fefuLoad2baseLoadMap.put(FefuLoadTypeCodes.TYPE_LECTURES_INTER, EppALoadTypeCodes.TYPE_LECTURES);
            fefuLoad2baseLoadMap.put(FefuLoadTypeCodes.TYPE_PRACTICE_INTER, EppALoadTypeCodes.TYPE_PRACTICE);
            fefuLoad2baseLoadMap.put(FefuLoadTypeCodes.TYPE_LABS_INTER, EppALoadTypeCodes.TYPE_LABS);
        }

        Map<Long, Map<Long, FefuEpvRowTermLoad>> oldValueMap = SafeMap.get(HashMap.class);
        for (FefuEpvRowTermLoad rowTermLoad : getList(FefuEpvRowTermLoad.class, FefuEpvRowTermLoad.rowTerm().row().id(), rowId))
        {
            oldValueMap.get(rowTermLoad.getRowTerm().getTerm().getId()).put(rowTermLoad.getLoadType().getId(), rowTermLoad);
        }

        Map<Term, EppEpvRowTerm> rowTermMap = SafeMap.get(key -> {
            EppEpvRowTerm rowTerm = getByNaturalId(new EppEpvRowTerm.NaturalId(termDistributedRow, key));
            if (rowTerm == null)
            {
                save(rowTerm = new EppEpvRowTerm(termDistributedRow, key));
            }
            return rowTerm;
        });

        Map<PairKey<EppEpvRowTerm, FefuLoadType>, FefuEpvRowTermLoad> rowTermLoadMap = new HashMap<>();
        for (FefuEpvRowTermLoad rowTermLoad : getList(FefuEpvRowTermLoad.class, FefuEpvRowTermLoad.rowTerm().row().id(), rowId))
        {
            rowTermLoadMap.put(PairKey.create(rowTermLoad.getRowTerm(), rowTermLoad.getLoadType()), rowTermLoad);
        }

        for (EppEpvRowTerm rowTerm : getList(EppEpvRowTerm.class, EppEpvRowTerm.row().id(), rowId))
        {
            rowTermMap.put(rowTerm.getTerm(), rowTerm);
        }

        String emptyBase = "Невозможно задать часы интерактивной нагрузки. Не задана общая нагрузка.";
        String lessBase = "Невозможно задать часы интерактивной нагрузки. Значение не должно превосходить общую.";

        for (Map.Entry<Term, Map<ICatalogItem, Double>> termEntry : new HashMap<>(dataMap).entrySet())
        {
            Term term = termEntry.getKey();
            Map<ICatalogItem, Double> termLoadTypeMap = dataMap.get(term);
            for (Map.Entry<ICatalogItem, Double> loadTypeEntry : termEntry.getValue().entrySet())
            {
                ICatalogItem key = loadTypeEntry.getKey();
                if (key instanceof EppALoadType)
                {
                    continue;
                }

                FefuLoadType loadType = (FefuLoadType) key;

                Double value = loadTypeEntry.getValue();
                EppALoadType boundedType = loadTypeMap.get(fefuLoad2baseLoadMap.get(loadType.getCode()));

                boolean bounded = boundedType != null;
                Double maxValue = bounded ? termLoadTypeMap.get(boundedType) : null;


                /* VALIDATE */
                if (bounded && value != null)
                {
                    if (maxValue == null || maxValue == 0.0d)
                    {
                        if (value != 0.0d)
                        {
                            errorCollector.add(emptyBase, String.valueOf(term.getId()) + "_" + loadType.getId());
                            continue;
                        }

                    }
                    else
                    {
                        if (value > maxValue)
                        {
                            errorCollector.add(lessBase, String.valueOf(term.getId()) + "_" + loadType.getId());
                            continue;
                        }
                    }
                }

                /* UPDATE */
                if (value == null)
                {
                    FefuEpvRowTermLoad rowTermLoad;
                    if (rowTermMap.containsKey(term) && (rowTermLoad = rowTermLoadMap.get(PairKey.create(rowTermMap.get(term), loadType))) != null)
                    {
                        delete(rowTermLoad);
                    }

                }
                else
                {
                    Long valueAsLong = UniEppUtils.unwrap(value);
                    FefuEpvRowTermLoad rowTermLoad = oldValueMap.get(term.getId()).get(loadType.getId());
                    if (rowTermLoad == null)
                    {
                        rowTermLoad = new FefuEpvRowTermLoad(rowTermMap.get(term), loadType);
                        rowTermLoad.setLoad(valueAsLong);

                        save(rowTermLoad);

                    }
                    else if (rowTermLoad.getLoad() != valueAsLong)
                    {
                        rowTermLoad.setLoad(valueAsLong);

                        update(rowTermLoad);
                    }
                }
            }
        }
    }


    @Override
    public Map<EppEduPlanVersionBlock, PairKey<StaticListDataSource<FefuEpvRowWrapper>, StaticListDataSource<FefuEpvRowWrapper>>> getEduPlanVersionBlockDataSourceMap(Long versionId)
    {
        Map<EppEduPlanVersionBlock, PairKey<StaticListDataSource<FefuEpvRowWrapper>, StaticListDataSource<FefuEpvRowWrapper>>> result = new HashMap<>();

        DQLSelectBuilder developGridTermBuilder = new DQLSelectBuilder()
                .fromEntity(DevelopGridTerm.class, "dgt")
                .where(eq(property("dgt", DevelopGridTerm.developGrid().id()),
                          new DQLSelectBuilder()
                                  .fromEntity(EppEduPlanVersion.class, "v")
                                  .column(property("v", EppEduPlanVersion.eduPlan().developGrid().id()))
                                  .where(eq(property("v", EppEduPlanVersion.id()), value(versionId)))
                                  .buildQuery()))
                .order(property("dgt", DevelopGridTerm.course().intValue()))
                .order(property("dgt", DevelopGridTerm.part().number()));

        Map<Course, List<Term>> courseTermMap = new LinkedHashMap<>();
        for (DevelopGridTerm developGridTerm : this.<DevelopGridTerm>getList(developGridTermBuilder))
        {
            SafeMap.safeGet(courseTermMap, developGridTerm.getCourse(), ArrayList.class).add(developGridTerm.getTerm());
        }

        List<EppEduPlanVersionBlock> blocks = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion().id(), versionId);
        for (EppEduPlanVersionBlock block : blocks)
        {
            DQLSelectBuilder registryCompetenceBuilder = new DQLSelectBuilder()
                    .fromEntity(FefuCompetence2RegistryElementRel.class, "rer")
                    .joinEntity("rer", DQLJoinType.inner, EppEpvRegistryRow.class, "rr", eq(property("rr", EppEpvRegistryRow.registryElement()), property("rer", FefuCompetence2RegistryElementRel.registryElement())))
                    .column(property("rr", EppEpvRegistryRow.id()))
                    .column(property("rer", FefuCompetence2RegistryElementRel.fefuCompetence().id()))
                    .where(eq(property("rr", EppEpvRegistryRow.owner()), value(block)));

            List<Object[]> registryCompetence = getList(registryCompetenceBuilder);
            registryCompetence.stream().collect(Collectors.groupingBy(row -> (Long) row[0], Collectors.mapping(row -> (Long) row[1], Collectors.toSet())));

            // row.id -> term.id -> loadType.id -> load value
            Map<Long, Map<Long, Map<Long, Double>>> fullDataMap = SafeMap.get(key -> SafeMap.get(HashMap.class));

            DQLSelectBuilder baseLoadBuilder = new DQLSelectBuilder()
                    .fromEntity(EppEpvRowTermLoad.class, "rtl")
                    .column(property("rtl", EppEpvRowTermLoad.rowTerm().row().id()))
                    .column(property("rtl", EppEpvRowTermLoad.rowTerm().term().id()))
                    .column(property("rtl", EppEpvRowTermLoad.loadType().id()))
                    .column(property("rtl", EppEpvRowTermLoad.hours()))
                    .where(eq(property("rtl", EppEpvRowTermLoad.rowTerm().row().owner()), value(block)));

            for (Object[] row : this.<Object[]>getList(baseLoadBuilder))
            {
                fullDataMap.get((Long) row[0]).get((Long) row[1]).put((Long) row[2], UniEppUtils.wrap((Long) row[3]));
            }

            DQLSelectBuilder extLoadBuilder = new DQLSelectBuilder()
                    .fromEntity(FefuEpvRowTermLoad.class, "rtl")
                    .joinPath(DQLJoinType.inner, FefuEpvRowTermLoad.rowTerm().fromAlias("rtl"), "rt")
                    .column(property("rt", EppEpvRowTerm.row().id()))
                    .column(property("rt", EppEpvRowTerm.term().id()))
                    .column(property("rtl", FefuEpvRowTermLoad.loadType().id()))
                    .column(property("rtl", FefuEpvRowTermLoad.load()))
                    .where(eq(property("rt", EppEpvRowTerm.row().owner()), value(block)));

            for (Object[] row : this.<Object[]>getList(extLoadBuilder))
            {
                fullDataMap.get((Long) row[0]).get((Long) row[1]).put((Long) row[2], UniEppUtils.wrap((Long) row[3]));
            }

            Map<EppEpvRow, List<EppEpvRow>> parentMap = SafeMap.get(ArrayList.class);
            for (EppEpvRow row : getList(EppEpvRow.class, EppEpvRow.owner(), block))
            {
                parentMap.get(row.getHierarhyParent()).add(row);
            }

            if (block instanceof EppEduPlanVersionSpecializationBlock)
            {
                // добавляем структурные узлы общего блока
                EppEduPlanVersionBlock rootBlock = get(EppEduPlanVersionRootBlock.class, EppEduPlanVersionRootBlock.eduPlanVersion().id(), versionId);
                for (EppEpvHierarchyRow row : getList(EppEpvHierarchyRow.class, EppEpvRow.owner(), rootBlock))
                {
                    parentMap.get(row.getHierarhyParent()).add(row);
                }
            }

            List<FefuAbstractCompetence2EpvRegistryRowRel> row2CompetenceRel = getList(
                    new DQLSelectBuilder()
                            .fromEntity(FefuAbstractCompetence2EpvRegistryRowRel.class, "rrr")
                            .column(property("rrr"))
                            .where(eq(property("rrr", FefuAbstractCompetence2EpvRegistryRowRel.registryRow().owner()), value(block))));
            Map<Long, Set<Long>> row2competenceMap = row2CompetenceRel.stream()
                    .collect(Collectors.groupingBy(rel -> rel.getRegistryRow().getId(),
                                                   Collectors.mapping(rel -> rel.getFefuCompetence().getId(), Collectors.toSet())));
            Map<Long, FefuAbstractCompetence2EpvRegistryRowRel> relByCompetenceMap = row2CompetenceRel.stream()
                    .collect(Collectors.toMap(rel -> rel.getFefuCompetence().getId(), rel -> rel, (r1, r2) -> r1));
            final Map<Long, String> competenceId2indexMap = SafeMap.get(new SafeMap.Callback<Long, String>()
            {
                @Override
                public String resolve(Long key)
                {
                    FefuAbstractCompetence2EpvRegistryRowRel rel = relByCompetenceMap.get(key);
                    if (rel == null) return "";
                    return rel.getGroupCode();
                }
            });
            Transformer<Long, String> competenceTransformer = competenceId2indexMap::get;
            Comparator<Long> competenceComparator = (o1, o2) -> {
                FefuAbstractCompetence2EpvRegistryRowRel rel1 = relByCompetenceMap.get(o1);
                FefuAbstractCompetence2EpvRegistryRowRel rel2 = relByCompetenceMap.get(o2);
                return ComparisonChain.start()
                        .compare(rel1.getGroupShortTitle(),rel2.getGroupShortTitle())
                        .compare(rel1.getGroupNumber(),rel2.getGroupNumber())
                        .result();
            };

            List<FefuEpvRowWrapper> wrappers = new ArrayList<>();
            Model model = new Model(parentMap, row2competenceMap, competenceTransformer, competenceComparator, fullDataMap);

            wrap(wrappers, null, model);

            List<FefuEpvRowWrapper> actionWrappers = removeActionWrappers(wrappers);

            StaticListDataSource<FefuEpvRowWrapper> dataSource = getDataSource(courseTermMap, true, block);
            dataSource.setRowList(wrappers);

            StaticListDataSource<FefuEpvRowWrapper> actionDataSource = getDataSource(courseTermMap, false, block);
            actionDataSource.setRowList(actionWrappers);

            result.put(block, PairKey.create(dataSource, actionDataSource));
        }

        return result;
    }


    private List<FefuEpvRowWrapper> removeActionWrappers(List<FefuEpvRowWrapper> wrappers)
    {
        Map<Long, FefuEpvRowWrapper> wrapperMap = new LinkedHashMap<>();
        for (FefuEpvRowWrapper wrapper : new ArrayList<>(wrappers))
        {
            if (wrapper.getRow() instanceof EppEpvRegistryRow)
            {
                EppRegistryStructure structure = ((EppEpvRegistryRow) wrapper.getRow()).getRegistryElementType();
                while (structure.getHierarhyParent() != null)
                {
                    structure = structure.getParent();
                }

                if (!structure.getCode().equals(EppRegistryStructureCodes.REGISTRY_DISCIPLINE))
                {
                    wrappers.remove(wrapper);
                    FefuEpvRowWrapper parentWrapper = wrapper;
                    while (parentWrapper != null)
                    {
                        wrapperMap.put(parentWrapper.getId(), parentWrapper);
                        parentWrapper = (FefuEpvRowWrapper) parentWrapper.getHierarhyParent();
                    }
                }
            }
        }

        return new ArrayList<>(wrapperMap.values());
    }

    private Map<Long, Map<Long, Double>> wrap(List<FefuEpvRowWrapper> wrappers, @Nullable final FefuEpvRowWrapper parentWrapper, Model model)
    {
        boolean distrRow = parentWrapper != null && model._dataMap.containsKey(parentWrapper.getRow().getId());
        Map<Long, Map<Long, Double>> localDataMap = distrRow ? model._dataMap.get(parentWrapper.getRow().getId()) : SafeMap.<Long, Map<Long, Double>>get(HashMap.class);

        for (final EppEpvRow row : model._parentMap.get(parentWrapper == null ? null : parentWrapper.getRow()))
        {
            List<String> competenceIds = new ArrayList<>();
            Set<Long> ids = model._row2competenceMap.get(row.getId());
            if (ids != null)
                competenceIds = ids.stream()
                        .sorted(model._competenceComparator)
                        .map(model._competenceTransformer::transform)
                        .collect(Collectors.toList());

            FefuEpvRowWrapper wrapper = new FefuEpvRowWrapper(row, competenceIds)
            {
                @Override
                public IHierarchyItem getHierarhyParent()
                {
                    return parentWrapper;
                }
            };

            wrappers.add(wrapper);

            Map<Long, Map<Long, Double>> childDataMap = wrap(wrappers, wrapper, model);

            if (distrRow)
            {
                continue;
            }

            for (Map.Entry<Long, Map<Long, Double>> loadTypeEntry : childDataMap.entrySet())
            {
                Long typeId = loadTypeEntry.getKey();
                Map<Long, Double> termDataMap = SafeMap.safeGet(localDataMap, typeId, HashMap.class);
                for (Map.Entry<Long, Double> termEntry : loadTypeEntry.getValue().entrySet())
                {

                    Double oldValue = termDataMap.get(termEntry.getKey());
                    termDataMap.put(termEntry.getKey(), termEntry.getValue() + (oldValue == null ? 0.0d : oldValue));
                }
            }
        }

        if (parentWrapper != null)
            parentWrapper.setDataMap(localDataMap);

        return localDataMap;
    }

    private StaticListDataSource<FefuEpvRowWrapper> getDataSource(Map<Course, List<Term>> courseTermMap, boolean discipline, final EppEduPlanVersionBlock block)
    {
        class ShortTitlePresenter
        {
            public String shortTitle(ICatalogItem catalogItem)
            {
                if (catalogItem instanceof FefuLoadType || catalogItem instanceof EppALoadType)
                {
                    return catalogItem instanceof FefuLoadType ? ((FefuLoadType) catalogItem).getShortTitle() : ((EppALoadType) catalogItem).getShortTitle();
                }

                throw new IllegalArgumentException("Unknown catalog item " + catalogItem.getClass());
            }
        }

        ShortTitlePresenter shortTitlePresenter = new ShortTitlePresenter();
        String headerAlignCenter = "center";

        Map<String, ICatalogItem> baseLoadTypeMap = new HashMap<>();
        Map<String, ICatalogItem> extLoadTypeMap = new HashMap<>();

        for (EppALoadType loadType : getList(EppALoadType.class))
        {
            baseLoadTypeMap.put(loadType.getCode(), loadType);
        }
        for (FefuLoadType loadType : getList(FefuLoadType.class))
        {
            extLoadTypeMap.put(loadType.getCode(), loadType);
        }

        List<ICatalogItem> loadTypes = new ArrayList<>();
        {
            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_LECTURES));
            loadTypes.add(extLoadTypeMap.get(FefuLoadTypeCodes.TYPE_LECTURES_INTER));
            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_PRACTICE));
            loadTypes.add(extLoadTypeMap.get(FefuLoadTypeCodes.TYPE_PRACTICE_INTER));
            loadTypes.add(baseLoadTypeMap.get(EppALoadTypeCodes.TYPE_LABS));
            loadTypes.add(extLoadTypeMap.get(FefuLoadTypeCodes.TYPE_LABS_INTER));

            loadTypes.add(extLoadTypeMap.get(FefuLoadTypeCodes.TYPE_EXAM_HOURS));
        }

        StaticListDataSource<FefuEpvRowWrapper> dataSource = new StaticListDataSource<>();
        dataSource.addColumn(new SimpleColumn("Название", FefuEpvRowWrapper.TITLE).setTreeColumn(true).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип", "type")
        {
            @Override
            public String getContent(IEntity entity)
            {
                return ((FefuEpvRowWrapper) entity).getRow().getRowType();
            }
        }.setOrderable(false));

        if (discipline)
        {
            HeadColumn loadColumn = new HeadColumn("load", "Нагрузка");
            loadColumn.setHeaderAlign(headerAlignCenter).setOrderable(false);
            dataSource.addColumn(loadColumn);

            for (final ICatalogItem loadType : loadTypes)
            {
                SimpleColumn column = new SimpleColumn(shortTitlePresenter.shortTitle(loadType), loadType.getTitle())
                {
                    @Override
                    public String getContent(IEntity entity)
                    {
                        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(((FefuEpvRowWrapper) entity).getDataMap().get(null).get(loadType.getId()));
                    }
                };
                column.setVerticalHeader(true).setOrderable(false);
                loadColumn.addColumn(column);
            }

            HeadColumn termDetailColumn = new HeadColumn("termDetail", "Нагрузка по семестрам");
            termDetailColumn.setHeaderAlign(headerAlignCenter).setOrderable(false);
            dataSource.addColumn(termDetailColumn);

            for (Map.Entry<Course, List<Term>> courseEntry : courseTermMap.entrySet())
            {
                Course course = courseEntry.getKey();
                HeadColumn courseColumn = new HeadColumn("course" + course.getIntValue(), course.getTitle() + " курс");
                courseColumn.setOrderable(false).setHeaderAlign(headerAlignCenter);
                termDetailColumn.addColumn(courseColumn);

                for (final Term term : courseEntry.getValue())
                {
                    HeadColumn termColumn = new HeadColumn("term" + term.getTitle(), "Сем " + term.getTitle());
                    termColumn.setHeaderAlign(headerAlignCenter).setOrderable(false);
                    courseColumn.addColumn(termColumn);

                    for (final ICatalogItem loadType : loadTypes)
                    {
                        SimpleColumn column = new SimpleColumn(shortTitlePresenter.shortTitle(loadType), term.getTitle() + loadType.getTitle())
                        {
                            @Override
                            public String getContent(IEntity entity)
                            {
                                return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(((FefuEpvRowWrapper) entity).getDataMap().get(term.getId()).get(loadType.getId()));
                            }
                        };
                        column.setVerticalHeader(true).setOrderable(false);
                        termColumn.addColumn(column);
                    }
                }
            }

            dataSource.setRowCustomizer(new SimpleRowCustomizer<FefuEpvRowWrapper>()
            {
                @Override
                public String getRowStyle(FefuEpvRowWrapper wrapper)
                {
                    return wrapper.getRow() instanceof EppEpvTermDistributedRow ? "" : "font-weight: bold;";
                }
            });
        }

        dataSource.addColumn(new SimpleColumn("Компетенции", FefuEpvRowWrapper.COMPETENCES).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditRow").setDisabledProperty(FefuEpvRowWrapper.EDIT_DISABLED).setPermissionKey("edit_epvRowExt"));
        IEntityHandler competencesEditDisableHandler = entity -> {
            final FefuEpvRowWrapper row = (FefuEpvRowWrapper) entity;
            return !((row.getRow() instanceof EppEpvRegistryRow) && block.equals(row.getRow().getOwner()));
        };
        dataSource.addColumn(new ActionColumn("Редактировать компетенции", ActionColumn.EDIT, "onClickEditCompetences")
                                     .setPermissionKey("edit_epvRowExt")
                                     .setDisableHandler(competencesEditDisableHandler));
        return dataSource;
    }

    private static class Model
    {
        private Map<EppEpvRow, List<EppEpvRow>> _parentMap;
        private Map<Long, Set<Long>> _row2competenceMap;
        private Transformer<Long, String> _competenceTransformer;
        private Comparator<Long> _competenceComparator;
        private Map<Long, Map<Long, Map<Long, Double>>> _dataMap;

        private Model(Map<EppEpvRow, List<EppEpvRow>> parentMap, Map<Long, Set<Long>> row2competenceMap, Transformer<Long, String> competenceTransformer, Comparator<Long> competenceComparator, Map<Long, Map<Long, Map<Long, Double>>> dataMap)
        {
            _parentMap = parentMap;
            _row2competenceMap = row2competenceMap;
            _competenceTransformer = competenceTransformer;
            _competenceComparator = competenceComparator;
            _dataMap = dataMap;
        }
    }

    @Override
    public void saveImtsaXml(EppEduPlanVersionBlock block, IUploadFile source, String fileName, String encoding, int number)
    {
        FefuImtsaXml xml = get(FefuImtsaXml.class, FefuImtsaXml.block(), block);
        if (xml != null)
        {
            delete(xml);
            getSession().flush();
            getSession().clear();
        }

        File file;
        byte[] xmlTemplate;
        try
        {
            file = File.createTempFile("fefuTemp-", "xml");
            try
            {
                source.write(file);
                xmlTemplate = FileUtils.readFileToString(file, encoding).getBytes();

            }
            finally
            {
                file.delete();
            }

        }
        catch (IOException e)
        {
            throw new ApplicationException("Невозможно сохранить файл XML.");
        }

        xml = new FefuImtsaXml(block, fileName, encoding, xmlTemplate, number);
        save(xml);
    }

    @Override
    public FefuEpvCheckState getEpvCheckState(Long versionId)
    {
        EppEduPlanVersion version = get(EppEduPlanVersion.class, versionId);
        FefuEpvCheckState checkState = getByNaturalId(new FefuEpvCheckState.NaturalId(version));
        return checkState == null ? new FefuEpvCheckState(version) : checkState;
    }

    public void doCopyEduPlanVersionSchedule(@NotNull EppEduPlanVersion source, @NotNull EppEduPlanVersion target, @NotNull Collection<Course> courses)
    {
        checkNotNull(source);
        checkNotNull(target);
        checkNotNull(courses);
        checkArgument(!courses.isEmpty());
        checkArgument(source.getEduPlan().getDevelopGrid().equals(target.getEduPlan().getDevelopGrid()));

        FefuEduPlanVersionPartitionType srcPartitionType = this.get(FefuEduPlanVersionPartitionType.class, FefuEduPlanVersionPartitionType.version(), source);
        int srcPartsNumber = srcPartitionType == null ? 1 : srcPartitionType.getPartitionType().getPartsNumber();
        if (srcPartsNumber != 1)
        {
            // если разбиение учебного графика источника отлично от недельного, оно должно совпадать с учебным графиком цели
            FefuEduPlanVersionPartitionType trgPartitionType = this.get(FefuEduPlanVersionPartitionType.class, FefuEduPlanVersionPartitionType.version(), target);
            int trgPartsNumber = trgPartitionType == null ? 1 : trgPartitionType.getPartitionType().getPartsNumber();

            if (srcPartsNumber != trgPartsNumber)
                throw new ApplicationException("Разбиения учебного графика выбранной и целевой УПв должны совпадать.");
        }

        // удаляем учебный график выбранных курсов
        new DQLDeleteBuilder(FefuEduPlanVersionWeek.class)
                .where(eq(property(FefuEduPlanVersionWeek.version()), value(target)))
                .where(in(property(FefuEduPlanVersionWeek.course()), courses))
                .createStatement(this.getSession())
                .execute();

        {
            /*short scheduleWeekEntityCode = EntityRuntime.getMeta(FefuEduPlanVersionWeek.class).getEntityCode();
            DQLSelectBuilder fromBuilder = new DQLSelectBuilder()
                .column(DQLFunctions.createNewIdFromId(property("w.id"), scheduleWeekEntityCode)) //TODO Решение довольно изящное, но увы, пока не работает как должно
                .column(value(scheduleWeekEntityCode))
                .column(value(target))
                .column(property("w", FefuEduPlanVersionWeek.course()))
                .column(property("w", FefuEduPlanVersionWeek.week()))
                .column(property("w", FefuEduPlanVersionWeek.term()))
                .column(property("w", FefuEduPlanVersionWeek.weekType()))

                .fromEntity(FefuEduPlanVersionWeek.class, "w")
                .where(eq(property("w", FefuEduPlanVersionWeek.version()), value(source)))
                .where(in(property("w", FefuEduPlanVersionWeek.course()), courses));

            DQLInsertSelectBuilder insertBuilder = new DQLInsertSelectBuilder(FefuEduPlanVersionWeek.class);
            insertBuilder.setSelectRule(fromBuilder.buildSelectRule());
            insertBuilder.properties(FefuEduPlanVersionWeek.P_ID, "class", FefuEduPlanVersionWeek.L_VERSION, FefuEduPlanVersionWeek.L_COURSE, FefuEduPlanVersionWeek.L_WEEK, FefuEduPlanVersionWeek.L_TERM, FefuEduPlanVersionWeek.L_WEEK_TYPE);
            insertBuilder.createStatement(this.getSession()).execute();*/

            List<FefuEduPlanVersionWeek> weeks = new DQLSelectBuilder().fromEntity(FefuEduPlanVersionWeek.class, "w")
                    .where(eq(property("w", FefuEduPlanVersionWeek.version()), value(source)))
                    .where(in(property("w", FefuEduPlanVersionWeek.course()), courses))
                    .column(property("w")).createStatement(getSession()).list();

            int updCnt = 0;
            DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(FefuEduPlanVersionWeek.class);
            for (FefuEduPlanVersionWeek srcWeek : weeks)
            {
                insertBuilder.value(FefuEduPlanVersionWeek.version().s(), target);
                insertBuilder.value(FefuEduPlanVersionWeek.course().s(), srcWeek.getCourse());
                insertBuilder.value(FefuEduPlanVersionWeek.week().s(), srcWeek.getWeek());
                insertBuilder.value(FefuEduPlanVersionWeek.term().s(), srcWeek.getTerm());
                insertBuilder.value(FefuEduPlanVersionWeek.weekType().s(), srcWeek.getWeekType());
                insertBuilder.value(FefuEduPlanVersionWeek.course().s(), srcWeek.getCourse());
                insertBuilder.addBatch();
                updCnt++;

                if (updCnt == 300)
                {
                    createStatement(insertBuilder).execute();
                    insertBuilder = new DQLInsertValuesBuilder(FefuEduPlanVersionWeek.class);
                    updCnt = 0;
                }
            }

            if (updCnt > 0) createStatement(insertBuilder).execute();
        }

        {
            /*short scheduleWeekPartEntityCode = EntityRuntime.getMeta(FefuEduPlanVersionWeekPart.class).getEntityCode();
            DQLSelectBuilder fromBuilder = new DQLSelectBuilder()
                .column(DQLFunctions.createNewIdFromId(property("p.id"), scheduleWeekPartEntityCode)) //TODO Решение довольно изящное, но увы, пока не работает как должно
                .column(value(scheduleWeekPartEntityCode))
                .column(property("wt"))
                .column(property("p", FefuEduPlanVersionWeekPart.partitionElementNumber()))
                .column(property("p", FefuEduPlanVersionWeekPart.weekType()))

                .fromEntity(FefuEduPlanVersionWeekPart.class, "p")
                .joinPath(DQLJoinType.inner, FefuEduPlanVersionWeekPart.eduPlanVersionWeek().fromAlias("p"), "ws")
                .where(eq(property("p", FefuEduPlanVersionWeekPart.eduPlanVersionWeek().version()), value(source)))
                .where(in(property("p", FefuEduPlanVersionWeekPart.eduPlanVersionWeek().course()), courses))

                .fromEntity(FefuEduPlanVersionWeek.class, "wt") // учебный график цели
                .where(eq(property("wt", FefuEduPlanVersionWeek.course()), property("ws", FefuEduPlanVersionWeek.course())))
                .where(eq(property("wt", FefuEduPlanVersionWeek.week()), property("ws", FefuEduPlanVersionWeek.week())))
                .where(eq(property("wt", FefuEduPlanVersionWeek.version()), value(target)));

            DQLInsertSelectBuilder insertBuilder = new DQLInsertSelectBuilder(FefuEduPlanVersionWeekPart.class);
            insertBuilder.setSelectRule(fromBuilder.buildSelectRule());
            insertBuilder.properties(FefuEduPlanVersionWeekPart.P_ID, "class", FefuEduPlanVersionWeekPart.L_EDU_PLAN_VERSION_WEEK, FefuEduPlanVersionWeekPart.P_PARTITION_ELEMENT_NUMBER, FefuEduPlanVersionWeekPart.L_WEEK_TYPE);
            insertBuilder.createStatement(this.getSession()).execute();*/


            List<Object[]> weekParts = new DQLSelectBuilder()
                    .fromEntity(FefuEduPlanVersionWeekPart.class, "p").column(property("p"))
                    .joinPath(DQLJoinType.inner, FefuEduPlanVersionWeekPart.eduPlanVersionWeek().fromAlias("p"), "ws")
                    .where(eq(property("p", FefuEduPlanVersionWeekPart.eduPlanVersionWeek().version()), value(source)))
                    .where(in(property("p", FefuEduPlanVersionWeekPart.eduPlanVersionWeek().course()), courses))
                    .fromEntity(FefuEduPlanVersionWeek.class, "wt").column(property("wt")) // учебный график цели
                    .where(eq(property("wt", FefuEduPlanVersionWeek.course()), property("ws", FefuEduPlanVersionWeek.course())))
                    .where(eq(property("wt", FefuEduPlanVersionWeek.week()), property("ws", FefuEduPlanVersionWeek.week())))
                    .where(eq(property("wt", FefuEduPlanVersionWeek.version()), value(target)))
                    .createStatement(getSession()).list();

            int updCnt = 0;
            DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(FefuEduPlanVersionWeekPart.class);
            for (Object[] srcItem : weekParts)
            {
                FefuEduPlanVersionWeekPart srcWeekPart = (FefuEduPlanVersionWeekPart) srcItem[0];
                FefuEduPlanVersionWeek week = (FefuEduPlanVersionWeek) srcItem[1];

                insertBuilder.value(FefuEduPlanVersionWeekPart.eduPlanVersionWeek().s(), week);
                insertBuilder.value(FefuEduPlanVersionWeekPart.partitionElementNumber().s(), srcWeekPart.getPartitionElementNumber());
                insertBuilder.value(FefuEduPlanVersionWeekPart.weekType().s(), srcWeekPart.getWeekType());
                insertBuilder.addBatch();
                updCnt++;

                if (updCnt == 300)
                {
                    createStatement(insertBuilder).execute();
                    insertBuilder = new DQLInsertValuesBuilder(FefuEduPlanVersionWeekPart.class);
                    updCnt = 0;
                }
            }

            if (updCnt > 0) createStatement(insertBuilder).execute();
        }

        for (Course course : courses)
            IImtsaImportDAO.instance.get().updateEduPlanVersionWeekTypes(target.getId(), course.getId());
    }

    @Override
    public void doCheckEduPlanVersion(EppEduPlanVersionBlock block, boolean... params)
    {
        Date checkTime = new Date();
        for (int i = 0; i < params.length; i++)
        {
            boolean check = params[i];
            switch (i)
            {
                case 0:
                    if (check) checkEduPlanVersionMainParameters(block, checkTime);
                    break;
                case 1:
                    if (check) checkEduPlanVersionCompetence(block, checkTime);
                    break;
                case 2:
                    if (check) checkEduPlanVersionStructure(block, checkTime);
                    break;
                case 3:
                    if (check) checkEduPlanVersionControlAction(block, checkTime);
                    break;
                case 4:
                    if (check) checkEduPlanVersionAdditionalChecks(block, checkTime);
                    break;
            }
        }
    }

    @Override
    public Map<Integer, Map<Integer, Map<String, Double>>> getEpvSummaryBudgetDataMap(EppEduPlanVersion version)
    {
        class DoubleWrapper
        {
            private int _ratio;

            public DoubleWrapper(int ratio)
            {
                _ratio = ratio;
            }

            public Double wrap(Long value)
            {
                return ((double) value / _ratio);
            }

            public Long unwrap(Double value)
            {
                return Math.round(value * _ratio);
            }

            public Double updateValue(Double value)
            {
                return wrap(unwrap(value));
            }

            public Double sum(Double d1, Double d2)
            {
                return mathSum(d1, d2, true);
            }

            public Double diff(Double d1, Double d2)
            {
                return mathSum(d1, d2, false);
            }

            private Double mathSum(Double d1, Double d2, boolean sign)
            {
                return wrap(unwrap(d1) + (sign ? 1 : -1) * unwrap(d2));
            }
        }

        DoubleWrapper vw = new DoubleWrapper(6);
        Map<String, String> typeFromTypeToMap = SafeMap.get(key -> key);

        typeFromTypeToMap.put(EppWeekTypeCodes.TEACH_PRACTICE_WITH_AUDIT_LOAD, EppWeekTypeCodes.THEORY);
        typeFromTypeToMap.put(EppWeekTypeCodes.RESEARCH_WORK_WITH_AUDIT_LOAD, EppWeekTypeCodes.THEORY);
        typeFromTypeToMap.put(EppWeekTypeCodes.WORK_PRACTICE_WITH_AUDIT_LOAD, EppWeekTypeCodes.THEORY);

        Map<Long, PairKey<Integer, Integer>> term2CoursePartMap = new HashMap<>();
        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), version.getEduPlan().getDevelopGrid()))
        {
            term2CoursePartMap.put(developGridTerm.getTerm().getId(), PairKey.create(developGridTerm.getCourse().getIntValue(), developGridTerm.getPartNumber()));
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());
        }

        //    course       part         row code    value
        final Map<Integer, Map<Integer, Map<String, Double>>> dataMap = SafeMap.get(key -> SafeMap.get(key1 -> SafeMap.get(key2 -> 0.0d)));

        FefuEduPlanVersionPartitionType partitionType = get(FefuEduPlanVersionPartitionType.class, FefuEduPlanVersionPartitionType.version(), version);
        if (null == partitionType) return dataMap;

        DQLSelectBuilder termWeekTypeBuilder = new DQLSelectBuilder()
                .fromEntity(FefuEduPlanVersionWeek.class, "w")
                .column(property("w", FefuEduPlanVersionWeek.term().id()))
                .column(property("w", FefuEduPlanVersionWeek.weekType().code()))
                .column(DQLFunctions.count(property("w", FefuEduPlanVersionWeek.id())))
                .where(eq(property("w", FefuEduPlanVersionWeek.version()), value(version)))
                .where(isNotNull(property("w", FefuEduPlanVersionWeek.weekType())))
                .group(property("w", FefuEduPlanVersionWeek.term().id()))
                .group(property("w", FefuEduPlanVersionWeek.weekType().code()));


        int parts = partitionType.getPartitionType().getPartsNumber();
        DQLSelectBuilder termWeekPartTypeBuilder = new DQLSelectBuilder()
                .fromEntity(FefuEduPlanVersionWeekPart.class, "wp")
                .column(property("wp", FefuEduPlanVersionWeekPart.eduPlanVersionWeek().term().id()))
                .column(property("wp", FefuEduPlanVersionWeekPart.weekType().code()))
                .column(DQLFunctions.count(property("wp", FefuEduPlanVersionWeekPart.id())))
                .where(eq(property("wp", FefuEduPlanVersionWeekPart.eduPlanVersionWeek().version()), value(version)))
                .group(property("wp", FefuEduPlanVersionWeekPart.eduPlanVersionWeek().term().id()))
                .group(property("wp", FefuEduPlanVersionWeekPart.weekType().code()));

        for (boolean part : new boolean[]{false, true})
        {
            DQLSelectBuilder builder = part ? termWeekPartTypeBuilder : termWeekTypeBuilder;
            Double coeff = part ? 1.0d / parts : 1.0d;
            List<Object[]> rows = builder.createStatement(getSession()).<Object[]>list();
            for (Object[] row : rows)
            {
                Long termId = (Long) row[0];
                String typeCode = typeFromTypeToMap.get((String) row[1]);
                Long count = (Long) row[2];

                PairKey<Integer, Integer> key = term2CoursePartMap.get(termId);
                Integer courseNumber = key.getFirst();
                Integer partNumber = key.getSecond();

                Map<Integer, Map<String, Double>> partDataMap = dataMap.get(courseNumber);

                Map<String, Double> typeDataMap = partDataMap.get(partNumber);
                double value = coeff * count + typeDataMap.get(typeCode);
                value = vw.updateValue(value);
                typeDataMap.put(typeCode, value);

                partDataMap.get(0).put(typeCode, vw.sum(value, partDataMap.get(0).get(typeCode)));
            }
        }

        Map<String, String> typeMap = ImmutableMap.of(
                EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING, FefuSummaryBudgetRowWrapper.TEACH_DISPERSED,
                EppRegistryStructureCodes.REGISTRY_PRACTICE_NIR, FefuSummaryBudgetRowWrapper.NIR_DISPERSED,
                EppRegistryStructureCodes.REGISTRY_PRACTICE_PRODUCTION, FefuSummaryBudgetRowWrapper.WORK_DISPERSED);

        String dis_alias = "dis";
        String rt_alias = "rt";
        DQLSelectBuilder practiceBuilder = new DQLSelectBuilder()
                .fromEntity(FefuPracticeDispersion.class, dis_alias)
                .column(property(dis_alias, FefuPracticeDispersion.practice().registryElementType().code()))

                .where(and(
                        eq(property(dis_alias, FefuPracticeDispersion.practice().owner().eduPlanVersion().id()), value(version.getId())),
                        instanceOf(FefuPracticeDispersion.practice().owner().fromAlias(dis_alias).s(), EppEduPlanVersionRootBlock.class)
                ))
                .where(in(property(dis_alias, FefuPracticeDispersion.practice().registryElementType().code()), typeMap.keySet()))
                .where(eq(property(dis_alias, FefuPracticeDispersion.dispersed()), value(Boolean.TRUE)))

                .joinEntity(dis_alias, DQLJoinType.inner, EppEpvRowTerm.class, rt_alias,
                            eq(property(rt_alias, EppEpvRowTerm.row()), property(dis_alias, FefuPracticeDispersion.practice())))

                .column(property(rt_alias, EppEpvRowTerm.term().id()))
                .column(property(rt_alias, EppEpvRowTerm.weeks()));

        for (Object[] row : practiceBuilder.createStatement(getSession()).<Object[]>list())
        {
            String typeCode = typeMap.get((String) row[0]);
            Long termId = (Long) row[1];
            Double weesSize = UniEppUtils.wrap((Long) row[2]);

            PairKey<Integer, Integer> key = term2CoursePartMap.get(termId);
            Integer courseNumber = key.getFirst();
            Integer partNumber = key.getSecond();

            Map<Integer, Map<String, Double>> partDataMap = dataMap.get(courseNumber);

            Map<String, Double> typeDataMap = partDataMap.get(partNumber);
            double value = typeDataMap.get(typeCode) + weesSize;
            value = vw.updateValue(value);
            typeDataMap.put(typeCode, value);
            typeDataMap.put(EppWeekTypeCodes.THEORY, vw.diff(typeDataMap.get(EppWeekTypeCodes.THEORY), weesSize));
            partDataMap.get(0).put(typeCode, vw.sum(weesSize, partDataMap.get(0).get(typeCode)));
            partDataMap.get(0).put(EppWeekTypeCodes.THEORY, vw.diff(partDataMap.get(0).get(EppWeekTypeCodes.THEORY), weesSize));
        }


        Double total = 0.0d;
        for (Map.Entry<Integer, Map<Integer, Map<String, Double>>> courseEntry : new HashMap<>(dataMap).entrySet())
        {
            Integer courseNumber = courseEntry.getKey();
            for (Map.Entry<Integer, Map<String, Double>> partEntry : courseEntry.getValue().entrySet())
            {
                Integer partNumber = partEntry.getKey();
                Double totalPartValue = 0.0d;
                for (Map.Entry<String, Double> typeEntry : partEntry.getValue().entrySet())
                {
                    String typeCode = typeEntry.getKey();
                    Double value = typeEntry.getValue();

                    if (partNumber != 0)
                        dataMap.get(0).get(0).put(typeCode, value + dataMap.get(0).get(0).get(typeCode));

                    totalPartValue += value;
                }

                dataMap.get(courseNumber).get(partNumber).put(FefuSummaryBudgetRowWrapper.TOTAL, totalPartValue);
                if (partNumber != 0)
                    total += totalPartValue;
            }
        }

        dataMap.get(0).get(0).put(FefuSummaryBudgetRowWrapper.TOTAL, total);
        return dataMap;
    }

    @SuppressWarnings("unchecked")
    private void checkEduPlanVersionMainParameters(final EppEduPlanVersionBlock block, Date checkTime)
    {
        EppEduPlan eduPlan = block.getEduPlanVersion().getEduPlan();
        EppStateEduStandard eduStandard = eduPlan.getParent();
        if (null == eduStandard) return;

        FefuEppStateEduStandardParameters parameters = getByNaturalId(new FefuEppStateEduStandardParameters.NaturalId(eduStandard));
        if (null == parameters) return;

        EduProgramForm programForm = eduPlan.getProgramForm();
        DevelopGrid developGrid = eduPlan.getDevelopGrid();
        String programFormCode = programForm.getCode();

        IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(Collections.singleton(block.getId()), true).get(block.getId());
        Collection<IEppEpvRowWrapper> rows = blockWrapper.getRowMap().values();
        PlaneTree tree = new PlaneTree((Collection) rows);
        Collection<IEppEpvRowWrapper> rowWrapperList = (Collection) Arrays.asList(tree.getFlatTreeObjects());
        Collection<IEppEpvRowWrapper> filteredRows = EppEpvTotalRow.filterRowsForTotalStats(rowWrapperList);
        EppEpvTotalRow totalRow = EppEpvTotalRow.getTotalRowTotal(filteredRows);

        if (EduProgramFormCodes.OCHNAYA.equals(programFormCode))
        {
            // проверка 1
            DevelopGrid baseDG = parameters.getDevelopGridForFullTimeForm();
            saveFefuEpvCheckResult(block, checkTime, "Нормативный срок освоения образовательной программы для очной формы - " + (baseDG.equals(developGrid) ? "OK" : "не соответствует указанному в стандарте (" + baseDG.getTitle() + ")"));
            // проверка 2
            String totalLabor = totalRow.getLoadValue(0, EppLoadType.FULL_CODE_LABOR);
            String checkLaborFullTimeTitle = StringUtils.isEmpty(totalLabor) ? "Отсутствуют итоговые данные по общей трудоемкости" : totalLabor.equals(String.valueOf(parameters.getLaborFullTime())) ? "OK" : "не соответствует указанному в стандарте (" + parameters.getLaborFullTime() + ")";
            saveFefuEpvCheckResult(block, checkTime, "Общая трудоемкость образовательной программы для очной формы освоения, в ЗЕТ - " + checkLaborFullTimeTitle);
            // проверка 3
            checkLaborFullTimeOneYear(eduPlan, block, totalRow, checkTime, parameters.getLaborFullTimeOneYear());
            // проверка 4
            saveFefuEpvCheckResult(block, checkTime, "Общая трудоемкость образовательной программы для очно-заочной и заочной форм освоения, реализуемой за один учебный год, в ЗЕТ - Проверка пропущена");
        }
        else if (EduProgramFormCodes.ZAOCHNAYA.equals(programFormCode) || EduProgramFormCodes.OCHNO_ZAOCHNAYA.equals(programFormCode))
        {
            // проверка 4
            checkLaborFullTimeOneYear(eduPlan, block, totalRow, checkTime, parameters.getLaborPartTimeAndExtramuralOneYear());
        }
        // проверка 5
        checkMaxHoursLectureTypePercent(block, checkTime, parameters, rowWrapperList);
        // проверка 6
        checkMinUnitsChoiceDisciplines(block, checkTime, parameters, rowWrapperList);

        // проверка 7-13
        checkScheduleEppWeekTypeNumbers(block, checkTime, parameters);
        // проверка 14-16
        checkTotalAuditAndSelfWorkAvgLoads(block, checkTime, parameters, blockWrapper, filteredRows);
        // проверка 17
        checkMinLaborOneDiscipline(block, checkTime, filteredRows, parameters.getMinLaborOneDiscipline());

        // проверка 18
        if (EduProgramFormCodes.OCHNAYA.equals(programFormCode))
            checkHoursForOneExamFullTimeForm(block, checkTime, filteredRows, parameters.getHoursForOneExamFullTimeForm());

        // проверка 19
        checkEpvLecturesPercent(block, checkTime, totalRow, parameters.getMaxLecturesPercent());
        // проверка 20
        checkEpvInteractiveLessonsPercent(block, checkTime, totalRow, filteredRows, parameters.getMaxInteractiveLessonsPercent());
        // проверка 21
        checkEpvGroupImRowPercent(block, checkTime, rowWrapperList, parameters.getMinChoiceDisciplinesPercent());
    }

    /**
     * Проверяет, соответствует ли число заданных недель для элемента, числу недель в уч. графике, для:
     * - теоретического обучения;
     * - экзаменационных сессий;
     * - учебной практики;
     * - производственной практики;
     * - итоговой аттестации;
     * - каникул;
     * - каникул первой части учебного года.
     */
    private void checkScheduleEppWeekTypeNumbers(EppEduPlanVersionBlock block, Date checkTime, FefuEppStateEduStandardParameters parameters)
    {
        final EppEduPlanVersion version = block.getEduPlanVersion();
        Map<Integer, Map<Integer, Map<String, Double>>> summaryBudgetDataWeekMap = FefuEduPlanManager.instance().dao().getEpvSummaryBudgetDataMap(version);
        Map<String, Double> totalDataWeekMap = summaryBudgetDataWeekMap.get(0).get(0);
        String programFormCode = version.getEduPlan().getProgramForm().getCode();

        for (String code : FefuSummaryBudgetRowWrapper.CODES)
        {
            if (!totalDataWeekMap.containsKey(code)) totalDataWeekMap.put(code, 0.0);
        }

        checkScheduleEppWeekTypeNumber(block, checkTime, totalDataWeekMap, parameters.getTheoryWeek(), "Теоретическое обучение", "теоретического обучения", FefuSummaryBudgetRowWrapper.THEORY);
        checkScheduleEppWeekTypeNumber(block, checkTime, totalDataWeekMap, parameters.getExamSessionWeek(), "Экзаменационные сессии", "экзаменационных сессий", FefuSummaryBudgetRowWrapper.EXAM);
        checkScheduleEppWeekTypeNumber(block, checkTime, totalDataWeekMap, parameters.getTeachPracticeWeek(), "Практика учебная", "учебной практики", FefuSummaryBudgetRowWrapper.TEACH, FefuSummaryBudgetRowWrapper.TEACH_DISPERSED);
        checkScheduleEppWeekTypeNumber(block, checkTime, totalDataWeekMap, parameters.getWorkPracticeWeek(), "Практика производственная", "производственной практики", FefuSummaryBudgetRowWrapper.WORK, FefuSummaryBudgetRowWrapper.WORK_DISPERSED);
        checkScheduleEppWeekTypeNumber(block, checkTime, totalDataWeekMap, parameters.getResultAttestationWeek(), "Итоговая аттестация", "итоговой аттестации", FefuSummaryBudgetRowWrapper.DISSERTATION, FefuSummaryBudgetRowWrapper.STATE_EXAM);
        checkScheduleEppWeekTypeNumber(block, checkTime, totalDataWeekMap, parameters.getHolidaysWeek(), "Каникулы", "каникул", FefuSummaryBudgetRowWrapper.HOLIDAYS);

        if (EduProgramFormCodes.OCHNAYA.equals(programFormCode))
        {
            Double defaultValue = parameters.getHolidaysFirstPartYearFullTimeForm();
            String message = "Каникулы первой части учебного года";

            if (null != defaultValue)
            {
                FefuEduPlanVersionScheduleUtils schedule = new FefuEduPlanVersionScheduleUtils()
                {
                    @Override
                    protected boolean isTotalCourseRowPresent()
                    {
                        return false;
                    }

                    @Override
                    protected EppEduPlanVersion getEduPlanVersion()
                    {
                        return version;
                    }
                };

                List<EppWeek> weekList = DataAccessServices.dao().getList(EppWeek.class, EppWeek.P_NUMBER);
                FefuRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> dataSource = schedule.getFefuRangeSelectionWeekTypeListDataSource();
                Map<Long, Map<Long, Map<Integer, EppWeekType>>> dataMap = dataSource.getFullDataMap();
                Map<Long, int[]> points = dataSource.getRow2points();
                int firstPartYearCount = EppWeek.YEAR_WEEK_COUNT;
                int weekValue = 0;

                if (!points.values().isEmpty())
                {
                    List<int[]> pointList = Lists.newArrayList(points.values());
                    firstPartYearCount = !pointList.isEmpty() && pointList.get(0).length > 0 ? pointList.get(0)[1] : firstPartYearCount;
                }

                for (Long courseId : dataMap.keySet())
                {
                    int number = 0;
                    for (EppWeek week : weekList)
                    {
                        if (number > firstPartYearCount) continue;
                        Map<Integer, EppWeekType> weekMap = dataMap.get(courseId).get(week.getId());
                        if (null != weekMap)
                        {
                            for (EppWeekType weekType : weekMap.values())
                            {
                                if (null != weekType && EppWeekTypeCodes.HOLIDAYS.equals(weekType.getCode()))
                                {
                                    weekValue++;
                                }
                            }
                        }
                        number++;
                    }
                }

                if (weekValue != defaultValue)
                    saveFefuEpvCheckResult(block, checkTime, "Число недель каникул первой части учебного года (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(weekValue) + ") не соответствует значению, заданному в стандарте (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(defaultValue) + ")");
                else
                    saveFefuEpvCheckResult(block, checkTime, message + " - OK");
            }
            else saveFefuEpvCheckResult(block, checkTime, message + " - Проверка пропущена");
        }
    }

    private void checkScheduleEppWeekTypeNumber(EppEduPlanVersionBlock block, Date checkTime, Map<String, Double> totalDataWeekMap, Double defaultValue, String message, String messageGenitive, String... weekTypeCodes)
    {
        if (null == defaultValue)
        {
            saveFefuEpvCheckResult(block, checkTime, message + " - Проверка пропущена");
            return;
        }

        Double weekValue = null;
        for (String weekTypeCode : weekTypeCodes)
        {
            for (Map.Entry<String, Double> weekEntry : totalDataWeekMap.entrySet())
            {
                if (weekTypeCode.equals(weekEntry.getKey()))
                {
                    if (null == weekValue) weekValue = 0.0;
                    weekValue += weekEntry.getValue();
                }
            }
        }
        if (null == weekValue) weekValue = 0.0;

        if (!weekValue.equals(defaultValue))
            saveFefuEpvCheckResult(block, checkTime, "Число недель " + messageGenitive + " (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(weekValue) + ") не соответствует значению, заданному в стандарте (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(defaultValue) + ")");
        else
            saveFefuEpvCheckResult(block, checkTime, message + " - OK");
    }

    /**
     * Проверяет:
     * - отношение суммы средней аудиторной и самостоятельной нагрузки в неделю к общему числу недель теоретического обучения из уч. графика;
     * - отношение суммы средней аудиторной и самостоятельной нагрузки в неделю к общему числу недель экзаменационных сессий из уч. графика;
     * - отношение средней аудиторной нагрузки к общему числу недель теоретического обучения из уч. графика.
     */
    private void checkTotalAuditAndSelfWorkAvgLoads(EppEduPlanVersionBlock block, Date checkTime, FefuEppStateEduStandardParameters parameters, IEppEpvBlockWrapper blockWrapper, Collection<IEppEpvRowWrapper> filteredRows)
    {
        EppEpvTotalRow totalAvgRow = EppEpvTotalRow.getTotalRowAvgLoad(blockWrapper, filteredRows);

        checkTotalAuditAndSelfWorkAvgLoad(block, checkTime, totalAvgRow, parameters.getMaxAmountAvgLoadTheory(), false, FefuSummaryBudgetRowWrapper.THEORY);
        checkTotalAuditAndSelfWorkAvgLoad(block, checkTime, totalAvgRow, parameters.getMaxAmountAvgLoadExamSession(), false, FefuSummaryBudgetRowWrapper.EXAM);
        checkTotalAuditAndSelfWorkAvgLoad(block, checkTime, totalAvgRow, parameters.getMaxAmountAvgAuditLoadTheory(), true, FefuSummaryBudgetRowWrapper.THEORY);
    }

    private void checkTotalAuditAndSelfWorkAvgLoad(EppEduPlanVersionBlock block, Date checkTime, EppEpvTotalRow totalAvgRow, Integer value, Boolean isOnlyAudit, String weekTypeCode)
    {
        String periodType;
        switch (weekTypeCode)
        {
            case FefuSummaryBudgetRowWrapper.THEORY:
                periodType = "теоретического обучения";
                break;
            case FefuSummaryBudgetRowWrapper.EXAM:
                periodType = "экзаменационной сессии";
                break;
            default:
                periodType = "";
                break;
        }

        String message = "Средняя нагрузка (" + (isOnlyAudit ? "аудиторная" : "аудиторная и самостоятельная") + ") в период " + periodType;
        if (null == value)
        {
            saveFefuEpvCheckResult(block, checkTime, message + " - Проверка пропущена");
            return;
        }
        double defaultValue = value.doubleValue();

        EppEduPlanVersion version = block.getEduPlanVersion();
        Map<Integer, Map<Integer, Map<String, Double>>> summaryBudgetDataWeekMap = FefuEduPlanManager.instance().dao().getEpvSummaryBudgetDataMap(version);
        Map<String, Double> totalDataWeekMap = summaryBudgetDataWeekMap.get(0).get(0);
        Double weekTypeValue = totalDataWeekMap.get(weekTypeCode);

        EppEduPlan eduPlan = block.getEduPlanVersion().getEduPlan();
        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), eduPlan.getDevelopGrid()))
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());

        Double total = 0.0;
        for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet())
        {
            Integer courseNumber = entry.getKey();
            Set<Integer> termNumbers = entry.getValue();

            int endTerm = courseNumber * termNumbers.size() + 1;
            int beginTerm = endTerm - termNumbers.size();
            for (int term = beginTerm; term < endTerm; term++)
            {
                String audit = totalAvgRow.getLoadValue(term, EppELoadType.FULL_CODE_AUDIT);

                if (!audit.isEmpty())
                    total += Double.parseDouble(audit.replace(",", "."));

                if (!isOnlyAudit)
                {
                    String selfWork = totalAvgRow.getLoadValue(term, EppELoadType.FULL_CODE_SELFWORK);
                    if (!selfWork.isEmpty())
                        total += Double.parseDouble(selfWork.replace(",", "."));
                }
            }
        }

        double week2Total = weekTypeValue == null || weekTypeValue == 0.0 ? 0.0 : total / weekTypeValue;

        if (UniEppUtils.eq(week2Total, defaultValue))
            saveFefuEpvCheckResult(block, checkTime, message + " - OK");
        else
            saveFefuEpvCheckResult(block, checkTime, message + " (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(week2Total) + ") не соответствует заданному в стандарте значению (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(defaultValue) + ")");
    }

    /**
     * Минимальная трудоемкость одной дисциплины, в ЗЕТ
     */
    private void checkMinLaborOneDiscipline(EppEduPlanVersionBlock block, Date checkTime, Collection<IEppEpvRowWrapper> filteredRows, Integer minLabor)
    {
        boolean check = true;
        String message = "Минимальная трудоемкость одной дисциплины";

        if (null == minLabor)
        {
            saveFefuEpvCheckResult(block, checkTime, message + " - Проверка пропущена");
            return;
        }

        for (IEppEpvRowWrapper rowWrapper : filteredRows)
        {
            double labor = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
            if (labor < minLabor)
            {
                check = false;
                saveFefuEpvCheckResult(block, checkTime, "Трудоемкость дисциплины «" + rowWrapper.getTitle() + "» (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(labor) + ") не соответствует, заданному в стандарте значению (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(minLabor) + ")");
            }
        }

        if (check)
            saveFefuEpvCheckResult(block, checkTime, message + " - OK");
    }

    /**
     * Количество часов на подготовку к экзамену
     */
    private void checkHoursForOneExamFullTimeForm(EppEduPlanVersionBlock block, Date checkTime, Collection<IEppEpvRowWrapper> filteredRows, Integer examHours)
    {
        boolean check = true;
        String message = "Количество часов на подготовку к экзамену";

        if (null == examHours) saveFefuEpvCheckResult(block, checkTime, message + " - Проверка пропущена");
        else
        {
            EppEduPlan eduPlan = block.getEduPlanVersion().getEduPlan();
            Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
            for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), eduPlan.getDevelopGrid()))
                SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());

            List<EppControlActionType> actionTypes = IEppWorkPlanDAO.instance.get().getActiveControlActionTypes(null);

            for (IEppEpvRowWrapper rowWrapper : filteredRows)
            {
                List<String> termList = Lists.newArrayList();
                boolean checkTerm = true;

                for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet())
                {
                    Integer courseNumber = entry.getKey();
                    Set<Integer> termNumbers = entry.getValue();

                    int endTerm = courseNumber * termNumbers.size() + 1;
                    int beginTerm = endTerm - termNumbers.size();
                    for (int term = beginTerm; term < endTerm; term++)
                    {
                        for (EppControlActionType actionType : actionTypes)
                        {
                            String actionCode = actionType.getCode();

                            if (actionType instanceof EppFControlActionType && actionCode.equals(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM))
                            {
                                int actionCount = rowWrapper.getActionSize(term, actionType.getFullCode());
                                if (actionCount > 0)
                                {
                                    double control = rowWrapper.getTotalLoad(term, EppLoadType.FULL_CODE_CONTROL);
                                    if (control != examHours)
                                    {
                                        check = false;
                                        checkTerm = false;
                                        termList.add("сем." + term + " (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(control) + ")");
                                    }
                                }
                            }
                        }
                    }
                }
                if (!checkTerm)
                    saveFefuEpvCheckResult(block, checkTime, message + " «" + rowWrapper.getTitle() + "», " + StringUtils.join(termList, ", ") + " не соответствует, заданному в стандарте значению (" + examHours + ")");
            }
            if (check)
                saveFefuEpvCheckResult(block, checkTime, message + " - OK");
        }
    }

    /**
     * Проверяет долю лекционных занятий.
     */
    private void checkEpvLecturesPercent(EppEduPlanVersionBlock block, Date checkTime, EppEpvTotalRow totalRow, Integer percent)
    {
        String message = "Доля лекционных занятий";
        if (null == percent || null == totalRow)
        {
            saveFefuEpvCheckResult(block, checkTime, message + " - Проверка пропущена");
            return;
        }

        String totalHours = totalRow.getLoadValue(0, EppLoadType.FULL_CODE_TOTAL_HOURS);
        String lectures = totalRow.getLoadValue(0, EppALoadType.FULL_CODE_TOTAL_LECTURES);

        Double totalHoursValue = totalHours.isEmpty() ? 0.0 : Double.parseDouble(totalHours.replace(",", "."));
        Double lecturesValue = lectures.isEmpty() ? 0.0 : Double.parseDouble(lectures.replace(",", "."));
        double value = totalHoursValue == 0.0 ? 0.0 : lecturesValue / totalHoursValue * 100;

        if (value <= percent)
            saveFefuEpvCheckResult(block, checkTime, message + " - OK");
        else
            saveFefuEpvCheckResult(block, checkTime, message + " (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value) + "%) не соответствует значению, заданному в стандарте (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(percent) + "%)");
    }

    /**
     * Проверяет долю интерактивных занятий.
     */
    private void checkEpvInteractiveLessonsPercent(EppEduPlanVersionBlock block, Date checkTime, EppEpvTotalRow totalRow, Collection<IEppEpvRowWrapper> filteredRows, Integer percent)
    {
        String message = "Доля занятий в интерактивной форме";
        if (null == percent || null == totalRow)
        {
            saveFefuEpvCheckResult(block, checkTime, message + " - Проверка пропущена");
            return;
        }

        Double totalAudit = 0.0;
        for (IEppEpvRowWrapper rowWrapper : filteredRows)
        {
            double lectures = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LECTURES_I);
            double practice = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_PRACTICE_I);
            double labs = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LABS_I);
            totalAudit += (lectures + practice + labs);
        }
        String totalHours = totalRow.getLoadValue(0, EppLoadType.FULL_CODE_TOTAL_HOURS);
        Double totalHoursValue = totalHours.isEmpty() ? 0 : Double.parseDouble(totalHours.replace(",", "."));
        double value = totalHoursValue == 0 ? 0 : totalAudit / totalHoursValue * 100;

        if (value >= percent)
            saveFefuEpvCheckResult(block, checkTime, message + " - OK");
        else
            saveFefuEpvCheckResult(block, checkTime, message + " (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value) + "%) не соответствует значению, заданному в стандарте (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(percent) + "%)");
    }

    /**
     * Проверяет долю дисциплин по выбору в вариативных частях.
     */
    private void checkEpvGroupImRowPercent(EppEduPlanVersionBlock block, Date checkTime, Collection<IEppEpvRowWrapper> rowWrapperList, Integer percent)
    {
        boolean check = true;
        boolean variablePartFound = false;
        String message = "Доля дисциплин по выбору в вариативных частях";

        if (null == percent)
        {
            saveFefuEpvCheckResult(block, checkTime, message + " - Проверка пропущена");
            return;
        }

        Map<IEppEpvRowWrapper, Collection<IEppEpvRowWrapper>> rowWrapperMap = Maps.newLinkedHashMap();
        for (IEppEpvRowWrapper rowWrapper : rowWrapperList)
        {
            IEppEpvRow epvRow = rowWrapper.getRow();
            if (!rowWrapperMap.containsKey(rowWrapper))
            {
                rowWrapperMap.put(rowWrapper, Lists.<IEppEpvRowWrapper>newArrayList());
            }
            for (IEppEpvRowWrapper childWrapper : rowWrapperList)
            {
                IEppEpvRow childRow = childWrapper.getRow();
                IEppEpvRowWrapper parent = childWrapper.getHierarhyParent();

                while (null != parent)
                {
                    IEppEpvRow parentRow = parent.getRow();
                    if (parentRow.equals(epvRow))
                    {
                        if (childRow instanceof EppEpvStructureRow)
                        {
                            EppEpvStructureRow structureRow = (EppEpvStructureRow) childRow;
                            if (FefuEduPlanPrintDAO.isBasePart(structureRow) || FefuEduPlanPrintDAO.isVariablePart(structureRow))
                            {
                                rowWrapperMap.get(rowWrapper).add(childWrapper);
                            }
                        }
                        else if (childRow instanceof EppEpvGroupImRow)
                        {
                            IEppEpvRowWrapper groupImRowParent = childWrapper.getHierarhyParent();
                            IEppEpvRow groupImRowEpvRow = groupImRowParent.getRow();
                            if (groupImRowEpvRow instanceof EppEpvStructureRow && FefuEduPlanPrintDAO.isVariablePart((EppEpvStructureRow) groupImRowEpvRow))
                            {
                                rowWrapperMap.get(rowWrapper).add(childWrapper);
                            }
                        }
                        parent = null;
                    }
                    else parent = parent.getHierarhyParent();
                }
            }
        }

        for (IEppEpvRowWrapper rowWrapper : rowWrapperList)
        {
            Collection<IEppEpvRowWrapper> childWrappers = rowWrapperMap.get(rowWrapper);
            if (null != childWrappers)
            {
                Double variableHours = null;
                double groupImRowHours = 0.0;

                for (IEppEpvRowWrapper childWrapper : childWrappers)
                {
                    IEppEpvRow childRow = childWrapper.getRow();

                    if (childRow instanceof EppEpvStructureRow)
                    {
                        EppEpvStructureRow structureRow = (EppEpvStructureRow) childRow;
                        if (FefuEduPlanPrintDAO.isVariablePart(structureRow))
                        {
                            variablePartFound = true;
                            if (null == variableHours) variableHours = 0.0;
                            variableHours += childWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                        }
                    }
                    else if (childRow instanceof EppEpvGroupImRow)
                    {
                        groupImRowHours += childWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                    }
                }
                if (null != variableHours)
                {
                    double value = variableHours == 0.0 ? 0.0 : groupImRowHours / variableHours * 100;
                    if (value < percent)
                    {
                        check = false;
                        saveFefuEpvCheckResult(block, checkTime, message + " «" + rowWrapper.getTitle() + "» (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value) + "%) не соответствует значению, заданному в стандарте (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(percent) + "%)");
                    }
                }
            }
        }

        if (!variablePartFound)
        {
            check = false;
            saveFefuEpvCheckResult(block, checkTime, message + " - Вариативная часть отсутствует");
        }
        if (check)
            saveFefuEpvCheckResult(block, checkTime, message + " - OK");
    }

    private void checkEduPlanVersionCompetence(EppEduPlanVersionBlock block, Date checkTime)
    {
        EppStateEduStandard eduStandard = block.getEduPlanVersion().getEduPlan().getParent();
        if(eduStandard == null)  saveFefuEpvCheckResult(block, checkTime, "Компетенции - OK");//другой проверки кроме как по стандарту нет

        // берем компетенции из блока УПв
        List<FefuAbstractCompetence2EpvRegistryRowRel> rels = getList(FefuAbstractCompetence2EpvRegistryRowRel.class, FefuAbstractCompetence2EpvRegistryRowRel.registryRow().owner(), block);
        Collection<String> competenceEpvList = rels.stream().map(FefuAbstractCompetence2EpvRegistryRowRel::getGroupCode).collect(Collectors.toList());

        // берем перечень всех заданных в ГОС компетенций
        List<FefuCompetence2EppStateEduStandardRel> relList = getList(
                new DQLSelectBuilder()
                        .fromEntity(FefuCompetence2EppStateEduStandardRel.class, "rel")
                        .where(eq(property("rel", FefuCompetence2EppStateEduStandardRel.eduStandard()), value(eduStandard)))
                        .order(property("rel", FefuCompetence2EppStateEduStandardRel.number())));

        final Boolean[] checkSuccessful = {true};
        relList.stream()
                .map(FefuCompetence2EppStateEduStandardRel::getCompetenceGroupCode)
                .filter(competenceEpvList::contains)
                .forEach(code -> {
                    checkSuccessful[0] = false;
                    saveFefuEpvCheckResult(block, checkTime, "Компетенция «" + code + "» отсутствует в учебном плане");
                });
        if (checkSuccessful[0]) saveFefuEpvCheckResult(block, checkTime, "Компетенции - OK");
    }

    @SuppressWarnings("unchecked")
    private void checkEduPlanVersionStructure(EppEduPlanVersionBlock block, Date checkTime)
    {
        boolean checkSuccessful = true;
        EppEduPlan eduPlan = block.getEduPlanVersion().getEduPlan();
        EduProgramQualification qualification = eduPlan instanceof EppEduPlanProf ? ((EppEduPlanProf) eduPlan).getProgramQualification() : null;

        if (null == qualification)
            saveFefuEpvCheckResult(block, checkTime, "При выполнении проверки по трудоемкости блоков, не удалось определить квалификацию УП - Проверка пропущена");
        else
        {
            List<FefuEppStateEduStandardLaborBlocks> notFoundBlockList = Lists.newArrayList();
            List<FefuEppStateEduStandardLaborBlocks> blockList = FefuEduStdManager.instance().dao().getEduStandardLaborBlockRows(eduPlan.getParent(), qualification);

            IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(Collections.singleton(block.getId()), true).get(block.getId());
            Collection<IEppEpvRowWrapper> rows = blockWrapper.getRowMap().values();
            PlaneTree tree = new PlaneTree((Collection) rows);
            Collection<IEppEpvRowWrapper> rowWrapperList = (Collection) Arrays.asList(tree.getFlatTreeObjects());

            // находим элементы, которые отсутствуют в структуре УПв
            for (FefuEppStateEduStandardLaborBlocks laborBlock : blockList)
            {
                boolean blockFound = false;
                for (IEppEpvRowWrapper rowWrapper : rowWrapperList)
                {
                    if (!blockFound && rowWrapper.getRow() instanceof EppEpvStructureRow)
                    {
                        EppEpvStructureRow row = (EppEpvStructureRow) rowWrapper.getRow();
                        if (row.getValue().equals(laborBlock.getBlock()) && isRootBlocksEquals(laborBlock, rowWrapper))
                        {
                            blockFound = true;
                        }
                    }
                }
                if (!blockFound)
                {
                    checkSuccessful = false;
                    notFoundBlockList.add(laborBlock);
                    saveFefuEpvCheckResult(block, checkTime, "Отсутствует элемент «" + laborBlock.getTitle() + "»");
                }
            }

            // удаляем из общего списка блоков ГОСа, те элементы, которых нет в УПв
            blockList.removeAll(notFoundBlockList);

            // проверяем, попадает ли в диапазон ГОСа каждый из блоков
            for (IEppEpvRowWrapper rowWrapper : rowWrapperList)
            {
                if (rowWrapper.getRow() instanceof EppEpvStructureRow)
                {
                    EppEpvStructureRow row = (EppEpvStructureRow) rowWrapper.getRow();
                    for (FefuEppStateEduStandardLaborBlocks laborBlock : blockList)
                    {
                        if (row.getValue().equals(laborBlock.getBlock()) && isRootBlocksEquals(laborBlock, rowWrapper))
                        {
                            double labor = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                            int min = laborBlock.getMinNumber();
                            int max = laborBlock.getMaxNumber();

                            if (min > labor || labor > max)
                            {
                                checkSuccessful = false;
                                saveFefuEpvCheckResult(block, checkTime, "Для элемента «" + laborBlock.getTitle() + "» трудоемкость не соответствует заданной в стандарте (от " + min + " до " + max + ")");
                            }
                            break;
                        }
                    }
                }
            }
            if (checkSuccessful) saveFefuEpvCheckResult(block, checkTime, "Структура - OK");
        }
    }

    @SuppressWarnings("unchecked")
    private void checkEduPlanVersionControlAction(EppEduPlanVersionBlock block, Date checkTime)
    {
        EppEduPlan eduPlan = block.getEduPlanVersion().getEduPlan();
        EppStateEduStandard eduStandard = eduPlan.getParent();
        if (null == eduStandard) return;

        String messagePartYear = "Число мероприятий в части учебного года";
        String messageYear = "Число мероприятий в учебном году";

        List<FefuFControlActionType2EppStateEduStandardRel> relList = getList(FefuFControlActionType2EppStateEduStandardRel.class, FefuFControlActionType2EppStateEduStandardRel.eduStandard(), eduStandard);
        if (relList.isEmpty())
        {
            saveFefuEpvCheckResult(block, checkTime, messagePartYear + " - Проверка пропущена");
            saveFefuEpvCheckResult(block, checkTime, messageYear + " - Проверка пропущена");
            return;
        }

        IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(Collections.singleton(block.getId()), true).get(block.getId());
        Collection<IEppEpvRowWrapper> rows = blockWrapper.getRowMap().values();
        PlaneTree tree = new PlaneTree((Collection) rows);
        Collection<IEppEpvRowWrapper> rowWrapperList = (Collection) Arrays.asList(tree.getFlatTreeObjects());
        Collection<IEppEpvRowWrapper> filteredRows = EppEpvTotalRow.filterRowsForTotalStats(rowWrapperList);

        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), eduPlan.getDevelopGrid()))
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());

        boolean checkPartYear = true;
        boolean checkYear = true;

        for (FefuFControlActionType2EppStateEduStandardRel rel : relList)
        {
            EppFControlActionType controlAction = rel.getControlAction();
            int maxPartYear = rel.getMaxPartYear();
            int maxYear = rel.getMaxYear();

            for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet())
            {
                Integer courseNumber = entry.getKey();
                Set<Integer> termNumbers = entry.getValue();

                int endTerm = courseNumber * termNumbers.size() + 1;
                int beginTerm = endTerm - termNumbers.size();

                int actionCount = 0;
                for (int term = beginTerm; term < endTerm; term++)
                {
                    int actionPartCount = 0;
                    for (IEppEpvRowWrapper rowWrapper : filteredRows)
                    {
                        actionPartCount += rowWrapper.getActionSize(term, controlAction.getFullCode());
                    }
                    if (maxPartYear != 0.0 && actionPartCount > maxPartYear)
                    {
                        checkPartYear = false;
                        saveFefuEpvCheckResult(block, checkTime, "Число мероприятий «" + controlAction.getTitle() + "»(" + actionPartCount + ") в части учебного года (сем. " + term + ") не соответствует заданному в стандарте значению (" + maxPartYear + ")");
                    }
                    actionCount += actionPartCount;
                }
                if (maxYear != 0.0 && actionCount > maxYear)
                {
                    checkYear = false;
                    saveFefuEpvCheckResult(block, checkTime, "Число мероприятий «" + controlAction.getTitle() + "»(" + actionCount + ") в учебном году (" + courseNumber + " курс) не соответствует заданному в стандарте значению (" + maxYear + ")");
                }
            }
        }
        if (checkPartYear)
            saveFefuEpvCheckResult(block, checkTime, messagePartYear + " - OK");
        if (checkYear)
            saveFefuEpvCheckResult(block, checkTime, messageYear + " - OK");
    }

    @SuppressWarnings("unchecked")
    private void checkEduPlanVersionAdditionalChecks(EppEduPlanVersionBlock block, Date checkTime)
    {
        EppEduPlanVersion version = block.getEduPlanVersion();
        EppEduPlan eduPlan = version.getEduPlan();
        EppStateEduStandard eduStandard = eduPlan.getParent();
        if (null == eduStandard) return;

        FefuEppStateEduStandardAdditionalChecks additionalChecks = getByNaturalId(new FefuEppStateEduStandardAdditionalChecks.NaturalId(eduStandard));
        if (null == additionalChecks) return;

        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), eduPlan.getDevelopGrid()))
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());

        IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(block.getId(), true);
        Collection<IEppEpvRowWrapper> rows = blockWrapper.getRowMap().values();
        PlaneTree tree = new PlaneTree((Collection) rows);
        Collection<IEppEpvRowWrapper> rowWrapperList = (Collection) Arrays.asList(tree.getFlatTreeObjects());
        Collection<IEppEpvRowWrapper> filteredRows = EppEpvTotalRow.filterRowsForTotalStats(rowWrapperList);


        Map<EppRegistryElement, IEppEpvRowWrapper> elementMap = Maps.newHashMap();
        List<EppRegistryElement> disciplineAndPracticeList = Lists.newArrayList();
        List<IEppEpvRowWrapper> groupImRowList = Lists.newArrayList();
        Map<String, List<IEppEpvRowWrapper>> rowTitleMap = Maps.newHashMap();
        Map<String, List<EppRegistryElement>> elementTitleMap = Maps.newHashMap();

        for (IEppEpvRowWrapper rowWrapper : filteredRows)
        {
            IEppEpvRow epvRow = rowWrapper.getRow();

            if (epvRow instanceof EppEpvRegistryRow)
            {
                EppEpvRegistryRow row = (EppEpvRegistryRow) epvRow;
                EppRegistryElement element = row.getRegistryElement();
                String rowTitle = rowWrapper.getTitle();

                // отбираем строки УП, где есть связь с элементом реестра
                if (null != element)
                {
                    elementMap.put(element, rowWrapper);
                    if (element instanceof EppRegistryDiscipline || element instanceof EppRegistryPractice)
                    {
                        disciplineAndPracticeList.add(element);
                    }

                    String title = element.getTitle();
                    if (!elementTitleMap.containsKey(title))
                        elementTitleMap.put(title, Lists.<EppRegistryElement>newArrayList());
                    elementTitleMap.get(title).add(element);
                }

                if (!rowTitleMap.containsKey(rowTitle))
                    rowTitleMap.put(rowTitle, Lists.<IEppEpvRowWrapper>newArrayList());
                rowTitleMap.get(rowTitle).add(rowWrapper);
            }
            else if (epvRow instanceof EppEpvGroupImRow)
            {
                groupImRowList.add(rowWrapper);
            }
        }
        addDiscAndPracticeWithGroupImRows(disciplineAndPracticeList, elementMap, groupImRowList);
        Set<EppRegistryElement> elementList = elementMap.keySet();
        Map<Long, IEppRegElWrapper> elementDataMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(ids(elementList));

        List<Long> moduleIds = Lists.newArrayList();
        for (EppRegistryElement element : elementList)
        {
            if (element instanceof EppRegistryDiscipline)
            {
                IEppRegElWrapper wrapper = elementDataMap.get(element.getId());
                for (IEppRegElPartWrapper partWrapper : wrapper.getPartMap().values())
                {
                    if (null == partWrapper) continue;
                    for (IEppRegElPartModuleWrapper moduleWrapper : partWrapper.getModuleMap().values())
                    {
                        EppRegistryElementPartModule partModule = moduleWrapper.getItem();
                        moduleIds.add(partModule.getModule().getId());
                    }
                }
            }
        }

        Map<Long, EppRegistryModuleALoad> aLoadMap = Maps.newHashMap();
        Map<Long, Double> selfLoadMap = Maps.newHashMap();

        List<EppRegistryModuleALoad> aLoads = new DQLSelectBuilder().fromEntity(EppRegistryModuleALoad.class, "al")
                .where(in(property("al", EppRegistryModuleALoad.module().id()), moduleIds))
                .createStatement(getSession()).list();

        List<FefuEppRegistryModuleELoad> selfLoads = new DQLSelectBuilder().fromEntity(FefuEppRegistryModuleELoad.class, "el")
                .where(eq(property("el", FefuEppRegistryModuleELoad.loadType().code()), value(EppELoadTypeCodes.TYPE_TOTAL_SELFWORK)))
                .where(in(property("el", FefuEppRegistryModuleELoad.module().id()), moduleIds))
                .createStatement(getSession()).list();

        for (EppRegistryModuleALoad aLoad : aLoads)
            aLoadMap.put(aLoad.getModule().getId(), aLoad);

        for (FefuEppRegistryModuleELoad load : selfLoads)
            selfLoadMap.put(load.getModule().getId(), load.getLoadAsDouble());

        // проверка 1
        if (additionalChecks.isAltInChoiceDisciplines())
        {
            boolean check = true;
            for (IEppEpvRowWrapper rowWrapper : groupImRowList)
            {
                EppEpvGroupImRow row = (EppEpvGroupImRow) rowWrapper.getRow();
                Double size = (double) row.getSize();
                Double childSize = (double) rowWrapper.getChilds().size();

                if (size == 0 || childSize / size <= 1)
                {
                    check = false;
                    saveFefuEpvCheckResult(block, checkTime, "Группа дисциплин по выбору «" + rowWrapper.getTitle() + "» сформирована некорректно");
                }
            }
            if (check)
                saveFefuEpvCheckResult(block, checkTime, "Альтернативные дисциплины в блоках дисциплин по выбору - OK");
        }
        // проверка 2
        if (additionalChecks.isControlActionOnTotalRegElement())
        {
            boolean check = true;

            // заполняем части
            List<EppRegistryElementPart> partList = Lists.newArrayList();
            for (EppRegistryElement element : disciplineAndPracticeList)
            {
                IEppRegElWrapper wrapper = elementDataMap.get(element.getId());
                Map<Integer, IEppRegElPartWrapper> partMap = wrapper.getPartMap();

                for (IEppRegElPartWrapper partWrapper : partMap.values())
                    partList.add(partWrapper.getItem());
            }

            if (!partList.isEmpty())
            {
                Map<EppRegistryElement, Map<Integer, List<EppFControlActionType>>> elementPartMap = Maps.newHashMap();

                DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                        new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "ca")
                                .joinPath(DQLJoinType.left, EppRegistryElementPartFControlAction.part().fromAlias("ca"), "p")
                                .where(in(property("p"), partList))
                                .order(property("p", EppRegistryElementPart.registryElement()))
                                .order(property("p", EppRegistryElementPart.number()))
                );

                int element_col = dql.column(property("p", EppRegistryElementPart.registryElement()));
                int partNumber_col = dql.column(property("p", EppRegistryElementPart.number()));
                int controlAction_col = dql.column(property("ca", EppRegistryElementPartFControlAction.controlAction()));

                // заполняем формы итогового контроля
                List<Object[]> items = dql.getDql().createStatement(getSession()).list();
                for (Object[] item : items)
                {
                    EppRegistryElement element = (EppRegistryElement) item[element_col];
                    Integer partNumber = (Integer) item[partNumber_col];
                    EppFControlActionType controlAction = (EppFControlActionType) item[controlAction_col];

                    if (!elementPartMap.containsKey(element))
                        elementPartMap.put(element, Maps.<Integer, List<EppFControlActionType>>newHashMap());
                    Map<Integer, List<EppFControlActionType>> partMap = elementPartMap.get(element);
                    if (!partMap.containsKey(partNumber))
                        partMap.put(partNumber, Lists.<EppFControlActionType>newArrayList());

                    partMap.get(partNumber).add(controlAction);
                }

                for (EppRegistryElement element : disciplineAndPracticeList)
                {
                    Map<Integer, List<EppFControlActionType>> partMap = elementPartMap.get(element);

                    if (null == partMap)
                    {
                        check = false;
                        saveFefuEpvCheckResult(block, checkTime, "Мероприятие «" + element.getTitle() + "» не имеет форм промежуточного контроля");
                    }
                    else
                    {
                        int number = 1;
                        for (Map.Entry<Integer, List<EppFControlActionType>> entry : partMap.entrySet())
                        {
                            int partNumber = entry.getKey();
                            if (number != partNumber || entry.getValue().isEmpty())
                            {
                                for (int part = number; part < partNumber; part++)
                                {
                                    check = false;
                                    saveFefuEpvCheckResult(block, checkTime, "Мероприятие («" + element.getTitle() + "», " + part + " часть) не имеет форму промежуточного контроля");
                                }

                                number = partNumber;
                            }
                            number++;
                        }
                        if (number != element.getParts())
                        {
                            for (int part = number; part < element.getParts(); part++)
                            {
                                check = false;
                                saveFefuEpvCheckResult(block, checkTime, "Мероприятие («" + element.getTitle() + "», " + part + " часть) не имеет форму промежуточного контроля");
                            }
                        }
                    }
                }
            }
            if (check)
                saveFefuEpvCheckResult(block, checkTime, "Наличие контрольных мероприятий по итогам прохождения мероприятия - OK");
        }
        // проверка 3
        if (additionalChecks.isFormingCompetenceRegElement())
        {
            boolean check = true;
            StaticListDataSource<FefuEpvRowWrapper> dataSource = FefuEduPlanManager.instance().dao().getEduPlanVersionBlockDataSourceMap(version.getId()).get(block).getFirst();
            for (FefuEpvRowWrapper rowWrapper : dataSource.getRowList())
            {
                EppEpvRow epvRow = rowWrapper.getRow();
                if (epvRow instanceof EppEpvRegistryRow)
                {
                    EppEpvRegistryRow row = (EppEpvRegistryRow) epvRow;
                    EppRegistryElement element = row.getRegistryElement();

                    if (null != element && element instanceof EppRegistryDiscipline || element instanceof EppRegistryPractice)
                    {
                        if (rowWrapper.getCompetences().isEmpty())
                        {
                            check = false;
                            saveFefuEpvCheckResult(block, checkTime, "Мероприятие «" + rowWrapper.getTitle() + "» не формирует компетенции");
                        }
                    }
                }
            }
            if (check)
                saveFefuEpvCheckResult(block, checkTime, "Формирование компетенций мероприятиями - OK");
        }
        // проверка 4
        if (additionalChecks.isIntegralityLaborPartYear())
        {
            boolean check = true;
            for (EppRegistryElement element : elementList)
            {
                IEppRegElWrapper wrapper = elementDataMap.get(element.getId());
                Map<Integer, IEppRegElPartWrapper> partMap = wrapper.getPartMap();

                for (IEppRegElPartWrapper partWrapper : partMap.values())
                {
                    EppRegistryElementPart part = partWrapper.getItem();
                    Double labor = part.getLaborAsDouble();
                    if (null != labor && labor % 1 != 0)
                    {
                        check = false;
                        saveFefuEpvCheckResult(block, checkTime, "Мероприятие («" + element.getTitle() + "», " + part.getNumber() + " часть) имеет нецелое значение трудоемкости (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(labor) + ")");
                    }
                }
            }
            if (check)
                saveFefuEpvCheckResult(block, checkTime, "Целочисленность трудоемкости мероприятий в части учебного года - OK");
        }
        // проверка 5
        if (additionalChecks.isEvenAuditHours())
        {
            boolean check = true;
            for (EppRegistryElement element : disciplineAndPracticeList)
            {
                IEppRegElWrapper wrapper = elementDataMap.get(element.getId());
                Map<Integer, IEppRegElPartWrapper> partMap = wrapper.getPartMap();

                for (IEppRegElPartWrapper partWrapper : partMap.values())
                {
                    if (null == partWrapper) continue;
                    EppRegistryElementPart part = partWrapper.getItem();

                    for (String aLoadCode : EppALoadType.FULL_CODES)
                    {
                        double value = 0.0;
                        for (IEppRegElPartModuleWrapper moduleWrapper : partWrapper.getModuleMap().values())
                            value += moduleWrapper.getLoadAsDouble(aLoadCode);

                        if (value % 2 != 0)
                        {
                            check = false;
                            saveFefuEpvCheckResult(block, checkTime, "Мероприятие («" + element.getTitle() + "», " + part.getNumber() + " часть) имеет нечетное число аудиторных часов (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value) + ")");
                        }
                    }
                }
            }
            if (check)
                saveFefuEpvCheckResult(block, checkTime, "Четность аудиторных часов - OK");
        }
        // проверка 6
        if (additionalChecks.isMultipleAuditHoursPartYear())
        {
            boolean check = true;
            Map<Integer, Map<Integer, Map<String, Double>>> summaryBudgetDataWeekMap = FefuEduPlanManager.instance().dao().getEpvSummaryBudgetDataMap(version);

            for (EppRegistryElement element : disciplineAndPracticeList)
            {
                IEppRegElWrapper wrapper = elementDataMap.get(element.getId());
                Map<Integer, IEppRegElPartWrapper> partMap = wrapper.getPartMap();

                for (IEppRegElPartWrapper partWrapper : partMap.values())
                {
                    if (null == partWrapper) continue;
                    EppRegistryElementPart part = partWrapper.getItem();

                    double audit = 0.0;
                    for (String aLoadCode : EppALoadType.FULL_CODES)
                    {
                        for (IEppRegElPartModuleWrapper moduleWrapper : partWrapper.getModuleMap().values())
                            audit += moduleWrapper.getLoadAsDouble(aLoadCode);
                    }

                    boolean partFound = false;
                    for (Map.Entry<Integer, Map<Integer, Map<String, Double>>> courseEntry : summaryBudgetDataWeekMap.entrySet())
                    {
                        Integer courseNumber = courseEntry.getKey();
                        if (partFound || courseNumber == 0) continue;

                        Map<Integer, Map<String, Double>> termMap = courseEntry.getValue();
                        Integer termNumbers = termMap.size() - 1;

                        int endTerm = courseNumber * termNumbers + 1;
                        int beginTerm = endTerm - termNumbers;

                        for (int term = beginTerm; term < endTerm; term++)
                        {
                            if (!partFound && term == part.getNumber())
                            {
                                Map<String, Double> valueMap = termMap.get(part.getNumber());
                                if (null != valueMap && valueMap.containsKey(FefuSummaryBudgetRowWrapper.THEORY))
                                {
                                    Double theory = valueMap.get(FefuSummaryBudgetRowWrapper.THEORY);
                                    if (null != theory && theory != 0 && audit % theory != 0)
                                    {
                                        check = false;
                                        saveFefuEpvCheckResult(block, checkTime, "Мероприятие («" + element.getTitle() + "», " + part.getNumber() + " часть) имеет некратное числу недель (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(theory) + ") число аудиторных часов (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(audit) + ")");
                                    }
                                }
                                partFound = true;
                            }
                        }
                    }
                }
            }
            if (check)
                saveFefuEpvCheckResult(block, checkTime, "Кратность аудиторных часов - OK");
        }
        // проверка 7
        if (additionalChecks.isBindEduPlanAndFixOwner())
        {
            boolean check = true;
            for (IEppEpvRowWrapper rowWrapper : filteredRows)
            {
                IEppEpvRow epvRow = rowWrapper.getRow();
                if (epvRow instanceof EppEpvRegistryRow)
                {
                    String message = "";
                    EppEpvRegistryRow row = (EppEpvRegistryRow) epvRow;

                    if (null == row.getRegistryElement())
                    {
                        check = false;
                        message += "не имеет связи с реестровым элементом";
                    }
                    if (null == row.getOwner())
                    {
                        check = false;
                        if (!message.isEmpty()) message += " и ";
                        message += "не закреплено за ответственным подразделением";
                    }
                    if (!message.isEmpty())
                        saveFefuEpvCheckResult(block, checkTime, "Мероприятие «" + rowWrapper.getTitle() + "» " + message);
                }
            }
            if (check)
                saveFefuEpvCheckResult(block, checkTime, "Связь с реестром и закрепление за ответственными подразделениями - OK");
        }
        // проверка 8
        if (additionalChecks.isEduPlanConformityScheduleInPractices())
        {
            boolean check = true;
            boolean checkEpvTeach = false;
            boolean checkEpvWork = false;
            boolean checkScheduleTeach = false;
            boolean checkScheduleWork = false;

            List<String> teachList = Lists.newArrayList();
            List<String> workList = Lists.newArrayList();
            Map<Integer, Map<Integer, Map<String, Double>>> summaryBudgetDataWeekMap = FefuEduPlanManager.instance().dao().getEpvSummaryBudgetDataMap(version);
            Map<String, Double> totalDataWeekMap = summaryBudgetDataWeekMap.get(0).get(0);

            EppRegistryStructure practiceTeach = getByCode(EppRegistryStructure.class, EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING);
            EppRegistryStructure practiceWork = getByCode(EppRegistryStructure.class, EppRegistryStructureCodes.REGISTRY_PRACTICE_PRODUCTION);
            String[] teachCodes = new String[]{FefuSummaryBudgetRowWrapper.TEACH, FefuSummaryBudgetRowWrapper.TEACH_DISPERSED};
            String[] workCodes = new String[]{FefuSummaryBudgetRowWrapper.WORK, FefuSummaryBudgetRowWrapper.WORK_DISPERSED};

            for (int i = 0; i < teachCodes.length; i++)
                teachList.add(new FefuSummaryBudgetRowWrapper((long) i, teachCodes[i]).getTitle());
            for (int i = 0; i < workCodes.length; i++)
                workList.add(new FefuSummaryBudgetRowWrapper((long) i, workCodes[i]).getTitle());

            String teachTitle = StringUtils.join(teachList, " / ");
            String workTitle = StringUtils.join(workList, " / ");

            for (EppRegistryElement element : elementList)
            {
                if (element instanceof EppRegistryPractice)
                {
                    EppRegistryStructure group = element.getParent();

                    if (!group.equals(practiceTeach) && !group.equals(practiceWork))
                    {
                        check = false;
                        saveFefuEpvCheckResult(block, checkTime, "Практика «" + element.getTitle() + "», имеет неверное наименование группы («" + group.getTitle() + "»)");
                    }
                    if (group.equals(practiceTeach)) checkEpvTeach = true;
                    if (group.equals(practiceWork)) checkEpvWork = true;
                }
            }
            for (String code : teachCodes)
            {
                Double value = totalDataWeekMap.get(code);
                if (null != value && value == 0.0) checkScheduleTeach = true;
            }
            for (String code : workCodes)
            {
                Double value = totalDataWeekMap.get(code);
                if (null != value && value == 0.0) checkScheduleWork = true;
            }
            if (!checkScheduleTeach && checkEpvTeach)
                saveFefuEpvCheckResult(block, checkTime, "В учебном графике отсутствуют недели «" + teachTitle + "» при наличии «" + practiceTeach.getTitle() + "» в учебном плане");
            if (!checkScheduleWork && checkEpvWork)
                saveFefuEpvCheckResult(block, checkTime, "В учебном графике отсутствуют недели «" + workTitle + "» при наличии «" + practiceWork.getTitle() + "» в учебном плане");

            if (checkScheduleTeach && !checkEpvTeach)
                saveFefuEpvCheckResult(block, checkTime, "В учебном плане отсутствуют практики «" + practiceTeach.getTitle() + "» при наличии «" + teachTitle + "» в учебном графике");
            if (checkScheduleWork && !checkEpvWork)
                saveFefuEpvCheckResult(block, checkTime, "В учебном плане отсутствуют практики «" + practiceWork.getTitle() + "» при наличии «" + workTitle + "» в учебном графике");

            if (!checkScheduleTeach && !checkEpvTeach)
                saveFefuEpvCheckResult(block, checkTime, "В учебном плане и графике учебного процесса отсутствует элемент «" + practiceTeach.getTitle() + "»");
            if (!checkScheduleWork && !checkEpvWork)
                saveFefuEpvCheckResult(block, checkTime, "В учебном плане и графике учебного процесса отсутствует элемент «" + practiceWork.getTitle() + "»");

            check = check ? checkScheduleTeach && checkEpvTeach : check;
            if (check)
                saveFefuEpvCheckResult(block, checkTime, "Соответствие учебного плана графику учебного процесса в части практик - OK");
        }
        // проверка 9
        if (additionalChecks.isSelfWorkForAnyDiscipline())
        {
            boolean check = true;
            for (EppRegistryElement element : elementList)
            {
                if (element instanceof EppRegistryDiscipline)
                {
                    IEppRegElWrapper wrapper = elementDataMap.get(element.getId());
                    for (IEppRegElPartWrapper partWrapper : wrapper.getPartMap().values())
                    {
                        if (null == partWrapper) continue;
                        EppRegistryElementPart part = partWrapper.getItem();

                        double value = 0.0;
                        for (IEppRegElPartModuleWrapper moduleWrapper : partWrapper.getModuleMap().values())
                        {
                            EppRegistryElementPartModule partModule = moduleWrapper.getItem();
                            Double selfValue = selfLoadMap.get(partModule.getModule().getId());
                            if (null != selfValue)
                                value += selfValue;
                        }
                        if (value == 0)
                        {
                            check = false;
                            saveFefuEpvCheckResult(block, checkTime, "Мероприятие («" + element.getTitle() + "», " + part.getNumber() + " часть) не имеет часов самостоятельной работы");
                        }
                    }
                }
            }
            if (check)
                saveFefuEpvCheckResult(block, checkTime, "Наличие самостоятельной работы - OK");
        }
        // проверка 10
        if (additionalChecks.isDuplicateDisciplinesTitles())
        {
            boolean check = true;
            List<String> foundRowTitles = Lists.newArrayList();
            List<String> foundTitles = Lists.newArrayList();

            for (IEppEpvRowWrapper rowWrapper : filteredRows)
            {
                IEppEpvRow epvRow = rowWrapper.getRow();
                if (epvRow instanceof EppEpvRegistryRow)
                {
                    String title = rowWrapper.getTitle();
                    List<IEppEpvRowWrapper> copies = rowTitleMap.get(title);

                    if (!foundRowTitles.contains(title) && null != copies && !copies.isEmpty() && copies.size() > 1)
                    {
                        check = false;
                        saveFefuEpvCheckResult(block, checkTime, "Мероприятие «" + title + "» встречается в учебном плане более одного раза");
                        foundRowTitles.add(title);
                    }
                }
            }
            for (EppRegistryElement element : elementList)
            {
                String title = element.getTitle();
                List<EppRegistryElement> copies = elementTitleMap.get(title);

                if (!foundTitles.contains(title) && null != copies && !copies.isEmpty() && copies.size() > 1)
                {
                    check = false;
                    saveFefuEpvCheckResult(block, checkTime, "Мероприятие реестра №" + element.getNumber() + " встречается в учебном плане более одного раза");
                    foundTitles.add(title);
                }
            }
            if (check)
                saveFefuEpvCheckResult(block, checkTime, "Дублей в названиях дисциплин не выявлено");
        }
        // проверка 11
        if (additionalChecks.isRegElementConformityEpv())
        {
            boolean check = true;

            Map<String, Double> aLoadValueMap = Maps.newLinkedHashMap();
            for (EppALoadType aLoadType : getList(EppALoadType.class, EppALoadType.code().s()))
                aLoadValueMap.put(aLoadType.getFullCode(), 0.0);

            for (Map.Entry<EppRegistryElement, IEppEpvRowWrapper> entry : elementMap.entrySet())
            {
                EppRegistryElement element = entry.getKey();
                IEppEpvRowWrapper epvWrapper = entry.getValue();
                IEppRegElWrapper elWrapper = elementDataMap.get(element.getId());

                Map<Integer, IEppRegElPartWrapper> partMap = elWrapper.getPartMap();
                for (IEppRegElPartWrapper partWrapper : partMap.values())
                {
                    EppRegistryElementPart part = partWrapper.getItem();
                    Double partLabor = part.getLaborAsDouble();
                    double partSelfLoad = 0.0;

                    for (IEppRegElPartModuleWrapper moduleWrapper : partWrapper.getModuleMap().values())
                    {
                        EppRegistryElementPartModule partModule = moduleWrapper.getItem();
                        Long moduleId = partModule.getModule().getId();

                        EppRegistryModuleALoad aLoad = aLoadMap.get(moduleId);
                        if (null != aLoad)
                            aLoadValueMap.put(aLoad.getLoadType().getFullCode(), aLoad.getLoadAsDouble());

                        Double selfValue = selfLoadMap.get(moduleId);
                        if (null != selfValue)
                            partSelfLoad += selfValue;
                    }

                    for (Map.Entry<Integer, Set<Integer>> courseEntry : coursePartsMap.entrySet())
                    {
                        Integer courseNumber = courseEntry.getKey();
                        Set<Integer> termNumbers = courseEntry.getValue();

                        int endTerm = courseNumber * termNumbers.size() + 1;
                        int beginTerm = endTerm - termNumbers.size();
                        for (int term = beginTerm; term < endTerm; term++)
                        {
                            if (term == part.getNumber())
                            {
                                String preamble = "Для элемента («" + epvWrapper.getTitle() + "», " + term + " часть)";
                                double termLabor = epvWrapper.getTotalLoad(term, EppLoadType.FULL_CODE_LABOR);
                                double termSelfWork = epvWrapper.getTotalLoad(term, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);

                                if (termLabor != partLabor)
                                {
                                    check = false;
                                    saveFefuEpvCheckResult(block, checkTime, preamble + " трудоемкость (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(termLabor) + ") не соответствует реестровому элементу (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(partLabor) + ")");
                                }
                                for (Map.Entry<String, Double> aLoadEntry : aLoadValueMap.entrySet())
                                {
                                    String fullCode = aLoadEntry.getKey();
                                    Double partALoad = aLoadEntry.getValue();
                                    double termALoad = epvWrapper.getTotalLoad(term, fullCode);

                                    if (termALoad != partALoad)
                                    {
                                        String aLoadTitle = "";

                                        if (fullCode.equals(EppALoadType.FULL_CODE_TOTAL_LECTURES))
                                            aLoadTitle = "лекции";
                                        else if (fullCode.equals(EppALoadType.FULL_CODE_TOTAL_PRACTICE))
                                            aLoadTitle = "практические занятия";
                                        else if (fullCode.equals(EppALoadType.FULL_CODE_TOTAL_LABS))
                                            aLoadTitle = "лабораторные занятия";

                                        check = false;
                                        saveFefuEpvCheckResult(block, checkTime, preamble + " " + aLoadTitle + " (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(termALoad) + ") не соответствует реестровому элементу (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(partALoad) + ")");
                                    }
                                }
                                if (!UniEppUtils.eq(termSelfWork, partSelfLoad))
                                {
                                    check = false;
                                    saveFefuEpvCheckResult(block, checkTime, preamble + " часы самостоятельной работы (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(termSelfWork) + ") не соответствуют реестровому элементу (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(partSelfLoad) + ")");
                                }
                            }
                        }
                    }
                }
            }
            if (check)
                saveFefuEpvCheckResult(block, checkTime, "Соответствие нагрузки реестрового элемента и строки УПв - OK");
        }
    }

    private void addDiscAndPracticeWithGroupImRows(List<EppRegistryElement> disciplineAndPracticeList, Map<EppRegistryElement, IEppEpvRowWrapper> elementMap, List<IEppEpvRowWrapper> groupImRowList)
    {
        for (IEppEpvRowWrapper rowWrapper : groupImRowList)
        {
            IEppEpvRow epvRow = rowWrapper.getRow();

            if (epvRow instanceof EppEpvGroupImRow)
            {
                List<IEppEpvRowWrapper> childs = rowWrapper.getChilds();

                if (!childs.isEmpty())
                    addDiscAndPracticeWithGroupImRows(disciplineAndPracticeList, elementMap, childs);
            }
            else if (epvRow instanceof EppEpvRegistryRow)
            {
                EppEpvRegistryRow row = (EppEpvRegistryRow) epvRow;
                EppRegistryElement element = row.getRegistryElement();

                // отбираем строки УП, где есть связь с элементом реестра
                if (null != element)
                {
                    elementMap.put(element, rowWrapper);
                    if (element instanceof EppRegistryDiscipline || element instanceof EppRegistryPractice)
                    {
                        disciplineAndPracticeList.add(element);
                    }
                }
            }
        }
    }

    private Boolean isRootBlocksEquals(FefuEppStateEduStandardLaborBlocks laborBlock, IEppEpvRowWrapper rowWrapper)
    {
        boolean blockEquals = true;
        IEppEpvRowWrapper rowWrapperParent = rowWrapper.getHierarhyParent();
        FefuEppStateEduStandardLaborBlocks laborParentBlock = laborBlock.getParent();

        while (null != rowWrapperParent && null != laborParentBlock)
        {
            EppEpvStructureRow structureRow = (EppEpvStructureRow) rowWrapperParent.getRow();
            if (!structureRow.getValue().getId().equals(laborParentBlock.getBlock().getId())) blockEquals = false;

            rowWrapperParent = rowWrapperParent.getHierarhyParent();
            laborParentBlock = laborParentBlock.getParent();
        }
        return blockEquals && null == rowWrapperParent && null == laborParentBlock;
    }

    private void checkLaborFullTimeOneYear(EppEduPlan eduPlan, EppEduPlanVersionBlock block, EppEpvTotalRow totalRow, Date checkTime, Integer baseLaborOneYear)
    {
        if (null == totalRow) return;

        boolean checkSkipped = true;
        String code = eduPlan.getProgramForm().getCode();

        List<Integer> courseNumbers = Lists.newArrayList();
        List<DevelopGridTerm> developGridTermList = IDevelopGridDAO.instance.get().getDevelopGridTermList(eduPlan.getDevelopGrid());
        Map<Integer, Double> termLaborMap = Maps.newHashMap();

        for (DevelopGridTerm developGridTerm : developGridTermList)
        {
            String termLabor = totalRow.getLoadValue(developGridTerm.getTermNumber(), EppLoadType.FULL_CODE_LABOR);
            if (!StringUtils.isEmpty(termLabor))
            {
                checkSkipped = false;
                Integer courseNumber = developGridTerm.getCourse().getIntValue();
                if (!termLaborMap.containsKey(courseNumber)) termLaborMap.put(courseNumber, 0.0);
                termLaborMap.put(courseNumber, termLaborMap.get(courseNumber) + Double.parseDouble(termLabor.replace(",", ".")));
            }
        }
        for (Map.Entry<Integer, Double> entry : termLaborMap.entrySet())
        {
            if (!entry.getValue().equals(baseLaborOneYear.doubleValue())) courseNumbers.add(entry.getKey());
        }
        String programFormTitle = code.equals(EduProgramFormCodes.OCHNAYA) ? "очной формы" : code.equals(EduProgramFormCodes.ZAOCHNAYA) || code.equals(EduProgramFormCodes.OCHNO_ZAOCHNAYA) ? "очно-заочной и заочной форм" : "";
        String preamble = "Общая трудоемкость образовательной программы для " + programFormTitle + " освоения, реализуемой за один учебный год, в ЗЕТ - ";
        saveFefuEpvCheckResult(block, checkTime, preamble + (checkSkipped ? "Отсутствуют итоговые данные по общей трудоемкости за курс" : courseNumbers.isEmpty() ? "OK" : "не соответствует указанному в стандарте (" + baseLaborOneYear + ") для курсов: " + StringUtils.join(courseNumbers, ", ")));
    }

    private void checkMaxHoursLectureTypePercent(EppEduPlanVersionBlock block, Date checkTime, FefuEppStateEduStandardParameters parameters, Collection<IEppEpvRowWrapper> rowWrapperList)
    {
        boolean checkSuccessful = true;
        boolean blockFound = false;

        Integer maxHoursLectureType = parameters.getMaxHoursLectureTypePercent();
        EppPlanStructure blockLectures = parameters.getBlockCheckNumberLectures();
        List<IEppEpvRowWrapper> foundBlockList = Lists.newArrayList();

        for (IEppEpvRowWrapper row : rowWrapperList)
        {
            if (row.getTitle().equals(blockLectures.getTitle()))
            {
                blockFound = true;
                double hours = row.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_HOURS);
                double lectures = row.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LECTURES);
                double percent = hours == 0 ? 0 : (lectures / hours) * 100;

                if (percent > maxHoursLectureType) foundBlockList.add(row);
            }
        }

        String preamble = "Максимальное количество часов, отводимых на занятия лекционного типа - ";
        if (!blockFound)
            saveFefuEpvCheckResult(block, checkTime, preamble + "В УП(в) отсутствует блок, указанный в проверке");
        else
        {
            for (IEppEpvRowWrapper row : foundBlockList)
            {
                checkSuccessful = false;
                String parentBlock = foundBlockList.size() > 1 && null != row.getHierarhyParent() ? ", старший блок: «" + row.getHierarhyParent().getTitle() + "»" : "";
                saveFefuEpvCheckResult(block, checkTime, preamble + "Превышен процент (" + maxHoursLectureType + "%)" + parentBlock);
            }
            if (checkSuccessful) saveFefuEpvCheckResult(block, checkTime, preamble + "OK");
        }
    }

    private void checkMinUnitsChoiceDisciplines(EppEduPlanVersionBlock block, Date checkTime, FefuEppStateEduStandardParameters parameters, Collection<IEppEpvRowWrapper> rowWrapperList)
    {
        boolean checkMinUnitsSuccessful = true;
        boolean blockFound = false;

        List<IEppEpvRowWrapper> groupImRowList = Lists.newArrayList();
        Integer minUnitsChoiceDisciplines = parameters.getMinUnitsChoiceDisciplines();
        EppPlanStructure blockVariablePart = parameters.getBlockCheckZETVariablePart();

        for (IEppEpvRowWrapper row : rowWrapperList)
        {
            if (row.getTitle().equals(blockVariablePart.getTitle())) blockFound = true;
        }

        String preamble = "Минимальное количество зачетных единиц, отводимых на дисциплины по выбору - ";
        if (!blockFound)
            saveFefuEpvCheckResult(block, checkTime, preamble + "В УП(в) отсутствует блок, указанный в проверке");
        else
        {
            for (IEppEpvRowWrapper row : rowWrapperList)
            {
                if (row.getRow() instanceof EppEpvGroupImRow)
                {
                    IEppEpvRowWrapper parent = row.getHierarhyParent();

                    while (null != parent)
                    {
                        if (parent.getTitle().equals(blockVariablePart.getTitle()))
                        {
                            groupImRowList.add(row);
                            parent = null;
                        }
                        else parent = parent.getHierarhyParent();
                    }
                }
            }

            if (groupImRowList.isEmpty())
                saveFefuEpvCheckResult(block, checkTime, preamble + "В блоке «" + blockVariablePart.getTitle() + "» отсутствуют дисциплины по выбору");
            else
            {
                for (IEppEpvRowWrapper row : groupImRowList)
                {
                    double totalLabor = row.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                    if (minUnitsChoiceDisciplines > totalLabor)
                    {
                        checkMinUnitsSuccessful = false;
                        saveFefuEpvCheckResult(block, checkTime, preamble + "Дисциплина «" + row.getTitle() + "» не соответствует указанному минимуму в стандарте (" + minUnitsChoiceDisciplines + ")");
                    }
                }
                if (checkMinUnitsSuccessful) saveFefuEpvCheckResult(block, checkTime, preamble + "OK");
            }
        }
    }

    private void saveFefuEpvCheckResult(EppEduPlanVersionBlock block, Date checkTime, String message)
    {
        FefuEduPlanVersionCheckResults check = new FefuEduPlanVersionCheckResults(block, checkTime);
        check.setMessage(message);
        saveOrUpdate(check);
    }
}