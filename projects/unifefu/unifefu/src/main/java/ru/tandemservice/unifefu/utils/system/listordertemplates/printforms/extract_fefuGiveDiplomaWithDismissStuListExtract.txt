\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
О присвоении квалификации, выдаче документа государственного образца о {eduLevelTypeStr_P} образовании и отчислении в связи с окончанием университета\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_D}\cell\b0
{studentCategory_D} {course} курса {formativeOrgUnitStrWithTerritorial_G} {developForm_GF} формы обучения, {learned_D}{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStr}, на основании решения государственной экзаменационной комиссии присвоить {graduateWithLevel_A} по {fefuEducationStr_D}{diplomaQualificationAward}, выдать диплом {diplomaName_G} и отчислить в связи с окончанием университета.\par
\par
{basicsWord}{listBasics}{basicsPoint}\fi0\cell\row\pard