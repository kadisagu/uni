package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unifefu.entity.ws.FefuContractor;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Контрагент по договору ДВФУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuContractorGen extends PersonRole
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuContractor";
    public static final String ENTITY_NAME = "fefuContractor";
    public static final int VERSION_HASH = 1405023665;
    private static IEntityMeta ENTITY_META;

    public static final String P_CONTRACTOR_I_D = "contractorID";
    public static final String P_NAME_FULL = "nameFull";
    public static final String P_INN = "inn";
    public static final String P_KPP = "kpp";
    public static final String P_OGRN = "ogrn";
    public static final String P_OKPO = "okpo";

    private String _contractorID;     // Идентификатор контрагента НСИ
    private String _nameFull;     // Наименование контрагента
    private String _inn;     // ИНН
    private String _kpp;     // КПП
    private String _ogrn;     // ОГРН
    private String _okpo;     // ОКПО

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор контрагента НСИ.
     */
    @Length(max=255)
    public String getContractorID()
    {
        return _contractorID;
    }

    /**
     * @param contractorID Идентификатор контрагента НСИ.
     */
    public void setContractorID(String contractorID)
    {
        dirty(_contractorID, contractorID);
        _contractorID = contractorID;
    }

    /**
     * @return Наименование контрагента.
     */
    @Length(max=255)
    public String getNameFull()
    {
        return _nameFull;
    }

    /**
     * @param nameFull Наименование контрагента.
     */
    public void setNameFull(String nameFull)
    {
        dirty(_nameFull, nameFull);
        _nameFull = nameFull;
    }

    /**
     * @return ИНН.
     */
    @Length(max=255)
    public String getInn()
    {
        return _inn;
    }

    /**
     * @param inn ИНН.
     */
    public void setInn(String inn)
    {
        dirty(_inn, inn);
        _inn = inn;
    }

    /**
     * @return КПП.
     */
    @Length(max=255)
    public String getKpp()
    {
        return _kpp;
    }

    /**
     * @param kpp КПП.
     */
    public void setKpp(String kpp)
    {
        dirty(_kpp, kpp);
        _kpp = kpp;
    }

    /**
     * @return ОГРН.
     */
    @Length(max=255)
    public String getOgrn()
    {
        return _ogrn;
    }

    /**
     * @param ogrn ОГРН.
     */
    public void setOgrn(String ogrn)
    {
        dirty(_ogrn, ogrn);
        _ogrn = ogrn;
    }

    /**
     * @return ОКПО.
     */
    @Length(max=255)
    public String getOkpo()
    {
        return _okpo;
    }

    /**
     * @param okpo ОКПО.
     */
    public void setOkpo(String okpo)
    {
        dirty(_okpo, okpo);
        _okpo = okpo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuContractorGen)
        {
            setContractorID(((FefuContractor)another).getContractorID());
            setNameFull(((FefuContractor)another).getNameFull());
            setInn(((FefuContractor)another).getInn());
            setKpp(((FefuContractor)another).getKpp());
            setOgrn(((FefuContractor)another).getOgrn());
            setOkpo(((FefuContractor)another).getOkpo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuContractorGen> extends PersonRole.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuContractor.class;
        }

        public T newInstance()
        {
            return (T) new FefuContractor();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "contractorID":
                    return obj.getContractorID();
                case "nameFull":
                    return obj.getNameFull();
                case "inn":
                    return obj.getInn();
                case "kpp":
                    return obj.getKpp();
                case "ogrn":
                    return obj.getOgrn();
                case "okpo":
                    return obj.getOkpo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "contractorID":
                    obj.setContractorID((String) value);
                    return;
                case "nameFull":
                    obj.setNameFull((String) value);
                    return;
                case "inn":
                    obj.setInn((String) value);
                    return;
                case "kpp":
                    obj.setKpp((String) value);
                    return;
                case "ogrn":
                    obj.setOgrn((String) value);
                    return;
                case "okpo":
                    obj.setOkpo((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "contractorID":
                        return true;
                case "nameFull":
                        return true;
                case "inn":
                        return true;
                case "kpp":
                        return true;
                case "ogrn":
                        return true;
                case "okpo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "contractorID":
                    return true;
                case "nameFull":
                    return true;
                case "inn":
                    return true;
                case "kpp":
                    return true;
                case "ogrn":
                    return true;
                case "okpo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "contractorID":
                    return String.class;
                case "nameFull":
                    return String.class;
                case "inn":
                    return String.class;
                case "kpp":
                    return String.class;
                case "ogrn":
                    return String.class;
                case "okpo":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuContractor> _dslPath = new Path<FefuContractor>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuContractor");
    }
            

    /**
     * @return Идентификатор контрагента НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getContractorID()
     */
    public static PropertyPath<String> contractorID()
    {
        return _dslPath.contractorID();
    }

    /**
     * @return Наименование контрагента.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getNameFull()
     */
    public static PropertyPath<String> nameFull()
    {
        return _dslPath.nameFull();
    }

    /**
     * @return ИНН.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getInn()
     */
    public static PropertyPath<String> inn()
    {
        return _dslPath.inn();
    }

    /**
     * @return КПП.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getKpp()
     */
    public static PropertyPath<String> kpp()
    {
        return _dslPath.kpp();
    }

    /**
     * @return ОГРН.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getOgrn()
     */
    public static PropertyPath<String> ogrn()
    {
        return _dslPath.ogrn();
    }

    /**
     * @return ОКПО.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getOkpo()
     */
    public static PropertyPath<String> okpo()
    {
        return _dslPath.okpo();
    }

    public static class Path<E extends FefuContractor> extends PersonRole.Path<E>
    {
        private PropertyPath<String> _contractorID;
        private PropertyPath<String> _nameFull;
        private PropertyPath<String> _inn;
        private PropertyPath<String> _kpp;
        private PropertyPath<String> _ogrn;
        private PropertyPath<String> _okpo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор контрагента НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getContractorID()
     */
        public PropertyPath<String> contractorID()
        {
            if(_contractorID == null )
                _contractorID = new PropertyPath<String>(FefuContractorGen.P_CONTRACTOR_I_D, this);
            return _contractorID;
        }

    /**
     * @return Наименование контрагента.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getNameFull()
     */
        public PropertyPath<String> nameFull()
        {
            if(_nameFull == null )
                _nameFull = new PropertyPath<String>(FefuContractorGen.P_NAME_FULL, this);
            return _nameFull;
        }

    /**
     * @return ИНН.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getInn()
     */
        public PropertyPath<String> inn()
        {
            if(_inn == null )
                _inn = new PropertyPath<String>(FefuContractorGen.P_INN, this);
            return _inn;
        }

    /**
     * @return КПП.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getKpp()
     */
        public PropertyPath<String> kpp()
        {
            if(_kpp == null )
                _kpp = new PropertyPath<String>(FefuContractorGen.P_KPP, this);
            return _kpp;
        }

    /**
     * @return ОГРН.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getOgrn()
     */
        public PropertyPath<String> ogrn()
        {
            if(_ogrn == null )
                _ogrn = new PropertyPath<String>(FefuContractorGen.P_OGRN, this);
            return _ogrn;
        }

    /**
     * @return ОКПО.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractor#getOkpo()
     */
        public PropertyPath<String> okpo()
        {
            if(_okpo == null )
                _okpo = new PropertyPath<String>(FefuContractorGen.P_OKPO, this);
            return _okpo;
        }

        public Class getEntityClass()
        {
            return FefuContractor.class;
        }

        public String getEntityName()
        {
            return "fefuContractor";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
