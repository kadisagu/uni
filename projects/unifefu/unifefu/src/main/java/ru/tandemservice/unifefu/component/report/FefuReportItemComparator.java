/* $Id$ */
package ru.tandemservice.unifefu.component.report;

import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 29.07.2014
 */
public class FefuReportItemComparator implements Comparator<IReportItem>
{

    private Map<CompetitionKind, Integer> _competitionKindPriorities;
    private List<EntranceDiscipline> _discSortedList; //отсортированный по приоритету список дисциплин для выбранного направления

    /**
     * @param competitionKindPriorities приоритеты видов конкурса
     * @param disciplineList - сортированный список дисциплин по приоритетам, если он не задан - сортировка с учетом приоритета дисциплин не применяется
     */
    public FefuReportItemComparator(Map<CompetitionKind, Integer> competitionKindPriorities, List<EntranceDiscipline> disciplineList)
    {
        _competitionKindPriorities = competitionKindPriorities;
        _discSortedList = disciplineList;
    }

    @Override
    public int compare(IReportItem o1, IReportItem o2)
    {
        // получаем выбранные направления
        RequestedEnrollmentDirection r1 = o1.getRequestedEnrollmentDirection();
        RequestedEnrollmentDirection r2 = o2.getRequestedEnrollmentDirection();

        int result;

        CompetitionKind c1 = r1.getCompetitionKind();
        CompetitionKind c2 = r2.getCompetitionKind();

        // сравнить по приоритетам видов конкурса
        result = _competitionKindPriorities.get(c1) - _competitionKindPriorities.get(c2);
        // сначала сравниваем по общей сумме баллов

        if (result == 0)
            result = -UniBaseUtils.compare((o1.getFinalMark() + o1.getAchievementMark()), (o2.getFinalMark() + o2.getAchievementMark()), true);

        //сравниваем по сумме баллов без ИД
        if (result == 0) {
            result = -UniBaseUtils.compare(o1.getFinalMark(), o2.getFinalMark(), true);
        }

        //по баллам за дисциплину с наивысшим приоритетом
        if (_discSortedList != null) {
            if (result == 0) {
                for (EntranceDiscipline disc : _discSortedList)
                {
                    if (disc.getDiscipline() instanceof Discipline2RealizationWayRelation) {
                        Discipline2RealizationWayRelation key = (Discipline2RealizationWayRelation)disc.getDiscipline();
                        result = -UniBaseUtils.compare(o1.getMarkMap().get(key), o2.getMarkMap().get(key), true);
                        if (result != 0) break;
                    }
                }

            }
        } else
        {
            // теперь по баллам за профильное вступительное
            if (result == 0)
                result = -UniBaseUtils.compare(o1.getProfileMark(), o2.getProfileMark(), true);
        }

        if (result == 0)
            result = -UniBaseUtils.compare(o1.hasBenefit(), o2.hasBenefit(), true);

        // теперь по среднему баллу аттестата для тех, кто без ВИ
        if (result == 0 && UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(c1.getCode()))
            result = -UniBaseUtils.compare(o1.getCertificateAverageMark(), o2.getCertificateAverageMark(), true);

        // все осмысленное кончилось, теперь по фио
        if (result == 0)
            result = CommonCollator.RUSSIAN_COLLATOR.compare(o1.getFio(), o2.getFio());

        if (result == 0)
            result = Long.compare(r1.getId(), r2.getId());

        return result;
    }
}