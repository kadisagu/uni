/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.SignerList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuSettings.ui.SignerDocumentAddEdit.FefuSettingsSignerDocumentAddEdit;
import ru.tandemservice.unifefu.base.bo.FefuSettings.ui.SignerExtractAddEdit.FefuSettingsSignerExtractAddEdit;

/**
 * @author Alexey Lopatin
 * @since 27.12.2013
 */
public class FefuSettingsSignerListUI extends UIPresenter
{
    public static final String FILTER_DOCUMENT_TITLE = "documentTitle";
    public static final String FILTER_EXTRACT_TITLE = "extractTitle";

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(FefuSettingsSignerList.SETTING_SIGNER_DOCUMENT_TYPE_DS))
        {
            dataSource.put(FILTER_DOCUMENT_TITLE, getSettings().get(FILTER_DOCUMENT_TITLE));
        }
        else if (dataSource.getName().equals(FefuSettingsSignerList.SETTING_SIGNER_EXTRACT_DS))
        {
            dataSource.put(FILTER_EXTRACT_TITLE, getSettings().get(FILTER_EXTRACT_TITLE));
        }
    }

    public void onClickAddSignerDocumentType()
    {
        getActivationBuilder().asRegion(FefuSettingsSignerDocumentAddEdit.class).activate();
    }

    public void onClickAddSignerExtract()
    {
        getActivationBuilder().asRegion(FefuSettingsSignerExtractAddEdit.class).activate();
    }

    public void onEditSignerDocumentTypeFromList()
    {
        _uiActivation.asDesktopRoot(FefuSettingsSignerDocumentAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onEditSignerExtractFromList()
    {
        _uiActivation.asDesktopRoot(FefuSettingsSignerExtractAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickSearch()
    {
        getSettings().save();
    }

    public void onClickClearDocument()
    {
        getSettings().set(FILTER_DOCUMENT_TITLE, null);
        onClickSearch();
    }

    public void onClickClearExtract()
    {
        getSettings().set(FILTER_EXTRACT_TITLE, null);
        onClickSearch();
    }
}
