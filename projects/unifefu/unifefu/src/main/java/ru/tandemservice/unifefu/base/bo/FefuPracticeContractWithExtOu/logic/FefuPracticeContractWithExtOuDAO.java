/* $Id: FefuPracticeContractWithExtOuDAO.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.logic;

import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/28/13
 * Time: 6:04 PM
 */
public class FefuPracticeContractWithExtOuDAO extends BaseModifyAggregateDAO implements IFefuPracticeContractWithExtOuDAO
{
    @Override
    public void createOrUpdate(FefuPracticeContractWithExtOu practiceContract)
    {
        baseCreateOrUpdate(practiceContract);
    }
}
