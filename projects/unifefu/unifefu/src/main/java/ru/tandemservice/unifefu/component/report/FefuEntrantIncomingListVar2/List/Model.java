/* $Id$ */
package ru.tandemservice.unifefu.component.report.FefuEntrantIncomingListVar2.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report;

import java.util.List;

/**
 * @author Igor Belanov
 * @since 25.07.2016
 */
public class Model implements IEnrollmentCampaignSelectModel
{
    private List<EnrollmentCampaign> _enrollmentCampaignList;
    private IDataSettings _settings;
    private DynamicListDataSource<FefuEntrantIncomingListVar2Report> _dataSource;

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getSettings().get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource<FefuEntrantIncomingListVar2Report> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<FefuEntrantIncomingListVar2Report> dataSource)
    {
        _dataSource = dataSource;
    }
}
