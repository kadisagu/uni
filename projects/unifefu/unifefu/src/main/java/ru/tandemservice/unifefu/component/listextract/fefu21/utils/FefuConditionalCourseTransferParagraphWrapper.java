/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu21.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Belanov
 * @since 01.07.2016
 */
public class FefuConditionalCourseTransferParagraphWrapper implements Comparable<FefuConditionalCourseTransferParagraphWrapper>
{
    private final StudentCategory _studentCategory;
    private final CompensationType _compensationType;
    private final ListStudentExtract _firstExtract;
    private final List<FefuConditionalCourseTransferParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<>();

    public FefuConditionalCourseTransferParagraphWrapper(StudentCategory studentCategory, CompensationType compensationType, ListStudentExtract firstExtract)
    {
        _studentCategory = studentCategory;
        _compensationType = compensationType;
        _firstExtract = firstExtract;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public List<FefuConditionalCourseTransferParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuConditionalCourseTransferParagraphWrapper))
            return false;

        FefuConditionalCourseTransferParagraphWrapper that = (FefuConditionalCourseTransferParagraphWrapper) o;

        return _compensationType.equals(that.getCompensationType()) && _studentCategory.equals(that.getStudentCategory());
    }

    @Override
    public int hashCode()
    {
        return _compensationType.hashCode() & _studentCategory.hashCode();
    }

    @Override
    public int compareTo(FefuConditionalCourseTransferParagraphWrapper o)
    {
        if (StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(o.getStudentCategory().getCode()))
            return -1;
        else if (StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(o.getStudentCategory().getCode()))
            return 1;
        else
        {
            if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(o.getCompensationType().getCode()))
                return -1;
            else if (CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(o.getCompensationType().getCode()))
                return 1;
            else return 0;
        }

    }
}
