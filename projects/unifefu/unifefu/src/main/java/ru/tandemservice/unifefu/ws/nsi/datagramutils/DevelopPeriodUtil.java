/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.DevelopPeriodType;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 07.05.2014
 */
public class DevelopPeriodUtil extends SimpleNsiCatalogUtil<DevelopPeriodType, DevelopPeriod>
{
    public static final String DEVELOP_PERIOD_LAST_COURSE_NSI_FIELD = "DevelopPeriodLastCourse";
    public static final String DEVELOP_PERIOD_PRIORITY_NSI_FIELD = "DevelopPeriodPriority";

    @Override
    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            super.getNsiFields();
            NSI_FIELDS.add(DEVELOP_PERIOD_LAST_COURSE_NSI_FIELD);
            NSI_FIELDS.add(DEVELOP_PERIOD_PRIORITY_NSI_FIELD);
        }
        return NSI_FIELDS;
    }

    @Override
    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            super.getEntityFields();
            ENTITY_FIELDS.add(DevelopPeriod.lastCourse().s());
            ENTITY_FIELDS.add(DevelopPeriod.priority().s());
        }
        return ENTITY_FIELDS;
    }

    @Override
    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            super.getNsiToObFieldsMap();
            NSI_TO_OB_FIELDS_MAP.put(DEVELOP_PERIOD_LAST_COURSE_NSI_FIELD, DevelopPeriod.lastCourse().s());
            NSI_TO_OB_FIELDS_MAP.put(DEVELOP_PERIOD_PRIORITY_NSI_FIELD, DevelopPeriod.priority().s());
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    @Override
    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            super.getObToNsiFieldsMap();
            OB_TO_NSI_FIELDS_MAP.put(DevelopPeriod.lastCourse().s(), DEVELOP_PERIOD_LAST_COURSE_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(DevelopPeriod.priority().s(), DEVELOP_PERIOD_PRIORITY_NSI_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    public long getNsiDevelopPeriodLastCourseHash(DevelopPeriodType nsiEntity)
    {
        if (null == nsiEntity.getDevelopPeriodLastCourse()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getDevelopPeriodLastCourse());
    }

    @Override
    public boolean isFieldEmpty(DevelopPeriodType nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case DEVELOP_PERIOD_LAST_COURSE_NSI_FIELD:
                return null == nsiEntity.getDevelopPeriodLastCourse();
            case DEVELOP_PERIOD_PRIORITY_NSI_FIELD:
                return false;
            default:
                return super.isFieldEmpty(nsiEntity, fieldName);
        }
    }

    @Override
    public long getNsiFieldHash(DevelopPeriodType nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case DEVELOP_PERIOD_LAST_COURSE_NSI_FIELD:
                return getNsiDevelopPeriodLastCourseHash(nsiEntity);
            case DEVELOP_PERIOD_PRIORITY_NSI_FIELD:
                return 0;
            default:
                return super.getNsiFieldHash(nsiEntity, fieldName, caseInsensitive);
        }
    }

    public long getDevelopPeriodLastCourseHash(DevelopPeriod entity)
    {
        return MD5HashBuilder.getCheckSum(String.valueOf(entity.getLastCourse()));
    }

    @Override
    public long getEntityFieldHash(DevelopPeriod entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case DevelopPeriod.P_LAST_COURSE:
                return getDevelopPeriodLastCourseHash(entity);
            case DevelopPeriod.P_PRIORITY:
                return 0;
            default:
                return super.getEntityFieldHash(entity, nsiId, fieldName, caseInsensitive);
        }
    }
}