/* $Id$ */
package ru.tandemservice.unifefu.base.ext.DiplomaIssuance.ui.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.IDipDocumentDao;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unifefu.entity.DiplomaIssuanceFefuExt;

import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 26.09.2014
 */
public interface IDipDocumentExtDao extends IDipDocumentDao
{
    /**
     * Метод сохраняет в БД список переданных фактов выдачи диплома.
     *
     * @param diplomaIssuance     Факт выдачи документа об обучении
     * @param diplomaIssuanceFefuExt     Факт выдачи документа об обучении (расширение ДВФУ)
     * @param issuanceContentList   Приложения к факту выдачи
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void saveDiplomaIssuanceExtension(DiplomaIssuance diplomaIssuance, DiplomaIssuanceFefuExt diplomaIssuanceFefuExt,
                                             List<DiplomaContentIssuance> issuanceContentList, Map<Long, DipDiplomaBlank> oldBlankMap);
}
