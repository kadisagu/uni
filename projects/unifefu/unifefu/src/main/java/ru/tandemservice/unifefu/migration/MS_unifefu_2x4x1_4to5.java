package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefu_1C_pack

        // создана новая сущность
        if (!tool.tableExists("fefu_1c_pack_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("fefu_1c_pack_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("dispatchdate_p", DBType.TIMESTAMP),
                                      new DBColumn("request_p", DBType.BLOB),
                                      new DBColumn("response_p", DBType.createVarchar(255)),
                                      new DBColumn("success_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefu_1C_pack");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefu_1C_log

        if (tool.tableExists("fefu_1c_log_t"))
        {
            tool.getStatement().executeUpdate("delete from fefu_1c_log_t");

            if (tool.columnExists("fefu_1c_log_t", "dispatchdate_p"))
            {
                tool.dropColumn("fefu_1c_log_t", "dispatchdate_p");
                tool.dropColumn("fefu_1c_log_t", "request_p");
                tool.dropColumn("fefu_1c_log_t", "response_p");
                tool.dropColumn("fefu_1c_log_t", "success_p");
            }

            if (!tool.columnExists("fefu_1c_log_t", "pack_id"))
            {
                tool.createColumn("fefu_1c_log_t", new DBColumn("pack_id", DBType.LONG));
            }

            if (!tool.columnExists("fefu_1c_log_t", "format_p"))
            {
                tool.createColumn("fefu_1c_log_t", new DBColumn("format_p", DBType.INTEGER));
                tool.setColumnNullable("fefu_1c_log_t", "format_p", false);
            }
        }
    }
}