package ru.tandemservice.unifefu.base.bo.FefuTrHomePage.ui.JournalGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.unifefu.base.bo.FefuTrHomePage.FefuTrHomePageManager;
import ru.tandemservice.unifefu.base.bo.FefuTrHomePage.logic.FefuGroupsDSHandler;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author amakarova
 */
@Configuration
public class FefuTrHomePageJournalGroup extends BusinessComponentManager
{
    public static final String TR_HOME_JOURNAL_DS = "trHomeJournalDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(TR_HOME_JOURNAL_DS, trHomeJournalDSColumns(), trJournalDSHandler()))
                .addDataSource(FefuTrHomePageManager.instance().trOrgUnitDSConfig())
                .addDataSource(FefuTrHomePageManager.instance().trOrgUnitYearPartDSConfig())
                .addDataSource(EppStateManager.instance().eppStateDSConfig())
                .create();
    }

    @Bean
    public ColumnListExtPoint trHomeJournalDSColumns() {
        return this.columnListExtPointBuilder(TR_HOME_JOURNAL_DS)
                .addColumn(textColumn(TrJournal.P_NUMBER, TrJournalGroup.journal().number()).order().width("1").create())
                .addColumn(actionColumn(TrJournal.L_REGISTRY_ELEMENT_PART, TrJournalGroup.group().fullTitle(), "onClickShowJournal").order().create())  // дисциплина
                .addColumn(textColumn(FefuGroupsDSHandler.COLUMN_YEAR_PART, TrJournalGroup.journal().yearPart().title()).create())  // часть учебного года
                .addColumn(textColumn(FefuGroupsDSHandler.COLUMN_UGS,  TrJournalGroup.group().title()).create()) // Группа
                .addColumn(textColumn("fca", FefuGroupsDSHandler.COLUMN_FCA).formatter(UniEppUtils.NEW_LINE_FORMATTER).create())    // Форма контроля
                .addColumn(textColumn(FefuGroupsDSHandler.COLUMN_STATE,  TrJournalGroup.journal().state().title()).create()) // Состояние
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).create())                                                 //
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trJournalDSHandler() {
        return new FefuGroupsDSHandler(getName());
    }
}
