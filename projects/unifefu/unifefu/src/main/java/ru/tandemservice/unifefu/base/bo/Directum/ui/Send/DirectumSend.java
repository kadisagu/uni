/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.Send;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Dmitry Seleznev
 * @since 15.07.2013
 */
@Configuration
public class DirectumSend extends BusinessComponentManager
{
    public static final String ORDER_TYPE_DS = "orderTypeDS";
    public final static String SIGNER_DS = "signerDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ORDER_TYPE_DS, orderTypeDSHandler()))
                .addDataSource(selectDS(SIGNER_DS, signerComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler orderTypeDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler signerComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).filtered(true);
    }
}