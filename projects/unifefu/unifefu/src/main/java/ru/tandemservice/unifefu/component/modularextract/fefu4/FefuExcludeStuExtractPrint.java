/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu4;

import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unifefu.entity.FefuExcludeStuExtract;

import java.util.Collections;

/**
 * @author Dmitry Seleznev
 * @since 27.08.2012
 */
public class FefuExcludeStuExtractPrint implements IPrintFormCreator<FefuExcludeStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuExcludeStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        if(extract.isTwoDiplomaInternationalProgram()) modifier.put("internationalDiploma", " по международной двухдипломной программе");
        else modifier.put("internationalDiploma", "");

        if (CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(extract.getEntity().getCompensationType().getCode()))
        {
            modifier.put("contractTerminationStr", "Договор об оказании платных образовательных услуг считать расторгнутым на основании п.3 ст.450 ГК РФ и условий договора.");
        }
        else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("contractTerminationStr"), true, false);
        }

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}