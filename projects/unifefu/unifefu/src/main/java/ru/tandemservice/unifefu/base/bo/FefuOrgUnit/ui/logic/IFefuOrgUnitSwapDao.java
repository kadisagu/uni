/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author Dmitry Seleznev
 * @since 28.03.2014
 */
public interface IFefuOrgUnitSwapDao extends INeedPersistenceSupport
{
    /**
     * Пересаживает сотрудников подразделения-донора (orgUnitToSwapId) на подразделение-акцептор (orgUnitId).
     * Так же для подразделения-донора устанавливается guid равный guid'у подразделения-донора, а для подразделения-донора в guid'е добавляется символ "_" в конце.
     *
     * @param orgUnitId       - Идентификатор подразделения-акцептора
     * @param orgUnitToSwapId - Идентификатор подразделения-донора
     */
    void doSwapOrgUnits(Long orgUnitId, Long orgUnitToSwapId);
}