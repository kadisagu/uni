/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu15.Pub;

import org.tandemframework.core.view.list.column.FormatterColumn;
import org.tandemframework.tapsupport.translator.FormatterBasedTranslator;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract;

import java.util.List;

/**
 * @author nvankov
 * @since 11/15/13
 */
public class Model extends ModularStudentExtractPubModel<FullStateMaintenanceEnrollmentStuExtract>
{
    private String _payments;

    public String getPayments()
    {
        return _payments;
    }

    public void setPayments(String payments)
    {
        _payments = payments;
    }
}
