/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu23.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 22.01.2015
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuTransfStuDPOExtract, IDAO, Model>
{
}