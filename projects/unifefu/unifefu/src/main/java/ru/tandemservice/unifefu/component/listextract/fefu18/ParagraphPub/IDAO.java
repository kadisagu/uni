/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuOrderContingentStuDPOListExtract, Model>
{
}