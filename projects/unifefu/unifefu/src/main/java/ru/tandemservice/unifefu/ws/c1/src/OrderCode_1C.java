/* $Id$ */
package ru.tandemservice.unifefu.ws.c1.src;

/**
 * @author Nikolay Fedorovskih
 * @since 16.12.2013
 */
public class OrderCode_1C
{
    private OrderType_1C _type;
    //private String _orderCode;
    private String _paymentCode;

    public OrderCode_1C(OrderType_1C type, String paymentCode)
    {
        _type = type;
        //_orderCode = orderCode;
        _paymentCode = paymentCode;
    }

    public OrderType_1C getType()
    {
        return _type;
    }

    /*public String getOrderCode()
    {
        return _orderCode;
    }*/

    public String getPaymentCode()
    {
        return _paymentCode;
    }
}