package ru.tandemservice.unifefu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Виды приказов в Directum"
 * Имя сущности : fefuDirectumOrderType
 * Файл data.xml : unifefu-catalog.data.xml
 */
public interface FefuDirectumOrderTypeCodes
{
    /** Константа кода (code) элемента : Учебная деятельность (ВПО) (title) */
    String VPO = "vpo";
    /** Константа кода (code) элемента : Учебная деятельность (CПО) (title) */
    String SPO = "spo";
    /** Константа кода (code) элемента : Учебная деятельность (ДПО) (title) */
    String DPO = "dpo";
    /** Константа кода (code) элемента : Приемная кампания (title) */
    String ENR = "enr";

    Set<String> CODES = ImmutableSet.of(VPO, SPO, DPO, ENR);
}
