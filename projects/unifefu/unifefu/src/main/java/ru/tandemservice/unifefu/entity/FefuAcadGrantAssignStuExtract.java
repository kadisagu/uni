/* $Id$ */
package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAssignPaymentExtract;
import ru.tandemservice.movestudent.component.commons.gradation.IGroupManagerPaymentExtract;
import ru.tandemservice.unifefu.entity.gen.FefuAcadGrantAssignStuExtractGen;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О назначении академической стипендии
 */
public class FefuAcadGrantAssignStuExtract extends FefuAcadGrantAssignStuExtractGen implements IAssignPaymentExtract, IGroupManagerPaymentExtract
{
    @Override
    public boolean hasGroupManagerBonus()
    {
        return getGroupManagerBonusSize() != null;
    }

    @Override
    public BigDecimal getPaymentAmount()
    {
        return new BigDecimal(getGrantSize());
    }

    @Override
    public BigDecimal getGroupManagerPaymentAmount()
    {
        return isGroupManagerBonus() && getGroupManagerBonusSize() != null ? new BigDecimal(getGroupManagerBonusSize()) : null;
    }

    @Override
    public Date getPaymentBeginDate()
    {
        return getBeginDate();
    }

    @Override
    public Date getPaymentEndDate()
    {
        return getEndDate();
    }
}