/* $Id$ */
package ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.OuSettingsPub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuSettingsPub.TrBrsCoefficientOuSettingsPub;

/**
 * @author Nikolay Fedorovskih
 * @since 14.07.2014
 */
@Configuration
public class TrBrsCoefficientOuSettingsPubExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrBrsCoefficientOuSettingsPubExtUI.class.getSimpleName();

    @Autowired
    private TrBrsCoefficientOuSettingsPub _trBrsCoefficientOuSettingsPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trBrsCoefficientOuSettingsPub.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrBrsCoefficientOuSettingsPubExtUI.class))
                .create();
    }
}