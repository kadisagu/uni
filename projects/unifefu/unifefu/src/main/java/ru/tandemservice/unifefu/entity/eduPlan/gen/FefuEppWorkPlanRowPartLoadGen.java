package ru.tandemservice.unifefu.entity.eduPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.unifefu.entity.catalog.FefuLoadType;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Нагрузка строки РУП в части (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEppWorkPlanRowPartLoadGen extends EntityBase
 implements INaturalIdentifiable<FefuEppWorkPlanRowPartLoadGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad";
    public static final String ENTITY_NAME = "fefuEppWorkPlanRowPartLoad";
    public static final int VERSION_HASH = 8082851;
    private static IEntityMeta ENTITY_META;

    public static final String P_PART = "part";
    public static final String L_ROW = "row";
    public static final String L_LOAD_TYPE = "loadType";
    public static final String P_LOAD = "load";
    public static final String P_LOAD_AS_DOUBLE = "loadAsDouble";

    private int _part;     // Часть
    private EppWorkPlanRegistryElementRow _row;     // Строка РУП
    private FefuLoadType _loadType;     // Вид нагрузки
    private long _load;     // Число часов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Часть. Свойство не может быть null.
     */
    @NotNull
    public int getPart()
    {
        return _part;
    }

    /**
     * @param part Часть. Свойство не может быть null.
     */
    public void setPart(int part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return Строка РУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkPlanRegistryElementRow getRow()
    {
        return _row;
    }

    /**
     * @param row Строка РУП. Свойство не может быть null.
     */
    public void setRow(EppWorkPlanRegistryElementRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Вид нагрузки. Свойство не может быть null.
     */
    @NotNull
    public FefuLoadType getLoadType()
    {
        return _loadType;
    }

    /**
     * @param loadType Вид нагрузки. Свойство не может быть null.
     */
    public void setLoadType(FefuLoadType loadType)
    {
        dirty(_loadType, loadType);
        _loadType = loadType;
    }

    /**
     * @return Число часов. Свойство не может быть null.
     */
    @NotNull
    public long getLoad()
    {
        return _load;
    }

    /**
     * @param load Число часов. Свойство не может быть null.
     */
    public void setLoad(long load)
    {
        dirty(_load, load);
        _load = load;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEppWorkPlanRowPartLoadGen)
        {
            if (withNaturalIdProperties)
            {
                setPart(((FefuEppWorkPlanRowPartLoad)another).getPart());
                setRow(((FefuEppWorkPlanRowPartLoad)another).getRow());
                setLoadType(((FefuEppWorkPlanRowPartLoad)another).getLoadType());
            }
            setLoad(((FefuEppWorkPlanRowPartLoad)another).getLoad());
        }
    }

    public INaturalId<FefuEppWorkPlanRowPartLoadGen> getNaturalId()
    {
        return new NaturalId(getPart(), getRow(), getLoadType());
    }

    public static class NaturalId extends NaturalIdBase<FefuEppWorkPlanRowPartLoadGen>
    {
        private static final String PROXY_NAME = "FefuEppWorkPlanRowPartLoadNaturalProxy";

        private int _part;
        private Long _row;
        private Long _loadType;

        public NaturalId()
        {}

        public NaturalId(int part, EppWorkPlanRegistryElementRow row, FefuLoadType loadType)
        {
            _part = part;
            _row = ((IEntity) row).getId();
            _loadType = ((IEntity) loadType).getId();
        }

        public int getPart()
        {
            return _part;
        }

        public void setPart(int part)
        {
            _part = part;
        }

        public Long getRow()
        {
            return _row;
        }

        public void setRow(Long row)
        {
            _row = row;
        }

        public Long getLoadType()
        {
            return _loadType;
        }

        public void setLoadType(Long loadType)
        {
            _loadType = loadType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuEppWorkPlanRowPartLoadGen.NaturalId) ) return false;

            FefuEppWorkPlanRowPartLoadGen.NaturalId that = (NaturalId) o;

            if( !equals(getPart(), that.getPart()) ) return false;
            if( !equals(getRow(), that.getRow()) ) return false;
            if( !equals(getLoadType(), that.getLoadType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPart());
            result = hashCode(result, getRow());
            result = hashCode(result, getLoadType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPart());
            sb.append("/");
            sb.append(getRow());
            sb.append("/");
            sb.append(getLoadType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEppWorkPlanRowPartLoadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEppWorkPlanRowPartLoad.class;
        }

        public T newInstance()
        {
            return (T) new FefuEppWorkPlanRowPartLoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "part":
                    return obj.getPart();
                case "row":
                    return obj.getRow();
                case "loadType":
                    return obj.getLoadType();
                case "load":
                    return obj.getLoad();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "part":
                    obj.setPart((Integer) value);
                    return;
                case "row":
                    obj.setRow((EppWorkPlanRegistryElementRow) value);
                    return;
                case "loadType":
                    obj.setLoadType((FefuLoadType) value);
                    return;
                case "load":
                    obj.setLoad((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "part":
                        return true;
                case "row":
                        return true;
                case "loadType":
                        return true;
                case "load":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "part":
                    return true;
                case "row":
                    return true;
                case "loadType":
                    return true;
                case "load":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "part":
                    return Integer.class;
                case "row":
                    return EppWorkPlanRegistryElementRow.class;
                case "loadType":
                    return FefuLoadType.class;
                case "load":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEppWorkPlanRowPartLoad> _dslPath = new Path<FefuEppWorkPlanRowPartLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEppWorkPlanRowPartLoad");
    }
            

    /**
     * @return Часть. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad#getPart()
     */
    public static PropertyPath<Integer> part()
    {
        return _dslPath.part();
    }

    /**
     * @return Строка РУП. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad#getRow()
     */
    public static EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad#getLoadType()
     */
    public static FefuLoadType.Path<FefuLoadType> loadType()
    {
        return _dslPath.loadType();
    }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad#getLoad()
     */
    public static PropertyPath<Long> load()
    {
        return _dslPath.load();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad#getLoadAsDouble()
     */
    public static SupportedPropertyPath<Double> loadAsDouble()
    {
        return _dslPath.loadAsDouble();
    }

    public static class Path<E extends FefuEppWorkPlanRowPartLoad> extends EntityPath<E>
    {
        private PropertyPath<Integer> _part;
        private EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> _row;
        private FefuLoadType.Path<FefuLoadType> _loadType;
        private PropertyPath<Long> _load;
        private SupportedPropertyPath<Double> _loadAsDouble;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Часть. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad#getPart()
     */
        public PropertyPath<Integer> part()
        {
            if(_part == null )
                _part = new PropertyPath<Integer>(FefuEppWorkPlanRowPartLoadGen.P_PART, this);
            return _part;
        }

    /**
     * @return Строка РУП. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad#getRow()
     */
        public EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> row()
        {
            if(_row == null )
                _row = new EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad#getLoadType()
     */
        public FefuLoadType.Path<FefuLoadType> loadType()
        {
            if(_loadType == null )
                _loadType = new FefuLoadType.Path<FefuLoadType>(L_LOAD_TYPE, this);
            return _loadType;
        }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad#getLoad()
     */
        public PropertyPath<Long> load()
        {
            if(_load == null )
                _load = new PropertyPath<Long>(FefuEppWorkPlanRowPartLoadGen.P_LOAD, this);
            return _load;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad#getLoadAsDouble()
     */
        public SupportedPropertyPath<Double> loadAsDouble()
        {
            if(_loadAsDouble == null )
                _loadAsDouble = new SupportedPropertyPath<Double>(FefuEppWorkPlanRowPartLoadGen.P_LOAD_AS_DOUBLE, this);
            return _loadAsDouble;
        }

        public Class getEntityClass()
        {
            return FefuEppWorkPlanRowPartLoad.class;
        }

        public String getEntityName()
        {
            return "fefuEppWorkPlanRowPartLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getLoadAsDouble();
}
