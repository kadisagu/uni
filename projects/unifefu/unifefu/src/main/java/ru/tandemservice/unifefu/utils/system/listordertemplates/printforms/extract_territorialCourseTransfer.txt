\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
О переводе с курса на следующий курс, из филиала в школу\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_A}\cell\b0
{studentCategory_A} {courseOld} курса{intoFefuGroupOld}, {learned_A}{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStr} по {fefuEducationStrOld_D} {orgUnitPrepOld} {formativeOrgUnitStrWithTerritorialOld_P}, перевести на {courseNew} курс{intoFefuGroupNew} для продолжения обучения по {fefuEducationStrNewOrSame_D} {orgUnitPrepNew} {formativeOrgUnitStrWithTerritorialNew_P} по {developFormNew_DF} форме обучения.\par
\par
{basicsWord}{listBasics}{basicsPoint}\fi0\cell\row\pard
