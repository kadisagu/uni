/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.listextract.e39;

import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.movestudent.entity.AcadGrantBonusAssignStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 21.01.2013
 */
public class AcadGrantBonusAssignStuListExtractDao extends ru.tandemservice.movestudent.component.listextract.e39.AcadGrantBonusAssignStuListExtractDao
{
    @Override
    public void doCommit(AcadGrantBonusAssignStuListExtract extract, Map parameters)
    {
        super.doCommit(extract, parameters);

        //выплата
        StudentListOrder listOrder = (StudentListOrder) extract.getParagraph().getOrder();

        FefuStudentPayment payment = new FefuStudentPayment();
        payment.setExtract(extract);
        payment.setStartDate(extract.getBeginDate());
        payment.setStopDate(extract.getEndDate());
        payment.setPaymentSum(extract.getGrantBonusSize());
        if(null != listOrder.getReason())
            payment.setReason(listOrder.getReason().getTitle());
        save(payment);
    }

    @Override
    public void doRollback(AcadGrantBonusAssignStuListExtract extract, Map parameters)
    {
        super.doRollback(extract, parameters);

        // Удаляем выплату
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuStudentPayment.class);
        deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(FefuStudentPayment.extract()), DQLExpressions.value(extract)));
        deleteBuilder.createStatement(getSession()).execute();
    }
}
