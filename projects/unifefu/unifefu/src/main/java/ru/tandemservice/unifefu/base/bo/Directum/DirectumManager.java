/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.Directum.logic.DirectumDAO;
import ru.tandemservice.unifefu.base.bo.Directum.logic.IDirectumDAO;

/**
 * @author Dmitry Seleznev
 * @since 15.07.2013
 */
@Configuration
public class DirectumManager extends BusinessObjectManager
{
    public static DirectumManager instance()
    {
        return instance(DirectumManager.class);
    }

    @Bean
    public IDirectumDAO dao()
    {
        return new DirectumDAO();
    }
}