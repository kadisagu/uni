package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.ui.Pub;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.AdditionalProfessionalEducationProgramForStudentManager;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.ui.AddEdit.AdditionalProfessionalEducationProgramForStudentAddEdit;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.ui.EditParameters.AdditionalProfessionalEducationProgramForStudentEditParameters;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic.Constants.STUDENT_ID;

@State({
               @Bind(key = "publisherId", binding = "studentId")
       })
public class AdditionalProfessionalEducationProgramForStudentPubUI extends UIPresenter
{
    private Long studentId;
    private Student student;
    private FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters programIndividual;
    private FefuAdditionalProfessionalEducationProgramForStudent programForStudent;
    private String studentCustomStates;


    public void setStudentCustomStates(String studentCustomStates)
    {
        this.studentCustomStates = studentCustomStates;
    }

    public FefuAdditionalProfessionalEducationProgramForStudent getProgramForStudent()
    {
        return programForStudent;
    }

    public void setProgramForStudent(FefuAdditionalProfessionalEducationProgramForStudent programForStudent)
    {
        this.programForStudent = programForStudent;
    }

    public FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters getProgramIndividual()
    {
        return programIndividual;
    }

    public void setProgramIndividual(FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters programIndividual)
    {
        this.programIndividual = programIndividual;
    }

    public Long getStudentId()
    {
        return studentId;
    }

    public void setStudentId(Long studentId)
    {
        this.studentId = studentId;
    }


    public Student getStudent()
    {
        return student;
    }

    public void setStudent(Student student)
    {
        this.student = student;
    }

    @Override
    public void onComponentRefresh()
    {
        if (studentId != null)
        {
            student = DataAccessServices.dao().get(Student.class, studentId);
            programForStudent = AdditionalProfessionalEducationProgramForStudentManager.instance().apeProgramForStudentDao().getApeProgramForStudentByStudentId(studentId);
            if (programForStudent != null)
            {
                programIndividual = programForStudent.getIndividualParameters();
            }
            studentCustomStates = getStudentCustomStates();
        }
    }

    public String getStudentCustomStates()
    {
        String states = "";
        List<StudentCustomState> studentCustomStateList = DataAccessServices.dao().getList(StudentCustomState.class, StudentCustomState.student(), student);
        for (StudentCustomState studentCustomState : studentCustomStateList)
        {
            states = states + studentCustomState.getCustomState().getTitle() + " ";
        }
        return states;
    }


    public void onClickEditAdditionalProfessionalEducationProgramForStudent()
    {

        _uiActivation.asRegionDialog(AdditionalProfessionalEducationProgramForStudentAddEdit.class)
                .parameter(STUDENT_ID, studentId)
                .activate();

    }

    public void onClickEditAdditionalProfessionalEducationProgramForStudentIndividualParameters()
    {
        _uiActivation.asRegionDialog(AdditionalProfessionalEducationProgramForStudentEditParameters.class)
                .parameter(STUDENT_ID, studentId)
                .activate();


    }

    public boolean getProgramForStudentExists()
    {
        return this.programForStudent == null;
    }

}
