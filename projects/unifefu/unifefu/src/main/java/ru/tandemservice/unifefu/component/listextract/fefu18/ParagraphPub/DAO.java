/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18.ParagraphPub;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOListExtract;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public class DAO extends AbstractListParagraphPubDAO<FefuOrderContingentStuDPOListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        String printFormName = getProperty(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.P_FILE_NAME, FefuOrderToPrintFormRelation.L_ORDER, model.getParagraph().getOrder());
        model.setPrintFormFileName(printFormName);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        super.prepareListDataSource(model);
        for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            viewWrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, true);
        }

    }
}