/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu8.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 23.10.2013
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuHolidayStuListExtract, Model>
{
}
