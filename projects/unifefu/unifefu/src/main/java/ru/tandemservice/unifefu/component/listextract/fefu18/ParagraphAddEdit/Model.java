/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18.ParagraphAddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOListExtract;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public class Model extends AbstractListParagraphAddEditModel<FefuOrderContingentStuDPOListExtract>
{
    private ISelectModel _dpoProgramModel;
    private IUploadFile _uploadFileOrder;
    private StudentStatus _studentStatusNew;
    private FefuAdditionalProfessionalEducationProgram _dpoProgram;
    private Course _course;
    private StudentCustomStateCI _studentCustomStateNew;
    private ISelectModel _studentStatusModel;
    private ISelectModel _newStudentCustomStateCIModel;
    private boolean _canAddTemplate;

    public boolean isCanAddTemplate()
    {
        return _canAddTemplate;
    }

    public void setCanAddTemplate(boolean canAddTemplate)
    {
        _canAddTemplate = canAddTemplate;
    }

    public ISelectModel getNewStudentCustomStateCIModel()
    {
        return _newStudentCustomStateCIModel;
    }

    public void setNewStudentCustomStateCIModel(ISelectModel newStudentCustomStateCIModel)
    {
        _newStudentCustomStateCIModel = newStudentCustomStateCIModel;
    }

    public ISelectModel getDpoProgramModel()
    {
        return _dpoProgramModel;
    }

    public void setDpoProgramModel(ISelectModel dpoProgramModel)
    {
        _dpoProgramModel = dpoProgramModel;
    }

    public IUploadFile getUploadFileOrder()
    {
        return _uploadFileOrder;
    }

    public void setUploadFileOrder(IUploadFile uploadFileOrder)
    {
        _uploadFileOrder = uploadFileOrder;
    }

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }


    public FefuAdditionalProfessionalEducationProgram getDpoProgram()
    {
        return _dpoProgram;
    }

    public void setDpoProgram(FefuAdditionalProfessionalEducationProgram dpoProgram)
    {
        _dpoProgram = dpoProgram;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public StudentCustomStateCI getStudentCustomStateNew()
    {
        return _studentCustomStateNew;
    }

    public void setStudentCustomStateNew(StudentCustomStateCI studentCustomStateNew)
    {
        _studentCustomStateNew = studentCustomStateNew;
    }

    public ISelectModel getStudentStatusModel()
    {
        return _studentStatusModel;
    }

    public void setStudentStatusModel(ISelectModel studentStatusModel)
    {
        _studentStatusModel = studentStatusModel;
    }
}