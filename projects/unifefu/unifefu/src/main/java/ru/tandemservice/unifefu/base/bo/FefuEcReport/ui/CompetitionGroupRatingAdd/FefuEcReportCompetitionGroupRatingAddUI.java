/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.CompetitionGroupRatingAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.ITitledEntity;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportParam;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportParam;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.FefuEcReportManager;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.FefuEntrantCGRatingReportBuilder;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.IFefuEcReportBuilder;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.IFefuEcReportDAO;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.CompetitionGroupRatingPub.FefuEcReportCompetitionGroupRatingPub;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport;

import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 10.07.2013
 */
public class FefuEcReportCompetitionGroupRatingAddUI extends UIPresenter
{
    private FefuEntrantRatingReport report = new FefuEntrantRatingReport();
    private IReportParam<CompetitionGroup> competitionGroup = new ReportParam<>();
    private IReportParam<OrgUnit> formativeOrgUnit = new ReportParam<>();
    private IReportParam<OrgUnit> territorialOrgUnit = new ReportParam<>();
    private IReportParam<List<Qualifications>> qualification = new ReportParam<>();
    private ITitledEntity isIncludeForeignPerson = FefuEcReportCompetitionGroupRatingAdd.BOOLEAN_NO;

    @Override
    public void onComponentRefresh()
    {
        EnrollmentCampaign ec = UnifefuDaoFacade.getFefuEntrantDAO().getLastEnrollmentCampaign();
        report.setEnrollmentCampaign(ec);
        report.setDateFrom(ec != null ? ec.getStartDate() : CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())));
        report.setDateTo(new Date());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(FefuEcReportCompetitionGroupRatingAdd.ENROLLMENT_CAMPAIGN_PARAM, report.getEnrollmentCampaign());
        if (FefuEcReportCompetitionGroupRatingAdd.COMPETITION_GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put(FefuEcReportCompetitionGroupRatingAdd.QUALIFICATIONS_PARAM, qualification.getDataIfActive());
            dataSource.put(FefuEcReportCompetitionGroupRatingAdd.FORMATIVE_ORGUNIT_PARAM, formativeOrgUnit.getDataIfActive());
            dataSource.put(FefuEcReportCompetitionGroupRatingAdd.TERRITORIAL_ORGUNIT_PARAM, territorialOrgUnit.getDataIfActive());
        }
    }

    public FefuEntrantRatingReport getReport()
    {
        return report;
    }

    public void onClickApply()
    {
        final IFefuEcReportDAO dao = FefuEcReportManager.instance().dao();

        report.setFormingDate(new Date());
        report.setCompetitionGroupStr(competitionGroup.isActive() ? competitionGroup.getData().getTitle() : null);
        report.setFormativeOrgUnitStr(formativeOrgUnit.isActive() ? formativeOrgUnit.getData().getTitle() : null);
        report.setTerritorialOrgUnitStr(territorialOrgUnit.isActive() ? territorialOrgUnit.getData().getTitle() : null);
        report.setQualificationStr(qualification.isActive() ? CommonBaseStringUtil.join(qualification.getData(), Qualifications.P_TITLE, "; ") : null);
        report.setIncludeForeignPerson(isIncludeForeignPerson.getId().equals(FefuEcReportCompetitionGroupRatingAdd.BOOLEAN_YES.getId()));

        IFefuEcReportBuilder reportBuilder =
                new FefuEntrantCGRatingReportBuilder(
                        report,
                        competitionGroup.getDataIfActive(),
                        qualification.getDataIfActive(),
                        formativeOrgUnit.getDataIfActive(),
                        territorialOrgUnit.getDataIfActive()
                );

        dao.createReport(reportBuilder, report);

        deactivate();

        _uiActivation.asDesktopRoot(FefuEcReportCompetitionGroupRatingPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }

    public IReportParam<List<Qualifications>> getQualification()
    {
        return qualification;
    }

    public IReportParam<CompetitionGroup> getCompetitionGroup()
    {
        return competitionGroup;
    }

    public IReportParam<OrgUnit> getFormativeOrgUnit()
    {
        return formativeOrgUnit;
    }

    public IReportParam<OrgUnit> getTerritorialOrgUnit()
    {
        return territorialOrgUnit;
    }

    public ITitledEntity getIncludeForeignPerson()
    {
        return isIncludeForeignPerson;
    }

    public void setIncludeForeignPerson(ITitledEntity includeForeignPerson)
    {
        isIncludeForeignPerson = includeForeignPerson;
    }
}