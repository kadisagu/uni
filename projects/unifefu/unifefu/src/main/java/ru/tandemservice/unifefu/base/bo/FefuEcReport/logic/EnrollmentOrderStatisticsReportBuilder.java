/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import org.hibernate.Session;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.entity.catalog.FefuEnrollmentStep;
import ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt;
import ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.*;

import static org.tandemframework.core.CoreCollectionUtils.Pair;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 10.09.2013
 */
public class EnrollmentOrderStatisticsReportBuilder implements IFefuEcReportBuilder
{
    private Session session;
    // Параметры отчета
    private FefuEnrollmentOrderStatisticsReport report;
    private List<StudentCategory> studentCategoryList;
    private List<Qualifications> qualificationList;
    private List<OrgUnit> formativeOrgUnitList;
    private List<OrgUnit> territorialOrgUnitList;
    private List<EducationLevelsHighSchool> directionList;
    private List<DevelopForm> developFormList;
    private List<DevelopCondition> developConditionList;
    private List<DevelopTech> developTechList;
    private List<DevelopPeriod> developPeriodList;

    private Map<Pair<Long, Long>, List<DirectionItem>> eduMap = new HashMap<>();
    private Map<Long, String> orgUnitMap = new HashMap<>();
    private Map<Long, String> hsMap = new HashMap<>();
    private List<FefuEnrollmentStep> stepList;

    // Локальные переменные
    private Long allEnrolledCount;
    private int rowCount; // Количество строк в таблице (оргюниты + направления)
    private int grayColorIndex;

    public EnrollmentOrderStatisticsReportBuilder(FefuEnrollmentOrderStatisticsReport report, List<StudentCategory> studentCategoryList, List<Qualifications> qualificationList, List<OrgUnit> formativeOrgUnitList, List<OrgUnit> territorialOrgUnitList, List<EducationLevelsHighSchool> directionList, List<DevelopForm> developFormList, List<DevelopCondition> developConditionList, List<DevelopTech> developTechList, List<DevelopPeriod> developPeriodList)
    {
        this.report = report;
        this.studentCategoryList = studentCategoryList;
        this.qualificationList = qualificationList;
        this.formativeOrgUnitList = formativeOrgUnitList;
        this.territorialOrgUnitList = territorialOrgUnitList;
        this.directionList = directionList;
        this.developFormList = developFormList;
        this.developConditionList = developConditionList;
        this.developTechList = developTechList;
        this.developPeriodList = developPeriodList;
    }

    public DatabaseFile getContent(Session session)
    {
        this.session = session;
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "fefuEnrollmentOrderStatisticsReport");
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        grayColorIndex = document.getHeader().getColorTable().addColor(233, 233, 233);
        allEnrolledCount = 0L;

        loadData();
        createTableModifier("head", "data").modify(document);

        new RtfInjectModifier()
                .put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getFormingDate()))
                .put("allCount", allEnrolledCount.toString())
                .modify(document);

        return RtfUtil.toByteArray(document);
    }

    private void loadData()
    {
        rowCount = 0;

        // грузим этапы зачисления, использованные в приказов из данной приемной кампании
        stepList = new DQLSelectBuilder().fromEntity(EnrollmentOrderFefuExt.class, "ext")
                .predicate(DQLPredicateType.distinct)
                .column(property("ext", EnrollmentOrderFefuExt.enrollmentStep()))
                .joinPath(DQLJoinType.inner, EnrollmentOrderFefuExt.enrollmentOrder().fromAlias("ext"), "enrOrder")
                .where(isNotNull(property("ext", EnrollmentOrderFefuExt.enrollmentStep())))
                .where(eq(property("enrOrder", EnrollmentOrder.enrollmentCampaign()), value(report.getEnrollmentCampaign())))
                .where(eq(property("enrOrder", EnrollmentOrder.state().code()), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
                .createStatement(session).list();

        if (stepList.isEmpty())
            return;

        // Сортируем этапы по коду элемента справочника, но колонка по договору должна быть последней
        Collections.sort(stepList, new Comparator<FefuEnrollmentStep>()
        {
            @Override
            public int compare(FefuEnrollmentStep o1, FefuEnrollmentStep o2)
            {
                if (UniFefuDefines.ENROLLMENT_STEP_CONTRACT.equals(o1.getCode()))
                    return 1;
                else if (UniFefuDefines.ENROLLMENT_STEP_CONTRACT.equals(o2.getCode()))
                    return -1;
                else
                    return o1.getCode().compareTo(o2.getCode());
            }
        });

        // Считаем количество зачисленных по направлениям подготовки по каждому этапу зачисления
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "extract")
                .joinPath(DQLJoinType.inner, EnrollmentExtract.paragraph().order().fromAlias("extract"), "enrOrder")
                .joinPath(DQLJoinType.inner, EnrollmentExtract.entity().fromAlias("extract"), "preStudent")
                .joinPath(DQLJoinType.inner, PreliminaryEnrollmentStudent.requestedEnrollmentDirection().fromAlias("preStudent"), "red")
                .joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.enrollmentDirection().fromAlias("red"), "enrDir")
                .joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("enrDir"), "ou")
                .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().fromAlias("ou"), "hs")
                .joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().fromAlias("red"), "request")
                .joinPath(DQLJoinType.inner, EntrantRequest.entrant().fromAlias("request"), "entrant")
                .joinEntity("enrOrder", DQLJoinType.inner, EnrollmentOrderFefuExt.class, "ext",
                            eq(
                                    property("enrOrder", EnrollmentOrder.id()),
                                    property("ext", EnrollmentOrderFefuExt.enrollmentOrder().id())
                            ))
                .joinPath(DQLJoinType.inner, EnrollmentOrderFefuExt.enrollmentStep().fromAlias("ext"), "step");

        // Условия выбоки

        builder.where(eq(property("entrant", Entrant.enrollmentCampaign()), value(report.getEnrollmentCampaign())));
        builder.where(betweenDays(EntrantRequest.regDate().fromAlias("request"), report.getDateFrom(), report.getDateTo()));
        builder.where(eq(property("enrOrder", EnrollmentOrder.state().code()), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)));
        builder.where(eq(property("red", RequestedEnrollmentDirection.state().code()), value(UniecDefines.ENTRANT_STATE_ENROLED_CODE)));

        if (studentCategoryList != null && !studentCategoryList.isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.studentCategory()), studentCategoryList));

        if (qualificationList != null && !qualificationList.isEmpty())
            builder.where(in(property("hs", EducationLevelsHighSchool.educationLevel().qualification()), qualificationList));

        if (report.getCompensationType() != null)
            builder.where(eq(property("preStudent", PreliminaryEnrollmentStudent.compensationType()), value(report.getCompensationType())));

        if (formativeOrgUnitList != null && !formativeOrgUnitList.isEmpty())
            builder.where(in(property("ou", EducationOrgUnit.formativeOrgUnit()), formativeOrgUnitList));

        if (territorialOrgUnitList != null && !territorialOrgUnitList.isEmpty())
            builder.where(in(property("ou", EducationOrgUnit.territorialOrgUnit()), territorialOrgUnitList));

        if (directionList != null && !directionList.isEmpty())
            builder.where(in(property("ou", EducationOrgUnit.educationLevelHighSchool()), directionList));

        if (developFormList != null && !developFormList.isEmpty())
            builder.where(in(property("ou", EducationOrgUnit.developForm()), developFormList));

        if (developConditionList != null && !developConditionList.isEmpty())
            builder.where(in(property("ou", EducationOrgUnit.developCondition()), developConditionList));

        if (developTechList != null && !developTechList.isEmpty())
            builder.where(in(property("ou", EducationOrgUnit.developTech()), developTechList));

        if (developPeriodList != null && !developPeriodList.isEmpty())
            builder.where(in(property("ou", EducationOrgUnit.developPeriod()), developPeriodList));

        if (report.getIncludeForeignPerson() == null || !report.getIncludeForeignPerson())
            builder.where(eq(property("entrant", Entrant.person().identityCard().citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));

        // Группируем
        builder.group(property("enrDir", EnrollmentDirection.id()));
        builder.group(property("enrDir", EnrollmentDirection.ministerialPlan()));
        builder.group(property("ou", EducationOrgUnit.formativeOrgUnit().id()));
        builder.group(property("ou", EducationOrgUnit.territorialOrgUnit().id()));
        builder.group(property("hs", EducationLevelsHighSchool.id()));
        builder.group(property("hs", EducationLevelsHighSchool.title()));

        /* [0] */
        builder.column(property("ou", EducationOrgUnit.formativeOrgUnit().id()));
        /* [1] */
        builder.column(property("ou", EducationOrgUnit.territorialOrgUnit().id()));
        /* [2] */
        builder.column(property("hs", EducationLevelsHighSchool.id()));
        /* [3] */
        builder.column(property("enrDir", EnrollmentDirection.ministerialPlan()));

        // Сортируем по названию (без ОКСО)
        builder.order(property("hs", EducationLevelsHighSchool.title()));

        // Динамически формируем колонки для подсчета абитуриентов
        for (FefuEnrollmentStep currentStep : stepList)
        {
            // Для каждого этапа суммируем количество зачисленных приказом по данному этапу
            builder.column(DQLFunctions.sum(
                    caseExpr(
                            new IDQLExpression[]{eq(property("step", FefuEnrollmentStep.id()), value(currentStep.getId()))},
                            new IDQLExpression[]{value(1)},
                            value(0)
                    )
            ));
        }

        // Переводим данные в удобоваримый вид и группируем по связке формирующий + территориальный оргюнит
        final int dynamicColCount = stepList.size();
        final int staticColCount = builder.getColumnCount() - stepList.size();

        for (Object[] item : builder.createStatement(session).<Object[]>list())
        {
            Pair<Long, Long> orgUnitPair = new Pair<>((Long) item[0], (Long) item[1]);
            List<DirectionItem> directionItems = eduMap.get(orgUnitPair);
            if (directionItems == null)
            {
                eduMap.put(orgUnitPair, directionItems = new ArrayList<>());
            }

            Object[] countByStep = new Long[dynamicColCount];
            System.arraycopy(item, staticColCount, countByStep, 0, dynamicColCount);

            Long dirId = (Long) item[2];
            directionItems.add(new DirectionItem(dirId, (Long[]) countByStep, (Integer) item[3]));
            orgUnitMap.put(orgUnitPair.getX(), null);
            orgUnitMap.put(orgUnitPair.getY(), null);
            hsMap.put(dirId, null);
            rowCount++;
        }

        rowCount += eduMap.size();

        if (eduMap.isEmpty())
            return;

        // Загружаем названия подразделений (если есть печатное название берем его, иначе - обычное)
        DQLSelectBuilder ouTitleBuilder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "o")
                .column("o.id")
                .column(DQLFunctions.coalesce(property("o", OrgUnit.nominativeCaseTitle()), property("o", OrgUnit.title())))
                .where(in("o.id", orgUnitMap.keySet()));

        for (Object[] item : ouTitleBuilder.createStatement(session).<Object[]>list())
        {
            orgUnitMap.put((Long) item[0], (String) item[1]);
        }

        // Загружаем названия направлений подготовки
        DQLSelectBuilder hsTitleBuilder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, "hs")
                .column("hs.id")
                .column(property("hs", EducationLevelsHighSchool.printTitle()))
                .where(in("hs.id", hsMap.keySet()));

        for (Object item[] : hsTitleBuilder.createStatement(session).<Object[]>list())
        {
            hsMap.put((Long) item[0], (String) item[1]);
        }
    }

    private class DirectionItem
    {
        private Long directionId;
        private Long[] countByStep;
        private Integer plan;

        private DirectionItem(Long directionId, Long[] countByStep, Integer plan)
        {
            this.countByStep = countByStep;
            this.directionId = directionId;
            this.plan = plan;
        }

        @Override
        public boolean equals(Object o)
        {
            return (o instanceof DirectionItem) && this.directionId.equals(((DirectionItem) o).directionId);
        }
    }

    private RtfTableModifier createTableModifier(String headName, String tableLabel)
    {
        final String greyTag = "{grey}";
        final int colCount = stepList.size() + 2 + 1; // 2 - колонка с планом на бджет и вычиляемая (план минус прием), 1 - колонка с названием
        List<String[]> dataList = new ArrayList<>(rowCount); // 1 - строка заголовков (названия колонок)

        int curCol = 1;
        String[][] headRow = new String[1][colCount];
        headRow[0][0] = "";
        FefuEnrollmentStep contractStep = null;
        // Заголовки (сокращенные названия этапов и две вычисляемые колонки)
        for (FefuEnrollmentStep step : stepList)
        {
            if (contractStep != null)
                throw new IllegalStateException(); // этап контрактников должен быть последним в списке этапов

            // Котнракт должен идти последней колонкой
            if (!UniFefuDefines.ENROLLMENT_STEP_CONTRACT.equals(step.getCode()))
                headRow[0][curCol++] = step.getShortTitle();
            else
                contractStep = step;
        }
        headRow[0][curCol++] = "план";
        headRow[0][curCol++] = "выполн.";
        if (contractStep != null)
        {
            headRow[0][curCol] = contractStep.getShortTitle();
            stepList.remove(contractStep);
        }

        long[] allSum = new long[colCount];
        for (Pair<Pair<Long, Long>, List<DirectionItem>> entry : getSortedOrgUnitPairs())
        {
            Pair<Long, Long> orgUnitPair = entry.getX();
            List<DirectionItem> directionItems = entry.getY();

            String[] row = new String[colCount];
            long[] orgUnitSum = new long[colCount];
            dataList.add(row);

            // Название формирующего и в скобках название территориального подразделений
            row[0] = greyTag + orgUnitMap.get(orgUnitPair.getX()) + " (" + orgUnitMap.get(orgUnitPair.getY()) + ")";

            for (DirectionItem directionItem : directionItems)
            {
                int dirCol = 0, stepIdx;
                long budjetEnrolled = 0; // количество зачисленных на бюджет
                String[] directionRow = new String[colCount];
                // название направления подготовки
                directionRow[dirCol++] = hsMap.get(directionItem.directionId);
                for (stepIdx = 0; stepIdx < stepList.size(); stepIdx++)
                {
                    long count = directionItem.countByStep[stepIdx];
                    orgUnitSum[dirCol] += count;
                    directionRow[dirCol++] = String.valueOf(count);
                    allEnrolledCount += count;
                    budjetEnrolled += count;
                }
                orgUnitSum[dirCol] += directionItem.plan;
                directionRow[dirCol++] = String.valueOf(directionItem.plan); // план
                orgUnitSum[dirCol] += budjetEnrolled - directionItem.plan;
                directionRow[dirCol++] = String.valueOf(budjetEnrolled - directionItem.plan); // сколько осталось для выполнения плана зачисления на бюджет
                if (contractStep != null)
                {
                    // последней колонкой контрактники
                    long count = directionItem.countByStep[stepIdx];
                    orgUnitSum[dirCol] += count;
                    allEnrolledCount += count;
                    directionRow[dirCol] = String.valueOf(count);
                }

                dataList.add(directionRow);
            }

            for (int i = 1; i < colCount; i++)
            {
                row[i] = greyTag + String.valueOf(orgUnitSum[i]);
                allSum[i] += orgUnitSum[i];
            }
        }

        String[] allSumRow = new String[allSum.length];
        for (int i = 1; i < allSum.length; i++)
        {
            allSumRow[i] = String.valueOf(allSum[i]);
        }
        dataList.add(0, allSumRow);

        IRtfRowIntercepter rowInterceptor = new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                // Разбиваем вторую ячейку на динамическое количество колонок
                int[] parts = new int[colCount - 1];
                Arrays.fill(parts, 1);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 1, null, parts);
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (rowIndex == 0)
                {
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                }

                if (value.startsWith(greyTag))
                {
                    cell.setBackgroundColorIndex(grayColorIndex);
                    return new RtfString().boldBegin().append(value.substring(greyTag.length())).boldEnd().toList();
                }

                return null;
            }
        };

        IRtfRowIntercepter headRowInterceptor = new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                // Разбиваем вторую ячейку на динамическое количество колонок
                int[] parts = new int[colCount - 1];
                Arrays.fill(parts, 1);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 1, null, parts);
            }
        };

        return new RtfTableModifier()
                .put(tableLabel, dataList.toArray(new String[dataList.size()][]))
                .put(tableLabel, rowInterceptor)
                .put(headName, headRow)
                .put(headName, headRowInterceptor);
    }

    private List<Pair<Pair<Long, Long>, List<DirectionItem>>> getSortedOrgUnitPairs()
    {
        List<Pair<Pair<Long, Long>, List<DirectionItem>>> resultList = new ArrayList<>(eduMap.size());
        // Сортируем по названию формирующего подразделелния. В рамках формирующего - по названию территориального
        List<Pair<Long, Long>> sortedList = new ArrayList<>(eduMap.keySet());
        Collections.sort(sortedList, new Comparator<Pair<Long, Long>>()
        {
            @Override
            public int compare(Pair<Long, Long> o1, Pair<Long, Long> o2)
            {
                if (!o1.getX().equals(o2.getX()))
                    return orgUnitMap.get(o1.getX()).compareTo(orgUnitMap.get(o2.getX()));
                return orgUnitMap.get(o1.getY()).compareTo(orgUnitMap.get(o2.getY()));
            }
        });
        for (Pair<Long, Long> pair : sortedList)
        {
            resultList.add(new Pair<>(pair, eduMap.get(pair)));
        }
        return resultList;
    }
}