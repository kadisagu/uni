package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. «О смене условий освоения (перевод на обучение в ускоренные сроки)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuTransfAcceleratedTimeStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract";
    public static final String ENTITY_NAME = "fefuTransfAcceleratedTimeStuExtract";
    public static final int VERSION_HASH = -776964702;
    private static IEntityMeta ENTITY_META;

    public static final String P_SEASON = "season";
    public static final String P_PLANNED_DATE = "plannedDate";

    private String _season;     // Сезон планируемого выпуска
    private Date _plannedDate;     // Планируемая дата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сезон планируемого выпуска.
     */
    @Length(max=255)
    public String getSeason()
    {
        return _season;
    }

    /**
     * @param season Сезон планируемого выпуска.
     */
    public void setSeason(String season)
    {
        dirty(_season, season);
        _season = season;
    }

    /**
     * @return Планируемая дата.
     */
    public Date getPlannedDate()
    {
        return _plannedDate;
    }

    /**
     * @param plannedDate Планируемая дата.
     */
    public void setPlannedDate(Date plannedDate)
    {
        dirty(_plannedDate, plannedDate);
        _plannedDate = plannedDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuTransfAcceleratedTimeStuExtractGen)
        {
            setSeason(((FefuTransfAcceleratedTimeStuExtract)another).getSeason());
            setPlannedDate(((FefuTransfAcceleratedTimeStuExtract)another).getPlannedDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuTransfAcceleratedTimeStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuTransfAcceleratedTimeStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuTransfAcceleratedTimeStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "season":
                    return obj.getSeason();
                case "plannedDate":
                    return obj.getPlannedDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "season":
                    obj.setSeason((String) value);
                    return;
                case "plannedDate":
                    obj.setPlannedDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "season":
                        return true;
                case "plannedDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "season":
                    return true;
                case "plannedDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "season":
                    return String.class;
                case "plannedDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuTransfAcceleratedTimeStuExtract> _dslPath = new Path<FefuTransfAcceleratedTimeStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuTransfAcceleratedTimeStuExtract");
    }
            

    /**
     * @return Сезон планируемого выпуска.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract#getSeason()
     */
    public static PropertyPath<String> season()
    {
        return _dslPath.season();
    }

    /**
     * @return Планируемая дата.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract#getPlannedDate()
     */
    public static PropertyPath<Date> plannedDate()
    {
        return _dslPath.plannedDate();
    }

    public static class Path<E extends FefuTransfAcceleratedTimeStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<String> _season;
        private PropertyPath<Date> _plannedDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сезон планируемого выпуска.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract#getSeason()
     */
        public PropertyPath<String> season()
        {
            if(_season == null )
                _season = new PropertyPath<String>(FefuTransfAcceleratedTimeStuExtractGen.P_SEASON, this);
            return _season;
        }

    /**
     * @return Планируемая дата.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract#getPlannedDate()
     */
        public PropertyPath<Date> plannedDate()
        {
            if(_plannedDate == null )
                _plannedDate = new PropertyPath<Date>(FefuTransfAcceleratedTimeStuExtractGen.P_PLANNED_DATE, this);
            return _plannedDate;
        }

        public Class getEntityClass()
        {
            return FefuTransfAcceleratedTimeStuExtract.class;
        }

        public String getEntityName()
        {
            return "fefuTransfAcceleratedTimeStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
