package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 24.01.2014
 */
public class MS_unifefu_2x4x1_11to12 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select dl.directumid_p, dl.ORDER_ID, dl.OPERATIONTYPE_P, ex.id, ex.DIRECTUMID_P" +
                " from FEFUDIRECTUMLOG_T dl" +
                " inner join FEFUSTUDENTORDEREXTENSION_T ex on dl.orderext_id=ex.id" +
                " order by dl.OPERATIONDATETIME_P desc");
        ResultSet rs = stmt.getResultSet();

        Map<Long, String> orderToLastDirectumDocIdMap = new HashMap<>();
        PreparedStatement update = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMID_P=? where ID=?");

        while (rs.next())
        {
            String operationDirectumId = rs.getString(1);
            String operationType = rs.getString(3);
            String directumId = rs.getString(5);
            Long orderId = rs.getLong(2);

            if (!orderToLastDirectumDocIdMap.containsKey(orderId) && operationType.contains("Создание документа в Directum"))
            {
                orderToLastDirectumDocIdMap.put(orderId, operationDirectumId);
                if (null != operationDirectumId && null != directumId && !operationDirectumId.equals(directumId))
                {
                    Long exId = rs.getLong(4);
                    update.setString(1, operationDirectumId);
                    update.setLong(2, exId);
                    update.executeUpdate();
                }
            }
        }

        update.close();
        stmt.close();
    }
}