/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu10.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuEduEnrolmentToSecondAndNextCourseStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 17.04.2013
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuEduEnrolmentToSecondAndNextCourseStuExtract, Model>
{
}