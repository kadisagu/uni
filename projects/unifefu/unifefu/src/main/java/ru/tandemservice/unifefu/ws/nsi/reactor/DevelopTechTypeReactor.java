/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.DevelopTechType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class DevelopTechTypeReactor extends SimpleCatalogEntityNewReactor<DevelopTechType, DevelopTech>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<DevelopTech> getEntityClass()
    {
        return DevelopTech.class;
    }

    @Override
    public Class<DevelopTechType> getNSIEntityClass()
    {
        return DevelopTechType.class;
    }

    @Override
    public DevelopTechType getCatalogElementRetrieveRequestObject(String guid)
    {
        DevelopTechType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createDevelopTechType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public DevelopTechType createEntity(CoreCollectionUtils.Pair<DevelopTech, FefuNsiIds> entityPair)
    {
        DevelopTechType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createDevelopTechType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setDeveloptechID(entityPair.getX().getCode());
        nsiEntity.setDevelopTechName(entityPair.getX().getTitle());
        nsiEntity.setDevelopTechNameShort(entityPair.getX().getShortTitle());
        nsiEntity.setDeveloptechGroup(entityPair.getX().getGroup());
        return nsiEntity;
    }

    @Override
    public DevelopTech createEntity(DevelopTechType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getDevelopTechName()) return null;

        DevelopTech entity = new DevelopTech();
        entity.setTitle(nsiEntity.getDevelopTechName());
        entity.setShortTitle(nsiEntity.getDevelopTechNameShort());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(DevelopTech.class));
        entity.setGroup(nsiEntity.getDeveloptechGroup());

        return entity;
    }

    @Override
    public DevelopTech updatePossibleDuplicateFields(DevelopTechType nsiEntity, CoreCollectionUtils.Pair<DevelopTech, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), DevelopTech.title().s()))
                entityPair.getX().setTitle(nsiEntity.getDevelopTechName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), DevelopTech.shortTitle().s()))
                entityPair.getX().setShortTitle(nsiEntity.getDevelopTechNameShort());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), DevelopTech.group().s()))
                entityPair.getX().setGroup(nsiEntity.getDeveloptechGroup());
        }
        return entityPair.getX();
    }

    @Override
    public DevelopTechType updateNsiEntityFields(DevelopTechType nsiEntity, DevelopTech entity)
    {
        nsiEntity.setDeveloptechID(entity.getCode());
        nsiEntity.setDevelopTechName(entity.getTitle());
        nsiEntity.setDevelopTechNameShort(entity.getShortTitle());
        nsiEntity.setDeveloptechGroup(entity.getGroup());
        return nsiEntity;
    }
}