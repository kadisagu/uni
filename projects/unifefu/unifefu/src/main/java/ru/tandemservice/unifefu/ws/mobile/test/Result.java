/**
 * Result.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.mobile.test;

public class Result  implements java.io.Serializable {
    private java.lang.String id;

    private java.lang.String student_id;

    private java.lang.String subject_id;

    private java.lang.String edu_year_id;

    private java.lang.String semester;

    private java.lang.String credit;

    private java.lang.String exam;

    private java.lang.String diff_credit;

    private java.lang.String result;

    private java.lang.String max_result;

    private ru.tandemservice.unifefu.ws.mobile.test.Milestone[] milestones;

    private ru.tandemservice.unifefu.ws.mobile.test.WorkResult[] work_results;

    public Result() {
    }

    public Result(
           java.lang.String id,
           java.lang.String student_id,
           java.lang.String subject_id,
           java.lang.String edu_year_id,
           java.lang.String semester,
           java.lang.String credit,
           java.lang.String exam,
           java.lang.String diff_credit,
           java.lang.String result,
           java.lang.String max_result,
           ru.tandemservice.unifefu.ws.mobile.test.Milestone[] milestones,
           ru.tandemservice.unifefu.ws.mobile.test.WorkResult[] work_results) {
           this.id = id;
           this.student_id = student_id;
           this.subject_id = subject_id;
           this.edu_year_id = edu_year_id;
           this.semester = semester;
           this.credit = credit;
           this.exam = exam;
           this.diff_credit = diff_credit;
           this.result = result;
           this.max_result = max_result;
           this.milestones = milestones;
           this.work_results = work_results;
    }


    /**
     * Gets the id value for this Result.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this Result.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the student_id value for this Result.
     * 
     * @return student_id
     */
    public java.lang.String getStudent_id() {
        return student_id;
    }


    /**
     * Sets the student_id value for this Result.
     * 
     * @param student_id
     */
    public void setStudent_id(java.lang.String student_id) {
        this.student_id = student_id;
    }


    /**
     * Gets the subject_id value for this Result.
     * 
     * @return subject_id
     */
    public java.lang.String getSubject_id() {
        return subject_id;
    }


    /**
     * Sets the subject_id value for this Result.
     * 
     * @param subject_id
     */
    public void setSubject_id(java.lang.String subject_id) {
        this.subject_id = subject_id;
    }


    /**
     * Gets the edu_year_id value for this Result.
     * 
     * @return edu_year_id
     */
    public java.lang.String getEdu_year_id() {
        return edu_year_id;
    }


    /**
     * Sets the edu_year_id value for this Result.
     * 
     * @param edu_year_id
     */
    public void setEdu_year_id(java.lang.String edu_year_id) {
        this.edu_year_id = edu_year_id;
    }


    /**
     * Gets the semester value for this Result.
     * 
     * @return semester
     */
    public java.lang.String getSemester() {
        return semester;
    }


    /**
     * Sets the semester value for this Result.
     * 
     * @param semester
     */
    public void setSemester(java.lang.String semester) {
        this.semester = semester;
    }


    /**
     * Gets the credit value for this Result.
     * 
     * @return credit
     */
    public java.lang.String getCredit() {
        return credit;
    }


    /**
     * Sets the credit value for this Result.
     * 
     * @param credit
     */
    public void setCredit(java.lang.String credit) {
        this.credit = credit;
    }


    /**
     * Gets the exam value for this Result.
     * 
     * @return exam
     */
    public java.lang.String getExam() {
        return exam;
    }


    /**
     * Sets the exam value for this Result.
     * 
     * @param exam
     */
    public void setExam(java.lang.String exam) {
        this.exam = exam;
    }


    /**
     * Gets the diff_credit value for this Result.
     * 
     * @return diff_credit
     */
    public java.lang.String getDiff_credit() {
        return diff_credit;
    }


    /**
     * Sets the diff_credit value for this Result.
     * 
     * @param diff_credit
     */
    public void setDiff_credit(java.lang.String diff_credit) {
        this.diff_credit = diff_credit;
    }


    /**
     * Gets the result value for this Result.
     * 
     * @return result
     */
    public java.lang.String getResult() {
        return result;
    }


    /**
     * Sets the result value for this Result.
     * 
     * @param result
     */
    public void setResult(java.lang.String result) {
        this.result = result;
    }


    /**
     * Gets the max_result value for this Result.
     * 
     * @return max_result
     */
    public java.lang.String getMax_result() {
        return max_result;
    }


    /**
     * Sets the max_result value for this Result.
     * 
     * @param max_result
     */
    public void setMax_result(java.lang.String max_result) {
        this.max_result = max_result;
    }


    /**
     * Gets the milestones value for this Result.
     * 
     * @return milestones
     */
    public ru.tandemservice.unifefu.ws.mobile.test.Milestone[] getMilestones() {
        return milestones;
    }


    /**
     * Sets the milestones value for this Result.
     * 
     * @param milestones
     */
    public void setMilestones(ru.tandemservice.unifefu.ws.mobile.test.Milestone[] milestones) {
        this.milestones = milestones;
    }

    public ru.tandemservice.unifefu.ws.mobile.test.Milestone getMilestones(int i) {
        return this.milestones[i];
    }

    public void setMilestones(int i, ru.tandemservice.unifefu.ws.mobile.test.Milestone _value) {
        this.milestones[i] = _value;
    }


    /**
     * Gets the work_results value for this Result.
     * 
     * @return work_results
     */
    public ru.tandemservice.unifefu.ws.mobile.test.WorkResult[] getWork_results() {
        return work_results;
    }


    /**
     * Sets the work_results value for this Result.
     * 
     * @param work_results
     */
    public void setWork_results(ru.tandemservice.unifefu.ws.mobile.test.WorkResult[] work_results) {
        this.work_results = work_results;
    }

    public ru.tandemservice.unifefu.ws.mobile.test.WorkResult getWork_results(int i) {
        return this.work_results[i];
    }

    public void setWork_results(int i, ru.tandemservice.unifefu.ws.mobile.test.WorkResult _value) {
        this.work_results[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Result)) return false;
        Result other = (Result) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.student_id==null && other.getStudent_id()==null) || 
             (this.student_id!=null &&
              this.student_id.equals(other.getStudent_id()))) &&
            ((this.subject_id==null && other.getSubject_id()==null) || 
             (this.subject_id!=null &&
              this.subject_id.equals(other.getSubject_id()))) &&
            ((this.edu_year_id==null && other.getEdu_year_id()==null) || 
             (this.edu_year_id!=null &&
              this.edu_year_id.equals(other.getEdu_year_id()))) &&
            ((this.semester==null && other.getSemester()==null) || 
             (this.semester!=null &&
              this.semester.equals(other.getSemester()))) &&
            ((this.credit==null && other.getCredit()==null) || 
             (this.credit!=null &&
              this.credit.equals(other.getCredit()))) &&
            ((this.exam==null && other.getExam()==null) || 
             (this.exam!=null &&
              this.exam.equals(other.getExam()))) &&
            ((this.diff_credit==null && other.getDiff_credit()==null) || 
             (this.diff_credit!=null &&
              this.diff_credit.equals(other.getDiff_credit()))) &&
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult()))) &&
            ((this.max_result==null && other.getMax_result()==null) || 
             (this.max_result!=null &&
              this.max_result.equals(other.getMax_result()))) &&
            ((this.milestones==null && other.getMilestones()==null) || 
             (this.milestones!=null &&
              java.util.Arrays.equals(this.milestones, other.getMilestones()))) &&
            ((this.work_results==null && other.getWork_results()==null) || 
             (this.work_results!=null &&
              java.util.Arrays.equals(this.work_results, other.getWork_results())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getStudent_id() != null) {
            _hashCode += getStudent_id().hashCode();
        }
        if (getSubject_id() != null) {
            _hashCode += getSubject_id().hashCode();
        }
        if (getEdu_year_id() != null) {
            _hashCode += getEdu_year_id().hashCode();
        }
        if (getSemester() != null) {
            _hashCode += getSemester().hashCode();
        }
        if (getCredit() != null) {
            _hashCode += getCredit().hashCode();
        }
        if (getExam() != null) {
            _hashCode += getExam().hashCode();
        }
        if (getDiff_credit() != null) {
            _hashCode += getDiff_credit().hashCode();
        }
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        if (getMax_result() != null) {
            _hashCode += getMax_result().hashCode();
        }
        if (getMilestones() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMilestones());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMilestones(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getWork_results() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getWork_results());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getWork_results(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Result.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "Result"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("student_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "student_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subject_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "subject_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("edu_year_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "edu_year_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("semester");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "semester"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("credit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "credit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exam");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "exam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diff_credit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "diff_credit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("max_result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "max_result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("milestones");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "milestones"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "Milestone"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_results");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "work_results"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "WorkResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
