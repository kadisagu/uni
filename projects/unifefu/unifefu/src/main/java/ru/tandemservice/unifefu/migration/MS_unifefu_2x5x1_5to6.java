package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.5.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.5.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewAddress

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewaddress_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("addressbase_id", DBType.LONG).setNullable(false),
				new DBColumn("adress_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewAddress");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPerson

		// удалено свойство addressRegCountry
		{

			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressregcountry_p");

		}

		// удалено свойство addressRegSettlement
		{


			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressregsettlement_p");

		}

		// удалено свойство addressRegStreet
		{


			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressregstreet_p");

		}

		// удалено свойство addressRegHouseNumber
		{


			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressreghousenumber_p");

		}

		// удалено свойство addressRegHouseUnitNumber
		{


			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressreghouseunitnumber_p");

		}

		// удалено свойство addressRegFlatNumber
		{


			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressregflatnumber_p");

		}

		// удалено свойство addressFactCountry
		{


			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressfactcountry_p");

		}

		// удалено свойство addressFactSettlement
		{


			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressfactsettlement_p");

		}

		// удалено свойство addressFactStreet
		{


			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressfactstreet_p");

		}

		// удалено свойство addressFactHouseNumber
		{


			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressfacthousenumber_p");

		}

		// удалено свойство addressFactHouseUnitNumber
		{

			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressfacthouseunitnumber_p");

		}

		// удалено свойство addressFactFlatNumber
		{


			// удалить колонку
			tool.dropColumn("mdbviewperson_t", "addressfactflatnumber_p");

		}

		// создано свойство addressRegId
		{
			// создать колонку
			tool.createColumn("mdbviewperson_t", new DBColumn("addressregid_id", DBType.LONG));

		}

		// создано свойство addressFactId
		{
			// создать колонку
			tool.createColumn("mdbviewperson_t", new DBColumn("addressfactid_id", DBType.LONG));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonNextofkin

		// удалено свойство addressCountry
		{

			// удалить колонку
			tool.dropColumn("mdbviewpersonnextofkin_t", "addresscountry_p");

		}

		// удалено свойство addressSettlement
		{


			// удалить колонку
			tool.dropColumn("mdbviewpersonnextofkin_t", "addresssettlement_p");

		}

		// удалено свойство addressStreet
		{


			// удалить колонку
			tool.dropColumn("mdbviewpersonnextofkin_t", "addressstreet_p");

		}

		// удалено свойство addressHouseNumber
		{


			// удалить колонку
			tool.dropColumn("mdbviewpersonnextofkin_t", "addresshousenumber_p");

		}

		// удалено свойство addressHouseUnitNumber
		{


			// удалить колонку
			tool.dropColumn("mdbviewpersonnextofkin_t", "addresshouseunitnumber_p");

		}

		// удалено свойство addressFlatNumber
		{


			// удалить колонку
			tool.dropColumn("mdbviewpersonnextofkin_t", "addressflatnumber_p");

		}

		// создано свойство addressId
		{
			// создать колонку
			tool.createColumn("mdbviewpersonnextofkin_t", new DBColumn("addressid_id", DBType.LONG));

		}


    }
}