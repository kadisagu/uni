/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d101.Add;

import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;

import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 29.09.2013
 */
public class Model extends DocumentAddBaseModel
{
    private String _addresseeName;
    private String _addresseeSettlement;
    private String _addresseeStreet;
    private String _addresseeHouse;
    private String _addresseePostCode;
    private String _enrollmentOrderNumber;
    private Date _enrollmentOrderDate;

    private List<AbstractStudentExtract> _orderList;

    public String getAddresseeName()
    {
        return _addresseeName;
    }

    public void setAddresseeName(String addresseeName)
    {
        _addresseeName = addresseeName;
    }

    public String getAddresseeSettlement()
    {
        return _addresseeSettlement;
    }

    public void setAddresseeSettlement(String addresseeSettlement)
    {
        _addresseeSettlement = addresseeSettlement;
    }

    public String getAddresseeStreet()
    {
        return _addresseeStreet;
    }

    public void setAddresseeStreet(String addresseeStreet)
    {
        _addresseeStreet = addresseeStreet;
    }

    public String getAddresseeHouse()
    {
        return _addresseeHouse;
    }

    public void setAddresseeHouse(String addresseeHouse)
    {
        _addresseeHouse = addresseeHouse;
    }

    public String getAddresseePostCode()
    {
        return _addresseePostCode;
    }

    public void setAddresseePostCode(String addresseePostCode)
    {
        _addresseePostCode = addresseePostCode;
    }

    public List<AbstractStudentExtract> getOrderList()
    {
        return _orderList;
    }

    public void setOrderList(List<AbstractStudentExtract> orderList)
    {
        _orderList = orderList;
    }

    public String getEnrollmentOrderNumber()
    {
        return _enrollmentOrderNumber;
    }

    public void setEnrollmentOrderNumber(String enrollmentOrderNumber)
    {
        _enrollmentOrderNumber = enrollmentOrderNumber;
    }

    public Date getEnrollmentOrderDate()
    {
        return _enrollmentOrderDate;
    }

    public void setEnrollmentOrderDate(Date enrollmentOrderDate)
    {
        _enrollmentOrderDate = enrollmentOrderDate;
    }
}
