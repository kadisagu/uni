package ru.tandemservice.unifefu.entity.ws;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unifefu.base.bo.NSISync.NSISyncManager;
import ru.tandemservice.unifefu.entity.ws.gen.FefuNsiLogRowGen;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

/**
 * Строка журнала адаптера НСИ
 */
public class FefuNsiLogRow extends FefuNsiLogRowGen
{
    public static final String OUT_EVENT_TYPE = "out";

    @EntityDSLSupport(parts = P_MESSAGE_STATUS)
    public String getStatusStrColored()
    {
        if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_INITIAL.intValue() == getMessageStatus())
            return "<span style=\"color:#AAAAAA\">" + NSISyncManager.MESSAGE_STATUS_UNKNOWN_TITLE + "</span>";
        if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_OK.intValue() == getMessageStatus())
            return "<span style=\"color:#00CC00\">" + NSISyncManager.MESSAGE_STATUS_SUCCESS_TITLE + "</span>";
        if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_WARN.intValue() == getMessageStatus())
            return "<span style=\"color:#CCCC00\">" + NSISyncManager.MESSAGE_STATUS_WARNING_TITLE + "</span>";
        if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_ERROR.intValue() == getMessageStatus())
            return "<span style=\"color:red\">" + NSISyncManager.MESSAGE_STATUS_ERROR_TITLE + "</span>";
        if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_TRANSPORT_ERROR.intValue() == getMessageStatus())
            return "<span style=\"color:#EEAAAA\">" + NSISyncManager.MESSAGE_STATUS_TRANSPORT_ERROR_TITLE + "</span>";
        else return "<span style=\"color:#00CCCC\">Не известен</span>";
    }

    @EntityDSLSupport
    public boolean isCouldNotBeReSent()
    {
        if (!OUT_EVENT_TYPE.equals(getEventType().toLowerCase()) && -1 == getMessageStatus()) return false;
        return !OUT_EVENT_TYPE.equals(getEventType().toLowerCase()) || null == getMessageBody() || FefuNsiRequestsProcessor.OPERATION_TYPE_RETRIEVE.equals(getOperationType());
    }
}