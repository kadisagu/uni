/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPerson.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonExt;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 05.12.2014
 */
public class FefuPersonDao extends CommonDAO implements IFefuPersonDao
{
    @Override
    public void updateFefuLogin(Person person, String login, AuthenticationType authenticationType)
    {
        List<Object[]> principalList = new DQLSelectBuilder().fromEntity(Principal.class, "p").column(property("p"))
                .column(property(Person2PrincipalRelation.person().id().fromAlias("r")))
                .column(property(Person2PrincipalRelation.id().fromAlias("r")))
                .joinEntity("p", DQLJoinType.left, Person2PrincipalRelation.class, "r", eq(property(Principal.id().fromAlias("p")), property(Person2PrincipalRelation.principal().id().fromAlias("r"))))
                .where(or(
                        eq(property(Person2PrincipalRelation.person().id().fromAlias("r")), value(person.getId())),
                        eq(property(Principal.login().fromAlias("p")), value(login))
                ))
                .createStatement(getSession()).list();

        Principal principal = null;
        Principal loginPrincipal = null;
        Long person2PrincipalId = null;
        boolean loginOccupied = false;

        for (Object[] item : principalList)
        {
            Long personId = (Long) item[1];
            if (null != personId && person.getId().equals(personId))
            {
                principal = (Principal) item[0];
                person2PrincipalId = (Long) item[2];
            }

            if (null == personId) loginPrincipal = (Principal) item[0];
            if (null != personId && !person.getId().equals(personId))
                loginOccupied = true;
        }

        if (null == loginPrincipal && !loginOccupied)
        {
            principal.setLogin(login);
        } else if (!loginOccupied)
        {
            if (null != person2PrincipalId)
            {
                delete(person2PrincipalId);
                getSession().flush();
            }

            loginPrincipal.setAuthenticationType(authenticationType);
            getSession().saveOrUpdate(loginPrincipal);

            Person2PrincipalRelation rel = new Person2PrincipalRelation();
            rel.setPrincipal(loginPrincipal);
            rel.setPerson(person);
            getSession().saveOrUpdate(rel);
        }

        principal.setAuthenticationType(authenticationType);
        getSession().saveOrUpdate(principal);

        FefuNsiPersonExt perExt = getByNaturalId(new FefuNsiPersonExt.NaturalId(person));
        if (null == perExt)
        {
            perExt = new FefuNsiPersonExt();
            perExt.setPerson(person);
        }
        perExt.setLogin(login);

        getSession().saveOrUpdate(perExt);
    }
}