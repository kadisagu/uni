/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.unifefu.ws.nsi.datagram.AcademicRankType;

/**
 * @author Dmitry Seleznev
 * @since 07.05.2014
 */
public class ScienceStatusUtil extends SimpleNsiCatalogUtil<AcademicRankType, ScienceStatus>
{
    @Override
    public long getShortTitleHash(ScienceStatus entity, boolean caseInsensitive)
    {
        return 0;
    }
}