/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic;

import com.healthmarketscience.jackcess.Database;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.ImportContracts.FefuContractImportWrapper;
import ru.tandemservice.unifefu.base.vo.ProcessedOnlineEntrantsVO;
import ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel;
import ru.tandemservice.unifefu.entity.FefuImtsaXml;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
public interface IFefuSystemActionDao extends INeedPersistenceSupport
{
    void doExportEduInstitutions(File file);

    String doImportEduInstitutions(File file);

    void makeArchivalEmptyGroupDuplicates();

    void makeArchivalEmptyGroups();

    void makeArchivalGroupsWithNotActiveStudents();

    void doAssignForeignStudentsDeclinationSettings();

    void doChangeEntranceYearForArchiveStudents();

    void doFillEduLevelQualifications() throws Exception;

    void renderPersonalDataReport(boolean includeStudents, boolean includeEntrants, boolean includeEmployee, boolean includeActiveStudent, boolean includeArchival, final OutputStream stream) throws IOException;

    void renderPersonalDataReportLibrary(boolean includeArchival, final OutputStream stream) throws IOException;

    void doTestIdmPersonRegistrationService() throws Exception;

    List<String> doImportContracts(Map<Long, FefuContractImportWrapper> contractsInfoMap);

    void fixCompetitionGroups(EnrollmentCampaign enrollmentCampaign);

    void doMoveOriginals(EnrollmentCampaign enrollmentCampaign);

    void export_StudentOrdersList(final Database mdb) throws IOException;

    List<ProcessedOnlineEntrantsVO> sendOnlineEntrantNotifications(List<ProcessedOnlineEntrantsVO> onlineEntrants);

    void doSendToArchiveStudents(List<Long> studentIds);

    void doArchivePracticeContract();


    /**
     * Возвращает список привязанных к учебным планам xml.
     *
     * @param blockIds id блоков
     * @return список xml
     */
    List<FefuImtsaXml> getXmlList(Collection<Long> blockIds);

    /**
     * Сохраняет лог ошибки импорта ИМЦА. Стирает предшествующий (если был).
     *
     * @param block блок
     * @param error текст ошибки
     */
    void saveOrRewriteImportLog(EppEduPlanVersionBlock block, String error);

    /**
     * Возвращает список соспоставлений циклов ИМЦА циклам Uni.
     *
     * @param secondGosGeneration ГОС2/ФГОС
     * @return список сопоставлений циклов ИМЦА и циклов Uni
     */
    List<FefuImtsaCyclePlanStructureRel> getImtsaCycles(boolean secondGosGeneration);

    /**
     * Возвращает части ГОС треебуемого поколения.
     *
     * @param secondGosGeneration ГОС2/ФГОС
     * @return список частей ГОС
     */
    List<EppPlanStructure> getParts(boolean secondGosGeneration);

    /**
     * Переводит УПв в состояние "Архив" согласно выбранным параметрам
     *
     * @param eduEndYear Год окончания обучения
     * @param eduProgramDuration Срок обучения
     */
    void doArchiveEduPlanVersions(Integer eduEndYear, EduProgramDuration eduProgramDuration);

    void doDeleteAllFefuScheduleICalData();

    /**
     * Подготавливает список идентификаторов прочих приказов, являющихся дублями
     *
     * @return - набор идентификаторов прочих приказов
     */
    Set<Long> getOtherOrdersDuplicates();

    /**
     * Удаляет дубликаты прочих приказов по переданному списку идентификаторов
     *
     * @param otherOrderIdsToDelete - список приказов для удаления
     */
    void doDeleteOtherOrdersDuplicates(List<Long> otherOrderIdsToDelete);

    void doResetPersonSyncDateForFailedStudents();

    void doMergePersonsAndChangeGuidsByCssFile(String fileContent);

    void doRegenBadIdentityCardIdsByFile(String fileContent);

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    int doRegenerateExtractPrintForms(Collection<Long> idList);

    void regenerateExtractPrintForms(Collection<String> extractTypes, Date formingDateFrom, Date formingDateTo);

    void setCustomStateForStuTargetEnrollment(EnrollmentCampaign enrollmentCampaign, Date startDate, StudentCustomStateCI newStatus);

    void doSendNotAcceptRegElementToArchive();

    /**
     * Удаление выбранных приказов
     * @param ids - список идентификаторов приказов
     */
    void deleteNotCoordinatedOrders(List<Long> ids);

    /**
     * Генерирует guid'ы для некоторых сущностей, связанных со студентом
     */
    void doGenerateGuids();

}
