/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.EntrantAdd;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.uniec.component.entrant.EntrantAdd.Model;
import ru.tandemservice.unifefu.dao.daemon.IFEFUPasswordRegistratorDaemonDao;
import ru.tandemservice.unifefu.dao.daemon.IFEFUPersonGuidDaemonDao;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;

/**
 * @author Dmitry Seleznev
 * @since 31.05.2013
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantAdd.DAO
{
    @Override
    public void create(Model model)
    {
        super.create(model);

        Principal principal = get(Person2PrincipalRelation.class, Person2PrincipalRelation.L_PERSON, model.getEntrant().getPerson()).getPrincipal();
        IdentityCard icard = model.getEntrant().getPerson().getIdentityCard();
        String password = StringUtils.capitalize(CoreStringUtils.transliterate(icard.getLastName()).toLowerCase().replaceAll("\'", "")) + (StringUtils.isEmpty(icard.getNumber()) ? "" : icard.getNumber());
        if (password.length() < 8)
        {
            password += RussianDateFormatUtils.getDayString(icard.getBirthDate(), true);
            password += RussianDateFormatUtils.getMonthString(icard.getBirthDate(), true);
            password += RussianDateFormatUtils.getYearString(icard.getBirthDate(), false);
        }
        System.out.println(password);
        PersonManager.instance().dao().updatePersonLogin(model.getEntrant().getPerson().getId(), principal.getLogin(), password);

        FefuNsiIds nsiId = IFEFUPersonGuidDaemonDao.instance.get().doActualizePersonGuid(model.getEntrant().getPerson().getId());
        IFEFUPasswordRegistratorDaemonDao.instance.get().doRegisterNSIRequest(nsiId, model.getEntrant().getPerson(), password);
    }
}