/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.ui.ResultsList;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.PageOrientation;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.unifefu.entity.DiplomaIssuanceFefuExt;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 30.09.2014
 */
public class FefuDipDocumentExcelBuilder
{
    private EduLevel _eduLevel;
    private boolean _showEduLevel;
    private DipDocumentType _dipDocumentType;
    private boolean _showDipDocType;
    private OrgUnit _formativeOrgUnit;
    private boolean _showFormativeOrgUnit;
    private List<EduProgramSubject> _programSubjectList;
    private boolean _showProgramSubject;
    private Date _issueDateFrom;
    private Date _issueDateTo;
    private boolean _showIssueDateFrom;
    private boolean _showIssueDateTo;
    private boolean _duplicate;

    public FefuDipDocumentExcelBuilder(boolean showEduLevel, boolean showDipDocType, boolean showFormativeOrgUnit, boolean showProgramSubject,
                                       boolean showIssueDateFrom, boolean showIssueDateTo, EduLevel eduLevel, DipDocumentType dipDocumentType,
                                       OrgUnit formativeOrgUnit, List<EduProgramSubject> programSubjectList, Date issueDateFrom, Date issueDateTo, boolean duplicate)
    {
        setShowEduLevel(showEduLevel);
        setShowDipDocType(showDipDocType);
        setShowFormativeOrgUnit(showFormativeOrgUnit);
        setShowProgramSubject(showProgramSubject);
        setShowIssueDateFrom(showIssueDateFrom);
        setShowIssueDateTo(showIssueDateTo);
        setEduLevel(eduLevel);
        setDipDocumentType(dipDocumentType);
        setFormativeOrgUnit(formativeOrgUnit);
        setProgramSubjectList(programSubjectList);
        setIssueDateFrom(issueDateFrom);
        setIssueDateTo(issueDateTo);
        setDuplicate(duplicate);

    }

    private String[] _columnTitles = {"Индекс книги регистрации", "Регистрационный номер", "Фамилия", "Имя", "Отчество", "Серия бланка диплома",
            "Номер бланка диплома", "Серия бланка приложения к диплому", "Номер бланка приложения к диплому", "Дата выдачи документа",
            "Направление подготовки/Специальность", "Код специальности по справочнику", "Квалификация/Степень",
            "Код квалификации по ОКСО", "Дата решения ГАК/ГЭК", "Номер протокола ГАК/ГЭК", "Дата приказа об отчислении", "Номер приказа об отчислении",
            "Тип документа", "Уровень образования", "Наименование образовательного учреждения", "Город выдачи",
            "Регистрационный номер заменяемого документа", "Серия бланка заменяемого документа", "Номер бланка заменяемого документа"
    };

    public ByteArrayOutputStream buildReport() throws Exception
    {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(DiplomaObject.class, "d").column(property("d"));
        if (isShowEduLevel())
            dql.where(or(
					eqValue(property("d", DiplomaObject.content().programSubject().subjectIndex().programKind().eduLevel()), getEduLevel()),
					eqValue(property("d", DiplomaObject.content().programSubject().subjectIndex().programKind().eduLevel().parent()), getEduLevel())));
        if (isShowDipDocType())
            dql.where(or(
                    eqValue(property("d", DiplomaObject.content().type()), getDipDocumentType()),
                    eqValue(property("d", DiplomaObject.content().type().parent()), getDipDocumentType())
            ));
        if (isShowFormativeOrgUnit())
            dql.where(eqValue(property("d", DiplomaObject.student().educationOrgUnit().formativeOrgUnit()), getFormativeOrgUnit()));
        if (isShowProgramSubject())
            dql.where(in(property("d", DiplomaObject.content().programSubject()), getProgramSubjectList()));
        if (isShowIssueDateFrom() || isShowIssueDateTo() || isDuplicate())
        {
            if (isDuplicate())
            {
                dql.where(exists(
                        new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "i")
                                .column(property("i.id"))
                                .where(eq(property("i", DiplomaIssuance.diplomaObject()), property("d")))
                                .where(isNotNull(property("i", DiplomaIssuance.replacedIssuance())))
                                .buildQuery()
                ));
            }
            if (isShowIssueDateFrom() && !isShowIssueDateTo())
            {
                dql.where(exists(
                        new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "i")
                                .column(property("i.id"))
                                .where(eq(property("i", DiplomaIssuance.diplomaObject()), property("d")))
                                .where(ge(property("i", DiplomaIssuance.issuanceDate()), value(CoreDateUtils.getDayFirstTimeMoment(getIssueDateFrom()), PropertyType.DATE)))
                                .buildQuery()
                ));
            }

            else if (!isShowIssueDateFrom() && isShowIssueDateTo())
            {
                dql.where(exists(
                        new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "i")
                                .column(property("i.id"))
                                .where(eq(property("i", DiplomaIssuance.diplomaObject()), property("d")))
                                .where(le(property("i", DiplomaIssuance.issuanceDate()), value(CoreDateUtils.getDayFirstTimeMoment(getIssueDateTo()), PropertyType.DATE)))
                                .buildQuery()
                ));
            }

            else if (isShowIssueDateFrom() && isShowIssueDateTo())
            {
                dql.where(exists(
                        new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "i")
                                .column(property("i.id"))
                                .where(eq(property("i", DiplomaIssuance.diplomaObject()), property("d")))
                                .where(betweenDays(DiplomaIssuance.issuanceDate().fromAlias("i"), getIssueDateFrom(), getIssueDateTo()))
                                .buildQuery()
                ));
            }

        }

        dql.order(property("d", DiplomaObject.student().person().identityCard().lastName()))
                .order(property("d", DiplomaObject.student().person().identityCard().middleName()))
                .order(property("d", DiplomaObject.student().person().identityCard().firstName()))
                .order(property("d", DiplomaObject.content().withSuccess()));
        List<DiplomaObject> diplomaObjectList = DataAccessServices.dao().getList(dql);

        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setEncoding("UTF-8");
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создаем шрифты
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);

        // шрифт для шапки отчета
        WritableCellFormat headerFormat = new WritableCellFormat(arial8bold);
        headerFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        headerFormat.setBackground(jxl.format.Colour.GRAY_25);
        headerFormat.setAlignment(jxl.format.Alignment.CENTRE);
        headerFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);

        // шрифт для строк отчета
        WritableCellFormat rowFormat = new WritableCellFormat(arial8);
        rowFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        rowFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        rowFormat.setWrap(true);

        //Заполняем отчет
        try
        {
            WritableSheet sheet = workbook.createSheet("Документы", workbook.getSheets().length);
            sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
            sheet.getSettings().setCopies(1);
            sheet.getSettings().setVerticalFreeze(1);
            for (int i = 0; i < _columnTitles.length; i++)
            {
                // задаем ширину колонки
                sheet.setColumnView(i, 15);

                // задаем название колонки
                sheet.addCell(new Label(i, 0, _columnTitles[i], headerFormat));
            }


            int excelRow = 1;
            for (DiplomaObject diplomaObject : diplomaObjectList)
            {
                String regNumberBook = "";
                String registrationNumber = "";
                String issuanceBlankNumber = "";
                String issuanceBlankSeria = "";
                String contentIssuanceBlankNumber = "";
                String contentIssuanceBlankSeria = "";
                String issuanceDate = "";
                String programQualification = "";
                String qualificationLevelCode = "";
                String excludeDate = "";
                String excludeOrderNumber = "";
                String programSubjectTitle = "";
                String programSubjectCode = "";
                String eduLevel = "";
                String topOrgUnitTitle = "";
                String territorialTitle = "";
                String replacedIssuanceRegNumber = "";
                String replacedIssuanceBlankSeria = "";
                String replacedIssuanceBlankNumber = "";

				EduProgramSubject dipProgramSubject = getEduProgramSubject(diplomaObject);

                List<DiplomaIssuance> diplomaIssuanceList = DataAccessServices.dao().getList(DiplomaIssuance.class, DiplomaIssuance.L_DIPLOMA_OBJECT, diplomaObject,
                                                                                             DiplomaIssuance.P_BLANK_SERIA, DiplomaIssuance.P_BLANK_NUMBER);
                for (DiplomaIssuance diplomaIssuance : diplomaIssuanceList)
                {
                    DiplomaIssuanceFefuExt diplomaIssuanceFefuExt = DataAccessServices.dao().getByNaturalId(new DiplomaIssuanceFefuExt.NaturalId(diplomaIssuance));
                    if (diplomaIssuanceFefuExt != null)
                    {
                        regNumberBook += regNumberBook.length() > 0 ? "\n" + diplomaIssuanceFefuExt.getRegistrationNumberBook() : diplomaIssuanceFefuExt.getRegistrationNumberBook();
                    }
                    registrationNumber += registrationNumber.length() > 0 ? "\n" + diplomaIssuance.getRegistrationNumber() : diplomaIssuance.getRegistrationNumber();

                    if (null != diplomaIssuance.getBlankNumber())
                        issuanceBlankNumber += (issuanceBlankNumber.length() > 0 ? "\n" : "") + diplomaIssuance.getBlankNumber();
                    if (null != diplomaIssuance.getBlankSeria())
                        issuanceBlankSeria += (issuanceBlankSeria.length() > 0 ? "\n" : "") + diplomaIssuance.getBlankSeria();

                    issuanceDate += issuanceDate.length() > 0 ? "\n" + DateFormatter.DEFAULT_DATE_FORMATTER.format(diplomaIssuance.getIssuanceDate()) : DateFormatter.DEFAULT_DATE_FORMATTER.format(diplomaIssuance.getIssuanceDate());

                    DiplomaIssuance dipIssuanceReplaced = diplomaIssuance.getReplacedIssuance();
                    if (null != dipIssuanceReplaced)
                    {
                        replacedIssuanceRegNumber += (replacedIssuanceRegNumber.length() > 0 ? "\n" : "") + dipIssuanceReplaced.getRegistrationNumber();
                        if (null != dipIssuanceReplaced.getBlankSeria())
                            replacedIssuanceBlankSeria += (replacedIssuanceBlankSeria.length() > 0 ? "\n" : "") + dipIssuanceReplaced.getBlankSeria();
                        if (null != dipIssuanceReplaced.getBlankNumber())
                            replacedIssuanceBlankNumber += (replacedIssuanceBlankNumber.length() > 0 ? "\n" : "") + dipIssuanceReplaced.getBlankNumber();
                    }
                }
                List<DiplomaContentIssuance> diplomaContentIssuanceList = DataAccessServices.dao().getList(DiplomaContentIssuance.class, DiplomaContentIssuance.diplomaIssuance(), diplomaIssuanceList);
                for (DiplomaContentIssuance contentIssuance : diplomaContentIssuanceList)
                {
                    contentIssuanceBlankSeria += contentIssuanceBlankSeria.length() > 0 ? "\n" + contentIssuance.getBlankSeria() : contentIssuance.getBlankSeria();

                    String formattedBlankNumber = contentIssuance.isDuplicate() ? contentIssuance.getBlankNumber() + " - Лист " + contentIssuance.getContentListNumber() + ", дубликат рег.№ "
                            + contentIssuance.getDuplicateRegustrationNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(contentIssuance.getDuplicateIssuanceDate())
                            : contentIssuance.getBlankNumber() + " - Лист " + contentIssuance.getContentListNumber();
                    contentIssuanceBlankNumber += contentIssuanceBlankNumber.length() > 0 ? "\n" + formattedBlankNumber : formattedBlankNumber;
                }

                List<EduProgramSubjectQualification> subjectQualificationList = DataAccessServices.dao().getList(EduProgramSubjectQualification.class,
                                                                                                                 EduProgramSubjectQualification.L_PROGRAM_SUBJECT, dipProgramSubject);
                for (EduProgramSubjectQualification subjectQualification : subjectQualificationList)
                {
                    programQualification = programSubjectTitle.length() > 0 ? "\n" + subjectQualification.getProgramQualification().getTitle() :
                            subjectQualification.getProgramQualification().getTitle();
                    if (subjectQualification.getProgramQualification().getCode().contains("2005"))
                    {
                        qualificationLevelCode = programSubjectCode.length() > 0 ? "\n" + subjectQualification.getProgramQualification().getQualificationLevelCode() :
                                subjectQualification.getProgramQualification().getQualificationLevelCode();
                    }

                }


                OrderData orderDate = DataAccessServices.dao().get(OrderData.class, OrderData.L_STUDENT, diplomaObject.getStudent());
                excludeDate = orderDate != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(orderDate.getExcludeOrderDate()) : "";
                excludeOrderNumber = orderDate != null ? orderDate.getExcludeOrderNumber() : "";
                if (dipProgramSubject != null)
                {
                    programSubjectTitle = dipProgramSubject.getTitle() != null ? dipProgramSubject.getTitle() : "";
                    programSubjectCode = dipProgramSubject.getSubjectCode() != null ? dipProgramSubject.getSubjectCode() : "";
                    if (dipProgramSubject.getEduProgramKind() != null)
                    {
                        eduLevel = dipProgramSubject.getEduProgramKind().getEduLevel().getTitle() != null ?
                                dipProgramSubject.getEduProgramKind().getEduLevel().getTitle() : "";
                    }
                }

                final TopOrgUnit topOrgUnit = OrgUnitManager.instance().dao().getTopOrgUnit(false);
                topOrgUnitTitle = topOrgUnit.getPrintTitle();
                territorialTitle = topOrgUnit.getTerritorialTitle().replace(".", "");

                sheet.addCell(new Label(0, excelRow, regNumberBook, rowFormat));
                sheet.addCell(new Label(1, excelRow, registrationNumber, rowFormat));
                sheet.addCell(new Label(2, excelRow, diplomaObject.getStudent().getPerson().getIdentityCard().getLastName(), rowFormat));
                sheet.addCell(new Label(3, excelRow, diplomaObject.getStudent().getPerson().getIdentityCard().getFirstName(), rowFormat));
                sheet.addCell(new Label(4, excelRow, diplomaObject.getStudent().getPerson().getIdentityCard().getMiddleName(), rowFormat));
                sheet.addCell(new Label(5, excelRow, issuanceBlankSeria, rowFormat));
                sheet.addCell(new Label(6, excelRow, issuanceBlankNumber, rowFormat));
                sheet.addCell(new Label(7, excelRow, contentIssuanceBlankSeria, rowFormat));
                sheet.addCell(new Label(8, excelRow, contentIssuanceBlankNumber, rowFormat));
                sheet.addCell(new Label(9, excelRow, issuanceDate, rowFormat));
                sheet.addCell(new Label(10, excelRow, programSubjectTitle, rowFormat));
                sheet.addCell(new Label(11, excelRow, programSubjectCode, rowFormat));
                sheet.addCell(new Label(12, excelRow, programQualification, rowFormat));
                sheet.addCell(new Label(13, excelRow, qualificationLevelCode, rowFormat));
                sheet.addCell(new Label(14, excelRow, DateFormatter.DEFAULT_DATE_FORMATTER.format(diplomaObject.getStateCommissionDate()), rowFormat));
                sheet.addCell(new Label(15, excelRow, diplomaObject.getStateCommissionProtocolNumber(), rowFormat));
                sheet.addCell(new Label(16, excelRow, excludeDate, rowFormat));
                sheet.addCell(new Label(17, excelRow, excludeOrderNumber, rowFormat));
                sheet.addCell(new Label(18, excelRow, diplomaObject.getContent().getType().getTitle(), rowFormat));
                sheet.addCell(new Label(19, excelRow, eduLevel, rowFormat));
                sheet.addCell(new Label(20, excelRow, topOrgUnitTitle, rowFormat));
                sheet.addCell(new Label(21, excelRow, territorialTitle, rowFormat));
                sheet.addCell(new Label(22, excelRow, replacedIssuanceRegNumber, rowFormat));
                sheet.addCell(new Label(23, excelRow, replacedIssuanceBlankSeria, rowFormat));
                sheet.addCell(new Label(24, excelRow, replacedIssuanceBlankNumber, rowFormat));
                excelRow++;
            }

        }
        catch (RowsExceededException e)
        {
            throw new ApplicationException("Невозможно сформировать отчет, так как объем итоговых данных превышает допустимый предел в 65536 строк.");
        }
        workbook.write();
        workbook.close();

        return out;
    }

	private EduProgramSubject getEduProgramSubject(DiplomaObject diplomaObject)
	{
		if (diplomaObject.getContent().getProgramSubject() != null)
			return diplomaObject.getContent().getProgramSubject();

		if (diplomaObject.getStudentEpv() == null)
			return null;

		EppEduPlan eduPlan = diplomaObject.getStudentEpv().getEduPlanVersion().getEduPlan();
		if (eduPlan instanceof EppEduPlanProf)
			return ((EppEduPlanProf) eduPlan).getProgramSubject();

		return null;
	}

	//getters & setters

    public EduLevel getEduLevel()
    {
        return _eduLevel;
    }

    public void setEduLevel(EduLevel eduLevel)
    {
        _eduLevel = eduLevel;
    }

    public boolean isShowEduLevel()
    {
        return _showEduLevel;
    }

    public void setShowEduLevel(boolean showEduLevel)
    {
        _showEduLevel = showEduLevel;
    }

    public DipDocumentType getDipDocumentType()
    {
        return _dipDocumentType;
    }

    public void setDipDocumentType(DipDocumentType dipDocumentType)
    {
        _dipDocumentType = dipDocumentType;
    }

    public boolean isShowDipDocType()
    {
        return _showDipDocType;
    }

    public void setShowDipDocType(boolean showDipDocType)
    {
        _showDipDocType = showDipDocType;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public boolean isShowFormativeOrgUnit()
    {
        return _showFormativeOrgUnit;
    }

    public void setShowFormativeOrgUnit(boolean showFormativeOrgUnit)
    {
        _showFormativeOrgUnit = showFormativeOrgUnit;
    }

    public List<EduProgramSubject> getProgramSubjectList()
    {
        return _programSubjectList;
    }

    public void setProgramSubjectList(List<EduProgramSubject> programSubjectList)
    {
        _programSubjectList = programSubjectList;
    }

    public boolean isShowProgramSubject()
    {
        return _showProgramSubject;
    }

    public void setShowProgramSubject(boolean showProgramSubject)
    {
        _showProgramSubject = showProgramSubject;
    }

    public Date getIssueDateFrom()
    {
        return _issueDateFrom;
    }

    public void setIssueDateFrom(Date issueDateFrom)
    {
        _issueDateFrom = issueDateFrom;
    }

    public Date getIssueDateTo()
    {
        return _issueDateTo;
    }

    public void setIssueDateTo(Date issueDateTo)
    {
        _issueDateTo = issueDateTo;
    }

    public boolean isShowIssueDateFrom()
    {
        return _showIssueDateFrom;
    }

    public void setShowIssueDateFrom(boolean showIssueDateFrom)
    {
        _showIssueDateFrom = showIssueDateFrom;
    }

    public boolean isShowIssueDateTo()
    {
        return _showIssueDateTo;
    }

    public void setShowIssueDateTo(boolean showIssueDateTo)
    {
        _showIssueDateTo = showIssueDateTo;
    }

    public boolean isDuplicate()
    {
        return _duplicate;
    }

    public void setDuplicate(boolean duplicate)
    {
        _duplicate = duplicate;
    }
}
