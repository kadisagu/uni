package ru.tandemservice.unifefu.base.ext.TrJournal.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalPrintDao;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;

import java.util.List;

/**
 * @author amakarova
 */
public interface IFefuTrJournalPrintDao extends ITrJournalPrintDao
{
    List<TrEduGroupEvent> getGroupEvents(Long trEduGroupId);

    /**
     * Печатает рейтинг группы.
     * @param journalGroupId Группа в журнале
     * @return rtf
     */
    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    byte[] printRatingGroup(Long journalGroupId);

    /**
     * Печатает рейтинговую ведомость на экзамен.
     * @param journalGroupId Группа в журнале
     * @return rtf
     */
    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    byte[] printRatingSheet(Long journalGroupId, boolean isExam);

    /**
     * Печатает итоговую рейтинговую ведомость.
     * @param journalGroupId Группа в журнале
     * @return rtf
     */
    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    byte[] printRatingSheetTotal(Long journalGroupId, boolean isExam);
}
