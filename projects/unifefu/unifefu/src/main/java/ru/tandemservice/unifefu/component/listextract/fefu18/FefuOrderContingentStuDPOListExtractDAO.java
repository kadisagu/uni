/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18;

import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public class FefuOrderContingentStuDPOListExtractDAO  extends UniBaseDao implements IExtractComponentDao<FefuOrderContingentStuDPOListExtract>
{
    /**
     * Внесение выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doCommit(FefuOrderContingentStuDPOListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setStudentStatusOld(extract.getEntity().getStatus());
        extract.getEntity().setStatus(extract.getStudentStatusNew());

        if (extract.getStudentCustomStateNew() != null)
        {
            StudentCustomState studentCustomState = new StudentCustomState();
            studentCustomState.setCustomState(extract.getStudentCustomStateNew());
            studentCustomState.setStudent(extract.getEntity());
            UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(studentCustomState);
            StudentCustomStateToExtractRelation extractRelation = new StudentCustomStateToExtractRelation();
            extractRelation.setStudentCustomState(studentCustomState);
            extractRelation.setExtract(extract);
            save(extractRelation);
        }
    }

    /**
     * Отмена вносимых выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doRollback(FefuOrderContingentStuDPOListExtract extract, Map parameters)
    {
        extract.getEntity().setStatus(extract.getStudentStatusOld());

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateToExtractRelation.class, "r").
                column(DQLExpressions.property("r", StudentCustomStateToExtractRelation.studentCustomState().id())).
                where(DQLExpressions.eq(DQLExpressions.property("r", StudentCustomStateToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(StudentCustomState.class).
                where(DQLExpressions.in(DQLExpressions.property(StudentCustomState.id()), builder.buildQuery()));
        deleteBuilder.createStatement(getSession()).execute();
    }
}