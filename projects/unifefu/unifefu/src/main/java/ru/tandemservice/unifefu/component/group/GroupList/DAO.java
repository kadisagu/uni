/* $Id$ */
package ru.tandemservice.unifefu.component.group.GroupList;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;

import java.util.Map;


/**
 * @author Ekaterina Zvereva
 * @since 26.11.2014
 */
public class DAO extends ru.tandemservice.uni.component.group.GroupList.DAO
{

    @Override
    protected void patchGroupList(DynamicListDataSource dataSource)
    {
        super.patchGroupList(dataSource);

        //получаем количество студентов в акад.отпуске для каждой группы
        Map<Long, Long> vacStudentMap = UnifefuDaoFacade.getFefuGroupDAO().getVacStudentToGroupMap();

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
        {
            Long value = vacStudentMap.get(wrapper.getId());
            wrapper.setViewProperty(Model.P_VACATION_STUDENT_COUNT, value == null ? 0 : value);
        }
    }

}
