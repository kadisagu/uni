/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.util;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import java.util.Collection;
import java.util.Comparator;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
public class FefuEpvBlockWrapper extends IdentifiableWrapper<EppEduPlanVersion>
{
    public static final String TITLE = "title";
    public static final String BLOCKS = "blocks";

    public static final Comparator<FefuEpvBlockWrapper> COMPARATOR = new Comparator<FefuEpvBlockWrapper>()
    {
        @Override
        public int compare(FefuEpvBlockWrapper o1, FefuEpvBlockWrapper o2)
        {
            int result = o1._version.getEduPlan().getNumber().compareTo(o2._version.getEduPlan().getNumber());
            if (result == 0)
            {
                result = o1._version.getNumber().compareTo(o2._version.getNumber());
            }

            return result;
        }
    };

    private EppEduPlanVersion _version;
    private Collection<String> _blocks;

    public FefuEpvBlockWrapper(EppEduPlanVersion version, Collection<String> blocks)
    {
        super(version);
        _version = version;
        _blocks = blocks;
    }

    public String getTitle(){ return _version.getFullTitle(); }
    public Collection<String> getBlocks(){ return _blocks; }
}