/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.AddCourseToRegElement;

import com.google.common.collect.Collections2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse2EppRegElementRel;
import ru.tandemservice.unifefu.ws.blackboard.course.BBCourseUtils;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.CourseVO;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 07.03.2014
 */
@Configuration
public class BlackboardAddCourseToRegElement extends BusinessComponentManager
{
    public static final String CHECKBOX_COURSE_DS = "checkboxCourseDS";
    public static final String RADIO_COURSE_DS = "radioCourseDS";
    public static final String ACTION_DS = "actionDS";
    public static final String PPS_ENTRY_DS = "ppsEntryDS";

    public static final String CHECK_COLUMN_NAME = "select";

    public static final String PPS_ENTRY_PARAM = "pps";
    public static final String REG_ELEMENT_PARAM = "regElement";
    public static final String BB_COURSE_PARAM = "bbCourse";
    public static final String BB_COURSE_ID_PARAM = "bbCourseId";

    public static final IEntity CREATE_NEW_COURSE_ITEM = new IdentifiableWrapper(1L, "Создать новый курс");
    public static final IEntity LINK_COURSE_ITEM = new IdentifiableWrapper(2L, "Привязать существующий курс");
    public static final IEntity CLONE_COURSE_ITEM = new IdentifiableWrapper(3L, "Создать новый курс на базе существующего");

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ACTION_DS, actionDSHandler()))
                .addDataSource(selectDS(PPS_ENTRY_DS, ppsEntryDSHandler()).addColumn("fio", PpsEntry.title().s()))
                .addDataSource(searchListDS(RADIO_COURSE_DS, getRadioCourseDS(), courseDSHandler()))
                .addDataSource(searchListDS(CHECKBOX_COURSE_DS, getCheckboxCourseDS(), courseDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> ppsEntryDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PpsEntry.class)
                .filter(PpsEntry.person().identityCard().fullFio())
                .order(PpsEntry.person().identityCard().lastName())
                .order(PpsEntry.person().identityCard().firstName())
                .order(PpsEntry.person().identityCard().middleName())
                .where(PpsEntry.removalDate(), (Object) null)
                .pageable(true);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> actionDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(CREATE_NEW_COURSE_ITEM, LINK_COURSE_ITEM, CLONE_COURSE_ITEM));
    }

    @Bean
    public ColumnListExtPoint getRadioCourseDS()
    {
        return columnListExtPointBuilder(RADIO_COURSE_DS)
                .addColumn(radioColumn(CHECK_COLUMN_NAME).controlInHeader(false))
                .addColumn(textColumn(DataWrapper.TITLE, DataWrapper.TITLE))
                .addColumn(textColumn(BB_COURSE_ID_PARAM, BB_COURSE_ID_PARAM))
                .create();
    }

    @Bean
    public ColumnListExtPoint getCheckboxCourseDS()
    {
        return columnListExtPointBuilder(CHECKBOX_COURSE_DS)
                .addColumn(checkboxColumn(CHECK_COLUMN_NAME))
                .addColumn(textColumn(DataWrapper.TITLE, DataWrapper.TITLE))
                .addColumn(textColumn(BB_COURSE_ID_PARAM, BB_COURSE_ID_PARAM))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final List<DataWrapper> resultList = new ArrayList<>();
                final PpsEntry ppsEntry = context.get(PPS_ENTRY_PARAM);
                final EppRegistryElement registryElement = context.get(REG_ELEMENT_PARAM);
                if (ppsEntry != null && registryElement != null)
                {
                    long id = 0;

                    // Получаем список активных курсов преподавателя и исключаем из списка уже привязанные курса
                    final Set<String> existsPrimaryIds = new HashSet<>(
                            new DQLSelectBuilder().fromEntity(BbCourse2EppRegElementRel.class, "rel")
                                    .column(property("rel", BbCourse2EppRegElementRel.bbCourse().bbPrimaryId()))
                                    .where(eq(property("rel", BbCourse2EppRegElementRel.L_EPP_REGISTRY_ELEMENT), value(registryElement)))
                                    .createStatement(context.getSession()).<String>list()
                    );
                    final List<CourseVO> courses = new ArrayList<>(Collections2.filter(BBCourseUtils.findActiveTutorCourses(ppsEntry), input1 -> !existsPrimaryIds.contains(input1.getId().getValue())));

                    // Сортируем курсы по названию
                    Collections.sort(courses, (o1, o2) -> o1.getName().getValue().compareTo(o2.getName().getValue()));

                    for (CourseVO course : courses)
                    {
                        final DataWrapper wrapper = new DataWrapper(id++, course.getName().getValue());
                        wrapper.setProperty(BB_COURSE_PARAM, course);
                        wrapper.setProperty(BB_COURSE_ID_PARAM, course.getCourseId().getValue());
                        resultList.add(wrapper);
                    }
                }
                return ListOutputBuilder.get(input, resultList).pageable(true).build();
            }
        };
    }
}