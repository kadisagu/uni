package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.unifefu.entity.ws.MdbViewAddress;
import ru.tandemservice.unifefu.entity.ws.MdbViewPerson;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Персона
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewPersonGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.MdbViewPerson";
    public static final String ENTITY_NAME = "mdbViewPerson";
    public static final int VERSION_HASH = 1650122304;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON = "person";
    public static final String P_IDENTITY_TYPE = "identityType";
    public static final String P_IDENTITY_SERIA = "identitySeria";
    public static final String P_IDENTITY_NUMBER = "identityNumber";
    public static final String P_IDENTITY_FIRST_NAME = "identityFirstName";
    public static final String P_IDENTITY_LAST_NAME = "identityLastName";
    public static final String P_IDENTITY_MIDDLE_NAME = "identityMiddleName";
    public static final String P_IDENTITY_BIRTH_DATE = "identityBirthDate";
    public static final String P_IDENTITY_BIRTH_PLACE = "identityBirthPlace";
    public static final String P_IDENTITY_SEX = "identitySex";
    public static final String P_IDENTITY_DATE = "identityDate";
    public static final String P_IDENTITY_CODE = "identityCode";
    public static final String P_IDENTITY_PLACE = "identityPlace";
    public static final String P_IDENTITY_CITIZEN = "identityCitizen";
    public static final String P_FAMILY_STATUS = "familyStatus";
    public static final String P_CHILD_COUNT = "childCount";
    public static final String P_PENSION_TYPE = "pensionType";
    public static final String P_PENSION_ISSUANCE_DATE = "pensionIssuanceDate";
    public static final String P_FLAT_PRESENCE = "flatPresence";
    public static final String P_SERVICE_LENGTH_YEARS = "serviceLengthYears";
    public static final String P_SERVICE_LENGTH_MONTHS = "serviceLengthMonths";
    public static final String P_SERVICE_LENGTH_DAYS = "serviceLengthDays";
    public static final String L_ADDRESS_REG_ID = "addressRegId";
    public static final String L_ADDRESS_FACT_ID = "addressFactId";
    public static final String L_PERSON_EDU_INSTITUTION = "personEduInstitution";
    public static final String P_INN_NUMBER = "innNumber";
    public static final String P_SNILS_NUMBER = "snilsNumber";
    public static final String P_PERSON_GUID = "personGuid";

    private Person _person;     // Персона
    private String _identityType;     // Тип удостоверения личности
    private String _identitySeria;     // Серия
    private String _identityNumber;     // Номер
    private String _identityFirstName;     // Имя
    private String _identityLastName;     // Фамилия
    private String _identityMiddleName;     // Отчество
    private Date _identityBirthDate;     // Дата рождения
    private String _identityBirthPlace;     // Место рождения
    private String _identitySex;     // Пол
    private Date _identityDate;     // Дата выдачи удостоверения
    private String _identityCode;     // Код подразделения
    private String _identityPlace;     // Кем выдано удостоверение
    private String _identityCitizen;     // Гражданство
    private String _familyStatus;     // Семейное положение
    private Integer _childCount;     // Количество детей
    private String _pensionType;     // Вид пенсии
    private Date _pensionIssuanceDate;     // Дата назначения пенсии
    private String _flatPresence;     // Обеспеченность жильем
    private int _serviceLengthYears;     // Общий стаж работы (лет)
    private int _serviceLengthMonths;     // Общий стаж работы (месяцев)
    private int _serviceLengthDays;     // Общий стаж работы (дней)
    private MdbViewAddress _addressRegId;     // Адрес
    private MdbViewAddress _addressFactId;     // Адрес
    private PersonEduInstitution _personEduInstitution;     // Документ о полученном образовании (базовый)
    private String _innNumber;     // ИНН
    private String _snilsNumber;     // СНИЛС
    private String _personGuid;     // Идентификатор ФЛ в НСИ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона. Свойство не может быть null.
     */
    @NotNull
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Персона. Свойство не может быть null.
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Тип удостоверения личности. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getIdentityType()
    {
        return _identityType;
    }

    /**
     * @param identityType Тип удостоверения личности. Свойство не может быть null.
     */
    public void setIdentityType(String identityType)
    {
        dirty(_identityType, identityType);
        _identityType = identityType;
    }

    /**
     * @return Серия.
     */
    @Length(max=255)
    public String getIdentitySeria()
    {
        return _identitySeria;
    }

    /**
     * @param identitySeria Серия.
     */
    public void setIdentitySeria(String identitySeria)
    {
        dirty(_identitySeria, identitySeria);
        _identitySeria = identitySeria;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getIdentityNumber()
    {
        return _identityNumber;
    }

    /**
     * @param identityNumber Номер.
     */
    public void setIdentityNumber(String identityNumber)
    {
        dirty(_identityNumber, identityNumber);
        _identityNumber = identityNumber;
    }

    /**
     * @return Имя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getIdentityFirstName()
    {
        return _identityFirstName;
    }

    /**
     * @param identityFirstName Имя. Свойство не может быть null.
     */
    public void setIdentityFirstName(String identityFirstName)
    {
        dirty(_identityFirstName, identityFirstName);
        _identityFirstName = identityFirstName;
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getIdentityLastName()
    {
        return _identityLastName;
    }

    /**
     * @param identityLastName Фамилия. Свойство не может быть null.
     */
    public void setIdentityLastName(String identityLastName)
    {
        dirty(_identityLastName, identityLastName);
        _identityLastName = identityLastName;
    }

    /**
     * @return Отчество.
     */
    @Length(max=255)
    public String getIdentityMiddleName()
    {
        return _identityMiddleName;
    }

    /**
     * @param identityMiddleName Отчество.
     */
    public void setIdentityMiddleName(String identityMiddleName)
    {
        dirty(_identityMiddleName, identityMiddleName);
        _identityMiddleName = identityMiddleName;
    }

    /**
     * @return Дата рождения.
     */
    public Date getIdentityBirthDate()
    {
        return _identityBirthDate;
    }

    /**
     * @param identityBirthDate Дата рождения.
     */
    public void setIdentityBirthDate(Date identityBirthDate)
    {
        dirty(_identityBirthDate, identityBirthDate);
        _identityBirthDate = identityBirthDate;
    }

    /**
     * @return Место рождения.
     */
    @Length(max=255)
    public String getIdentityBirthPlace()
    {
        return _identityBirthPlace;
    }

    /**
     * @param identityBirthPlace Место рождения.
     */
    public void setIdentityBirthPlace(String identityBirthPlace)
    {
        dirty(_identityBirthPlace, identityBirthPlace);
        _identityBirthPlace = identityBirthPlace;
    }

    /**
     * @return Пол. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getIdentitySex()
    {
        return _identitySex;
    }

    /**
     * @param identitySex Пол. Свойство не может быть null.
     */
    public void setIdentitySex(String identitySex)
    {
        dirty(_identitySex, identitySex);
        _identitySex = identitySex;
    }

    /**
     * @return Дата выдачи удостоверения.
     */
    public Date getIdentityDate()
    {
        return _identityDate;
    }

    /**
     * @param identityDate Дата выдачи удостоверения.
     */
    public void setIdentityDate(Date identityDate)
    {
        dirty(_identityDate, identityDate);
        _identityDate = identityDate;
    }

    /**
     * @return Код подразделения.
     */
    @Length(max=255)
    public String getIdentityCode()
    {
        return _identityCode;
    }

    /**
     * @param identityCode Код подразделения.
     */
    public void setIdentityCode(String identityCode)
    {
        dirty(_identityCode, identityCode);
        _identityCode = identityCode;
    }

    /**
     * @return Кем выдано удостоверение.
     */
    @Length(max=255)
    public String getIdentityPlace()
    {
        return _identityPlace;
    }

    /**
     * @param identityPlace Кем выдано удостоверение.
     */
    public void setIdentityPlace(String identityPlace)
    {
        dirty(_identityPlace, identityPlace);
        _identityPlace = identityPlace;
    }

    /**
     * @return Гражданство. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getIdentityCitizen()
    {
        return _identityCitizen;
    }

    /**
     * @param identityCitizen Гражданство. Свойство не может быть null.
     */
    public void setIdentityCitizen(String identityCitizen)
    {
        dirty(_identityCitizen, identityCitizen);
        _identityCitizen = identityCitizen;
    }

    /**
     * @return Семейное положение.
     */
    @Length(max=255)
    public String getFamilyStatus()
    {
        return _familyStatus;
    }

    /**
     * @param familyStatus Семейное положение.
     */
    public void setFamilyStatus(String familyStatus)
    {
        dirty(_familyStatus, familyStatus);
        _familyStatus = familyStatus;
    }

    /**
     * @return Количество детей.
     */
    public Integer getChildCount()
    {
        return _childCount;
    }

    /**
     * @param childCount Количество детей.
     */
    public void setChildCount(Integer childCount)
    {
        dirty(_childCount, childCount);
        _childCount = childCount;
    }

    /**
     * @return Вид пенсии.
     */
    @Length(max=255)
    public String getPensionType()
    {
        return _pensionType;
    }

    /**
     * @param pensionType Вид пенсии.
     */
    public void setPensionType(String pensionType)
    {
        dirty(_pensionType, pensionType);
        _pensionType = pensionType;
    }

    /**
     * @return Дата назначения пенсии.
     */
    public Date getPensionIssuanceDate()
    {
        return _pensionIssuanceDate;
    }

    /**
     * @param pensionIssuanceDate Дата назначения пенсии.
     */
    public void setPensionIssuanceDate(Date pensionIssuanceDate)
    {
        dirty(_pensionIssuanceDate, pensionIssuanceDate);
        _pensionIssuanceDate = pensionIssuanceDate;
    }

    /**
     * @return Обеспеченность жильем.
     */
    @Length(max=255)
    public String getFlatPresence()
    {
        return _flatPresence;
    }

    /**
     * @param flatPresence Обеспеченность жильем.
     */
    public void setFlatPresence(String flatPresence)
    {
        dirty(_flatPresence, flatPresence);
        _flatPresence = flatPresence;
    }

    /**
     * @return Общий стаж работы (лет). Свойство не может быть null.
     */
    @NotNull
    public int getServiceLengthYears()
    {
        return _serviceLengthYears;
    }

    /**
     * @param serviceLengthYears Общий стаж работы (лет). Свойство не может быть null.
     */
    public void setServiceLengthYears(int serviceLengthYears)
    {
        dirty(_serviceLengthYears, serviceLengthYears);
        _serviceLengthYears = serviceLengthYears;
    }

    /**
     * @return Общий стаж работы (месяцев). Свойство не может быть null.
     */
    @NotNull
    public int getServiceLengthMonths()
    {
        return _serviceLengthMonths;
    }

    /**
     * @param serviceLengthMonths Общий стаж работы (месяцев). Свойство не может быть null.
     */
    public void setServiceLengthMonths(int serviceLengthMonths)
    {
        dirty(_serviceLengthMonths, serviceLengthMonths);
        _serviceLengthMonths = serviceLengthMonths;
    }

    /**
     * @return Общий стаж работы (дней). Свойство не может быть null.
     */
    @NotNull
    public int getServiceLengthDays()
    {
        return _serviceLengthDays;
    }

    /**
     * @param serviceLengthDays Общий стаж работы (дней). Свойство не может быть null.
     */
    public void setServiceLengthDays(int serviceLengthDays)
    {
        dirty(_serviceLengthDays, serviceLengthDays);
        _serviceLengthDays = serviceLengthDays;
    }

    /**
     * @return Адрес.
     */
    public MdbViewAddress getAddressRegId()
    {
        return _addressRegId;
    }

    /**
     * @param addressRegId Адрес.
     */
    public void setAddressRegId(MdbViewAddress addressRegId)
    {
        dirty(_addressRegId, addressRegId);
        _addressRegId = addressRegId;
    }

    /**
     * @return Адрес.
     */
    public MdbViewAddress getAddressFactId()
    {
        return _addressFactId;
    }

    /**
     * @param addressFactId Адрес.
     */
    public void setAddressFactId(MdbViewAddress addressFactId)
    {
        dirty(_addressFactId, addressFactId);
        _addressFactId = addressFactId;
    }

    /**
     * @return Документ о полученном образовании (базовый).
     */
    public PersonEduInstitution getPersonEduInstitution()
    {
        return _personEduInstitution;
    }

    /**
     * @param personEduInstitution Документ о полученном образовании (базовый).
     */
    public void setPersonEduInstitution(PersonEduInstitution personEduInstitution)
    {
        dirty(_personEduInstitution, personEduInstitution);
        _personEduInstitution = personEduInstitution;
    }

    /**
     * @return ИНН.
     */
    @Length(max=255)
    public String getInnNumber()
    {
        return _innNumber;
    }

    /**
     * @param innNumber ИНН.
     */
    public void setInnNumber(String innNumber)
    {
        dirty(_innNumber, innNumber);
        _innNumber = innNumber;
    }

    /**
     * @return СНИЛС.
     */
    @Length(max=255)
    public String getSnilsNumber()
    {
        return _snilsNumber;
    }

    /**
     * @param snilsNumber СНИЛС.
     */
    public void setSnilsNumber(String snilsNumber)
    {
        dirty(_snilsNumber, snilsNumber);
        _snilsNumber = snilsNumber;
    }

    /**
     * @return Идентификатор ФЛ в НСИ.
     */
    @Length(max=255)
    public String getPersonGuid()
    {
        return _personGuid;
    }

    /**
     * @param personGuid Идентификатор ФЛ в НСИ.
     */
    public void setPersonGuid(String personGuid)
    {
        dirty(_personGuid, personGuid);
        _personGuid = personGuid;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewPersonGen)
        {
            setPerson(((MdbViewPerson)another).getPerson());
            setIdentityType(((MdbViewPerson)another).getIdentityType());
            setIdentitySeria(((MdbViewPerson)another).getIdentitySeria());
            setIdentityNumber(((MdbViewPerson)another).getIdentityNumber());
            setIdentityFirstName(((MdbViewPerson)another).getIdentityFirstName());
            setIdentityLastName(((MdbViewPerson)another).getIdentityLastName());
            setIdentityMiddleName(((MdbViewPerson)another).getIdentityMiddleName());
            setIdentityBirthDate(((MdbViewPerson)another).getIdentityBirthDate());
            setIdentityBirthPlace(((MdbViewPerson)another).getIdentityBirthPlace());
            setIdentitySex(((MdbViewPerson)another).getIdentitySex());
            setIdentityDate(((MdbViewPerson)another).getIdentityDate());
            setIdentityCode(((MdbViewPerson)another).getIdentityCode());
            setIdentityPlace(((MdbViewPerson)another).getIdentityPlace());
            setIdentityCitizen(((MdbViewPerson)another).getIdentityCitizen());
            setFamilyStatus(((MdbViewPerson)another).getFamilyStatus());
            setChildCount(((MdbViewPerson)another).getChildCount());
            setPensionType(((MdbViewPerson)another).getPensionType());
            setPensionIssuanceDate(((MdbViewPerson)another).getPensionIssuanceDate());
            setFlatPresence(((MdbViewPerson)another).getFlatPresence());
            setServiceLengthYears(((MdbViewPerson)another).getServiceLengthYears());
            setServiceLengthMonths(((MdbViewPerson)another).getServiceLengthMonths());
            setServiceLengthDays(((MdbViewPerson)another).getServiceLengthDays());
            setAddressRegId(((MdbViewPerson)another).getAddressRegId());
            setAddressFactId(((MdbViewPerson)another).getAddressFactId());
            setPersonEduInstitution(((MdbViewPerson)another).getPersonEduInstitution());
            setInnNumber(((MdbViewPerson)another).getInnNumber());
            setSnilsNumber(((MdbViewPerson)another).getSnilsNumber());
            setPersonGuid(((MdbViewPerson)another).getPersonGuid());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewPersonGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewPerson.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewPerson();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "person":
                    return obj.getPerson();
                case "identityType":
                    return obj.getIdentityType();
                case "identitySeria":
                    return obj.getIdentitySeria();
                case "identityNumber":
                    return obj.getIdentityNumber();
                case "identityFirstName":
                    return obj.getIdentityFirstName();
                case "identityLastName":
                    return obj.getIdentityLastName();
                case "identityMiddleName":
                    return obj.getIdentityMiddleName();
                case "identityBirthDate":
                    return obj.getIdentityBirthDate();
                case "identityBirthPlace":
                    return obj.getIdentityBirthPlace();
                case "identitySex":
                    return obj.getIdentitySex();
                case "identityDate":
                    return obj.getIdentityDate();
                case "identityCode":
                    return obj.getIdentityCode();
                case "identityPlace":
                    return obj.getIdentityPlace();
                case "identityCitizen":
                    return obj.getIdentityCitizen();
                case "familyStatus":
                    return obj.getFamilyStatus();
                case "childCount":
                    return obj.getChildCount();
                case "pensionType":
                    return obj.getPensionType();
                case "pensionIssuanceDate":
                    return obj.getPensionIssuanceDate();
                case "flatPresence":
                    return obj.getFlatPresence();
                case "serviceLengthYears":
                    return obj.getServiceLengthYears();
                case "serviceLengthMonths":
                    return obj.getServiceLengthMonths();
                case "serviceLengthDays":
                    return obj.getServiceLengthDays();
                case "addressRegId":
                    return obj.getAddressRegId();
                case "addressFactId":
                    return obj.getAddressFactId();
                case "personEduInstitution":
                    return obj.getPersonEduInstitution();
                case "innNumber":
                    return obj.getInnNumber();
                case "snilsNumber":
                    return obj.getSnilsNumber();
                case "personGuid":
                    return obj.getPersonGuid();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "identityType":
                    obj.setIdentityType((String) value);
                    return;
                case "identitySeria":
                    obj.setIdentitySeria((String) value);
                    return;
                case "identityNumber":
                    obj.setIdentityNumber((String) value);
                    return;
                case "identityFirstName":
                    obj.setIdentityFirstName((String) value);
                    return;
                case "identityLastName":
                    obj.setIdentityLastName((String) value);
                    return;
                case "identityMiddleName":
                    obj.setIdentityMiddleName((String) value);
                    return;
                case "identityBirthDate":
                    obj.setIdentityBirthDate((Date) value);
                    return;
                case "identityBirthPlace":
                    obj.setIdentityBirthPlace((String) value);
                    return;
                case "identitySex":
                    obj.setIdentitySex((String) value);
                    return;
                case "identityDate":
                    obj.setIdentityDate((Date) value);
                    return;
                case "identityCode":
                    obj.setIdentityCode((String) value);
                    return;
                case "identityPlace":
                    obj.setIdentityPlace((String) value);
                    return;
                case "identityCitizen":
                    obj.setIdentityCitizen((String) value);
                    return;
                case "familyStatus":
                    obj.setFamilyStatus((String) value);
                    return;
                case "childCount":
                    obj.setChildCount((Integer) value);
                    return;
                case "pensionType":
                    obj.setPensionType((String) value);
                    return;
                case "pensionIssuanceDate":
                    obj.setPensionIssuanceDate((Date) value);
                    return;
                case "flatPresence":
                    obj.setFlatPresence((String) value);
                    return;
                case "serviceLengthYears":
                    obj.setServiceLengthYears((Integer) value);
                    return;
                case "serviceLengthMonths":
                    obj.setServiceLengthMonths((Integer) value);
                    return;
                case "serviceLengthDays":
                    obj.setServiceLengthDays((Integer) value);
                    return;
                case "addressRegId":
                    obj.setAddressRegId((MdbViewAddress) value);
                    return;
                case "addressFactId":
                    obj.setAddressFactId((MdbViewAddress) value);
                    return;
                case "personEduInstitution":
                    obj.setPersonEduInstitution((PersonEduInstitution) value);
                    return;
                case "innNumber":
                    obj.setInnNumber((String) value);
                    return;
                case "snilsNumber":
                    obj.setSnilsNumber((String) value);
                    return;
                case "personGuid":
                    obj.setPersonGuid((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "person":
                        return true;
                case "identityType":
                        return true;
                case "identitySeria":
                        return true;
                case "identityNumber":
                        return true;
                case "identityFirstName":
                        return true;
                case "identityLastName":
                        return true;
                case "identityMiddleName":
                        return true;
                case "identityBirthDate":
                        return true;
                case "identityBirthPlace":
                        return true;
                case "identitySex":
                        return true;
                case "identityDate":
                        return true;
                case "identityCode":
                        return true;
                case "identityPlace":
                        return true;
                case "identityCitizen":
                        return true;
                case "familyStatus":
                        return true;
                case "childCount":
                        return true;
                case "pensionType":
                        return true;
                case "pensionIssuanceDate":
                        return true;
                case "flatPresence":
                        return true;
                case "serviceLengthYears":
                        return true;
                case "serviceLengthMonths":
                        return true;
                case "serviceLengthDays":
                        return true;
                case "addressRegId":
                        return true;
                case "addressFactId":
                        return true;
                case "personEduInstitution":
                        return true;
                case "innNumber":
                        return true;
                case "snilsNumber":
                        return true;
                case "personGuid":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "person":
                    return true;
                case "identityType":
                    return true;
                case "identitySeria":
                    return true;
                case "identityNumber":
                    return true;
                case "identityFirstName":
                    return true;
                case "identityLastName":
                    return true;
                case "identityMiddleName":
                    return true;
                case "identityBirthDate":
                    return true;
                case "identityBirthPlace":
                    return true;
                case "identitySex":
                    return true;
                case "identityDate":
                    return true;
                case "identityCode":
                    return true;
                case "identityPlace":
                    return true;
                case "identityCitizen":
                    return true;
                case "familyStatus":
                    return true;
                case "childCount":
                    return true;
                case "pensionType":
                    return true;
                case "pensionIssuanceDate":
                    return true;
                case "flatPresence":
                    return true;
                case "serviceLengthYears":
                    return true;
                case "serviceLengthMonths":
                    return true;
                case "serviceLengthDays":
                    return true;
                case "addressRegId":
                    return true;
                case "addressFactId":
                    return true;
                case "personEduInstitution":
                    return true;
                case "innNumber":
                    return true;
                case "snilsNumber":
                    return true;
                case "personGuid":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "person":
                    return Person.class;
                case "identityType":
                    return String.class;
                case "identitySeria":
                    return String.class;
                case "identityNumber":
                    return String.class;
                case "identityFirstName":
                    return String.class;
                case "identityLastName":
                    return String.class;
                case "identityMiddleName":
                    return String.class;
                case "identityBirthDate":
                    return Date.class;
                case "identityBirthPlace":
                    return String.class;
                case "identitySex":
                    return String.class;
                case "identityDate":
                    return Date.class;
                case "identityCode":
                    return String.class;
                case "identityPlace":
                    return String.class;
                case "identityCitizen":
                    return String.class;
                case "familyStatus":
                    return String.class;
                case "childCount":
                    return Integer.class;
                case "pensionType":
                    return String.class;
                case "pensionIssuanceDate":
                    return Date.class;
                case "flatPresence":
                    return String.class;
                case "serviceLengthYears":
                    return Integer.class;
                case "serviceLengthMonths":
                    return Integer.class;
                case "serviceLengthDays":
                    return Integer.class;
                case "addressRegId":
                    return MdbViewAddress.class;
                case "addressFactId":
                    return MdbViewAddress.class;
                case "personEduInstitution":
                    return PersonEduInstitution.class;
                case "innNumber":
                    return String.class;
                case "snilsNumber":
                    return String.class;
                case "personGuid":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewPerson> _dslPath = new Path<MdbViewPerson>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewPerson");
    }
            

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Тип удостоверения личности. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityType()
     */
    public static PropertyPath<String> identityType()
    {
        return _dslPath.identityType();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentitySeria()
     */
    public static PropertyPath<String> identitySeria()
    {
        return _dslPath.identitySeria();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityNumber()
     */
    public static PropertyPath<String> identityNumber()
    {
        return _dslPath.identityNumber();
    }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityFirstName()
     */
    public static PropertyPath<String> identityFirstName()
    {
        return _dslPath.identityFirstName();
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityLastName()
     */
    public static PropertyPath<String> identityLastName()
    {
        return _dslPath.identityLastName();
    }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityMiddleName()
     */
    public static PropertyPath<String> identityMiddleName()
    {
        return _dslPath.identityMiddleName();
    }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityBirthDate()
     */
    public static PropertyPath<Date> identityBirthDate()
    {
        return _dslPath.identityBirthDate();
    }

    /**
     * @return Место рождения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityBirthPlace()
     */
    public static PropertyPath<String> identityBirthPlace()
    {
        return _dslPath.identityBirthPlace();
    }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentitySex()
     */
    public static PropertyPath<String> identitySex()
    {
        return _dslPath.identitySex();
    }

    /**
     * @return Дата выдачи удостоверения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityDate()
     */
    public static PropertyPath<Date> identityDate()
    {
        return _dslPath.identityDate();
    }

    /**
     * @return Код подразделения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityCode()
     */
    public static PropertyPath<String> identityCode()
    {
        return _dslPath.identityCode();
    }

    /**
     * @return Кем выдано удостоверение.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityPlace()
     */
    public static PropertyPath<String> identityPlace()
    {
        return _dslPath.identityPlace();
    }

    /**
     * @return Гражданство. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityCitizen()
     */
    public static PropertyPath<String> identityCitizen()
    {
        return _dslPath.identityCitizen();
    }

    /**
     * @return Семейное положение.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getFamilyStatus()
     */
    public static PropertyPath<String> familyStatus()
    {
        return _dslPath.familyStatus();
    }

    /**
     * @return Количество детей.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getChildCount()
     */
    public static PropertyPath<Integer> childCount()
    {
        return _dslPath.childCount();
    }

    /**
     * @return Вид пенсии.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getPensionType()
     */
    public static PropertyPath<String> pensionType()
    {
        return _dslPath.pensionType();
    }

    /**
     * @return Дата назначения пенсии.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getPensionIssuanceDate()
     */
    public static PropertyPath<Date> pensionIssuanceDate()
    {
        return _dslPath.pensionIssuanceDate();
    }

    /**
     * @return Обеспеченность жильем.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getFlatPresence()
     */
    public static PropertyPath<String> flatPresence()
    {
        return _dslPath.flatPresence();
    }

    /**
     * @return Общий стаж работы (лет). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getServiceLengthYears()
     */
    public static PropertyPath<Integer> serviceLengthYears()
    {
        return _dslPath.serviceLengthYears();
    }

    /**
     * @return Общий стаж работы (месяцев). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getServiceLengthMonths()
     */
    public static PropertyPath<Integer> serviceLengthMonths()
    {
        return _dslPath.serviceLengthMonths();
    }

    /**
     * @return Общий стаж работы (дней). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getServiceLengthDays()
     */
    public static PropertyPath<Integer> serviceLengthDays()
    {
        return _dslPath.serviceLengthDays();
    }

    /**
     * @return Адрес.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getAddressRegId()
     */
    public static MdbViewAddress.Path<MdbViewAddress> addressRegId()
    {
        return _dslPath.addressRegId();
    }

    /**
     * @return Адрес.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getAddressFactId()
     */
    public static MdbViewAddress.Path<MdbViewAddress> addressFactId()
    {
        return _dslPath.addressFactId();
    }

    /**
     * @return Документ о полученном образовании (базовый).
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getPersonEduInstitution()
     */
    public static PersonEduInstitution.Path<PersonEduInstitution> personEduInstitution()
    {
        return _dslPath.personEduInstitution();
    }

    /**
     * @return ИНН.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getInnNumber()
     */
    public static PropertyPath<String> innNumber()
    {
        return _dslPath.innNumber();
    }

    /**
     * @return СНИЛС.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getSnilsNumber()
     */
    public static PropertyPath<String> snilsNumber()
    {
        return _dslPath.snilsNumber();
    }

    /**
     * @return Идентификатор ФЛ в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getPersonGuid()
     */
    public static PropertyPath<String> personGuid()
    {
        return _dslPath.personGuid();
    }

    public static class Path<E extends MdbViewPerson> extends EntityPath<E>
    {
        private Person.Path<Person> _person;
        private PropertyPath<String> _identityType;
        private PropertyPath<String> _identitySeria;
        private PropertyPath<String> _identityNumber;
        private PropertyPath<String> _identityFirstName;
        private PropertyPath<String> _identityLastName;
        private PropertyPath<String> _identityMiddleName;
        private PropertyPath<Date> _identityBirthDate;
        private PropertyPath<String> _identityBirthPlace;
        private PropertyPath<String> _identitySex;
        private PropertyPath<Date> _identityDate;
        private PropertyPath<String> _identityCode;
        private PropertyPath<String> _identityPlace;
        private PropertyPath<String> _identityCitizen;
        private PropertyPath<String> _familyStatus;
        private PropertyPath<Integer> _childCount;
        private PropertyPath<String> _pensionType;
        private PropertyPath<Date> _pensionIssuanceDate;
        private PropertyPath<String> _flatPresence;
        private PropertyPath<Integer> _serviceLengthYears;
        private PropertyPath<Integer> _serviceLengthMonths;
        private PropertyPath<Integer> _serviceLengthDays;
        private MdbViewAddress.Path<MdbViewAddress> _addressRegId;
        private MdbViewAddress.Path<MdbViewAddress> _addressFactId;
        private PersonEduInstitution.Path<PersonEduInstitution> _personEduInstitution;
        private PropertyPath<String> _innNumber;
        private PropertyPath<String> _snilsNumber;
        private PropertyPath<String> _personGuid;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Тип удостоверения личности. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityType()
     */
        public PropertyPath<String> identityType()
        {
            if(_identityType == null )
                _identityType = new PropertyPath<String>(MdbViewPersonGen.P_IDENTITY_TYPE, this);
            return _identityType;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentitySeria()
     */
        public PropertyPath<String> identitySeria()
        {
            if(_identitySeria == null )
                _identitySeria = new PropertyPath<String>(MdbViewPersonGen.P_IDENTITY_SERIA, this);
            return _identitySeria;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityNumber()
     */
        public PropertyPath<String> identityNumber()
        {
            if(_identityNumber == null )
                _identityNumber = new PropertyPath<String>(MdbViewPersonGen.P_IDENTITY_NUMBER, this);
            return _identityNumber;
        }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityFirstName()
     */
        public PropertyPath<String> identityFirstName()
        {
            if(_identityFirstName == null )
                _identityFirstName = new PropertyPath<String>(MdbViewPersonGen.P_IDENTITY_FIRST_NAME, this);
            return _identityFirstName;
        }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityLastName()
     */
        public PropertyPath<String> identityLastName()
        {
            if(_identityLastName == null )
                _identityLastName = new PropertyPath<String>(MdbViewPersonGen.P_IDENTITY_LAST_NAME, this);
            return _identityLastName;
        }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityMiddleName()
     */
        public PropertyPath<String> identityMiddleName()
        {
            if(_identityMiddleName == null )
                _identityMiddleName = new PropertyPath<String>(MdbViewPersonGen.P_IDENTITY_MIDDLE_NAME, this);
            return _identityMiddleName;
        }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityBirthDate()
     */
        public PropertyPath<Date> identityBirthDate()
        {
            if(_identityBirthDate == null )
                _identityBirthDate = new PropertyPath<Date>(MdbViewPersonGen.P_IDENTITY_BIRTH_DATE, this);
            return _identityBirthDate;
        }

    /**
     * @return Место рождения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityBirthPlace()
     */
        public PropertyPath<String> identityBirthPlace()
        {
            if(_identityBirthPlace == null )
                _identityBirthPlace = new PropertyPath<String>(MdbViewPersonGen.P_IDENTITY_BIRTH_PLACE, this);
            return _identityBirthPlace;
        }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentitySex()
     */
        public PropertyPath<String> identitySex()
        {
            if(_identitySex == null )
                _identitySex = new PropertyPath<String>(MdbViewPersonGen.P_IDENTITY_SEX, this);
            return _identitySex;
        }

    /**
     * @return Дата выдачи удостоверения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityDate()
     */
        public PropertyPath<Date> identityDate()
        {
            if(_identityDate == null )
                _identityDate = new PropertyPath<Date>(MdbViewPersonGen.P_IDENTITY_DATE, this);
            return _identityDate;
        }

    /**
     * @return Код подразделения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityCode()
     */
        public PropertyPath<String> identityCode()
        {
            if(_identityCode == null )
                _identityCode = new PropertyPath<String>(MdbViewPersonGen.P_IDENTITY_CODE, this);
            return _identityCode;
        }

    /**
     * @return Кем выдано удостоверение.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityPlace()
     */
        public PropertyPath<String> identityPlace()
        {
            if(_identityPlace == null )
                _identityPlace = new PropertyPath<String>(MdbViewPersonGen.P_IDENTITY_PLACE, this);
            return _identityPlace;
        }

    /**
     * @return Гражданство. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getIdentityCitizen()
     */
        public PropertyPath<String> identityCitizen()
        {
            if(_identityCitizen == null )
                _identityCitizen = new PropertyPath<String>(MdbViewPersonGen.P_IDENTITY_CITIZEN, this);
            return _identityCitizen;
        }

    /**
     * @return Семейное положение.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getFamilyStatus()
     */
        public PropertyPath<String> familyStatus()
        {
            if(_familyStatus == null )
                _familyStatus = new PropertyPath<String>(MdbViewPersonGen.P_FAMILY_STATUS, this);
            return _familyStatus;
        }

    /**
     * @return Количество детей.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getChildCount()
     */
        public PropertyPath<Integer> childCount()
        {
            if(_childCount == null )
                _childCount = new PropertyPath<Integer>(MdbViewPersonGen.P_CHILD_COUNT, this);
            return _childCount;
        }

    /**
     * @return Вид пенсии.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getPensionType()
     */
        public PropertyPath<String> pensionType()
        {
            if(_pensionType == null )
                _pensionType = new PropertyPath<String>(MdbViewPersonGen.P_PENSION_TYPE, this);
            return _pensionType;
        }

    /**
     * @return Дата назначения пенсии.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getPensionIssuanceDate()
     */
        public PropertyPath<Date> pensionIssuanceDate()
        {
            if(_pensionIssuanceDate == null )
                _pensionIssuanceDate = new PropertyPath<Date>(MdbViewPersonGen.P_PENSION_ISSUANCE_DATE, this);
            return _pensionIssuanceDate;
        }

    /**
     * @return Обеспеченность жильем.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getFlatPresence()
     */
        public PropertyPath<String> flatPresence()
        {
            if(_flatPresence == null )
                _flatPresence = new PropertyPath<String>(MdbViewPersonGen.P_FLAT_PRESENCE, this);
            return _flatPresence;
        }

    /**
     * @return Общий стаж работы (лет). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getServiceLengthYears()
     */
        public PropertyPath<Integer> serviceLengthYears()
        {
            if(_serviceLengthYears == null )
                _serviceLengthYears = new PropertyPath<Integer>(MdbViewPersonGen.P_SERVICE_LENGTH_YEARS, this);
            return _serviceLengthYears;
        }

    /**
     * @return Общий стаж работы (месяцев). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getServiceLengthMonths()
     */
        public PropertyPath<Integer> serviceLengthMonths()
        {
            if(_serviceLengthMonths == null )
                _serviceLengthMonths = new PropertyPath<Integer>(MdbViewPersonGen.P_SERVICE_LENGTH_MONTHS, this);
            return _serviceLengthMonths;
        }

    /**
     * @return Общий стаж работы (дней). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getServiceLengthDays()
     */
        public PropertyPath<Integer> serviceLengthDays()
        {
            if(_serviceLengthDays == null )
                _serviceLengthDays = new PropertyPath<Integer>(MdbViewPersonGen.P_SERVICE_LENGTH_DAYS, this);
            return _serviceLengthDays;
        }

    /**
     * @return Адрес.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getAddressRegId()
     */
        public MdbViewAddress.Path<MdbViewAddress> addressRegId()
        {
            if(_addressRegId == null )
                _addressRegId = new MdbViewAddress.Path<MdbViewAddress>(L_ADDRESS_REG_ID, this);
            return _addressRegId;
        }

    /**
     * @return Адрес.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getAddressFactId()
     */
        public MdbViewAddress.Path<MdbViewAddress> addressFactId()
        {
            if(_addressFactId == null )
                _addressFactId = new MdbViewAddress.Path<MdbViewAddress>(L_ADDRESS_FACT_ID, this);
            return _addressFactId;
        }

    /**
     * @return Документ о полученном образовании (базовый).
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getPersonEduInstitution()
     */
        public PersonEduInstitution.Path<PersonEduInstitution> personEduInstitution()
        {
            if(_personEduInstitution == null )
                _personEduInstitution = new PersonEduInstitution.Path<PersonEduInstitution>(L_PERSON_EDU_INSTITUTION, this);
            return _personEduInstitution;
        }

    /**
     * @return ИНН.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getInnNumber()
     */
        public PropertyPath<String> innNumber()
        {
            if(_innNumber == null )
                _innNumber = new PropertyPath<String>(MdbViewPersonGen.P_INN_NUMBER, this);
            return _innNumber;
        }

    /**
     * @return СНИЛС.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getSnilsNumber()
     */
        public PropertyPath<String> snilsNumber()
        {
            if(_snilsNumber == null )
                _snilsNumber = new PropertyPath<String>(MdbViewPersonGen.P_SNILS_NUMBER, this);
            return _snilsNumber;
        }

    /**
     * @return Идентификатор ФЛ в НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPerson#getPersonGuid()
     */
        public PropertyPath<String> personGuid()
        {
            if(_personGuid == null )
                _personGuid = new PropertyPath<String>(MdbViewPersonGen.P_PERSON_GUID, this);
            return _personGuid;
        }

        public Class getEntityClass()
        {
            return MdbViewPerson.class;
        }

        public String getEntityName()
        {
            return "mdbViewPerson";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
