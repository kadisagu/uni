package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unifefu.entity.SessionAttSheetDocument;
import ru.tandemservice.unifefu.entity.SessionAttSheetSlot;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись студента по мероприятию в документе аттестационного листа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttSheetSlotGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.SessionAttSheetSlot";
    public static final String ENTITY_NAME = "sessionAttSheetSlot";
    public static final int VERSION_HASH = -1564086861;
    private static IEntityMeta ENTITY_META;

    public static final String L_DOCUMENT = "document";
    public static final String L_STUDENT = "student";
    public static final String L_COMMISSION = "commission";

    private SessionAttSheetDocument _document;     // Документ
    private EppStudentWpeCAction _student;     // Форма контроля для сдачи по дисциплине студента
    private SessionComission _commission;     // Комиссия (назначенная)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Документ. Свойство не может быть null.
     */
    @NotNull
    public SessionAttSheetDocument getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ. Свойство не может быть null.
     */
    public void setDocument(SessionAttSheetDocument document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Форма контроля для сдачи по дисциплине студента.
     */
    public EppStudentWpeCAction getStudent()
    {
        return _student;
    }

    /**
     * @param student Форма контроля для сдачи по дисциплине студента.
     */
    public void setStudent(EppStudentWpeCAction student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Комиссия (назначенная).
     */
    public SessionComission getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия (назначенная).
     */
    public void setCommission(SessionComission commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionAttSheetSlotGen)
        {
            setDocument(((SessionAttSheetSlot)another).getDocument());
            setStudent(((SessionAttSheetSlot)another).getStudent());
            setCommission(((SessionAttSheetSlot)another).getCommission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttSheetSlotGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttSheetSlot.class;
        }

        public T newInstance()
        {
            return (T) new SessionAttSheetSlot();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "document":
                    return obj.getDocument();
                case "student":
                    return obj.getStudent();
                case "commission":
                    return obj.getCommission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "document":
                    obj.setDocument((SessionAttSheetDocument) value);
                    return;
                case "student":
                    obj.setStudent((EppStudentWpeCAction) value);
                    return;
                case "commission":
                    obj.setCommission((SessionComission) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "document":
                        return true;
                case "student":
                        return true;
                case "commission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "document":
                    return true;
                case "student":
                    return true;
                case "commission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "document":
                    return SessionAttSheetDocument.class;
                case "student":
                    return EppStudentWpeCAction.class;
                case "commission":
                    return SessionComission.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionAttSheetSlot> _dslPath = new Path<SessionAttSheetSlot>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttSheetSlot");
    }
            

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetSlot#getDocument()
     */
    public static SessionAttSheetDocument.Path<SessionAttSheetDocument> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Форма контроля для сдачи по дисциплине студента.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetSlot#getStudent()
     */
    public static EppStudentWpeCAction.Path<EppStudentWpeCAction> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Комиссия (назначенная).
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetSlot#getCommission()
     */
    public static SessionComission.Path<SessionComission> commission()
    {
        return _dslPath.commission();
    }

    public static class Path<E extends SessionAttSheetSlot> extends EntityPath<E>
    {
        private SessionAttSheetDocument.Path<SessionAttSheetDocument> _document;
        private EppStudentWpeCAction.Path<EppStudentWpeCAction> _student;
        private SessionComission.Path<SessionComission> _commission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Документ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetSlot#getDocument()
     */
        public SessionAttSheetDocument.Path<SessionAttSheetDocument> document()
        {
            if(_document == null )
                _document = new SessionAttSheetDocument.Path<SessionAttSheetDocument>(L_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Форма контроля для сдачи по дисциплине студента.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetSlot#getStudent()
     */
        public EppStudentWpeCAction.Path<EppStudentWpeCAction> student()
        {
            if(_student == null )
                _student = new EppStudentWpeCAction.Path<EppStudentWpeCAction>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Комиссия (назначенная).
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetSlot#getCommission()
     */
        public SessionComission.Path<SessionComission> commission()
        {
            if(_commission == null )
                _commission = new SessionComission.Path<SessionComission>(L_COMMISSION, this);
            return _commission;
        }

        public Class getEntityClass()
        {
            return SessionAttSheetSlot.class;
        }

        public String getEntityName()
        {
            return "sessionAttSheetSlot";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
