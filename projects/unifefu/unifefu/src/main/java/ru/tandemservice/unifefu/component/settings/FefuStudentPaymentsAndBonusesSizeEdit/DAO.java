/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.settings.FefuStudentPaymentsAndBonusesSizeEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unifefu.dao.student.FefuAcademicGrantSizeDAO;
import ru.tandemservice.unifefu.entity.FefuStudentPaymentsAndBonusesSizeSetting;

/**
 * @author Alexander Zhebko
 * @since 12.03.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        FefuStudentPaymentsAndBonusesSizeSetting setting = FefuAcademicGrantSizeDAO.getGrantSettings(this);
        if (setting != null)
        {
            model.setDefaultGrantSize(setting.getDefaultGrantSize());
            model.setDefaultGroupManagerBonusSize(setting.getDefaultGroupManagerBonusSize());
            model.setDefaultSocialGrantSize(setting.getDefaultSocialGrantSize());
        }
    }

    @Override
    public void update(Model model)
    {
        FefuStudentPaymentsAndBonusesSizeSetting setting = FefuAcademicGrantSizeDAO.getGrantSettings(this);
        if (setting == null)
            setting = new FefuStudentPaymentsAndBonusesSizeSetting();
        setting.setDefaultGrantSize(model.getDefaultGrantSize());
        setting.setDefaultGroupManagerBonusSize(model.getDefaultGroupManagerBonusSize());
        setting.setDefaultSocialGrantSize(model.getDefaultSocialGrantSize());
        saveOrUpdate(setting);
        super.update(model);
    }
}