/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleDaily.ui.LearningProcessPrintFormList;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.LearningProcessPrintFormList.SppScheduleDailyLearningProcessPrintFormList;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.LearningProcessPrintFormList.SppScheduleDailyLearningProcessPrintFormListUI;

/**
 * @author Igor Belanov
 * @since 05.09.2016
 */
public class SppScheduleDailyLearningProcessPrintFormListExtUI extends UIAddon
{
    public SppScheduleDailyLearningProcessPrintFormListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleDailyLearningProcessPrintFormList.PRINT_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(getParentPresenter().getSettings().getAsMap(true, "admin"));
        }
    }

    private SppScheduleDailyLearningProcessPrintFormListUI getParentPresenter()
    {
        return getPresenter();
    }
}
