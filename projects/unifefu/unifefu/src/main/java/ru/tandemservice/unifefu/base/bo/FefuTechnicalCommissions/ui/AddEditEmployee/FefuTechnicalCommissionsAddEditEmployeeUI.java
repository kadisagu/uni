/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.AddEditEmployee;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.FefuTechnicalCommissionsDataManager;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.View.FefuTechnicalCommissionsView;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee;

/**
 * @author Nikolay Fedorovskih
 * @since 11.06.2013
 */
@Input({
               @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "technicalCommissionId"),
               @Bind(key = FefuTechnicalCommissionsAddEditEmployeeUI.TC_EMPLOYEE_ID, binding = "tcEmployeeId")
       })
public class FefuTechnicalCommissionsAddEditEmployeeUI extends UIPresenter
{
    public static final String TC_EMPLOYEE_ID = "employeeID";

    private Long technicalCommissionId;
    private Long tcEmployeeId;
    private FefuTechnicalCommission technicalCommission;
    private FefuTechnicalCommissionEmployee tcEmployee;
    private boolean isAddForm;

    @Override
    public void onComponentRefresh()
    {
        isAddForm = getTechnicalCommissionId() != null && getTcEmployeeId() == null;
        if (isAddForm)
        {
            technicalCommission = DataAccessServices.dao().getNotNull(getTechnicalCommissionId());
            setTcEmployee(new FefuTechnicalCommissionEmployee());
            getTcEmployee().setTechnicalCommission(technicalCommission);
            getTcEmployee().setAccessLimitation(true);
        }
        else
        {
            setTcEmployee((FefuTechnicalCommissionEmployee) DataAccessServices.dao().getNotNull(getTcEmployeeId()));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuTechnicalCommissionsAddEditEmployee.EMPLOYEE_COMBO_DS.equals(dataSource.getName()))
            dataSource.put(FefuTechnicalCommissionsView.TECHNICAL_COMMISSION_PARAM, getTechnicalCommission());
    }

    public void onClickApply()
    {
        FefuTechnicalCommissionsDataManager.instance().dao().saveEmployeeFromTechnicalCommission(getTcEmployee());
        deactivate();
    }

    public Long getTechnicalCommissionId()
    {
        return technicalCommissionId;
    }

    public void setTechnicalCommissionId(Long technicalCommissionId)
    {
        this.technicalCommissionId = technicalCommissionId;
    }

    public FefuTechnicalCommission getTechnicalCommission()
    {
        return technicalCommission;
    }

    public Long getTcEmployeeId()
    {
        return tcEmployeeId;
    }

    public void setTcEmployeeId(Long tcEmployeeId)
    {
        this.tcEmployeeId = tcEmployeeId;
    }

    public boolean getAddForm()
    {
        return isAddForm;
    }

    public FefuTechnicalCommissionEmployee getTcEmployee()
    {
        return tcEmployee;
    }

    public void setTcEmployee(FefuTechnicalCommissionEmployee tcEmployee)
    {
        this.tcEmployee = tcEmployee;
    }
}