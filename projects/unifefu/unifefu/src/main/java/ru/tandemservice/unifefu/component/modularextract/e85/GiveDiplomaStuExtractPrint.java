/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e85;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;

/**
 * @author Ekaterina Zvereva
 * @since 22.06.2015
 */
public class GiveDiplomaStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e85.GiveDiplomaStuExtractPrint
{
    /**
     * Добавлено временно
     */
    @Override
    protected void printQualification(GiveDiplomaStuExtract extract, RtfInjectModifier modifier, EducationLevelsHighSchool eduHs)
    {

        EducationLevels eduLevel = eduHs.getEducationLevel();
        boolean isGos2 = eduLevel.getLevelType().isGos2();
        String qualificationCode = eduLevel.getSafeQCode();
        String assignedQualificationTitle = eduHs.getAssignedQualification() != null ? eduHs.getAssignedQualification().getTitle().toUpperCase() : "______________________________";
        StringBuilder diplomaQualificationAward = new StringBuilder();
        switch (qualificationCode)
        {
            case QualificationsCodes.BAKALAVR:
            case QualificationsCodes.MAGISTR:
            {
                modifier.put("graduate_A", "степень");
                modifier.put("graduate_G", "степени");
                modifier.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);
                modifier.put("eduLevelTypeStr_P", CommonExtractPrint.getEduLevelTypeStr_P(eduLevel));

                if (extract.isPrintDiplomaQualification() && !isGos2)
                {
                    diplomaQualificationAward.append(" с присвоением специального звания ");
                    diplomaQualificationAward.append(assignedQualificationTitle);
                }
                break;
            }
            case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
            case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
            case QualificationsCodes.SPETSIALIST:
            {
                modifier.put("graduate_A", "квалификацию");
                modifier.put("graduate_G", "квалификации");
                modifier.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);
                modifier.put("eduLevelTypeStr_P", CommonExtractPrint.getEduLevelTypeStr_P(eduLevel));
                break;
            }
            default:
            {
                modifier.put("graduate_A", "__________");
                modifier.put("graduate_G", "__________");
                modifier.put("graduateWithLevel_A", "______________________________");
                modifier.put("eduLevelTypeStr_P", "__________");
            }
        }
        modifier.put("diplomaName_G", getDiplomaName_G(qualificationCode));
        modifier.put("diplomaQualificationAward", diplomaQualificationAward.toString());
    }
}