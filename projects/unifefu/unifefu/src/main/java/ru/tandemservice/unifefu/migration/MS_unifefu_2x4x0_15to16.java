package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x0_15to16 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuBrsAttestationResultsReport
        if (tool.tableExists("ffbrsattsttnrsltsrprt_t"))
        {

            // удалено свойство checkExamAndOffset
            {
                // удалить колонку
                tool.dropColumn("ffbrsattsttnrsltsrprt_t", "checkexamandoffset_p");

            }

            if (!tool.columnExists("ffbrsattsttnrsltsrprt_t", "excludeactiontypes_p"))
            // создано свойство excludeActionTypes
            {
                // создать колонку
                tool.createColumn("ffbrsattsttnrsltsrprt_t", new DBColumn("excludeactiontypes_p", DBType.createVarchar(255)));

            }
        }

    }
}