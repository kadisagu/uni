/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EcOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.IEcOrderDao;
import ru.tandemservice.unifefu.base.ext.EcOrder.logic.FefuExtEcOrderDAO;

/**
 * @author Nikolay Fedorovskih
 * @since 29.07.2013
 */
@Configuration
public class EcOrderExrManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEcOrderDao dao()
    {
        return new FefuExtEcOrderDAO();
    }
}