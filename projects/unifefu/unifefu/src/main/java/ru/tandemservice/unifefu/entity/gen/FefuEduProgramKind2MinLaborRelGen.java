package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.unifefu.entity.FefuEduProgramKind2MinLaborRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь вида образовательной программы c минимальной трудоемкостью дисциплины (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEduProgramKind2MinLaborRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEduProgramKind2MinLaborRel";
    public static final String ENTITY_NAME = "fefuEduProgramKind2MinLaborRel";
    public static final int VERSION_HASH = 864770026;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PROGRAM_KIND = "eduProgramKind";
    public static final String P_LABOR = "labor";

    private EduProgramKind _eduProgramKind;     // Вид образовательной программы
    private long _labor;     // Минимальная трудоемкость

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид образовательной программы. Свойство не может быть null.
     */
    @NotNull
    public EduProgramKind getEduProgramKind()
    {
        return _eduProgramKind;
    }

    /**
     * @param eduProgramKind Вид образовательной программы. Свойство не может быть null.
     */
    public void setEduProgramKind(EduProgramKind eduProgramKind)
    {
        dirty(_eduProgramKind, eduProgramKind);
        _eduProgramKind = eduProgramKind;
    }

    /**
     * @return Минимальная трудоемкость. Свойство не может быть null.
     */
    @NotNull
    public long getLabor()
    {
        return _labor;
    }

    /**
     * @param labor Минимальная трудоемкость. Свойство не может быть null.
     */
    public void setLabor(long labor)
    {
        dirty(_labor, labor);
        _labor = labor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEduProgramKind2MinLaborRelGen)
        {
            setEduProgramKind(((FefuEduProgramKind2MinLaborRel)another).getEduProgramKind());
            setLabor(((FefuEduProgramKind2MinLaborRel)another).getLabor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEduProgramKind2MinLaborRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEduProgramKind2MinLaborRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuEduProgramKind2MinLaborRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduProgramKind":
                    return obj.getEduProgramKind();
                case "labor":
                    return obj.getLabor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduProgramKind":
                    obj.setEduProgramKind((EduProgramKind) value);
                    return;
                case "labor":
                    obj.setLabor((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduProgramKind":
                        return true;
                case "labor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduProgramKind":
                    return true;
                case "labor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduProgramKind":
                    return EduProgramKind.class;
                case "labor":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEduProgramKind2MinLaborRel> _dslPath = new Path<FefuEduProgramKind2MinLaborRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEduProgramKind2MinLaborRel");
    }
            

    /**
     * @return Вид образовательной программы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduProgramKind2MinLaborRel#getEduProgramKind()
     */
    public static EduProgramKind.Path<EduProgramKind> eduProgramKind()
    {
        return _dslPath.eduProgramKind();
    }

    /**
     * @return Минимальная трудоемкость. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduProgramKind2MinLaborRel#getLabor()
     */
    public static PropertyPath<Long> labor()
    {
        return _dslPath.labor();
    }

    public static class Path<E extends FefuEduProgramKind2MinLaborRel> extends EntityPath<E>
    {
        private EduProgramKind.Path<EduProgramKind> _eduProgramKind;
        private PropertyPath<Long> _labor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид образовательной программы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduProgramKind2MinLaborRel#getEduProgramKind()
     */
        public EduProgramKind.Path<EduProgramKind> eduProgramKind()
        {
            if(_eduProgramKind == null )
                _eduProgramKind = new EduProgramKind.Path<EduProgramKind>(L_EDU_PROGRAM_KIND, this);
            return _eduProgramKind;
        }

    /**
     * @return Минимальная трудоемкость. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduProgramKind2MinLaborRel#getLabor()
     */
        public PropertyPath<Long> labor()
        {
            if(_labor == null )
                _labor = new PropertyPath<Long>(FefuEduProgramKind2MinLaborRelGen.P_LABOR, this);
            return _labor;
        }

        public Class getEntityClass()
        {
            return FefuEduProgramKind2MinLaborRel.class;
        }

        public String getEntityName()
        {
            return "fefuEduProgramKind2MinLaborRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
