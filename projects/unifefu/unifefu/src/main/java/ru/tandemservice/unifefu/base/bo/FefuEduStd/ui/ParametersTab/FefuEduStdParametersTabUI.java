/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ParametersTab;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.FefuEduStdManager;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.CompetenceAdd.FefuEduStdCompetenceAdd;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.CompetenceAdd.FefuEduStdCompetenceAddUI;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ControlActionAddEdit.FefuEduStdControlActionAddEdit;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ControlActionAddEdit.FefuEduStdControlActionAddEditUI;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.LaborBlockRowAdd.FefuEduStdLaborBlockRowAdd;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.LaborBlockRowAdd.FefuEduStdLaborBlockRowAddUI;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.LaborBlockRowEdit.FefuEduStdLaborBlockRowEdit;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ParametersAddEdit.FefuEduStdParametersAddEdit;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ParametersAdditChecksAddEdit.FefuEduStdParametersAdditChecksAddEdit;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 01.12.2014
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "eduStandard.id")})
public class FefuEduStdParametersTabUI extends UIPresenter
{
    public static final String EDU_STANDARD_ID = "eduStandardId";

    private EppStateEduStandard _eduStandard = new EppStateEduStandard();
    private FefuEppStateEduStandardParameters _parameter;
    private FefuEppStateEduStandardAdditionalChecks _additionalChecks;

    private List<EduProgramSpecialization> _specializationList;
    private EduProgramSpecialization _currentSpecialization;

    private List<EduProgramQualification> _qualificationList;
    private EduProgramQualification _currentQualification;

    private DataWrapper _currentCompetenceRow;

    private Map<EduProgramQualification, List<FefuEppStateEduStandardLaborBlocks>> _blockTreeMap = Maps.newHashMap();
    private FefuEppStateEduStandardLaborBlocks _currentBlockRow;
    private Set<Long> _selectedBlockRowIds = new HashSet<>();

    @Override
    public void onComponentRefresh()
    {
        _eduStandard = DataAccessServices.dao().getNotNull(_eduStandard.getId());
        _parameter = DataAccessServices.dao().getByNaturalId(new FefuEppStateEduStandardParameters.NaturalId(_eduStandard));
        _additionalChecks = DataAccessServices.dao().getByNaturalId(new FefuEppStateEduStandardAdditionalChecks.NaturalId(_eduStandard));

        DQLSelectBuilder qualificationBuilder = new DQLSelectBuilder().fromEntity(EduProgramSubjectQualification.class, "sq")
                .column(property("sq", EduProgramSubjectQualification.programQualification()))
                .where(eq(property("sq", EduProgramSubjectQualification.programSubject()), value(_eduStandard.getProgramSubject())));

        _specializationList = CommonBaseUtil.getPropertiesList(DataAccessServices.dao().getList(EppStateEduStandardBlock.class, EppStateEduStandardBlock.stateEduStandard(), _eduStandard, EppStateEduStandardBlock.programSpecialization().title().s()), EppStateEduStandardBlock.L_PROGRAM_SPECIALIZATION);
        _qualificationList = DataAccessServices.dao().getList(qualificationBuilder);

        List<FefuEppStateEduStandardLaborBlocks> blockList = DataAccessServices.dao().getList(FefuEppStateEduStandardLaborBlocks.class, FefuEppStateEduStandardLaborBlocks.eduStandard(), _eduStandard);

        _blockTreeMap.clear();
        _selectedBlockRowIds.clear();
        for (FefuEppStateEduStandardLaborBlocks block : blockList)
        {
            EduProgramQualification qualification = block.getQualification();
            if (!_blockTreeMap.containsKey(qualification)) _blockTreeMap.put(qualification, Lists.<FefuEppStateEduStandardLaborBlocks>newArrayList());
            _blockTreeMap.get(qualification).add(block);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuEduStdParametersTab.COMPETENCE_DS.equals(dataSource.getName()))
        {
            dataSource.put(EDU_STANDARD_ID, _eduStandard.getId());
        }
        else if(FefuEduStdParametersTab.CONTROL_ACTION_DS.equals(dataSource.getName()))
        {
            dataSource.put(EDU_STANDARD_ID, _eduStandard.getId());
        }
    }

    // Listeners

    public void onClickAddEduStdParameters()
    {
        _uiActivation.asCurrent(FefuEduStdParametersAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, _eduStandard.getId()).activate();
    }

    public void onClickAddCompetence()
    {
        _uiActivation.asCurrent(FefuEduStdCompetenceAdd.class)
                .parameter(UIPresenter.PUBLISHER_ID, _eduStandard.getId())
                .parameter(FefuEduStdCompetenceAddUI.SPECIALIZATION_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickCompetenceNumberUp()
    {
        FefuEduStdManager.instance().dao().doChangeCompetencePriorityUp(getListenerParameterAsLong());
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickCompetenceNumberDown()
    {
        FefuEduStdManager.instance().dao().doChangeCompetencePriorityDown(getListenerParameterAsLong());
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickDeleteCompetence()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        FefuEduStdManager.instance().dao().doReMoveFefuCompetence(_eduStandard, null);
    }

    public void onClickAddLaborBlockRow()
    {
        _uiActivation.asRegionDialog(FefuEduStdLaborBlockRowAdd.class)
                .parameter(FefuEduStdLaborBlockRowAddUI.EDU_STANDARD_ID, _eduStandard.getId())
                .parameter(FefuEduStdLaborBlockRowAddUI.QUALIFICATION_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickEditBlockRow()
    {
        _uiActivation.asRegionDialog(FefuEduStdLaborBlockRowEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onClickDeleteLaborBlockRow()
    {
        final Collection<Long> rowIds = getSelectedBlockRowIds();
        if (rowIds.isEmpty())
        {
            ContextLocal.getErrorCollector().add("Не выбрано ни одного элемента.");
            return;
        }
        Iterator<Long> iterator = rowIds.iterator(); iterator.hasNext();
        EduProgramQualification qualification = ((FefuEppStateEduStandardLaborBlocks) DataAccessServices.dao().getNotNull(iterator.next())).getQualification();
        final List<FefuEppStateEduStandardLaborBlocks> blocksList = _blockTreeMap.get(qualification);

        DataAccessServices.dao().doInTransaction(session -> {
            Collections.reverse(blocksList);

            // удаляем в обратном порядке
            for (FefuEppStateEduStandardLaborBlocks row : blocksList)
            {
                FefuEppStateEduStandardLaborBlocks tmp = row;
                while (null != tmp)
                {
                    if (rowIds.contains(tmp.getId()))
                    {
                        session.delete(row);
                        break;
                    }
                    tmp = (FefuEppStateEduStandardLaborBlocks) tmp.getHierarhyParent();
                }
            }
            return null;
        });
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickUpCurrentCompetenceRow()
    {
        FefuEduStdManager.instance().dao().doChangeCompetencePriorityUp(getListenerParameterAsLong());
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickDownCurrentCompetenceRow()
    {
        FefuEduStdManager.instance().dao().doChangeCompetencePriorityDown(getListenerParameterAsLong());
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickDeleteCurrentCompetenceRow()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        FefuEduStdManager.instance().dao().doReMoveFefuCompetence(_eduStandard, getCurrentSpecialization());
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickAddControlAction()
    {
        _uiActivation.asRegionDialog(FefuEduStdControlActionAddEdit.class).parameter(FefuEduStdControlActionAddEditUI.EDU_STANDARD_ID, _eduStandard.getId()).activate();
    }

    public void onClickEditControlAction()
    {
        _uiActivation.asRegionDialog(FefuEduStdControlActionAddEdit.class)
                .parameter(FefuEduStdControlActionAddEditUI.EDU_STANDARD_ID, _eduStandard.getId())
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteControlAction()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickAddEduStdParametersAdditionalChecks()
    {
        _uiActivation.asRegionDialog(FefuEduStdParametersAdditChecksAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, _additionalChecks.getId()).activate();
    }

    // Getters & Setters

    public String getCurrentCompetenceRowCode()
    {
        return (String) getCurrentCompetenceRow().getProperty(FefuEduStdParametersTab.COMPETENCE_CODE);
    }

    public String getCurrentCompetenceRowTitle()
    {
        return (String) getCurrentCompetenceRow().getProperty(FefuEduStdParametersTab.COMPETENCE_TITLE);
    }

    @SuppressWarnings("unchecked")
    public Collection<IEppEpvRowWrapper> getBlockRowList()
    {
        return (Collection) Arrays.asList(getTree().getFlatTreeObjects());
    }

    public int getBlockRowHierarchyLevel()
    {
        return getTree().getLevel(getCurrentBlockRow());
    }

    public boolean isBlockRowLeaf()
    {
        return !getTree().hasChildren(getCurrentBlockRow());
    }

    public boolean isBlockRowSelected()
    {
        return getSelectedBlockRowIds().contains(getCurrentBlockRow().getId());
    }

    public String getBlockRowFullTitle()
    {
        return getCurrentBlockRow().getFullTitle();
    }

    public int getBlockRowMinNumber()
    {
        return getCurrentBlockRow().getMinNumber();
    }

    public int getBlockRowMaxNumber()
    {
        return getCurrentBlockRow().getMaxNumber();
    }

    public void setBlockRowSelected(boolean rowSelected)
    {
        if (rowSelected)
            getSelectedBlockRowIds().add(getCurrentBlockRow().getId());
        else
            getSelectedBlockRowIds().remove(getCurrentBlockRow().getId());
    }

    public EppStateEduStandard getEduStandard()
    {
        return _eduStandard;
    }

    public void setEduStandard(EppStateEduStandard eduStandard)
    {
        _eduStandard = eduStandard;
    }

    public FefuEppStateEduStandardParameters getParameter()
    {
        return _parameter;
    }

    public void setParameter(FefuEppStateEduStandardParameters parameter)
    {
        _parameter = parameter;
    }

    public FefuEppStateEduStandardAdditionalChecks getAdditionalChecks()
    {
        return _additionalChecks;
    }

    public void setAdditionalChecks(FefuEppStateEduStandardAdditionalChecks additionalChecks)
    {
        _additionalChecks = additionalChecks;
    }

    public List<EduProgramSpecialization> getSpecializationList()
    {
        return _specializationList;
    }

    public void setSpecializationList(List<EduProgramSpecialization> specializationList)
    {
        _specializationList = specializationList;
    }

    public EduProgramSpecialization getCurrentSpecialization()
    {
        return _currentSpecialization;
    }

    public void setCurrentSpecialization(EduProgramSpecialization currentSpecialization)
    {
        _currentSpecialization = currentSpecialization;
    }

    public DataWrapper getCurrentCompetenceRow()
    {
        return _currentCompetenceRow;
    }

    public void setCurrentCompetenceRow(DataWrapper currentCompetenceRow)
    {
        _currentCompetenceRow = currentCompetenceRow;
    }

    public EduProgramQualification getCurrentQualification()
    {
        return _currentQualification;
    }

    public void setCurrentQualification(EduProgramQualification currentQualification)
    {
        _currentQualification = currentQualification;
    }

    public List<EduProgramQualification> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<EduProgramQualification> qualificationList)
    {
        _qualificationList = qualificationList;
    }


    @SuppressWarnings("unchecked")
    public PlaneTree getTree()
    {
        List<FefuEppStateEduStandardLaborBlocks> rows = _blockTreeMap.get(getCurrentQualification());
        if (null == rows) rows = Collections.emptyList();
        else Collections.sort(rows, LABOR_BLOCK_COMPARATOR);

        return new PlaneTree((Collection) rows);
    }

    public FefuEppStateEduStandardLaborBlocks getCurrentBlockRow()
    {
        return _currentBlockRow;
    }

    public void setCurrentBlockRow(FefuEppStateEduStandardLaborBlocks currentBlockRow)
    {
        _currentBlockRow = currentBlockRow;
    }

    public Set<Long> getSelectedBlockRowIds()
    {
        return _selectedBlockRowIds;
    }

    public void setSelectedBlockRowIds(Set<Long> selectedBlockRowIds)
    {
        _selectedBlockRowIds = selectedBlockRowIds;
    }

    public static Comparator<FefuEppStateEduStandardLaborBlocks> LABOR_BLOCK_COMPARATOR = new Comparator<FefuEppStateEduStandardLaborBlocks>()
    {
        @Override
        public int compare(final FefuEppStateEduStandardLaborBlocks b1, final FefuEppStateEduStandardLaborBlocks b2)
        {
            return compare(b1.getBlock().getPriorityTrace().iterator(), b2.getBlock().getPriorityTrace().iterator());
        }

        private <T extends Comparable<T>> int compare(final Iterator<T> i1, final Iterator<T> i2)
        {
            while (i1.hasNext() && i2.hasNext())
            {
                final int result = i1.next().compareTo(i2.next());
                if (result != 0) return result;
            }
            if (i1.hasNext()) return 1;
            if (i2.hasNext()) return -1;
            return 0;
        }
    };
}
