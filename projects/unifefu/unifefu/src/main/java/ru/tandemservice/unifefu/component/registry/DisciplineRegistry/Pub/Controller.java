/* $Id$ */
package ru.tandemservice.unifefu.component.registry.DisciplineRegistry.Pub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;

/**
 * @author Alexey Lopatin
 * @since 24.11.2014
 */
public class Controller extends ru.tandemservice.uniepp.component.registry.DisciplineRegistry.Pub.Controller
{
    @Override
    public void onClickEditElement(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        String name = StringUtils.removeEnd(model.getClass().getPackage().getName(), ".Pub").replace("unifefu", "uniepp");
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(name + ".AddEdit", model.getParameters4Edit()));
    }
}
