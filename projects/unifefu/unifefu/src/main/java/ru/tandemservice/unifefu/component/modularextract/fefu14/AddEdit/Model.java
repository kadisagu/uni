/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu14.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.e41.utils.EmployeePostVO;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unifefu.entity.SendPracticInnerStuExtract;

import java.util.List;

/**
 * @author nvankov
 * @since 6/20/13
 */
public class Model extends CommonModularStudentExtractAddEditModel<SendPracticInnerStuExtract>
{
    private EmployeePostVO _preventAccidentsIC;
    private EmployeePostVO _responsForRecieveCash;
    private EmployeePostVO _practiceHeader;
    private String practiceType;
    private String practiceKind;

    private List<Course> _courseList;
    private List<String> practiceTypeList;
    private ISelectModel practiceKindModel;
    private ISelectModel _employeePostVOModel;
    private ISelectModel _orgUnitModel;

    public EmployeePostVO getPreventAccidentsIC()
    {
        return _preventAccidentsIC;
    }

    public void setPreventAccidentsIC(EmployeePostVO preventAccidentsIC)
    {
        _preventAccidentsIC = preventAccidentsIC;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public String getPracticeType()
    {
        return practiceType;
    }

    public void setPracticeType(String practiceType)
    {
        this.practiceType = practiceType;
    }

    public String getPracticeKind()
    {
        return practiceKind;
    }

    public void setPracticeKind(String practiceKind)
    {
        this.practiceKind = practiceKind;
    }

    public List<String> getPracticeTypeList()
    {
        return practiceTypeList;
    }

    public void setPracticeTypeList(List<String> practiceTypeList)
    {
        this.practiceTypeList = practiceTypeList;
    }

    public ISelectModel getPracticeKindModel()
    {
        return practiceKindModel;
    }

    public void setPracticeKindModel(ISelectModel practiceKindModel)
    {
        this.practiceKindModel = practiceKindModel;
    }

    public ISelectModel getEmployeePostVOModel()
    {
        return _employeePostVOModel;
    }

    public void setEmployeePostVOModel(ISelectModel employeePostVOModel)
    {
        _employeePostVOModel = employeePostVOModel;
    }

    public EmployeePostVO getPracticeHeader()
    {
        return _practiceHeader;
    }

    public void setPracticeHeader(EmployeePostVO practiceHeader)
    {
        _practiceHeader = practiceHeader;
    }

    public ISelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public EmployeePostVO getResponsForRecieveCash()
    {
        return _responsForRecieveCash;
    }

    public void setResponsForRecieveCash(EmployeePostVO responsForRecieveCash)
    {
        _responsForRecieveCash = responsForRecieveCash;
    }
}
