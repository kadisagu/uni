/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.FefuTechnicalCommissionsDataManager;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.AddEdit.FefuTechnicalCommissionsAddEdit;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.View.FefuTechnicalCommissionsView;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

/**
 * @author Nikolay Fedorovskih
 * @since 11.06.2013
 */
public class FefuTechnicalCommissionsListUI extends UIPresenter
{
    private static final String ENROLLMENT_CAMPAIGN_FILTER = "enrollmentCampaign";

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuTechnicalCommissionsList.TECHNICAL_COMMISSIONS_DS.equals(dataSource.getName()))
            dataSource.put(FefuTechnicalCommissionsList.ENROLLMENT_CAMPAIGN_PARAM, getEnrollmentCampaign());
    }

    @Override
    public void onComponentRefresh()
    {
        initDefaults();
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _uiSettings.get(ENROLLMENT_CAMPAIGN_FILTER);
    }

    public void onClickAdd()
    {
        EnrollmentCampaign enrollmentCampaign = getEnrollmentCampaign();
        if (enrollmentCampaign == null)
            throw new ApplicationException(_uiConfig.getPropertySafe("action.add.needEnrollmentCampaign"));
        _uiActivation.asDesktopRoot(FefuTechnicalCommissionsAddEdit.class)
                .parameter(FefuTechnicalCommissionsList.ENROLLMENT_CAMPAIGN_PARAM, enrollmentCampaign.getId())
                .activate();
    }

    public void onClickView()
    {
        _uiActivation.asDesktopRoot(FefuTechnicalCommissionsView.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    private void initDefaults()
    {
        if (getEnrollmentCampaign() == null)
        {
            _uiSettings.set(ENROLLMENT_CAMPAIGN_FILTER, UnifefuDaoFacade.getFefuEntrantDAO().getLastEnrollmentCampaign());
        }
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asDesktopRoot(FefuTechnicalCommissionsAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        FefuTechnicalCommission technicalCommission = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        FefuTechnicalCommissionsDataManager.instance().dao().delete(technicalCommission);
    }
}