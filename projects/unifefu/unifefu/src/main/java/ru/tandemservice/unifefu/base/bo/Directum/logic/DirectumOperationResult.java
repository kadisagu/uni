/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.logic;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;

/**
 * @author Dmitry Seleznev
 * @since 22.01.2014
 */
public class DirectumOperationResult
{
    private AbstractStudentOrder _studentOrder;
    private EnrollmentOrder _entrantOrder;
    private String _operationType;
    private String _operationResult;
    private String _directumOrderId;
    private String _directumTaskId;
    private String _datagram;
    private String _wsResult;

    public DirectumOperationResult(AbstractStudentOrder studentOrder, EnrollmentOrder entrantOrder, String operationType, String operationResult, String directumOrderId, String directumTaskId, String datagram, String wsResult)
    {
        _studentOrder = studentOrder;
        _entrantOrder = entrantOrder;
        _operationType = operationType;
        _operationResult = StringUtils.substring(operationResult, 0, 1022);
        _directumOrderId = directumOrderId;
        _directumTaskId = directumTaskId;
        _datagram = datagram;
        _wsResult = wsResult;
    }

    public AbstractStudentOrder getStudentOrder()
    {
        return _studentOrder;
    }

    public void setStudentOrder(AbstractStudentOrder studentOrder)
    {
        _studentOrder = studentOrder;
    }

    public EnrollmentOrder getEntrantOrder()
    {
        return _entrantOrder;
    }

    public void setEntrantOrder(EnrollmentOrder entrantOrder)
    {
        _entrantOrder = entrantOrder;
    }

    public String getOperationType()
    {
        return _operationType;
    }

    public void setOperationType(String operationType)
    {
        _operationType = operationType;
    }

    public String getOperationResult()
    {
        return _operationResult;
    }

    public void setOperationResult(String operationResult)
    {
        _operationResult = StringUtils.substring(operationResult, 0, 1022);
    }

    public String getDirectumOrderId()
    {
        return _directumOrderId;
    }

    public void setDirectumOrderId(String directumOrderId)
    {
        _directumOrderId = directumOrderId;
    }

    public String getDirectumTaskId()
    {
        return _directumTaskId;
    }

    public void setDirectumTaskId(String directumTaskId)
    {
        _directumTaskId = directumTaskId;
    }

    public String getDatagram()
    {
        return _datagram;
    }

    public void setDatagram(String datagram)
    {
        _datagram = datagram;
    }

    public String getWsResult()
    {
        return _wsResult;
    }

    public void setWsResult(String wsResult)
    {
        _wsResult = wsResult;
    }
}