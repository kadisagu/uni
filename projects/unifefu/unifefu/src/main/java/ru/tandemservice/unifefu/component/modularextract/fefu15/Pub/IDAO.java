/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu15.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract;

/**
 * @author nvankov
 * @since 11/15/13
 */
public interface IDAO extends IModularStudentExtractPubDAO<FullStateMaintenanceEnrollmentStuExtract, Model>
{
}
