/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d4.Add;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.component.documents.d4.Add.Model;

/**
 * @author Nikolay Fedorovskih
 * @since 11.02.2014
 */
public class PrintBean extends ru.tandemservice.uni.component.documents.d4.Add.PrintBean
{
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier modifier = super.createInjectModifier(model);
        modifier.put("levelHighSchool", CommonExtractPrint.getFefuHighEduLevelStr(model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel()));
        return modifier;
    }
}