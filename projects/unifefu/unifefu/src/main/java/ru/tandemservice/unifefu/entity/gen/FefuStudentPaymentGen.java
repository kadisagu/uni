package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выплата студентам(ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuStudentPaymentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuStudentPayment";
    public static final String ENTITY_NAME = "fefuStudentPayment";
    public static final int VERSION_HASH = 947117303;
    private static IEntityMeta ENTITY_META;

    public static final String P_PAYMENT_SUM = "paymentSum";
    public static final String P_START_DATE = "startDate";
    public static final String P_STOP_DATE = "stopDate";
    public static final String P_PAUSE_DATE = "pauseDate";
    public static final String P_CONTINUE_DATE = "continueDate";
    public static final String P_COMMENT = "comment";
    public static final String P_REASON = "reason";
    public static final String P_STOP_OR_PAUSE_ORDER = "stopOrPauseOrder";
    public static final String P_CONTINUE_ORDER = "continueOrder";
    public static final String P_INFLUENCED_ORDER_DATE = "influencedOrderDate";
    public static final String P_INFLUENCED_ORDER_NUM = "influencedOrderNum";
    public static final String L_INFLUENCED_EXTRACT = "influencedExtract";
    public static final String L_EXTRACT = "extract";

    private Double _paymentSum;     // Сумма
    private Date _startDate;     // Дата начала
    private Date _stopDate;     // Дата окончания
    private Date _pauseDate;     // Дата приостановления/прекращения
    private Date _continueDate;     // Дата возобновления
    private String _comment;     // Примечание
    private String _reason;     // Причина
    private boolean _stopOrPauseOrder = false;     // Приостанавливающий или прекращающий выплату приказ
    private boolean _continueOrder = false;     // Возобновляющий выплату приказ
    private Date _influencedOrderDate;     // Дата изменяемого приказа
    private String _influencedOrderNum;     // Номер изменяемого приказа
    private AbstractStudentExtract _influencedExtract;     // Изменяемая выписка
    private AbstractStudentExtract _extract;     // Выписка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сумма.
     */
    public Double getPaymentSum()
    {
        return _paymentSum;
    }

    /**
     * @param paymentSum Сумма.
     */
    public void setPaymentSum(Double paymentSum)
    {
        dirty(_paymentSum, paymentSum);
        _paymentSum = paymentSum;
    }

    /**
     * @return Дата начала.
     */
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getStopDate()
    {
        return _stopDate;
    }

    /**
     * @param stopDate Дата окончания.
     */
    public void setStopDate(Date stopDate)
    {
        dirty(_stopDate, stopDate);
        _stopDate = stopDate;
    }

    /**
     * @return Дата приостановления/прекращения.
     */
    public Date getPauseDate()
    {
        return _pauseDate;
    }

    /**
     * @param pauseDate Дата приостановления/прекращения.
     */
    public void setPauseDate(Date pauseDate)
    {
        dirty(_pauseDate, pauseDate);
        _pauseDate = pauseDate;
    }

    /**
     * @return Дата возобновления.
     */
    public Date getContinueDate()
    {
        return _continueDate;
    }

    /**
     * @param continueDate Дата возобновления.
     */
    public void setContinueDate(Date continueDate)
    {
        dirty(_continueDate, continueDate);
        _continueDate = continueDate;
    }

    /**
     * @return Примечание.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Примечание.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Причина.
     */
    @Length(max=255)
    public String getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина.
     */
    public void setReason(String reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Приостанавливающий или прекращающий выплату приказ. Свойство не может быть null.
     */
    @NotNull
    public boolean isStopOrPauseOrder()
    {
        return _stopOrPauseOrder;
    }

    /**
     * @param stopOrPauseOrder Приостанавливающий или прекращающий выплату приказ. Свойство не может быть null.
     */
    public void setStopOrPauseOrder(boolean stopOrPauseOrder)
    {
        dirty(_stopOrPauseOrder, stopOrPauseOrder);
        _stopOrPauseOrder = stopOrPauseOrder;
    }

    /**
     * @return Возобновляющий выплату приказ. Свойство не может быть null.
     */
    @NotNull
    public boolean isContinueOrder()
    {
        return _continueOrder;
    }

    /**
     * @param continueOrder Возобновляющий выплату приказ. Свойство не может быть null.
     */
    public void setContinueOrder(boolean continueOrder)
    {
        dirty(_continueOrder, continueOrder);
        _continueOrder = continueOrder;
    }

    /**
     * @return Дата изменяемого приказа.
     */
    public Date getInfluencedOrderDate()
    {
        return _influencedOrderDate;
    }

    /**
     * @param influencedOrderDate Дата изменяемого приказа.
     */
    public void setInfluencedOrderDate(Date influencedOrderDate)
    {
        dirty(_influencedOrderDate, influencedOrderDate);
        _influencedOrderDate = influencedOrderDate;
    }

    /**
     * @return Номер изменяемого приказа.
     */
    @Length(max=255)
    public String getInfluencedOrderNum()
    {
        return _influencedOrderNum;
    }

    /**
     * @param influencedOrderNum Номер изменяемого приказа.
     */
    public void setInfluencedOrderNum(String influencedOrderNum)
    {
        dirty(_influencedOrderNum, influencedOrderNum);
        _influencedOrderNum = influencedOrderNum;
    }

    /**
     * @return Изменяемая выписка.
     */
    public AbstractStudentExtract getInfluencedExtract()
    {
        return _influencedExtract;
    }

    /**
     * @param influencedExtract Изменяемая выписка.
     */
    public void setInfluencedExtract(AbstractStudentExtract influencedExtract)
    {
        dirty(_influencedExtract, influencedExtract);
        _influencedExtract = influencedExtract;
    }

    /**
     * @return Выписка. Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка. Свойство не может быть null.
     */
    public void setExtract(AbstractStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuStudentPaymentGen)
        {
            setPaymentSum(((FefuStudentPayment)another).getPaymentSum());
            setStartDate(((FefuStudentPayment)another).getStartDate());
            setStopDate(((FefuStudentPayment)another).getStopDate());
            setPauseDate(((FefuStudentPayment)another).getPauseDate());
            setContinueDate(((FefuStudentPayment)another).getContinueDate());
            setComment(((FefuStudentPayment)another).getComment());
            setReason(((FefuStudentPayment)another).getReason());
            setStopOrPauseOrder(((FefuStudentPayment)another).isStopOrPauseOrder());
            setContinueOrder(((FefuStudentPayment)another).isContinueOrder());
            setInfluencedOrderDate(((FefuStudentPayment)another).getInfluencedOrderDate());
            setInfluencedOrderNum(((FefuStudentPayment)another).getInfluencedOrderNum());
            setInfluencedExtract(((FefuStudentPayment)another).getInfluencedExtract());
            setExtract(((FefuStudentPayment)another).getExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuStudentPaymentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuStudentPayment.class;
        }

        public T newInstance()
        {
            return (T) new FefuStudentPayment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "paymentSum":
                    return obj.getPaymentSum();
                case "startDate":
                    return obj.getStartDate();
                case "stopDate":
                    return obj.getStopDate();
                case "pauseDate":
                    return obj.getPauseDate();
                case "continueDate":
                    return obj.getContinueDate();
                case "comment":
                    return obj.getComment();
                case "reason":
                    return obj.getReason();
                case "stopOrPauseOrder":
                    return obj.isStopOrPauseOrder();
                case "continueOrder":
                    return obj.isContinueOrder();
                case "influencedOrderDate":
                    return obj.getInfluencedOrderDate();
                case "influencedOrderNum":
                    return obj.getInfluencedOrderNum();
                case "influencedExtract":
                    return obj.getInfluencedExtract();
                case "extract":
                    return obj.getExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "paymentSum":
                    obj.setPaymentSum((Double) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "stopDate":
                    obj.setStopDate((Date) value);
                    return;
                case "pauseDate":
                    obj.setPauseDate((Date) value);
                    return;
                case "continueDate":
                    obj.setContinueDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "reason":
                    obj.setReason((String) value);
                    return;
                case "stopOrPauseOrder":
                    obj.setStopOrPauseOrder((Boolean) value);
                    return;
                case "continueOrder":
                    obj.setContinueOrder((Boolean) value);
                    return;
                case "influencedOrderDate":
                    obj.setInfluencedOrderDate((Date) value);
                    return;
                case "influencedOrderNum":
                    obj.setInfluencedOrderNum((String) value);
                    return;
                case "influencedExtract":
                    obj.setInfluencedExtract((AbstractStudentExtract) value);
                    return;
                case "extract":
                    obj.setExtract((AbstractStudentExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "paymentSum":
                        return true;
                case "startDate":
                        return true;
                case "stopDate":
                        return true;
                case "pauseDate":
                        return true;
                case "continueDate":
                        return true;
                case "comment":
                        return true;
                case "reason":
                        return true;
                case "stopOrPauseOrder":
                        return true;
                case "continueOrder":
                        return true;
                case "influencedOrderDate":
                        return true;
                case "influencedOrderNum":
                        return true;
                case "influencedExtract":
                        return true;
                case "extract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "paymentSum":
                    return true;
                case "startDate":
                    return true;
                case "stopDate":
                    return true;
                case "pauseDate":
                    return true;
                case "continueDate":
                    return true;
                case "comment":
                    return true;
                case "reason":
                    return true;
                case "stopOrPauseOrder":
                    return true;
                case "continueOrder":
                    return true;
                case "influencedOrderDate":
                    return true;
                case "influencedOrderNum":
                    return true;
                case "influencedExtract":
                    return true;
                case "extract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "paymentSum":
                    return Double.class;
                case "startDate":
                    return Date.class;
                case "stopDate":
                    return Date.class;
                case "pauseDate":
                    return Date.class;
                case "continueDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "reason":
                    return String.class;
                case "stopOrPauseOrder":
                    return Boolean.class;
                case "continueOrder":
                    return Boolean.class;
                case "influencedOrderDate":
                    return Date.class;
                case "influencedOrderNum":
                    return String.class;
                case "influencedExtract":
                    return AbstractStudentExtract.class;
                case "extract":
                    return AbstractStudentExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuStudentPayment> _dslPath = new Path<FefuStudentPayment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuStudentPayment");
    }
            

    /**
     * @return Сумма.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getPaymentSum()
     */
    public static PropertyPath<Double> paymentSum()
    {
        return _dslPath.paymentSum();
    }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getStopDate()
     */
    public static PropertyPath<Date> stopDate()
    {
        return _dslPath.stopDate();
    }

    /**
     * @return Дата приостановления/прекращения.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getPauseDate()
     */
    public static PropertyPath<Date> pauseDate()
    {
        return _dslPath.pauseDate();
    }

    /**
     * @return Дата возобновления.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getContinueDate()
     */
    public static PropertyPath<Date> continueDate()
    {
        return _dslPath.continueDate();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Причина.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getReason()
     */
    public static PropertyPath<String> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Приостанавливающий или прекращающий выплату приказ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#isStopOrPauseOrder()
     */
    public static PropertyPath<Boolean> stopOrPauseOrder()
    {
        return _dslPath.stopOrPauseOrder();
    }

    /**
     * @return Возобновляющий выплату приказ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#isContinueOrder()
     */
    public static PropertyPath<Boolean> continueOrder()
    {
        return _dslPath.continueOrder();
    }

    /**
     * @return Дата изменяемого приказа.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getInfluencedOrderDate()
     */
    public static PropertyPath<Date> influencedOrderDate()
    {
        return _dslPath.influencedOrderDate();
    }

    /**
     * @return Номер изменяемого приказа.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getInfluencedOrderNum()
     */
    public static PropertyPath<String> influencedOrderNum()
    {
        return _dslPath.influencedOrderNum();
    }

    /**
     * @return Изменяемая выписка.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getInfluencedExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> influencedExtract()
    {
        return _dslPath.influencedExtract();
    }

    /**
     * @return Выписка. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    public static class Path<E extends FefuStudentPayment> extends EntityPath<E>
    {
        private PropertyPath<Double> _paymentSum;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _stopDate;
        private PropertyPath<Date> _pauseDate;
        private PropertyPath<Date> _continueDate;
        private PropertyPath<String> _comment;
        private PropertyPath<String> _reason;
        private PropertyPath<Boolean> _stopOrPauseOrder;
        private PropertyPath<Boolean> _continueOrder;
        private PropertyPath<Date> _influencedOrderDate;
        private PropertyPath<String> _influencedOrderNum;
        private AbstractStudentExtract.Path<AbstractStudentExtract> _influencedExtract;
        private AbstractStudentExtract.Path<AbstractStudentExtract> _extract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сумма.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getPaymentSum()
     */
        public PropertyPath<Double> paymentSum()
        {
            if(_paymentSum == null )
                _paymentSum = new PropertyPath<Double>(FefuStudentPaymentGen.P_PAYMENT_SUM, this);
            return _paymentSum;
        }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(FefuStudentPaymentGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getStopDate()
     */
        public PropertyPath<Date> stopDate()
        {
            if(_stopDate == null )
                _stopDate = new PropertyPath<Date>(FefuStudentPaymentGen.P_STOP_DATE, this);
            return _stopDate;
        }

    /**
     * @return Дата приостановления/прекращения.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getPauseDate()
     */
        public PropertyPath<Date> pauseDate()
        {
            if(_pauseDate == null )
                _pauseDate = new PropertyPath<Date>(FefuStudentPaymentGen.P_PAUSE_DATE, this);
            return _pauseDate;
        }

    /**
     * @return Дата возобновления.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getContinueDate()
     */
        public PropertyPath<Date> continueDate()
        {
            if(_continueDate == null )
                _continueDate = new PropertyPath<Date>(FefuStudentPaymentGen.P_CONTINUE_DATE, this);
            return _continueDate;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(FefuStudentPaymentGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Причина.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getReason()
     */
        public PropertyPath<String> reason()
        {
            if(_reason == null )
                _reason = new PropertyPath<String>(FefuStudentPaymentGen.P_REASON, this);
            return _reason;
        }

    /**
     * @return Приостанавливающий или прекращающий выплату приказ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#isStopOrPauseOrder()
     */
        public PropertyPath<Boolean> stopOrPauseOrder()
        {
            if(_stopOrPauseOrder == null )
                _stopOrPauseOrder = new PropertyPath<Boolean>(FefuStudentPaymentGen.P_STOP_OR_PAUSE_ORDER, this);
            return _stopOrPauseOrder;
        }

    /**
     * @return Возобновляющий выплату приказ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#isContinueOrder()
     */
        public PropertyPath<Boolean> continueOrder()
        {
            if(_continueOrder == null )
                _continueOrder = new PropertyPath<Boolean>(FefuStudentPaymentGen.P_CONTINUE_ORDER, this);
            return _continueOrder;
        }

    /**
     * @return Дата изменяемого приказа.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getInfluencedOrderDate()
     */
        public PropertyPath<Date> influencedOrderDate()
        {
            if(_influencedOrderDate == null )
                _influencedOrderDate = new PropertyPath<Date>(FefuStudentPaymentGen.P_INFLUENCED_ORDER_DATE, this);
            return _influencedOrderDate;
        }

    /**
     * @return Номер изменяемого приказа.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getInfluencedOrderNum()
     */
        public PropertyPath<String> influencedOrderNum()
        {
            if(_influencedOrderNum == null )
                _influencedOrderNum = new PropertyPath<String>(FefuStudentPaymentGen.P_INFLUENCED_ORDER_NUM, this);
            return _influencedOrderNum;
        }

    /**
     * @return Изменяемая выписка.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getInfluencedExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> influencedExtract()
        {
            if(_influencedExtract == null )
                _influencedExtract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_INFLUENCED_EXTRACT, this);
            return _influencedExtract;
        }

    /**
     * @return Выписка. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPayment#getExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

        public Class getEntityClass()
        {
            return FefuStudentPayment.class;
        }

        public String getEntityName()
        {
            return "fefuStudentPayment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
