/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrgUnitNumberAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniOrgUnit.logic.OrgUnitByKindComboDataSourceHandler;

/**
 * @author Nikolay Fedorovskih
 * @since 12.08.2013
 */
@Configuration
public class FefuSettingsOrgUnitNumberAddEdit extends BusinessComponentManager
{
    public static final String FORMATIVE_ORGUNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_ORGUNIT_DS = "territorialOrgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(FORMATIVE_ORGUNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(TERRITORIAL_ORGUNIT_DS, territorialOrgUnitDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> territorialOrgUnitDSHandler()
    {
        return new OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL);
    }
}