/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import ru.tandemservice.unifefu.ws.nsi.datagram.CompensationTypeType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import javax.xml.namespace.QName;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceCompensationTypeTest extends NsiLocalAccessBaseTest
{
    public static final String[][] RETRIEVE_WHOLE_PARAMS = new String[][]{{null}};
    public static final String[][] RETRIEVE_NON_EXISTS_PARAMS = new String[][]{{"aaaaaaaa-f786-3c19-8594-2d4e5838939a"}};
    public static final String[][] RETRIEVE_SINGLE_PARAMS = new String[][]{{"57fa54b7-f786-3c19-8594-2d4e5838939a"}};
    public static final String[][] RETRIEVE_FEW_PARAMS = new String[][]{{"57fa54b7-f786-3c19-8594-2d4e5838939a"}, {"80b36c09-9004-3949-82bd-af93df3d5d92"}, {"aaaaaaaa-9004-3949-82bd-af93df3d5d92"}, {"d7a1b7cd-35d1-3266-ba94-c8df29db6bc4"}};

    public static final String[][] INSERT_NEW_PARAMS = new String[][]{{"80b36c09-9004-3949-82bd-xxxxxxxxxxxX", "Сверх-сверхплановое место", "догодоговор", "кк"}};
    public static final String[][] INSERT_NEW_ANALOG_PARAMS = new String[][]{{"80b36c09-9004-3949-82bd-xxxxxxxxxxxY", "Сверхплановое место", "договор", "к"}};
    public static final String[][] INSERT_NEW_GUID_EXISTS_PARAMS = new String[][]{{"80b36c09-9004-3949-82bd-xxxxxxxxxxxY", "Сверхплановое место 1", "договор 1", "к"}};
    public static final String[][] INSERT_NEW_MERGE_GUID_EXISTS_PARAMS = new String[][]{{"80b36c09-9004-3949-82bd-xxxxxxxxxxxZ", "Сверхплановое место 2", "договор 2", "к", "80b36c09-9004-3949-82bd-xxxxxxxxxxxZ; 80b36c09-9004-3949-82bd-xxxxxxxxxxxY; 90800349-619a-11e0-a335-xxxxxxxxxx41"}};
    public static final String[] INSERT_MASS_TEMPLATE_PARAMS = new String[]{"Тест", "Т", "т"};

    public static final String[][] DELETE_EMPTY_PARAMS = new String[][]{{null}};
    public static final String[][] DELETE_NON_EXISTS_PARAMS = new String[][]{{"57fa54b7-f786-3c19-8594-2d4e5838939X"}};
    public static final String[][] DELETE_SINGLE_PARAMS = new String[][]{{"57fa54b7-f786-3c19-8594-2d4e5838939a"}};
    public static final String[][] DELETE_NON_DELETABLE_PARAMS = new String[][]{{"57fa54b7-f786-3c19-8594-2d4e5838939a"}};
    public static final String[][] DELETE_MASS_PARAMS = new String[][]{{"57fa54b7-f786-3c19-8594-2d4e5838939a"}};

    @Override
    protected INsiEntity createNsiEntity(String[] fieldValues)
    {
        CompensationTypeType entity = NsiLocalAccessServiceTestUtil.FACTORY.createCompensationTypeType();
        if (null != fieldValues)
        {
            if (fieldValues.length > 0) entity.setID(fieldValues[0]);
            if (fieldValues.length > 1) entity.setCompensationTypeName(fieldValues[1]);
            if (fieldValues.length > 2) entity.setCompensationTypeNameShort(fieldValues[2]);
            if (fieldValues.length > 3) entity.setCompensationTypeGroup(fieldValues[3]);
            if (fieldValues.length > 4 && null != fieldValues[4])
                entity.getOtherAttributes().put(new QName("mergeDuplicates"), fieldValues[4]);
        }
        return entity;
    }
}