/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Nikolay Fedorovskih
 * @since 31.01.2014
 */
public class MS_unifefu_2x4x1_18to19 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Удаляем склонения для названий сессии осеннего и весеннего семестра.
        // В declination.data.xml лежат правильные названия сессий - при старте они автоматически запишутся в базу.

        tool.getStatement().executeUpdate("delete from DECLINABLEPROPERTY_T " +
                                                  "where DECLINABLE_ID in (select id from YEARDISTRIBUTIONPART_T where CODE_P='21' or CODE_P='22') " +
                                                  "and (PROPERTY_P='sessionTitle' or PROPERTY_P='sessionShortTitle')");
    }
}