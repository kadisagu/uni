/* $Id: Model.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.modularextract.fefu4.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuExcludeStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 27.08.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuExcludeStuExtract>
{
    private StudentStatus _studentStatusNew;

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }
}