/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.validator;

/**
 * Валидатор целого числа: проверяет, что число больше или равно заданному.
 * @author Alexander Zhebko
 * @since 18.07.2013
 */
public class GEIntegerValidator extends Validator<Integer>
{
    private final int minValue;

    public GEIntegerValidator(int minValue)
    {
        this.minValue = minValue;
    }

    @Override
    protected boolean validateValue(Integer value)
    {
        return value >= minValue;
    }

    @Override
    public String getInvalidMessage()
    {
        return "Значение меньше " + minValue + ".";
    }
}