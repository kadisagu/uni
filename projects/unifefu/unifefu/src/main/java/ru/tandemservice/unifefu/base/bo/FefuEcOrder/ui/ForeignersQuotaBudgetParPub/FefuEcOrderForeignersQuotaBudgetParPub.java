/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.ForeignersQuotaBudgetParPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;

/**
 * @author Igor Belanov
 * @since 21.07.2016
 */
@Configuration
public class FefuEcOrderForeignersQuotaBudgetParPub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EcOrderManager.ENROLLMENT_EXTRACT_DS, EcOrderManager.instance().enrollmentExtractDS(), EcOrderManager.instance().enrollmentExtractDSHandler()))
                .create();
    }
}
