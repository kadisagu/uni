/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.StudentsSendToArchive;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.datasource.column.ITextColumnBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic.FefuStudentsSendToArchiveDSHandler;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
@Configuration
public class FefuSystemActionStudentsSendToArchive extends AbstractUniStudentList
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, studentSearchListDSColumnExtPoint(), fefuStudentsSendToArchiveDSHandler())));
    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint()
    {
        IColumnListExtPointBuilder builder = columnListExtPointBuilder(STUDENT_SEARCH_LIST_DS);

        List<ColumnBase> columnList = prepareColumnList();
        columnList = customizeStudentSearchListColumns(columnList);
        columnList.add(1, checkboxColumn("select").disabled("ui:orderNonStateFinished").create());
        columnList.add(newOrderNonStateFinishedColumnBuilder().create());

        for (ColumnBase item : columnList)
            builder.addColumn(item);

        return builder.create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> fefuStudentsSendToArchiveDSHandler()
    {
        return new FefuStudentsSendToArchiveDSHandler(getName());
    }

    private ITextColumnBuilder newOrderNonStateFinishedColumnBuilder()
    {
        return textColumn("orderNonStateFinished", FefuStudentsSendToArchiveDSHandler.ORDER_NON_STATE_FINISHED)
                .formatter(source -> (Boolean) source ? "Есть непроведенные приказы" : "")
                .styleResolver(rowEntity -> (boolean) ((DataWrapper) rowEntity).get(FefuStudentsSendToArchiveDSHandler.ORDER_NON_STATE_FINISHED) ? "color:red" : null);
    }
}