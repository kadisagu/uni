/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.OnlineEntrantNotifications.FefuSystemActionOnlineEntrantNotifications;
import ru.tandemservice.unifefu.base.vo.ProcessedOnlineEntrantsVO;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 6/17/13
 */
public class FefuProcessedOnlineEntrantsDSHandler extends DefaultSearchDataSourceHandler
{
    public FefuProcessedOnlineEntrantsDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long enrollmentCampaignId = context.get("enrollmentCampaign");
        Date processDateFrom = context.get("processDateFrom");
        Date processDateTo = context.get("processDateTo");
        Long entrantType = context.get("entrantType");


        List<ProcessedOnlineEntrantsVO> resultList = Lists.newArrayList();

        if (null == entrantType || FefuSystemActionManager.ENTRANT_TYPE_DEFAULT.equals(entrantType))
        {
            // обычные онлайн-абитуриенты
            DQLSelectBuilder onlineEntrantsBuilder = new DQLSelectBuilder().fromEntity(OnlineEntrant.class, "oe");
            if(null != enrollmentCampaignId)
                onlineEntrantsBuilder.where(eq(property("oe", OnlineEntrant.enrollmentCampaign().id()), value(enrollmentCampaignId)));

            onlineEntrantsBuilder.where(isNotNull(property("oe", OnlineEntrant.entrant())));
            onlineEntrantsBuilder.where(betweenDays(OnlineEntrant.entrant().registrationDate().fromAlias("oe"), processDateFrom, processDateTo));

            List<OnlineEntrant> onlineEntrants = createStatement(onlineEntrantsBuilder).list();

            for (OnlineEntrant entrant : onlineEntrants)
            {
                resultList.add(new ProcessedOnlineEntrantsVO(entrant));
            }
        }


        if (null == entrantType || FefuSystemActionManager.ENTRANT_TYPE_FOREIGN.equals(entrantType))
        {
            // иностанные онлайн-абитуриенты
            DQLSelectBuilder foreignOnlineEntrantsBuilder = new DQLSelectBuilder().fromEntity(FefuForeignOnlineEntrant.class, "foe");
            if(null != enrollmentCampaignId)
                foreignOnlineEntrantsBuilder.where(eq(property("foe", FefuForeignOnlineEntrant.enrollmentCampaign().id()), value(enrollmentCampaignId)));
            foreignOnlineEntrantsBuilder.where(isNotNull(property("foe", FefuForeignOnlineEntrant.entrant())));
            foreignOnlineEntrantsBuilder.where(betweenDays(FefuForeignOnlineEntrant.entrant().registrationDate().fromAlias("foe"), processDateFrom, processDateTo));

            List<FefuForeignOnlineEntrant> foreignOnlineEntrants = createStatement(foreignOnlineEntrantsBuilder).list();

            for (FefuForeignOnlineEntrant entrant : foreignOnlineEntrants)
            {
                resultList.add(new ProcessedOnlineEntrantsVO(entrant));
            }
        }

        if (FefuSystemActionOnlineEntrantNotifications.ENTRANT_TYPE_COLUMN.equals(input.getEntityOrder().getColumnName()))
        {
            Collections.sort(resultList, new Comparator<ProcessedOnlineEntrantsVO>()
            {
                @Override
                public int compare(ProcessedOnlineEntrantsVO o1, ProcessedOnlineEntrantsVO o2)
                {
                    return o1.getEntrantType().compareTo(o2.getEntrantType());
                }
            });

            if (OrderDirection.desc.equals(input.getEntityOrder().getDirection()))
            {
                Collections.reverse(resultList);
            }
        }
        else if (FefuSystemActionOnlineEntrantNotifications.PROCESS_DATE_COLUMN.equals(input.getEntityOrder().getColumnName()))
        {
            Collections.sort(resultList, new Comparator<ProcessedOnlineEntrantsVO>()
            {
                @Override
                public int compare(ProcessedOnlineEntrantsVO o1, ProcessedOnlineEntrantsVO o2)
                {
                    return o1.getProcessedDate().compareTo(o2.getProcessedDate());
                }
            });


            if (OrderDirection.desc.equals(input.getEntityOrder().getDirection()))
            {
                Collections.reverse(resultList);
            }
        }
        else
        {
            Collections.sort(resultList, new Comparator<ProcessedOnlineEntrantsVO>()
            {
                @Override
                public int compare(ProcessedOnlineEntrantsVO o1, ProcessedOnlineEntrantsVO o2)
                {
                    return o1.getTitle().compareTo(o2.getTitle());
                }
            });

            if (OrderDirection.desc.equals(input.getEntityOrder().getDirection()))
            {
                Collections.reverse(resultList);
            }
        }

        return ListOutputBuilder.get(input, resultList).pageable(true).build();
    }
}
