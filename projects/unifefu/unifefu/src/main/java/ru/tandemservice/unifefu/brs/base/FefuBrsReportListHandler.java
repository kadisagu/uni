/* $Id$ */
package ru.tandemservice.unifefu.brs.base;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unifefu.entity.report.IFefuBrsReport;
import ru.tandemservice.unifefu.entity.report.gen.IFefuBrsReportGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 30.05.2014
 */
public class FefuBrsReportListHandler extends DefaultSearchDataSourceHandler
{
    public static final String YEAR_PART_PARAM = "yearPart";
    public static final String ORGUNIT_ID_PARAM = "orgunit";

    public FefuBrsReportListHandler(String ownerId, Class<? extends IEntity> entityClass)
    {
        super(ownerId, entityClass);
        if (!IFefuBrsReport.class.isAssignableFrom(entityClass))
            throw new IllegalArgumentException("Fefu brs report class must implement interface " + IFefuBrsReport.class.getSimpleName());
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(getEntityClass(), "e")
                .column("e");

        EppYearPart yearPart = context.get(YEAR_PART_PARAM);
        if (null != yearPart)
            dql.where(eq(property("e", IFefuBrsReportGen.P_YEAR_PART), value(yearPart.getTitle())));

        Long orgUnitId = context.get(ORGUNIT_ID_PARAM);
        if (orgUnitId != null)
            dql.where(eq(property("e", IFefuBrsReportGen.L_ORG_UNIT), value(orgUnitId))); // Отчеты, построенные с подразделения показываем только только на этом подразделении
        else
            dql.where(isNull(property("e", IFefuBrsReportGen.L_ORG_UNIT))); // а в глобальных отчетах не показываем построенные на подразделениях

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).order().build();
    }
}