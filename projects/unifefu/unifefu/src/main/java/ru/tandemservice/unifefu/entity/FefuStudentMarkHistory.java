package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.FefuStudentMarkHistoryGen;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;

import java.util.Date;

/**
 * История оценки студента в журнале
 */
public class FefuStudentMarkHistory extends FefuStudentMarkHistoryGen
{
    public FefuStudentMarkHistory()
    {
    }

    public FefuStudentMarkHistory(TrEduGroupEventStudent event, Date date, Long mark)
    {
        setTrEduGroupEventStudent(event);
        setMarkDate(date);
        setGradeAsLong(mark);
    }

    public Double getMark()
    {
        return getGradeAsLong() == null ? null : ((double) getGradeAsLong()) / 100;
    }

}