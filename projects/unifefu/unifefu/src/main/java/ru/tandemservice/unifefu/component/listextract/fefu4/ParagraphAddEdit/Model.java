/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class Model extends AbstractListParagraphAddEditModel<FefuAcadGrantAssignStuEnrolmentListExtract> implements IGroupModel
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private Date _beginDate;
    private Date _endDate;

    private ISelectModel _groupListModel;
    private List<CompensationType> _compensationTypeList;

    private boolean _compensationTypeDisabled;

    private List<Long> _groupManagerIds = new ArrayList<>();

    public String getGrantSizeId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "grantSizeId_" + dataSource.getCurrentEntity().getId();
    }

    public boolean isNotGroupManager()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return !getGroupManagerIds().contains(dataSource.getCurrentEntity().getId());
    }


    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public Date getBeginDate()
    {
        return _beginDate;
    }

    public void setBeginDate(Date beginDate)
    {
        _beginDate = beginDate;
    }

    public Date getEndDate()
    {
        return _endDate;
    }

    public void setEndDate(Date endDate)
    {
        _endDate = endDate;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public boolean isCompensationTypeDisabled()
    {
        return _compensationTypeDisabled;
    }

    public void setCompensationTypeDisabled(boolean compensationTypeDisabled)
    {
        _compensationTypeDisabled = compensationTypeDisabled;
    }

    public List<Long> getGroupManagerIds()
    {
        return _groupManagerIds;
    }

    public void setGroupManagerIds(List<Long> groupManagerIds)
    {
        _groupManagerIds = groupManagerIds;
    }
}