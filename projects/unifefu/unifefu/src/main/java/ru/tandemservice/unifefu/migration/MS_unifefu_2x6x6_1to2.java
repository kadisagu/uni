package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x6_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuBrsAttestationResultsReport

		// создано свойство responsibilityOrgUnit
		{
			// создать колонку
			tool.createColumn("ffbrsattsttnrsltsrprt_t", new DBColumn("responsibilityorgunit_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuBrsDelayGradingReport

		// создано свойство responsibilityOrgUnit
		{
			// создать колонку
			tool.createColumn("fefubrsdelaygradingreport_t", new DBColumn("responsibilityorgunit_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuBrsPercentGradingReport

		// создано свойство responsibilityOrgUnit
		{
			// создать колонку
			tool.createColumn("fefubrspercentgradingreport_t", new DBColumn("responsibilityorgunit_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuBrsStudentDisciplinesReport

		// создано свойство responsibilityOrgUnit
		{
			// создать колонку
			tool.createColumn("ffbrsstdntdscplnsrprt_t", new DBColumn("responsibilityorgunit_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuListTrJournalDisciplinesReport

		// создано свойство responsibilityOrgUnit
		{
			// создать колонку
			tool.createColumn("fflsttrjrnldscplnsrprt_t", new DBColumn("responsibilityorgunit_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuRatingGroupAllDiscReport

		// создано свойство responsibilityOrgUnit
		{
			// создать колонку
			tool.createColumn("fefuratinggroupalldiscreport_t", new DBColumn("responsibilityorgunit_p", DBType.createVarchar(255)));

		}


    }
}