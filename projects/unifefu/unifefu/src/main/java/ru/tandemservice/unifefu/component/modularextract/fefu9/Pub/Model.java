/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu9.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class Model extends ModularStudentExtractPubModel<FefuSocGrantResumptionStuExtract>
{
}