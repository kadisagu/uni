/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 27.05.2014
 */
public class FefuNsiFullSyncDaemonDao extends UniBaseDao implements IFefuNsiFullSyncDaemonDao
{
    public static final int DAEMON_ITERATION_TIME = 3;

    public static final SyncDaemon DAEMON = new SyncDaemon(FefuNsiFullSyncDaemonDao.class.getName(), DAEMON_ITERATION_TIME, IFefuNsiFullSyncDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            // Проверяем, включена ли автосинхронизация. Если порпертя отсутствует в конфигах, то прерываем выполнение демона
            if (null == ApplicationRuntime.getProperty(FefuNsiDaemonDao.NSI_SERVICE_AUTOSYNC)) return;
            if (!Boolean.parseBoolean(ApplicationRuntime.getProperty(FefuNsiDaemonDao.NSI_SERVICE_AUTOSYNC))) return;

            final IFefuNsiFullSyncDaemonDao dao = IFefuNsiFullSyncDaemonDao.instance.get();

            try
            {
                dao.doFullSyncCatalogsWithNSI();
            } catch (final Throwable t)
            {
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            }
        }
    };

    @Override
    public void doFullSyncCatalogsWithNSI()
    {
        if (!FefuNsiDaemonDao.catalogsToManualSync.isEmpty() && null == FefuNsiDaemonDao.current_sync_catalog_id)
        {
            FefuNsiCatalogType catalog = FefuNsiDaemonDao.catalogsToManualSync.iterator().next();
            String[] catalogCodes = catalog.getUniCode().split(",");
            String catalogCode = catalogCodes[0];
            Class<IEntity> clazz = EntityRuntime.getMeta(catalogCode).getEntityClass();
            Date actualDate = CoreDateUtils.add(new Date(), Calendar.MONTH, -1);

            FefuNsiDaemonDao.current_sync_catalog_id = catalog.getId();
            try
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(clazz, "e").column(property("e", IEntity.P_ID)).top(50) // TODO
                        .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "ids", eq(property("e", IEntity.P_ID), property(FefuNsiIds.entityId().fromAlias("ids"))))
                        .where(or(
                                isNull(property(FefuNsiIds.syncTime().fromAlias("ids"))),
                                le(property(FefuNsiIds.syncTime().fromAlias("ids")), valueTimestamp(actualDate))
                        ));

                if(catalog.getUniCode().toLowerCase().contains("person"))
                {
                    builder.joinEntity("e", DQLJoinType.inner, Student.class, "s", eq(property("e", IEntity.P_ID), property(Student.person().id().fromAlias("s"))));
                }

                List<Long> elements = DataAccessServices.dao().getList(builder);

                IFefuNsiDaemonDao.instance.get().doRegisterEntityAdd(elements.toArray(new Long[]{}));

                if (elements.isEmpty() || elements.size() < 50) // TODO
                {
                    FefuNsiDaemonDao.removeCatalogToSync(catalog.getId());
                    FefuNsiCatalogType catalogToUpdate = get(FefuNsiCatalogType.class, catalog.getId());
                    catalogToUpdate.setSyncDateTime(new Date());
                    update(catalogToUpdate);
                }

                FefuNsiDaemonDao.current_sync_catalog_id = null;

            } catch (Throwable t)
            {
                FefuNsiDaemonDao.current_sync_catalog_id = null;
            }
        }
    }
}