/*$Id$*/
package ru.tandemservice.unifefu.component.listextract.fefu12;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e86.utils.EmployeePostDecl;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public class FefuStuffCompensationStuListExtractPrint implements IPrintFormCreator<FefuStuffCompensationStuListExtract>, IListParagraphPrintFormCreator<FefuStuffCompensationStuListExtract>
{
	@Override
	public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuStuffCompensationStuListExtract firstExtract)
	{
	}

	@Override
	public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuStuffCompensationStuListExtract firstExtract)
	{
		return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
	}

	@Override
	public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuStuffCompensationStuListExtract firstExtract)
	{
		return null;
	}

	@Override
	public RtfDocument createPrintForm(byte[] template, FefuStuffCompensationStuListExtract extract)
	{
		final RtfDocument document = new RtfReader().read(template);
		RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
		CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

		Student student = extract.getEntity();

		CommonExtractPrint.modifyEducationStr(modifier, student.getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());

		EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
		Group group = student.getGroup();
		CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");
		CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(group), "fefuShortFastExtendedOptionalText");
		CommonExtractPrint.initFefuGroup(modifier, "fefuGroup", extract.getEntity().getGroup(), extract.getEntity().getEducationOrgUnit().getDevelopForm(), " группы ");
		CommonExtractPrint.modifyEducationStr(modifier, extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(), new String[]{"fefuEducationStrDirection", "educationStrDirection"}, false);

		modifier.put("protocol",String.format("%s года № %s", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(extract.getProtocolDate()), extract.getProtocolNumber()));

		modifier.put("responsiblePerson", StringUtils.capitalize(EmployeePostDecl.getEmployeeStrDat(extract.getResponsiblePerson())));
		modifier.put("matchingPerson", EmployeePostDecl.getEmployeeStrInst(extract.getMatchingPerson()));

		modifier.put("sEnd", extract.getEntity().getPerson().isMale() ? "его" : "ую");
		modifier.put("compensationSum", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getCompensationSumAsDouble()) );
		modifier.put("immediateSum", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getImmediateSumAsDouble()) );
		modifier.put("matchingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getMatchingDate()));

		modifier.modify(document);
		CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
		return document;
	}
}
