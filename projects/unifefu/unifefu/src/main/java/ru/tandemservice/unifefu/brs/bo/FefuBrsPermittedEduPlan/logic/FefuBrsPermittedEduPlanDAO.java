/*$Id$*/
package ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.logic;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.unifefu.entity.FefuBrsPermittedWorkPlan;

import javax.annotation.Nullable;
import java.util.Collection;

/**
 * @author DMITRY KNYAZEV
 * @since 22.01.2015
 */
public class FefuBrsPermittedEduPlanDAO extends SharedBaseDao implements IFefuBrsPermittedEduPlanDAO
{
    private static final String FEFU_BRS_SETTINGS = "fefuBrsSettings";
    private static final String FEFU_DISABLE_CREATION = "fefuDisableCreation";//параметр "Запретить создание реализаций"

    @Override
    public Boolean isDisableCreation()
    {
        IDataSettings settings = DataSettingsFacade.getSettings(FEFU_BRS_SETTINGS);
        Boolean disableCreation = settings.get(FEFU_DISABLE_CREATION);
        return disableCreation == null ? false : disableCreation;
    }

    @Override
    public void saveDisableCreation(@Nullable Boolean value)
    {
        IDataSettings settings = DataSettingsFacade.getSettings(FEFU_BRS_SETTINGS);
        settings.set(FEFU_DISABLE_CREATION, value);
        DataSettingsFacade.saveSettings(settings);
    }

    @Override
    public void saveWorkPlansAsPermitted(Collection<IEntity> workPlanList)
    {
        for (IEntity entity : workPlanList)
        {
            EppWorkPlan workPlan = (EppWorkPlan) entity;
            FefuBrsPermittedWorkPlan PermittedWorkPlan = new FefuBrsPermittedWorkPlan();
            PermittedWorkPlan.setWorkPlan(workPlan);
            save(PermittedWorkPlan);
        }
    }
}
