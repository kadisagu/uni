/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionPartitionTypeChange;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.unifefu.dao.eppEduPlan.IImtsaImportDAO;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionPartitionType;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeekPart;
import ru.tandemservice.unifefu.entity.catalog.FefuSchedulePartitionType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuSchedulePartitionTypeCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 03.06.2013
 */
public class DAO extends UniDao<Model>
{
    private static int MIN_PART = 6;

    @Override
    public void prepare(Model model)
    {
        FefuEduPlanVersionPartitionType versionPartitionType = get(FefuEduPlanVersionPartitionType.class, FefuEduPlanVersionPartitionType.version().id(), model.getId());
        model.setVersionPartitionType(versionPartitionType);

        model.setPartitionTypes(getCatalogItemListOrderByCode(FefuSchedulePartitionType.class));
        model.setPartitionTypeOld(versionPartitionType.getPartitionType());
    }

    @Override
    public void update(Model model)
    {
        FefuSchedulePartitionType partitionTypeOld = model.getPartitionTypeOld();
        FefuSchedulePartitionType partitionTypeNew = model.getVersionPartitionType().getPartitionType();

        if (partitionTypeNew.getCode().equals(partitionTypeOld.getCode()))
            return;

        // меняем тип разбиения УПв
        update(model.getVersionPartitionType());

        // переразбиваем недели в учебном графике
        if (!partitionTypeOld.getCode().equals(FefuSchedulePartitionTypeCodes.WEEK))
        {
            // в недельном разбиении все недели не разбиты, с ними ничего не происходит
            List<FefuEduPlanVersionWeek> weekList = new DQLSelectBuilder()
                    .fromEntity(FefuEduPlanVersionWeek.class, "w")
                    .where(eq(property(FefuEduPlanVersionWeek.version().id().fromAlias("w")), value(model.getId())))
                    .where(isNull(property(FefuEduPlanVersionWeek.weekType().fromAlias("w"))))
                    .createStatement(getSession())
                    .list();

            Map<FefuEduPlanVersionWeek, Map<Integer, EppWeekType>> weekPartMapFrom = SafeMap.get(HashMap.class);

            List<FefuEduPlanVersionWeekPart> weekPartList = new DQLSelectBuilder()
                    .fromEntity(FefuEduPlanVersionWeekPart.class, "wp")
                    .where(in(property(FefuEduPlanVersionWeekPart.eduPlanVersionWeek().fromAlias("wp")), weekList))
                    .createStatement(getSession())
                    .list();

            for (FefuEduPlanVersionWeekPart weekPart: weekPartList)
            {
                weekPartMapFrom.get(weekPart.getEduPlanVersionWeek()).put(weekPart.getPartitionElementNumber(), weekPart.getWeekType());
            }

            // удаляем элементы разбиения учебных недель, на их месте создаем новые
            new DQLDeleteBuilder(FefuEduPlanVersionWeekPart.class).where(eq(property(FefuEduPlanVersionWeekPart.eduPlanVersionWeek().version()), value(model.getId()))).createStatement(getSession()).execute();

            if (partitionTypeNew.getCode().equals(FefuSchedulePartitionTypeCodes.WEEK))
            {
                for (FefuEduPlanVersionWeek week: weekList)
                {
                    EppWeekType result = weekPartMapFrom.get(week).get(1);

                    week.setWeekType(result);
                    update(week);
                }

            } else
            {
                for (FefuEduPlanVersionWeek week: weekList)
                {
                    Map<Integer, EppWeekType> weekTypeMap = weekPartMapFrom.get(week);
                    if (!partitionTypeOld.getCode().equals(FefuSchedulePartitionTypeCodes.ONE_DAY))
                    {
                        // подневное не будем в холостую гонять
                        weekTypeMap = transformToOneDayPartition(weekTypeMap, partitionTypeOld.getPartsNumber());
                    }

                    if (!partitionTypeNew.getCode().equals(FefuSchedulePartitionTypeCodes.ONE_DAY))
                    {
                        // подневное не будем в холостую гонять
                        weekTypeMap = transformFromOneDayPartition(weekTypeMap, partitionTypeNew.getPartsNumber());
                    }

                    for (Map.Entry<Integer, EppWeekType> weekTypeEntry: weekTypeMap.entrySet())
                    {
                        save(new FefuEduPlanVersionWeekPart(week, weekTypeEntry.getKey(), weekTypeEntry.getValue()));
                    }
                }
            }

            IImtsaImportDAO.instance.get().updateEduPlanVersionWeekTypes(model.getId(), null);
        }
    }

    /* Преобразует разбиение от заданного к подневному */
    private Map<Integer, EppWeekType> transformToOneDayPartition(Map<Integer, EppWeekType> weekTypeMap, int partsNumberFrom)
    {
        // 3 -> 6:      1 -> 1      1 -> 2      2 -> 3      2 -> 4      3 -> 5      3 -> 6
        // 2 -> 6:      1 -> 1      1 -> 2      1 -> 3      2 -> 4      2 -> 5      2 -> 6
        Map<Integer, EppWeekType> result = new HashMap<>();
        for (int i = 0; i < MIN_PART; i++)
        {
            result.put(i + 1, weekTypeMap.get(i * partsNumberFrom / MIN_PART + 1));
        }

        return result;
    }

    /* Преобразует разбиение от подневного к требуемому */
    private Map<Integer, EppWeekType> transformFromOneDayPartition(Map<Integer, EppWeekType> weekTypeMap, int partsNumberTo)
    {
        // 6 -> 3:      1 -> 1      2 -> 1      3 -> 2      4 -> 2      5 -> 3      6 -> 3
        // 6 -> 2:      1 -> 1      2 -> 1      3 -> 1      4 -> 2      5 -> 2      6 -> 2
        Map<Integer, EppWeekType> result = new HashMap<>();
        for (int i = 0; i < partsNumberTo; i++)
        {
            result.put(i + 1, weekTypeMap.get(i * MIN_PART / partsNumberTo  + 1));
        }

        return result;
    }
}