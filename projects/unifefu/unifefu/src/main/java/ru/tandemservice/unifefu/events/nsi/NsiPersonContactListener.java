/* $Id$ */
package ru.tandemservice.unifefu.events.nsi;

import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressString;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact;
import ru.tandemservice.unifefu.entity.ws.MdbViewAddress;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry V. Seleznev
 * @since 11.01.2015
 */
public class NsiPersonContactListener extends ParamTransactionCompleteListener<Boolean>
{
    public static Set<Long> JUST_CHANGED_CONTACT_ID_SET = new HashSet<>();

    @SuppressWarnings("unchecked")
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, FefuNsiPersonContact.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, FefuNsiPersonContact.class, this);
        //TODO обработка удаления
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {

        List<FefuNsiPersonContact> contactsList = new DQLSelectBuilder().fromEntity(FefuNsiPersonContact.class, "e").column(property("e"))
                .where(in(property(FefuNsiPersonContact.id().fromAlias("e")), params))
                .where(or(
                        isNotNull(property(FefuNsiPersonContact.address().fromAlias("e"))),
                        isNotNull(property(FefuNsiPersonContact.field().fromAlias("e")))
                ))
                .createStatement(session).list();

        // Предварительно отсеиваем все контакты НСИ, не влияющие на изменения в реальных объектах с адресами и контактами персоны
        List<FefuNsiPersonContact> preProcessedContactList = new ArrayList<>();
        for (FefuNsiPersonContact contact : contactsList)
        {
            if (!JUST_CHANGED_CONTACT_ID_SET.contains(contact.getId()))
            {
                if (null != contact.getAddress() && null != contact.getType()) preProcessedContactList.add(contact);
                if (null != contact.getField()) preProcessedContactList.add(contact);
            } else JUST_CHANGED_CONTACT_ID_SET.remove(contact.getId());
        }


        // Обрабатываем связанные адреса и контакты персоны по пришедшим из НСИ
        Set<Long> addressToDelIdSet = new HashSet<>();
        Map<Long, PersonContactData> contactDataMap = new HashMap<>();

        for (FefuNsiPersonContact contact : preProcessedContactList)
        {
            if (null != contact.getField())
            {
                PersonContactData contactData = contactDataMap.get(contact.getPerson().getContactData().getId());
                if (null == contactData) contactData = contact.getPerson().getContactData();
                contactDataMap.put(contactData.getId(), contactData);

                if (PersonContactData.P_PHONE_REG.equals(contact.getField()))
                    contactData.setPhoneReg(contact.getDescription());
                if (PersonContactData.P_PHONE_REG_TEMP.equals(contact.getField()))
                    contactData.setPhoneRegTemp(contact.getDescription());
                if (PersonContactData.P_PHONE_FACT.equals(contact.getField()))
                    contactData.setPhoneFact(contact.getDescription());
                if (PersonContactData.P_PHONE_DEFAULT.equals(contact.getField()))
                    contactData.setPhoneDefault(contact.getDescription());
                if (PersonContactData.P_PHONE_WORK.equals(contact.getField()))
                    contactData.setPhoneWork(contact.getDescription());
                if (PersonContactData.P_PHONE_MOBILE.equals(contact.getField()))
                    contactData.setPhoneMobile(contact.getDescription());
                if (PersonContactData.P_PHONE_RELATIVES.equals(contact.getField()))
                    contactData.setPhoneRelatives(contact.getDescription());
            }

            if (null != contact.getAddress())
            {
                AddressString addr = contact.getAddress() instanceof AddressString ? (AddressString) contact.getAddress() : new AddressString();
                addr.setAddress(contact.getDescription());
                session.saveOrUpdate(addr);
                session.flush();

                AddressListener.JUST_CHANGED_CONTACT_ID_SET.add(addr.getId());

                if (!(contact.getAddress() instanceof AddressString))
                {
                    addressToDelIdSet.add(contact.getAddress().getId());

                    new DQLUpdateBuilder(Person.class).set(Person.L_ADDRESS, value(addr))
                            .where(eq(property(Person.address().id()), value(contact.getAddress().getId())))
                            .createStatement(session).execute();

                    new DQLUpdateBuilder(IdentityCard.class).set(IdentityCard.L_ADDRESS, value(addr))
                            .where(eq(property(IdentityCard.address().id()), value(contact.getAddress().getId())))
                            .createStatement(session).execute();

                    new DQLUpdateBuilder(MdbViewAddress.class).set(MdbViewAddress.L_ADDRESS_BASE, value(addr))
                            .where(eq(property(MdbViewAddress.addressBase().id()), value(contact.getAddress().getId())))
                            .createStatement(session).execute();

                    new DQLUpdateBuilder(FefuNsiPersonContact.class).set(FefuNsiPersonContact.L_ADDRESS, value(addr))
                            .where(eq(property(FefuNsiPersonContact.address().id()), value(contact.getAddress().getId())))
                            .createStatement(session).execute();

                    AddressListener.JUST_CHANGED_CONTACT_ID_SET.add(contact.getAddress().getId());
                }
            }
        }

        // Сохраняем изменения и вносим в сет исключений для листенера, обрабатывающего изменения контактов персоны дабы исключить зацикливание листенеров
        for (PersonContactData contactData : contactDataMap.values())
        {
            session.saveOrUpdate(contactData);
            ContactDataListener.JUST_CHANGED_CONTACT_ID_SET.add(contactData.getId());
        }

        session.flush();

        return true;
    }
}