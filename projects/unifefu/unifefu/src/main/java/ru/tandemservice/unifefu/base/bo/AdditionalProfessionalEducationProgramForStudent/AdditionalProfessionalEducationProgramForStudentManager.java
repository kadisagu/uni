package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic.ApeProgramForStudentDao;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic.IApeProgramForStudentDao;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.catalog.FefuIndividualDevelopConditionApe;
import ru.tandemservice.unifefu.entity.catalog.FefuIndividualDevelopFormApe;
import ru.tandemservice.unifefu.entity.catalog.FefuIndividualDevelopTechApe;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic.Constants.EDUCATION_ORG_UNIT;

@Configuration
public class AdditionalProfessionalEducationProgramForStudentManager extends BusinessObjectManager
{
    public static AdditionalProfessionalEducationProgramForStudentManager instance()
    {
        return instance(AdditionalProfessionalEducationProgramForStudentManager.class);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> additionalProfessionalEducationProgramDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FefuAdditionalProfessionalEducationProgram.class)
        {

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                EducationOrgUnit educationOrgUnit = context.get(EDUCATION_ORG_UNIT);
                if (educationOrgUnit != null)
                {
                    dql.where(eq(property(FefuAdditionalProfessionalEducationProgram.educationOrgUnit().fromAlias(alias)), value(educationOrgUnit)));
                }
            }
        };
    }

    @Bean
    public IApeProgramForStudentDao apeProgramForStudentDao()
    {
        return new ApeProgramForStudentDao();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> individualDevelopFormDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuIndividualDevelopFormApe.class, FefuIndividualDevelopFormApe.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> individualDevelopConditionDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuIndividualDevelopConditionApe.class, FefuIndividualDevelopConditionApe.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> individualDevelopTechDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuIndividualDevelopTechApe.class, FefuIndividualDevelopTechApe.title());
    }
}
