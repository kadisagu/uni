package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuPortalNotification;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Уведомление для пользователя Портала ДВФУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuPortalNotificationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuPortalNotification";
    public static final String ENTITY_NAME = "fefuPortalNotification";
    public static final int VERSION_HASH = 364635814;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON_GUID = "personGuid";
    public static final String L_PERSON = "person";
    public static final String P_SUBJECT = "subject";
    public static final String P_TEXT = "text";
    public static final String P_LINK = "link";
    public static final String P_SENT = "sent";
    public static final String P_PORTAL_ID = "portalId";

    private FefuNsiIds _personGuid;     // Идентификатор сущности в НСИ
    private Person _person;     // Персона (физическое лицо)
    private String _subject;     // Тема уведомления
    private String _text;     // Текст уведомления
    private String _link;     // Ссылка на ресурс
    private boolean _sent;     // Запрос отправлен на портал
    private String _portalId;     // Идентификатор на портале

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор сущности в НСИ. Свойство не может быть null.
     */
    @NotNull
    public FefuNsiIds getPersonGuid()
    {
        return _personGuid;
    }

    /**
     * @param personGuid Идентификатор сущности в НСИ. Свойство не может быть null.
     */
    public void setPersonGuid(FefuNsiIds personGuid)
    {
        dirty(_personGuid, personGuid);
        _personGuid = personGuid;
    }

    /**
     * @return Персона (физическое лицо).
     */
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Персона (физическое лицо).
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Тема уведомления. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Тема уведомления. Свойство не может быть null.
     */
    public void setSubject(String subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Текст уведомления.
     */
    public String getText()
    {
        return _text;
    }

    /**
     * @param text Текст уведомления.
     */
    public void setText(String text)
    {
        dirty(_text, text);
        _text = text;
    }

    /**
     * @return Ссылка на ресурс.
     */
    @Length(max=255)
    public String getLink()
    {
        return _link;
    }

    /**
     * @param link Ссылка на ресурс.
     */
    public void setLink(String link)
    {
        dirty(_link, link);
        _link = link;
    }

    /**
     * @return Запрос отправлен на портал. Свойство не может быть null.
     */
    @NotNull
    public boolean isSent()
    {
        return _sent;
    }

    /**
     * @param sent Запрос отправлен на портал. Свойство не может быть null.
     */
    public void setSent(boolean sent)
    {
        dirty(_sent, sent);
        _sent = sent;
    }

    /**
     * @return Идентификатор на портале.
     */
    @Length(max=255)
    public String getPortalId()
    {
        return _portalId;
    }

    /**
     * @param portalId Идентификатор на портале.
     */
    public void setPortalId(String portalId)
    {
        dirty(_portalId, portalId);
        _portalId = portalId;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuPortalNotificationGen)
        {
            setPersonGuid(((FefuPortalNotification)another).getPersonGuid());
            setPerson(((FefuPortalNotification)another).getPerson());
            setSubject(((FefuPortalNotification)another).getSubject());
            setText(((FefuPortalNotification)another).getText());
            setLink(((FefuPortalNotification)another).getLink());
            setSent(((FefuPortalNotification)another).isSent());
            setPortalId(((FefuPortalNotification)another).getPortalId());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuPortalNotificationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuPortalNotification.class;
        }

        public T newInstance()
        {
            return (T) new FefuPortalNotification();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "personGuid":
                    return obj.getPersonGuid();
                case "person":
                    return obj.getPerson();
                case "subject":
                    return obj.getSubject();
                case "text":
                    return obj.getText();
                case "link":
                    return obj.getLink();
                case "sent":
                    return obj.isSent();
                case "portalId":
                    return obj.getPortalId();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "personGuid":
                    obj.setPersonGuid((FefuNsiIds) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "subject":
                    obj.setSubject((String) value);
                    return;
                case "text":
                    obj.setText((String) value);
                    return;
                case "link":
                    obj.setLink((String) value);
                    return;
                case "sent":
                    obj.setSent((Boolean) value);
                    return;
                case "portalId":
                    obj.setPortalId((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "personGuid":
                        return true;
                case "person":
                        return true;
                case "subject":
                        return true;
                case "text":
                        return true;
                case "link":
                        return true;
                case "sent":
                        return true;
                case "portalId":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "personGuid":
                    return true;
                case "person":
                    return true;
                case "subject":
                    return true;
                case "text":
                    return true;
                case "link":
                    return true;
                case "sent":
                    return true;
                case "portalId":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "personGuid":
                    return FefuNsiIds.class;
                case "person":
                    return Person.class;
                case "subject":
                    return String.class;
                case "text":
                    return String.class;
                case "link":
                    return String.class;
                case "sent":
                    return Boolean.class;
                case "portalId":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuPortalNotification> _dslPath = new Path<FefuPortalNotification>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuPortalNotification");
    }
            

    /**
     * @return Идентификатор сущности в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getPersonGuid()
     */
    public static FefuNsiIds.Path<FefuNsiIds> personGuid()
    {
        return _dslPath.personGuid();
    }

    /**
     * @return Персона (физическое лицо).
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Тема уведомления. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getSubject()
     */
    public static PropertyPath<String> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Текст уведомления.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getText()
     */
    public static PropertyPath<String> text()
    {
        return _dslPath.text();
    }

    /**
     * @return Ссылка на ресурс.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getLink()
     */
    public static PropertyPath<String> link()
    {
        return _dslPath.link();
    }

    /**
     * @return Запрос отправлен на портал. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#isSent()
     */
    public static PropertyPath<Boolean> sent()
    {
        return _dslPath.sent();
    }

    /**
     * @return Идентификатор на портале.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getPortalId()
     */
    public static PropertyPath<String> portalId()
    {
        return _dslPath.portalId();
    }

    public static class Path<E extends FefuPortalNotification> extends EntityPath<E>
    {
        private FefuNsiIds.Path<FefuNsiIds> _personGuid;
        private Person.Path<Person> _person;
        private PropertyPath<String> _subject;
        private PropertyPath<String> _text;
        private PropertyPath<String> _link;
        private PropertyPath<Boolean> _sent;
        private PropertyPath<String> _portalId;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор сущности в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getPersonGuid()
     */
        public FefuNsiIds.Path<FefuNsiIds> personGuid()
        {
            if(_personGuid == null )
                _personGuid = new FefuNsiIds.Path<FefuNsiIds>(L_PERSON_GUID, this);
            return _personGuid;
        }

    /**
     * @return Персона (физическое лицо).
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Тема уведомления. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getSubject()
     */
        public PropertyPath<String> subject()
        {
            if(_subject == null )
                _subject = new PropertyPath<String>(FefuPortalNotificationGen.P_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Текст уведомления.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getText()
     */
        public PropertyPath<String> text()
        {
            if(_text == null )
                _text = new PropertyPath<String>(FefuPortalNotificationGen.P_TEXT, this);
            return _text;
        }

    /**
     * @return Ссылка на ресурс.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getLink()
     */
        public PropertyPath<String> link()
        {
            if(_link == null )
                _link = new PropertyPath<String>(FefuPortalNotificationGen.P_LINK, this);
            return _link;
        }

    /**
     * @return Запрос отправлен на портал. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#isSent()
     */
        public PropertyPath<Boolean> sent()
        {
            if(_sent == null )
                _sent = new PropertyPath<Boolean>(FefuPortalNotificationGen.P_SENT, this);
            return _sent;
        }

    /**
     * @return Идентификатор на портале.
     * @see ru.tandemservice.unifefu.entity.ws.FefuPortalNotification#getPortalId()
     */
        public PropertyPath<String> portalId()
        {
            if(_portalId == null )
                _portalId = new PropertyPath<String>(FefuPortalNotificationGen.P_PORTAL_ID, this);
            return _portalId;
        }

        public Class getEntityClass()
        {
            return FefuPortalNotification.class;
        }

        public String getEntityName()
        {
            return "fefuPortalNotification";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
