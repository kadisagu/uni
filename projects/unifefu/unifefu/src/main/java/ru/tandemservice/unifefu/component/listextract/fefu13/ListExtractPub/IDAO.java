package ru.tandemservice.unifefu.component.listextract.fefu13.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuAdditionalAcademGrantStuListExtract, Model>
{
}
