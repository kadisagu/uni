/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionScheduleTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.util.WeekTypeLegendRow;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionPartitionType;
import ru.tandemservice.unifefu.tapestry.richTableList.FefuRangeSelectionWeekTypeListDataSource;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 06.06.2013
 */
@Input
(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
    private Long _id;
    private EppEduPlanVersion _version;
    private FefuEduPlanVersionPartitionType _versionPartitionType;
    private FefuRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> _scheduleDataSource; // таблица учебного графика

    private List<WeekTypeLegendRow> _weekTypeLegendList;          // строки легенды
    private WeekTypeLegendRow _weekTypeLegendItem;                // текущее значение цикла

    private StaticListDataSource<FefuSummaryBudgetRowWrapper> _summaryBudgetDataSource;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public EppEduPlanVersion getVersion()
    {
        return _version;
    }

    public void setVersion(EppEduPlanVersion version)
    {
        _version = version;
    }

    public FefuEduPlanVersionPartitionType getVersionPartitionType()
    {
        return _versionPartitionType;
    }

    public void setVersionPartitionType(FefuEduPlanVersionPartitionType versionPartitionType)
    {
        _versionPartitionType = versionPartitionType;
    }

    public FefuRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> getScheduleDataSource()
    {
        return _scheduleDataSource;
    }

    public void setScheduleDataSource(FefuRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> scheduleDataSource)
    {
        _scheduleDataSource = scheduleDataSource;
    }

    public List<WeekTypeLegendRow> getWeekTypeLegendList()
    {
        return _weekTypeLegendList;
    }

    public void setWeekTypeLegendList(List<WeekTypeLegendRow> weekTypeLegendList)
    {
        _weekTypeLegendList = weekTypeLegendList;
    }

    public WeekTypeLegendRow getWeekTypeLegendItem()
    {
        return _weekTypeLegendItem;
    }

    public void setWeekTypeLegendItem(WeekTypeLegendRow weekTypeLegendItem)
    {
        _weekTypeLegendItem = weekTypeLegendItem;
    }

    public StaticListDataSource<FefuSummaryBudgetRowWrapper> getSummaryBudgetDataSource(){ return _summaryBudgetDataSource; }
    public void setSummaryBudgetDataSource(StaticListDataSource<FefuSummaryBudgetRowWrapper> summaryBudgetDataSource){ _summaryBudgetDataSource = summaryBudgetDataSource; }
}