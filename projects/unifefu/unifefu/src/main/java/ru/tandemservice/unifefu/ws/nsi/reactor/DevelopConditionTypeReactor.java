/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.DevelopConditionType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class DevelopConditionTypeReactor extends SimpleCatalogEntityNewReactor<DevelopConditionType, DevelopCondition>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<DevelopCondition> getEntityClass()
    {
        return DevelopCondition.class;
    }

    @Override
    public Class<DevelopConditionType> getNSIEntityClass()
    {
        return DevelopConditionType.class;
    }

    @Override
    public DevelopConditionType getCatalogElementRetrieveRequestObject(String guid)
    {
        DevelopConditionType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createDevelopConditionType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public DevelopConditionType createEntity(CoreCollectionUtils.Pair<DevelopCondition, FefuNsiIds> entityPair)
    {
        DevelopConditionType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createDevelopConditionType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setDevelopconditionID(entityPair.getX().getCode());
        nsiEntity.setDevelopConditionName(entityPair.getX().getTitle());
        nsiEntity.setDevelopConditionNameShort(entityPair.getX().getShortTitle());
        return nsiEntity;
    }

    @Override
    public DevelopCondition createEntity(DevelopConditionType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getDevelopConditionName()) return null;

        DevelopCondition entity = new DevelopCondition();
        entity.setTitle(nsiEntity.getDevelopConditionName());
        entity.setShortTitle(nsiEntity.getDevelopConditionNameShort());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(DevelopCondition.class));

        return entity;
    }

    @Override
    public DevelopCondition updatePossibleDuplicateFields(DevelopConditionType nsiEntity, CoreCollectionUtils.Pair<DevelopCondition, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), DevelopCondition.title().s()))
                entityPair.getX().setTitle(nsiEntity.getDevelopConditionName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), DevelopCondition.shortTitle().s()))
                entityPair.getX().setShortTitle(nsiEntity.getDevelopConditionNameShort());
        }
        return entityPair.getX();
    }

    @Override
    public DevelopConditionType updateNsiEntityFields(DevelopConditionType nsiEntity, DevelopCondition entity)
    {
        nsiEntity.setDevelopconditionID(entity.getCode());
        nsiEntity.setDevelopConditionName(entity.getTitle());
        nsiEntity.setDevelopConditionNameShort(entity.getShortTitle());
        return nsiEntity;
    }
}