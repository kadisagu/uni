package ru.tandemservice.unifefu.dao.registry;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 17.11.2014
 */
public interface IFefuEppRegistryDAO extends IEppRegistryDAO
{
    public static final SpringBeanCache<IFefuEppRegistryDAO> instance = new SpringBeanCache<>(IFefuEppRegistryDAO.class.getName());

    /**
     * Возвращает мап связей дисциплины со списком РУП.
     *
     * @param disciplineIds ids элементов реестра
     * @return { discipline.id -> { EppWorkPlanRegistryElementRow.ids }}
     */
    Map<Long, List<Long>> getRegistryElement2WorkPlanRelMap(Collection<Long> disciplineIds);


    /**
     * Возвращает мап связей дисциплины со списком УПв.
     *
     * @param disciplineIds ids элементов реестра
     * @return { discipline.id -> { EppEpvRegistryRow.ids }}
     */
    Map<Long, List<Long>> getRegistryElement2EduPlanVersionRelMap(Collection<Long> disciplineIds);
}