package ru.tandemservice.unifefu.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport;
import ru.tandemservice.unifefu.entity.report.IFefuBrsReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Рейтинг группы по всем дисциплинам»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuRatingGroupAllDiscReportGen extends EntityBase
 implements IFefuBrsReport{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport";
    public static final String ENTITY_NAME = "fefuRatingGroupAllDiscReport";
    public static final int VERSION_HASH = -512367915;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_YEAR_PART = "yearPart";
    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_RESPONSIBILITY_ORG_UNIT = "responsibilityOrgUnit";
    public static final String P_GROUP = "group";
    public static final String P_ONLY_FILLED_JOURNALS = "onlyFilledJournals";
    public static final String P_FORMING_DATE_STR = "formingDateStr";

    private OrgUnit _orgUnit;     // Подразделение
    private String _yearPart;     // Часть учебного года
    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private String _formativeOrgUnit;     // Формирующее подразделение
    private String _responsibilityOrgUnit;     // Ответственное подразделение
    private String _group;     // Академ. группа
    private Boolean _onlyFilledJournals;     // Учитывать только заполненные журналы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Часть учебного года.
     */
    @Length(max=255)
    public String getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть учебного года.
     */
    public void setYearPart(String yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Формирующее подразделение.
     */
    @Length(max=255)
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение.
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Ответственное подразделение.
     */
    @Length(max=255)
    public String getResponsibilityOrgUnit()
    {
        return _responsibilityOrgUnit;
    }

    /**
     * @param responsibilityOrgUnit Ответственное подразделение.
     */
    public void setResponsibilityOrgUnit(String responsibilityOrgUnit)
    {
        dirty(_responsibilityOrgUnit, responsibilityOrgUnit);
        _responsibilityOrgUnit = responsibilityOrgUnit;
    }

    /**
     * @return Академ. группа.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Академ. группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Учитывать только заполненные журналы.
     */
    public Boolean getOnlyFilledJournals()
    {
        return _onlyFilledJournals;
    }

    /**
     * @param onlyFilledJournals Учитывать только заполненные журналы.
     */
    public void setOnlyFilledJournals(Boolean onlyFilledJournals)
    {
        dirty(_onlyFilledJournals, onlyFilledJournals);
        _onlyFilledJournals = onlyFilledJournals;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuRatingGroupAllDiscReportGen)
        {
            setOrgUnit(((FefuRatingGroupAllDiscReport)another).getOrgUnit());
            setYearPart(((FefuRatingGroupAllDiscReport)another).getYearPart());
            setContent(((FefuRatingGroupAllDiscReport)another).getContent());
            setFormingDate(((FefuRatingGroupAllDiscReport)another).getFormingDate());
            setFormativeOrgUnit(((FefuRatingGroupAllDiscReport)another).getFormativeOrgUnit());
            setResponsibilityOrgUnit(((FefuRatingGroupAllDiscReport)another).getResponsibilityOrgUnit());
            setGroup(((FefuRatingGroupAllDiscReport)another).getGroup());
            setOnlyFilledJournals(((FefuRatingGroupAllDiscReport)another).getOnlyFilledJournals());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuRatingGroupAllDiscReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuRatingGroupAllDiscReport.class;
        }

        public T newInstance()
        {
            return (T) new FefuRatingGroupAllDiscReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "yearPart":
                    return obj.getYearPart();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "responsibilityOrgUnit":
                    return obj.getResponsibilityOrgUnit();
                case "group":
                    return obj.getGroup();
                case "onlyFilledJournals":
                    return obj.getOnlyFilledJournals();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "yearPart":
                    obj.setYearPart((String) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "responsibilityOrgUnit":
                    obj.setResponsibilityOrgUnit((String) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
                case "onlyFilledJournals":
                    obj.setOnlyFilledJournals((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "yearPart":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "responsibilityOrgUnit":
                        return true;
                case "group":
                        return true;
                case "onlyFilledJournals":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "yearPart":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "responsibilityOrgUnit":
                    return true;
                case "group":
                    return true;
                case "onlyFilledJournals":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "yearPart":
                    return String.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "formativeOrgUnit":
                    return String.class;
                case "responsibilityOrgUnit":
                    return String.class;
                case "group":
                    return String.class;
                case "onlyFilledJournals":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuRatingGroupAllDiscReport> _dslPath = new Path<FefuRatingGroupAllDiscReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuRatingGroupAllDiscReport");
    }
            

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getYearPart()
     */
    public static PropertyPath<String> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Ответственное подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getResponsibilityOrgUnit()
     */
    public static PropertyPath<String> responsibilityOrgUnit()
    {
        return _dslPath.responsibilityOrgUnit();
    }

    /**
     * @return Академ. группа.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Учитывать только заполненные журналы.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getOnlyFilledJournals()
     */
    public static PropertyPath<Boolean> onlyFilledJournals()
    {
        return _dslPath.onlyFilledJournals();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getFormingDateStr()
     */
    public static SupportedPropertyPath<String> formingDateStr()
    {
        return _dslPath.formingDateStr();
    }

    public static class Path<E extends FefuRatingGroupAllDiscReport> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _yearPart;
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _responsibilityOrgUnit;
        private PropertyPath<String> _group;
        private PropertyPath<Boolean> _onlyFilledJournals;
        private SupportedPropertyPath<String> _formingDateStr;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getYearPart()
     */
        public PropertyPath<String> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new PropertyPath<String>(FefuRatingGroupAllDiscReportGen.P_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(FefuRatingGroupAllDiscReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(FefuRatingGroupAllDiscReportGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Ответственное подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getResponsibilityOrgUnit()
     */
        public PropertyPath<String> responsibilityOrgUnit()
        {
            if(_responsibilityOrgUnit == null )
                _responsibilityOrgUnit = new PropertyPath<String>(FefuRatingGroupAllDiscReportGen.P_RESPONSIBILITY_ORG_UNIT, this);
            return _responsibilityOrgUnit;
        }

    /**
     * @return Академ. группа.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(FefuRatingGroupAllDiscReportGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Учитывать только заполненные журналы.
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getOnlyFilledJournals()
     */
        public PropertyPath<Boolean> onlyFilledJournals()
        {
            if(_onlyFilledJournals == null )
                _onlyFilledJournals = new PropertyPath<Boolean>(FefuRatingGroupAllDiscReportGen.P_ONLY_FILLED_JOURNALS, this);
            return _onlyFilledJournals;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport#getFormingDateStr()
     */
        public SupportedPropertyPath<String> formingDateStr()
        {
            if(_formingDateStr == null )
                _formingDateStr = new SupportedPropertyPath<String>(FefuRatingGroupAllDiscReportGen.P_FORMING_DATE_STR, this);
            return _formingDateStr;
        }

        public Class getEntityClass()
        {
            return FefuRatingGroupAllDiscReport.class;
        }

        public String getEntityName()
        {
            return "fefuRatingGroupAllDiscReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFormingDateStr();
}
