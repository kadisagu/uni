/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.RegenGuids;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;

/**
 * @author Dmitry Seleznev
 * @since 10.06.2014
 */
public class FefuSystemActionRegenGuidsUI extends UIPresenter
{
    private IUploadFile _file;

    // Getters & Setters

    public IUploadFile getFile()
    {
        return _file;
    }

    public void setFile(IUploadFile file)
    {
        _file = file;
    }


    // Listeners

    public void onClickApply() throws Exception
    {
        ErrorCollector err = UserContext.getInstance().getErrorCollector();

        if (_file.getSize() > 10 * 1024 * 1024)
            err.add("Размер загружаемого файла не может превышать 10 МБ.", "file");

        String fileContent = IOUtils.toString(_file.getStream(), "Utf-8").trim();
        if (fileContent.length() == 0) err.add("Файл не должен быть пустым.", "file");

        if (err.isHasFieldErrors()) return;

        FefuSystemActionManager.instance().dao().doRegenBadIdentityCardIdsByFile(fileContent);

        deactivate();
    }
}
