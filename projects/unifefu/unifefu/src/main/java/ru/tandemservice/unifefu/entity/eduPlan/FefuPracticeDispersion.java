package ru.tandemservice.unifefu.entity.eduPlan;

import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.unifefu.entity.eduPlan.gen.*;

/**
 * Рассредоточенность практики (ДВФУ)
 */
public class FefuPracticeDispersion extends FefuPracticeDispersionGen
{
    public FefuPracticeDispersion()
    {

    }

    public FefuPracticeDispersion(EppEpvRegistryRow practice, boolean dispersed)
    {
        this.setPractice(practice);
        this.setDispersed(dispersed);
    }
}