/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 16.07.2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Object", propOrder = {})
public abstract class ObjectType
{
    @XmlAttribute(name = "Type")
    protected String type;
    @XmlAttribute(name = "VED")
    protected String ved;
    @XmlAttribute(name = "Name")
    protected String name;
    @XmlAttribute(name = "Editor")
    protected String editor;
    @XmlAttribute(name = "RouteCode")
    protected String routeCode;

    @XmlElements({
            @XmlElement(name = "Section", type = SectionType.class),
            @XmlElement(name = "WorkflowParams", type = WorkflowParamsType.class),
            @XmlElement(name = "Attachments", type = AttachmentsType.class)
    })
    protected List<Object> childs = new ArrayList<>();

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getVed()
    {
        return ved;
    }

    public void setVed(String ved)
    {
        this.ved = ved;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEditor()
    {
        return editor;
    }

    public void setEditor(String editor)
    {
        this.editor = editor;
    }

    public String getRouteCode()
    {
        return routeCode;
    }

    public void setRouteCode(String routeCode)
    {
        this.routeCode = routeCode;
    }

    public List<Object> getChilds()
    {
        return childs;
    }

    public void setChilds(List<Object> childs)
    {
        this.childs = childs;
    }
}