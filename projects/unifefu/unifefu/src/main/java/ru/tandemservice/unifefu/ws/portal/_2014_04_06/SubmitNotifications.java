/**
 * SubmitNotifications.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.portal._2014_04_06;

public interface SubmitNotifications extends java.rmi.Remote
{
    public Commit[] submitNotifications(Notification[] notifications) throws java.rmi.RemoteException, SubmitNotificationsFault;
}