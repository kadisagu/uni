/**
 *$Id: FefuUserManualList.java 38584 2014-10-08 11:05:36Z azhebko $
 */
package ru.tandemservice.unifefu.base.bo.FefuUserManual.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.UserManual;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Rostuncev
 * @since 28.11.12
 */
@Configuration
public class FefuUserManualList extends BusinessComponentManager
{
    public static final String USER_MANUAL_SEARCH_DS = "userManualSearchDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(USER_MANUAL_SEARCH_DS, userManualSearchDSColumns(), userManualSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint userManualSearchDSColumns()
    {
        return columnListExtPointBuilder(USER_MANUAL_SEARCH_DS)
                .addColumn(textColumn("title", UserManual.title()).order())
                .addColumn(dateColumn("editDate", UserManual.editDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn("comment", UserManual.comment()).order())
                .addColumn(actionColumn("download", new Icon("template_save"), "onClickDownload"))// прав на скачивание нет, т.к. в списке отображаются только те элементы на которые у пользователя есть права
                .addColumn(actionColumn("edit", new Icon("template_add"), "onClickEdit").permissionKey("userManual_edit, userManual_admin"))
                .addColumn(actionColumn("delete", new Icon("delete"), "onClickDelete").alert(alert("userManualSearchDS.delete.alert", UserManual.title().s())).permissionKey("userManual_delete, userManual_admin").disabled("ui:deleteDisabled"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> userManualSearchDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), UserManual.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final String title = context.get("title");
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UserManual.class, "b").column(property("b"));
                    if (title != null)
                        builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property("b", UserManual.title())), DQLExpressions.value(CoreStringUtils.escapeLike(title, true))));

                    if (input.getEntityOrder().getColumnName().equals("comment"))
                    {
                        final List<UserManual> resultList = builder.createStatement(context.getSession()).list();
                        Collections.sort(resultList, new EntityComparator<>(input.getEntityOrder()));
                        return ListOutputBuilder.get(input, resultList).pageable(true).build();
                    }

                    new DQLOrderDescriptionRegistry(UserManual.class, "b").applyOrder(builder, input.getEntityOrder());
                    return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
                }
        };
    }
}
