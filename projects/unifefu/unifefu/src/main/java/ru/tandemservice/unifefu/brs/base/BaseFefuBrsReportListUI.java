/* $Id$ */
package ru.tandemservice.unifefu.brs.base;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unifefu.entity.report.IFefuBrsReport;

/**
 * @author Nikolay Fedorovskih
 * @since 23.05.2014
 */
public abstract class BaseFefuBrsReportListUI extends BaseBrsReportPresenter
{
    public static final String REPORT_DS = "reportDS";

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (REPORT_DS.equals(dataSource.getName()))
        {
            dataSource.put(FefuBrsReportListHandler.ORGUNIT_ID_PARAM, getOrgUnitId());
            dataSource.put(FefuBrsReportListHandler.YEAR_PART_PARAM, getSettings().get("yearPart"));
        }
    }

    public void onClickAddReport()
    {
        _uiActivation.asRegion(getClass().getSimpleName().replaceFirst("ListUI", "Add"))
                .parameter(BaseFefuReportAddUI.YEAR_PART_BIND_PARAM, getSettings().get("yearPart"))
                .activate();
    }

    public void onClickPrint() throws Exception
    {
        IFefuBrsReport report = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        byte[] content = report.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл отчета пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Report.rtf").document(content), true);
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
