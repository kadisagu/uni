/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.user;

import ru.tandemservice.unifefu.ws.blackboard.BBConstants;
import ru.tandemservice.unifefu.ws.blackboard.BBContextHelper;
import ru.tandemservice.unifefu.ws.blackboard.HeaderHandlerResolver;
import ru.tandemservice.unifefu.ws.blackboard.PortTypeInitializer;
import ru.tandemservice.unifefu.ws.blackboard.user.gen.ObjectFactory;
import ru.tandemservice.unifefu.ws.blackboard.user.gen.UserWS;
import ru.tandemservice.unifefu.ws.blackboard.user.gen.UserWSPortType;

/**
 * @author Nikolay Fedorovskih
 * @since 16.03.2014
 */
public class BBUserInitializer extends PortTypeInitializer<UserWSPortType, ObjectFactory>
{
    private UserWS _userWS;

    public BBUserInitializer(HeaderHandlerResolver headerHandlerResolver)
    {
        super(BBConstants.USER_WS, headerHandlerResolver);
        _userWS = new UserWS(BBContextHelper.getWSDL_resource_URL("User.wsdl"));
        _userWS.setHandlerResolver(headerHandlerResolver);
    }

    @Override
    protected UserWSPortType initPortType()
    {
        return _userWS.getUserWSSOAP11PortHttp();
    }

    @Override
    protected ObjectFactory initObjectFactory()
    {
        return new ObjectFactory();
    }
}