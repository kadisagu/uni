/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleDaily.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyPrintParams;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 07.09.2016
 */
public class FefuScheduleDailyComboDSHandler extends SppScheduleDailyComboDSHandler
{
    public FefuScheduleDailyComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    protected void addAdditionalFilters(DQLSelectBuilder builder, SppScheduleDailyPrintParams scheduleData)
    {
        builder.where(eq(property("s", SppScheduleDaily.group().educationOrgUnit().territorialOrgUnit()), value(scheduleData.getTerritorialOrgUnit())));
    }
}
