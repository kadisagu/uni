package ru.tandemservice.unifefu.entity.entrantOrder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.entity.entrantOrder.EntrFefuSPOBudgetExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О зачислении в число студентов СПО (бюджет)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrFefuSPOBudgetExtractGen extends EnrollmentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.entrantOrder.EntrFefuSPOBudgetExtract";
    public static final String ENTITY_NAME = "entrFefuSPOBudgetExtract";
    public static final int VERSION_HASH = 1942678964;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EntrFefuSPOBudgetExtractGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrFefuSPOBudgetExtractGen> extends EnrollmentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrFefuSPOBudgetExtract.class;
        }

        public T newInstance()
        {
            return (T) new EntrFefuSPOBudgetExtract();
        }
    }
    private static final Path<EntrFefuSPOBudgetExtract> _dslPath = new Path<EntrFefuSPOBudgetExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrFefuSPOBudgetExtract");
    }
            

    public static class Path<E extends EntrFefuSPOBudgetExtract> extends EnrollmentExtract.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EntrFefuSPOBudgetExtract.class;
        }

        public String getEntityName()
        {
            return "entrFefuSPOBudgetExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
