/* $Id$ */
package ru.tandemservice.unifefu.component.order.EnrollmentOrderPub;

import ru.tandemservice.unifefu.base.bo.Directum.logic.DirectumDAO;
import ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt;
import ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension;

/**
 * @author Nikolay Fedorovskih
 * @since 31.07.2013
 */
public class Model
{
    private EnrollmentOrderFefuExt orderExt;
    private FefuEntrantOrderExtension _enrOrderExt;

    public EnrollmentOrderFefuExt getOrderExt()
    {
        return orderExt;
    }

    public void setOrderExt(EnrollmentOrderFefuExt orderExt)
    {
        this.orderExt = orderExt;
    }

    public FefuEntrantOrderExtension getEnrOrderExt()
    {
        return _enrOrderExt;
    }

    public void setEnrOrderExt(FefuEntrantOrderExtension enrOrderExt)
    {
        _enrOrderExt = enrOrderExt;
    }

    public boolean isDirectumLinkAvailable()
    {
        if (null != _enrOrderExt)
        {
            String directumId = null != _enrOrderExt ? _enrOrderExt.getDirectumOrderId() : null;
            if (null != directumId) return true;
        }
        return false;
    }

    public String getOrderLink()
    {
        return DirectumDAO.getDirectumBaseUrl() + ((null != _enrOrderExt && null != _enrOrderExt.getDirectumOrderId()) ? _enrOrderExt.getDirectumOrderId() : "");
    }
}