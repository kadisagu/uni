/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Dmitry Seleznev
 * @since 16.07.2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Attachment", propOrder = {})
public class AttachmentType
{
    public static final String EDOCUMENT_TYPE = "EDocument";

    @XmlAttribute(name = "Type")
    protected String type = EDOCUMENT_TYPE;
    @XmlAttribute(name = "ID")
    protected String id;
    @XmlAttribute(name = "Name")
    protected String name;

    public AttachmentType()
    {
    }

    public AttachmentType(String id)
    {
        this.id = id;
        this.name = "";
    }

    public AttachmentType(String id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}