package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unifefu.entity.ws.FefuEmployeePostExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сотрудника для ДВФУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEmployeePostExtGen extends EntityBase
 implements INaturalIdentifiable<FefuEmployeePostExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuEmployeePostExt";
    public static final String ENTITY_NAME = "fefuEmployeePostExt";
    public static final int VERSION_HASH = -730483283;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String P_RATE = "rate";

    private EmployeePost _employeePost;     // Сотрудник
    private Integer _rate;     // Ставка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null и должно быть уникальным.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Ставка.
     */
    public Integer getRate()
    {
        return _rate;
    }

    /**
     * @param rate Ставка.
     */
    public void setRate(Integer rate)
    {
        dirty(_rate, rate);
        _rate = rate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEmployeePostExtGen)
        {
            if (withNaturalIdProperties)
            {
                setEmployeePost(((FefuEmployeePostExt)another).getEmployeePost());
            }
            setRate(((FefuEmployeePostExt)another).getRate());
        }
    }

    public INaturalId<FefuEmployeePostExtGen> getNaturalId()
    {
        return new NaturalId(getEmployeePost());
    }

    public static class NaturalId extends NaturalIdBase<FefuEmployeePostExtGen>
    {
        private static final String PROXY_NAME = "FefuEmployeePostExtNaturalProxy";

        private Long _employeePost;

        public NaturalId()
        {}

        public NaturalId(EmployeePost employeePost)
        {
            _employeePost = ((IEntity) employeePost).getId();
        }

        public Long getEmployeePost()
        {
            return _employeePost;
        }

        public void setEmployeePost(Long employeePost)
        {
            _employeePost = employeePost;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuEmployeePostExtGen.NaturalId) ) return false;

            FefuEmployeePostExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getEmployeePost(), that.getEmployeePost()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEmployeePost());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEmployeePost());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEmployeePostExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEmployeePostExt.class;
        }

        public T newInstance()
        {
            return (T) new FefuEmployeePostExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "rate":
                    return obj.getRate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "rate":
                    obj.setRate((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "rate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "rate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "rate":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEmployeePostExt> _dslPath = new Path<FefuEmployeePostExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEmployeePostExt");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEmployeePostExt#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Ставка.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEmployeePostExt#getRate()
     */
    public static PropertyPath<Integer> rate()
    {
        return _dslPath.rate();
    }

    public static class Path<E extends FefuEmployeePostExt> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private PropertyPath<Integer> _rate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEmployeePostExt#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Ставка.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEmployeePostExt#getRate()
     */
        public PropertyPath<Integer> rate()
        {
            if(_rate == null )
                _rate = new PropertyPath<Integer>(FefuEmployeePostExtGen.P_RATE, this);
            return _rate;
        }

        public Class getEntityClass()
        {
            return FefuEmployeePostExt.class;
        }

        public String getEntityName()
        {
            return "fefuEmployeePostExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
