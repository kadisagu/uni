/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.course;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.base.bo.Blackboard.BlackboardManager;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.IBlackboardDAO;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;
import ru.tandemservice.unifefu.ws.blackboard.BBContextHelper;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.CourseFilter;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.CourseVO;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.GroupVO;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.ObjectFactory;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.GradebookWSConstants;
import ru.tandemservice.unifefu.ws.blackboard.membership.BBMembershipUtils;
import ru.tandemservice.unifefu.ws.blackboard.membership.CourseMembershipWSConstants;
import ru.tandemservice.unifefu.ws.blackboard.membership.gen.CourseMembershipVO;
import ru.tandemservice.unifefu.ws.blackboard.user.BBUserUtils;
import ru.tandemservice.unifefu.ws.blackboard.user.gen.UserVO;

import javax.annotation.Nullable;
import javax.transaction.Status;
import javax.validation.constraints.NotNull;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 11.03.2014
 */
public class BBCourseUtils
{
    public static final String BB_COURSE_ID_PREFIX_LOCK = "blackboardCourseIdLock";
    public static final Predicate<CourseVO> ACTIVE_COURSE_PREDICATE = courseVO -> {
        // Архивные курсы отсеиваем по приписке в названии и флагу активности (isAvailable)
        return courseVO != null && courseVO.isAvailable() && !courseVO.getName().getValue().startsWith(IBlackboardDAO.ARCHIVE_PREFIX);
    };

    public static void registerDeleteCourseListenerIfTransactionRollback(Session session, final String bbCoursePrimaryId)
    {
        new TransactionCompleteAction<Boolean>()
        {
            @Override
            public void afterCompletion(final Session session, final int status, final Boolean beforeCompleteResult)
            {
                if (status == Status.STATUS_ROLLEDBACK)
                {
                    if (!deleteCourse(bbCoursePrimaryId))
                        Logger.getRootLogger().error("Course with id " + bbCoursePrimaryId + " is not deleted from Blackboard after transaction rollback. Please, do it by hand.");
                }
            }
        }.register(session);
    }

    /**
     * Удаление списка ЭУК
     *
     * @param coursePrimaryIds идентификаторы курсов (примари)
     * @return список идентификаторов удаленных курсов, который возвращает ВВ при удалении курсов
     */
    @NotNull
    public static List<String> deleteCourse(@NotNull final List<String> coursePrimaryIds)
    {
        return BBContextHelper.getInstance().doCourseRequest((courseWSPortType, of) -> courseWSPortType.deleteCourse(coursePrimaryIds));
    }

    /**
     * Удаление ЭУК
     *
     * @param coursePrimaryId идентификатор курса (примари)
     * @return успешно ли удален курс
     */
    public static boolean deleteCourse(@NotNull String coursePrimaryId)
    {
        List<String> ret = deleteCourse(Collections.singletonList(coursePrimaryId));
        return ret != null && ret.size() == 1 && coursePrimaryId.equals(ret.get(0));
    }

    /**
     * Удаление набора групп из ЭУК
     *
     * @param coursePrimaryId идентификатор курса (примари)
     * @param groups          список групп
     * @return удалось ли удалить
     */
    public static boolean deleteGroups(@NotNull final String coursePrimaryId, @NotNull final List<String> groups)
    {
        List<String> retList = BBContextHelper.getInstance().doCourseRequest((courseWSPortType, of) -> courseWSPortType.deleteGroup(coursePrimaryId, groups));
        return retList.size() == groups.size();
    }

    /**
     * Переименовывание курса и списывание в архив на стороне Blackboard
     *
     * @param courseID    идентификатор курса (не примари)
     * @param newName     новое название
     * @param isAvailable признак доступности - при списывании в архив передаем false (т.е. недоступным для студентов)
     */
    public static void updateCourse(@NotNull String courseID, @NotNull final String newName, final boolean isAvailable)
    {
        final CourseVO courseVO = findCourseByCourseIdNotNull(courseID);
        BBContextHelper.getInstance().doCourseRequest((courseWSPortType, of) -> {
            courseVO.setName(of.createCourseVOName(newName));
            courseVO.setAvailable(isAvailable);
            return courseWSPortType.updateCourse(courseVO);
        });
    }

    /**
     * Создание нового курса на основе элемента реестра дисциплин
     *
     * @param title           название нового курса
     * @param ppsEntryList    назначенные преподаватели
     * @param registryElement элемент реестра
     * @param description     описание нового курса
     * @return идентификатор нового курса
     * @throws ApplicationException если что-то пошло не так
     */
    @NotNull
    public static Long createNewCourse(@NotNull final String title, @NotNull final Collection<PpsEntry> ppsEntryList, @NotNull final EppRegistryElement registryElement, @Nullable final String description)
    {
        // Генерируем часть идентификатора УЭК (префикс A-B-C-)
        final String courseIdPrefix = generateCourseIdPrefixABC(registryElement);

        // Получаем юзера для преподавателя
        final Map<PpsEntry, UserVO> ppsUsersMap = BBUserUtils.findUsersNotNull(ppsEntryList);

        // Проверяем, что все преподаватели зачислены на курс по изучению BB в качестве студента
        for (UserVO userVO : ppsUsersMap.values())
        {
            BBMembershipUtils.checkInstructor(userVO);
        }

        // Далее нам всё придется делать в одной транзакции, чтобы поставить блокировку на префикс, дабы не возникло конфликта по новому идентификатору
        return DataAccessServices.dao().doInTransaction(session -> {
            // Ставим блокировку по данному префиксу
            NamedSyncInTransactionCheckLocker.register(session, BB_COURSE_ID_PREFIX_LOCK + courseIdPrefix, 120);

            final int maxNumberLength = 3; // длина постфикса (D)

            // Получаем максимальный идентификатор для данного префикса. Следующий будет +1
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(BbCourse.class, "e")
                    .column(DQLFunctions.max(property("e", BbCourse.bbCourseId())))
                    .where(gt(property("e", BbCourse.bbCourseId()), value(courseIdPrefix + StringUtils.repeat("0", maxNumberLength))))
                    .where(le(property("e", BbCourse.bbCourseId()), value(courseIdPrefix + StringUtils.repeat("Z", maxNumberLength))));

            String maxNumber = builder.createStatement(session).uniqueResult();

            String postfixD;
            if (maxNumber != null)
            {
                // Обрезаем префикс, оставляем только постфикс D и сразу инкрементируем его (+1)
                postfixD = incrementCourseIdPostfixD(maxNumber.substring(courseIdPrefix.length()));
            }
            else
            {
                // Начинаем с чистого листа. Это первый идентификатор для данного префикса
                postfixD = StringUtils.leftPad("1", maxNumberLength, '0'); // A-B-C-001
            }
            final String nextCourseId = courseIdPrefix + postfixD;

            if (findCourseByCourseId(nextCourseId) != null)
                throw new ApplicationException("Курс с идентификатором \"" + nextCourseId + "\" уже зарегистрирован в Blackboard, однако отсутствует в базе данных ОБ.");

            final String newCoursePrimaryId = BBContextHelper.getInstance().doCourseRequest((courseWSPortType, of) -> courseWSPortType.createCourse(initCourseVO(nextCourseId, title, description)));

            if (newCoursePrimaryId == null)
                throw new ApplicationException("По неизвестной причине не удалось зарегистрировать новый курс.");

            // Курс уже создан. Если что-то дальше пойдет не так и транзакция откатится, то надо будет курс удалить
            registerDeleteCourseListenerIfTransactionRollback(session, newCoursePrimaryId);

            // Назначаем преподавателей в качестве инстркуторов
            for (UserVO userVO : ppsUsersMap.values())
            {
                if (BBMembershipUtils.saveCourseMembership(newCoursePrimaryId, userVO.getId().getValue(), CourseMembershipWSConstants.INSTRUCTOR_ROLE) == null)
                    throw new ApplicationException("По неизвестной причине не удалось назначить преподавателя на новый курс.");
            }

            // Сохраняем данные в нашу базу
            BbCourse newCourse = BlackboardManager.instance().dao().createCourse(newCoursePrimaryId, nextCourseId, title, registryElement, ppsUsersMap.keySet(), true);
            session.flush();

            return newCourse.getId();
        });
    }

    /**
     * Создание группы на курсе
     *
     * @param coursePrimaryId идентификатор курса (примари)
     * @param title           название группы (обязательно)
     * @param description     описание группы (нужно обязательно, иначе в ВВ падает NPEшечка, когда открываешь список групп на курсе)
     * @return идентификатор созданной группы
     */
    @NotNull
    public static String createNewGroup(@NotNull final String coursePrimaryId, @NotNull final String title, @NotNull final String description)
    {
        return BBContextHelper.getInstance().doCourseRequest((courseWSPortType, of) -> {
            GroupVO groupVO = of.createGroupVO();
            groupVO.setAvailable(true);
            groupVO.setCourseId(of.createGroupVOCourseId(coursePrimaryId));
            groupVO.setTitle(of.createGroupVOTitle(title));
            if (StringUtils.isNotEmpty(description))
            {
                groupVO.setDescription(of.createGroupVODescription(description));
                groupVO.setDescriptionType(of.createGroupVODescriptionType(GradebookWSConstants.PLAIN_TEXT_TEXT_TYPE));
            }
            return courseWSPortType.saveGroup(coursePrimaryId, groupVO);
        });
    }

    /**
     * Создание объкта {@link CourseVO}. В BB ничего не отправляется.
     *
     * @param courseId    идентификатор курса (не примари)
     * @param title       название
     * @param description описание
     * @return subj
     */
    @NotNull
    public static CourseVO initCourseVO(@NotNull String courseId, @NotNull String title, @Nullable String description)
    {
        ObjectFactory of = BBContextHelper.getInstance().getCourseInitializer().getObjectFactory();
        CourseVO vo = of.createCourseVO();
        vo.setAvailable(true);
        vo.setAllowGuests(true);
        vo.setCourseDuration(of.createCourseVOCourseDuration("Continuous"));
        vo.setName(of.createCourseVOName(title));
        vo.setCourseId(of.createCourseVOCourseId(courseId));
        vo.setDescription(of.createCourseVODescription(description));
        return vo;
    }

    /**
     * Получение курса по не-примари-идентификатору
     *
     * @param courseId идентификатор курса (не примари)
     * @return найденный курс или null, если ничего не найдено
     * @throws ApplicationException если найдено более одного курса
     */
    @Nullable
    public static CourseVO findCourseByCourseId(@NotNull final String courseId)
    {
        List<CourseVO> resultList = BBContextHelper.getInstance().doCourseRequest((courseWSPortType, of) -> {
            CourseFilter courseFilter = of.createCourseFilter();
            courseFilter.setFilterType(CourseWSConstants.GET_COURSE_BY_COURSEID);
            courseFilter.getCourseIds().add(courseId);

            return courseWSPortType.getCourse(courseFilter);
        });

        if (resultList != null && !resultList.isEmpty())
        {
            if (resultList.size() > 1)
                throw new ApplicationException("В Blackboard найдено более одного курса с идентификатором " + courseId);

            return resultList.get(0);
        }
        return null;
    }

    /**
     * Получение списка курсов по примари идентификаторам
     *
     * @param ids идентификаторы курсов (примари)
     * @return список найденных курсов
     * @throws ApplicationException если найдено более одного курса
     */
    @NotNull
    public static Collection<CourseVO> findCoursesById(@NotNull final Collection<String> ids)
    {
        List<CourseVO> resultList = BBContextHelper.getInstance().doCourseRequest((courseWSPortType, of) -> {
            CourseFilter courseFilter = of.createCourseFilter();
            courseFilter.setFilterType(CourseWSConstants.GET_COURSE_BY_ID);
            courseFilter.getIds().addAll(ids);

            return courseWSPortType.getCourse(courseFilter);
        });

        return resultList != null ? resultList : Collections.<CourseVO>emptyList();
    }

    /**
     * Поиск всех неархивных курсов ППСа, где он является преподаавтелем.
     * Архивность курса определяется по приписке "{@value IBlackboardDAO#ARCHIVE_PREFIX}" в названии.
     *
     * @param ppsEntry член ППС
     * @return список {@link CourseVO}
     */
    @NotNull
    public static Collection<CourseVO> findActiveTutorCourses(@NotNull final PpsEntry ppsEntry)
    {
        // Находим юзера данного преподавателя
        final Map<PpsEntry, UserVO> ppsMap = BBUserUtils.findUsersNotNull(Collections.singletonList(ppsEntry));
        final String userId = ppsMap.get(ppsEntry).getId().getValue();

        // Находим участие юзера в курсах. Получаем набор примари идентификаторов курсов
        final Collection<CourseMembershipVO> memberships = BBMembershipUtils.findCourseMembershipsByUserId(userId);
        final Set<String> courseIds = Sets.newHashSetWithExpectedSize(memberships.size());
        for (CourseMembershipVO vo : memberships)
        {
            // участник курса должен иметь роль преподавателя
            if (CourseMembershipWSConstants.INSTRUCTOR_ROLE.equals(vo.getRoleId().getValue()))
            {
                courseIds.add(vo.getCourseId().getValue());
            }
        }

        // Исключаем те, которые в нашей базе есть и помечены, как архивные
        courseIds.removeAll(DataAccessServices.dao().<String>getList(
                new DQLSelectBuilder().fromEntity(BbCourse.class, "c")
                        .column(property("c", BbCourse.P_BB_PRIMARY_ID))
                        .where(eq(property("c", BbCourse.P_ARCHIVE), value(true)))
                        .where(in(property("c", BbCourse.P_BB_PRIMARY_ID), courseIds))
        ));

        // Запрашиваем список курсов и сразу Фильтруем их по другим признакам архивности курса
        return Collections2.filter(BBCourseUtils.findCoursesById(courseIds), ACTIVE_COURSE_PREDICATE);
    }

    /**
     * Получение курса ВВ по идентификатору. Если курс не найден, кидается ApplicationException.
     *
     * @param courseId идентификатор курса (не примари)
     * @return курс ВВ
     */
    @NotNull
    public static CourseVO findCourseByCourseIdNotNull(@NotNull String courseId)
    {
        CourseVO courseVO = findCourseByCourseId(courseId);
        if (courseVO == null)
            throw new ApplicationException("Курс с идентификатором " + courseId + " не найден в Blackboard.");

        return courseVO;
    }

    /**
     * Инкрментирует постфикс (D), для получения следующего свободного идентификатора.
     * Постфикс (D) может состоять из цифр и букв английского алфавита.
     *
     * @return инкрементированный постфикс
     */
    @NotNull
    private static String incrementCourseIdPostfixD(@NotNull String postfixD)
    {
        char[] chars = postfixD.toCharArray();
        for (int i = chars.length - 1; i >= 0; i--)
        {
            char c = chars[i];
            if (c == '9')
            {
                c = 'A';
            }
            else
            {
                c++;
                if (c > 'Z')
                    c = '0';
            }

            chars[i] = c;

            if (c != '0')
            {
                return String.valueOf(chars);
            }
        }
        throw new ApplicationException("Пул идентификаторов исчерпан.");
    }

    /**
     * Данный метод генерирует часть идентификатора УЭК, а именно "A-B-C-".
     * Идентификатор нового ЭУК формируется в следующем формате: A-B-C-D, где
     * <p/>
     * A – 7-символьный шифр подразделения, состоящий из шифра школы и шифра кафедры. Например,
     * FU50016, где FU500 – школа экономики и менеджмента, 16 – порядковый номер кафедры;
     * <p/>
     * B – 5-символьный цифровой код элемента реестра дисциплины;
     * <p/>
     * C – Символьное обозначение (аббревиатура) дисциплины латинскими буквами. Количество символов не более 7.
     * Аббревиатура должна формироваться автоматически на основе наименования элемента реестра дисциплин;
     * <p/>
     * D – 3-значный порядковый номер, уникальный для ЭУК, в составе идентификаторов которых A, B, C – совпадают.
     * Порядковый номер изменяются путем увеличения значения на единицу по отношению к старому ЭУК.
     */
    @NotNull
    private static String generateCourseIdPrefixABC(@NotNull EppRegistryElement registryElement)
    {
        OrgUnit cathedra = registryElement.getOwner(); // Кафедра (по задумке)

        // Получаем школу, в которой находится кафедра
        OrgUnit parentOrgUnit = getParentOrgUnit(cathedra, UniFefuDefines.SCHOOL_ORGUNIT_TYPE_CODE);

        // Если кафедра не в школе, она может быть в филиале
        if (parentOrgUnit == null)
        {
            parentOrgUnit = getParentOrgUnit(cathedra, OrgUnitTypeCodes.BRANCH);
        }

        // Получаем код ИМЦА для кафедры
        EppTutorOrgUnit eppTutorOrgUnit = DataAccessServices.dao().get(EppTutorOrgUnit.class, EppTutorOrgUnit.L_ORG_UNIT, cathedra);

        // Шифр подразделения
        String A =
                StringUtils.leftPad(parentOrgUnit != null && parentOrgUnit.getDivisionCode() != null ? parentOrgUnit.getDivisionCode() : "", 5, '0') +
                        StringUtils.leftPad(eppTutorOrgUnit != null && eppTutorOrgUnit.getCodeImtsa() != null ? eppTutorOrgUnit.getCodeImtsa() : "", 2, '0');

        // Номер элемента реестра
        String B = StringUtils.leftPad(registryElement.getNumber(), 5, '0');

        // Символьное обозначение дисциплины
        String C = getDisciplineTranslitAbbreviation(registryElement.getTitle(), 7);

        // Теперь возвращаем A-B-C-
        return A + "-" + B + "-" + C + "-";
    }


    /**
     * Получение ближайшего родительского оргюнита заданного типа. Метод необходим для получения школы (или филиала), в которой находится кафедра.
     *
     * @param orgUnit  текущий оргюнит. Если он сам имеет заданный тип, то его и возращаем.
     * @param typeCode код типа оргюнита
     * @return оргюнит заданного типа или null, если такой не найден
     */
    @Nullable
    private static OrgUnit getParentOrgUnit(@Nullable OrgUnit orgUnit, @NotNull String typeCode)
    {
        while (orgUnit != null && !typeCode.equals(orgUnit.getOrgUnitType().getCode()))
        {
            orgUnit = orgUnit.getParent();
        }
        return orgUnit;
    }

    /**
     * <p>
     * Возвращает аббревиатуру для названия дисциплины в виде транслитерации в верхнем регистре.
     * Строка делится на слова по принципу - что не буква, то разделитель слова.
     * У каждого слова берется первая буква и добавляется в результат пока не закончатся слова или пока длина аббревиатуры не достигнет {@code maxLength}.
     * </p>
     * <p>Примеры:</p>
     * <ul>
     * <li>"1С программирование" -> "SP"</li>
     * <li>"Деловое общение в профессиональной деятельности юриста" -> "DOVPDYU"</li>
     * </ul>
     *
     * @param disciplineTitle Исходное название дисциплины
     * @param maxLength       ограничение по длине итоговой строки
     */
    @NotNull
    private static String getDisciplineTranslitAbbreviation(@NotNull String disciplineTitle, int maxLength)
    {
        String result = "";
        boolean prevLetter = false;
        for (int i = 0; i < disciplineTitle.length(); i++)
        {
            char c = disciplineTitle.charAt(i);
            if (Character.isLetter(c))
            {
                if (!prevLetter)
                {
                    result += CoreStringUtils.transliterate(String.valueOf(c));
                    if (result.length() >= maxLength)
                    {
                        break;
                    }
                    prevLetter = true;
                }
            }
            else
            {
                prevLetter = false;
            }
        }

        if (maxLength < result.length())
            result = result.substring(0, maxLength);

        return result.toUpperCase();
    }
}