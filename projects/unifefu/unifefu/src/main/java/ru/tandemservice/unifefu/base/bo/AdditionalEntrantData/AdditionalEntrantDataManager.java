/* $Id$ */
package ru.tandemservice.unifefu.base.bo.AdditionalEntrantData;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.AdditionalEntrantData.logic.AdditionalEntrantDataDAO;
import ru.tandemservice.unifefu.base.bo.AdditionalEntrantData.logic.IAdditionalEntrantDataDAO;

/**
 * @author Nikolay Fedorovskih
 * @since 06.06.2013
 */
@Configuration
public class AdditionalEntrantDataManager extends BusinessObjectManager
{
    public static AdditionalEntrantDataManager instance()
    {
        return instance(AdditionalEntrantDataManager.class);
    }

    @Bean
    public IAdditionalEntrantDataDAO dao()
    {
        return new AdditionalEntrantDataDAO();
    }
}