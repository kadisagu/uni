package ru.tandemservice.unifefu.component.report.FefuEntrantResidenceDistrib.Pub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.unifefu.entity.report.FefuEntrantResidenceDistrib;

@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    private FefuEntrantResidenceDistrib _report = new FefuEntrantResidenceDistrib();

    public FefuEntrantResidenceDistrib getReport()
    {
        return _report;
    }

    public void setReport(FefuEntrantResidenceDistrib report)
    {
        _report = report;
    }
}
