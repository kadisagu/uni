package ru.tandemservice.unifefu.base.ext.Employee.ui.logic;

import org.tandemframework.shared.employeebase.base.bo.Employee.logic.IEmployeeDAO;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

/**
 * @author Dmitry Seleznev
 * @since 09.07.2014
 */
public interface IFefuEmployeePostDao extends IEmployeeDAO
{
    void saveEmployeePostExtension(Long employeeId, EmployeePost employeePostDTO, Integer rate);

    void updateEmployeePostExtension(Long employeePostId, EmployeePost employeePostDTO, Integer rate);
}