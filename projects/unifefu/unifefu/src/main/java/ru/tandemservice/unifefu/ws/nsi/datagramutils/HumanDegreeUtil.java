/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import org.apache.commons.collections.map.HashedMap;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.NsiDatagramUtil;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanDegreeType;

import javax.xml.namespace.QName;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 26.01.2015
 */
public class HumanDegreeUtil implements INsiEntityUtil<HumanDegreeType, PersonAcademicDegree>
{
    public static final String ID_NSI_FIELD = "ID";
    public static final String GUID_FIELD = "GUID";
    public static final String DATE_FIELD = "date";
    public static final String ISSUANCE_PLACE_FIELD = "issuancePlace";
    public static final String COUNCIL_FIELD = "council";

    protected List<String> NSI_FIELDS;
    protected List<String> ENTITY_FIELDS;

    protected Map<String, String> NSI_TO_OB_FIELDS_MAP;
    protected Map<String, String> OB_TO_NSI_FIELDS_MAP;

    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            NSI_FIELDS = new ArrayList<>();
            NSI_FIELDS.add(ID_NSI_FIELD);
            NSI_FIELDS.add(DATE_FIELD);
            NSI_FIELDS.add(ISSUANCE_PLACE_FIELD);
            NSI_FIELDS.add(COUNCIL_FIELD);
        }

        return NSI_FIELDS;
    }

    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            ENTITY_FIELDS = new ArrayList<>();
            ENTITY_FIELDS.add(GUID_FIELD);
            ENTITY_FIELDS.add(PersonAcademicDegree.date().s());
            ENTITY_FIELDS.add(PersonAcademicDegree.issuancePlace().s());
            ENTITY_FIELDS.add(PersonAcademicDegree.council().s());
        }

        return ENTITY_FIELDS;
    }

    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            NSI_TO_OB_FIELDS_MAP = new HashedMap();
            NSI_TO_OB_FIELDS_MAP.put(ID_NSI_FIELD, GUID_FIELD);
            NSI_TO_OB_FIELDS_MAP.put(DATE_FIELD, PersonAcademicDegree.date().s());
            NSI_TO_OB_FIELDS_MAP.put(ISSUANCE_PLACE_FIELD, PersonAcademicDegree.issuancePlace().s());
            NSI_TO_OB_FIELDS_MAP.put(COUNCIL_FIELD, PersonAcademicDegree.council().s());
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            OB_TO_NSI_FIELDS_MAP = new HashedMap();
            OB_TO_NSI_FIELDS_MAP.put(GUID_FIELD, ID_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(PersonAcademicDegree.date().s(), DATE_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(PersonAcademicDegree.issuancePlace().s(), ISSUANCE_PLACE_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(PersonAcademicDegree.council().s(), COUNCIL_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    @Override
    public long getNsiIdHash(HumanDegreeType nsiEntity)
    {
        return MD5HashBuilder.getCheckSum(nsiEntity.getID().toLowerCase());
    }

    @Override
    public long getNsiCodeHash(HumanDegreeType nsiEntity)
    {
        return 0L;
    }

    @Override
    public long getNsiTitleHash(HumanDegreeType nsiEntity, boolean caseInsensitive)
    {
        return 0L;
    }

    @Override
    public long getNsiShortTitleHash(HumanDegreeType nsiEntity, boolean caseInsensitive)
    {
        return 0L;
    }

    public long getNsiDateHash(HumanDegreeType nsiEntity)
    {
        if (null == nsiEntity.getHumanDegreeDegreeDate()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getHumanDegreeDegreeDate());
    }

    public long getNsiIssuancePlaceHash(HumanDegreeType nsiEntity, boolean caseInsensitive)
    {
        if (null == nsiEntity.getShortTitle()) return 0;
        if (caseInsensitive)
            return MD5HashBuilder.getCheckSum(nsiEntity.getHumanDegreeOrganization().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(nsiEntity.getHumanDegreeOrganization());
    }

    public long getNsiCouncilHash(HumanDegreeType nsiEntity, boolean caseInsensitive)
    {
        if (null == nsiEntity.getHumanDegreeDSID()) return 0;
        if (caseInsensitive) return MD5HashBuilder.getCheckSum(nsiEntity.getHumanDegreeDSID().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(nsiEntity.getHumanDegreeDSID());
    }

    @Override
    public long getNsiFieldHash(HumanDegreeType nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case ID_NSI_FIELD:
                return getNsiIdHash(nsiEntity);
            case DATE_FIELD:
                return getNsiDateHash(nsiEntity);
            case ISSUANCE_PLACE_FIELD:
                return getNsiIssuancePlaceHash(nsiEntity, caseInsensitive);
            case COUNCIL_FIELD:
                return getNsiCouncilHash(nsiEntity, caseInsensitive);
            default:
                throw new UnsupportedOperationException("Field '" + fieldName + "' not found for '" + nsiEntity.getClass().getSimpleName() + "' entity type.");
        }
    }

    @Override
    public boolean isFieldEmpty(HumanDegreeType nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case ID_NSI_FIELD:
                return null == nsiEntity.getID();
            case DATE_FIELD:
                return null == nsiEntity.getHumanDegreeDegreeDate();
            case ISSUANCE_PLACE_FIELD:
                return null == nsiEntity.getHumanDegreeOrganization();
            case COUNCIL_FIELD:
                return null == nsiEntity.getHumanDegreeDSID();
            default:
                throw new UnsupportedOperationException("Field '" + fieldName + "' not found for '" + nsiEntity.getClass().getSimpleName() + "' entity type.");
        }
    }

    @Override
    public long getNsiEntityHash(HumanDegreeType nsiEntity, boolean takeId, boolean takeCode, boolean caseInsensitive)
    {
        Set<String> excludeFieldsSet = new HashSet();
        if (!takeId) excludeFieldsSet.add(ID_NSI_FIELD);

        MD5HashBuilder builder = new MD5HashBuilder();
        for (String fieldName : getNsiFields())
        {
            if (!excludeFieldsSet.contains(fieldName))
                builder.add(getNsiFieldHash(nsiEntity, fieldName, caseInsensitive));
        }
        return builder.getCheckSum();
    }

    @Override
    public long getNsiEntityHash(HumanDegreeType nsiEntity, boolean caseInsensitive)
    {
        return getNsiEntityHash(nsiEntity, true, true, caseInsensitive);
    }

    @Override
    public long getGuidHash(FefuNsiIds nsiId)
    {
        if (null == nsiId || null == nsiId.getGuid()) return 0;
        return MD5HashBuilder.getCheckSum(nsiId.getGuid().toLowerCase());
    }

    @Override
    public long getCodeHash(PersonAcademicDegree entity)
    {
        return 0;
    }

    @Override
    public long getTitleHash(PersonAcademicDegree entity, boolean caseInsensitive)
    {
        return 0;
    }

    @Override
    public long getShortTitleHash(PersonAcademicDegree entity, boolean caseInsensitive)
    {
        return 0;
    }

    public long getDateHash(PersonAcademicDegree entity)
    {
        if (null != entity.getDate())
        {
            String dateStr = NsiDatagramUtil.formatDate(entity.getDate());
            if (null != dateStr)
            {
                return MD5HashBuilder.getCheckSum(dateStr);
            }
        }

        return 0;
    }

    public long getIssuancePlaceHash(PersonAcademicDegree entity, boolean caseInsensitive)
    {
        if (null != entity.getIssuancePlace())
        {
            if (caseInsensitive)
                return MD5HashBuilder.getCheckSum(entity.getIssuancePlace().trim().toUpperCase());
            return MD5HashBuilder.getCheckSum(entity.getIssuancePlace());
        }

        return 0;
    }

    public long getCouncilHash(PersonAcademicDegree entity, boolean caseInsensitive)
    {
        if (null != entity.getCouncil())
        {
            if (caseInsensitive)
                return MD5HashBuilder.getCheckSum(entity.getCouncil().trim().toUpperCase());
            return MD5HashBuilder.getCheckSum(entity.getCouncil());
        }

        return 0;
    }

    @Override
    public long getEntityFieldHash(PersonAcademicDegree entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case GUID_FIELD:
                return getGuidHash(nsiId);
            case PersonAcademicDegree.P_DATE:
                return getDateHash(entity);
            case PersonAcademicDegree.P_ISSUANCE_PLACE:
                return getIssuancePlaceHash(entity, caseInsensitive);
            case PersonAcademicDegree.P_COUNCIL:
                return getCouncilHash(entity, caseInsensitive);
            default:
                throw new UnsupportedOperationException("Field '" + fieldName + "' not found for '" + entity.getClass().getSimpleName() + "' entity type.");
        }
    }

    @Override
    public long getEntityHash(PersonAcademicDegree entity, FefuNsiIds nsiIds, boolean takeId, boolean takeCode, boolean caseInsensitive)
    {
        Set<String> excludeFieldsSet = new HashSet();
        if (!takeId) excludeFieldsSet.add(GUID_FIELD);

        MD5HashBuilder builder = new MD5HashBuilder();
        for (String fieldName : getEntityFields())
        {
            if (!excludeFieldsSet.contains(fieldName))
                builder.add(getEntityFieldHash(entity, nsiIds, fieldName, caseInsensitive));
        }
        return builder.getCheckSum();
    }

    @Override
    public long getEntityHash(PersonAcademicDegree entity, FefuNsiIds nsiIds, boolean caseInsensitive)
    {
        return getEntityHash(entity, nsiIds, true, true, caseInsensitive);
    }

    private String getEntityFieldNameByNsiEntityFieldName(String nsiFieldName)
    {
        if (!getNsiToObFieldsMap().containsKey(nsiFieldName))
            throw new UnsupportedOperationException("Field '" + nsiFieldName + "' not found.");
        return getNsiToObFieldsMap().get(nsiFieldName);
    }

    private String getNsiEntityFieldNameByEntityFieldName(String fieldName)
    {
        if (!getObToNsiFieldsMap().containsKey(fieldName))
            throw new UnsupportedOperationException("Field '" + fieldName + "' not found.");
        return getObToNsiFieldsMap().get(fieldName);
    }

    @Override
    public boolean isTwoObjectsIdenticalInGeneral(HumanDegreeType nsiEntity, PersonAcademicDegree entity, FefuNsiIds nsiIds)
    {
        return getEntityHash(entity, nsiIds, false, false, true) == getNsiEntityHash(nsiEntity, false, false, true);
    }

    @Override
    public boolean isTwoObjectsIdenticalFieldByField(HumanDegreeType nsiEntity, PersonAcademicDegree entity, FefuNsiIds nsiIds)
    {
        // Поскольку идентичность примитивных справочников сравнивается исключительно по названию элемента,
        // то hash сравнения поле-в-поле совпадает с hash'ем всего объекта
        return isTwoObjectsIdenticalInGeneral(nsiEntity, entity, nsiIds);
    }

    @Override
    public boolean isFieldChanged(HumanDegreeType nsiEntity, PersonAcademicDegree entity, FefuNsiIds nsiIds, String nsiFieldName)
    {
        if (isFieldEmpty(nsiEntity, nsiFieldName))
            return false; // Поля, в которых сидит null мы не имеем права обновлять, поскольку из НСИ приходят только изменившиеся поля
        return getNsiFieldHash(nsiEntity, nsiFieldName, false) != getEntityFieldHash(entity, nsiIds, getEntityFieldNameByNsiEntityFieldName(nsiFieldName), false);
    }

    @Override
    public boolean isFieldChanged(PersonAcademicDegree entity, HumanDegreeType nsiEntity, FefuNsiIds nsiIds, String fieldName)
    {
        if (isFieldEmpty(nsiEntity, getNsiEntityFieldNameByEntityFieldName(fieldName)))
            return false; // Поля, в которых сидит null мы не имеем права обновлять, поскольку из НСИ приходят только изменившиеся поля
        return getEntityFieldHash(entity, nsiIds, fieldName, false) != getNsiFieldHash(nsiEntity, getNsiEntityFieldNameByEntityFieldName(fieldName), false);
    }

    @Override
    public boolean isEntityChanged(HumanDegreeType nsiEntity, PersonAcademicDegree entity, FefuNsiIds nsiIds)
    {
        return getEntityHash(entity, nsiIds, false, false, false) != getNsiEntityHash(nsiEntity, false, false, false);
    }

    @Override
    public List<String> getMergeDuplicatesList(HumanDegreeType nsiEntity)
    {
        List<String> result = new ArrayList<>();
        String mergeDuplicates = null != nsiEntity.getMergeDublicates() ? nsiEntity.getMergeDublicates() : nsiEntity.getOtherAttributes().get(new QName("mergeDuplicates"));

        if (null != mergeDuplicates)
        {
            String[] duplicates = mergeDuplicates.split(";");
            for (String duplicate : duplicates) result.add(duplicate.trim());
        }

        if (result.size() < 2) return null;
        return result;
    }
}