/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 20.01.2015
 */
public class FefuNsiOrphanGuidDaemonDao extends UniBaseDao implements IFefuNsiOrphanGuidDaemonDao
{
    public static final int DAEMON_ITERATION_TIME = 240;

    private static boolean sync_locked = false;

    public static final SyncDaemon DAEMON = new SyncDaemon(FefuNsiOrphanGuidDaemonDao.class.getName(), DAEMON_ITERATION_TIME, IFefuNsiOrphanGuidDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            // Если в данный момент идёт синхронизация, то пропускаем очередной шаг демона
            if (sync_locked) return;

            final IFefuNsiOrphanGuidDaemonDao dao = IFefuNsiOrphanGuidDaemonDao.instance.get();
            sync_locked = true;

            // отключаем логирование в базу и прочие системные штуки
            final IEventServiceLock eventLock = CoreServices.eventService().lock();
            Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

            try
            {
                dao.doCleanOrphanGuids();
            } catch (final Throwable t)
            {
                sync_locked = false;
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            } finally
            {
                // включаем штуки и логирование обратно
                Debug.resumeLogging();
                eventLock.release();
            }

            sync_locked = false;
        }
    };

    @Override
    public void doCleanOrphanGuids()
    {
        System.out.println("+++++++++++ FefuNsiOrphanGuidDaemonDao has started.");
        List<String> entityTypesList = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column(property(FefuNsiIds.entityType().fromAlias("e")))
                .distinct().order(property(FefuNsiIds.entityType().fromAlias("e"))).createStatement(getSession()).list();

        Set<Long> idsToDelSet = new HashSet<>();
        for (String entityType : entityTypesList)
        {
            IEntityMeta meta = EntityRuntime.getMeta(entityType);

            // TODO meta = null
            List<Long> orphanIdList = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "ids").column(property(FefuNsiIds.id().fromAlias("ids")))
                    .joinEntity("ids", DQLJoinType.left, meta.getEntityClass(), "e", eq(property(FefuNsiIds.entityId().fromAlias("ids")), property(EntityBase.id().fromAlias("e"))))
                    .where(eq(property(FefuNsiIds.entityType().fromAlias("ids")), value(entityType))).where(isNull(property("e"))).createStatement(getSession()).list();

            if (!orphanIdList.isEmpty())
                System.out.println("+++++++++++ FefuNsiOrphanGuidDaemonDao - " + orphanIdList.size() + " items with type '" + entityType + "' should be deleted.");

            idsToDelSet.addAll(orphanIdList);
        }

        BatchUtils.execute(idsToDelSet, 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(final Collection<Long> ids)
            {
                for (Long id : ids) delete(id);
                getSession().flush();
            }
        });

        System.out.println("+++++++++++ FefuNsiOrphanGuidDaemonDao has finished. " + idsToDelSet.size() + " items were deleted.");
    }
}