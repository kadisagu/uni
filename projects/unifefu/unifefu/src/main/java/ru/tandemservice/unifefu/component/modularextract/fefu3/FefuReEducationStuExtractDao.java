/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu3;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuReEducationStuExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 22.08.2012
 */
public class FefuReEducationStuExtractDao extends UniBaseDao implements IExtractComponentDao<FefuReEducationStuExtract>
{
    public void doCommit(FefuReEducationStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        MoveStudentDaoFacade.getCommonExtractUtil().doCommitWithoutChangeStatus(extract);
    }

    public void doRollback(FefuReEducationStuExtract extract, Map parameters)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().doRollbackWithoutChangeStatus(extract);
    }
}