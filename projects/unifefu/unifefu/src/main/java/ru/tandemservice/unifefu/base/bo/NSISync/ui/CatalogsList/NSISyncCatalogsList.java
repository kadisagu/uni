/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.CatalogsList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.base.bo.NSISync.ui.CatalogLog.NSISyncCatalogLog;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiDaemonDao;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 12.09.2013
 */
@Configuration
public class NSISyncCatalogsList extends BusinessComponentManager
{
    public static final String NSI_CATALOGS_LIST_DS = "nsiCatalogsListDS";
    public static final String STATE_VIEW_PROPERTY = "stateViewProperty";
    public static final String SYNC_DISABLED = "syncDisabled";
    public static final String SYNC_ICON = "ui:syncIcon";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(NSI_CATALOGS_LIST_DS, getNsiCatalogsListDS(), nsiCatalogsListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getNsiCatalogsListDS()
    {
        return columnListExtPointBuilder(NSI_CATALOGS_LIST_DS)
                .addColumn(publisherColumn("title", FefuNsiCatalogType.title())
                        .publisherLinkResolver(new IPublisherLinkResolver()
                        {
                            @Override
                            public Object getParameters(IEntity entity)
                            {
                                Map<String, Object> params = new HashMap();
                                params.put(UIPresenter.PUBLISHER_ID, entity.getId());
                                return params;
                            }

                            @Override
                            public String getComponentName(IEntity entity)
                            {
                                return NSISyncCatalogLog.class.getSimpleName();
                            }
                        }).order().create())
                .addColumn(textColumn("nsiCode", FefuNsiCatalogType.nsiCode()).order())
                .addColumn(textColumn("uniCode", FefuNsiCatalogType.uniCode()).order())
                .addColumn(textColumn("state", STATE_VIEW_PROPERTY))
                .addColumn(textColumn("syncDateTime", FefuNsiCatalogType.syncDateTime()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
                .addColumn(toggleColumn("realTimeSync", FefuNsiCatalogType.realTimeSync()).toggleOnListener("onClickChangeRealTimeSync").toggleOffListener("onClickChangeRealTimeSync"))
                //.addColumn(toggleColumn("autoSync", FefuNsiCatalogType.autoSync()).toggleOnListener("onClickChangeAutoSync").toggleOffListener("onClickChangeAutoSync"))
                .addColumn(actionColumn("syncNow", new Icon("daemon", "Синхронизировать"), "onClickSyncNow", alert("nsiCatalogsListDS.syncNow.alert", FefuNsiCatalogType.title())).icon(SYNC_ICON)/*.disabled(SYNC_DISABLED)*/)
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> nsiCatalogsListDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                context.getSession().clear();
                List<FefuNsiCatalogType> catalogTypes = new DQLSelectBuilder().fromEntity(FefuNsiCatalogType.class, "e").column("e")
                        .order(DQLExpressions.property("e", input.getEntityOrder().getColumnName()), input.getEntityOrder().getDirection())
                        .createStatement(context.getSession()).list();

                List<DataWrapper> wrappersList = new ArrayList<>();
                for (FefuNsiCatalogType catalogType : catalogTypes)
                {
                    DataWrapper wrapper = new DataWrapper(catalogType.getId(), catalogType.getTitle(), catalogType);
                    wrapper.setProperty(SYNC_DISABLED, Boolean.FALSE);
                    if (FefuNsiDaemonDao.isEntitySyncInProcess(catalogType.getId()))
                    {
                        wrapper.setProperty(STATE_VIEW_PROPERTY, "Идет синхронизация");
                        wrapper.setProperty(SYNC_DISABLED, Boolean.TRUE);
                        wrapper.setProperty(SYNC_ICON, CommonDefines.ICON_DELETE.getName());
                    } else if (FefuNsiDaemonDao.isEntityInQueueForSync(catalogType.getId()))
                    {
                        wrapper.setProperty(STATE_VIEW_PROPERTY, "Ожидает синхронизации");
                        wrapper.setProperty(SYNC_DISABLED, Boolean.TRUE);
                        wrapper.setProperty(SYNC_ICON, CommonDefines.ICON_ADD.getName());
                    } else if (null == catalogType.getSyncDateTime())
                    {
                        wrapper.setProperty(STATE_VIEW_PROPERTY, "Не синхронизировался");
                        wrapper.setProperty(SYNC_ICON, CommonDefines.ICON_EXECUTE.getName());
                    } else
                    {
                        wrapper.setProperty(STATE_VIEW_PROPERTY, "Актуален");
                        wrapper.setProperty(SYNC_ICON, CommonDefines.ICON_EXECUTE.getName());
                    }
                    //TODO: отображать ошибки синхронизации
                    wrappersList.add(wrapper);
                }

                return ListOutputBuilder.get(input, wrappersList).pageable(true).build();
            }
        };
    }
}