/* $Id$ */
package ru.tandemservice.unifefu.dao.eppEduPlan;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvTotalRow;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;

import java.util.ArrayList;
import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 27.10.2014
 */
public class FefuEduPlanVersionDataDAO extends EppEduPlanVersionDataDAO
{
    public static final String FEFU_CONTROL_ACTION_TOTAL_EXAM_TITLE = "Всего экзаменов";
    public static final String FEFU_CONTROL_ACTION_TOTAL_SETOFF_TITLE = "Всего зачетов";
    public static final String FEFU_CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE = "Всего курсовых работ";
    public static final String FEFU_CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE = "Всего курсовых проектов";
    public static final String FEFU_CONTROL_ACTION_TOTAL_CONTROL_WORK_TITLE = "Всего контрольных работ";

    @Override
    public Collection<EppEpvTotalRow> getTotalRows(IEppEpvBlockWrapper versionWrapper, Collection<IEppEpvRowWrapper> versionRows)
    {
        Collection<IEppEpvRowWrapper> filteredRows = EppEpvTotalRow.filterRowsForTotalStats(versionRows);

        Collection<EppEpvTotalRow> result = new ArrayList<>();

        result.add(EppEpvTotalRow.getTotalRowTotal(filteredRows));
        result.add(EppEpvTotalRow.getTotalRowAvgLoad(versionWrapper, filteredRows));
        result.add(getTotalRowWeeks(filteredRows));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, FEFU_CONTROL_ACTION_TOTAL_EXAM_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, FEFU_CONTROL_ACTION_TOTAL_SETOFF_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, FEFU_CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, FEFU_CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, FEFU_CONTROL_ACTION_TOTAL_CONTROL_WORK_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_CONTROL_WORK));

        return result;
    }

    public EppEpvTotalRow getTotalRowWeeks(final Collection<IEppEpvRowWrapper> filteredRows)
    {
        return new EppEpvTotalRow("Всего недель")
        {
            private static final long serialVersionUID = 1L;

            @Override
            public String getLoadValue(final int term, final String loadFullCode)
            {
                if (term <= 0)
                    return "";

                Collection<IEppEpvRowWrapper> usedInLoad = CollectionUtils.select(filteredRows, IEppEpvRow::getUsedInLoad);

                return  UniEppUtils.formatLoad(UniEppUtils.wrap(
                    new DQLSelectBuilder()
                        .fromEntity(EppEpvRowTerm.class, "t")
                        .column(DQLFunctions.sum(DQLExpressions.property("t", EppEpvRowTerm.weeks())))
                        .where(gt(property("t", EppEpvRowTerm.weeks()), value(0L)))
                        .where(in(property("t", EppEpvRowTerm.row()), usedInLoad))
                        .where(eq(property("t", EppEpvRowTerm.term().intValue()), value(term)))
                        .createStatement(FefuEduPlanVersionDataDAO.this.getSession())
                        .<Long>uniqueResult()), true);
            }

            @Override public boolean isMergeTermCells() { return true; }
        };
    }
}