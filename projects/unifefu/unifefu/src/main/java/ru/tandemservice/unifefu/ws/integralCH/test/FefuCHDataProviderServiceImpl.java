/**
 * FefuCHDataProviderServiceImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.integralCH.test;

public interface FefuCHDataProviderServiceImpl extends javax.xml.rpc.Service {
    public java.lang.String getFefuCHDataProviderServicePortAddress();

    public ru.tandemservice.unifefu.ws.integralCH.test.FefuCHDataProviderService getFefuCHDataProviderServicePort() throws javax.xml.rpc.ServiceException;

    public ru.tandemservice.unifefu.ws.integralCH.test.FefuCHDataProviderService getFefuCHDataProviderServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
