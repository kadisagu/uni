/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup;

/**
 * @author nvankov
 * @since 11/7/13
 */
public class FefuThemGroup2ExtTypesDSHandler extends DefaultSearchDataSourceHandler
{
    public FefuThemGroup2ExtTypesDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long themGroupId = context.getNotNull("thematicGroupId");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuStudentExtractType2ThematicGroup.class, "tg");
        builder.where(DQLExpressions.eq(DQLExpressions.property("tg", FefuStudentExtractType2ThematicGroup.thematicGroup().id()), DQLExpressions.value(themGroupId)));
        builder.order(DQLExpressions.property("tg", FefuStudentExtractType2ThematicGroup.type().title()));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }
}
