/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;

/**
 * @author Dmitry Seleznev
 * @since 04.02.2015
 */
public interface INSITestDAO extends INeedPersistenceSupport, ICommonDAO
{
    void doCreateStudent(boolean lockNsiListeners);
}