/**
 * Data.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.c1;

public class Data  implements java.io.Serializable {
    private java.util.Date scholarshipOrderDate;

    private java.lang.String scholarshipOrderNo;

    private java.lang.String scholarshipOrderCode;

    private java.util.Date scholarshipStartDate;

    private java.util.Date scholarshipEndDate;

    private ru.tandemservice.unifefu.ws.c1.TableRecordType[] table;

    public Data() {
    }

    public Data(
           java.util.Date scholarshipOrderDate,
           java.lang.String scholarshipOrderNo,
           java.lang.String scholarshipOrderCode,
           java.util.Date scholarshipStartDate,
           java.util.Date scholarshipEndDate,
           ru.tandemservice.unifefu.ws.c1.TableRecordType[] table) {
           this.scholarshipOrderDate = scholarshipOrderDate;
           this.scholarshipOrderNo = scholarshipOrderNo;
           this.scholarshipOrderCode = scholarshipOrderCode;
           this.scholarshipStartDate = scholarshipStartDate;
           this.scholarshipEndDate = scholarshipEndDate;
           this.table = table;
    }


    /**
     * Gets the scholarshipOrderDate value for this Data.
     * 
     * @return scholarshipOrderDate
     */
    public java.util.Date getScholarshipOrderDate() {
        return scholarshipOrderDate;
    }


    /**
     * Sets the scholarshipOrderDate value for this Data.
     * 
     * @param scholarshipOrderDate
     */
    public void setScholarshipOrderDate(java.util.Date scholarshipOrderDate) {
        this.scholarshipOrderDate = scholarshipOrderDate;
    }


    /**
     * Gets the scholarshipOrderNo value for this Data.
     * 
     * @return scholarshipOrderNo
     */
    public java.lang.String getScholarshipOrderNo() {
        return scholarshipOrderNo;
    }


    /**
     * Sets the scholarshipOrderNo value for this Data.
     * 
     * @param scholarshipOrderNo
     */
    public void setScholarshipOrderNo(java.lang.String scholarshipOrderNo) {
        this.scholarshipOrderNo = scholarshipOrderNo;
    }


    /**
     * Gets the scholarshipOrderCode value for this Data.
     * 
     * @return scholarshipOrderCode
     */
    public java.lang.String getScholarshipOrderCode() {
        return scholarshipOrderCode;
    }


    /**
     * Sets the scholarshipOrderCode value for this Data.
     * 
     * @param scholarshipOrderCode
     */
    public void setScholarshipOrderCode(java.lang.String scholarshipOrderCode) {
        this.scholarshipOrderCode = scholarshipOrderCode;
    }


    /**
     * Gets the scholarshipStartDate value for this Data.
     * 
     * @return scholarshipStartDate
     */
    public java.util.Date getScholarshipStartDate() {
        return scholarshipStartDate;
    }


    /**
     * Sets the scholarshipStartDate value for this Data.
     * 
     * @param scholarshipStartDate
     */
    public void setScholarshipStartDate(java.util.Date scholarshipStartDate) {
        this.scholarshipStartDate = scholarshipStartDate;
    }


    /**
     * Gets the scholarshipEndDate value for this Data.
     * 
     * @return scholarshipEndDate
     */
    public java.util.Date getScholarshipEndDate() {
        return scholarshipEndDate;
    }


    /**
     * Sets the scholarshipEndDate value for this Data.
     * 
     * @param scholarshipEndDate
     */
    public void setScholarshipEndDate(java.util.Date scholarshipEndDate) {
        this.scholarshipEndDate = scholarshipEndDate;
    }


    /**
     * Gets the table value for this Data.
     * 
     * @return table
     */
    public ru.tandemservice.unifefu.ws.c1.TableRecordType[] getTable() {
        return table;
    }


    /**
     * Sets the table value for this Data.
     * 
     * @param table
     */
    public void setTable(ru.tandemservice.unifefu.ws.c1.TableRecordType[] table) {
        this.table = table;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Data)) return false;
        Data other = (Data) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.scholarshipOrderDate==null && other.getScholarshipOrderDate()==null) || 
             (this.scholarshipOrderDate!=null &&
              this.scholarshipOrderDate.equals(other.getScholarshipOrderDate()))) &&
            ((this.scholarshipOrderNo==null && other.getScholarshipOrderNo()==null) || 
             (this.scholarshipOrderNo!=null &&
              this.scholarshipOrderNo.equals(other.getScholarshipOrderNo()))) &&
            ((this.scholarshipOrderCode==null && other.getScholarshipOrderCode()==null) || 
             (this.scholarshipOrderCode!=null &&
              this.scholarshipOrderCode.equals(other.getScholarshipOrderCode()))) &&
            ((this.scholarshipStartDate==null && other.getScholarshipStartDate()==null) || 
             (this.scholarshipStartDate!=null &&
              this.scholarshipStartDate.equals(other.getScholarshipStartDate()))) &&
            ((this.scholarshipEndDate==null && other.getScholarshipEndDate()==null) || 
             (this.scholarshipEndDate!=null &&
              this.scholarshipEndDate.equals(other.getScholarshipEndDate()))) &&
            ((this.table==null && other.getTable()==null) || 
             (this.table!=null &&
              java.util.Arrays.equals(this.table, other.getTable())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getScholarshipOrderDate() != null) {
            _hashCode += getScholarshipOrderDate().hashCode();
        }
        if (getScholarshipOrderNo() != null) {
            _hashCode += getScholarshipOrderNo().hashCode();
        }
        if (getScholarshipOrderCode() != null) {
            _hashCode += getScholarshipOrderCode().hashCode();
        }
        if (getScholarshipStartDate() != null) {
            _hashCode += getScholarshipStartDate().hashCode();
        }
        if (getScholarshipEndDate() != null) {
            _hashCode += getScholarshipEndDate().hashCode();
        }
        if (getTable() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTable());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTable(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Data.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "Data"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scholarshipOrderDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "ScholarshipOrderDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scholarshipOrderNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "ScholarshipOrderNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scholarshipOrderCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "ScholarshipOrderCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scholarshipStartDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "ScholarshipStartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scholarshipEndDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "ScholarshipEndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("table");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "Table"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "TableRecordType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "TableRecord"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
