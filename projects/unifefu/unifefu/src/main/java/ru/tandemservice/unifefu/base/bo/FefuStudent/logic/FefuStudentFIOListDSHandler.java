/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.employee.Student;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 08.12.2013
 */
public class FefuStudentFIOListDSHandler extends EntityComboDataSourceHandler
{
    public FefuStudentFIOListDSHandler(String ownerId)
    {
        super(ownerId, Student.class);
    }

    @Override
    protected DQLSelectBuilder query(String alias, String filter)
    {
        DQLSelectBuilder builder = super.query(alias, filter);
        builder.joinEntity(alias, DQLJoinType.left, IdentityCard.class, "ic",
                           eq(
                                   property(Student.person().id().fromAlias(alias)),
                                   property(IdentityCard.person().id().fromAlias("ic"))
                           ));

        if (!StringUtils.isEmpty(filter))
        {
            int index = 0;
            String[] filterSplit = filter.split(" ");
            for (String filt : filterSplit)
            {
                index++;
                if (!filt.startsWith("!"))
                    filt = "!" + filt;
                filt = CoreStringUtils.escapeLike(filt);

                if (index == 1)
                    builder.where(likeUpper(property("ic", IdentityCard.lastName()), value(filt)));
                else if (index == 2)
                    builder.where(likeUpper(property("ic", IdentityCard.firstName()), value(filt)));
                else if (index == 3)
                    builder.where(likeUpper(property("ic", IdentityCard.middleName()), value(filt)));
            }
        }
        return builder;
    }
}
