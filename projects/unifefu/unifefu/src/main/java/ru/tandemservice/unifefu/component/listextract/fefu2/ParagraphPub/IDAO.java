/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu2.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 15.03.2013
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuAdmitToStateExamsStuListExtract, Model>
{
}