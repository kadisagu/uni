/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu17.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubModel;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.01.2015
 */
public class Model extends AbstractListParagraphPubModel<FefuTransfStuDPOListExtract>
{
    private String _printFormFileName;

    public String getPrintFormFileName()
    {
        return _printFormFileName;
    }

    public void setPrintFormFileName(String printFormFileName)
    {
        _printFormFileName = printFormFileName;
    }
}