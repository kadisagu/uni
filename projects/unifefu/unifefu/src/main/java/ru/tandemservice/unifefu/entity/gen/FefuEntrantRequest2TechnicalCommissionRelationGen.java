package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.unifefu.entity.FefuEntrantRequest2TechnicalCommissionRelation;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь заявления абитуриента с технической комиссией
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEntrantRequest2TechnicalCommissionRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEntrantRequest2TechnicalCommissionRelation";
    public static final String ENTITY_NAME = "fefuEntrantRequest2TechnicalCommissionRelation";
    public static final int VERSION_HASH = -591097072;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT_REQUEST = "entrantRequest";
    public static final String L_TECHNICAL_COMMISSION = "technicalCommission";

    private EntrantRequest _entrantRequest;     // Заявление абитуриента
    private FefuTechnicalCommission _technicalCommission;     // Техническая комиссия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Заявление абитуриента. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EntrantRequest getEntrantRequest()
    {
        return _entrantRequest;
    }

    /**
     * @param entrantRequest Заявление абитуриента. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntrantRequest(EntrantRequest entrantRequest)
    {
        dirty(_entrantRequest, entrantRequest);
        _entrantRequest = entrantRequest;
    }

    /**
     * @return Техническая комиссия. Свойство не может быть null.
     */
    @NotNull
    public FefuTechnicalCommission getTechnicalCommission()
    {
        return _technicalCommission;
    }

    /**
     * @param technicalCommission Техническая комиссия. Свойство не может быть null.
     */
    public void setTechnicalCommission(FefuTechnicalCommission technicalCommission)
    {
        dirty(_technicalCommission, technicalCommission);
        _technicalCommission = technicalCommission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEntrantRequest2TechnicalCommissionRelationGen)
        {
            setEntrantRequest(((FefuEntrantRequest2TechnicalCommissionRelation)another).getEntrantRequest());
            setTechnicalCommission(((FefuEntrantRequest2TechnicalCommissionRelation)another).getTechnicalCommission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEntrantRequest2TechnicalCommissionRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEntrantRequest2TechnicalCommissionRelation.class;
        }

        public T newInstance()
        {
            return (T) new FefuEntrantRequest2TechnicalCommissionRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrantRequest":
                    return obj.getEntrantRequest();
                case "technicalCommission":
                    return obj.getTechnicalCommission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrantRequest":
                    obj.setEntrantRequest((EntrantRequest) value);
                    return;
                case "technicalCommission":
                    obj.setTechnicalCommission((FefuTechnicalCommission) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrantRequest":
                        return true;
                case "technicalCommission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrantRequest":
                    return true;
                case "technicalCommission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrantRequest":
                    return EntrantRequest.class;
                case "technicalCommission":
                    return FefuTechnicalCommission.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEntrantRequest2TechnicalCommissionRelation> _dslPath = new Path<FefuEntrantRequest2TechnicalCommissionRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEntrantRequest2TechnicalCommissionRelation");
    }
            

    /**
     * @return Заявление абитуриента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantRequest2TechnicalCommissionRelation#getEntrantRequest()
     */
    public static EntrantRequest.Path<EntrantRequest> entrantRequest()
    {
        return _dslPath.entrantRequest();
    }

    /**
     * @return Техническая комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantRequest2TechnicalCommissionRelation#getTechnicalCommission()
     */
    public static FefuTechnicalCommission.Path<FefuTechnicalCommission> technicalCommission()
    {
        return _dslPath.technicalCommission();
    }

    public static class Path<E extends FefuEntrantRequest2TechnicalCommissionRelation> extends EntityPath<E>
    {
        private EntrantRequest.Path<EntrantRequest> _entrantRequest;
        private FefuTechnicalCommission.Path<FefuTechnicalCommission> _technicalCommission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Заявление абитуриента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantRequest2TechnicalCommissionRelation#getEntrantRequest()
     */
        public EntrantRequest.Path<EntrantRequest> entrantRequest()
        {
            if(_entrantRequest == null )
                _entrantRequest = new EntrantRequest.Path<EntrantRequest>(L_ENTRANT_REQUEST, this);
            return _entrantRequest;
        }

    /**
     * @return Техническая комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantRequest2TechnicalCommissionRelation#getTechnicalCommission()
     */
        public FefuTechnicalCommission.Path<FefuTechnicalCommission> technicalCommission()
        {
            if(_technicalCommission == null )
                _technicalCommission = new FefuTechnicalCommission.Path<FefuTechnicalCommission>(L_TECHNICAL_COMMISSION, this);
            return _technicalCommission;
        }

        public Class getEntityClass()
        {
            return FefuEntrantRequest2TechnicalCommissionRelation.class;
        }

        public String getEntityName()
        {
            return "fefuEntrantRequest2TechnicalCommissionRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
