/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author Nikolay Fedorovskih
 * @since 11.06.2013
 */
@Configuration
public class FefuTechnicalCommissionsView extends BusinessComponentManager
{
    public static final String EMPLOYEE_POST_DS = "employeePostDS";
    public static final String TECHNICAL_COMMISSION_PARAM = "fefuTechCommission";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EMPLOYEE_POST_DS, getEmployeePostDS(), employeePostDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getEmployeePostDS()
    {
        return columnListExtPointBuilder(EMPLOYEE_POST_DS)
                .addColumn(textColumn("fio", FefuTechnicalCommissionEmployee.employeePost().person().fullFio()))
                .addColumn(textColumn("post", FefuTechnicalCommissionEmployee.employeePost().postRelation().postBoundedWithQGandQL().title()))
                .addColumn(textColumn("orgUnit", FefuTechnicalCommissionEmployee.employeePost().orgUnit().fullTitle()))
                .addColumn(textColumn("accessLimitation", FefuTechnicalCommissionEmployee.accessLimitation()).formatter(YesNoFormatter.IGNORE_NUMBERS))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                                        alert("employeePostDS.delete.alert", FefuTechnicalCommissionEmployee.employeePost().fullTitle())))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> employeePostDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                FefuTechnicalCommission technicalCommission = context.get(TECHNICAL_COMMISSION_PARAM);

                // Получаем список сотрудников привязанных к технической комиссии
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuTechnicalCommissionEmployee.class, "e")
                        .column(property("e"))
                        .where(eq(property(FefuTechnicalCommissionEmployee.technicalCommission().fromAlias("e")),
                                  value(technicalCommission)))
                        .order(property(FefuTechnicalCommissionEmployee.employeePost().person().identityCard().fullFio().fromAlias("e")));

                DSOutput result = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(false).build();
                result.setCountRecord(Math.max(result.getTotalSize(), 3));
                return result;
            }
        };
    }
}