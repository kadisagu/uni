package ru.tandemservice.unifefu.base.ext.TrJournal.util;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkColumn;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

/**
 * User: amakarova
 * Date: 17.09.13
 */
public class FefuMarkColumnUtil
{
    private static final DateFormatter DATE_FORMATTER = new DateFormatter("dd.MM.yyy");

    public static String tooltip(TrJournalMarkColumn column)
    {
        TrJournalEvent event = column.getJEvent();
        StringBuilder tooltipBuilder = new StringBuilder();
        tooltipBuilder.append("<div>").append(event.getShortTitle()).append("</div>");
        if (event.getTheme() != null)
            tooltipBuilder.append("<div>Тема: ").append(event.getTheme()).append("</div>");
        for (TrBrsCoefficientValue value : DataAccessServices.dao().getList(TrBrsCoefficientValue.class, TrBrsCoefficientValue.owner().s(), event, TrBrsCoefficientValue.definition().title().s()))
            tooltipBuilder.append("<div>").append(value.getDefinition().getShortTitle()).append(": ").append(value.getValueStr()).append("</div>");
        for (TrEduGroupEvent eduGroupEvent : DataAccessServices.dao().getList(TrEduGroupEvent.class, TrEduGroupEvent.journalEvent().s(), event)) {
            String strDate = eduGroupEvent.getScheduleEvent() != null ? DATE_FORMATTER.format(eduGroupEvent.getScheduleEvent().getDurationBegin()) : "";
            tooltipBuilder.append("<div>").append(strDate).append("</div>");
        }
        return tooltipBuilder.toString();
    }
}