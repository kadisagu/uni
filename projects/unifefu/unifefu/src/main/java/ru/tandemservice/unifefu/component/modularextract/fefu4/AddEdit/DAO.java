/* $Id: DAO.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.modularextract.fefu4.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuExcludeStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 22.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuExcludeStuExtract, Model>
{
    @Override
    protected FefuExcludeStuExtract createNewInstance()
    {
        return new FefuExcludeStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentStatusNew(UniDaoFacade.getCoreDao().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());
        super.update(model);
    }
}