package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x2_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewEduPlanVersionRow

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbvieweduplanversionrow_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("rowversion_id", DBType.LONG).setNullable(false), 
				new DBColumn("roweduhs_id", DBType.LONG).setNullable(false), 
				new DBColumn("rowowner_id", DBType.LONG), 
				new DBColumn("rowpath_p", DBType.createVarchar(255)), 
				new DBColumn("rowdsctype_p", DBType.createVarchar(255)), 
				new DBColumn("rowdscrel_p", DBType.createVarchar(255)), 
				new DBColumn("rowindex_p", DBType.createVarchar(255)), 
				new DBColumn("rowtitle_p", DBType.createVarchar(255)), 
				new DBColumn("rowterms_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewEduPlanVersionRow");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewEduPlanVersionRowTerm

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbvieweduplanversionrowterm_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("rowversionid_id", DBType.LONG).setNullable(false), 
				new DBColumn("rowpath_p", DBType.createVarchar(255)), 
				new DBColumn("term_p", DBType.INTEGER), 
				new DBColumn("weeks_p", DBType.createVarchar(255)), 
				new DBColumn("size_p", DBType.createVarchar(255)), 
				new DBColumn("labor_p", DBType.createVarchar(255)), 
				new DBColumn("eaudit_p", DBType.createVarchar(255)), 
				new DBColumn("alect_p", DBType.createVarchar(255)), 
				new DBColumn("apract_p", DBType.createVarchar(255)), 
				new DBColumn("alab_p", DBType.createVarchar(255)), 
				new DBColumn("eself_p", DBType.createVarchar(255)), 
				new DBColumn("actions_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewEduPlanVersionRowTerm");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewEppDevelopGridMeta

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbvieweppdevelopgridmeta_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("mdbvieweppeduplanversion_id", DBType.LONG).setNullable(false), 
				new DBColumn("developgridmeta_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewEppDevelopGridMeta");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewEppEduPlanVersion

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbvieweppeduplanversion_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("eppeduplanversion_id", DBType.LONG).setNullable(false), 
				new DBColumn("educationlevels_id", DBType.LONG).setNullable(false), 
				new DBColumn("educationlevelshighschool_id", DBType.LONG).setNullable(false), 
				new DBColumn("stdnumber_p", DBType.createVarchar(255)), 
				new DBColumn("plandevelopform_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("plandevelopcond_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("plandeveloptech_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("plandevelopgrid_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("plannumber_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("versionnumber_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("versiontitle_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewEppEduPlanVersion");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewEppEduPlanVersionBlock

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbvweppedplnvrsnblck_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("mdbvieweppeduplanversion_id", DBType.LONG).setNullable(false), 
				new DBColumn("educationlevelshighschool_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewEppEduPlanVersionBlock");

		}


    }
}