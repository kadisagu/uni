/* $Id$ */
package ru.tandemservice.unifefu.component.report.FefuEntrantIncomingListVar2.Add;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSetStructure;
import ru.tandemservice.uniec.entity.examset.ExamSetStructureItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.component.report.FefuReportUtil;
import ru.tandemservice.unifefu.component.report.ReportItem;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 25.07.2016
 * копипаста FefuEntrantSubmittedDocumentsReportBuilder
 */
@SuppressWarnings("Duplicates")
public class FefuEntrantIncomingListVar2ReportBuilder
{
    private Model _model;
    protected Session _session;
    protected RtfDocument _emptyTitleTemplate;
    protected RtfDocument _emptyTableTemplate;

    /**
     * map(EnrollmentDirection Id, list(RequestedEnrollmentDirection) )
     */
    private Map<Long, List<RequestedEnrollmentDirection>> _requestedDirectionByDirection;
    private Map<Long, FefuEnrollmentPlans> _enrollmentPlansByDirection;
    protected Map<EnrollmentDirection, EnrollmentDirectionExamSetData> _direction2examSetDetailMap;
    protected boolean isBudget;
    protected Map<Long, Set<EntrantIndividualProgress>> individualAchievementsMap;
    protected Map<Entrant, Map<Discipline2RealizationWayRelation, List<SubjectPassForm>>> _passFormByEntrantByDiscipline;

    /**
     * map(RequestedEnrollmentDirection Id, list(ChosenEntranceDiscipline) )
     */
    protected Map<Long, Set<ChosenEntranceDiscipline>> _chosenDisciplineByRequestedDirection;

    /**
     * map(EntrantRequest Id, list(RequestedEnrollmentDirection) )
     */
    private Map<Long, List<RequestedEnrollmentDirection>> _requestedDirectionByRequest;

    /**
     * map(Person Id, list(RequestedEnrollmentDirection) )
     */
    protected Map<Long, List<PersonBenefit>> personBenefits;

    /**
     * map(Entrant Id, list(EntrantEnrolmentRecommendation) )
     */
    protected Map<Long, List<EntrantEnrolmentRecommendation>> _entrantEnrolmentRecommendations;
    protected Map<CompetitionKind, Integer> _competitionKindPriorities;

    /**
     * map(EnrollmentDirection Id, map[StudentCategoryId, map[Discipline2RealizationWayRelation Id, Priority) ) )
     */
    protected Map<Long, Map<Long,  Map<Integer, List<Long>>>> _disciplinesByStudentCategoryByDirectionMap;

    // номера столбцов в шаблоне
    final int COL_NUM_INDEX = 0;
    final int COL_NUM_REG = 1;
    final int COL_NUM_FIO = 2;
    final int COL_NUM_MARKS = 3;
    final int COL_NUM_IND_ACH = 4;
    final int COL_NUM_SUM = 5;
    final int COL_NUM_CATEGORY = 6;
    final int COL_NUM_ORIGINAL_DOCUMENT = 7;
    final int COL_NUM_PRIORITY = 8;
    final int COL_NUM_COMMENT = 9;
    final int COL_NUM_EGE = 10;
    final int COL_NUM_AGREEMNT = 11;
    final int COL_NUM_ORIGINAL = 12;

    public FefuEntrantIncomingListVar2ReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    protected byte[] buildReport()
    {
        // получаем список направлений приема
        List<EnrollmentDirection> enrollmentDirections = FefuReportUtil.getEnrollmentDirectionList(_session, _model);

        _disciplinesByStudentCategoryByDirectionMap = FefuReportUtil.getDisciplinesByStudentCategoryByDirection(_session, enrollmentDirections);

        DQLSelectBuilder requestedDirectionsBuilder = FefuReportUtil.getRequestedEnrollmentDirections(_model, enrollmentDirections);
        List<RequestedEnrollmentDirection> requestedEnrollmentDirections = requestedDirectionsBuilder.createStatement(_session).list();


        // fetch data
        init(requestedEnrollmentDirections, requestedDirectionsBuilder);

        _requestedDirectionByDirection = requestedEnrollmentDirections.stream().collect(Collectors.groupingBy(
                red -> red.getEnrollmentDirection().getId(),
                Collectors.mapping(red -> red, Collectors.toList())
        ));

        // нужно запомнить информацию по местам и уже зачисленным абитуриентам:
        // для этого нужно взять весь список зачисленных по этому направлению
        List<RequestedEnrollmentDirection> enrolledRequestedEnrollmentDirections = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.ENTITY_CLASS, "r")
                .column(property("r"))
                .where(in(property("r", RequestedEnrollmentDirection.enrollmentDirection()), enrollmentDirections))
                .where(eq(property("r", RequestedEnrollmentDirection.compensationType()), value(_model.getCompensationType())))
                .where(eq(property("r", RequestedEnrollmentDirection.studentCategory().code()), value(StudentCategoryCodes.STUDENT_CATEGORY_STUDENT)))
                .where(eq(property("r", RequestedEnrollmentDirection.state().code()), value(EntrantStateCodes.ENROLED)))
                .createStatement(_session).list();
        Map<Long, List<RequestedEnrollmentDirection>> enrolledRequestedDirectionByDirection = enrolledRequestedEnrollmentDirections.stream().collect(Collectors.groupingBy(
                red -> red.getEnrollmentDirection().getId(),
                Collectors.mapping(red -> red, Collectors.toList())
        ));
        _enrollmentPlansByDirection = new HashMap<>();
        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
            _enrollmentPlansByDirection.put(enrollmentDirection.getId(), new FefuEnrollmentPlans(enrollmentDirection, enrolledRequestedDirectionByDirection.get(enrollmentDirection.getId())));


        RtfDocument document = prepareTemplates(requestedEnrollmentDirections.isEmpty() && !_model.isByAllEnrollmentDirections());

        TreeMap<String, List<EnrollmentDirection>> mainMap = enrollmentDirections.stream()
                // если нужно отфильтровываются направления без заявлений
                .filter(direction -> !(_model.getReport().isNotPrintSpesWithoutRequest()
                        && CollectionUtils.isEmpty(_requestedDirectionByDirection.get(direction.getId()))))
                // если нужно отфильтровываются направления без оставшихся мест
                .filter(direction -> !(_model.getReport().isNotShowTakenDirection()
                        && (_enrollmentPlansByDirection.get(direction.getId()).getAllLeft() == 0)))
                .collect(Collectors.groupingBy(
                        direction -> {
                            EducationOrgUnit educationOrgUnit = direction.getEducationOrgUnit();
                            return educationOrgUnit.getFormativeOrgUnit().getPrintTitle() + " (" + educationOrgUnit.getTerritorialOrgUnit().getFullTitle() + ")";
                        },
                        TreeMap::new,
                        Collectors.mapping(direction -> direction, Collectors.toList())
                ));

        boolean[] notFirst = {false};
        mainMap.entrySet().forEach(ouEntry -> {
                    if (notFirst[0])
                        FefuReportUtil.insertPageBreak(document.getElementList());
                    document.getElementList().addAll(getOuTable(ouEntry.getKey(), ouEntry.getValue()));
                    notFirst[0] = true;
                }

        );

        return RtfUtil.toByteArray(document);
    }

    protected RtfDocument prepareTemplates(boolean empty)
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniFefuDefines.TEMPLATE_ENTRANT_INCOMING_LIST_VAR2);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        IRtfElement searchResultTitle = UniRtfUtil.findElement(template.getElementList(), "titleOu");
        _emptyTitleTemplate = RtfBean.getElementFactory().createRtfDocument();
        _emptyTitleTemplate.getElementList().add(searchResultTitle);
        _emptyTitleTemplate.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        _emptyTitleTemplate.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        IRtfElement searchResultTable = UniRtfUtil.findElement(template.getElementList(), "T");
        _emptyTableTemplate = RtfBean.getElementFactory().createRtfDocument();
        _emptyTableTemplate.getElementList().add(searchResultTable.getClone());
        if (!empty)
        {
            template.getElementList().remove(searchResultTitle);
            template.getElementList().remove(searchResultTable);
        }

        String titleParam = "";
        if (_model.isIncludeEntrantNoMark())
            titleParam = " без баллов";
        if (_model.isIncludeEntrantWithBenefit())
            titleParam = ", имеющих льготы";
        if (_model.isIncludeEntrantTargetAdmission())
            titleParam = ", поступающих по целевому набору";
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("titleParam", titleParam);

        if (empty)
        {
            String titleOu = _model.getFormativeOrgUnit().getPrintTitle() + " (" + _model.getTerritorialOrgUnit().getFullTitle() + ")";
            injectModifier.put("titleOu", titleOu);
            RtfTableModifier tm = new RtfTableModifier();
            tm.put("T1", new String[][]{new String[3]});
            tm.put("T", new String[][]{});
            tm.modify(template);
        }
        injectModifier.modify(template);

        return template;
    }

    protected void init(List<RequestedEnrollmentDirection> requestedDirections, DQLSelectBuilder requestedDirectionsBuilder)
    {
        _requestedDirectionByRequest = FefuReportUtil.getOtherRequestedDirection(_session, requestedDirections);
        personBenefits = FefuReportUtil.getPersonBenefitsMap(_session, requestedDirectionsBuilder);

        _competitionKindPriorities = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(_model.getEnrollmentCampaign());

        _direction2examSetDetailMap = ExamSetUtil.getDirectionExamSetDataMap(
                _session, _model.getReport().getEnrollmentCampaign(), _model.getStudentCategoryList(), _model.getReport().getCompensationType());
        _direction2examSetDetailMap.forEach((direction, examSetData) -> {
            if (!examSetData.isStructuresEqual())
                throw new ApplicationException("Для направления приема '" + direction.getTitle() + "' по категориям поступающих и виду затрат наборы экзаменов различные.");
        });

        individualAchievementsMap = FefuReportUtil.getIndividualAchievementsMap(_session, requestedDirectionsBuilder);
        isBudget = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_model.getReport().getCompensationType().getCode());
        _chosenDisciplineByRequestedDirection = FefuReportUtil.getChosenDisciplineByRequestedDirection(_session, requestedDirectionsBuilder);
        _passFormByEntrantByDiscipline = ExamSetUtil.getExamPassDisciplineByEntrantByDiscipline(_session, requestedDirections);

        _entrantEnrolmentRecommendations = EntrantDataUtil.getEntrantEnrolmentRecommendations(_session, requestedDirectionsBuilder);
    }

    protected List<IRtfElement> getOuTable(String orgUnitTitle, List<EnrollmentDirection> directions)
    {
        RtfDocument result = _emptyTitleTemplate.getClone();
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("titleOu", orgUnitTitle);
        injectModifier.modify(result);
        directions.sort((o1, o2) -> o1.getEducationOrgUnit().getEducationLevelHighSchool().getTitle()
                .compareTo(o2.getEducationOrgUnit().getEducationLevelHighSchool().getTitle()));

        Iterator<EnrollmentDirection> iterator = directions.iterator();
        while (iterator.hasNext())
        {
            result.getElementList().addAll(getDirectionTable(iterator.next()));

            if (iterator.hasNext())
                FefuReportUtil.insertPageBreak(result.getElementList());
        }

        return result.getElementList();
    }

    protected List<IRtfElement> getDirectionTable(EnrollmentDirection direction)
    {
        List<RequestedEnrollmentDirection> requestedDirections = _requestedDirectionByDirection.get(direction.getId());
        if (requestedDirections == null)
            requestedDirections = new ArrayList<>();

        FefuEnrollmentPlans enrollmentPlans = _enrollmentPlansByDirection.get(direction.getId());

        RtfDocument table = _emptyTableTemplate.getClone();
        RtfTableModifier modifier = new RtfTableModifier();
        fillDirectionTableHead(modifier, direction, enrollmentPlans);
        fillDirectionTableBody(modifier, direction, requestedDirections, enrollmentPlans);
        modifier.modify(table);

        return table.getElementList();
    }

    protected void fillDirectionTableHead(RtfTableModifier modifier, EnrollmentDirection direction, FefuEnrollmentPlans enrollmentPlans)
    {
        int allLeft = enrollmentPlans.getAllLeft();
        int targetLeft = enrollmentPlans.getTargetLeft();
        int quotaLeft = enrollmentPlans.getQuotaLeft();

        String[][] headerRow = new String[1][4];
        headerRow[0][0] = direction.getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
        headerRow[0][1] = String.valueOf(allLeft);
        headerRow[0][2] = String.valueOf(targetLeft);
        headerRow[0][3] = String.valueOf(quotaLeft);

        modifier.put("T1", headerRow);
    }

    protected void fillDirectionTableBody(RtfTableModifier modifier, EnrollmentDirection direction, List<RequestedEnrollmentDirection> requestedDirections, FefuEnrollmentPlans enrollmentPlans)
    {
        // получаем структуру набора экзаменов текущего направления приема
        final EnrollmentDirectionExamSetData examSetData = _direction2examSetDetailMap.get(direction);


        Map<Long,  Map<Integer, List<Long>>> disciplinesByStudentCategoryMap =
                _disciplinesByStudentCategoryByDirectionMap.get(direction.getId());
        Map<Integer, List<Long>> disciplines = null;
        Long studentCategoryId = null;
        if (!requestedDirections.isEmpty())
            studentCategoryId = requestedDirections.iterator().next().getStudentCategory().getId();
        if (disciplinesByStudentCategoryMap != null && studentCategoryId != null)
            disciplines = disciplinesByStudentCategoryMap.get(studentCategoryId);

        List<ReportItem> reportItemList = requestedDirections.stream().sequential()
                .filter(red -> {
                    String entrantStateCode = red.getState().getCode();
                    // отображаем только абитуриентов в статусах "К зачислению" или "Пред. зачислен"
                    boolean isIncludeEntrant =
                            EntrantStateCodes.TO_BE_ENROLED.equals(entrantStateCode) ||
                            EntrantStateCodes.PRELIMENARY_ENROLLED.equals(entrantStateCode);
                    // если был проставлен чекбокс "Выводить данные по этапам зачисления", то покажем только тех, кто
                    // дал согласие (из абитуриентов со статусом "К зачислению", "Пред. зачислен", "Зачислен")
                    if (_model.getReport().isShowDataByEnrollmentStage())
                        isIncludeEntrant = red.isAgree4Enrollment() && (
                                EntrantStateCodes.TO_BE_ENROLED.equals(entrantStateCode) ||
                                EntrantStateCodes.PRELIMENARY_ENROLLED.equals(entrantStateCode) ||
                                EntrantStateCodes.ENROLED.equals(entrantStateCode)
                        );
                    return isIncludeEntrant;
                })
                .map(red -> getReportItem(red, examSetData))
                .sorted(new ReportItem.ItemComparator(_competitionKindPriorities, disciplines))
                .collect(Collectors.toList());

        // если стоит флаг "Выводить данные по этапам зачисления", то нужно отфильтровать список ещё раз
        if (_model.getReport().isShowDataByEnrollmentStage())
        {
            Set<RequestedEnrollmentDirection> includeSet = getIncludeSet(reportItemList, enrollmentPlans, false, false, 1);
            reportItemList = reportItemList.stream().sequential()
                    .filter(item -> includeSet.contains(item.getRequestedDirection()))
                    .collect(Collectors.toList());
        }
        int[] index = {1};
        String[][] data = reportItemList.stream().sequential()
                .map(item -> getTableRow(index[0]++, item, examSetData))
                .toArray(String[][]::new);

        modifier.put("T", data);
        modifier.put("T", new CustomRtfRowIntercepter(examSetData, getBoldRows(reportItemList, enrollmentPlans)));
    }

    /**
     * метод создаёт множество вНП, по которому будет работать фильтр (используется в обрезании списока и в выделении)
     * @param reportItemList список (это не список вНП, т.к. нужен отсортированный, а сортировка уже написана для reportItem)
     * @param enrollmentPlans информация о планах и кол-ве зачисленных
     * @param minusEnrolledPlace нужно ли вычитать кол-во уже занятых мест (отправлять false если в списке и так есть зачисленные)
     * @param part доля включаемых во множество абитуриентов по общим основаниям (используется для выделения не всех абитуриентов)
     * @return множество включаемых согласно переданным параметрам
     */
    protected Set<RequestedEnrollmentDirection> getIncludeSet(List<ReportItem> reportItemList, FefuEnrollmentPlans enrollmentPlans, boolean minusEnrolledPlace, boolean haveOriginalsOnly, double part)
    {
        int allLeft = minusEnrolledPlace ? enrollmentPlans.getAllLeft() : enrollmentPlans.getAllPlan();
        int targetLeft = minusEnrolledPlace ? enrollmentPlans.getTargetLeft() : enrollmentPlans.getTargetPlan();
        int quotaLeft = minusEnrolledPlace ? enrollmentPlans.getQuotaLeft() : enrollmentPlans.getQuotaPlan();

        Set<RequestedEnrollmentDirection> includeSet = new HashSet<>();
        for (ReportItem reportItem : reportItemList)
        {
            RequestedEnrollmentDirection red = reportItem.getRequestedDirection();
            // нужно проверить есть ли у них оригиналы
            if (!haveOriginalsOnly || red.isOriginalDocumentHandedIn())
            {
                if (isBudget && (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(red.getCompetitionKind().getCode()) ||
                        UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(red.getCompetitionKind().getCode())))
                {
                    if (quotaLeft > 0)
                    {
                        includeSet.add(reportItem.getRequestedDirection());
                        --quotaLeft;
                        --allLeft;
                    }
                }
                else if (red.isTargetAdmission())
                {
                    if (targetLeft > 0)
                    {
                        includeSet.add(reportItem.getRequestedDirection());
                        --targetLeft;
                        --allLeft;
                    }
                }
                else
                {
                    // (небольшой костыль (до этого места мы дойдём в конце списока))
                    // возможно нужно выделить только часть от оставшихся абитуриентов
                    // поэтому урежем кол-во оставшихся мест
                    if (allLeft > 0 && part < 1)
                    {
                        allLeft = (int)Math.floor(allLeft * part);
                        part = 1;
                    }
                    if (allLeft > 0)
                    {
                        includeSet.add(reportItem.getRequestedDirection());
                        --allLeft;
                    }
                }
            }
        }
        return includeSet;
    }

    /**
     * @return множество индексов строк, которые будут выделены жирным
     */
    protected Set<Integer> getBoldRows(List<ReportItem> reportItemList, FefuEnrollmentPlans enrollmentPlans)
    {
        Set<Integer> result = null;
        int firstRowIndex = 0;
        if (_model.getReport().isShowDataByEnrollmentStage())
        {
            result = new HashSet<>();
            Set<RequestedEnrollmentDirection> includeSet = getIncludeSet(reportItemList, enrollmentPlans, false, false, 0.8);
            int rowIndex = firstRowIndex;
            for (ReportItem reportItem : reportItemList)
            {
                if (includeSet.contains(reportItem.getRequestedDirection())) result.add(rowIndex);
                ++rowIndex;
            }
        }
        else if (_model.getReport().isOrderByOriginals())
        {
            result = new HashSet<>();
            Set<RequestedEnrollmentDirection> includeSet = getIncludeSet(reportItemList, enrollmentPlans, true, true, 1);
            int rowIndex = firstRowIndex;
            for (ReportItem reportItem : reportItemList)
            {
                if (includeSet.contains(reportItem.getRequestedDirection())) result.add(rowIndex);
                ++rowIndex;
            }
        }
        return result;
    }

    protected ReportItem getReportItem(RequestedEnrollmentDirection requestedDirection, EnrollmentDirectionExamSetData examSetData)
    {
        ReportItem item = new ReportItem(requestedDirection);

        EntrantRequest request = requestedDirection.getEntrantRequest();
        Entrant entrant = request.getEntrant();
        Person person = entrant.getPerson();

        ExamSetStructure examSetStructure = examSetData != null ? examSetData.getExamSetStructure() : null;
        List<ExamSetStructureItem> examSetStructureItems = examSetStructure != null ? examSetStructure.getItemList() : null;

        Set<ChosenEntranceDiscipline> chosenEntranceDisciplines = _chosenDisciplineByRequestedDirection.get(requestedDirection.getId());
        ChosenEntranceDiscipline[] chosenDistribution = MarkDistributionUtil.getChosenDistribution(
                chosenEntranceDisciplines != null ? chosenEntranceDisciplines : new HashSet<>(), examSetStructureItems);
        item.setChosenDistribution(chosenDistribution);

        Map<Discipline2RealizationWayRelation, List<SubjectPassForm>> passFormsByDiscipline = _passFormByEntrantByDiscipline.get(entrant);
        item.setChosenDisciplinesByWay(Arrays.asList(chosenDistribution).stream()
                .filter(cho -> cho != null)
                .peek(cho -> {
                    Discipline2RealizationWayRelation discipline = cho.getEnrollmentCampaignDiscipline();
                    List<SubjectPassForm> passForms = passFormsByDiscipline == null ? null : passFormsByDiscipline.get(discipline);
                    FefuReportUtil.addPassFormNSummMark(item, passForms, cho);
                })
                .collect(Collectors.toMap(cho -> cho.getEnrollmentCampaignDiscipline().getId(), cho -> cho)));
        item.setPassFormsByDiscipline(_passFormByEntrantByDiscipline.get(entrant));

        List<RequestedEnrollmentDirection> otherRequestedDirections = _requestedDirectionByRequest.get(requestedDirection.getEntrantRequest().getId());
        EnrollmentCampaign campaign = requestedDirection.getEnrollmentDirection().getEnrollmentCampaign();
        boolean originalOnly = campaign.isUseCompetitionGroup() || campaign.isOriginalDocumentPerDirection();
        RequestedEnrollmentDirection original = otherRequestedDirections.stream()
                .filter(red -> red.isOriginalDocumentHandedIn() || !originalOnly)
                .sorted((o1, o2) -> Integer.compare(o1.getPriority(), o2.getPriority()))
                .findFirst().orElse(null);
        item.setOrigReqDir(original);

        item.setIndividualProgresses(individualAchievementsMap.get(request.getId()));
        item.setBenefits(personBenefits.get(person.getId()));
        item.setEnrolmentRecommendations(_entrantEnrolmentRecommendations.get(entrant.getId()));

        return item;
    }

    protected String[] getTableRow(int index, ReportItem item, EnrollmentDirectionExamSetData examSetData)
    {
        List<String> result = new ArrayList<>();

        RequestedEnrollmentDirection requestedDirection = item.getRequestedDirection();
        Entrant entrant = requestedDirection.getEntrantRequest().getEntrant();
        Person person = entrant.getPerson();
        ExamSetStructure examSetStructure = examSetData != null ? examSetData.getExamSetStructure() : null;
        List<ExamSetStructureItem> examSetStructureItems = examSetStructure != null ? examSetStructure.getItemList() : null;

        result.add(Integer.toString(index));
        result.add(FefuReportUtil.getRegNumber(requestedDirection));
        result.add(person.getFullFio());

        if (examSetStructureItems == null || examSetStructureItems.isEmpty())
            result.add("");
        else
        {
            for (ChosenEntranceDiscipline chosenDiscipline : item.getChosenDistribution())
            {

                String markStr;
                if (chosenDiscipline == null) markStr = "x";
                else
                {
                    Double mark = chosenDiscipline.getFinalMark();
                    if (mark != null) markStr = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark);
                    else markStr = "";
                }
                if (!_model.getReport().isWithoutDetailSumMark())
                    result.add(markStr);
            }
        }

        // индивидуальные достижения нужно проставить только если они используются в приёмной кампании
        if (_model.getEnrollmentCampaign().isUseIndividualProgressInAmountMark())
        {
            Collection<EntrantIndividualProgress> progressList = item.getIndividualProgresses();
            String progress = "";
            if (!CollectionUtils.isEmpty(progressList))
                progress = progressList.stream().map(ip -> ip.getIndividualProgressType().getShortTitle()).collect(Collectors.joining(", "));
            result.add(progress);
        }

        result.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getFinalSumMark()));
        result.add(FefuReportUtil.getCategory(requestedDirection, _model.isIncludeEntrantTargetAdmission(),
                !CollectionUtils.isEmpty(item.getBenefits()), !CollectionUtils.isEmpty(item.getEnrolmentRecommendations())));
        result.add(item.getPassForm("+", "-"));
//        result.add(String.valueOf(requestedDirection.getPriority())); Колонка "Приор."

        if (_model.getReport().getIncludeEntrantWithBenefit())
        {
            List<PersonBenefit> benefits = personBenefits.get(person.getId());
            String strBenefits = "";
            if (!CollectionUtils.isEmpty(benefits))
                strBenefits = benefits.stream().map(PersonBenefit::getTitle).collect(Collectors.joining("; "));
            result.add(strBenefits);
        }

        result.add(requestedDirection.isOriginalDocumentHandedIn() ? "п" : "к");
        result.add(requestedDirection.isAgree4Enrollment() ? "+" : "-");

        if (_model.isWithOriginalColumn())
        {
            RequestedEnrollmentDirection origReqDir = item.getOrigReqDir(); //Колонка "Ориг"
            String origReqDirStr = "";
            if (origReqDir != null)
                origReqDirStr = origReqDir.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle();
            result.add(origReqDirStr);
        }

        return result.toArray(new String[result.size()]);
    }

    protected class CustomRtfRowIntercepter extends RtfRowIntercepterBase {
        EnrollmentDirectionExamSetData _examSetData;
        Set<Integer> _boldRows;

        public CustomRtfRowIntercepter(EnrollmentDirectionExamSetData examSetData, Set<Integer> boldRows)
        {
            _examSetData = examSetData;
            _boldRows = boldRows;
        }

        @Override
        public void beforeModify(RtfTable table, int currentRowIndex)
        {
            RtfRow headRow = table.getRowList().get(currentRowIndex - 1);
            List<RtfCell> headerCellList = headRow.getCellList();
            RtfRow firstRow = table.getRowList().get(currentRowIndex);
            List<RtfCell> cellList = firstRow.getCellList();
            int freeWidth = 0;

            List<RtfCell> forHeaderDelete = new ArrayList<>();
            List<RtfCell> forDelete = new ArrayList<>();
            if (!_model.getReport().getIncludeEntrantWithBenefit())
            {
                forHeaderDelete.add(headerCellList.get(COL_NUM_COMMENT));
                forDelete.add(cellList.get(COL_NUM_COMMENT));
                freeWidth += cellList.get(COL_NUM_COMMENT).getWidth();
            }

            if (!_model.getEnrollmentCampaign().isUseIndividualProgressInAmountMark())
            {
                forHeaderDelete.add(headerCellList.get(COL_NUM_IND_ACH));
                forDelete.add(cellList.get(COL_NUM_IND_ACH));
                freeWidth += cellList.get(COL_NUM_IND_ACH).getWidth();
            }

            {// Колонка "Приор." ненужна, но в шаблоне есть
                forHeaderDelete.add(headerCellList.get(COL_NUM_PRIORITY));
                forDelete.add(cellList.get(COL_NUM_PRIORITY));
                freeWidth += cellList.get(COL_NUM_PRIORITY).getWidth();
            }

            if (!_model.isWithOriginalColumn())
            {
                forHeaderDelete.add(headerCellList.get(COL_NUM_ORIGINAL));
                forDelete.add(cellList.get(COL_NUM_ORIGINAL));
                freeWidth += cellList.get(COL_NUM_ORIGINAL).getWidth();
            }

            headerCellList.removeAll(forHeaderDelete);
            cellList.removeAll(forDelete);

            List<ExamSetStructureItem> examSetStructureItems = _examSetData.getExamSetStructure().getItemList();
            if (!examSetStructureItems.isEmpty())
            {
                List<StudentCategory> listCategory = _model.getStudentCategoryList();
                if (listCategory == null || listCategory.isEmpty())
                    listCategory = IUniDao.instance.get().getCatalogItemList(StudentCategory.class);

                FefuReportUtil.splitMarksColumns(headRow, firstRow, COL_NUM_MARKS, _examSetData, listCategory, _model.getReport().isShowDisciplineTitles());
            }

            int newFioWidth = headerCellList.get(COL_NUM_FIO).getWidth() + freeWidth;
            headerCellList.get(COL_NUM_FIO).setWidth(newFioWidth);
            cellList.get(COL_NUM_FIO).setWidth(newFioWidth);
        }

        @Override
        public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
        {
            return (_boldRows != null && _boldRows.contains(rowIndex)) ? new RtfString().boldBegin().append(value).boldEnd().toList() : null;
        }
    }

    /**
     * Планы и квоты зачисления, а так же кол-во уже зачисленных по ним
     */
    protected class FefuEnrollmentPlans
    {
        private int _allPlan = 0;
        private int _targetPlan = 0;
        private int _quotaPlan = 0;

        private int _allEnrolled = 0;
        private int _targetEnrolled = 0;
        private int _quotaEnrolled = 0;

        public FefuEnrollmentPlans(EnrollmentDirection direction, List<RequestedEnrollmentDirection> enrolledList)
        {
            if (direction != null)
            {
                if (enrolledList != null)
                {
                    // подсчитаем кол-во уже зачисленных
                    enrolledList.stream().sequential()
                            .forEach(red -> {
                                        if (isBudget && (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(red.getCompetitionKind().getCode()) ||
                                                UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(red.getCompetitionKind().getCode())))
                                            ++_quotaEnrolled;
                                        else if (red.isTargetAdmission())
                                            ++_targetEnrolled;
                                        ++_allEnrolled;
                                    }
                            );
                }

                if (isBudget)
                    _allPlan = direction.getMinisterialPlan() != null ? direction.getMinisterialPlan() : 0;
                else
                    _allPlan = direction.getContractPlan() != null ? direction.getContractPlan() : 0;
                if (isBudget)
                    _targetPlan = direction.getTargetAdmissionPlanBudget() != null ? direction.getTargetAdmissionPlanBudget() : 0;
                else
                    _targetPlan = direction.getTargetAdmissionPlanContract() != null ? direction.getTargetAdmissionPlanContract() : 0;
                _quotaPlan = direction.getSpecRightsQuota() != null ? direction.getSpecRightsQuota() : 0;
            }
        }

        public int getAllPlan()
        {
            return _allPlan;
        }

        public int getTargetPlan()
        {
            return _targetPlan;
        }

        public int getQuotaPlan()
        {
            return _quotaPlan;
        }

        public int getAllLeft()
        {
            return Math.max(_allPlan - _allEnrolled, 0);
        }

        public int getTargetLeft()
        {
            return Math.max(_targetPlan - _targetEnrolled, 0);
        }

        public int getQuotaLeft()
        {
            return Math.max(_quotaPlan - _quotaEnrolled, 0);
        }
    }
}
