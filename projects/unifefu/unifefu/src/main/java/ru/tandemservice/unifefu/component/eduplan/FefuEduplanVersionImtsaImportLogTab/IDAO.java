/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduplanVersionImtsaImportLogTab;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 05.09.2013
 */
public interface IDAO extends IUniDao<Model>
{
}