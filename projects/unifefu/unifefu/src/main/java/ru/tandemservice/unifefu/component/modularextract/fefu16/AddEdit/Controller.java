/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu16.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;

/**
 * @author nvankov
 * @since 11/20/13
 */
public class Controller  extends CommonModularStudentExtractAddEditController<FullStateMaintenanceStuExtract, IDAO, Model>
{
    public void onPaymentTypeChange(IBusinessComponent component)
    {
        Model model = getModel(component);
        int index = component.getListenerParameter();
        FefuOrphanPayment payment = model.getPayments().get(index);
        FefuOrphanPaymentType type = model.getPayments().get(index).getType();
        if(null != type)
            payment.setPrint(type.getPrint());
    }

    public void onClickAddPayment(IBusinessComponent component)
    {
        Model model = getModel(component);
        addPayment(model);
    }

    private void addPayment(Model model)
    {
        FefuOrphanPayment newPayment = new FefuOrphanPayment();
        newPayment.setExtract(model.getExtract());
        model.getPayments().add(newPayment);
    }

    public void onClickDeletePayment(IBusinessComponent component)
    {
        Model model = getModel(component);
        int index = component.getListenerParameter();
        model.getPayments().remove(index);
    }
//
//    public void onResponsibleForPaymentsChange(IBusinessComponent component)
//    {
//        Model model = getModel(component);
//        if(null != model.getExtract().getResponsibleForPayments())
//        {
//            model.getExtract().setResponsibleForPaymentsStr();
//        }
//    }
//
//    public void onResponsibleForAgreementChange(IBusinessComponent component)
//    {
//        Model model = getModel(component);
//        if(null != model.getExtract().getResponsibleForAgreement())
//            model.getExtract().setResponsibleForAgreementStr();
//    }
//
//    Dative
//
//    Dative INSTRUMENTAL

    private String getEmployeeStrInst(EmployeePost employeePost)
    {
        StringBuilder emplPostStrBuilder = new StringBuilder();

        String emplPostIT = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getInstrumentalCaseTitle();
        String emplPostInstrumental = !StringUtils.isEmpty(emplPostIT) ? emplPostIT :
                employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(emplPostInstrumental))
            emplPostStrBuilder.append(StringUtils.isEmpty(emplPostStrBuilder.toString()) ? "" : " ").append(emplPostInstrumental.toLowerCase());

        emplPostStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedIofInitials(employeePost.getPerson().getIdentityCard(), GrammaCase.INSTRUMENTAL));

        return emplPostStrBuilder.toString();
    }

    private String getEmployeeStrDat(EmployeePost employeePost)
    {
        StringBuilder emplPostStrBuilder = new StringBuilder();

        String emplPostIT = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getDativeCaseTitle();
        String emplPostDative = !StringUtils.isEmpty(emplPostIT) ? emplPostIT :
                employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(emplPostDative))
            emplPostStrBuilder.append(StringUtils.isEmpty(emplPostStrBuilder.toString()) ? "" : " ").append(emplPostDative.toLowerCase());

        emplPostStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedIofInitials(employeePost.getPerson().getIdentityCard(), GrammaCase.DATIVE));

        return emplPostStrBuilder.toString();
    }
}
