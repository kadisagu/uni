/*$Id$*/
package ru.tandemservice.unifefu.component.student.ControlActionMarkHistory;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.SessionSheetMarkBy;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

/**
 * @author DMITRY KNYAZEV
 * @since 18.09.2015
 */
public class Model extends ru.tandemservice.unisession.component.student.ControlActionMarkHistory.Model
{
    public String getCurrentMarkBy()
    {
        SessionDocumentSlot current = getCurrent();
        SessionDocument document = current.getDocument();
        SessionSheetMarkBy markBy = DataAccessServices.dao().get(SessionSheetMarkBy.class, SessionSheetMarkBy.sessionDocument(), document);
        return markBy == null ? "" : markBy.getPrincipalContext().getFio();
    }
}
