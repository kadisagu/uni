package ru.tandemservice.unifefu.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport;
import ru.tandemservice.unifefu.entity.report.FefuEntrantRequestReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ежедневный рейтинг абитуриентов по конкурсным группам (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEntrantRatingReportGen extends FefuEntrantRequestReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport";
    public static final String ENTITY_NAME = "fefuEntrantRatingReport";
    public static final int VERSION_HASH = -127740931;
    private static IEntityMeta ENTITY_META;

    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_QUALIFICATION_STR = "qualificationStr";
    public static final String P_COMPETITION_GROUP_STR = "competitionGroupStr";
    public static final String P_FORMATIVE_ORG_UNIT_STR = "formativeOrgUnitStr";
    public static final String P_TERRITORIAL_ORG_UNIT_STR = "territorialOrgUnitStr";
    public static final String P_INCLUDE_FOREIGN_PERSON = "includeForeignPerson";

    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _qualificationStr;     // Квалификация
    private String _competitionGroupStr;     // Конкурсная группа
    private String _formativeOrgUnitStr;     // Формирующее подразделение
    private String _territorialOrgUnitStr;     // Территориальное подразделение
    private boolean _includeForeignPerson;     // Учитывать иностранных граждан

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationStr()
    {
        return _qualificationStr;
    }

    /**
     * @param qualificationStr Квалификация.
     */
    public void setQualificationStr(String qualificationStr)
    {
        dirty(_qualificationStr, qualificationStr);
        _qualificationStr = qualificationStr;
    }

    /**
     * @return Конкурсная группа.
     */
    @Length(max=255)
    public String getCompetitionGroupStr()
    {
        return _competitionGroupStr;
    }

    /**
     * @param competitionGroupStr Конкурсная группа.
     */
    public void setCompetitionGroupStr(String competitionGroupStr)
    {
        dirty(_competitionGroupStr, competitionGroupStr);
        _competitionGroupStr = competitionGroupStr;
    }

    /**
     * @return Формирующее подразделение.
     */
    @Length(max=255)
    public String getFormativeOrgUnitStr()
    {
        return _formativeOrgUnitStr;
    }

    /**
     * @param formativeOrgUnitStr Формирующее подразделение.
     */
    public void setFormativeOrgUnitStr(String formativeOrgUnitStr)
    {
        dirty(_formativeOrgUnitStr, formativeOrgUnitStr);
        _formativeOrgUnitStr = formativeOrgUnitStr;
    }

    /**
     * @return Территориальное подразделение.
     */
    @Length(max=255)
    public String getTerritorialOrgUnitStr()
    {
        return _territorialOrgUnitStr;
    }

    /**
     * @param territorialOrgUnitStr Территориальное подразделение.
     */
    public void setTerritorialOrgUnitStr(String territorialOrgUnitStr)
    {
        dirty(_territorialOrgUnitStr, territorialOrgUnitStr);
        _territorialOrgUnitStr = territorialOrgUnitStr;
    }

    /**
     * @return Учитывать иностранных граждан. Свойство не может быть null.
     */
    @NotNull
    public boolean isIncludeForeignPerson()
    {
        return _includeForeignPerson;
    }

    /**
     * @param includeForeignPerson Учитывать иностранных граждан. Свойство не может быть null.
     */
    public void setIncludeForeignPerson(boolean includeForeignPerson)
    {
        dirty(_includeForeignPerson, includeForeignPerson);
        _includeForeignPerson = includeForeignPerson;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuEntrantRatingReportGen)
        {
            setCompensationType(((FefuEntrantRatingReport)another).getCompensationType());
            setQualificationStr(((FefuEntrantRatingReport)another).getQualificationStr());
            setCompetitionGroupStr(((FefuEntrantRatingReport)another).getCompetitionGroupStr());
            setFormativeOrgUnitStr(((FefuEntrantRatingReport)another).getFormativeOrgUnitStr());
            setTerritorialOrgUnitStr(((FefuEntrantRatingReport)another).getTerritorialOrgUnitStr());
            setIncludeForeignPerson(((FefuEntrantRatingReport)another).isIncludeForeignPerson());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEntrantRatingReportGen> extends FefuEntrantRequestReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEntrantRatingReport.class;
        }

        public T newInstance()
        {
            return (T) new FefuEntrantRatingReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "compensationType":
                    return obj.getCompensationType();
                case "qualificationStr":
                    return obj.getQualificationStr();
                case "competitionGroupStr":
                    return obj.getCompetitionGroupStr();
                case "formativeOrgUnitStr":
                    return obj.getFormativeOrgUnitStr();
                case "territorialOrgUnitStr":
                    return obj.getTerritorialOrgUnitStr();
                case "includeForeignPerson":
                    return obj.isIncludeForeignPerson();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "qualificationStr":
                    obj.setQualificationStr((String) value);
                    return;
                case "competitionGroupStr":
                    obj.setCompetitionGroupStr((String) value);
                    return;
                case "formativeOrgUnitStr":
                    obj.setFormativeOrgUnitStr((String) value);
                    return;
                case "territorialOrgUnitStr":
                    obj.setTerritorialOrgUnitStr((String) value);
                    return;
                case "includeForeignPerson":
                    obj.setIncludeForeignPerson((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "compensationType":
                        return true;
                case "qualificationStr":
                        return true;
                case "competitionGroupStr":
                        return true;
                case "formativeOrgUnitStr":
                        return true;
                case "territorialOrgUnitStr":
                        return true;
                case "includeForeignPerson":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "compensationType":
                    return true;
                case "qualificationStr":
                    return true;
                case "competitionGroupStr":
                    return true;
                case "formativeOrgUnitStr":
                    return true;
                case "territorialOrgUnitStr":
                    return true;
                case "includeForeignPerson":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "compensationType":
                    return CompensationType.class;
                case "qualificationStr":
                    return String.class;
                case "competitionGroupStr":
                    return String.class;
                case "formativeOrgUnitStr":
                    return String.class;
                case "territorialOrgUnitStr":
                    return String.class;
                case "includeForeignPerson":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEntrantRatingReport> _dslPath = new Path<FefuEntrantRatingReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEntrantRatingReport");
    }
            

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#getQualificationStr()
     */
    public static PropertyPath<String> qualificationStr()
    {
        return _dslPath.qualificationStr();
    }

    /**
     * @return Конкурсная группа.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#getCompetitionGroupStr()
     */
    public static PropertyPath<String> competitionGroupStr()
    {
        return _dslPath.competitionGroupStr();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#getFormativeOrgUnitStr()
     */
    public static PropertyPath<String> formativeOrgUnitStr()
    {
        return _dslPath.formativeOrgUnitStr();
    }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#getTerritorialOrgUnitStr()
     */
    public static PropertyPath<String> territorialOrgUnitStr()
    {
        return _dslPath.territorialOrgUnitStr();
    }

    /**
     * @return Учитывать иностранных граждан. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#isIncludeForeignPerson()
     */
    public static PropertyPath<Boolean> includeForeignPerson()
    {
        return _dslPath.includeForeignPerson();
    }

    public static class Path<E extends FefuEntrantRatingReport> extends FefuEntrantRequestReport.Path<E>
    {
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _qualificationStr;
        private PropertyPath<String> _competitionGroupStr;
        private PropertyPath<String> _formativeOrgUnitStr;
        private PropertyPath<String> _territorialOrgUnitStr;
        private PropertyPath<Boolean> _includeForeignPerson;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#getQualificationStr()
     */
        public PropertyPath<String> qualificationStr()
        {
            if(_qualificationStr == null )
                _qualificationStr = new PropertyPath<String>(FefuEntrantRatingReportGen.P_QUALIFICATION_STR, this);
            return _qualificationStr;
        }

    /**
     * @return Конкурсная группа.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#getCompetitionGroupStr()
     */
        public PropertyPath<String> competitionGroupStr()
        {
            if(_competitionGroupStr == null )
                _competitionGroupStr = new PropertyPath<String>(FefuEntrantRatingReportGen.P_COMPETITION_GROUP_STR, this);
            return _competitionGroupStr;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#getFormativeOrgUnitStr()
     */
        public PropertyPath<String> formativeOrgUnitStr()
        {
            if(_formativeOrgUnitStr == null )
                _formativeOrgUnitStr = new PropertyPath<String>(FefuEntrantRatingReportGen.P_FORMATIVE_ORG_UNIT_STR, this);
            return _formativeOrgUnitStr;
        }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#getTerritorialOrgUnitStr()
     */
        public PropertyPath<String> territorialOrgUnitStr()
        {
            if(_territorialOrgUnitStr == null )
                _territorialOrgUnitStr = new PropertyPath<String>(FefuEntrantRatingReportGen.P_TERRITORIAL_ORG_UNIT_STR, this);
            return _territorialOrgUnitStr;
        }

    /**
     * @return Учитывать иностранных граждан. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport#isIncludeForeignPerson()
     */
        public PropertyPath<Boolean> includeForeignPerson()
        {
            if(_includeForeignPerson == null )
                _includeForeignPerson = new PropertyPath<Boolean>(FefuEntrantRatingReportGen.P_INCLUDE_FOREIGN_PERSON, this);
            return _includeForeignPerson;
        }

        public Class getEntityClass()
        {
            return FefuEntrantRatingReport.class;
        }

        public String getEntityName()
        {
            return "fefuEntrantRatingReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
