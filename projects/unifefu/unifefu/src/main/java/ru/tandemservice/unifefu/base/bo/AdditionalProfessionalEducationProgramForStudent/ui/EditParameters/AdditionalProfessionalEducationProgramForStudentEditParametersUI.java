package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.ui.EditParameters;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.AdditionalProfessionalEducationProgramForStudentManager;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters;

import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic.Constants.STUDENT_ID;

@Input({
               @Bind(key = STUDENT_ID, binding = STUDENT_ID)
       })
public class AdditionalProfessionalEducationProgramForStudentEditParametersUI extends UIPresenter
{
    private FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters programIndividual;

    private FefuAdditionalProfessionalEducationProgramForStudent programForStudent;

    private Long studentId;

    public Long getStudentId()
    {
        return studentId;
    }

    public void setStudentId(Long studentId)
    {
        this.studentId = studentId;
    }

    public FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters getProgramIndividual()
    {
        return programIndividual;
    }

    public void setProgramIndividual(FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters programIndividual)
    {
        this.programIndividual = programIndividual;
    }

    @Override
    public void onComponentRefresh()
    {
        if (studentId == null)
            return;

        programForStudent = AdditionalProfessionalEducationProgramForStudentManager.instance().apeProgramForStudentDao().getApeProgramForStudentByStudentId(studentId);

        if (programForStudent != null)
        {
            programIndividual = programForStudent.getIndividualParameters();

            if (programIndividual == null)
            {
                programIndividual = new FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters();
            }
        }
    }

    public void onClickApply()
    {
        if (programIndividual != null)
        {
            DataAccessServices.dao().saveOrUpdate(programIndividual);
        }

        if (programForStudent != null)
        {
            programForStudent.setIndividualParameters(programIndividual);
            DataAccessServices.dao().saveOrUpdate(programForStudent);
        }

        deactivate();
    }

}
