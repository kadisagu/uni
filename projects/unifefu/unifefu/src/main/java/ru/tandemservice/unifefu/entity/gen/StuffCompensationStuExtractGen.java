package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.unifefu.entity.StuffCompensationStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О выплате компенсации взамен одежды, обуви, мягкого инвентаря и оборудования (при выпуске)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StuffCompensationStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.StuffCompensationStuExtract";
    public static final String ENTITY_NAME = "stuffCompensationStuExtract";
    public static final int VERSION_HASH = -406902958;
    private static IEntityMeta ENTITY_META;

    public static final String P_PROTOCOL_DATE = "protocolDate";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";
    public static final String P_COMPENSATION_SUM = "compensationSum";
    public static final String P_IMMEDIATE_SUM = "immediateSum";
    public static final String P_MATCHING_PERSON_FIO = "matchingPersonFio";
    public static final String P_RESPONSIBLE_PERSON_FIO = "responsiblePersonFio";
    public static final String L_MATCHING_PERSON = "matchingPerson";
    public static final String L_RESPONSIBLE_PERSON = "responsiblePerson";
    public static final String P_MATCHING_DATE = "matchingDate";

    private Date _protocolDate;     // Дата протокола
    private String _protocolNumber;     // Номер протокола
    private long _compensationSum;     // Сумма компенсации (коп.)
    private long _immediateSum;     // Единовременная выплата (коп.)
    private String _matchingPersonFio;     // Согласующее лицо (печать)
    private String _responsiblePersonFio;     // Ответственное лицо (печать)
    private EmployeePost _matchingPerson;     // Согласующее лицо
    private EmployeePost _responsiblePerson;     // Ответственное лицо
    private Date _matchingDate;     // Дата согласования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата протокола. Свойство не может быть null.
     */
    @NotNull
    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    /**
     * @param protocolDate Дата протокола. Свойство не может быть null.
     */
    public void setProtocolDate(Date protocolDate)
    {
        dirty(_protocolDate, protocolDate);
        _protocolDate = protocolDate;
    }

    /**
     * @return Номер протокола. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола. Свойство не может быть null.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    /**
     * @return Сумма компенсации (коп.). Свойство не может быть null.
     */
    @NotNull
    public long getCompensationSum()
    {
        return _compensationSum;
    }

    /**
     * @param compensationSum Сумма компенсации (коп.). Свойство не может быть null.
     */
    public void setCompensationSum(long compensationSum)
    {
        dirty(_compensationSum, compensationSum);
        _compensationSum = compensationSum;
    }

    /**
     * @return Единовременная выплата (коп.). Свойство не может быть null.
     */
    @NotNull
    public long getImmediateSum()
    {
        return _immediateSum;
    }

    /**
     * @param immediateSum Единовременная выплата (коп.). Свойство не может быть null.
     */
    public void setImmediateSum(long immediateSum)
    {
        dirty(_immediateSum, immediateSum);
        _immediateSum = immediateSum;
    }

    /**
     * @return Согласующее лицо (печать). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getMatchingPersonFio()
    {
        return _matchingPersonFio;
    }

    /**
     * @param matchingPersonFio Согласующее лицо (печать). Свойство не может быть null.
     */
    public void setMatchingPersonFio(String matchingPersonFio)
    {
        dirty(_matchingPersonFio, matchingPersonFio);
        _matchingPersonFio = matchingPersonFio;
    }

    /**
     * @return Ответственное лицо (печать). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getResponsiblePersonFio()
    {
        return _responsiblePersonFio;
    }

    /**
     * @param responsiblePersonFio Ответственное лицо (печать). Свойство не может быть null.
     */
    public void setResponsiblePersonFio(String responsiblePersonFio)
    {
        dirty(_responsiblePersonFio, responsiblePersonFio);
        _responsiblePersonFio = responsiblePersonFio;
    }

    /**
     * @return Согласующее лицо. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getMatchingPerson()
    {
        return _matchingPerson;
    }

    /**
     * @param matchingPerson Согласующее лицо. Свойство не может быть null.
     */
    public void setMatchingPerson(EmployeePost matchingPerson)
    {
        dirty(_matchingPerson, matchingPerson);
        _matchingPerson = matchingPerson;
    }

    /**
     * @return Ответственное лицо. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getResponsiblePerson()
    {
        return _responsiblePerson;
    }

    /**
     * @param responsiblePerson Ответственное лицо. Свойство не может быть null.
     */
    public void setResponsiblePerson(EmployeePost responsiblePerson)
    {
        dirty(_responsiblePerson, responsiblePerson);
        _responsiblePerson = responsiblePerson;
    }

    /**
     * @return Дата согласования. Свойство не может быть null.
     */
    @NotNull
    public Date getMatchingDate()
    {
        return _matchingDate;
    }

    /**
     * @param matchingDate Дата согласования. Свойство не может быть null.
     */
    public void setMatchingDate(Date matchingDate)
    {
        dirty(_matchingDate, matchingDate);
        _matchingDate = matchingDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StuffCompensationStuExtractGen)
        {
            setProtocolDate(((StuffCompensationStuExtract)another).getProtocolDate());
            setProtocolNumber(((StuffCompensationStuExtract)another).getProtocolNumber());
            setCompensationSum(((StuffCompensationStuExtract)another).getCompensationSum());
            setImmediateSum(((StuffCompensationStuExtract)another).getImmediateSum());
            setMatchingPersonFio(((StuffCompensationStuExtract)another).getMatchingPersonFio());
            setResponsiblePersonFio(((StuffCompensationStuExtract)another).getResponsiblePersonFio());
            setMatchingPerson(((StuffCompensationStuExtract)another).getMatchingPerson());
            setResponsiblePerson(((StuffCompensationStuExtract)another).getResponsiblePerson());
            setMatchingDate(((StuffCompensationStuExtract)another).getMatchingDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StuffCompensationStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StuffCompensationStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new StuffCompensationStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    return obj.getProtocolDate();
                case "protocolNumber":
                    return obj.getProtocolNumber();
                case "compensationSum":
                    return obj.getCompensationSum();
                case "immediateSum":
                    return obj.getImmediateSum();
                case "matchingPersonFio":
                    return obj.getMatchingPersonFio();
                case "responsiblePersonFio":
                    return obj.getResponsiblePersonFio();
                case "matchingPerson":
                    return obj.getMatchingPerson();
                case "responsiblePerson":
                    return obj.getResponsiblePerson();
                case "matchingDate":
                    return obj.getMatchingDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    obj.setProtocolDate((Date) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
                case "compensationSum":
                    obj.setCompensationSum((Long) value);
                    return;
                case "immediateSum":
                    obj.setImmediateSum((Long) value);
                    return;
                case "matchingPersonFio":
                    obj.setMatchingPersonFio((String) value);
                    return;
                case "responsiblePersonFio":
                    obj.setResponsiblePersonFio((String) value);
                    return;
                case "matchingPerson":
                    obj.setMatchingPerson((EmployeePost) value);
                    return;
                case "responsiblePerson":
                    obj.setResponsiblePerson((EmployeePost) value);
                    return;
                case "matchingDate":
                    obj.setMatchingDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                        return true;
                case "protocolNumber":
                        return true;
                case "compensationSum":
                        return true;
                case "immediateSum":
                        return true;
                case "matchingPersonFio":
                        return true;
                case "responsiblePersonFio":
                        return true;
                case "matchingPerson":
                        return true;
                case "responsiblePerson":
                        return true;
                case "matchingDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    return true;
                case "protocolNumber":
                    return true;
                case "compensationSum":
                    return true;
                case "immediateSum":
                    return true;
                case "matchingPersonFio":
                    return true;
                case "responsiblePersonFio":
                    return true;
                case "matchingPerson":
                    return true;
                case "responsiblePerson":
                    return true;
                case "matchingDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    return Date.class;
                case "protocolNumber":
                    return String.class;
                case "compensationSum":
                    return Long.class;
                case "immediateSum":
                    return Long.class;
                case "matchingPersonFio":
                    return String.class;
                case "responsiblePersonFio":
                    return String.class;
                case "matchingPerson":
                    return EmployeePost.class;
                case "responsiblePerson":
                    return EmployeePost.class;
                case "matchingDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StuffCompensationStuExtract> _dslPath = new Path<StuffCompensationStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StuffCompensationStuExtract");
    }
            

    /**
     * @return Дата протокола. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getProtocolDate()
     */
    public static PropertyPath<Date> protocolDate()
    {
        return _dslPath.protocolDate();
    }

    /**
     * @return Номер протокола. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    /**
     * @return Сумма компенсации (коп.). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getCompensationSum()
     */
    public static PropertyPath<Long> compensationSum()
    {
        return _dslPath.compensationSum();
    }

    /**
     * @return Единовременная выплата (коп.). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getImmediateSum()
     */
    public static PropertyPath<Long> immediateSum()
    {
        return _dslPath.immediateSum();
    }

    /**
     * @return Согласующее лицо (печать). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getMatchingPersonFio()
     */
    public static PropertyPath<String> matchingPersonFio()
    {
        return _dslPath.matchingPersonFio();
    }

    /**
     * @return Ответственное лицо (печать). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getResponsiblePersonFio()
     */
    public static PropertyPath<String> responsiblePersonFio()
    {
        return _dslPath.responsiblePersonFio();
    }

    /**
     * @return Согласующее лицо. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getMatchingPerson()
     */
    public static EmployeePost.Path<EmployeePost> matchingPerson()
    {
        return _dslPath.matchingPerson();
    }

    /**
     * @return Ответственное лицо. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getResponsiblePerson()
     */
    public static EmployeePost.Path<EmployeePost> responsiblePerson()
    {
        return _dslPath.responsiblePerson();
    }

    /**
     * @return Дата согласования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getMatchingDate()
     */
    public static PropertyPath<Date> matchingDate()
    {
        return _dslPath.matchingDate();
    }

    public static class Path<E extends StuffCompensationStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _protocolDate;
        private PropertyPath<String> _protocolNumber;
        private PropertyPath<Long> _compensationSum;
        private PropertyPath<Long> _immediateSum;
        private PropertyPath<String> _matchingPersonFio;
        private PropertyPath<String> _responsiblePersonFio;
        private EmployeePost.Path<EmployeePost> _matchingPerson;
        private EmployeePost.Path<EmployeePost> _responsiblePerson;
        private PropertyPath<Date> _matchingDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата протокола. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getProtocolDate()
     */
        public PropertyPath<Date> protocolDate()
        {
            if(_protocolDate == null )
                _protocolDate = new PropertyPath<Date>(StuffCompensationStuExtractGen.P_PROTOCOL_DATE, this);
            return _protocolDate;
        }

    /**
     * @return Номер протокола. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(StuffCompensationStuExtractGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

    /**
     * @return Сумма компенсации (коп.). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getCompensationSum()
     */
        public PropertyPath<Long> compensationSum()
        {
            if(_compensationSum == null )
                _compensationSum = new PropertyPath<Long>(StuffCompensationStuExtractGen.P_COMPENSATION_SUM, this);
            return _compensationSum;
        }

    /**
     * @return Единовременная выплата (коп.). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getImmediateSum()
     */
        public PropertyPath<Long> immediateSum()
        {
            if(_immediateSum == null )
                _immediateSum = new PropertyPath<Long>(StuffCompensationStuExtractGen.P_IMMEDIATE_SUM, this);
            return _immediateSum;
        }

    /**
     * @return Согласующее лицо (печать). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getMatchingPersonFio()
     */
        public PropertyPath<String> matchingPersonFio()
        {
            if(_matchingPersonFio == null )
                _matchingPersonFio = new PropertyPath<String>(StuffCompensationStuExtractGen.P_MATCHING_PERSON_FIO, this);
            return _matchingPersonFio;
        }

    /**
     * @return Ответственное лицо (печать). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getResponsiblePersonFio()
     */
        public PropertyPath<String> responsiblePersonFio()
        {
            if(_responsiblePersonFio == null )
                _responsiblePersonFio = new PropertyPath<String>(StuffCompensationStuExtractGen.P_RESPONSIBLE_PERSON_FIO, this);
            return _responsiblePersonFio;
        }

    /**
     * @return Согласующее лицо. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getMatchingPerson()
     */
        public EmployeePost.Path<EmployeePost> matchingPerson()
        {
            if(_matchingPerson == null )
                _matchingPerson = new EmployeePost.Path<EmployeePost>(L_MATCHING_PERSON, this);
            return _matchingPerson;
        }

    /**
     * @return Ответственное лицо. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getResponsiblePerson()
     */
        public EmployeePost.Path<EmployeePost> responsiblePerson()
        {
            if(_responsiblePerson == null )
                _responsiblePerson = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_PERSON, this);
            return _responsiblePerson;
        }

    /**
     * @return Дата согласования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StuffCompensationStuExtract#getMatchingDate()
     */
        public PropertyPath<Date> matchingDate()
        {
            if(_matchingDate == null )
                _matchingDate = new PropertyPath<Date>(StuffCompensationStuExtractGen.P_MATCHING_DATE, this);
            return _matchingDate;
        }

        public Class getEntityClass()
        {
            return StuffCompensationStuExtract.class;
        }

        public String getEntityName()
        {
            return "stuffCompensationStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
