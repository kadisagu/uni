/* $Id: PersonHomeExtUI.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.ext.Person.ui.Home;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unifefu.base.bo.FefuUserManual.ui.List.FefuUserManualList;

/**
 * @author Rostuncev
 * @since 17.11.2011
 */
public class PersonHomeExtUI extends UIAddon
{

    public PersonHomeExtUI(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }



    public void onClickShowInstuct()
    {
        getActivationBuilder()
                .asDesktopRoot(FefuUserManualList.class)
                .activate();
    }


}
