package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x9x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuTrJournalExt

		// создана новая сущность
		if(!tool.tableExists("fefutrjournalext_t")){
			// создать таблицу
			DBTable dbt = new DBTable("fefutrjournalext_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_fefutrjournalext"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("trjournal_id", DBType.LONG).setNullable(false), 
				new DBColumn("gradescale_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuTrJournalExt");

		}


    }
}