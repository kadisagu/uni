/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu23.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 22.01.2015
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuTransfStuDPOExtract, Model>
{
}