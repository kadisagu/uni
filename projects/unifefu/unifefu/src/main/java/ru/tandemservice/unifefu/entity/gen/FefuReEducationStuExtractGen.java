package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unifefu.entity.FefuReEducationStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О предоставлении повторного года обучения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuReEducationStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuReEducationStuExtract";
    public static final String ENTITY_NAME = "fefuReEducationStuExtract";
    public static final int VERSION_HASH = -1772921772;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_ANNUL_ATTESTATION_RESULTS = "annulAttestationResults";
    public static final String L_PREV_COURSE = "prevCourse";
    public static final String L_EDU_YEAR = "eduYear";

    private Date _beginDate;     // Дата начала повторного обучения
    private boolean _annulAttestationResults;     // Аннулировать аттестации по прослушанным за прошедший курс дисциплинам
    private Course _prevCourse;     // Курс, за который требуется аннулировать аттестации по дисциплинам
    private EducationYear _eduYear;     // Учебный год, за который требуется аннулировать аттестации по дисциплинам

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала повторного обучения. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала повторного обучения. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Аннулировать аттестации по прослушанным за прошедший курс дисциплинам. Свойство не может быть null.
     */
    @NotNull
    public boolean isAnnulAttestationResults()
    {
        return _annulAttestationResults;
    }

    /**
     * @param annulAttestationResults Аннулировать аттестации по прослушанным за прошедший курс дисциплинам. Свойство не может быть null.
     */
    public void setAnnulAttestationResults(boolean annulAttestationResults)
    {
        dirty(_annulAttestationResults, annulAttestationResults);
        _annulAttestationResults = annulAttestationResults;
    }

    /**
     * @return Курс, за который требуется аннулировать аттестации по дисциплинам.
     */
    public Course getPrevCourse()
    {
        return _prevCourse;
    }

    /**
     * @param prevCourse Курс, за который требуется аннулировать аттестации по дисциплинам.
     */
    public void setPrevCourse(Course prevCourse)
    {
        dirty(_prevCourse, prevCourse);
        _prevCourse = prevCourse;
    }

    /**
     * @return Учебный год, за который требуется аннулировать аттестации по дисциплинам.
     */
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год, за который требуется аннулировать аттестации по дисциплинам.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuReEducationStuExtractGen)
        {
            setBeginDate(((FefuReEducationStuExtract)another).getBeginDate());
            setAnnulAttestationResults(((FefuReEducationStuExtract)another).isAnnulAttestationResults());
            setPrevCourse(((FefuReEducationStuExtract)another).getPrevCourse());
            setEduYear(((FefuReEducationStuExtract)another).getEduYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuReEducationStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuReEducationStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuReEducationStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return obj.getBeginDate();
                case "annulAttestationResults":
                    return obj.isAnnulAttestationResults();
                case "prevCourse":
                    return obj.getPrevCourse();
                case "eduYear":
                    return obj.getEduYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "annulAttestationResults":
                    obj.setAnnulAttestationResults((Boolean) value);
                    return;
                case "prevCourse":
                    obj.setPrevCourse((Course) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                        return true;
                case "annulAttestationResults":
                        return true;
                case "prevCourse":
                        return true;
                case "eduYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return true;
                case "annulAttestationResults":
                    return true;
                case "prevCourse":
                    return true;
                case "eduYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return Date.class;
                case "annulAttestationResults":
                    return Boolean.class;
                case "prevCourse":
                    return Course.class;
                case "eduYear":
                    return EducationYear.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuReEducationStuExtract> _dslPath = new Path<FefuReEducationStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuReEducationStuExtract");
    }
            

    /**
     * @return Дата начала повторного обучения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuReEducationStuExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Аннулировать аттестации по прослушанным за прошедший курс дисциплинам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuReEducationStuExtract#isAnnulAttestationResults()
     */
    public static PropertyPath<Boolean> annulAttestationResults()
    {
        return _dslPath.annulAttestationResults();
    }

    /**
     * @return Курс, за который требуется аннулировать аттестации по дисциплинам.
     * @see ru.tandemservice.unifefu.entity.FefuReEducationStuExtract#getPrevCourse()
     */
    public static Course.Path<Course> prevCourse()
    {
        return _dslPath.prevCourse();
    }

    /**
     * @return Учебный год, за который требуется аннулировать аттестации по дисциплинам.
     * @see ru.tandemservice.unifefu.entity.FefuReEducationStuExtract#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    public static class Path<E extends FefuReEducationStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Boolean> _annulAttestationResults;
        private Course.Path<Course> _prevCourse;
        private EducationYear.Path<EducationYear> _eduYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала повторного обучения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuReEducationStuExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(FefuReEducationStuExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Аннулировать аттестации по прослушанным за прошедший курс дисциплинам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuReEducationStuExtract#isAnnulAttestationResults()
     */
        public PropertyPath<Boolean> annulAttestationResults()
        {
            if(_annulAttestationResults == null )
                _annulAttestationResults = new PropertyPath<Boolean>(FefuReEducationStuExtractGen.P_ANNUL_ATTESTATION_RESULTS, this);
            return _annulAttestationResults;
        }

    /**
     * @return Курс, за который требуется аннулировать аттестации по дисциплинам.
     * @see ru.tandemservice.unifefu.entity.FefuReEducationStuExtract#getPrevCourse()
     */
        public Course.Path<Course> prevCourse()
        {
            if(_prevCourse == null )
                _prevCourse = new Course.Path<Course>(L_PREV_COURSE, this);
            return _prevCourse;
        }

    /**
     * @return Учебный год, за который требуется аннулировать аттестации по дисциплинам.
     * @see ru.tandemservice.unifefu.entity.FefuReEducationStuExtract#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

        public Class getEntityClass()
        {
            return FefuReEducationStuExtract.class;
        }

        public String getEntityName()
        {
            return "fefuReEducationStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
