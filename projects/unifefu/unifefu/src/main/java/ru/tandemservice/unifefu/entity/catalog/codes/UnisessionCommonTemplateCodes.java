package ru.tandemservice.unifefu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Печатные шаблоны общих документов «Сессия»"
 * Имя сущности : unisessionCommonTemplate
 * Файл data.xml : unifefu-templates.data.xml
 */
public interface UnisessionCommonTemplateCodes
{
    /** Константа кода (code) элемента : Шаблон экзаменационного листа (title) */
    String SHEET = "sheet";
    /** Константа кода (code) элемента : Шаблон экзаменационного листа (с выводом рейт. экв. оценки) (title) */
    String SHEET_WITH_POINTS = "sheetWithPoints";
    /** Константа кода (code) элемента : Шаблон экзаменационного листа (с выводом тек. рейтинга) (title) */
    String SHEET_WITH_CURRENT_RATING = "sheetWithCurrentRating";
    /** Константа кода (code) элемента : Шаблон таблицы для массовой печати экзаменационного листа (title) */
    String SHEET_MASS_PRINT_FRAME = "sheetMassPrintFrame";
    /** Константа кода (code) элемента : Шаблон экзаменационной карточки (title) */
    String EXAM_CARD = "examCard";
    /** Константа кода (code) элемента : Шаблон ведомости пересдач (title) */
    String RETAKE_DOC = "retakeDoc";
    /** Константа кода (code) элемента : Шаблон ведомости пересдач (с выводом рейт. экв. оценки) (title) */
    String RETAKE_DOC_WITH_POINTS = "retakeDocWithPoints";
    /** Константа кода (code) элемента : Шаблон ведомости пересдач (с выводом тек. рейтинга) (title) */
    String RETAKE_DOC_WITH_CURRENT_RATING = "retakeDocWithCurrentRating";
    /** Константа кода (code) элемента : Шаблон отчета «Сводная ведомость (на академ. группу)» (title) */
    String SUMMARY_BULLETIN = "summaryBulletin";
    /** Константа кода (code) элемента : Шаблон отчета «Должники» (title) */
    String DEBTORS_REPORT = "debtorsReport";
    /** Константа кода (code) элемента : Шаблон отчета «Сведения об успеваемости студентов (по курсам, по группам, по направлениям)» (title) */
    String RESULTS_REPORT = "resultsReport";
    /** Константа кода (code) элемента : Шаблон отчета «Сведения об успеваемости студентов (по дисциплинам)» (title) */
    String RESULTS_BY_DISC_REPORT = "resultsByDiscReport";
    /** Константа кода (code) элемента : Шаблон отчета «Сводные данные по ведомостям (на академ. группу)» (title) */
    String GROUP_BULLETIN_LIST_REPORT = "groupBulletinListReport";
    /** Константа кода (code) элемента : Шаблон аттестационной ведомости (title) */
    String ATTESTATION_BULLETIN = "attestationBulletin";
    /** Константа кода (code) элемента : Шаблон вспомогательной таблицы для выставления сессионной оценки по балльно-рейтинговой системе (title) */
    String BRS_SESSION_SCALE_BULLETIN = "brsSessionScaleSheet";
    /** Константа кода (code) элемента : Шаблон отчета «Сводная ведомость по межсессионной аттестации (на академ. группу)» (title) */
    String ATTESTATION_RESULT_REPORT = "attestationResultReport";
    /** Константа кода (code) элемента : Шаблон отчета «Итоги межсессионной аттестации студентов» (title) */
    String ATTESTATION_TOTAL_RESULT_REPORT = "attestationTotalResultReport";
    /** Константа кода (code) элемента : Шаблон протокола перезачтения и переаттестации (title) */
    String SESSION_TRANSFER_PROTOCOL = "sessionTransferProtocol";
    /** Константа кода (code) элемента : Шаблон заявления о переводе на ускоренное обучение (title) */
    String SESSION_AL_REQUEST = "sessionALRequest";
    /** Константа кода (code) элемента : Шаблон протокола государственного экзамена (title) */
    String SESSION_STATE_EXAM_PROTOCOL = "sessionStateExamProtocol";
    /** Константа кода (code) элемента : Шаблон протокола по защите ВКР (title) */
    String SESSION_FINAL_QUALIFYING_WORK_PROTOCOL = "sessionFQWProtocol";
    /** Константа кода (code) элемента : Шаблон отчета 'Рейтинг академической группы' (title) */
    String FEFU_RATING_GROUP = "fefuRatingGroup";
    /** Константа кода (code) элемента : Шаблон отчета 'Рейтинговая ведомость на экзамен' (title) */
    String FEFU_RATING_SHEET_EXAM = "fefuRatingSheetExam";
    /** Константа кода (code) элемента : Шаблон отчета 'Итоговая рейтинговая ведомость (экзамен)' (title) */
    String FEFU_RATING_SHEET_EXAM_TOTAL = "fefuRatingSheetTotal";
    /** Константа кода (code) элемента : Шаблон отчета 'Рейтинговая ведомость на зачет' (title) */
    String FEFU_RATING_SHEET_SETOFF = "fefuRatingSheetSetoff";
    /** Константа кода (code) элемента : Шаблон отчета 'Итоговая рейтинговая ведомость (зачет)' (title) */
    String FEFU_RATING_SHEET_SETOFF_TOTAL = "fefuRatingSheetSetoffTotal";
    /** Константа кода (code) элемента : Шаблон аттестационного листа (title) */
    String FEFU_ATT_SHEET = "fefuAttSheet";

    Set<String> CODES = ImmutableSet.of(SHEET, SHEET_WITH_POINTS, SHEET_WITH_CURRENT_RATING, SHEET_MASS_PRINT_FRAME, EXAM_CARD, RETAKE_DOC, RETAKE_DOC_WITH_POINTS, RETAKE_DOC_WITH_CURRENT_RATING, SUMMARY_BULLETIN, DEBTORS_REPORT, RESULTS_REPORT, RESULTS_BY_DISC_REPORT, GROUP_BULLETIN_LIST_REPORT, ATTESTATION_BULLETIN, BRS_SESSION_SCALE_BULLETIN, ATTESTATION_RESULT_REPORT, ATTESTATION_TOTAL_RESULT_REPORT, SESSION_TRANSFER_PROTOCOL, SESSION_AL_REQUEST, SESSION_STATE_EXAM_PROTOCOL, SESSION_FINAL_QUALIFYING_WORK_PROTOCOL, FEFU_RATING_GROUP, FEFU_RATING_SHEET_EXAM, FEFU_RATING_SHEET_EXAM_TOTAL, FEFU_RATING_SHEET_SETOFF, FEFU_RATING_SHEET_SETOFF_TOTAL, FEFU_ATT_SHEET);
}
