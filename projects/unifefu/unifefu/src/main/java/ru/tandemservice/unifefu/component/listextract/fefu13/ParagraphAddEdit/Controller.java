package ru.tandemservice.unifefu.component.listextract.fefu13.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative.AbstractParagraphAddEditAlternativeController;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author ilunin
 * @since 22.10.2014
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractParagraphAddEditAlternativeController<FefuAdditionalAcademGrantStuListExtract, IDAO, Model>
{
	@Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
	    ActionColumn clone = new ActionColumn("Заполнить по образцу", "clone", "onClickCopyGrantSize");
	    clone.setValidate(true);
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false).setOrderable(false));
	    dataSource.addColumn(new SimpleColumn("Группа", Student.group().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
	    dataSource.addColumn(new SimpleColumn("Форма освоения", Student.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("grantSize","Размер дополнительной стипендии").setClickable(false).setOrderable(false));
        dataSource.addColumn(clone);
	    dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteStudent", "Исключить из параграфа студента «{0}»?", Student.FIO_KEY));
        dataSource.setOrder("status", OrderDirection.asc);

        Model model = getModel(component);
        if (model.isEditForm())
        {
            IValueMapHolder grantSizeHolder = (IValueMapHolder) dataSource.getColumn("grantSize");
            Map<Long, Double> grantSizeMap = (null == grantSizeHolder ? Collections.emptyMap() : grantSizeHolder.getValueMap());

            for (FefuAdditionalAcademGrantStuListExtract grantExtract: (List<FefuAdditionalAcademGrantStuListExtract>) model.getParagraph().getExtractList())
            {
                grantSizeMap.put(grantExtract.getEntity().getId(), grantExtract.getAdditionalAcademGrantSize());
            }
        }
    }

    public void onClickCopyGrantSize(IBusinessComponent component)
    {
        Model model = getModel(component);

        final IValueMapHolder grantSizeHolder = (IValueMapHolder) model.getDataSource().getColumn("grantSize");
        final Map<Long, Double> grantSizeMap = (null == grantSizeHolder ? Collections.emptyMap() : grantSizeHolder.getValueMap());

        Number currentGrantSize = grantSizeMap.get((Long) component.getListenerParameter());

        DynamicListDataSource dataSource = model.getDataSource();

        for (ViewWrapper<Student> wrapper: (List<ViewWrapper<Student>>) dataSource.getEntityList())
        {
            if (grantSizeMap.get(wrapper.getEntity().getId()) == null)
            {
                grantSizeMap.put(wrapper.getEntity().getId(), currentGrantSize == null ? 0D : currentGrantSize.doubleValue());
            }
        }

        model.getDataSource().refresh();
    }
}
