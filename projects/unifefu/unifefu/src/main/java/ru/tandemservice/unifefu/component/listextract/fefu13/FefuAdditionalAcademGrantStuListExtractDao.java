package ru.tandemservice.unifefu.component.listextract.fefu13;

import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public class FefuAdditionalAcademGrantStuListExtractDao extends UniBaseDao implements IExtractComponentDao<FefuAdditionalAcademGrantStuListExtract>
{
    @Override
    public void doCommit(FefuAdditionalAcademGrantStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);
        //выплата
        StudentListOrder listOrder = (StudentListOrder) extract.getParagraph().getOrder();

        FefuStudentPayment payment = new FefuStudentPayment();
        payment.setExtract(extract);

        payment.setStartDate(extract.getDateStart());
        payment.setStopDate(extract.getDateEnd());
        payment.setPaymentSum(extract.getAdditionalAcademGrantSize());
        if(null != listOrder.getReason())
            payment.setReason(listOrder.getReason().getTitle());
        save(payment);
    }

    @Override
    public void doRollback(FefuAdditionalAcademGrantStuListExtract extract, Map parameters)
    {
        // Удаляем выплату
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuStudentPayment.class);
        deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(FefuStudentPayment.extract()), DQLExpressions.value(extract)));
        deleteBuilder.createStatement(getSession()).execute();
    }
}