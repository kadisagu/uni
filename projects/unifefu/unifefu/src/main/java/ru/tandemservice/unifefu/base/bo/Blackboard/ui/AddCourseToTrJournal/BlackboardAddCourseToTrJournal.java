/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.AddCourseToTrJournal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.BBCourseDSHandler;

/**
 * @author Nikolay Fedorovskih
 * @since 20.03.2014
 */
@Configuration
public class BlackboardAddCourseToTrJournal extends BusinessComponentManager
{
    public static final String COURSE_DS = "courseDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> courseDSHandler()
    {
        return new BBCourseDSHandler(getName(), true);
    }
}