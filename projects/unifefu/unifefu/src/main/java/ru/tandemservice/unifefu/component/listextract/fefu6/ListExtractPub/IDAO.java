/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuGiveDiplomaWithDismissStuListExtract, Model>
{
}
