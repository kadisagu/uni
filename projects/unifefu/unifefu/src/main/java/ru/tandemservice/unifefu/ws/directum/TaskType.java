/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

/**
 * @author Dmitry Seleznev
 * @since 16.07.2013
 */

public class TaskType extends ObjectType
{
    public static final String TASK_TYPE = "Task";
    public static final String DEFAULT_ROUTE_CODE = "ПУД_ДВФУ3";

    public TaskType()
    {
        super.setType(TASK_TYPE);
    }

    public TaskType(String routeCode, WorkflowParamsType workflowParams, AttachmentsType attachments)
    {
        this();
        super.setRouteCode(null != routeCode ? routeCode : DEFAULT_ROUTE_CODE);
        if (null != workflowParams) super.getChilds().add(workflowParams);
        if (null != attachments) super.getChilds().add(attachments);
    }
}