/* $Id$ */
package ru.tandemservice.unifefu.dao.mdbio;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.uniepp.entity.catalog.codes.EppSkillGroupCodes;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 29.04.2015
 */
public class FefuImtsaCompetenceIODao extends BaseIODao implements IFefuImtsaCompetenceIODao
{

    private static final String COMPETENCE_TABLE = "госКомпетенции";
    private static final String COMPETENCE_CODE_COLUMN = "Код";
    private static final String COMPETENCE_TITLE_COLUMN = "Описание";

    @Override
    public String doImportImtsaCompetence(Database mdb) throws IOException
    {
        Table competence_t = mdb.getTable(COMPETENCE_TABLE);
        if (competence_t == null)
            throw new ApplicationException("Таблица " + COMPETENCE_TABLE + " отсутствует в базе данных.");

        Map<String, Map<String, EppSkillGroup>> competenceForImport = getCompetenceForImport(competence_t);
        List<FefuCompetence> existingCompetenceList = getList(FefuCompetence.class);
        int skipCompetence = 0;
        int importedCompetence = 0;
        for (Map<String, EppSkillGroup> competenceDataMap : competenceForImport.values())
        {
            String title = (String) competenceDataMap.keySet().toArray()[0];
            EppSkillGroup skillGroup = competenceDataMap.get(title);
            String code = DefaultCatalogAddEditDAO.getNewCatalogItemCode(FefuCompetence.class);
            FefuCompetence competence = new FefuCompetence(skillGroup, code, title);

            if (isCompetenceExist(existingCompetenceList, competence))
            {
                skipCompetence++;
            }
            else
            {
                importedCompetence++;
                save(competence);
            }
        }
        return String.valueOf("Импортировано " + importedCompetence + " элементов, пропущено " + skipCompetence + " элементов");
    }


    /**
     * Проходит по строкам таблицы в mdb
     * и ищет уникальные сочетания группа компетенций - названия компетенции
     *
     * @return Возвращает уникальные сочетания Группы компетенций и названия компетенции.
     */
    private Map<String, Map<String, EppSkillGroup>> getCompetenceForImport(Table competence_t)
    {
        final Map<String, Map<String, EppSkillGroup>> competenceForImport = new HashMap<>();
        final Map<String, EppSkillGroup> skillGroupByShortTitleMap = getSkillGroupByShortTitleMap();

        execute(competence_t, 500, "Импорт компетенций", rows -> {
            for (final Map<String, Object> row : rows)
            {
                String shortTitleWithTail = row.get(COMPETENCE_CODE_COLUMN)!=null?((String) row.get(COMPETENCE_CODE_COLUMN)).trim():"";
                String title = row.get(COMPETENCE_TITLE_COLUMN)!=null?((String) row.get(COMPETENCE_TITLE_COLUMN)).trim():"";
                String shortTitle = getCodeWithoutTail(shortTitleWithTail);
                String uniqueKey = (shortTitle + title).replace(" ", "").toLowerCase();
                if (!skillGroupByShortTitleMap.containsKey(shortTitle)
                        || StringUtils.isEmpty(title)
                        || competenceForImport.containsKey(uniqueKey)) continue;

                Map<String, EppSkillGroup> rowForImport = new HashMap<>(1);
                rowForImport.put(title.trim(), skillGroupByShortTitleMap.get(shortTitle));
                competenceForImport.put(uniqueKey, rowForImport);
            }
        });

        return competenceForImport;

    }

    private boolean isCompetenceExist(List<FefuCompetence> existingCompetenceList, FefuCompetence competenceForCheck)
    {

        for (FefuCompetence competence : existingCompetenceList)
        {
            if (competence.getEppSkillGroup().getCode().equals(competenceForCheck.getEppSkillGroup().getCode())
                    && competence.getTitle().trim().toLowerCase().equals(competenceForCheck.getTitle().trim().toLowerCase()))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Обрезает хвост у кода, хранящегося в mdb
     *
     * @param shortTitleWithTail shortTitle с хвостом, пример: ОПК-3
     * @return код без хвоста, пример: ОПК
     */
    private String getCodeWithoutTail(String shortTitleWithTail)
    {
        String shortTitle = "";
        String[] shortTitleParts = shortTitleWithTail.split("-");
        if (shortTitleParts.length > 0)
        {
            shortTitle = shortTitleParts[0].trim();
        }
        return shortTitle;
    }

    private Map<String, EppSkillGroup> getSkillGroupByShortTitleMap()
    {
        Map<String, EppSkillGroup> skillGroupByCodeMap = new HashMap<>();
        List<String> skillGroupCodes = new ArrayList<>();
        skillGroupCodes.add(EppSkillGroupCodes.INSTRUMENTALNYE_KOMPETENTSII);
        skillGroupCodes.add(EppSkillGroupCodes.OBTSHEKULTURNYE_KOMPETENTSII);
        skillGroupCodes.add(EppSkillGroupCodes.PROFESSIONALNYE_KOMPETENTSII);
        skillGroupCodes.add(EppSkillGroupCodes.PROFESSIONALNO_SPETSIALIZIROVANNYE_KOMPETENTSII);
        skillGroupCodes.add(EppSkillGroupCodes.OBTSHENAUCHNYE_KOMPETENTSII);
        skillGroupCodes.add(EppSkillGroupCodes.OBTSHEPROFESSIONALNYE_KOMPETENTSII);

        List<EppSkillGroup> skillGroupList = getList(EppSkillGroup.class, EppSkillGroup.P_CODE, skillGroupCodes);

        for (EppSkillGroup skillGroup : skillGroupList)
        {
            skillGroupByCodeMap.put(skillGroup.getShortTitle(), skillGroup);
        }
        return skillGroupByCodeMap;
    }


}

