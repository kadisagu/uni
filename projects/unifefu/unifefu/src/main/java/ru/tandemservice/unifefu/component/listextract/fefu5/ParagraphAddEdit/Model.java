/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu5.ParagraphAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuCourseTransferStuListExtract;

import java.util.List;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class Model extends AbstractListParagraphAddEditModel<FefuCourseTransferStuListExtract>
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private StudentCustomStateCI _studentCustomStateCI;
    private Course _courseNew;
    private Group _groupNew;

    private List<CompensationType> _compensationTypeList;
    private ISelectModel _groupListModel;
    private ISelectModel _courseNewListModel;
    private ISelectModel _groupNewListModel;

    private boolean _courseAndGroupDisabled;

    public StudentCustomStateCI getStudentCustomStateCI()
    {
        return _studentCustomStateCI;
    }

    public void setStudentCustomStateCI(StudentCustomStateCI studentCustomStateCI)
    {
        _studentCustomStateCI = studentCustomStateCI;
    }


    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public Course getCourseNew()
    {
        return _courseNew;
    }

    public void setCourseNew(Course courseNew)
    {
        _courseNew = courseNew;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    public void setGroupNew(Group groupNew)
    {
        _groupNew = groupNew;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public ISelectModel getCourseNewListModel()
    {
        return _courseNewListModel;
    }

    public void setCourseNewListModel(ISelectModel courseNewListModel)
    {
        _courseNewListModel = courseNewListModel;
    }

    public ISelectModel getGroupNewListModel()
    {
        return _groupNewListModel;
    }

    public void setGroupNewListModel(ISelectModel groupNewListModel)
    {
        _groupNewListModel = groupNewListModel;
    }

    public boolean isCourseAndGroupDisabled()
    {
        return _courseAndGroupDisabled;
    }

    public void setCourseAndGroupDisabled(boolean courseAndGroupDisabled)
    {
        _courseAndGroupDisabled = courseAndGroupDisabled;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }
}
