/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.orgunit.EppEpvAcceptionTab;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEpvCheckState;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexander Zhebko
 * @since 09.12.2013
 */
public class DAO extends ru.tandemservice.uniepp.component.orgunit.EppEpvAcceptionTab.DAO implements IDAO
{
    @Override
    protected void customizeEpvRowIdsBuilder(DQLSelectBuilder epvRowIdsBuilder, String epvRowAlias, IDataSettings settings)
    {
        boolean anyCheck = false;
        DQLSelectBuilder versionStateBuilder = new DQLSelectBuilder()
                .fromEntity(FefuEpvCheckState.class, "cs")
                .column(property("cs", FefuEpvCheckState.version().id()));

        IEntity checkByUMU = settings.get("checkedByUMU");
        if (checkByUMU != null)
        {
            versionStateBuilder.where(eq(property("cs", FefuEpvCheckState.checkedByUMU()), value(Boolean.TRUE)));
            anyCheck = true;
        }

        IEntity checkByCRK = settings.get("checkedByCRK");
        if (checkByCRK != null)
        {
            versionStateBuilder.where(eq(property("cs", FefuEpvCheckState.checkedByCRK()), value(Boolean.TRUE)));
            anyCheck = true;
        }

        IEntity checkByOOP = settings.get("checkedByOOP");
        if (checkByOOP != null)
        {
            versionStateBuilder.where(eq(property("cs", FefuEpvCheckState.checkedByOOP()), value(Boolean.TRUE)));
            anyCheck = true;
        }

        Date checkedByUMUDateFrom = settings.get("checkedByUMUDateFrom");
        Date checkedByUMUDateTo = settings.get("checkedByUMUDateTo");
        versionStateBuilder.where(betweenDays(FefuEpvCheckState.checkedByUMUDate().fromAlias("cs"), checkedByUMUDateFrom, checkedByUMUDateTo));

        Date checkedByCRKDateFrom = settings.get("checkedByCRKDateFrom");
        Date checkedByCRKDateTo = settings.get("checkedByCRKDateTo");
        versionStateBuilder.where(betweenDays(FefuEpvCheckState.checkedByCRKDate().fromAlias("cs"), checkedByCRKDateFrom, checkedByCRKDateTo));

        Date checkedByOOPDateFrom = settings.get("checkedByOOPDateFrom");
        Date checkedByOOPDateTo = settings.get("checkedByOOPDateTo");
        versionStateBuilder.where(betweenDays(FefuEpvCheckState.checkedByOOPDate().fromAlias("cs"), checkedByOOPDateFrom, checkedByOOPDateTo));

        if (anyCheck)
            epvRowIdsBuilder.where(in(property(epvRowAlias, EppEpvRow.owner().eduPlanVersion().id()), versionStateBuilder.buildQuery()));
    }

    @Override
    public List<Long> getRegistryRowIds(Long orgUnitId, IDataSettings settings)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "r")
                .column(property("r", EppEpvRegistryRow.id()))
                .joinPath(DQLJoinType.inner, EppEpvRegistryRow.owner().eduPlanVersion().fromAlias("r"), "v")
                .where(eq(property("r", EppEpvRegistryRow.registryElementOwner().id()), value(orgUnitId)))
                .where(isNull(property("r", EppEpvRegistryRow.registryElement().id())));

        FilterUtils.applyLikeFilter(builder, (String) settings.get("title"), EppEpvRegistryRow.title().fromAlias("r"));

        EduProgramSubject programSubject = settings.get("programSubject");
        if (programSubject != null)
        {
            builder.fromEntity(EppEduPlanProf.class, "prof");
            builder.where(eq(property("v", EppEduPlanVersion.eduPlan()), property("prof")));
            builder.where(eq(property("prof", EppEduPlanProf.programSubject()), value(programSubject)));
        }

        FilterUtils.applySelectFilter(builder, EppEduPlanVersion.eduPlan().programForm().fromAlias("v"), settings.get("programForm"));
        FilterUtils.applySelectFilter(builder, EppEduPlanVersion.eduPlan().developCondition().fromAlias("v"), settings.get("developCond"));
        FilterUtils.applySelectFilter(builder, EppEduPlanVersion.eduPlan().programTrait().fromAlias("v"), settings.get("programTrait"));
        FilterUtils.applySelectFilter(builder, EppEduPlanVersion.eduPlan().developGrid().fromAlias("v"), settings.get("developGrid"));

        EppRegistryStructure registryStructure = (EppRegistryStructure) settings.get("registryStructure");
        EppState state = (EppState) settings.get("state");

        if (null != state)
        {
            builder.where(eq(property(EppEpvRegistryRow.owner().eduPlanVersion().state().fromAlias("r")), value(state)));
        }

        if (null != registryStructure)
        {
            builder.where(eq(property(EppEpvRegistryRow.registryElementType().fromAlias("r")), value(registryStructure)));
        }

        customizeEpvRowIdsBuilder(builder, "r", settings);

        {
            @SuppressWarnings("unchecked")
            final List<IdentifiableWrapper<IEntity>> errors = (List<IdentifiableWrapper<IEntity>>)settings.get("errors");
            if ((null != errors) && (errors.size() > 0)) {
                final Collection<Long> errorIds = new HashSet<>(ids(errors));
                IDQLExpression e = null;

                // нет элемента реестра
                if (errorIds.contains(ERROR_NO_REGEL)) {
                    e = or(e, isNull(property(EppEpvRegistryRow.registryElement().fromAlias("r"))));
                }

                // не совпадают подразделения
                if (errorIds.contains(ERROR_MISMATCH_ORG_UNIT)) {
                    e = or(e, ne(property(EppEpvRegistryRow.registryElementOwner().fromAlias("r")), property(EppRegistryElement.owner().fromAlias("rel"))));
                }

                // не совпадают типы элементов
                if (errorIds.contains(ERROR_MISMATCH_TYPE)) {
                    e = or(e, ne(property(EppEpvRegistryRow.registryElementType().fromAlias("r")), property(EppRegistryElement.parent().fromAlias("rel"))));
                }

                // не совпадают часы и трудоемкость
                if (errorIds.contains(ERROR_MISMATCH_TOTAL_LOAD)) {
                    final int threshold = 99;
                    e = or(e, DQL.parseExpression("abs(" + EppRegistryElement.size().fromAlias("rel") + "-" + EppEpvRegistryRow.hoursTotal().fromAlias("r") + ") > " + threshold));
                    e = or(e, DQL.parseExpression("abs("+EppRegistryElement.labor().fromAlias("rel")+"-"+EppEpvRegistryRow.totalLabor().fromAlias("r")+") > "+threshold));
                }

                if (null != e) {
                    builder.joinPath(DQLJoinType.left, EppEpvRegistryRow.registryElement().fromAlias("r"), "rel");
                    builder.where(e);
                }
            }
        }

        return builder.createStatement(getSession()).list();
    }

    private static final Long ERROR_NO_REGEL = 1L;
    private static final Long ERROR_MISMATCH_ORG_UNIT = 2L;
    private static final Long ERROR_MISMATCH_TYPE = 3L;
    private static final Long ERROR_MISMATCH_TOTAL_LOAD = 4L;
}
