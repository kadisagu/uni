/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu2.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubController;
import ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 15.03.2013
 */
public class Controller extends AbstractListExtractPubController<FefuAdmitToStateExamsStuListExtract, Model, IDAO>
{
}