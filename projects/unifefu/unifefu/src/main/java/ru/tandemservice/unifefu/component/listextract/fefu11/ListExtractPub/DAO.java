/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu11.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 19.11.2013
 */
public class DAO extends AbstractListExtractPubDAO<FefuCompensationTypeTransferStuListExtract, Model> implements IDAO
{
}
