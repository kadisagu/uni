package ru.tandemservice.unifefu.component.report.EntrantDailyRatingByED.EntrantDailyRatingByEDAdd;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uniec.component.report.EntrantDailyRatingByED.EntrantDailyRatingByEDAdd.Model;
/**
 * User: amakarova
 * Date: 22.05.13
 */
@Zlo
public class DAO extends ru.tandemservice.uniec.component.report.EntrantDailyRatingByED.EntrantDailyRatingByEDAdd.DAO {

    protected DatabaseFile generateContent(Model model, Session session)
    {
        return new EntrantDailyRatingByEDReportBuilder(model, session).getContent();
    }
}
