/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu13.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;

/**
 * @author nvankov
 * @since 6/20/13
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<SendPracticOutStuExtract, Model>
{
}
