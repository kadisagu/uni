package ru.tandemservice.unifefu.component.report.FefuStatisticEnrolled.Add;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.unifefu.entity.report.FefuStatisticEnrolledReport;

import java.util.Date;

/**
 * @author amakarova
 * @since 13.06.2013
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        model.setParameters(new MultiEnrollmentDirectionUtil.Parameters());
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));

        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setCompensationTypeList(getCatalogItemListOrderByCode(CompensationType.class));

        model.setFormativeOrgUnitListModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitListModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelHighSchoolListModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormListModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionListModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechListModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodListModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));
    }

    @Override
    public void update(Model model)
    {
        if (model.getReport().getDateFrom().after(model.getReport().getDateTo()))
            throw new ApplicationException("Дата в поле «Заявления с» должна быть не позже даты в поле «Заявления по».");

        final Session session = getSession();

        FefuStatisticEnrolledReport report = model.getReport();
        report.setFormingDate(new Date());

        if (model.isStudentCategoryActive())
            report.setStudentCategory(UniStringUtils.join(model.getStudentCategoryList(), "title", "; "));
        if (model.isQualificationActive())
            report.setQualification(UniStringUtils.join(model.getQualificationList(), "title", "; "));
        if (model.isCompensTypeActive())
            report.setCompensationType(model.getCompensationType().getTitle());
        if (model.isFormativeOrgUnitActive())
            report.setFormativeOrgUnit(UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.P_FULL_TITLE, "; "));
        if (model.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnit(UniStringUtils.join(model.getTerritorialOrgUnitList(), OrgUnit.P_TERRITORIAL_FULL_TITLE, "; "));
        if (model.isEducationLevelHighSchoolActive())
            report.setEducationLevelHighSchool(UniStringUtils.join(model.getEducationLevelHighSchoolList(), EducationLevelsHighSchool.P_DISPLAYABLE_TITLE, "; "));
        if (model.isDevelopFormActive())
            report.setDevelopForm(UniStringUtils.join(model.getDevelopFormList(), "title", "; "));
        if (model.isDevelopConditionActive())
            report.setDevelopCondition(UniStringUtils.join(model.getDevelopConditionList(), "title", "; "));
        if (model.isDevelopTechActive())
            report.setDevelopTech(UniStringUtils.join(model.getDevelopTechList(), "title", "; "));
        if (model.isDevelopPeriodActive())
            report.setDevelopPeriod(UniStringUtils.join(model.getDevelopPeriodList(), "title", "; "));

        report.setIncludeForeignPerson(model.isIncludeForeignPerson());
        report.setByCompetitionGroup(model.isByCompetitionGroup());

        DatabaseFile databaseFile = new FefuStatisticEnrolledReportBuilder(model, session).getContent();
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }
}