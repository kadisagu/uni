/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.PersonSync;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 03.07.2013
 */
public class NSISyncPersonSyncUI extends AbstractUniStudentListUI
{
    @Override
    public void onComponentRefresh()
    {
        setShowDevelopTechFilter(true);
        super.onComponentRefresh();
    }

    @Override
    public String getSettingsKey()
    {
        return "NSISyncPersonSyncList.filter";
    }

    public void onClickSendStudentsToNSI()
    {
        if (null == ApplicationRuntime.getProperty(FefuNsiSyncDAO.NSI_SERVICE_URL))
            throw new ApplicationException("Адрес сервиса НСИ не задан в конфигурационных файлах.");

        Collection<IEntity> selected = ((PageableSearchListDataSource) getConfig().getDataSource(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS)).getOptionColumnSelectedObjects("select");
        if (selected.isEmpty()) throw new ApplicationException("Необходимо выбрать хотя бы одного студента для отправки в НСИ.");

        List<Long> selectedStudIds = new ArrayList<>();
        for (IEntity wrapper : selected)
        {
            selectedStudIds.add(wrapper.getId());
        }

        IFefuNsiSyncDAO.instance.get().doInsertSinglePerson(selectedStudIds);
    }

    public void onClickSendAllStudentsToNSI()
    {
        if (null == ApplicationRuntime.getProperty(FefuNsiSyncDAO.NSI_SERVICE_URL))
            throw new ApplicationException("Адрес сервиса НСИ не задан в конфигурационных файлах.");

        IFefuNsiSyncDAO.instance.get().doInsertSinglePerson(null);
    }

    public void onClickSendOksmDataToNSI()
    {
        if (null == ApplicationRuntime.getProperty(FefuNsiSyncDAO.NSI_SERVICE_URL))
            throw new ApplicationException("Адрес сервиса НСИ не задан в конфигурационных файлах.");

        Collection<IEntity> selected = ((PageableSearchListDataSource) getConfig().getDataSource(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS)).getOptionColumnSelectedObjects("select");
        if (selected.isEmpty()) throw new ApplicationException("Необходимо выбрать хотя бы одного студента для отправки в НСИ.");

        List<Long> selectedStudIds = new ArrayList<>();
        for (IEntity wrapper : selected)
        {
            selectedStudIds.add(wrapper.getId());
        }

        IFefuNsiSyncDAO.instance.get().doUpdateSinglePersonCitizenship(selectedStudIds);
    }

    public void onClickSendAllOksmDataToNSI()
    {
        if (null == ApplicationRuntime.getProperty(FefuNsiSyncDAO.NSI_SERVICE_URL))
            throw new ApplicationException("Адрес сервиса НСИ не задан в конфигурационных файлах.");

        IFefuNsiSyncDAO.instance.get().doUpdateSinglePersonCitizenship(null);
    }
}