package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.OriginalsVerificationVar2Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report;


/**
 * User: amakarova
 * Date: 20.09.13
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
})
public class FefuEcReportOriginalsVerificationVar2PubUI extends UIPresenter
{
    // fields

    private Long _reportId;
    private FefuEntrantOriginalsVerificationVar2Report _report;

    // Presenter listeners

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(FefuEntrantOriginalsVerificationVar2Report.class, _reportId);

    }

    // Listeners

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", _reportId)
                .parameter("extension", "rtf")
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report);
        deactivate();
    }

    // Getters & Setters

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public FefuEntrantOriginalsVerificationVar2Report getReport()
    {
        return _report;
    }

    public void setReport(FefuEntrantOriginalsVerificationVar2Report report)
    {
        _report = report;
    }

    public boolean getNotAllEnrollmentDirections()
    {
        return !_report.isAllEnrollmentDirections();
    }
}
