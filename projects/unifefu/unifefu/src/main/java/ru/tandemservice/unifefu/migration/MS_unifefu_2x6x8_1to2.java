/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Alexey Lopatin
 * @since 22.10.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x8_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
                new ScriptDependency("org.tandemframework", "1.6.15"),
                new ScriptDependency("org.tandemframework.shared", "1.6.8")
        };
    }

    @Override
    public ScriptDependency[] getAfterDependencies()
    {
        return new ScriptDependency[] {new ScriptDependency("uniepp", "2.6.8", 1)};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.dropConstraint("sessionattsheetslot_t", "fk_student_sessionattsheetslot");
    }
}