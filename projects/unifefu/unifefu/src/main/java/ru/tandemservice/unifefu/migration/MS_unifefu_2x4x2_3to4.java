package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x2_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPerson

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewperson_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("person_id", DBType.LONG).setNullable(false), 
				new DBColumn("identitytype_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("identityseria_p", DBType.createVarchar(255)), 
				new DBColumn("identitynumber_p", DBType.createVarchar(255)), 
				new DBColumn("identityfirstname_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("identitylastname_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("identitymiddlename_p", DBType.createVarchar(255)), 
				new DBColumn("identitybirthdate_p", DBType.DATE), 
				new DBColumn("identitybirthplace_p", DBType.createVarchar(255)), 
				new DBColumn("identitysex_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("identitydate_p", DBType.DATE), 
				new DBColumn("identitycode_p", DBType.createVarchar(255)), 
				new DBColumn("identityplace_p", DBType.createVarchar(255)), 
				new DBColumn("identitycitizen_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("familystatus_p", DBType.createVarchar(255)), 
				new DBColumn("childcount_p", DBType.INTEGER), 
				new DBColumn("pensiontype_p", DBType.createVarchar(255)), 
				new DBColumn("pensionissuancedate_p", DBType.DATE), 
				new DBColumn("flatpresence_p", DBType.createVarchar(255)), 
				new DBColumn("servicelengthyears_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("servicelengthmonths_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("servicelengthdays_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("addressregcountry_p", DBType.createVarchar(255)), 
				new DBColumn("addressregsettlement_p", DBType.createVarchar(255)), 
				new DBColumn("addressregstreet_p", DBType.createVarchar(255)), 
				new DBColumn("addressreghousenumber_p", DBType.createVarchar(255)), 
				new DBColumn("addressreghouseunitnumber_p", DBType.createVarchar(255)), 
				new DBColumn("addressregflatnumber_p", DBType.createVarchar(255)), 
				new DBColumn("addressfactcountry_p", DBType.createVarchar(255)), 
				new DBColumn("addressfactsettlement_p", DBType.createVarchar(255)), 
				new DBColumn("addressfactstreet_p", DBType.createVarchar(255)), 
				new DBColumn("addressfacthousenumber_p", DBType.createVarchar(255)), 
				new DBColumn("addressfacthouseunitnumber_p", DBType.createVarchar(255)), 
				new DBColumn("addressfactflatnumber_p", DBType.createVarchar(255)), 
				new DBColumn("personeduinstitution_id", DBType.LONG), 
				new DBColumn("innnumber_p", DBType.createVarchar(255)), 
				new DBColumn("snilsnumber_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewPerson");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonAdditionalData

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewpersonadditionaldata_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("mdbviewperson_id", DBType.LONG).setNullable(false), 
				new DBColumn("vipdate_p", DBType.DATE), 
				new DBColumn("commentvip_p", DBType.createVarchar(255)), 
				new DBColumn("fiocuratorvip_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewPersonAdditionalData");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonBenefit

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewpersonbenefit_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("mdbviewperson_id", DBType.LONG).setNullable(false), 
				new DBColumn("benefit_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("basic_p", DBType.createVarchar(255)), 
				new DBColumn("date_p", DBType.DATE), 
				new DBColumn("document_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewPersonBenefit");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonEduinstitution

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewpersoneduinstitution_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("mdbviewperson_id", DBType.LONG).setNullable(false), 
				new DBColumn("eduinstitution_id", DBType.LONG), 
				new DBColumn("dctnlinstttncntry_p", DBType.createVarchar(255)), 
				new DBColumn("dctnlinstttnsttlmnt_p", DBType.createVarchar(255)), 
				new DBColumn("dctnlinstttntypknd_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("educationdocumenttype_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("educationlevelstage_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("employeespeciality_p", DBType.createVarchar(255)), 
				new DBColumn("diplomaqualification_p", DBType.createVarchar(255)), 
				new DBColumn("qualification_p", DBType.createVarchar(255)), 
				new DBColumn("date_p", DBType.DATE), 
				new DBColumn("seria_p", DBType.createVarchar(255)), 
				new DBColumn("number_p", DBType.createVarchar(255)), 
				new DBColumn("marks_p", DBType.createVarchar(255)), 
				new DBColumn("regioncode_p", DBType.createVarchar(255)), 
				new DBColumn("graduationhonour_p", DBType.createVarchar(255)), 
				new DBColumn("yearend_p", DBType.INTEGER).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewPersonEduinstitution");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonForeignlanguage

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewpersonforeignlanguage_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("mdbviewperson_id", DBType.LONG).setNullable(false), 
				new DBColumn("language_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("skill_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewPersonForeignlanguage");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonNextofkin

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewpersonnextofkin_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("mdbviewperson_id", DBType.LONG).setNullable(false), 
				new DBColumn("relationdegree_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("identityseria_p", DBType.createVarchar(255)), 
				new DBColumn("identitynumber_p", DBType.createVarchar(255)), 
				new DBColumn("identityfirstname_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("identitylastname_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("identitymiddlename_p", DBType.createVarchar(255)), 
				new DBColumn("identitybirthdate_p", DBType.DATE), 
				new DBColumn("identitydate_p", DBType.DATE), 
				new DBColumn("identitycode_p", DBType.createVarchar(255)), 
				new DBColumn("identityplace_p", DBType.createVarchar(255)), 
				new DBColumn("employeeplace_p", DBType.createVarchar(255)), 
				new DBColumn("phones_p", DBType.createVarchar(255)), 
				new DBColumn("addresscountry_p", DBType.createVarchar(255)), 
				new DBColumn("addresssettlement_p", DBType.createVarchar(255)), 
				new DBColumn("addressstreet_p", DBType.createVarchar(255)), 
				new DBColumn("addresshousenumber_p", DBType.createVarchar(255)), 
				new DBColumn("addresshouseunitnumber_p", DBType.createVarchar(255)), 
				new DBColumn("addressflatnumber_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewPersonNextofkin");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonSportachievement

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbvwprsnsprtchvmnt_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("mdbviewperson_id", DBType.LONG).setNullable(false), 
				new DBColumn("sporttype_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("sportrank_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("bestachievement_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewPersonSportachievement");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewStudent

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewstudent_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("mdbviewperson_id", DBType.LONG).setNullable(false), 
				new DBColumn("number_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("entrance_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("group_id", DBType.LONG), 
				new DBColumn("eduou_id", DBType.LONG).setNullable(false), 
				new DBColumn("course_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("compensation_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("category_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("booknumber_p", DBType.createVarchar(255)), 
				new DBColumn("status_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("archival_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("finishyear_p", DBType.INTEGER), 
				new DBColumn("personalfilenumber_p", DBType.createVarchar(255)), 
				new DBColumn("specialpurposerecruit_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewStudent");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewStudentAdditionalData

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewstudentadditionaldata_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("mdbviewstudent_id", DBType.LONG).setNullable(false), 
				new DBColumn("integrationid_p", DBType.createVarchar(255)), 
				new DBColumn("email_p", DBType.createVarchar(255)), 
				new DBColumn("phones_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewStudentAdditionalData");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewStudentCustomState

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewstudentcustomstate_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("mdbviewstudent_id", DBType.LONG).setNullable(false), 
				new DBColumn("customstate_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("begindate_p", DBType.DATE), 
				new DBColumn("enddate_p", DBType.DATE), 
				new DBColumn("description_p", DBType.TEXT)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewStudentCustomState");

		}


    }
}