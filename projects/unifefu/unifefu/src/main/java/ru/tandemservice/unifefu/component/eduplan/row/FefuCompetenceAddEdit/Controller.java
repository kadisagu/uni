/**
 * $Id$
 */
package ru.tandemservice.unifefu.component.eduplan.row.FefuCompetenceAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.unifefu.entity.FefuAbstractCompetence2EpvRegistryRowRel;
/**
 * @author Alexander Zhebko
 * @since 13.05.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getCompetencesDS() != null) return;

        DynamicListDataSource<FefuAbstractCompetence2EpvRegistryRowRel> dataSource = new DynamicListDataSource<>(component, this);

        dataSource.addColumn(new SimpleColumn("Код компетенции", "", o -> ((FefuAbstractCompetence2EpvRegistryRowRel) o).getGroupCode())
                                     .setWidth(8).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Название", "", o -> ((FefuAbstractCompetence2EpvRegistryRowRel) o).getFefuCompetence().getTitle())
                                     .setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteCompetence"));

        model.setCompetencesDS(dataSource);
    }


    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickDeleteCompetence(IBusinessComponent component)
    {
       getDao().deleteRowCompetence2EduStandartRel(getModel(component), component.getListenerParameter());
    }

    public void onClickUpdateCompetence(IBusinessComponent component)
    {
        getDao().updateCompetence(getModel(component));
    }

    public void onAddCompetence(IBusinessComponent component)
    {
        getDao().addRowCompetence2EduStandartRel(component.getModel());
    }

    public void onChangeDispersion(IBusinessComponent component)
    {
        getDao().changeDispersion(component.getModel());
    }
}