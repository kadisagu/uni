/* $Id$ */
package ru.tandemservice.unifefu.report.externalView;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Andrey Avetisov
 * @since 16.04.2014
 */
public class EppInfoExtViewProvider extends SimpleDQLExternalViewConfig
{
    @Override
    protected DQLSelectBuilder buildDqlQuery()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlan.class, "workPlan")
                .joinPath(DQLJoinType.inner, EppWorkPlan.parent().fromAlias("workPlan"), "planVerBlock")
                .joinEntity("planVerBlock", DQLJoinType.left, EppEduPlanVersionSpecializationBlock.class, "sb", eq(property("sb"), property("planVerBlock")))
                .joinPath(DQLJoinType.inner, EppEduPlanVersionBlock.eduPlanVersion().eduPlan().fromAlias("planVerBlock"), "eduPlan")
                .joinEntity("eduPlan", DQLJoinType.left, EppEduPlanHigherProf.class, "php", eq(property("php"), property("eduPlan")))
                .joinEntity("eduPlan", DQLJoinType.left, EppEduPlanSecondaryProf.class, "shp", eq(property("shp"), property("eduPlan")))
                .joinPath(DQLJoinType.left, EppEduPlanVersionSpecializationBlock.programSpecialization().fromAlias("sb"), "spec")
                .joinEntity("spec", DQLJoinType.left, EducationLevels.class, "l", eq(property("l", EducationLevels.eduProgramSpecialization()), property("spec")))
                .joinEntity("l", DQLJoinType.left, EducationLevelsHighSchool.class, "lhs", eq(property("lhs", EducationLevelsHighSchool.educationLevel()), property("l")))
                .where(eqNullSafe(property("lhs", EducationLevelsHighSchool.assignedQualification()), property("php", EppEduPlanHigherProf.programQualification())))
                .where(eqNullSafe(property("lhs", EducationLevelsHighSchool.programOrientation()), property("php", EppEduPlanHigherProf.programOrientation())));


        column(dql, property("workPlan", EppWorkPlan.P_ID), "rupId").comment("ID_РУП");
        column(dql, property("planVerBlock", EppEduPlanVersionBlock.L_EDU_PLAN_VERSION), "eduPlanVersionID").comment("ID_версии_учебного_плана");
        column(dql, property("workPlan", EppWorkPlan.P_TITLE_POSTFIX), "title").comment("Название");
        column(dql, property("lhs", EducationLevelsHighSchool.P_ID), "trainDir").comment("ID_направления_подготовки");
        column(dql, property("workPlan", EppWorkPlan.P_NUMBER), "workPlanNumber").comment("Номер");
        column(dql, property("workPlan", EppWorkPlan.P_REGISTRATION_NUMBER), "reqNum").comment("номер_регистрации");
        column(dql, property("workPlan", EppWorkPlan.year().educationYear().title()), "studYear").comment("учебный_год");
        column(dql, property("workPlan", EppWorkPlan.term().title()), "term").comment("семестр");
        column(dql, property("eduPlan", EppEduPlan.L_PROGRAM_FORM), "eduFormId").comment("форма");
        column(dql, property("eduPlan", EppEduPlan.L_DEVELOP_CONDITION), "eduConditionId").comment("условия");
        column(dql, property("eduPlan", EppEduPlan.L_PROGRAM_TRAIT), "eduTechId").comment("технология");
        column(dql, property("eduPlan", EppEduPlan.developGrid().developPeriod().id()), "devPeriodId").comment("срок");

        return dql;
    }
}
