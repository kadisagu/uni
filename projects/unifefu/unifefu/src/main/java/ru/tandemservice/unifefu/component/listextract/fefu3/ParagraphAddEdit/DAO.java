/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu3.ParagraphAddEdit;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 18.03.2013
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuAdmitToDiplomaStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentCustomStateCI(DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), UniFefuDefines.ADMITTED_TO_DIPLOMA));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        model.setCourseAndGroupDisabled(!model.isParagraphOnlyOneInTheOrder());
        FefuAdmitToDiplomaStuListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            model.setCourse(firstExtract.getGroup().getCourse());
            model.setGroup(firstExtract.getGroup());
            model.setGosExamDefine(firstExtract.getPluralForGosExam() != null);
            model.setPluralForGosExam(firstExtract.getPluralForGosExam() == null ? false : firstExtract.getPluralForGosExam());
            model.setCompensationType(firstExtract.getCompensationType());
            model.setNotNeedAdmissionToGIA(firstExtract.isNotNeedAdmissionToGIA());
        }
    }

    @Override
    public void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourse()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    public FefuAdmitToDiplomaStuListExtract createNewInstance(Model model)
    {
        return new FefuAdmitToDiplomaStuListExtract();
    }

    @Override
    public void fillExtract(FefuAdmitToDiplomaStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
        extract.setPluralForGosExam(model.getGosExamDefine() ? model.getPluralForGosExam() : null);
        extract.setNotNeedAdmissionToGIA(model.getNotNeedAdmissionToGIA());
    }
}