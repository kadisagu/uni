/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuOrderContingentStuDPOListExtract, Model>
{
}