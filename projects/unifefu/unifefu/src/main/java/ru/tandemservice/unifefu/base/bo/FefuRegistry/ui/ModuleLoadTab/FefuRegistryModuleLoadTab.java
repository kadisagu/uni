/* $Id: FefuRegistryModuleLoadTab.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.ModuleLoadTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.FefuRegistryManager;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 05.02.14
 * Time: 12:43
 */

@Configuration
public class FefuRegistryModuleLoadTab extends BusinessComponentManager
{
    public final static String LOAD_DS = "loadDS";

    @Bean
    public ColumnListExtPoint loadCL()
    {
        return columnListExtPointBuilder(LOAD_DS)
                .addColumn(textColumn("title", "title").required(true))
                .addColumn(textColumn("value", "value").required(true))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LOAD_DS, loadCL(), FefuRegistryManager.instance().fefuModuleLoadDSHandler()))
                .create();
    }
}
