/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.CatalogLog;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.document.provider.SimpleDocumentRenderer;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unifefu.base.bo.NSISync.NSISyncManager;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiDaemonDao;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiHumanLoginDaemonDao;
import ru.tandemservice.unifefu.dao.daemon.IFefuNsiDaemonDao;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuNsiCatalogTypeCodes;
import ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;

import java.io.OutputStream;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 04.12.2013
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "nsiCatalogId")
})
public class NSISyncCatalogLogUI extends UIPresenter
{

    private Long _nsiCatalogId;
    private FefuNsiCatalogType _nsiCatalog;
    private Date _requestDateFrom;
    private Date _requestDateTo;
    private String _messageGuid;
    private IdentifiableWrapper _eventType;
    private IdentifiableWrapper _operationType;
    private IdentifiableWrapper _messageStatus;
    private String _entityGuid;
    private String _anyXmlBodyGuid;
    private boolean _filtersVisible = false;
    private ButtonWrapper _currentButton;

    private Map<String, List<ButtonWrapper>> _buttonsMap = new HashMap<>();


    @Override
    public void onComponentRefresh()
    {
        _nsiCatalog = DataAccessServices.dao().getNotNull(FefuNsiCatalogType.class, _nsiCatalogId);
        refreshButtonsList();
    }

    public void refreshButtonsList()
    {
        _buttonsMap.clear();

        List<ButtonWrapper> organizationButtons = new ArrayList<>();
        organizationButtons.add(new ButtonWrapper("Получить соответствие организаций", "onClickGetOrganizationsMapping"));
        List<ButtonWrapper> departmentButtons = new ArrayList<>();
        departmentButtons.add(new ButtonWrapper("Выгрузить подразделения из НСИ", "onClickGetDepartmentsFromNSI"));
        departmentButtons.add(new ButtonWrapper("Получить разницу оргструктур", "onClickGetDeprartDifferences"));
        //departmentButtons.add(new ButtonWrapper("Удалить подразделения", "onClickDeleteOrgUnits"));
        //departmentButtons.add(new ButtonWrapper("Получить разницу оргструктур", "onClickGetOrgstructDifferences"));
        _buttonsMap.put(FefuNsiCatalogTypeCodes.DEPARTMENT_TYPE, departmentButtons);

        boolean showSyncLoginButtons = NSISyncManager.instance().dao().isAnyIdInLoginSyncStack();
        List<ButtonWrapper> humanButtons = new ArrayList<>();
        //humanButtons.add(new ButtonWrapper("Протестировать", "onClickTestHumanReactor", false));
        if (FefuNsiHumanLoginDaemonDao.isLocked() && showSyncLoginButtons)
        {
            humanButtons.add(new ButtonWrapper("Возобновить синхронизацию логинов", "onClickResumeSyncLoginsFromNSI", false));
        } else
        {
            if (showSyncLoginButtons)
            {
                humanButtons.add(new ButtonWrapper("Синхронизировать логины", "onClickSyncLoginsFromNSI", false));
                humanButtons.add(new ButtonWrapper("Приостановить синхронизацию логинов", "onClickStopSyncLoginsFromNSI", false));
            } else
            {
                humanButtons.add(new ButtonWrapper("Синхронизировать логины", "onClickSyncLoginsFromNSI", false));
            }
        }
        _buttonsMap.put(FefuNsiCatalogTypeCodes.HUMAN_TYPE, humanButtons);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (NSISyncCatalogLog.NSI_CATALOG_LOG_DS.equals(dataSource.getName()))
        {
            dataSource.put(NSISyncCatalogLog.NSI_CATALOG, _nsiCatalog);
            dataSource.put(NSISyncCatalogLog.REQUEST_DATE_FROM_GUID, _requestDateFrom);
            dataSource.put(NSISyncCatalogLog.REQUEST_DATE_TO_GUID, _requestDateTo);
            dataSource.put(NSISyncCatalogLog.MESSAGE_GUID, _messageGuid);
            dataSource.put(NSISyncCatalogLog.EVENT_TYPE, _eventType);
            dataSource.put(NSISyncCatalogLog.OPERATION_TYPE, _operationType);
            dataSource.put(NSISyncCatalogLog.MESSAGE_STATUS, _messageStatus);
            dataSource.put(NSISyncCatalogLog.ENTITY_GUID, _entityGuid);
            dataSource.put(NSISyncCatalogLog.ANY_XML_BODY_GUID, _anyXmlBodyGuid);
        }
    }

    public void onClickForceDaemonStart()
    {
        FefuNsiDaemonDao.DAEMON.wakeUpDaemon();
    }

    public void onClickGetOrganizationsMapping()
    {
        try
        {
            NSISyncManager.instance().dao().doCreateOrganizationsMappingReport();
        } catch (Exception ignored)
        {

        }
    }

    public void onClickGetDepartmentsFromNSI()
    {
        if (NSISyncManager.instance().dao().isOrgStructureInProcess())
            throw new ApplicationException("Идёт получение файла с орг. структурой из НСИ. Попробуйте чуть позже.");

        if (null == NSISyncManager.instance().dao().getOrgStructureFile())
        {
            try
            {
                NSISyncManager.instance().dao().doGetFullOrgStructureFromNsi();
            } catch (Exception e)
            {
                System.out.println("Could not get full orgstructure from NSI.");
                e.printStackTrace();
            }
        }

        if (null != NSISyncManager.instance().dao().getOrgStructureFile())
        {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("application/txt")
                    .fileName("NsiDepartments" + DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date()).replaceAll(":", "_") + ".xml")
                    .document(NSISyncManager.instance().dao().getOrgStructureFile()), true);
        }
    }

    public void onClickGetDeprartDifferences()
    {
        if (NSISyncManager.instance().dao().isOrgStructureInProcess())
            throw new ApplicationException("Идёт получение файла с орг. структурой из НСИ. Попробуйте чуть позже.");

        if (null != NSISyncManager.instance().dao().getOrgStructureFile())
        {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("application/txt")
                    .fileName("OrgstructureDifferences" + DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date()).replaceAll(":", "_") + ".csv")
                    .document(NSISyncManager.instance().dao().getOrgstructureDifferencesFile()), true);
        } else
            throw new ApplicationException("Данные об оргструктуре НСИ не получены. Необходимо сначала получить оргструктуру из НСИ. Нажмите кнопку \"Выгрузить подразделения из НСИ\".");
    }

    public void onClickDeleteOrgUnits()
    {
        NSISyncManager.instance().dao().doDeleteOrgUnitsTree(1488383407549899368L);
    }

    public void onClickGetOrgstructDifferences()
    {
        try
        {
            BusinessComponentUtils.downloadDocument(buildOrgStructureDifferenceDocumentRenderer(), true);
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public IDocumentRenderer buildOrgStructureDifferenceDocumentRenderer()
    {
        return new SimpleDocumentRenderer(null)
        {
            @Override
            public String getContentType()
            {
                return DatabaseFile.CONTENT_TYPE_TEXT_CSV;
            }

            @Override
            public String getFilename()
            {
                return "nsiToObOrgstructDifferences.csv";
            }

            @Override
            public void render(OutputStream stream) throws Exception
            {
                NSISyncManager.instance().dao().prepareOrgstructDifferences(stream);
            }
        };
    }

    public void onClickSyncLoginsFromNSI()
    {
        NSISyncManager.instance().dao().doFullFillHumanLoginSyncStack();
        refreshButtonsList();
    }

    public void onClickStopSyncLoginsFromNSI()
    {
        FefuNsiHumanLoginDaemonDao.lockDaemon();
        refreshButtonsList();
    }

    public void onClickResumeSyncLoginsFromNSI()
    {
        FefuNsiHumanLoginDaemonDao.unlockDaemon();
        refreshButtonsList();
    }

    public void onClickTestHumanReactor()
    {
        NSISyncManager.instance().testDao().doCreateStudent(true);
    }

    public String getMessageStatusColored()
    {
        FefuNsiLogRow logRow = _uiConfig.getDataSource(NSISyncCatalogLog.NSI_CATALOG_LOG_DS).getCurrent();
        return null != logRow ? logRow.getStatusStrColored() : null;
    }

    public boolean isDaemonHasTasksInQueue()
    {
        return IFefuNsiDaemonDao.instance.get().isDaemonHasTasksInQueue();
    }

    public boolean isAnyActionAvailable()
    {
        return isDaemonHasTasksInQueue() || isAnyButtonAvailable();
    }

    public boolean isAnyButtonAvailable()
    {
        return null != _buttonsMap.get(getNsiCatalog().getCode()) && !_buttonsMap.get(getNsiCatalog().getCode()).isEmpty();
    }

    public List<ButtonWrapper> getButtons()
    {
        return null != _buttonsMap.get(getNsiCatalog().getCode()) ? _buttonsMap.get(getNsiCatalog().getCode()) : new ArrayList<>();
    }

    public void onClickSearch()
    {
        //TODO getSettings().save();
    }

    public void onClickClear()
    {
        _requestDateFrom = null;
        _requestDateTo = null;
        _messageGuid = null;
        _eventType = null;
        _operationType = null;
        _messageStatus = null;
        _entityGuid = null;
        _anyXmlBodyGuid = null;
        onClickSearch();
    }

    public void onClickShowHideFilters()
    {
        _filtersVisible = !_filtersVisible;
    }

    public void onSendMessageAgain()
    {
        FefuNsiLogRow srcLogRow = DataAccessServices.dao().getNotNull(FefuNsiLogRow.class, getListenerParameterAsLong());

        if (!FefuNsiLogRow.OUT_EVENT_TYPE.equals(srcLogRow.getEventType().toLowerCase()) && -1 == srcLogRow.getMessageStatus())
        {
            NsiReactorUtils.reExecuteOBAction(getListenerParameterAsLong());
        } else
        {
            NsiReactorUtils.executeNSIAction(getListenerParameterAsLong(), true);
        }
    }

    public void onGetMessagePack()
    {
        NSISyncManager.instance().dao().getNsiCatalogLogRowPackFile(getListenerParameterAsLong());
    }

    public Long getNsiCatalogId()
    {
        return _nsiCatalogId;
    }

    public void setNsiCatalogId(Long nsiCatalogId)
    {
        _nsiCatalogId = nsiCatalogId;
    }

    public FefuNsiCatalogType getNsiCatalog()
    {
        return _nsiCatalog;
    }

    public void setNsiCatalog(FefuNsiCatalogType nsiCatalog)
    {
        _nsiCatalog = nsiCatalog;
    }

    public Date getRequestDateFrom()
    {
        return _requestDateFrom;
    }

    public void setRequestDateFrom(Date requestDateFrom)
    {
        _requestDateFrom = requestDateFrom;
    }

    public Date getRequestDateTo()
    {
        return _requestDateTo;
    }

    public void setRequestDateTo(Date requestDateTo)
    {
        _requestDateTo = requestDateTo;
    }

    public String getMessageGuid()
    {
        return _messageGuid;
    }

    public void setMessageGuid(String messageGuid)
    {
        _messageGuid = messageGuid;
    }

    public IdentifiableWrapper getEventType()
    {
        return _eventType;
    }

    public void setEventType(IdentifiableWrapper eventType)
    {
        _eventType = eventType;
    }

    public IdentifiableWrapper getOperationType()
    {
        return _operationType;
    }

    public void setOperationType(IdentifiableWrapper operationType)
    {
        _operationType = operationType;
    }

    public IdentifiableWrapper getMessageStatus()
    {
        return _messageStatus;
    }

    public void setMessageStatus(IdentifiableWrapper messageStatus)
    {
        _messageStatus = messageStatus;
    }

    public String getEntityGuid()
    {
        return _entityGuid;
    }

    public void setEntityGuid(String entityGuid)
    {
        _entityGuid = entityGuid;
    }

    public String getAnyXmlBodyGuid()
    {
        return _anyXmlBodyGuid;
    }

    public void setAnyXmlBodyGuid(String anyXmlBodyGuid)
    {
        _anyXmlBodyGuid = anyXmlBodyGuid;
    }

    public boolean isFiltersVisible()
    {
        return _filtersVisible;
    }

    public void setFiltersVisible(boolean filtersVisible)
    {
        _filtersVisible = filtersVisible;
    }

    public ButtonWrapper getCurrentButton()
    {
        return _currentButton;
    }

    public void setCurrentButton(ButtonWrapper currentButton)
    {
        _currentButton = currentButton;
    }
}