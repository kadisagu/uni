/* $Id: FefuPracticeContractWithExtOuDSHandler.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;

import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.fias.base.entity.AddressInter;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.FefuPracticeContractWithExtOuManager;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/28/13
 * Time: 6:31 PM
 */
public class FefuPracticeContractWithExtOuDSHandler extends DefaultSearchDataSourceHandler
{
    public FefuPracticeContractWithExtOuDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String contractNum = context.get("contractNum");
        String internalNum = context.get("internalNum");
        Date contractDateFrom = context.get("contractDateFrom");
        Date contractDateTo = context.get("contractDateTo");
        String extOuTitle = context.get("extOuTitle");
//        Long externalOrgUnit = context.get("externalOrgUnit");
        Long headerExtOu = context.get("headerExtOu");
        Long checked = context.get("checked");
        Long archival = context.get("archival");

        String abbreviatedNameOrganization = context.get("abbreviatedNameOrganization");
        String legalAddress = context.get("legalAddress");
        String actualAddress = context.get("actualAddress");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuPracticeContractWithExtOu.class, "pc");
        builder.fetchPath(DQLJoinType.left, FefuPracticeContractWithExtOu.externalOrgUnit().fromAlias("pc"), "ou");
        builder.fetchPath(DQLJoinType.left, FefuPracticeContractWithExtOu.headerExtOu().fromAlias("pc"), "head");
        builder.fetchPath(DQLJoinType.left, FefuPracticeContractWithExtOu.headerExtOu().person().fromAlias("pc"), "person");
        builder.fetchPath(DQLJoinType.left, FefuPracticeContractWithExtOu.headerExtOu().person().identityCard().fromAlias("pc"), "idCard");
        builder.column("pc");

        builder.where(betweenDays(FefuPracticeContractWithExtOu.contractDate().fromAlias("pc"), contractDateFrom, contractDateTo));

        if(!StringUtils.isEmpty(contractNum))
            builder.where(like(DQLFunctions.upper(property("pc", FefuPracticeContractWithExtOu.contractNum())), value(CoreStringUtils.escapeLike(contractNum, true))));
        if(!StringUtils.isEmpty(internalNum))
            builder.where(like(DQLFunctions.upper(property("pc", FefuPracticeContractWithExtOu.internalNum())), value(CoreStringUtils.escapeLike(internalNum, true))));
        if(!StringUtils.isEmpty(extOuTitle))
            builder.where(like(DQLFunctions.upper(property("pc", FefuPracticeContractWithExtOu.externalOrgUnit().title())), value(CoreStringUtils.escapeLike(extOuTitle, true))));
//        if(null != externalOrgUnit)
//            builder.where(eq(property("pc", FefuPracticeContractWithExtOu.externalOrgUnit().id()), value(externalOrgUnit)));
        if(null != headerExtOu)
            builder.where(eq(property("pc", FefuPracticeContractWithExtOu.headerExtOu().id()), value(headerExtOu)));
        if(null != checked)
        {
            Boolean isChecked = FefuPracticeContractWithExtOuManager.YES.equals(checked) ? Boolean.TRUE : Boolean.FALSE;
            builder.where(eq(property("pc", FefuPracticeContractWithExtOu.checked()), value(isChecked)));
        }
        if(null != archival)
        {
            Boolean isArchival = FefuPracticeContractWithExtOuManager.YES.equals(archival) ? Boolean.TRUE : Boolean.FALSE;
            builder.where(eq(property("pc", FefuPracticeContractWithExtOu.archival()), value(isArchival)));
        }

        EntityOrder entityOrder = input.getEntityOrder();
        if("headerExtOu".equals(entityOrder.getColumnName()))
        {
            builder.order(property("pc", FefuPracticeContractWithExtOu.headerExtOu().person().identityCard().fullFio()), entityOrder.getDirection());
        }

        if(null !=abbreviatedNameOrganization)
        {
            DQLSelectBuilder sub = new DQLSelectBuilder().fromEntity(FefuPracticeContractWithExtOu.class,"ex")
                    .fetchPath(DQLJoinType.left,FefuPracticeContractWithExtOu.externalOrgUnit().fromAlias("ex"), "e");
            for(String s:abbreviatedNameOrganization.split(" "))
            {
                sub.where(
                        like(property("ex",FefuPracticeContractWithExtOu.externalOrgUnit().shortTitle()), value("%" + s + "%"))
                );
            }
            builder.where(in(property("pc", FefuPracticeContractWithExtOu.id()), sub.buildQuery()));
        }

        if(!StringUtils.isEmpty(legalAddress))
        {
            builder.joinEntity("ou", DQLJoinType.left, AddressRu.class, "alr", eq(property("ou", ExternalOrgUnit.legalAddress().id()), property("alr", AddressRu.id())));
            builder.joinPath(DQLJoinType.left, AddressRu.country().fromAlias("alr"), "alrc");
            builder.joinPath(DQLJoinType.left, AddressRu.settlement().fromAlias("alr"), "alrs");
            builder.joinPath(DQLJoinType.left, AddressRu.street().fromAlias("alr"), "alrst");
            builder.joinPath(DQLJoinType.left, AddressRu.district().fromAlias("alr"), "alrd");
            builder.joinEntity("ou", DQLJoinType.left, AddressInter.class, "ali", eq(property("ou", ExternalOrgUnit.legalAddress().id()), property("ali", AddressInter.id())));
            builder.joinPath(DQLJoinType.left, AddressInter.country().fromAlias("ali"), "alic");
            builder.joinPath(DQLJoinType.left, AddressInter.settlement().fromAlias("ali"), "alis");

            builder.where(
                    or(
                            like(DQLFunctions.upper(
                                    DQLFunctions.concat(
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("alr", AddressRu.inheritedPostCode()))},
                                                    new IDQLExpression[]{property("alr", AddressRu.inheritedPostCode())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("alr", AddressRu.country()))},
                                                    new IDQLExpression[]{property("alr", AddressRu.country().title())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("alr", AddressRu.settlement()))},
                                                    new IDQLExpression[]{property("alr", AddressRu.settlement().directTitle())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("alr", AddressRu.district()))},
                                                    new IDQLExpression[]{property("alr", AddressRu.district().title())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("alr", AddressRu.street()))},
                                                    new IDQLExpression[]{property("alr", AddressRu.street().title())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("alr", AddressRu.houseNumber()))},
                                                    new IDQLExpression[]{property("alr", AddressRu.houseNumber())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("alr", AddressRu.houseUnitNumber()))},
                                                    new IDQLExpression[]{property("alr", AddressRu.houseUnitNumber())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("alr", AddressRu.flatNumber()))},
                                                    new IDQLExpression[]{property("alr", AddressRu.flatNumber())},
                                                    value(" "))
                                    )
                            ), value(CoreStringUtils.escapeLike(legalAddress, true))),
                            like(DQLFunctions.upper(
                                    DQLFunctions.concat(
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("ali", AddressInter.postCode()))},
                                                    new IDQLExpression[]{property("ali", AddressInter.postCode())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("ali", AddressInter.country()))},
                                                    new IDQLExpression[]{property("ali", AddressInter.country().title())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("ali", AddressInter.settlement()))},
                                                    new IDQLExpression[]{property("ali", AddressInter.settlement().directTitle())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("ali", AddressInter.region()))},
                                                    new IDQLExpression[]{property("ali", AddressInter.region())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("ali", AddressInter.district()))},
                                                    new IDQLExpression[]{property("ali", AddressInter.district())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("ali", AddressInter.addressLocation()))},
                                                    new IDQLExpression[]{property("ali", AddressInter.addressLocation())},
                                                    value(" "))
                                    )
                            ), value(CoreStringUtils.escapeLike(legalAddress, true)))
                    )
            );
        }

        if(!StringUtils.isEmpty(actualAddress))
        {
            builder.joinEntity("ou", DQLJoinType.left, AddressRu.class, "afr", eq(property("ou", ExternalOrgUnit.factAddress().id()), property("afr", AddressRu.id())));
            builder.joinPath(DQLJoinType.left, AddressRu.country().fromAlias("afr"), "afrc");
            builder.joinPath(DQLJoinType.left, AddressRu.settlement().fromAlias("afr"), "afrs");
            builder.joinPath(DQLJoinType.left, AddressRu.street().fromAlias("afr"), "afrst");
            builder.joinPath(DQLJoinType.left, AddressRu.district().fromAlias("afr"), "afrd");
            builder.joinEntity("ou", DQLJoinType.left, AddressInter.class, "afi", eq(property("ou", ExternalOrgUnit.factAddress().id()), property("afi", AddressInter.id())));
            builder.joinPath(DQLJoinType.left, AddressInter.country().fromAlias("afi"), "afic");
            builder.joinPath(DQLJoinType.left, AddressInter.settlement().fromAlias("afi"), "afis");


            builder.where(
                    or(
                            like(DQLFunctions.upper(
                                    DQLFunctions.concat(
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afr", AddressRu.inheritedPostCode()))},
                                                    new IDQLExpression[]{property("afr", AddressRu.inheritedPostCode())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afr", AddressRu.country()))},
                                                    new IDQLExpression[]{property("afr", AddressRu.country().title())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afr", AddressRu.settlement()))},
                                                    new IDQLExpression[]{property("afr", AddressRu.settlement().directTitle())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afr", AddressRu.district()))},
                                                    new IDQLExpression[]{property("afr", AddressRu.district().title())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afr", AddressRu.street()))},
                                                    new IDQLExpression[]{property("afr", AddressRu.street().title())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afr", AddressRu.houseNumber()))},
                                                    new IDQLExpression[]{property("afr", AddressRu.houseNumber())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afr", AddressRu.houseUnitNumber()))},
                                                    new IDQLExpression[]{property("afr", AddressRu.houseUnitNumber())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afr", AddressRu.flatNumber()))},
                                                    new IDQLExpression[]{property("afr", AddressRu.flatNumber())},
                                                    value(" "))
                                    )
                            ), value(CoreStringUtils.escapeLike(actualAddress, true))),
                            like(DQLFunctions.upper(
                                    DQLFunctions.concat(
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afi", AddressInter.postCode()))},
                                                    new IDQLExpression[]{property("afi", AddressInter.postCode())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afi", AddressInter.country()))},
                                                    new IDQLExpression[]{property("afi", AddressInter.country().title())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afi", AddressInter.settlement()))},
                                                    new IDQLExpression[]{property("afi", AddressInter.settlement().directTitle())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afi", AddressInter.region()))},
                                                    new IDQLExpression[]{property("afi", AddressInter.region())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afi", AddressInter.district()))},
                                                    new IDQLExpression[]{property("afi", AddressInter.district())},
                                                    value(" ")),
                                            caseExpr(
                                                    new IDQLExpression[]{isNotNull(property("afi", AddressInter.addressLocation()))},
                                                    new IDQLExpression[]{property("afi", AddressInter.addressLocation())},
                                                    value(" "))
                                    )
                            ), value(CoreStringUtils.escapeLike(actualAddress, true)))
                    )
            );
        }

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
    }

}
