/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;

import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 17.07.2013
 */
public interface IDirectumDAO extends INeedPersistenceSupport
{
    byte[] getStudentOrderPrintForm(RtfDocument document, String fileName, String docDirectumOrderId);

    byte[] getEntrantOrderPrintForm(Long orderId, String docDirectumOrderId);

    void doRegisterDirectumActivityLogRow(DirectumOperationResult resultToLog);

    void doRegisterDirectumActivityErrorLogRow(String directumOrderId, String operationType, String operationResultMsg);

    // DEV-4697
    void createStudentOrderDirectumExtension(AbstractStudentOrder order, String directumOrderId, String directumTaskId, String directumNumber, Date directumDate);

    // DEV-4697
    void createStudentOrderDirectumExtension(EnrollmentOrder order, String directumOrderId, String directumTaskId, String directumNumber, Date directumDate);

    void doUpdateStudentOrderDirectumExtension(String directumOrderId, String directumTaskId, String directumNumber, Date directumDate);

    DirectumOperationResult doRejectOrder(String directumOrderId);

    DirectumOperationResult doSendOrderToFormation(String directumOrderId);

    Long getOrderIdByDirectumOrderId(String directumOrderId);

    // DEV-4592
    DirectumOperationResult doCommitOrder(String directumOrderId, String orderNumber, String orderDate, String directumScanUrl);

    // DEV-4592
    DirectumOperationResult doUpdateOrderWithSignedScanUrl(String directumOrderId, String directumScanUrl);

    DirectumOperationResult doUpdateOrderRequisites(String directumOrderId, String orderNumber, String orderDate, String directumScanUrl);

    // DEV-4728
    void doUpdateDirectumTaskId(String directumOrderId, String directumTaskId);

    /**
     * Проверяет, находится ли текущий приказ в очереди на согласовании
     *
     * @param orderId id приказа
     * @param validate выводить исключения
     * @return true - находится, иначе - false
     */
    boolean checkDirectumSendingOrderToAccept(Long orderId, Boolean validate);

    /**
     * Снятие приказа с очереди на согласование
     * @param orderId id приказа
     */
    void doDeleteSendingOrder(Long orderId);
}