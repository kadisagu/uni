/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e62;

import ru.tandemservice.movestudent.entity.ExcludeStateExamStuExtract;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 21.11.2014
 */
public class ExcludeStateExamStuExtractDao extends ru.tandemservice.movestudent.component.modularextract.e62.ExcludeStateExamStuExtractDao
{
    @Override
    public void doCommit(ExcludeStateExamStuExtract extract, Map parameters)
    {
        super.doCommit(extract, parameters);
        UnifefuDaoFacade.getFefuMoveStudentDAO().setStatusEndDateByCode(extract, UniFefuDefines.ADMITTED_TO_STATE_EXAMES, extract.getExcludeDate());
    }

    @Override
    public void doRollback(ExcludeStateExamStuExtract extract, Map parameters)
    {
        super.doRollback(extract, parameters);

        UnifefuDaoFacade.getFefuMoveStudentDAO().revertCustomStateDates(extract);
    }
}