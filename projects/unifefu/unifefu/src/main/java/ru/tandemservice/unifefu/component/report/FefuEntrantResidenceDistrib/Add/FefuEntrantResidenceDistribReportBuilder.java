package ru.tandemservice.unifefu.component.report.FefuEntrantResidenceDistrib.Add;

import com.google.common.collect.Lists;
import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.component.report.FefuReportUtil;
import ru.tandemservice.unifefu.entity.EntrantFefuExt;
import ru.tandemservice.unifefu.entity.FefuFederalAreasRel;
import ru.tandemservice.unifefu.entity.catalog.FefuFederalDistrict;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

class FefuEntrantResidenceDistribReportBuilder
{
    private Model _model;
    private Session _session;
    // создаем структуры данных
    final int ALL_IDX = 0;
    final int ONLINE_IDX = 1;
    final int ORIG_IDX = 2;
//    явное указание любого из пяти: физика, информатика, биология, химия, география (информатику лучше проверять по вхождению - т.к. может быть "Информатика и ИКТ").
    final String[] PROFILE_DISC = new String[] {"ФИЗИКА", "ИНФОРМАТИКА", "БИОЛОГИЯ", "ХИМИЯ", "ГЕОГРАФИЯ"};

    FefuEntrantResidenceDistribReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        int REQUEST_ID = 0;
        int ENTRANT_ID = 1;
        int SETTLEMENT_ID = 2;
        int COUNT = 3;
        if (_model.isIncludeHighMarks()) {
            ENTRANT_ID = 0;
            SETTLEMENT_ID = 1;
            COUNT = 2;
        }

        List<Object[]> data = getData();
        List<Long> settlementIds = new ArrayList<>();
        for (Object[] row : data)
            settlementIds.add(((Number) row[SETTLEMENT_ID]).longValue());

        // грузим населенные пункты в кэш
        BatchUtils.execute(settlementIds, 1000, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                if (elements.isEmpty()) return;
                MQBuilder builder = new MQBuilder(AddressItem.ENTITY_CLASS, "a");
                builder.addLeftJoinFetch("a", AddressItem.L_PARENT, "p1");
                builder.addLeftJoinFetch("p1", AddressItem.L_PARENT, "p2");
                builder.addLeftJoinFetch("p2", AddressItem.L_PARENT, "p3");
                builder.add(MQExpression.in("a", AddressItem.P_ID, elements));
            }
        });

        final List<Long> onlineEntrantIds = getOnlineEntrant();
        final Map<AddressItem, int[]> area2count = new HashMap<>();
        final Map<FefuFederalDistrict, Map<AddressItem, int[]>> area2countFed = new HashMap<>();
        final Map<FefuFederalDistrict, int[]> federal2count = new HashMap<>();
        final Map<AddressItem, Map<AddressItem, int[]>> area2city2count = new HashMap<>(); // детализация
        final int[] foreignCountry = new int[3];

        String title = "абитуриентов";
        if (_model.isIncludeHighMarks())
            title = "абитуриентов с высокими баллами";

        List<String> testList = new ArrayList<>();
        int count = 1;
        Map<AddressItem, FefuFederalDistrict> federalAreasMap = new HashMap<>();
        for (Object[] row : data)
        {
            Long entrantId = ((Number) row[ENTRANT_ID]).longValue();

            boolean isOrig;
            if (_model.isIncludeHighMarks()) {
                // TODO for test
                Entrant entrant = DataAccessServices.dao().get(entrantId);
                testList.add(entrant.getFullFio());
                isOrig = isEntrantWithOriginal(_model.getEnrollmentCampaign(), entrantId);
            } else {
                long requestId = ((Number) row[REQUEST_ID]).longValue();
                isOrig = isRequestIdsWithOrig(requestId);
            }

            boolean isOnline = false;

            if (onlineEntrantIds.contains(entrantId))
                isOnline = true;

            long settlementId = ((Number) row[SETTLEMENT_ID]).longValue();
            if (!_model.isIncludeHighMarks())
                count = ((Number) row[COUNT]).intValue();

            if (_model.isGroupByFederalDistrict())
                federalAreasMap = getFefuFederalDistrictMap();

            // получаем город и регион абитуриента
            AddressItem settlement = (AddressItem) _session.get(AddressItem.class, settlementId);
            AddressItem region = settlement;
            while (region.getParent() != null)
                region = region.getParent();

            if (settlement.getCountry().getCode() == IKladrDefines.RUSSIA_COUNTRY_CODE)
            {
                int[] value = area2count.get(region);
                if (value == null)
                    area2count.put(region, value = new int[3]);
                value = fillMapValue(value, isOrig, isOnline, count);
                if (_model.isGroupByFederalDistrict()) {
                    FefuFederalDistrict fefuFederalDistrict = federalAreasMap.get(region);
                    if (fefuFederalDistrict != null) {
                        int[] valueFed = federal2count.get(fefuFederalDistrict);
                        if (valueFed == null)
                            federal2count.put(fefuFederalDistrict, valueFed = new int[3]);
                        valueFed = fillMapValue(valueFed, isOrig, isOnline, count);

                        Map<AddressItem, int[]> fedAreas = area2countFed.get(fefuFederalDistrict);
                        if (fedAreas == null)
                            area2countFed.put(fefuFederalDistrict, fedAreas = new HashMap<>());
                        int[] mapValueArea = fedAreas.get(region);
                        if (mapValueArea == null)
                            fedAreas.put(region, mapValueArea = new int[3]);
                        mapValueArea = fillMapValue(mapValueArea, isOrig, isOnline, count);
                    }
                }
                if (_model.getDetail().isTrue())
                {
                    Map<AddressItem, int[]> map = area2city2count.get(region);
                    if (map == null)
                        area2city2count.put(region, map = new HashMap<>());
                    int[] mapValue = map.get(settlement);
                    if (mapValue == null)
                        map.put(settlement, mapValue = new int[3]);
                    mapValue = fillMapValue(mapValue, isOrig, isOnline, count);
                }
            } else {
                foreignCountry[ALL_IDX] += count;
                if (isOrig)
                    foreignCountry[ORIG_IDX] += 1;
                if (isOnline)
                    foreignCountry[ONLINE_IDX] += 1;
            }
        }

        // считаем сумму
        final int[] sum = new int[3];
        if (_model.isIncludeForeignPerson()) {
            sum[ALL_IDX] += foreignCountry[ALL_IDX];
            sum[ORIG_IDX] += foreignCountry[ORIG_IDX];
            sum[ONLINE_IDX] += foreignCountry[ONLINE_IDX];
        }
        if (_model.isGroupByFederalDistrict())
        {
             for (int[] entry : federal2count.values())
            {
                sum[ALL_IDX] += entry[ALL_IDX];
                sum[ORIG_IDX] += entry[ORIG_IDX];
                sum[ONLINE_IDX] += entry[ONLINE_IDX];
            }
        } else {
            for (int[] entry : area2count.values())
            {
                sum[ALL_IDX] += entry[ALL_IDX];
                sum[ORIG_IDX] += entry[ORIG_IDX];
                sum[ONLINE_IDX] += entry[ONLINE_IDX];
            }
        }

        final int totalSum = sum[ALL_IDX];

        final List<AddressItem> areaList = new ArrayList<>(area2count.keySet());
        Collections.sort(areaList, new ValuesComparator(area2count));

        final List<FefuFederalDistrict> federalList = new ArrayList<>(federal2count.keySet());
        Collections.sort(federalList, new ValuesComparatorFederal(federal2count));

        // load template
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniFefuDefines.TEMPLATE_ENTRANT_RESIDENCE_DISTRIB);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument clone = document.getClone();
        final Map<Long, String> area2percentMap = new HashMap<>();

        final int blackIndex = document.getHeader().getColorTable().addColor(0, 0, 0);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("vuz", TopOrgUnit.getInstance().getTitle());
        injectModifier.put("stage", _model.getReport().getEnrollmentCampaignStage());
        injectModifier.put("title", title);
        injectModifier.put("S1", Integer.toString(sum[ALL_IDX]));
        injectModifier.put("S2", Integer.toString(sum[ONLINE_IDX]));
        injectModifier.put("S3", Integer.toString(sum[ORIG_IDX]));
        injectModifier.put("S4", "100");

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", new String[0][]);
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                RtfString string = new RtfString();
                if (_model.getReport().getCompensationType() != null) {
                    if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_model.getReport().getCompensationType().getCode()))
                        string.append("Подали (бюд.)");
                    else
                        string.append("Подали (дог.)");
                } else {
                    string.append("Подали (всего)");
                }
                RtfCell cell = table.getRowList().get(currentRowIndex - 1).getCellList().get(1);
                cell.setElementList(string.toList());
                SharedRtfUtil.setCellAlignment(cell, IRtfData.B);

                RtfRow templateRow = table.getRowList().get(currentRowIndex);
                List<RtfRow> newRowList = new ArrayList<>();
                if (_model.isGroupByFederalDistrict())
                {
                    // создаем строки с федеральныими округами РФ
                    for (FefuFederalDistrict district : federalList)
                    {
                        int[] value = federal2count.get(district);
                        if (value[ALL_IDX] > 0)
                            newRowList.add(createRow(templateRow, 0, blackIndex, district.getTitle(), value[ALL_IDX], value[ONLINE_IDX], value[ORIG_IDX], totalSum, district.getId(), area2percentMap, true));
                        // создаем строки с областями РФ
                        List<AddressItem> areaListFed = new ArrayList<>(area2countFed.get(district).keySet());
                        Collections.sort(areaListFed, new ValuesComparator(area2countFed.get(district)));
                        for (AddressItem area : areaListFed)
                        {
                            int[] valueArea = area2countFed.get(district).get(area);
                            if (valueArea[ALL_IDX] > 0)
                                newRowList.add(createRow(templateRow, 0, blackIndex, "  " + area.getTitleWithType(), valueArea[ALL_IDX], valueArea[ONLINE_IDX], valueArea[ORIG_IDX], totalSum, area.getId(), area2percentMap, false));
                        }
                    }
                } else {
                    // создаем строки с областями РФ
                    for (AddressItem area : areaList)
                    {
                        int[] value = area2count.get(area);
                        if (value[ALL_IDX] > 0)
                            newRowList.add(createRow(templateRow, 0, blackIndex, area.getTitleWithType(), value[ALL_IDX], value[ONLINE_IDX], value[ORIG_IDX], totalSum, area.getId(), area2percentMap, false));
                    }
                }
                // создаем строки с иностранцами
                if (foreignCountry[ALL_IDX] != 0)
                    newRowList.add(createRow(templateRow, 0, blackIndex, "Другие государства", foreignCountry[ALL_IDX], foreignCountry[ONLINE_IDX], foreignCountry[ORIG_IDX], totalSum, null, null, false));
                table.getRowList().addAll(currentRowIndex + 1, newRowList);
            }
        });

        Collections.sort(testList);
        injectModifier.put("test", testList.toString());
        injectModifier.modify(document);
        tableModifier.modify(document);

        if (_model.getDetail().isTrue())
        {
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            clone.getElementList().remove(1);
            clone.getElementList().remove(1);
            clone.setHeader(document.getHeader());

            for (AddressItem area : areaList)
            {
                final Map<AddressItem, int[]> map = area2city2count.get(area);

                if (map != null)
                {
                    int[] subSum = new int[3];
                    for (int[] value : map.values())
                    {
                        subSum[ALL_IDX] += value[ALL_IDX];
                        subSum[ONLINE_IDX] += value[ONLINE_IDX];
                        subSum[ORIG_IDX] += value[ORIG_IDX];
                    }

                    final List<AddressItem> cityList = new ArrayList<>(map.keySet());

                    RtfDocument doc = clone.getClone();
                    doc.setHeader(clone.getHeader());

                    RtfInjectModifier im = new RtfInjectModifier();
                    im.put("vuz", area.getTitleWithType() + " (детализация по населенным пунктам)");
                    im.put("S1", Integer.toString(subSum[ALL_IDX]));
                    im.put("S2", Integer.toString(subSum[ONLINE_IDX]));
                    im.put("S3", Integer.toString(subSum[ORIG_IDX]));
                    im.put("S4", area2percentMap.get(area.getId()));

                    RtfTableModifier tm = new RtfTableModifier();
                    tm.put("T", new String[0][]);
                    tm.put("T", new RtfRowIntercepterBase()
                    {
                        @Override
                        public void beforeModify(RtfTable table, int currentRowIndex)
                        {
                            RtfString string = new RtfString();
                            if (_model.getReport().getCompensationType() != null) {
                                if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_model.getReport().getCompensationType().getCode()))
                                    string.append("Подали (бюд.)");
                                else
                                    string.append("Подали (дог.)");
                            } else {
                                string.append("Подали (всего)");
                            }
                            RtfCell cell = table.getRowList().get(currentRowIndex - 1).getCellList().get(1);
                            cell.setElementList(string.toList());
                            SharedRtfUtil.setCellAlignment(cell, IRtfData.B);

                            RtfRow templateRow = table.getRowList().get(currentRowIndex);
                            List<RtfRow> newRowList = new ArrayList<>();
                            // создаем строки с областями РФ
                            for (AddressItem city : cityList)
                            {
                                int[] value = map.get(city);
                                newRowList.add(createRow(templateRow, 0, blackIndex, city.getTitleWithType(), value[ALL_IDX], value[ONLINE_IDX], value[ORIG_IDX], totalSum, null, null, false));
                            }
                            table.getRowList().addAll(currentRowIndex + 1, newRowList);
                        }
                    });

                    im.modify(doc);
                    tm.modify(doc);

                    document.getElementList().add(doc);
                }
            }
        }

        return RtfUtil.toByteArray(document);
    }

    private int[] fillMapValue(int[] mapValue, boolean isOrig, boolean isOnline, int count) {
        mapValue[ALL_IDX] += count;
        if (isOrig)
            mapValue[ORIG_IDX] += 1;
        if (isOnline)
            mapValue[ONLINE_IDX] += 1;
        return mapValue;
    }

    private static RtfRow createRow(RtfRow templateRow, int indentValue, int colorIndex, String title, int all, int online, int orig, int totalSum, Long areaId, Map<Long, String> area2persentMap, boolean bold)
    {
        IRtfElement left = RtfBean.getElementFactory().createRtfControl(IRtfData.QL);
        IRtfElement center = RtfBean.getElementFactory().createRtfControl(IRtfData.QC);
        IRtfControl color = RtfBean.getElementFactory().createRtfControl(IRtfData.CF, colorIndex);
        IRtfControl indent = RtfBean.getElementFactory().createRtfControl(IRtfData.LI, indentValue);
        IRtfControl boldFont;
        if (bold)
        {boldFont = RtfBean.getElementFactory().createRtfControl(IRtfData.B);}
        else
        {boldFont = RtfBean.getElementFactory().createRtfControl(IRtfData.B, 0);}

        IRtfElement titleText = RtfBean.getElementFactory().createRtfText(title);
        IRtfElement allText = RtfBean.getElementFactory().createRtfText(Integer.toString(all));
        IRtfElement onlineText = RtfBean.getElementFactory().createRtfText(Integer.toString(online));
        IRtfElement origText = RtfBean.getElementFactory().createRtfText(Integer.toString(orig));

        String percent = new DoubleFormatter(1, true).format((double) (all) * 100.0 / (double) totalSum);
        if (area2persentMap != null)
            area2persentMap.put(areaId, percent);
        IRtfElement percentText = RtfBean.getElementFactory().createRtfText(percent);

        RtfRow row = templateRow.getClone();
        List<RtfCell> list = row.getCellList();

        list.get(0).setElementList(Arrays.asList(color, boldFont, left, indent, titleText));
        list.get(1).setElementList(Arrays.asList(color, boldFont, center, allText));
        list.get(2).setElementList(Arrays.asList(color, boldFont, onlineText));
        list.get(3).setElementList(Arrays.asList(color, boldFont, origText));
        list.get(4).setElementList(Arrays.asList(color, boldFont, percentText));
        return row;
    }

    private List<Object[]> getData()
    {
        DQLSelectBuilder builder;
        switch (_model.getEnrollmentCampaignStage().getId().intValue())
        {
            case Model.ENROLLMENT_CAMP_STAGE_DOCUMENTS:
                builder = getRequestedBuilder();
                if (_model.isIncludeHighMarks()) {
                    builder.group(property("entrant"));
                } else {
                    builder.group(property("request"));
                }
                break;
            case Model.ENROLLMENT_CAMP_STAGE_EXAMS:
                builder = getRequestedBuilder();
                builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_STATE, "state");
                builder.where(DQLExpressions.notIn(DQLExpressions.property("state." + EntrantState.P_CODE), Arrays.asList(
                        UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE,
                        UniecDefines.ENTRANT_STATE_ACTIVE_CODE
                )));
                if (_model.isIncludeHighMarks()) {
                    builder.group(DQLExpressions.property("entrant"));
                }
                else {
                    builder.group(DQLExpressions.property("request"));
                }
                break;
            case Model.ENROLLMENT_CAMP_STAGE_ENROLLMENT:
                builder = getPreliminaryBuilder();
                builder.group(DQLExpressions.property("p.id"));
                break;
            default:
                throw new RuntimeException("Unknown report stage!");
        }

        if (!_model.isIncludeHighMarks())
            builder.group(property("request.id"));
        builder.group(property("entrant.id"));
        builder.group(property("settlement.id"));

        if (!_model.isIncludeHighMarks())
            builder.column(DQLExpressions.property("request.id"), "requestId");
        builder.column(DQLExpressions.property("entrant.id"), "entrantId");
        builder.column(DQLExpressions.property("settlement.id"), "settlementId");

        DQLSelectBuilder outerBuilder = new DQLSelectBuilder();
        if (!_model.isIncludeHighMarks())
            outerBuilder.column(DQLExpressions.property("t.requestId"), "requestId_alias");
        outerBuilder.column(DQLExpressions.property("t.entrantId"), "entrantId_alias");
        outerBuilder.column(DQLExpressions.property("t.settlementId"), "settlementId_alias");
        outerBuilder.column(DQLFunctions.countStar(), "count");
        outerBuilder.fromDataSource(builder.buildQuery(), "t");
        if (!_model.isIncludeHighMarks())
            outerBuilder.group(property("t.requestId"));
        outerBuilder.group(property("t.entrantId"));
        outerBuilder.group(property("t.settlementId"));

        List<Object[]> allList;
        allList = outerBuilder.createStatement(new DQLExecutionContext(_session)).list();
        return allList;
    }

    private DQLSelectBuilder getRequestedBuilder()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(RequestedEnrollmentDirection.class, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        builder.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");

        if (_model.isCompensationTypeActive())
            builder.where(DQLExpressions.eq(DQLExpressions.property("d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE), DQLExpressions.value(_model.getReport().getCompensationType())));

        patchEduOrgUnit(builder, "d." + RequestedEnrollmentDirection.L_STUDENT_CATEGORY);

        return builder;
    }

    private DQLSelectBuilder getPreliminaryBuilder()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(PreliminaryEnrollmentStudent.class, "p");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        builder.joinPath(DQLJoinType.inner, "d." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
        if (_model.isCompensationTypeActive())
            builder.where(DQLExpressions.eq(DQLExpressions.property("d." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE), DQLExpressions.value(_model.getReport().getCompensationType())));
        if (_model.isParallelActive() && _model.getParallel().isTrue())
            builder.where(DQLExpressions.eq(DQLExpressions.property("p." + PreliminaryEnrollmentStudent.P_PARALLEL), DQLExpressions.value(Boolean.FALSE)));

        builder.where(DQLExpressions.ne(DQLExpressions.property("d." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));

        patchEduOrgUnit(builder, "p." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY);

        return builder;
    }

    private void patchEduOrgUnit(DQLSelectBuilder builder, String studentCategoryAlias)
    {
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person");
        if (_model.getAdressReg().equals(_model.getSourceFilter()))
        {
            builder.joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "identityCard");
            builder.joinEntity("identityCard", DQLJoinType.inner, AddressDetailed.class, "address", eq(property(IdentityCard.address().id().fromAlias("identityCard")), property(AddressDetailed.id().fromAlias("address"))));
        } else
        {
            builder.joinEntity("person", DQLJoinType.inner, AddressDetailed.class, "address", eq(property(Person.address().id().fromAlias("person")), property(AddressDetailed.id().fromAlias("address"))));
        }
        builder.joinEntity("address", DQLJoinType.left, AddressRu.class, "addressRu", eq(property(AddressBase.id().fromAlias("address")), property(AddressRu.id().fromAlias("addressRu"))));
        builder.joinPath(DQLJoinType.inner, "address." + AddressDetailed.L_SETTLEMENT, "settlement");
        builder.joinPath(DQLJoinType.left, "addressRu." + AddressRu.L_DISTRICT, "district");
        builder.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        builder.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");
        builder.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.P_ARCHIVAL), DQLExpressions.value(Boolean.FALSE)));
        builder.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), DQLExpressions.value(_model.getReport().getEnrollmentCampaign())));
        builder.where(DQLExpressions.betweenDays("request." + EntrantRequest.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));

        if (_model.isStudentCategoryActive())
            builder.where(DQLExpressions.in(DQLExpressions.property(studentCategoryAlias), _model.getStudentCategoryList()));
        if (_model.isQualificationActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("el." + EducationLevels.L_QUALIFICATION), _model.getQualificationList()));
        if (_model.isFormativeOrgUnitActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT), _model.getFormativeOrgUnitList()));
        if (_model.isTerritorialOrgUnitActive())
        {
            if (_model.getTerritorialOrgUnitList().isEmpty())
                builder.where(DQLExpressions.isNull(DQLExpressions.property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)));
            else
                builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT), _model.getTerritorialOrgUnitList()));
        }
        if (_model.isEducationLevelHighSchoolActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL), _model.getEducationLevelHighSchoolList()));
        if (_model.isDevelopFormActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_FORM), _model.getDevelopFormList()));
        if (_model.isDevelopConditionActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), _model.getDevelopConditionList()));
        if (_model.isDevelopTechActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_TECH), _model.getDevelopTechList()));
        if (_model.isDevelopPeriodActive())
            builder.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_PERIOD), _model.getDevelopPeriodList()));
        if (!_model.isIncludeForeignPerson())
            builder.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.person().identityCard().citizenship().code()), DQLExpressions.value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
        if (_model.isIncludeHighMarks()) {
            List<IDQLExpression> dqlDiscName = Lists.newArrayList();
            for (String name : PROFILE_DISC) {
                dqlDiscName.add(likeUpper(property("disc", ChosenEntranceDiscipline.enrollmentCampaignDiscipline().educationSubject().title()), value("%" + name.toUpperCase() + "%")));
            }
            IDQLExpression[] orDiscName = dqlDiscName.toArray(new IDQLExpression[dqlDiscName.size()]);

            DQLSelectBuilder discSubQuery = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "disc");
            discSubQuery.column(property("disc", ChosenEntranceDiscipline.chosenEnrollmentDirection().id()));
            discSubQuery.where(ge(property("disc", ChosenEntranceDiscipline.finalMark()), value(FefuReportUtil.PROFILE_MAX_MARK)));
            discSubQuery.where(or(orDiscName));

            DQLSelectBuilder competitionSubQuery = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "d2");
            competitionSubQuery.column(property("d2", RequestedEnrollmentDirection.id()));
            competitionSubQuery.group(property("d2", RequestedEnrollmentDirection.id()));
            competitionSubQuery.where(eq(property("d2", RequestedEnrollmentDirection.competitionKind().code()), value(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES)));
            builder.joinEntity("d", DQLJoinType.left, ChosenEntranceDiscipline.class, "ch",
                    eq(
                            property(RequestedEnrollmentDirection.id().fromAlias("d")),
                            property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ch"))
                    ));
            builder.group(property("d", RequestedEnrollmentDirection.id()));
            builder.having(
                    or(
                            // либо набрали >= 200 баллов
                            ge(DQLFunctions.sum(property("ch", ChosenEntranceDiscipline.finalMark())), value(FefuReportUtil.MAX_MARK)),
                            // либо без экзаменов прошли
                            in(property("d", RequestedEnrollmentDirection.id()), competitionSubQuery.buildQuery()),
                            // либо по какой-либо естественнонаучной дисциплине >= 70 баллов
                            in(property("d", RequestedEnrollmentDirection.id()), discSubQuery.buildQuery())
                    )
            );
            String tt = "";
        }
    }

    private List<Long> getOnlineEntrant() {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(EntrantFefuExt.class, "entrantFefu");
        builder.column(property("entrantFefu", EntrantFefuExt.entrant().id()));
        builder.where(DQLExpressions.eq(DQLExpressions.property("entrantFefu." + EntrantFefuExt.P_IS_ONLINE_ENTRANT), DQLExpressions.value(Boolean.TRUE)));
        return builder.createStatement(_session).list();
    }

    private boolean isRequestIdsWithOrig(Long requestId) {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", RequestedEnrollmentDirection.entrantRequest().id(), requestId));
        builder.add(MQExpression.eq("e", RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN, Boolean.TRUE));
        return builder.getResultList(_session).size() != 0;
    }

    public boolean isEntrantWithOriginal(EnrollmentCampaign campaign, Long entrantId) {
        DQLSelectBuilder directionBuilder = new DQLSelectBuilder();
        directionBuilder.fromEntity(EntrantOriginalDocumentRelation.class, "orig");
        directionBuilder.where(DQLExpressions.eq(DQLExpressions.property(EntrantOriginalDocumentRelation.requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign().fromAlias("orig")), DQLExpressions.value(campaign)));
        directionBuilder.where(DQLExpressions.eq(DQLExpressions.property(EntrantOriginalDocumentRelation.entrant().id().fromAlias("orig")), DQLExpressions.value(entrantId)));
        return directionBuilder.createStatement(_session).list().size() > 0;
    }

    private Map<AddressItem, FefuFederalDistrict> getFefuFederalDistrictMap() {
        Map<AddressItem, FefuFederalDistrict> federalAreasMap = new HashMap<>();
        DQLSelectBuilder directionBuilder = new DQLSelectBuilder();
        directionBuilder.fromEntity(FefuFederalAreasRel.class, "rel");
        List<FefuFederalAreasRel> federalAreasRels = directionBuilder.createStatement(_session).list();
        if (federalAreasRels.size() > 0) {
            for (FefuFederalAreasRel rel : federalAreasRels) {
                federalAreasMap.put(rel.getAddressItem(), rel.getFefuFederalDistrict());
            }
        }
        return federalAreasMap;
    }

    private class ValuesComparator implements Comparator<AddressItem>
    {
        private Map<AddressItem, int[]> _addressMap;

        public ValuesComparator(Map<AddressItem, int[]> addressMap)
        {
            _addressMap = addressMap;
        }

        @Override
        public int compare(AddressItem o1, AddressItem o2)
        {
            Integer oo1 = _addressMap.get(o1)[ALL_IDX];
            Integer oo2 = _addressMap.get(o2)[ALL_IDX];
            return oo2.compareTo(oo1);
        }
    }

    private class ValuesComparatorFederal implements Comparator<FefuFederalDistrict>
    {
        private Map<FefuFederalDistrict, int[]> _addressMap;

        public ValuesComparatorFederal(Map<FefuFederalDistrict, int[]> addressMap)
        {
            _addressMap = addressMap;
        }

        @Override
        public int compare(FefuFederalDistrict o1, FefuFederalDistrict o2)        {
            Integer oo1 = _addressMap.get(o1)[ALL_IDX];
            Integer oo2 = _addressMap.get(o2)[ALL_IDX];
            return oo2.compareTo(oo1);
        }
    }

}
