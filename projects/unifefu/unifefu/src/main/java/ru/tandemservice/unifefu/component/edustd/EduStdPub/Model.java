/* $Id$ */
package ru.tandemservice.unifefu.component.edustd.EduStdPub;

import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt;

/**
 * @author Ekaterina Zvereva
 * @since 29.05.2015
 */
public class Model extends ru.tandemservice.uniepp.component.edustd.EduStdPub.Model
{

    private FefuEppStateEduStandardExt _eduStandardExt;

    public FefuEppStateEduStandardExt getEduStandardExt()
    {
        return _eduStandardExt;
    }

    public void setEduStandardExt(FefuEppStateEduStandardExt eduStandardExt)
    {
        _eduStandardExt = eduStandardExt;
    }
}
