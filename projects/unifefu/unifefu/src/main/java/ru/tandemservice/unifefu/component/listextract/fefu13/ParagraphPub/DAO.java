package ru.tandemservice.unifefu.component.listextract.fefu13.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public class DAO extends AbstractListParagraphPubDAO<FefuAdditionalAcademGrantStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
    }
}
