package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x7_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.7"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.7")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEpvCheckState

		// создано свойство acceptedDate
		{
			// создать колонку
			tool.createColumn("fefuepvcheckstate_t", new DBColumn("accepteddate_p", DBType.TIMESTAMP));

		}

		// создано свойство acceptedBy
		{
			// создать колонку
			tool.createColumn("fefuepvcheckstate_t", new DBColumn("acceptedby_p", DBType.createVarchar(255)));

		}

		// создано свойство rejectedComment
		{
			// создать колонку
			tool.createColumn("fefuepvcheckstate_t", new DBColumn("rejectedcomment_p", DBType.createVarchar(255)));

		}

        // создано свойство eduPlanAcceptedDate
        {
            // создать колонку
            tool.createColumn("fefuepvcheckstate_t", new DBColumn("eduplanaccepteddate_p", DBType.TIMESTAMP));

        }

        // создано свойство eduPlanAcceptedBy
        {
            // создать колонку
            tool.createColumn("fefuepvcheckstate_t", new DBColumn("eduplanacceptedby_p", DBType.createVarchar(255)));

        }

    }
}