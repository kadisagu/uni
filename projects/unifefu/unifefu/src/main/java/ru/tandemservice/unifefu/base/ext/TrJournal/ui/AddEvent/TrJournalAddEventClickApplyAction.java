package ru.tandemservice.unifefu.base.ext.TrJournal.ui.AddEvent;

import org.hibernate.Session;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.AddEvent.TrJournalAddEventUI;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.IBrsCoefficientRowWrapper;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBaseUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * User: amakarova
 * Date: 18.12.13
 */

public class TrJournalAddEventClickApplyAction extends NamedUIAction
{
    public static final String MAX_MIN_MESSAGE_ERROR = "Максимальный балл не может быть меньше минимального.";

    protected TrJournalAddEventClickApplyAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof TrJournalAddEventUI)
        {
            Map<String, Object> values = new HashMap<>();
            TrJournalAddEventUI ui = (TrJournalAddEventUI) presenter;
            TrBrsCoefficientEditBaseUI brsCoefEditComponent = (TrBrsCoefficientEditBaseUI) ui.getSupport().getChildrenUI().entrySet().iterator().next().getValue();
            for (IBrsCoefficientRowWrapper row : brsCoefEditComponent.getRowList())
            {
                if (row.getDef().getUserCode().equals(FefuBrs.MAX_POINTS_CODE) || row.getDef().getUserCode().equals(FefuBrs.MIN_POINTS_CODE))
                    values.put(row.getDef().getUserCode(), row.getValue());
            }
            if (!values.isEmpty())
            {
                Double max = 0.0d;
                Double min = 0.0d;
                if (values.get(FefuBrs.MAX_POINTS_CODE) instanceof Double)
                    max = (Double) values.get(FefuBrs.MAX_POINTS_CODE);
                else if (values.get(FefuBrs.MAX_POINTS_CODE) instanceof Long)
                    max = ((Long) values.get(FefuBrs.MAX_POINTS_CODE)).doubleValue();
                if (values.get(FefuBrs.MIN_POINTS_CODE) instanceof Double)
                    min = (Double) values.get(FefuBrs.MIN_POINTS_CODE);
                else if (values.get(FefuBrs.MIN_POINTS_CODE) instanceof Long)
                    min = ((Long) values.get(FefuBrs.MIN_POINTS_CODE)).doubleValue();
                if (max < min)
                    throw new ApplicationException(MAX_MIN_MESSAGE_ERROR);
            }
            onSave(ui);
        }
    }

    public void onSave(final TrJournalAddEventUI presenter)
    {
        DataAccessServices.dao().doInTransaction(session -> {
            TrJournalEvent newEvent = (TrJournalEvent) presenter.getOwnerHolder().getValue();
            TrJournalManager.instance().eventDao().saveNewEvent(newEvent, presenter.getEvent().getJournalModule(), presenter.getGroup());

            TrBrsCoefficientEditBaseUI brsCoeffEditComponent = (TrBrsCoefficientEditBaseUI) presenter.getSupport().getChildrenUI().entrySet().iterator().next().getValue();
            brsCoeffEditComponent.save();

            TrEduGroupEvent groupEvent = getTrEduGroupEvent(newEvent, session);
            IUIAddon uiAddon = presenter.getConfig().getAddon(TrJournalAddEventExt.ADDON_NAME);
            if (groupEvent == null)
            {
                EppRealEduGroup group = ((TrJournalAddEventExtUI) uiAddon).getEppRealEduGroup4LoadType();
                if (group != null)
                {
                    groupEvent = new TrEduGroupEvent(group, newEvent);
                    session.save(groupEvent);
                }
            }
            if (groupEvent != null)
                doSaveScheduleDates(presenter, groupEvent);
            return newEvent;
        });
        presenter.deactivate();
    }

    /**
     * Сохранение дат расписания
     */
    private void doSaveScheduleDates(IUIPresenter presenter, TrEduGroupEvent event)
    {
        IUIAddon uiAddon = presenter.getConfig().getAddon(TrJournalAddEventExt.ADDON_NAME);
        if (null != uiAddon)
        {
            if (uiAddon instanceof TrJournalAddEventExtUI)
            {
                TrJournalAddEventExtUI addon = (TrJournalAddEventExtUI) uiAddon;
                ScheduleEvent scheduleEvent = addon.getSchEvent();
                if (scheduleEvent != null)
                {
                    scheduleEvent.setDurationBegin(addon.getDate());
                    scheduleEvent.setDurationEnd(addon.getDurationEnd());
                    TrJournalManager.instance().eventDao().saveOrUpdateScheduleEvent(event, addon.getSchEvent(), new ArrayList<>());
                }
            }
        }
    }

    private TrEduGroupEvent getTrEduGroupEvent(TrJournalEvent event, Session session)
    {
        DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ge").column("ge").top(1)
                .where(DQLExpressions.eq(property(TrEduGroupEvent.journalEvent().fromAlias("ge")), value(event)));
        return query.createStatement(session).uniqueResult();
    }

//    private String checkDate(TrJournalAddEventExtUI addon)
//    {
//        final Date moduleBeginDate = addon.getGroupEvent().getJournalEvent().getJournalModule().getModuleBeginDate();
//        boolean startError = null != moduleBeginDate && CoreDateUtils.getDayFirstTimeMoment(moduleBeginDate).after(addon.getSchEvent().getDurationBegin());
//        final Date moduleEndDate = addon.getGroupEvent().getJournalEvent().getJournalModule().getModuleEndDate();
//        boolean endError = null != moduleEndDate && CoreDateUtils.getDayLastTimeMoment(moduleEndDate).before(addon.getSchEvent().getDurationEnd());
//        if (startError && endError)
//            return "Вы ввели дату начала события до даты начала обучения по модулю, и дату окончания события после окончания обучения по модулю. Сохранить событие с этими датами?";
//        else if (startError)
//            return "Вы ввели дату начала события до даты начала обучения по модулю. Сохранить событие с такой датой начала?";
//        else if (endError)
//            return "Вы ввели дату окончания события после окончания обучения по модулю. Сохранить событие с такой датой окончания?";
//        return null;
//    }
}

