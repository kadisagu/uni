/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu9;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class FefuFormativeTransferStuListExtractDao extends UniBaseDao implements IExtractComponentDao<FefuFormativeTransferStuListExtract>
{
    @Override
    public void doCommit(FefuFormativeTransferStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setGroupOld(extract.getGroupOld());
        extract.getEntity().setGroup(extract.getGroupNew());

        extract.setCompensationTypeOld(extract.getEntity().getCompensationType());
        extract.getEntity().setCompensationType(extract.getCompensationTypeNew());

        extract.setEducationOrgUnitOld(extract.getEntity().getEducationOrgUnit());
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitNew());
    }

    @Override
    public void doRollback(FefuFormativeTransferStuListExtract extract, Map parameters)
    {
        Student student = extract.getEntity();
        student.setGroup(extract.getGroupOld());
        student.setCompensationType(extract.getCompensationTypeOld());
        student.setEducationOrgUnit(extract.getEducationOrgUnitOld());
    }
}