/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e76;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.TransferProfileGroupExtStuExtract;

/**
 * @author Alexey Lopatin
 * @since 13.11.2013
 */
public class TransferProfileGroupExtStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e76.TransferProfileGroupExtStuExtractPrint
{
    @Override
    public void additionalModify(RtfInjectModifier modifier, TransferProfileGroupExtStuExtract extract)
    {
        if (!extract.getCompensationTypeOld().equals(extract.getCompensationTypeNew()))
            modifier.put("fefuCompensationTypeStrNew", CommonExtractPrint.getFefuCompTypeStr(extract.getCompensationTypeNew().isBudget(), true));
        else
            modifier.put("fefuCompensationTypeStrNew", "");
    }
}
