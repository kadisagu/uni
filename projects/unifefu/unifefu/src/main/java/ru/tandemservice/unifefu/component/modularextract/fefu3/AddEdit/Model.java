/* $Id: Model.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.modularextract.fefu3.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unifefu.entity.FefuReEducationStuExtract;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 22.08.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuReEducationStuExtract>
{
    private CommonExtractModel _eduModel;
    private List<Course> _prevCourseList;
    private ISelectModel _educationYearsList;

    public CommonExtractModel getEduModel()
    {
        return _eduModel;
    }

    public void setEduModel(CommonExtractModel eduModel)
    {
        _eduModel = eduModel;
    }

    public List<Course> getPrevCourseList()
    {
        return _prevCourseList;
    }

    public void setPrevCourseList(List<Course> prevCourseList)
    {
        _prevCourseList = prevCourseList;
    }

    public ISelectModel getEducationYearsList()
    {
        return _educationYearsList;
    }

    public void setEducationYearsList(ISelectModel educationYearsList)
    {
        _educationYearsList = educationYearsList;
    }
}