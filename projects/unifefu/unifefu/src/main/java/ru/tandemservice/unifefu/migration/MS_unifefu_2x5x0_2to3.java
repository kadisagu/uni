package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.5.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonAdditionalData

		// создано обязательное свойство personFefuExt
		{
			// создать колонку
			tool.createColumn("mdbviewpersonadditionaldata_t", new DBColumn("personfefuext_id", DBType.LONG));

			tool.executeUpdate("delete from mdbviewpersonadditionaldata_t");

			// сделать колонку NOT NULL
			tool.setColumnNullable("mdbviewpersonadditionaldata_t", "personfefuext_id", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonBenefit

		// создано обязательное свойство personBenefit
		{
			// создать колонку
			tool.createColumn("mdbviewpersonbenefit_t", new DBColumn("personbenefit_id", DBType.LONG));

            tool.executeUpdate("delete from mdbviewpersonbenefit_t");

			// сделать колонку NOT NULL
			tool.setColumnNullable("mdbviewpersonbenefit_t", "personbenefit_id", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonEduinstitution

		// создано обязательное свойство personEduInstitution
		{
			// создать колонку
			tool.createColumn("mdbviewpersoneduinstitution_t", new DBColumn("personeduinstitution_id", DBType.LONG));

            tool.executeUpdate("delete from mdbviewpersoneduinstitution_t");

			// сделать колонку NOT NULL
			tool.setColumnNullable("mdbviewpersoneduinstitution_t", "personeduinstitution_id", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonForeignlanguage

		// создано обязательное свойство personForeignLanguage
		{
			// создать колонку
			tool.createColumn("mdbviewpersonforeignlanguage_t", new DBColumn("personforeignlanguage_id", DBType.LONG));

            tool.executeUpdate("delete from mdbviewpersonforeignlanguage_t");

			// сделать колонку NOT NULL
			tool.setColumnNullable("mdbviewpersonforeignlanguage_t", "personforeignlanguage_id", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonNextofkin

		// создано обязательное свойство personNextOfKin
		{
			// создать колонку
			tool.createColumn("mdbviewpersonnextofkin_t", new DBColumn("personnextofkin_id", DBType.LONG));

            tool.executeUpdate("delete from mdbviewpersonnextofkin_t");

			// сделать колонку NOT NULL
			tool.setColumnNullable("mdbviewpersonnextofkin_t", "personnextofkin_id", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPersonSportachievement

		// создано обязательное свойство personSportAchievement
		{
			// создать колонку
			tool.createColumn("mdbvwprsnsprtchvmnt_t", new DBColumn("personsportachievement_id", DBType.LONG));

            tool.executeUpdate("delete from mdbvwprsnsprtchvmnt_t");

			// сделать колонку NOT NULL
			tool.setColumnNullable("mdbvwprsnsprtchvmnt_t", "personsportachievement_id", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewStudentAdditionalData

		// создано свойство studentFefuExt
		{
			// создать колонку
			tool.createColumn("mdbviewstudentadditionaldata_t", new DBColumn("studentfefuext_id", DBType.LONG));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewStudentCustomState

		// создано обязательное свойство studentCustomState
		{
			// создать колонку
			tool.createColumn("mdbviewstudentcustomstate_t", new DBColumn("studentcustomstate_id", DBType.LONG));

            tool.executeUpdate("delete from mdbviewstudentcustomstate_t");

			// сделать колонку NOT NULL
			tool.setColumnNullable("mdbviewstudentcustomstate_t", "studentcustomstate_id", false);

		}


    }
}