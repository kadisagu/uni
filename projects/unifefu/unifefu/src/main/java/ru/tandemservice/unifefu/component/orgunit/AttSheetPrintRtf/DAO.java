/*$Id$*/
package ru.tandemservice.unifefu.component.orgunit.AttSheetPrintRtf;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTabPrintRtf.Model;
import ru.tandemservice.unifefu.entity.SessionAttSheetOperation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 11.07.2014
 */
public class DAO extends ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTabPrintRtf.DAO implements IDAO
{

    @Override
    public String[][] getSessionTransferDocument(Model model)
    {
        String[] courses = {"Первый курс", "Второй курс", "Третий курс", "Четвертый курс", "Пятый курс", "Шестой курс"};

        Integer courseLast = model.getSessionTransferDocument().getTargetStudent().getEducationOrgUnit().getDevelopPeriod().getLastCourse();
        List<SessionAttSheetOperation> sessionTransferOperationList = new DQLSelectBuilder().fromEntity(SessionAttSheetOperation.class, "c")
                .where(eq(property("c", SessionAttSheetOperation.slot().document()), value(model.getSessionTransferDocument())))
                .createStatement(getSession()).list();
        List<Integer> caption = new ArrayList<>();
        String[][] sessionTransferOperationData = new String[courseLast + sessionTransferOperationList.size()][];
        int i = 0;
        for (int g = 0; g < courseLast; g++)
        {
            caption.add(i);
            sessionTransferOperationData[i++] = new String[]{"", courses[g], "", "", "", "", ""};

            List<SessionAttSheetOperation> courseOperationList = getCourseOperationList(sessionTransferOperationList, g + 1);

            int countDisc = 0;
            for (SessionAttSheetOperation sessionTransferOperation : courseOperationList)
            {
                sessionTransferOperationData[i++] = getRow(countDisc, sessionTransferOperation);
                model.setCaptainRowList(caption);
                countDisc++;
            }
            if (g == courseLast - 1)
            {
                countDisc = 0;
                for (SessionAttSheetOperation sessionTransferOperation : sessionTransferOperationList)
                {
                    sessionTransferOperationData[i++] = getRow(countDisc, sessionTransferOperation);
                    model.setCaptainRowList(caption);
                    countDisc++;
                }
            }
        }
        return sessionTransferOperationData;
    }

    List<SessionAttSheetOperation> getCourseOperationList(Collection<SessionAttSheetOperation> sourceList, int course)
    {
        List<SessionAttSheetOperation> courseList = new ArrayList<>();
        for (SessionAttSheetOperation item : sourceList)
        {
            if (item.getSlot().getStudent() != null && item.getSlot().getStudent().getStudentWpe().getCourse().getIntValue() == course)
            {
                courseList.add(item);
            }
        }
        sourceList.removeAll(courseList);
        return courseList;
    }

    private String[] getRow(int countDisc, SessionAttSheetOperation sessionTransferOperation)
    {
        String number = Integer.toString(countDisc + 1);
        String disc = sessionTransferOperation.getSlot().getStudent() == null ? "" : sessionTransferOperation.getSlot().getStudent().getStudentWpe().getSourceRow().getTitle();
        String workUch = sessionTransferOperation.getSlot().getStudent() == null ? "" : sessionTransferOperation.getSlot().getStudent().getStudentWpe().getRegistryElementPart().getSizeAsDouble().toString();
        String registryElementTitleUch = StringUtils.uncapitalize(sessionTransferOperation.getSlot().getStudent() == null ? "" : sessionTransferOperation.getSlot().getStudent().getType().getTitle());
        String workMarkD = sessionTransferOperation.getMark() == null ? "-" : sessionTransferOperation.getMark();
        String comment = sessionTransferOperation.getComment();
        String workTimeDisc = sessionTransferOperation.getWorkTimeDisc() == null ? "" : sessionTransferOperation.getWorkTimeDisc().toString();
//                          1       2     3        4                        5             6          7
        return new String[]{number, disc, workUch, registryElementTitleUch, workTimeDisc, workMarkD, comment};
    }
}
