/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.ext.PersonShell;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.person.base.bo.PersonShell.PersonShellManager;

/**
 * @author Alexander Zhebko
 * @since 06.06.2013
 */
@Configuration
public class PersonShellExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private PersonShellManager _personManager;

    @Bean
    public ItemListExtension<String> cssListExtension()
    {
        return itemListExtension(_personManager.cssListExtPoint())
                .add("unifefu-rich-table-list", "css/fefu-rich-table-list.css")
                .create();
    }
}