/**
 * SubmitFileService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportalmass;

public interface SubmitFileService extends javax.xml.rpc.Service {
    public java.lang.String getSubmitFileSOAP11PortAddress();

    public ru.tandemservice.unifefu.ws.rateportalmass.SubmitFile getSubmitFileSOAP11Port() throws javax.xml.rpc.ServiceException;

    public ru.tandemservice.unifefu.ws.rateportalmass.SubmitFile getSubmitFileSOAP11Port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
