package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unifefu.entity.PersonFefuExt;
import ru.tandemservice.unifefu.entity.ws.MdbViewPerson;
import ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительные данные по персоне
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewPersonAdditionalDataGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData";
    public static final String ENTITY_NAME = "mdbViewPersonAdditionalData";
    public static final int VERSION_HASH = -872411765;
    private static IEntityMeta ENTITY_META;

    public static final String L_MDB_VIEW_PERSON = "mdbViewPerson";
    public static final String L_PERSON_FEFU_EXT = "personFefuExt";
    public static final String P_VIP_DATE = "vipDate";
    public static final String P_COMMENT_VIP = "commentVip";
    public static final String P_FIO_CURATOR_VIP = "fioCuratorVip";

    private MdbViewPerson _mdbViewPerson;     // Персона
    private PersonFefuExt _personFefuExt;     // Персона (расширение ДВФУ)
    private Date _vipDate;     // Признак vip
    private String _commentVip;     // Комментарий для vip
    private String _fioCuratorVip;     // ФИО куратора vip

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона. Свойство не может быть null.
     */
    @NotNull
    public MdbViewPerson getMdbViewPerson()
    {
        return _mdbViewPerson;
    }

    /**
     * @param mdbViewPerson Персона. Свойство не может быть null.
     */
    public void setMdbViewPerson(MdbViewPerson mdbViewPerson)
    {
        dirty(_mdbViewPerson, mdbViewPerson);
        _mdbViewPerson = mdbViewPerson;
    }

    /**
     * @return Персона (расширение ДВФУ). Свойство не может быть null.
     */
    @NotNull
    public PersonFefuExt getPersonFefuExt()
    {
        return _personFefuExt;
    }

    /**
     * @param personFefuExt Персона (расширение ДВФУ). Свойство не может быть null.
     */
    public void setPersonFefuExt(PersonFefuExt personFefuExt)
    {
        dirty(_personFefuExt, personFefuExt);
        _personFefuExt = personFefuExt;
    }

    /**
     * @return Признак vip.
     */
    public Date getVipDate()
    {
        return _vipDate;
    }

    /**
     * @param vipDate Признак vip.
     */
    public void setVipDate(Date vipDate)
    {
        dirty(_vipDate, vipDate);
        _vipDate = vipDate;
    }

    /**
     * @return Комментарий для vip.
     */
    @Length(max=255)
    public String getCommentVip()
    {
        return _commentVip;
    }

    /**
     * @param commentVip Комментарий для vip.
     */
    public void setCommentVip(String commentVip)
    {
        dirty(_commentVip, commentVip);
        _commentVip = commentVip;
    }

    /**
     * @return ФИО куратора vip.
     */
    @Length(max=255)
    public String getFioCuratorVip()
    {
        return _fioCuratorVip;
    }

    /**
     * @param fioCuratorVip ФИО куратора vip.
     */
    public void setFioCuratorVip(String fioCuratorVip)
    {
        dirty(_fioCuratorVip, fioCuratorVip);
        _fioCuratorVip = fioCuratorVip;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewPersonAdditionalDataGen)
        {
            setMdbViewPerson(((MdbViewPersonAdditionalData)another).getMdbViewPerson());
            setPersonFefuExt(((MdbViewPersonAdditionalData)another).getPersonFefuExt());
            setVipDate(((MdbViewPersonAdditionalData)another).getVipDate());
            setCommentVip(((MdbViewPersonAdditionalData)another).getCommentVip());
            setFioCuratorVip(((MdbViewPersonAdditionalData)another).getFioCuratorVip());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewPersonAdditionalDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewPersonAdditionalData.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewPersonAdditionalData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mdbViewPerson":
                    return obj.getMdbViewPerson();
                case "personFefuExt":
                    return obj.getPersonFefuExt();
                case "vipDate":
                    return obj.getVipDate();
                case "commentVip":
                    return obj.getCommentVip();
                case "fioCuratorVip":
                    return obj.getFioCuratorVip();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mdbViewPerson":
                    obj.setMdbViewPerson((MdbViewPerson) value);
                    return;
                case "personFefuExt":
                    obj.setPersonFefuExt((PersonFefuExt) value);
                    return;
                case "vipDate":
                    obj.setVipDate((Date) value);
                    return;
                case "commentVip":
                    obj.setCommentVip((String) value);
                    return;
                case "fioCuratorVip":
                    obj.setFioCuratorVip((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mdbViewPerson":
                        return true;
                case "personFefuExt":
                        return true;
                case "vipDate":
                        return true;
                case "commentVip":
                        return true;
                case "fioCuratorVip":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mdbViewPerson":
                    return true;
                case "personFefuExt":
                    return true;
                case "vipDate":
                    return true;
                case "commentVip":
                    return true;
                case "fioCuratorVip":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mdbViewPerson":
                    return MdbViewPerson.class;
                case "personFefuExt":
                    return PersonFefuExt.class;
                case "vipDate":
                    return Date.class;
                case "commentVip":
                    return String.class;
                case "fioCuratorVip":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewPersonAdditionalData> _dslPath = new Path<MdbViewPersonAdditionalData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewPersonAdditionalData");
    }
            

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData#getMdbViewPerson()
     */
    public static MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
    {
        return _dslPath.mdbViewPerson();
    }

    /**
     * @return Персона (расширение ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData#getPersonFefuExt()
     */
    public static PersonFefuExt.Path<PersonFefuExt> personFefuExt()
    {
        return _dslPath.personFefuExt();
    }

    /**
     * @return Признак vip.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData#getVipDate()
     */
    public static PropertyPath<Date> vipDate()
    {
        return _dslPath.vipDate();
    }

    /**
     * @return Комментарий для vip.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData#getCommentVip()
     */
    public static PropertyPath<String> commentVip()
    {
        return _dslPath.commentVip();
    }

    /**
     * @return ФИО куратора vip.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData#getFioCuratorVip()
     */
    public static PropertyPath<String> fioCuratorVip()
    {
        return _dslPath.fioCuratorVip();
    }

    public static class Path<E extends MdbViewPersonAdditionalData> extends EntityPath<E>
    {
        private MdbViewPerson.Path<MdbViewPerson> _mdbViewPerson;
        private PersonFefuExt.Path<PersonFefuExt> _personFefuExt;
        private PropertyPath<Date> _vipDate;
        private PropertyPath<String> _commentVip;
        private PropertyPath<String> _fioCuratorVip;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData#getMdbViewPerson()
     */
        public MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
        {
            if(_mdbViewPerson == null )
                _mdbViewPerson = new MdbViewPerson.Path<MdbViewPerson>(L_MDB_VIEW_PERSON, this);
            return _mdbViewPerson;
        }

    /**
     * @return Персона (расширение ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData#getPersonFefuExt()
     */
        public PersonFefuExt.Path<PersonFefuExt> personFefuExt()
        {
            if(_personFefuExt == null )
                _personFefuExt = new PersonFefuExt.Path<PersonFefuExt>(L_PERSON_FEFU_EXT, this);
            return _personFefuExt;
        }

    /**
     * @return Признак vip.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData#getVipDate()
     */
        public PropertyPath<Date> vipDate()
        {
            if(_vipDate == null )
                _vipDate = new PropertyPath<Date>(MdbViewPersonAdditionalDataGen.P_VIP_DATE, this);
            return _vipDate;
        }

    /**
     * @return Комментарий для vip.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData#getCommentVip()
     */
        public PropertyPath<String> commentVip()
        {
            if(_commentVip == null )
                _commentVip = new PropertyPath<String>(MdbViewPersonAdditionalDataGen.P_COMMENT_VIP, this);
            return _commentVip;
        }

    /**
     * @return ФИО куратора vip.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonAdditionalData#getFioCuratorVip()
     */
        public PropertyPath<String> fioCuratorVip()
        {
            if(_fioCuratorVip == null )
                _fioCuratorVip = new PropertyPath<String>(MdbViewPersonAdditionalDataGen.P_FIO_CURATOR_VIP, this);
            return _fioCuratorVip;
        }

        public Class getEntityClass()
        {
            return MdbViewPersonAdditionalData.class;
        }

        public String getEntityName()
        {
            return "mdbViewPersonAdditionalData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
