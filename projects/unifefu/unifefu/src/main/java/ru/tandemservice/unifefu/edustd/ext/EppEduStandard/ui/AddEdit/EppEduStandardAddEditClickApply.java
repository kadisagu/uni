/* $Id$ */
package ru.tandemservice.unifefu.edustd.ext.EppEduStandard.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.ui.AddEdit.EppEduStandardAddEditUI;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks;
import ru.tandemservice.unifefu.entity.gen.FefuEppStateEduStandardAdditionalChecksGen;

/**
 * @author Alexey Lopatin
 * @since 19.01.2015
 */
public class EppEduStandardAddEditClickApply extends NamedUIAction
{
    public EppEduStandardAddEditClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof EppEduStandardAddEditUI)
        {
            EppEduStandardAddEditUI pr = (EppEduStandardAddEditUI) presenter;
            pr.onClickApply();

            IUIAddon uiAddon = presenter.getConfig().getAddon(EppEduStandardAddEditExt.ADDON_NAME);
            if (null != uiAddon)
            {
                if (uiAddon instanceof EppEduStandardAddEditExtUI)
                {
                    EppEduStandardAddEditExtUI addon = (EppEduStandardAddEditExtUI) uiAddon;
                    DataAccessServices.dao().saveOrUpdate(addon.getStandardExt());
                }
            }

            FefuEppStateEduStandardAdditionalChecks additionalChecks = DataAccessServices.dao().getByNaturalId(new FefuEppStateEduStandardAdditionalChecksGen.NaturalId(pr.getStateEduStandard()));
            if (additionalChecks == null)
            {
                additionalChecks = new FefuEppStateEduStandardAdditionalChecks(true);
                additionalChecks.setEduStandard(pr.getStateEduStandard());
            }
            else additionalChecks.update(new FefuEppStateEduStandardAdditionalChecks(true), false);
            DataAccessServices.dao().saveOrUpdate(additionalChecks);
        }
    }
}
