/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.translator;

/**
 * Транслятор. Приводит значение к нужному типу.
 * @author Alexander Zhebko
 * @since 23.07.2013
 */
public interface Translator<T>
{
    /**
     * Приводит значение к требуемому типу.
     * @param value привожимое значение
     * @return значение, приведенное к требуемому типу
     * @throws IllegalArgumentException если знаечение не приводится к требуемому типу
     */
    public T translate(String value) throws IllegalArgumentException;
}