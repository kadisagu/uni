/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6;

import org.tandemframework.core.CoreDateUtils;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author nvankov
 * @since 7/2/13
 */
public class FefuGiveDiplomaWithDismissStuListExtractDao extends UniBaseDao implements IExtractComponentDao<FefuGiveDiplomaWithDismissStuListExtract>
{
    @Override
    public void doCommit(FefuGiveDiplomaWithDismissStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setStudentStatusOld(extract.getEntity().getStatus());
        extract.getEntity().setStatus(extract.getStudentStatusNew());

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getGraduateDiplomaOrderDate());
            extract.setPrevOrderNumber(orderData.getGraduateDiplomaOrderNumber());
        }

        extract.setFinishedYear(student.getFinishYear());
        student.setFinishYear(CoreDateUtils.getYear(extract.getParagraph().getOrder().getCommitDate()));

        orderData.setGraduateDiplomaOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setGraduateDiplomaOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);

        //проверяем являлся ли студент старостой, если да - то разрываем связь
        UnifefuDaoFacade.getFefuMoveStudentDAO().breakLinkCaptainWithGroups(extract);
    }

    @Override
    public void doRollback(FefuGiveDiplomaWithDismissStuListExtract extract, Map parameters)
    {
        extract.getEntity().setStatus(extract.getStudentStatusOld());

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }

        student.setFinishYear(extract.getFinishedYear());

        orderData.setGraduateDiplomaOrderDate(extract.getPrevOrderDate());
        orderData.setGraduateDiplomaOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);

        UnifefuDaoFacade.getFefuMoveStudentDAO().linkStudCaptainWithGroups(extract);
    }
}
