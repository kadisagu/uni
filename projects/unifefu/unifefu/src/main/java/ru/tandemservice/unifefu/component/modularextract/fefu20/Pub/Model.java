/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu20.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 20.01.2015
 */
public class Model extends ModularStudentExtractPubModel<FefuEnrollStuDPOExtract>
{
    private StudentStatus _studentStatusNew;
    private String _printFormFileName;
    private FefuAdditionalProfessionalEducationProgram _dpoProgramOld;

    public FefuAdditionalProfessionalEducationProgram getDpoProgramOld()
    {
        return _dpoProgramOld;
    }

    public void setDpoProgramOld(FefuAdditionalProfessionalEducationProgram dpoProgramOld)
    {
        _dpoProgramOld = dpoProgramOld;
    }

    public String getPrintFormFileName()
    {
        return _printFormFileName;
    }

    public void setPrintFormFileName(String printFormFileName)
    {
        _printFormFileName = printFormFileName;
    }

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }

    public boolean isShowPrintForm()
    {
        return isIndividualOrder() && getExtract().getParagraph().getOrder().getState().getCode().equals("1");

    }
}