/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Person.ui.Tab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.Person.ui.Tab.PersonTabUI;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 10.01.2015
 */
public class PersonTabExtUI extends UIAddon
{
    private List<FefuNsiPersonContact> _nsiContactList;
    private FefuNsiPersonContact _currentContact;

    public PersonTabExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuNsiPersonContact.class, "e").column(DQLExpressions.property("e"))
                .where(DQLExpressions.eq(DQLExpressions.property(FefuNsiPersonContact.person().id().fromAlias("e")), DQLExpressions.value(((PersonTabUI) getPresenter()).getPerson().getId())))
                .where(DQLExpressions.isNull(DQLExpressions.property(FefuNsiPersonContact.address().fromAlias("e"))))
                .where(DQLExpressions.isNull(DQLExpressions.property(FefuNsiPersonContact.field().fromAlias("e"))))
                .order(DQLExpressions.property(FefuNsiPersonContact.type().userCode().fromAlias("e")))
                .order(DQLExpressions.property(FefuNsiPersonContact.description().fromAlias("e")));

        _nsiContactList = DataAccessServices.dao().getList(builder);
    }

    public List<FefuNsiPersonContact> getNsiContactList()
    {
        return _nsiContactList;
    }

    public void setNsiContactList(List<FefuNsiPersonContact> nsiContactList)
    {
        _nsiContactList = nsiContactList;
    }

    public FefuNsiPersonContact getCurrentContact()
    {
        return _currentContact;
    }

    public void setCurrentContact(FefuNsiPersonContact currentContact)
    {
        _currentContact = currentContact;
    }
}