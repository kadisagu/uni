/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu21;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.component.listextract.fefu21.utils.FefuConditionalCourseTransferParagraphPartWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu21.utils.FefuConditionalCourseTransferParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Igor Belanov
 * @since 29.06.2016
 * <p>
 * Класс является копией такого же из fefu5 (вместе с обёртками из utils)
 * (т.к. приказ - почти такой же как fefu5)
 */
@SuppressWarnings("Duplicates")
public class FefuConditionalCourseTransferListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);
        List<FefuConditionalCourseTransferListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, extracts.get(0), prepareParagraphsStructure(extracts));

        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    protected List<FefuConditionalCourseTransferParagraphWrapper> prepareParagraphsStructure(List<FefuConditionalCourseTransferListExtract> extracts)
    {
        List<FefuConditionalCourseTransferParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        int ind;

        for (FefuConditionalCourseTransferListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            CompensationType compensationType = student.getCompensationType();
            StudentCategory studentCategory = student.getStudentCategory();
            EducationLevelsHighSchool educationLevelsHighSchool = student.getEducationOrgUnit().getEducationLevelHighSchool();
            FefuConditionalCourseTransferParagraphWrapper paragraphWrapper = new FefuConditionalCourseTransferParagraphWrapper(studentCategory, compensationType, extract);
            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            FefuConditionalCourseTransferParagraphPartWrapper paragraphPartWrapper = new FefuConditionalCourseTransferParagraphPartWrapper(educationLevelsHighSchool, extract);
            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);

            Person person = student.getPerson();

            if (!paragraphPartWrapper.getPersonList().contains(person))
                paragraphPartWrapper.getPersonList().add(person);
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, FefuConditionalCourseTransferListExtract firstExtract, List<FefuConditionalCourseTransferParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            Group group = firstExtract.getGroup();
            EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();

            int parNumber = 0;
            for (FefuConditionalCourseTransferParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_CONDITIONAL_COURSE_TRANSFER_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());

                CommonListExtractPrint.injectCommonListExtractData(paragraphInjectModifier, firstExtract);
                CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());

                paragraphInjectModifier.put("courseOld", firstExtract.getCourse().getTitle())
                        .put("courseNew", firstExtract.getCourseNew().getTitle());

                if (DevelopFormCodes.FULL_TIME_FORM.equals(firstExtract.getGroupNew().getEducationOrgUnit().getDevelopForm().getCode()))
                    paragraphInjectModifier.put("fefuGroupNew", " в группу " + firstExtract.getGroupNew().getTitle());
                else
                    paragraphInjectModifier.put("fefuGroupNew", "");

                if (DevelopFormCodes.FULL_TIME_FORM.equals(firstExtract.getGroup().getEducationOrgUnit().getDevelopForm().getCode()))
                    paragraphInjectModifier.put("fefuGroupOld", " группы " + firstExtract.getGroup().getTitle());
                else
                    paragraphInjectModifier.put("fefuGroupOld", "");

                EducationLevels educationLevelsOld = firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, educationLevelsOld, "Old");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstExtract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(), "orgUnit", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, educationOrgUnit, "formativeOrgUnitStr", "Old");
                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, paragraphWrapper.getCompensationType(), "", false);

                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() == 1 ? "" : String.valueOf(++parNumber) + ". ");
                paragraphInjectModifier.put("paragraphNumberConditional", paragraphWrappers.size() == 1 ? "" : String.valueOf(parNumber) + ". ");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(group), "fefuShortFastExtendedOptionalText");
                modifyReason(paragraphInjectModifier, firstExtract);

                String educationStrDirection;

                EducationLevelsHighSchool educationLevelsHighSchool = firstExtract.getGroup().getEducationOrgUnit().getEducationLevelHighSchool();
                StructureEducationLevels levelType = educationLevelsHighSchool.getEducationLevel().getLevelType();

                if (levelType.isSpecialization()) // специализация
                {
                    educationStrDirection = "специальности ";
                } else if (levelType.isSpecialty()) // специальность
                {
                    educationStrDirection = "специальности ";
                } else if (levelType.isBachelor() && levelType.isProfile()) // бакалаврский профиль
                {
                    educationStrDirection = "направлению ";
                } else if (levelType.isMaster() && levelType.isProfile()) // магистерский профиль
                {
                    educationStrDirection = "направлению ";
                } else // направление подготовки
                {
                    educationStrDirection = "направлению подготовки ";
                }

                String direction = CommonExtractPrint.getFefuHighEduLevelStr(educationLevelsHighSchool.getEducationLevel());

                paragraphInjectModifier.put("fefuEducationStrDirectionOld_D", educationStrDirection + direction);

                // отличие: добавлена метка, в которой выводится дата погашения акад.задолженности
                paragraphInjectModifier.put("fefuEliminateDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getEliminateDate()));

                paragraphInjectModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<FefuConditionalCourseTransferParagraphPartWrapper> paragraphPartWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (FefuConditionalCourseTransferParagraphPartWrapper paragraphPartWrapper : paragraphPartWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_CONDITIONAL_COURSE_TRANSFER_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, paragraphPartWrapper.getFirstExtract());
                if (paragraphPartWrapper.getEducationLevelsHighSchool().getEducationLevel().isProfileOrSpecialization())
                {
                    modifyFefuEducationStrProfile(paragraphPartInjectModifier, paragraphPartWrapper.getEducationLevelsHighSchool());
                } else
                {
                    paragraphPartInjectModifier.put("fefuEducationStrProfile", "");
                }

                List<Person> personList = paragraphPartWrapper.getPersonList();
                RtfString rtfString = new RtfString();

                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);

                int i = 0;

                for (; i < personList.size() - 1; i++)
                {
                    rtfString.append(String.valueOf(i + 1)).append(". ").append(personList.get(i).getFullFio()).par();
                }

                rtfString.append(String.valueOf(i + 1)).append(". ").append(personList.get(i).getFullFio());

                paragraphPartInjectModifier.put("STUDENT_LIST", rtfString);
                paragraphPartInjectModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected static void modifyFefuEducationStrProfile(RtfInjectModifier modifier, EducationLevelsHighSchool educationLevelsHighSchool)
    {
        StructureEducationLevels levels = educationLevelsHighSchool.getEducationLevel().getLevelType();
        RtfString fefuEducationStrProfile = new RtfString();
        if (levels.isSpecialization() || levels.isProfile())
        {
            fefuEducationStrProfile.append(levels.isProfile() ? levels.isBachelor() ? "Профиль «" : "Магистерская программа «" : "Специализация «");
            fefuEducationStrProfile.append(educationLevelsHighSchool.getTitle() + "»").par();
        }
        modifier.put("fefuEducationStrProfile", fefuEducationStrProfile);
    }

    protected static void modifyReason(RtfInjectModifier modifier, AbstractStudentExtract extract)
    {
        StudentOrderReasons reason = ((StudentListOrder) extract.getParagraph().getOrder()).getReason();
        if (null != reason)
        {
            String reasonPrint = reason.getPrintTitle() == null ? reason.getTitle() : reason.getPrintTitle();
            modifier.put("reason", reasonPrint);
            modifier.put("Reason", StringUtils.capitalize(reasonPrint));
            modifier.put("reasonPrint", reasonPrint);
            modifier.put("ReasonPrint", StringUtils.capitalize(reasonPrint));
            modifier.put("reasonTitle", reason.getTitle());
            modifier.put("ReasonTitle", StringUtils.capitalize(reason.getTitle()));
        } else
        {
            modifier.put("reason", "");
            modifier.put("Reason", "");
            modifier.put("reasonPrint", "");
            modifier.put("ReasonPrint", "");
            modifier.put("reasonTitle", "");
            modifier.put("ReasonTitle", "");
        }
    }
}
