/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu7;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.utils.SessionPartModel;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract;

import java.util.Date;

/**
 * @author Alexander Zhebko
 * @since 9/5/12
 */
public class FefuAcadGrantAssignStuExtractPrint implements IPrintFormCreator<FefuAcadGrantAssignStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuAcadGrantAssignStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("sessionType_G", SessionPartModel.getSessionShortTitle(extract.getSessionType(), GrammaCase.GENITIVE));
        modifier.put("grantSize", DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(extract.getGrantSize()));
        modifier.put("groupManagerBonusAssignment", !extract.isGroupManagerBonus() ? "" :
                getGroupManagerBonusAssignmentText(extract.getGroupManagerBonusSize(),
                                                   extract.getGroupManagerBonusBeginDate(),
                                                   extract.getGroupManagerBonusEndDate()));

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    public static String getGroupManagerBonusAssignmentText(Double grandSize, Date beginDate, Date endDate)
    {
        StringBuilder ret = new StringBuilder(", назначить надбавку к государственной академической стипендии как старосте учебной группы в размере ");
        ret.append(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(grandSize));
        ret.append(" руб. с ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(beginDate));
        ret.append(" по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate));
        return ret.toString();
    }
}