/* $Id$ */
package ru.tandemservice.unifefu.component.registry.AttestationRegistry.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;

/**
 * @author Alexey Lopatin
 * @since 21.11.2014
 */
@Input({@Bind(key = "fromOrgUnit", binding = "fromOrgUnit")})
public class Model extends ru.tandemservice.uniepp.component.registry.AttestationRegistry.Pub.Model
{
    private boolean fromOrgUnit;

    @Override
    public String getSelectedTab()
    {
        String selectedTab = super.getSelectedTab();
        return selectedTab == null ? "fefuRegistryElementExtLoadTab" : selectedTab;
    }

    public void setFromOrgUnit(boolean fromOrgUnit)
    {
        this.fromOrgUnit = fromOrgUnit;
    }

    public ParametersMap getParameters4Edit(){
        return new ParametersMap().add(UIPresenter.PUBLISHER_ID, getElement().getId()).add("fromOrgUnit", fromOrgUnit);
    }
}
