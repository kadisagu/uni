
package ru.tandemservice.unifefu.ws.blackboard.course.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="c" type="{http://course.ws.blackboard/xsd}CourseVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "c"
})
@XmlRootElement(name = "saveCourse")
public class SaveCourse {

    @XmlElementRef(name = "c", namespace = "http://course.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<CourseVO> c;

    /**
     * Gets the value of the c property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CourseVO }{@code >}
     *     
     */
    public JAXBElement<CourseVO> getC() {
        return c;
    }

    /**
     * Sets the value of the c property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CourseVO }{@code >}
     *     
     */
    public void setC(JAXBElement<CourseVO> value) {
        this.c = ((JAXBElement<CourseVO> ) value);
    }

}
