package ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unifefu.brs.base.BaseFefuReportAddUI;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport.FefuListTrJournalDisciplinesReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport.logic.FefuListTrJournalDisciplinesReportParams;
import ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport.ui.View.FefuListTrJournalDisciplinesReportView;
import ru.tandemservice.unifefu.entity.report.FefuListTrJournalDisciplinesReport;

import java.util.Collections;

/**
 * @author amakarova
 */
public class FefuListTrJournalDisciplinesReportAddUI extends BaseFefuReportAddUI<FefuListTrJournalDisciplinesReportParams>
{
    @Override
    protected void fillDefaults(FefuListTrJournalDisciplinesReportParams reportSettings)
    {
        super.fillDefaults(reportSettings);
        reportSettings.setFormativeOrgUnit(getCurrentFormativeOrgUnit());
        reportSettings.setOnlyFilledJournals(FefuBrsReportManager.instance().yesNoItemListExtPoint().getItem(FefuBrsReportManager.YES_ID.toString()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuBrsReportManager.GROUP_DS.equals(dataSource.getName()))
        {
            if(null != getReportSettings().getFormativeOrgUnit())
                dataSource.put(FefuBrsReportManager.PARAM_FORMATIVE_OU, Collections.singletonList(getReportSettings().getFormativeOrgUnit()));
        }
    }

    public void onClickApply()
    {
        FefuListTrJournalDisciplinesReport report = FefuListTrJournalDisciplinesReportManager.instance().dao().createReport(getReportSettings());
        deactivate();
        _uiActivation.asDesktopRoot(FefuListTrJournalDisciplinesReportView.class).parameter(UIPresenter.PUBLISHER_ID, report.getId()).activate();
    }
}
