package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexGenerationCodes;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuEduStandartGenerationCodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x8x0_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEduStandartGeneration

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("fefu_c_edu_standart_gen_t"))
            {
                DBTable dbt = new DBTable("fefu_c_edu_standart_gen_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_fefuedustandartgeneration"),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuEduStandartGeneration");


                PreparedStatement insertInCatalogFirst = tool.prepareStatement("insert into fefu_c_edu_standart_gen_t (id, discriminator, code_p, title_p) values (?, ?, ?, ?)");
                insertInCatalogFirst.setObject(1, EntityIDGenerator.generateNewId(entityCode).longValue());
                insertInCatalogFirst.setObject(2, entityCode);
                insertInCatalogFirst.setObject(3, FefuEduStandartGenerationCodes.GOS_2);
                insertInCatalogFirst.setObject(4, "ГОС 2");
                insertInCatalogFirst.execute();

                PreparedStatement insertInCatalogSecond = tool.prepareStatement("insert into fefu_c_edu_standart_gen_t (id, discriminator, code_p, title_p) values (?, ?, ?, ?)");
                insertInCatalogSecond.setObject(1, EntityIDGenerator.generateNewId(entityCode).longValue());
                insertInCatalogSecond.setObject(2, entityCode);
                insertInCatalogSecond.setObject(3, FefuEduStandartGenerationCodes.FGOS_3);
                insertInCatalogSecond.setObject(4, "ФГОС 3");
                insertInCatalogSecond.execute();

            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEppStateEduStandardExt

		// создано обязательное свойство eduStandardGen
		{
			// создать колонку
            if(!tool.columnExists("fefueppstateedustandardext_t", "edustandardgen_id"))
            {
                tool.createColumn("fefueppstateedustandardext_t", new DBColumn("edustandardgen_id", DBType.LONG));
                Map<String, Long> standartGenerationMap = new HashMap<>();

                PreparedStatement selectStandartGeneration = tool.prepareStatement("select id, code_p from fefu_c_edu_standart_gen_t");
                selectStandartGeneration.execute();
                ResultSet standartGenerationRes = selectStandartGeneration.getResultSet();
                while (standartGenerationRes.next())
                {
                    Long stateStandartId = standartGenerationRes.getLong(1);
                    String generationCode = standartGenerationRes.getString(2);
                    standartGenerationMap.put(generationCode, stateStandartId);
                }

                PreparedStatement selectEduStandart = tool.prepareStatement("SELECT t1.ID, t5.CODE_P " +
                                                                                    "FROM fefueppstateedustandardext_t t1 " +
                                                                                    "LEFT JOIN epp_stateedustd_t t2 ON t1.EDUSTANDARD_ID = t2.ID " +
                                                                                    "LEFT JOIN edu_c_pr_subject_t t3 ON t2.PROGRAMSUBJECT_ID = t3.ID " +
                                                                                    "LEFT JOIN edu_c_pr_subject_index_t t4 ON t3.SUBJECTINDEX_ID = t4.ID " +
                                                                                    "LEFT JOIN edu_c_pr_subject_index_gen_t t5 ON t4.GENERATION_ID = t5.ID ");
                selectEduStandart.execute();
                ResultSet res = selectEduStandart.getResultSet();
                while (res.next())
                {
                    Long stateStandartId = res.getLong(1);
                    String generationCode = res.getString(2);
                    if (generationCode.equals(EduProgramSubjectIndexGenerationCodes.OKSO))
                    {
                        Long defaultEduStandardGen = standartGenerationMap.get(FefuEduStandartGenerationCodes.GOS_2);
                        tool.executeUpdate("update fefueppstateedustandardext_t set edustandardgen_id=? where id=?", defaultEduStandardGen, stateStandartId);
                    }
                    else
                    {
                        Long defaultEduStandardGen = standartGenerationMap.get(FefuEduStandartGenerationCodes.FGOS_3);
                        tool.executeUpdate("update fefueppstateedustandardext_t set edustandardgen_id=? where id=?", defaultEduStandardGen, stateStandartId);
                    }
                }
                // сделать колонку NOT NULL
                tool.setColumnNullable("fefueppstateedustandardext_t", "edustandardgen_id", false);
            }

		}


    }
}