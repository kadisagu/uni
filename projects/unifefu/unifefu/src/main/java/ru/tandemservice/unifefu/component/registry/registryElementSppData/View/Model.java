/* $Id$ */
package ru.tandemservice.unifefu.component.registry.registryElementSppData.View;

import ru.tandemservice.uniepp.entity.catalog.EppState;

/**
 * @author Alexey Lopatin
 * @since 28.11.2014
 */
public class Model extends ru.tandemservice.unispp.component.registry.registryElementSppData.View.Model
{
    public boolean isDisabledSppData()
    {
        String stateCode = getElement().getState().getCode();
        return !stateCode.equals(EppState.STATE_FORMATIVE);
    }
}
