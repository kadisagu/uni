/**
 * FefuCHDataProviderServiceImplLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.integralCH.test;

public class FefuCHDataProviderServiceImplLocator extends org.apache.axis.client.Service implements ru.tandemservice.unifefu.ws.integralCH.test.FefuCHDataProviderServiceImpl {

    public FefuCHDataProviderServiceImplLocator() {
    }


    public FefuCHDataProviderServiceImplLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FefuCHDataProviderServiceImplLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for FefuCHDataProviderServicePort
    private java.lang.String FefuCHDataProviderServicePort_address = "http://localhost:8080/services/FefuCHDataProviderService";

    public java.lang.String getFefuCHDataProviderServicePortAddress() {
        return FefuCHDataProviderServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String FefuCHDataProviderServicePortWSDDServiceName = "FefuCHDataProviderServicePort";

    public java.lang.String getFefuCHDataProviderServicePortWSDDServiceName() {
        return FefuCHDataProviderServicePortWSDDServiceName;
    }

    public void setFefuCHDataProviderServicePortWSDDServiceName(java.lang.String name) {
        FefuCHDataProviderServicePortWSDDServiceName = name;
    }

    public ru.tandemservice.unifefu.ws.integralCH.test.FefuCHDataProviderService getFefuCHDataProviderServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(FefuCHDataProviderServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getFefuCHDataProviderServicePort(endpoint);
    }

    public ru.tandemservice.unifefu.ws.integralCH.test.FefuCHDataProviderService getFefuCHDataProviderServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ru.tandemservice.unifefu.ws.integralCH.test.FefuCHDataProviderServiceSoapBindingStub _stub = new ru.tandemservice.unifefu.ws.integralCH.test.FefuCHDataProviderServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getFefuCHDataProviderServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setFefuCHDataProviderServicePortEndpointAddress(java.lang.String address) {
        FefuCHDataProviderServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ru.tandemservice.unifefu.ws.integralCH.test.FefuCHDataProviderService.class.isAssignableFrom(serviceEndpointInterface)) {
                ru.tandemservice.unifefu.ws.integralCH.test.FefuCHDataProviderServiceSoapBindingStub _stub = new ru.tandemservice.unifefu.ws.integralCH.test.FefuCHDataProviderServiceSoapBindingStub(new java.net.URL(FefuCHDataProviderServicePort_address), this);
                _stub.setPortName(getFefuCHDataProviderServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("FefuCHDataProviderServicePort".equals(inputPortName)) {
            return getFefuCHDataProviderServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://integralCH.ws.unifefu.tandemservice.ru/", "FefuCHDataProviderServiceImpl");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://integralCH.ws.unifefu.tandemservice.ru/", "FefuCHDataProviderServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("FefuCHDataProviderServicePort".equals(portName)) {
            setFefuCHDataProviderServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
