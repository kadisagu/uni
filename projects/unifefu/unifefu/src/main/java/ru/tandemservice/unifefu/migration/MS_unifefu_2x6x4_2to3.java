package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x4_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.4"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.4")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность bbCourse

		// изменен тип свойства bbCourseId
		{
			// изменить тип колонки
			tool.changeColumnType("bbcourse_t", "bbcourseid_p", DBType.createVarchar(48));

		}

		// изменен тип свойства bbPrimaryId
		{
			// изменить тип колонки
			tool.changeColumnType("bbcourse_t", "bbprimaryid_p", DBType.createVarchar(16));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbCourseStudent

		// изменен тип свойства bbPrimaryId
		{
			// изменить тип колонки
			tool.changeColumnType("bbcoursestudent_t", "bbprimaryid_p", DBType.createVarchar(16));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbGroup

		// изменен тип свойства bbPrimaryId
		{
			// изменить тип колонки
			tool.changeColumnType("bbgroup_t", "bbprimaryid_p", DBType.createVarchar(16));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbGroupMembership

		// изменен тип свойства bbPrimaryId
		{
			// изменить тип колонки
			tool.changeColumnType("bbgroupmembership_t", "bbprimaryid_p", DBType.createVarchar(16));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbTrJournalEventRel

		// изменен тип свойства bbCourseColumnPrimaryId
		{
			// изменить тип колонки
			tool.changeColumnType("bbtrjournaleventrel_t", "bbcoursecolumnprimaryid_p", DBType.createVarchar(16));

		}


    }
}