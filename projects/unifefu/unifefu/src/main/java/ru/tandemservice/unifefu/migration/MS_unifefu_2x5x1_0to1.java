/* $Id: MS_unifefu_2x5x1_0to1.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuEntrantOrderExtension

        // создано свойство directumScanUrl
        {
            // создать колонку
            tool.createColumn("fefuentrantorderextension_t", new DBColumn("directumscanurl_p", DBType.createVarchar(255)));

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuStudentOrderExtension

        // создано свойство directumScanUrl
        {
            // создать колонку
            tool.createColumn("fefustudentorderextension_t", new DBColumn("directumscanurl_p", DBType.createVarchar(255)));

        }


    }
}
