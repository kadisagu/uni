/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu11;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.component.listextract.fefu11.utils.FefuCompensationTypeTransferExtractWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu11.utils.FefuCompensationTypeTransferParagraphPartWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu11.utils.FefuCompensationTypeTransferParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class FefuCompensationTypeTransferStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<FefuCompensationTypeTransferStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);
        FefuCompensationTypeTransferStuListExtract firstExtract = extracts.get(0);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, firstExtract);
        OrgUnit formativeOrgUnit = order.getOrgUnit();
        List<FefuCompensationTypeTransferParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts);

        // Вставляем список параграфов
        injectParagraphs(document, preparedParagraphsStructure, injectModifier, formativeOrgUnit);

        injectModifier.put("protocolDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getProtocolDate()));
        injectModifier.put("protocolNumbers", firstExtract.getProtocolNumbers());

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<FefuCompensationTypeTransferParagraphWrapper> prepareParagraphsStructure(List<FefuCompensationTypeTransferStuListExtract> extracts)
    {
        int ind;
        List<FefuCompensationTypeTransferParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        for (FefuCompensationTypeTransferStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = extract.getEntity().getPerson();

            FefuCompensationTypeTransferParagraphWrapper paragraphWrapper = new FefuCompensationTypeTransferParagraphWrapper(
                    extract.getTransferDate(),
                    student.getStudentCategory(),
                    student.getEducationOrgUnit().getDevelopForm());

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            FefuCompensationTypeTransferParagraphPartWrapper paragraphPartWrapper = new FefuCompensationTypeTransferParagraphPartWrapper(student.getEducationOrgUnit().getTerritorialOrgUnit());

            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);

            paragraphPartWrapper.getExtractWrapperList().add(new FefuCompensationTypeTransferExtractWrapper(person, student.getEducationOrgUnit().getDevelopForm(), student.getCourse(), student.getGroup()));
        }
        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<FefuCompensationTypeTransferParagraphWrapper> paragraphWrappers, RtfInjectModifier injectModifier, OrgUnit formativeOrgUnit)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (FefuCompensationTypeTransferParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_TRANSFER_BUDGET_COMPENSATION_TYPE_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = new RtfInjectModifier();

                paragraphInjectModifier.put("parNumber", String.valueOf(++parNumber) + ". ");
                paragraphInjectModifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(paragraphWrapper.getTransferDate()));

                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, paragraphWrapper.getDevelopForm(), "");
                CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());

                paragraphInjectModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList(), formativeOrgUnit);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            injectModifier.put("parNumber", String.valueOf(++parNumber) + ". ");
            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<FefuCompensationTypeTransferParagraphPartWrapper> paragraphPartWrappers, OrgUnit formativeOrgUnit)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            for (FefuCompensationTypeTransferParagraphPartWrapper paragraphPartWrapper : paragraphPartWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_TRANSFER_BUDGET_COMPENSATION_TYPE_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphPartInjectModifier = new RtfInjectModifier();
                OrgUnit territorialOrgUnit = paragraphPartWrapper.getTerritorialOrgUnit();
                paragraphPartInjectModifier.put("territorialOrgUnit",(territorialOrgUnit.equals(TopOrgUnit.getInstance()) ? null : (territorialOrgUnit.equals(formativeOrgUnit)? null : new RtfString().append(territorialOrgUnit.getPrintTitle()).par())));
                paragraphPartInjectModifier.modify(paragraphPart);

                RtfTableModifier paragraphPartTableModifier = new RtfTableModifier();
                List<FefuCompensationTypeTransferExtractWrapper> extractWrapperList = paragraphPartWrapper.getExtractWrapperList();

                Collections.sort(extractWrapperList);

                int j = 0;
                String[][] tableData = new String[extractWrapperList.size()][];
                for (FefuCompensationTypeTransferExtractWrapper extractWrapper : extractWrapperList)
                {
                    String developFormCode = extractWrapper.getDevelopForm().getCode();
                    tableData[j++] = new String[]{String.valueOf(j) + ".", extractWrapper.getPerson().getFullFio(), developFormCode.equals(DevelopFormCodes.CORESP_FORM) ? extractWrapper.getCourse().getTitle() + " курс" : "группа " + extractWrapper.getGroup().getTitle()};
                }

                paragraphPartTableModifier.put("STUDENTS_TABLE", tableData);
                paragraphPartTableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}