/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu15;

import org.joda.time.DateTime;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 11/15/13
 */
public class FullStateMaintenanceEnrollmentStuExtractDao extends UniBaseDao implements IExtractComponentDao<FullStateMaintenanceEnrollmentStuExtract>
{
    @Override
    public void doCommit(FullStateMaintenanceEnrollmentStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        DQLSelectBuilder paymentsBuilder = new DQLSelectBuilder().fromEntity(FefuOrphanPayment.class, "op");
        paymentsBuilder.where(DQLExpressions.eq(DQLExpressions.property("op", FefuOrphanPayment.extract().id()), DQLExpressions.value(extract.getId())));
        List<FefuOrphanPayment> orphanPayments = paymentsBuilder.createStatement(getSession()).list();

        for(FefuOrphanPayment orphanPayment : orphanPayments)
        {
            //выплата
            FefuStudentPayment payment = new FefuStudentPayment();
            payment.setExtract(extract);
            payment.setStopOrPauseOrder(false);
            DateTime date = new DateTime().withYear(orphanPayment.getPaymentYear()).withMonthOfYear(orphanPayment.getPaymentMonth());
            Date firstDate = date.dayOfMonth().withMinimumValue().toDate();
            Date lastDate = date.dayOfMonth().withMaximumValue().toDate();
            payment.setStartDate(firstDate);
            payment.setStopDate(lastDate);
            payment.setPaymentSum(orphanPayment.getPaymentSumInRuble());
            if(null != extract.getReason())
                payment.setReason(extract.getReason().getTitle());
            save(payment);
        }
    }

    @Override
    public void doRollback(FullStateMaintenanceEnrollmentStuExtract extract, Map parameters)
    {
        // Удаляем выплаты
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuStudentPayment.class);
        deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(FefuStudentPayment.extract()), DQLExpressions.value(extract)));
        deleteBuilder.createStatement(getSession()).execute();
    }
}
