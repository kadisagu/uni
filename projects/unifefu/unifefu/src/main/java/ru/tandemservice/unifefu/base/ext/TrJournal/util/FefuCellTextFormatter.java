package ru.tandemservice.unifefu.base.ext.TrJournal.util;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unifefu.entity.FefuStudentMarkHistory;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.CellTextFormatter;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkCell;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;

import java.util.List;

/**
 * User: amakarova
 * Date: 26.11.13
 */
public class FefuCellTextFormatter extends CellTextFormatter{

    public static String format(TrJournalMarkCell cell)
    {
        StringBuilder sb = new StringBuilder();
        if (null != cell.isAbsent() && cell.isShowAttendance())
            sb.append(cell.isAbsent() ? "н"  : "");
        else
            sb.append("&nbsp;");
        if (cell.getGrade() != null)
            sb.append("&nbsp;").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(cell.getGrade()));
        else
            sb.append(getSymbol(cell.getMark()));
        return sb.toString();
    }

    private static String getSymbol(TrEduGroupEventStudent eventStudent)
    {
        List<FefuStudentMarkHistory> history = UniDaoFacade.getCoreDao().getList(FefuStudentMarkHistory.class, FefuStudentMarkHistory.L_TR_EDU_GROUP_EVENT_STUDENT, eventStudent);
        if (history != null && history.size() > 0)
            return ".";
        return "";
    }
}
