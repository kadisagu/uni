/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 16.06.2013
 */
public class FefuEntrantsDSHandler extends DefaultComboDataSourceHandler
{
    public static final String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    public FefuEntrantsDSHandler(String ownerId)
    {
        super(ownerId, Entrant.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        EnrollmentCampaign campaign = (EnrollmentCampaign) ep.context.get(ENROLLMENT_CAMPAIGN);

        if (StringUtils.isNotBlank(filter))
        {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(Entrant.person().identityCard().fullFio().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }

        ep.dqlBuilder.where(eq(property(Entrant.enrollmentCampaign().fromAlias("e")), value(campaign)));
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order(property(Entrant.person().identityCard().fullFio().fromAlias("e")));
    }
}