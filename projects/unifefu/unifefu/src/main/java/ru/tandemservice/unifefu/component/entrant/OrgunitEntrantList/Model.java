/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.OrgunitEntrantList;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author Nikolay Fedorovskih
 * @since 08.05.2013
 */
public class Model
{
    private OrgUnit orgUnit;

    public OrgUnit getOrgUnit()
    {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }

    public String getAddEntrantPermissionKey()
    {
        return "orgUnit_fefuAddEntrant_" + getOrgUnit().getOrgUnitType().getCode();
    }

    public String getAddEntrantWizardPermissionKey()
    {
        return "orgUnit_fefuAddEntrantWizard_" + getOrgUnit().getOrgUnitType().getCode();
    }
}