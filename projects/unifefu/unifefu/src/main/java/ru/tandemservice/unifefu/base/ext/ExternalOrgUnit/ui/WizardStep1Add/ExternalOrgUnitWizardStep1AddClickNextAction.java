/* $Id$ */
package ru.tandemservice.unifefu.base.ext.ExternalOrgUnit.ui.WizardStep1Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.Edit.ExternalOrgUnitEditUI;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.WizardStep1Add.ExternalOrgUnitWizardStep1Add;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.WizardStep1Add.ExternalOrgUnitWizardStep1AddUI;


/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 12/21/12
 * Time: 7:19 PM
 */
public class ExternalOrgUnitWizardStep1AddClickNextAction extends NamedUIAction
{
    protected ExternalOrgUnitWizardStep1AddClickNextAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if(presenter instanceof ExternalOrgUnitWizardStep1AddUI)
        {
            ExternalOrgUnitWizardStep1AddUI prsnt = (ExternalOrgUnitWizardStep1AddUI) presenter;
            if(StringUtils.isEmpty(prsnt.getOu().getShortTitle()))
                prsnt.getOu().setShortTitle(prsnt.getOu().getTitle());
            prsnt.onClickNext();
        }
    }
}
