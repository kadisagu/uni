package ru.tandemservice.unifefu.base.ext.EcDistribution.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.List.EcDistributionList;

/**
 * Date: 25.07.13
 * Time: 17:06
 */

@Configuration
public class EcDistributionListExt extends BusinessComponentExtensionManager {

    public static final String ADDON_NAME = "unifefu" + EcDistributionListExtUI.class.getSimpleName();

    @Autowired
    private EcDistributionList _ecDistributionList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_ecDistributionList.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EcDistributionListExtUI.class))
                .create();
    }
}
