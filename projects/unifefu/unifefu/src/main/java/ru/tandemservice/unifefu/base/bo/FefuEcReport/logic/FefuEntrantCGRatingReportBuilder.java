/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.unifefu.entity.report.FefuEntrantRatingReport;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 11.07.2013
 */
public class FefuEntrantCGRatingReportBuilder implements IFefuEcReportBuilder
{
    private static final Map<String, String> _developFormByCode = new HashMap<>();

    static
    {
        _developFormByCode.put(DevelopFormCodes.FULL_TIME_FORM, "очной");
        _developFormByCode.put(DevelopFormCodes.CORESP_FORM, "заочной");
        _developFormByCode.put(DevelopFormCodes.PART_TIME_FORM, "очно-заочной");
        _developFormByCode.put(DevelopFormCodes.EXTERNAL_FORM, "эктернат");
        _developFormByCode.put(DevelopFormCodes.APPLICANT_FORM, "самостоятельное обучение и итоговая аттестация");
    }

    private static final Map<String, String> _developConditionByCode = new HashMap<>();

    static
    {
        _developConditionByCode.put(UniDefines.DEVELOP_CONDITION_SHORT, "сокращенной");
        _developConditionByCode.put(UniDefines.DEVELOP_CONDITION_FAST, "ускоренной");
        _developConditionByCode.put(UniDefines.DEVELOP_CONDITION_SHORT_FAST, "сокращенной ускоренной");
    }

    private FefuEntrantRatingReport report;
    private CompetitionGroup competitionGroup;
    private OrgUnit formativeOrgUnit;
    private OrgUnit territorialOrgUnit;
    private List<Qualifications> qualifications;
    private Session session;
    private EntrantDataUtil dataUtil;

    public FefuEntrantCGRatingReportBuilder(FefuEntrantRatingReport report, CompetitionGroup competitionGroup,
                                            List<Qualifications> qualifications, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit)
    {
        this.report = report;
        this.competitionGroup = competitionGroup;
        this.qualifications = qualifications;
        this.formativeOrgUnit = formativeOrgUnit;
        this.territorialOrgUnit = territorialOrgUnit;
    }

    @Override
    public DatabaseFile getContent(Session session)
    {
        this.session = session;
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private List<CompetitionGroup> getSortedCompetitionGroupList(Set<CompetitionGroup> groupSet)
    {
        List<CompetitionGroup> groups = new ArrayList<>(groupSet);
        Collections.sort(groups, new Comparator<ITitled>()
        {
            private String getTitleWithoutCodePrefix(String title)
            {
                for (int i = 0; i < title.length(); i++)
                {
                    char c = title.charAt(i);
                    if ((c >= '0' && c <= '9') || (c == '.') || c == ' ') continue;
                    if (i > 3) return title.substring(i, title.length());
                    break;
                }
                return title;
            }

            public int compare(ITitled o1, ITitled o2)
            {
                String s1 = getTitleWithoutCodePrefix(o1.getTitle());
                if (s1 == null) s1 = "";
                String s2 = getTitleWithoutCodePrefix(o2.getTitle());
                if (s2 == null) s2 = "";
                return s1.compareToIgnoreCase(s2);
            }
        });
        return groups;
    }

    private <T extends ITitled> List<T> getSortedByTitle(Set<T> set) {
        return set.stream().sorted(CommonCollator.TITLED_WITH_ID_COMPARATOR).collect(Collectors.toList());
    }

    @SuppressWarnings("deprecation")
    private void injectCompetitionGroupsTables(RtfTable tableTemplate, List<IRtfElement> elementList, int startIndex,
                                               Map<OrgUnit, Map<CompetitionGroup, Program<Entrant>>> completePrograms, Map<CompetitionKind, Integer> competitionKind2Priority, boolean budget)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        IRtfControl par = elementFactory.createRtfControl(IRtfData.PAR);
        IRtfControl pard = elementFactory.createRtfControl(IRtfData.PARD);
        IRtfControl pageBreak = elementFactory.createRtfControl(IRtfData.PAGEBB);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        boolean fromNewPage = false;

        for (OrgUnit formativeOrgUnit : getSortedByTitle(completePrograms.keySet()))
        {
            Map<CompetitionGroup, Program<Entrant>> groups = completePrograms.get(formativeOrgUnit);

            if (fromNewPage)
                elementList.add(startIndex++, pageBreak);
            fromNewPage = true;
            List<IRtfElement> orgUnitFullTitle = new RtfString().boldBegin().fontSize(28).append(formativeOrgUnit.getTitle()).boldEnd().par().toList();
            elementList.addAll(startIndex, orgUnitFullTitle);
            startIndex += orgUnitFullTitle.size();
            elementList.add(startIndex++, pard);
            elementList.add(startIndex++, par);


            for (CompetitionGroup cg : getSortedCompetitionGroupList(groups.keySet()))
            {
                Program<Entrant> program = groups.get(cg);
                if (program.isEmpty())
                    continue;

                // вставляем таблицу
                RtfTable table = tableTemplate.getClone();
                elementList.add(startIndex++, table);

                // составляем список направлений
                Set<EnrollmentDirection> enrollmentDirectionSet = new HashSet<>();
                for (List<RequestedEnrollmentDirection> dirs : program.values())
                {
                    for (RequestedEnrollmentDirection rDir : dirs)
                        enrollmentDirectionSet.add(rDir.getEnrollmentDirection());
                }

                injectModifier.put("competitionGroup", cg== null? "" : cg.getTitle());
                injectModifier.put("completeProgramDetails", getDetails(getSortedByTitle(enrollmentDirectionSet), budget));
                injectModifier.modify(elementList);

                // записываем данные в документ
                UniRtfUtil.modify(table, getCompleteDevelopConditionTable(program, competitionKind2Priority, budget));

                // отбивка абзацев
                elementList.add(startIndex++, pard);
                elementList.add(startIndex++, par);
            }
        }
    }

    /**
     * @param completeProgram          данные полной программы обучения
     * @param competitionKind2Priority приоритеты видов конкурса
     * @param budget                   по бюджету или по контракту
     * @return ячейки таблицы полной программы
     */
    private String[][] getCompleteDevelopConditionTable(Map<Entrant, List<RequestedEnrollmentDirection>> completeProgram, Map<CompetitionKind, Integer> competitionKind2Priority, boolean budget)
    {
        // формируем список строк
        List<CompleteProgramRow> rows = new ArrayList<>(completeProgram.size());

        for (Map.Entry<Entrant, List<RequestedEnrollmentDirection>> entry : completeProgram.entrySet())
        {
            Entrant entrant = entry.getKey();
            List<RequestedEnrollmentDirection> directions = entry.getValue();
            Collections.sort(directions, Comparator.<RequestedEnrollmentDirection>
                    comparingLong(red -> red.getEntrantRequest().getId()).thenComparingInt(RequestedEnrollmentDirectionGen::getPriority));

            RequestedEnrollmentDirection firstDirection = directions.get(0);

            CompleteProgramRow row = new CompleteProgramRow(firstDirection);
            row.number = entrant.getPersonalNumber();
            row.fio = entrant.getPerson().getFullFio();
            row.competitionKindOrBenefitsExistence = getCompetitionKindStr(firstDirection, budget);
            row.marksSum = getMarksSum(firstDirection);
            row.directionsPriorities = UniStringUtils.join(directions, RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION +
                    "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT +
                    "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL +
                    "." + EducationLevelsHighSchool.P_SHORT_TITLE, ", ");
            row.originalDocumentHandedIn = isOriginalDocumentHandedIn(directions);

            rows.add(row);
        }
        Collections.sort(rows, new RequestedEnrollmentDirectionComparator(competitionKind2Priority));

        // преобразовываем его в таблицу ячеек и проставляем номера строк
        List<String[]> result = new ArrayList<>(rows.size());
        for (int i = 0; i < rows.size(); i++)
        {
            String[] row = rows.get(i).toStringArray();
            row[0] = Integer.toString(i + 1);
            result.add(row);
        }
        return result.toArray(new String[][]{});
    }

    @SuppressWarnings({"deprecation"})
    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "fefuEntrantCompetitionGroupRating");
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument document = template.getClone();
        List<IRtfElement> elementList = document.getElementList();

        List<EnrollmentDirection> enrollmentDirections = getEnrollmentDirections();
        boolean budget = report.getCompensationType().isBudget();

        // инжектим данные
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        injectModifier.put("compensationTypeAndDevelopForm", getCompensationTypeAndDevelopForm(budget, competitionGroup));
        injectModifier.put("column", (budget) ? "ВК" : "Л");
        injectModifier.modify(document);

        // создаем таблицы для программ обучения

        // одна таблица полной программы
        Map<OrgUnit, Map<CompetitionGroup, Program<Entrant>>> completePrograms = new HashMap<>();

        // для каждого направления приема своя таблица по неполной программе
        Program<EnrollmentDirection> incompleteProgram = new Program<>();

        // пустые таблицы тут тоже нужны
        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            if (!isFullDevelopCondition(enrollmentDirection))
            {
                incompleteProgram.get(enrollmentDirection); // safeMap положит пустой список направлений
            }
        }

        // для каждого направления приема своя таблица по второму высшему образованию
        Program<EnrollmentDirection> secondProgram = new Program<>();

        MQBuilder directionsBuilder = getRequestedEnrollmentDirectionBuilder(enrollmentDirections);
        dataUtil = new EntrantDataUtil(session, report.getEnrollmentCampaign(), directionsBuilder);

        Program<Entrant> fullProgram;
        CompetitionGroup cg;

        // рассортировать выбранные направления приема по таблицам
        for (Map.Entry<EnrollmentDirection, List<RequestedEnrollmentDirection>> entry : getRequestedEnrollmentDirections(enrollmentDirections, report.getDateFrom(), report.getDateTo(), budget).entrySet())
        {
            EnrollmentDirection enrollmentDirection = entry.getKey();
            if (entry.getValue().isEmpty())
                continue;

            cg = enrollmentDirection.getCompetitionGroup();

            OrgUnit formativeOrgUnit = enrollmentDirection.getEducationOrgUnit().getFormativeOrgUnit();
            Map<CompetitionGroup, Program<Entrant>> completeProgram = completePrograms.get(formativeOrgUnit);
            if (completeProgram == null)
            {
                completeProgram = new HashMap<>();
                completePrograms.put(formativeOrgUnit, completeProgram);
            }

            if ((fullProgram = completeProgram.get(cg)) == null)
            {
                fullProgram = new Program<>();
                completeProgram.put(cg, fullProgram);
            }

            for (RequestedEnrollmentDirection requestedEnrollmentDirection : entry.getValue())
            {
                if (!isSecondHighEducation(requestedEnrollmentDirection))
                {
                    if (isFullDevelopCondition(enrollmentDirection))
                        fullProgram.get(requestedEnrollmentDirection.getEntrantRequest().getEntrant()).add(requestedEnrollmentDirection);
                    else
                        incompleteProgram.get(enrollmentDirection).add(requestedEnrollmentDirection);
                }
                else
                {
                    secondProgram.get(enrollmentDirection).add(requestedEnrollmentDirection);
                }
            }
        }

        // подготовить приоритеты видов конкурса в данной приемной кампании
        Map<CompetitionKind, Integer> competitionKind2Priority = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(report.getEnrollmentCampaign());

        // получить шаблон таблицы по КГ
        RtfTable table = (RtfTable) UniRtfUtil.findElement(elementList, "T1");
        int index = elementList.indexOf(table);
        // и удалить его из документа, вместе с абзацем
        elementList.remove(index - 1);
        elementList.remove(index - 1);

        // заполнить таблицы по КГ полной программы
        injectCompetitionGroupsTables(table, elementList, index, completePrograms, competitionKind2Priority, budget);


        // получить шаблон таблицы для остальных программ
        table = (RtfTable) UniRtfUtil.findElement(elementList, "T2");

        // и удалить его из документа, вместе с абзацем
        index = elementList.indexOf(table);
        elementList.remove(index - 1);
        elementList.remove(index - 1);

        // заполнить таблицы остальных программ
        if (!incompleteProgram.isEmpty())
            index = fillOtherTables(incompleteProgram, table, elementList, index, competitionKind2Priority, true, budget);
        if (!secondProgram.isEmpty())
            fillOtherTables(secondProgram, table, elementList, index, competitionKind2Priority, false, budget);

        return RtfUtil.toByteArray(document);
    }

    @SuppressWarnings("unchecked")
    private Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> getRequestedEnrollmentDirections(List<EnrollmentDirection> enrollmentDirections, Date from, Date to, boolean budget)
    {
        Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> result = new LinkedHashMap<>();
        if (enrollmentDirections.isEmpty())
            return result;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "req")
                .column(property("req"))
                .joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().fromAlias("req"), "enreq");

        builder.where(in(property("req", RequestedEnrollmentDirection.enrollmentDirection()), enrollmentDirections));

        if (from != null)
            builder.where(ge(property(EntrantRequest.regDate().fromAlias("req")), valueTimestamp(CoreDateUtils.getDayFirstTimeMoment(from))));
        if (to != null)
            builder.where(le(property(EntrantRequest.regDate().fromAlias("req")), valueTimestamp(CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(to, Calendar.DAY_OF_YEAR, 1)))));

        builder.where(notIn(property(RequestedEnrollmentDirection.state().code().fromAlias("req")), new String[]{UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY}))
                .where(eq(property("enreq", EntrantRequest.entrant().archival()), value(Boolean.FALSE)))
                .where(eq(property("req", RequestedEnrollmentDirection.compensationType().code()), value((budget) ? UniDefines.COMPENSATION_TYPE_BUDGET : UniDefines.COMPENSATION_TYPE_CONTRACT)));


        if (!report.isIncludeForeignPerson())
            builder.where(eq(property(EntrantRequest.entrant().person().identityCard().citizenship().code().fromAlias("enreq")), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));

        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            result.put(enrollmentDirection, new ArrayList<>());
        }

        List<RequestedEnrollmentDirection> requestList = DataAccessServices.dao().getList(builder);
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestList)
        {
            result.get(requestedEnrollmentDirection.getEnrollmentDirection()).add(requestedEnrollmentDirection);
        }
        return result;
    }

    private MQBuilder getRequestedEnrollmentDirectionBuilder(List<EnrollmentDirection> enrollmentDirections)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        builder.add(MQExpression.in("red", RequestedEnrollmentDirection.enrollmentDirection().s(), enrollmentDirections));
        builder.addJoin("red", RequestedEnrollmentDirection.entrantRequest().s(), "r");
        builder.add(UniMQExpression.betweenDate("r", EntrantRequest.regDate().s(), report.getDateFrom(), report.getDateTo()));
        builder.add(MQExpression.notIn("red", RequestedEnrollmentDirection.state().code().s(), UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.compensationType().s(), report.getCompensationType()));
        builder.add(MQExpression.eq("r", EntrantRequest.entrant().archival().s(), Boolean.FALSE));
        if (!report.isIncludeForeignPerson())
            builder.add(MQExpression.eq("r", EntrantRequest.entrant().person().identityCard().citizenship().code().s(), IKladrDefines.RUSSIA_COUNTRY_CODE));
        return builder;
    }

    private String getCompetitionKindStr(RequestedEnrollmentDirection dir, boolean budget)
    {
        CompetitionKind competitionKind = dir.getCompetitionKind();
        if (competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES) ||
                competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION))
        {
            if (budget)
                return competitionKind.getShortTitle();
            else
            {
                if (competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
                    return competitionKind.getShortTitle();
                else if (competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION))
                    return "льготы";
            }
        }

        return "";
    }

    /**
     * Формирует таблицы данных кроме полной программы обучения
     *
     * @param program                  данные программы
     * @param tableTemplate            шаблон таблицы
     * @param elementList              список элементов в документе
     * @param startIndex               после какого вставлять таблицу
     * @param competitionKind2Priority приоритеты видов конкурса
     * @param firstHighEducation       первое высшее для абитуриента или нет
     * @param budget                   по бюджету или по контракту
     * @return индекс элемента после вставки
     */
    private int fillOtherTables(Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> program, RtfTable tableTemplate, List<IRtfElement> elementList, int startIndex, Map<CompetitionKind, Integer> competitionKind2Priority, boolean firstHighEducation, boolean budget)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        IRtfControl par = elementFactory.createRtfControl(IRtfData.PAR);
        IRtfControl pard = elementFactory.createRtfControl(IRtfData.PARD);

        // для каждой таблицы
        for (Map.Entry<EnrollmentDirection, List<RequestedEnrollmentDirection>> entry : program.entrySet())
        {
            // если пуста и второе высшее, то не показывать
            List<RequestedEnrollmentDirection> requestedDirections = entry.getValue();
            if (!firstHighEducation && requestedDirections.isEmpty())
                continue;

            // отбивка абзацев
            elementList.add(startIndex++, pard);
            elementList.add(startIndex++, par);

            EnrollmentDirection direction = entry.getKey();
            EducationOrgUnit educationOrgUnit = direction.getEducationOrgUnit();

            // информация по направлению приема таблицы
            StringBuilder details = new StringBuilder();
            details.append("по ").append(direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getDativeCaseSimpleShortTitle()).append(": ");
            details.append(educationOrgUnit.getEducationLevelHighSchool().getPrintTitle());
            if (!firstHighEducation)
            {
                details.append(", второе высшее образование");
            }
            String developCondition = _developConditionByCode.get(educationOrgUnit.getDevelopCondition().getCode());
            if (developCondition != null)
            {
                details.append(", с освоением образовательной программы по ").append(developCondition).append(" программе");
            }

            elementList.add(startIndex++, elementFactory.createRtfText(details.toString()));

            // отбивка абзацев
            elementList.add(startIndex++, par);
            elementList.add(startIndex++, par);

            // вставляем таблицу
            RtfTable table = tableTemplate.getClone();
            elementList.add(startIndex++, table);

            // формируем список строк
            List<Row> rows = new ArrayList<>();
            for (RequestedEnrollmentDirection requestedDirection : requestedDirections)
            {
                rows.add(createRow(requestedDirection, firstHighEducation, budget));
            }
            Collections.sort(rows, new RequestedEnrollmentDirectionComparator(competitionKind2Priority));

            // формируем ячейки таблицы и проставляем номер строк
            List<String[]> tableData = new ArrayList<>(rows.size());
            for (int i = 0; i < rows.size(); i++)
            {
                String[] row = rows.get(i).toStringArray();
                row[0] = Integer.toString(i + 1);
                tableData.add(row);
            }

            // записываем данные в документ
            UniRtfUtil.modify(table, tableData.toArray(new String[][]{}));
        }

        return startIndex;
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @param firstHighEducation           первое высшее образование для абитуриента или нет
     * @param budget                       по бюджету или по контракту
     * @return строка таблицы, кроме полной программы обучения для первого высшего образования
     */
    private Row createRow(RequestedEnrollmentDirection requestedEnrollmentDirection, boolean firstHighEducation, boolean budget)
    {
        Row row = (firstHighEducation) ? new IncompleteProgramRow(requestedEnrollmentDirection) : new SecondHighEducationRow(requestedEnrollmentDirection);
        Entrant entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
        row.number = entrant.getPersonalNumber();
        row.fio = entrant.getPerson().getFullFio();
        row.competitionKindOrBenefitsExistence = getCompetitionKindStr(requestedEnrollmentDirection, budget);
        row.marksSum = getMarksSum(requestedEnrollmentDirection);
        row.averageEduInstMark = dataUtil.getAverageEduInstMark(entrant.getId());
        row.originalDocumentHandedIn = requestedEnrollmentDirection.isOriginalDocumentHandedIn();
        return row;
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return true если в качестве второго высшего образования, false в противном случае
     */
    private boolean isSecondHighEducation(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return requestedEnrollmentDirection.getStudentCategory().getCode().equals(UniDefines.STUDENT_CATEGORY_SECOND_HIGH);
    }

    /**
     * @param enrollmentDirection направление приема
     * @return true если полная программа обучения, false в противном случае
     */
    private boolean isFullDevelopCondition(EnrollmentDirection enrollmentDirection)
    {
        return enrollmentDirection.getEducationOrgUnit().getDevelopCondition().getCode().equals(UniDefines.DEVELOP_CONDITION_FULL_TIME);
    }

    /**
     * @param budget           по бюджету или контракту
     * @param competitionGroup конкурсная группа
     * @return детализация рейтинг каких абитуриентов строится
     */
    private String getCompensationTypeAndDevelopForm(boolean budget, CompetitionGroup competitionGroup)
    {
        StringBuilder result = new StringBuilder();
        result.append((budget) ? "поступающих на места, финансируемые из федерального бюджета, по " : "поступающих на места с оплатой стоимости обучения по ");

        Criteria criteria = session.createCriteria(EnrollmentDirection.class);
        criteria.createAlias(EnrollmentDirection.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        criteria.add(Restrictions.eq(EnrollmentDirection.L_COMPETITION_GROUP, competitionGroup));
        criteria.setProjection(Projections.property("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_FORM));
        criteria.setMaxResults(1);
        DevelopForm developForm = (DevelopForm) criteria.uniqueResult();
        if (developForm != null)
            result.append(_developFormByCode.get(developForm.getCode())).append(" форме обучения");

        return result.toString();
    }

    /**
     * @param enrollmentDirections направления приема
     * @return детализация по каким направлениям приема строится рейтинг
     */
    private List<IRtfElement> getDetails(List<EnrollmentDirection> enrollmentDirections, boolean isBudget)
    {
        List<IRtfElement> result = new ArrayList<>();

        List<String> directionsTitles = new ArrayList<>();
        List<String> specialitiesTitles = new ArrayList<>();
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        // План приема

        // рассортировываем
        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            Integer plan;
            if (isBudget)
            {
                plan = enrollmentDirection.getMinisterialPlan();
                if (plan != null && enrollmentDirection.getTargetAdmissionPlanBudget() != null)
                    plan -= enrollmentDirection.getTargetAdmissionPlanBudget();
            }
            else
            {
                plan = enrollmentDirection.getContractPlan();
                if (plan != null && enrollmentDirection.getTargetAdmissionPlanContract() != null)
                    plan -= enrollmentDirection.getTargetAdmissionPlanContract();
            }
            String planStr = plan != null ? " (" + plan + ")" : "";
            (isSpeciality(enrollmentDirection) ? specialitiesTitles : directionsTitles)
                    .add(enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle() + planStr);
        }

        // выводим направления
        if (!directionsTitles.isEmpty())
        {
            result.add(elementFactory.createRtfText("по направлениям: " + StringUtils.join(directionsTitles, ", ")));

            if (!specialitiesTitles.isEmpty())
                result.add(elementFactory.createRtfControl(IRtfData.PAR));
        }

        // выводим специальности
        if (!specialitiesTitles.isEmpty())
        {
            result.add(elementFactory.createRtfText("по специальностям: " + StringUtils.join(specialitiesTitles, ", ")));
        }

        return result;
    }

    private List<EnrollmentDirection> getEnrollmentDirections()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e").column("e")
                .where(eq(
                        property(EnrollmentDirection.enrollmentCampaign().fromAlias("e")),
                        value(report.getEnrollmentCampaign())
                ))
                .order(property(EnrollmentDirection.title().fromAlias("e")));

        if (competitionGroup != null)
        {
            builder.where(eq(
                    property(EnrollmentDirection.competitionGroup().fromAlias("e")),
                    value(competitionGroup)
            ));
        }
        else if (qualifications != null || formativeOrgUnit != null || territorialOrgUnit != null)
        {
            DQLSelectBuilder cgBuilder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "dir")
                    .predicate(DQLPredicateType.distinct)
                    .column(property(EnrollmentDirection.competitionGroup().id().fromAlias("dir")))
                    .where(eq(
                            property(EnrollmentDirection.enrollmentCampaign().fromAlias("dir")),
                            value(report.getEnrollmentCampaign())
                    ))
                    .where(isNotNull(property(EnrollmentDirection.competitionGroup().fromAlias("dir"))));

            if (qualifications != null)
            {
                cgBuilder.where(in(
                        property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("dir")),
                        qualifications
                ));
            }

            if (formativeOrgUnit != null)
            {
                cgBuilder.where(eq(
                        property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().fromAlias("dir")),
                        value(formativeOrgUnit)
                ));
            }

            if (territorialOrgUnit != null)
            {
                cgBuilder.where(eq(
                        property(EnrollmentDirection.educationOrgUnit().territorialOrgUnit().fromAlias("dir")),
                        value(territorialOrgUnit)
                ));
            }

            builder.where(in(
                    property(EnrollmentDirection.competitionGroup().id().fromAlias("e")),
                    cgBuilder.buildQuery()
            ));
        }

        return builder.createStatement(session).list();
    }

    /**
     * @param enrollmentDirection направление приема
     * @return true если данное направление является специальностью, false в противном случае
     */
    private boolean isSpeciality(EnrollmentDirection enrollmentDirection)
    {
        return enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().isSpeciality();
    }

    /**
     * @param requestedEnrollmentDirections выбранные направления приема
     * @return true если сданы оригиналы документов хотя бы по одному направлению, false в противном случае
     */
    private boolean isOriginalDocumentHandedIn(List<RequestedEnrollmentDirection> requestedEnrollmentDirections)
    {
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections)
        {
            if (requestedEnrollmentDirection.isOriginalDocumentHandedIn())
                return true;
        }
        return false;
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return сумма баллов по данному направлению
     */
    private Double getMarksSum(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        double mark = dataUtil.getFinalMark(requestedEnrollmentDirection);
        return mark == 0 ? null : mark;
    }

    /**
     * Данные по какой-то программе обучения
     *
     * @author agolubenko
     * @since 21.06.2009
     */
    private static class Program<K> extends LinkedHashMap<K, List<RequestedEnrollmentDirection>>
    {
        private static final long serialVersionUID = 1L;

        @SuppressWarnings("unchecked")
        @Override
        public List<RequestedEnrollmentDirection> get(Object key)
        {
            List<RequestedEnrollmentDirection> result = super.get(key);
            if (null != result)
                return result;

            put((K) key, result = new ArrayList<>());
            return result;
        }
    }

    /**
     * Строка таблицы полной формы обучения первого высшего образования
     *
     * @author agolubenko
     * @since 21.06.2009
     */
    private static class CompleteProgramRow implements IReportRow
    {
        @Override
        public Double getFinalMark()
        {
            return marksSum;
        }

        @Override
        public String getFio()
        {
            return fio;
        }

        public String getNumber()
        {
            return number;
        }

        RequestedEnrollmentDirection requestedEnrollmentDirection;
        String number;
        String fio;
        String competitionKindOrBenefitsExistence;
        Double marksSum;
        String directionsPriorities;
        boolean originalDocumentHandedIn;

        public CompleteProgramRow(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            this.requestedEnrollmentDirection = requestedEnrollmentDirection;
        }

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return requestedEnrollmentDirection;
        }

        @Override
        public Double getProfileMark()
        {
            return (Double) FastBeanUtils.getValue(getRequestedEnrollmentDirection(), RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
        }

        String[] toStringArray()
        {
            String[] result = new String[7];
            result[1] = number;
            result[2] = fio;
            result[3] = competitionKindOrBenefitsExistence;
            result[4] = (marksSum != null) ? String.format("%.0f", marksSum) : null;
            result[5] = directionsPriorities;
            result[6] = (originalDocumentHandedIn) ? "оригиналы" : "копии";
            return result;
        }
    }

    /**
     * Строка таблицы неполной программы обучения
     *
     * @author agolubenko
     * @since 21.06.2009
     */
    private static class IncompleteProgramRow extends Row
    {
        public IncompleteProgramRow(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            super(requestedEnrollmentDirection);
        }
    }

    /**
     * Строка таблицы второго высшего образования
     *
     * @author agolubenko
     * @since 21.06.2009
     */
    private static class SecondHighEducationRow extends Row
    {
        public SecondHighEducationRow(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            super(requestedEnrollmentDirection);
        }
    }

    private static abstract class Row implements IReportRow
    {
        RequestedEnrollmentDirection requestedEnrollmentDirection;
        String number;
        String fio;
        String competitionKindOrBenefitsExistence;
        Double marksSum;
        Double averageEduInstMark;
        boolean originalDocumentHandedIn;

        public Row(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            this.requestedEnrollmentDirection = requestedEnrollmentDirection;
        }

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return requestedEnrollmentDirection;
        }

        @Override
        public Double getProfileMark()
        {
            return (Double) FastBeanUtils.getValue(getRequestedEnrollmentDirection(), RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
        }

        @Override
        public Double getFinalMark()
        {
            return marksSum;
        }

        public String getNumber()
        {
            return number;
        }

        @Override
        public String getFio()
        {
            return fio;
        }

        String[] toStringArray()
        {
            String[] result = new String[6];
            result[1] = number;
            result[2] = fio;
            result[3] = competitionKindOrBenefitsExistence;
            result[4] = (marksSum != null) ? String.format("%.0f", marksSum) : null;
            result[5] = (originalDocumentHandedIn) ? "оригиналы" : "копии";
            return result;
        }
    }

    private interface IReportRow
    {
        RequestedEnrollmentDirection getRequestedEnrollmentDirection();

        Double getFinalMark();

        Double getProfileMark();

        String getFio();
    }

    class RequestedEnrollmentDirectionComparator implements Comparator<IReportRow>
    {
        private Map<CompetitionKind, Integer> _competitionKindPriorities;

        /**
         * @param competitionKindPriorities приоритеты видов конкурса (по ключу null должен лежать "Целевой прием")
         */
        public RequestedEnrollmentDirectionComparator(Map<CompetitionKind, Integer> competitionKindPriorities)
        {
            _competitionKindPriorities = competitionKindPriorities;
        }

        @Override
        public int compare(IReportRow o1, IReportRow o2)
        {
            // сравнить по приоритетам видов конкурса
            int result = _competitionKindPriorities.get(o1.getRequestedEnrollmentDirection().getCompetitionKind())
                    - _competitionKindPriorities.get(o2.getRequestedEnrollmentDirection().getCompetitionKind());

            // сначала сравниваем по сумме баллов
            if (result == 0)
                result = -UniBaseUtils.compare(o1.getFinalMark(), o2.getFinalMark(), true);

            // теперь по баллам за профильное вступительное
            if (result == 0)
                result = -UniBaseUtils.compare(o1.getProfileMark(), o2.getProfileMark(), true);

            // все осмысленное кончилось, теперь по фио
            if (result == 0)
                result = CommonCollator.RUSSIAN_COLLATOR.compare(o1.getFio(), o2.getFio());

            if (result == 0)
                result = Long.compare(o1.getRequestedEnrollmentDirection().getId(), o2.getRequestedEnrollmentDirection().getId());

            return result;
        }
    }
}