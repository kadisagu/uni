/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 16.07.2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {})
@XmlRootElement(name = "DataExchangePackage")
public class DataExchangePackage
{
    @XmlElements({
            @XmlElement(name = "Object", type = EDocumentType.class),
            @XmlElement(name = "Object", type = TaskType.class)
    })

    protected List<Object> objects = new ArrayList<>();

    public List<Object> getObjects()
    {
        return objects;
    }

    public void setObjects(List<Object> objects)
    {
        this.objects = objects;
    }
}