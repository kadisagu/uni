package ru.tandemservice.unifefu.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unifefu.entity.report.FefuEntrantReport;
import ru.tandemservice.unifefu.entity.report.FefuEntrantRequestReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет по заявлениям абитуриентов (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEntrantRequestReportGen extends FefuEntrantReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.report.FefuEntrantRequestReport";
    public static final String ENTITY_NAME = "fefuEntrantRequestReport";
    public static final int VERSION_HASH = -840877804;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_PERIOD_TITLE = "periodTitle";

    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuEntrantRequestReportGen)
        {
            setDateFrom(((FefuEntrantRequestReport)another).getDateFrom());
            setDateTo(((FefuEntrantRequestReport)another).getDateTo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEntrantRequestReportGen> extends FefuEntrantReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEntrantRequestReport.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("FefuEntrantRequestReport is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEntrantRequestReport> _dslPath = new Path<FefuEntrantRequestReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEntrantRequestReport");
    }
            

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRequestReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRequestReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRequestReport#getPeriodTitle()
     */
    public static SupportedPropertyPath<String> periodTitle()
    {
        return _dslPath.periodTitle();
    }

    public static class Path<E extends FefuEntrantRequestReport> extends FefuEntrantReport.Path<E>
    {
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private SupportedPropertyPath<String> _periodTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRequestReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(FefuEntrantRequestReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRequestReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(FefuEntrantRequestReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantRequestReport#getPeriodTitle()
     */
        public SupportedPropertyPath<String> periodTitle()
        {
            if(_periodTitle == null )
                _periodTitle = new SupportedPropertyPath<String>(FefuEntrantRequestReportGen.P_PERIOD_TITLE, this);
            return _periodTitle;
        }

        public Class getEntityClass()
        {
            return FefuEntrantRequestReport.class;
        }

        public String getEntityName()
        {
            return "fefuEntrantRequestReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getPeriodTitle();
}
