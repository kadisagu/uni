/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e82.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.e82.AddEdit.Model;

import java.util.stream.Collectors;

/**
 * @author Dmitry Seleznev
 * @since 05.12.2012
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e82.AddEdit.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setTermsList(model.getTermsList().stream().limit(14).collect(Collectors.toList()));
    }
}