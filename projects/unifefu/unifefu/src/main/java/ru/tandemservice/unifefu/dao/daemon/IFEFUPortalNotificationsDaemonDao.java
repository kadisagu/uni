/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuPortalNotification;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 14.06.2013
 */
public interface IFEFUPortalNotificationsDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IFEFUPortalNotificationsDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IFEFUPortalNotificationsDaemonDao> instance = new SpringBeanCache<>(IFEFUPortalNotificationsDaemonDao.class.getName());

    /**
     * Посылает зарегистрированные, но не отправленные уведомления в портал ДВФУ
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doSendPortalNotifications();

    /**
     * Регистрирует уведомления для отправки
     *
     * @param notifications - уведомления для регистрации
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doRegisterPortalNotifications(List<FefuPortalNotification> notifications);

    /**
     * Возвращает список абитуриентов в рамках приемной кампании с GUID'ами персон
     *
     * @param campaign - приемная кампания
     * @return - список абитуриентов с GUID'ами персон
     */
    List<CoreCollectionUtils.Pair<FefuNsiIds, Entrant>> getEntrantsList(EnrollmentCampaign campaign);

    /**
     * Возвращает список абитуриентов, включенных в приказы о зачислении в рамках приемной кампании с GUID'ами персон
     *
     * @param campaign - приемная кампания
     * @return - список абитуриентов с GUID'ами персон
     */
    List<CoreCollectionUtils.Pair<FefuNsiIds, Entrant>> getEntrantsFromOrders(EnrollmentCampaign campaign);

    /**
     * Возвращает список студентов предзачисления с GUID'ами персон
     *
     * @param campaign - приемная кампания
     * @return - список студентов предзачисления с GUID'ами персон
     */
    List<CoreCollectionUtils.Pair<FefuNsiIds, PreliminaryEnrollmentStudent>> getPreliminaryEnrollmentStudents(EnrollmentCampaign campaign);

    /**
     * Возвращает список абитуриентов, рекомендованных к зачислению с GUID'ами персон
     *
     * @param campaign - приемная кампания
     * @return - список абитуриентов, рекомендованных к зачислению с GUID'ами персон
     */
    List<CoreCollectionUtils.Pair<FefuNsiIds, EcgEntrantRecommended>> getEntrantsRecommended(EnrollmentCampaign campaign);
}