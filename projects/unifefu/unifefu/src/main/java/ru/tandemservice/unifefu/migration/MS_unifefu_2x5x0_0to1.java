package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.5.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность bbCourse

		// создана новая сущность
        if (!tool.tableExists("bbcourse_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("bbcourse_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("blackboardid_p", DBType.createVarchar(25)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("archive_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("bbCourse");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbCourse2EppRegElementRel

		// создана новая сущность
        if (!tool.tableExists("bbcourse2eppregelementrel_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("bbcourse2eppregelementrel_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("bbcourse_id", DBType.LONG).setNullable(false), 
				new DBColumn("eppregistryelement_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("bbCourse2EppRegElementRel");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbCourse2PpsEntryRel

		// создана новая сущность
        if (!tool.tableExists("bbcourse2ppsentryrel_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("bbcourse2ppsentryrel_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("bbcourse_id", DBType.LONG).setNullable(false), 
				new DBColumn("ppsentry_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("bbCourse2PpsEntryRel");

		}


    }
}