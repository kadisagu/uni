package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion;
import ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersionBlock;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * mdbViewEppEduplanversionBlock
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewEppEduPlanVersionBlockGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersionBlock";
    public static final String ENTITY_NAME = "mdbViewEppEduPlanVersionBlock";
    public static final int VERSION_HASH = -821155300;
    private static IEntityMeta ENTITY_META;

    public static final String L_MDB_VIEW_EPP_EDU_PLAN_VERSION = "mdbViewEppEduPlanVersion";
    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";

    private MdbViewEppEduPlanVersion _mdbViewEppEduPlanVersion;     // Версия учебного плана
    private EducationLevelsHighSchool _educationLevelsHighSchool;     // Направление подготовки (специальность) ОУ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     */
    @NotNull
    public MdbViewEppEduPlanVersion getMdbViewEppEduPlanVersion()
    {
        return _mdbViewEppEduPlanVersion;
    }

    /**
     * @param mdbViewEppEduPlanVersion Версия учебного плана. Свойство не может быть null.
     */
    public void setMdbViewEppEduPlanVersion(MdbViewEppEduPlanVersion mdbViewEppEduPlanVersion)
    {
        dirty(_mdbViewEppEduPlanVersion, mdbViewEppEduPlanVersion);
        _mdbViewEppEduPlanVersion = mdbViewEppEduPlanVersion;
    }

    /**
     * @return Направление подготовки (специальность) ОУ. Свойство не может быть null.
     */
    @NotNull
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    /**
     * @param educationLevelsHighSchool Направление подготовки (специальность) ОУ. Свойство не может быть null.
     */
    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        dirty(_educationLevelsHighSchool, educationLevelsHighSchool);
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewEppEduPlanVersionBlockGen)
        {
            setMdbViewEppEduPlanVersion(((MdbViewEppEduPlanVersionBlock)another).getMdbViewEppEduPlanVersion());
            setEducationLevelsHighSchool(((MdbViewEppEduPlanVersionBlock)another).getEducationLevelsHighSchool());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewEppEduPlanVersionBlockGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewEppEduPlanVersionBlock.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewEppEduPlanVersionBlock();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mdbViewEppEduPlanVersion":
                    return obj.getMdbViewEppEduPlanVersion();
                case "educationLevelsHighSchool":
                    return obj.getEducationLevelsHighSchool();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mdbViewEppEduPlanVersion":
                    obj.setMdbViewEppEduPlanVersion((MdbViewEppEduPlanVersion) value);
                    return;
                case "educationLevelsHighSchool":
                    obj.setEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mdbViewEppEduPlanVersion":
                        return true;
                case "educationLevelsHighSchool":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mdbViewEppEduPlanVersion":
                    return true;
                case "educationLevelsHighSchool":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mdbViewEppEduPlanVersion":
                    return MdbViewEppEduPlanVersion.class;
                case "educationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewEppEduPlanVersionBlock> _dslPath = new Path<MdbViewEppEduPlanVersionBlock>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewEppEduPlanVersionBlock");
    }
            

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersionBlock#getMdbViewEppEduPlanVersion()
     */
    public static MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> mdbViewEppEduPlanVersion()
    {
        return _dslPath.mdbViewEppEduPlanVersion();
    }

    /**
     * @return Направление подготовки (специальность) ОУ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersionBlock#getEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
    {
        return _dslPath.educationLevelsHighSchool();
    }

    public static class Path<E extends MdbViewEppEduPlanVersionBlock> extends EntityPath<E>
    {
        private MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> _mdbViewEppEduPlanVersion;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchool;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersionBlock#getMdbViewEppEduPlanVersion()
     */
        public MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> mdbViewEppEduPlanVersion()
        {
            if(_mdbViewEppEduPlanVersion == null )
                _mdbViewEppEduPlanVersion = new MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion>(L_MDB_VIEW_EPP_EDU_PLAN_VERSION, this);
            return _mdbViewEppEduPlanVersion;
        }

    /**
     * @return Направление подготовки (специальность) ОУ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersionBlock#getEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
        {
            if(_educationLevelsHighSchool == null )
                _educationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _educationLevelsHighSchool;
        }

        public Class getEntityClass()
        {
            return MdbViewEppEduPlanVersionBlock.class;
        }

        public String getEntityName()
        {
            return "mdbViewEppEduPlanVersionBlock";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
