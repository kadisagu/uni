package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О зачислении слушателей ДПО/ДО»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEnrollStuDPOListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract";
    public static final String ENTITY_NAME = "fefuEnrollStuDPOListExtract";
    public static final int VERSION_HASH = -1293743223;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENROLL_DATE = "enrollDate";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String L_DPO_PROGRAM_OLD = "dpoProgramOld";
    public static final String L_DPO_PROGRAM_NEW = "dpoProgramNew";

    private Date _enrollDate;     // Дата зачисления
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private FefuAdditionalProfessionalEducationProgram _dpoProgramOld;     // Предыдущая программа ДПО/ДО
    private FefuAdditionalProfessionalEducationProgram _dpoProgramNew;     // Новая программа ДПО/ДО

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата зачисления. Свойство не может быть null.
     */
    @NotNull
    public Date getEnrollDate()
    {
        return _enrollDate;
    }

    /**
     * @param enrollDate Дата зачисления. Свойство не может быть null.
     */
    public void setEnrollDate(Date enrollDate)
    {
        dirty(_enrollDate, enrollDate);
        _enrollDate = enrollDate;
    }

    /**
     * @return Предыдущее состояние студента.
     */
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Предыдущая программа ДПО/ДО.
     */
    public FefuAdditionalProfessionalEducationProgram getDpoProgramOld()
    {
        return _dpoProgramOld;
    }

    /**
     * @param dpoProgramOld Предыдущая программа ДПО/ДО.
     */
    public void setDpoProgramOld(FefuAdditionalProfessionalEducationProgram dpoProgramOld)
    {
        dirty(_dpoProgramOld, dpoProgramOld);
        _dpoProgramOld = dpoProgramOld;
    }

    /**
     * @return Новая программа ДПО/ДО. Свойство не может быть null.
     */
    @NotNull
    public FefuAdditionalProfessionalEducationProgram getDpoProgramNew()
    {
        return _dpoProgramNew;
    }

    /**
     * @param dpoProgramNew Новая программа ДПО/ДО. Свойство не может быть null.
     */
    public void setDpoProgramNew(FefuAdditionalProfessionalEducationProgram dpoProgramNew)
    {
        dirty(_dpoProgramNew, dpoProgramNew);
        _dpoProgramNew = dpoProgramNew;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuEnrollStuDPOListExtractGen)
        {
            setEnrollDate(((FefuEnrollStuDPOListExtract)another).getEnrollDate());
            setStudentStatusOld(((FefuEnrollStuDPOListExtract)another).getStudentStatusOld());
            setDpoProgramOld(((FefuEnrollStuDPOListExtract)another).getDpoProgramOld());
            setDpoProgramNew(((FefuEnrollStuDPOListExtract)another).getDpoProgramNew());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEnrollStuDPOListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEnrollStuDPOListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuEnrollStuDPOListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    return obj.getEnrollDate();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "dpoProgramOld":
                    return obj.getDpoProgramOld();
                case "dpoProgramNew":
                    return obj.getDpoProgramNew();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    obj.setEnrollDate((Date) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "dpoProgramOld":
                    obj.setDpoProgramOld((FefuAdditionalProfessionalEducationProgram) value);
                    return;
                case "dpoProgramNew":
                    obj.setDpoProgramNew((FefuAdditionalProfessionalEducationProgram) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                        return true;
                case "studentStatusOld":
                        return true;
                case "dpoProgramOld":
                        return true;
                case "dpoProgramNew":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    return true;
                case "studentStatusOld":
                    return true;
                case "dpoProgramOld":
                    return true;
                case "dpoProgramNew":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    return Date.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "dpoProgramOld":
                    return FefuAdditionalProfessionalEducationProgram.class;
                case "dpoProgramNew":
                    return FefuAdditionalProfessionalEducationProgram.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEnrollStuDPOListExtract> _dslPath = new Path<FefuEnrollStuDPOListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEnrollStuDPOListExtract");
    }
            

    /**
     * @return Дата зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract#getEnrollDate()
     */
    public static PropertyPath<Date> enrollDate()
    {
        return _dslPath.enrollDate();
    }

    /**
     * @return Предыдущее состояние студента.
     * @see ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Предыдущая программа ДПО/ДО.
     * @see ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract#getDpoProgramOld()
     */
    public static FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> dpoProgramOld()
    {
        return _dslPath.dpoProgramOld();
    }

    /**
     * @return Новая программа ДПО/ДО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract#getDpoProgramNew()
     */
    public static FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> dpoProgramNew()
    {
        return _dslPath.dpoProgramNew();
    }

    public static class Path<E extends FefuEnrollStuDPOListExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<Date> _enrollDate;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> _dpoProgramOld;
        private FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> _dpoProgramNew;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата зачисления. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract#getEnrollDate()
     */
        public PropertyPath<Date> enrollDate()
        {
            if(_enrollDate == null )
                _enrollDate = new PropertyPath<Date>(FefuEnrollStuDPOListExtractGen.P_ENROLL_DATE, this);
            return _enrollDate;
        }

    /**
     * @return Предыдущее состояние студента.
     * @see ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Предыдущая программа ДПО/ДО.
     * @see ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract#getDpoProgramOld()
     */
        public FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> dpoProgramOld()
        {
            if(_dpoProgramOld == null )
                _dpoProgramOld = new FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram>(L_DPO_PROGRAM_OLD, this);
            return _dpoProgramOld;
        }

    /**
     * @return Новая программа ДПО/ДО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract#getDpoProgramNew()
     */
        public FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> dpoProgramNew()
        {
            if(_dpoProgramNew == null )
                _dpoProgramNew = new FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram>(L_DPO_PROGRAM_NEW, this);
            return _dpoProgramNew;
        }

        public Class getEntityClass()
        {
            return FefuEnrollStuDPOListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuEnrollStuDPOListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
