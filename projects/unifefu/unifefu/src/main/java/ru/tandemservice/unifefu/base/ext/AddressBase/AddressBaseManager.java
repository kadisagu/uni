/* $Id$ */
package ru.tandemservice.unifefu.base.ext.AddressBase;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.fias.base.bo.AddressBase.logic.IAddressBaseDAO;
import ru.tandemservice.unifefu.base.ext.AddressBase.logic.AddressBaseDAO;

/**
 * @author Dmitry Seleznev
 * @since 12.01.2015
 */
@Configuration
public class AddressBaseManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IAddressBaseDAO dao()
    {
        return new AddressBaseDAO();
    }
}