/* $Id$ */
package ru.tandemservice.unifefu.base.ext.OrgUnit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexey Lopatin
 * @since 24.09.2013
 */
@Configuration
public class OrgUnitExtManager extends BusinessObjectExtensionManager
{
}
