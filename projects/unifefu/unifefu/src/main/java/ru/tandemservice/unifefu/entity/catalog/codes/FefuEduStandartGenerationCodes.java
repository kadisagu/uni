package ru.tandemservice.unifefu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Поколения образовательных стандартов"
 * Имя сущности : fefuEduStandartGeneration
 * Файл data.xml : unifefu-catalog.data.xml
 */
public interface FefuEduStandartGenerationCodes
{
    /** Константа кода (code) элемента : ГОС 2 (title) */
    String GOS_2 = "1";
    /** Константа кода (code) элемента : ФГОС 3 (title) */
    String FGOS_3 = "2";

    Set<String> CODES = ImmutableSet.of(GOS_2, FGOS_3);
}
