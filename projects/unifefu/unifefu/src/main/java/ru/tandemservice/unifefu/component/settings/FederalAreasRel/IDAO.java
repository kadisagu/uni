package ru.tandemservice.unifefu.component.settings.FederalAreasRel;

import org.hibernate.Session;
import org.w3c.dom.Document;
import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author amakarova
 * @since 25.10.2013
 */
public interface IDAO extends IUniDao<Model>
{
    public void fillData(Document doc);
    public void clearData();
    public void updateData(Document doc);
}
