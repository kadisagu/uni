/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.NSISync.logic.INSISyncDAO;
import ru.tandemservice.unifefu.base.bo.NSISync.logic.INSITestDAO;
import ru.tandemservice.unifefu.base.bo.NSISync.logic.NSISyncDAO;
import ru.tandemservice.unifefu.base.bo.NSISync.logic.NSITestDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import java.util.Arrays;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 07.07.2013
 */
@Configuration
public class NSISyncManager extends BusinessObjectManager
{
    public static final String EVENT_TYPE_OPTION_DS = "eventTypeOptionDS";
    public static final String OPERATION_TYPE_OPTION_DS = "operationTypeOptionDS";
    public static final String MESSAGE_STATUS_OPTION_DS = "messageStatusOptionDS";
    public static final String CATALOG_TYPE_OPTION_DS = "catalogTypeOptionDS";

    public static Long EVENT_TYPE_IN = 0L;
    public static Long EVENT_TYPE_OUT = 1L;
    public static String EVENT_TYPE_IN_CODE = "in";
    public static String EVENT_TYPE_OUT_CODE = "out";
    public static String EVENT_TYPE_IN_TITLE = "Входящее";
    public static String EVENT_TYPE_OUT_TITLE = "Исходящее";
    public static List<IdentifiableWrapper> EVENT_TYPE_LIST = Arrays.asList(
            new IdentifiableWrapper(EVENT_TYPE_IN, EVENT_TYPE_IN_TITLE),
            new IdentifiableWrapper(EVENT_TYPE_OUT, EVENT_TYPE_OUT_TITLE)
    );

    public static Long OPERATION_TYPE_RETRIEVE = 0L;
    public static Long OPERATION_TYPE_INSERT = 1L;
    public static Long OPERATION_TYPE_UPDATE = 2L;
    public static Long OPERATION_TYPE_DELETE = 3L;
    public static List<IdentifiableWrapper> OPERATION_TYPE_LIST = Arrays.asList(
            new IdentifiableWrapper(OPERATION_TYPE_RETRIEVE, FefuNsiRequestsProcessor.OPERATION_TYPE_RETRIEVE),
            new IdentifiableWrapper(OPERATION_TYPE_INSERT, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT),
            new IdentifiableWrapper(OPERATION_TYPE_UPDATE, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE),
            new IdentifiableWrapper(OPERATION_TYPE_DELETE, FefuNsiRequestsProcessor.OPERATION_TYPE_DELETE)
    );

    public static final Long MESSAGE_STATUS_UNKNOWN = -1L;
    public static Long MESSAGE_STATUS_SUCCESS = 0L;
    public static Long MESSAGE_STATUS_WARNING = 1L;
    public static Long MESSAGE_STATUS_ERROR = 2L;
    public static Long MESSAGE_STATUS_TRANSPORT_ERROR = 3L;
    public static Long MESSAGE_STATUS_OUTOFDATE_ERROR = 4L;
    public static String MESSAGE_STATUS_UNKNOWN_TITLE = "В обработке,  либо не известен";
    public static String MESSAGE_STATUS_SUCCESS_TITLE = "Успешно";
    public static String MESSAGE_STATUS_WARNING_TITLE = "Предупреждение";
    public static String MESSAGE_STATUS_ERROR_TITLE = "Ошибка";
    public static String MESSAGE_STATUS_TRANSPORT_ERROR_TITLE = "Транспортная ошибка";
    public static String MESSAGE_STATUS_OUTOFDATE_ERROR_TITLE = "Устаревшие данные";
    public static List<IdentifiableWrapper> MESSAGE_STATUS_LIST = Arrays.asList(
            new IdentifiableWrapper(MESSAGE_STATUS_UNKNOWN, MESSAGE_STATUS_UNKNOWN_TITLE),
            new IdentifiableWrapper(MESSAGE_STATUS_SUCCESS, MESSAGE_STATUS_SUCCESS_TITLE),
            new IdentifiableWrapper(MESSAGE_STATUS_WARNING, MESSAGE_STATUS_WARNING_TITLE),
            new IdentifiableWrapper(MESSAGE_STATUS_ERROR, MESSAGE_STATUS_ERROR_TITLE),
            new IdentifiableWrapper(MESSAGE_STATUS_TRANSPORT_ERROR, MESSAGE_STATUS_TRANSPORT_ERROR_TITLE),
            new IdentifiableWrapper(MESSAGE_STATUS_OUTOFDATE_ERROR, MESSAGE_STATUS_OUTOFDATE_ERROR_TITLE)
    );

    public static NSISyncManager instance()
    {
        return instance(NSISyncManager.class);
    }

    @Bean
    public INSISyncDAO dao()
    {
        return new NSISyncDAO();
    }

    @Bean
    public INSITestDAO testDao()
    {
        return new NSITestDAO();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> operationTypeOptionDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(OPERATION_TYPE_LIST);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eventTypeOptionDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(EVENT_TYPE_LIST);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> messageStatusOptionDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(MESSAGE_STATUS_LIST);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> catalogTypeOptionDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(DataAccessServices.dao().getList(FefuNsiCatalogType.class, FefuNsiCatalogType.title().s()));
    }

    @Bean
    public UIDataSourceConfig eventTypeOptionDSConfig()
    {
        return SelectDSConfig.with(EVENT_TYPE_OPTION_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(eventTypeOptionDSHandler())
                .addColumn("title")
                .create();
    }

    @Bean
    public UIDataSourceConfig operationTypeOptionDSConfig()
    {
        return SelectDSConfig.with(OPERATION_TYPE_OPTION_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(operationTypeOptionDSHandler())
                .addColumn("title")
                .create();
    }

    @Bean
    public UIDataSourceConfig messageStatusOptionDSConfig()
    {
        return SelectDSConfig.with(MESSAGE_STATUS_OPTION_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(messageStatusOptionDSHandler())
                .addColumn("title")
                .create();
    }

    @Bean
    public UIDataSourceConfig catalogTypeOptionDSConfig()
    {
        return SelectDSConfig.with(CATALOG_TYPE_OPTION_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(catalogTypeOptionDSHandler())
                .addColumn("title")
                .create();
    }
}