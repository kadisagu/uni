/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.AdditionalStatusAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;

/**
 * @author Alexey Lopatin
 * @since 26.11.2013
 */
@Configuration
public class FefuSettingsAdditionalStatusAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(UniStudentManger.instance().studentCustomStateCIDSConfig())
                .create();
    }
}
