/* $Id$ */
package ru.tandemservice.unifefu.base.ext.DiplomaIssuance.ui.AddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.DiplomaIssuanceManager;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.ui.AddEdit.DiplomaIssuanceAddEditUI;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unifefu.base.ext.DiplomaIssuance.ui.logic.IDipDocumentExtDao;
import ru.tandemservice.unifefu.entity.DiplomaIssuanceFefuExt;


import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 26.09.2014
 */
public class DiplomaIssuanceAddEditClickApply extends NamedUIAction
{
    DiplomaIssuance _diplomaIssuance;
    List<DiplomaContentIssuance> _issuanceContentList;
    DiplomaIssuanceFefuExt _extension;

    public DiplomaIssuanceAddEditClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof DiplomaIssuanceAddEditUI)
        {
            DiplomaIssuanceAddEditUI addEditUI = (DiplomaIssuanceAddEditUI) presenter;
            _diplomaIssuance = addEditUI.getDiplomaIssuance();
            _diplomaIssuance.setDiplomaObject(addEditUI.getDiplomaObject());


            _issuanceContentList = new ArrayList<>();
            for (DataWrapper wrapper : addEditUI.getDiplomaContentIssuanceList())
            {
                _issuanceContentList.add((DiplomaContentIssuance) (wrapper.getWrapped()));
            }


            IUIAddon uiAddon = presenter.getConfig().getAddon(DiplomaIssuanceAddEditExt.ADDON_NAME);
            if (null != uiAddon)
            {
                if (uiAddon instanceof DiplomaIssuanceAddEditExtUI)
                {
                    _extension = ((DiplomaIssuanceAddEditExtUI) uiAddon).getExtension();
                }
            }

            ((IDipDocumentExtDao) DiplomaIssuanceManager.instance().dao()).saveDiplomaIssuanceExtension(_diplomaIssuance, _extension, _issuanceContentList, addEditUI.getOldBlankMap());
            addEditUI.deactivate();

        }
    }


}
