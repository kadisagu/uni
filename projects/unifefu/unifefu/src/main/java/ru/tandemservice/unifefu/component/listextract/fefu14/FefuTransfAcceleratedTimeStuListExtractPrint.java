/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu14;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.entity.gen.StudentFefuExtGen;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 12.11.2014
 */
public class FefuTransfAcceleratedTimeStuListExtractPrint implements IPrintFormCreator<FefuTransfAcceleratedTimeStuListExtract>, IListParagraphPrintFormCreator<FefuTransfAcceleratedTimeStuListExtract>, IStudentListParagraphPrintFormatter
{
    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\\par ").append(extractNumber).append(".  ");
        buffer.append(student.getPerson().getFullFio());
        return buffer.toString();
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, FefuTransfAcceleratedTimeStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        CommonExtractPrint.initEducationType(modifier, extract.getEducationOrgUnitOld(), "Old");
        CommonExtractPrint.initFefuGroup(modifier, "intoFefuGroupOld", extract.getGroupOld(), extract.getEducationOrgUnitOld().getDevelopForm(), " группы ");
        CommonExtractPrint.initFefuGroup(modifier, "intoFefuGroupNew", extract.getGroupNew(), extract.getEducationOrgUnitNew().getDevelopForm(), " в группу ");

        //базовое образование
        Student student = extract.getEntity();
        StudentFefuExt studentExt = DataAccessServices.dao().getByNaturalId(new StudentFefuExtGen.NaturalId(student));

        String eduLevel = (studentExt != null && studentExt.getBaseEdu() != null) ? (studentExt.getBaseEdu().equals(FefuStudentManager.BASE_VPO) ? "высшее образование" : "среднее профессиональное образование") : "";
        modifier.put("baseEducationLevel", eduLevel.isEmpty()? "" : (extract.getEntity().getPerson().isMale() ? " имеющего " : " имеющую ")+ eduLevel + ",");

        if (UniDefines.DEVELOP_TECH_REMOTE.equals(extract.getEducationOrgUnitOld().getDevelopTech().getCode()))
            modifier.put("developTech",CommonExtractPrint.WITH_REMOTE_EDU_TECH);
        else
            modifier.put("developTech","");


        //предполагаемая дата выпуска
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(extract.getPlannedDate());

        List<String> seasons_a = Arrays.asList((ApplicationRuntime.getProperty("seasons_A")).split(";"));
        List<String> seasons_i = Arrays.asList((ApplicationRuntime.getProperty("seasons_I")).split(";"));
        String season = "";
        if (seasons_a.contains(extract.getSeason()))
            season = seasons_i.get(seasons_a.indexOf(extract.getSeason()));
        modifier.put("season", season + " " + calendar.get(Calendar.YEAR));
        modifier.put("plannedDate", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) extract.getPlannedDate()));
        //срок обучения
        modifier.put("developPeriod", extract.getEducationOrgUnitNew().getDevelopPeriod().getTitle());

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuTransfAcceleratedTimeStuListExtract firstExtract)
    {
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuTransfAcceleratedTimeStuListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuTransfAcceleratedTimeStuListExtract firstExtract)
    {
        return null;
    }
}