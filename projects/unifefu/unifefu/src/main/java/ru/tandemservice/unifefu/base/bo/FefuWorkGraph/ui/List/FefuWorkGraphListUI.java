/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.FefuWorkGraphManager;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.Add.FefuWorkGraphAdd;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
public class FefuWorkGraphListUI extends UIPresenter
{
    public void onClickSearch()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        FefuWorkGraphManager.instance().dao().setWorkGraphListFilterDefaultValues(getSettings());
        saveSettings();
    }

    public void onClickAddWorkGraph()
    {
        _uiActivation.asRegionDialog(FefuWorkGraphAdd.class).activate();
    }

    public void onDeleteEntityFromList()
    {
        FefuWorkGraphManager.instance().dao().delete(getListenerParameterAsLong());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(FefuWorkGraphList.WORK_GRAPH_DS))
        {
            dataSource.putAll(getSettings().getAsMap(
                    false,
                    FefuWorkGraphList.PUPNAG,
                    FefuWorkGraphList.STATE,
                    FefuWorkGraphList.DEVELOP_FORM,
                    FefuWorkGraphList.DEVELOP_CONDITION,
                    FefuWorkGraphList.DEVELOP_TECH,
                    FefuWorkGraphList.DEVELOP_GRID));
        }
    }
}