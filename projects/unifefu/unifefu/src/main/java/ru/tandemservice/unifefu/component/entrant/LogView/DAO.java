/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.LogView;

import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.uniec.entity.entrant.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 10.08.2016
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.LogView.DAO
{
    private static final List<String> _entityClassNames = Arrays.asList(
            Entrant.ENTITY_CLASS,
            Person.ENTITY_CLASS,
            IdentityCard.ENTITY_CLASS,
            PersonEduInstitution.ENTITY_CLASS,
            PersonBenefit.ENTITY_CLASS,
            PersonNextOfKin.ENTITY_CLASS,
            PersonSportAchievement.ENTITY_CLASS,
            PersonForeignLanguage.ENTITY_CLASS,
            Address.ENTITY_CLASS,
            OlympiadDiploma.ENTITY_CLASS,
            EntrantStateExamCertificate.ENTITY_CLASS,
            StateExamSubjectMark.ENTITY_CLASS,
            PreliminaryEnrollmentStudent.ENTITY_CLASS,
            EntrantRequest.ENTITY_CLASS,
            RequestedEnrollmentDirection.ENTITY_CLASS,
            EntrantEnrollmentDocument.ENTITY_CLASS,
            ChosenEntranceDiscipline.ENTITY_CLASS,
            ExamPassDiscipline.ENTITY_CLASS,
            ExamPassMark.ENTITY_CLASS,
            ExamPassMarkAppeal.ENTITY_CLASS,
            InterviewResult.ENTITY_CLASS,
            RequestedProfileKnowledge.ENTITY_CLASS,
            ProfileExaminationMark.ENTITY_CLASS,
            Discipline2OlympiadDiplomaRelation.ENTITY_CLASS,
            EntrantInfoAboutUniversity.ENTITY_CLASS,
            EntrantExamList.ENTITY_CLASS,
            EntrantAccessCourse.ENTITY_CLASS,
            EntrantIndividualProgress.ENTITY_CLASS
    );

    @Override
    protected List<String> getEntityClassNames()
    {
        return _entityClassNames;
    }
}