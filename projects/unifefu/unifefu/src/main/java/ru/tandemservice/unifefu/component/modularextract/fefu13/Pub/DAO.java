/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu13.Pub;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;

/**
 * @author nvankov
 * @since 6/20/13
 */
public class DAO extends ModularStudentExtractPubDAO<SendPracticOutStuExtract, Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (null != model.getExtract().getPracticeHeaderInner())
        {
            model.setPracticeHeaderInner(getEmployeePostTitle(model.getExtract().getPracticeHeaderInner(), model.getExtract().getPracticeHeaderInnerDegree()));
        }
        else
        {
            model.setPracticeHeaderInner(null);
        }

        if (null != model.getExtract().getPreventAccidentsIC())
        {
            model.setPreventAccidentsIC(getEmployeePostTitle(model.getExtract().getPreventAccidentsIC(), model.getExtract().getPreventAccidentsICDegree()));
        }
        else
        {
            model.setPreventAccidentsIC(null);
        }

        if (null != model.getExtract().getResponsForRecieveCash())
        {
            model.setResponsForRecieveCash(getEmployeePostTitle(model.getExtract().getResponsForRecieveCash(), model.getExtract().getResponsForRecieveCashDegree()));
        }
        else
        {
            model.setResponsForRecieveCash(null);
        }
    }

    private String getEmployeePostTitle(EmployeePost employeePost, PersonAcademicDegree academicDegree)
    {
        StringBuilder fioStrBuilder = new StringBuilder();

        fioStrBuilder.append(getDegreeShortTitleWithDots(academicDegree));

        String practiceHeaderPostNT = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        String practiceHeaderNominative = !StringUtils.isEmpty(practiceHeaderPostNT) ? practiceHeaderPostNT :
                employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if (!StringUtils.isEmpty(practiceHeaderNominative))
            fioStrBuilder.append(StringUtils.isEmpty(fioStrBuilder.toString()) ? "" : " ").append(practiceHeaderNominative.toLowerCase());

        fioStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(employeePost.getPerson().getIdentityCard(), GrammaCase.NOMINATIVE));

        return fioStrBuilder.toString();
    }

    private String getDegreeShortTitleWithDots(PersonAcademicDegree prevDegree)
    {
        String degreeShortTitleWithDots = "";
        if (null != prevDegree)
        {
            String[] degreeTitle = prevDegree.getAcademicDegree().getTitle().toLowerCase().split(" ");
            StringBuilder shortDegreeTitle = new StringBuilder();
            for (String deg : Lists.newArrayList(degreeTitle))
            {
                shortDegreeTitle.append(deg.charAt(0)).append(".");
            }
            degreeShortTitleWithDots += shortDegreeTitle;
        }
        return degreeShortTitleWithDots;
    }
}
