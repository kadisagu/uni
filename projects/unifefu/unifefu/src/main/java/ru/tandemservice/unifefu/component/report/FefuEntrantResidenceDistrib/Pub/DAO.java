package ru.tandemservice.unifefu.component.report.FefuEntrantResidenceDistrib.Pub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unifefu.entity.report.FefuEntrantResidenceDistrib;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReport(getNotNull(FefuEntrantResidenceDistrib.class, model.getReport().getId()));
    }
}
