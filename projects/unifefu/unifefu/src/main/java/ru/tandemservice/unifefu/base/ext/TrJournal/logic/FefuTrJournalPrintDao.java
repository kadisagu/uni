package ru.tandemservice.unifefu.base.ext.TrJournal.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.base.ext.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditExtUI;
import ru.tandemservice.unifefu.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unifefu.utils.FefuTrGroupEventDateComparator;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsIRatingValueFormatter;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.TrJournalPrintDao;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAction;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author amakarova
 */
public class FefuTrJournalPrintDao extends TrJournalPrintDao implements IFefuTrJournalPrintDao
{
    private static final DateFormatter DATE_FORMATTER_DAY_AND_MONTH = new DateFormatter("dd.MM");
    private static final DateFormatter DATE_FORMATTER_DAY_AND_TIME = DateFormatter.DATE_FORMATTER_WITH_TIME;
    protected static DoubleFormatter ratingFormatter = DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS;

    @Override
    public List<TrEduGroupEvent> getGroupEvents(Long eppRealEduGroup4LoadTypeId)
    {
        List<TrEduGroupEvent> list = new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "e").column("e")
                .where(eq(property("e", TrEduGroupEvent.group()), value(eppRealEduGroup4LoadTypeId)))
                .createStatement(getSession()).list();

        for (int i = list.size() - 1; i >= 0; i--)
        {
            if (!(list.get(i).getJournalEvent() instanceof TrEventAction))
                list.remove(i);
        }
        return list;
    }

    @Override
    public byte[] printRatingGroup(Long journalGroupId)
    {
        final UnisessionCommonTemplate template = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.FEFU_RATING_GROUP);
        RtfDocument rtf = new RtfReader().read(template.getContent());

        TrJournalGroup journalGroup = get(TrJournalGroup.class, journalGroupId);
        Collection<EppStudentWorkPlanElement> studentList = TrJournalManager.instance().dao().getStudentList(journalGroup);

        Collection<IBrsDao.ISessionMarkCalc> calcs = TrBrsCoefficientManager.instance().brsDao().getCalculatedSessionMark(journalGroup);
        IBrsDao.IJournalEventStatCalc stats = TrBrsCoefficientManager.instance().brsDao().getCalculatedStat(journalGroup);
        //TrBrsCoefficientManager.instance().brsDao().getDisplayableAdditParams(journalGroup.getJournal());

        RtfDocument result = null;
        final List<TrEduGroupEvent> eventsAction = getGroupEvents(journalGroup.getGroup().getId());
        if (eventsAction.isEmpty())
            throw new ApplicationException("Нет контрольных мероприятий.");

        Collections.sort(eventsAction, new FefuTrGroupEventDateComparator());

        DQLSelectBuilder eventStudentDQL = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "e").column("e")
                .where(eq(property(TrEduGroupEventStudent.event().group().fromAlias("e")), value(journalGroup.getGroup())));

        List<TrEduGroupEventStudent> eventStudents = eventStudentDQL.createStatement(getSession()).list();

        Map<TrEduGroupEvent, List<TrEduGroupEventStudent>> mapEvents = new HashMap<>();
        Map<EppStudentWorkPlanElement, Map<TrEduGroupEvent, List<TrEduGroupEventStudent>>> mapStudents = new HashMap<>();
        for (TrEduGroupEventStudent eventStudent : eventStudents)
        {
            if (eventStudent.getEvent().getJournalEvent() instanceof TrEventAction)
            {
                SafeMap.safeGet(mapEvents, eventStudent.getEvent(), ArrayList.class).add(eventStudent);
                SafeMap.safeGet(SafeMap.safeGet(mapStudents, eventStudent.getStudentWpe(), HashMap.class), eventStudent.getEvent(), ArrayList.class).add(eventStudent);
            }
        }

        EppFControlActionGroup fControlActionGroup = TrBrsCoefficientManager.instance().coefDao().getFcaGroup(journalGroup.getJournal());
        boolean isExam = EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(fControlActionGroup.getCode());

        for (final IBrsDao.ISessionMarkCalc calc : calcs)
        {
            fillTable(rtf, eventsAction, calc, mapStudents, stats, journalGroup);
            final RtfDocument document = rtf.getClone();

            fillHeader(document, journalGroup, studentList, calc, isExam);

            if (null == result)
            {
                result = document;
            }
            else
            {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
            }
        }

        if (null == result)
            throw new ApplicationException("Не выбраны ведомости для печати.");

        return RtfUtil.toByteArray(result);
    }

    private static String getSchoolPrintTitle(OrgUnit orgUnit)
    {
        while (orgUnit != null)
        {
            if (UniFefuDefines.SCHOOL_ORGUNIT_TYPE_CODE.equals(orgUnit.getOrgUnitType().getCode()))
                return orgUnit.getPrintTitle();
            orgUnit = orgUnit.getParent();
        }
        return "ДВФУ";
    }

    private void fillHeader(RtfDocument rtf, TrJournalGroup journalGroup, Collection<EppStudentWorkPlanElement> studentList, IBrsDao.ISessionMarkCalc calc, boolean isExam)
    {
        TrJournal journal = journalGroup.getJournal();
        EppRegistryElementPart discipline = journal.getRegistryElementPart();
        List<EppPpsCollectionItem> tutors = getList(EppPpsCollectionItem.class, EppPpsCollectionItem.list().s(), journalGroup.getGroup());
        final TopOrgUnit academy = TopOrgUnit.getInstance();
        EmployeePost cathedraHead = EmployeeManager.instance().dao().getHead(discipline.getTutorOu());

        final RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("academyTitle", academy.getPrintTitle());
        modifier.put("ouTitle", getSchoolPrintTitle(discipline.getTutorOu()));
        modifier.put("discipline", "№" + journal.getNumber() + " " + journal.getRegistryElementPart().getTitle());
        String studentGroups = getGroups(studentList);
        modifier.put("eduGroup", journalGroup.getGroup().getTitle() + " (" + studentGroups + ")");
        modifier.put("educationOrgUnitTitle", getEduOUs(studentList));
        modifier.put("eduYear", journal.getYearPart().getYear().getEducationYear().getTitle());
        modifier.put("term", journal.getYearPart().getPart().getShortTitle());
        modifier.put("group", studentGroups);
        modifier.put("course", getCourses(studentList));
        modifier.put("ratingInfo", calc.getPrintableRatingFormula());
        modifier.put("tutors", UniStringUtils.joinUnique(tutors, EppPpsCollectionItem.pps().person().fullFio().s(), ", "));
        modifier.put("cathedra", discipline.getTutorOu().getPrintTitle());
        modifier.put("cathedraHead", cathedraHead == null ? "" : cathedraHead.getPerson().getFio());
        modifier.put("currentDateTime", DATE_FORMATTER_DAY_AND_TIME.format(new Date()));
        modifier.put("markSetting", FefuBrs.getJournalScaleDescription(isExam, journal));
        modifier.modify(rtf);
    }

    private String getEduOUs(Collection<EppStudentWorkPlanElement> studentList)
    {
        return CommonBaseStringUtil.joinUniqueSorted(studentList, EppStudentWorkPlanElement.student().educationOrgUnit().educationLevelHighSchool().title().s(), ", ");
    }

    private String getCourses(Collection<EppStudentWorkPlanElement> studentList)
    {
        return CommonBaseStringUtil.joinUniqueSorted(studentList, EppStudentWorkPlanElement.course().title().s(), ", ");
    }

    private String getGroups(Collection<EppStudentWorkPlanElement> studentList)
    {
        Set<String> strings = Sets.newHashSet(CommonBaseEntityUtil.<String>getPropertiesList(studentList, EppStudentWorkPlanElement.student().group().title()));
        return StringUtils.join(strings, ", ");
    }

    /**
     * Рейтинг академической группы
     */
    private void fillTable(RtfDocument rtf, final List<TrEduGroupEvent> events,
                           IBrsDao.ISessionMarkCalc calculatedRating,
                           Map<EppStudentWorkPlanElement, Map<TrEduGroupEvent, List<TrEduGroupEventStudent>>> mapStudents,
                           IBrsDao.IJournalEventStatCalc stats, TrJournalGroup journalGroup)
    {
        final IBrsDao.IBrsPreparedSettings brsSettings = TrBrsCoefficientManager.instance().brsDao().getPreparedBrsSettings(journalGroup.getJournal());
        List<String[]> table = new ArrayList<>();
        Double allWeight = 0d;
        final Map<TrEduGroupEvent, Double> weightMap = new HashMap<>(events.size());
        for (TrEduGroupEvent event : events)
        {
            TrBrsCoefficientValue weightCoefSettings = brsSettings.getEventSettings(event.getJournalEvent(), FefuBrs.WEIGHT_EVENT_CODE);
            if (weightCoefSettings != null)
            {
                Double value = weightCoefSettings.getValueAsDouble();
                allWeight += value;
                weightMap.put(event, value);
            }
        }
        final Double allW = allWeight == 0d ? 1d : allWeight;

        List<EppStudentWorkPlanElement> students = new ArrayList<>(mapStudents.keySet());
        Collections.sort(students, EppStudentWorkPlanElement.FULL_FIO_COMPARATOR);

        IBrsDao.ICurrentRatingCalc ratingCalc = TrBrsCoefficientManager.instance().brsDao().getCalculatedRating(journalGroup.getJournal());

        List<String> additParamList = new ArrayList<>();
        additParamList.add(FefuBrs.TOTAL_COLUMN_KEY);
        additParamList.add(FefuBrs.MARK_COLUMN_KEY);

        // Получаем список непереведенных студентов - это те, кто есть УГС. Нужен, чтобы вычислить переведенных - их не будет в этом списке
        Set<Long> inGroupStudents = new HashSet<>(
                new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "r")
                        .column(property("r", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().id()))
                        .where(eq(property("r", EppRealEduGroup4LoadTypeRow.L_GROUP), value(journalGroup.getGroup())))
                        .createStatement(getSession()).<Long>list()
        );

        final Set<Integer> inactiveStudents = new HashSet<>();

        int rowNumber = 1;
        for (EppStudentWorkPlanElement student : students)
        {
            if (!inGroupStudents.contains(student.getId()) || !student.getStudent().getStatus().isActive())
                inactiveStudents.add(rowNumber - 1);

            IBrsDao.IStudentSessionMarkData studentSessionMark = calculatedRating.getSessionMark(student);
            Map<TrEduGroupEvent, List<TrEduGroupEventStudent>> mapEvents = mapStudents.get(student);
            List<String> tableRow = new ArrayList<>();
            tableRow.add(String.valueOf(rowNumber++));
            tableRow.add(student.getStudent().getFio());

            for (TrEduGroupEvent event : events)
            {
                String mark = "-";
                List<TrEduGroupEventStudent> markEvents = mapEvents.get(event);
                if (markEvents != null && !markEvents.isEmpty())
                {
                    TrEduGroupEventStudent markEvent = markEvents.get(0);
                    if (markEvent.getGrade() != null)
                        mark = ratingFormatter.format(markEvent.getGrade());
                }
                tableRow.add(mark);
            }

            if (studentSessionMark != null)
            {
                IBrsDao.IStudentCurrentRatingData studentRating = ratingCalc.getCurrentRating(student);
                tableRow.add(TrJournalGroupMarkEditExtUI.getRatingStr(studentRating.getRatingValue()));
                for (String additParam : additParamList)
                {
                    tableRow.add(TrJournalGroupMarkEditExtUI.getAdditParamStr(studentRating.getRatingAdditParam(additParam)));
                }
            }
            table.add(tableRow.toArray(new String[tableRow.size()]));
        }

        for (ISessionBrsDao.IRatingAdditParamDef statRow : stats.getStatParamDefinitions())
        {
            List<String> table1Row = new ArrayList<>();
            table1Row.add("");
            table1Row.add(statRow.getTitle());
            for (TrEduGroupEvent event : events)
            {
                Double medium = stats.getStatValue(event.getJournalEvent(), statRow.getKey()).getValue();
                table1Row.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(medium));
            }
            // рейтинг
            IBrsDao.IAggregatedRatingValue paramValue = stats.getStatValueAggregated(statRow.getKey(), IBrsDao.RATING_COLUMN_KEY);
            table1Row.add(paramValue.isApplicable() ? BrsIRatingValueFormatter.instance.format(paramValue) : "");
            // доп. колонки
            for (String additParam : additParamList)
            {
                paramValue = stats.getStatValueAggregated(statRow.getKey(), additParam);
                table1Row.add(paramValue.isApplicable() ? BrsIRatingValueFormatter.instance.format(paramValue) : "");
            }

            table.add(table1Row.toArray(new String[table1Row.size()]));
        }

        new RtfTableModifier()
                .put("T", table.toArray(new String[table.size()][]))
                .put("T", new RtfRowIntercepterBase()
                {
                    @Override
                    public void beforeModify(RtfTable table, int currentRowIndex)
                    {
                        int[] parts = new int[events.size()];
                        Arrays.fill(parts, 1);
                        RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 2, (newCell, index) -> {
                            TrEduGroupEvent key = events.get(index);
                            String max = "-";
                            String min = "-";
                            String weight = "";
                            TrBrsCoefficientValue maxPointsSettings = brsSettings.getEventSettings(key.getJournalEvent(), FefuBrs.MAX_POINTS_CODE);
                            if (maxPointsSettings != null)
                                max = ratingFormatter.format(maxPointsSettings.getValueAsDouble());

                            TrBrsCoefficientValue minPointsSettings = brsSettings.getEventSettings(key.getJournalEvent(), FefuBrs.MIN_POINTS_CODE);
                            if (minPointsSettings != null)
                                min = ratingFormatter.format(minPointsSettings.getValueAsDouble());

                            Double wightValue = weightMap.get(key);
                            if (wightValue != null)
                                weight = ratingFormatter.format(wightValue / allW * 100);

                            String dateEvent = key.getScheduleEvent() == null ? "" : DATE_FORMATTER_DAY_AND_MONTH.format(key.getScheduleEvent().getDurationBegin());
                            newCell.setElementList(new RtfString().append(dateEvent + " " + min + "/" + max + " (" + weight + "%)").toList());
                        }, parts);
                        RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 2, null, parts);
                    }

                    @Override
                    public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
                    {
                        if (colIndex == 1 && inactiveStudents.contains(rowIndex))
                            return new RtfString().boldBegin().append(IRtfData.I).append(value).append(IRtfData.I, 0).boldEnd().toList();

                        return super.beforeInject(table, row, cell, rowIndex, colIndex, value);
                    }
                }).modify(rtf);
    }

    @Override
    public byte[] printRatingSheet(Long journalGroupId, boolean isExam)
    {
        final UnisessionCommonTemplate template = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(),
                                                           isExam ? UnisessionCommonTemplateCodes.FEFU_RATING_SHEET_EXAM : UnisessionCommonTemplateCodes.FEFU_RATING_SHEET_SETOFF);
        RtfDocument rtf = new RtfReader().read(template.getContent());

        TrJournalGroup journalGroup = get(TrJournalGroup.class, journalGroupId);
        Collection<EppStudentWorkPlanElement> studentList = TrJournalManager.instance().dao().getStudentList(journalGroup);
        CollectionUtils.filter(studentList, slot -> slot.getRemovalDate() == null);

        Collection<IBrsDao.ISessionMarkCalc> calcs = TrBrsCoefficientManager.instance().brsDao().getCalculatedSessionMark(journalGroup);
        IBrsDao.ICurrentRatingCalc currentRating = TrBrsCoefficientManager.instance().brsDao().getCalculatedRating(journalGroup.getJournal());
        TrBrsCoefficientManager.instance().brsDao().getDisplayableAdditParams(journalGroup.getJournal());

        RtfDocument result = null;
        final List<TrEduGroupEvent> eventsAction = getGroupEvents(journalGroup.getGroup().getId());

        DQLSelectBuilder eventStudentDQL = new DQLSelectBuilder()
                .fromEntity(TrEduGroupEventStudent.class, "event").column("event")
                .where(eq(property(TrEduGroupEventStudent.event().group().fromAlias("event")), value(journalGroup.getGroup())));

        List<TrEduGroupEventStudent> eventStudents = eventStudentDQL.createStatement(getSession()).list();
        Map<TrEduGroupEvent, List<TrEduGroupEventStudent>> mapEvents = new HashMap<>();
        Map<EppStudentWorkPlanElement, Map<TrEduGroupEvent, List<TrEduGroupEventStudent>>> mapStudents = new HashMap<>();
        for (TrEduGroupEventStudent eventStudent : eventStudents)
        {
            if (eventStudent.getEvent().getJournalEvent() instanceof TrEventAction)
            {
                SafeMap.safeGet(mapEvents, eventStudent.getEvent(), ArrayList.class).add(eventStudent);
                SafeMap.safeGet(SafeMap.safeGet(mapStudents, eventStudent.getStudentWpe(), HashMap.class), eventStudent.getEvent(), ArrayList.class).add(eventStudent);
            }
        }

        for (final IBrsDao.ISessionMarkCalc calc : calcs)
        {
            fillTableSheetExam(rtf, calc, studentList, journalGroup, eventsAction, currentRating, isExam);
            final RtfDocument document = rtf.getClone();

            fillHeader(document, journalGroup, studentList, calc, isExam);

            if (null == result)
            {
                result = document;
            }
            else
            {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
            }
        }

        if (null == result)
            throw new ApplicationException("Не выбраны ведомости для печати.");

        return RtfUtil.toByteArray(result);
    }

    private void fillTableSheetExam(RtfDocument rtf, IBrsDao.ISessionMarkCalc calculatedRating,
                                    Collection<EppStudentWorkPlanElement> studentList, TrJournalGroup journalGroup,
                                    List<TrEduGroupEvent> eventsAction,
                                    IBrsDao.ICurrentRatingCalc currentRating,
                                    boolean isExam)
    {
        int totalExcelent = 0;
        int totalGood = 0;
        int totalUd = 0;
        int totalSetoff = 0;

        boolean checkMin = false;
        final IBrsDao.IBrsPreparedSettings brsSettings = TrBrsCoefficientManager.instance().brsDao().getPreparedBrsSettings(journalGroup.getJournal());
        for (TrEduGroupEvent event : eventsAction)
        {
            TrBrsCoefficientValue minPoints = brsSettings.getEventSettings(event.getJournalEvent(), FefuBrs.MIN_POINTS_CODE);
            if (minPoints != null && minPoints.getValue() != null)
            {
                checkMin = true;
                break;
            }
        }
        //
        List<TrEventAction> eventActions = Lists.newArrayList();
        List<TrEventAction> mainEventActions = Lists.newArrayList();

        boolean allEventsMinIsZero = true;

        String typeTitle = isExam ? "Экзамен" : "Зачет";
        String anotherTypeTitle = isExam ? "Зачет" : "Экзамен";
        for (TrEduGroupEvent event : eventsAction)
        {
            if (event.getJournalEvent() instanceof TrEventAction)
            {
                eventActions.add((TrEventAction) event.getJournalEvent());
                String actionTypeTitle = ((TrEventAction) event.getJournalEvent()).getActionType().getTitle();
                if (typeTitle.equals(actionTypeTitle))
                {
                    mainEventActions.add((TrEventAction) event.getJournalEvent());
                }
                else if (!anotherTypeTitle.equals(actionTypeTitle))
                {
                    TrBrsCoefficientValue minPoints = brsSettings.getEventSettings(event.getJournalEvent(), FefuBrs.MIN_POINTS_CODE);
                    if (null == minPoints || minPoints.getValueAsLong() > 0) allEventsMinIsZero = false;
                }
            }
        }

        if (mainEventActions.isEmpty())
            throw new ApplicationException("Мероприятие «" + typeTitle + "» отсутствует в структуре чтения.");
        if (mainEventActions.size() > 1)
            throw new ApplicationException("В структуре чтения присутствует несколько мероприятий с типом «" + typeTitle + "».");

        TrEventAction mainEvent = mainEventActions.get(0);

        DQLSelectBuilder marksBuilder = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "m");
        marksBuilder.where(in(property("m", TrEduGroupEventStudent.event().journalEvent()), eventActions));
        marksBuilder.where(isNull(property("m", TrEduGroupEventStudent.transferDate())));

        Map<EppStudentWorkPlanElement, Map<TrEventAction, TrEduGroupEventStudent>> studentMarksMap = Maps.newHashMap();
        for (TrEduGroupEventStudent mark : marksBuilder.createStatement(getSession()).<TrEduGroupEventStudent>list())
        {
            EppStudentWorkPlanElement studentEpvSlot = mark.getStudentWpe();
            TrEventAction eventAction = (TrEventAction) mark.getEvent().getJournalEvent();
            if (!studentMarksMap.containsKey(studentEpvSlot))
                studentMarksMap.put(studentEpvSlot, Maps.<TrEventAction, TrEduGroupEventStudent>newHashMap());
            if (!studentMarksMap.get(studentEpvSlot).containsKey(eventAction))
                studentMarksMap.get(studentEpvSlot).put(eventAction, mark);
        }

        Map<EppStudentWorkPlanElement, StudentPoint> studentNeedPoints = getStudentPoints(eventActions, studentList, studentMarksMap, isExam);

        List<String[]> table = new ArrayList<>();
        int rowNumber = 1;
        for (EppStudentWorkPlanElement student : studentList)
        {
            IBrsDao.IStudentSessionMarkData studentSessionMark = calculatedRating.getSessionMark(student);

            List<String> tableRow = new ArrayList<>();
            tableRow.add(String.valueOf(rowNumber++));
            tableRow.add(student.getStudent().getFio());

            if (studentSessionMark != null)
            {
                Double total = TrBrsCoefficientManager.instance().brsDao().getCalculatedRating(journalGroup.getJournal()).getCurrentRating(student).getRatingAdditParam(FefuBrs.TOTAL_COLUMN_KEY).getValue();
                tableRow.add(ratingFormatter.format(total));
                if (allEventsMinIsZero)
                    tableRow.add(YesNoFormatter.INSTANCE.format(Boolean.TRUE));
                else if (checkMin)
                    tableRow.add(YesNoFormatter.INSTANCE.format(Boolean.TRUE.equals(studentSessionMark.isAllowed())));
                else
                    tableRow.add(YesNoFormatter.INSTANCE.format(Boolean.TRUE));

                // Балл за экзамен
                Map<TrEventAction, TrEduGroupEventStudent> eventStudentMap = studentMarksMap.get(student);
                TrEduGroupEventStudent eventStudent = null != eventStudentMap ? eventStudentMap.get(mainEvent) : null;
                if (null != eventStudent && null != eventStudent.getGrade())
                    tableRow.add(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(eventStudent.getGrade()));
                else
                    tableRow.add("");

                //        (Балл за экзамен*нормированный вес экзамена)+итоговый рейтинг (без учета экзамена)=итоговая оценка
                StudentPoint sp = studentNeedPoints.get(student);
                if (null == sp)
                {
                    tableRow.add("-"); // удовл | зачет
                    if (isExam)
                    {
                        tableRow.add("-"); // хор
                        tableRow.add("-"); // отл
                    }
                }
                else
                {
                    if (isExam)
                    {
                        tableRow.add(null != sp.getUd() ? DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(Math.ceil(sp.getUd())) : "-"); // удовл
                        tableRow.add(null != sp.getGood() ? DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(Math.ceil(sp.getGood())) : "-"); // хор
                        tableRow.add(null != sp.getExcellent() ? DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(Math.ceil(sp.getExcellent())) : "-"); // отл
                    }
                    else
                    {
                        tableRow.add(null != sp.getSetoff() ? DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(sp.getSetoff()) : "-"); // зачет
                    }
                }

                ISessionBrsDao.IRatingValue mark = currentRating.getCurrentRating(student).getRatingAdditParam(FefuBrs.MARK_COLUMN_KEY);
                String markStr = null != mark ? mark.getMessage() : null;
                if (null != markStr && null != currentRating.getCurrentRating(student) && StringUtils.isNotEmpty(markStr))
                {
                    switch (markStr)
                    {
                        case FefuBrs.TEXT_MARK_5:
                            tableRow.add(StringUtils.capitalize(markStr));
                            totalExcelent++;
                            break;
                        case FefuBrs.TEXT_MARK_4:
                            tableRow.add(StringUtils.capitalize(markStr));
                            totalGood++;
                            break;
                        case FefuBrs.TEXT_MARK_3:
                            tableRow.add(StringUtils.capitalize(FefuBrs.TEXT_MARK_3));
                            totalUd++;
                        case FefuBrs.TEXT_MARK_SETOFF:
                            tableRow.add(FefuBrs.TEXT_MARK_SETOFF_FULL);
                            totalSetoff++;
                            break;
                    }
                }
                else
                    tableRow.add("");
            }
            table.add(tableRow.toArray(new String[tableRow.size()]));
        }
        new RtfTableModifier()
                .put("T", table.toArray(new String[table.size()][]))
                .modify(rtf);

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("totalExcelent", String.valueOf(totalExcelent));
        modifier.put("totalGood", String.valueOf(totalGood));
        modifier.put("totalUd", String.valueOf(totalUd));
        modifier.put("totalSetoff", String.valueOf(totalSetoff));
        TrBrsCoefficientValue maxPoints = brsSettings.getEventSettings(mainEvent, FefuBrs.MAX_POINTS_CODE);
        modifier.put("max", maxPoints != null ? ("(макс. " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(maxPoints.getValueAsDouble()) + ")") : null);
        modifier.modify(rtf);
    }


    class StudentPoint
    {
        private Double _excellent;
        private Double _good;
        private Double _ud;
        private Double _setoff;

        public void setExcellent(Double excellent)
        {
            _excellent = excellent;
        }

        public void setGood(Double good)
        {
            _good = good;
        }

        public void setUd(Double ud)
        {
            _ud = ud;
        }

        public Double getExcellent()
        {
            return _excellent;
        }

        public Double getGood()
        {
            return _good;
        }

        public Double getUd()
        {
            return _ud;
        }

        public Double getSetoff()
        {
            return _setoff;
        }

        public void setSetoff(Double setoff)
        {
            _setoff = setoff;
        }
    }

    private Map<EppStudentWorkPlanElement, StudentPoint> getStudentPoints(List<TrEventAction> eventActions, Collection<EppStudentWorkPlanElement> students, Map<EppStudentWorkPlanElement, Map<TrEventAction, TrEduGroupEventStudent>> studentMarksMap, boolean isExam)
    {
        Map<EppStudentWorkPlanElement, StudentPoint> ratingWithoutExamMap = Maps.newHashMap();
        for (EppStudentWorkPlanElement student : students)
        {
            ratingWithoutExamMap.put(student, getStudentRating(student, eventActions, studentMarksMap, isExam));
        }
        return ratingWithoutExamMap;
    }


    //Получить рейтинг
    public StudentPoint getStudentRating(EppStudentWorkPlanElement student, List<TrEventAction> eventActions, Map<EppStudentWorkPlanElement, Map<TrEventAction, TrEduGroupEventStudent>> studentMarksMap, boolean isExam)
    {
        /////
        IBrsDao.IBrsPreparedSettings brsSettings = TrBrsCoefficientManager.instance().brsDao().getPreparedBrsSettings(eventActions.get(0).getJournalModule().getJournal());
        double max_points = 0; // на текущую дату - макс. балл по КМ с учетом весовых коэффициентов

        String eventTypeTitle = isExam ? "Экзамен" : "Зачет";
        List<TrEventAction> actionEvents = Lists.newArrayList();
        TrEventAction event = null;
        for (TrEventAction eventAction : eventActions)
        {
            if (!eventTypeTitle.equals((eventAction.getActionType().getTitle())))
            {
                actionEvents.add(eventAction);
            }
            else
                event = eventAction;
        }
        double eventMinPointD = 0;
        TrBrsCoefficientValue examMinPoint = brsSettings.getEventSettings(event, FefuBrs.MIN_POINTS_CODE);
        if (null != examMinPoint)
            eventMinPointD = examMinPoint.getValueAsDouble();

        double eventMaxPointD = 0;
        TrBrsCoefficientValue examMaxPoint = brsSettings.getEventSettings(event, FefuBrs.MAX_POINTS_CODE);
        if (null != examMaxPoint)
            eventMaxPointD = examMaxPoint.getValueAsDouble();

        double eventWeightD = 0;
        TrBrsCoefficientValue examWeight = brsSettings.getEventSettings(event, FefuBrs.WEIGHT_EVENT_CODE);
        if (null != examWeight)
            eventWeightD = examWeight.getValueAsDouble();

        double sum_event_weight_t = 0;
        try
        {
            for (TrEventAction curEvent : actionEvents)
            {
                if (!curEvent.equals(event))
                {
                    double event_max_point = 0;     // макс. балл по КМ
                    double event_weight = 1;   // весовой коэф. КМ

                    TrBrsCoefficientValue maxPointsSettings = brsSettings.getEventSettings(curEvent, FefuBrs.MAX_POINTS_CODE);
                    if (null != maxPointsSettings)
                        event_max_point = maxPointsSettings.getValueAsDouble();

                    TrBrsCoefficientValue weightCoefSettings = brsSettings.getEventSettings(curEvent, FefuBrs.WEIGHT_EVENT_CODE);
                    if (null != weightCoefSettings)
                        event_weight = weightCoefSettings.getValueAsDouble();

                    max_points = max_points + (event_max_point * event_weight);
                    sum_event_weight_t += event_weight * 10;
                }
            }


        }

        catch (Exception e)
        {
            throw new ApplicationException("Рейтинг не может быть вычислен: не удалось получить сумму коэффициентов «Макс. балл» для контрольных мероприятий журнала. Проверьте настройки рейтинга.");
        }

        double points = 0;
        double points_t = 0;

        List<TrEduGroupEventStudent> studentMarks = new ArrayList<>();
        Map<TrEventAction, TrEduGroupEventStudent> studentMap = studentMarksMap.get(student);
        if (studentMap != null)
        {
            for (TrEventAction eventAction : eventActions)
            {
                TrEduGroupEventStudent eventStudent = studentMap.get(eventAction);
                if (null != eventStudent)
                    studentMarks.add(eventStudent);
            }
        }

        for (TrEduGroupEventStudent mark : studentMarks)
        {
            if (!mark.getEvent().getJournalEvent().equals(event))
            {
                double weight_coef = 1; // весовой коэффициент
                Double point = mark.getGrade() == null ? 0.0 : mark.getGrade();
                TrBrsCoefficientValue weightCoefSettings = brsSettings.getEventSettings(mark.getEvent().getJournalEvent(), FefuBrs.WEIGHT_EVENT_CODE);
                TrBrsCoefficientValue maxPoint = brsSettings.getEventSettings(mark.getEvent().getJournalEvent(), FefuBrs.MAX_POINTS_CODE);
                double dMaxPoint = 1;
                if (null != maxPoint)
                {
                    dMaxPoint = maxPoint.getValueAsDouble();
                    if (point != null && point > maxPoint.getValueAsDouble())
                    {
                        point = maxPoint.getValueAsDouble();
                    }
                }
                if (null != weightCoefSettings)
                    weight_coef = weightCoefSettings.getValueAsDouble();


                if (point != null)
                {
                    points_t += (point / dMaxPoint) * weight_coef * 100;
                    points = points_t / 100;
                }
            }
        }

        double dRange3 = 0;
        double dRange4 = 0;
        double dRange5 = 0;
        double dSetoffRange = 0;
        if (isExam)
        {
            TrBrsCoefficientValue range3 = brsSettings.getSettings(FefuBrs.RANGE_3_CODE);
            if (range3 == null)
                range3 = brsSettings.getSettings(FefuBrs.RANGE_3_DEF_CODE);
            TrBrsCoefficientValue range4 = brsSettings.getSettings(FefuBrs.RANGE_4_CODE);
            if (range4 == null)
                range4 = brsSettings.getSettings(FefuBrs.RANGE_4_DEF_CODE);
            TrBrsCoefficientValue range5 = brsSettings.getSettings(FefuBrs.RANGE_5_CODE);
            if (range5 == null)
                range5 = brsSettings.getSettings(FefuBrs.RANGE_5_DEF_CODE);

            if (range3 != null)
                dRange3 = range3.getValueAsDouble();
            if (range4 != null)
                dRange4 = range4.getValueAsDouble();
            if (range5 != null)
                dRange5 = range5.getValueAsDouble();
        }
        else
        {
            TrBrsCoefficientValue rangeSetOff = brsSettings.getSettings(FefuBrs.RANGE_SETOFF_CODE);
            if (rangeSetOff == null)
                rangeSetOff = brsSettings.getSettings(FefuBrs.RANGE_SETOFF_DEF_CODE);

            if (rangeSetOff != null)
                dSetoffRange = rangeSetOff.getValueAsDouble();
        }

        StudentPoint studentPoint = new StudentPoint();

        Double mark3;
        Double mark4;
        Double mark5;
        Double setoff;
        if (0 == eventWeightD)
        {
            studentPoint.setExcellent(0.0d);
            studentPoint.setGood(0.0d);
            studentPoint.setUd(0.0d);
            studentPoint.setSetoff(0.0d);
        }
        else
        {
            double summary_weight = (sum_event_weight_t + eventWeightD * 10) / 10;
//        (Балл за экзамен*нормированный вес экзамена)+итоговый рейтинг (без учета экзамена)= (points / summary_weight) * 100;

//            dRange3 / 100 * summary_weight = minPoints3;
//
//            minPoints3 - points = 0;
//
//            minPoints3 * examWeightD +
//
//            double rating = (points / sum_event_weight) * 100;
//
//
//            a = (d/f * 100 - c)/b
            if (isExam)
            {
                mark3 = (dRange3 / 100 * summary_weight - points) * eventMaxPointD / eventWeightD;
                mark4 = (dRange4 / 100 * summary_weight - points) * eventMaxPointD / eventWeightD;
                mark5 = (dRange5 / 100 * summary_weight - points) * eventMaxPointD / eventWeightD;

                if (mark3 <= eventMaxPointD)
                {
                    if (mark3 > eventMinPointD)
                        studentPoint.setUd(mark3);
                    else
                        studentPoint.setUd(eventMinPointD);
                }
                if (mark4 <= eventMaxPointD)
                {
                    if (mark4 > eventMinPointD)
                        studentPoint.setGood(mark4);
                    else
                        studentPoint.setGood(eventMinPointD);
                }
                if (mark5 <= eventMaxPointD)
                {
                    if (mark5 > eventMinPointD)
                        studentPoint.setExcellent(mark5);
                    else
                        studentPoint.setExcellent(eventMinPointD);
                }
            }
            else
            {
                setoff = (dSetoffRange / 100 * summary_weight - points) * eventMaxPointD / eventWeightD;
                if (setoff <= eventMaxPointD)
                {
                    if (setoff > eventMinPointD)
                        studentPoint.setSetoff(setoff);
                    else
                        studentPoint.setSetoff(eventMinPointD);
                }
            }
        }
        return studentPoint;
    }

    @Override
    public byte[] printRatingSheetTotal(Long journalGroupId, boolean isExam)
    {
        final UnisessionCommonTemplate template = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(),
                                                           isExam ? UnisessionCommonTemplateCodes.FEFU_RATING_SHEET_EXAM_TOTAL : UnisessionCommonTemplateCodes.FEFU_RATING_SHEET_SETOFF_TOTAL);
        RtfDocument rtf = new RtfReader().read(template.getContent());

        TrJournalGroup journalGroup = get(TrJournalGroup.class, journalGroupId);
        Collection<EppStudentWorkPlanElement> studentList = TrJournalManager.instance().dao().getStudentList(journalGroup);
        CollectionUtils.filter(studentList, slot -> slot.getRemovalDate() == null);

        Collection<IBrsDao.ISessionMarkCalc> calcs = TrBrsCoefficientManager.instance().brsDao().getCalculatedSessionMark(journalGroup);

        RtfDocument result = null;

        for (final IBrsDao.ISessionMarkCalc calc : calcs)
        {
            fillTableSheetTotal(rtf, calc, studentList, journalGroup, isExam);
            final RtfDocument document = rtf.getClone();

            fillHeader(document, journalGroup, studentList, calc, isExam);

            if (null == result)
            {
                result = document;
            }
            else
            {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
            }
        }

        if (null == result)
            throw new ApplicationException("Не выбраны ведомости для печати.");

        return RtfUtil.toByteArray(result);
    }

    /**
     * Итоговая ведомость
     */
    private void fillTableSheetTotal(RtfDocument rtf, IBrsDao.ISessionMarkCalc calculatedRating,
                                     Collection<EppStudentWorkPlanElement> studentList, TrJournalGroup journalGroup, boolean isExam)
    {
        List<String[]> table = new ArrayList<>();
        int total5 = 0;
        int total4 = 0;
        int total3 = 0;
        int totalSetoff = 0;
        int totalFail = 0;

        int rowNumber = 1;
        IBrsDao.ICurrentRatingCalc calc = TrBrsCoefficientManager.instance().brsDao().getCalculatedRating(journalGroup.getJournal());
        for (EppStudentWorkPlanElement student : studentList)
        {
            IBrsDao.IStudentSessionMarkData studentSessionMark = calculatedRating.getSessionMark(student);

            List<String> tableRow = new ArrayList<>();
            tableRow.add(String.valueOf(rowNumber++));
            tableRow.add(student.getStudent().getFio());

            if (studentSessionMark != null)
            {
                IBrsDao.IStudentCurrentRatingData ratingData = calc.getCurrentRating(student);
                tableRow.add(ratingFormatter.format(ratingData.getRatingAdditParam(FefuBrs.TOTAL_COLUMN_KEY).getValue()));
                ISessionBrsDao.IRatingValue iRatingValue = ratingData.getRatingAdditParam(FefuBrs.MARK_COLUMN_KEY);
                String markStr = iRatingValue != null ? iRatingValue.getMessage() : "";
                switch (markStr)
                {
                    case FefuBrs.TEXT_MARK_5:
                        total5++;
                        break;
                    case FefuBrs.TEXT_MARK_4:
                        total4++;
                        break;
                    case FefuBrs.TEXT_MARK_3:
                        total3++;
                        break;
                    case FefuBrs.TEXT_MARK_SETOFF:
                        totalSetoff++;
                        markStr = FefuBrs.TEXT_MARK_SETOFF_FULL;
                        break;
                    default:
                        markStr = isExam ? FefuBrs.TEXT_MARK_EXAM_FAIL : FefuBrs.TEXT_MARK_SETOFF_FAIL_FULL;
                        totalFail++;
                }
                tableRow.add(markStr);
            }
            table.add(tableRow.toArray(new String[tableRow.size()]));
        }

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("totalExcelent", String.valueOf(total5));
        modifier.put("totalGood", String.valueOf(total4));
        modifier.put("totalUd", String.valueOf(total3));
        modifier.put("totalSetoff", String.valueOf(totalSetoff));
        modifier.put("totalFail", String.valueOf(totalFail));
        modifier.modify(rtf);

        new RtfTableModifier()
                .put("T", table.toArray(new String[table.size()][]))
                .modify(rtf);
    }
}
