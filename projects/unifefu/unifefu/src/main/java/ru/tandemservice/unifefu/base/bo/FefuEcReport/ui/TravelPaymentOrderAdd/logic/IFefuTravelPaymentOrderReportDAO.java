/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.TravelPaymentOrderAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;

import java.util.Collection;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 10.08.2016
 */
public interface IFefuTravelPaymentOrderReportDAO extends INeedPersistenceSupport
{
    byte[] buildReport(EnrollmentCampaign enrollmentCampaign,
                       List<EntrantEnrollmentOrderType> entrantEnrollmentOrderTypeList,
                       Collection<EnrollmentOrder> enrollmentOrderList,
                       Double minSumMark4AdditionalAcademGrant3,
                       Double minSumMark4AdditionalAcademGrant4,
                       Double minMark,
                       boolean includeForeignPerson);
}