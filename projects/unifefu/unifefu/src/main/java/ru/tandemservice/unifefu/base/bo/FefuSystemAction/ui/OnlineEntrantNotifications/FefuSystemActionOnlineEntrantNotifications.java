/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.OnlineEntrantNotifications;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.*;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic.FefuProcessedOnlineEntrantsDSHandler;

/**
 * @author nvankov
 * @since 6/17/13
 */
@Configuration
public class FefuSystemActionOnlineEntrantNotifications extends BusinessComponentManager
{
    public static final String ONLINE_ENTRANT_DS = "onlineEntrantDS";
    public static final String ENR_CAMPAIGN_DS = "enrCampaignDS";
    public static final String ENTRANT_TYPE_DS = "entrantTypeDS";

    public static final String FIO_COLUMN = "fio";
    public static final String PROCESS_DATE_COLUMN = "processDate";
    public static final String ENTRANT_TYPE_COLUMN = "type";

    @Bean
    public ColumnListExtPoint onlineEntrantCL()
    {
        return columnListExtPointBuilder(ONLINE_ENTRANT_DS)
                .addColumn(checkboxColumn("select"))
                .addColumn(textColumn(FIO_COLUMN, "title").order())
                .addColumn(dateColumn(PROCESS_DATE_COLUMN, "processedDate").formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn(ENTRANT_TYPE_COLUMN, "type").order())
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ONLINE_ENTRANT_DS, onlineEntrantCL(), onlineEntrantDSHandler()))
                .addDataSource(selectDS(ENR_CAMPAIGN_DS, enrollmentCampaignDSHandler()))
                .addDataSource(selectDS(ENTRANT_TYPE_DS, entrantTypeComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler onlineEntrantDSHandler()
    {
        return new FefuProcessedOnlineEntrantsDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler enrollmentCampaignDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EnrollmentCampaign.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", EnrollmentCampaign.useCompetitionGroup()), DQLExpressions.value(Boolean.TRUE)));
                ep.dqlBuilder.order(DQLExpressions.property("e", EnrollmentCampaign.educationYear().intValue()), OrderDirection.desc);
            }
        };
    }


    @Bean
    public IDefaultComboDataSourceHandler entrantTypeComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(FefuSystemActionManager.instance().entrantTypeExtPoint());
    }

}
