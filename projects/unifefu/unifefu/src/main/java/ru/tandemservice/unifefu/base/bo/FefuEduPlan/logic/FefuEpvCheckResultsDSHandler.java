/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.logic;

import com.google.common.collect.Lists;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 03.12.2014
 */
public class FefuEpvCheckResultsDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public FefuEpvCheckResultsDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
//        FefuEduPlanVersionCheckResults

        List<DataWrapper> resultList = Lists.newArrayList();

//        Long eduStandardId = context.get(FefuEduStdParametersTabUI.EDU_STANDARD_ID);
//        List<FefuCompetence2EppStateEduStandardRel> relList = DataAccessServices.dao().getList(FefuCompetence2EppStateEduStandardRel.class, FefuCompetence2EppStateEduStandardRel.eduStandard().id(), eduStandardId, FefuCompetence2EppStateEduStandardRel.number().s());
//        Map<EppSkillGroup, List<FefuCompetence2EppStateEduStandardRel>> fefuCompetenceMap = Maps.newHashMap();
//
//        for (FefuCompetence2EppStateEduStandardRel rel : relList)
//        {
//            EppSkillGroup group = rel.getFefuCompetence().getEppSkillGroup();
//
//            if (!fefuCompetenceMap.containsKey(group))
//                fefuCompetenceMap.put(group, Lists.<FefuCompetence2EppStateEduStandardRel>newArrayList());
//            fefuCompetenceMap.get(group).add(rel);
//        }
//
//        for (EppSkillGroup group : fefuCompetenceMap.keySet())
//        {
//            int i = 0;
//            List<FefuCompetence2EppStateEduStandardRel> groupRelList = fefuCompetenceMap.get(group);
//
//            for (FefuCompetence2EppStateEduStandardRel rel : groupRelList)
//            {
//                DataWrapper wrapper = new DataWrapper(rel.getId(), rel.getId().toString(), rel);
//                wrapper.setProperty(FefuEduStdParametersTab.FEFU_COMPETENCE_CODE, group.getShortTitle() + "-" + ++i);
//                wrapper.setProperty(FefuEduStdParametersTab.FEFU_COMPETENCE_TITLE, rel.getFefuCompetence().getTitle());
//                resultList.add(wrapper);
//            }
//        }
//        Collections.sort(resultList, new Comparator<DataWrapper>()
//        {
//            @Override
//            public int compare(DataWrapper d1, DataWrapper d2)
//            {
//                FefuCompetence2EppStateEduStandardRel r1 = d1.getWrapped();
//                FefuCompetence2EppStateEduStandardRel r2 = d2.getWrapped();
//                return Integer.compare(r1.getNumber(), r2.getNumber());
//            }
//        });
        return ListOutputBuilder.get(input, resultList).pageable(false).build();
    }
}