package ru.tandemservice.unifefu.events.dset;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import ru.tandemservice.unifefu.dao.daemon.FEFUAllStudExportDaemonDAO;
import ru.tandemservice.unifefu.dao.daemon.IFEFUAllStudExportDaemonDAO;
import ru.tandemservice.unifefu.entity.ws.MdbViewPersonForeignlanguage;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vnekrasov
 */
public class FefuPersonForeignLanguageListener extends ParamTransactionCompleteListener<Boolean>
{

    @SuppressWarnings("unchecked")
    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, PersonForeignLanguage.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, PersonForeignLanguage.class, this);

    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        /*DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(PersonForeignLanguage.class, "pfl")
                .column(property(PersonForeignLanguage.person().id().fromAlias("pfl")))
                .where(in(property(PersonForeignLanguage.id().fromAlias("pfl")), params));

        List<Long> personIds = eBuilder.createStatement(session).list();

        */
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(MdbViewPersonForeignlanguage.class);
        deleteBuilder.where(in(property(MdbViewPersonForeignlanguage.personForeignLanguage().id()), params));
        deleteBuilder.createStatement(session).execute();
        IFEFUAllStudExportDaemonDAO.instance.get().doRegisterEntity(MdbViewPersonForeignlanguage.ENTITY_NAME, params);
        FEFUAllStudExportDaemonDAO.DAEMON.registerAfterCompleteWakeUp(session);
        return true;
    }

    @Override
    @Transactional(readOnly = false)
    public void afterCompletion(Session session, int status, Collection<Long> params, Boolean beforeCompletionResult)
    {




    }

}
