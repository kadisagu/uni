/* $Id$ */
package ru.tandemservice.unifefu.component.catalog.fefuStudentPractice.FefuStudentPracticePub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.bo.Declinable.ui.Edit.DeclinableEdit;
import org.tandemframework.shared.commonbase.base.bo.Declinable.ui.Edit.DeclinableEditUI;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorLanguage;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice;

/**
 * @author Nikolay Fedorovskih
 * @since 25.06.2013
 */
public class Controller extends DefaultCatalogPubController<FefuStudentPractice, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<FefuStudentPractice> createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);
        DynamicListDataSource<FefuStudentPractice> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", FefuStudentPractice.title().s()));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        dataSource.addColumn(new ActionColumn("Редактировать падежные формы", UniDefines.ICO_EDIT_CASE, "onClickEditItemDeclination")
                                     .setPermissionKey(model.getCatalogItemEdit())
                                     .setDisableHandler(entity -> !((FefuStudentPractice) entity).isIsParent()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem",
                                              "Удалить элемент «{0}» из справочника?", FefuStudentPractice.P_TITLE)
                                     .setPermissionKey(model.getCatalogItemDelete())
                                     .setDisabledProperty(FefuStudentPractice.isParent()));
        return dataSource;
    }

    public void onClickEditItemDeclination(IBusinessComponent context)
    {
        final InflectorLanguage lang = IUniBaseDao.instance.get().get(InflectorLanguage.class, InflectorLanguage.enabled().s(), Boolean.TRUE);
        if (null == lang)
            throw new ApplicationException("Не установлен активный язык склонений.");

        context.createDefaultChildRegion(new ComponentActivator(DeclinableEdit.class.getSimpleName(), new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, context.getListenerParameter())
                .add(DeclinableEditUI.BIND_LANGUAGE_ID, lang.getId())
        ));

    }
}