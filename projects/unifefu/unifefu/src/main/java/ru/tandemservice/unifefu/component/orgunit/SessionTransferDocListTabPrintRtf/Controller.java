/* $Id $ */
package ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTabPrintRtf;


import org.apache.commons.io.IOUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.unifefu.entity.SessionAttSheetDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author Rostuncev Savva
 * @since 20.03.2014
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{

    public static final String sexM = "подавший";
    public static final String sexW = "подавшая";
    public static final String DVFU = "ДВФУ";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));

        try
        {
            BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true);
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public RtfDocument createRtfDocument() throws IOException
    {
        InputStream in = getClass().getClassLoader().getResourceAsStream("unifefu/templates/StatementTransferTests.rtf");
        byte[] template = IOUtils.toByteArray(in);
        return new RtfReader().read(template);
    }

    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component) throws IOException
    {
        Model model = getModel(component);
        final RtfDocument document = createRtfDocument();

        GrammaCase rusCase = GrammaCase.NOMINATIVE;
        boolean isMaleSex = model.getSessionTransferDocument().getTargetStudent().getPerson().getIdentityCard().getSex().isMale();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(model.getSessionTransferDocument().getTargetStudent().getPerson().getIdentityCard().getLastName(), rusCase, isMaleSex));
        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(model.getSessionTransferDocument().getTargetStudent().getPerson().getIdentityCard().getFirstName(), rusCase, isMaleSex));
        if (org.apache.commons.lang.StringUtils.isNotEmpty(model.getSessionTransferDocument().getTargetStudent().getPerson().getIdentityCard().getMiddleName()))
        {
            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(model.getSessionTransferDocument().getTargetStudent().getPerson().getIdentityCard().getMiddleName(), rusCase, isMaleSex));
        }
        String nameUniversity = "";
        if (model.getSessionTransferDocument() instanceof SessionTransferOutsideDocument || model.getSessionTransferDocument() instanceof SessionAttSheetDocument)
        {
            nameUniversity = model.getSessionTransferDocument().getEduInstitutionTitle();
        }
        if (model.getSessionTransferDocument() instanceof SessionTransferInsideDocument)
        {
            nameUniversity = DVFU;
        }

        new RtfInjectModifier()
                .put("studentFIO", str.toString())
                .put("filed", model.getSessionTransferDocument().getTargetStudent().getPerson().getIdentityCard().getSex().getCode().equals("1") ? sexM : sexW)
                .put("course", model.getSessionTransferDocument().getTargetStudent().getCourse().getTitle())
                .put("base", model.getSessionTransferDocument().getComment())
                .put("specialty", model.getSessionTransferDocument().getTargetStudent().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle())
                .put("nameUniversity", nameUniversity)
                .modify(document);

        prepareTableModifier(model).modify(document);

        deactivate(component);
        return new CommonBaseRenderer().rtf().fileName(getFileName()).document(document);
    }

    public RtfTableModifier prepareTableModifier(final Model model)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();

        tableModifier.put("T", getDao().getSessionTransferDocument(model));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {

                if (model.getCaptainRowList().contains(rowIndex))
                {
                    cell.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.QC));
                    return new RtfString().boldBegin().append(IRtfData.B).append(value).append(IRtfData.B, 0).boldEnd().toList();
                } else
                {
                    if (colIndex == 1)
                    {
                        cell.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.QL));
                    } else
                    {
                        cell.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.QC));
                    }
                    return new RtfString().append(IRtfData.I).append(value).append(IRtfData.I, 0).toList();
                }
            }
        });

        return tableModifier;
    }

    protected String getFileName()
    {
        return "StatementTransferTests.rtf";
    }
}
