/* $Id$ */
package ru.tandemservice.unifefu.events.mobile;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.listener.IHibernateDeleteListener;
import org.tandemframework.hibsupport.event.single.type.HibernateDeleteEvent;
import ru.tandemservice.unifefu.base.bo.FefuMobile.FefuMobileManager;
import ru.tandemservice.unifefu.base.bo.FefuMobile.logic.FefuMobileDao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 26.08.2014
 */
public class MobileEntityDeleteListener extends FilteredSingleEntityEventListener<HibernateDeleteEvent> implements IHibernateDeleteListener
{
    @Override
    public void onFilteredEvent(HibernateDeleteEvent event)
    {
        if (!FefuMobileDao.AUTO_SYNC_ENABLED) return;

        IEntityMeta meta = EntityRuntime.getMeta(event.getEntity());
        List<Class> entityTypeSet = Arrays.asList(IMobileEntityToBeTracked.ENTITY_TYPES_TO_BE_TRACKED);
        if (null != meta && entityTypeSet.contains(meta.getEntityClass()))
        {
            System.out.println(event.getEntity().getId());
            List<Long> deletedEntityList = new ArrayList<>();
            deletedEntityList.add(event.getEntity().getId());
            FefuMobileManager.instance().dao().doRegisterChangedEntities(deletedEntityList, true);
        }
    }
}