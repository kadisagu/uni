/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu10.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubController;
import ru.tandemservice.unifefu.entity.FefuEduEnrolmentToSecondAndNextCourseStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 17.04.2013
 */
public class Controller extends ModularStudentExtractPubController<FefuEduEnrolmentToSecondAndNextCourseStuExtract, IDAO, Model>
{
}