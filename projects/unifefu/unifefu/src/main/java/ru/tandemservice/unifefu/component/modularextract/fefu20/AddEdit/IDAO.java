/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu20.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 20.01.2015
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuEnrollStuDPOExtract, Model>
{
}