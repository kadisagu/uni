/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPerson.ui.LoginEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.unifefu.base.bo.FefuPerson.FefuPersonManager;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonExt;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 05.12.2014
 */
@Input(
        @Bind(key = "personId", binding = "personId")
)
public class FefuPersonLoginEditUI extends UIPresenter
{
    private Long _personId;
    private Person _person;
    private Person2PrincipalRelation _relation;
    private FefuNsiPersonExt _extension;
    private String _login;
    private AuthenticationType _authenticationType;
    private List<AuthenticationType> _authenticationTypeList;

    @Override
    public void onComponentRefresh()
    {
        setPerson(DataAccessServices.dao().getNotNull(Person.class, getPersonId()));
        _relation = DataAccessServices.dao().getByNaturalId(new Person2PrincipalRelation.NaturalId(getPerson()));
        _extension = DataAccessServices.dao().getByNaturalId(new FefuNsiPersonExt.NaturalId(getPerson()));

        if (null != _extension) setLogin(_extension.getLogin());
        else setLogin(getRelation().getPrincipal().getLogin());

        setAuthenticationTypeList(CatalogManager.instance().dao().getCatalogItemList(AuthenticationType.class));
        setAuthenticationType(getRelation().getPrincipal().getAuthenticationType());
    }

    public void onClickApply()
    {
        FefuPersonManager.instance().dao().updateFefuLogin(getPerson(), getLogin(), getAuthenticationType());
        deactivate();
    }

    public Long getPersonId()
    {
        return _personId;
    }

    public void setPersonId(Long personId)
    {
        _personId = personId;
    }

    public Person getPerson()
    {
        return _person;
    }

    public void setPerson(Person person)
    {
        _person = person;
    }

    public Person2PrincipalRelation getRelation()
    {
        return _relation;
    }

    public void setRelation(Person2PrincipalRelation relation)
    {
        _relation = relation;
    }

    public FefuNsiPersonExt getExtension()
    {
        return _extension;
    }

    public void setExtension(FefuNsiPersonExt extension)
    {
        _extension = extension;
    }

    public String getLogin()
    {
        return _login;
    }

    public void setLogin(String login)
    {
        _login = login;
    }

    public AuthenticationType getAuthenticationType()
    {
        return _authenticationType;
    }

    public void setAuthenticationType(AuthenticationType authenticationType)
    {
        _authenticationType = authenticationType;
    }

    public List<AuthenticationType> getAuthenticationTypeList()
    {
        return _authenticationTypeList;
    }

    public void setAuthenticationTypeList(List<AuthenticationType> authenticationTypeList)
    {
        _authenticationTypeList = authenticationTypeList;
    }
}