/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.context;

/**
 * @author Nikolay Fedorovskih
 * @since 28.01.2014
 */
public class ContextWSConstants
{
    /*
    "WSFW000"; // 'Other' exception thrown from a service method
    "WSFW001"; // Invalid session sent from client
    "WSFW002"; // Invalid source IP for client program
    "WSFW003"; // Invalid local access to a Web Service method
    "WSFW004"; // Access Exception (invalid operation for current session was attempted)
    "WSFW005"; // Attempt to use a tool-only method as a 'user'
    "WSFW006"; // Request must be over https
    "WSFW007"; // Originator address denied
    "WSFW008"; // Operation is invalid
    "WSFW009"; // Service is not active
    "WSFW010"; // Unknown client programs are not allowed
    "WSFW011"; // Client program is not available
    "WSFW012"; // Method called requires a valid user in the current session
    "WSFW013"; // Nonexistent proxy tool
    "WSFW014"; // Proxy Tool is not available
    "WSFW015"; // Invalid proxy tool password
    */

    public static final String UNKNOWN_ERROR = "WSFW000";
    public static final String INVALID_SESSION = "WSFW001";
    public static final String ACCESS_DENIED = "WSFW004";

    public static final String CTXWS001 = "Context.WS001"; // Invalid user/password combination
    public static final String CTXWS002 = "Context.WS002"; // Invalid password for a client program
    public static final String CTXWS003 = "Context.WS003"; // Invalid client program (or vendor) specified (or Unknown vendor/program combination if unknown agents are not allowed)
    public static final String CTXWS004 = "Context.WS004"; // Invalid user specified to emulate
    public static final String CTXWS005 = "Context.WS005"; // Unsupported authentication method configured on the server (default:rdbms is the only supported method)
    public static final String CTXWS006 = "Context.WS006"; // Proxy Tool is not currently available.
    public static final String CTXWS007 = "Context.WS007"; // User's password has expired.
}