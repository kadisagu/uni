/* $Id$ */
package ru.tandemservice.unifefu.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 26.11.2014
 */
public interface IFefuGroupDAO extends IUniBaseDao
{
    String FEFU_GROUP_DAO_BEAN_NAME = "unifefuGroupDao";

    final SpringBeanCache<IUnifefuDao> instance = new SpringBeanCache<>(FEFU_GROUP_DAO_BEAN_NAME);

    Map<Long, Long> getVacStudentToGroupMap();

}
