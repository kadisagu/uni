/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.ui.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.tapsupport.component.selection.*;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Avetisov
 * @since 29.09.2014
 */
public class EduLevelSelectModel extends CommonBaseSelectModel implements ISingleSelectModel
{
    private boolean _hierarchical;

    public EduLevelSelectModel(boolean hierarchical)
    {

        _hierarchical = hierarchical;
    }

    @Override
    public int getLevel(Object value)
    {
        int level = 0;
        if (_hierarchical)
        {
            EduLevel item = (EduLevel) value;
            while (item.getParent() != null)
            {
                item = item.getParent();
                level++;
            }
        }
        return level;
    }

    @Override
    protected IListResultBuilder createBuilder(String filter, Object o)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduLevel.class, "l").column("l");

        if (StringUtils.isNotEmpty(filter))
            builder.where(likeUpper(property("l", EduLevel.P_TITLE), value(CoreStringUtils.escapeLike(filter, true))));

        if (o != null)
            builder.where(eqValue(property("l", EduLevel.P_ID), o));
        builder.order(property("l", EduLevel.P_CODE));

        List<EduLevel> EduLevelList = DataAccessServices.dao().getList(builder);

        return new SimpleListResultBuilder<>(EduLevelList);
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        ListResult list = findValues(null);
        List objects = list.getObjects();
        if (objects.size() != list.getMaxCount())
            throw new RuntimeException("HBaseSelectModel need all elements!");

        for (Object obj : objects)
            if (getPrimaryKey(obj).equals(primaryKey))
                return obj;

        return null;
    }
}
