package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x1_12to13 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuEppRegistryModuleALoadExt

        // создана новая сущность
        {
            if (!tool.tableExists("ffepprgstrymdlaldext_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("ffepprgstrymdlaldext_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("module_id", DBType.LONG).setNullable(false),
                        new DBColumn("loadtype_id", DBType.LONG).setNullable(false),
                        new DBColumn("load_p", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuEppRegistryModuleALoadExt");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuEppWorkPlanRowPartLoad

        // создана новая сущность
        {
            if (!tool.tableExists("fefueppworkplanrowpartload_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefueppworkplanrowpartload_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("part_p", DBType.INTEGER).setNullable(false),
                        new DBColumn("row_id", DBType.LONG).setNullable(false),
                        new DBColumn("loadtype_id", DBType.LONG).setNullable(false),
                        new DBColumn("load_p", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuEppWorkPlanRowPartLoad");

        }


    }
}