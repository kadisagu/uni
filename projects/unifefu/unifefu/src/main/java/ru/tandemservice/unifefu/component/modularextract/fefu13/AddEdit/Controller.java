/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu13.AddEdit;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.e41.utils.EmployeePostVO;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;

import java.util.List;

/**
 * @author nvankov
 * @since 6/20/13
 */
public class Controller extends CommonModularStudentExtractAddEditController<SendPracticOutStuExtract, IDAO, Model>
{
    public void onChangeExternalOu(IBusinessComponent component)
    {
        Model model = getModel(component);

        // Организация
        ExternalOrgUnit currentExternalOrgUnit = model.getExtract().getPracticeExtOrgUnit();

        if(null != currentExternalOrgUnit)
        {
            // Руководитель практики от организации
            model.getExtract().setPracticeHeaderOutStr(null);

            // Договор на прохождение практики
            DQLSelectBuilder practiceContractBuilder = new DQLSelectBuilder().fromEntity(FefuPracticeContractWithExtOu.class, "pc");
            practiceContractBuilder.where(DQLExpressions.eq(DQLExpressions.property("pc", FefuPracticeContractWithExtOu.externalOrgUnit().id()), DQLExpressions.value(currentExternalOrgUnit.getId())));
            List<FefuPracticeContractWithExtOu> practiceContracts = practiceContractBuilder.createStatement(DataAccessServices.dao().getComponentSession()).list();
            if(practiceContracts.size() == 1)
                model.getExtract().setPracticeContract(practiceContracts.get(0));

        }
    }

    public void onChangeContract(IBusinessComponent component)
    {
        Model model = getModel(component);

        // Договор
        FefuPracticeContractWithExtOu contract =  model.getExtract().getPracticeContract();

        // Руководитель практики от организации

        if(null != contract)
        {
            if(null != contract.getHeaderExtOu())
            {
                model.getExtract().setPracticeHeaderOutStr(contract.getHeaderExtOu().getFio());
            }
            else
            {
                model.getExtract().setPracticeHeaderOutStr(null);
            }
        }
        else
        {
            model.getExtract().setPracticeHeaderOutStr(null);
        }
    }

    public void onPreventAccidentsICChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        if(null != model.getPreventAccidentsIC())
        {
            model.getExtract().setPreventAccidentsICStr(getIofStr(model.getPreventAccidentsIC()));
        }
    }

    public void onResponsForRecieveCashChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        if(null != model.getResponsForRecieveCash())
        {
            model.getExtract().setResponsForRecieveCashStr(getFioAccustiveStr(model.getResponsForRecieveCash()));
        }
    }

    public void onChangePracticeHeaderInner(IBusinessComponent component)
    {
        Model model = getModel(component);

        // Руководитель практики от ОУ
        if(null != model.getPracticeHeaderInner())
        {
            model.getExtract().setPracticeHeaderInnerStr(getFioNominativeStr(model.getPracticeHeaderInner()));
        }
    }

    private String getIofStr(EmployeePostVO employeePostVO)
    {
        StringBuilder iofStrBuilder = new StringBuilder();

        iofStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));

        String preventPostAT = employeePostVO.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
        String preventAccidentsICAccustive = !StringUtils.isEmpty(preventPostAT) ? preventPostAT :
                employeePostVO.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(preventAccidentsICAccustive))
            iofStrBuilder.append(StringUtils.isEmpty(iofStrBuilder.toString()) ? "" : " ").append(preventAccidentsICAccustive.toLowerCase());

        iofStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedIofInitials(employeePostVO.getEmployeePost().getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));

        return iofStrBuilder.toString();
    }

    private String getFioAccustiveStr(EmployeePostVO employeePostVO)
    {
        EmployeePost practiceHeader = employeePostVO.getEmployeePost();

        StringBuilder fioStrBuilder = new StringBuilder();

        fioStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));

        String practiceHeaderPostAT = practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
        String practiceHeaderAccustive = !StringUtils.isEmpty(practiceHeaderPostAT) ? practiceHeaderPostAT :
                practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(practiceHeaderAccustive))
            fioStrBuilder.append(StringUtils.isEmpty(fioStrBuilder.toString()) ? "" : " ").append(practiceHeaderAccustive.toLowerCase());

        fioStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(practiceHeader.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));

        return fioStrBuilder.toString();
    }

    private String getFioNominativeStr(EmployeePostVO employeePostVO)
    {
        EmployeePost practiceHeader = employeePostVO.getEmployeePost();

        StringBuilder fioStrBuilder = new StringBuilder();

        fioStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));

        String practiceHeaderPostNT = practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        String practiceHeaderNominative = !StringUtils.isEmpty(practiceHeaderPostNT) ? practiceHeaderPostNT :
                practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(practiceHeaderNominative))
            fioStrBuilder.append(StringUtils.isEmpty(fioStrBuilder.toString()) ? "" : " ").append(practiceHeaderNominative.toLowerCase());

        fioStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(practiceHeader.getPerson().getIdentityCard(), GrammaCase.NOMINATIVE));

        return fioStrBuilder.toString();
    }

    private String getDegreeShortTitleWithDots(PersonAcademicDegree prevDegree)
    {
        String degreeShortTitleWithDots = "";
        if (null != prevDegree)
        {
            String[] degreeTitle = prevDegree.getAcademicDegree().getTitle().toLowerCase().split(" ");
            StringBuilder shortDegreeTitle = new StringBuilder();
            for (String deg : Lists.newArrayList(degreeTitle))
            {
                shortDegreeTitle.append(deg.charAt(0)).append(".");
            }
            degreeShortTitleWithDots += shortDegreeTitle;
        }
        return degreeShortTitleWithDots;
    }
}
