/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.OrderLogView;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumLog;
import ru.tandemservice.unifefu.tapestry.formatter.FefuCrazyXmlFormatter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 23.07.2013
 */
@Configuration
public class DirectumOrderLogView extends BusinessComponentManager
{
    public static final String ORDER_LOG_DS = "orderLogDS";
    public static final String ORDER_ID_PARAM = "orderId";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ORDER_LOG_DS, getOrderLogDS(), orderLogDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getOrderLogDS()
    {
        return columnListExtPointBuilder(ORDER_LOG_DS)
                .addColumn(publisherColumn("operationDateTime", FefuDirectumLog.operationDateTime()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME)
                        .publisherLinkResolver(new IPublisherLinkResolver()
                        {
                            @Override
                            public Object getParameters(IEntity entity)
                            {
                                Map<String, Object> params = new HashMap<>();
                                params.put(UIPresenter.PUBLISHER_ID, entity.getId());
                                return params;
                            }

                            @Override
                            public String getComponentName(IEntity entity)
                            {
                                return null;
                            }
                        }).order().create())
                .addColumn(textColumn("operationType", FefuDirectumLog.operationType()).order())
                .addColumn(textColumn("executor", FefuDirectumLog.executorStr()).order())
                .addColumn(textColumn("operationResult", FefuDirectumLog.operationResult()).order())
                .addColumn(textColumn("directumOrderId", FefuDirectumLog.directumOrderId()))
                // DEV-4728
                .addColumn(blockColumn("directumTaskId", "directumTaskBlockColumn").order())
                .addColumn(textColumn("datagram", FefuDirectumLog.datagram()).formatter(FefuCrazyXmlFormatter.INSTANCE))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> orderLogDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long orderId = context.get(ORDER_ID_PARAM);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuDirectumLog.class, "e").column("e")
                        .where(DQLExpressions.eq(DQLExpressions.property(FefuDirectumLog.order().id().fromAlias("e")), DQLExpressions.value(orderId)))
                        .order(DQLExpressions.property("e", input.getEntityOrder().getColumnName()), input.getEntityOrder().getDirection());
                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).build();
            }
        };
    }
}