/* $Id$ */
package ru.tandemservice.unifefu.ws.directumtest;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.HandlerRegistry;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 21.01.2014
 */
public class DirectumServiceTest
{
    public static void main(String[] args)
    {
        FefuDirectumServiceImplLocator locator = new FefuDirectumServiceImplLocator();

        try
        {
            HandlerRegistry registry = locator.getHandlerRegistry();
            QName servicePort = new QName("http://www.qa.com","FefuDirectumServicePort");
            List handlerChain = registry.getHandlerChain(servicePort);
            HandlerInfo info = new HandlerInfo();
            info.setHandlerClass(SecurityHandler.class);
            handlerChain.add(info);

            //String result = locator.getFefuDirectumServicePort().commitOrder("894144", "xxxx", NsiDatagramUtil.formatDate(new Date()));
            //String result = locator.getFefuDirectumServicePort().sendToRevision("894144");
            //String result = locator.getFefuDirectumServicePort().rejectOrder("894376");
            //String result = locator.getFefuDirectumServicePort().commitOrderWithSignedScanUrl("1001080", "xxxx", NsiDatagramUtil.formatDate(new Date()), "http://directum.dvfu.ru/doc.asp?sys=dvfu&id=1023301");
            //String result = locator.getFefuDirectumServicePort().commitOrderWithSignedScanUrl("894375", "xxxx", NsiDatagramUtil.formatDate(new Date()), "http://directum.dvfu.ru/doc.asp?sys=dvfu&id=894375");
            //String result = locator.getFefuDirectumServicePort().sendToRevision("894374");
            //String result = locator.getFefuDirectumServicePort().rejectOrder("1001080");
//            String result = locator.getFefuDirectumServicePort().updateOrderWithSignedScanUrl("1001080", "http://directum.dvfu.ru/doc.asp?sys=dvfu&id=1023301");
            String result = locator.getFefuDirectumServicePort().updateOrderRequisites("773035", "123", "2015-01-26", "http://directum.dvfu.ru/doc.asp?sys=dvfu&id=1023301");
            System.out.println("Result: " + result);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
