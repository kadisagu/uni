/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EppRegistry.ui.DisciplineList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uniedu.program.bo.EduProgram.EduProgramManager;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractList;
import ru.tandemservice.unifefu.base.ext.EppRegistry.EppRegistryExtManager;
import ru.tandemservice.unifefu.base.ext.EppRegistry.logic.FefuRegistryBaseDSHandler;
import ru.tandemservice.unifefu.base.ext.EppRegistry.ui.Base.FefuRegistryListAddon;
import ru.tandemservice.unifefu.base.ext.EppRegistry.ui.Base.IFefuRegistryListConfig;
import ru.tandemservice.unifefu.entity.catalog.codes.EppRegistryStructureCodes;

/**
 * @author Irina Ugfeld
 * @since 15.03.2016
 */
@Configuration
public class EppRegistryDisciplineListExt extends BusinessComponentExtensionManager implements IFefuRegistryListConfig {

    @Autowired
    private ru.tandemservice.uniepp.base.bo.EppRegistry.ui.DisciplineList.EppRegistryDisciplineList eppRegistryDisciplineList;

    @Bean
    public PresenterExtension presenterExtension() {
        EppRegistryExtManager eppRegistryExtManager = EppRegistryExtManager.instance();
        return presenterExtensionBuilder(eppRegistryDisciplineList.presenterExtPoint())
                .replaceDataSource(searchListDS(EppRegistryAbstractList.ELEMENT_DS, eppRegistryDisciplineList.getColumns(), getDisciplineListDSHandler()))
                .addDataSource(selectDS(EPP_REGISTRY_STRUCTURE_DS, getEppRegistryStructureDSHandler()))
                .addDataSource(selectDS(EPP_F_CONTROL_ACTION_TYPE_DS, eppRegistryExtManager.getEppFControlActionTypeDSHandler()))
                .addDataSource(EduProgramManager.instance().programKindDSConfig())
                .addDataSource(selectDS(SUBJECT_INDEX_DS, eppRegistryExtManager.getSubjectIndexDSHandler()))
                .addDataSource(selectDS(EDU_ORG_UNIT_DS, eppRegistryExtManager.getEduOrgUnitDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addAddon(uiAddon(FefuRegistryListAddon.NAME, FefuRegistryListAddon.class))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler getEppRegistryStructureDSHandler() {
        return EppRegistryExtManager.instance().getEppRegistryStructureDSHandler(EppRegistryStructureCodes.REGISTRY_DISCIPLINE);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> getDisciplineListDSHandler() {
        return new FefuRegistryBaseDSHandler(getName());
    }

    @Bean
    public ColumnListExtension getElementListDSColumnExtension() {
        return EppRegistryExtManager.instance().getElementListDSColumnExtension(eppRegistryDisciplineList.getColumns());
    }
}