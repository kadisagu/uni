package ru.tandemservice.unifefu.ws.blackboard.context.gen;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.2.12
 * Thu Jan 30 16:53:23 YEKT 2014
 * Generated source version: 2.2.12
 * 
 */
 
@WebService(targetNamespace = "http://context.ws.blackboard/", name = "Context.WSPortType")
@XmlSeeAlso({ObjectFactory.class})
public interface ContextWSPortType {

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @WebResult(name = "getMyMembershipsResponse", targetNamespace = "http://context.ws.blackboard", partName = "parameters")
    @Action(input = "getMyMemberships", output = "//context.ws.blackboard/Context/getMyMembershipsResponse")
    @WebMethod(action = "getMyMemberships")
    public GetMyMembershipsResponse getMyMemberships();

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @WebResult(name = "getSystemInstallationIdResponse", targetNamespace = "http://context.ws.blackboard", partName = "parameters")
    @Action(input = "getSystemInstallationId", output = "//context.ws.blackboard/Context/getSystemInstallationIdResponse")
    @WebMethod(action = "getSystemInstallationId")
    public GetSystemInstallationIdResponse getSystemInstallationId();

    @WebResult(name = "return", targetNamespace = "http://context.ws.blackboard")
    @Action(input = "extendSessionLife", output = "//context.ws.blackboard/Context/extendSessionLifeResponse")
    @RequestWrapper(localName = "extendSessionLife", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.ExtendSessionLife")
    @WebMethod(action = "extendSessionLife")
    @ResponseWrapper(localName = "extendSessionLifeResponse", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.ExtendSessionLifeResponse")
    public java.lang.Boolean extendSessionLife(
        @WebParam(name = "additionalSeconds", targetNamespace = "http://context.ws.blackboard")
        java.lang.Long additionalSeconds
    );

    @WebResult(name = "return", targetNamespace = "http://context.ws.blackboard")
    @Action(input = "getServerVersion", output = "//context.ws.blackboard/Context/getServerVersionResponse")
    @RequestWrapper(localName = "getServerVersion", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.GetServerVersion")
    @WebMethod(action = "getServerVersion")
    @ResponseWrapper(localName = "getServerVersionResponse", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.GetServerVersionResponse")
    public ru.tandemservice.unifefu.ws.blackboard.context.gen.VersionVO getServerVersion(
        @WebParam(name = "unused", targetNamespace = "http://context.ws.blackboard")
        ru.tandemservice.unifefu.ws.blackboard.context.gen.VersionVO unused
    );

    @WebResult(name = "return", targetNamespace = "http://context.ws.blackboard")
    @Action(input = "getRequiredEntitlements", output = "//context.ws.blackboard/Context/getRequiredEntitlementsResponse")
    @RequestWrapper(localName = "getRequiredEntitlements", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.GetRequiredEntitlements")
    @WebMethod(action = "getRequiredEntitlements")
    @ResponseWrapper(localName = "getRequiredEntitlementsResponse", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.GetRequiredEntitlementsResponse")
    public java.util.List<java.lang.String> getRequiredEntitlements(
        @WebParam(name = "method", targetNamespace = "http://context.ws.blackboard")
        java.lang.String method
    );

    @WebResult(name = "return", targetNamespace = "http://context.ws.blackboard")
    @Action(input = "login", output = "//context.ws.blackboard/Context/loginResponse")
    @RequestWrapper(localName = "login", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.Login")
    @WebMethod(action = "login")
    @ResponseWrapper(localName = "loginResponse", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.LoginResponse")
    public java.lang.Boolean login(
        @WebParam(name = "userid", targetNamespace = "http://context.ws.blackboard")
        java.lang.String userid,
        @WebParam(name = "password", targetNamespace = "http://context.ws.blackboard")
        java.lang.String password,
        @WebParam(name = "clientVendorId", targetNamespace = "http://context.ws.blackboard")
        java.lang.String clientVendorId,
        @WebParam(name = "clientProgramId", targetNamespace = "http://context.ws.blackboard")
        java.lang.String clientProgramId,
        @WebParam(name = "loginExtraInfo", targetNamespace = "http://context.ws.blackboard")
        java.lang.String loginExtraInfo,
        @WebParam(name = "expectedLifeSeconds", targetNamespace = "http://context.ws.blackboard")
        java.lang.Long expectedLifeSeconds
    );

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @WebResult(name = "logoutResponse", targetNamespace = "http://context.ws.blackboard", partName = "parameters")
    @Action(input = "logout", output = "//context.ws.blackboard/Context/logoutResponse")
    @WebMethod(action = "logout")
    public LogoutResponse logout();

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @WebResult(name = "initializeResponse", targetNamespace = "http://context.ws.blackboard", partName = "parameters")
    @Action(input = "initialize", output = "//context.ws.blackboard/Context/initializeResponse")
    @WebMethod(action = "initialize")
    public InitializeResponse initialize();

    @WebResult(name = "return", targetNamespace = "http://context.ws.blackboard")
    @Action(input = "emulateUser", output = "//context.ws.blackboard/Context/emulateUserResponse")
    @RequestWrapper(localName = "emulateUser", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.EmulateUser")
    @WebMethod(action = "emulateUser")
    @ResponseWrapper(localName = "emulateUserResponse", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.EmulateUserResponse")
    public java.lang.Boolean emulateUser(
        @WebParam(name = "userToEmulate", targetNamespace = "http://context.ws.blackboard")
        java.lang.String userToEmulate
    );

    @WebResult(name = "return", targetNamespace = "http://context.ws.blackboard")
    @Action(input = "deactivateTool", output = "//context.ws.blackboard/Context/deactivateToolResponse")
    @RequestWrapper(localName = "deactivateTool", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.DeactivateTool")
    @WebMethod(action = "deactivateTool")
    @ResponseWrapper(localName = "deactivateToolResponse", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.DeactivateToolResponse")
    public ru.tandemservice.unifefu.ws.blackboard.context.gen.DeactivateToolResultVO deactivateTool(
        @WebParam(name = "ignore", targetNamespace = "http://context.ws.blackboard")
        java.lang.String ignore
    );

    @SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
    @WebResult(name = "initializeVersion2Response", targetNamespace = "http://context.ws.blackboard", partName = "parameters")
    @Action(input = "initializeVersion2", output = "//context.ws.blackboard/Context/initializeVersion2Response")
    @WebMethod(action = "initializeVersion2")
    public InitializeVersion2Response initializeVersion2();

    @WebResult(name = "return", targetNamespace = "http://context.ws.blackboard")
    @Action(input = "getMemberships", output = "//context.ws.blackboard/Context/getMembershipsResponse")
    @RequestWrapper(localName = "getMemberships", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.GetMemberships")
    @WebMethod(action = "getMemberships")
    @ResponseWrapper(localName = "getMembershipsResponse", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.GetMembershipsResponse")
    public java.util.List<ru.tandemservice.unifefu.ws.blackboard.context.gen.CourseIdVO> getMemberships(
        @WebParam(name = "userid", targetNamespace = "http://context.ws.blackboard")
        java.lang.String userid
    );

    @WebResult(name = "return", targetNamespace = "http://context.ws.blackboard")
    @Action(input = "loginTool", output = "//context.ws.blackboard/Context/loginToolResponse")
    @RequestWrapper(localName = "loginTool", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.LoginTool")
    @WebMethod(action = "loginTool")
    @ResponseWrapper(localName = "loginToolResponse", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.LoginToolResponse")
    public java.lang.Boolean loginTool(
        @WebParam(name = "password", targetNamespace = "http://context.ws.blackboard")
        java.lang.String password,
        @WebParam(name = "clientVendorId", targetNamespace = "http://context.ws.blackboard")
        java.lang.String clientVendorId,
        @WebParam(name = "clientProgramId", targetNamespace = "http://context.ws.blackboard")
        java.lang.String clientProgramId,
        @WebParam(name = "loginExtraInfo", targetNamespace = "http://context.ws.blackboard")
        java.lang.String loginExtraInfo,
        @WebParam(name = "expectedLifeSeconds", targetNamespace = "http://context.ws.blackboard")
        java.lang.Long expectedLifeSeconds
    );

    @WebResult(name = "return", targetNamespace = "http://context.ws.blackboard")
    @Action(input = "registerTool", output = "//context.ws.blackboard/Context/registerToolResponse")
    @RequestWrapper(localName = "registerTool", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.RegisterTool")
    @WebMethod(action = "registerTool")
    @ResponseWrapper(localName = "registerToolResponse", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.RegisterToolResponse")
    public ru.tandemservice.unifefu.ws.blackboard.context.gen.RegisterToolResultVO registerTool(
        @WebParam(name = "clientVendorId", targetNamespace = "http://context.ws.blackboard")
        java.lang.String clientVendorId,
        @WebParam(name = "clientProgramId", targetNamespace = "http://context.ws.blackboard")
        java.lang.String clientProgramId,
        @WebParam(name = "registrationPassword", targetNamespace = "http://context.ws.blackboard")
        java.lang.String registrationPassword,
        @WebParam(name = "description", targetNamespace = "http://context.ws.blackboard")
        java.lang.String description,
        @WebParam(name = "initialSharedSecret", targetNamespace = "http://context.ws.blackboard")
        java.lang.String initialSharedSecret,
        @WebParam(name = "requiredToolMethods", targetNamespace = "http://context.ws.blackboard")
        java.util.List<java.lang.String> requiredToolMethods,
        @WebParam(name = "requiredTicketMethods", targetNamespace = "http://context.ws.blackboard")
        java.util.List<java.lang.String> requiredTicketMethods
    );

    @WebResult(name = "return", targetNamespace = "http://context.ws.blackboard")
    @Action(input = "loginTicket", output = "//context.ws.blackboard/Context/loginTicketResponse")
    @RequestWrapper(localName = "loginTicket", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.LoginTicket")
    @WebMethod(action = "loginTicket")
    @ResponseWrapper(localName = "loginTicketResponse", targetNamespace = "http://context.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.context.gen.LoginTicketResponse")
    public java.lang.Boolean loginTicket(
        @WebParam(name = "ticket", targetNamespace = "http://context.ws.blackboard")
        java.lang.String ticket,
        @WebParam(name = "clientVendorId", targetNamespace = "http://context.ws.blackboard")
        java.lang.String clientVendorId,
        @WebParam(name = "clientProgramId", targetNamespace = "http://context.ws.blackboard")
        java.lang.String clientProgramId,
        @WebParam(name = "loginExtraInfo", targetNamespace = "http://context.ws.blackboard")
        java.lang.String loginExtraInfo,
        @WebParam(name = "expectedLifeSeconds", targetNamespace = "http://context.ws.blackboard")
        java.lang.Long expectedLifeSeconds
    );
}
