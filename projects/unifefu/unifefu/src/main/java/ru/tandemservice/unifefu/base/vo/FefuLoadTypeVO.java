/* $Id$ */
package ru.tandemservice.unifefu.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;
import ru.tandemservice.unifefu.entity.catalog.FefuLoadType;

/**
 * @author nvankov
 * @since 2/4/14
 */
public class FefuLoadTypeVO extends DataWrapper
{
    private FefuLoadType _fefuLoadType;
    private EppALoadType _eppLoadType;

    public FefuLoadTypeVO(FefuLoadType fefuLoadType)
    {
        super(fefuLoadType.getId(), fefuLoadType.getTitle());
        _fefuLoadType = fefuLoadType;
    }

    public FefuLoadTypeVO(EppALoadType eppLoadType)
    {
        super(eppLoadType.getId(), eppLoadType.getTitle());
        _eppLoadType = eppLoadType;
    }

    public FefuLoadType getFefuLoadType()
    {
        return _fefuLoadType;
    }

    public EppALoadType getEppLoadType()
    {
        return _eppLoadType;
    }

    public boolean isEditDisabled()
    {
        return null != _eppLoadType;
    }

    public boolean isFefuLoadType()
    {
        return null != _fefuLoadType;
    }

    public boolean isEppLoadType()
    {
        return !isFefuLoadType();
    }

    public String getCode()
    {
        return isFefuLoadType() ? _fefuLoadType.getCode() : _eppLoadType.getFullCode();
    }
}
