package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.logic;

public class Constants
{
    public static final String APE_PROGRAM_ID = "apeProgramId";

    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_ORG_UNIT_DS = "territorialOrgUnitDS";
    public static final String EDUCATION_ORG_UNIT_DS = "educationOrgUnitDS";
    public static final String PRODUCING_ORG_UNIT_DS = "producingOrgUnitDS";
    public static final String DIPLOMA_QUALIFICATION_DS = "diplomaQualificationDS";
    public static final String DIPLOMA_TYPE_DS = "diplomaTypeDS";
    public static final String PROGRAM_STATUS_DS = "programStatusDS";
    public static final String PROGRAM_PUBLICATION_STATUS_DS = "programPublicationStatusDS";
    public static final String EDUCATION_LEVEL_DS = "educationLevelDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String DEVELOP_CONDITION_DS = "developConditionDS";
    public static final String DEVELOP_TECH_DS = "developTechDS";
    public static final String DEVELOP_PERIOD_DS = "developPeriodDS";
    public static final String ISSUED_DOCUMENT_DS = "issuedDocumentDS";
    public static final String APE_PROGRAM_DS = "apeProgramDS";

    public static final String FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT = "territorialOrgUnit";

    public static final String TITLE = "title";

    public static final String TITLE_C = "title";
    public static final String FORMATIVE_ORG_UNIT_C = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT_C = "territorialOrgUnit";
    public static final String PROGRAM_OPENING_DATE = "programOpeningDate";
    public static final String PROGRAM_CLOSING_DATE = "programClosingDate";
    public static final String PRODUCING_ORG_UNIT_C = "producingOrgUnit";
    public static final String PROGRAM_STATUS_C = "programStatus";
    public static final String PROGRAM_PUBLICATION_STATUS_C = "programPublicationStatus";
    public static final String EDUCATION_LEVEL_C = "educationLevel";

    public static final String EDUCATION_ORG_UNIT_F = "educationOrgUnit";
    public static final String TITLE_F = "title";
    public static final String FORMATIVE_ORG_UNIT_F = "formativeOrgUnit";
    public static final String TERRITORIAL_ORG_UNIT_F = "territorialOrgUnit";
    public static final String PROGRAM_OPENING_DATE_FROM_F = "programOpeningDateFrom";
    public static final String PROGRAM_OPENING_DATE_TO_F = "programOpeningDateTo";
    public static final String PROGRAM_CLOSING_DATE_FROM_F = "programClosingDateFrom";
    public static final String PROGRAM_CLOSING_DATE_TO_F = "programClosingDateTo";
    public static final String PRODUCING_ORG_UNIT_F = "producingOrgUnit";
    public static final String PROGRAM_STATUS_F = "programStatus";
    public static final String PROGRAM_PUBLICATION_STATUS_F = "programPublicationStatus";
    public static final String EDUCATION_LEVEL_F = "educationLevel";

    public static final String ALIAS = "alias";
}
