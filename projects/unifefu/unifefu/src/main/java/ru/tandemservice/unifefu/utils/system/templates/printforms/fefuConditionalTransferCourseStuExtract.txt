\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx5000
\pard\intbl
Об условном переводе с курса\par на следующий курс\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_A}\cell\b0
{studentCategory_A} {courseOld} курса{groupInternalOld_G}, {custom_learned_A}{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStrOld} по {fefuEducationStrOld_D} {orgUnitPrep} {formativeOrgUnitStrWithTerritorialOld_P} по {developFormOld_DF} форме обучения, условно перевести на {courseNew} курс{intoGroupNew} и установить срок ликвидации академической задолженности до {fefuEliminateDate}.\par
\fi0\cell\row\pard

\keepn\nowidctlpar\qj
Основание: {listBasics}.