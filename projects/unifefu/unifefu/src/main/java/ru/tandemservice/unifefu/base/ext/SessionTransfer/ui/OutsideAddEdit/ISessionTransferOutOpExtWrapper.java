/* $Id $ */
package ru.tandemservice.unifefu.base.ext.SessionTransfer.ui.OutsideAddEdit;

/**
 * @author Rostuncev Savva
 * @since 27.03.2014
 */

public interface ISessionTransferOutOpExtWrapper
{
    Integer getWorkTimeDisc();
}

