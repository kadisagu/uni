/**
 * ServiceSoap_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.nsi;

public interface ServiceSoap_Service extends javax.xml.rpc.Service {
    public java.lang.String getServiceSoap12Address();

    public ru.tandemservice.unifefu.ws.nsi.ServiceSoap_PortType getServiceSoap12() throws javax.xml.rpc.ServiceException;

    public ru.tandemservice.unifefu.ws.nsi.ServiceSoap_PortType getServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
