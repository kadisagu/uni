package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x8_10to11 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuAdditionalProfessionalEducationProgramForStudent

		// создана новая сущность
        if (!tool.tableExists("fefu_ape_program_for_student_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_ape_program_for_student_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("program_id", DBType.LONG), 
				new DBColumn("educationstartdate_p", DBType.DATE), 
				new DBColumn("educationenddate_p", DBType.DATE), 
				new DBColumn("reason_p", DBType.createVarchar(255)), 
				new DBColumn("individualdevelopform_id", DBType.LONG).setNullable(false), 
				new DBColumn("individualdevelopcondition_id", DBType.LONG).setNullable(false), 
				new DBColumn("individualdeveloptech_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuAdditionalProfessionalEducationProgramForStudent");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuIndividualDevelopConditionApe

		// создана новая сущность
        if (!tool.tableExists("fefu_c_ind_develop_cond_ape_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_c_ind_develop_cond_ape_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuIndividualDevelopConditionApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuIndividualDevelopFormApe

		// создана новая сущность
        if (!tool.tableExists("fefu_c_ind_develop_form_ape_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_c_ind_develop_form_ape_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuIndividualDevelopFormApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuIndividualDevelopTechApe

		// создана новая сущность
        if (!tool.tableExists("fefu_c_ind_develop_tech_ape_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_c_ind_develop_tech_ape_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuIndividualDevelopTechApe");

		}
    }
}