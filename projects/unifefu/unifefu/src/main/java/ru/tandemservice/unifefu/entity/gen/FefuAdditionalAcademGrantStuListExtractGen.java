package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;
import ru.tandemservice.unifefu.entity.catalog.FefuPromotedActivityType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О назначении дополнительной повышенной академической стипендии»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuAdditionalAcademGrantStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract";
    public static final String ENTITY_NAME = "fefuAdditionalAcademGrantStuListExtract";
    public static final int VERSION_HASH = -241892717;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_PROMOTED_ACTIVITY_TYPE = "promotedActivityType";
    public static final String P_DATE_START = "dateStart";
    public static final String P_DATE_END = "dateEnd";
    public static final String P_ADDITIONAL_ACADEM_GRANT_SIZE = "additionalAcademGrantSize";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";
    public static final String L_RESPONSIBLE_FOR_PAYMENTS = "responsibleForPayments";
    public static final String L_RESPONSIBLE_FOR_AGREEMENT = "responsibleForAgreement";
    public static final String P_MATCHING_DATE = "matchingDate";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private FefuPromotedActivityType _promotedActivityType;     // Вид поощряемой деятельности
    private Date _dateStart;     // Дата начала выплат
    private Date _dateEnd;     // Дата окончания выплат
    private double _additionalAcademGrantSize;     // Размер дополнительной стипендии
    private String _protocolNumber;     // Номер протокола
    private EmployeePost _responsibleForPayments;     // Ответственный за начисление выплат
    private EmployeePost _responsibleForAgreement;     // Ответственный за согласование начислений выплат
    private Date _matchingDate;     // Дата согласования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Вид поощряемой деятельности.
     */
    public FefuPromotedActivityType getPromotedActivityType()
    {
        return _promotedActivityType;
    }

    /**
     * @param promotedActivityType Вид поощряемой деятельности.
     */
    public void setPromotedActivityType(FefuPromotedActivityType promotedActivityType)
    {
        dirty(_promotedActivityType, promotedActivityType);
        _promotedActivityType = promotedActivityType;
    }

    /**
     * @return Дата начала выплат. Свойство не может быть null.
     */
    @NotNull
    public Date getDateStart()
    {
        return _dateStart;
    }

    /**
     * @param dateStart Дата начала выплат. Свойство не может быть null.
     */
    public void setDateStart(Date dateStart)
    {
        dirty(_dateStart, dateStart);
        _dateStart = dateStart;
    }

    /**
     * @return Дата окончания выплат. Свойство не может быть null.
     */
    @NotNull
    public Date getDateEnd()
    {
        return _dateEnd;
    }

    /**
     * @param dateEnd Дата окончания выплат. Свойство не может быть null.
     */
    public void setDateEnd(Date dateEnd)
    {
        dirty(_dateEnd, dateEnd);
        _dateEnd = dateEnd;
    }

    /**
     * @return Размер дополнительной стипендии. Свойство не может быть null.
     */
    @NotNull
    public double getAdditionalAcademGrantSize()
    {
        return _additionalAcademGrantSize;
    }

    /**
     * @param additionalAcademGrantSize Размер дополнительной стипендии. Свойство не может быть null.
     */
    public void setAdditionalAcademGrantSize(double additionalAcademGrantSize)
    {
        dirty(_additionalAcademGrantSize, additionalAcademGrantSize);
        _additionalAcademGrantSize = additionalAcademGrantSize;
    }

    /**
     * @return Номер протокола.
     */
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    /**
     * @return Ответственный за начисление выплат.
     */
    public EmployeePost getResponsibleForPayments()
    {
        return _responsibleForPayments;
    }

    /**
     * @param responsibleForPayments Ответственный за начисление выплат.
     */
    public void setResponsibleForPayments(EmployeePost responsibleForPayments)
    {
        dirty(_responsibleForPayments, responsibleForPayments);
        _responsibleForPayments = responsibleForPayments;
    }

    /**
     * @return Ответственный за согласование начислений выплат.
     */
    public EmployeePost getResponsibleForAgreement()
    {
        return _responsibleForAgreement;
    }

    /**
     * @param responsibleForAgreement Ответственный за согласование начислений выплат.
     */
    public void setResponsibleForAgreement(EmployeePost responsibleForAgreement)
    {
        dirty(_responsibleForAgreement, responsibleForAgreement);
        _responsibleForAgreement = responsibleForAgreement;
    }

    /**
     * @return Дата согласования.
     */
    public Date getMatchingDate()
    {
        return _matchingDate;
    }

    /**
     * @param matchingDate Дата согласования.
     */
    public void setMatchingDate(Date matchingDate)
    {
        dirty(_matchingDate, matchingDate);
        _matchingDate = matchingDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuAdditionalAcademGrantStuListExtractGen)
        {
            setCourse(((FefuAdditionalAcademGrantStuListExtract)another).getCourse());
            setGroup(((FefuAdditionalAcademGrantStuListExtract)another).getGroup());
            setCompensationType(((FefuAdditionalAcademGrantStuListExtract)another).getCompensationType());
            setPromotedActivityType(((FefuAdditionalAcademGrantStuListExtract)another).getPromotedActivityType());
            setDateStart(((FefuAdditionalAcademGrantStuListExtract)another).getDateStart());
            setDateEnd(((FefuAdditionalAcademGrantStuListExtract)another).getDateEnd());
            setAdditionalAcademGrantSize(((FefuAdditionalAcademGrantStuListExtract)another).getAdditionalAcademGrantSize());
            setProtocolNumber(((FefuAdditionalAcademGrantStuListExtract)another).getProtocolNumber());
            setResponsibleForPayments(((FefuAdditionalAcademGrantStuListExtract)another).getResponsibleForPayments());
            setResponsibleForAgreement(((FefuAdditionalAcademGrantStuListExtract)another).getResponsibleForAgreement());
            setMatchingDate(((FefuAdditionalAcademGrantStuListExtract)another).getMatchingDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuAdditionalAcademGrantStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuAdditionalAcademGrantStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuAdditionalAcademGrantStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "promotedActivityType":
                    return obj.getPromotedActivityType();
                case "dateStart":
                    return obj.getDateStart();
                case "dateEnd":
                    return obj.getDateEnd();
                case "additionalAcademGrantSize":
                    return obj.getAdditionalAcademGrantSize();
                case "protocolNumber":
                    return obj.getProtocolNumber();
                case "responsibleForPayments":
                    return obj.getResponsibleForPayments();
                case "responsibleForAgreement":
                    return obj.getResponsibleForAgreement();
                case "matchingDate":
                    return obj.getMatchingDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "promotedActivityType":
                    obj.setPromotedActivityType((FefuPromotedActivityType) value);
                    return;
                case "dateStart":
                    obj.setDateStart((Date) value);
                    return;
                case "dateEnd":
                    obj.setDateEnd((Date) value);
                    return;
                case "additionalAcademGrantSize":
                    obj.setAdditionalAcademGrantSize((Double) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
                case "responsibleForPayments":
                    obj.setResponsibleForPayments((EmployeePost) value);
                    return;
                case "responsibleForAgreement":
                    obj.setResponsibleForAgreement((EmployeePost) value);
                    return;
                case "matchingDate":
                    obj.setMatchingDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "promotedActivityType":
                        return true;
                case "dateStart":
                        return true;
                case "dateEnd":
                        return true;
                case "additionalAcademGrantSize":
                        return true;
                case "protocolNumber":
                        return true;
                case "responsibleForPayments":
                        return true;
                case "responsibleForAgreement":
                        return true;
                case "matchingDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "promotedActivityType":
                    return true;
                case "dateStart":
                    return true;
                case "dateEnd":
                    return true;
                case "additionalAcademGrantSize":
                    return true;
                case "protocolNumber":
                    return true;
                case "responsibleForPayments":
                    return true;
                case "responsibleForAgreement":
                    return true;
                case "matchingDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "promotedActivityType":
                    return FefuPromotedActivityType.class;
                case "dateStart":
                    return Date.class;
                case "dateEnd":
                    return Date.class;
                case "additionalAcademGrantSize":
                    return Double.class;
                case "protocolNumber":
                    return String.class;
                case "responsibleForPayments":
                    return EmployeePost.class;
                case "responsibleForAgreement":
                    return EmployeePost.class;
                case "matchingDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuAdditionalAcademGrantStuListExtract> _dslPath = new Path<FefuAdditionalAcademGrantStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuAdditionalAcademGrantStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Вид поощряемой деятельности.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getPromotedActivityType()
     */
    public static FefuPromotedActivityType.Path<FefuPromotedActivityType> promotedActivityType()
    {
        return _dslPath.promotedActivityType();
    }

    /**
     * @return Дата начала выплат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getDateStart()
     */
    public static PropertyPath<Date> dateStart()
    {
        return _dslPath.dateStart();
    }

    /**
     * @return Дата окончания выплат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getDateEnd()
     */
    public static PropertyPath<Date> dateEnd()
    {
        return _dslPath.dateEnd();
    }

    /**
     * @return Размер дополнительной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getAdditionalAcademGrantSize()
     */
    public static PropertyPath<Double> additionalAcademGrantSize()
    {
        return _dslPath.additionalAcademGrantSize();
    }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    /**
     * @return Ответственный за начисление выплат.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getResponsibleForPayments()
     */
    public static EmployeePost.Path<EmployeePost> responsibleForPayments()
    {
        return _dslPath.responsibleForPayments();
    }

    /**
     * @return Ответственный за согласование начислений выплат.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getResponsibleForAgreement()
     */
    public static EmployeePost.Path<EmployeePost> responsibleForAgreement()
    {
        return _dslPath.responsibleForAgreement();
    }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getMatchingDate()
     */
    public static PropertyPath<Date> matchingDate()
    {
        return _dslPath.matchingDate();
    }

    public static class Path<E extends FefuAdditionalAcademGrantStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private FefuPromotedActivityType.Path<FefuPromotedActivityType> _promotedActivityType;
        private PropertyPath<Date> _dateStart;
        private PropertyPath<Date> _dateEnd;
        private PropertyPath<Double> _additionalAcademGrantSize;
        private PropertyPath<String> _protocolNumber;
        private EmployeePost.Path<EmployeePost> _responsibleForPayments;
        private EmployeePost.Path<EmployeePost> _responsibleForAgreement;
        private PropertyPath<Date> _matchingDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Вид поощряемой деятельности.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getPromotedActivityType()
     */
        public FefuPromotedActivityType.Path<FefuPromotedActivityType> promotedActivityType()
        {
            if(_promotedActivityType == null )
                _promotedActivityType = new FefuPromotedActivityType.Path<FefuPromotedActivityType>(L_PROMOTED_ACTIVITY_TYPE, this);
            return _promotedActivityType;
        }

    /**
     * @return Дата начала выплат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getDateStart()
     */
        public PropertyPath<Date> dateStart()
        {
            if(_dateStart == null )
                _dateStart = new PropertyPath<Date>(FefuAdditionalAcademGrantStuListExtractGen.P_DATE_START, this);
            return _dateStart;
        }

    /**
     * @return Дата окончания выплат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getDateEnd()
     */
        public PropertyPath<Date> dateEnd()
        {
            if(_dateEnd == null )
                _dateEnd = new PropertyPath<Date>(FefuAdditionalAcademGrantStuListExtractGen.P_DATE_END, this);
            return _dateEnd;
        }

    /**
     * @return Размер дополнительной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getAdditionalAcademGrantSize()
     */
        public PropertyPath<Double> additionalAcademGrantSize()
        {
            if(_additionalAcademGrantSize == null )
                _additionalAcademGrantSize = new PropertyPath<Double>(FefuAdditionalAcademGrantStuListExtractGen.P_ADDITIONAL_ACADEM_GRANT_SIZE, this);
            return _additionalAcademGrantSize;
        }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(FefuAdditionalAcademGrantStuListExtractGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

    /**
     * @return Ответственный за начисление выплат.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getResponsibleForPayments()
     */
        public EmployeePost.Path<EmployeePost> responsibleForPayments()
        {
            if(_responsibleForPayments == null )
                _responsibleForPayments = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_FOR_PAYMENTS, this);
            return _responsibleForPayments;
        }

    /**
     * @return Ответственный за согласование начислений выплат.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getResponsibleForAgreement()
     */
        public EmployeePost.Path<EmployeePost> responsibleForAgreement()
        {
            if(_responsibleForAgreement == null )
                _responsibleForAgreement = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_FOR_AGREEMENT, this);
            return _responsibleForAgreement;
        }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract#getMatchingDate()
     */
        public PropertyPath<Date> matchingDate()
        {
            if(_matchingDate == null )
                _matchingDate = new PropertyPath<Date>(FefuAdditionalAcademGrantStuListExtractGen.P_MATCHING_DATE, this);
            return _matchingDate;
        }

        public Class getEntityClass()
        {
            return FefuAdditionalAcademGrantStuListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuAdditionalAcademGrantStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
