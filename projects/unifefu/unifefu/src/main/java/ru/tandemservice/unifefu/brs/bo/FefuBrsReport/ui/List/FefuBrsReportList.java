/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.logic.FefuBrsReportDSHandler;

/**
 * @author nvankov
 * @since 12/3/13
 */
@Configuration
public class FefuBrsReportList extends BusinessComponentManager
{
    public static String REPORT_DS = "reportDS";

    @Bean
    public ColumnListExtPoint reportDS()
    {
        return columnListExtPointBuilder(REPORT_DS)
                .addColumn(actionColumn("title", "title", "onClickView"))
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPORT_DS, reportDS(), reportDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reportDSHandler()
    {
        return new FefuBrsReportDSHandler(getName());
    }
}
