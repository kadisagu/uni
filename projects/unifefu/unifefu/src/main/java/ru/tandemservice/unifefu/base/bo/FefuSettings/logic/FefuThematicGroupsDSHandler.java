/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.catalog.FefuThematicGroupStuExtractTypes;

/**
 * @author nvankov
 * @since 11/5/13
 */
public class FefuThematicGroupsDSHandler extends DefaultSearchDataSourceHandler
{

    public FefuThematicGroupsDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuThematicGroupStuExtractTypes.class, "t");
        builder.order(DQLExpressions.property("t", FefuThematicGroupStuExtractTypes.title()));
        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }
}
