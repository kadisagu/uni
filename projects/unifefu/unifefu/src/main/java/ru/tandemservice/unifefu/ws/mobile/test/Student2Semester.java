/**
 * Student2Semester.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.mobile.test;

public class Student2Semester  implements java.io.Serializable {
    private java.lang.String id;

    private java.lang.String student_id;

    private java.lang.String semester_id;

    private java.lang.String edu_year_id;

    private java.lang.String sem_number;

    public Student2Semester() {
    }

    public Student2Semester(
           java.lang.String id,
           java.lang.String student_id,
           java.lang.String semester_id,
           java.lang.String edu_year_id,
           java.lang.String sem_number) {
           this.id = id;
           this.student_id = student_id;
           this.semester_id = semester_id;
           this.edu_year_id = edu_year_id;
           this.sem_number = sem_number;
    }


    /**
     * Gets the id value for this Student2Semester.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this Student2Semester.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the student_id value for this Student2Semester.
     * 
     * @return student_id
     */
    public java.lang.String getStudent_id() {
        return student_id;
    }


    /**
     * Sets the student_id value for this Student2Semester.
     * 
     * @param student_id
     */
    public void setStudent_id(java.lang.String student_id) {
        this.student_id = student_id;
    }


    /**
     * Gets the semester_id value for this Student2Semester.
     * 
     * @return semester_id
     */
    public java.lang.String getSemester_id() {
        return semester_id;
    }


    /**
     * Sets the semester_id value for this Student2Semester.
     * 
     * @param semester_id
     */
    public void setSemester_id(java.lang.String semester_id) {
        this.semester_id = semester_id;
    }


    /**
     * Gets the edu_year_id value for this Student2Semester.
     * 
     * @return edu_year_id
     */
    public java.lang.String getEdu_year_id() {
        return edu_year_id;
    }


    /**
     * Sets the edu_year_id value for this Student2Semester.
     * 
     * @param edu_year_id
     */
    public void setEdu_year_id(java.lang.String edu_year_id) {
        this.edu_year_id = edu_year_id;
    }


    /**
     * Gets the sem_number value for this Student2Semester.
     * 
     * @return sem_number
     */
    public java.lang.String getSem_number() {
        return sem_number;
    }


    /**
     * Sets the sem_number value for this Student2Semester.
     * 
     * @param sem_number
     */
    public void setSem_number(java.lang.String sem_number) {
        this.sem_number = sem_number;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Student2Semester)) return false;
        Student2Semester other = (Student2Semester) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.student_id==null && other.getStudent_id()==null) || 
             (this.student_id!=null &&
              this.student_id.equals(other.getStudent_id()))) &&
            ((this.semester_id==null && other.getSemester_id()==null) || 
             (this.semester_id!=null &&
              this.semester_id.equals(other.getSemester_id()))) &&
            ((this.edu_year_id==null && other.getEdu_year_id()==null) || 
             (this.edu_year_id!=null &&
              this.edu_year_id.equals(other.getEdu_year_id()))) &&
            ((this.sem_number==null && other.getSem_number()==null) || 
             (this.sem_number!=null &&
              this.sem_number.equals(other.getSem_number())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getStudent_id() != null) {
            _hashCode += getStudent_id().hashCode();
        }
        if (getSemester_id() != null) {
            _hashCode += getSemester_id().hashCode();
        }
        if (getEdu_year_id() != null) {
            _hashCode += getEdu_year_id().hashCode();
        }
        if (getSem_number() != null) {
            _hashCode += getSem_number().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Student2Semester.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "Student2Semester"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("student_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "student_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("semester_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "semester_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("edu_year_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "edu_year_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sem_number");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "sem_number"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
