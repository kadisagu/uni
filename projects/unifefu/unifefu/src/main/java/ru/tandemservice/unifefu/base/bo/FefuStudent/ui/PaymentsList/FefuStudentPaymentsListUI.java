/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.ui.PaymentsList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author nvankov
 * @since 8/27/13
 */
@Input({
        @Bind(key = "studentId", binding = "studentId")
})
public class FefuStudentPaymentsListUI extends UIPresenter
{
    private Long _studentId;
    private Student _student;

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    @Override
    public void onComponentRefresh()
    {
        _student = DataAccessServices.dao().get(_studentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuStudentPaymentsList.STUDENT_PAYMENTS_DS.equals(dataSource.getName()))
        {
            dataSource.put("studentId", _studentId);
            dataSource.putAll(_uiSettings.getAsMap(true, "paymentsDateFrom", "paymentsDateTo", "orderNum", "extractTypes"));
        }
        if(FefuStudentPaymentsList.EXTRACT_TYPE_DS.equals(dataSource.getName()))
        {
            dataSource.put("studentId", _studentId);
        }
    }
}
