/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTrHomePage.logic;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.gen.EppStateGen;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 13.02.2014
 */
public class FefuTrHomePageDao extends UniBaseDao implements IFefuTrHomePageDao
{
    @Override
    public EppYearPart getLastYearPart()
    {
        return new DQLSelectBuilder().fromEntity(EppYearPart.class, "e")
                .column("e")
                .top(1)
                .order(property("e", EppYearPart.year().educationYear().intValue()), OrderDirection.desc)
                .order(property("e", EppYearPart.part().yearDistribution().amount()), OrderDirection.desc)
                .order(property("e", EppYearPart.part().number()), OrderDirection.desc).createStatement(getSession()).uniqueResult();
    }

    @Override
    public void doSendToCoordinateRegistryElement(TrJournal journal)
    {
        journal.setState(this.<EppState>getByNaturalId(new EppStateGen.NaturalId(EppState.STATE_ACCEPTABLE)));
        save(journal);
    }
}