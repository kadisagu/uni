/**
 *$Id$
 */
package ru.tandemservice.unifefu.dao;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uniepp.dao.index.*;
import ru.tandemservice.unifefu.entity.catalog.codes.EppIndexRuleCodes;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 17.12.2013
 */
public class FefuIndexRuleDAO extends EppIndexRuleDAO implements IEppIndexRuleDAO
{
    @Override
    protected Map<String, IEppIndexRule> getRuleMap()
    {
        Map<String, IEppIndexRule> result = super.getRuleMap();
        result.put(EppIndexRuleCodes.PREFIX_COMPONENT_NUMBER_HIERARCHY, RULE_PREFIX_COMPONENT_HIERARCHY);
        return result;
    }

    public static IEppIndexRule RULE_PREFIX_COMPONENT_HIERARCHY = new IEppIndexRule()
    {
        @Override
        public String getIndex(IEppIndexedRowWrapper eppRowWrapper)
        {
            IEppIndexedRowWrapper hierarchyParent = (IEppIndexedRowWrapper) eppRowWrapper.getHierarhyParent();
            String postfix = hierarchyParent != null && (eppRowWrapper.getRow() instanceof IEppStructureIndexedRow) ? ((IEppStructureIndexedRow) eppRowWrapper.getRow()).getValue().getShortTitle() : String.valueOf(eppRowWrapper.getSequenceNumber());

            if (null == hierarchyParent)
            {
                String qualification = StringUtils.trimToNull(eppRowWrapper.getQualificationCode());
                return ((null != qualification ? qualification : "") + postfix);
            }

            return (hierarchyParent.getIndex() + "." + postfix);
        }
    };
}