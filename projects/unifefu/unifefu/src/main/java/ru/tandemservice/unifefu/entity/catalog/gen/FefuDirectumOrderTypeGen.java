package ru.tandemservice.unifefu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Виды приказов в Directum
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuDirectumOrderTypeGen extends EntityBase
 implements INaturalIdentifiable<FefuDirectumOrderTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType";
    public static final String ENTITY_NAME = "fefuDirectumOrderType";
    public static final int VERSION_HASH = -709287945;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_ROUTE_CODE = "routeCode";
    public static final String P_DOCUMENT_TYPE = "documentType";

    private String _code;     // Системный код
    private String _title;     // Название
    private String _shortTitle;     // Сокращенное название
    private String _routeCode;     // Код маршрута
    private String _documentType;     // Вид документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null и должно быть уникальным.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Код маршрута. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRouteCode()
    {
        return _routeCode;
    }

    /**
     * @param routeCode Код маршрута. Свойство не может быть null.
     */
    public void setRouteCode(String routeCode)
    {
        dirty(_routeCode, routeCode);
        _routeCode = routeCode;
    }

    /**
     * @return Вид документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDocumentType()
    {
        return _documentType;
    }

    /**
     * @param documentType Вид документа. Свойство не может быть null.
     */
    public void setDocumentType(String documentType)
    {
        dirty(_documentType, documentType);
        _documentType = documentType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuDirectumOrderTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((FefuDirectumOrderType)another).getCode());
            }
            setTitle(((FefuDirectumOrderType)another).getTitle());
            setShortTitle(((FefuDirectumOrderType)another).getShortTitle());
            setRouteCode(((FefuDirectumOrderType)another).getRouteCode());
            setDocumentType(((FefuDirectumOrderType)another).getDocumentType());
        }
    }

    public INaturalId<FefuDirectumOrderTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<FefuDirectumOrderTypeGen>
    {
        private static final String PROXY_NAME = "FefuDirectumOrderTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuDirectumOrderTypeGen.NaturalId) ) return false;

            FefuDirectumOrderTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuDirectumOrderTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuDirectumOrderType.class;
        }

        public T newInstance()
        {
            return (T) new FefuDirectumOrderType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "routeCode":
                    return obj.getRouteCode();
                case "documentType":
                    return obj.getDocumentType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "routeCode":
                    obj.setRouteCode((String) value);
                    return;
                case "documentType":
                    obj.setDocumentType((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "shortTitle":
                        return true;
                case "routeCode":
                        return true;
                case "documentType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "shortTitle":
                    return true;
                case "routeCode":
                    return true;
                case "documentType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "routeCode":
                    return String.class;
                case "documentType":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuDirectumOrderType> _dslPath = new Path<FefuDirectumOrderType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuDirectumOrderType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Код маршрута. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType#getRouteCode()
     */
    public static PropertyPath<String> routeCode()
    {
        return _dslPath.routeCode();
    }

    /**
     * @return Вид документа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType#getDocumentType()
     */
    public static PropertyPath<String> documentType()
    {
        return _dslPath.documentType();
    }

    public static class Path<E extends FefuDirectumOrderType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _routeCode;
        private PropertyPath<String> _documentType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(FefuDirectumOrderTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FefuDirectumOrderTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(FefuDirectumOrderTypeGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Код маршрута. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType#getRouteCode()
     */
        public PropertyPath<String> routeCode()
        {
            if(_routeCode == null )
                _routeCode = new PropertyPath<String>(FefuDirectumOrderTypeGen.P_ROUTE_CODE, this);
            return _routeCode;
        }

    /**
     * @return Вид документа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType#getDocumentType()
     */
        public PropertyPath<String> documentType()
        {
            if(_documentType == null )
                _documentType = new PropertyPath<String>(FefuDirectumOrderTypeGen.P_DOCUMENT_TYPE, this);
            return _documentType;
        }

        public Class getEntityClass()
        {
            return FefuDirectumOrderType.class;
        }

        public String getEntityName()
        {
            return "fefuDirectumOrderType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
