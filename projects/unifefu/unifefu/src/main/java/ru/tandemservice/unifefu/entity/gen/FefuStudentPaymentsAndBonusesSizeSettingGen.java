package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.FefuStudentPaymentsAndBonusesSizeSetting;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Размеры выплат и надбавок студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuStudentPaymentsAndBonusesSizeSettingGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuStudentPaymentsAndBonusesSizeSetting";
    public static final String ENTITY_NAME = "fefuStudentPaymentsAndBonusesSizeSetting";
    public static final int VERSION_HASH = 1200995248;
    private static IEntityMeta ENTITY_META;

    public static final String P_DEFAULT_GRANT_SIZE = "defaultGrantSize";
    public static final String P_DEFAULT_GROUP_MANAGER_BONUS_SIZE = "defaultGroupManagerBonusSize";
    public static final String P_DEFAULT_SOCIAL_GRANT_SIZE = "defaultSocialGrantSize";

    private int _defaultGrantSize;     // Размер академической стипендии
    private int _defaultGroupManagerBonusSize;     // Размер надбавки старостам
    private int _defaultSocialGrantSize;     // Размер социальной стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Размер академической стипендии. Свойство не может быть null.
     */
    @NotNull
    public int getDefaultGrantSize()
    {
        return _defaultGrantSize;
    }

    /**
     * @param defaultGrantSize Размер академической стипендии. Свойство не может быть null.
     */
    public void setDefaultGrantSize(int defaultGrantSize)
    {
        dirty(_defaultGrantSize, defaultGrantSize);
        _defaultGrantSize = defaultGrantSize;
    }

    /**
     * @return Размер надбавки старостам. Свойство не может быть null.
     */
    @NotNull
    public int getDefaultGroupManagerBonusSize()
    {
        return _defaultGroupManagerBonusSize;
    }

    /**
     * @param defaultGroupManagerBonusSize Размер надбавки старостам. Свойство не может быть null.
     */
    public void setDefaultGroupManagerBonusSize(int defaultGroupManagerBonusSize)
    {
        dirty(_defaultGroupManagerBonusSize, defaultGroupManagerBonusSize);
        _defaultGroupManagerBonusSize = defaultGroupManagerBonusSize;
    }

    /**
     * @return Размер социальной стипендии. Свойство не может быть null.
     */
    @NotNull
    public int getDefaultSocialGrantSize()
    {
        return _defaultSocialGrantSize;
    }

    /**
     * @param defaultSocialGrantSize Размер социальной стипендии. Свойство не может быть null.
     */
    public void setDefaultSocialGrantSize(int defaultSocialGrantSize)
    {
        dirty(_defaultSocialGrantSize, defaultSocialGrantSize);
        _defaultSocialGrantSize = defaultSocialGrantSize;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuStudentPaymentsAndBonusesSizeSettingGen)
        {
            setDefaultGrantSize(((FefuStudentPaymentsAndBonusesSizeSetting)another).getDefaultGrantSize());
            setDefaultGroupManagerBonusSize(((FefuStudentPaymentsAndBonusesSizeSetting)another).getDefaultGroupManagerBonusSize());
            setDefaultSocialGrantSize(((FefuStudentPaymentsAndBonusesSizeSetting)another).getDefaultSocialGrantSize());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuStudentPaymentsAndBonusesSizeSettingGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuStudentPaymentsAndBonusesSizeSetting.class;
        }

        public T newInstance()
        {
            return (T) new FefuStudentPaymentsAndBonusesSizeSetting();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "defaultGrantSize":
                    return obj.getDefaultGrantSize();
                case "defaultGroupManagerBonusSize":
                    return obj.getDefaultGroupManagerBonusSize();
                case "defaultSocialGrantSize":
                    return obj.getDefaultSocialGrantSize();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "defaultGrantSize":
                    obj.setDefaultGrantSize((Integer) value);
                    return;
                case "defaultGroupManagerBonusSize":
                    obj.setDefaultGroupManagerBonusSize((Integer) value);
                    return;
                case "defaultSocialGrantSize":
                    obj.setDefaultSocialGrantSize((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "defaultGrantSize":
                        return true;
                case "defaultGroupManagerBonusSize":
                        return true;
                case "defaultSocialGrantSize":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "defaultGrantSize":
                    return true;
                case "defaultGroupManagerBonusSize":
                    return true;
                case "defaultSocialGrantSize":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "defaultGrantSize":
                    return Integer.class;
                case "defaultGroupManagerBonusSize":
                    return Integer.class;
                case "defaultSocialGrantSize":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuStudentPaymentsAndBonusesSizeSetting> _dslPath = new Path<FefuStudentPaymentsAndBonusesSizeSetting>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuStudentPaymentsAndBonusesSizeSetting");
    }
            

    /**
     * @return Размер академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPaymentsAndBonusesSizeSetting#getDefaultGrantSize()
     */
    public static PropertyPath<Integer> defaultGrantSize()
    {
        return _dslPath.defaultGrantSize();
    }

    /**
     * @return Размер надбавки старостам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPaymentsAndBonusesSizeSetting#getDefaultGroupManagerBonusSize()
     */
    public static PropertyPath<Integer> defaultGroupManagerBonusSize()
    {
        return _dslPath.defaultGroupManagerBonusSize();
    }

    /**
     * @return Размер социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPaymentsAndBonusesSizeSetting#getDefaultSocialGrantSize()
     */
    public static PropertyPath<Integer> defaultSocialGrantSize()
    {
        return _dslPath.defaultSocialGrantSize();
    }

    public static class Path<E extends FefuStudentPaymentsAndBonusesSizeSetting> extends EntityPath<E>
    {
        private PropertyPath<Integer> _defaultGrantSize;
        private PropertyPath<Integer> _defaultGroupManagerBonusSize;
        private PropertyPath<Integer> _defaultSocialGrantSize;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Размер академической стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPaymentsAndBonusesSizeSetting#getDefaultGrantSize()
     */
        public PropertyPath<Integer> defaultGrantSize()
        {
            if(_defaultGrantSize == null )
                _defaultGrantSize = new PropertyPath<Integer>(FefuStudentPaymentsAndBonusesSizeSettingGen.P_DEFAULT_GRANT_SIZE, this);
            return _defaultGrantSize;
        }

    /**
     * @return Размер надбавки старостам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPaymentsAndBonusesSizeSetting#getDefaultGroupManagerBonusSize()
     */
        public PropertyPath<Integer> defaultGroupManagerBonusSize()
        {
            if(_defaultGroupManagerBonusSize == null )
                _defaultGroupManagerBonusSize = new PropertyPath<Integer>(FefuStudentPaymentsAndBonusesSizeSettingGen.P_DEFAULT_GROUP_MANAGER_BONUS_SIZE, this);
            return _defaultGroupManagerBonusSize;
        }

    /**
     * @return Размер социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentPaymentsAndBonusesSizeSetting#getDefaultSocialGrantSize()
     */
        public PropertyPath<Integer> defaultSocialGrantSize()
        {
            if(_defaultSocialGrantSize == null )
                _defaultSocialGrantSize = new PropertyPath<Integer>(FefuStudentPaymentsAndBonusesSizeSettingGen.P_DEFAULT_SOCIAL_GRANT_SIZE, this);
            return _defaultSocialGrantSize;
        }

        public Class getEntityClass()
        {
            return FefuStudentPaymentsAndBonusesSizeSetting.class;
        }

        public String getEntityName()
        {
            return "fefuStudentPaymentsAndBonusesSizeSetting";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
