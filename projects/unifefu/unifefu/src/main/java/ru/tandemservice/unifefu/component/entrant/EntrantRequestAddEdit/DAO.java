/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.EntrantRequestAddEdit;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unifefu.dao.IFefuEntrantDAO;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 14.06.2013
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.DAO
{
    @Override
    public void prepare(final ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;

        // Технические комиссии
        List<FefuTechnicalCommission> commissions = UnifefuDaoFacade.getFefuEntrantDAO().getTechnicalCommissionsList(model.getEnrollmentCampaign());
        m.setTechnicalCommissionsSelectModel(new LazySimpleSelectModel<>(commissions).setSearchFromStart(false));
        m.setTechnicalCommission(UnifefuDaoFacade.getFefuEntrantDAO().getTechnicalCommission(model.getEntrantRequest()));
        m.setOldTechnicalCommission(m.getTechnicalCommission());

        if (!m.isAddForm())
            UnifefuDaoFacade.getFefuEntrantDAO().checkAccessForTCEmployee(model.getEntrantRequest());
    }

    @Override
    public void update(ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model model)
    {
        if (!model.isAddForm())
            UnifefuDaoFacade.getFefuEntrantDAO().checkAccessForTCEmployee(model.getEntrantRequest());

        super.update(model);

        Model m = (Model) model;
        IFefuEntrantDAO entrantDAO = UnifefuDaoFacade.getFefuEntrantDAO();

        entrantDAO.setTechnicalCommission(m.getEntrantRequest(), m.getTechnicalCommission());

        getSession().flush(); // сохраняем заявление и связь с ТК


        // Если у нас единственное заявление и изменилась тех. комиссия, то перегенерируем номер абитуриента
        Entrant entrant = m.getEntrantRequest().getEntrant();
        if (entrantDAO.getEntrantRequestCount(entrant) == 1 &&
                (m.getOldTechnicalCommission() == null || !m.getOldTechnicalCommission().equals(m.getTechnicalCommission())))
        {
            entrant.setPersonalNumber(entrantDAO.getFefuEntrantNumber(m.getEntrantRequest(), getSession()));
            update(entrant);
            m.setOldTechnicalCommission(m.getTechnicalCommission());
        }
    }
}