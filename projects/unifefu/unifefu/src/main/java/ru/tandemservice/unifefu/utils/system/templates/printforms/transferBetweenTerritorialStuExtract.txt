\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
{reason}\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_A}\cell\b0
{studentCategory_A} {courseOld} курса{intoFefuGroupOld}, {custom_learned_A}{fefuShortFastExtendedOptionalTextOld} {fefuCompensationTypeStrOld} по {fefuEducationStrOld_D} в {formativeOrgUnitOld_P}{territorialOrgUnitOld_P} по {developForm_DF} форме обучения, перевести на {courseNew} курс{intoFefuGroupNew} для обучения по {fefuEducationStrNew_D} в {formativeOrgUnitNew_P}{territorialOrgUnitNew_P}{fefuShortFastExtendedOptionalTextNew} по {developForm_DF} форме обучения {fefuCompensationTypeStrNew}.\par
{changeEduPlan}
\fi0\cell\row\pard

\keepn\nowidctlpar\qj
Основание: {listBasics}.