/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.ui.ResultsList;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.process.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentTypeSelectModel;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.FefuDipDocumentReportManager;
import ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.ui.logic.EduLevelSelectModel;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 29.09.2014
 */
public class FefuDipDocumentReportResultsListUI extends UIPresenter
{
    private ISingleSelectModel _eduLevelSelectModel;
    private ISingleSelectModel _dipDocumentTypeSelectModel;
    private EduLevel _eduLevel;
    private boolean _showEduLevel;
    private DipDocumentType _dipDocumentType;
    private boolean _showDipDocType;
    private OrgUnit _formativeOrgUnit;
    private boolean _showFormativeOrgUnit;
    private List<EduProgramSubject> _programSubjectList;
    private boolean _showProgramSubject;
    private Date _issueDateFrom;
    private Date _issueDateTo;
    private boolean _showIssueDateFrom;
    private boolean _showIssueDateTo;
    private boolean _duplicate;

    private byte[] report = null;


    @Override
    public void onComponentPrepareRender()
    {
        if (report != null) {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("DipDocumentReport.xls").document(report), true);
            report = null;
        }
    }

    @Override
    public void onComponentRefresh()
    {
        setEduLevelSelectModel(new EduLevelSelectModel(true));
        setDipDocumentTypeSelectModel(new DipDocumentTypeSelectModel(null, false));
    }

    //GETTERS & SETTERS
    public ISingleSelectModel getEduLevelSelectModel()
    {
        return _eduLevelSelectModel;
    }

    public void setEduLevelSelectModel(ISingleSelectModel eduLevelSelectModel)
    {
        _eduLevelSelectModel = eduLevelSelectModel;
    }

    public EduLevel getEduLevel()
    {
        return _eduLevel;
    }

    public void setEduLevel(EduLevel eduLevel)
    {
        _eduLevel = eduLevel;
    }

    public ISingleSelectModel getDipDocumentTypeSelectModel()
    {
        return _dipDocumentTypeSelectModel;
    }

    public void setDipDocumentTypeSelectModel(ISingleSelectModel dipDocumentTypeSelectModel)
    {
        _dipDocumentTypeSelectModel = dipDocumentTypeSelectModel;
    }

    public DipDocumentType getDipDocumentType()
    {
        return _dipDocumentType;
    }

    public void setDipDocumentType(DipDocumentType dipDocumentType)
    {
        _dipDocumentType = dipDocumentType;
    }

    public boolean isShowEduLevel()
    {
        return _showEduLevel;
    }

    public void setShowEduLevel(boolean showEduLevel)
    {
        _showEduLevel = showEduLevel;
    }

    public boolean isShowDipDocType()
    {
        return _showDipDocType;
    }

    public void setShowDipDocType(boolean showDipDocType)
    {
        _showDipDocType = showDipDocType;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public boolean isShowFormativeOrgUnit()
    {
        return _showFormativeOrgUnit;
    }

    public void setShowFormativeOrgUnit(boolean showFormativeOrgUnit)
    {
        _showFormativeOrgUnit = showFormativeOrgUnit;
    }

    public boolean isShowProgramSubject()
    {
        return _showProgramSubject;
    }

    public void setShowProgramSubject(boolean showProgramSubject)
    {
        _showProgramSubject = showProgramSubject;
    }

    public Date getIssueDateFrom()
    {
        return _issueDateFrom;
    }

    public void setIssueDateFrom(Date issueDateFrom)
    {
        _issueDateFrom = issueDateFrom;
    }

    public Date getIssueDateTo()
    {
        return _issueDateTo;
    }

    public void setIssueDateTo(Date issueDateTo)
    {
        _issueDateTo = issueDateTo;
    }

    public boolean isShowIssueDateFrom()
    {
        return _showIssueDateFrom;
    }

    public void setShowIssueDateFrom(boolean showIssueDateFrom)
    {
        _showIssueDateFrom = showIssueDateFrom;
    }

    public boolean isShowIssueDateTo()
    {
        return _showIssueDateTo;
    }

    public void setShowIssueDateTo(boolean showIssueDateTo)
    {
        _showIssueDateTo = showIssueDateTo;
    }

    public boolean isDuplicate()
    {
        return _duplicate;
    }

    public void setDuplicate(boolean duplicate)
    {
        _duplicate = duplicate;
    }

    public List<EduProgramSubject> getProgramSubjectList()
    {
        return _programSubjectList;
    }

    public void setProgramSubjectList(List<EduProgramSubject> programSubjectList)
    {
        _programSubjectList = programSubjectList;
    }

    public void onClickApply() throws Exception
    {

        final FefuDipDocumentExcelBuilder builder = new FefuDipDocumentExcelBuilder(isShowEduLevel(), isShowDipDocType(), isShowFormativeOrgUnit(),
                                                                                    isShowProgramSubject(),isShowIssueDateFrom(), isShowIssueDateTo(),
                                                                                    getEduLevel(), getDipDocumentType(), getFormativeOrgUnit(), getProgramSubjectList(),
                                                                                    getIssueDateFrom(), getIssueDateTo(), isDuplicate());

        IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                try {
                    ByteArrayOutputStream out = FefuDipDocumentReportManager.instance().dao().buildReport(builder);
                    report = out.toByteArray();
                }
                catch (Throwable t)
                {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
                return null;
            }
        };

        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Построение отчета", process, ProcessDisplayMode.unknown));
    }
}
