/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRequest;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 31.05.2013
 */
public interface IFEFUPasswordRegistratorDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IFEFUPasswordRegistratorDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IFEFUPasswordRegistratorDaemonDao> instance = new SpringBeanCache<>(IFEFUPasswordRegistratorDaemonDao.class.getName());

    /**
     * Обновляет (либо создаёт) данные о пароле в шифрованном виде в таблицу с гуидами персон для последующей передачи в IDM
     *
     * @param userId           - идентификатор пользователя в системе онлайн-регистрации абитуриентов
     * @param passwordEnrypted - пароль в шифрованном виде, полученный из вэб-сервиса обмена с онлайн-регистрацией
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void updateOnlineEntrantExtPassword(Long userId, String passwordEnrypted);

    /**
     * Создаёт запрос на регистрацию пользователя в НСИ и сразу же пытается его выполнить.
     * Если сервис НСИ недоступен, то демон чуть позже повторит попытку отправки запроса в НСИ.
     *
     * @param nsiId    - идентификатор записи в таблице соответствия идентификатора объекта УНИ идентификатору в НСИ
     * @param person   - персона
     * @param password - пароль в незашифрованном виде
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doRegisterNSIRequest(FefuNsiIds nsiId, Person person, String password);

    /**
     * Пытается отправить накопленные запросы на регистрацию пользователей в НСИ
     * В случае отсутствия соединения с НСИ, строка помечается как неотправленная и будет отправлена повторно чуть позже
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doSendRequestsToNSI();

    /**
     * Обновляет запрос на регистрацию пароля, внося туда данные о выданном пользователю из НСИ логине
     * Так же обновляется логин персоны в УНИ
     *
     * @param guid     - идентификатор физ лица в НСИ
     * @param nsiLogin - возвращенный из НСИ логин для персоны, не должен быть заполнен, если заполнен параметр с кодом ошибки
     * @param errorMsg - возвращенный код ошибки из НСИ в случае неудачной регистрации, не должен быть заполнен, если заполнен параметр с логином
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void updateNSIRequest(String guid, String nsiLogin, String errorMsg);

    /**
     * Повторно посылает запрос к IDM на регистрацию пользователя.
     *
     * @param idmRequest - запрос на регистрацию в IDM
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doResendRequest(FefuNsiRequest idmRequest);

    /**
     * Повторно посылает запросы к IDM на регистрацию пользователя.
     *
     * @param requestIds - список идентификаторов запросов на регистрацию в IDM
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doResendRequests(List<Long> requestIds);
}