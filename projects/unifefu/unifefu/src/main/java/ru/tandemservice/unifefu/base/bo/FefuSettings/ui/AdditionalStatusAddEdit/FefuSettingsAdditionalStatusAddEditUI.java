/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.AdditionalStatusAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.unifefu.entity.FefuCaseCISetting2CustomStateRel;
import ru.tandemservice.unifefu.entity.FefuStudentCustomStateCISetting;
import ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 26.11.2013
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "settingId")})
public class FefuSettingsAdditionalStatusAddEditUI extends UIPresenter
{
    private Long _settingId;

    private GrammaCase _currentGrammaCase;
    private static Collection<GrammaCase> _grammaCases;

    private FefuStudentCustomStateCISetting _setting;
    private List<StudentCustomStateCI> _studentCustomStateList = new ArrayList<>();
    private List<FefuStudentCustomStateDeclination> _declinationList = new ArrayList<>();

    private boolean _editForm;

    @Override
    public void onComponentRefresh()
    {
        _grammaCases = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.keySet();

        if (null != _settingId)
        {
            _editForm = true;
            _setting = DataAccessServices.dao().getNotNull(FefuStudentCustomStateCISetting.class, _settingId);
            _declinationList = DataAccessServices.dao().getList(FefuStudentCustomStateDeclination.class, FefuStudentCustomStateDeclination.setting().id(), _settingId);
            _studentCustomStateList = new DQLSelectBuilder()
                    .fromEntity(FefuCaseCISetting2CustomStateRel.class, "rel")
                    .where(eq(property(FefuCaseCISetting2CustomStateRel.setting().id().fromAlias("rel")), value(_settingId)))
                    .column(property(FefuCaseCISetting2CustomStateRel.customState().fromAlias("rel")))
                    .createStatement(DataAccessServices.dao().getComponentSession()).list();
        }
        else
            _setting = new FefuStudentCustomStateCISetting();
    }

    // Listeners

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_setting);

        if (!_editForm)
        {
            for (StudentCustomStateCI customState : _studentCustomStateList)
            {
                FefuCaseCISetting2CustomStateRel stateRel = new FefuCaseCISetting2CustomStateRel();
                stateRel.setSetting(_setting);
                stateRel.setCustomState(customState);
                DataAccessServices.dao().saveOrUpdate(stateRel);
            }
        }
        if (!_declinationList.isEmpty())
        {
            for (FefuStudentCustomStateDeclination declination : _declinationList)
            {
                if (!_editForm) declination.setSetting(_setting);
                DataAccessServices.dao().saveOrUpdate(declination);
            }
        }
        deactivate();
    }

    // Getters & Setters

    public Long getSettingId()
    {
        return _settingId;
    }

    public void setSettingId(Long settingId)
    {
        _settingId = settingId;
    }

    public FefuStudentCustomStateCISetting getSetting()
    {
        return _setting;
    }

    public void setSetting(FefuStudentCustomStateCISetting setting)
    {
        _setting = setting;
    }

    public InflectorVariant getInflectorVariant()
    {
        return DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(_currentGrammaCase);
    }

    public String getInflectorVariantTitle()
    {
        return getInflectorVariant().getTitle();
    }

    public String getMaleValue()
    {
        return getValue(true, false);
    }

    public String getMalePastValue()
    {
        return getValue(true, true);
    }

    public String getFemaleValue()
    {
        return getValue(false, false);
    }

    public String getFemalePastValue()
    {
        return getValue(false, true);
    }

    private String getValue(boolean male, boolean past)
    {
        FefuStudentCustomStateDeclination declination = getDeclination(male, past, getInflectorVariant());
        return declination == null ? "" : declination.getValue();
    }

    public void setMaleValue(String value)
    {
        setValue(true, false, value);
    }

    public void setMalePastValue(String value)
    {
        setValue(true, true, value);
    }

    public void setFemaleValue(String value)
    {
        setValue(false, false, value);
    }

    public void setFemalePastValue(String value)
    {
        setValue(false, true, value);
    }

    private void setValue(boolean male, boolean past, String value)
    {
        FefuStudentCustomStateDeclination declination = new FefuStudentCustomStateDeclination();
        if (_editForm)
        {
            declination = getDeclination(male, past, getInflectorVariant());
            int index = _declinationList.indexOf(declination);
            declination.setValue(value);
            _declinationList.add(index, declination);
        }
        else
        {
            declination.setMale(male);
            declination.setPast(past);
            declination.setInflectorVariant(getInflectorVariant());
            declination.setValue(value);
            _declinationList.add(declination);
        }
    }

    private FefuStudentCustomStateDeclination getDeclination(boolean male, boolean past, InflectorVariant inflectorVariant)
    {
        for (FefuStudentCustomStateDeclination d : _declinationList)
        {
            if (d.isMale() == male && d.isPast() == past && d.getInflectorVariant().equals(inflectorVariant))
            {
                return d;
            }
        }
        return null;
    }

    public GrammaCase getCurrentGrammaCase()
    {
        return _currentGrammaCase;
    }

    public void setCurrentGrammaCase(GrammaCase currentGrammaCase)
    {
        _currentGrammaCase = currentGrammaCase;
    }

    public Collection<GrammaCase> getGrammaCases()
    {
        return _grammaCases;
    }

    public void setGrammaCases(Collection<GrammaCase> grammaCases)
    {
        _grammaCases = grammaCases;
    }

    public List<StudentCustomStateCI> getStudentCustomStateList()
    {
        return _studentCustomStateList;
    }

    public void setStudentCustomStateList(List<StudentCustomStateCI> studentCustomStateList)
    {
        _studentCustomStateList = studentCustomStateList;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }
}
