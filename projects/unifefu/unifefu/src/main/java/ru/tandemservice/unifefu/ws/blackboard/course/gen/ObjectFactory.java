
package ru.tandemservice.unifefu.ws.blackboard.course.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tandemservice.unifefu.ws.blackboard.course.gen package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UpdateCourseResponseReturn_QNAME = new QName("http://course.ws.blackboard", "return");
    private final static QName _SaveOrgCategoryMembershipMembership_QNAME = new QName("http://course.ws.blackboard", "membership");
    private final static QName _SetCourseBannerImageFileContents_QNAME = new QName("http://course.ws.blackboard", "fileContents");
    private final static QName _SetCourseBannerImageCourseId_QNAME = new QName("http://course.ws.blackboard", "courseId");
    private final static QName _SetCourseBannerImageFileName_QNAME = new QName("http://course.ws.blackboard", "fileName");
    private final static QName _GetCourseFilter_QNAME = new QName("http://course.ws.blackboard", "filter");
    private final static QName _SaveOrgCategoryAdminCategory_QNAME = new QName("http://course.ws.blackboard", "adminCategory");
    private final static QName _TermVODescriptionType_QNAME = new QName("http://course.ws.blackboard/xsd", "descriptionType");
    private final static QName _TermVOSourcedidSource_QNAME = new QName("http://course.ws.blackboard/xsd", "sourcedidSource");
    private final static QName _TermVOSourcedidId_QNAME = new QName("http://course.ws.blackboard/xsd", "sourcedidId");
    private final static QName _TermVODataSrcId_QNAME = new QName("http://course.ws.blackboard/xsd", "dataSrcId");
    private final static QName _TermVOId_QNAME = new QName("http://course.ws.blackboard/xsd", "id");
    private final static QName _TermVODuration_QNAME = new QName("http://course.ws.blackboard/xsd", "duration");
    private final static QName _TermVOName_QNAME = new QName("http://course.ws.blackboard/xsd", "name");
    private final static QName _TermVODescription_QNAME = new QName("http://course.ws.blackboard/xsd", "description");
    private final static QName _LoadTermsByNameName_QNAME = new QName("http://course.ws.blackboard", "name");
    private final static QName _CategoryVODataSourceId_QNAME = new QName("http://course.ws.blackboard/xsd", "dataSourceId");
    private final static QName _CategoryVOParentId_QNAME = new QName("http://course.ws.blackboard/xsd", "parentId");
    private final static QName _CategoryVOTitle_QNAME = new QName("http://course.ws.blackboard/xsd", "title");
    private final static QName _CategoryVOBatchUid_QNAME = new QName("http://course.ws.blackboard/xsd", "batchUid");
    private final static QName _LoadTermTermID_QNAME = new QName("http://course.ws.blackboard", "termID");
    private final static QName _SaveCartridgeVo_QNAME = new QName("http://course.ws.blackboard", "vo");
    private final static QName _CreateCourseC_QNAME = new QName("http://course.ws.blackboard", "c");
    private final static QName _ChangeOrgCategoryBatchUidCategoryId_QNAME = new QName("http://course.ws.blackboard", "categoryId");
    private final static QName _ChangeOrgCategoryBatchUidNewBatchUid_QNAME = new QName("http://course.ws.blackboard", "newBatchUid");
    private final static QName _ChangeOrgBatchUidOldBatchUid_QNAME = new QName("http://course.ws.blackboard", "oldBatchUid");
    private final static QName _SaveTermTermVo_QNAME = new QName("http://course.ws.blackboard", "termVo");
    private final static QName _CategoryMembershipVOCourseId_QNAME = new QName("http://course.ws.blackboard/xsd", "courseId");
    private final static QName _CategoryMembershipVOCategoryId_QNAME = new QName("http://course.ws.blackboard/xsd", "categoryId");
    private final static QName _ChangeCourseDataSourceIdNewDataSourceId_QNAME = new QName("http://course.ws.blackboard", "newDataSourceId");
    private final static QName _GetClassificationsClassificationIdMask_QNAME = new QName("http://course.ws.blackboard", "classificationIdMask");
    private final static QName _StaffInfoVOOfficeHours_QNAME = new QName("http://course.ws.blackboard/xsd", "officeHours");
    private final static QName _StaffInfoVOFamilyName_QNAME = new QName("http://course.ws.blackboard/xsd", "familyName");
    private final static QName _StaffInfoVOHomePageUrl_QNAME = new QName("http://course.ws.blackboard/xsd", "homePageUrl");
    private final static QName _StaffInfoVOGivenName_QNAME = new QName("http://course.ws.blackboard/xsd", "givenName");
    private final static QName _StaffInfoVOTitleColor_QNAME = new QName("http://course.ws.blackboard/xsd", "titleColor");
    private final static QName _StaffInfoVOBiography_QNAME = new QName("http://course.ws.blackboard/xsd", "biography");
    private final static QName _StaffInfoVOBiographyType_QNAME = new QName("http://course.ws.blackboard/xsd", "biographyType");
    private final static QName _StaffInfoVOPhone_QNAME = new QName("http://course.ws.blackboard/xsd", "phone");
    private final static QName _StaffInfoVOPersistentTitle_QNAME = new QName("http://course.ws.blackboard/xsd", "persistentTitle");
    private final static QName _StaffInfoVOOfficeAddress_QNAME = new QName("http://course.ws.blackboard/xsd", "officeAddress");
    private final static QName _StaffInfoVOEmail_QNAME = new QName("http://course.ws.blackboard/xsd", "email");
    private final static QName _StaffInfoVOSirTitle_QNAME = new QName("http://course.ws.blackboard/xsd", "sirTitle");
    private final static QName _CartridgeVOIdentifier_QNAME = new QName("http://course.ws.blackboard/xsd", "identifier");
    private final static QName _CartridgeVOPublisherName_QNAME = new QName("http://course.ws.blackboard/xsd", "publisherName");
    private final static QName _GetCartridgeCartridgeId_QNAME = new QName("http://course.ws.blackboard", "cartridgeId");
    private final static QName _CourseVONavigationStyle_QNAME = new QName("http://course.ws.blackboard/xsd", "navigationStyle");
    private final static QName _CourseVONavColorBg_QNAME = new QName("http://course.ws.blackboard/xsd", "navColorBg");
    private final static QName _CourseVOEnrollmentType_QNAME = new QName("http://course.ws.blackboard/xsd", "enrollmentType");
    private final static QName _CourseVOCourseDuration_QNAME = new QName("http://course.ws.blackboard/xsd", "courseDuration");
    private final static QName _CourseVOCoursePace_QNAME = new QName("http://course.ws.blackboard/xsd", "coursePace");
    private final static QName _CourseVOClassificationId_QNAME = new QName("http://course.ws.blackboard/xsd", "classificationId");
    private final static QName _CourseVOCartridgeId_QNAME = new QName("http://course.ws.blackboard/xsd", "cartridgeId");
    private final static QName _CourseVOLocale_QNAME = new QName("http://course.ws.blackboard/xsd", "locale");
    private final static QName _CourseVOCourseServiceLevel_QNAME = new QName("http://course.ws.blackboard/xsd", "courseServiceLevel");
    private final static QName _CourseVONavColorFg_QNAME = new QName("http://course.ws.blackboard/xsd", "navColorFg");
    private final static QName _CourseVOInstitutionName_QNAME = new QName("http://course.ws.blackboard/xsd", "institutionName");
    private final static QName _CourseVOButtonStyleShape_QNAME = new QName("http://course.ws.blackboard/xsd", "buttonStyleShape");
    private final static QName _CourseVOButtonStyleType_QNAME = new QName("http://course.ws.blackboard/xsd", "buttonStyleType");
    private final static QName _CourseVOEnrollmentAccessCode_QNAME = new QName("http://course.ws.blackboard/xsd", "enrollmentAccessCode");
    private final static QName _CourseVOButtonStyleBbId_QNAME = new QName("http://course.ws.blackboard/xsd", "buttonStyleBbId");
    private final static QName _LoadTermByCourseIdCourseID_QNAME = new QName("http://course.ws.blackboard", "courseID");
    private final static QName _GetServerVersionUnused_QNAME = new QName("http://course.ws.blackboard", "unused");
    private final static QName _CourseFilterSearchDateOperator_QNAME = new QName("http://course.ws.blackboard/xsd", "searchDateOperator");
    private final static QName _CourseFilterSearchDate_QNAME = new QName("http://course.ws.blackboard/xsd", "searchDate");
    private final static QName _CourseFilterSearchValue_QNAME = new QName("http://course.ws.blackboard/xsd", "searchValue");
    private final static QName _CourseFilterSearchOperator_QNAME = new QName("http://course.ws.blackboard/xsd", "searchOperator");
    private final static QName _CourseFilterSearchKey_QNAME = new QName("http://course.ws.blackboard/xsd", "searchKey");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tandemservice.unifefu.ws.blackboard.course.gen
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdateCourseResponse }
     * 
     */
    public UpdateCourseResponse createUpdateCourseResponse() {
        return new UpdateCourseResponse();
    }

    /**
     * Create an instance of {@link SaveOrgCategoryMembership }
     * 
     */
    public SaveOrgCategoryMembership createSaveOrgCategoryMembership() {
        return new SaveOrgCategoryMembership();
    }

    /**
     * Create an instance of {@link DeleteCartridgeResponse }
     * 
     */
    public DeleteCartridgeResponse createDeleteCartridgeResponse() {
        return new DeleteCartridgeResponse();
    }

    /**
     * Create an instance of {@link SetCourseBannerImage }
     * 
     */
    public SetCourseBannerImage createSetCourseBannerImage() {
        return new SetCourseBannerImage();
    }

    /**
     * Create an instance of {@link RemoveCourseFromTermResponse }
     * 
     */
    public RemoveCourseFromTermResponse createRemoveCourseFromTermResponse() {
        return new RemoveCourseFromTermResponse();
    }

    /**
     * Create an instance of {@link DeleteTermResponse }
     * 
     */
    public DeleteTermResponse createDeleteTermResponse() {
        return new DeleteTermResponse();
    }

    /**
     * Create an instance of {@link GetOrg }
     * 
     */
    public GetOrg createGetOrg() {
        return new GetOrg();
    }

    /**
     * Create an instance of {@link TermVO }
     * 
     */
    public TermVO createTermVO() {
        return new TermVO();
    }

    /**
     * Create an instance of {@link SaveTermResponse }
     * 
     */
    public SaveTermResponse createSaveTermResponse() {
        return new SaveTermResponse();
    }

    /**
     * Create an instance of {@link CategoryFilter }
     * 
     */
    public CategoryFilter createCategoryFilter() {
        return new CategoryFilter();
    }

    /**
     * Create an instance of {@link SaveCourseResponse }
     * 
     */
    public SaveCourseResponse createSaveCourseResponse() {
        return new SaveCourseResponse();
    }

    /**
     * Create an instance of {@link GetOrgCategoryMembership }
     * 
     */
    public GetOrgCategoryMembership createGetOrgCategoryMembership() {
        return new GetOrgCategoryMembership();
    }

    /**
     * Create an instance of {@link LoadTerm }
     * 
     */
    public LoadTerm createLoadTerm() {
        return new LoadTerm();
    }

    /**
     * Create an instance of {@link SaveCartridge }
     * 
     */
    public SaveCartridge createSaveCartridge() {
        return new SaveCartridge();
    }

    /**
     * Create an instance of {@link ChangeCourseDataSourceIdResponse }
     * 
     */
    public ChangeCourseDataSourceIdResponse createChangeCourseDataSourceIdResponse() {
        return new ChangeCourseDataSourceIdResponse();
    }

    /**
     * Create an instance of {@link DeleteCourseCategoryMembership }
     * 
     */
    public DeleteCourseCategoryMembership createDeleteCourseCategoryMembership() {
        return new DeleteCourseCategoryMembership();
    }

    /**
     * Create an instance of {@link CategoryMembershipFilter }
     * 
     */
    public CategoryMembershipFilter createCategoryMembershipFilter() {
        return new CategoryMembershipFilter();
    }

    /**
     * Create an instance of {@link ChangeOrgCategoryBatchUid }
     * 
     */
    public ChangeOrgCategoryBatchUid createChangeOrgCategoryBatchUid() {
        return new ChangeOrgCategoryBatchUid();
    }

    /**
     * Create an instance of {@link GetGroup }
     * 
     */
    public GetGroup createGetGroup() {
        return new GetGroup();
    }

    /**
     * Create an instance of {@link InitializeCourseWS }
     * 
     */
    public InitializeCourseWS createInitializeCourseWS() {
        return new InitializeCourseWS();
    }

    /**
     * Create an instance of {@link SaveTerm }
     * 
     */
    public SaveTerm createSaveTerm() {
        return new SaveTerm();
    }

    /**
     * Create an instance of {@link SaveCourseCategoryMembershipResponse }
     * 
     */
    public SaveCourseCategoryMembershipResponse createSaveCourseCategoryMembershipResponse() {
        return new SaveCourseCategoryMembershipResponse();
    }

    /**
     * Create an instance of {@link ChangeCourseDataSourceId }
     * 
     */
    public ChangeCourseDataSourceId createChangeCourseDataSourceId() {
        return new ChangeCourseDataSourceId();
    }

    /**
     * Create an instance of {@link LoadTerms }
     * 
     */
    public LoadTerms createLoadTerms() {
        return new LoadTerms();
    }

    /**
     * Create an instance of {@link GetAvailableGroupToolsResponse }
     * 
     */
    public GetAvailableGroupToolsResponse createGetAvailableGroupToolsResponse() {
        return new GetAvailableGroupToolsResponse();
    }

    /**
     * Create an instance of {@link DeleteTerm }
     * 
     */
    public DeleteTerm createDeleteTerm() {
        return new DeleteTerm();
    }

    /**
     * Create an instance of {@link CreateOrgResponse }
     * 
     */
    public CreateOrgResponse createCreateOrgResponse() {
        return new CreateOrgResponse();
    }

    /**
     * Create an instance of {@link StaffInfoVO }
     * 
     */
    public StaffInfoVO createStaffInfoVO() {
        return new StaffInfoVO();
    }

    /**
     * Create an instance of {@link CartridgeVO }
     * 
     */
    public CartridgeVO createCartridgeVO() {
        return new CartridgeVO();
    }

    /**
     * Create an instance of {@link ChangeOrgDataSourceId }
     * 
     */
    public ChangeOrgDataSourceId createChangeOrgDataSourceId() {
        return new ChangeOrgDataSourceId();
    }

    /**
     * Create an instance of {@link CourseVO }
     * 
     */
    public CourseVO createCourseVO() {
        return new CourseVO();
    }

    /**
     * Create an instance of {@link LoadTermByCourseId }
     * 
     */
    public LoadTermByCourseId createLoadTermByCourseId() {
        return new LoadTermByCourseId();
    }

    /**
     * Create an instance of {@link SetCourseBannerImageResponse }
     * 
     */
    public SetCourseBannerImageResponse createSetCourseBannerImageResponse() {
        return new SetCourseBannerImageResponse();
    }

    /**
     * Create an instance of {@link DeleteOrgCategoryMembershipResponse }
     * 
     */
    public DeleteOrgCategoryMembershipResponse createDeleteOrgCategoryMembershipResponse() {
        return new DeleteOrgCategoryMembershipResponse();
    }

    /**
     * Create an instance of {@link SaveStaffInfo }
     * 
     */
    public SaveStaffInfo createSaveStaffInfo() {
        return new SaveStaffInfo();
    }

    /**
     * Create an instance of {@link DeleteCourseCategory }
     * 
     */
    public DeleteCourseCategory createDeleteCourseCategory() {
        return new DeleteCourseCategory();
    }

    /**
     * Create an instance of {@link GetServerVersionResponse }
     * 
     */
    public GetServerVersionResponse createGetServerVersionResponse() {
        return new GetServerVersionResponse();
    }

    /**
     * Create an instance of {@link DeleteCourse }
     * 
     */
    public DeleteCourse createDeleteCourse() {
        return new DeleteCourse();
    }

    /**
     * Create an instance of {@link SaveOrgCategoryResponse }
     * 
     */
    public SaveOrgCategoryResponse createSaveOrgCategoryResponse() {
        return new SaveOrgCategoryResponse();
    }

    /**
     * Create an instance of {@link GetStaffInfo }
     * 
     */
    public GetStaffInfo createGetStaffInfo() {
        return new GetStaffInfo();
    }

    /**
     * Create an instance of {@link SaveOrgCategoryMembershipResponse }
     * 
     */
    public SaveOrgCategoryMembershipResponse createSaveOrgCategoryMembershipResponse() {
        return new SaveOrgCategoryMembershipResponse();
    }

    /**
     * Create an instance of {@link CreateOrg }
     * 
     */
    public CreateOrg createCreateOrg() {
        return new CreateOrg();
    }

    /**
     * Create an instance of {@link GetServerVersion }
     * 
     */
    public GetServerVersion createGetServerVersion() {
        return new GetServerVersion();
    }

    /**
     * Create an instance of {@link DeleteOrgCategoryMembership }
     * 
     */
    public DeleteOrgCategoryMembership createDeleteOrgCategoryMembership() {
        return new DeleteOrgCategoryMembership();
    }

    /**
     * Create an instance of {@link GetCourseResponse }
     * 
     */
    public GetCourseResponse createGetCourseResponse() {
        return new GetCourseResponse();
    }

    /**
     * Create an instance of {@link GetOrgCategoryMembershipResponse }
     * 
     */
    public GetOrgCategoryMembershipResponse createGetOrgCategoryMembershipResponse() {
        return new GetOrgCategoryMembershipResponse();
    }

    /**
     * Create an instance of {@link DeleteCourseCategoryMembershipResponse }
     * 
     */
    public DeleteCourseCategoryMembershipResponse createDeleteCourseCategoryMembershipResponse() {
        return new DeleteCourseCategoryMembershipResponse();
    }

    /**
     * Create an instance of {@link ChangeCourseBatchUidResponse }
     * 
     */
    public ChangeCourseBatchUidResponse createChangeCourseBatchUidResponse() {
        return new ChangeCourseBatchUidResponse();
    }

    /**
     * Create an instance of {@link GetCourseCategoryMembership }
     * 
     */
    public GetCourseCategoryMembership createGetCourseCategoryMembership() {
        return new GetCourseCategoryMembership();
    }

    /**
     * Create an instance of {@link GetGroupResponse }
     * 
     */
    public GetGroupResponse createGetGroupResponse() {
        return new GetGroupResponse();
    }

    /**
     * Create an instance of {@link GetCategories }
     * 
     */
    public GetCategories createGetCategories() {
        return new GetCategories();
    }

    /**
     * Create an instance of {@link ChangeOrgBatchUidResponse }
     * 
     */
    public ChangeOrgBatchUidResponse createChangeOrgBatchUidResponse() {
        return new ChangeOrgBatchUidResponse();
    }

    /**
     * Create an instance of {@link DeleteCartridge }
     * 
     */
    public DeleteCartridge createDeleteCartridge() {
        return new DeleteCartridge();
    }

    /**
     * Create an instance of {@link AddCourseToTerm }
     * 
     */
    public AddCourseToTerm createAddCourseToTerm() {
        return new AddCourseToTerm();
    }

    /**
     * Create an instance of {@link LoadTermResponse }
     * 
     */
    public LoadTermResponse createLoadTermResponse() {
        return new LoadTermResponse();
    }

    /**
     * Create an instance of {@link GroupVO }
     * 
     */
    public GroupVO createGroupVO() {
        return new GroupVO();
    }

    /**
     * Create an instance of {@link ClassificationVO }
     * 
     */
    public ClassificationVO createClassificationVO() {
        return new ClassificationVO();
    }

    /**
     * Create an instance of {@link SaveStaffInfoResponse }
     * 
     */
    public SaveStaffInfoResponse createSaveStaffInfoResponse() {
        return new SaveStaffInfoResponse();
    }

    /**
     * Create an instance of {@link SaveCartridgeResponse }
     * 
     */
    public SaveCartridgeResponse createSaveCartridgeResponse() {
        return new SaveCartridgeResponse();
    }

    /**
     * Create an instance of {@link DeleteStaffInfoResponse }
     * 
     */
    public DeleteStaffInfoResponse createDeleteStaffInfoResponse() {
        return new DeleteStaffInfoResponse();
    }

    /**
     * Create an instance of {@link SaveGroupResponse }
     * 
     */
    public SaveGroupResponse createSaveGroupResponse() {
        return new SaveGroupResponse();
    }

    /**
     * Create an instance of {@link DeleteCourseCategoryResponse }
     * 
     */
    public DeleteCourseCategoryResponse createDeleteCourseCategoryResponse() {
        return new DeleteCourseCategoryResponse();
    }

    /**
     * Create an instance of {@link GetOrgResponse }
     * 
     */
    public GetOrgResponse createGetOrgResponse() {
        return new GetOrgResponse();
    }

    /**
     * Create an instance of {@link GetCourse }
     * 
     */
    public GetCourse createGetCourse() {
        return new GetCourse();
    }

    /**
     * Create an instance of {@link VersionVO }
     * 
     */
    public VersionVO createVersionVO() {
        return new VersionVO();
    }

    /**
     * Create an instance of {@link DeleteOrg }
     * 
     */
    public DeleteOrg createDeleteOrg() {
        return new DeleteOrg();
    }

    /**
     * Create an instance of {@link DeleteOrgCategoryResponse }
     * 
     */
    public DeleteOrgCategoryResponse createDeleteOrgCategoryResponse() {
        return new DeleteOrgCategoryResponse();
    }

    /**
     * Create an instance of {@link SaveOrgCategory }
     * 
     */
    public SaveOrgCategory createSaveOrgCategory() {
        return new SaveOrgCategory();
    }

    /**
     * Create an instance of {@link GroupFilter }
     * 
     */
    public GroupFilter createGroupFilter() {
        return new GroupFilter();
    }

    /**
     * Create an instance of {@link LoadTermsByName }
     * 
     */
    public LoadTermsByName createLoadTermsByName() {
        return new LoadTermsByName();
    }

    /**
     * Create an instance of {@link CategoryVO }
     * 
     */
    public CategoryVO createCategoryVO() {
        return new CategoryVO();
    }

    /**
     * Create an instance of {@link UpdateOrgResponse }
     * 
     */
    public UpdateOrgResponse createUpdateOrgResponse() {
        return new UpdateOrgResponse();
    }

    /**
     * Create an instance of {@link ChangeOrgCategoryBatchUidResponse }
     * 
     */
    public ChangeOrgCategoryBatchUidResponse createChangeOrgCategoryBatchUidResponse() {
        return new ChangeOrgCategoryBatchUidResponse();
    }

    /**
     * Create an instance of {@link GetStaffInfoResponse }
     * 
     */
    public GetStaffInfoResponse createGetStaffInfoResponse() {
        return new GetStaffInfoResponse();
    }

    /**
     * Create an instance of {@link CreateCourse }
     * 
     */
    public CreateCourse createCreateCourse() {
        return new CreateCourse();
    }

    /**
     * Create an instance of {@link SaveCourseCategoryMembership }
     * 
     */
    public SaveCourseCategoryMembership createSaveCourseCategoryMembership() {
        return new SaveCourseCategoryMembership();
    }

    /**
     * Create an instance of {@link UpdateCourse }
     * 
     */
    public UpdateCourse createUpdateCourse() {
        return new UpdateCourse();
    }

    /**
     * Create an instance of {@link DeleteOrgCategory }
     * 
     */
    public DeleteOrgCategory createDeleteOrgCategory() {
        return new DeleteOrgCategory();
    }

    /**
     * Create an instance of {@link LoadTermsResponse }
     * 
     */
    public LoadTermsResponse createLoadTermsResponse() {
        return new LoadTermsResponse();
    }

    /**
     * Create an instance of {@link GetCategoriesResponse }
     * 
     */
    public GetCategoriesResponse createGetCategoriesResponse() {
        return new GetCategoriesResponse();
    }

    /**
     * Create an instance of {@link DeleteCourseResponse }
     * 
     */
    public DeleteCourseResponse createDeleteCourseResponse() {
        return new DeleteCourseResponse();
    }

    /**
     * Create an instance of {@link ChangeOrgBatchUid }
     * 
     */
    public ChangeOrgBatchUid createChangeOrgBatchUid() {
        return new ChangeOrgBatchUid();
    }

    /**
     * Create an instance of {@link CategoryMembershipVO }
     * 
     */
    public CategoryMembershipVO createCategoryMembershipVO() {
        return new CategoryMembershipVO();
    }

    /**
     * Create an instance of {@link SaveCourseCategory }
     * 
     */
    public SaveCourseCategory createSaveCourseCategory() {
        return new SaveCourseCategory();
    }

    /**
     * Create an instance of {@link GetAvailableGroupTools }
     * 
     */
    public GetAvailableGroupTools createGetAvailableGroupTools() {
        return new GetAvailableGroupTools();
    }

    /**
     * Create an instance of {@link GetClassifications }
     * 
     */
    public GetClassifications createGetClassifications() {
        return new GetClassifications();
    }

    /**
     * Create an instance of {@link CreateCourseResponse }
     * 
     */
    public CreateCourseResponse createCreateCourseResponse() {
        return new CreateCourseResponse();
    }

    /**
     * Create an instance of {@link LoadTermsByNameResponse }
     * 
     */
    public LoadTermsByNameResponse createLoadTermsByNameResponse() {
        return new LoadTermsByNameResponse();
    }

    /**
     * Create an instance of {@link UpdateOrg }
     * 
     */
    public UpdateOrg createUpdateOrg() {
        return new UpdateOrg();
    }

    /**
     * Create an instance of {@link InitializeCourseWSResponse }
     * 
     */
    public InitializeCourseWSResponse createInitializeCourseWSResponse() {
        return new InitializeCourseWSResponse();
    }

    /**
     * Create an instance of {@link GetCourseCategoryMembershipResponse }
     * 
     */
    public GetCourseCategoryMembershipResponse createGetCourseCategoryMembershipResponse() {
        return new GetCourseCategoryMembershipResponse();
    }

    /**
     * Create an instance of {@link GetCartridge }
     * 
     */
    public GetCartridge createGetCartridge() {
        return new GetCartridge();
    }

    /**
     * Create an instance of {@link DeleteGroup }
     * 
     */
    public DeleteGroup createDeleteGroup() {
        return new DeleteGroup();
    }

    /**
     * Create an instance of {@link LoadCoursesInTerm }
     * 
     */
    public LoadCoursesInTerm createLoadCoursesInTerm() {
        return new LoadCoursesInTerm();
    }

    /**
     * Create an instance of {@link SaveGroup }
     * 
     */
    public SaveGroup createSaveGroup() {
        return new SaveGroup();
    }

    /**
     * Create an instance of {@link SaveCourseCategoryResponse }
     * 
     */
    public SaveCourseCategoryResponse createSaveCourseCategoryResponse() {
        return new SaveCourseCategoryResponse();
    }

    /**
     * Create an instance of {@link LoadCoursesInTermResponse }
     * 
     */
    public LoadCoursesInTermResponse createLoadCoursesInTermResponse() {
        return new LoadCoursesInTermResponse();
    }

    /**
     * Create an instance of {@link SaveCourse }
     * 
     */
    public SaveCourse createSaveCourse() {
        return new SaveCourse();
    }

    /**
     * Create an instance of {@link RemoveCourseFromTerm }
     * 
     */
    public RemoveCourseFromTerm createRemoveCourseFromTerm() {
        return new RemoveCourseFromTerm();
    }

    /**
     * Create an instance of {@link LoadTermByCourseIdResponse }
     * 
     */
    public LoadTermByCourseIdResponse createLoadTermByCourseIdResponse() {
        return new LoadTermByCourseIdResponse();
    }

    /**
     * Create an instance of {@link ChangeOrgDataSourceIdResponse }
     * 
     */
    public ChangeOrgDataSourceIdResponse createChangeOrgDataSourceIdResponse() {
        return new ChangeOrgDataSourceIdResponse();
    }

    /**
     * Create an instance of {@link DeleteGroupResponse }
     * 
     */
    public DeleteGroupResponse createDeleteGroupResponse() {
        return new DeleteGroupResponse();
    }

    /**
     * Create an instance of {@link ChangeCourseBatchUid }
     * 
     */
    public ChangeCourseBatchUid createChangeCourseBatchUid() {
        return new ChangeCourseBatchUid();
    }

    /**
     * Create an instance of {@link CourseFilter }
     * 
     */
    public CourseFilter createCourseFilter() {
        return new CourseFilter();
    }

    /**
     * Create an instance of {@link ChangeCourseCategoryBatchUid }
     * 
     */
    public ChangeCourseCategoryBatchUid createChangeCourseCategoryBatchUid() {
        return new ChangeCourseCategoryBatchUid();
    }

    /**
     * Create an instance of {@link DeleteOrgResponse }
     * 
     */
    public DeleteOrgResponse createDeleteOrgResponse() {
        return new DeleteOrgResponse();
    }

    /**
     * Create an instance of {@link ChangeCourseCategoryBatchUidResponse }
     * 
     */
    public ChangeCourseCategoryBatchUidResponse createChangeCourseCategoryBatchUidResponse() {
        return new ChangeCourseCategoryBatchUidResponse();
    }

    /**
     * Create an instance of {@link AddCourseToTermResponse }
     * 
     */
    public AddCourseToTermResponse createAddCourseToTermResponse() {
        return new AddCourseToTermResponse();
    }

    /**
     * Create an instance of {@link GetCartridgeResponse }
     * 
     */
    public GetCartridgeResponse createGetCartridgeResponse() {
        return new GetCartridgeResponse();
    }

    /**
     * Create an instance of {@link GetClassificationsResponse }
     * 
     */
    public GetClassificationsResponse createGetClassificationsResponse() {
        return new GetClassificationsResponse();
    }

    /**
     * Create an instance of {@link DeleteStaffInfo }
     * 
     */
    public DeleteStaffInfo createDeleteStaffInfo() {
        return new DeleteStaffInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = UpdateCourseResponse.class)
    public JAXBElement<String> createUpdateCourseResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, UpdateCourseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryMembershipVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "membership", scope = SaveOrgCategoryMembership.class)
    public JAXBElement<CategoryMembershipVO> createSaveOrgCategoryMembershipMembership(CategoryMembershipVO value) {
        return new JAXBElement<>(_SaveOrgCategoryMembershipMembership_QNAME, CategoryMembershipVO.class, SaveOrgCategoryMembership.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = SaveStaffInfoResponse.class)
    public JAXBElement<String> createSaveStaffInfoResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, SaveStaffInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = DeleteCartridgeResponse.class)
    public JAXBElement<String> createDeleteCartridgeResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, DeleteCartridgeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = SaveCartridgeResponse.class)
    public JAXBElement<String> createSaveCartridgeResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, SaveCartridgeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = SaveGroupResponse.class)
    public JAXBElement<String> createSaveGroupResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, SaveGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "fileContents", scope = SetCourseBannerImage.class)
    public JAXBElement<byte[]> createSetCourseBannerImageFileContents(byte[] value) {
        return new JAXBElement<>(_SetCourseBannerImageFileContents_QNAME, byte[].class, SetCourseBannerImage.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseId", scope = SetCourseBannerImage.class)
    public JAXBElement<String> createSetCourseBannerImageCourseId(String value) {
        return new JAXBElement<>(_SetCourseBannerImageCourseId_QNAME, String.class, SetCourseBannerImage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "fileName", scope = SetCourseBannerImage.class)
    public JAXBElement<String> createSetCourseBannerImageFileName(String value) {
        return new JAXBElement<>(_SetCourseBannerImageFileName_QNAME, String.class, SetCourseBannerImage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CourseFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "filter", scope = GetCourse.class)
    public JAXBElement<CourseFilter> createGetCourseFilter(CourseFilter value) {
        return new JAXBElement<>(_GetCourseFilter_QNAME, CourseFilter.class, GetCourse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CourseFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "filter", scope = GetOrg.class)
    public JAXBElement<CourseFilter> createGetOrgFilter(CourseFilter value) {
        return new JAXBElement<>(_GetCourseFilter_QNAME, CourseFilter.class, GetOrg.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "adminCategory", scope = SaveOrgCategory.class)
    public JAXBElement<CategoryVO> createSaveOrgCategoryAdminCategory(CategoryVO value) {
        return new JAXBElement<>(_SaveOrgCategoryAdminCategory_QNAME, CategoryVO.class, SaveOrgCategory.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "descriptionType", scope = TermVO.class)
    public JAXBElement<String> createTermVODescriptionType(String value) {
        return new JAXBElement<>(_TermVODescriptionType_QNAME, String.class, TermVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "sourcedidSource", scope = TermVO.class)
    public JAXBElement<String> createTermVOSourcedidSource(String value) {
        return new JAXBElement<>(_TermVOSourcedidSource_QNAME, String.class, TermVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "sourcedidId", scope = TermVO.class)
    public JAXBElement<String> createTermVOSourcedidId(String value) {
        return new JAXBElement<>(_TermVOSourcedidId_QNAME, String.class, TermVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "dataSrcId", scope = TermVO.class)
    public JAXBElement<String> createTermVODataSrcId(String value) {
        return new JAXBElement<>(_TermVODataSrcId_QNAME, String.class, TermVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "id", scope = TermVO.class)
    public JAXBElement<String> createTermVOId(String value) {
        return new JAXBElement<>(_TermVOId_QNAME, String.class, TermVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "duration", scope = TermVO.class)
    public JAXBElement<String> createTermVODuration(String value) {
        return new JAXBElement<>(_TermVODuration_QNAME, String.class, TermVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "name", scope = TermVO.class)
    public JAXBElement<String> createTermVOName(String value) {
        return new JAXBElement<>(_TermVOName_QNAME, String.class, TermVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "description", scope = TermVO.class)
    public JAXBElement<String> createTermVODescription(String value) {
        return new JAXBElement<>(_TermVODescription_QNAME, String.class, TermVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = SaveTermResponse.class)
    public JAXBElement<String> createSaveTermResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, SaveTermResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "name", scope = LoadTermsByName.class)
    public JAXBElement<String> createLoadTermsByNameName(String value) {
        return new JAXBElement<>(_LoadTermsByNameName_QNAME, String.class, LoadTermsByName.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "dataSourceId", scope = CategoryVO.class)
    public JAXBElement<String> createCategoryVODataSourceId(String value) {
        return new JAXBElement<>(_CategoryVODataSourceId_QNAME, String.class, CategoryVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "parentId", scope = CategoryVO.class)
    public JAXBElement<String> createCategoryVOParentId(String value) {
        return new JAXBElement<>(_CategoryVOParentId_QNAME, String.class, CategoryVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "id", scope = CategoryVO.class)
    public JAXBElement<String> createCategoryVOId(String value) {
        return new JAXBElement<>(_TermVOId_QNAME, String.class, CategoryVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "title", scope = CategoryVO.class)
    public JAXBElement<String> createCategoryVOTitle(String value) {
        return new JAXBElement<>(_CategoryVOTitle_QNAME, String.class, CategoryVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "batchUid", scope = CategoryVO.class)
    public JAXBElement<String> createCategoryVOBatchUid(String value) {
        return new JAXBElement<>(_CategoryVOBatchUid_QNAME, String.class, CategoryVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "description", scope = CategoryVO.class)
    public JAXBElement<String> createCategoryVODescription(String value) {
        return new JAXBElement<>(_TermVODescription_QNAME, String.class, CategoryVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = SaveCourseResponse.class)
    public JAXBElement<String> createSaveCourseResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, SaveCourseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = UpdateOrgResponse.class)
    public JAXBElement<String> createUpdateOrgResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, UpdateOrgResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryMembershipFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "filter", scope = GetOrgCategoryMembership.class)
    public JAXBElement<CategoryMembershipFilter> createGetOrgCategoryMembershipFilter(CategoryMembershipFilter value) {
        return new JAXBElement<>(_GetCourseFilter_QNAME, CategoryMembershipFilter.class, GetOrgCategoryMembership.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "termID", scope = LoadTerm.class)
    public JAXBElement<String> createLoadTermTermID(String value) {
        return new JAXBElement<>(_LoadTermTermID_QNAME, String.class, LoadTerm.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CartridgeVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "vo", scope = SaveCartridge.class)
    public JAXBElement<CartridgeVO> createSaveCartridgeVo(CartridgeVO value) {
        return new JAXBElement<>(_SaveCartridgeVo_QNAME, CartridgeVO.class, SaveCartridge.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CourseVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "c", scope = CreateCourse.class)
    public JAXBElement<CourseVO> createCreateCourseC(CourseVO value) {
        return new JAXBElement<>(_CreateCourseC_QNAME, CourseVO.class, CreateCourse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryMembershipVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "membership", scope = SaveCourseCategoryMembership.class)
    public JAXBElement<CategoryMembershipVO> createSaveCourseCategoryMembershipMembership(CategoryMembershipVO value) {
        return new JAXBElement<>(_SaveOrgCategoryMembershipMembership_QNAME, CategoryMembershipVO.class, SaveCourseCategoryMembership.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CourseVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "c", scope = UpdateCourse.class)
    public JAXBElement<CourseVO> createUpdateCourseC(CourseVO value) {
        return new JAXBElement<>(_CreateCourseC_QNAME, CourseVO.class, UpdateCourse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "categoryId", scope = ChangeOrgCategoryBatchUid.class)
    public JAXBElement<String> createChangeOrgCategoryBatchUidCategoryId(String value) {
        return new JAXBElement<>(_ChangeOrgCategoryBatchUidCategoryId_QNAME, String.class, ChangeOrgCategoryBatchUid.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "newBatchUid", scope = ChangeOrgCategoryBatchUid.class)
    public JAXBElement<String> createChangeOrgCategoryBatchUidNewBatchUid(String value) {
        return new JAXBElement<>(_ChangeOrgCategoryBatchUidNewBatchUid_QNAME, String.class, ChangeOrgCategoryBatchUid.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseId", scope = GetGroup.class)
    public JAXBElement<String> createGetGroupCourseId(String value) {
        return new JAXBElement<>(_SetCourseBannerImageCourseId_QNAME, String.class, GetGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "filter", scope = GetGroup.class)
    public JAXBElement<GroupFilter> createGetGroupFilter(GroupFilter value) {
        return new JAXBElement<>(_GetCourseFilter_QNAME, GroupFilter.class, GetGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "newBatchUid", scope = ChangeOrgBatchUid.class)
    public JAXBElement<String> createChangeOrgBatchUidNewBatchUid(String value) {
        return new JAXBElement<>(_ChangeOrgCategoryBatchUidNewBatchUid_QNAME, String.class, ChangeOrgBatchUid.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "oldBatchUid", scope = ChangeOrgBatchUid.class)
    public JAXBElement<String> createChangeOrgBatchUidOldBatchUid(String value) {
        return new JAXBElement<>(_ChangeOrgBatchUidOldBatchUid_QNAME, String.class, ChangeOrgBatchUid.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TermVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "termVo", scope = SaveTerm.class)
    public JAXBElement<TermVO> createSaveTermTermVo(TermVO value) {
        return new JAXBElement<>(_SaveTermTermVo_QNAME, TermVO.class, SaveTerm.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "courseId", scope = CategoryMembershipVO.class)
    public JAXBElement<String> createCategoryMembershipVOCourseId(String value) {
        return new JAXBElement<>(_CategoryMembershipVOCourseId_QNAME, String.class, CategoryMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "dataSourceId", scope = CategoryMembershipVO.class)
    public JAXBElement<String> createCategoryMembershipVODataSourceId(String value) {
        return new JAXBElement<>(_CategoryVODataSourceId_QNAME, String.class, CategoryMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "id", scope = CategoryMembershipVO.class)
    public JAXBElement<String> createCategoryMembershipVOId(String value) {
        return new JAXBElement<>(_TermVOId_QNAME, String.class, CategoryMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "categoryId", scope = CategoryMembershipVO.class)
    public JAXBElement<String> createCategoryMembershipVOCategoryId(String value) {
        return new JAXBElement<>(_CategoryMembershipVOCategoryId_QNAME, String.class, CategoryMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = SaveCourseCategoryMembershipResponse.class)
    public JAXBElement<String> createSaveCourseCategoryMembershipResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, SaveCourseCategoryMembershipResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "newDataSourceId", scope = ChangeCourseDataSourceId.class)
    public JAXBElement<String> createChangeCourseDataSourceIdNewDataSourceId(String value) {
        return new JAXBElement<>(_ChangeCourseDataSourceIdNewDataSourceId_QNAME, String.class, ChangeCourseDataSourceId.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseId", scope = ChangeCourseDataSourceId.class)
    public JAXBElement<String> createChangeCourseDataSourceIdCourseId(String value) {
        return new JAXBElement<>(_SetCourseBannerImageCourseId_QNAME, String.class, ChangeCourseDataSourceId.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "adminCategory", scope = SaveCourseCategory.class)
    public JAXBElement<CategoryVO> createSaveCourseCategoryAdminCategory(CategoryVO value) {
        return new JAXBElement<>(_SaveOrgCategoryAdminCategory_QNAME, CategoryVO.class, SaveCourseCategory.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseId", scope = GetAvailableGroupTools.class)
    public JAXBElement<String> createGetAvailableGroupToolsCourseId(String value) {
        return new JAXBElement<>(_SetCourseBannerImageCourseId_QNAME, String.class, GetAvailableGroupTools.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "classificationIdMask", scope = GetClassifications.class)
    public JAXBElement<String> createGetClassificationsClassificationIdMask(String value) {
        return new JAXBElement<>(_GetClassificationsClassificationIdMask_QNAME, String.class, GetClassifications.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = CreateCourseResponse.class)
    public JAXBElement<String> createCreateCourseResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, CreateCourseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CourseVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "c", scope = UpdateOrg.class)
    public JAXBElement<CourseVO> createUpdateOrgC(CourseVO value) {
        return new JAXBElement<>(_CreateCourseC_QNAME, CourseVO.class, UpdateOrg.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "termID", scope = DeleteTerm.class)
    public JAXBElement<String> createDeleteTermTermID(String value) {
        return new JAXBElement<>(_LoadTermTermID_QNAME, String.class, DeleteTerm.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = CreateOrgResponse.class)
    public JAXBElement<String> createCreateOrgResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, CreateOrgResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "officeHours", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOOfficeHours(String value) {
        return new JAXBElement<>(_StaffInfoVOOfficeHours_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "familyName", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOFamilyName(String value) {
        return new JAXBElement<>(_StaffInfoVOFamilyName_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "homePageUrl", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOHomePageUrl(String value) {
        return new JAXBElement<>(_StaffInfoVOHomePageUrl_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "givenName", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOGivenName(String value) {
        return new JAXBElement<>(_StaffInfoVOGivenName_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "titleColor", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOTitleColor(String value) {
        return new JAXBElement<>(_StaffInfoVOTitleColor_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "biography", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOBiography(String value) {
        return new JAXBElement<>(_StaffInfoVOBiography_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "biographyType", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOBiographyType(String value) {
        return new JAXBElement<>(_StaffInfoVOBiographyType_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "phone", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOPhone(String value) {
        return new JAXBElement<>(_StaffInfoVOPhone_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "persistentTitle", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOPersistentTitle(String value) {
        return new JAXBElement<>(_StaffInfoVOPersistentTitle_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "officeAddress", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOOfficeAddress(String value) {
        return new JAXBElement<>(_StaffInfoVOOfficeAddress_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "parentId", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOParentId(String value) {
        return new JAXBElement<>(_CategoryVOParentId_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "id", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOId(String value) {
        return new JAXBElement<>(_TermVOId_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "title", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOTitle(String value) {
        return new JAXBElement<>(_CategoryVOTitle_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "email", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOEmail(String value) {
        return new JAXBElement<>(_StaffInfoVOEmail_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "sirTitle", scope = StaffInfoVO.class)
    public JAXBElement<String> createStaffInfoVOSirTitle(String value) {
        return new JAXBElement<>(_StaffInfoVOSirTitle_QNAME, String.class, StaffInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "identifier", scope = CartridgeVO.class)
    public JAXBElement<String> createCartridgeVOIdentifier(String value) {
        return new JAXBElement<>(_CartridgeVOIdentifier_QNAME, String.class, CartridgeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "id", scope = CartridgeVO.class)
    public JAXBElement<String> createCartridgeVOId(String value) {
        return new JAXBElement<>(_TermVOId_QNAME, String.class, CartridgeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "title", scope = CartridgeVO.class)
    public JAXBElement<String> createCartridgeVOTitle(String value) {
        return new JAXBElement<>(_CategoryVOTitle_QNAME, String.class, CartridgeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "publisherName", scope = CartridgeVO.class)
    public JAXBElement<String> createCartridgeVOPublisherName(String value) {
        return new JAXBElement<>(_CartridgeVOPublisherName_QNAME, String.class, CartridgeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "description", scope = CartridgeVO.class)
    public JAXBElement<String> createCartridgeVODescription(String value) {
        return new JAXBElement<>(_TermVODescription_QNAME, String.class, CartridgeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "cartridgeId", scope = GetCartridge.class)
    public JAXBElement<String> createGetCartridgeCartridgeId(String value) {
        return new JAXBElement<>(_GetCartridgeCartridgeId_QNAME, String.class, GetCartridge.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseId", scope = DeleteGroup.class)
    public JAXBElement<String> createDeleteGroupCourseId(String value) {
        return new JAXBElement<>(_SetCourseBannerImageCourseId_QNAME, String.class, DeleteGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "newDataSourceId", scope = ChangeOrgDataSourceId.class)
    public JAXBElement<String> createChangeOrgDataSourceIdNewDataSourceId(String value) {
        return new JAXBElement<>(_ChangeCourseDataSourceIdNewDataSourceId_QNAME, String.class, ChangeOrgDataSourceId.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseId", scope = ChangeOrgDataSourceId.class)
    public JAXBElement<String> createChangeOrgDataSourceIdCourseId(String value) {
        return new JAXBElement<>(_SetCourseBannerImageCourseId_QNAME, String.class, ChangeOrgDataSourceId.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "termID", scope = LoadCoursesInTerm.class)
    public JAXBElement<String> createLoadCoursesInTermTermID(String value) {
        return new JAXBElement<>(_LoadTermTermID_QNAME, String.class, LoadCoursesInTerm.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseId", scope = SaveGroup.class)
    public JAXBElement<String> createSaveGroupCourseId(String value) {
        return new JAXBElement<>(_SetCourseBannerImageCourseId_QNAME, String.class, SaveGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "vo", scope = SaveGroup.class)
    public JAXBElement<GroupVO> createSaveGroupVo(GroupVO value) {
        return new JAXBElement<>(_SaveCartridgeVo_QNAME, GroupVO.class, SaveGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "navigationStyle", scope = CourseVO.class)
    public JAXBElement<String> createCourseVONavigationStyle(String value) {
        return new JAXBElement<>(_CourseVONavigationStyle_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "navColorBg", scope = CourseVO.class)
    public JAXBElement<String> createCourseVONavColorBg(String value) {
        return new JAXBElement<>(_CourseVONavColorBg_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "courseId", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOCourseId(String value) {
        return new JAXBElement<>(_CategoryMembershipVOCourseId_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "enrollmentType", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOEnrollmentType(String value) {
        return new JAXBElement<>(_CourseVOEnrollmentType_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "courseDuration", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOCourseDuration(String value) {
        return new JAXBElement<>(_CourseVOCourseDuration_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "coursePace", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOCoursePace(String value) {
        return new JAXBElement<>(_CourseVOCoursePace_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "dataSourceId", scope = CourseVO.class)
    public JAXBElement<String> createCourseVODataSourceId(String value) {
        return new JAXBElement<>(_CategoryVODataSourceId_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "classificationId", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOClassificationId(String value) {
        return new JAXBElement<>(_CourseVOClassificationId_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "cartridgeId", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOCartridgeId(String value) {
        return new JAXBElement<>(_CourseVOCartridgeId_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "locale", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOLocale(String value) {
        return new JAXBElement<>(_CourseVOLocale_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "courseServiceLevel", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOCourseServiceLevel(String value) {
        return new JAXBElement<>(_CourseVOCourseServiceLevel_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "navColorFg", scope = CourseVO.class)
    public JAXBElement<String> createCourseVONavColorFg(String value) {
        return new JAXBElement<>(_CourseVONavColorFg_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "institutionName", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOInstitutionName(String value) {
        return new JAXBElement<>(_CourseVOInstitutionName_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "buttonStyleShape", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOButtonStyleShape(String value) {
        return new JAXBElement<>(_CourseVOButtonStyleShape_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "id", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOId(String value) {
        return new JAXBElement<>(_TermVOId_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "batchUid", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOBatchUid(String value) {
        return new JAXBElement<>(_CategoryVOBatchUid_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "buttonStyleType", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOButtonStyleType(String value) {
        return new JAXBElement<>(_CourseVOButtonStyleType_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "enrollmentAccessCode", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOEnrollmentAccessCode(String value) {
        return new JAXBElement<>(_CourseVOEnrollmentAccessCode_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "name", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOName(String value) {
        return new JAXBElement<>(_TermVOName_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "buttonStyleBbId", scope = CourseVO.class)
    public JAXBElement<String> createCourseVOButtonStyleBbId(String value) {
        return new JAXBElement<>(_CourseVOButtonStyleBbId_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "description", scope = CourseVO.class)
    public JAXBElement<String> createCourseVODescription(String value) {
        return new JAXBElement<>(_TermVODescription_QNAME, String.class, CourseVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = SaveCourseCategoryResponse.class)
    public JAXBElement<String> createSaveCourseCategoryResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, SaveCourseCategoryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseID", scope = LoadTermByCourseId.class)
    public JAXBElement<String> createLoadTermByCourseIdCourseID(String value) {
        return new JAXBElement<>(_LoadTermByCourseIdCourseID_QNAME, String.class, LoadTermByCourseId.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CourseVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "c", scope = SaveCourse.class)
    public JAXBElement<CourseVO> createSaveCourseC(CourseVO value) {
        return new JAXBElement<>(_CreateCourseC_QNAME, CourseVO.class, SaveCourse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseId", scope = SaveStaffInfo.class)
    public JAXBElement<String> createSaveStaffInfoCourseId(String value) {
        return new JAXBElement<>(_SetCourseBannerImageCourseId_QNAME, String.class, SaveStaffInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StaffInfoVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "vo", scope = SaveStaffInfo.class)
    public JAXBElement<StaffInfoVO> createSaveStaffInfoVo(StaffInfoVO value) {
        return new JAXBElement<>(_SaveCartridgeVo_QNAME, StaffInfoVO.class, SaveStaffInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseID", scope = RemoveCourseFromTerm.class)
    public JAXBElement<String> createRemoveCourseFromTermCourseID(String value) {
        return new JAXBElement<>(_LoadTermByCourseIdCourseID_QNAME, String.class, RemoveCourseFromTerm.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = GetServerVersionResponse.class)
    public JAXBElement<VersionVO> createGetServerVersionResponseReturn(VersionVO value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, VersionVO.class, GetServerVersionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TermVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = LoadTermByCourseIdResponse.class)
    public JAXBElement<TermVO> createLoadTermByCourseIdResponseReturn(TermVO value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, TermVO.class, LoadTermByCourseIdResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = SaveOrgCategoryResponse.class)
    public JAXBElement<String> createSaveOrgCategoryResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, SaveOrgCategoryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseId", scope = GetStaffInfo.class)
    public JAXBElement<String> createGetStaffInfoCourseId(String value) {
        return new JAXBElement<>(_SetCourseBannerImageCourseId_QNAME, String.class, GetStaffInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = SaveOrgCategoryMembershipResponse.class)
    public JAXBElement<String> createSaveOrgCategoryMembershipResponseReturn(String value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, String.class, SaveOrgCategoryMembershipResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CourseVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "c", scope = CreateOrg.class)
    public JAXBElement<CourseVO> createCreateOrgC(CourseVO value) {
        return new JAXBElement<>(_CreateCourseC_QNAME, CourseVO.class, CreateOrg.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "unused", scope = GetServerVersion.class)
    public JAXBElement<VersionVO> createGetServerVersionUnused(VersionVO value) {
        return new JAXBElement<>(_GetServerVersionUnused_QNAME, VersionVO.class, GetServerVersion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "newBatchUid", scope = ChangeCourseBatchUid.class)
    public JAXBElement<String> createChangeCourseBatchUidNewBatchUid(String value) {
        return new JAXBElement<>(_ChangeOrgCategoryBatchUidNewBatchUid_QNAME, String.class, ChangeCourseBatchUid.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "oldBatchUid", scope = ChangeCourseBatchUid.class)
    public JAXBElement<String> createChangeCourseBatchUidOldBatchUid(String value) {
        return new JAXBElement<>(_ChangeOrgBatchUidOldBatchUid_QNAME, String.class, ChangeCourseBatchUid.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "searchDateOperator", scope = CourseFilter.class)
    public JAXBElement<String> createCourseFilterSearchDateOperator(String value) {
        return new JAXBElement<>(_CourseFilterSearchDateOperator_QNAME, String.class, CourseFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "searchDate", scope = CourseFilter.class)
    public JAXBElement<Long> createCourseFilterSearchDate(Long value) {
        return new JAXBElement<>(_CourseFilterSearchDate_QNAME, Long.class, CourseFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "searchValue", scope = CourseFilter.class)
    public JAXBElement<String> createCourseFilterSearchValue(String value) {
        return new JAXBElement<>(_CourseFilterSearchValue_QNAME, String.class, CourseFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "searchOperator", scope = CourseFilter.class)
    public JAXBElement<String> createCourseFilterSearchOperator(String value) {
        return new JAXBElement<>(_CourseFilterSearchOperator_QNAME, String.class, CourseFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "searchKey", scope = CourseFilter.class)
    public JAXBElement<String> createCourseFilterSearchKey(String value) {
        return new JAXBElement<>(_CourseFilterSearchKey_QNAME, String.class, CourseFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "categoryId", scope = ChangeCourseCategoryBatchUid.class)
    public JAXBElement<String> createChangeCourseCategoryBatchUidCategoryId(String value) {
        return new JAXBElement<>(_ChangeOrgCategoryBatchUidCategoryId_QNAME, String.class, ChangeCourseCategoryBatchUid.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "newBatchUid", scope = ChangeCourseCategoryBatchUid.class)
    public JAXBElement<String> createChangeCourseCategoryBatchUidNewBatchUid(String value) {
        return new JAXBElement<>(_ChangeOrgCategoryBatchUidNewBatchUid_QNAME, String.class, ChangeCourseCategoryBatchUid.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryMembershipFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "filter", scope = GetCourseCategoryMembership.class)
    public JAXBElement<CategoryMembershipFilter> createGetCourseCategoryMembershipFilter(CategoryMembershipFilter value) {
        return new JAXBElement<>(_GetCourseFilter_QNAME, CategoryMembershipFilter.class, GetCourseCategoryMembership.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CartridgeVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = GetCartridgeResponse.class)
    public JAXBElement<CartridgeVO> createGetCartridgeResponseReturn(CartridgeVO value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, CartridgeVO.class, GetCartridgeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "filter", scope = GetCategories.class)
    public JAXBElement<CategoryFilter> createGetCategoriesFilter(CategoryFilter value) {
        return new JAXBElement<>(_GetCourseFilter_QNAME, CategoryFilter.class, GetCategories.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "cartridgeId", scope = DeleteCartridge.class)
    public JAXBElement<String> createDeleteCartridgeCartridgeId(String value) {
        return new JAXBElement<>(_GetCartridgeCartridgeId_QNAME, String.class, DeleteCartridge.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseID", scope = AddCourseToTerm.class)
    public JAXBElement<String> createAddCourseToTermCourseID(String value) {
        return new JAXBElement<>(_LoadTermByCourseIdCourseID_QNAME, String.class, AddCourseToTerm.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "termID", scope = AddCourseToTerm.class)
    public JAXBElement<String> createAddCourseToTermTermID(String value) {
        return new JAXBElement<>(_LoadTermTermID_QNAME, String.class, AddCourseToTerm.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "courseId", scope = DeleteStaffInfo.class)
    public JAXBElement<String> createDeleteStaffInfoCourseId(String value) {
        return new JAXBElement<>(_SetCourseBannerImageCourseId_QNAME, String.class, DeleteStaffInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TermVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard", name = "return", scope = LoadTermResponse.class)
    public JAXBElement<TermVO> createLoadTermResponseReturn(TermVO value) {
        return new JAXBElement<>(_UpdateCourseResponseReturn_QNAME, TermVO.class, LoadTermResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "descriptionType", scope = GroupVO.class)
    public JAXBElement<String> createGroupVODescriptionType(String value) {
        return new JAXBElement<>(_TermVODescriptionType_QNAME, String.class, GroupVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "courseId", scope = GroupVO.class)
    public JAXBElement<String> createGroupVOCourseId(String value) {
        return new JAXBElement<>(_CategoryMembershipVOCourseId_QNAME, String.class, GroupVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "id", scope = GroupVO.class)
    public JAXBElement<String> createGroupVOId(String value) {
        return new JAXBElement<>(_TermVOId_QNAME, String.class, GroupVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "title", scope = GroupVO.class)
    public JAXBElement<String> createGroupVOTitle(String value) {
        return new JAXBElement<>(_CategoryVOTitle_QNAME, String.class, GroupVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "description", scope = GroupVO.class)
    public JAXBElement<String> createGroupVODescription(String value) {
        return new JAXBElement<>(_TermVODescription_QNAME, String.class, GroupVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "dataSourceId", scope = ClassificationVO.class)
    public JAXBElement<String> createClassificationVODataSourceId(String value) {
        return new JAXBElement<>(_CategoryVODataSourceId_QNAME, String.class, ClassificationVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "parentId", scope = ClassificationVO.class)
    public JAXBElement<String> createClassificationVOParentId(String value) {
        return new JAXBElement<>(_CategoryVOParentId_QNAME, String.class, ClassificationVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "id", scope = ClassificationVO.class)
    public JAXBElement<String> createClassificationVOId(String value) {
        return new JAXBElement<>(_TermVOId_QNAME, String.class, ClassificationVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "title", scope = ClassificationVO.class)
    public JAXBElement<String> createClassificationVOTitle(String value) {
        return new JAXBElement<>(_CategoryVOTitle_QNAME, String.class, ClassificationVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://course.ws.blackboard/xsd", name = "batchUid", scope = ClassificationVO.class)
    public JAXBElement<String> createClassificationVOBatchUid(String value) {
        return new JAXBElement<>(_CategoryVOBatchUid_QNAME, String.class, ClassificationVO.class, value);
    }

}
