/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.registry.FefuRegElementCompetenceAdd;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.FefuCompetence2RegistryElementRel;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

import java.util.Set;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setCompetenceModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                        .fromEntity(FefuCompetence2RegistryElementRel.class, "rel")
                        .column(DQLExpressions.property(FefuCompetence2RegistryElementRel.fefuCompetence().id().fromAlias("rel")))
                        .where(DQLExpressions.eq(DQLExpressions.property(FefuCompetence2RegistryElementRel.registryElement().id().fromAlias("rel")), DQLExpressions.value(model.getId())));

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(FefuCompetence.class, "fc")
                        .where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(FefuCompetence.title().fromAlias("fc"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))))
                        .where(DQLExpressions.notIn(DQLExpressions.property(FefuCompetence.id().fromAlias("fc")), subBuilder.buildQuery()));

                if (set != null)
                {
                    builder.where(DQLExpressions.in(DQLExpressions.property(FefuCompetence.id().fromAlias("fc")), set));
                }

                return new DQLListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                FefuCompetence competence = (FefuCompetence) value;

                return competence.getEppSkillGroup().getShortTitle() + ". " + competence.getTitle();
            }
        });
    }

    @Override
    public void update(Model model)
    {
        EppRegistryElement registryElement = get(model.getId());

        for (FefuCompetence competence : model.getPickedCompetences())
        {
            save(new FefuCompetence2RegistryElementRel(registryElement, competence));
        }

        super.update(model);
    }
}