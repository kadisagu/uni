/*$Id$*/
package ru.tandemservice.unifefu.component.orgunit.SessionSheetListTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.SessionSheetMarkBy;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;

import java.util.Date;

/**
 * @author DMITRY KNYAZEV
 * @since 07.09.2015
 */
public class Controller extends ru.tandemservice.unisession.component.orgunit.SessionSheetListTab.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        super.onRefreshComponent(component);
        final ru.tandemservice.unisession.component.orgunit.SessionSheetListTab.Model model = this.getModel(component);
        final DynamicListDataSource<SessionSheetDocument> dataSource = model.getDataSource();

        IFormatter formatter = o -> {
            if (o instanceof Long) {
                final SessionSheetMarkBy markBy = DataAccessServices.dao().get(SessionSheetMarkBy.class, SessionSheetMarkBy.sessionDocument().id(), (Long) o);
                if (markBy != null) {
                    return markBy.getPrincipalContext().getFio();
                }
            }
            return null;
        };
        dataSource.addColumn(new SimpleColumn("ФИО преподавателя", SessionSheetDocument.id()).setFormatter(formatter).setClickable(false).setOrderable(false));
    }

    @Override
    public void onClickSearch(IBusinessComponent component)
    {
        final ru.tandemservice.unisession.component.orgunit.SessionSheetListTab.Model model = this.getModel(component);
        final Date performDateFrom = model.getSettings().get("performDateFrom");
        final Date performDateTo = model.getSettings().get("performDateTo");
        final Date formedFrom = model.getSettings().get("formedFrom");
        final Date formedTo = model.getSettings().get("formedTo");

        //если нет «Дата сдачи» то и проверять нечего
        if (performDateFrom != null || performDateTo != null)
        {
            //«Дата сдачи с» есть, то проверяем
            if (performDateFrom != null)
            {
                //«Дата выдачи с» должна быть и должна быть не раньше «Дата сдачи с»
                if (formedFrom == null || formedFrom.before(performDateFrom))
                    throw new ApplicationException("«Дата сдачи» должна быть больше и равна чем «Дата выдачи»");
            }
            //«Дата сдачи по» есть, то проверяем
            if (performDateTo != null)
            {
                //«Дата выдачи по» должна быть и должна быть не позже «Дата сдачи по»
                if (formedTo == null || formedTo.after(performDateTo))
                    throw new ApplicationException("«Дата сдачи» должна быть больше и равна чем «Дата выдачи»");
            }
        }
        super.onClickSearch(component);
    }
}
