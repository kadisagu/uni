/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.membership;

import ru.tandemservice.unifefu.ws.blackboard.BBConstants;
import ru.tandemservice.unifefu.ws.blackboard.BBContextHelper;
import ru.tandemservice.unifefu.ws.blackboard.HeaderHandlerResolver;
import ru.tandemservice.unifefu.ws.blackboard.PortTypeInitializer;
import ru.tandemservice.unifefu.ws.blackboard.membership.gen.CourseMembershipWS;
import ru.tandemservice.unifefu.ws.blackboard.membership.gen.CourseMembershipWSPortType;
import ru.tandemservice.unifefu.ws.blackboard.membership.gen.ObjectFactory;

/**
 * @author Nikolay Fedorovskih
 * @since 16.03.2014
 */
public class BBMembershipInitializer extends PortTypeInitializer<CourseMembershipWSPortType, ObjectFactory>
{
    private CourseMembershipWS _ws;

    public BBMembershipInitializer(HeaderHandlerResolver headerHandlerResolver)
    {
        super(BBConstants.COURSE_MEMBERSHIP_WS, headerHandlerResolver);
        _ws = new CourseMembershipWS(BBContextHelper.getWSDL_resource_URL("CourseMembership.wsdl"));
        _ws.setHandlerResolver(headerHandlerResolver);
    }

    @Override
    protected CourseMembershipWSPortType initPortType()
    {
        return _ws.getCourseMembershipWSSOAP11PortHttp();
    }

    @Override
    protected ObjectFactory initObjectFactory()
    {
        return new ObjectFactory();
    }
}