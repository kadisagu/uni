/* $Id: MS_unifefu_2x5x2_2to3.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.*;

/**
 * @author Denis Perminov
 * @since 22.04.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x2_2to3 extends IndependentMigrationScript
{
    private static final DateFormatter DATE_FORMATTER_DAY_AND_TIME_WITH_SECONDS = new DateFormatter("dd.MM.yyyy HH:mm:ss");

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement st = tool.getConnection().createStatement();
        Statement firSt = tool.getConnection().createStatement();
        Statement laSt = tool.getConnection().createStatement();

        // ищем многократные попытки проведения приказа, опираемся на directumOrderId
        PreparedStatement fdlUpdate = tool.prepareStatement("update FEFUDIRECTUMLOG_T set OPERATIONRESULT_P=?, DIRECTUMTASKID_P=? where ID=?");
        PreparedStatement fdlDelete = tool.prepareStatement("delete from FEFUDIRECTUMLOG_T where DIRECTUMORDERID_P=? and ID!=? and ORDER_ID is null and ENRORDER_ID is null and ORDEREXT_ID is null and OPERATIONTYPE_P like 'Проведение%' and OPERATIONRESULT_P like 'Не найден%'");

        ResultSet rs = st.executeQuery("select fdl.directumorderid_p,count(fdl.directumorderid_p) as cnt" +
                " from fefudirectumlog_t as fdl" +
                " where fdl.directumorderid_p is not null and fdl.order_id is null and fdl.enrorder_id is null and fdl.orderext_id is null" +
                " and fdl.operationtype_p like 'Проведение%' and fdl.operationresult_p like 'Не найден%'" +
                " group by fdl.directumorderid_p" +
                " order by fdl.directumorderid_p desc");
        int i = 0;
        while (rs.next())
        {
            String directumOrderId = rs.getString(1);
            int commitTries = rs.getInt(2);
            if (commitTries > 1)
            {
                // первая попытка проведения приказа
                ResultSet firstTry = firSt.executeQuery("select top 1 fdl.operationdatetime_p,fdl.directumtaskid_p" +
                        " from fefudirectumlog_t as fdl" +
                        " where fdl.order_id is null and fdl.enrorder_id is null and fdl.orderext_id is null" +
                        " and fdl.operationtype_p like 'Проведение%' and fdl.operationresult_p like 'Не найден%'" +
                        " and fdl.directumorderid_p=" + directumOrderId +
                        " order by fdl.operationdatetime_p asc");
                firstTry.next();
                String firstDT = DATE_FORMATTER_DAY_AND_TIME_WITH_SECONDS.format(firstTry.getTimestamp(1));
                String firstTask = firstTry.getString(2);
                // последняя попытка проведения приказа
                ResultSet lastTry = laSt.executeQuery("select top 1 fdl.id,fdl.operationresult_p" +
                        " from fefudirectumlog_t as fdl" +
                        " where fdl.order_id is null and fdl.enrorder_id is null and fdl.orderext_id is null" +
                        " and fdl.operationtype_p like 'Проведение%' and fdl.operationresult_p like 'Не найден%'" +
                        " and fdl.directumorderid_p=" + directumOrderId +
                        " order by fdl.operationdatetime_p desc");
                lastTry.next();
                Long lastId = lastTry.getLong(1);
                String lastResult = lastTry.getString(2) + " Попыток " + Integer.toString(commitTries) + ", начиная с " + firstDT + ".";

                fdlUpdate.setString(1, lastResult); // OPERATIONRESULT_P=
                fdlUpdate.setString(2, firstTask);  // DIRECTUMTASKID_P=
                fdlUpdate.setLong(3, lastId);       // ID=
                fdlUpdate.addBatch();

                fdlDelete.setString(1, directumOrderId);    // DIRECTUMORDERID_P=
                fdlDelete.setLong(2, lastId);               // ID<>
                fdlDelete.addBatch();
            }

            if (++i % 256 == 0)
            {
                fdlUpdate.executeBatch();
                fdlDelete.executeBatch();
                System.out.println("Get 256 commitOrder queries.");
            }
        }
        fdlDelete.executeBatch();

        // ищем многократные попытки отправки приказа на доработку, опираемся на directumOrderId
        fdlDelete = tool.prepareStatement("delete from FEFUDIRECTUMLOG_T where DIRECTUMORDERID_P=? and ID!=? and ORDER_ID is null and ENRORDER_ID is null and ORDEREXT_ID is null and OPERATIONTYPE_P like 'Отправка%' and OPERATIONRESULT_P like 'Не найден%'");

        rs = st.executeQuery("select fdl.directumorderid_p,count(fdl.directumorderid_p) as cnt" +
                " from fefudirectumlog_t as fdl" +
                " where fdl.directumorderid_p is not null and fdl.order_id is null and fdl.enrorder_id is null and fdl.orderext_id is null" +
                " and fdl.operationtype_p like 'Отправка%' and fdl.operationresult_p like 'Не найден%'" +
                " group by fdl.directumorderid_p" +
                " order by fdl.directumorderid_p desc");
        while (rs.next())
        {
            String directumOrderId = rs.getString(1);
            int commitTries = rs.getInt(2);
            if (commitTries > 1)
            {
                // первая попытка отправки приказа на доработку
                ResultSet firstTry = firSt.executeQuery("select top 1 fdl.operationdatetime_p,fdl.directumtaskid_p" +
                        " from fefudirectumlog_t as fdl" +
                        " where fdl.order_id is null and fdl.enrorder_id is null and fdl.orderext_id is null" +
                        " and fdl.operationtype_p like 'Отправка%' and fdl.operationresult_p like 'Не найден%'" +
                        " and fdl.directumorderid_p=" + directumOrderId +
                        " order by fdl.operationdatetime_p asc");
                firstTry.next();
                String firstDT = DATE_FORMATTER_DAY_AND_TIME_WITH_SECONDS.format(firstTry.getDate(1));
                String firstTask = firstTry.getString(2);
                // последняя попытка отправки приказа на доработку
                ResultSet lastTry = laSt.executeQuery("select top 1 fdl.id,fdl.operationresult_p" +
                        " from fefudirectumlog_t as fdl" +
                        " where fdl.order_id is null and fdl.enrorder_id is null and fdl.orderext_id is null" +
                        " and fdl.operationtype_p like 'Отправка%' and fdl.operationresult_p like 'Не найден%'" +
                        " and fdl.directumorderid_p=" + directumOrderId +
                        " order by fdl.operationdatetime_p desc");
                lastTry.next();
                Long lastId = lastTry.getLong(1);
                String lastResult = lastTry.getString(2) + " Попыток " + Integer.toString(commitTries) + ", начиная с " + firstDT + ".";

                fdlUpdate.setString(1, lastResult); // OPERATIONRESULT_P=
                fdlUpdate.setString(2, firstTask);  // DIRECTUMTASKID_P=
                fdlUpdate.setLong(3, lastId);       // ID=
                fdlUpdate.addBatch();

                fdlDelete.setString(1, directumOrderId);    // DIRECTUMORDERID_P=
                fdlDelete.setLong(2, lastId);               // ID<>
                fdlDelete.addBatch();
            }

            if (++i % 256 == 0)
            {
                fdlUpdate.executeBatch();
                fdlDelete.executeBatch();
                System.out.println("Get 256 sendToFormation queries.");
            }
        }
        fdlDelete.executeBatch();

        // ищем многократные попытки отклонения приказа, опираемся на directumOrderId
        fdlDelete = tool.prepareStatement("delete from FEFUDIRECTUMLOG_T where DIRECTUMORDERID_P=? and ID!=? and ORDER_ID is null and ENRORDER_ID is null and ORDEREXT_ID is null and OPERATIONTYPE_P like 'Отклонение%' and OPERATIONRESULT_P like 'Не найден%'");

        rs = st.executeQuery("select fdl.directumorderid_p,count(fdl.directumorderid_p) as cnt" +
                " from fefudirectumlog_t as fdl" +
                " where fdl.directumorderid_p is not null and fdl.order_id is null and fdl.enrorder_id is null and fdl.orderext_id is null" +
                " and fdl.operationtype_p like 'Отклонение%' and fdl.operationresult_p like 'Не найден%'" +
                " group by fdl.directumorderid_p" +
                " order by fdl.directumorderid_p desc");
        while (rs.next())
        {
            String directumOrderId = rs.getString(1);
            int commitTries = rs.getInt(2);
            if (commitTries > 1)
            {
                // первая попытка отклонения приказа на доработку
                ResultSet firstTry = firSt.executeQuery("select top 1 fdl.operationdatetime_p,fdl.directumtaskid_p" +
                        " from fefudirectumlog_t as fdl" +
                        " where fdl.order_id is null and fdl.enrorder_id is null and fdl.orderext_id is null" +
                        " and fdl.operationtype_p like 'Отклонение%' and fdl.operationresult_p like 'Не найден%'" +
                        " and fdl.directumorderid_p=" + directumOrderId +
                        " order by fdl.operationdatetime_p asc");
                firstTry.next();
                String firstDT = DATE_FORMATTER_DAY_AND_TIME_WITH_SECONDS.format(firstTry.getDate(1));
                String firstTask = firstTry.getString(2);
                // последняя попытка отклонения приказа на доработку
                ResultSet lastTry = laSt.executeQuery("select top 1 fdl.id,fdl.operationresult_p" +
                        " from fefudirectumlog_t as fdl" +
                        " where fdl.order_id is null and fdl.enrorder_id is null and fdl.orderext_id is null" +
                        " and fdl.operationtype_p like 'Отклонение%' and fdl.operationresult_p like 'Не найден%'" +
                        " and fdl.directumorderid_p=" + directumOrderId +
                        " order by fdl.operationdatetime_p desc");
                lastTry.next();
                Long lastId = lastTry.getLong(1);
                String lastResult = lastTry.getString(2) + " Попыток " + Integer.toString(commitTries) + ", начиная с " + firstDT + ".";

                fdlUpdate.setString(1, lastResult); // OPERATIONRESULT_P=
                fdlUpdate.setString(2, firstTask);  // DIRECTUMTASKID_P=
                fdlUpdate.setLong(3, lastId);       // ID=
                fdlUpdate.addBatch();

                fdlDelete.setString(1, directumOrderId);    // DIRECTUMORDERID_P=
                fdlDelete.setLong(2, lastId);               // ID<>
                fdlDelete.addBatch();
            }

            if (++i % 256 == 0)
            {
                fdlUpdate.executeBatch();
                fdlDelete.executeBatch();
                System.out.println("Get 256 rejectOrder queries.");
            }
        }

        fdlUpdate.executeBatch();
        fdlDelete.executeBatch();
        System.out.println("Get last queries.");
    }
}
