/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.CompensationTypeType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class CompensationTypeTypeReactor extends SimpleCatalogEntityNewReactor<CompensationTypeType, CompensationType>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<CompensationType> getEntityClass()
    {
        return CompensationType.class;
    }

    @Override
    public Class<CompensationTypeType> getNSIEntityClass()
    {
        return CompensationTypeType.class;
    }

    @Override
    public CompensationTypeType getCatalogElementRetrieveRequestObject(String guid)
    {
        CompensationTypeType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createCompensationTypeType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public CompensationTypeType createEntity(CoreCollectionUtils.Pair<CompensationType, FefuNsiIds> entityPair)
    {
        CompensationTypeType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createCompensationTypeType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setCompensationTypeID(entityPair.getX().getCode());
        nsiEntity.setCompensationTypeName(entityPair.getX().getTitle());
        nsiEntity.setCompensationTypeNameShort(entityPair.getX().getShortTitle());
        nsiEntity.setCompensationTypeGroup(entityPair.getX().getGroup());
        return nsiEntity;
    }

    @Override
    public CompensationType createEntity(CompensationTypeType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getCompensationTypeName()) return null;

        CompensationType entity = new CompensationType();
        entity.setTitle(nsiEntity.getCompensationTypeName());
        if (null != StringUtils.trimToNull(nsiEntity.getCompensationTypeNameShort()))
            entity.setShortTitle(nsiEntity.getCompensationTypeNameShort());
        else entity.setShortTitle(nsiEntity.getCompensationTypeName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(CompensationType.class));
        entity.setGroup(nsiEntity.getCompensationTypeGroup());

        return entity;
    }

    @Override
    public CompensationType updatePossibleDuplicateFields(CompensationTypeType nsiEntity, CoreCollectionUtils.Pair<CompensationType, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), CompensationType.title().s()))
                entityPair.getX().setTitle(nsiEntity.getCompensationTypeName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), CompensationType.shortTitle().s()))
                entityPair.getX().setShortTitle(null != StringUtils.trimToNull(nsiEntity.getCompensationTypeNameShort()) ? nsiEntity.getCompensationTypeNameShort() : nsiEntity.getCompensationTypeName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), CompensationType.group().s()))
                entityPair.getX().setGroup(nsiEntity.getCompensationTypeGroup());
            return entityPair.getX();
        }
        return null;
    }

    @Override
    public CompensationTypeType updateNsiEntityFields(CompensationTypeType nsiEntity, CompensationType entity)
    {
        nsiEntity.setCompensationTypeID(entity.getCode());
        nsiEntity.setCompensationTypeName(entity.getTitle());
        nsiEntity.setCompensationTypeNameShort(entity.getShortTitle());
        nsiEntity.setCompensationTypeGroup(entity.getGroup());
        return nsiEntity;
    }
}