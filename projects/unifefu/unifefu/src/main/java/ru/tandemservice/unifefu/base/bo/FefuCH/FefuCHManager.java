/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuCH;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuCH.logic.FefuCHDao;
import ru.tandemservice.unifefu.base.bo.FefuCH.logic.IFefuCHDao;

/**
 * @author Ekaterina Zvereva
 * @since 13.03.2015
 */
@Configuration
public class FefuCHManager extends BusinessObjectManager
{
    public static FefuCHManager instance()
    {
        return instance(FefuCHManager.class);
    }

    @Bean
    public IFefuCHDao dao()
    {
        return new FefuCHDao();
    }
}