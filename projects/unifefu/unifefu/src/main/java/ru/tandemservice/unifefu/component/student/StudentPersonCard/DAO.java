/* $Id$ */
package ru.tandemservice.unifefu.component.student.StudentPersonCard;

import com.google.common.collect.Lists;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.component.student.StudentPersonCard.Model;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuThematicGroupStuExtractTypesCodes;
import ru.tandemservice.unifefu.utils.FefuOrderData;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 11/5/13
 */
public class DAO extends ru.tandemservice.uni.component.student.StudentPersonCard.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        StudentPersonCardPrintFactory printFactory = (StudentPersonCardPrintFactory) model.getPrintFactory();

        DQLSelectBuilder enrExtractBuilder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e");
        enrExtractBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", EnrollmentExtract.studentNew().id()), DQLExpressions.value(model.getStudent().getId())));

        List<EnrollmentExtract> extracts = enrExtractBuilder.createStatement(getSession()).list();
        if(!extracts.isEmpty()) printFactory.setEnrollmentExtract(extracts.get(0));

        DQLSelectBuilder benefitsBuilder = new DQLSelectBuilder().fromEntity(PersonBenefit.class, "b");
        benefitsBuilder.where(DQLExpressions.eq(DQLExpressions.property("b", PersonBenefit.person().id()), DQLExpressions.value(model.getStudent().getPerson().getId())));
        printFactory.setPersonBenefits(benefitsBuilder.createStatement(getSession()).<PersonBenefit>list());

        List<String> themGroupCodes = Lists.newArrayList(
                FefuThematicGroupStuExtractTypesCodes.MAIN_EDU_PROGRAMM,
                FefuThematicGroupStuExtractTypesCodes.PART_COM_SERV_INCENT_PENALT,
                FefuThematicGroupStuExtractTypesCodes.PRACTICE,
                FefuThematicGroupStuExtractTypesCodes.ORPHAN_PAYMENTS);
        DQLSelectBuilder extractsBuilder = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e");
        extractsBuilder.joinEntity("e", DQLJoinType.inner, FefuStudentExtractType2ThematicGroup.class, "tg",
                DQLExpressions.eq(DQLExpressions.property("e", AbstractStudentExtract.type().id()), DQLExpressions.property("tg", FefuStudentExtractType2ThematicGroup.type().id())));
        extractsBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", AbstractStudentExtract.entity().id()), DQLExpressions.value(model.getStudent().getId())));
        extractsBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", AbstractStudentExtract.paragraph().order().state().code()), DQLExpressions.value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)));
        extractsBuilder.where(DQLExpressions.in(DQLExpressions.property("tg", FefuStudentExtractType2ThematicGroup.thematicGroup().code()), themGroupCodes));


//        Map<Long, Map<String, List<AbstractStudentExtract>>> students2extractsMap = Maps.newHashMap();

        List<AbstractStudentExtract> mainEduProgrammExtracts = Lists.newArrayList();
        List<AbstractStudentExtract> partComExtracts = Lists.newArrayList();
        List<AbstractStudentExtract> practiceExtracts = Lists.newArrayList();
        List<AbstractStudentExtract> orphanPaymentsExtracts = Lists.newArrayList();

        for(Object[] extractWithT2G : extractsBuilder.createStatement(getSession()).<Object[]>list())
        {
            FefuStudentExtractType2ThematicGroup type2ThematicGroup = (FefuStudentExtractType2ThematicGroup) extractWithT2G[1];
            if(FefuThematicGroupStuExtractTypesCodes.MAIN_EDU_PROGRAMM.equals(type2ThematicGroup.getThematicGroup().getCode()))
                mainEduProgrammExtracts.add((AbstractStudentExtract) extractWithT2G[0]);
            else if(FefuThematicGroupStuExtractTypesCodes.PART_COM_SERV_INCENT_PENALT.equals(type2ThematicGroup.getThematicGroup().getCode()))
                partComExtracts.add((AbstractStudentExtract) extractWithT2G[0]);
            else if(FefuThematicGroupStuExtractTypesCodes.PRACTICE.equals(type2ThematicGroup.getThematicGroup().getCode()))
                practiceExtracts.add((AbstractStudentExtract) extractWithT2G[0]);
            else if(FefuThematicGroupStuExtractTypesCodes.ORPHAN_PAYMENTS.equals(type2ThematicGroup.getThematicGroup().getCode()))
                orphanPaymentsExtracts.add((AbstractStudentExtract) extractWithT2G[0]);
        }

        Collections.sort(mainEduProgrammExtracts, StudentPersonCardPrintFactory.ORDER_COMP);
        Collections.sort(partComExtracts, StudentPersonCardPrintFactory.ORDER_COMP);
        Collections.sort(practiceExtracts, StudentPersonCardPrintFactory.ORDER_COMP);
        Collections.sort(orphanPaymentsExtracts, StudentPersonCardPrintFactory.ORDER_COMP);

        List<Map<String, String>> mainEduProgrammextractsData = Lists.newArrayList();
        List<Map<String, String>> partComextractsData = Lists.newArrayList();
        List<Map<String, String>> practiceExtractsData = Lists.newArrayList();
        List<Map<String, String>> orphanPaymentsExtractsData = Lists.newArrayList();

        for(AbstractStudentExtract extract : mainEduProgrammExtracts)
        {
            mainEduProgrammextractsData.add(FefuOrderData.getData(extract));
        }
        for(AbstractStudentExtract extract : partComExtracts)
        {
            partComextractsData.add(FefuOrderData.getData(extract));
        }
        for(AbstractStudentExtract extract : practiceExtracts)
        {
            practiceExtractsData.add(FefuOrderData.getData(extract));
        }
        for(AbstractStudentExtract extract : orphanPaymentsExtracts)
        {
            orphanPaymentsExtractsData.add(FefuOrderData.getData(extract));
        }

        printFactory.setMainEduProgrammExtractsData(mainEduProgrammextractsData);
        printFactory.setPartComExtractsData(partComextractsData);
        printFactory.setPracticeExtractsData(practiceExtractsData);
        printFactory.setOrphanPaymentsData(orphanPaymentsExtractsData);
    }
}
