package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unifefu.entity.DiplomaIssuanceFefuExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Факт выдачи документа об обучении (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DiplomaIssuanceFefuExtGen extends EntityBase
 implements INaturalIdentifiable<DiplomaIssuanceFefuExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.DiplomaIssuanceFefuExt";
    public static final String ENTITY_NAME = "diplomaIssuanceFefuExt";
    public static final int VERSION_HASH = -32680532;
    private static IEntityMeta ENTITY_META;

    public static final String L_DIPLOMA_ISSUANCE = "diplomaIssuance";
    public static final String P_REGISTRATION_NUMBER_BOOK = "registrationNumberBook";

    private DiplomaIssuance _diplomaIssuance;     // Факт выдачи документа об обучении
    private String _registrationNumberBook;     // Индекс книги регистрации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Факт выдачи документа об обучении. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public DiplomaIssuance getDiplomaIssuance()
    {
        return _diplomaIssuance;
    }

    /**
     * @param diplomaIssuance Факт выдачи документа об обучении. Свойство не может быть null и должно быть уникальным.
     */
    public void setDiplomaIssuance(DiplomaIssuance diplomaIssuance)
    {
        dirty(_diplomaIssuance, diplomaIssuance);
        _diplomaIssuance = diplomaIssuance;
    }

    /**
     * @return Индекс книги регистрации. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRegistrationNumberBook()
    {
        return _registrationNumberBook;
    }

    /**
     * @param registrationNumberBook Индекс книги регистрации. Свойство не может быть null.
     */
    public void setRegistrationNumberBook(String registrationNumberBook)
    {
        dirty(_registrationNumberBook, registrationNumberBook);
        _registrationNumberBook = registrationNumberBook;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DiplomaIssuanceFefuExtGen)
        {
            if (withNaturalIdProperties)
            {
                setDiplomaIssuance(((DiplomaIssuanceFefuExt)another).getDiplomaIssuance());
            }
            setRegistrationNumberBook(((DiplomaIssuanceFefuExt)another).getRegistrationNumberBook());
        }
    }

    public INaturalId<DiplomaIssuanceFefuExtGen> getNaturalId()
    {
        return new NaturalId(getDiplomaIssuance());
    }

    public static class NaturalId extends NaturalIdBase<DiplomaIssuanceFefuExtGen>
    {
        private static final String PROXY_NAME = "DiplomaIssuanceFefuExtNaturalProxy";

        private Long _diplomaIssuance;

        public NaturalId()
        {}

        public NaturalId(DiplomaIssuance diplomaIssuance)
        {
            _diplomaIssuance = ((IEntity) diplomaIssuance).getId();
        }

        public Long getDiplomaIssuance()
        {
            return _diplomaIssuance;
        }

        public void setDiplomaIssuance(Long diplomaIssuance)
        {
            _diplomaIssuance = diplomaIssuance;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof DiplomaIssuanceFefuExtGen.NaturalId) ) return false;

            DiplomaIssuanceFefuExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getDiplomaIssuance(), that.getDiplomaIssuance()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDiplomaIssuance());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDiplomaIssuance());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DiplomaIssuanceFefuExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DiplomaIssuanceFefuExt.class;
        }

        public T newInstance()
        {
            return (T) new DiplomaIssuanceFefuExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "diplomaIssuance":
                    return obj.getDiplomaIssuance();
                case "registrationNumberBook":
                    return obj.getRegistrationNumberBook();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "diplomaIssuance":
                    obj.setDiplomaIssuance((DiplomaIssuance) value);
                    return;
                case "registrationNumberBook":
                    obj.setRegistrationNumberBook((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "diplomaIssuance":
                        return true;
                case "registrationNumberBook":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "diplomaIssuance":
                    return true;
                case "registrationNumberBook":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "diplomaIssuance":
                    return DiplomaIssuance.class;
                case "registrationNumberBook":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DiplomaIssuanceFefuExt> _dslPath = new Path<DiplomaIssuanceFefuExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DiplomaIssuanceFefuExt");
    }
            

    /**
     * @return Факт выдачи документа об обучении. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.DiplomaIssuanceFefuExt#getDiplomaIssuance()
     */
    public static DiplomaIssuance.Path<DiplomaIssuance> diplomaIssuance()
    {
        return _dslPath.diplomaIssuance();
    }

    /**
     * @return Индекс книги регистрации. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.DiplomaIssuanceFefuExt#getRegistrationNumberBook()
     */
    public static PropertyPath<String> registrationNumberBook()
    {
        return _dslPath.registrationNumberBook();
    }

    public static class Path<E extends DiplomaIssuanceFefuExt> extends EntityPath<E>
    {
        private DiplomaIssuance.Path<DiplomaIssuance> _diplomaIssuance;
        private PropertyPath<String> _registrationNumberBook;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Факт выдачи документа об обучении. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.DiplomaIssuanceFefuExt#getDiplomaIssuance()
     */
        public DiplomaIssuance.Path<DiplomaIssuance> diplomaIssuance()
        {
            if(_diplomaIssuance == null )
                _diplomaIssuance = new DiplomaIssuance.Path<DiplomaIssuance>(L_DIPLOMA_ISSUANCE, this);
            return _diplomaIssuance;
        }

    /**
     * @return Индекс книги регистрации. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.DiplomaIssuanceFefuExt#getRegistrationNumberBook()
     */
        public PropertyPath<String> registrationNumberBook()
        {
            if(_registrationNumberBook == null )
                _registrationNumberBook = new PropertyPath<String>(DiplomaIssuanceFefuExtGen.P_REGISTRATION_NUMBER_BOOK, this);
            return _registrationNumberBook;
        }

        public Class getEntityClass()
        {
            return DiplomaIssuanceFefuExt.class;
        }

        public String getEntityName()
        {
            return "diplomaIssuanceFefuExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
