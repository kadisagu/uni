/* $Id$ */
package ru.tandemservice.unifefu.edustd.ext.EppEduStandard.ui.List;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unifefu.entity.catalog.FefuEduStandartGeneration;

/**
 * @author Ekaterina Zvereva
 * @since 28.05.2015
 */
public class EppEduStandardListExtUI extends UIAddon
{
    public static final String CUSTOM_EDU_STANDARD = "customEduStandard";
    public static final Long YES = 0L;

    private ISelectModel _customListModel = new LazySimpleSelectModel<>(ImmutableList.of(new IdentifiableWrapper(YES, "Да")));
    private ISelectModel _generationListModel = new LazySimpleSelectModel<>(FefuEduStandartGeneration.class, FefuEduStandartGeneration.P_TITLE);

    public EppEduStandardListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public ISelectModel getCustomListModel()
    {
        return _customListModel;
    }

    public void setCustomListModel(ISelectModel customListModel)
    {
        this._customListModel = customListModel;
    }

    public ISelectModel getGenerationListModel()
    {
        return _generationListModel;
    }

    public void setGenerationListModel(ISelectModel generationListModel)
    {
        _generationListModel = generationListModel;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(CUSTOM_EDU_STANDARD, getPresenter().getSettings().get(CUSTOM_EDU_STANDARD));
    }
}