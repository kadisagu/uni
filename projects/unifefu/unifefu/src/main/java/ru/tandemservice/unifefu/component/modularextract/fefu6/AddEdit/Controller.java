/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu6.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuChangeFioStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 03.09.2012
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuChangeFioStuExtract, IDAO, Model>
{
    public void onRefresh(IBusinessComponent component)
    {
        FefuChangeFioStuExtract extract = getModel(component).getExtract();
        IdentityCardType cardType = extract.getCardType();
        if (cardType != null && cardType.isSeriaRequired() && isNeedChange(extract.getCardSeria(), cardType.getMaxSeriaLength()))
        {
            extract.setCardSeria(getDefaultValue(cardType.getMaxSeriaLength()));
        }
        if (cardType != null && cardType.isNumberRequired() && isNeedChange(extract.getCardNumber(), cardType.getMaxNumberLength()))
        {
            extract.setCardNumber(getDefaultValue(cardType.getMaxNumberLength()));
        }
        if (cardType == null && StringUtils.containsOnly(extract.getCardNumber(), "0"))
        {
            extract.setCardNumber(null);
        }
        if (null != cardType && !cardType.isShowSeria())
        {
            extract.setCardSeria(null);
        }
    }

    private static boolean isNeedChange(String value, int maxLength)
    {
        return StringUtils.isEmpty(value) || StringUtils.containsOnly(value, "0") || value.length() > maxLength;
    }

    private static String getDefaultValue(int maxLength)
    {
        return String.format("%" + maxLength + "s", "").replaceAll(" ", "0");
    }
}