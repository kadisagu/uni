/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard;

/**
 * Интерфейс для запросов к веб-сервисам Blackboard. Рекомендуется делать в виде анонимных классов.
 * В методе invoke реализуется сам запрос. Если вес-сервис вернет ошибку, что сессия устарела,
 * то сессия должна будет переинициализирована а метод выполнен повторно.
 *
 * @param <ResponseType>  тип ответа вебсервиса
 * @param <ObjectFactory> тип фабрики объектов для конкретного веб-сервиса
 * @param <WSPortType>    PortType конкретного вебсервиса. Например ContextWSPortType
 * @author Nikolay Fedorovskih
 * @since 05.03.2014
 */
@FunctionalInterface
public interface IBBRequest<WSPortType, ObjectFactory, ResponseType>
{
    /**
     * В методе реализуется сам запрос. Если вес-сервис вернет ошибку, что сессия устарела,
     * то сессия должна будет переинициализирована а метод выполнен повторно.
     * Поэтому, внимание (sic!), не пихайте логику внутрь метода. Тут должно быть только формирование запроса.
     * Всю подготовку данных и обработку ответа следует вынести наружу.
     *
     * @param portType веб-порт, через который отправляются запросы
     * @param of       фабрика объектов нужного вебсервиса
     * @return результат нужного типа
     */
    ResponseType invoke(WSPortType portType, ObjectFactory of);
}