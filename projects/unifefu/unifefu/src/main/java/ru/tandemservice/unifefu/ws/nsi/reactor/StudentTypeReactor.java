/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.NsiDatagramUtil;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.datagram.*;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.FefuNsiIdWrapper;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.INsiEntityUtil;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.NsiEntityUtilFactory;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 27.08.2013
 */
public class StudentTypeReactor extends SimpleCatalogEntityReactor<StudentType, Student>
{
    private Map<String, CourseType> coursesMap = new HashMap<>();
    private Map<String, StudentCategoryType> categoriesMap = new HashMap<>();
    private Map<String, StudentStatusType> studentStatusMap = new HashMap<>();
    private Map<String, CompensationTypeType> compensationTypesMap = new HashMap<>();
    private Map<String, AcademicGroupType> academicGroupsMap = new HashMap<>();
    private Map<String, EducationalProgramType> eduProgramsMap = new HashMap<>();
    private Map<String, HumanType> humansMap = new HashMap<>();

    private INsiEntityUtil UTIL;

    public INsiEntityUtil getUtil()
    {
        if (null == UTIL) UTIL = NsiEntityUtilFactory.getUtil(Student.class);
        return UTIL;
    }

    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<Student> getEntityClass()
    {
        return Student.class;
    }

    @Override
    public Class<StudentType> getNSIEntityClass()
    {
        return StudentType.class;
    }

    @Override
    public String getGUID(StudentType nsiEntity)
    {
        return nsiEntity.getID();
    }

    @Override
    public StudentType getCatalogElementRetrieveRequestObject(String guid)
    {
        StudentType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createStudentType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    protected Map<String, Student> getEntityMap(Class<Student> entityClass)
    {
        return getEntityMap(entityClass, null);
    }

    @Override
    protected Map<String, Student> getEntityMap(final Class<Student> entityClass, Set<Long> entityIds)
    {
        Map<String, Student> result = new HashMap<>();
        final List<Student> entityList = new ArrayList<>();

        if (null == entityIds)
        {
            entityList.addAll(DataAccessServices.dao().<Student>getList(
                    new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
                    //.where(eq(property(Student.id().fromAlias("e")), value(1415886031576115739l)))

            ));
        } else
        {
            BatchUtils.execute(entityIds, 100, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> entityIdsPortion)
                {
                    entityList.addAll(DataAccessServices.dao().<Student>getList(
                            new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
                                    .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                    ));
                }
            });
        }

        for (Student item : entityList)
        {
            result.put(item.getId().toString(), item);
        }

        return result;
    }

    @Override
    protected void prepareRelatedObjectsMap()
    {
        List<FefuNsiIds> courseIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(Course.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : courseIdsList)
        {
            CourseType courseType = NsiReactorUtils.NSI_OBJECT_FACTORY.createCourseType();
            courseType.setID(nsiId.getGuid());
            coursesMap.put(nsiId.getGuid(), courseType);
            coursesMap.put(String.valueOf(nsiId.getEntityId()), courseType);
        }


        List<FefuNsiIds> studCategoryIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(StudentCategory.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : studCategoryIdsList)
        {
            StudentCategoryType studentCategoryType = NsiReactorUtils.NSI_OBJECT_FACTORY.createStudentCategoryType();
            studentCategoryType.setID(nsiId.getGuid());
            categoriesMap.put(nsiId.getGuid(), studentCategoryType);
            categoriesMap.put(String.valueOf(nsiId.getEntityId()), studentCategoryType);
        }


        List<FefuNsiIds> studStatusIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(StudentStatus.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : studStatusIdsList)
        {
            StudentStatusType statusType = NsiReactorUtils.NSI_OBJECT_FACTORY.createStudentStatusType();
            statusType.setID(nsiId.getGuid());
            studentStatusMap.put(nsiId.getGuid(), statusType);
            studentStatusMap.put(String.valueOf(nsiId.getEntityId()), statusType);
        }


        List<FefuNsiIds> compTypeIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(CompensationType.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : compTypeIdsList)
        {
            CompensationTypeType compType = NsiReactorUtils.NSI_OBJECT_FACTORY.createCompensationTypeType();
            compType.setID(nsiId.getGuid());
            compensationTypesMap.put(nsiId.getGuid(), compType);
            compensationTypesMap.put(String.valueOf(nsiId.getEntityId()), compType);
        }


        List<FefuNsiIds> academicGroupIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(Group.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : academicGroupIdsList)
        {
            AcademicGroupType groupType = NsiReactorUtils.NSI_OBJECT_FACTORY.createAcademicGroupType();
            groupType.setID(nsiId.getGuid());
            academicGroupsMap.put(nsiId.getGuid(), groupType);
            academicGroupsMap.put(String.valueOf(nsiId.getEntityId()), groupType);
        }


        List<FefuNsiIds> eduProgramIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(EducationOrgUnit.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : eduProgramIdsList)
        {
            EducationalProgramType programType = NsiReactorUtils.NSI_OBJECT_FACTORY.createEducationalProgramType();
            programType.setID(nsiId.getGuid());
            eduProgramsMap.put(nsiId.getGuid(), programType);
            eduProgramsMap.put(String.valueOf(nsiId.getEntityId()), programType);
        }


        List<FefuNsiIds> personIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(Person.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : personIdsList)
        {
            HumanType human = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanType();
            human.setID(nsiId.getGuid());
            humansMap.put(nsiId.getGuid(), human);
            humansMap.put(String.valueOf(nsiId.getEntityId()), human);
        }
    }

    private class NsiCatalogsStore
    {
        Map<String, CoreCollectionUtils.Pair<EducationOrgUnit, FefuNsiIds>> _guidToEduOuWithIdPairMap;
        Map<String, CoreCollectionUtils.Pair<Group, FefuNsiIds>> _guidToGroupWithIdPairMap;
        Map<String, CoreCollectionUtils.Pair<Person, FefuNsiIds>> _guidToPersonWithIdPairMap;

        private Map<String, CoreCollectionUtils.Pair<EducationOrgUnit, FefuNsiIds>> getGuidToEduOuWithIdPairMap()
        {
            return _guidToEduOuWithIdPairMap;
        }

        private void setGuidToEduOuWithIdPairMap(Map<String, CoreCollectionUtils.Pair<EducationOrgUnit, FefuNsiIds>> guidToEduOuWithIdPairMap)
        {
            _guidToEduOuWithIdPairMap = guidToEduOuWithIdPairMap;
        }

        private Map<String, CoreCollectionUtils.Pair<Group, FefuNsiIds>> getGuidToGroupWithIdPairMap()
        {
            return _guidToGroupWithIdPairMap;
        }

        private void setGuidToGroupWithIdPairMap(Map<String, CoreCollectionUtils.Pair<Group, FefuNsiIds>> guidToGroupWithIdPairMap)
        {
            _guidToGroupWithIdPairMap = guidToGroupWithIdPairMap;
        }

        private Map<String, CoreCollectionUtils.Pair<Person, FefuNsiIds>> getGuidToPersonWithIdPairMap()
        {
            return _guidToPersonWithIdPairMap;
        }

        private void setGuidToPersonWithIdPairMap(Map<String, CoreCollectionUtils.Pair<Person, FefuNsiIds>> guidToPersonWithIdPairMap)
        {
            _guidToPersonWithIdPairMap = guidToPersonWithIdPairMap;
        }
    }

    private NsiCatalogsStore prepareCatalogStore()
    {
        // Подготавливаем мап категорий студентов.
        // Ключ - GUID, ID, название категории студента в верхнем регистре, и/или код элемента справочника. Значение - пара: Категория студента и Идентификатор НСИ.
        NsiObjectsHolder.getStudentCategoryMap();

        // Подготавливаем мап статусов студентов.
        // Ключ - GUID, ID, название статуса студента в верхнем регистре, и/или код элемента справочника. Значение - пара: Статус студента и Идентификатор НСИ.
        NsiObjectsHolder.getStudentStatusMap();

        // Подготавливаем мап видов возмещения затрат на обучение.
        // Ключ - GUID, ID, название вида возмещения затрат в верхнем регистре, и/или код элемента справочника. Значение - пара: Вид возмещения затрат и Идентификатор НСИ.
        NsiObjectsHolder.getCompensationTypeMap();

        // Подготавливаем мап курсов.
        // Ключ - GUID, ID, название курса в верхнем регистре, и/или код элемента справочника. Значение - пара: Курс и Идентификатор НСИ.
        NsiObjectsHolder.getCourseMap();

        return new NsiCatalogsStore(); // TODO Далее нужно инициализировать его значениями
    }

    @Override
    public Student getPossibleDuplicate(StudentType nsiEntity, Map<String, FefuNsiIds> nsiIdsMap, Map<String, Student> similarEntityMap)
    {
        if (null == nsiEntity.getID()) return null;

        // Проверяем, есть ли в базе аналогичные переданной сущности
        Student possibleDuplicate = null;

        if (nsiIdsMap.containsKey(nsiEntity.getID()))
        {
            possibleDuplicate = similarEntityMap.get(String.valueOf(nsiIdsMap.get(nsiEntity.getID()).getEntityId()));
        }

        return possibleDuplicate;
    }

    @Override
    public boolean isAnythingChanged(StudentType nsiEntity, Student entity, Map<String, Student> similarEntityMap)
    {
        if (null == nsiEntity.getID()) return false;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentPersonalNumber(), entity.getPerNumber(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentBookN(), entity.getBookNumber(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentEntranceYear(), String.valueOf(entity.getEntranceYear()), isNsiMasterSystem()))
                return true;
            /*if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentAnnotation(), entity.getComment(), isNsiMasterSystem()))
                return true;*/
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudenIndividualTraining(), entity.isPersonalEducation() ? "1" : "0", isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentTarget(), entity.isTargetAdmission() ? "1" : "0", isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentEconomicGroup(), entity.isThriftyGroup() ? "1" : "0", isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentAgreementN(), entity.getContractNumber(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentVKRTopic(), entity.getFinalQualifyingWorkTheme(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentPracticePlacement(), entity.getPracticePlace(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentJobPlacement(), entity.getEmployment(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentArhive(), entity.isArchival() ? "1" : "0", isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentYearEnd(), null != entity.getFinishYear() ? String.valueOf(entity.getFinishYear()) : null, isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentPrivacyN(), entity.getPersonalFileNumber(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentArhivePrivacyN(), entity.getPersonalArchivalFileNumber(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentComment(), entity.getComment(), isNsiMasterSystem()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(nsiEntity.getStudentArhiveDate(), NsiDatagramUtil.formatDate(entity.getArchivingDate()), isNsiMasterSystem()))
                return true;

            StudentCategoryType studentCategory = categoriesMap.get(entity.getStudentCategory().getId().toString());
            if (null != studentCategory && (null == nsiEntity.getStudentCategoryID() || !studentCategory.getID().equals(nsiEntity.getStudentCategoryID().getStudentCategory().getID())))
                return true;

            StudentStatusType studentStatus = studentStatusMap.get(entity.getStatus().getId().toString());
            if (null != studentStatus && (null == nsiEntity.getStudentStatusID() || !studentStatus.getID().equals(nsiEntity.getStudentStatusID().getStudentStatus().getID())))
                return true;

            CompensationTypeType compensationType = compensationTypesMap.get(entity.getCompensationType().getId().toString());
            if (null != compensationType && (null == nsiEntity.getCompensationTypeID() || !compensationType.getID().equals(nsiEntity.getCompensationTypeID().getCompensationType().getID())))
                return true;

            CourseType courseType = coursesMap.get(entity.getCourse().getId().toString());
            if (null != courseType && (null == nsiEntity.getCourseID() || !courseType.getID().equals(nsiEntity.getCourseID().getCourse().getID())))
                return true;

            EducationalProgramType eduProgramType = eduProgramsMap.get(entity.getEducationOrgUnit().getId().toString());
            if (null != eduProgramType && (null == nsiEntity.getEducationalProgramID() || !eduProgramType.getID().equals(nsiEntity.getEducationalProgramID().getEducationalProgram().getID())))
                return true;

            AcademicGroupType groupType = null != entity.getGroup() ? academicGroupsMap.get(entity.getGroup().getId().toString()) : null;
            if (null != groupType && (null == nsiEntity.getAcademicGroupID() || !groupType.getID().equals(nsiEntity.getAcademicGroupID().getAcademicGroup().getID())))
                return true;

            HumanType humanType = humansMap.get(entity.getPerson().getId().toString());
            if (null != humanType && (null == nsiEntity.getHumanID() || !humanType.getID().equals(nsiEntity.getHumanID().getHuman().getID())))
                return true;
        }

        return false;
    }

    @Override
    public StudentType createEntity(Student entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        StudentType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createStudentType();
        nsiEntity.setID(getGUID(entity, nsiIdsMap));
        nsiEntity.setStudentPersonalNumber(entity.getPerNumber());
        nsiEntity.setStudentBookN(entity.getBookNumber());
        nsiEntity.setStudentEntranceYear(String.valueOf(entity.getEntranceYear()));
        /*nsiEntity.setStudentAnnotation(entity.getComment());*/
        nsiEntity.setStudenIndividualTraining(entity.isPersonalEducation() ? "1" : "0");
        nsiEntity.setStudentTarget(entity.isTargetAdmission() ? "1" : "0");
        nsiEntity.setStudentEconomicGroup(entity.isThriftyGroup() ? "1" : "0");
        nsiEntity.setStudentAgreementN(entity.getContractNumber());
        nsiEntity.setStudentVKRTopic(entity.getFinalQualifyingWorkTheme());
        nsiEntity.setStudentPracticePlacement(entity.getPracticePlace());
        nsiEntity.setStudentJobPlacement(entity.getEmployment());
        nsiEntity.setStudentArhive(entity.isArchival() ? "1" : "0");
        nsiEntity.setStudentYearEnd(null != entity.getFinishYear() ? String.valueOf(entity.getFinishYear()) : null);
        nsiEntity.setStudentPrivacyN(entity.getPersonalFileNumber());
        nsiEntity.setStudentArhivePrivacyN(entity.getPersonalArchivalFileNumber());
        nsiEntity.setStudentComment(entity.getComment());
        nsiEntity.setStudentArhiveDate(NsiDatagramUtil.formatDate(entity.getArchivingDate()));

        StudentCategoryType studentCategory = categoriesMap.get(entity.getStudentCategory().getId().toString());
        if (null != studentCategory)
        {
            StudentType.StudentCategoryID categoryID = new StudentType.StudentCategoryID();
            categoryID.setStudentCategory(studentCategory);
            nsiEntity.setStudentCategoryID(categoryID);
        }

        StudentStatusType studentStatus = studentStatusMap.get(entity.getStatus().getId().toString());
        if (null != studentStatus)
        {
            StudentType.StudentStatusID statusID = new StudentType.StudentStatusID();
            statusID.setStudentStatus(studentStatus);
            nsiEntity.setStudentStatusID(statusID);
        }

        CompensationTypeType compensationType = compensationTypesMap.get(entity.getCompensationType().getId().toString());
        if (null != compensationType)
        {
            StudentType.CompensationTypeID compensationTypeID = new StudentType.CompensationTypeID();
            compensationTypeID.setCompensationType(compensationType);
            nsiEntity.setCompensationTypeID(compensationTypeID);
        }

        CourseType courseType = coursesMap.get(entity.getCourse().getId().toString());
        if (null != courseType)
        {
            StudentType.CourseID courseID = new StudentType.CourseID();
            courseID.setCourse(courseType);
            nsiEntity.setCourseID(courseID);
        }

        EducationalProgramType eduProgramType = eduProgramsMap.get(entity.getEducationOrgUnit().getId().toString());
        if (null != eduProgramType)
        {
            StudentType.EducationalProgramID eduProgramID = new StudentType.EducationalProgramID();
            eduProgramID.setEducationalProgram(eduProgramType);
            nsiEntity.setEducationalProgramID(eduProgramID);
        }

        if (null != entity.getGroup())
        {
            AcademicGroupType groupType = academicGroupsMap.get(entity.getGroup().getId().toString());
            if (null != groupType)
            {
                StudentType.AcademicGroupID groupID = new StudentType.AcademicGroupID();
                groupID.setAcademicGroup(groupType);
                nsiEntity.setAcademicGroupID(groupID);
            }
        }

        HumanType humanType = humansMap.get(entity.getPerson().getId().toString());
        if (null != humanType)
        {
            StudentType.HumanID humanID = new StudentType.HumanID();
            humanID.setHuman(humanType);
            nsiEntity.setHumanID(humanID);
        }

        return nsiEntity;
    }

    @Override
    public Student createEntity(StudentType nsiEntity, Map<String, Student> similarEntityMap)
    {
        return null;
        //TODO: Как создавать у нас персон по данным НСИ?
    }

    @Override
    public Student updatePossibleDuplicateFields(StudentType nsiEntity, Student entity, Map<String, Student> similarEntityMap)
    {
        return null;
    }

    @Override
    public StudentType updateNsiEntityFields(StudentType nsiEntity, Student entity)
    {
        nsiEntity.setStudentPersonalNumber(entity.getPerNumber());
        nsiEntity.setStudentBookN(entity.getBookNumber());
        nsiEntity.setStudentEntranceYear(String.valueOf(entity.getEntranceYear()));
        /*nsiEntity.setStudentAnnotation(entity.getComment());*/
        nsiEntity.setStudenIndividualTraining(entity.isPersonalEducation() ? "1" : "0");
        nsiEntity.setStudentTarget(entity.isTargetAdmission() ? "1" : "0");
        nsiEntity.setStudentEconomicGroup(entity.isThriftyGroup() ? "1" : "0");
        nsiEntity.setStudentAgreementN(entity.getContractNumber());
        nsiEntity.setStudentVKRTopic(entity.getFinalQualifyingWorkTheme());
        nsiEntity.setStudentPracticePlacement(entity.getPracticePlace());
        nsiEntity.setStudentJobPlacement(entity.getEmployment());
        nsiEntity.setStudentArhive(entity.isArchival() ? "1" : "0");
        nsiEntity.setStudentYearEnd(null != entity.getFinishYear() ? String.valueOf(entity.getFinishYear()) : null);
        nsiEntity.setStudentPrivacyN(entity.getPersonalFileNumber());
        nsiEntity.setStudentArhivePrivacyN(entity.getPersonalArchivalFileNumber());
        nsiEntity.setStudentComment(entity.getComment());
        nsiEntity.setStudentArhiveDate(NsiDatagramUtil.formatDate(entity.getArchivingDate()));

        StudentCategoryType studentCategory = categoriesMap.get(entity.getStudentCategory().getId().toString());
        if (null != studentCategory)
        {
            StudentType.StudentCategoryID categoryID = new StudentType.StudentCategoryID();
            categoryID.setStudentCategory(studentCategory);
            nsiEntity.setStudentCategoryID(categoryID);
        }

        StudentStatusType studentStatus = studentStatusMap.get(entity.getStatus().getId().toString());
        if (null != studentStatus)
        {
            StudentType.StudentStatusID statusID = new StudentType.StudentStatusID();
            statusID.setStudentStatus(studentStatus);
            nsiEntity.setStudentStatusID(statusID);
        }

        CompensationTypeType compensationType = compensationTypesMap.get(entity.getCompensationType().getId().toString());
        if (null != compensationType)
        {
            StudentType.CompensationTypeID compensationTypeID = new StudentType.CompensationTypeID();
            compensationTypeID.setCompensationType(compensationType);
            nsiEntity.setCompensationTypeID(compensationTypeID);
        }

        CourseType courseType = coursesMap.get(entity.getCourse().getId().toString());
        if (null != courseType)
        {
            StudentType.CourseID courseID = new StudentType.CourseID();
            courseID.setCourse(courseType);
            nsiEntity.setCourseID(courseID);
        }

        EducationalProgramType eduProgramType = eduProgramsMap.get(entity.getEducationOrgUnit().getId().toString());
        if (null != eduProgramType)
        {
            StudentType.EducationalProgramID eduProgramID = new StudentType.EducationalProgramID();
            eduProgramID.setEducationalProgram(eduProgramType);
            nsiEntity.setEducationalProgramID(eduProgramID);
        }

        AcademicGroupType groupType = null != entity.getGroup() ? academicGroupsMap.get(entity.getGroup().getId().toString()) : null;
        if (null != groupType)
        {
            StudentType.AcademicGroupID groupID = new StudentType.AcademicGroupID();
            groupID.setAcademicGroup(groupType);
            nsiEntity.setAcademicGroupID(groupID);
        }

        HumanType humanType = humansMap.get(entity.getPerson().getId().toString());
        if (null != humanType)
        {
            StudentType.HumanID humanID = new StudentType.HumanID();
            humanID.setHuman(humanType);
            nsiEntity.setHumanID(humanID);
        }

        return nsiEntity;
    }

    @Override
    public void synchronizeFullCatalogWithNsi()
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG HAVE STARTED ==========");

        // Формируем мап сущностей ОБ, где ключ - идентификатор сущности ОБ, либо Название (ITitled), или пользовательский код (IActiveCatalogItem)
        final Map<String, Student> entityMap = getEntityMap(getEntityClass());
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");
        // Формируем мап идентификаторов НСИ из числа сохраненных в ОБ, где ключ - идентификатор ОБ, либо GUID
        final Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMap(EntityRuntime.getMeta(getEntityClass()).getName());
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        // Если для синхронизации нужны дополнительные данные, то инициализируем их
        prepareRelatedObjectsMap();

        final Set<Long> alreadySynchronizedEntitySet = new HashSet<>(); // Контрольный сет, блокирующий возможность повторного добавления сущности в НСИ, если она обновляется.


        BatchUtils.execute(entityMap.values(), 200, new BatchUtils.Action<Student>()
        {
            @Override
            public void execute(final Collection<Student> entityPortion)
            {
                /*List<StudentType> entityTypesToRetrieve = new ArrayList<>();

                for (Student student : entityPortion)
                {
                    entityTypesToRetrieve.add(getCatalogElementRetrieveRequestObject(getGUID(student, nsiIdsMap)));
                }*/

                // Забираем порцию элементов справочника из НСИ
                List<StudentType> nsiEntityList = new ArrayList(); //NsiReactorUtils.retrieveObjectsFromNSIByElementsList(getNSIEntityClass(), entityTypesToRetrieve);
                final List<Object> nsiEntityToUpdate = new ArrayList<>(); // Сюда сваливаем все сущности НСИ, которые требуется обновить данными из ОБ

                // Проводим синхронизацию всех полученных элементов из НСИ с имеющимися в ОБ
                FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start local (OB) synchronization of elements for " + getEntityClass().getSimpleName() + " catalog ----------");
                for (StudentType nsiEntity : nsiEntityList)
                {
                    Student entity = getPossibleDuplicate(nsiEntity, nsiIdsMap, entityMap); // Вычисляем элемент ОБ, соответствующий элементу НСИ (по GUID, затем по названию и пользовательскому коду)
                    FefuNsiIds nsiId = nsiIdsMap.get(getGUID(nsiEntity)); // Достаём GUID ОБ, если он есть

                    if (isNsiMasterSystem()) // Если НСИ - мастер-система, то обновляем элементы в ОБ
                    {
                        // TODO: shit happens! We could not create or update persons in OB yet
                        /*Student modifiedEntity = null;
                        if (null != entity) // Если найдены схожие элементы
                        {
                            if (isAnythingChanged(nsiEntity, entity, entityMap)) // Сверяем наличие изменений, чтобы не проводить обновление объекта без необходимости
                            {
                                modifiedEntity = updatePossibleDuplicateFields(nsiEntity, entity, entityMap); // Вносим изменения в сущность ОБ и далее обновляем сущности ОБ
                                IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiEntities(modifiedEntity, nsiId, getGUID(nsiEntity), isAddItemsToOBAllowed(), isNsiMasterSystem(), getEntityName());
                            } else
                                IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(entity, nsiId, getGUID(nsiEntity), getEntityName()); // Иначе просто обновляем данные о GUID и времени синхронизации элемента
                        } else if (isAddItemsToOBAllowed()) // Если схожих не найдено, и позволено добавление новых элементов в ОБ, то создаём новый элемент и сохраняем его в ОБ
                        {
                            modifiedEntity = createEntity(nsiEntity, entityMap);
                            IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiEntities(modifiedEntity, nsiId, getGUID(nsiEntity), isAddItemsToOBAllowed(), isNsiMasterSystem(), getEntityName());
                        } */
                    } else // Если НСИ - не мастер система, то обновляем элементы в НСИ значениями из ОБ (подготавливаем список для дальнейшего обновления)
                    {
                        // Чтобы повторно не заслать в НСИ элемент, который ранее оттуда был удалён, проверяем соответствующий признак, а так же смотрим, что что-то изменилось
                        if ((null == nsiId || (nsiId != null && !nsiId.isDeletedFromNsi())) && isAnythingChanged(nsiEntity, entity, entityMap))
                            nsiEntityToUpdate.add(updateNsiEntityFields(nsiEntity, entity));
                    }

                    // Чтобы избежать повторной обработки далее, добавляем в соответствующий сет
                    if (null != entity) alreadySynchronizedEntitySet.add(entity.getId());
                }

                FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Local (OB) synchronization of elements for " + getEntityClass().getSimpleName() + " catalog was finished ----------");

                // Если разрешено добавление новых элементов в НСИ
                if (isAddItemsToNSIAllowed())
                {
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog ----------");

                    List<Object> nsiInsertEntityList = new ArrayList<>();
                    Set<Long> alreadyPreparedForSendingToNSIEntitySet = new HashSet<>();

                    // Бежим по всем сущностям ОБ и выискиваем те, которых нет в НСИ
                    for (Student entity : entityPortion)
                    {
                        FefuNsiIds nsiId = nsiIdsMap.get(entity.getId().toString()); // Вытаскиваем GUID ОБ, если есть такой
                        if (null != nsiId && nsiId.isDeletedFromNsi())
                            continue; // Проверяем, чтобы соответствующий элемент не был удалён ранее из НСИ

                        // Если ранее ещё не обрабатывали данную сущность и не отправляли на правки в НСИ
                        if (!alreadySynchronizedEntitySet.contains(entity.getId())
                                && !alreadyPreparedForSendingToNSIEntitySet.contains(entity.getId()))
                        {
                            nsiInsertEntityList.add(createEntity(entity, null));
                            alreadyPreparedForSendingToNSIEntitySet.add(entity.getId());
                            IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(entity, nsiId, new FefuNsiIdWrapper(null, getEntityName()));
                        }
                    }
                    // Собственно, производим отправку свежедобавляемых элементов в НСИ
                    NsiReactorUtils.executeNSIAction(nsiInsertEntityList, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT, true);
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
                }


                // Если НСИ не является мастер-системой по данному справочнику и есть некие отличия элементов с ОБ, то производим их обновление в НСИ
                if (!isNsiMasterSystem() && nsiEntityToUpdate.size() > 0)
                {
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start update elements at NSI for " + getEntityClass().getSimpleName() + " catalog ----------");
                    NsiReactorUtils.executeNSIAction(nsiEntityToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE, true);
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Update elements at NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
                }
            }
        });


        // Изменяем время последней синхронизации для справочника в целом
        IFefuNsiSyncDAO.instance.get().updateNsiCatalogSyncTime(getNSIEntityClass().getSimpleName());
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG WAS FINISHED ==========");
    }

    @Override
    public void insertOrUpdateNewItemsAtNSI(Set<Long> entityToAddIdsSet)
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== INSERT/UPDATE ITEMS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG HAVE STARTED ==========");

        // Формируем мап сущностей ОБ, где ключ - идентификатор сущности ОБ, либо Название (ITitled), или пользовательский код (IActiveCatalogItem)
        final Map<String, Student> entityMap = getEntityMap(getEntityClass(), entityToAddIdsSet);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Some elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database by their ids ----------");
        // Формируем мап идентификаторов НСИ из числа сохраненных в ОБ, где ключ - идентификатор ОБ, либо GUID
        final Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMap(EntityRuntime.getMeta(getEntityClass()).getName(), entityToAddIdsSet);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Some nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database by their ids ----------");

        // Если для синхронизации нужны дополнительные данные, то инициализируем их
        prepareRelatedObjectsMap();

        final List<Object> nsiEntityToUpdate = new ArrayList<>(); // Сюда сваливаем все сущности НСИ, которые требуется обновить данными из ОБ
        final Set<Long> alreadySynchronizedEntitySet = new HashSet<>(); // Контрольный сет, блокирующий возможность повторного добавления сущности в НСИ, если она обновляется.

        BatchUtils.execute(entityMap.values(), 200, new BatchUtils.Action<Student>()
        {
            @Override
            public void execute(final Collection<Student> entityPortion)
            {
                /*List<StudentType> entityTypesToRetrieve = new ArrayList<>();

                for (Student student : entityPortion)
                {
                    entityTypesToRetrieve.add(getCatalogElementRetrieveRequestObject(getGUID(student, nsiIdsMap)));
                }

                // Забираем порцию элементов справочника из НСИ
                List<StudentType> nsiEntityList = NsiReactorUtils.retrieveObjectsFromNSIByElementsList(getNSIEntityClass(), entityTypesToRetrieve);

                // Проводим синхронизацию всех полученных элементов из НСИ с имеющимися в ОБ
                FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start local (OB) synchronization of elements for " + getEntityClass().getSimpleName() + " catalog ----------");
                for (StudentType nsiEntity : nsiEntityList)
                {
                    Student entity = getPossibleDuplicate(nsiEntity, nsiIdsMap, entityMap); // Вычисляем элемент ОБ, соответствующий элементу НСИ (по GUID, затем по названию и пользовательскому коду)
                    FefuNsiIds nsiId = nsiIdsMap.get(getGUID(nsiEntity)); // Достаём GUID ОБ, если он есть

                    if (!isNsiMasterSystem()) // Если НСИ - не мастер система, то обновляем элементы в НСИ значениями из ОБ (подготавливаем список для дальнейшего обновления)
                    {
                        // Чтобы повторно не заслать в НСИ элемент, который ранее оттуда был удалён, проверяем соответствующий признак, а так же смотрим, что что-то изменилось
                        if ((null == nsiId || (nsiId != null && !nsiId.isDeletedFromNsi())) && isAnythingChanged(nsiEntity, entity, entityMap))
                            nsiEntityToUpdate.add(updateNsiEntityFields(nsiEntity, entity));
                    }

                    // Чтобы избежать повторной обработки далее, добавляем в соответствующий сет
                    if (null != entity) alreadySynchronizedEntitySet.add(entity.getId());
                }

                FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Local (OB) synchronization of elements for " + getEntityClass().getSimpleName() + " catalog was finished ----------");*/

                // Если разрешено добавление новых элементов в НСИ
                if (isAddItemsToNSIAllowed())
                {
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog ----------");

                    List<Object> nsiInsertEntityList = new ArrayList<>();
                    Set<Long> alreadyPreparedForSendingToNSIEntitySet = new HashSet<>();

                    // Бежим по всем сущностям ОБ и выискиваем те, которых нет в НСИ
                    for (Student entity : entityPortion)
                    {
                        FefuNsiIds nsiId = nsiIdsMap.get(entity.getId().toString()); // Вытаскиваем GUID ОБ, если есть такой
                        if (null != nsiId && nsiId.isDeletedFromNsi())
                            continue; // Проверяем, чтобы соответствующий элемент не был удалён ранее из НСИ

                        // Если ранее ещё не обрабатывали данную сущность и не отправляли на правки в НСИ
                        if (!alreadySynchronizedEntitySet.contains(entity.getId())
                                && !alreadyPreparedForSendingToNSIEntitySet.contains(entity.getId()))
                        {
                            if (null == nsiId) nsiInsertEntityList.add(createEntity(entity, null));
                            else nsiEntityToUpdate.add(createEntity(entity, nsiIdsMap));
                            alreadyPreparedForSendingToNSIEntitySet.add(entity.getId());
                            IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(entity, nsiId, new FefuNsiIdWrapper(null, getEntityName()));
                        }
                    }
                    // Собственно, производим отправку свежедобавляемых элементов в НСИ
                    NsiReactorUtils.prepareNSIAction(nsiInsertEntityList, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT, true);
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
                }


                // Если НСИ не является мастер-системой по данному справочнику и есть некие отличия элементов с ОБ, то производим их обновление в НСИ
                if (!isNsiMasterSystem() && nsiEntityToUpdate.size() > 0)
                {
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start update elements at NSI for " + getEntityClass().getSimpleName() + " catalog ----------");
                    NsiReactorUtils.prepareNSIAction(nsiEntityToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE, true);
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Update elements at NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
                }
            }
        });

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== INSERT/UPDATE ITEMS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG WAS FINISHED ==========");
    }

    private List<Object> insertOrUpdate(List<INsiEntity> itemsToProcess, String operationType) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + Student.ENTITY_NAME.toUpperCase() + " CATALOG AT OB HAVE STARTED ==========");

        // Подготавливаем список консистентных сущностей из НСИ (вдруг там внезапно окажется дубль)
        Set<String> guidsToInsertSet = new HashSet<>();
        List<StudentType> itemsToInsertChecked = new ArrayList<>();
        List<Object> itemsProcessed = new ArrayList<>();
        Set<String> eduLevelGuidSet = new HashSet<>();
        Set<String> groupGuidSet = new HashSet<>();
        Set<String> humanGuidSet = new HashSet<>();

        for (INsiEntity nsiEntity : itemsToProcess)
        {
            // Если сущность помечена как неконсистентная (например, дубль), то её не нужно учитывать
            if (null == nsiEntity.getIsNotConsistent() || 1 != nsiEntity.getIsNotConsistent())
            {
                // Если у какой-то из сущностей в пакете не указан GUID, то обрабатывать такой пакет нельзя
                if (null == nsiEntity.getID())
                {
                    FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing incoming package was failed. There are one or more elements without GUID specified for " + Student.ENTITY_NAME + " catalog ----------");
                    FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + Student.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
                    throw new NSIProcessingErrorException("Could not " + operationType.toLowerCase() + " element without GUID specified. Please, specify GUID for all items in request package.");
                }

                itemsToInsertChecked.add((StudentType) nsiEntity);
                itemsProcessed.add(nsiEntity);

                guidsToInsertSet.add(nsiEntity.getID());

                if (nsiEntity instanceof StudentType)
                {
                    StudentType stud = (StudentType) nsiEntity;

                    if (null != stud.getEducationalProgramID() && null != stud.getEducationalProgramID().getEducationalProgram() && null != stud.getEducationalProgramID().getEducationalProgram().getID())
                        eduLevelGuidSet.add(stud.getEducationalProgramID().getEducationalProgram().getID());

                    if (null != stud.getAcademicGroupID() && null != stud.getAcademicGroupID().getAcademicGroup() && null != stud.getAcademicGroupID().getAcademicGroup().getID())
                        groupGuidSet.add(stud.getAcademicGroupID().getAcademicGroup().getID());

                    if (null != stud.getHumanID() && null != stud.getHumanID().getHuman() && null != stud.getHumanID().getHuman().getID())
                        humanGuidSet.add(stud.getHumanID().getHuman().getID());
                }

                // Добавляем список guid'ов, для которых может понадобиться объединение дублей
                List<String> mergeDuplicatesList = getUtil().getMergeDuplicatesList(nsiEntity);
                if (null != mergeDuplicatesList) guidsToInsertSet.addAll(mergeDuplicatesList);
            }
        }

        // Если после отсеивания невалидных сущностей в пакете не осталось ничего, то обработка пакета теряет смысл
        if (itemsToInsertChecked.isEmpty())
        {
            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing incoming package was failed. There is no any entity guid to " + operationType.toLowerCase() + " in datagram. Could not process an empty request. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + Student.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
            throw new NSIProcessingErrorException("There is no any entity guid to " + operationType.toLowerCase() + " in datagram. Could not process an empty request.");
        }

        // Готовим список сущностей НСИ, для которых требуется поднять данные из БД.
        // Здесь же учитываются идентификаторы НСИ для дедубликации
        List<INsiEntity> guidEntitySet = new ArrayList<>();
        guidEntitySet.addAll(itemsToProcess);

        for (String duplicateGuid : guidsToInsertSet)
        {
            guidEntitySet.add(getCatalogElementRetrieveRequestObject(duplicateGuid));
        }

        // Подготавливаем сет идентификаторов ОБ для инициализации мапов
        Set<Long> entityIdSet = IFefuNsiSyncDAO.instance.get().getEntityIdSetByNsiEntityList(guidEntitySet);

        // Получаем список GUID'ов неудаляемых объектов (в силу наличия ссылок с других объектов), на случай, если придётся дедублицировать
        List<String> nonDeletableGuids = IFefuNsiSyncDAO.instance.get().getNonDeletableGuidsList(Student.class, entityIdSet);

        // Инициализируем мапы, необходимые для создания/обновления сотрудников
        NsiCatalogsStore store = prepareCatalogStore();

        Set<Long> entityDuplicateIdsToDel = new HashSet<>();
        Set<String> alreadyProcessedEntitySet = new HashSet<>();
        List<String> notDeletedDuplicateGuids = new ArrayList<>();
        List<String> nonProcessableGuids = new ArrayList<>();

        Map<String, CoreCollectionUtils.Pair<Student, FefuNsiIds>> guidToStudentWithIdPairMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMapByGuids(Student.class, guidsToInsertSet, false);
        Map<String, CoreCollectionUtils.Pair<EducationOrgUnit, FefuNsiIds>> guidToEduOuWithIdPairMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMapByGuids(EducationOrgUnit.class, eduLevelGuidSet, false);
        store.setGuidToGroupWithIdPairMap(IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMapByGuids(Group.class, groupGuidSet, false));
        store.setGuidToPersonWithIdPairMap(IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMapByGuids(Person.class, humanGuidSet, false));
        store.setGuidToEduOuWithIdPairMap(guidToEduOuWithIdPairMap);

        // Подготавливаем Set guid'ов для быстрого определения аспирантских направлений.
        Set<Long> eduOrgUnitIdPostGradSet = new HashSet<>();
        Set<String> eduLevelGuidToPostGradSet = new HashSet<>();
        for (Map.Entry<String, CoreCollectionUtils.Pair<EducationOrgUnit, FefuNsiIds>> entry : guidToEduOuWithIdPairMap.entrySet())
        {
            if (null != entry.getValue().getX().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject())
            {
                EduProgramSubject subj = entry.getValue().getX().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
                if (EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_.equals(subj.getEduProgramKind().getCode()))
                {
                    eduOrgUnitIdPostGradSet.add(entry.getValue().getX().getId());
                    eduLevelGuidToPostGradSet.add(entry.getKey());
                }
            }
        }

        // Сюда сложим студентов, которых пришлось проигнорировать (мы обрабатываем исключительно аспирантов)
        List<StudentType> ignoredStudentsList = new ArrayList<>();

        for (StudentType nsiEntity : itemsToInsertChecked)
        {
            // Получаем студента, если есть с таким GUID
            CoreCollectionUtils.Pair<Student, FefuNsiIds> studentPair = guidToStudentWithIdPairMap.get(nsiEntity.getID());

            // Если это не аспирант, то игнорируем.
            if (null != studentPair && !eduOrgUnitIdPostGradSet.contains(studentPair.getX().getEducationOrgUnit().getId()))
            {
                ignoredStudentsList.add(nsiEntity);
                continue;
            }

            // Если пришедший студент будет назначен на направление, не имеющее отношения к аспирантуре - игнорируем
            if (null != nsiEntity.getEducationalProgramID() && null != nsiEntity.getEducationalProgramID().getEducationalProgram() && null != nsiEntity.getEducationalProgramID().getEducationalProgram().getID())
            {
                if (!eduLevelGuidToPostGradSet.contains(nsiEntity.getEducationalProgramID().getEducationalProgram().getID()))
                {
                    ignoredStudentsList.add(nsiEntity);
                    continue;
                }
            }

            if (!alreadyProcessedEntitySet.contains(nsiEntity.getID()))
            {
                alreadyProcessedEntitySet.add(nsiEntity.getID());
                List<CoreCollectionUtils.Pair<Student, FefuNsiIds>> entityDuplicates = new ArrayList<>();
                CoreCollectionUtils.Pair<Student, FefuNsiIds> entityPair = guidToStudentWithIdPairMap.get(nsiEntity.getID());
                if (null != entityPair) entityDuplicates.add(entityPair);

                // Пробегаемся по дублям, если таковые имеются
                List<String> mergeDuplicatesList = getUtil().getMergeDuplicatesList(nsiEntity);
                if (null != mergeDuplicatesList)
                {
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- There are some entities needs to be deduplicated (" + mergeDuplicatesList.size() + " items) for " + Student.ENTITY_NAME + " catalog ----------");
                    for (String duplicateGuid : mergeDuplicatesList)
                    {
                        CoreCollectionUtils.Pair<Student, FefuNsiIds> duplicateEntityPair = guidToStudentWithIdPairMap.get(duplicateGuid);
                        if (null == entityPair && null != duplicateEntityPair) // Не найден элемент с таким guid'ом, но есть элемент с GUID'ом дубликата
                        {
                            entityPair = duplicateEntityPair;
                        }

                        if (null != duplicateEntityPair && !entityDuplicates.contains(duplicateEntityPair))
                        {
                            // Если это не аспирант, то игнорируем.
                            if (null != studentPair && !eduOrgUnitIdPostGradSet.contains(studentPair.getX().getEducationOrgUnit().getId()))
                                ignoredStudentsList.add(nsiEntity);
                            else
                                entityDuplicates.add(duplicateEntityPair);
                        }
                    }
                }

                if (null != entityPair && null != entityPair.getX())
                {
                    entityDuplicates.remove(entityPair);
                    try
                    {
                        createStudent(nsiEntity, entityPair, store);
                    } catch (NSIProcessingErrorException e)
                    {
                        FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing incoming package was failed. " + e.getMessage() + " ----------");
                        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + Student.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
                        throw e;
                    }
                } else
                {
                    try
                    {
                        CoreCollectionUtils.Pair<Student, FefuNsiIds> modifiedPair = createStudent(nsiEntity, null, store);
                        if (null == modifiedPair || null == modifiedPair.getX())
                        {
                            nonProcessableGuids.add(nsiEntity.getID());
                        }
                    } catch (NSIProcessingErrorException e)
                    {
                        FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing incoming package was failed. " + e.getMessage() + " ----------");
                        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + Student.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
                        throw e;
                    }
                }

                // Формируем сет идентификаторов сущностей ОБ для удаления в процессе дедубликации
                for (CoreCollectionUtils.Pair<Student, FefuNsiIds> duplicateToDelPair : entityDuplicates)
                {
                    if (!nonDeletableGuids.contains(duplicateToDelPair.getY().getGuid()))
                    {
                        NsiObjectsHolder.getEmployeePostIdToEntityMap().remove(duplicateToDelPair);
                        entityDuplicateIdsToDel.add(duplicateToDelPair.getX().getId());
                    } else notDeletedDuplicateGuids.add(duplicateToDelPair.getY().getGuid());
                }
            }
        }

        // Если есть невставляемые сущности (в силу отсутствия значений обязательных полей, то возвращаем НСИ ошибку
        if (!nonProcessableGuids.isEmpty())
        {
            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing entities for " + Student.ENTITY_NAME + " catalog was failed due to some required field values absence. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + Student.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
            throw new NSIProcessingErrorException("One, or more entities could not be processed due to some required field values absence. Error object GUID's: " + CommonBaseStringUtil.joinNotEmpty(nonProcessableGuids, ", "));
        }

        // Если список неудаляемых объектов не пуст, то возвращаем в НСИ ошибку, не выполняя удаления других объектов
        if (!notDeletedDuplicateGuids.isEmpty())
        {
            StringBuilder errGuids = new StringBuilder();
            for (String guid : notDeletedDuplicateGuids)
            {
                errGuids.append(errGuids.length() > 0 ? ", " : "").append(guid);
            }

            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Deleting entities in process of deduplication for " + Student.ENTITY_NAME + " catalog was failed. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + Student.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");

            throw new NSIProcessingErrorException("One, or more entities can not be deleted, because of other objects relations with the entities have to be deleted. " +
                    "Please, check dependencies for the objects " + CommonBaseStringUtil.joinNotEmpty(notDeletedDuplicateGuids, ", ") + ".");
        }

        // Удаляем дубликаты и связанные с ними идентификаторы НСИ
        IFefuNsiSyncDAO.instance.get().deleteObjectsSetCheckedForDeletability(Student.class, entityDuplicateIdsToDel, true);
        IFefuNsiSyncDAO.instance.get().deleteRelatedNsiId(entityDuplicateIdsToDel);

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + Student.ENTITY_NAME.toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");

        if (!ignoredStudentsList.isEmpty())
        {
            List<String> ignoredGuids = new ArrayList<>();
            for (StudentType ignoredStudent : ignoredStudentsList) ignoredGuids.add(ignoredStudent.getID());
            throw new NSIProcessingWarnException("One, or more entities were ignored, because OB is only source for all the Students, except postgraduate students, Ignored GUID's: " + CommonBaseStringUtil.joinNotEmpty(ignoredGuids, ", "), itemsProcessed);
        }


        return itemsProcessed;
    }

    @Override
    public List<Object> insert(List<INsiEntity> itemsToInsert) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        return insertOrUpdate(itemsToInsert, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT);
    }

    @Override
    public List<Object> update(List<INsiEntity> itemsToUpdate) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        return insertOrUpdate(itemsToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE);
    }

    private CoreCollectionUtils.Pair<Student, FefuNsiIds> createStudent(StudentType studentType, CoreCollectionUtils.Pair<Student, FefuNsiIds> studentPair, NsiCatalogsStore store) throws NSIProcessingErrorException
    {
        // Если сущность новая, то обязательно нужно чтобы были заполнены некоторые ссылочные поля
        boolean newEntity = null == studentPair || null == studentPair.getX();
        if (newEntity)
        {
            if (null == studentType.getHumanID() || null == studentType.getHumanID().getHuman() || null == studentType.getHumanID().getHuman().getID())
                throw new NSIProcessingErrorException("Could not create Student (guid=" + studentType.getID() + "), because the HumanID is not specified.");

            if (null == studentType.getEducationalProgramID() || null == studentType.getEducationalProgramID().getEducationalProgram() || null == studentType.getEducationalProgramID().getEducationalProgram().getID())
                throw new NSIProcessingErrorException("Could not create Student (guid=" + studentType.getID() + "), because the EducationalProgramID is not specified.");

            if (null == studentType.getCompensationTypeID() || null == studentType.getCompensationTypeID().getCompensationType() || null == studentType.getCompensationTypeID().getCompensationType().getID())
                throw new NSIProcessingErrorException("Could not create Student (guid=" + studentType.getID() + "), because the CompensationTypeID is not specified.");

            if (null == studentType.getCourseID() || null == studentType.getCourseID().getCourse() || null == studentType.getCourseID().getCourse().getID())
                throw new NSIProcessingErrorException("Could not create Student (guid=" + studentType.getID() + "), because the CourseID is not specified.");

            if (null == studentType.getStudentCategoryID() || null == studentType.getStudentCategoryID().getStudentCategory() || null == studentType.getStudentCategoryID().getStudentCategory().getID())
                throw new NSIProcessingErrorException("Could not create Student (guid=" + studentType.getID() + "), because the StudentCategoryID is not specified.");

            if (null == studentType.getStudentStatusID() || null == studentType.getStudentStatusID().getStudentStatus() || null == studentType.getStudentStatusID().getStudentStatus().getID())
                throw new NSIProcessingErrorException("Could not create Student (guid=" + studentType.getID() + "), because the StudentStatusID is not specified.");

            if (null == StringUtils.trimToNull(studentType.getStudentEntranceYear()))
                throw new NSIProcessingErrorException("Could not create Student (guid=" + studentType.getID() + "), because the StudentEntranceYear is not specified.");

            if (null == StringUtils.trimToNull(studentType.getStudentPersonalNumber()))
                throw new NSIProcessingErrorException("Could not create Student (guid=" + studentType.getID() + "), because the StudentPersonalNumber is not specified.");
        }

        Student student = (null != studentPair && null != studentPair.getX()) ? studentPair.getX() : new Student();

        if (null != studentType.getHumanID() && null != studentType.getHumanID().getHuman() && null != studentType.getHumanID().getHuman().getID())
        {
            CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = store.getGuidToPersonWithIdPairMap().get(studentType.getHumanID().getHuman().getID());
            Person person = null != personPair ? personPair.getX() : null;
            if (null != person) student.setPerson(person);
            else
                throw new NSIProcessingErrorException("Could not find the given Human with guid=" + studentType.getHumanID().getHuman().getID() + " at OB.");
        }

        if (null != studentType.getEducationalProgramID() && null != studentType.getEducationalProgramID().getEducationalProgram() && null != studentType.getEducationalProgramID().getEducationalProgram().getID())
        {
            CoreCollectionUtils.Pair<EducationOrgUnit, FefuNsiIds> eduOuPair = store.getGuidToEduOuWithIdPairMap().get(studentType.getEducationalProgramID().getEducationalProgram().getID());
            EducationOrgUnit educationOrgUnit = null != eduOuPair ? eduOuPair.getX() : null;
            if (null != educationOrgUnit)
            {
                student.setEducationOrgUnit(educationOrgUnit);
                student.setDevelopPeriodAuto(educationOrgUnit.getDevelopPeriod());
            }
            else
                throw new NSIProcessingErrorException("Could not find the given EducationalProgram with guid=" + studentType.getEducationalProgramID().getEducationalProgram().getID() + " at OB.");
        }

        if (null != studentType.getCompensationTypeID() && null != studentType.getCompensationTypeID().getCompensationType() && null != studentType.getCompensationTypeID().getCompensationType().getID())
        {
            CoreCollectionUtils.Pair<CompensationType, FefuNsiIds> compTypePair = NsiObjectsHolder.getCompensationTypeMap().get(studentType.getCompensationTypeID().getCompensationType().getID());
            CompensationType compensationType = null != compTypePair ? compTypePair.getX() : null;
            if (null != compensationType) student.setCompensationType(compensationType);
            else
                throw new NSIProcessingErrorException("Could not find the given CompensationType with guid=" + studentType.getCompensationTypeID().getCompensationType().getID() + " at OB.");
        }

        if (null != studentType.getCourseID() && null != studentType.getCourseID().getCourse() && null != studentType.getCourseID().getCourse().getID())
        {
            CoreCollectionUtils.Pair<Course, FefuNsiIds> coursePair = NsiObjectsHolder.getCourseMap().get(studentType.getCourseID().getCourse().getID());
            Course course = null != coursePair ? coursePair.getX() : null;
            if (null != course) student.setCourse(course);
            else
                throw new NSIProcessingErrorException("Could not find the given Course with guid=" + studentType.getCourseID().getCourse().getID() + " at OB.");
        }

        if (null != studentType.getStudentCategoryID() && null != studentType.getStudentCategoryID().getStudentCategory() && null != studentType.getStudentCategoryID().getStudentCategory().getID())
        {
            CoreCollectionUtils.Pair<StudentCategory, FefuNsiIds> categoryPair = NsiObjectsHolder.getStudentCategoryMap().get(studentType.getStudentCategoryID().getStudentCategory().getID());
            StudentCategory category = null != categoryPair ? categoryPair.getX() : null;
            if (null != category) student.setStudentCategory(category);
            else
                throw new NSIProcessingErrorException("Could not find the given StudentCategory with guid=" + studentType.getStudentCategoryID().getStudentCategory().getID() + " at OB.");
        }

        if (null != studentType.getStudentStatusID() && null != studentType.getStudentStatusID().getStudentStatus() && null != studentType.getStudentStatusID().getStudentStatus().getID())
        {
            CoreCollectionUtils.Pair<StudentStatus, FefuNsiIds> statusPair = NsiObjectsHolder.getStudentStatusMap().get(studentType.getStudentStatusID().getStudentStatus().getID());
            StudentStatus status = null != statusPair ? statusPair.getX() : null;
            if (null != status) student.setStatus(status);
            else
                throw new NSIProcessingErrorException("Could not find the given StudentStatus with guid=" + studentType.getStudentStatusID().getStudentStatus().getID() + " at OB.");
        }

        if (null != studentType.getAcademicGroupID() && null != studentType.getAcademicGroupID().getAcademicGroup() && null != studentType.getAcademicGroupID().getAcademicGroup().getID())
        {
            CoreCollectionUtils.Pair<Group, FefuNsiIds> groupPair = store.getGuidToGroupWithIdPairMap().get(studentType.getAcademicGroupID().getAcademicGroup().getID());
            Group group = null != groupPair ? groupPair.getX() : null;
            if (null != group) student.setGroup(group);
        }

        if (null != StringUtils.trimToNull(studentType.getStudentPersonalNumber()))
            student.setPersonalNumber(studentType.getStudentPersonalNumber());
        if (null != StringUtils.trimToNull(studentType.getStudentEntranceYear()))
        {
            try
            {
                student.setEntranceYear(Integer.valueOf(studentType.getStudentEntranceYear()));
            } catch (Exception e)
            {
                throw new NSIProcessingErrorException("Could not parse the EntranceYear field value '" + studentType.getStudentEntranceYear() + "' for the student guid=" + studentType.getID() + " at OB.");
            }
        }
        if (null != StringUtils.trimToNull(studentType.getStudentYearEnd()))
        {
            {
                try
                {
                    student.setFinishYear(Integer.valueOf(studentType.getStudentYearEnd()));
                } catch (Exception e)
                {
                    throw new NSIProcessingErrorException("Could not parse the YearEnd field value '" + studentType.getStudentYearEnd() + "' for the student guid=" + studentType.getID() + " at OB.");
                }
            }
        }
        if (null != studentType.getStudentBookN()) student.setBookNumber(studentType.getStudentBookN());
        if (null != studentType.getStudentPrivacyN()) student.setPersonalFileNumber(studentType.getStudentPrivacyN());
        if (null != studentType.getStudentArhivePrivacyN())
            student.setPersonalArchivalFileNumber(studentType.getStudentArhivePrivacyN());
        if (null != studentType.getStudentAgreementN()) student.setContractNumber(studentType.getStudentAgreementN());
        if (null != studentType.getStudentComment()) student.setComment(studentType.getStudentComment());
        if (null != studentType.getStudentVKRTopic())
            student.setFinalQualifyingWorkTheme(studentType.getStudentVKRTopic());
        if (null != studentType.getStudentPracticePlacement())
            student.setPracticePlace(studentType.getStudentPracticePlacement());
        if (null != studentType.getStudentJobPlacement()) student.setEmployment(studentType.getStudentJobPlacement());
        if (null != StringUtils.trimToNull(studentType.getStudenIndividualTraining()))
            student.setPersonalEducation("1".equals(studentType.getStudenIndividualTraining()) ? Boolean.TRUE : Boolean.FALSE);
        if (null != StringUtils.trimToNull(studentType.getStudentEconomicGroup()))
            student.setThriftyGroup("1".equals(studentType.getStudentEconomicGroup()) ? Boolean.TRUE : Boolean.FALSE);
        if (null != StringUtils.trimToNull(studentType.getStudentTarget()))
            student.setTargetAdmission("1".equals(studentType.getStudentTarget()) ? Boolean.TRUE : Boolean.FALSE);
        if (null != StringUtils.trimToNull(studentType.getStudentArhive()))
            student.setArchival("1".equals(studentType.getStudentArhive()) ? Boolean.TRUE : Boolean.FALSE);
        if (null != NsiDatagramUtil.parseDate(studentType.getStudentArhiveDate()))
            student.setArchivingDate(NsiDatagramUtil.parseDate(studentType.getStudentArhiveDate()));
        if (!student.isArchival()) student.setArchivingDate(null);

        try
        {
            DataAccessServices.dao().saveOrUpdate(student);
        } catch (Exception e)
        {
            throw new NSIProcessingErrorException("Could not process Student (guid: " + studentType.getID() + "). Cause: " + e.getMessage());
        }

        FefuNsiIds studentNsiId = (null != studentPair && null != studentPair.getY()) ? studentPair.getY() : null;
        if (null == studentNsiId)
        {
            studentNsiId = new FefuNsiIds();
        } else
        {
            studentNsiId = DataAccessServices.dao().get(studentNsiId.getId());
        }

        if (studentType.getID().toLowerCase().equals(studentNsiId.getGuid()))
        {
            DataAccessServices.dao().delete(studentNsiId);
            studentNsiId = new FefuNsiIds();
        }

        studentNsiId.setEntityId(student.getId());
        studentNsiId.setGuid(studentType.getID().toLowerCase());
        studentNsiId.setEntityType(Student.ENTITY_NAME);
        studentNsiId.setSyncTime(new Date());
        DataAccessServices.dao().saveOrUpdate(studentNsiId);
        FefuNsiSyncDAO.logEvent(Level.INFO, "EmployeePostNsiId \"" + studentNsiId.getGuid() + "\" was added.");

        CoreCollectionUtils.Pair<Student, FefuNsiIds> result = new CoreCollectionUtils.Pair(student, studentNsiId);

        return result;
    }
}