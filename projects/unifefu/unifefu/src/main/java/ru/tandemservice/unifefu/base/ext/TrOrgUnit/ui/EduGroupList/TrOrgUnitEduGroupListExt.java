package ru.tandemservice.unifefu.base.ext.TrOrgUnit.ui.EduGroupList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.EduGroupList.TrOrgUnitEduGroupList;

/**
 * User: amakarova
 */
@Configuration
public class TrOrgUnitEduGroupListExt  extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrOrgUnitEduGroupListExtUI.class.getSimpleName();

    @Autowired
    private TrOrgUnitEduGroupList _trOrgUnitEduGroupsList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trOrgUnitEduGroupsList.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrOrgUnitEduGroupListExtUI.class))
                .create();
    }
}