package ru.tandemservice.unifefu.events.dset;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.unifefu.dao.daemon.FEFUAllStudExportDaemonDAO;
import ru.tandemservice.unifefu.dao.daemon.IFEFUAllStudExportDaemonDAO;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vnekrasov
 */
public class FefuStudentFefuExtListener extends ParamTransactionCompleteListener<Boolean>
{

    @SuppressWarnings("unchecked")
    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, StudentFefuExt.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, StudentFefuExt.class, this);

    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(MdbViewStudentAdditionalData.class);
        deleteBuilder.where(in(property(MdbViewStudentAdditionalData.studentFefuExt().id()), params));
        deleteBuilder.createStatement(session).execute();
        IFEFUAllStudExportDaemonDAO.instance.get().doRegisterEntity(MdbViewStudentAdditionalData.ENTITY_NAME, params);
        FEFUAllStudExportDaemonDAO.DAEMON.registerAfterCompleteWakeUp(session);
        return true;
    }

    @Override
    @Transactional(readOnly = false)
    public void afterCompletion(Session session, int status, Collection<Long> params, Boolean beforeCompletionResult)
    {

    }

}
