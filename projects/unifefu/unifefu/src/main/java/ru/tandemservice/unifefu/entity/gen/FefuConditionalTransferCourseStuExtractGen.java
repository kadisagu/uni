package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. «Об условном переводе с курса на следующий курс»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuConditionalTransferCourseStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract";
    public static final String ENTITY_NAME = "fefuConditionalTransferCourseStuExtract";
    public static final int VERSION_HASH = -98315627;
    private static IEntityMeta ENTITY_META;

    public static final String P_OLD_GROUP_NUMBER = "oldGroupNumber";
    public static final String P_ELIMINATE_DATE = "eliminateDate";

    private String _oldGroupNumber;     // Номер группы на момент формирования приказа
    private Date _eliminateDate;     // Срок ликвидации академической задолженности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер группы на момент формирования приказа.
     */
    @Length(max=255)
    public String getOldGroupNumber()
    {
        return _oldGroupNumber;
    }

    /**
     * @param oldGroupNumber Номер группы на момент формирования приказа.
     */
    public void setOldGroupNumber(String oldGroupNumber)
    {
        dirty(_oldGroupNumber, oldGroupNumber);
        _oldGroupNumber = oldGroupNumber;
    }

    /**
     * @return Срок ликвидации академической задолженности.
     */
    public Date getEliminateDate()
    {
        return _eliminateDate;
    }

    /**
     * @param eliminateDate Срок ликвидации академической задолженности.
     */
    public void setEliminateDate(Date eliminateDate)
    {
        dirty(_eliminateDate, eliminateDate);
        _eliminateDate = eliminateDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuConditionalTransferCourseStuExtractGen)
        {
            setOldGroupNumber(((FefuConditionalTransferCourseStuExtract)another).getOldGroupNumber());
            setEliminateDate(((FefuConditionalTransferCourseStuExtract)another).getEliminateDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuConditionalTransferCourseStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuConditionalTransferCourseStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuConditionalTransferCourseStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroupNumber":
                    return obj.getOldGroupNumber();
                case "eliminateDate":
                    return obj.getEliminateDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "oldGroupNumber":
                    obj.setOldGroupNumber((String) value);
                    return;
                case "eliminateDate":
                    obj.setEliminateDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroupNumber":
                        return true;
                case "eliminateDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroupNumber":
                    return true;
                case "eliminateDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "oldGroupNumber":
                    return String.class;
                case "eliminateDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuConditionalTransferCourseStuExtract> _dslPath = new Path<FefuConditionalTransferCourseStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuConditionalTransferCourseStuExtract");
    }
            

    /**
     * @return Номер группы на момент формирования приказа.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract#getOldGroupNumber()
     */
    public static PropertyPath<String> oldGroupNumber()
    {
        return _dslPath.oldGroupNumber();
    }

    /**
     * @return Срок ликвидации академической задолженности.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract#getEliminateDate()
     */
    public static PropertyPath<Date> eliminateDate()
    {
        return _dslPath.eliminateDate();
    }

    public static class Path<E extends FefuConditionalTransferCourseStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<String> _oldGroupNumber;
        private PropertyPath<Date> _eliminateDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер группы на момент формирования приказа.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract#getOldGroupNumber()
     */
        public PropertyPath<String> oldGroupNumber()
        {
            if(_oldGroupNumber == null )
                _oldGroupNumber = new PropertyPath<String>(FefuConditionalTransferCourseStuExtractGen.P_OLD_GROUP_NUMBER, this);
            return _oldGroupNumber;
        }

    /**
     * @return Срок ликвидации академической задолженности.
     * @see ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract#getEliminateDate()
     */
        public PropertyPath<Date> eliminateDate()
        {
            if(_eliminateDate == null )
                _eliminateDate = new PropertyPath<Date>(FefuConditionalTransferCourseStuExtractGen.P_ELIMINATE_DATE, this);
            return _eliminateDate;
        }

        public Class getEntityClass()
        {
            return FefuConditionalTransferCourseStuExtract.class;
        }

        public String getEntityName()
        {
            return "fefuConditionalTransferCourseStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
