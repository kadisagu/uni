package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Договор на прохождение практики
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuPracticeContractWithExtOuGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu";
    public static final String ENTITY_NAME = "fefuPracticeContractWithExtOu";
    public static final int VERSION_HASH = 1986710269;
    private static IEntityMeta ENTITY_META;

    public static final String P_CONTRACT_NUM = "contractNum";
    public static final String P_INTERNAL_NUM = "internalNum";
    public static final String P_CONTRACT_DATE = "contractDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_CHECKED = "checked";
    public static final String P_ARCHIVAL = "archival";
    public static final String P_RATING = "rating";
    public static final String P_ADDITIONAL_PROMISES = "additionalPromises";
    public static final String L_HEADER_EXT_OU = "headerExtOu";
    public static final String L_EXTERNAL_ORG_UNIT = "externalOrgUnit";
    public static final String P_HAS_ADDITIONAL_PROMISES = "hasAdditionalPromises";

    private String _contractNum;     // Номер договора
    private Integer _internalNum;     // Внутренний номер
    private Date _contractDate;     // Дата заключения договора
    private Date _endDate;     // Дата завершения договора
    private boolean _checked = false;     // Проверен
    private boolean _archival = false;     // В архиве
    private String _rating;     // Рейтинг организации
    private String _additionalPromises;     // Дополнительные обязательства по договору
    private JuridicalContactor _headerExtOu;     // Руководитель
    private ExternalOrgUnit _externalOrgUnit;     // Организация-контрагент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getContractNum()
    {
        return _contractNum;
    }

    /**
     * @param contractNum Номер договора. Свойство не может быть null.
     */
    public void setContractNum(String contractNum)
    {
        dirty(_contractNum, contractNum);
        _contractNum = contractNum;
    }

    /**
     * @return Внутренний номер.
     */
    public Integer getInternalNum()
    {
        return _internalNum;
    }

    /**
     * @param internalNum Внутренний номер.
     */
    public void setInternalNum(Integer internalNum)
    {
        dirty(_internalNum, internalNum);
        _internalNum = internalNum;
    }

    /**
     * @return Дата заключения договора. Свойство не может быть null.
     */
    @NotNull
    public Date getContractDate()
    {
        return _contractDate;
    }

    /**
     * @param contractDate Дата заключения договора. Свойство не может быть null.
     */
    public void setContractDate(Date contractDate)
    {
        dirty(_contractDate, contractDate);
        _contractDate = contractDate;
    }

    /**
     * @return Дата завершения договора.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата завершения договора.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Проверен. Свойство не может быть null.
     */
    @NotNull
    public boolean isChecked()
    {
        return _checked;
    }

    /**
     * @param checked Проверен. Свойство не может быть null.
     */
    public void setChecked(boolean checked)
    {
        dirty(_checked, checked);
        _checked = checked;
    }

    /**
     * @return В архиве. Свойство не может быть null.
     */
    @NotNull
    public boolean isArchival()
    {
        return _archival;
    }

    /**
     * @param archival В архиве. Свойство не может быть null.
     */
    public void setArchival(boolean archival)
    {
        dirty(_archival, archival);
        _archival = archival;
    }

    /**
     * @return Рейтинг организации.
     */
    public String getRating()
    {
        return _rating;
    }

    /**
     * @param rating Рейтинг организации.
     */
    public void setRating(String rating)
    {
        dirty(_rating, rating);
        _rating = rating;
    }

    /**
     * @return Дополнительные обязательства по договору.
     */
    public String getAdditionalPromises()
    {
        return _additionalPromises;
    }

    /**
     * @param additionalPromises Дополнительные обязательства по договору.
     */
    public void setAdditionalPromises(String additionalPromises)
    {
        dirty(_additionalPromises, additionalPromises);
        _additionalPromises = additionalPromises;
    }

    /**
     * @return Руководитель.
     */
    public JuridicalContactor getHeaderExtOu()
    {
        return _headerExtOu;
    }

    /**
     * @param headerExtOu Руководитель.
     */
    public void setHeaderExtOu(JuridicalContactor headerExtOu)
    {
        dirty(_headerExtOu, headerExtOu);
        _headerExtOu = headerExtOu;
    }

    /**
     * @return Организация-контрагент.
     */
    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    /**
     * @param externalOrgUnit Организация-контрагент.
     */
    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        dirty(_externalOrgUnit, externalOrgUnit);
        _externalOrgUnit = externalOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuPracticeContractWithExtOuGen)
        {
            setContractNum(((FefuPracticeContractWithExtOu)another).getContractNum());
            setInternalNum(((FefuPracticeContractWithExtOu)another).getInternalNum());
            setContractDate(((FefuPracticeContractWithExtOu)another).getContractDate());
            setEndDate(((FefuPracticeContractWithExtOu)another).getEndDate());
            setChecked(((FefuPracticeContractWithExtOu)another).isChecked());
            setArchival(((FefuPracticeContractWithExtOu)another).isArchival());
            setRating(((FefuPracticeContractWithExtOu)another).getRating());
            setAdditionalPromises(((FefuPracticeContractWithExtOu)another).getAdditionalPromises());
            setHeaderExtOu(((FefuPracticeContractWithExtOu)another).getHeaderExtOu());
            setExternalOrgUnit(((FefuPracticeContractWithExtOu)another).getExternalOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuPracticeContractWithExtOuGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuPracticeContractWithExtOu.class;
        }

        public T newInstance()
        {
            return (T) new FefuPracticeContractWithExtOu();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "contractNum":
                    return obj.getContractNum();
                case "internalNum":
                    return obj.getInternalNum();
                case "contractDate":
                    return obj.getContractDate();
                case "endDate":
                    return obj.getEndDate();
                case "checked":
                    return obj.isChecked();
                case "archival":
                    return obj.isArchival();
                case "rating":
                    return obj.getRating();
                case "additionalPromises":
                    return obj.getAdditionalPromises();
                case "headerExtOu":
                    return obj.getHeaderExtOu();
                case "externalOrgUnit":
                    return obj.getExternalOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "contractNum":
                    obj.setContractNum((String) value);
                    return;
                case "internalNum":
                    obj.setInternalNum((Integer) value);
                    return;
                case "contractDate":
                    obj.setContractDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "checked":
                    obj.setChecked((Boolean) value);
                    return;
                case "archival":
                    obj.setArchival((Boolean) value);
                    return;
                case "rating":
                    obj.setRating((String) value);
                    return;
                case "additionalPromises":
                    obj.setAdditionalPromises((String) value);
                    return;
                case "headerExtOu":
                    obj.setHeaderExtOu((JuridicalContactor) value);
                    return;
                case "externalOrgUnit":
                    obj.setExternalOrgUnit((ExternalOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "contractNum":
                        return true;
                case "internalNum":
                        return true;
                case "contractDate":
                        return true;
                case "endDate":
                        return true;
                case "checked":
                        return true;
                case "archival":
                        return true;
                case "rating":
                        return true;
                case "additionalPromises":
                        return true;
                case "headerExtOu":
                        return true;
                case "externalOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "contractNum":
                    return true;
                case "internalNum":
                    return true;
                case "contractDate":
                    return true;
                case "endDate":
                    return true;
                case "checked":
                    return true;
                case "archival":
                    return true;
                case "rating":
                    return true;
                case "additionalPromises":
                    return true;
                case "headerExtOu":
                    return true;
                case "externalOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "contractNum":
                    return String.class;
                case "internalNum":
                    return Integer.class;
                case "contractDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "checked":
                    return Boolean.class;
                case "archival":
                    return Boolean.class;
                case "rating":
                    return String.class;
                case "additionalPromises":
                    return String.class;
                case "headerExtOu":
                    return JuridicalContactor.class;
                case "externalOrgUnit":
                    return ExternalOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuPracticeContractWithExtOu> _dslPath = new Path<FefuPracticeContractWithExtOu>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuPracticeContractWithExtOu");
    }
            

    /**
     * @return Номер договора. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getContractNum()
     */
    public static PropertyPath<String> contractNum()
    {
        return _dslPath.contractNum();
    }

    /**
     * @return Внутренний номер.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getInternalNum()
     */
    public static PropertyPath<Integer> internalNum()
    {
        return _dslPath.internalNum();
    }

    /**
     * @return Дата заключения договора. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getContractDate()
     */
    public static PropertyPath<Date> contractDate()
    {
        return _dslPath.contractDate();
    }

    /**
     * @return Дата завершения договора.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Проверен. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#isChecked()
     */
    public static PropertyPath<Boolean> checked()
    {
        return _dslPath.checked();
    }

    /**
     * @return В архиве. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#isArchival()
     */
    public static PropertyPath<Boolean> archival()
    {
        return _dslPath.archival();
    }

    /**
     * @return Рейтинг организации.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getRating()
     */
    public static PropertyPath<String> rating()
    {
        return _dslPath.rating();
    }

    /**
     * @return Дополнительные обязательства по договору.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getAdditionalPromises()
     */
    public static PropertyPath<String> additionalPromises()
    {
        return _dslPath.additionalPromises();
    }

    /**
     * @return Руководитель.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getHeaderExtOu()
     */
    public static JuridicalContactor.Path<JuridicalContactor> headerExtOu()
    {
        return _dslPath.headerExtOu();
    }

    /**
     * @return Организация-контрагент.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getExternalOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
    {
        return _dslPath.externalOrgUnit();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#isHasAdditionalPromises()
     */
    public static SupportedPropertyPath<Boolean> hasAdditionalPromises()
    {
        return _dslPath.hasAdditionalPromises();
    }

    public static class Path<E extends FefuPracticeContractWithExtOu> extends EntityPath<E>
    {
        private PropertyPath<String> _contractNum;
        private PropertyPath<Integer> _internalNum;
        private PropertyPath<Date> _contractDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Boolean> _checked;
        private PropertyPath<Boolean> _archival;
        private PropertyPath<String> _rating;
        private PropertyPath<String> _additionalPromises;
        private JuridicalContactor.Path<JuridicalContactor> _headerExtOu;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _externalOrgUnit;
        private SupportedPropertyPath<Boolean> _hasAdditionalPromises;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер договора. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getContractNum()
     */
        public PropertyPath<String> contractNum()
        {
            if(_contractNum == null )
                _contractNum = new PropertyPath<String>(FefuPracticeContractWithExtOuGen.P_CONTRACT_NUM, this);
            return _contractNum;
        }

    /**
     * @return Внутренний номер.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getInternalNum()
     */
        public PropertyPath<Integer> internalNum()
        {
            if(_internalNum == null )
                _internalNum = new PropertyPath<Integer>(FefuPracticeContractWithExtOuGen.P_INTERNAL_NUM, this);
            return _internalNum;
        }

    /**
     * @return Дата заключения договора. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getContractDate()
     */
        public PropertyPath<Date> contractDate()
        {
            if(_contractDate == null )
                _contractDate = new PropertyPath<Date>(FefuPracticeContractWithExtOuGen.P_CONTRACT_DATE, this);
            return _contractDate;
        }

    /**
     * @return Дата завершения договора.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(FefuPracticeContractWithExtOuGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Проверен. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#isChecked()
     */
        public PropertyPath<Boolean> checked()
        {
            if(_checked == null )
                _checked = new PropertyPath<Boolean>(FefuPracticeContractWithExtOuGen.P_CHECKED, this);
            return _checked;
        }

    /**
     * @return В архиве. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#isArchival()
     */
        public PropertyPath<Boolean> archival()
        {
            if(_archival == null )
                _archival = new PropertyPath<Boolean>(FefuPracticeContractWithExtOuGen.P_ARCHIVAL, this);
            return _archival;
        }

    /**
     * @return Рейтинг организации.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getRating()
     */
        public PropertyPath<String> rating()
        {
            if(_rating == null )
                _rating = new PropertyPath<String>(FefuPracticeContractWithExtOuGen.P_RATING, this);
            return _rating;
        }

    /**
     * @return Дополнительные обязательства по договору.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getAdditionalPromises()
     */
        public PropertyPath<String> additionalPromises()
        {
            if(_additionalPromises == null )
                _additionalPromises = new PropertyPath<String>(FefuPracticeContractWithExtOuGen.P_ADDITIONAL_PROMISES, this);
            return _additionalPromises;
        }

    /**
     * @return Руководитель.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getHeaderExtOu()
     */
        public JuridicalContactor.Path<JuridicalContactor> headerExtOu()
        {
            if(_headerExtOu == null )
                _headerExtOu = new JuridicalContactor.Path<JuridicalContactor>(L_HEADER_EXT_OU, this);
            return _headerExtOu;
        }

    /**
     * @return Организация-контрагент.
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#getExternalOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
        {
            if(_externalOrgUnit == null )
                _externalOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_EXTERNAL_ORG_UNIT, this);
            return _externalOrgUnit;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu#isHasAdditionalPromises()
     */
        public SupportedPropertyPath<Boolean> hasAdditionalPromises()
        {
            if(_hasAdditionalPromises == null )
                _hasAdditionalPromises = new SupportedPropertyPath<Boolean>(FefuPracticeContractWithExtOuGen.P_HAS_ADDITIONAL_PROMISES, this);
            return _hasAdditionalPromises;
        }

        public Class getEntityClass()
        {
            return FefuPracticeContractWithExtOu.class;
        }

        public String getEntityName()
        {
            return "fefuPracticeContractWithExtOu";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract boolean isHasAdditionalPromises();
}
