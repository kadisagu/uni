/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleSession.ui.PrintFormAdd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.base.ext.SppScheduleSession.logic.FefuScheduleSessionComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.PrintFormAdd.SppSchedulePrintFormAdd;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.PrintFormAdd.SppScheduleSessionPrintFormAdd;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;

/**
 * @author Igor Belanov
 * @since 22.09.2016
 */
@Configuration
public class SppScheduleSessionPrintFormAddExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "fefu" + SppScheduleSessionPrintFormAddExtUI.class.getSimpleName();

    public static final String TERRITORIAL_OU_DS = "territorialOrgUnitDS";
    public static final String TERM_DS = "termDS";

    @Autowired
    private SppScheduleSessionPrintFormAdd _sppScheduleSessionPrintFormAdd;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_sppScheduleSessionPrintFormAdd.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, SppScheduleSessionPrintFormAddExtUI.class))
                .addDataSource(selectDS(TERRITORIAL_OU_DS, SppScheduleSessionManager.instance().sppGroupComboDSHandler()).addColumn("title", OrgUnit.territorialFullTitle().s()))
                .addDataSource(selectDS(TERM_DS, SppScheduleManager.instance().termComboDSHandler()))
                .replaceDataSource(selectDS(SppScheduleSessionPrintFormAdd.SEASON_DS, schedulesSessionComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleSessionSeason>()
                {
                    @Override
                    public String format(SppScheduleSessionSeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .replaceDataSource(selectDS(SppScheduleSessionPrintFormAdd.SCHEDULES_DS, schedulesSessionComboDSHandler()).addColumn("group", SppScheduleSession.group().title().s()).addColumn("schedule", SppScheduleSession.title().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler schedulesSessionComboDSHandler()
    {
        return new FefuScheduleSessionComboDSHandler(getName());
    }
}
