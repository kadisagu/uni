/* $Id$ */
package ru.tandemservice.unifefu.dao;

/**
 * @author Dmitry Seleznev
 * @since 08.04.2013
 */
public class UnifefuDaoFacade
{
    public static IUnifefuDao getUnifefuDAO()
    {
        return IUnifefuDao.instance.get();
    }

    public static IFefuOnlineEntrantRegistrationDAO getOnlineEntrantDAO()
    {
        return IFefuOnlineEntrantRegistrationDAO.instance.get();
    }

    public static IFefuEntrantDAO getFefuEntrantDAO()
    {
        return (IFefuEntrantDAO) IFefuEntrantDAO.instance.get();
    }

    public static IFefuMoveStudentDAO getFefuMoveStudentDAO()
    {
        return (IFefuMoveStudentDAO) IFefuMoveStudentDAO.instance.get();
    }

    public static IFefuGroupDAO getFefuGroupDAO() {return (IFefuGroupDAO) IFefuGroupDAO.instance.get();}
}