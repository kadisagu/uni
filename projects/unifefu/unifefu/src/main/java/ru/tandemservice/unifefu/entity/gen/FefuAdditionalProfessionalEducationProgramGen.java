package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.DiplomaQualifications;
import ru.tandemservice.uni.entity.catalog.EducationLevelAdditional;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.catalog.FefuDevelopConditionApe;
import ru.tandemservice.unifefu.entity.catalog.FefuDevelopFormApe;
import ru.tandemservice.unifefu.entity.catalog.FefuDevelopPeriodApe;
import ru.tandemservice.unifefu.entity.catalog.FefuDevelopTechApe;
import ru.tandemservice.unifefu.entity.catalog.FefuDiplomaTypeApe;
import ru.tandemservice.unifefu.entity.catalog.FefuIssuedDocumentApe;
import ru.tandemservice.unifefu.entity.catalog.FefuProgramPublicationStatusApe;
import ru.tandemservice.unifefu.entity.catalog.FefuProgramStatusApe;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Программа ДПО/ДО
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuAdditionalProfessionalEducationProgramGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram";
    public static final String ENTITY_NAME = "fefuAdditionalProfessionalEducationProgram";
    public static final int VERSION_HASH = -2037229055;
    private static IEntityMeta ENTITY_META;

    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String P_TITLE = "title";
    public static final String P_SPECIALTY_GROUP = "specialtyGroup";
    public static final String P_PROGRAM_OPENING_DATE = "programOpeningDate";
    public static final String P_PROGRAM_CLOSING_DATE = "programClosingDate";
    public static final String L_PRODUCING_ORG_UNIT = "producingOrgUnit";
    public static final String L_DIPLOMA_QUALIFICATION = "diplomaQualification";
    public static final String L_DIPLOMA_TYPE = "diplomaType";
    public static final String P_TEST_UNITS = "testUnits";
    public static final String P_HOURS = "hours";
    public static final String P_FGOS_CYCLE = "fgosCycle";
    public static final String L_PROGRAM_STATUS = "programStatus";
    public static final String L_PROGRAM_PUBLICATION_STATUS = "programPublicationStatus";
    public static final String P_OPENING_PROGRAM_ORDER_DETAILS = "openingProgramOrderDetails";
    public static final String L_EDUCATION_LEVEL = "educationLevel";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_DEVELOP_TECH = "developTech";
    public static final String L_DEVELOP_PERIOD = "developPeriod";
    public static final String L_ISSUED_DOCUMENT = "issuedDocument";

    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private OrgUnit _territorialOrgUnit;     // Территориальное подразделение
    private EducationOrgUnit _educationOrgUnit;     // Тип программы ДПО/ДО
    private String _title;     // Наименование программы ДПО/ДО
    private String _specialtyGroup;     // Укрупненная группа специальностей
    private Date _programOpeningDate;     // Дата открытия программы
    private Date _programClosingDate;     // Дата закрытия программы
    private OrgUnit _producingOrgUnit;     // Выпускающее подразделение
    private DiplomaQualifications _diplomaQualification;     // Квалификация по диплому
    private FefuDiplomaTypeApe _diplomaType;     // Вид диплома
    private String _testUnits;     // Зачетные единицы
    private String _hours;     // Часы
    private String _fgosCycle;     // Цикл ФГОС, наименование, шифр, квалификационные или профессиональные требования
    private FefuProgramStatusApe _programStatus;     // Статус программы
    private FefuProgramPublicationStatusApe _programPublicationStatus;     // Статус публикации программы
    private String _openingProgramOrderDetails;     // Реквизиты приказа об открытии программы
    private EducationLevelAdditional _educationLevel;     // Уровень программы ДПО
    private FefuDevelopFormApe _developForm;     // Форма освоения
    private FefuDevelopConditionApe _developCondition;     // Условия освоения
    private FefuDevelopTechApe _developTech;     // Технология освоения
    private FefuDevelopPeriodApe _developPeriod;     // Сроки освоения
    private FefuIssuedDocumentApe _issuedDocument;     // Выдаваемый документ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение. Свойство не может быть null.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подразделение. Свойство не может быть null.
     */
    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Тип программы ДПО/ДО. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Тип программы ДПО/ДО. Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Наименование программы ДПО/ДО. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Наименование программы ДПО/ДО. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Укрупненная группа специальностей.
     */
    @Length(max=255)
    public String getSpecialtyGroup()
    {
        return _specialtyGroup;
    }

    /**
     * @param specialtyGroup Укрупненная группа специальностей.
     */
    public void setSpecialtyGroup(String specialtyGroup)
    {
        dirty(_specialtyGroup, specialtyGroup);
        _specialtyGroup = specialtyGroup;
    }

    /**
     * @return Дата открытия программы.
     */
    public Date getProgramOpeningDate()
    {
        return _programOpeningDate;
    }

    /**
     * @param programOpeningDate Дата открытия программы.
     */
    public void setProgramOpeningDate(Date programOpeningDate)
    {
        dirty(_programOpeningDate, programOpeningDate);
        _programOpeningDate = programOpeningDate;
    }

    /**
     * @return Дата закрытия программы.
     */
    public Date getProgramClosingDate()
    {
        return _programClosingDate;
    }

    /**
     * @param programClosingDate Дата закрытия программы.
     */
    public void setProgramClosingDate(Date programClosingDate)
    {
        dirty(_programClosingDate, programClosingDate);
        _programClosingDate = programClosingDate;
    }

    /**
     * @return Выпускающее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getProducingOrgUnit()
    {
        return _producingOrgUnit;
    }

    /**
     * @param producingOrgUnit Выпускающее подразделение. Свойство не может быть null.
     */
    public void setProducingOrgUnit(OrgUnit producingOrgUnit)
    {
        dirty(_producingOrgUnit, producingOrgUnit);
        _producingOrgUnit = producingOrgUnit;
    }

    /**
     * @return Квалификация по диплому.
     */
    public DiplomaQualifications getDiplomaQualification()
    {
        return _diplomaQualification;
    }

    /**
     * @param diplomaQualification Квалификация по диплому.
     */
    public void setDiplomaQualification(DiplomaQualifications diplomaQualification)
    {
        dirty(_diplomaQualification, diplomaQualification);
        _diplomaQualification = diplomaQualification;
    }

    /**
     * @return Вид диплома.
     */
    public FefuDiplomaTypeApe getDiplomaType()
    {
        return _diplomaType;
    }

    /**
     * @param diplomaType Вид диплома.
     */
    public void setDiplomaType(FefuDiplomaTypeApe diplomaType)
    {
        dirty(_diplomaType, diplomaType);
        _diplomaType = diplomaType;
    }

    /**
     * @return Зачетные единицы.
     */
    @Length(max=255)
    public String getTestUnits()
    {
        return _testUnits;
    }

    /**
     * @param testUnits Зачетные единицы.
     */
    public void setTestUnits(String testUnits)
    {
        dirty(_testUnits, testUnits);
        _testUnits = testUnits;
    }

    /**
     * @return Часы.
     */
    @Length(max=255)
    public String getHours()
    {
        return _hours;
    }

    /**
     * @param hours Часы.
     */
    public void setHours(String hours)
    {
        dirty(_hours, hours);
        _hours = hours;
    }

    /**
     * @return Цикл ФГОС, наименование, шифр, квалификационные или профессиональные требования.
     */
    @Length(max=255)
    public String getFgosCycle()
    {
        return _fgosCycle;
    }

    /**
     * @param fgosCycle Цикл ФГОС, наименование, шифр, квалификационные или профессиональные требования.
     */
    public void setFgosCycle(String fgosCycle)
    {
        dirty(_fgosCycle, fgosCycle);
        _fgosCycle = fgosCycle;
    }

    /**
     * @return Статус программы. Свойство не может быть null.
     */
    @NotNull
    public FefuProgramStatusApe getProgramStatus()
    {
        return _programStatus;
    }

    /**
     * @param programStatus Статус программы. Свойство не может быть null.
     */
    public void setProgramStatus(FefuProgramStatusApe programStatus)
    {
        dirty(_programStatus, programStatus);
        _programStatus = programStatus;
    }

    /**
     * @return Статус публикации программы. Свойство не может быть null.
     */
    @NotNull
    public FefuProgramPublicationStatusApe getProgramPublicationStatus()
    {
        return _programPublicationStatus;
    }

    /**
     * @param programPublicationStatus Статус публикации программы. Свойство не может быть null.
     */
    public void setProgramPublicationStatus(FefuProgramPublicationStatusApe programPublicationStatus)
    {
        dirty(_programPublicationStatus, programPublicationStatus);
        _programPublicationStatus = programPublicationStatus;
    }

    /**
     * @return Реквизиты приказа об открытии программы.
     */
    @Length(max=255)
    public String getOpeningProgramOrderDetails()
    {
        return _openingProgramOrderDetails;
    }

    /**
     * @param openingProgramOrderDetails Реквизиты приказа об открытии программы.
     */
    public void setOpeningProgramOrderDetails(String openingProgramOrderDetails)
    {
        dirty(_openingProgramOrderDetails, openingProgramOrderDetails);
        _openingProgramOrderDetails = openingProgramOrderDetails;
    }

    /**
     * @return Уровень программы ДПО.
     */
    public EducationLevelAdditional getEducationLevel()
    {
        return _educationLevel;
    }

    /**
     * @param educationLevel Уровень программы ДПО.
     */
    public void setEducationLevel(EducationLevelAdditional educationLevel)
    {
        dirty(_educationLevel, educationLevel);
        _educationLevel = educationLevel;
    }

    /**
     * @return Форма освоения.
     */
    public FefuDevelopFormApe getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(FefuDevelopFormApe developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условия освоения.
     */
    public FefuDevelopConditionApe getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условия освоения.
     */
    public void setDevelopCondition(FefuDevelopConditionApe developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    public FefuDevelopTechApe getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(FefuDevelopTechApe developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Сроки освоения.
     */
    public FefuDevelopPeriodApe getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Сроки освоения.
     */
    public void setDevelopPeriod(FefuDevelopPeriodApe developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Выдаваемый документ.
     */
    public FefuIssuedDocumentApe getIssuedDocument()
    {
        return _issuedDocument;
    }

    /**
     * @param issuedDocument Выдаваемый документ.
     */
    public void setIssuedDocument(FefuIssuedDocumentApe issuedDocument)
    {
        dirty(_issuedDocument, issuedDocument);
        _issuedDocument = issuedDocument;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuAdditionalProfessionalEducationProgramGen)
        {
            setFormativeOrgUnit(((FefuAdditionalProfessionalEducationProgram)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((FefuAdditionalProfessionalEducationProgram)another).getTerritorialOrgUnit());
            setEducationOrgUnit(((FefuAdditionalProfessionalEducationProgram)another).getEducationOrgUnit());
            setTitle(((FefuAdditionalProfessionalEducationProgram)another).getTitle());
            setSpecialtyGroup(((FefuAdditionalProfessionalEducationProgram)another).getSpecialtyGroup());
            setProgramOpeningDate(((FefuAdditionalProfessionalEducationProgram)another).getProgramOpeningDate());
            setProgramClosingDate(((FefuAdditionalProfessionalEducationProgram)another).getProgramClosingDate());
            setProducingOrgUnit(((FefuAdditionalProfessionalEducationProgram)another).getProducingOrgUnit());
            setDiplomaQualification(((FefuAdditionalProfessionalEducationProgram)another).getDiplomaQualification());
            setDiplomaType(((FefuAdditionalProfessionalEducationProgram)another).getDiplomaType());
            setTestUnits(((FefuAdditionalProfessionalEducationProgram)another).getTestUnits());
            setHours(((FefuAdditionalProfessionalEducationProgram)another).getHours());
            setFgosCycle(((FefuAdditionalProfessionalEducationProgram)another).getFgosCycle());
            setProgramStatus(((FefuAdditionalProfessionalEducationProgram)another).getProgramStatus());
            setProgramPublicationStatus(((FefuAdditionalProfessionalEducationProgram)another).getProgramPublicationStatus());
            setOpeningProgramOrderDetails(((FefuAdditionalProfessionalEducationProgram)another).getOpeningProgramOrderDetails());
            setEducationLevel(((FefuAdditionalProfessionalEducationProgram)another).getEducationLevel());
            setDevelopForm(((FefuAdditionalProfessionalEducationProgram)another).getDevelopForm());
            setDevelopCondition(((FefuAdditionalProfessionalEducationProgram)another).getDevelopCondition());
            setDevelopTech(((FefuAdditionalProfessionalEducationProgram)another).getDevelopTech());
            setDevelopPeriod(((FefuAdditionalProfessionalEducationProgram)another).getDevelopPeriod());
            setIssuedDocument(((FefuAdditionalProfessionalEducationProgram)another).getIssuedDocument());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuAdditionalProfessionalEducationProgramGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuAdditionalProfessionalEducationProgram.class;
        }

        public T newInstance()
        {
            return (T) new FefuAdditionalProfessionalEducationProgram();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "title":
                    return obj.getTitle();
                case "specialtyGroup":
                    return obj.getSpecialtyGroup();
                case "programOpeningDate":
                    return obj.getProgramOpeningDate();
                case "programClosingDate":
                    return obj.getProgramClosingDate();
                case "producingOrgUnit":
                    return obj.getProducingOrgUnit();
                case "diplomaQualification":
                    return obj.getDiplomaQualification();
                case "diplomaType":
                    return obj.getDiplomaType();
                case "testUnits":
                    return obj.getTestUnits();
                case "hours":
                    return obj.getHours();
                case "fgosCycle":
                    return obj.getFgosCycle();
                case "programStatus":
                    return obj.getProgramStatus();
                case "programPublicationStatus":
                    return obj.getProgramPublicationStatus();
                case "openingProgramOrderDetails":
                    return obj.getOpeningProgramOrderDetails();
                case "educationLevel":
                    return obj.getEducationLevel();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "issuedDocument":
                    return obj.getIssuedDocument();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((OrgUnit) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "specialtyGroup":
                    obj.setSpecialtyGroup((String) value);
                    return;
                case "programOpeningDate":
                    obj.setProgramOpeningDate((Date) value);
                    return;
                case "programClosingDate":
                    obj.setProgramClosingDate((Date) value);
                    return;
                case "producingOrgUnit":
                    obj.setProducingOrgUnit((OrgUnit) value);
                    return;
                case "diplomaQualification":
                    obj.setDiplomaQualification((DiplomaQualifications) value);
                    return;
                case "diplomaType":
                    obj.setDiplomaType((FefuDiplomaTypeApe) value);
                    return;
                case "testUnits":
                    obj.setTestUnits((String) value);
                    return;
                case "hours":
                    obj.setHours((String) value);
                    return;
                case "fgosCycle":
                    obj.setFgosCycle((String) value);
                    return;
                case "programStatus":
                    obj.setProgramStatus((FefuProgramStatusApe) value);
                    return;
                case "programPublicationStatus":
                    obj.setProgramPublicationStatus((FefuProgramPublicationStatusApe) value);
                    return;
                case "openingProgramOrderDetails":
                    obj.setOpeningProgramOrderDetails((String) value);
                    return;
                case "educationLevel":
                    obj.setEducationLevel((EducationLevelAdditional) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((FefuDevelopFormApe) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((FefuDevelopConditionApe) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((FefuDevelopTechApe) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((FefuDevelopPeriodApe) value);
                    return;
                case "issuedDocument":
                    obj.setIssuedDocument((FefuIssuedDocumentApe) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "title":
                        return true;
                case "specialtyGroup":
                        return true;
                case "programOpeningDate":
                        return true;
                case "programClosingDate":
                        return true;
                case "producingOrgUnit":
                        return true;
                case "diplomaQualification":
                        return true;
                case "diplomaType":
                        return true;
                case "testUnits":
                        return true;
                case "hours":
                        return true;
                case "fgosCycle":
                        return true;
                case "programStatus":
                        return true;
                case "programPublicationStatus":
                        return true;
                case "openingProgramOrderDetails":
                        return true;
                case "educationLevel":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "issuedDocument":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "title":
                    return true;
                case "specialtyGroup":
                    return true;
                case "programOpeningDate":
                    return true;
                case "programClosingDate":
                    return true;
                case "producingOrgUnit":
                    return true;
                case "diplomaQualification":
                    return true;
                case "diplomaType":
                    return true;
                case "testUnits":
                    return true;
                case "hours":
                    return true;
                case "fgosCycle":
                    return true;
                case "programStatus":
                    return true;
                case "programPublicationStatus":
                    return true;
                case "openingProgramOrderDetails":
                    return true;
                case "educationLevel":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "issuedDocument":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "territorialOrgUnit":
                    return OrgUnit.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "title":
                    return String.class;
                case "specialtyGroup":
                    return String.class;
                case "programOpeningDate":
                    return Date.class;
                case "programClosingDate":
                    return Date.class;
                case "producingOrgUnit":
                    return OrgUnit.class;
                case "diplomaQualification":
                    return DiplomaQualifications.class;
                case "diplomaType":
                    return FefuDiplomaTypeApe.class;
                case "testUnits":
                    return String.class;
                case "hours":
                    return String.class;
                case "fgosCycle":
                    return String.class;
                case "programStatus":
                    return FefuProgramStatusApe.class;
                case "programPublicationStatus":
                    return FefuProgramPublicationStatusApe.class;
                case "openingProgramOrderDetails":
                    return String.class;
                case "educationLevel":
                    return EducationLevelAdditional.class;
                case "developForm":
                    return FefuDevelopFormApe.class;
                case "developCondition":
                    return FefuDevelopConditionApe.class;
                case "developTech":
                    return FefuDevelopTechApe.class;
                case "developPeriod":
                    return FefuDevelopPeriodApe.class;
                case "issuedDocument":
                    return FefuIssuedDocumentApe.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuAdditionalProfessionalEducationProgram> _dslPath = new Path<FefuAdditionalProfessionalEducationProgram>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuAdditionalProfessionalEducationProgram");
    }
            

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getTerritorialOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Тип программы ДПО/ДО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Наименование программы ДПО/ДО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Укрупненная группа специальностей.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getSpecialtyGroup()
     */
    public static PropertyPath<String> specialtyGroup()
    {
        return _dslPath.specialtyGroup();
    }

    /**
     * @return Дата открытия программы.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getProgramOpeningDate()
     */
    public static PropertyPath<Date> programOpeningDate()
    {
        return _dslPath.programOpeningDate();
    }

    /**
     * @return Дата закрытия программы.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getProgramClosingDate()
     */
    public static PropertyPath<Date> programClosingDate()
    {
        return _dslPath.programClosingDate();
    }

    /**
     * @return Выпускающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getProducingOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> producingOrgUnit()
    {
        return _dslPath.producingOrgUnit();
    }

    /**
     * @return Квалификация по диплому.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDiplomaQualification()
     */
    public static DiplomaQualifications.Path<DiplomaQualifications> diplomaQualification()
    {
        return _dslPath.diplomaQualification();
    }

    /**
     * @return Вид диплома.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDiplomaType()
     */
    public static FefuDiplomaTypeApe.Path<FefuDiplomaTypeApe> diplomaType()
    {
        return _dslPath.diplomaType();
    }

    /**
     * @return Зачетные единицы.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getTestUnits()
     */
    public static PropertyPath<String> testUnits()
    {
        return _dslPath.testUnits();
    }

    /**
     * @return Часы.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getHours()
     */
    public static PropertyPath<String> hours()
    {
        return _dslPath.hours();
    }

    /**
     * @return Цикл ФГОС, наименование, шифр, квалификационные или профессиональные требования.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getFgosCycle()
     */
    public static PropertyPath<String> fgosCycle()
    {
        return _dslPath.fgosCycle();
    }

    /**
     * @return Статус программы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getProgramStatus()
     */
    public static FefuProgramStatusApe.Path<FefuProgramStatusApe> programStatus()
    {
        return _dslPath.programStatus();
    }

    /**
     * @return Статус публикации программы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getProgramPublicationStatus()
     */
    public static FefuProgramPublicationStatusApe.Path<FefuProgramPublicationStatusApe> programPublicationStatus()
    {
        return _dslPath.programPublicationStatus();
    }

    /**
     * @return Реквизиты приказа об открытии программы.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getOpeningProgramOrderDetails()
     */
    public static PropertyPath<String> openingProgramOrderDetails()
    {
        return _dslPath.openingProgramOrderDetails();
    }

    /**
     * @return Уровень программы ДПО.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getEducationLevel()
     */
    public static EducationLevelAdditional.Path<EducationLevelAdditional> educationLevel()
    {
        return _dslPath.educationLevel();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDevelopForm()
     */
    public static FefuDevelopFormApe.Path<FefuDevelopFormApe> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условия освоения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDevelopCondition()
     */
    public static FefuDevelopConditionApe.Path<FefuDevelopConditionApe> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDevelopTech()
     */
    public static FefuDevelopTechApe.Path<FefuDevelopTechApe> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Сроки освоения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDevelopPeriod()
     */
    public static FefuDevelopPeriodApe.Path<FefuDevelopPeriodApe> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Выдаваемый документ.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getIssuedDocument()
     */
    public static FefuIssuedDocumentApe.Path<FefuIssuedDocumentApe> issuedDocument()
    {
        return _dslPath.issuedDocument();
    }

    public static class Path<E extends FefuAdditionalProfessionalEducationProgram> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private OrgUnit.Path<OrgUnit> _territorialOrgUnit;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private PropertyPath<String> _title;
        private PropertyPath<String> _specialtyGroup;
        private PropertyPath<Date> _programOpeningDate;
        private PropertyPath<Date> _programClosingDate;
        private OrgUnit.Path<OrgUnit> _producingOrgUnit;
        private DiplomaQualifications.Path<DiplomaQualifications> _diplomaQualification;
        private FefuDiplomaTypeApe.Path<FefuDiplomaTypeApe> _diplomaType;
        private PropertyPath<String> _testUnits;
        private PropertyPath<String> _hours;
        private PropertyPath<String> _fgosCycle;
        private FefuProgramStatusApe.Path<FefuProgramStatusApe> _programStatus;
        private FefuProgramPublicationStatusApe.Path<FefuProgramPublicationStatusApe> _programPublicationStatus;
        private PropertyPath<String> _openingProgramOrderDetails;
        private EducationLevelAdditional.Path<EducationLevelAdditional> _educationLevel;
        private FefuDevelopFormApe.Path<FefuDevelopFormApe> _developForm;
        private FefuDevelopConditionApe.Path<FefuDevelopConditionApe> _developCondition;
        private FefuDevelopTechApe.Path<FefuDevelopTechApe> _developTech;
        private FefuDevelopPeriodApe.Path<FefuDevelopPeriodApe> _developPeriod;
        private FefuIssuedDocumentApe.Path<FefuIssuedDocumentApe> _issuedDocument;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getTerritorialOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new OrgUnit.Path<OrgUnit>(L_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Тип программы ДПО/ДО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Наименование программы ДПО/ДО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FefuAdditionalProfessionalEducationProgramGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Укрупненная группа специальностей.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getSpecialtyGroup()
     */
        public PropertyPath<String> specialtyGroup()
        {
            if(_specialtyGroup == null )
                _specialtyGroup = new PropertyPath<String>(FefuAdditionalProfessionalEducationProgramGen.P_SPECIALTY_GROUP, this);
            return _specialtyGroup;
        }

    /**
     * @return Дата открытия программы.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getProgramOpeningDate()
     */
        public PropertyPath<Date> programOpeningDate()
        {
            if(_programOpeningDate == null )
                _programOpeningDate = new PropertyPath<Date>(FefuAdditionalProfessionalEducationProgramGen.P_PROGRAM_OPENING_DATE, this);
            return _programOpeningDate;
        }

    /**
     * @return Дата закрытия программы.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getProgramClosingDate()
     */
        public PropertyPath<Date> programClosingDate()
        {
            if(_programClosingDate == null )
                _programClosingDate = new PropertyPath<Date>(FefuAdditionalProfessionalEducationProgramGen.P_PROGRAM_CLOSING_DATE, this);
            return _programClosingDate;
        }

    /**
     * @return Выпускающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getProducingOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> producingOrgUnit()
        {
            if(_producingOrgUnit == null )
                _producingOrgUnit = new OrgUnit.Path<OrgUnit>(L_PRODUCING_ORG_UNIT, this);
            return _producingOrgUnit;
        }

    /**
     * @return Квалификация по диплому.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDiplomaQualification()
     */
        public DiplomaQualifications.Path<DiplomaQualifications> diplomaQualification()
        {
            if(_diplomaQualification == null )
                _diplomaQualification = new DiplomaQualifications.Path<DiplomaQualifications>(L_DIPLOMA_QUALIFICATION, this);
            return _diplomaQualification;
        }

    /**
     * @return Вид диплома.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDiplomaType()
     */
        public FefuDiplomaTypeApe.Path<FefuDiplomaTypeApe> diplomaType()
        {
            if(_diplomaType == null )
                _diplomaType = new FefuDiplomaTypeApe.Path<FefuDiplomaTypeApe>(L_DIPLOMA_TYPE, this);
            return _diplomaType;
        }

    /**
     * @return Зачетные единицы.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getTestUnits()
     */
        public PropertyPath<String> testUnits()
        {
            if(_testUnits == null )
                _testUnits = new PropertyPath<String>(FefuAdditionalProfessionalEducationProgramGen.P_TEST_UNITS, this);
            return _testUnits;
        }

    /**
     * @return Часы.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getHours()
     */
        public PropertyPath<String> hours()
        {
            if(_hours == null )
                _hours = new PropertyPath<String>(FefuAdditionalProfessionalEducationProgramGen.P_HOURS, this);
            return _hours;
        }

    /**
     * @return Цикл ФГОС, наименование, шифр, квалификационные или профессиональные требования.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getFgosCycle()
     */
        public PropertyPath<String> fgosCycle()
        {
            if(_fgosCycle == null )
                _fgosCycle = new PropertyPath<String>(FefuAdditionalProfessionalEducationProgramGen.P_FGOS_CYCLE, this);
            return _fgosCycle;
        }

    /**
     * @return Статус программы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getProgramStatus()
     */
        public FefuProgramStatusApe.Path<FefuProgramStatusApe> programStatus()
        {
            if(_programStatus == null )
                _programStatus = new FefuProgramStatusApe.Path<FefuProgramStatusApe>(L_PROGRAM_STATUS, this);
            return _programStatus;
        }

    /**
     * @return Статус публикации программы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getProgramPublicationStatus()
     */
        public FefuProgramPublicationStatusApe.Path<FefuProgramPublicationStatusApe> programPublicationStatus()
        {
            if(_programPublicationStatus == null )
                _programPublicationStatus = new FefuProgramPublicationStatusApe.Path<FefuProgramPublicationStatusApe>(L_PROGRAM_PUBLICATION_STATUS, this);
            return _programPublicationStatus;
        }

    /**
     * @return Реквизиты приказа об открытии программы.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getOpeningProgramOrderDetails()
     */
        public PropertyPath<String> openingProgramOrderDetails()
        {
            if(_openingProgramOrderDetails == null )
                _openingProgramOrderDetails = new PropertyPath<String>(FefuAdditionalProfessionalEducationProgramGen.P_OPENING_PROGRAM_ORDER_DETAILS, this);
            return _openingProgramOrderDetails;
        }

    /**
     * @return Уровень программы ДПО.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getEducationLevel()
     */
        public EducationLevelAdditional.Path<EducationLevelAdditional> educationLevel()
        {
            if(_educationLevel == null )
                _educationLevel = new EducationLevelAdditional.Path<EducationLevelAdditional>(L_EDUCATION_LEVEL, this);
            return _educationLevel;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDevelopForm()
     */
        public FefuDevelopFormApe.Path<FefuDevelopFormApe> developForm()
        {
            if(_developForm == null )
                _developForm = new FefuDevelopFormApe.Path<FefuDevelopFormApe>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условия освоения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDevelopCondition()
     */
        public FefuDevelopConditionApe.Path<FefuDevelopConditionApe> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new FefuDevelopConditionApe.Path<FefuDevelopConditionApe>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDevelopTech()
     */
        public FefuDevelopTechApe.Path<FefuDevelopTechApe> developTech()
        {
            if(_developTech == null )
                _developTech = new FefuDevelopTechApe.Path<FefuDevelopTechApe>(L_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Сроки освоения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getDevelopPeriod()
     */
        public FefuDevelopPeriodApe.Path<FefuDevelopPeriodApe> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new FefuDevelopPeriodApe.Path<FefuDevelopPeriodApe>(L_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Выдаваемый документ.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram#getIssuedDocument()
     */
        public FefuIssuedDocumentApe.Path<FefuIssuedDocumentApe> issuedDocument()
        {
            if(_issuedDocument == null )
                _issuedDocument = new FefuIssuedDocumentApe.Path<FefuIssuedDocumentApe>(L_ISSUED_DOCUMENT, this);
            return _issuedDocument;
        }

        public Class getEntityClass()
        {
            return FefuAdditionalProfessionalEducationProgram.class;
        }

        public String getEntityName()
        {
            return "fefuAdditionalProfessionalEducationProgram";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
