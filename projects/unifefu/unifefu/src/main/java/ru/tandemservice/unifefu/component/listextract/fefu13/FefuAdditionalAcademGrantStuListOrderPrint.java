package ru.tandemservice.unifefu.component.listextract.fefu13;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e86.utils.EmployeePostDecl;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.component.listextract.fefu13.utils.FefuAdditionalAcademGrantExtractWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu13.utils.FefuAdditionalAcademGrantParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public class FefuAdditionalAcademGrantStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<FefuAdditionalAcademGrantStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        FefuAdditionalAcademGrantStuListExtract extract = extracts.get(0);
        List<FefuAdditionalAcademGrantParagraphWrapper> paragraphWrapperList = prepareParagraphsStructure(extracts);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extract);

        injectModifier.put("protocolData", " " + extract.getProtocolNumber());
        injectModifier.put("responsibleForPayments", StringUtils.capitalize(EmployeePostDecl.getEmployeeStrDat(extract.getResponsibleForPayments())));
        injectModifier.put("responsibleForAgreement", EmployeePostDecl.getEmployeeStrInst(extract.getResponsibleForAgreement()));
        injectModifier.put("matchingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getMatchingDate()));

        int num = paragraphWrapperList.size();
        injectModifier.put("numberPrevEnd", String.valueOf(++num));
        injectModifier.put("numberEnd", String.valueOf(++num));

        List<String> activities = new ArrayList<>(paragraphWrapperList.size());
        for (FefuAdditionalAcademGrantParagraphWrapper par : paragraphWrapperList)
        {
            activities.add(DeclinableManager.instance().dao().getPropertyValue(par.getPromotedActivityType(), "title", IUniBaseDao.instance.get().getByCode(InflectorVariant.class, InflectorVariantCodes.RU_GENITIVE)));
        }

        injectModifier.put("promotedActivityType_GF", CoreStringUtils.join(activities, ", "));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, paragraphWrapperList);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<FefuAdditionalAcademGrantParagraphWrapper> prepareParagraphsStructure(List<FefuAdditionalAcademGrantStuListExtract> extracts)
    {
        List<FefuAdditionalAcademGrantParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        int ind;
        for (FefuAdditionalAcademGrantStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = student.getPerson();

            Group group = student.getGroup();

            FefuAdditionalAcademGrantParagraphWrapper paragraphWrapper = new FefuAdditionalAcademGrantParagraphWrapper(extract.getPromotedActivityType(), extract.getDateStart(), extract.getDateEnd(), extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            paragraphWrapper.getExtractWrapperList().add(new FefuAdditionalAcademGrantExtractWrapper(person, extract.getAdditionalAcademGrantSize(), group));
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<FefuAdditionalAcademGrantParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            int parNumber = 0;
            for (FefuAdditionalAcademGrantParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_ADDITIONAL_ACAD_GRANT_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());

                paragraphInjectModifier.put("promotedActivityType_GF", DeclinableManager.instance().dao().getPropertyValue(paragraphWrapper.getPromotedActivityType(), "title", IUniBaseDao.instance.get().getByCode(InflectorVariant.class, InflectorVariantCodes.RU_GENITIVE)));
                paragraphInjectModifier.put("dateStart", DateFormatter.DEFAULT_DATE_FORMATTER.format(paragraphWrapper.getDateStart()));
                paragraphInjectModifier.put("dateEnd", DateFormatter.DEFAULT_DATE_FORMATTER.format(paragraphWrapper.getDateEnd()));

                paragraphInjectModifier.put("parNumber", String.valueOf(++parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");

                paragraphInjectModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getExtractWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<FefuAdditionalAcademGrantExtractWrapper> extractWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(extractWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            // Получаем шаблон подпараграфа
            byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_ADDITIONAL_ACAD_GRANT_LIST_EXTRACT), 3);
            RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

            RtfTableModifier paragraphPartTableModifier = new RtfTableModifier();

            int j = 0;
            String[][] tableData = new String[extractWrappers.size()][];
            for (FefuAdditionalAcademGrantExtractWrapper extractWrapper : extractWrappers)
            {
                tableData[j++] = new String[]{String.valueOf(j) + ".", extractWrapper.getPerson().getFullFio(), extractWrapper.getGroup().getTitle(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(extractWrapper.getGrantSize()) + " руб."};
            }

            paragraphPartTableModifier.put("STUDENTS_TABLE", tableData);
            paragraphPartTableModifier.modify(paragraphPart);

            IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
            rtfGroup.setElementList(paragraphPart.getElementList());
            parList.add(rtfGroup);


            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}