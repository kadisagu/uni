/* $Id $ */
package ru.tandemservice.unifefu.base.ext.SessionTransfer;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Rostuncev Savva
 * @since 20.03.2014
 */
@Configuration
public class SessionTransferExtManager extends BusinessObjectExtensionManager
{
}
