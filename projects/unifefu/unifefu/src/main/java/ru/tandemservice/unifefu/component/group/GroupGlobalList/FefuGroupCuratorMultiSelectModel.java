package ru.tandemservice.unifefu.component.group.GroupGlobalList;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Stanislav Shibarshin
 * @since 07.11.2016
 */
class FefuGroupCuratorMultiSelectModel extends BaseMultiSelectModel
{

    private ru.tandemservice.uni.component.group.GroupGlobalList.Model _model;

    FefuGroupCuratorMultiSelectModel(ru.tandemservice.uni.component.group.GroupGlobalList.Model model)
    {
        super("id", new String[] {Employee.P_FULL_FIO});
        this._model = model;
    }

    @Override
    public List getValues(Set primaryKeys)
    {
        return UniDaoFacade.getCoreDao().getList(createBuilder(null, primaryKeys));
    }

    @Override
    public ListResult findValues(String filter)
    {
        DQLSelectBuilder builder = createBuilder(filter, null);
        return new ListResult<>(UniDaoFacade.getCoreDao().getList(builder, 0, 50), UniDaoFacade.getCoreDao().getCount(builder));
    }

    private DQLSelectBuilder createBuilder(String filter, Set primaryKeys)
    {
        DQLSelectBuilder employeeIdsBuilder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep")
                .column(property("ep", EmployeePost.employee().id()));

        final YesNoWrapper archival = _model.getSettings().get("archival");
        if (archival != null)
            employeeIdsBuilder.where(exists(Group.class, Group.curator().s(), property("ep"), Group.archival().s(), value(archival.isTrue())));
         else
            employeeIdsBuilder.where(exists(Group.class, Group.curator().s(), property("ep")));;


        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Employee.class, "employee")
                .where(in(property("employee", Employee.id()), employeeIdsBuilder.buildQuery()));
        if (CollectionUtils.isNotEmpty(primaryKeys)) {
            builder.where(in(property("employee", Employee.id()), primaryKeys));
        }
        if (StringUtils.isNotEmpty(filter))
            builder.where(likeUpper(property("employee", Employee.person().identityCard().fullFio()), value(CoreStringUtils.escapeLike(filter, true))));

        builder.order(property("employee", Employee.person().identityCard().fullFio()));

        return builder;
    }
}
