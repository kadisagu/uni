/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu18.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.StuffCompensationStuExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2014
 */
public class DAO extends ModularStudentExtractPubDAO<StuffCompensationStuExtract, Model> implements IDAO
{
	@Override
	public void prepare(Model model)
	{
		super.prepare(model);
		model.setCompensationSum(model.getExtract().getCompensationSum() / 100D);
		model.setImmediateSum(model.getExtract().getImmediateSum() / 100D);
	}
}
