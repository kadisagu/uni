/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.node;

import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.unifefu.entity.FefuSpeciality;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.validator.Validator;

import java.util.*;

/**
 * Специальность ИМЦА.
 * @author Alexander Zhebko
 * @since 24.07.2013
 */
public class ImtsaSpeciality
{
    // представления
    public static IAttributeView ATTRIBUTE_VIEW = new SpecialityAttributeView();
    public static INodeView NODE_VIEW = new SpecialityNodeView();


    // загружаемые данные
    private final Map<String, Object> _attributes;
    public Map<String, Object> getAttributes(){ return _attributes; }

    private ImtsaSpeciality(Map<String, Object> attributes)
    {
        _attributes = attributes;
    }

    public static ImtsaSpeciality createSpeciality(Map<String, Object> attributes)
    {
        return new ImtsaSpeciality(attributes);
    }


    // вспомогательные данные
    public static Map<String, String> getAttributePropertyMap(){ return SpecialityAttributeView.ATTRIBUTE_PROPERTY_MAP; }


    /**
     * Представление атрибутов нода "Специальность".
     */
    private static class SpecialityAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String A_NUMBER = "Ном";
        private static final String A_TITLE = "Название";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(A_NUMBER, Integer.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                A_NUMBER,
                A_TITLE
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(A_NUMBER, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Collections.emptyList();


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Arrays.asList(
                A_NUMBER
        );


        // 7. поля сущности
        private static final String P_NUMBER = FefuSpeciality.P_NUMBER;
        private static final String P_TITLE = FefuSpeciality.P_TITLE;


        // 8. отображение названия атрибута на название поля сущности
        private static final Map<String, String> ATTRIBUTE_PROPERTY_MAP = new HashMap<>();
        static
        {
            ATTRIBUTE_PROPERTY_MAP.put(A_NUMBER, P_NUMBER);
            ATTRIBUTE_PROPERTY_MAP.put(A_TITLE, P_TITLE);
        }
    }


    /**
     * Представление нода "Специальность".
     */
    private static class SpecialityNodeView implements INodeView
    {
        @Override
        public String getNodeName()
        {
            return SPECIALITY_NODE_NAME;
        }

        @Override
        public IAttributeView getAttributeView()
        {
            return ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return SpecialityAttributeView.UNIQUE_ATTRIBUTES;
        }


        // ноды
        private static final String SPECIALITY_NODE_NAME = "Специальность";
    }
}