/* $Id: SendPracticeOutStuListExtract.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IPracticeOutExtract;
import ru.tandemservice.unifefu.entity.gen.SendPracticeOutStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «О направлении на практику за пределами ОУ»
 */
public class SendPracticeOutStuListExtract extends SendPracticeOutStuListExtractGen implements IPracticeOutExtract
{
    @Override
    public Date getBeginDate()
    {
        return getPracticeBeginDate();
    }

    @Override
    public Date getEndDate()
    {
        return getPracticeEndDate();
    }

    @Override
    public String getPracticePlace()
    {
        return getPracticeExtOrgUnit() != null ? getPracticeExtOrgUnit().getTitle() : null;
    }
}