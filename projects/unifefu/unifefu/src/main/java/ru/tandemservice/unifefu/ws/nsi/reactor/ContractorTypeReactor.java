/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.ws.FefuContractor;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.ContractorType;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Dmitry Seleznev
 * @since 14.05.2014
 */
public class ContractorTypeReactor extends SimpleCatalogEntityNewReactor<ContractorType, FefuContractor>
{
    private Map<String, Person> _guidToPersonMap = new HashMap<>();
    private Map<Long, String> _personIdToGuidMap = new HashMap<>();

    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return false;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<FefuContractor> getEntityClass()
    {
        return FefuContractor.class;
    }

    @Override
    public Class<ContractorType> getNSIEntityClass()
    {
        return ContractorType.class;
    }

    @Override
    public ContractorType getCatalogElementRetrieveRequestObject(String guid)
    {
        // TODO: Имеет смысл переопределить метод получения полного списка контрагентов из НСИ, поскольку их может быть очень много.
        ContractorType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createContractorType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    protected void prepareRelatedObjectsMap(Set<Long> entityIds)
    {
        super.prepareRelatedObjectsMap(entityIds);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Person.class, "p").column("p")
                .joinEntity("p", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(Person.id().fromAlias("p")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .column(property(FefuNsiIds.guid().fromAlias("ids")));

        List<Object[]> persons = DataAccessServices.dao().getList(builder);
        for(Object[] item : persons)
        {
            String guid = (String) item[1];
            Person person = (Person) item[0];
            _personIdToGuidMap.put(person.getId(), guid);
            _guidToPersonMap.put(guid, person);
        }
    }

    @Override
    public ContractorType createEntity(CoreCollectionUtils.Pair<FefuContractor, FefuNsiIds> entityPair)
    {
        ContractorType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createContractorType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setContractorID(entityPair.getX().getContractorID());
        nsiEntity.setContractorNameFull(entityPair.getX().getNameFull());
        nsiEntity.setContractorINN(entityPair.getX().getInn());
        nsiEntity.setContractorKPP(entityPair.getX().getKpp());
        nsiEntity.setContractorOGRN(entityPair.getX().getOgrn());
        nsiEntity.setContractorOKPO(entityPair.getX().getOkpo());

        if(_personIdToGuidMap.containsKey(entityPair.getX().getPerson().getId()))
        {
            ContractorType.HumanID humanId = NsiReactorUtils.NSI_OBJECT_FACTORY.createContractorTypeHumanID();
            HumanType human = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanType();
            human.setID(_personIdToGuidMap.get(entityPair.getX().getPerson().getId()));
            humanId.setHuman(human);
            nsiEntity.setHumanID(humanId);
        }

        return nsiEntity;
    }

    @Override
    public FefuContractor createEntity(ContractorType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getContractorNameFull() || null == nsiEntity.getHumanID()) return null;
        if(!_guidToPersonMap.containsKey(nsiEntity.getHumanID().getHuman().getID())) return null;

        FefuContractor entity = new FefuContractor();
        entity.setContractorID(nsiEntity.getContractorID());
        entity.setNameFull(nsiEntity.getContractorNameFull());
        entity.setInn(nsiEntity.getContractorINN());
        entity.setKpp(nsiEntity.getContractorKPP());
        entity.setOgrn(nsiEntity.getContractorOGRN());
        entity.setOkpo(nsiEntity.getContractorOKPO());
        entity.setPerson(_guidToPersonMap.get(nsiEntity.getHumanID().getHuman().getID()));

        return entity;
    }

    @Override
    public FefuContractor updatePossibleDuplicateFields(ContractorType nsiEntity, CoreCollectionUtils.Pair<FefuContractor, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuContractor.contractorID().s()))
                entityPair.getX().setContractorID(nsiEntity.getContractorID());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuContractor.nameFull().s()))
                entityPair.getX().setNameFull(nsiEntity.getContractorNameFull());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuContractor.inn().s()))
                entityPair.getX().setInn(nsiEntity.getContractorINN());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuContractor.kpp().s()))
                entityPair.getX().setKpp(nsiEntity.getContractorKPP());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuContractor.ogrn().s()))
                entityPair.getX().setOgrn(nsiEntity.getContractorOGRN());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuContractor.okpo().s()))
                entityPair.getX().setOkpo(nsiEntity.getContractorOKPO());
        }
        return entityPair.getX();
    }

    @Override
    public ContractorType updateNsiEntityFields(ContractorType nsiEntity, FefuContractor entity)
    {
        nsiEntity.setContractorID(entity.getContractorID());
        nsiEntity.setContractorNameFull(entity.getNameFull());
        nsiEntity.setContractorINN(entity.getInn());
        nsiEntity.setContractorKPP(entity.getKpp());
        nsiEntity.setContractorOGRN(entity.getOgrn());
        nsiEntity.setContractorOKPO(entity.getOkpo());
        return nsiEntity;
    }
}