/**
 * Period.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.mobile.test;

public class Period  implements java.io.Serializable {
    private java.lang.String id;

    private java.lang.String academic_group_id;

    private java.lang.String type;

    private java.lang.String date;

    private java.lang.String date_end;

    private java.lang.String date_start;

    private java.lang.String time_end;

    private java.lang.String time_start;

    private java.lang.String place;

    private java.lang.String lecturer;

    private java.lang.String position;

    private java.lang.String day;

    private java.lang.String week;

    private java.lang.String subject_id;

    private java.lang.String subject_title;

    private java.lang.String subject_short_title;

    public Period() {
    }

    public Period(
           java.lang.String id,
           java.lang.String academic_group_id,
           java.lang.String type,
           java.lang.String date,
           java.lang.String date_end,
           java.lang.String date_start,
           java.lang.String time_end,
           java.lang.String time_start,
           java.lang.String place,
           java.lang.String lecturer,
           java.lang.String position,
           java.lang.String day,
           java.lang.String week,
           java.lang.String subject_id,
           java.lang.String subject_title,
           java.lang.String subject_short_title) {
           this.id = id;
           this.academic_group_id = academic_group_id;
           this.type = type;
           this.date = date;
           this.date_end = date_end;
           this.date_start = date_start;
           this.time_end = time_end;
           this.time_start = time_start;
           this.place = place;
           this.lecturer = lecturer;
           this.position = position;
           this.day = day;
           this.week = week;
           this.subject_id = subject_id;
           this.subject_title = subject_title;
           this.subject_short_title = subject_short_title;
    }


    /**
     * Gets the id value for this Period.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this Period.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the academic_group_id value for this Period.
     * 
     * @return academic_group_id
     */
    public java.lang.String getAcademic_group_id() {
        return academic_group_id;
    }


    /**
     * Sets the academic_group_id value for this Period.
     * 
     * @param academic_group_id
     */
    public void setAcademic_group_id(java.lang.String academic_group_id) {
        this.academic_group_id = academic_group_id;
    }


    /**
     * Gets the type value for this Period.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this Period.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }


    /**
     * Gets the date value for this Period.
     * 
     * @return date
     */
    public java.lang.String getDate() {
        return date;
    }


    /**
     * Sets the date value for this Period.
     * 
     * @param date
     */
    public void setDate(java.lang.String date) {
        this.date = date;
    }


    /**
     * Gets the date_end value for this Period.
     * 
     * @return date_end
     */
    public java.lang.String getDate_end() {
        return date_end;
    }


    /**
     * Sets the date_end value for this Period.
     * 
     * @param date_end
     */
    public void setDate_end(java.lang.String date_end) {
        this.date_end = date_end;
    }


    /**
     * Gets the date_start value for this Period.
     * 
     * @return date_start
     */
    public java.lang.String getDate_start() {
        return date_start;
    }


    /**
     * Sets the date_start value for this Period.
     * 
     * @param date_start
     */
    public void setDate_start(java.lang.String date_start) {
        this.date_start = date_start;
    }


    /**
     * Gets the time_end value for this Period.
     * 
     * @return time_end
     */
    public java.lang.String getTime_end() {
        return time_end;
    }


    /**
     * Sets the time_end value for this Period.
     * 
     * @param time_end
     */
    public void setTime_end(java.lang.String time_end) {
        this.time_end = time_end;
    }


    /**
     * Gets the time_start value for this Period.
     * 
     * @return time_start
     */
    public java.lang.String getTime_start() {
        return time_start;
    }


    /**
     * Sets the time_start value for this Period.
     * 
     * @param time_start
     */
    public void setTime_start(java.lang.String time_start) {
        this.time_start = time_start;
    }


    /**
     * Gets the place value for this Period.
     * 
     * @return place
     */
    public java.lang.String getPlace() {
        return place;
    }


    /**
     * Sets the place value for this Period.
     * 
     * @param place
     */
    public void setPlace(java.lang.String place) {
        this.place = place;
    }


    /**
     * Gets the lecturer value for this Period.
     * 
     * @return lecturer
     */
    public java.lang.String getLecturer() {
        return lecturer;
    }


    /**
     * Sets the lecturer value for this Period.
     * 
     * @param lecturer
     */
    public void setLecturer(java.lang.String lecturer) {
        this.lecturer = lecturer;
    }


    /**
     * Gets the position value for this Period.
     * 
     * @return position
     */
    public java.lang.String getPosition() {
        return position;
    }


    /**
     * Sets the position value for this Period.
     * 
     * @param position
     */
    public void setPosition(java.lang.String position) {
        this.position = position;
    }


    /**
     * Gets the day value for this Period.
     * 
     * @return day
     */
    public java.lang.String getDay() {
        return day;
    }


    /**
     * Sets the day value for this Period.
     * 
     * @param day
     */
    public void setDay(java.lang.String day) {
        this.day = day;
    }


    /**
     * Gets the week value for this Period.
     * 
     * @return week
     */
    public java.lang.String getWeek() {
        return week;
    }


    /**
     * Sets the week value for this Period.
     * 
     * @param week
     */
    public void setWeek(java.lang.String week) {
        this.week = week;
    }


    /**
     * Gets the subject_id value for this Period.
     * 
     * @return subject_id
     */
    public java.lang.String getSubject_id() {
        return subject_id;
    }


    /**
     * Sets the subject_id value for this Period.
     * 
     * @param subject_id
     */
    public void setSubject_id(java.lang.String subject_id) {
        this.subject_id = subject_id;
    }


    /**
     * Gets the subject_title value for this Period.
     * 
     * @return subject_title
     */
    public java.lang.String getSubject_title() {
        return subject_title;
    }


    /**
     * Sets the subject_title value for this Period.
     * 
     * @param subject_title
     */
    public void setSubject_title(java.lang.String subject_title) {
        this.subject_title = subject_title;
    }


    /**
     * Gets the subject_short_title value for this Period.
     * 
     * @return subject_short_title
     */
    public java.lang.String getSubject_short_title() {
        return subject_short_title;
    }


    /**
     * Sets the subject_short_title value for this Period.
     * 
     * @param subject_short_title
     */
    public void setSubject_short_title(java.lang.String subject_short_title) {
        this.subject_short_title = subject_short_title;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Period)) return false;
        Period other = (Period) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.academic_group_id==null && other.getAcademic_group_id()==null) || 
             (this.academic_group_id!=null &&
              this.academic_group_id.equals(other.getAcademic_group_id()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.date==null && other.getDate()==null) || 
             (this.date!=null &&
              this.date.equals(other.getDate()))) &&
            ((this.date_end==null && other.getDate_end()==null) || 
             (this.date_end!=null &&
              this.date_end.equals(other.getDate_end()))) &&
            ((this.date_start==null && other.getDate_start()==null) || 
             (this.date_start!=null &&
              this.date_start.equals(other.getDate_start()))) &&
            ((this.time_end==null && other.getTime_end()==null) || 
             (this.time_end!=null &&
              this.time_end.equals(other.getTime_end()))) &&
            ((this.time_start==null && other.getTime_start()==null) || 
             (this.time_start!=null &&
              this.time_start.equals(other.getTime_start()))) &&
            ((this.place==null && other.getPlace()==null) || 
             (this.place!=null &&
              this.place.equals(other.getPlace()))) &&
            ((this.lecturer==null && other.getLecturer()==null) || 
             (this.lecturer!=null &&
              this.lecturer.equals(other.getLecturer()))) &&
            ((this.position==null && other.getPosition()==null) || 
             (this.position!=null &&
              this.position.equals(other.getPosition()))) &&
            ((this.day==null && other.getDay()==null) || 
             (this.day!=null &&
              this.day.equals(other.getDay()))) &&
            ((this.week==null && other.getWeek()==null) || 
             (this.week!=null &&
              this.week.equals(other.getWeek()))) &&
            ((this.subject_id==null && other.getSubject_id()==null) || 
             (this.subject_id!=null &&
              this.subject_id.equals(other.getSubject_id()))) &&
            ((this.subject_title==null && other.getSubject_title()==null) || 
             (this.subject_title!=null &&
              this.subject_title.equals(other.getSubject_title()))) &&
            ((this.subject_short_title==null && other.getSubject_short_title()==null) || 
             (this.subject_short_title!=null &&
              this.subject_short_title.equals(other.getSubject_short_title())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getAcademic_group_id() != null) {
            _hashCode += getAcademic_group_id().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getDate() != null) {
            _hashCode += getDate().hashCode();
        }
        if (getDate_end() != null) {
            _hashCode += getDate_end().hashCode();
        }
        if (getDate_start() != null) {
            _hashCode += getDate_start().hashCode();
        }
        if (getTime_end() != null) {
            _hashCode += getTime_end().hashCode();
        }
        if (getTime_start() != null) {
            _hashCode += getTime_start().hashCode();
        }
        if (getPlace() != null) {
            _hashCode += getPlace().hashCode();
        }
        if (getLecturer() != null) {
            _hashCode += getLecturer().hashCode();
        }
        if (getPosition() != null) {
            _hashCode += getPosition().hashCode();
        }
        if (getDay() != null) {
            _hashCode += getDay().hashCode();
        }
        if (getWeek() != null) {
            _hashCode += getWeek().hashCode();
        }
        if (getSubject_id() != null) {
            _hashCode += getSubject_id().hashCode();
        }
        if (getSubject_title() != null) {
            _hashCode += getSubject_title().hashCode();
        }
        if (getSubject_short_title() != null) {
            _hashCode += getSubject_short_title().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Period.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "Period"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("academic_group_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "academic_group_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("date_end");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "date_end"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("date_start");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "date_start"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("time_end");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "time_end"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("time_start");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "time_start"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("place");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "place"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lecturer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "lecturer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("position");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "position"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("day");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "day"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("week");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "week"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subject_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "subject_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subject_title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "subject_title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subject_short_title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "subject_short_title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
