/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu5.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuCourseTransferStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuCourseTransferStuListExtract, Model>
{
}
