/* $Id: IDAO.java 38584 2014-10-08 11:05:36Z azhebko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.listextract.fefu1.ParagraphAddEdit;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.SendPracticeOutStuListExtract;


/**
 * @author ListExtractComponentGenerator
 * @since 13.02.2013
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<SendPracticeOutStuListExtract, Model>
{
    PersonAcademicDegree getAcademicDegree(EmployeePost employeePost);
}
