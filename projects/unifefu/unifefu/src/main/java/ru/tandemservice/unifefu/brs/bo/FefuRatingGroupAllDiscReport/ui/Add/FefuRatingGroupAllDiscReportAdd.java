package ru.tandemservice.unifefu.brs.bo.FefuRatingGroupAllDiscReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;

/**
 * User: amakarova
 * Date: 23.12.13
 */
@Configuration
public class FefuRatingGroupAllDiscReportAdd extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(FefuBrsReportManager.instance().orgUnitDSConfig())
                .addDataSource(FefuBrsReportManager.instance().yearPartDSConfig())
                .addDataSource(FefuBrsReportManager.instance().groupDSConfig())
                .addDataSource(FefuBrsReportManager.instance().yesNoDSConfig())
                .addDataSource(FefuBrsReportManager.instance().responsibilityOrgUnitDSConfig())
                .create();
    }
}