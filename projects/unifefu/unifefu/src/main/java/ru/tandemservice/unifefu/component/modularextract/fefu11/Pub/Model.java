/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu11.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 25.04.2013
 */
public class Model extends ModularStudentExtractPubModel<FefuAcadGrantAssignStuEnrolmentExtract>
{
}