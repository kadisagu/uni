package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О предоставлении каникул (ДВФУ)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuHolidayStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract";
    public static final String ENTITY_NAME = "fefuHolidayStuListExtract";
    public static final int VERSION_HASH = -77831042;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private Date _beginDate;     // Дата начала каникул
    private Date _endDate;     // Дата окончания каникул

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Дата начала каникул. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала каникул. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания каникул. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания каникул. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuHolidayStuListExtractGen)
        {
            setCourse(((FefuHolidayStuListExtract)another).getCourse());
            setGroup(((FefuHolidayStuListExtract)another).getGroup());
            setBeginDate(((FefuHolidayStuListExtract)another).getBeginDate());
            setEndDate(((FefuHolidayStuListExtract)another).getEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuHolidayStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuHolidayStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuHolidayStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuHolidayStuListExtract> _dslPath = new Path<FefuHolidayStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuHolidayStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Дата начала каникул. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания каникул. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    public static class Path<E extends FefuHolidayStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Дата начала каникул. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(FefuHolidayStuListExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания каникул. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(FefuHolidayStuListExtractGen.P_END_DATE, this);
            return _endDate;
        }

        public Class getEntityClass()
        {
            return FefuHolidayStuListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuHolidayStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
