package ru.tandemservice.unifefu.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.fias.base.entity.AddressInter;
import org.tandemframework.shared.fias.base.entity.AddressRu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 15.01.2015
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.16"),
                        new ScriptDependency("org.tandemframework.shared", "1.7.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<Long, Long> personIdToAddrIdMap = new HashMap<>();
        Map<Long, Long> personIdToNsiContactIdMap = new HashMap<>();
        Map<Long, Long> icIdToAddrIdMap = new HashMap<>();
        Map<Long, Long> icIdToNsiContactIdMap = new HashMap<>();

        // Инициализируем наборы идентификаторов объектов, на которые будем ссылаться (для проверки их наличия в базе
        Set<Long> personIdSet = getEntityIdSet(tool, "person_t");
        Set<Long> iCardIdSet = getEntityIdSet(tool, "identitycard_t");
        Set<Long> countryIdSet = getEntityIdSet(tool, "addresscountry_t");
        Set<Long> addrItemIdSet = getEntityIdSet(tool, "addressitem_t");
        Set<Long> districtIdSet = getEntityIdSet(tool, "addressdistrict_t");
        Set<Long> streetIdSet = getEntityIdSet(tool, "addressstreet_t");

        // Забираем текущие адреса персоны
        Statement perStmt = tool.getConnection().createStatement();
        perStmt.execute("select p.id, a.id, c.id from addressstring_t a inner join person_t p on a.id=p.address_id inner join fefunsipersoncontact_t c on a.id=c.address_id");
        ResultSet perRs = perStmt.getResultSet();

        while (perRs.next())
        {
            personIdToAddrIdMap.put(perRs.getLong(1), perRs.getLong(2));
            personIdToNsiContactIdMap.put(perRs.getLong(1), perRs.getLong(3));
        }

        // Забираем текущие адреса удостоверений личности
        Statement icStmt = tool.getConnection().createStatement();
        icStmt.execute("select ic.id, a.id, c.id from addressstring_t a inner join identitycard_t ic on a.id=ic.address_id inner join fefunsipersoncontact_t c on a.id=c.address_id");
        ResultSet icRs = icStmt.getResultSet();

        while (icRs.next())
        {
            icIdToAddrIdMap.put(icRs.getLong(1), icRs.getLong(2));
            icIdToNsiContactIdMap.put(icRs.getLong(1), icRs.getLong(3));
        }

        icStmt.close();
        perStmt.close();

        // Подгружаем список реальных кладровых адресов, выгруженный ранее из бэкапа
        String filePath = ApplicationRuntime.getAppInstallPath();
        File file = new File(filePath, "data/addr1.txt");
        if (!file.exists())
        {
            System.out.println("Импорт провести не удалось: не найден файл «addr1.txt» с исходными данными в каталоге «${app.install.path}/data/». Миграция пропущена");
            return;
        }

        if (!file.canRead())
        {
            System.out.println("Импорт провести не удалось: файл «${app.install.path}/data/addr1.txt» не доступен для чтения. Миграция пропущена.");
            return;
        }

        // Формируем список объектов для сохранения в БД
        List<AddressWrapper> addWrappers = new ArrayList<>();

        try
        {
            String line;
            String[] parts;

            BufferedReader reader = new BufferedReader(new FileReader(file.getAbsolutePath()));

            while ((line = reader.readLine()) != null)
            {
                boolean invalid = false;
                AddressWrapper wrapper = null;

                line = line + "  ";
                parts = line.split("; ");
                String addrId = parts[2];

                if (parts.length > 9)
                {
                    System.out.println("ru\t" + line);
                    wrapper = new AddressRuWrapper(parts);

                } else
                {
                    wrapper = new AddressInterWrapper(parts);
                    System.out.println("inter\t" + line);
                }

                if (null != wrapper)
                {
                    if (null != wrapper.getPersonId())
                        invalid = validateField(addrId, wrapper.getPersonId(), personIdSet, false, "Person", invalid);

                    if (null != wrapper.getIcId())
                        invalid = validateField(addrId, wrapper.getIcId(), iCardIdSet, false, "IdentityCard", invalid);

                    invalid = validateField(addrId, wrapper.getCountryId(), countryIdSet, false, "Country", invalid);
                    invalid = validateField(addrId, wrapper.getSettlementId(), addrItemIdSet, true, "Settlement", invalid);

                    if (wrapper instanceof AddressRuWrapper)
                    {
                        invalid = validateField(addrId, ((AddressRuWrapper) wrapper).getStreetId(), streetIdSet, true, "Street", invalid);
                        invalid = validateField(addrId, ((AddressRuWrapper) wrapper).getDistrictId(), streetIdSet, true, "District", invalid);
                    }

                    if (!invalid) addWrappers.add(wrapper);

                }
            }
        } catch (Exception x)
        {
            throw new RuntimeException("Ошибка чтения файла с адресами персон (IOException): %s%n. Миграция пропущена.", x);
        }

        short adRuDiscriminator = tool.entityCodes().get(AddressRu.ENTITY_NAME);
        short adIntDiscriminator = tool.entityCodes().get(AddressInter.ENTITY_NAME);

        PreparedStatement badrIns = tool.prepareStatement("insert into addressbase_t (id, discriminator) values(?, ?)");
        PreparedStatement deadrIns = tool.prepareStatement("insert into addressdetailed_t (id, country_id, settlement_id) values(?, ?, ?)");
        PreparedStatement ruadrIns = tool.prepareStatement("insert into addressru_t (id, district_id, street_id, housenumber_p, houseunitnumber_p, flatnumber_p, inheritedpostcode_p) values(?, ?, ?, ?, ?, ?, ?)");
        PreparedStatement interadrIns = tool.prepareStatement("insert into addressinter_t (id, region_p, district_p, addresslocation_p, postcode_p) values(?, ?, ?, ?, ?)");
        PreparedStatement personUpdate = tool.prepareStatement("update person_t set address_id=? where id=?");
        PreparedStatement iCardUpdate = tool.prepareStatement("update identitycard_t set address_id=? where id=?");
        PreparedStatement nsiContactUpdate = tool.prepareStatement("update fefunsipersoncontact_t set address_id=? where id=?");
        PreparedStatement mdbViewUpdate = tool.prepareStatement("update mdbviewaddress_t set addressbase_id=? where addressbase_id=?");
        PreparedStatement oldAddrStrDelete = tool.prepareStatement("delete from addressstring_t where id=?");
        PreparedStatement oldAddrBaseDelete = tool.prepareStatement("delete from addressbase_t where id=?");

        for (AddressWrapper wrapper : addWrappers)
        {
            Long oldAddrId = null;
            Long nsiContactId = null;

            if (null != wrapper.getPersonId())
            {
                oldAddrId = personIdToAddrIdMap.get(wrapper.getPersonId());
                nsiContactId = personIdToNsiContactIdMap.get(wrapper.getPersonId());
            }

            if (null != wrapper.getIcId())
            {
                oldAddrId = icIdToAddrIdMap.get(wrapper.getIcId());
                nsiContactId = icIdToNsiContactIdMap.get(wrapper.getIcId());
            }

            if (null != oldAddrId)
            {

                Long addrId = EntityIDGenerator.generateNewId(adRuDiscriminator);

                badrIns.setLong(1, addrId);
                badrIns.setShort(2, adRuDiscriminator);
                badrIns.executeUpdate();

                deadrIns.setLong(1, addrId);
                deadrIns.setObject(2, wrapper.getCountryId());
                deadrIns.setObject(3, wrapper.getSettlementId());
                deadrIns.executeUpdate();

                if (wrapper instanceof AddressRuWrapper)
                {
                    ruadrIns.setLong(1, addrId);
                    ruadrIns.setObject(2, ((AddressRuWrapper) wrapper).getDistrictId());
                    ruadrIns.setObject(3, ((AddressRuWrapper) wrapper).getStreetId());
                    ruadrIns.setObject(4, ((AddressRuWrapper) wrapper).getHouseNumber());
                    ruadrIns.setObject(5, ((AddressRuWrapper) wrapper).getHouseUnitNumber());
                    ruadrIns.setObject(6, ((AddressRuWrapper) wrapper).getFlatNumber());
                    ruadrIns.setObject(7, wrapper.getPostCode());
                    ruadrIns.executeUpdate();
                } else
                {
                    interadrIns.setLong(1, addrId);
                    interadrIns.setObject(2, ((AddressInterWrapper) wrapper).getRegion());
                    interadrIns.setObject(3, ((AddressInterWrapper) wrapper).getDistrict());
                    interadrIns.setObject(4, ((AddressInterWrapper) wrapper).getLocation());
                    interadrIns.setObject(5, ((AddressInterWrapper) wrapper).getPostCode());
                    interadrIns.executeUpdate();
                }

                if (null != wrapper.getPersonId())
                {
                    personUpdate.setLong(1, addrId);
                    personUpdate.setLong(2, wrapper.getPersonId());
                    personUpdate.executeUpdate();
                }

                if (null != wrapper.getIcId())
                {
                    iCardUpdate.setLong(1, addrId);
                    iCardUpdate.setLong(2, wrapper.getIcId());
                    iCardUpdate.executeUpdate();
                }

                if (null != nsiContactId)
                {
                    nsiContactUpdate.setLong(1, addrId);
                    nsiContactUpdate.setLong(2, nsiContactId);
                    nsiContactUpdate.executeUpdate();
                }

                mdbViewUpdate.setLong(1, addrId);
                mdbViewUpdate.setLong(2, oldAddrId);
                mdbViewUpdate.executeUpdate();

                oldAddrStrDelete.setLong(1, oldAddrId);
                oldAddrStrDelete.executeUpdate();

                try
                {
                    oldAddrBaseDelete.setLong(1, oldAddrId);
                    oldAddrBaseDelete.executeUpdate();
                } catch (SQLException ex)
                {
                    System.out.println("Could not delete address id= " + oldAddrId + ": " + ex.getMessage());
                }
            }
        }

        badrIns.close();
        deadrIns.close();
        ruadrIns.close();
        interadrIns.close();
        personUpdate.close();
        iCardUpdate.close();
        nsiContactUpdate.close();
        mdbViewUpdate.close();
        oldAddrStrDelete.close();
        oldAddrBaseDelete.close();
    }

    private Set<Long> getEntityIdSet(DBTool tool, String tableName) throws SQLException
    {
        Set<Long> resultSet = new HashSet<>();
        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select id from " + tableName);
        ResultSet rs = stmt.getResultSet();
        while (rs.next()) resultSet.add(rs.getLong(1));
        return resultSet;
    }

    private boolean validateField(String addrId, Long objId, Set<Long> objIdSet, boolean allowNulls, String objectName, boolean invalid)
    {
        if (!allowNulls && null == objId)
        {
            System.out.println("Address id= " + addrId + " could not be imported. " + objectName + " does not allow nulls.");
            return true;
        }
        if (null != objId && !objIdSet.contains(objId))
        {
            System.out.println("Address id= " + addrId + " could not be imported. " + objectName + " with id= " + objId + " not found at database.");
            return true;
        }

        return invalid;
    }

    public abstract class AddressWrapper
    {
        private Long _personId;
        private Long _icId;
        private Long _countryId;
        private String _postCode;
        private Long _settlementId;

        protected AddressWrapper(String[] parts)
        {
            _personId = parts[0].length() > 0 ? Long.parseLong(parts[0]) : null;
            _icId = parts[1].length() > 0 ? Long.parseLong(parts[1]) : null;
            _countryId = parts[3].length() > 0 ? Long.parseLong(parts[3]) : null;
            _postCode = StringUtils.trimToNull(parts[4]);
            _settlementId = parts[5].length() > 0 ? Long.parseLong(parts[5]) : null;
        }

        public Long getPersonId()
        {
            return _personId;
        }

        public Long getIcId()
        {
            return _icId;
        }

        public Long getCountryId()
        {
            return _countryId;
        }

        public String getPostCode()
        {
            return _postCode;
        }

        public Long getSettlementId()
        {
            return _settlementId;
        }
    }

    public class AddressRuWrapper extends AddressWrapper
    {

        private Long _districtId;
        private Long _streetId;
        private String _houseNumber;
        private String _houseUnitNumber;
        private String _flatNumber;

        public AddressRuWrapper(String[] parts)
        {
            super(parts);
            _districtId = parts[6].length() > 0 ? Long.parseLong(parts[6]) : null;
            _streetId = parts[7].length() > 0 ? Long.parseLong(parts[7]) : null;
            _houseNumber = StringUtils.trimToNull(parts[8]);
            _houseUnitNumber = StringUtils.trimToNull(parts[9]);
            _flatNumber = StringUtils.trimToNull(parts[10]);
        }

        public Long getDistrictId()
        {
            return _districtId;
        }

        public Long getStreetId()
        {
            return _streetId;
        }

        public String getHouseNumber()
        {
            return _houseNumber;
        }

        public String getHouseUnitNumber()
        {
            return _houseUnitNumber;
        }

        public String getFlatNumber()
        {
            return _flatNumber;
        }
    }

    public class AddressInterWrapper extends AddressWrapper
    {
        private String _region;
        private String _district;
        private String _location;

        public AddressInterWrapper(String[] parts)
        {
            super(parts);
            _region = StringUtils.trimToNull(parts[6]);
            _district = StringUtils.trimToNull(parts[7]);
            _location = StringUtils.trimToNull(parts[8]);
        }

        public String getRegion()
        {
            return _region;
        }

        public String getDistrict()
        {
            return _district;
        }

        public String getLocation()
        {
            return _location;
        }
    }
}