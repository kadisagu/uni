package ru.tandemservice.unifefu.brs.bo.FefuRatingGroupAllDiscReport.logic;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLStatement;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.FefuBrsStudentDisciplinesReportManager;
import ru.tandemservice.unifefu.entity.catalog.FefuBrsDocTemplate;
import ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.logic.FefuBrsStudentDisciplinesReportDAO.StudentData;

/**
 * @author amakarova
 * @since 12/4/13
 * Реализация отчета Рейтинг группы по всем дисциплинам
 */

@Transactional
public class FefuRatingGroupAllDiscReportDAO extends BaseModifyAggregateDAO implements IFefuRatingGroupAllDiscReportDAO
{
    @Override
    public FefuRatingGroupAllDiscReport createReport(FefuRatingGroupAllDiscReportParams reportSettings)
    {
        FefuRatingGroupAllDiscReport report = new FefuRatingGroupAllDiscReport();
        report.setOrgUnit(reportSettings.getOrgUnit());
        report.setFormingDate(new Date());
        report.setFormativeOrgUnit(reportSettings.getFormativeOrgUnit() != null ? reportSettings.getFormativeOrgUnit().getTitle() : null);
        report.setResponsibilityOrgUnit(reportSettings.getResponsibilityOrgUnit() != null ? reportSettings.getResponsibilityOrgUnit().getTitle() : null);
        report.setYearPart(reportSettings.getYearPart().getTitle());
        if (null != reportSettings.getGroupList() && !reportSettings.getGroupList().isEmpty())
            report.setGroup(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportSettings.getGroupList(), Group.title()), ", "));
        report.setOnlyFilledJournals(FefuBrsReportManager.YES_ID.equals(reportSettings.getOnlyFilledJournals().getId()));

        DatabaseFile content = new DatabaseFile();
        //long startTime = System.currentTimeMillis();
        content.setContent(print(reportSettings));
        //System.out.println("" + ((double) (System.currentTimeMillis() - startTime)) / 1000d + " sec");
        content.setFilename("fefuRatingGroupAllDiscReport");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        baseCreate(content);
        report.setContent(content);
        baseCreate(report);

        return report;
    }

    private byte[] print(FefuRatingGroupAllDiscReportParams reportParams)
    {
        FefuBrsDocTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(FefuBrsDocTemplate.class, "fefuRatingGroupAllDiscReport");
        RtfDocument document = new RtfReader().read(templateDocument.getContent());
        RtfInjectModifier modifier = new RtfInjectModifier();
        String orgUnit = reportParams.getFormativeOrgUnit() != null ? reportParams.getFormativeOrgUnit().getPrintTitle() : "";
        if (reportParams.getResponsibilityOrgUnit() != null)
        {
            if (!orgUnit.isEmpty())
                orgUnit += ", ";
            orgUnit += reportParams.getResponsibilityOrgUnit().getPrintTitle();
        }
        String eduYearPart = reportParams.getYearPart().getTitle();
        modifier.put("reportParams", orgUnit + ", " + eduYearPart);
        modifier.modify(document);
        fillTable(document, reportParams);
        return RtfUtil.toByteArray(document);
    }

    static final int EXPECTED_STUDENT_PER_GROUP = 30;
    static final int AVG_STUDENT_PER_GROUP = 10;

    private static final class GroupData
    {
        double sumCurrentRating = 0d;
        double sumTotalRating = 0d;
        String title;
        final Set<Long> journalSet = new HashSet<>();
        final List<StudentData> students = new ArrayList<>(EXPECTED_STUDENT_PER_GROUP);

        void addStudent(StudentData data)
        {
            students.add(data);

            Double currentRating  = data.getAvgDoubleRating(true);
            if (currentRating != null)
                sumCurrentRating += currentRating;

            Double totalRating  = data.getAvgDoubleRating(false);
            if (totalRating != null)
                sumTotalRating += totalRating;
        }

        String getAvgRating(boolean current)
        {
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format((current ? sumCurrentRating : sumTotalRating) / students.size());
        }
    }

    private void fillTable(RtfDocument document, final FefuRatingGroupAllDiscReportParams reportParams)
    {
        // Получаем рейтинги студентов
        final Map<Long, StudentData> studentMap = FefuBrsStudentDisciplinesReportManager.instance().dao().calcStudentsRating(reportParams.getYearPart(), reportParams.getGroupList(), reportParams.getFormativeOrgUnit(), reportParams.getResponsibilityOrgUnit(), FefuBrsReportManager.YES_ID.equals(reportParams.getOnlyFilledJournals().getId()), true);

        // Группируем студентов по академическим группам
        final Map<Long, GroupData> groupMap = new HashMap<>(studentMap.size() / AVG_STUDENT_PER_GROUP);
        for (StudentData studentData : studentMap.values())
        {
            GroupData groupData = groupMap.get(studentData.groupId);
            if (groupData == null)
                groupMap.put(studentData.groupId, groupData = new GroupData());

            groupData.addStudent(studentData);
            groupData.journalSet.addAll(studentData.getJournalIds());
        }

        // Получаем названия групп
        final DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromEntity(Group.class, "g")
                .where(in(property("g.id"), parameter("ids", PropertyType.LONG)))
        );
        final int idCol = dql.column("g.id");
        final int titleCol = dql.column(property("g", Group.P_TITLE));
        final IDQLStatement statement = dql.getDql().createStatement(getSession());
        final List<GroupData> resultList = new ArrayList<>(groupMap.size());
        BatchUtils.execute(groupMap.keySet(), 1000, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                statement.setParameterList("ids", elements);
                for (Object[] item : statement.<Object[]>list())
                {
                    GroupData data = groupMap.get((Long) item[idCol]);
                    data.title = (String) item[titleCol];
                    resultList.add(data);
                }
            }
        });

        // Сортируем список по количеству дисциплин и названию группы
        Collections.sort(resultList, new Comparator<GroupData>()
        {
            @Override
            public int compare(GroupData o1, GroupData o2)
            {
                int ret = o1.journalSet.size() - o2.journalSet.size();
                return ret != 0 ? ret : o1.title.compareTo(o2.title);
            }
        });

        // Заполняем таблицу
        final String[][] table = new String[resultList.size()][6];
        int index = 0;
        for (GroupData data : resultList)
        {
            String[] row = table[index];
            row[0] = String.valueOf(++index);
            row[1] = data.title;
            row[2] = String.valueOf(data.students.size());
            row[3] = String.valueOf(data.journalSet.size());
            row[4] = data.getAvgRating(true);
            row[5] = data.getAvgRating(false);
        }

        RtfTableModifier tableModifier = new RtfTableModifier().put("T", table);
        tableModifier.modify(document);
    }
}
