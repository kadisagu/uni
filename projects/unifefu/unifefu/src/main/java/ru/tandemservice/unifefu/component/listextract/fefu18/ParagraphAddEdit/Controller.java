/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public class Controller extends AbstractListParagraphAddEditController<FefuOrderContingentStuDPOListExtract,IDAO, Model>
{
}