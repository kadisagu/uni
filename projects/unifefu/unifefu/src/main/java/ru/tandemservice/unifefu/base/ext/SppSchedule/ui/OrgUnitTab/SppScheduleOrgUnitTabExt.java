/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppSchedule.ui.OrgUnitTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.ui.OrgUnitPrintFormList.FefuScheduleOrgUnitPrintFormList;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.OrgUnitTab.SppScheduleOrgUnitTab;

/**
 * @author nvankov
 * @since 2/14/14
 */

@Configuration
public class SppScheduleOrgUnitTabExt extends BusinessComponentExtensionManager
{


    @Autowired
    private SppScheduleOrgUnitTab _sppScheduleOrgUnitTab;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_sppScheduleOrgUnitTab.tabPanelExtPoint())
                .replaceTab(componentTab(SppScheduleOrgUnitTab.PRINT_FORM_SCHEDULE_TAB, FefuScheduleOrgUnitPrintFormList.class).permissionKey("ui:sec.orgUnit_viewSppSchedulePrintFormTab").create())

                .create();
    }
}
