package ru.tandemservice.unifefu.base.ext.TrJournal.ui.MarkView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsIRatingValueFormatter;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkView.TrJournalMarkViewUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;

/**
 * @author amakarova
 */
public class TrJournalMarkViewExtUI extends UIAddon
{
    private String _scaleDescription;

    public TrJournalMarkViewExtUI(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    public String getAdditParamValue() {
        TrJournalMarkViewUI presenter = getPresenter();
        IBrsDao.IStudentCurrentRatingData studentRating = presenter.getCalculatedRating().getCurrentRating(presenter.getCurrentRow().getStudent());
        if (studentRating == null || presenter.getCurrentAdditParam() == null)
            return "";
        ISessionBrsDao.IRatingValue paramValue = studentRating.getRatingAdditParam(presenter.getCurrentAdditParam().getKey());
        return (paramValue != null && paramValue.getValue() == null && paramValue.getMessage() != null) ? paramValue.getMessage() : BrsIRatingValueFormatter.instance.format(paramValue);
    }

    @Override
    public void onComponentRefresh()
    {
        TrJournal journal = ((TrJournalMarkViewUI) getPresenter()).getJournal();
        EppFControlActionGroup fControlActionGroup = TrBrsCoefficientManager.instance().coefDao().getFcaGroup(journal);
        _scaleDescription = "Традиционная оценка: " + FefuBrs.getJournalScaleDescription(EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(fControlActionGroup.getCode()), journal);
    }

    public String getCurrentRowStyle()
    {
        boolean active = ((TrJournalMarkViewUI) getPresenter()).getCurrentRow().isActive();
        return active ? "" : "font-style:italic; font-weight: bold";
    }

    public String getRatingValue()
    {
        TrJournalMarkViewUI presenter = getPresenter();
        ISessionBrsDao.IRatingValue rating = presenter.getRowRating();
        if (rating == null)
            return "-";
        if (rating.getValue() == null && rating.getMessage() != null)
            return "!";
        if (rating.getValue() == null)
            return "-";
        return BrsIRatingValueFormatter.instance.format(rating).length() == 0 ? "0" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(rating.getValue());
    }

    public String getMarkScale()
    {
        return _scaleDescription;
    }

}