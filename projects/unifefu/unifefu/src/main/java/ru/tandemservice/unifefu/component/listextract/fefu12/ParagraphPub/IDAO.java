/* $Id$ */


package ru.tandemservice.unifefu.component.listextract.fefu12.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuStuffCompensationStuListExtract, Model>
{
}
