/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings;

/**
 * @author Dmitry Seleznev
 * @since 11.07.2013
 */
public class FefuSettingsDao extends UniBaseDao implements IFefuSettingsDao
{
    @Override
    public void updateDirectumSettings(FefuDirectumSettings settings)
    {
        getSession().saveOrUpdate(settings);
    }

    @Override
    public void updateDirectumSettings(FefuDirectumEnrSettings settings)
    {
        getSession().saveOrUpdate(settings);
    }

    @Override
    public void saveExtractType2ThematicGroup(FefuStudentExtractType2ThematicGroup extractType2ThematicGroup)
    {
        getSession().save(extractType2ThematicGroup);
    }
}