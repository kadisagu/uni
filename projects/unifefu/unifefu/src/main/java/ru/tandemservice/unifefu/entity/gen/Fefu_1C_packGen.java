package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unifefu.entity.Fefu_1C_pack;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.entity.visa.gen.IAbstractDocumentGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пакет обмена данными с подсистемой БУ/УК (1С)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Fefu_1C_packGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.Fefu_1C_pack";
    public static final String ENTITY_NAME = "fefu_1C_pack";
    public static final int VERSION_HASH = 683304744;
    private static IEntityMeta ENTITY_META;

    public static final String P_DISPATCH_DATE = "dispatchDate";
    public static final String P_REQUEST = "request";
    public static final String P_RESPONSE = "response";
    public static final String P_SUCCESS = "success";
    public static final String L_ORDER = "order";
    public static final String P_SENT_ORDER_NUMBER = "sentOrderNumber";

    private Date _dispatchDate;     // Дата отправки
    private byte[] _request;     // Запрос
    private String _response;     // Ответ
    private boolean _success;     // Успех
    private IAbstractDocument _order;     // Приказ
    private String _sentOrderNumber;     // Номер приказа, отправленный в пакете

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отправки.
     */
    public Date getDispatchDate()
    {
        return _dispatchDate;
    }

    /**
     * @param dispatchDate Дата отправки.
     */
    public void setDispatchDate(Date dispatchDate)
    {
        dirty(_dispatchDate, dispatchDate);
        _dispatchDate = dispatchDate;
    }

    /**
     * @return Запрос.
     */
    public byte[] getRequest()
    {
        initLazyForGet("request");
        return _request;
    }

    /**
     * @param request Запрос.
     */
    public void setRequest(byte[] request)
    {
        initLazyForSet("request");
        dirty(_request, request);
        _request = request;
    }

    /**
     * @return Ответ.
     */
    @Length(max=255)
    public String getResponse()
    {
        return _response;
    }

    /**
     * @param response Ответ.
     */
    public void setResponse(String response)
    {
        dirty(_response, response);
        _response = response;
    }

    /**
     * @return Успех. Свойство не может быть null.
     */
    @NotNull
    public boolean isSuccess()
    {
        return _success;
    }

    /**
     * @param success Успех. Свойство не может быть null.
     */
    public void setSuccess(boolean success)
    {
        dirty(_success, success);
        _success = success;
    }

    /**
     * @return Приказ.
     */
    public IAbstractDocument getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ.
     */
    public void setOrder(IAbstractDocument order)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && order!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractDocument.class);
            IEntityMeta actual =  order instanceof IEntity ? EntityRuntime.getMeta((IEntity) order) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Номер приказа, отправленный в пакете.
     */
    @Length(max=255)
    public String getSentOrderNumber()
    {
        return _sentOrderNumber;
    }

    /**
     * @param sentOrderNumber Номер приказа, отправленный в пакете.
     */
    public void setSentOrderNumber(String sentOrderNumber)
    {
        dirty(_sentOrderNumber, sentOrderNumber);
        _sentOrderNumber = sentOrderNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Fefu_1C_packGen)
        {
            setDispatchDate(((Fefu_1C_pack)another).getDispatchDate());
            setRequest(((Fefu_1C_pack)another).getRequest());
            setResponse(((Fefu_1C_pack)another).getResponse());
            setSuccess(((Fefu_1C_pack)another).isSuccess());
            setOrder(((Fefu_1C_pack)another).getOrder());
            setSentOrderNumber(((Fefu_1C_pack)another).getSentOrderNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Fefu_1C_packGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Fefu_1C_pack.class;
        }

        public T newInstance()
        {
            return (T) new Fefu_1C_pack();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "dispatchDate":
                    return obj.getDispatchDate();
                case "request":
                    return obj.getRequest();
                case "response":
                    return obj.getResponse();
                case "success":
                    return obj.isSuccess();
                case "order":
                    return obj.getOrder();
                case "sentOrderNumber":
                    return obj.getSentOrderNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "dispatchDate":
                    obj.setDispatchDate((Date) value);
                    return;
                case "request":
                    obj.setRequest((byte[]) value);
                    return;
                case "response":
                    obj.setResponse((String) value);
                    return;
                case "success":
                    obj.setSuccess((Boolean) value);
                    return;
                case "order":
                    obj.setOrder((IAbstractDocument) value);
                    return;
                case "sentOrderNumber":
                    obj.setSentOrderNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "dispatchDate":
                        return true;
                case "request":
                        return true;
                case "response":
                        return true;
                case "success":
                        return true;
                case "order":
                        return true;
                case "sentOrderNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "dispatchDate":
                    return true;
                case "request":
                    return true;
                case "response":
                    return true;
                case "success":
                    return true;
                case "order":
                    return true;
                case "sentOrderNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "dispatchDate":
                    return Date.class;
                case "request":
                    return byte[].class;
                case "response":
                    return String.class;
                case "success":
                    return Boolean.class;
                case "order":
                    return IAbstractDocument.class;
                case "sentOrderNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Fefu_1C_pack> _dslPath = new Path<Fefu_1C_pack>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Fefu_1C_pack");
    }
            

    /**
     * @return Дата отправки.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#getDispatchDate()
     */
    public static PropertyPath<Date> dispatchDate()
    {
        return _dslPath.dispatchDate();
    }

    /**
     * @return Запрос.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#getRequest()
     */
    public static PropertyPath<byte[]> request()
    {
        return _dslPath.request();
    }

    /**
     * @return Ответ.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#getResponse()
     */
    public static PropertyPath<String> response()
    {
        return _dslPath.response();
    }

    /**
     * @return Успех. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#isSuccess()
     */
    public static PropertyPath<Boolean> success()
    {
        return _dslPath.success();
    }

    /**
     * @return Приказ.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#getOrder()
     */
    public static IAbstractDocumentGen.Path<IAbstractDocument> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Номер приказа, отправленный в пакете.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#getSentOrderNumber()
     */
    public static PropertyPath<String> sentOrderNumber()
    {
        return _dslPath.sentOrderNumber();
    }

    public static class Path<E extends Fefu_1C_pack> extends EntityPath<E>
    {
        private PropertyPath<Date> _dispatchDate;
        private PropertyPath<byte[]> _request;
        private PropertyPath<String> _response;
        private PropertyPath<Boolean> _success;
        private IAbstractDocumentGen.Path<IAbstractDocument> _order;
        private PropertyPath<String> _sentOrderNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отправки.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#getDispatchDate()
     */
        public PropertyPath<Date> dispatchDate()
        {
            if(_dispatchDate == null )
                _dispatchDate = new PropertyPath<Date>(Fefu_1C_packGen.P_DISPATCH_DATE, this);
            return _dispatchDate;
        }

    /**
     * @return Запрос.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#getRequest()
     */
        public PropertyPath<byte[]> request()
        {
            if(_request == null )
                _request = new PropertyPath<byte[]>(Fefu_1C_packGen.P_REQUEST, this);
            return _request;
        }

    /**
     * @return Ответ.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#getResponse()
     */
        public PropertyPath<String> response()
        {
            if(_response == null )
                _response = new PropertyPath<String>(Fefu_1C_packGen.P_RESPONSE, this);
            return _response;
        }

    /**
     * @return Успех. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#isSuccess()
     */
        public PropertyPath<Boolean> success()
        {
            if(_success == null )
                _success = new PropertyPath<Boolean>(Fefu_1C_packGen.P_SUCCESS, this);
            return _success;
        }

    /**
     * @return Приказ.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#getOrder()
     */
        public IAbstractDocumentGen.Path<IAbstractDocument> order()
        {
            if(_order == null )
                _order = new IAbstractDocumentGen.Path<IAbstractDocument>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Номер приказа, отправленный в пакете.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_pack#getSentOrderNumber()
     */
        public PropertyPath<String> sentOrderNumber()
        {
            if(_sentOrderNumber == null )
                _sentOrderNumber = new PropertyPath<String>(Fefu_1C_packGen.P_SENT_ORDER_NUMBER, this);
            return _sentOrderNumber;
        }

        public Class getEntityClass()
        {
            return Fefu_1C_pack.class;
        }

        public String getEntityName()
        {
            return "fefu_1C_pack";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
