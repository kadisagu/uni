package ru.tandemservice.unifefu.utils;

import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;

import java.util.Comparator;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: newdev
 * Date: 29.01.14
 * Time: 20:17
 * To change this template use File | Settings | File Templates.
 */
public class FefuTrGroupEventDateComparator implements Comparator<TrEduGroupEvent>
{
    @Override
    public int compare(TrEduGroupEvent o1, TrEduGroupEvent o2)
    {
        Date date1 = o1.getScheduleEvent() != null ? o1.getScheduleEvent().getDurationBegin() : null;
        Date date2 = o2.getScheduleEvent() != null ? o2.getScheduleEvent().getDurationBegin() : null;
        if (date1 != null || date2 != null)
        {
            if (date1 == null)
                return -1;
            if (date2 == null)
                return 1;
            int result = date1.compareTo(date2);
            if (result != 0)
                return result;
        }
        return Integer.compare(o1.getJournalEvent().getNumber(), o2.getJournalEvent().getNumber());
    }
}

