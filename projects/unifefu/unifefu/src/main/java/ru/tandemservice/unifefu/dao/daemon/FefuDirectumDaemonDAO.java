/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.*;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.utils.ooffice.OpenOfficeServiceUtil;
import ru.tandemservice.movestudent.component.customorder.ICustomPrintFormContainer;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.movestudent.entity.catalog.MovestudentTemplate;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.unifefu.base.bo.Directum.DirectumManager;
import ru.tandemservice.unifefu.base.bo.Directum.ui.Send.DirectumSendUI;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuDirectumOrderTypeCodes;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder;
import ru.tandemservice.unifefu.ws.directum.FefuDirectumClient;
import ru.tandemservice.unifefu.ws.directum.RouteFormWrapper;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 10.12.2014
 */
public class FefuDirectumDaemonDAO extends UniBaseDao implements IFefuDirectumDaemonDAO
{
    public static final int DAEMON_ITERATION_TIME = 5;

    protected static final Logger log4j_logger = Logger.getLogger(FefuDirectumDaemonDAO.class);

    public static void logEvent(Level loggingLevel, String message)
    {
        // проверяем, есть ли уже appender
        Appender appender = log4j_logger.getAppender("FefuDirectumDaemon");

        if (null != appender) log4j_logger.log(loggingLevel, message);
        else
        {
            try
            {
                // добавляем, если нет
                final String path = ApplicationRuntime.getAppInstallPath();
                String logFileName = "FefuDirectumDaemon.log";
                try
                {
                    appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} %p - %m%n"), FilenameUtils.concat(path, "tomcat/logs/" + logFileName));
                    appender.setName("FefuDirectumDaemon");
                    ((FileAppender) appender).setThreshold(Level.INFO);
                    log4j_logger.addAppender(appender);
                }
                catch (IOException ignored){}

                log4j_logger.setLevel(Level.INFO);
                if (null != appender)
                    log4j_logger.log(loggingLevel, message);
            }
            finally
            {
                // и отцепляем, если добавляли
                if (null != appender)
                    log4j_logger.removeAppender(appender);
            }
        }
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(FefuDirectumDaemonDAO.class.getName(), DAEMON_ITERATION_TIME, IFefuDirectumDaemonDAO.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            List<FefuDirectumSendingOrder> sendingOrderList = DataAccessServices.dao().getList(FefuDirectumSendingOrder.class, FefuDirectumSendingOrder.doneSend(), Boolean.FALSE);
            if (sendingOrderList.isEmpty()) return;

            // Время запуска демона
            long startTime = System.currentTimeMillis();
            logEvent(Level.INFO, "||||| FefuDirectumDaemon - start");

            final IFefuDirectumDaemonDAO dao = IFefuDirectumDaemonDAO.instance.get();
            for (FefuDirectumSendingOrder sendingOrder : sendingOrderList)
            {
                try
                {
                    dao.doSendOrdersToAccept(sendingOrder);
                }
                catch (final Exception t)
                {
                    Debug.exception(t.getMessage(), t);
                    logEvent(Level.ERROR, t.getMessage());
                }
            }
            logEvent(Level.INFO, String.format("||||| FefuDirectumDaemon - finish (%d  ms)", (System.currentTimeMillis() - startTime)));
        }
    };

    @Override
    public void doSendOrdersToAccept(FefuDirectumSendingOrder sendingOrder)
    {
        OpenOfficeServiceUtil.checkPdfConverterAvailable();

        AbstractStudentOrder order = sendingOrder.getOrder();
        AbstractStudentExtract firstExtract = sendingOrder.getFirstExtract();
        String orderTopic, pdfFileName;
        StringBuilder fileNameBuilder = new StringBuilder();

        final RtfDocument rtfDocument;
        long lastOperationTime = System.currentTimeMillis();
        logEvent(Level.INFO, "Creating print form...");

        if (order instanceof StudentModularOrder)
        {
            boolean individual = MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual((StudentModularOrder) order);

            pdfFileName = order.getExtractCount() != 1 ? "Order " : firstExtract.getEntity().getPerson().getFio().replaceAll("\\.", "") + " ";
            pdfFileName += String.valueOf(order.getId());

            byte[] template;
            if (individual && firstExtract instanceof ICustomPrintFormContainer)
            {
                ICustomPrintFormContainer container = (ICustomPrintFormContainer) firstExtract;
                template = container.getFile() != null ? container.getFile().getContent() : null;
                if (template == null)
                    logEvent(Level.ERROR, "Невозможно отправить приказ на согласование, поскольку у приказа отсутствует печатная форма.");
            }
            else
                template = DataAccessServices.dao().get(MovestudentTemplate.class, MovestudentTemplate.type().code(), StudentExtractTypeCodes.MODULAR_ORDER).getContent();

            IPrintFormCreator<StudentModularOrder> formCreator = CommonExtractPrint.getPrintFormCreator("studentModularOrder_orderPrint");
            rtfDocument = formCreator.createPrintForm(template, (StudentModularOrder) order);

            if (individual)
            {
                orderTopic = "Индивидуальный приказ " + firstExtract.getType().getTitle();
                fileNameBuilder.append(firstExtract.getEntity().getFio()).append(" приказ ");
                fileNameBuilder.append(firstExtract.getType().getTitle());
            }
            else
            {
                orderTopic = "Сборный приказ по студентам";
                fileNameBuilder.append("Сборный приказ по студентам");
            }
        }
        else if (order instanceof StudentListOrder)
        {
            pdfFileName = "StudentListOrder";
            byte[] template = MoveStudentDaoFacade.getMoveStudentDao().getTemplate((StudentExtractType) order.getType(), 1);
            IPrintFormCreator<StudentListOrder> formCreator = CommonExtractPrint.getPrintFormCreator("studentListOrder_" + order.getType().getCode() + "_orderPrint");
            if (formCreator == null)
                formCreator = CommonExtractPrint.getPrintFormCreator("studentListOrder" + ((StudentExtractType) order.getType()).getIndex() + "_orderPrint");
            rtfDocument = formCreator.createPrintForm(template, (StudentListOrder) order);

            orderTopic = "Списочный приказ по студентам";
            fileNameBuilder.append(firstExtract.getEntity().getFio()).append(" приказ ");
            fileNameBuilder.append(order.getType().getTitle());
        }
        else throw new IllegalStateException();

        logEvent(Level.INFO, String.format("Creating print form - successfully. (%d ms)", (System.currentTimeMillis() - lastOperationTime)));

        pdfFileName += ".pdf";

        sendOrderToDirectum(sendingOrder, rtfDocument, orderTopic, fileNameBuilder.toString(), pdfFileName);
    }

    private void sendOrderToDirectum(FefuDirectumSendingOrder sendingOrder, RtfDocument rtfDocument, String orderTopic, String fileName, String pdfFileName)
    {
        AbstractStudentOrder order = sendingOrder.getOrder();
        AbstractStudentExtract firstExtract = sendingOrder.getFirstExtract();
        FefuDirectumOrderType orderType = sendingOrder.getOrderType();
        Long sendingOrderId = sendingOrder.getId();

        String studentFio = getStudentFio(order, firstExtract);
        EducationOrgUnit educationOrgUnit = DirectumSendUI.getSpecialEducationOrgUnitForRoute(firstExtract);
        if (educationOrgUnit == null)
            educationOrgUnit = firstExtract.getEntity().getEducationOrgUnit();

        String developForm = educationOrgUnit.getDevelopForm().getTitle();
        if (DevelopTechCodes.DISTANCE.equals(educationOrgUnit.getDevelopTech().getCode()))
            developForm = "Дистанционная";
        if (FefuDirectumOrderTypeCodes.ENR.equals(orderType.getCode())) studentFio = null;
        if (!FefuDirectumOrderTypeCodes.VPO.equals(orderType.getCode()) && !FefuDirectumOrderTypeCodes.SPO.equals(orderType.getCode()))
        {
            developForm = null;
        }

        long lastOperationTime = System.currentTimeMillis();
        logEvent(Level.INFO, "- Register document in Directum...");

        String[] result = FefuDirectumClient.registerEDocument(order, null, orderType, fileName, orderTopic, studentFio, developForm, "", new byte[]{'1'});

        if (null != result && "0".equals(result[0]))
        {
            String docOrderId = result[1].replaceAll(";", "");

            logEvent(Level.INFO, String.format("- Document was registered by Directum successfully. The incoming ID is '" + docOrderId + "'. (%d ms)", (System.currentTimeMillis() - lastOperationTime)));
            lastOperationTime = System.currentTimeMillis();
            logEvent(Level.INFO, "-- Attaching print form in Directum...");

            byte[] document = DirectumManager.instance().dao().getStudentOrderPrintForm(rtfDocument, pdfFileName, docOrderId);
            Integer docAttachResult = FefuDirectumClient.attachEDocumentPrintForm(docOrderId, order, null, orderType, document);
            if (null != docAttachResult && 1 == docAttachResult)
            {
                logEvent(Level.INFO, String.format("-- Print form was attached to Directum document successfully. The documentID is '" + docOrderId + "'. (%d ms)", (System.currentTimeMillis() - lastOperationTime)));
                lastOperationTime = System.currentTimeMillis();
                logEvent(Level.INFO, "--- Registration task...");

                String taskResult = FefuDirectumClient.registerTask(order, null, docOrderId, getRouteFormWrapper(sendingOrder), document);
                if (null != taskResult)
                {
                    logEvent(Level.INFO, String.format("--- Task registration result = '" + taskResult + "'. (%d ms)", (System.currentTimeMillis() - lastOperationTime)));

                    DirectumManager.instance().dao().createStudentOrderDirectumExtension(order, docOrderId, taskResult, null, null);
                    DirectumManager.instance().dao().doUpdateDirectumTaskId(docOrderId, taskResult);

                    //save print form
                    if (order instanceof StudentModularOrder)
                        MoveStudentDaoFacade.getMoveStudentDao().saveModularOrderText((StudentModularOrder) order);
                    else
                        MoveStudentDaoFacade.getMoveStudentDao().saveListOrderText((StudentListOrder) order);

                    ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
                    sendToCoordinationService.init(order, (IPersistentPersonable) sendingOrder.getExecutor());
                    sendToCoordinationService.execute();

                    sendingOrder.setSendDateTime(new Date());
                    sendingOrder.setDoneSend(true);
                    update(sendingOrder);
                }
                else logEvent(Level.ERROR, "--- Directum error: Task registration was failed. (sendingOrderId = " + sendingOrderId + " docOrderId = " + docOrderId + ").");
            }
        }
        else
        {
            if (null != result)
                logEvent(Level.ERROR, "- Directum error: EDocument registration was failed. Response code is '" + result[1] + "'. (sendingOrderId = " + sendingOrderId + ")");
            else
                logEvent(Level.ERROR, "- Directum error: There was some error. There is no response from Directum. (sendingOrderId = " + sendingOrderId + ")");
        }
    }

    private String getStudentFio(AbstractStudentOrder order, AbstractStudentExtract firstExtract)
    {
        if (order instanceof StudentListOrder) return "Списочный приказ";
        return firstExtract.getEntity().getPerson().getFullFio();
    }

    private RouteFormWrapper getRouteFormWrapper(FefuDirectumSendingOrder sendingOrder)
    {
        RouteFormWrapper wrapper = new RouteFormWrapper(sendingOrder.getOrderType());

        wrapper.setOrgUnitCode(sendingOrder.getOrgUnitCode());
        wrapper.setOrgUnitTitle(sendingOrder.getOrgUnitTitle());
        wrapper.setSigner(sendingOrder.getSigner());
        wrapper.setRouteCode(sendingOrder.getRouteCode());
        wrapper.setDismiss(sendingOrder.isDismiss());
        wrapper.setRestore(sendingOrder.isRestore());
        wrapper.setOffBudgetPayments(sendingOrder.isOffBudgetPayments());
        wrapper.setForeignStudent(sendingOrder.isForeignStudent());
        wrapper.setGrantMatAid(sendingOrder.isGrantMatAid());
        wrapper.setSocial(sendingOrder.isSocial());
        wrapper.setPractice(sendingOrder.isPractice());
        wrapper.setGroupManager(sendingOrder.isGroupManager());
        wrapper.setUvcStudents(sendingOrder.isUvcStudents());
        wrapper.setFvoStudents(sendingOrder.isFvoStudents());
        wrapper.setPenalty(sendingOrder.isPenalty());
        wrapper.setMpr(sendingOrder.isMagister());
        wrapper.setPayments(sendingOrder.isPayments());
        wrapper.setStateFormular(sendingOrder.isStateFormular());
        wrapper.setCheckMedicalDocs(sendingOrder.isCheckMedicalDocs());
        wrapper.setDiplomaSuccess(sendingOrder.isDiplomaSuccess());
        wrapper.setExecutor(sendingOrder.getExecutor().getPrincipal().getLogin());

        ICatalogItem orderType = sendingOrder.getOrder().getType();
        String type;
        if (orderType == null) type = sendingOrder.getFirstExtract().getType().getTitle();
        else type = orderType.getTitle();
        wrapper.setCommonOrderType(type);

        return wrapper;
    }
}