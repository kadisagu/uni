package ru.tandemservice.unifefu.brs.bo.FefuRatingGroupAllDiscReport.ui.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unifefu.brs.base.FefuBrsReportListHandler;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuRatingGroupAllDiscReport.ui.View.FefuRatingGroupAllDiscReportView;
import ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport;

/**
 * @author nvankov
 * @since 12/3/13
 */
@Configuration
public class FefuRatingGroupAllDiscReportList extends BusinessComponentManager
{
    public static final String REPORT_DS = "reportDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(FefuBrsReportManager.instance().yearPartDSConfig())
                .addDataSource(searchListDS(REPORT_DS, reportDS(), reportDSHandler()))
                .create();
    }

    @Qualifier
    @Bean
    public ColumnListExtPoint reportDS() {
        return this.columnListExtPointBuilder(REPORT_DS)
                .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")))
                .addColumn(publisherColumn("date", FefuRatingGroupAllDiscReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).businessComponent(FefuRatingGroupAllDiscReportView.class).order())
                .addColumn(textColumn("formativeOrgUnit", FefuRatingGroupAllDiscReport.formativeOrgUnit()))
                .addColumn(textColumn("yearPart", FefuRatingGroupAllDiscReport.yearPart()))
                .addColumn(textColumn("group", FefuRatingGroupAllDiscReport.group()))
                .addColumn(booleanColumn("filledJournals", FefuRatingGroupAllDiscReport.onlyFilledJournals()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                        .alert(FormattedMessage.with().template("reportDS.delete.alert").parameter(FefuRatingGroupAllDiscReport.formingDateStr()).create())
                )
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler reportDSHandler()
    {
        return new FefuBrsReportListHandler(getName(), FefuRatingGroupAllDiscReport.class);
    }
}
