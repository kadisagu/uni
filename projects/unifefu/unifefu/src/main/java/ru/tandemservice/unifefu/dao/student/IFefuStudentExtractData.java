/* $Id$ */
package ru.tandemservice.unifefu.dao.student;

import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.unifefu.entity.*;

import java.util.Map;

/**
 * @author nvankov
 * @since 11/5/13
 */
public interface IFefuStudentExtractData
{
    String FEFU_EXTRACT_DATA_BEAN_NAME = "fefuExtractData";

    //
    // Сборные (индивидуальные) приказы
    //

    //    <!-- 1. О предоставлении академического отпуска -->
    Map<String, String> getData(WeekendStuExtract extract);

    // <!-- 2. О восстановлении -->
    Map<String, String> getData(RestorationStuExtract extract);

    // <!-- 3. О выходе из академического отпуска -->
    Map<String, String> getData(WeekendOutStuExtract extract);

    // <!-- 4. О зачислении -->
    Map<String, String> getData(EduEnrolmentStuExtract extract);

    // <!-- 5. О переводе на другую форму освоения -->
    Map<String, String> getData(TransferDevFormStuExtract extract);

    // <!-- 6. О переводе на другую основу оплаты обучения -->
    Map<String, String> getData(TransferCompTypeStuExtract extract);

    // <!-- 7. О переводе на другое направление подготовки (специальность) -->
    Map<String, String> getData(TransferEduTypeStuExtract extract);

    // <!-- 8. Об изменении пункта обучения -->
    Map<String, String> getData(TransferStuExtract extract);

    // <!-- 9. Об отчислении -->
    Map<String, String> getData(ExcludeStuExtract extract);

    // <!-- 10. О смене фамилии (имени) -->
    Map<String, String> getData(ChangeFioStuExtract extract);

    // <!-- 11. Об отмене приказа -->
    Map<String, String> getData(RevertOrderStuExtract extract);

    // <!-- 12. О предоставлении отпуска по беременности и родам -->
    Map<String, String> getData(WeekendPregnancyStuExtract extract);

    // <!-- 13. О предоставлении отпуска по уходу за ребенком -->
    Map<String, String> getData(WeekendChildStuExtract extract);

    // <!-- 14. О зачислении, расширенный вариант -->
    Map<String, String> getData(EduEnrolmentStuExtractExt extract);

    // <!-- 15. О зачислении в порядке перевода -->
    Map<String, String> getData(EduEnrAsTransferStuExtract extract);

    // <!-- 16. О предоставлении академического отпуска, расширенный вариант -->
    Map<String, String> getData(WeekendStuExtractExt extract);

    // <!-- 17. О восстановлении, расширенный вариант -->
    Map<String, String> getData(RestorationStuExtractExt extract);

    // <!-- 18. О переводе, расширенный вариант -->
    Map<String, String> getData(TransferStuExtractExt extract);

    // <!-- 19. Об отстранении от занятий -->
    Map<String, String> getData(DischargingStuExtract extract);

    // <!-- 20. О продлении экзаменационной сессии -->
    Map<String, String> getData(SessionProlongStuExtract extract);

    // <!-- 21. О продлении отпуска по уходу за ребенком -->
    Map<String, String> getData(ProlongWeekndChildStuExtract extract);

    // <!-- 22. О назначении академической стипендии -->
    Map<String, String> getData(AcadGrantAssignStuExtract extract);

    // <!-- 23. О назначении социальной стипендии -->
    Map<String, String> getData(SocGrantAssignStuExtract extract);

    // <!-- 24. О приостановлении выплаты социальной стипендии -->
    Map<String, String> getData(SocGrantStopStuExtract extract);

    // <!-- 25. О возобновлении выплаты социальной стипендии -->
    Map<String, String> getData(SocGrantResumptionStuExtract extract);

    // <!-- 26. Об отчислении, расширенный вариант -->
    Map<String, String> getData(ExcludeStuExtractExt extract);

    // <!-- 27. Об отчислении в связи с переводом -->
    Map<String, String> getData(ExcludeTransfStuExtract extract);

    // <!-- 28. О выдаче дубликата зачетной книжки -->
    Map<String, String> getData(GiveBookDuplicateStuExtract extract);

    // <!-- 29. О выдаче дубликата студенческого билета -->
    Map<String, String> getData(GiveCardDuplicateStuExtract extract);

    // <!-- 30. О выдаче дубликата диплома -->
    Map<String, String> getData(GiveDiplomaDuplicateStuExtract extract);

    // <!-- 31. О выдаче дубликата приложения к диплому -->
    Map<String, String> getData(GiveDiplAppDuplicateStuExtract extract);

    // <!-- 32. О допуске к занятиям -->
    Map<String, String> getData(AdmissionStuExtract extract);

    // <!-- 33. О назначении выплаты пособий -->
    Map<String, String> getData(AssignBenefitStuExtract extract);

    // <!--   34. О смене имени -->
    Map<String, String> getData(ChangeFirstNameStuExtract extract);

    // <!--  35. О смене отчества -->
    Map<String, String> getData(ChangeMiddleNameStuExtract extract);

    // <!-- 36. О повторном обучении -->
    Map<String, String> getData(ReEducationStuExtract extract);

    // <!-- 37. О продлении академического отпуска -->
    Map<String, String> getData(ProlongWeekendStuExtract extract);

    // <!-- 38. О выходе из отпуска по уходу за ребенком -->
    Map<String, String> getData(WeekendChildOutStuExtract extract);

    // <!-- 39. О скидке по оплате за обучение -->
    Map<String, String> getData(PaymentDiscountStuExtract extract);

    // <!-- 40 О ВЫДАЧЕ ДИПЛОМА О НЕПОЛНОМ ВЫСШЕМ ОБРАЗОВАНИИ ПРОФЕССИОНАЛЬНОМ ОБРАЗОВАНИИ-->
    Map<String, String> getData(GiveDiplIncompleteStuListExtract extract);

    // <!-- 41. О досрочной сдаче сессии -->
    Map<String, String> getData(PrescheduleSessionPassStuExtract extract);

    // <!-- 42. О переносе сроков сдачи защиты дипломной работы -->
    Map<String, String> getData(ChangePassDiplomaWorkPeriodStuExtract extract);

    // <!-- 43. О переносе сроков сдачи государственного экзамена -->
    Map<String, String> getData(ChangeDateStateExaminationStuExtract extract);

    // <!-- 44. О повторной защите дипломной работы -->
    Map<String, String> getData(RepeatProtectDiplomaWorkStuExtract extract);

    // <!-- 45. О повторной сдаче государственного экзамена -->
    Map<String, String> getData(RepeatPassStateExamStuExtract extract);

    // <!-- 46. О свободном посещении занятий -->
    Map<String, String> getData(FreeStudiesAttendanceStuExtract extract);

    // <!-- 47. О назначении старостой учебной группы -->
    Map<String, String> getData(AddGroupManagerStuExtract extract);

    // <!-- 48. Об освобождении от обязанностей старосты учебной группы -->
    Map<String, String> getData(RemoveGroupManagerStuExtract extract);

    // <!-- 49. О дисциплинарном взыскании -->
    Map<String, String> getData(DisciplinaryPenaltyStuExtract extract);

    // <!-- 50. О наложении взыскания за утерю зачетной книжки -->
    Map<String, String> getData(RecordBookLossPenaltyStuExtract extract);

    // <!-- 51. О наложении взыскания за утерю студенческого билета -->
    Map<String, String> getData(StudentCardLossPenaltyStuExtract extract);

    // <!-- 52. О допуске к сдаче государственного экзамена -->
    Map<String, String> getData(AdmitToStateExamsStuExtract extract);

    // <!-- 53. О предоставлении каникул -->
    Map<String, String> getData(HolidayStuExtract extract);

    // <!-- 54. О переносе итоговой государственной аттестации -->
    Map<String, String> getData(ChangePassStateExamsStuExtract extract);

    // <!-- 55. Об установлении индивидуального срока сдачи сессии(продление сессии) -->
    Map<String, String> getData(SessionIndividualStuExtract extract);

    // <!-- 56. О переводе на индивидуальный план обучения -->
    Map<String, String> getData(TransferIndPlanStuExtract extract);

    // <!-- 57. О допуске к защите выпускной квалификационной работы(после ГЭ) -->
    Map<String, String> getData(AdmitToDiplomaStuExtract extract);

    // <!-- 58. О переводе на индивидуальный график обучения -->
    Map<String, String> getData(TransferIndGraphStuExtract extract);

    // <!-- 59. О предоставлении отпуска по уходу за ребенком до 1,5 лет -->
    Map<String, String> getData(WeekendChildOneAndHalfStuExtract extract);

    // <!-- 60. О предоставлении повторного года обучения с переходом на ФГОС -->
    Map<String, String> getData(ReEducationFGOSStuExtract extract);

    // <!-- 61. О восстановлении -->
    Map<String, String> getData(RestorationCourseStuExtract extract);

    // <!-- 62. Об отчислении (государственный экзамен) -->
    Map<String, String> getData(ExcludeStateExamStuExtract extract);

    // <!-- 63. Об отчислении (недопуск к ВКР) -->
    Map<String, String> getData(ExcludeUnacceptedToGPDefenceStuExtract extract);

    // <!-- 64. Об отчислении (ВКР) -->
    Map<String, String> getData(ExcludeGPDefenceFailStuExtract extract);

    // <!-- 65. О переводе из группы в группу -->
    Map<String, String> getData(TransferGroupStuExtract extract);

    // <!-- 66. О переводе с курса на следующий курс -->
    Map<String, String> getData(TransferCourseStuExtract extract);

    // <!-- 67. О переводе со специальности на специальность (с направления на направление) -->
    Map<String, String> getData(TransferEduLevelStuExtract extract);

    // <!-- 68. О переводе со специальности на специальность (с направления на направление) со сменой формирующего подразделения -->
    Map<String, String> getData(TransferFormativeStuExtract extract);

    // <!-- 69. О переводе со специальности на специальность (с направления на направление) со сменой территориального подразделения -->
    Map<String, String> getData(TransferTerritorialStuExtract extract);

    // <!-- 70. О переводе со специальности на специальность (с направления на направление) между территориальными подразделениями -->
    Map<String, String> getData(TransferTerritorialExtStuExtract extract);

    // <!-- 71. О переводе на другие условия освоения и срок освоения -->
    Map<String, String> getData(TransferDevCondExtStuExtract extract);

    // <!-- 72. О переводе на другие условия освоения -->
    Map<String, String> getData(TransferDevCondStuExtract extract);

    // <!-- 73. О восстановлении и повторном допуске к сдаче государственного экзамена (пересдача ГЭ) -->
    Map<String, String> getData(RestorationAdmitStateExamsExtStuExtract extract);

    // <!-- 74. О закреплении за специализацией (профилем) и перераспеределнии в группу -->
    Map<String, String> getData(TransferProfileStuExtract extract);

    // <!-- 75. О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ) -->
    Map<String, String> getData(RestorationAdmitStateExamsStuExtract extract);

    // <!-- 76. О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу -->
    Map<String, String> getData(TransferProfileGroupExtStuExtract extract);

    // <!-- 77. О восстановлении и допуске к подготовке ВКР -->
    Map<String, String> getData(RestorationAdmitToDiplomaStuExtract extract);

    // <!-- 78. О переводе на другую основу оплаты обучения (вакантное бюджетное место) -->
    Map<String, String> getData(TransferCompTypeExtStuExtract extract);

    // <!-- 79. О переводе со специализации на специализации (с профиля на профиль) -->
    Map<String, String> getData(TransferProfileExtStuExtract extract);

    // <!-- 80. Об изменении категории обучаемого -->
    Map<String, String> getData(ChangeCategoryStuExtract extract);

    // <!-- 81. О восстановлении и допуске к защите ВКР (неявка на защиту) -->
    Map<String, String> getData(RestorationAdmitToDiplomaDefStuExtract extract);

    // <!-- 82. О пересдаче дисциплин учебного плана -->
    Map<String, String> getData(RePassDiscStuExtract extract);

    // <!-- 83. О восстановлении и допуске к защите ВКР (повторная защита) -->
    Map<String, String> getData(RestorationAdmitToDiplomaDefExtStuExtract extract);

    // <!-- 84. О пересдаче дисциплин учебного плана комиссии -->
    Map<String, String> getData(RePassDiscComissStuExtract extract);

    // <!-- 85. О выдаче диплома и присвоении квалификации -->
    Map<String, String> getData(GiveDiplomaStuExtract extract);

    // <!-- 86. Об оказании материальной помощи -->
    Map<String, String> getData(MatAidAssignStuExtract extract);

    // <!-- 87. Об однократном повышении государственной академической стипендии -->
    Map<String, String> getData(GrantRiseStuExtract extract);

    // <!-- 88. О закреплении за специализацией (профилем) -->
    Map<String, String> getData(ProfileAssignStuExtract extract);

    // <!-- 89. О выходе из отпуска по беременности и родам -->
    Map<String, String> getData(WeekendPregnancyOutStuExtract extract);

    // <!-- 90. О выходе из отпуска по уходу за ребенком -->
    Map<String, String> getData(WeekendChildCareOutStuExtract extract);

    // <!-- 91. О назначении надбавки к государственной академической стипендии -->
    Map<String, String> getData(AcadGrantBonusAssignStuExtract extract);

    // <!-- 93. О распределении в группу -->
    Map<String, String> getData(GroupAssignStuExtract extract);

    // <!-- 92. О переводе с курса на следующий курс со сменой территориального подразделения -->
    Map<String, String> getData(TransferCourseWithChangeTerrOuStuExtract extract);

    // <!-- 94. О предоставлении отпуска по уходу за ребенком до 3 лет -->
    Map<String, String> getData(WeekendChildThreeStuExtract extract);

    // <!-- 95. Об установлении индивидуального срока сдачи сессии(досрочная сдача сессии) -->
    Map<String, String> getData(SessionIndividualAheadStuExtract extract);

    // <!-- 96. О допуске к защите выпускной квалификационной работы -->
    Map<String, String> getData(AdmitToDiplomaWithoutExamStuExtract extract);

    // <!-- 97. О приеме гражданства Российской Федерации -->
    Map<String, String> getData(AdoptRussianCitizenshipStuExtract extract);

    // <!-- 98. Об аттестации практик студентов на основании академической справки -->
    Map<String, String> getData(AttestationPracticStuExtract extract);

    // <!-- 99. О прекращении выплаты академической стипендии -->
    Map<String, String> getData(AcadGrantPaymentStopStuExtract extract);

    // <!-- 100. О прекращении выплаты социальной стипендии -->
    Map<String, String> getData(SocGrantPaymentStopStuExtract extract);

    // <!-- 101. О прекращении выплаты надбавки к государственной академической стипендии  -->
    Map<String, String> getData(AcadGrantBonusPaymentStopStuExtract extract);

    // <!-- 102. Об аттестации практик студентов имеющих стаж работы по профилю обучения -->
    Map<String, String> getData(AttestationPracticWithServiceRecStuExtract extract);

    // <!-- 103. Об аттестации практик студентов, прошедших стажировку -->
    Map<String, String> getData(AttestationPracticWithProbationStuExtract extract);

    // <!-- 104. Об аттестации практик студентов, прошедших стажировку -->
    Map<String, String> getData(TransferBetweenTerritorialStuExtract extract);

    //    <!-- fefu1. О переносе сдачи государственного экзамена-->
    Map<String, String> getData(FefuChangeDateStateExaminationStuExtract extract);

    //    <!-- fefu2. О переносе защиты выпускной квалификационной работы-->
    Map<String, String> getData(FefuChangePassDiplomaWorkPeriodStuExtract extract);

    //    <!-- fefu3. О предоставлении повторного года обучения -->
    Map<String, String> getData(FefuReEducationStuExtract extract);

    //    <!-- fefu4. Об отчислении -->
    Map<String, String> getData(FefuExcludeStuExtract extract);

    //    <!-- fefu5. О переводе на другую форму освоения -->
    Map<String, String> getData(FefuTransferDevFormStuExtract extract);

    //    <!-- fefu6. Об изменении фамилии, имени, отчества -->
    Map<String, String> getData(FefuChangeFioStuExtract extract);

    //    <!-- fefu7. О назначении академической стипендии -->
    Map<String, String> getData(FefuAcadGrantAssignStuExtract extract);

    //    <!-- fefu8. О приостановлении выплаты социальной стипендии -->
    Map<String, String> getData(FefuSocGrantStopStuExtract extract);

    //    <!-- fefu9. О возобновлении выплаты социальной стипендии -->
    Map<String, String> getData(FefuSocGrantResumptionStuExtract extract);

    //    <!-- fefu10. О зачислении на второй и последующий курс -->
    Map<String, String> getData(FefuEduEnrolmentToSecondAndNextCourseStuExtract extract);

    //    <!-- fefu11. О назначении академической стипендии (вступительные испытания) -->
    Map<String, String> getData(FefuAcadGrantAssignStuEnrolmentExtract extract);

    //    <!-- fefu12. О зачислении в порядке перевода -->
    Map<String, String> getData(FefuEduTransferEnrolmentStuExtract extract);

    //    <!-- fefu13. О направлении на практику за пределами ОУ-->
    Map<String, String> getData(SendPracticOutStuExtract extract);

    //    <!-- fefu14. О направлении на практику в пределах ОУ-->
    Map<String, String> getData(SendPracticInnerStuExtract extract);

    //    <!-- fefu15. О зачислении на полное государственное обеспечение-->
    Map<String, String> getData(FullStateMaintenanceEnrollmentStuExtract extract);

    //    <!-- fefu16. О полном государственном обеспечении-->
    Map<String, String> getData(FullStateMaintenanceStuExtract extract);

    //    <!-- fefu20. О зачислении слушателей ДПО-->
    Map<String, String> getData(FefuEnrollStuDPOExtract extract);

    //    <!-- fefu21. Об отчислении слушателей ДПО-->
    Map<String, String> getData(FefuExcludeStuDPOExtract extract);

    //    <!-- fefu22. Приказ по контингенту ДПО/ДО-->
    Map<String, String> getData(FefuOrderContingentStuDPOExtract extract);

    //    <!-- fefu23. О переводе слушателей ДПО-->
    Map<String, String> getData(FefuTransfStuDPOExtract extract);

    //
    // Списочные приказы
    //

    //    <!-- 2. ПЕРЕВОД С КУРСА НА КУРС -->
    Map<String, String> getData(CourseTransferStuListExtract extract);


    //    <!-- 12. ПЕРЕВОД С КУРСА НА КУРС СТУДЕНТОВ, ИМЕЮЩИХ ЗАДОЛЖЕННОСТИ -->
    Map<String, String> getData(CourseTransferDebtorStuListExtract extract);

    //    <!-- 3. СЧИТАТЬ НА ПРЕЖНЕМ КУРСЕ -->
    Map<String, String> getData(CourseNoChangeStuListExtract extract);


    //    <!-- 4. ОТЧИСЛЕНИЕ -->
    Map<String, String> getData(ExcludeStuListExtract extract);

    //    <!-- 15. ОТЧИСЛЕНИЕ ДЛЯ ОДНОЙ ГРУППЫ -->
    Map<String, String> getData(ExcludeSingGrpStuListExtract extract);


    //    <!-- 14. ОТЧИСЛЕНИЕ ДЛЯ ОДНОЙ ГРУППЫ, ДИПЛОМ С ОТЛИЧИЕМ -->
    Map<String, String> getData(ExcludeSingGrpSuccStuListExtract extract);


    //    <!-- 16. ОТЧИСЛЕНИЕ ДЛЯ ОДНОЙ ГРУППЫ (КАФЕДРАЛЬНЫЙ) -->
    Map<String, String> getData(ExcludeSingGrpCathStuListExtract extract);


    //    <!-- 5. ДОПУСК К ГОСУДАРСТВЕННОЙ ИТОГОВОЙ АТТЕСТАЦИИ -->
    Map<String, String> getData(AdmitToStateExamsStuListExtract extract);


    //    <!-- 6. О ВЫДАЧЕ ДИПЛОМА И ПРИСВОЕНИИ КВАЛИФИКАЦИИ -->
    Map<String, String> getData(GiveDiplStuListExtract extract);


    //    <!-- 7. О ВЫДАЧЕ ДИПЛОМА С ОТЛИЧИЕМ И ПРИСВОЕНИИ КВАЛИФИКАЦИИ -->
    Map<String, String> getData(GiveDiplSuccessStuListExtract extract);

    //    <!-- 8. О ПРЕДОСТАВЛЕНИИ КАНИКУЛ -->
    Map<String, String> getData(HolidayStuListExtract extract);

    //    <!-- 9. О ВЫПУСКЕ СТУДЕНТОВ С ОТЛИЧИЕМ -->
    Map<String, String> getData(GraduateSuccessStuListExtract extract);

    //    <!-- 10. О ВЫПУСКЕ СТУДЕНТОВ -->
    Map<String, String> getData(GraduateStuListExtract extract);

    //    <!-- 11. ОБ ИСКЛЮЧЕНИИ СТУДЕНТОВ, НЕ ВЫПОЛНИВШИХ УЧЕБНЫЙ ПЛАН -->
    Map<String, String> getData(GradExcludeStuListExtract extract);

    //    <!-- 18. О РАЗДЕЛЕНИИ СТУДЕНТОВ ПО СПЕЦИАЛИЗАЦИЯМ -->
    Map<String, String> getData(SplitStudentsStuListExtract extract);

    //    <!-- 19. О ПЕРЕВОДЕ НА УСКОРЕННОЕ (СОКРАЩЕННОЕ) ОБУЧЕНИЕ (ОБ ИЗМЕНЕНИИ УСЛОВИЯ ОСВОЕНИЯ) -->
    Map<String, String> getData(TransferDevConditionStuListExtract extract);

    //    <!-- 13. О ПЕРЕВОДЕ СТУДЕНТОВ ТЕРРИТОРИАЛЬНОГО ПОДРАЗДЕЛЕНИЯ -->
    Map<String, String> getData(TerritorialTransferStuListExtract extract);

    //    <!-- 17. О допуске к защите дипломной работы -->
    Map<String, String> getData(AdmitToPassDiplomaWorkStuListExtract extract);

    //    <!-- 20. О назначении рецензентов -->
    Map<String, String> getData(SetReviewerStuListExtract extract);

    //    <!-- 21. О смене научных руководителей -->
    Map<String, String> getData(ChangeScientificAdviserExtract extract);

    //    <!-- 22. О смене рецензентов -->
    Map<String, String> getData(ChangeReviewerStuListExtract extract);

    //    <!-- 23. Об изменении темы дипломной работы -->
    Map<String, String> getData(ChangeDiplomaWorkTopicStuListExtract extract);

    //    <!-- 24. Об утверждении тем дипломных работ и назначении научных руководителей -->
    Map<String, String> getData(SetDiplomaWorkTopicAndScientificAdviserStuListExtract extract);

    //    <!-- 25. О закреплении за специализациями (профилями) и перераспределении в группу -->
    Map<String, String> getData(SplitStudentsGroupStuListExtract extract);

    //    <!-- 26. О допуске к сдаче государственного экзамена -->
    Map<String, String> getData(AdmitToStateExamsExtStuListExtract extract);

    //    <!-- 27. О допуске к защите выпускной квалификационной работы -->
    Map<String, String> getData(AdmitToPassDiplomaWorkExtStuListExtract extract);

    //    <!-- 28. О назначении старостами учебных групп -->
    Map<String, String> getData(AddGroupManagerStuListExtract extract);

    //    <!-- 29. О выдаче диплома и присвоении квалификации (расширенный) -->
    Map<String, String> getData(GiveDiplExtStuListExtract extract);

    //    <!-- 30. О выдаче диплома с отличием и присвоении квалификации (расширенный) -->
    Map<String, String> getData(GiveDiplSuccessExtStuListExtract extract);

    //    <!-- 31. О переводе с курса на следующий курс (по направлениям подготовки) -->
    Map<String, String> getData(CourseTransferExtStuListExtract extract);

    //    <!-- 32. О назначении академической стипендии -->
    Map<String, String> getData(AcadGrantAssignStuListExtract extract);

    //    <!-- 33. О назначении социальной стипендии -->
    Map<String, String> getData(SocGrantAssignStuListExtract extract);

    //    <!-- 34. Об оказании материальной помощи -->
    Map<String, String> getData(FinAidAssignStuListExtract extract);

    //    <!-- 35. О распределении студентов 1 курса в группы -->
    Map<String, String> getData(SplitFirstCourseStudentsGroupStuListExtract extract);

    //    <!-- 36. Об однократном повышении государственной академической стипендии -->
    Map<String, String> getData(GrantRiseStuListExtract extract);

    //    <!-- 37. О переводе с курса на следующий курс (по направлениям подготовки) со сменой территориального подразделения -->
    Map<String, String> getData(TerritorialCourseTransferStuListExtract extract);

    //    <!-- 38. О закреплении за специализациями (профилями) -->
    Map<String, String> getData(SplitStudentsExtStuListExtract extract);

    //    <!-- 39. О назначении надбавки к государственной академической стипендии -->
    Map<String, String> getData(AcadGrantBonusAssignStuListExtract extract);

    //    <!-- 40. О переводе из группы в группу -->
    Map<String, String> getData(GroupTransferExtStuListExtract extract);

    //    <!-- 41. О направлении на практику в пределах ОУ -->
    Map<String, String> getData(SendPracticeInnerStuListExtract extract);

    //    <!-- 42. О переводе студентов территориального подразделения -->
    Map<String, String> getData(TerritorialTransferExtStuListExtract extract);

//    <!-- fefu1. О направлении на практику за пределами ОУ -->
    Map<String, String> getData(SendPracticeOutStuListExtract extract);

//    <!-- fefu2. О допуске к сдаче государственных экзаменов -->
    Map<String, String> getData(FefuAdmitToStateExamsStuListExtract extract);

//    <!-- fefu3. О допуске к защите выпускной квалификационной работы -->
    Map<String, String> getData(FefuAdmitToDiplomaStuListExtract extract);

//    <!-- fefu4. О назначении академической стипендии (вступительные испытания) -->
    Map<String, String> getData(FefuAcadGrantAssignStuEnrolmentListExtract extract);

//    <!-- fefu5. О переводе с курса на следующий курс -->
    Map<String, String> getData(FefuCourseTransferStuListExtract extract);

//    <!-- fefu6. О присвоении квалификации, выдаче диплома и отчислении в связи с окончанием университета -->
    Map<String, String> getData(FefuGiveDiplomaWithDismissStuListExtract extract);

//    <!-- fefu7. О присвоении квалификации, выдаче диплома с отличием и отчислении в связи с окончанием университета -->
    Map<String, String> getData(FefuGiveDiplomaSuccessWithDismissStuListExtract extract);

//    <!-- fefu8. О предоставлении каникул (ДВФУ) -->
    Map<String, String> getData(FefuHolidayStuListExtract extract);

    //    <!-- fefu10. О зачислении на полное государственное обеспечение (списочный) -->
    Map<String, String> getData(FullStateMaintenanceEnrollmentStuListExtract extract);

     //   <!-- fefu15. О зачислении слушателей ДПО-->
    Map<String, String> getData(FefuEnrollStuDPOListExtract extract);

    //    <!-- fefu16. Об отчислении слушателей ДПО-->
    Map<String, String> getData(FefuExcludeStuDPOListExtract extract);

    //    <!-- fefu18. Приказ по контингенту ДПО/ДО-->
    Map<String, String> getData(FefuOrderContingentStuDPOListExtract extract);

    //    <!-- fefu17. О переводе слушателей ДПО-->
    Map<String, String> getData(FefuTransfStuDPOListExtract extract);


    // Прочие приказы
    Map<String, String> getData(OtherStudentExtract extract);


    Map<String, String> getBaseData(AbstractStudentExtract extract);


//    Map<String, String> getData( extract);
}
