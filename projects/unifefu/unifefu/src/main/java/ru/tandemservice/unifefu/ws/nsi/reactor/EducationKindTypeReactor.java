/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.EducationKindType;

/**
 * @author Dmitry Seleznev
 * @since 25.08.2013
 */
public class EducationKindTypeReactor extends SimpleCatalogEntityNewReactor<EducationKindType, EducationLevelStage>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<EducationLevelStage> getEntityClass()
    {
        return EducationLevelStage.class;
    }

    @Override
    public Class<EducationKindType> getNSIEntityClass()
    {
        return EducationKindType.class;
    }

    @Override
    public EducationKindType getCatalogElementRetrieveRequestObject(String guid)
    {
        EducationKindType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createEducationKindType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public EducationKindType createEntity(CoreCollectionUtils.Pair<EducationLevelStage, FefuNsiIds> entityPair)
    {
        EducationKindType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createEducationKindType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setEducationKindID(getUserCodeForNSIChecked(entityPair.getX().getUserCode()));
        nsiEntity.setEducationKindName(entityPair.getX().getTitle().length() > 100 ? entityPair.getX().getTitle().substring(0, 99) : entityPair.getX().getTitle());
        return nsiEntity;
    }

    @Override
    public EducationLevelStage createEntity(EducationKindType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getEducationKindName()) return null;

        EducationLevelStage entity = new EducationLevelStage();
        entity.setTitle(nsiEntity.getEducationKindName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(EducationLevelStage.class));
        entity.setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getEducationKindID()));
        return entity;
    }

    @Override
    public EducationLevelStage updatePossibleDuplicateFields(EducationKindType nsiEntity, CoreCollectionUtils.Pair<EducationLevelStage, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), EducationLevelStage.title().s()))
                entityPair.getX().setTitle(nsiEntity.getEducationKindName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), EducationLevelStage.userCode().s()))
                entityPair.getX().setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getEducationKindID()));
        }
        return entityPair.getX();
    }

    @Override
    public EducationKindType updateNsiEntityFields(EducationKindType nsiEntity, EducationLevelStage entity)
    {
        nsiEntity.setEducationKindName(entity.getTitle().length() > 100 ? entity.getTitle().substring(0, 99) : entity.getTitle());
        nsiEntity.setEducationKindID(getUserCodeForNSIChecked(entity.getUserCode()));
        return nsiEntity;
    }
}