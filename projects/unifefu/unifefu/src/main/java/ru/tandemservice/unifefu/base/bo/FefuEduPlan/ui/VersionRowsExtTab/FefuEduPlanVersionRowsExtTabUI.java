/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.VersionRowsExtTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.logic.IFefuEduPlanDAO;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.VersionRowExtEdit.FefuEduPlanVersionRowExtEdit;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.util.FefuEpvRowWrapper;
import ru.tandemservice.unifefu.eduplan.ext.EppEduPlanVersion.ui.BlockPub.FefuEduPlanVersionBlockPubAddon;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "versionId", required = true))
public class FefuEduPlanVersionRowsExtTabUI extends UIPresenter
{
    private Long _versionId;
    private EppEduPlanVersionBlock _block;
    private Map<EppEduPlanVersionBlock, PairKey<StaticListDataSource<FefuEpvRowWrapper>, StaticListDataSource<FefuEpvRowWrapper>>> _dataSourceMap;

    public Long getVersionId(){ return _versionId; }
    public void setVersionId(Long versionId){ _versionId = versionId; }

    public EppEduPlanVersionBlock getBlock(){ return _block; }
    public void setBlock(EppEduPlanVersionBlock block){ _block = block; }

    public StaticListDataSource<FefuEpvRowWrapper> getDataSource(){ return _dataSourceMap.get(_block).getFirst(); }
    public StaticListDataSource<FefuEpvRowWrapper> getActionDataSource(){ return _dataSourceMap.get(_block).getSecond(); }


    @Override
    public void onComponentRefresh()
    {
        IFefuEduPlanDAO dao = FefuEduPlanManager.instance().dao();
        EppEduPlanVersion version = dao.<EppEduPlanVersion>get(_versionId);

        // по умолчанию указывается общий блок
        _block = getBlock()!=null?getBlock():dao.get(EppEduPlanVersionRootBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), version);

        _dataSourceMap = dao.getEduPlanVersionBlockDataSourceMap(_versionId);

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(FefuEduPlanVersionRowsExtTab.EDU_PLAN_VERSION_BLOCK_DS))
        {
            dataSource.put("versionId", _versionId);
        }
    }

    public void onClickEditRow()
    {
        _uiActivation.asRegionDialog(FefuEduPlanVersionRowExtEdit.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong()).activate();
    }

    public void onClickEditCompetences()
    {
        this.getActivationBuilder().asCurrent("ru.tandemservice.unifefu.component.eduplan.row.FefuCompetenceAddEdit")
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, this.getListenerParameterAsLong())
                .parameter(FefuEduPlanVersionBlockPubAddon.EDUPLAN_ID, getBlock().getEduPlanVersion().getEduPlan().getId())
                .activate();
    }
}