/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.FefuForeignOnlineEntrantManager;

/**
 * @author nvankov
 * @since 6/3/13
 */
@Configuration
public class FefuForeignOnlineEntrantEdit extends BusinessComponentManager
{
    public static final String ORG_UNIT_DS = "orgUnitDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ORG_UNIT_DS, formativeOrgUnitComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitComboDSHandler()
    {
        DefaultComboDataSourceHandler handler = new DefaultComboDataSourceHandler(getName(), OrgUnit.class, OrgUnit.title())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "kr");
                subBuilder.column(DQLExpressions.property("kr", OrgUnitToKindRelation.orgUnit().id()));
                subBuilder.where(DQLExpressions.eq(DQLExpressions.property("kr", OrgUnitToKindRelation.orgUnitKind().code()), DQLExpressions.value(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)));
//                ep.dqlBuilder.column(DQLExpressions.property("e", OrgUnitToKindRelation.orgUnit()));
                ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property("e", OrgUnit.id()), subBuilder.buildQuery()));
            }
        };

        return handler;
    }
}
