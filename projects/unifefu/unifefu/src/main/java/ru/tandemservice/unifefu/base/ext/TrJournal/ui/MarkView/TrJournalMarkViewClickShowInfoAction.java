/* $Id: TrJournalMarkViewClickShowInfoAction.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.ext.TrJournal.ui.MarkView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import ru.tandemservice.unifefu.base.bo.FefuTrJournal.ui.MarkInfo.FefuTrJournalMarkInfo;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkView.TrJournalMarkViewUI;

public class TrJournalMarkViewClickShowInfoAction extends NamedUIAction
{
    protected TrJournalMarkViewClickShowInfoAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        TrJournalMarkViewUI prsnt = (TrJournalMarkViewUI) presenter;
        prsnt.getActivationBuilder().asRegion(FefuTrJournalMarkInfo.class, "dialog")
                .parameter(IUIPresenter.PUBLISHER_ID, prsnt.getListenerParameterAsLong())
                .activate();
        }
}
