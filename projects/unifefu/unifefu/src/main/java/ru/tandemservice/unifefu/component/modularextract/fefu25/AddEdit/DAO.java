/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu25.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuConditionalTransferCourseStuExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected FefuConditionalTransferCourseStuExtract createNewInstance()
    {
        return new FefuConditionalTransferCourseStuExtract();
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract()));

        model.getEduModel().setGroupModel(
                new GroupSelectModel(model.getEduModel(), model)
                        .dependFromFUTS(true)
                        .extModel(model)
                        .showChildLevels(true)
        );

        if (model.isAddForm())
        {
            model.getEduModel().setFormativeOrgUnit(model.getExtract().getEntity().getEducationOrgUnit().getFormativeOrgUnit());
            model.getEduModel().setTerritorialOrgUnit(model.getExtract().getEntity().getEducationOrgUnit().getTerritorialOrgUnit());
            model.getEduModel().setCompensationType(model.getExtract().getEntity().getCompensationType());
            model.getEduModel().setEducationLevelsHighSchool(model.getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
            model.getEduModel().setDevelopForm(model.getExtract().getEntity().getEducationOrgUnit().getDevelopForm());
            model.getEduModel().setDevelopCondition(model.getExtract().getEntity().getEducationOrgUnit().getDevelopCondition());
            model.getEduModel().setDevelopTech(model.getExtract().getEntity().getEducationOrgUnit().getDevelopTech());
            model.getEduModel().setDevelopPeriod(model.getExtract().getEntity().getEducationOrgUnit().getDevelopPeriod());
            model.getExtract().setCourseOld(model.getExtract().getEntity().getCourse());
            if (null != model.getExtract().getEntity().getGroup())
            {
                model.getExtract().setGroupOld(model.getExtract().getEntity().getGroup());
                model.getExtract().setOldGroupNumber(model.getExtract().getGroupOld() == null ? "-" : model.getExtract().getGroupOld().getTitle());
            }
            Course newCourse = get(Course.class, Course.intValue(), model.getExtract().getEntity().getCourse().getIntValue() + 1);
            if (null != newCourse && model.getEduModel().getCourseList().contains(newCourse))
                model.getEduModel().setCourse(newCourse);
        }
    }

    @Override
    public void update(Model model)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        super.update(model);
    }
}
