/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.node;

import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.validator.Validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Представление нода для чтения/проверки атрибутов.
 * @author Alexander Zhebko
 * @since 23.07.2013
 */
public interface IAttributeView
{
    /**
     * Возвращает отображение название артибута на тип, к которому его требуется привести. По умолчанию тип атрибута - строка.
     * @return отображение навзание атрибута на его тип
     */
    Map<String, Class<?>> getAttributeTypeMap();

    /**
     * Возвращает отображение названия атрибута на список валидаторов, проверку которых значение этого атрибута должно проходить.
     * @return отображение названия атрибута на список его валидаторов
     */
    Map<String, List<Validator<?>>> getAttributeValidatorMap();

    /**
     * Возвращает список обязательных у этого элемента атрибутов.
     * @return список обязательных атрибутов
     */
    List<String> getRequiredAttributes();

    /**
     * Возвращает список атрибутов, значения которых необходимо перевести в верхний регистр.
     * @return список атрибутов, которые нужно перевести в верхний регистр
     */
    List<String> getUpperCaseAttributes();


    /**
     * Пустышка
     */
    IAttributeView EMPTY = new IAttributeView()
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return SafeMap.get(key -> String.class);
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return SafeMap.get(ArrayList.class);
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return Collections.emptyList();
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return Collections.emptyList();
        }
    };
}