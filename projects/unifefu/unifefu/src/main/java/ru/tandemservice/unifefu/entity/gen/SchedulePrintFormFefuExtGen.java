package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt;
import ru.tandemservice.unispp.base.entity.SppSchedulePrintForm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатная форма расписания (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SchedulePrintFormFefuExtGen extends EntityBase
 implements INaturalIdentifiable<SchedulePrintFormFefuExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt";
    public static final String ENTITY_NAME = "schedulePrintFormFefuExt";
    public static final int VERSION_HASH = -413892349;
    private static IEntityMeta ENTITY_META;

    public static final String L_SPP_SCHEDULE_PRINT_FORM = "sppSchedulePrintForm";
    public static final String P_EDU_START_DATE = "eduStartDate";
    public static final String P_ADMIN_PHONE_NUM = "adminPhoneNum";
    public static final String P_TERM = "term";
    public static final String L_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String L_EDU_LEVEL = "eduLevel";
    public static final String L_ADMIN = "admin";
    public static final String L_CHIEF_U_M_U = "chiefUMU";
    public static final String P_FORM_WITH_TERR_TITLE = "formWithTerrTitle";

    private SppSchedulePrintForm _sppSchedulePrintForm;     // Печатная форма расписания
    private Date _eduStartDate;     // Дата начала обучения
    private String _adminPhoneNum;     // Телефон администратора ООП
    private long _term;     // Семестр
    private OrgUnit _territorialOrgUnit;     // Территориальное подразделение
    private EducationLevelsHighSchool _eduLevel;     // Направление подготовки/специальности
    private EmployeePost _admin;     // Администратор ООП
    private EmployeePost _chiefUMU;     // Начальник УМУ подразделения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма расписания. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SppSchedulePrintForm getSppSchedulePrintForm()
    {
        return _sppSchedulePrintForm;
    }

    /**
     * @param sppSchedulePrintForm Печатная форма расписания. Свойство не может быть null и должно быть уникальным.
     */
    public void setSppSchedulePrintForm(SppSchedulePrintForm sppSchedulePrintForm)
    {
        dirty(_sppSchedulePrintForm, sppSchedulePrintForm);
        _sppSchedulePrintForm = sppSchedulePrintForm;
    }

    /**
     * @return Дата начала обучения.
     */
    public Date getEduStartDate()
    {
        return _eduStartDate;
    }

    /**
     * @param eduStartDate Дата начала обучения.
     */
    public void setEduStartDate(Date eduStartDate)
    {
        dirty(_eduStartDate, eduStartDate);
        _eduStartDate = eduStartDate;
    }

    /**
     * @return Телефон администратора ООП.
     */
    @Length(max=255)
    public String getAdminPhoneNum()
    {
        return _adminPhoneNum;
    }

    /**
     * @param adminPhoneNum Телефон администратора ООП.
     */
    public void setAdminPhoneNum(String adminPhoneNum)
    {
        dirty(_adminPhoneNum, adminPhoneNum);
        _adminPhoneNum = adminPhoneNum;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public long getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(long term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подразделение. Свойство не может быть null.
     */
    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Направление подготовки/специальности.
     */
    public EducationLevelsHighSchool getEduLevel()
    {
        return _eduLevel;
    }

    /**
     * @param eduLevel Направление подготовки/специальности.
     */
    public void setEduLevel(EducationLevelsHighSchool eduLevel)
    {
        dirty(_eduLevel, eduLevel);
        _eduLevel = eduLevel;
    }

    /**
     * @return Администратор ООП.
     */
    public EmployeePost getAdmin()
    {
        return _admin;
    }

    /**
     * @param admin Администратор ООП.
     */
    public void setAdmin(EmployeePost admin)
    {
        dirty(_admin, admin);
        _admin = admin;
    }

    /**
     * @return Начальник УМУ подразделения.
     */
    public EmployeePost getChiefUMU()
    {
        return _chiefUMU;
    }

    /**
     * @param chiefUMU Начальник УМУ подразделения.
     */
    public void setChiefUMU(EmployeePost chiefUMU)
    {
        dirty(_chiefUMU, chiefUMU);
        _chiefUMU = chiefUMU;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SchedulePrintFormFefuExtGen)
        {
            if (withNaturalIdProperties)
            {
                setSppSchedulePrintForm(((SchedulePrintFormFefuExt)another).getSppSchedulePrintForm());
            }
            setEduStartDate(((SchedulePrintFormFefuExt)another).getEduStartDate());
            setAdminPhoneNum(((SchedulePrintFormFefuExt)another).getAdminPhoneNum());
            setTerm(((SchedulePrintFormFefuExt)another).getTerm());
            setTerritorialOrgUnit(((SchedulePrintFormFefuExt)another).getTerritorialOrgUnit());
            setEduLevel(((SchedulePrintFormFefuExt)another).getEduLevel());
            setAdmin(((SchedulePrintFormFefuExt)another).getAdmin());
            setChiefUMU(((SchedulePrintFormFefuExt)another).getChiefUMU());
        }
    }

    public INaturalId<SchedulePrintFormFefuExtGen> getNaturalId()
    {
        return new NaturalId(getSppSchedulePrintForm());
    }

    public static class NaturalId extends NaturalIdBase<SchedulePrintFormFefuExtGen>
    {
        private static final String PROXY_NAME = "SchedulePrintFormFefuExtNaturalProxy";

        private Long _sppSchedulePrintForm;

        public NaturalId()
        {}

        public NaturalId(SppSchedulePrintForm sppSchedulePrintForm)
        {
            _sppSchedulePrintForm = ((IEntity) sppSchedulePrintForm).getId();
        }

        public Long getSppSchedulePrintForm()
        {
            return _sppSchedulePrintForm;
        }

        public void setSppSchedulePrintForm(Long sppSchedulePrintForm)
        {
            _sppSchedulePrintForm = sppSchedulePrintForm;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SchedulePrintFormFefuExtGen.NaturalId) ) return false;

            SchedulePrintFormFefuExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getSppSchedulePrintForm(), that.getSppSchedulePrintForm()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getSppSchedulePrintForm());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getSppSchedulePrintForm());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SchedulePrintFormFefuExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SchedulePrintFormFefuExt.class;
        }

        public T newInstance()
        {
            return (T) new SchedulePrintFormFefuExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sppSchedulePrintForm":
                    return obj.getSppSchedulePrintForm();
                case "eduStartDate":
                    return obj.getEduStartDate();
                case "adminPhoneNum":
                    return obj.getAdminPhoneNum();
                case "term":
                    return obj.getTerm();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "eduLevel":
                    return obj.getEduLevel();
                case "admin":
                    return obj.getAdmin();
                case "chiefUMU":
                    return obj.getChiefUMU();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sppSchedulePrintForm":
                    obj.setSppSchedulePrintForm((SppSchedulePrintForm) value);
                    return;
                case "eduStartDate":
                    obj.setEduStartDate((Date) value);
                    return;
                case "adminPhoneNum":
                    obj.setAdminPhoneNum((String) value);
                    return;
                case "term":
                    obj.setTerm((Long) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((OrgUnit) value);
                    return;
                case "eduLevel":
                    obj.setEduLevel((EducationLevelsHighSchool) value);
                    return;
                case "admin":
                    obj.setAdmin((EmployeePost) value);
                    return;
                case "chiefUMU":
                    obj.setChiefUMU((EmployeePost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sppSchedulePrintForm":
                        return true;
                case "eduStartDate":
                        return true;
                case "adminPhoneNum":
                        return true;
                case "term":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "eduLevel":
                        return true;
                case "admin":
                        return true;
                case "chiefUMU":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sppSchedulePrintForm":
                    return true;
                case "eduStartDate":
                    return true;
                case "adminPhoneNum":
                    return true;
                case "term":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "eduLevel":
                    return true;
                case "admin":
                    return true;
                case "chiefUMU":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sppSchedulePrintForm":
                    return SppSchedulePrintForm.class;
                case "eduStartDate":
                    return Date.class;
                case "adminPhoneNum":
                    return String.class;
                case "term":
                    return Long.class;
                case "territorialOrgUnit":
                    return OrgUnit.class;
                case "eduLevel":
                    return EducationLevelsHighSchool.class;
                case "admin":
                    return EmployeePost.class;
                case "chiefUMU":
                    return EmployeePost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SchedulePrintFormFefuExt> _dslPath = new Path<SchedulePrintFormFefuExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SchedulePrintFormFefuExt");
    }
            

    /**
     * @return Печатная форма расписания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getSppSchedulePrintForm()
     */
    public static SppSchedulePrintForm.Path<SppSchedulePrintForm> sppSchedulePrintForm()
    {
        return _dslPath.sppSchedulePrintForm();
    }

    /**
     * @return Дата начала обучения.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getEduStartDate()
     */
    public static PropertyPath<Date> eduStartDate()
    {
        return _dslPath.eduStartDate();
    }

    /**
     * @return Телефон администратора ООП.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getAdminPhoneNum()
     */
    public static PropertyPath<String> adminPhoneNum()
    {
        return _dslPath.adminPhoneNum();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getTerm()
     */
    public static PropertyPath<Long> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getTerritorialOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Направление подготовки/специальности.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getEduLevel()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> eduLevel()
    {
        return _dslPath.eduLevel();
    }

    /**
     * @return Администратор ООП.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getAdmin()
     */
    public static EmployeePost.Path<EmployeePost> admin()
    {
        return _dslPath.admin();
    }

    /**
     * @return Начальник УМУ подразделения.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getChiefUMU()
     */
    public static EmployeePost.Path<EmployeePost> chiefUMU()
    {
        return _dslPath.chiefUMU();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getFormWithTerrTitle()
     */
    public static SupportedPropertyPath<String> formWithTerrTitle()
    {
        return _dslPath.formWithTerrTitle();
    }

    public static class Path<E extends SchedulePrintFormFefuExt> extends EntityPath<E>
    {
        private SppSchedulePrintForm.Path<SppSchedulePrintForm> _sppSchedulePrintForm;
        private PropertyPath<Date> _eduStartDate;
        private PropertyPath<String> _adminPhoneNum;
        private PropertyPath<Long> _term;
        private OrgUnit.Path<OrgUnit> _territorialOrgUnit;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _eduLevel;
        private EmployeePost.Path<EmployeePost> _admin;
        private EmployeePost.Path<EmployeePost> _chiefUMU;
        private SupportedPropertyPath<String> _formWithTerrTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма расписания. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getSppSchedulePrintForm()
     */
        public SppSchedulePrintForm.Path<SppSchedulePrintForm> sppSchedulePrintForm()
        {
            if(_sppSchedulePrintForm == null )
                _sppSchedulePrintForm = new SppSchedulePrintForm.Path<SppSchedulePrintForm>(L_SPP_SCHEDULE_PRINT_FORM, this);
            return _sppSchedulePrintForm;
        }

    /**
     * @return Дата начала обучения.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getEduStartDate()
     */
        public PropertyPath<Date> eduStartDate()
        {
            if(_eduStartDate == null )
                _eduStartDate = new PropertyPath<Date>(SchedulePrintFormFefuExtGen.P_EDU_START_DATE, this);
            return _eduStartDate;
        }

    /**
     * @return Телефон администратора ООП.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getAdminPhoneNum()
     */
        public PropertyPath<String> adminPhoneNum()
        {
            if(_adminPhoneNum == null )
                _adminPhoneNum = new PropertyPath<String>(SchedulePrintFormFefuExtGen.P_ADMIN_PHONE_NUM, this);
            return _adminPhoneNum;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getTerm()
     */
        public PropertyPath<Long> term()
        {
            if(_term == null )
                _term = new PropertyPath<Long>(SchedulePrintFormFefuExtGen.P_TERM, this);
            return _term;
        }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getTerritorialOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new OrgUnit.Path<OrgUnit>(L_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Направление подготовки/специальности.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getEduLevel()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> eduLevel()
        {
            if(_eduLevel == null )
                _eduLevel = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDU_LEVEL, this);
            return _eduLevel;
        }

    /**
     * @return Администратор ООП.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getAdmin()
     */
        public EmployeePost.Path<EmployeePost> admin()
        {
            if(_admin == null )
                _admin = new EmployeePost.Path<EmployeePost>(L_ADMIN, this);
            return _admin;
        }

    /**
     * @return Начальник УМУ подразделения.
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getChiefUMU()
     */
        public EmployeePost.Path<EmployeePost> chiefUMU()
        {
            if(_chiefUMU == null )
                _chiefUMU = new EmployeePost.Path<EmployeePost>(L_CHIEF_U_M_U, this);
            return _chiefUMU;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt#getFormWithTerrTitle()
     */
        public SupportedPropertyPath<String> formWithTerrTitle()
        {
            if(_formWithTerrTitle == null )
                _formWithTerrTitle = new SupportedPropertyPath<String>(SchedulePrintFormFefuExtGen.P_FORM_WITH_TERR_TITLE, this);
            return _formWithTerrTitle;
        }

        public Class getEntityClass()
        {
            return SchedulePrintFormFefuExt.class;
        }

        public String getEntityName()
        {
            return "schedulePrintFormFefuExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFormWithTerrTitle();
}
