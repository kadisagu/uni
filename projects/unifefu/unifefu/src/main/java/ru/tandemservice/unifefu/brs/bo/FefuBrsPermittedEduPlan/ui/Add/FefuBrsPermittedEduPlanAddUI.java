/*$Id$*/
package ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.FefuBrsPermittedEduPlanManager;

import java.util.Collection;

/**
 * @author DMITRY KNYAZEV
 * @since 22.01.2015
 */
public class FefuBrsPermittedEduPlanAddUI extends UIPresenter
{
    EduProgramKind _eduProgramKind;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case FefuBrsPermittedEduPlanAdd.WORK_PLAN_DS:
            {
                dataSource.put(FefuBrsPermittedEduPlanAdd.PARAM_NUMBER, getNumber());

                dataSource.put(FefuBrsPermittedEduPlanAdd.PARAM_PROGRAM_FORM_LIST, getProgramForms());
                dataSource.put(FefuBrsPermittedEduPlanAdd.PARAM_DEVELOP_CONDITION_LIST, getDevelopConditions());
                dataSource.put(FefuBrsPermittedEduPlanAdd.PARAM_PROGRAM_TRAIT_LIST, getProgramTraits());
                dataSource.put(FefuBrsPermittedEduPlanAdd.PARAM_TERM, getTerm());
                dataSource.put(FefuBrsPermittedEduPlanAdd.PARAM_STATE, getState());
                dataSource.put(FefuBrsPermittedEduPlanAdd.PARAM_PROGRAM_SUBJECT, getProgramSubject());
                dataSource.put(FefuBrsPermittedEduPlanAdd.PARAM_PROGRAM_SPECIALIZATION_SUBJECT_LIST, getProgramSpecializations());
            }
            case FefuBrsPermittedEduPlanAdd.PROGRAM_SUBJECT_DS:
            {
                dataSource.put(FefuBrsPermittedEduPlanAdd.PARAM_EDU_YEAR_PROCESS, getEduYearProcess());
                dataSource.put(FefuBrsPermittedEduPlanAdd.PARAM_PROGRAM_KIND, getEduProgramKind());
                break;
            }

            case FefuBrsPermittedEduPlanAdd.PROGRAM_SPECIALIZATION_DS:
            {
                dataSource.put(FefuBrsPermittedEduPlanAdd.PARAM_PROGRAM_SUBJECT, getProgramSubject());
            }

        }
    }

    //handlers
    public void onClickApply()
    {
        FefuBrsPermittedEduPlanManager.instance().dao().saveWorkPlansAsPermitted(getSelectedEntities());
        deactivate();
    }

    //getters/setters
    public EduProgramKind getEduProgramKind()
    {
        return _eduProgramKind;
    }

    public void setEduProgramKind(EduProgramKind eduProgramKind)
    {
        _eduProgramKind = eduProgramKind;
    }

    private IUIDataSource getWorkPlanDS()
    {
        return getConfig().getDataSource(FefuBrsPermittedEduPlanAdd.WORK_PLAN_DS);
    }

    private Collection<IEntity> getSelectedEntities()
    {
        return ((CheckboxColumn) ((PageableSearchListDataSource) getWorkPlanDS()).getLegacyDataSource().getColumn(FefuBrsPermittedEduPlanAdd.CHECKBOX_COLUMN_NAME)).getSelectedObjects();
    }

    @SuppressWarnings("UnusedDeclaration")
    public boolean isNothingSelected()
    {
        return getEduProgramKind() == null;
    }

    public String getNumber()
    {
        return getSettings().get("number");
    }

    public EppYearEducationProcess getEduYearProcess()
    {
        return getSettings().get("yearEducationProcess");
    }

    public EduProgramSubject getProgramSubject()
    {
        return getSettings().get("programSubject");
    }

    public Collection<EduProgramSpecialization> getProgramSpecializations()
    {
        Collection<EduProgramSpecialization> programSpecializations = getSettings().get("programSpecializations");
        return programSpecializations == null || programSpecializations.isEmpty() ? null : programSpecializations;
    }

    public Collection<EduProgramForm> getProgramForms()
    {
        Collection<EduProgramForm> programFormList = getSettings().get("programFormList");
        return programFormList == null || programFormList.isEmpty() ? null : programFormList;
    }

    public Collection<DevelopCondition> getDevelopConditions()
    {
        Collection<DevelopCondition> developConditionList = getSettings().get("developConditionList");
        return developConditionList == null || developConditionList.isEmpty() ? null : developConditionList;
    }

    public Collection<EduProgramTrait> getProgramTraits()
    {
        Collection<EduProgramTrait> programTraitList = getSettings().get("programTraitList");
        return programTraitList == null || programTraitList.isEmpty() ? null : programTraitList;
    }

    public Term getTerm()
    {
        return getSettings().get("term");
    }

    public EppState getState()
    {
        return getSettings().get("state");
    }
}
