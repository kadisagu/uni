/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu9.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class DAO extends AbstractListParagraphPubDAO<FefuFormativeTransferStuListExtract, Model> implements IDAO
{
}
