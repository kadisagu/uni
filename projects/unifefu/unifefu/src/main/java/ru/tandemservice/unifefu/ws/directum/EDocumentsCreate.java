/**
 * EDocumentsCreate.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class EDocumentsCreate  implements java.io.Serializable {
    private java.lang.String XMLPackage;

    private byte[][] documents;

    public EDocumentsCreate() {
    }

    public EDocumentsCreate(
           java.lang.String XMLPackage,
           byte[][] documents) {
           this.XMLPackage = XMLPackage;
           this.documents = documents;
    }


    /**
     * Gets the XMLPackage value for this EDocumentsCreate.
     * 
     * @return XMLPackage
     */
    public java.lang.String getXMLPackage() {
        return XMLPackage;
    }


    /**
     * Sets the XMLPackage value for this EDocumentsCreate.
     * 
     * @param XMLPackage
     */
    public void setXMLPackage(java.lang.String XMLPackage) {
        this.XMLPackage = XMLPackage;
    }


    /**
     * Gets the documents value for this EDocumentsCreate.
     * 
     * @return documents
     */
    public byte[][] getDocuments() {
        return documents;
    }


    /**
     * Sets the documents value for this EDocumentsCreate.
     * 
     * @param documents
     */
    public void setDocuments(byte[][] documents) {
        this.documents = documents;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EDocumentsCreate)) return false;
        EDocumentsCreate other = (EDocumentsCreate) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.XMLPackage==null && other.getXMLPackage()==null) || 
             (this.XMLPackage!=null &&
              this.XMLPackage.equals(other.getXMLPackage()))) &&
            ((this.documents==null && other.getDocuments()==null) || 
             (this.documents!=null &&
              java.util.Arrays.equals(this.documents, other.getDocuments())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getXMLPackage() != null) {
            _hashCode += getXMLPackage().hashCode();
        }
        if (getDocuments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDocuments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDocuments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EDocumentsCreate.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">EDocumentsCreate"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("XMLPackage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "XMLPackage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documents");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "Documents"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "base64Binary"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
