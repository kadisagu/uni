/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee;

/**
 * @author Nikolay Fedorovskih
 * @since 11.06.2013
 */
@Transactional
public class FefuTechnicalCommissionsDAO extends CommonDAO implements IFefuTechnicalCommissionsDAO
{
    @Override
    public void saveEmployeeFromTechnicalCommission(FefuTechnicalCommissionEmployee tcEmployee)
    {
        getSession().refresh(tcEmployee.getTechnicalCommission());
        saveOrUpdate(tcEmployee);
    }
}