package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x2_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewGrant

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewgrant_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("prikaz_id", DBType.LONG).setNullable(false), 
				new DBColumn("grantsizeindac_p", DBType.DOUBLE), 
				new DBColumn("groupmanagerbonussizeindac_p", DBType.DOUBLE), 
				new DBColumn("grantsizeindacenr_p", DBType.DOUBLE), 
				new DBColumn("grpmngrbnsszindacenr_p", DBType.DOUBLE), 
				new DBColumn("grantsizeindonce_p", DBType.DOUBLE), 
				new DBColumn("grantsizeindbonus_p", DBType.DOUBLE), 
				new DBColumn("grantsizelistac_p", DBType.DOUBLE), 
				new DBColumn("groupmanagerbonussizelistac_p", DBType.DOUBLE), 
				new DBColumn("grantsizelistacenr_p", DBType.LONG), 
				new DBColumn("grpmngrbnsszlstacenr_p", DBType.LONG), 
				new DBColumn("grantsizelistbonus_p", DBType.DOUBLE), 
				new DBColumn("grantsizelistonce_p", DBType.DOUBLE), 
				new DBColumn("grantsizelistsoc_p", DBType.DOUBLE), 
				new DBColumn("grantsizelistfinaid_p", DBType.DOUBLE), 
				new DBColumn("grantstartdate_p", DBType.DATE), 
				new DBColumn("grantenddate_p", DBType.DATE), 
				new DBColumn("groupmanagerbonusstartdate_p", DBType.DATE), 
				new DBColumn("groupmanagerbonusenddate_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewGrant");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPractice

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewpractice_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("prikaz_id", DBType.LONG).setNullable(false), 
				new DBColumn("practicekind_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("practiceplace_p", DBType.createVarchar(255)), 
				new DBColumn("practiceplacelegalform_p", DBType.createVarchar(255)), 
				new DBColumn("practicecity_p", DBType.createVarchar(255)), 
				new DBColumn("practicedates_p", DBType.createVarchar(255)), 
				new DBColumn("practiceheader_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewPractice");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPrikazy

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("mdbviewprikazy_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("extract_id", DBType.LONG).setNullable(false), 
				new DBColumn("order_id", DBType.LONG).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("ordernumber_p", DBType.createVarchar(255)), 
				new DBColumn("ordertype_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("modularreason_p", DBType.createVarchar(255)), 
				new DBColumn("listreason_p", DBType.createVarchar(255)), 
				new DBColumn("listbasics_p", DBType.createVarchar(2048)),
				new DBColumn("createdate_p", DBType.DATE), 
				new DBColumn("orderdate_p", DBType.DATE), 
				new DBColumn("lastname_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("firstname_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("middlename_p", DBType.createVarchar(255)), 
				new DBColumn("personbenefits_p", DBType.createVarchar(255)), 
				new DBColumn("description_p", DBType.createVarchar(2048))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("mdbViewPrikazy");

		}


    }
}