package ru.tandemservice.unifefu.entity.entrantOrder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.entity.entrantOrder.EntrFefuOutOfCompetitionBudgetExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О зачислении в число студентов (бюджет, вне конкурса)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrFefuOutOfCompetitionBudgetExtractGen extends EnrollmentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.entrantOrder.EntrFefuOutOfCompetitionBudgetExtract";
    public static final String ENTITY_NAME = "entrFefuOutOfCompetitionBudgetExtract";
    public static final int VERSION_HASH = -1915065443;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EntrFefuOutOfCompetitionBudgetExtractGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrFefuOutOfCompetitionBudgetExtractGen> extends EnrollmentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrFefuOutOfCompetitionBudgetExtract.class;
        }

        public T newInstance()
        {
            return (T) new EntrFefuOutOfCompetitionBudgetExtract();
        }
    }
    private static final Path<EntrFefuOutOfCompetitionBudgetExtract> _dslPath = new Path<EntrFefuOutOfCompetitionBudgetExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrFefuOutOfCompetitionBudgetExtract");
    }
            

    public static class Path<E extends EntrFefuOutOfCompetitionBudgetExtract> extends EnrollmentExtract.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EntrFefuOutOfCompetitionBudgetExtract.class;
        }

        public String getEntityName()
        {
            return "entrFefuOutOfCompetitionBudgetExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
