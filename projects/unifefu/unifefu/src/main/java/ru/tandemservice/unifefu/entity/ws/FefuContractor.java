package ru.tandemservice.unifefu.entity.ws;

import ru.tandemservice.unifefu.entity.ws.gen.FefuContractorGen;

/**
 * Контрагент по договору ДВФУ
 */
public class FefuContractor extends FefuContractorGen
{
    @Override
    public String getFullTitle()
    {
        return "Контрагент по договору ДВФУ";
    }
}