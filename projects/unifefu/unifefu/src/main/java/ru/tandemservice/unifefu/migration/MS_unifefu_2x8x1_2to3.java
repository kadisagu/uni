/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexGenerationCodes;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuEduStandartGenerationCodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 16.06.2015
 */
public class MS_unifefu_2x8x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.8.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<String, Long> standartGenerationMap = new HashMap<>();

        PreparedStatement selectStandartGeneration = tool.prepareStatement("select id, code_p from fefu_c_edu_standart_gen_t");
        selectStandartGeneration.execute();
        ResultSet standartGenerationRes = selectStandartGeneration.getResultSet();
        while (standartGenerationRes.next())
        {
            Long stateStandartId = standartGenerationRes.getLong(1);
            String generationCode = standartGenerationRes.getString(2);
            standartGenerationMap.put(generationCode, stateStandartId);
        }


        PreparedStatement selectEduStandart = tool.prepareStatement("SELECT t1.ID, t4.CODE_P, t5.ID  " +
                                                                            "FROM epp_stateedustd_t t1 " +
                                                                            "LEFT JOIN edu_c_pr_subject_t t2 ON t1.PROGRAMSUBJECT_ID = t2.ID " +
                                                                            "LEFT JOIN edu_c_pr_subject_index_t t3 ON t2.SUBJECTINDEX_ID = t3.ID " +
                                                                            "LEFT JOIN edu_c_pr_subject_index_gen_t t4 ON t3.GENERATION_ID = t4.ID  " +
                                                                            "LEFT JOIN fefueppstateedustandardext_t t5 ON t1.ID = t5.EDUSTANDARD_ID ");
        selectEduStandart.execute();
        ResultSet res = selectEduStandart.getResultSet();
        while (res.next())
        {
            Long stateStandartId = res.getLong(1);
            String generationCode = res.getString(2);
            Long extId = res.getLong(3);
            Long defaultEduStandardGen = generationCode.equals(EduProgramSubjectIndexGenerationCodes.OKSO) ? standartGenerationMap.get(FefuEduStandartGenerationCodes.GOS_2) :
                    standartGenerationMap.get(FefuEduStandartGenerationCodes.FGOS_3);
            if (0 == extId)
            {
                short entityCode = EntityRuntime.getMeta(FefuEppStateEduStandardExt.class).getEntityCode();
                Long id = EntityIDGenerator.generateNewId(entityCode);
                PreparedStatement insertStmt = tool.prepareStatement("insert into fefueppstateedustandardext_t (ID, DISCRIMINATOR, EDUSTANDARD_ID, CUSTOMEDUSTANDART_P, EDUSTANDARDGEN_ID) values (?, ?, ?, ?, ?)");
                insertStmt.setLong(1, id);
                insertStmt.setShort(2, entityCode);
                insertStmt.setLong(3, stateStandartId);
                insertStmt.setBoolean(4, false);
                insertStmt.setLong(5, defaultEduStandardGen);
                insertStmt.execute();
            }
        }
    }
}
