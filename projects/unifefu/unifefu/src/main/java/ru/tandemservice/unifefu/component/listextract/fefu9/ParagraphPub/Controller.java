/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu9.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubController;
import ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class Controller extends AbstractListParagraphPubController<FefuFormativeTransferStuListExtract, IDAO, Model>
{
}
