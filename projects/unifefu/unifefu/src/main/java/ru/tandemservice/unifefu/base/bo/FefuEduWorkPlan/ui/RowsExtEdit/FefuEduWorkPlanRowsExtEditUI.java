/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.ui.RowsExtEdit;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;
import ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.FefuEduWorkPlanManager;
import ru.tandemservice.unifefu.base.vo.FefuLoadTypeVO;
import ru.tandemservice.unifefu.entity.catalog.FefuLoadType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuLoadTypeCodes;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 1/21/14
 */
@Input(@Bind(key = UIPresenter.PUBLISHER_ID, binding = "rowId"))
public class FefuEduWorkPlanRowsExtEditUI extends UIPresenter
{
    private Long _rowId;
    private EppWorkPlanRegistryElementRow _row;
    private List<FefuLoadTypeVO> _loadTypeList = Collections.emptyList();
    private FefuLoadTypeVO _currentLoadType;

    private List<EppWorkPlanPart> _partsList = Collections.emptyList();
    private EppWorkPlanPart _currentPart;

    private Map<Integer, Map<String, FefuEppWorkPlanRowPartLoad>> _fefuPartLoadMap = Maps.newHashMap();
    private Map<Integer, Map<String, EppWorkPlanRowPartLoad>> _eppPartLoadMap = Maps.newHashMap();
    private IEppRegElPartWrapper _registryElementPartWrapper;

    @Override
    public void onComponentRender()
    {
        _uiSupport.setPageTitle(getTitle() + " Распределение нагрузки по частям семестра");
    }

    @Override
    public void onComponentRefresh()
    {
//        if(null == _row)
            _row = DataAccessServices.dao().get(_rowId);
        _loadTypeList = FefuEduWorkPlanManager.instance().dao().getLoadTypeList();
        _partsList = FefuEduWorkPlanManager.instance().dao().getPartsForRow(_row.getWorkPlan().getId());
        _fefuPartLoadMap = FefuEduWorkPlanManager.instance().dao().getFefuPartLoadMapForRow(_rowId);
        _eppPartLoadMap = FefuEduWorkPlanManager.instance().dao().getEppPartLoadMapForRow(_rowId);

        _registryElementPartWrapper = IEppRegistryDAO.instance.get().getRegistryElementPartDataMap(
                        Collections.singleton(_row.getRegistryElementPart().getId())
                ).get(_row.getRegistryElementPart().getId());

    }

    public Long getRowId()
    {
        return _rowId;
    }

    public void setRowId(Long rowId)
    {
        _rowId = rowId;
    }

    public EppWorkPlanRegistryElementRow getRow()
    {
        return _row;
    }

    public void setRow(EppWorkPlanRegistryElementRow row)
    {
        _row = row;
    }

    public List<FefuLoadTypeVO> getLoadTypeList()
    {
        return _loadTypeList;
    }

    public void setLoadTypeList(List<FefuLoadTypeVO> loadTypeList)
    {
        _loadTypeList = loadTypeList;
    }

    public FefuLoadTypeVO getCurrentLoadType()
    {
        return _currentLoadType;
    }

    public void setCurrentLoadType(FefuLoadTypeVO currentLoadType)
    {
        _currentLoadType = currentLoadType;
    }

    public List<EppWorkPlanPart> getPartsList()
    {
        return _partsList;
    }

    public void setPartsList(List<EppWorkPlanPart> partsList)
    {
        _partsList = partsList;
    }

    public EppWorkPlanPart getCurrentPart()
    {
        return _currentPart;
    }

    public void setCurrentPart(EppWorkPlanPart currentPart)
    {
        _currentPart = currentPart;
    }

    public Map<Integer, Map<String, FefuEppWorkPlanRowPartLoad>> getFefuPartLoadMap()
    {
        return _fefuPartLoadMap;
    }

    public void setFefuPartLoadMap(Map<Integer, Map<String, FefuEppWorkPlanRowPartLoad>> fefuPartLoadMap)
    {
        _fefuPartLoadMap = fefuPartLoadMap;
    }

    public Map<Integer, Map<String, EppWorkPlanRowPartLoad>> getEppPartLoadMap()
    {
        return _eppPartLoadMap;
    }

    public void setEppPartLoadMap(Map<Integer, Map<String, EppWorkPlanRowPartLoad>> eppPartLoadMap)
    {
        _eppPartLoadMap = eppPartLoadMap;
    }

    private FefuEppWorkPlanRowPartLoad getFefuEppWorkPlanRowPartLoad()
    {
        if(null == _fefuPartLoadMap.get(_currentPart.getNumber()))
            _fefuPartLoadMap.put(_currentPart.getNumber(), Maps.<String, FefuEppWorkPlanRowPartLoad>newHashMap());
        Map<String, FefuEppWorkPlanRowPartLoad> map = _fefuPartLoadMap.get(_currentPart.getNumber());
        FefuEppWorkPlanRowPartLoad fefuPartLoad= map.get(_currentLoadType.getCode());
        if (null == fefuPartLoad)
        {
            fefuPartLoad = new FefuEppWorkPlanRowPartLoad();
            fefuPartLoad.setRow(_row);
            fefuPartLoad.setPart(_currentPart.getNumber());
            fefuPartLoad.setLoadType(_currentLoadType.getFefuLoadType());
            fefuPartLoad.setLoadAsDouble(null);
            map.put(_currentLoadType.getCode(), fefuPartLoad);
        }

        return fefuPartLoad;
    }

    private EppWorkPlanRowPartLoad getEppWorkPlanRowPartLoad()
    {
        if(null == _eppPartLoadMap.get(_currentPart.getNumber()) || null == _eppPartLoadMap.get(_currentPart.getNumber()).get(_currentLoadType.getCode()))
            return null;
        final Map<String, EppWorkPlanRowPartLoad> map = _eppPartLoadMap.get(_currentPart.getNumber());
        EppWorkPlanRowPartLoad eppWorkPlanRowPartLoad = map.get(_currentLoadType.getCode());
        return eppWorkPlanRowPartLoad;
    }


    public Double getCurrentValue() {
        if(_currentLoadType.isFefuLoadType())
            return getFefuEppWorkPlanRowPartLoad().getLoadAsDouble();
        else
        {
            if(null == getEppWorkPlanRowPartLoad()) return null;
            else return getEppWorkPlanRowPartLoad().getLoadAsDouble();
        }
    }

    public void setCurrentValue(final Double value) {
        if(_currentLoadType.isFefuLoadType())
            getFefuEppWorkPlanRowPartLoad().setLoadAsDouble(value);
    }
    public String getInputId(Integer part, String loadTypeCode) {
        return "load_"+part+"_"+loadTypeCode;
    }

    public String getInputId() {
        return getInputId(_currentPart.getNumber(), _currentLoadType.getCode());
    }

    public String getTitle() {
        return UniEppUtils.getEppWorkPlanRowFormTitle(_rowId);
    }

    public String getCurrentLoadTypeRegistryLoad()
    {
        if(_currentLoadType.isEppLoadType())
        {
            if (null == _registryElementPartWrapper) { return ""; }
            return UniEppUtils.formatLoad(_registryElementPartWrapper.getLoadAsDouble(_currentLoadType.getCode()), false);
        }
        else
        {
            return "";
        }
    }

    public void onClickApply()
    {
        List<FefuEppWorkPlanRowPartLoad> createOrUpdatePartLoads = Lists.newArrayList();
        List<FefuEppWorkPlanRowPartLoad> deletePartLoads = Lists.newArrayList();

        Map<String, String> loadTypesMap = Maps.newHashMap();
        loadTypesMap.put(FefuLoadTypeCodes.TYPE_LECTURES_INTER, EppALoadTypeCodes.TYPE_LECTURES);
        loadTypesMap.put(FefuLoadTypeCodes.TYPE_PRACTICE_INTER, EppALoadTypeCodes.TYPE_PRACTICE);
        loadTypesMap.put(FefuLoadTypeCodes.TYPE_LABS_INTER, EppALoadTypeCodes.TYPE_LABS);

        Map<String, EppALoadType> eppLoadTypeMap = Maps.newHashMap();
        for(FefuLoadTypeVO loadTypeVO : _loadTypeList)
        {
            if(loadTypeVO.isEppLoadType())
            {
                eppLoadTypeMap.put(loadTypeVO.getEppLoadType().getCode(), loadTypeVO.getEppLoadType());
            }
        }

        for(FefuLoadTypeVO loadTypeVO : _loadTypeList)
        {
            if(loadTypeVO.isFefuLoadType())
            {
                for(EppWorkPlanPart part : _partsList)
                {
                    FefuEppWorkPlanRowPartLoad partLoad = _fefuPartLoadMap.get(part.getNumber()).get(loadTypeVO.getCode());
                    if(null == partLoad.getLoadAsDouble())
                    {
                        if(null != partLoad.getId())
                            deletePartLoads.add(partLoad);
                    }
                    else
                    {
                        createOrUpdatePartLoads.add(partLoad);
                        if(!FefuLoadTypeCodes.TYPE_EXAM_HOURS.equals(partLoad.getLoadType().getCode()))
                        {
                            FefuLoadType fefuLoadType = partLoad.getLoadType();
                            EppALoadType eppALoadType = eppLoadTypeMap.get(loadTypesMap.get(loadTypeVO.getCode()));
                            if(null != _eppPartLoadMap.get(part.getNumber()) && !_eppPartLoadMap.get(part.getNumber()).isEmpty()
                                    && null != _eppPartLoadMap.get(part.getNumber()).get(eppALoadType.getFullCode()))
                            {
                                EppWorkPlanRowPartLoad eppPartLoad = _eppPartLoadMap.get(part.getNumber()).get(eppALoadType.getFullCode());
                                if(eppPartLoad.getLoad() < partLoad.getLoad())
                                    _uiSupport.error("Нагрузка «" + fefuLoadType.getTitle() + "» не может быть больше, чем нагрузка «"+ eppALoadType.getTitle() +"»" , getInputId(part.getNumber(), fefuLoadType.getCode()), getInputId(part.getNumber(), eppALoadType.getFullCode()));

                            }
                            else
                            {
                                _uiSupport.error("Нагрузка «" + fefuLoadType.getTitle() + "» не может быть больше, чем нагрузка «"+ eppALoadType.getTitle() +"»" , getInputId(part.getNumber(), fefuLoadType.getCode()), getInputId(part.getNumber(), eppALoadType.getFullCode()));
                            }
                        }
                    }
                }
            }
        }

        if(getUserContext().getErrorCollector().hasErrors()) return;
        FefuEduWorkPlanManager.instance().dao().updateLoadData(createOrUpdatePartLoads, deletePartLoads);
        deactivate();
    }

}
