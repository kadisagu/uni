package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.gen.FefuEduPlanVersionCheckResultsGen;

import java.util.Date;

/**
 * Результаты проверки версии УП в рамках блока (ДВФУ)
 */
public class FefuEduPlanVersionCheckResults extends FefuEduPlanVersionCheckResultsGen
{
    public FefuEduPlanVersionCheckResults()
    {
    }

    public FefuEduPlanVersionCheckResults(EppEduPlanVersionBlock block, Date checkTime)
    {
        setBlock(block);
        setCheckTime(checkTime);
    }
}