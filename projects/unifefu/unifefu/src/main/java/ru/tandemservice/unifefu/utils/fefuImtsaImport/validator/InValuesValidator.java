/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.validator;

import java.util.Arrays;
import java.util.List;

/**
 * Валидатор объектов: проверяет, что значение есть среди допустимых.
 * @author Alexander Zhebko
 * @since 18.07.2013
 */
public class InValuesValidator extends Validator<Object>
{
    private List<Object> values;

    public InValuesValidator(Object... values)
    {
        this.values = Arrays.asList(values);
    }

    @Override
    protected boolean validateValue(Object value)
    {
        return values.contains(value);
    }

    @Override
    public String getInvalidMessage()
    {
        return "Значения нет среди " + values.toString() + ".";
    }
}