/* $Id:$ */
package ru.tandemservice.unifefu.base.ext.DipDocument.ui.Print;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.Print.DipDocumentPrint;
import ru.tandemservice.unifefu.base.bo.FefuOrgUnit.FefuOrgUnitManager;


/**
 * @author rsizonenko
 * @since 05.11.2014
 */
@Configuration
public class DipDocumentPrintExt extends BusinessComponentExtensionManager {


    public static final String FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String EMPLOYEE_POST_DS = "employeePostDS";
    public static final String DIP_DOC_PRINT_EXT_UI = "dipDocPrintExtUi";


    @Autowired
    private DipDocumentPrint dipDocumentPrint;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(dipDocumentPrint.presenterExtPoint())
                .addAddon(uiAddon(DIP_DOC_PRINT_EXT_UI, DipDocumentPrintExtUi.class))
                .addDataSource(selectDS(EMPLOYEE_POST_DS, FefuOrgUnitManager.instance().formativeOrgUnitEmployeeHandler()).addColumn("col", EmployeePost.P_FULL_TITLE))
                .create();
    }





}
