/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportMeta;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportMeta;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.ui.List.FefuBrsAttestationResultsReportList;
import ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport.ui.List.FefuBrsDelayGradingReportList;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport.ui.List.FefuBrsPercentGradingReportList;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.ui.List.FefuBrsPpsGradingSumDataReportList;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.logic.FefuBrsReportDAO;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.logic.IFefuBrsReportDAO;
import ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.ui.List.FefuBrsStudentDisciplinesReportList;
import ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.ui.List.FefuBrsSumDataReportList;
import ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport.ui.List.FefuListTrJournalDisciplinesReportList;
import ru.tandemservice.unifefu.brs.bo.FefuRatingGroupAllDiscReport.ui.List.FefuRatingGroupAllDiscReportList;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 12/3/13
 */
@Configuration
public class FefuBrsReportManager extends BusinessObjectManager
{
    public static final String YEARPART_DS = "yearPartDS";
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public final static String TERRITORIAL_OU_DS = "territorialOrgUnitDS";
    public static final String TUTOR_ORG_UNIT_DS = "tutorOrgUnitDS";
    public static final String PPS_DS = "ppsDS";
    public static final String GROUP_DS = "groupDS";
    public static final String PARAM_FORMATIVE_OU = "formativeOrgUnit";
    public static final String PARAM_TERRITORIAL_OU = "territorialOrgUnit";
    public static final String PARAM_DEVELOP_FORM = "developForm";
    public static final String PARAM_EDU_LEVEL = "eduLevel";
    public static final String YES_NO_DS = "yesNoDS";
    public static final String ATTESTATION_DS = "attestationDS";
    public static final String GROUP_BY_DS = "groupByDS";
    public static final String CONTROL_ACTION_TYPES_DS = "controlActionTypesDS";
    public static final String RESPONSIBILITY_ORGUNIT_DS = "responsibilityOrgUnitDS";
    public static final String FCA_DS = "fcaDS";
    public static final String CUT_REPORT_DS = "cutReportDS";
    public static final String EDU_LEVEL_DS = "eduLevelDS";

    public static FefuBrsReportManager instance()
    {
        return instance(FefuBrsReportManager.class);
    }

    @Bean
    public ItemListExtPoint<IReportMeta> fefuBrsReportListExtPoint()
    {
        IReportMeta resultsReport = new ReportMeta("fefuBrsReportAttestationResultsPermissionKey", "Результаты текущей/промежуточной аттестации студентов по дисциплинам, участвующим в рейтинговой системе", FefuBrsAttestationResultsReportList.class);
        IReportMeta percentGradingReport = new ReportMeta("fefuBrsPercentGradingReportPermissionKey", "Процент проставленных оценок", FefuBrsPercentGradingReportList.class);
        IReportMeta disciplinesReport = new ReportMeta("fefuListTrJournalDisciplinesReportPermissionKey", "Перечень реализаций (рейтинг-планов) дисциплин", FefuListTrJournalDisciplinesReportList.class);
        IReportMeta delayGradingReport = new ReportMeta("fefuBrsDelayGradingReportPermissionKey", "Опоздание в простановке оценок", FefuBrsDelayGradingReportList.class);
        IReportMeta brsSumDataReport = new ReportMeta("fefuBrsSumDataReportPermissionKey", "Сводные данные балльно-рейтинговой системы", FefuBrsSumDataReportList.class);
        IReportMeta ppsGradingSumDataReport = new ReportMeta("fefuBrsPpsGradingSumDataReportPermissionKey", "Сводные данные выставления оценок преподавателями", FefuBrsPpsGradingSumDataReportList.class);
        IReportMeta studentDisciplinesReport = new ReportMeta("fefuBrsStudentDisciplinesReportPermissionKey", "Рейтинг студента по всем дисциплинам", FefuBrsStudentDisciplinesReportList.class);
        IReportMeta groupDisciplinesReport = new ReportMeta("fefuRatingGroupAllDiscPermissionKey", "Рейтинг группы по всем дисциплинам", FefuRatingGroupAllDiscReportList.class);


        return itemList(IReportMeta.class)
                .add(resultsReport.getKey(), resultsReport)
                .add(percentGradingReport.getKey(), percentGradingReport)
                .add(disciplinesReport.getKey(), disciplinesReport)
                .add(delayGradingReport.getKey(), delayGradingReport)
                .add(brsSumDataReport.getKey(), brsSumDataReport)
                .add(ppsGradingSumDataReport.getKey(), ppsGradingSumDataReport)
                .add(studentDisciplinesReport.getKey(), studentDisciplinesReport)
                .add(groupDisciplinesReport.getKey(), groupDisciplinesReport)
                .create();
    }

    @Bean
    public UIDataSourceConfig yearPartDSConfig()
    {
        return SelectDSConfig.with(YEARPART_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(yearPartDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler yearPartDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppYearPart.class)
                .order(EppYearPart.year().educationYear().intValue())
                .order(EppYearPart.part().yearDistribution().amount())
                .order(EppYearPart.part().number())
                .filter(EppYearPart.year().educationYear().title())
                .filter(EppYearPart.year().title())
                .filter(EppYearPart.part().yearDistribution().title())
                .filter(EppYearPart.part().title());
    }

    @Bean
    public UIDataSourceConfig orgUnitDSConfig()
    {
        return SelectDSConfig.with(ORG_UNIT_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(orgUnitDSHandler())
                .addColumn(OrgUnit.P_TITLE)
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                        OrgUnitToKindRelation.class,
                        OrgUnitToKindRelation.L_ORG_UNIT, property(EntityComboDataSourceHandler.ALIAS),
                        OrgUnitToKindRelation.orgUnitKind().code().s(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING
                ));
            }
        }
                .order(OrgUnit.title())
                .filter(OrgUnit.title());
    }

    @Bean
    public UIDataSourceConfig territorialOrgUnitDSConfig()
    {
        return SelectDSConfig.with(TERRITORIAL_OU_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(territorialOrgUnitDSHandler())
                .addColumn(OrgUnit.territorialTitle().s())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler territorialOrgUnitDSHandler()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {
            dql.where(eq(property(alias, OrgUnit.archival()), value(Boolean.FALSE)))
                    .where(exists(OrgUnitToKindRelation.class,
                                  OrgUnitToKindRelation.orgUnit().s(), property(alias),
                                  OrgUnitToKindRelation.orgUnitKind().code().s(), value(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL)))
                    .where(like(DQLFunctions.upper(property(alias, OrgUnit.territorialTitle())), value(CoreStringUtils.escapeLike(filter, true))));

            List<Long> formativeOuIds = context.get(PARAM_FORMATIVE_OU);
            if (formativeOuIds != null && !formativeOuIds.isEmpty())
                dql.where(exists(EducationOrgUnit.class,
                                 EducationOrgUnit.formativeOrgUnit().id().s(), formativeOuIds,
                                 EducationOrgUnit.territorialOrgUnit().s(), property(alias)));
            return dql;
        };

        return new EntityComboDataSourceHandler(getName(), OrgUnit.class).customize(customizer);
    }

    @Bean
    public UIDataSourceConfig ppsDSConfig()
    {
        return SelectDSConfig.with(PPS_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn("ФИО", PpsEntry.person().fio().s())
                .addColumn("Подразделение", PpsEntry.orgUnit().shortTitle().s())
                .addColumn("Данные преподавателя", PpsEntry.titleInfo().s())
                .handler(ppsDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler ppsDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PpsEntry.class)
                .where(PpsEntry.removalDate(), (Object) null)
                .filter(PpsEntry.person().identityCard().fullFio())
                .filter(PpsEntry.orgUnit().title())
                .order(PpsEntry.orgUnit().title())
                .order(PpsEntry.person().identityCard().fullFio())
                .pageable(true);
    }

    @Bean
    public UIDataSourceConfig groupDSConfig()
    {
        return SelectDSConfig.with(GROUP_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(groupDSHandler())
                .addColumn(Group.title().s())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler groupDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                List<Long> formativeOuIds = context.get(PARAM_FORMATIVE_OU);
                List<Long> territorialOuIds = context.get(PARAM_TERRITORIAL_OU);
                List<Long> developFormIds = context.get(PARAM_DEVELOP_FORM);
                List<Long> eduLevelIds = context.get(PARAM_EDU_LEVEL);

                if (null != formativeOuIds && !formativeOuIds.isEmpty())
                    dql.where(in(property(alias, Group.educationOrgUnit().formativeOrgUnit().id()), formativeOuIds));
                if (null != territorialOuIds && !territorialOuIds.isEmpty())
                    dql.where(in(property(alias, Group.educationOrgUnit().territorialOrgUnit().id()), territorialOuIds));
                if (null != developFormIds && !developFormIds.isEmpty())
                    dql.where(in(property(alias, Group.educationOrgUnit().developForm().id()), developFormIds));
                if (null != eduLevelIds && !eduLevelIds.isEmpty())
                    dql.where(in(property(alias, Group.educationOrgUnit().educationLevelHighSchool().id()), eduLevelIds));
                dql.where(eq(property(alias, Group.archival()), value(false)));
            }
        }
                .pageable(true)
                .order(Group.title())
                .filter(Group.title());
    }

    @Bean
    public UIDataSourceConfig controlActionTypesDSConfig()
    {
        return SelectDSConfig.with(CONTROL_ACTION_TYPES_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(controlActionTypesDSHandler())
                .addColumn(EppIControlActionType.title().s())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler controlActionTypesDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppIControlActionType.class)
                .where(EppIControlActionType.disabled(), false)
                .pageable(true)
                .order(EppIControlActionType.title())
                .filter(EppIControlActionType.title());
    }

    @Bean
    public UIDataSourceConfig yesNoDSConfig()
    {
        return SelectDSConfig.with(YES_NO_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(yesNoComboDSHandler())
                .create();
    }

    @Bean
    public UIDataSourceConfig responsibilityOrgUnitDSConfig()
    {
        return SelectDSConfig.with(RESPONSIBILITY_ORGUNIT_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(responsibilityOrgUnitDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler responsibilityOrgUnitDSHandler()
    {
        final EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(EppTutorOrgUnit.class, EppTutorOrgUnit.L_ORG_UNIT, (Object) property(EntityComboDataSourceHandler.ALIAS)));
            }
        };
        handler.order(OrgUnit.title());
        handler.filter(OrgUnit.title());
        return handler;
    }

    public static final Long YES_ID = 0L;
    public static final Long NO_ID = 1L;

    @Bean
    public ItemListExtPoint<DataWrapper> yesNoItemListExtPoint()
    {
        return itemList(DataWrapper.class).
                add(YES_ID.toString(), new DataWrapper(YES_ID, "Да")).
                add(NO_ID.toString(), new DataWrapper(NO_ID, "Нет")).
                create();
    }

    @Bean
    public IDefaultComboDataSourceHandler yesNoComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).addItemList(yesNoItemListExtPoint());
    }

    @Bean
    public UIDataSourceConfig tutorOUDSConfig()
    {
        return SelectDSConfig.with(TUTOR_ORG_UNIT_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(tutorOrgUnitDSHandler())
                .addColumn(OrgUnit.P_TITLE)
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler tutorOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(EppTutorOrgUnit.class, EppTutorOrgUnit.L_ORG_UNIT, (Object) property(alias)));
            }
        }
                .order(OrgUnit.title())
                .where(OrgUnit.archival(), false)
                .filter(OrgUnit.title());
    }


    @Bean
    public UIDataSourceConfig attestationDSConfig()
    {
        return SelectDSConfig.with(ATTESTATION_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(attestationDSHandler())
                .create();
    }

    public static final Long CURRENT_ATTESTATION_ID = 0L;
    public static final Long SUMMARY_ATTESTATION_ID = 1L;

    @Bean
    public IDefaultComboDataSourceHandler attestationDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addItemList(attestationItemListExtPoint());
    }

    @Bean
    public ItemListExtPoint<DataWrapper> attestationItemListExtPoint()
    {
        return itemList(DataWrapper.class).
                add(CURRENT_ATTESTATION_ID.toString(), new DataWrapper(CURRENT_ATTESTATION_ID, "Текущая")).
                add(SUMMARY_ATTESTATION_ID.toString(), new DataWrapper(SUMMARY_ATTESTATION_ID, "Промежуточная")).
                create();
    }


    @Bean
    public UIDataSourceConfig groupByDSConfig()
    {
        return SelectDSConfig.with(GROUP_BY_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(groupByDSHandler())
                .create();
    }

    public static final Long GROUP_BY_GROUP_ID = 0L;
    public static final Long GROUP_BY_EDU_PROGRAMM_ID = 1L;

    @Bean
    public IDefaultComboDataSourceHandler groupByDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addItemList(groupByItemListExtPoint());
    }

    @Bean
    public ItemListExtPoint<DataWrapper> groupByItemListExtPoint()
    {
        return itemList(DataWrapper.class).
                add(GROUP_BY_GROUP_ID.toString(), new DataWrapper(GROUP_BY_GROUP_ID, "По группам")).
                add(GROUP_BY_EDU_PROGRAMM_ID.toString(), new DataWrapper(GROUP_BY_EDU_PROGRAMM_ID, "По образовательным программам")).
                create();
    }

    @Bean
    public UIDataSourceConfig fcaDSConfig()
    {
        return SelectDSConfig.with(FCA_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(fcaDSHandler())
                .create();
    }

    public static final Long FCA_EXAM_ID = 0L;
    public static final Long FCA_EXAM_WITH_NULL_WEIGHT_ID = 1L;
    public static final Long FCA_OFFSET_ID = 2L;

    @Bean
    public IDefaultComboDataSourceHandler fcaDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addItemList(fcaItemListExtPoint());
    }

    @Bean
    public ItemListExtPoint<DataWrapper> fcaItemListExtPoint()
    {
        return itemList(DataWrapper.class).
                add(FCA_EXAM_ID.toString(), new DataWrapper(FCA_EXAM_ID, "Экзамен")).
                add(FCA_EXAM_WITH_NULL_WEIGHT_ID.toString(), new DataWrapper(FCA_EXAM_WITH_NULL_WEIGHT_ID, "Экзамен с нулевым весом")).
                add(FCA_OFFSET_ID.toString(), new DataWrapper(FCA_OFFSET_ID, "Зачет")).
                create();
    }

    @Bean
    public UIDataSourceConfig cutReportDSConfig()
    {
        return SelectDSConfig.with(CUT_REPORT_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(cutReportDSHandler())
                .create();
    }

    public static final Long CUT_REPORT_CURRENT_ID = 0L;
    public static final Long CUT_REPORT_STRUCT_ID = 1L;


    @Bean
    public ItemListExtPoint<DataWrapper> cutReportItemListExtPoint()
    {
        return itemList(DataWrapper.class).
                add(CUT_REPORT_CURRENT_ID.toString(), new DataWrapper(CUT_REPORT_CURRENT_ID, "По состоянию на дату")).
                add(CUT_REPORT_STRUCT_ID.toString(), new DataWrapper(CUT_REPORT_STRUCT_ID, "Все")).
                create();
    }

    @Bean
    public IDefaultComboDataSourceHandler cutReportDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).addItemList(cutReportItemListExtPoint());
    }

    @Bean
    public IFefuBrsReportDAO dao()
    {
        return new FefuBrsReportDAO();
    }


    @Bean
    public UIDataSourceConfig eduLvlDSConfig()
    {
        return SelectDSConfig.with(EDU_LEVEL_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(eduLvlDSHandler())
                .addColumn(EducationLevelsHighSchool.displayableTitle().s())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduLvlDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationLevelsHighSchool.class)
                .pageable(true)
                .order(EducationLevelsHighSchool.title())
                .filter(EducationLevelsHighSchool.title());
    }
}
