/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu12.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 13.05.2013
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuEduTransferEnrolmentStuExtract, Model>
{
}