/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSchedule.ui.PrintFormAdd;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.FefuScheduleManager;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.logic.FefuSchedulesComboDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.logic.GroupComboDSHandler;
import ru.tandemservice.unifefu.base.vo.FefuSchedulePrintDataVO;
import ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;

/**
 * @author nvankov
 * @since 9/25/13
 */
@Input({
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class FefuSchedulePrintFormAddUI extends UIPresenter
{
    private Long _orgUnitId;
    private Boolean allPosts = Boolean.FALSE;

    private FefuSchedulePrintDataVO _printData;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public FefuSchedulePrintDataVO getPrintData()
    {
        return _printData;
    }

    public void setPrintData(FefuSchedulePrintDataVO printData)
    {
        _printData = printData;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _printData)
        {
            _printData = new FefuSchedulePrintDataVO();
            OrgUnit currOrgUnit = DataAccessServices.dao().<OrgUnit>get(_orgUnitId);
            _printData.setCurrentOrgUnit(currOrgUnit);
            _printData.setFormativeOrgUnit(currOrgUnit);
            _printData.setEducationYear(DataAccessServices.dao().get(EducationYear.class, EducationYear.current(), true));
            IPrincipalContext context = getUserContext().getPrincipalContext();
            if (context instanceof EmployeePost)
            {
                EmployeePost currentEmpl = (EmployeePost) context;
                _printData.setAdmin(currentEmpl);
                onChangeAdmin();
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("scheduleData", _printData);
        if (FefuSchedulePrintFormAdd.TERRITORIAL_OU_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.TERRITORIAL_ORG_UNIT);
        }
        if (FefuSchedulePrintFormAdd.FORMATIVE_OU_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.FORMATIVE_ORG_UNIT);
        }
        if (FefuSchedulePrintFormAdd.EDU_LEVEL_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.EDUCATION_LEVEL);
        }
        if (FefuSchedulePrintFormAdd.DEVELOP_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.DEVELOP_FORM);
        }
        if (FefuSchedulePrintFormAdd.COURSE_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.COURSE);
        }
        if (FefuSchedulePrintFormAdd.SEASON_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", FefuSchedulesComboDSHandler.Columns.SEASON);
        }
        if (FefuSchedulePrintFormAdd.BELLS_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", FefuSchedulesComboDSHandler.Columns.BELLS);
        }
        if (FefuSchedulePrintFormAdd.SCHEDULES_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", FefuSchedulesComboDSHandler.Columns.SCHEDULE);
        }
        if (FefuSchedulePrintFormAdd.EMPLOYEE_POST_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleManager.ALL_POSTS_PROP, getAllPosts());
        }
    }

    public void onChangeSeason()
    {
        if (null != _printData.getSeason() && null != _printData.getEduStartDate())
            _printData.setEduStartDate(_printData.getSeason().getStartDate());
    }

    public void onChangeFormOU()
    {
        if (null != _printData.getFormativeOrgUnit())
            _printData.setChief((EmployeePost) _printData.getFormativeOrgUnit().getHead());
    }

    public void onChangeAdmin()
    {
        if (_printData.getAdmin() != null)
        {
            if (!StringUtils.isEmpty(_printData.getAdmin().getPhone()))
                _printData.setAdminPhoneNum(_printData.getAdmin().getPhone());
            else if (!StringUtils.isEmpty(_printData.getAdmin().getPerson().getContactData().getPhoneWork()))
                _printData.setAdminPhoneNum(_printData.getAdmin().getPerson().getContactData().getPhoneWork());
        }
    }

    public void onClickApply()
    {
        if (_printData.getEduStartDate().before(_printData.getSeason().getStartDate()) || _printData.getEduStartDate().after(_printData.getSeason().getEndDate()))
            _uiSupport.error("Дата начала занятий не может быть раньше даты начала периода расписания и позже дата окончания периода расписания", "eduStartDate");
        if (getUserContext().getErrorCollector().hasErrors())
            return;
        SchedulePrintFormFefuExt printForm = FefuScheduleManager.instance().dao().savePrintForm(_printData);
        byte[] content = printForm.getSppSchedulePrintForm().getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("xls").fileName("schedule.xls").document(content), true);
        deactivate();
    }

    public Boolean getAllPosts()
    {
        return allPosts;
    }

    public void setAllPosts(Boolean allPosts)
    {
        this.allPosts = allPosts;
    }
}
