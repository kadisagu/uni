package ru.tandemservice.unifefu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тематические группы типов приказов по движению студентов"
 * Имя сущности : fefuThematicGroupStuExtractTypes
 * Файл data.xml : unifefu-catalog.data.xml
 */
public interface FefuThematicGroupStuExtractTypesCodes
{
    /** Константа кода (code) элемента : mainEduProgramm (code). Название (title) : Приказы по основной образовательной программе */
    String MAIN_EDU_PROGRAMM = "mainEduProgramm";
    /** Константа кода (code) элемента : practice (code). Название (title) : Приказы о прохождении практики */
    String PRACTICE = "practice";
    /** Константа кода (code) элемента : partComServIncentPenalt (code). Название (title) : Приказы об участии в общественной работе, поощрениях и взысканиях */
    String PART_COM_SERV_INCENT_PENALT = "partComServIncentPenalt";
    /** Константа кода (code) элемента : orphanPayments (code). Название (title) : Приказы по выплатам сиротам */
    String ORPHAN_PAYMENTS = "orphanPayments";

    Set<String> CODES = ImmutableSet.of(MAIN_EDU_PROGRAMM, PRACTICE, PART_COM_SERV_INCENT_PENALT, ORPHAN_PAYMENTS);
}
