/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu8.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuSocGrantStopStuExtract, Model>
{
}