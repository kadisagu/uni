/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.ui.RowsExtTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.ui.WorkPlanDataSourceGenerator;
import ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.FefuEduWorkPlanManager;
import ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.logic.FefuWorkPlanRowExtDataSourceGenerator;
import ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.ui.RowsExtEdit.FefuEduWorkPlanRowsExtEdit;

/**
 * @author nvankov
 * @since 1/21/14
 */
@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id") })
public class FefuEduWorkPlanRowsExtTabUI extends UIPresenter
{
    private EntityHolder<EppWorkPlanBase> _holder = new EntityHolder<>();
    private StaticListDataSource<IEppWorkPlanRowWrapper> _dataSource = new StaticListDataSource<>();

    public EntityHolder<EppWorkPlanBase> getHolder()
    {
        return _holder;
    }

    public void setHolder(EntityHolder<EppWorkPlanBase> holder)
    {
        _holder = holder;
    }

    public StaticListDataSource<IEppWorkPlanRowWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(StaticListDataSource<IEppWorkPlanRowWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    @Override
    public void onComponentRefresh()
    {
        _holder.refresh(EppWorkPlanBase.class);

        final FefuWorkPlanRowExtDataSourceGenerator generator = new FefuWorkPlanRowExtDataSourceGenerator() {
            @Override protected String getPermissionKeyEdit() {
                return "editRowsExtTab_fefuEppWorkPlan";
            }
            @Override protected Long getWorkPlanBaseId() {
                return _holder.getId();
            }
            @Override protected boolean isEditable() {
                return !getHolder().getValue().getState().isReadOnlyState();
            }
        };
        generator.fillDisciplineDataSource(_dataSource);
    }

    public void onClickEditTotalPartLoad()
    {
        _uiActivation.asRegionDialog(FefuEduWorkPlanRowsExtEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }
}
