/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu25.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;
import ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuConditionalTransferCourseStuExtract> implements IExtEducationLevelModel, IGroupModel
{
    private CommonExtractModel _eduModel;

    public CommonExtractModel getEduModel()
    {
        return _eduModel;
    }

    public void setEduModel(CommonExtractModel eduModel)
    {
        _eduModel = eduModel;
    }

    @Override
    public EducationLevels getParentEduLevel()
    {
        return EducationOrgUnitUtil.getParentLevel(getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
    }

    @Override
    public Course getCourse()
    {
        return getEduModel().getCourse();
    }
}
