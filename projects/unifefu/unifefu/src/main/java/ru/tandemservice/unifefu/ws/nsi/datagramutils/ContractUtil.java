/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import ru.tandemservice.unifefu.entity.FefuStudentContract;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.NsiDatagramUtil;
import ru.tandemservice.unifefu.ws.nsi.datagram.AgreementType;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 15.05.2014
 */
public class ContractUtil extends SimpleNsiCatalogUtil<AgreementType, FefuStudentContract>
{
    public static final String AGREEMENT_ID_NSI_FIELD = "AgreementID";
    public static final String AGREEMENT_NAME_NSI_FIELD = "AgreementName";
    public static final String AGREEMENT_NUMBER_NSI_FIELD = "AgreementNumber";
    public static final String AGREEMENT_DATE_NSI_FIELD = "AgreementDate";
    public static final String AGREEMENT_RESPONSIBILITIES_NSI_FIELD = "AgreementResponsibilities";
    public static final String AGREEMENT_SHORT_CONTENT_NSI_FIELD = "AgreementShortContent";
    public static final String AGREEMENT_INFO_NSI_FIELD = "AgreementInfo";
    public static final String AGREEMENT_DATE_BEGIN_NSI_FIELD = "AgreementDateBegin";
    public static final String AGREEMENT_DATE_END_NSI_FIELD = "AgreementDateEnd";
    public static final String AGREEMENT_BALANCE_NSI_FIELD = "AgreementBalance";

    @Override
    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            super.getNsiFields();
            NSI_FIELDS.add(AGREEMENT_ID_NSI_FIELD);
            NSI_FIELDS.add(AGREEMENT_NAME_NSI_FIELD);
            NSI_FIELDS.add(AGREEMENT_NUMBER_NSI_FIELD);
            NSI_FIELDS.add(AGREEMENT_DATE_NSI_FIELD);
            NSI_FIELDS.add(AGREEMENT_RESPONSIBILITIES_NSI_FIELD);
            NSI_FIELDS.add(AGREEMENT_SHORT_CONTENT_NSI_FIELD);
            NSI_FIELDS.add(AGREEMENT_INFO_NSI_FIELD);
            NSI_FIELDS.add(AGREEMENT_DATE_BEGIN_NSI_FIELD);
            NSI_FIELDS.add(AGREEMENT_DATE_END_NSI_FIELD);
            NSI_FIELDS.add(AGREEMENT_BALANCE_NSI_FIELD);
        }
        return NSI_FIELDS;
    }

    @Override
    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            super.getEntityFields();
            ENTITY_FIELDS.add(FefuStudentContract.contractID().s());
            ENTITY_FIELDS.add(FefuStudentContract.contractName().s());
            ENTITY_FIELDS.add(FefuStudentContract.contractNumber().s());
            ENTITY_FIELDS.add(FefuStudentContract.contractDate().s());
            ENTITY_FIELDS.add(FefuStudentContract.responsibilities().s());
            ENTITY_FIELDS.add(FefuStudentContract.shortContent().s());
            ENTITY_FIELDS.add(FefuStudentContract.additionalInfo().s());
            ENTITY_FIELDS.add(FefuStudentContract.dateBegin().s());
            ENTITY_FIELDS.add(FefuStudentContract.dateEnd().s());
            ENTITY_FIELDS.add(FefuStudentContract.balance().s());
        }
        return ENTITY_FIELDS;
    }

    @Override
    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            super.getNsiToObFieldsMap();
            NSI_TO_OB_FIELDS_MAP.put(AGREEMENT_ID_NSI_FIELD, FefuStudentContract.contractID().s());
            NSI_TO_OB_FIELDS_MAP.put(AGREEMENT_NAME_NSI_FIELD, FefuStudentContract.contractName().s());
            NSI_TO_OB_FIELDS_MAP.put(AGREEMENT_NUMBER_NSI_FIELD, FefuStudentContract.contractNumber().s());
            NSI_TO_OB_FIELDS_MAP.put(AGREEMENT_DATE_NSI_FIELD, FefuStudentContract.contractDate().s());
            NSI_TO_OB_FIELDS_MAP.put(AGREEMENT_RESPONSIBILITIES_NSI_FIELD, FefuStudentContract.responsibilities().s());
            NSI_TO_OB_FIELDS_MAP.put(AGREEMENT_SHORT_CONTENT_NSI_FIELD, FefuStudentContract.shortContent().s());
            NSI_TO_OB_FIELDS_MAP.put(AGREEMENT_INFO_NSI_FIELD, FefuStudentContract.additionalInfo().s());
            NSI_TO_OB_FIELDS_MAP.put(AGREEMENT_DATE_BEGIN_NSI_FIELD, FefuStudentContract.dateBegin().s());
            NSI_TO_OB_FIELDS_MAP.put(AGREEMENT_DATE_END_NSI_FIELD, FefuStudentContract.dateEnd().s());
            NSI_TO_OB_FIELDS_MAP.put(AGREEMENT_BALANCE_NSI_FIELD, FefuStudentContract.balance().s());
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    @Override
    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            super.getObToNsiFieldsMap();
            OB_TO_NSI_FIELDS_MAP.put(FefuStudentContract.contractID().s(), AGREEMENT_ID_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuStudentContract.contractName().s(), AGREEMENT_NAME_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuStudentContract.contractNumber().s(), AGREEMENT_NUMBER_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuStudentContract.contractDate().s(), AGREEMENT_DATE_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuStudentContract.responsibilities().s(), AGREEMENT_RESPONSIBILITIES_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuStudentContract.shortContent().s(), AGREEMENT_SHORT_CONTENT_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuStudentContract.additionalInfo().s(), AGREEMENT_INFO_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuStudentContract.dateBegin().s(), AGREEMENT_DATE_BEGIN_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuStudentContract.dateEnd().s(), AGREEMENT_DATE_END_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuStudentContract.balance().s(), AGREEMENT_BALANCE_NSI_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    public long getNsiFefuStudentContractIdHash(AgreementType nsiEntity)
    {
        if (null == nsiEntity.getAgreementID()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getAgreementID());
    }

    public long getNsiFefuStudentContractNameHash(AgreementType nsiEntity)
    {
        if (null == nsiEntity.getAgreementName()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getAgreementName());
    }

    public long getNsiFefuStudentContractNumberHash(AgreementType nsiEntity)
    {
        if (null == nsiEntity.getAgreementNumber()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getAgreementNumber());
    }

    public long getNsiFefuStudentContractDateHash(AgreementType nsiEntity)
    {
        if (null == nsiEntity.getAgreementDate()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getAgreementDate());
    }

    public long getNsiFefuStudentContractResponsibilitiesHash(AgreementType nsiEntity)
    {
        if (null == nsiEntity.getAgreementResponsibilities()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getAgreementResponsibilities());
    }

    public long getNsiFefuStudentContractShortContentHash(AgreementType nsiEntity)
    {
        if (null == nsiEntity.getAgreementShortContent()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getAgreementShortContent());
    }

    public long getNsiFefuStudentContractInfoHash(AgreementType nsiEntity)
    {
        if (null == nsiEntity.getAgreementInfo()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getAgreementInfo());
    }

    public long getNsiFefuStudentContractDateBeginHash(AgreementType nsiEntity)
    {
        if (null == nsiEntity.getAgreementDateBegin()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getAgreementDateBegin());
    }

    public long getNsiFefuStudentContractDateEndHash(AgreementType nsiEntity)
    {
        if (null == nsiEntity.getAgreementDateEnd()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getAgreementDateEnd());
    }

    public long getNsiFefuStudentContractBalanceHash(AgreementType nsiEntity)
    {
        if (null == nsiEntity.getAgreementBalance()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getAgreementBalance());
    }

    @Override
    public boolean isFieldEmpty(AgreementType nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case AGREEMENT_ID_NSI_FIELD:
                return null == nsiEntity.getAgreementID();
            case AGREEMENT_NAME_NSI_FIELD:
                return null == nsiEntity.getAgreementName();
            case AGREEMENT_NUMBER_NSI_FIELD:
                return null == nsiEntity.getAgreementNumber();
            case AGREEMENT_DATE_NSI_FIELD:
                return null == nsiEntity.getAgreementDate();
            case AGREEMENT_RESPONSIBILITIES_NSI_FIELD:
                return null == nsiEntity.getAgreementResponsibilities();
            case AGREEMENT_SHORT_CONTENT_NSI_FIELD:
                return null == nsiEntity.getAgreementShortContent();
            case AGREEMENT_INFO_NSI_FIELD:
                return null == nsiEntity.getAgreementInfo();
            case AGREEMENT_DATE_BEGIN_NSI_FIELD:
                return null == nsiEntity.getAgreementDateBegin();
            case AGREEMENT_DATE_END_NSI_FIELD:
                return null == nsiEntity.getAgreementDateEnd();
            case AGREEMENT_BALANCE_NSI_FIELD:
                return null == nsiEntity.getAgreementBalance();
            default:
                return super.isFieldEmpty(nsiEntity, fieldName);
        }
    }

    @Override
    public long getNsiFieldHash(AgreementType nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case AGREEMENT_ID_NSI_FIELD:
                return getNsiFefuStudentContractIdHash(nsiEntity);
            case AGREEMENT_NAME_NSI_FIELD:
                return getNsiFefuStudentContractNameHash(nsiEntity);
            case AGREEMENT_NUMBER_NSI_FIELD:
                return getNsiFefuStudentContractNumberHash(nsiEntity);
            case AGREEMENT_DATE_NSI_FIELD:
                return getNsiFefuStudentContractDateHash(nsiEntity);
            case AGREEMENT_RESPONSIBILITIES_NSI_FIELD:
                return getNsiFefuStudentContractResponsibilitiesHash(nsiEntity);
            case AGREEMENT_SHORT_CONTENT_NSI_FIELD:
                return getNsiFefuStudentContractShortContentHash(nsiEntity);
            case AGREEMENT_INFO_NSI_FIELD:
                return getNsiFefuStudentContractInfoHash(nsiEntity);
            case AGREEMENT_DATE_BEGIN_NSI_FIELD:
                return getNsiFefuStudentContractDateBeginHash(nsiEntity);
            case AGREEMENT_DATE_END_NSI_FIELD:
                return getNsiFefuStudentContractDateEndHash(nsiEntity);
            case AGREEMENT_BALANCE_NSI_FIELD:
                return getNsiFefuStudentContractBalanceHash(nsiEntity);
            default:
                return super.getNsiFieldHash(nsiEntity, fieldName, caseInsensitive);
        }
    }

    public long getFefuStudentContractNameFullHash(FefuStudentContract entity)
    {
        if (null == entity.getContractName()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getContractName());
    }

    public long getFefuStudentContractIdHash(FefuStudentContract entity)
    {
        if (null == entity.getContractID()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getContractID());
    }

    public long getFefuStudentContractNumberHash(FefuStudentContract entity)
    {
        if (null == entity.getContractNumber()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getContractNumber());
    }

    public long getFefuStudentContractDateHash(FefuStudentContract entity)
    {
        if (null == entity.getContractDate()) return 0;
        return MD5HashBuilder.getCheckSum(NsiDatagramUtil.formatDate(entity.getContractDate()));
    }

    public long getFefuStudentContractResponsibilitiesHash(FefuStudentContract entity)
    {
        if (null == entity.getResponsibilities()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getResponsibilities());
    }

    public long getFefuStudentContractShortContentHash(FefuStudentContract entity)
    {
        if (null == entity.getShortContent()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getShortContent());
    }

    public long getFefuStudentContractInfoHash(FefuStudentContract entity)
    {
        if (null == entity.getAdditionalInfo()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getAdditionalInfo());
    }

    public long getFefuStudentContractDateBeginHash(FefuStudentContract entity)
    {
        if (null == entity.getDateBegin()) return 0;
        return MD5HashBuilder.getCheckSum(NsiDatagramUtil.formatDate(entity.getDateBegin()));
    }

    public long getFefuStudentContractDateEndHash(FefuStudentContract entity)
    {
        if (null == entity.getDateEnd()) return 0;
        return MD5HashBuilder.getCheckSum(NsiDatagramUtil.formatDate(entity.getDateEnd()));
    }

    public long getFefuStudentContractBalanceHash(FefuStudentContract entity)
    {
        if (null == entity.getBalance()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getBalance());
    }

    @Override
    public long getEntityFieldHash(FefuStudentContract entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case ICatalogItem.CATALOG_ITEM_TITLE:
                return getFefuStudentContractNameFullHash(entity);
            case FefuStudentContract.P_CONTRACT_I_D:
                return getFefuStudentContractIdHash(entity);
            case FefuStudentContract.P_CONTRACT_NAME:
                return getFefuStudentContractNameFullHash(entity);
            case FefuStudentContract.P_CONTRACT_NUMBER:
                return getFefuStudentContractNumberHash(entity);
            case FefuStudentContract.P_CONTRACT_DATE:
                return getFefuStudentContractDateHash(entity);
            case FefuStudentContract.P_RESPONSIBILITIES:
                return getFefuStudentContractResponsibilitiesHash(entity);
            case FefuStudentContract.P_SHORT_CONTENT:
                return getFefuStudentContractShortContentHash(entity);
            case FefuStudentContract.P_ADDITIONAL_INFO:
                return getFefuStudentContractInfoHash(entity);
            case FefuStudentContract.P_DATE_BEGIN:
                return getFefuStudentContractDateBeginHash(entity);
            case FefuStudentContract.P_DATE_END:
                return getFefuStudentContractDateEndHash(entity);
            case FefuStudentContract.P_BALANCE:
                return getFefuStudentContractBalanceHash(entity);
            default:
                return super.getEntityFieldHash(entity, nsiId, fieldName, caseInsensitive);
        }
    }
}