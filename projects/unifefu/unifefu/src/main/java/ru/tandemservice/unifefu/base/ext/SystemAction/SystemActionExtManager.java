/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.unifefu.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("uniio_importStudentFefuExtension", new SystemActionDefinition("unifefu", "importStudentFefuExtension", "onClickImportFefuExtendedStudents", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniio_importOrdersFromLotus", new SystemActionDefinition("unifefu", "importOrdersFromLotus", "onClickImportFefuOrdersFromLotus", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniio_testIdmPersonRegistrationService", new SystemActionDefinition("unifefu", "testIdmPersonRegistrationService", "onClickTestIdmPersonRegistrationService", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniio_test1C", new SystemActionDefinition("unifefu", "test1C", "onClickTest1C", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniio_exportStudentOrdersToMDB", new SystemActionDefinition("unifefu", "exportStudentOrdersToMDB", "onClickExportStudentOrdersToMDB", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniio_exportStudentOrdersToTable", new SystemActionDefinition("unifefu", "exportStudentOrdersToTable", "onClickExportStudentOrdersToTable", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unisc_makeArchivalEmptyGroupDuplicates", new SystemActionDefinition("unifefu", "makeArchivalEmptyGroupDuplicates", "onClickMakeArchivalEmptyGroupDuplicates", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unisc_makeArchivalEmptyGroups", new SystemActionDefinition("unifefu", "makeArchivalEmptyGroups", "onClickMakeArchivalEmptyGroups", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unisc_makeArchivalGroupsWithNotActiveStudents", new SystemActionDefinition("unifefu", "makeArchivalGroupsWithNotActiveStudents", "onClickMakeArchivalGroupsWithNotActiveStudents", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unisc_assignForeignStudentsDeclinationSettings", new SystemActionDefinition("unifefu", "assignForeignStudentsDeclinationSettings", "onClickAssignForeignStudentsDeclinationSettings", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unisc_changeEntranceYearForArchiveStudents", new SystemActionDefinition("unifefu", "changeEntranceYearForArchiveStudents", "onClickChangeEntranceYearForArchiveStudents", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unisc_fillEduLevelQualifications", new SystemActionDefinition("unifefu", "fillEduLevelQualifications", "onClickFillEduLevelQualifications", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unisc_getPersonsPersonalData", new SystemActionDefinition("unifefu", "getPersonsPersonalData", "onClickGetPersonsPersonalData", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unisc_getPersonsPersonalDataLibrary", new SystemActionDefinition("unifefu", "getPersonsPersonalDataLibrary", "onClickGetPersonsPersonalDataLibrary", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unisc_sendStudentsToArchive", new SystemActionDefinition("unifefu", "sendStudentsToArchive", "onClickSendStudentsToArchive", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_contractsImport", new SystemActionDefinition("unifefu", "importContracts", "onClickImportContracts", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_fixCompetitionGroups", new SystemActionDefinition("uniec", "fixCompetitionGroups", "onClickFixCompetitionGroups", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_moveOriginals", new SystemActionDefinition("uniec", "moveOriginals", "onClickMoveOriginals", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_actualizePersonGuids", new SystemActionDefinition("unifefu", "actualizePersonGuids", "onClickActualizePersonGuids", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_sendEntrantPortalNotifications", new SystemActionDefinition("unifefu", "sendEntrantPortalNotifications", "onClickSendEntrantPortalNotifications", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_idmRequestsProcessingList", new SystemActionDefinition("unifefu", "processIdmRequests", "onClickProcessIdmRequests", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniepp_loadDataFromImtsa", new SystemActionDefinition("uniepp", "loadDataFromImtsa", "onClickLoadDataFromImtsa", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_onlineEntrantSendNotifications", new SystemActionDefinition("uniec", "onlineEntrantSendNotifications", "onClickOnlineEntrantSendNotifications", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_studentsExport", new SystemActionDefinition("unifefu", "fefuStudentsExport", "onClickFefuStudentsExport", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_exportStudentsToTable", new SystemActionDefinition("unifefu", "exportStudentsToTable", "onClickExportStudentsToTable", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_syncStudentsWithNSI", new SystemActionDefinition("unifefu", "syncStudentsWithNSI", "onClickSyncStudentsWithNSI", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_exportEduInstitution", new SystemActionDefinition("unifefu", "exportEduInstitution", "onExportEduInstitution", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_importEduInstitution", new SystemActionDefinition("unifefu", "importEduInstitution", "onImportEduInstitution", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_exportEmployees", new SystemActionDefinition("unifefu", "exportEmployees", "onExportEmployees", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_importEmployees", new SystemActionDefinition("unifefu", "importEmployees", "onImportEmployees", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_archivePracticeContract", new SystemActionDefinition("unifefu", "archivePracticeContract", "onArchivePracticeContract", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_reImportImtsa", new SystemActionDefinition("unifefu", "reImportImtsa", "onClickReImportImtsa", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_deleteAllFefuScheduleICalData", new SystemActionDefinition("unifefu", "deleteAllFefuScheduleICalData", "onClickDeleteAllFefuScheduleICalData", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_removeOtherOrderDuplicates", new SystemActionDefinition("unifefu", "removeOtherOrderDuplicates", "onClickRemoveOtherOrderDuplicates", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_sendBlaBlaBlaRatingToPortal", new SystemActionDefinition("unifefu", "sendBlaBlaBlaRatingToPortal", "onClickSendBlaBlaBlaRatingToPortal", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_importMdbView", new SystemActionDefinition("unifefu", "importMdbView", "onClickImportMdbView", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_resetPersonSyncDateForFailedStudents", new SystemActionDefinition("unifefu", "resetPersonSyncDateForFailedStudents", "onClickResetPersonSyncDateForFailedStudents", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_mergePersonsAndUpdateTheirGuids", new SystemActionDefinition("unifefu", "mergePersonsAndUpdateTheirGuids", "onClickMergePersonsAndUpdateTheirGuids", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_regenBadIdentityCardIds", new SystemActionDefinition("unifefu", "regenBadIdentityCardIds", "onClickRegenBadIdentityCardIds", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_regenExtractPrintFormsDEV-5552", new SystemActionDefinition("unifefu", "regenExtractPrintFormsDEV-5552", "onClickRegenExtractPrintFormsDEV5552", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_regenExtractPrintFormsDEV-5570", new SystemActionDefinition("unifefu", "regenExtractPrintFormsDEV-5570", "onClickRegenExtractPrintFormsDEV5570", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_transferDataAboutTargetReception", new SystemActionDefinition("unifefu", "transferDataAboutTargetReception", "onClickTransferDataAboutTargetReception", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME ))
                .add("unifefu_archiveEduPlanVersions", new SystemActionDefinition("unifefu", "archiveEduPlanVersions", "onClickArchiveEduPlanVersions", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME ))
                .add("unifefu_sendNotAcceptRegElementToArchive", new SystemActionDefinition("unifefu", "sendNotAcceptRegElementToArchive", "onClickSendNotAcceptRegElementToArchive", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_lockAddressUpdateDaemon", new SystemActionDefinition("unifefu", "lockAddressUpdateDaemon", "onClickLockAddressUpdateDaemon", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_deleteNotCoordinatedOrders", new SystemActionDefinition("movestudent", "deleteNotCoordinatedOrders", "onClickDeleteNotCoordinatedOrders", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unifefu_importImtsaCompetence", new SystemActionDefinition("unifefu", "importImtsaCompetence", "onClickImportImtsaCompetence", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                //.add("unifefu_initGuids", new SystemActionDefinition("unifefu", "initGuids", "onClickInitGuids", SystemActionPubExt.FEFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}
