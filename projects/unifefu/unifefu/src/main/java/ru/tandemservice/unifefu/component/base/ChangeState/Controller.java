package ru.tandemservice.unifefu.component.base.ChangeState;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.RejectCommentDialog.FefuEduPlanRejectCommentDialog;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ilunin
 * @since 16.10.2014
 */
public class Controller extends ru.tandemservice.uniepp.component.base.ChangeState.Controller {

    public void onClickChangeSateToDeclined(final IBusinessComponent component)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("eduPlanVersionId", this.getModel(component).getId());
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(FefuEduPlanRejectCommentDialog.class.getSimpleName(), params));
    }

    @Override
    public void bindSetReturned(IBusinessComponent component, String childRegionName, Map<String, Object> returnedData)
    {
        super.bindSetReturned(component, childRegionName, returnedData);
        Object answer = returnedData.get(FefuEduPlanRejectCommentDialog.CHANGED_STATE);

        if (answer != null && answer.equals(Boolean.TRUE) && ((Model)getModel(component)).isEduPlanVersion())
        {
            component.setListenerParameter(EppState.STATE_REJECTED);
            onClickChangeState(component);
        }
    }
}
