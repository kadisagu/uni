package ru.tandemservice.unifefu.base.ext.EcDistribution.ui.List;

import org.apache.commons.io.IOUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.List.EcDistributionList;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.List.EcDistributionListUI;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgDistributionDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.base.ext.EcDistribution.logic.IFefuEcDistributionListPrint;
import ru.tandemservice.unifefu.component.report.FefuReportUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Date: 25.07.13
 * Time: 17:06
 */
public class EcDistributionListExtUI extends UIAddon {

    public EcDistributionListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    /**
     * button
     * Печатать все распределения (форма ДВФУ)
    */
    public void onClickAllFefuPrint()
    {
        Set<Long> ids = getSelectedDistributionIds();

        getPresenter().getSupport().doRefresh();

        if (ids.isEmpty()) return;

        byte[] data = getFefuAllRtfPrint(ids);

        String fileName = new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + "-enrollmentDistributionEnvironment.zip";

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().zip().fileName(fileName).document(data), false);
    }

    /**
     *
     * @param distributionIds
     * @return
     */
    public byte[] getFefuAllRtfPrint(Set<Long> distributionIds)
    {
        try
        {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ZipOutputStream zip = new ZipOutputStream(output);

            for (Long distributionId : distributionIds)
            {
                IEcgDistribution distribution = DataAccessServices.dao().getNotNull(distributionId);
                zip.putNextEntry(new ZipEntry(CoreStringUtils.transliterate(distribution.getTitle()) + ".rtf"));
                IOUtils.write(getFefuRtfPrint(distributionId, false), zip);
                zip.closeEntry();
            }
            zip.close();

            return output.toByteArray();
        } catch (IOException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Печатать все распределения в одном файле (форма ДВФУ)
     */
    public void onClickGlobalAllFefuPrint() {

        Set<Long> ids = getSelectedDistributionIds();

        getPresenter().getSupport().doRefresh();

        if (ids.isEmpty()) return;

        byte[] data = getGlobalAllFefuRtfPrint(ids, false);

        String fileName = new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + "-enrollmentDistributionEnvironment.zip";

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().zip().fileName(fileName).document(data), false);
    }

    /**
     *
     * @param distributionIds - выбранные распределения
     * @param isWithoutEnrolled - включать или нет зачисленных
     * @return
     */
    public byte[] getGlobalAllFefuRtfPrint(Set<Long> distributionIds, boolean isWithoutEnrolled)
    {
        try
        {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ZipOutputStream zip = new ZipOutputStream(output);
            List<EcgDistribution> ecgDistributions = new ArrayList<>();

            for (Long distributionId : distributionIds)
            {
                EcgDistribution distribution = DataAccessServices.dao().getNotNull(distributionId);
                ecgDistributions.add(distribution);
            }
            zip.putNextEntry(new ZipEntry(new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + "-enrollmentDistributionEnvironment.rtf"));
            IOUtils.write(getFefuRtfGlobalPrint(ecgDistributions, isWithoutEnrolled), zip);
            zip.close();

            return output.toByteArray();
        } catch (IOException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /*
     Печатать выбранное распределение без учета зачисленных
     */
    public void onClickWithoutEnrolledFefuPrint()
    {
        Set<Long> ids = getSelectedDistributionIds();

        getPresenter().getSupport().doRefresh();

        if (ids.isEmpty()) return;

        byte[] data = getGlobalAllFefuRtfPrint(ids, true);

        String fileName = new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + "-enrollmentDistributionEnvironment.zip";

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().zip().fileName(fileName).document(data), false);
    }

    private Set<Long> getSelectedDistributionIds()
    {
        EcDistributionListUI presenter = getPresenter();
        String dataSourceName = presenter.getEnrollmentCampaign().isUseCompetitionGroup() ? EcDistributionList.DISTRIBUTION_CG_DS : EcDistributionList.DISTRIBUTION_ED_DS;

        Collection<IEntity> selected = ((CheckboxColumn) (((BaseSearchListDataSource) presenter.getConfig().getDataSource(dataSourceName)).getLegacyDataSource()).getColumn("checkbox")).getSelectedObjects();

        Set<Long> distributionIds = new HashSet<>();

        for (IEntity entity : selected)
        {
            EcgDistributionDTO item = (EcgDistributionDTO) entity;
            if (!item.isDownloadDisabled())
                distributionIds.add(item.getPersistentId());
        }

        return distributionIds;
    }

    public byte[] getFefuRtfPrint(Long distributionId, boolean isWithoutEnrolled)
    {
        IEcgDistribution distribution = DataAccessServices.dao().getNotNull(distributionId);

        getSession().refresh(distribution);

        IFefuEcDistributionListPrint printForm = IFefuEcDistributionListPrint.instance.get();
        printForm.setWithoutEnrolled(isWithoutEnrolled);
        return RtfUtil.toByteArray(distribution.getConfig().getEcgItem() instanceof EnrollmentDirection ?
                printForm.getPrintFormForDistributionPerDirection(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniFefuDefines.TEMPLATE_FEFU_ECG_DISTRIBUTION_PER_DIRECTION).getCurrentTemplate(), (EcgDistribution) distribution) :
                printForm.getPrintFormForDistributionPerCompetitionGroup(DataAccessServices.dao().getByCode(UniecScriptItem.class, UniFefuDefines.TEMPLATE_FEFU_ECG_DISTRIBUTION_PER_COMPETITION_GROUP).getCurrentTemplate(), distribution)
        );
    }

    public byte[] getFefuRtfGlobalPrint(List<EcgDistribution> distributions, boolean isWithoutEnrolled)
    {
        byte[] template = DataAccessServices.dao().getByCode(UniecScriptItem.class, UniFefuDefines.TEMPLATE_FEFU_ECG_DISTRIBUTION_PER_COMPETITION_GROUP).getCurrentTemplate();
        RtfDocument templateDocument = new RtfReader().read(template);
        RtfDocument doc = RtfBean.getElementFactory().createRtfDocument();
        doc.setHeader(templateDocument.getHeader());
        doc.setSettings(templateDocument.getSettings());
        int index = 0;
        for (EcgDistribution distribution : distributions)
        {
            if (index > 0) {
                FefuReportUtil.insertPageBreak(doc.getElementList());
            }
            IFefuEcDistributionListPrint printForm = IFefuEcDistributionListPrint.instance.get();
            printForm.setWithoutEnrolled(isWithoutEnrolled);
            RtfDocument dirDoc = printForm.getPrintFormForDistributionPerCompetitionGroup(template, distribution);
            RtfUtil.modifySourceList(doc.getHeader(), dirDoc.getHeader(), dirDoc.getElementList());
            doc.addElement(dirDoc);
            index++;
        }

        return RtfUtil.toByteArray(doc);
    }


}
