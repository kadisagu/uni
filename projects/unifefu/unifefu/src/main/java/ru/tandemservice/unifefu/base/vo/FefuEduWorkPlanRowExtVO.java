/* $Id$ */
package ru.tandemservice.unifefu.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;

import java.util.Map;

/**
 * @author nvankov
 * @since 1/21/14
 */
public class FefuEduWorkPlanRowExtVO extends DataWrapper
{
    public FefuEduWorkPlanRowExtVO(long id, String title)
    {
        super(id, title);
    }
}
