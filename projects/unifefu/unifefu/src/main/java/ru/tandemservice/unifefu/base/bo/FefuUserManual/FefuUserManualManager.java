/**
 *$Id: FefuUserManualManager.java 38584 2014-10-08 11:05:36Z azhebko $
 */
package ru.tandemservice.unifefu.base.bo.FefuUserManual;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.shared.commonbase.base.bo.UserManual.logic.IUserManualDao;
import org.tandemframework.shared.commonbase.base.bo.UserManual.logic.UserManualDao;

/**
 * @author Rostuncev
 * @since 28.11.12
 */
@Configuration
public class FefuUserManualManager extends BusinessObjectManager
{
    public static FefuUserManualManager instance()
    {
        return instance(FefuUserManualManager.class);
    }

    @Bean
    public IUserManualDao userManualDao()
    {
        return new UserManualDao();
    }
}
