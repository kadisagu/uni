/**
 * SubmitNotificationsFault.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.portal._2014_04_06;

public class SubmitNotificationsFault extends org.apache.axis.AxisFault {
    public java.lang.String submitNotificationsFault;
    public java.lang.String getSubmitNotificationsFault() {
        return this.submitNotificationsFault;
    }

    public SubmitNotificationsFault() {
    }

    public SubmitNotificationsFault(java.lang.Exception target) {
        super(target);
    }

    public SubmitNotificationsFault(java.lang.String message, java.lang.Throwable t) {
        super(message, t);
    }

      public SubmitNotificationsFault(java.lang.String submitNotificationsFault) {
        this.submitNotificationsFault = submitNotificationsFault;
    }

    /**
     * Writes the exception data to the faultDetails
     */
    public void writeDetails(javax.xml.namespace.QName qname, org.apache.axis.encoding.SerializationContext context) throws java.io.IOException {
        context.serialize(qname, null, submitNotificationsFault);
    }
}
