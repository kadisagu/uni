package ru.tandemservice.unifefu.base.bo.FefuTrJournal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.BaseRegistryElementPartDSHandler;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.unifefu.base.ext.TrJournal.logic.*;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.catalog.TrAddonType;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author amakarova
 */
@Configuration
public class FefuTrJournalManager extends TrJournalManager
{
    public static final String TR_ORGUNIT_DS = "trOrgUnitDS";
    public static final String TR_ORGUNIT_YEARPART_DS = "trOrgUnitYearPartDS";

    public static final String TR_ORGUNIT_ACTIVITY_ELEMENT_PART_DS = "trOrgUnitActivityElementPartDS";
    public static final String TR_ORGUNIT_ORGUNIT_KEY = "trOrgUnitActivityElementPartDS_orgUnit";

    public static final String TR_ORGUNIT_PPS_DS = "trOrgUnitPpsDS";

    public static final String TR_ORGUNIT_JOURNAL_DS = "trOrgUnitJournalDS";
    public static final String TR_ORGUNIT_DISCIPLINE_KEY = "trOrgUnitJournalDS_dicipline";
    private static final String TR_ORGUNIT_YEAR_PART_KEY = "trOrgUnitJournalDS_yearPart";

    public static final String TR_JOURNAL_GROUP_DS = "trJournalGroupDS";

    public static final String TR_EVENT_ACTION_TYPES_DS = "actionTypeDS";
    public static final String TR_EVENT_ADDON_TYPES_DS = "addonTypeDS";


    public static FefuTrJournalManager instance() {
        return instance(FefuTrJournalManager.class);
    }

    @Bean
    public UIDataSourceConfig trOrgUnitDSConfig() {
        return SelectDSConfig.with(FefuTrJournalManager.TR_ORGUNIT_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(this.trOrgUnitDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler trOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(DQLExpressions.exists(
                        new DQLSelectBuilder().
                                fromEntity(EppTutorOrgUnit.class, "rel").
                                where(DQLExpressions.eq(DQLExpressions.property("e", OrgUnit.id()), DQLExpressions.property("rel", EppTutorOrgUnit.orgUnit().id()))).buildQuery()));
            }
        }
                .order(OrgUnit.title())
                .filter(OrgUnit.title());
    }

    @Bean
    public UIDataSourceConfig trOrgUnitYearPartDSConfig() {
        return SelectDSConfig.with(FefuTrJournalManager.TR_ORGUNIT_YEARPART_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(this.trOrgUnitYearPartDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trOrgUnitYearPartDSHandler() {
        return new EntityComboDataSourceHandler(this.getName(), EppYearPart.class)
                .order(EppYearPart.year().educationYear().intValue())
                .order(EppYearPart.part().yearDistribution().amount())
                .order(EppYearPart.part().number())
                .filter(EppYearPart.year().educationYear().title())
                .filter(EppYearPart.year().title())
                .filter(EppYearPart.part().yearDistribution().title())
                .filter(EppYearPart.part().title());
    }

    @Bean
    public UIDataSourceConfig trOrgUnitActivityElementPartDSConfig()
    {
        return SelectDSConfig.with(FefuTrJournalManager.TR_ORGUNIT_ACTIVITY_ELEMENT_PART_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(this.trOrgUnitActivityElementPartDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trOrgUnitActivityElementPartDSHandler()
    {
        return new BaseRegistryElementPartDSHandler(this.getName())
                .where(EppRegistryElementPart.registryElement().owner(), TR_ORGUNIT_ORGUNIT_KEY)
                ;
    }

    @Bean
    public UIDataSourceConfig trOrgUnitPpsDSConfig()
    {
        return SelectDSConfig.with(FefuTrJournalManager.TR_ORGUNIT_PPS_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn("ФИО", PpsEntry.person().fio().s())
                .addColumn("Подразделение", PpsEntry.orgUnit().shortTitle().s())
                .addColumn("Данные преподавателя", PpsEntry.titleInfo().s())
                .handler(this.trOrgUnitPpsDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trOrgUnitPpsDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), PpsEntry.class)
                .where(PpsEntry.orgUnit(), TR_ORGUNIT_ORGUNIT_KEY)
                .where(PpsEntry.removalDate(), (Object) null)
                .order(PpsEntry.orgUnit().title())
                .order(PpsEntry.person().identityCard().fullFio())
                .filter(PpsEntry.person().identityCard().fullFio())
                .filter(PpsEntry.orgUnit().title())
                ;
    }

    @Bean
    public UIDataSourceConfig trOrgUnitJournalDSConfig()
    {
        return SelectDSConfig.with(FefuTrJournalManager.TR_ORGUNIT_JOURNAL_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(this.trOrgUnitJournalDSHandler())
                .addColumn(TrJournal.P_CALCULATED_TITLE)
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trOrgUnitJournalDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrJournal.class)
                .where(TrJournal.registryElementPart(), TR_ORGUNIT_DISCIPLINE_KEY)
                .where(TrJournal.yearPart(), TR_ORGUNIT_YEAR_PART_KEY)
                .where(TrJournal.state().code(), (Object) EppState.STATE_ACCEPTED)
                .order(TrJournal.title())
                .filter(TrJournal.title())
                ;
    }

    @Bean
    public IFefuTrJournalPrintDao fefuPrintDao()
    {
        return new FefuTrJournalPrintDao();
    }

    @Bean
    public IFefuTrJournalEventDao fefuEventDao()
    {
        return new FefuTrJournalEventDao();
    }

    @Bean
    public UIDataSourceConfig trJournalGroupDSConfig()
    {
        return SelectDSConfig.with(TR_JOURNAL_GROUP_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn("title", TrJournalGroup.group().title().s())
                .handler(this.trJournalGroupDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trJournalGroupDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrJournalGroup.class)
                .where(TrJournalGroup.journal(), TR_JOURNAL_JOURNAL_KEY)
                .order(TrJournalGroup.group().title())
                .filter(TrJournalGroup.group().title())
                ;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> actionTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppIControlActionType.class)
                .where(EppIControlActionType.disabled(), Boolean.FALSE)
                .order(EppIControlActionType.code())
                .filter(EppIControlActionType.title())
                ;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> addonTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrAddonType.class)
                .order(TrAddonType.title())
                .filter(TrAddonType.title())
                ;
    }
}

