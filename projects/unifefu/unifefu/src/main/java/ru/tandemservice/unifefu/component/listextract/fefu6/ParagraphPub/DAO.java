/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class DAO extends AbstractListParagraphPubDAO<FefuGiveDiplomaWithDismissStuListExtract, Model> implements IDAO
{
}
