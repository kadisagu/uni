/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu10;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.e86.utils.EmployeePostDecl;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author nvankov
 * @since 11/21/13
 */
public class FullStateMaintenanceEnrollmentStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        final RtfDocument document = new RtfReader().read(template);

        List<FullStateMaintenanceEnrollmentStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        FullStateMaintenanceEnrollmentStuListExtract firstExtract = extracts.get(0);
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, firstExtract);

        Collections.sort(extracts, new Comparator<AbstractStudentExtract>()
        {
            @Override
            public int compare(AbstractStudentExtract o1, AbstractStudentExtract o2)
            {
                if (!o1.getEntity().getEducationOrgUnit().getFormativeOrgUnit().equals(o2.getEntity().getEducationOrgUnit().getFormativeOrgUnit()))
                {
                    return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getPrintTitle(), o2.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getPrintTitle());
                }
                else
                {
                    if (!o1.getEntity().getEducationOrgUnit().getTerritorialOrgUnit().equals(o2.getEntity().getEducationOrgUnit().getTerritorialOrgUnit()))
                    {
                        if (o1.getEntity().getEducationOrgUnit().getTerritorialOrgUnit() instanceof TopOrgUnit)
                            return -1;
                        else if (o2.getEntity().getEducationOrgUnit().getTerritorialOrgUnit() instanceof TopOrgUnit)
                            return 1;

                        return CommonCollator.RUSSIAN_COLLATOR.compare(o1.getEntity().getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialTitle(), o2.getEntity().getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialTitle());
                    }

                    return Student.FULL_FIO_AND_ID_COMPARATOR.compare(o1.getEntity(), o2.getEntity());
                }
            }
        });

        injectModifier.put("course", firstExtract.getCourse().getTitle());
        injectModifier.put("protocolDateAndNum", firstExtract.getProtocolNumAndDate());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(firstExtract.getEnrollDate());
        injectModifier.put("enrollDate", calendar.get(Calendar.DAY_OF_MONTH) + " " + CommonBaseDateUtil.getMonthNameDeclined(calendar.get(Calendar.MONTH) + 1, GrammaCase.GENITIVE) + " " + calendar.get(Calendar.YEAR) + " года");

        injectModifier.put("responsibleForAgreement", StringUtils.capitalize(EmployeePostDecl.getEmployeeStrDat(firstExtract.getResponsibleForAgreement())));
        injectModifier.put("responsibleForPayments", EmployeePostDecl.getEmployeeStrInst(firstExtract.getResponsibleForPayments()));
        injectModifier.put("paymentDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getStartPaymentPeriodDate()));

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);

        tableModifier.put("T", getTableData(extracts));
        tableModifier.modify(document);

        // Вставляем список параграфов
//        injectParagraphs(document, preparedParagraphsStructure);
        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    private String[][] getTableData(List<FullStateMaintenanceEnrollmentStuListExtract> extracts)
    {
        List<String[]> rows = Lists.newArrayList();
        for (FullStateMaintenanceEnrollmentStuListExtract extract : extracts)
        {
            String[] row = new String[3];
            row[0] = String.valueOf(extracts.indexOf(extract) + 1);
            row[1] = extract.getEntity().getFullFio();

            String ou = extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getPrintTitle();
            if (!(extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit() instanceof TopOrgUnit))
                ou += " (" + extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialTitle() + ")";
            row[2] = ou;
            rows.add(row);
        }
        return rows.toArray(new String[][]{});
    }
}
