/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class FefuAcadGrantAssignStuEnrolmentListExtractPrint implements IPrintFormCreator<FefuAcadGrantAssignStuEnrolmentListExtract>, IListParagraphPrintFormCreator<FefuAcadGrantAssignStuEnrolmentListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuAcadGrantAssignStuEnrolmentListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        Student student = extract.getEntity();

        CommonExtractPrint.initCompensationType(modifier, "", student.getCompensationType(), true, student.isTargetAdmission());
        CommonExtractPrint.modifyEducationStr(modifier, student.getEducationOrgUnit());

        CommonExtractPrint.initOrgUnit(modifier, student.getEducationOrgUnit(), "formativeOrgUnitStr", "");

        modifier.put("dateBegin", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));
        modifier.put("dateEnd", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));
        modifier.put("grantSize", getPrintedGrantSize(extract));

        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, student.getEducationOrgUnit(),
                                                               CommonListOrderPrint.getEducationBaseText(student.getGroup()), "fefuShortFastExtendedOptionalText");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    public static String getPrintedGrantSize(FefuAcadGrantAssignStuEnrolmentListExtract extract)
    {
        StringBuilder ret = new StringBuilder(String.valueOf(extract.getGrantSizeInRuble()));
        if (extract.getGroupManagerBonusSize() != null)
            ret.append("+").append(String.valueOf(extract.getGroupManagerBonusSizeInRuble()));
        ret.append(" руб.");
        return ret.toString();
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuAcadGrantAssignStuEnrolmentListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuAcadGrantAssignStuEnrolmentListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuAcadGrantAssignStuEnrolmentListExtract firstExtract)
    {
    }
}