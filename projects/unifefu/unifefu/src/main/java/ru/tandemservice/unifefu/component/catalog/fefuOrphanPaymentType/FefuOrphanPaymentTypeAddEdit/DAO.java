/* $Id$ */
package ru.tandemservice.unifefu.component.catalog.fefuOrphanPaymentType.FefuOrphanPaymentTypeAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;

/**
 * @author nvankov
 * @since 11/18/13
 */
public class DAO extends DefaultCatalogAddEditDAO<FefuOrphanPaymentType, Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.isAddForm())
        {
            MQBuilder builder = new MQBuilder(FefuOrphanPaymentType.ENTITY_CLASS, "o");
            int priority = (int) builder.getResultCount(getSession());
            model.getCatalogItem().setPriority(priority);
        }
    }
}