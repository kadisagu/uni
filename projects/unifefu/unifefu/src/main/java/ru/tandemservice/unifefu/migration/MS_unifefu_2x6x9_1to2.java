package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x9_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.9"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.9")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEppStateEduStandardParameters

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("ffeppsttedstndrdprmtrs_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("edustandard_id", DBType.LONG).setNullable(false),
				new DBColumn("developgridforfulltimeform_id", DBType.LONG).setNullable(false),
				new DBColumn("laborfulltime_p", DBType.INTEGER).setNullable(false),
				new DBColumn("laborfulltimeoneyear_p", DBType.INTEGER).setNullable(false),
				new DBColumn("lbrprttmandextrmrlonyr_p", DBType.INTEGER).setNullable(false),
				new DBColumn("maxhourslecturetypepercent_p", DBType.INTEGER).setNullable(false),
				new DBColumn("minunitschoicedisciplines_p", DBType.INTEGER).setNullable(false),
				new DBColumn("blockchecknumberlectures_id", DBType.LONG).setNullable(false),
				new DBColumn("blockcheckzetvariablepart_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuEppStateEduStandardParameters");
		}

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuCompetence2EppStateEduStandardRel

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("ffcmptnc2eppsttedstndrdrl_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("edustandard_id", DBType.LONG).setNullable(false),
				new DBColumn("fefucompetence_id", DBType.LONG).setNullable(false),
				new DBColumn("number_p", DBType.INTEGER).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuCompetence2EppStateEduStandardRel");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuEppStateEduStandardLaborBlocks

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("ffeppsttedstndrdlbrblcks_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("edustandard_id", DBType.LONG).setNullable(false),
				new DBColumn("qualification_id", DBType.LONG).setNullable(false),
				new DBColumn("parent_id", DBType.LONG),
				new DBColumn("block_id", DBType.LONG).setNullable(false),
				new DBColumn("minnumber_p", DBType.INTEGER).setNullable(false),
				new DBColumn("maxnumber_p", DBType.INTEGER).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuEppStateEduStandardLaborBlocks");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuEduPlanVersionCheckResults

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("ffedplnvrsnchckrslts_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("block_id", DBType.LONG).setNullable(false),
				new DBColumn("checktime_p", DBType.TIMESTAMP).setNullable(false),
				new DBColumn("message_p", DBType.createVarchar(1024)).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuEduPlanVersionCheckResults");
        }
    }
}