/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.MergePersonsAndUpdateGuids;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.NsiDatagramUtil;

import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 30.05.2014
 */
public class NsiPersonMergeWrapper
{
    private Long _id;
    private String _guid;
    private Date _birthDate;
    private String _lastName;
    private String _firstName;
    private String _middleName;
    private String _nsiGuid;

    public NsiPersonMergeWrapper()
    {
    }

    public NsiPersonMergeWrapper(Long id, String guid, Date birthDate, String lastName, String firstName, String middleName, String nsiGuid)
    {
        _id = id;
        _guid = guid;
        _birthDate = birthDate;
        _lastName = lastName;
        _firstName = firstName;
        _middleName = middleName;
        _nsiGuid = nsiGuid;
    }

    public NsiPersonMergeWrapper(Person person, FefuNsiIds nsiIds)
    {
        _id = person.getId();
        _guid = nsiIds.getGuid();
        _birthDate = person.getIdentityCard().getBirthDate();
        _lastName = person.getIdentityCard().getLastName();
        _firstName = person.getIdentityCard().getFirstName();
        _middleName = person.getIdentityCard().getMiddleName();
        _nsiGuid = nsiIds.getGuid();
    }

    public NsiPersonMergeWrapper(Object[] personLine)
    {
        _id = (Long) personLine[1];
        _guid = (String) personLine[0];
        _lastName = (String) personLine[2];
        _firstName = (String) personLine[3];
        _middleName = (String) personLine[4];
        _birthDate = (Date) personLine[5];
    }

    public NsiPersonMergeWrapper(String[] personLine)
    {
        _guid = personLine[5];
        _lastName = personLine[0];
        _firstName = personLine[1];
        _middleName = personLine[2];
        _birthDate = NsiDatagramUtil.parseDate(personLine[3]);
        _nsiGuid = personLine[4];

        if ("NULL".equals(_lastName)) _lastName = null;
        if ("NULL".equals(_firstName)) _firstName = null;
        if ("NULL".equals(_middleName)) _middleName = null;
    }

    public boolean isPersonsNameAreDifferent(NsiPersonMergeWrapper wrapper)
    {
        if (null == getLastName() && null != wrapper.getLastName()) return true;
        if (null != getLastName() && null == wrapper.getLastName()) return true;
        if (null != getLastName() && null != wrapper.getLastName() && !getLastName().equals(wrapper.getLastName()))
            return true;

        if (null == getFirstName() && null != wrapper.getFirstName()) return true;
        if (null != getFirstName() && null == wrapper.getFirstName()) return true;
        if (null != getFirstName() && null != wrapper.getFirstName() && !getFirstName().equals(wrapper.getFirstName()))
            return true;

        if (null == getMiddleName() && null != wrapper.getMiddleName()) return true;
        if (null != getMiddleName() && null == wrapper.getMiddleName()) return true;
        if (null != getMiddleName() && null != wrapper.getMiddleName() && !getMiddleName().equals(wrapper.getMiddleName()))
            return true;

        return false;
    }

    public boolean isPersonsBirthDatesAreDifferent(NsiPersonMergeWrapper wrapper)
    {
        if (null == getBirthDate() && null != wrapper.getBirthDate()) return true;
        if (null != getBirthDate() && null == wrapper.getBirthDate()) return true;
        if (null != getBirthDate() && null != wrapper.getBirthDate() && !getBirthDate().equals(wrapper.getBirthDate()))
            return true;

        return false;
    }

    public String getFullFioDifferenceStr(NsiPersonMergeWrapper wrapper)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(null != getLastName() ? getLastName() : "");
        builder.append(null != getFirstName() ? (builder.length() > 0 ? " " : "") + getFirstName() : "");
        builder.append(null != getMiddleName() ? (builder.length() > 0 ? " " : "") + getMiddleName() : "");
        builder.append(" - ");
        builder.append(null != wrapper.getLastName() ? wrapper.getLastName() : "");
        builder.append(null != wrapper.getFirstName() ? (" " + wrapper.getFirstName()) : "");
        builder.append(null != wrapper.getMiddleName() ? (" " + wrapper.getMiddleName()) : "");
        return builder.toString();
    }

    public String getDatesDifferenceStr(NsiPersonMergeWrapper wrapper)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(null != getBirthDate() ? DateFormatter.DEFAULT_DATE_FORMATTER.format(getBirthDate()) : "----");
        builder.append(" - ");
        builder.append(null != wrapper.getBirthDate() ? DateFormatter.DEFAULT_DATE_FORMATTER.format(wrapper.getBirthDate()) : "----");
        return builder.toString();
    }

    public String getPrintableTitle()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(getGuid()).append(" - ").append(getLastName()).append(" ").append(getFirstName()).append(" ").append(getMiddleName())
                .append(" ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getBirthDate()));
        return builder.toString();
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public Date getBirthDate()
    {
        return _birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
        _birthDate = birthDate;
    }

    public String getLastName()
    {
        return _lastName;
    }

    public void setLastName(String lastName)
    {
        _lastName = lastName;
    }

    public String getFirstName()
    {
        return _firstName;
    }

    public void setFirstName(String firstName)
    {
        _firstName = firstName;
    }

    public String getMiddleName()
    {
        return _middleName;
    }

    public void setMiddleName(String middleName)
    {
        _middleName = middleName;
    }

    public String getNsiGuid()
    {
        return _nsiGuid;
    }

    public void setNsiGuid(String nsiGuid)
    {
        _nsiGuid = nsiGuid;
    }
}
