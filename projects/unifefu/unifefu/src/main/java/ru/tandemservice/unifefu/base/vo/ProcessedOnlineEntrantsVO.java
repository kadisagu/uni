/* $Id$ */
package ru.tandemservice.unifefu.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.OnlineEntrantNotifications.FefuSystemActionOnlineEntrantNotifications;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;

import java.util.Date;

/**
 * @author nvankov
 * @since 6/17/13
 */
public class ProcessedOnlineEntrantsVO extends DataWrapper
{
    private OnlineEntrant _onlineEntrant;
    private FefuForeignOnlineEntrant _foreignOnlineEntrant;
    private Boolean _foreign;
    private Date _processedDate;
    private Long _entrantType;

    public ProcessedOnlineEntrantsVO(OnlineEntrant onlineEntrant)
    {
        super(onlineEntrant.getId(), onlineEntrant.getFullFio());
        _onlineEntrant = onlineEntrant;
        _foreign = Boolean.FALSE;
        _entrantType = FefuSystemActionManager.ENTRANT_TYPE_DEFAULT;
        _processedDate = onlineEntrant.getEntrant().getRegistrationDate();
    }

    public ProcessedOnlineEntrantsVO(FefuForeignOnlineEntrant foreignOnlineEntrant)
    {
        super(foreignOnlineEntrant.getId(), foreignOnlineEntrant.getFullFio());
        _foreignOnlineEntrant = foreignOnlineEntrant;
        _foreign = Boolean.TRUE;
        _entrantType = FefuSystemActionManager.ENTRANT_TYPE_FOREIGN;
        if(null != foreignOnlineEntrant.getEntrant())
        {
            _processedDate = foreignOnlineEntrant.getEntrant().getRegistrationDate();
        }
        else
        {
            _processedDate = null;
        }
    }

    public Boolean getForeign()
    {
        return _foreign;
    }

    public OnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    public FefuForeignOnlineEntrant getForeignOnlineEntrant()
    {
        return _foreignOnlineEntrant;
    }

    public Date getProcessedDate()
    {
        return _processedDate;
    }

    public Long getEntrantType()
    {
        return _entrantType;
    }

    public String getType()
    {
        return _foreign ? "Иностранный" : "Обычный";
    }
}
