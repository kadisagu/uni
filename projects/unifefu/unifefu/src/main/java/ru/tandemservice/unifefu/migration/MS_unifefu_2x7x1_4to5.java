package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.fias.base.entity.AddressInter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Dmitry Seleznev
 * @since 18.01.2015
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.16"),
                        new ScriptDependency("org.tandemframework.shared", "1.7.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        List<AddressInterWrapper> addWrappers = new ArrayList<>();
        short adIntDiscriminator = tool.entityCodes().get(AddressInter.ENTITY_NAME);

        // Забираем текущие адреса персоны
        Statement perStmt = tool.getConnection().createStatement();
        perStmt.execute("select p.id, c.id, a.id, ad.country_id, ad.settlement_id, ai.region_p, ai.district_p, ai.addresslocation_p, ai.postcode_p " +
                "from addressbase_t a inner join addressdetailed_t ad on a.id=ad.id inner join addressinter_t ai on a.id=ai.id inner join person_t p on a.id=p.address_id " +
                "inner join fefunsipersoncontact_t c on a.id=c.address_id where a.discriminator<>" + adIntDiscriminator);
        ResultSet perRs = perStmt.getResultSet();

        while (perRs.next())
        {
            addWrappers.add(new AddressInterWrapper(perRs, true));
        }

        // Забираем текущие адреса удостоверений личности
        Statement icStmt = tool.getConnection().createStatement();
        icStmt.execute("select ic.id, c.id, a.id, ad.country_id, ad.settlement_id, ai.region_p, ai.district_p, ai.addresslocation_p, ai.postcode_p " +
                "from addressbase_t a inner join addressdetailed_t ad on a.id=ad.id inner join addressinter_t ai on a.id=ai.id inner join identitycard_t ic on a.id=ic.address_id " +
                "inner join fefunsipersoncontact_t c on a.id=c.address_id where a.discriminator<>" + adIntDiscriminator);
        ResultSet icRs = icStmt.getResultSet();

        while (icRs.next())
        {
            addWrappers.add(new AddressInterWrapper(icRs, false));
        }

        icStmt.close();
        perStmt.close();

        Set<Long> countryIdSet = getEntityIdSet(tool, "addresscountry_t");
        Set<Long> addrItemIdSet = getEntityIdSet(tool, "addressitem_t");

        // Пересоздаём адреса с правильными идентификаторами и дискриминаторами
        PreparedStatement badrIns = tool.prepareStatement("insert into addressbase_t (id, discriminator) values(?, ?)");
        PreparedStatement deadrIns = tool.prepareStatement("insert into addressdetailed_t (id, country_id, settlement_id) values(?, ?, ?)");
        PreparedStatement interadrIns = tool.prepareStatement("insert into addressinter_t (id, region_p, district_p, addresslocation_p, postcode_p) values(?, ?, ?, ?, ?)");
        PreparedStatement personUpdate = tool.prepareStatement("update person_t set address_id=? where id=?");
        PreparedStatement iCardUpdate = tool.prepareStatement("update identitycard_t set address_id=? where id=?");
        PreparedStatement nsiContactUpdate = tool.prepareStatement("update fefunsipersoncontact_t set address_id=? where id=?");
        PreparedStatement mdbViewUpdate = tool.prepareStatement("update mdbviewaddress_t set addressbase_id=? where addressbase_id=?");
        PreparedStatement oldAddrIntDelete = tool.prepareStatement("delete from addressinter_t where id=?");
        PreparedStatement oldAddrDetDelete = tool.prepareStatement("delete from addressdetailed_t where id=?");
        PreparedStatement oldAddrBaseDelete = tool.prepareStatement("delete from addressbase_t where id=?");

        for (AddressInterWrapper wrapper : addWrappers)
        {
            Long addrId = EntityIDGenerator.generateNewId(adIntDiscriminator);

            badrIns.setLong(1, addrId);
            badrIns.setShort(2, adIntDiscriminator);
            badrIns.executeUpdate();

            boolean countryInvalid = validateField(addrId, wrapper.getCountryId(), countryIdSet, false, "Country", false);
            boolean settlInvalid = validateField(addrId, wrapper.getSettlementId(), addrItemIdSet, true, "Settlement", false);

            deadrIns.setLong(1, addrId);
            deadrIns.setObject(2, countryInvalid ? null : wrapper.getCountryId());
            deadrIns.setObject(3, settlInvalid ? null : wrapper.getSettlementId());
            deadrIns.executeUpdate();

            interadrIns.setLong(1, addrId);
            interadrIns.setObject(2, wrapper.getRegion());
            interadrIns.setObject(3, wrapper.getDistrict());
            interadrIns.setObject(4, wrapper.getLocation());
            interadrIns.setObject(5, wrapper.getPostCode());
            interadrIns.executeUpdate();

            if (null != wrapper.getPersonId())
            {
                personUpdate.setLong(1, addrId);
                personUpdate.setLong(2, wrapper.getPersonId());
                personUpdate.executeUpdate();
            }

            if (null != wrapper.getIcId())
            {
                iCardUpdate.setLong(1, addrId);
                iCardUpdate.setLong(2, wrapper.getIcId());
                iCardUpdate.executeUpdate();
            }

            nsiContactUpdate.setLong(1, addrId);
            nsiContactUpdate.setLong(2, wrapper.getNsiContactId());
            nsiContactUpdate.executeUpdate();

            mdbViewUpdate.setLong(1, addrId);
            mdbViewUpdate.setLong(2, wrapper.getOldAddrId());
            mdbViewUpdate.executeUpdate();

            oldAddrIntDelete.setLong(1, wrapper.getOldAddrId());
            oldAddrIntDelete.executeUpdate();

            oldAddrDetDelete.setLong(1, wrapper.getOldAddrId());
            oldAddrDetDelete.executeUpdate();

            try
            {
                oldAddrBaseDelete.setLong(1, wrapper.getOldAddrId());
                oldAddrBaseDelete.executeUpdate();
            } catch (SQLException ex)
            {
                System.out.println("Could not delete address id= " + wrapper.getOldAddrId() + ": " + ex.getMessage());
            }
        }

        badrIns.close();
        deadrIns.close();
        interadrIns.close();
        personUpdate.close();
        iCardUpdate.close();
        nsiContactUpdate.close();
        mdbViewUpdate.close();
        oldAddrIntDelete.close();
        oldAddrDetDelete.close();
        oldAddrBaseDelete.close();
    }

    private Set<Long> getEntityIdSet(DBTool tool, String tableName) throws SQLException
    {
        Set<Long> resultSet = new HashSet<>();
        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select id from " + tableName);
        ResultSet rs = stmt.getResultSet();
        while (rs.next()) resultSet.add(rs.getLong(1));
        return resultSet;
    }

    private boolean validateField(Long addrId, Long objId, Set<Long> objIdSet, boolean allowNulls, String objectName, boolean invalid)
    {
        if (!allowNulls && null == objId)
        {
            System.out.println("Address id= " + addrId + " could not be imported. " + objectName + " does not allow nulls.");
            return true;
        }
        if (null != objId && !objIdSet.contains(objId))
        {
            System.out.println("Address id= " + addrId + " could not be imported. " + objectName + " with id= " + objId + " not found at database.");
            return true;
        }

        return invalid;
    }

    public class AddressInterWrapper
    {
        private Long _personId;
        private Long _icId;
        private Long _nsiContactId;
        private Long _oldAddrId;
        private Long _countryId;
        private String _postCode;
        private Long _settlementId;
        private String _region;
        private String _district;
        private String _location;

        public AddressInterWrapper(ResultSet rs, boolean person) throws SQLException
        {
            if (person) _personId = rs.getLong(1);
            else _icId = rs.getLong(1);
            _nsiContactId = rs.getLong(2);
            _oldAddrId = rs.getLong(3);
            _countryId = rs.getLong(4);
            _settlementId = rs.getLong(5);
            _region = rs.getString(6);
            _district = rs.getString(7);
            _location = rs.getString(8);
            _postCode = rs.getString(9);

            if (0 == _countryId) _countryId = null;
            if (0 == _settlementId) _settlementId = null;
        }

        public Long getPersonId() { return _personId; }
        public Long getIcId() { return _icId; }
        public Long getNsiContactId() { return _nsiContactId; }
        public Long getOldAddrId() { return _oldAddrId; }
        public Long getCountryId() { return _countryId; }
        public String getPostCode() { return _postCode; }
        public Long getSettlementId() { return _settlementId; }
        public String getRegion() { return _region; }
        public String getDistrict() { return _district; }
        public String getLocation() { return _location; }
    }
}