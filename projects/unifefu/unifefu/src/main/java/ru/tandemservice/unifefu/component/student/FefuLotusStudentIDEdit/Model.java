/* $Id$ */
package ru.tandemservice.unifefu.component.student.FefuLotusStudentIDEdit;

import org.tandemframework.core.component.Input;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

/**
 * @author Ekaterina Zvereva
 * @since 11.08.2014
 */
@Input(keys = "studentId", bindings = "studentId")
public class Model
{
    private Long _studentId;
    private StudentFefuExt _studentFefuExt;
    private String _studentLotusID;

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public StudentFefuExt getStudentFefuExt()
    {
        return _studentFefuExt;
    }

    public void setStudentFefuExt(StudentFefuExt studentFefuExt)
    {
        _studentFefuExt = studentFefuExt;
    }

    public String getStudentLotusID()
    {
        return _studentLotusID;
    }

    public void setStudentLotusID(String studentLotusID)
    {
        _studentLotusID = studentLotusID;
    }
}