/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu12.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 13.05.2013
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuEduTransferEnrolmentStuExtract, Model>
{
}