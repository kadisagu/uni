/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author Nikolay Fedorovskih
 * @since 28.03.2014
 */
public interface IBBUpdateEppEduGroupDaemon
{
    SpringBeanCache<IBBUpdateEppEduGroupDaemon> instance = new SpringBeanCache<>(IBBUpdateEppEduGroupDaemon.class.getName());

    /**
     * Удаление с курсов ВВ групп, в которых нет студентов
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void deleteOrphanGroups();

    /**
     * Удаление с курсов студентов, которые пропали из соответствующих реализаций дисциплин
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void cleanCourseStudents();

    /**
     * Удаление из групп ВВ участников, которые пропали из групп в соответствующих реализациях дисциплин
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void cleanGroupMemberships();

    /**
     * Обновление списка групп и студентов для ЭУК
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doUpdate();
}