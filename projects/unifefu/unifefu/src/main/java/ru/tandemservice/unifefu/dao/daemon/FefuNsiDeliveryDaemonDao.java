/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import edu.emory.mathcs.backport.java.util.Collections;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 09.02.2015
 */
public class FefuNsiDeliveryDaemonDao extends UniBaseDao implements IFefuNsiDeliveryDaemonDao
{
    public static final String NSI_SERVICE_AUTOSYNC = "fefu.nsi.service.autosync";
    public static final int DAEMON_ITERATION_TIME = 1;

    private static final int[] ATTEMPTS_INTERVAL_ARRAY = new int[]
            {
                    0, 60000, 60000, 300000, 300000, 60000, 1800000, 60000, 3600000, 60000, 14400000
            };

    private static boolean sync_locked = false;
    private static boolean manual_locked = false;

    private static int attemptsCount = 0;
    private static Date deliveryLockTime = null;
    private static boolean deliveryLocked = false;
    private static Long logRowToReDeliver = null;

    public static void lockDaemon()
    {
        manual_locked = true;
    }

    public static void unlockDaemon()
    {
        manual_locked = false;
        deliveryLocked = false;
        deliveryLockTime = null;
    }

    public static boolean isLocked()
    {
        return manual_locked;
    }

    public static boolean isLockedByTransportError()
    {
        return deliveryLocked;
    }

    public static void lockByTransportError(Long logRowId)
    {
        deliveryLockTime = new Date();
        deliveryLocked = true;
        logRowToReDeliver = logRowId;
    }

    public static void unlockAfterTransportError()
    {
        deliveryLockTime = null;
        deliveryLocked = false;
        logRowToReDeliver = null;
        attemptsCount = 0;
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(FefuNsiDeliveryDaemonDao.class.getName(), DAEMON_ITERATION_TIME, IFefuNsiDeliveryDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            // Проверяем, включена ли автосинхронизация. Если порпертя отсутствует в конфигах, то прерываем выполнение демона
            if (null == ApplicationRuntime.getProperty(NSI_SERVICE_AUTOSYNC)) return;
            if (!Boolean.parseBoolean(ApplicationRuntime.getProperty(NSI_SERVICE_AUTOSYNC))) return;


            // Если в данный момент идёт синхронизация, то пропускаем очередной шаг демона
            if (sync_locked || manual_locked) return;

            if (null != deliveryLockTime)
            {
                int timeToWaitMilis = ATTEMPTS_INTERVAL_ARRAY.length > attemptsCount ? ATTEMPTS_INTERVAL_ARRAY[attemptsCount] : ATTEMPTS_INTERVAL_ARRAY[0];
                long timePassedMilis = System.currentTimeMillis() - deliveryLockTime.getTime();
                if (timePassedMilis > timeToWaitMilis)
                {
                    deliveryLocked = false;
                    deliveryLockTime = new Date();
                    attemptsCount++;
                } else
                {
                    deliveryLocked = true;
                }
            }

            if (null != deliveryLockTime && attemptsCount >= 10) unlockAfterTransportError();

            final IFefuNsiDeliveryDaemonDao dao = IFefuNsiDeliveryDaemonDao.instance.get();
            sync_locked = true;

            try
            {
                List<Long> packIdList = dao.getPreparedNSIPackageIds(logRowToReDeliver);
                if (deliveryLocked)
                {
                    sync_locked = false;
                    return;
                }

                for (Long packId : packIdList)
                {
                    if (deliveryLocked)
                    {
                        sync_locked = false;
                        return;
                    }
                    dao.doDeliverPackageToNSI(packId);
                }
            } catch (final Throwable t)
            {
                sync_locked = false;
                unlockAfterTransportError();
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            }
            sync_locked = false;
            if (!deliveryLocked) unlockAfterTransportError();
        }
    };

    protected Session lock4update()
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, IFefuNsiDeliveryDaemonDao.GLOBAL_DAEMON_LOCK);
        return session;
    }

    @Override
    public List<Long> getPreparedNSIPackageIds(Long logRowToReDeliver)
    {
        if (null != logRowToReDeliver) return Collections.singletonList(logRowToReDeliver);

        return new DQLSelectBuilder().fromEntity(FefuNsiLogRow.class, "e").column(property(FefuNsiLogRow.id().fromAlias("e")))
                .where(eq(property(FefuNsiLogRow.executed().fromAlias("e")), value(Boolean.FALSE)))
                .where(eq(property(FefuNsiLogRow.eventType().fromAlias("e")), value(FefuNsiLogRow.OUT_EVENT_TYPE)))
                .where(lt(property(FefuNsiLogRow.attemptsCount().fromAlias("e")), value(10)))
                .where(or(
                        eq(property(FefuNsiLogRow.messageStatus().fromAlias("e")), value(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_INITIAL)),
                        eq(property(FefuNsiLogRow.messageStatus().fromAlias("e")), value(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_TRANSPORT_ERROR))
                ))

                .order(property(FefuNsiLogRow.messageTime().fromAlias("e"))).createStatement(getSession()).list();
    }

    @Override
    public void doDeliverPackageToNSI(Long logRowId)
    {
        NsiReactorUtils.executeNSIAction(logRowId, false);
    }
}