/*$Id$*/
package ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad.logic;

import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermLoad;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 16.01.2015
 */

final class EpvRowTermWrapper
{
	private final EppEpvRowTerm _rowTerm;
	private final List<EppEpvRowTermAction> _rowTermActions;
	private final List<EppEpvRowTermLoad> _rowTermLoads;

	public EpvRowTermWrapper(EppEpvRowTerm rowTerm, List<EppEpvRowTermAction> rowTermActions, List<EppEpvRowTermLoad> rowTermLoads)
	{
		_rowTerm = rowTerm;
		_rowTermActions = rowTermActions;
		_rowTermLoads = rowTermLoads;
	}

	public EppEpvRowTerm getRowTerm()
	{
		return _rowTerm;
	}
	
	public List<EppEpvRowTermAction> getRowTermActions()
	{
		return _rowTermActions;
	}

	public List<EppEpvRowTermLoad> getRowTermLoads()
	{
		return _rowTermLoads;
	}
}
