package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x1_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuSocGrantResumptionStuExtract

		// создано свойство paymentEndDate
        if (!tool.columnExists("ffscgrntrsmptnstextrct_t", "paymentenddate_p"))
		{
			// создать колонку
			tool.createColumn("ffscgrntrsmptnstextrct_t", new DBColumn("paymentenddate_p", DBType.DATE));
        }

		////////////////////////////////////////////////////////////////////////////////
		// сущность fullStateMaintenanceEnrollmentStuExtract

		// создано свойство paymentEndDate
        if (!tool.columnExists("fllsttmntnncenrllmntstextrct_t", "paymentenddate_p"))
		{
			// создать колонку
			tool.createColumn("fllsttmntnncenrllmntstextrct_t", new DBColumn("paymentenddate_p", DBType.DATE));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fullStateMaintenanceEnrollmentStuListExtract

		// создано свойство paymentEndDate
        if (!tool.columnExists("fllsttmntnncenrllmntstlstext_t", "paymentenddate_p"))
		{
			// создать колонку
			tool.createColumn("fllsttmntnncenrllmntstlstext_t", new DBColumn("paymentenddate_p", DBType.DATE));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fullStateMaintenanceStuExtract

		// создано свойство paymentEndDate
        if (!tool.columnExists("fllsttmntnncstextrct_t", "paymentenddate_p"))
		{
			// создать колонку
			tool.createColumn("fllsttmntnncstextrct_t", new DBColumn("paymentenddate_p", DBType.DATE));
		}
    }
}