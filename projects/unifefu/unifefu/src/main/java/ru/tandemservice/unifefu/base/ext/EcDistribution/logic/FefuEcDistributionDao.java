package ru.tandemservice.unifefu.base.ext.EcDistribution.logic;

import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcDistributionDao;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.EcgDistributionDTO;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unifefu.UniFefuDefines;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: amakarova
 * Date: 28.07.13
 */
public class FefuEcDistributionDao extends EcDistributionDao {

    public byte[] getFefuRtfPrint(Long distributionId, boolean isAll) {
        if (isAll)
            return super.getRtfPrint(distributionId);
        else {
            IFefuEcDistributionListPrint printForm = IFefuEcDistributionListPrint.instance.get();
            IEcgDistribution distribution = getNotNull(distributionId);
            getSession().refresh(distribution);
            return RtfUtil.toByteArray(distribution.getConfig().getEcgItem() instanceof EnrollmentDirection ?
                    printForm.getPrintFormForDistributionPerDirection(getByCode(UniecScriptItem.class, UniFefuDefines.TEMPLATE_FEFU_ECG_DISTRIBUTION_PER_DIRECTION).getCurrentTemplate(), (EcgDistribution) distribution) :
                    printForm.getPrintFormForDistributionPerCompetitionGroup(getByCode(UniecScriptItem.class, UniFefuDefines.TEMPLATE_FEFU_ECG_DISTRIBUTION_PER_COMPETITION_GROUP).getCurrentTemplate(), distribution));
        }
    }

    @Override
    public byte[] getRtfPrint(Long distributionId)
    {
        return getFefuRtfPrint(distributionId, false);
    }

    @Override
    public byte[] getBulkRtfPrint(Set<Long> distributionIds)
    {
        try
        {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ZipOutputStream zip = new ZipOutputStream(output);

            for (Long distributionId : distributionIds)
            {
                IEcgDistribution distribution = DataAccessServices.dao().getNotNull(distributionId);
                zip.putNextEntry(new ZipEntry(getDistributionFileName(distribution) + ".rtf"));
                IOUtils.write(getFefuRtfPrint(distributionId, true), zip);
                zip.closeEntry();
            }
            zip.close();

            return output.toByteArray();
        } catch (IOException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public String getDistributionFileName(IEcgDistribution distribution)
    {
        return CoreStringUtils.transliterate(distribution.getTitle()) + " " +
                new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
    }

    @Override
    public String getDistributionFileName(EcgDistributionDTO distributionDTO)
    {
        String wave = "Volna " + Integer.toString(distributionDTO.getWave()) + " ";
        return wave + CoreStringUtils.transliterate(distributionDTO.getEcgItem().getTitle()) + " " +
                new SimpleDateFormat("yyyyMMdd").format(new Date());
    }

    protected List<EcgDistributionQuota> getEcgDistributionQuotaList(EcgDistribution distribution)
    {
        final List<EcgDistributionQuota> directionList = new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "e")
                .where(eq(property(EcgDistributionQuota.distribution().fromAlias("e")), value(distribution)))
                .order(property(EcgDistributionQuota.direction().title().fromAlias("e")))
                .createStatement(getSession()).list();

//        // небольшие проверки консистентности данных
//        if (directionList.isEmpty())
//            throw new RuntimeException("DirectionList is empty for distribution: " + distribution.getId());
//
//        if (directionList.size() != new HashSet<EcgDistributionQuota>(directionList).size())
//            throw new RuntimeException("DirectionList contains dublicates for distribution: " + distribution.getId());

        return directionList;
    }

    public static List<EcgDistributionQuota> getDistributionDirectionQuotaList(EcgDistribution distribution, Session session)
    {
        List<EcgDistributionQuota> directionList = new DQLSelectBuilder().fromEntity(EcgDistributionQuota.class, "e")
                .where(eq(property(EcgDistributionQuota.distribution().fromAlias("e")), value(distribution)))
                .order(property(EcgDistributionQuota.direction().title().fromAlias("e")))
                .createStatement(session).list();

        // небольшие проверки консистентности данных

        if (directionList.isEmpty())
            throw new RuntimeException("DirectionList is empty for distribution: " + distribution.getId());

        if (directionList.size() != new HashSet<>(directionList).size())
            throw new RuntimeException("DirectionList contains dublicates for distribution: " + distribution.getId());

        return directionList;
    }
}
