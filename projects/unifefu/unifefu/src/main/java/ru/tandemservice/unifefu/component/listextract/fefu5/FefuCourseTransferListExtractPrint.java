/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu5;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.unifefu.entity.FefuCourseTransferStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class FefuCourseTransferListExtractPrint implements IPrintFormCreator<FefuCourseTransferStuListExtract>, IListParagraphPrintFormCreator<FefuCourseTransferStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuCourseTransferStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.put("courseOld", extract.getCourse().getTitle())
                .put("courseNew", extract.getCourseNew().getTitle());

        if(DevelopFormCodes.FULL_TIME_FORM.equals(extract.getGroup().getEducationOrgUnit().getDevelopForm().getCode()))
            modifier.put("fefuGroupOld", " группы " + extract.getGroup().getTitle());
        else
            modifier.put("fefuGroupOld", "");

        if(DevelopFormCodes.FULL_TIME_FORM.equals(extract.getGroupNew().getEducationOrgUnit().getDevelopForm().getCode()))
            modifier.put("fefuGroupNew", " в группу " + extract.getGroupNew().getTitle());
        else
            modifier.put("fefuGroupNew", "");

        String educationStrDirection;

        EducationLevelsHighSchool educationLevelsHighSchool = extract.getGroup().getEducationOrgUnit().getEducationLevelHighSchool();
        StructureEducationLevels levelType = educationLevelsHighSchool.getEducationLevel().getLevelType();

        if (levelType.isSpecialization()) // специализация
        {
            educationStrDirection = "специальности ";
        }
        else if (levelType.isSpecialty()) // специальность
        {
            educationStrDirection = "специальности ";
        }
        else if (levelType.isBachelor() && levelType.isProfile()) // бакалаврский профиль
        {
            educationStrDirection = "направлению ";
        }
        else if (levelType.isMaster() && levelType.isProfile()) // магистерский профиль
        {
            educationStrDirection = "направлению ";
        }
        else // направление подготовки
        {
            educationStrDirection = "направлению подготовки ";
        }

        String direction = CommonExtractPrint.getFefuHighEduLevelStr(educationLevelsHighSchool.getEducationLevel());

        modifier.put("fefuEducationStrDirectionOld_D", educationStrDirection + direction);


        EducationLevels educationLevelsOld = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
        CommonExtractPrint.modifyEducationStr(modifier, educationLevelsOld, "Old");

        CommonExtractPrint.initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(), "orgUnit", "");
        CommonExtractPrint.initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "Old");
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, extract.getGroup().getEducationOrgUnit(), CommonListOrderPrint.getEducationBaseText(extract.getGroup()), "fefuShortFastExtendedOptionalText");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuCourseTransferStuListExtract firstExtract)
    {

    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuCourseTransferStuListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuCourseTransferStuListExtract firstExtract)
    {
        return null;
    }
}
