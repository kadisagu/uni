/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unifefu.ws.blackboard.context.gen.RegisterToolResultVO;

/**
 * @author Nikolay Fedorovskih
 * @since 27.02.2014
 */
public class BBConnector
{
    public static final long SESSION_KEEP_ALIVE = 24 * 60 * 60; // Дольше суток сессия жить не может. Хотя по факту умирает через 5 минут.
    private static final String SHARED_SECRET = "sdlfkja;lskdjfalk453iurtq12341zzz"; // Это по сути нужно только для регистрации Proxy Tool и дальше никогда не используется

    public static long getContextWSVersion()
    {
        return BBContextHelper.getInstance().doContextRequest((portType, of) -> portType.getServerVersion(null).getVersion());
    }

    public static long getCourseWSVersion()
    {
        return BBContextHelper.getInstance().doCourseRequest((portType, of) -> {
            if (!portType.initializeCourseWS(false))
                throw new ApplicationException("FAIL initializeCourseWS - unknown error.");
            return portType.getServerVersion(null).getVersion();
        });
    }

    public static long getUserWSVersion()
    {
        return BBContextHelper.getInstance().doUserRequest((portType, of) -> {
            if (!portType.initializeUserWS(false))
                throw new ApplicationException("FAIL initializeUserWS - unknown error.");
            return portType.getServerVersion(null).getVersion();
        });
    }

    public static long getGradebookWSVersion()
    {
        return BBContextHelper.getInstance().doGradebookRequest((portType, of) -> {
            if (!portType.initializeGradebookWS(false))
                throw new ApplicationException("FAIL initializeGradebookWS - unknown error.");
            return portType.getServerVersion(null).getVersion();
        });
    }

    public static long getCourseMembershipWSVersion()
    {
        return BBContextHelper.getInstance().doMembershipRequest((portType, of) -> {
            if (!portType.initializeCourseMembershipWS(false))
                throw new ApplicationException("FAIL initializeCourseMembershipWS - unknown error.");
            return portType.getServerVersion(null).getVersion();
        });
    }

    public static String getSystemInstallationId()
    {
        return BBContextHelper.getInstance().doContextRequest((portType, of) -> portType.getSystemInstallationId().getReturn().getValue());
    }

    public static boolean login()
    {
        return BBContextHelper.getInstance().doContextRequest((portType, of) -> {
            BBSettingsVO settingsVO = BBSettingsVO.getCachedVO();
            return portType.loginTool(settingsVO.getClientPassword(), settingsVO.getClientVendorId(), settingsVO.getClientProgramId(), "", SESSION_KEEP_ALIVE);
        });
    }

    public static boolean emulateUser()
    {
        return BBContextHelper.getInstance().doContextRequest((portType, of) -> portType.emulateUser(BBSettingsVO.getCachedVO().getEmulationUser()));
    }

    public static RegisterToolResultVO registerProxy(final BBSettingsVO settingsVO)
    {
        return BBContextHelper.getInstance().doContextRequest((portType, of) -> portType.registerTool(
                settingsVO.getClientVendorId(),
                settingsVO.getClientProgramId(),
                settingsVO.getRegistrationPassword(),
                settingsVO.getDescription(),
                SHARED_SECRET,
                settingsVO.getRequiredToolMethods(),
                settingsVO.getRequiredTicketMethods()
        ));
    }
}