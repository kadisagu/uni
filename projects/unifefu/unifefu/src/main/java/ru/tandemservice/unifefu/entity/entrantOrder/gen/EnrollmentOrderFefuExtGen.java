package ru.tandemservice.unifefu.entity.entrantOrder.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.entity.catalog.FefuEnrollmentStep;
import ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приказ о зачислении абитуриентов (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentOrderFefuExtGen extends EntityBase
 implements INaturalIdentifiable<EnrollmentOrderFefuExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt";
    public static final String ENTITY_NAME = "enrollmentOrderFefuExt";
    public static final int VERSION_HASH = 1555773581;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_ORDER = "enrollmentOrder";
    public static final String L_ENROLLMENT_STEP = "enrollmentStep";
    public static final String P_RESOLUTION_DATE = "resolutionDate";
    public static final String P_RESOLUTION_NUMBER = "resolutionNumber";

    private EnrollmentOrder _enrollmentOrder;     // Приказ о зачислении абитуриентов
    private FefuEnrollmentStep _enrollmentStep;     // Этапы зачисления
    private Date _resolutionDate;     // Дата решения приемной комиссии
    private String _resolutionNumber;     // Номер решения приемной комиссии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentOrder getEnrollmentOrder()
    {
        return _enrollmentOrder;
    }

    /**
     * @param enrollmentOrder Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrollmentOrder(EnrollmentOrder enrollmentOrder)
    {
        dirty(_enrollmentOrder, enrollmentOrder);
        _enrollmentOrder = enrollmentOrder;
    }

    /**
     * @return Этапы зачисления.
     */
    public FefuEnrollmentStep getEnrollmentStep()
    {
        return _enrollmentStep;
    }

    /**
     * @param enrollmentStep Этапы зачисления.
     */
    public void setEnrollmentStep(FefuEnrollmentStep enrollmentStep)
    {
        dirty(_enrollmentStep, enrollmentStep);
        _enrollmentStep = enrollmentStep;
    }

    /**
     * @return Дата решения приемной комиссии. Свойство не может быть null.
     */
    @NotNull
    public Date getResolutionDate()
    {
        return _resolutionDate;
    }

    /**
     * @param resolutionDate Дата решения приемной комиссии. Свойство не может быть null.
     */
    public void setResolutionDate(Date resolutionDate)
    {
        dirty(_resolutionDate, resolutionDate);
        _resolutionDate = resolutionDate;
    }

    /**
     * @return Номер решения приемной комиссии.
     */
    @Length(max=255)
    public String getResolutionNumber()
    {
        return _resolutionNumber;
    }

    /**
     * @param resolutionNumber Номер решения приемной комиссии.
     */
    public void setResolutionNumber(String resolutionNumber)
    {
        dirty(_resolutionNumber, resolutionNumber);
        _resolutionNumber = resolutionNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentOrderFefuExtGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrollmentOrder(((EnrollmentOrderFefuExt)another).getEnrollmentOrder());
            }
            setEnrollmentStep(((EnrollmentOrderFefuExt)another).getEnrollmentStep());
            setResolutionDate(((EnrollmentOrderFefuExt)another).getResolutionDate());
            setResolutionNumber(((EnrollmentOrderFefuExt)another).getResolutionNumber());
        }
    }

    public INaturalId<EnrollmentOrderFefuExtGen> getNaturalId()
    {
        return new NaturalId(getEnrollmentOrder());
    }

    public static class NaturalId extends NaturalIdBase<EnrollmentOrderFefuExtGen>
    {
        private static final String PROXY_NAME = "EnrollmentOrderFefuExtNaturalProxy";

        private Long _enrollmentOrder;

        public NaturalId()
        {}

        public NaturalId(EnrollmentOrder enrollmentOrder)
        {
            _enrollmentOrder = ((IEntity) enrollmentOrder).getId();
        }

        public Long getEnrollmentOrder()
        {
            return _enrollmentOrder;
        }

        public void setEnrollmentOrder(Long enrollmentOrder)
        {
            _enrollmentOrder = enrollmentOrder;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrollmentOrderFefuExtGen.NaturalId) ) return false;

            EnrollmentOrderFefuExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrollmentOrder(), that.getEnrollmentOrder()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrollmentOrder());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrollmentOrder());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentOrderFefuExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentOrderFefuExt.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentOrderFefuExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentOrder":
                    return obj.getEnrollmentOrder();
                case "enrollmentStep":
                    return obj.getEnrollmentStep();
                case "resolutionDate":
                    return obj.getResolutionDate();
                case "resolutionNumber":
                    return obj.getResolutionNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentOrder":
                    obj.setEnrollmentOrder((EnrollmentOrder) value);
                    return;
                case "enrollmentStep":
                    obj.setEnrollmentStep((FefuEnrollmentStep) value);
                    return;
                case "resolutionDate":
                    obj.setResolutionDate((Date) value);
                    return;
                case "resolutionNumber":
                    obj.setResolutionNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentOrder":
                        return true;
                case "enrollmentStep":
                        return true;
                case "resolutionDate":
                        return true;
                case "resolutionNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentOrder":
                    return true;
                case "enrollmentStep":
                    return true;
                case "resolutionDate":
                    return true;
                case "resolutionNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentOrder":
                    return EnrollmentOrder.class;
                case "enrollmentStep":
                    return FefuEnrollmentStep.class;
                case "resolutionDate":
                    return Date.class;
                case "resolutionNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentOrderFefuExt> _dslPath = new Path<EnrollmentOrderFefuExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentOrderFefuExt");
    }
            

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt#getEnrollmentOrder()
     */
    public static EnrollmentOrder.Path<EnrollmentOrder> enrollmentOrder()
    {
        return _dslPath.enrollmentOrder();
    }

    /**
     * @return Этапы зачисления.
     * @see ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt#getEnrollmentStep()
     */
    public static FefuEnrollmentStep.Path<FefuEnrollmentStep> enrollmentStep()
    {
        return _dslPath.enrollmentStep();
    }

    /**
     * @return Дата решения приемной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt#getResolutionDate()
     */
    public static PropertyPath<Date> resolutionDate()
    {
        return _dslPath.resolutionDate();
    }

    /**
     * @return Номер решения приемной комиссии.
     * @see ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt#getResolutionNumber()
     */
    public static PropertyPath<String> resolutionNumber()
    {
        return _dslPath.resolutionNumber();
    }

    public static class Path<E extends EnrollmentOrderFefuExt> extends EntityPath<E>
    {
        private EnrollmentOrder.Path<EnrollmentOrder> _enrollmentOrder;
        private FefuEnrollmentStep.Path<FefuEnrollmentStep> _enrollmentStep;
        private PropertyPath<Date> _resolutionDate;
        private PropertyPath<String> _resolutionNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt#getEnrollmentOrder()
     */
        public EnrollmentOrder.Path<EnrollmentOrder> enrollmentOrder()
        {
            if(_enrollmentOrder == null )
                _enrollmentOrder = new EnrollmentOrder.Path<EnrollmentOrder>(L_ENROLLMENT_ORDER, this);
            return _enrollmentOrder;
        }

    /**
     * @return Этапы зачисления.
     * @see ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt#getEnrollmentStep()
     */
        public FefuEnrollmentStep.Path<FefuEnrollmentStep> enrollmentStep()
        {
            if(_enrollmentStep == null )
                _enrollmentStep = new FefuEnrollmentStep.Path<FefuEnrollmentStep>(L_ENROLLMENT_STEP, this);
            return _enrollmentStep;
        }

    /**
     * @return Дата решения приемной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt#getResolutionDate()
     */
        public PropertyPath<Date> resolutionDate()
        {
            if(_resolutionDate == null )
                _resolutionDate = new PropertyPath<Date>(EnrollmentOrderFefuExtGen.P_RESOLUTION_DATE, this);
            return _resolutionDate;
        }

    /**
     * @return Номер решения приемной комиссии.
     * @see ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt#getResolutionNumber()
     */
        public PropertyPath<String> resolutionNumber()
        {
            if(_resolutionNumber == null )
                _resolutionNumber = new PropertyPath<String>(EnrollmentOrderFefuExtGen.P_RESOLUTION_NUMBER, this);
            return _resolutionNumber;
        }

        public Class getEntityClass()
        {
            return EnrollmentOrderFefuExt.class;
        }

        public String getEntityName()
        {
            return "enrollmentOrderFefuExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
