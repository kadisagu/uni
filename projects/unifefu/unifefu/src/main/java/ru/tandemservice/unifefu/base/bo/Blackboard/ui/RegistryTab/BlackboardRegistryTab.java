/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.RegistryTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.BBCourseDSHandler;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;

import java.util.Arrays;

/**
 * @author Nikolay Fedorovskih
 * @since 04.03.2014
 */
@Configuration
public class BlackboardRegistryTab extends BusinessComponentManager
{
    public static final String COURSE_DS = "courseDS";
    public static final String PPS_ENTRY_DS = "ppsEntryDS";
    public static final String YES_DS = "yesDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(COURSE_DS, getCourseDS(), courseDSHandler()))
                .addDataSource(selectDS(PPS_ENTRY_DS, ppsEntryDSHandler()).addColumn("fio", PpsEntry.title().s()))
                .addDataSource(selectDS(YES_DS, yesDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getCourseDS()
    {
        return columnListExtPointBuilder(COURSE_DS)
                .addColumn(textColumn(BbCourse.P_TITLE, BbCourse.P_TITLE).order())
                .addColumn(textColumn(BbCourse.P_BB_COURSE_ID, BbCourse.P_BB_COURSE_ID).order())
                .addColumn(textColumn(BbCourse.P_ARCHIVE, BbCourse.P_ARCHIVE).formatter(YesNoFormatter.INSTANCE))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("delete.alert", BbCourse.P_BB_COURSE_ID)))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> ppsEntryDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PpsEntry.class)
                .filter(PpsEntry.person().identityCard().fullFio())
                .order(PpsEntry.person().identityCard().lastName())
                .order(PpsEntry.person().identityCard().firstName())
                .order(PpsEntry.person().identityCard().middleName())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> yesDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(BBCourseDSHandler.YES_ITEM));
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return new BBCourseDSHandler(getName(), false);
    }
}