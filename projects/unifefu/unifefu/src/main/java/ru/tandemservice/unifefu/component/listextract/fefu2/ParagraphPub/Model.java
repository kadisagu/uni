/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu2.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubModel;
import ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 15.03.2013
 */
public class Model extends AbstractListParagraphPubModel<FefuAdmitToStateExamsStuListExtract>
{
}