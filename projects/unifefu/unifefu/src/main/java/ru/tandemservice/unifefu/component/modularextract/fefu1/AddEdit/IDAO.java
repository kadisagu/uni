package ru.tandemservice.unifefu.component.modularextract.fefu1.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/17/12
 * Time: 4:32 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuChangeDateStateExaminationStuExtract, Model>
{
}
