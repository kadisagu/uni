/* $Id$ */
package ru.tandemservice.unifefu.component.group.FefuGroupPub;

import ru.tandemservice.unifefu.utils.NsiIdWrapper;

/**
 * @author Dmitry Seleznev
 * @since 11.04.2014
 */
public class Model extends ru.tandemservice.uni.component.group.GroupPub.Model
{
    private NsiIdWrapper _nsiIdWrapper;

    public NsiIdWrapper getNsiIdWrapper()
    {
        return _nsiIdWrapper;
    }

    public void setNsiIdWrapper(NsiIdWrapper nsiIdWrapper)
    {
        _nsiIdWrapper = nsiIdWrapper;
    }
}