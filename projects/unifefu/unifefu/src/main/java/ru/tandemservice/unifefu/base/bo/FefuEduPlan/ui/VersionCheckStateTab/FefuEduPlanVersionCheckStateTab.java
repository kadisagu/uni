/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.VersionCheckStateTab;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Alexander Zhebko
 * @since 06.12.2013
 */
@Configuration
public class FefuEduPlanVersionCheckStateTab extends BusinessComponentManager
{
}