/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.VersionCheckTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionCheckResults;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 03.12.2014
 */
@Configuration
public class FefuEduPlanVersionCheckTab extends BusinessComponentManager
{
    public static final String CHECK_RESULTS_DS = "checkResultsDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(CHECK_RESULTS_DS, checkResultsDS(), checkResultsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint checkResultsDS()
    {
        IMergeRowIdResolver mergerDate = entity -> {
            FefuEduPlanVersionCheckResults result = (FefuEduPlanVersionCheckResults) entity;
            return result.getCheckTime().toString();
        };

        IMergeRowIdResolver mergerBlock = entity -> {
            FefuEduPlanVersionCheckResults result = (FefuEduPlanVersionCheckResults) entity;
            return result.getCheckTime().toString() + result.getBlock().toString();
        };

        return columnListExtPointBuilder(CHECK_RESULTS_DS)
                .addColumn(dateColumn(FefuEduPlanVersionCheckResults.P_CHECK_TIME, FefuEduPlanVersionCheckResults.checkTime()).merger(mergerDate).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME))
                .addColumn(textColumn(FefuEduPlanVersionCheckResults.L_BLOCK, FefuEduPlanVersionCheckResults.block().title()).merger(mergerBlock))
                .addColumn(textColumn(FefuEduPlanVersionCheckResults.P_MESSAGE, FefuEduPlanVersionCheckResults.message()))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("checkResultsDS.delete.alert", FefuEduPlanVersionCheckResults.P_MESSAGE)).permissionKey("deleteRow_fefuEpvCheckTab"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> checkResultsDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long versionId = context.get(FefuEduPlanVersionCheckTabUI.EDU_PLAN_VERSION_ID);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuEduPlanVersionCheckResults.class, "r")
                        .where(eq(property("r", FefuEduPlanVersionCheckResults.block().eduPlanVersion().id()), value(versionId)))
                        .order(property("r", FefuEduPlanVersionCheckResults.checkTime()), OrderDirection.desc)
                        .order(property("r", FefuEduPlanVersionCheckResults.id()));

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }
}
