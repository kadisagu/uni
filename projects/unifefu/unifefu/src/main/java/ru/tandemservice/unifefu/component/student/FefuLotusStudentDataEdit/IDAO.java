package ru.tandemservice.unifefu.component.student.FefuLotusStudentDataEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author amakarova
 * @since 14.10.2013
 */
public interface IDAO extends IUniDao<Model>
{
}