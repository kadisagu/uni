/* $Id$ */
package ru.tandemservice.unifefu.component.student.FefuLotusStudentIDEdit;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.StudentFefuExt;


/**
 * @author Ekaterina Zvereva
 * @since 11.08.2014
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
       model.setStudentFefuExt(get(StudentFefuExt.class, StudentFefuExt.L_STUDENT, model.getStudentId()));
        if (null == model.getStudentFefuExt())
        {
            model.setStudentFefuExt(new StudentFefuExt());
            model.getStudentFefuExt().setStudent(this.getNotNull(Student.class, model.getStudentId()));
        }
        else
        {
            model.setStudentLotusID(model.getStudentFefuExt().getIntegrationId());
        }
    }

    @Override
    public void update(Model model)
    {
        StudentFefuExt studentFefuExt = model.getStudentFefuExt();
        studentFefuExt.setIntegrationId(model.getStudentLotusID());
        saveOrUpdate(studentFefuExt);
    }
}