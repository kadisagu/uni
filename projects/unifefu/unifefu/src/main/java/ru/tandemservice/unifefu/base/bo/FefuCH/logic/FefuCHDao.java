/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuCH.logic;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.integralCH.entity.RequestAnswer;
import ru.tandemservice.unifefu.ws.integralCH.entity.RequestData;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;
import ru.tandemservice.unispp.base.entity.SppScheduleWeekRow;
import ru.tandemservice.unispp.util.SppScheduleUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 13.03.2015
 */
public class FefuCHDao extends UniBaseDao implements IFefuCHDao
{
    public static String emptyGuid="00000000-0000-0000-0000-000000000000";

    @Override
    public List<RequestAnswer> getScheduleData(RequestData data)
    {
        Date date = data.getDate().toGregorianCalendar().getTime();
        int dayOfWeek = SppScheduleUtil.getDayOfWeek(date);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        int time = hour * 60 + minutes;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "scp")
                .joinEntity("scp", DQLJoinType.inner, SppScheduleWeekRow.class,"swr", eq(property("swr.id"), property("scp", SppSchedulePeriod.weekRow())))
                .joinEntity("swr", DQLJoinType.inner, SppSchedule.class, "sch", eq(property("sch.id"), property("swr", SppScheduleWeekRow.schedule())))
                .column(property("scp"))
                .column(property("sch", SppSchedule.group()))
                .where(or(eq(property("scp", SppSchedulePeriod.lectureRoom()), value(data.getPlaceTitle())),
                          (eq(property("scp", SppSchedulePeriod.lectureRoomVal().title()), value(data.getPlaceTitle())))))
                .where(eq(property("scp", SppSchedulePeriod.dayOfWeek()), value(dayOfWeek)))
                .where(eq(property("sch", SppSchedule.approved()), value(Boolean.TRUE)))
                .where(eq(property("sch", SppSchedule.archived()), value(Boolean.FALSE)))
                .where(between(value(date, PropertyType.DATE), property("sch", SppSchedule.season().startDate()), property("sch", SppSchedule.season().endDate())))
                .where(between(value(time), property("swr", SppScheduleWeekRow.bell().startTime()), property("swr", SppScheduleWeekRow.bell().endTime())))
                .order(property("scp", SppSchedulePeriod.id()));

        List<Object[]> result = builder.createStatement(getSession()).list();
        List<RequestAnswer> answer = new ArrayList<>(result.size());

        if (result != null && !result.isEmpty())
        {

            for (Object[] item : result)
            {
                SppSchedulePeriod period = (SppSchedulePeriod) item[0];
                Group group = (Group) item[1];
                RequestAnswer itemAnswer = new RequestAnswer();
                itemAnswer.setDiscipline(period.getSubject());

                itemAnswer.setEduLevel(group.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getTitle());
                FefuNsiIds nsiIds =IFefuNsiSyncDAO.instance.get().getNsiId(group.getCourse().getId());
                itemAnswer.setCourse(nsiIds == null ? emptyGuid : nsiIds.getGuid());

                nsiIds = IFefuNsiSyncDAO.instance.get().getNsiId(group.getEducationOrgUnit().getFormativeOrgUnit().getId());
                itemAnswer.setSchool(nsiIds == null ? emptyGuid : nsiIds.getGuid());

                nsiIds = IFefuNsiSyncDAO.instance.get().getNsiId(group.getEducationOrgUnit().getEducationLevelHighSchool().getId());
                itemAnswer.setSpecialty(nsiIds == null ? emptyGuid : nsiIds.getGuid());
                if (period.getTeacherVal() != null)
                {
                    nsiIds = IFefuNsiSyncDAO.instance.get().getNsiId(period.getTeacherVal().getOrgUnit().getId());
                    itemAnswer.setCathedra(nsiIds == null ? emptyGuid : nsiIds.getGuid());

                    nsiIds = IFefuNsiSyncDAO.instance.get().getNsiId(period.getTeacherVal().getId());
                    itemAnswer.setLecturer(nsiIds == null ? emptyGuid : nsiIds.getGuid());
                }
                else
                {
                    itemAnswer.setLecturer(emptyGuid);
                    itemAnswer.setCathedra(emptyGuid);
                }
                itemAnswer.setTerm(getTermString(calendar));

                answer.add(itemAnswer);
            }
        }
        return answer;
    }

    private String getTermString(Calendar calendar)
    {
        StringBuilder term = new StringBuilder();
        if (Calendar.SEPTEMBER <= calendar.get(Calendar.MONTH) && Calendar.DECEMBER >= calendar.get(Calendar.MONTH))
            term.append("Осенний семестр ").append(calendar.get(Calendar.YEAR)).append("/").append(calendar.get(Calendar.YEAR) + 1)
            .append(" учебного года");

        else if (Calendar.JANUARY <= calendar.get(Calendar.MONTH) && Calendar.JUNE >= calendar.get(Calendar.MONTH))
            term.append("Весенний семестр ").append(calendar.get(Calendar.YEAR) - 1).append("/").append(calendar.get(Calendar.YEAR))
                    .append(" учебного года");

        return term.toString();
    }
}