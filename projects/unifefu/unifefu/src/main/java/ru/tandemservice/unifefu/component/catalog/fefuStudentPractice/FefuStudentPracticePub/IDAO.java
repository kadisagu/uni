/* $Id$ */
package ru.tandemservice.unifefu.component.catalog.fefuStudentPractice.FefuStudentPracticePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice;

/**
 * @author Nikolay Fedorovskih
 * @since 25.06.2013
 */
public interface IDAO extends IDefaultCatalogPubDAO<FefuStudentPractice, Model>
{
}