/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.validator;

import java.util.regex.Pattern;

/**
 * Валидатор строки: проверяет, что значение соответствует заданному регулярным выражением шаблону.
 * @author Alexander Zhebko
 * @since 26.07.2013
 */
public class RegularExpressionValidator extends Validator<String>
{
    private Pattern pattern;

    public RegularExpressionValidator(String pattern)
    {
        this.pattern = Pattern.compile(pattern);
    }

    @Override
    protected boolean validateValue(String value)
    {
        return pattern.matcher(value).find();
    }

    @Override
    public String getInvalidMessage()
    {
        return "Неизвестный формат.";
    }
}