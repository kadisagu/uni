package ru.tandemservice.unifefu.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifefu.entity.report.gen.FefuListTrJournalDisciplinesReportGen;

/**
 * Отчет «Перечень реализаций (рейтинг-планов) дисциплин»
 */
public class FefuListTrJournalDisciplinesReport extends FefuListTrJournalDisciplinesReportGen
{
    @Override
    @EntityDSLSupport(parts = FefuListTrJournalDisciplinesReport.P_FORMING_DATE)
    public String getFormingDateStr()
    {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(getFormingDate());
    }
}