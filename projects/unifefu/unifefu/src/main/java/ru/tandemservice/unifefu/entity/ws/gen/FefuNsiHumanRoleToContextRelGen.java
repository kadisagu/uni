package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.sec.entity.IRoleAssignment;
import org.tandemframework.shared.organization.sec.entity.gen.IRoleAssignmentGen;
import ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRole;
import ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRoleToContextRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь роли физического лица НСИ с контекстом пользователя ОБ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuNsiHumanRoleToContextRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRoleToContextRel";
    public static final String ENTITY_NAME = "fefuNsiHumanRoleToContextRel";
    public static final int VERSION_HASH = 2103039494;
    private static IEntityMeta ENTITY_META;

    public static final String L_HUMAN_ROLE = "humanRole";
    public static final String L_ROLE_ASSIGNMENT = "roleAssignment";

    private FefuNsiHumanRole _humanRole;     // Роль физического лица НСИ
    private IRoleAssignment _roleAssignment;     // Связанная роль

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Роль физического лица НСИ. Свойство не может быть null.
     */
    @NotNull
    public FefuNsiHumanRole getHumanRole()
    {
        return _humanRole;
    }

    /**
     * @param humanRole Роль физического лица НСИ. Свойство не может быть null.
     */
    public void setHumanRole(FefuNsiHumanRole humanRole)
    {
        dirty(_humanRole, humanRole);
        _humanRole = humanRole;
    }

    /**
     * @return Связанная роль. Свойство не может быть null.
     */
    @NotNull
    public IRoleAssignment getRoleAssignment()
    {
        return _roleAssignment;
    }

    /**
     * @param roleAssignment Связанная роль. Свойство не может быть null.
     */
    public void setRoleAssignment(IRoleAssignment roleAssignment)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && roleAssignment!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IRoleAssignment.class);
            IEntityMeta actual =  roleAssignment instanceof IEntity ? EntityRuntime.getMeta((IEntity) roleAssignment) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_roleAssignment, roleAssignment);
        _roleAssignment = roleAssignment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuNsiHumanRoleToContextRelGen)
        {
            setHumanRole(((FefuNsiHumanRoleToContextRel)another).getHumanRole());
            setRoleAssignment(((FefuNsiHumanRoleToContextRel)another).getRoleAssignment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuNsiHumanRoleToContextRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuNsiHumanRoleToContextRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuNsiHumanRoleToContextRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "humanRole":
                    return obj.getHumanRole();
                case "roleAssignment":
                    return obj.getRoleAssignment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "humanRole":
                    obj.setHumanRole((FefuNsiHumanRole) value);
                    return;
                case "roleAssignment":
                    obj.setRoleAssignment((IRoleAssignment) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "humanRole":
                        return true;
                case "roleAssignment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "humanRole":
                    return true;
                case "roleAssignment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "humanRole":
                    return FefuNsiHumanRole.class;
                case "roleAssignment":
                    return IRoleAssignment.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuNsiHumanRoleToContextRel> _dslPath = new Path<FefuNsiHumanRoleToContextRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuNsiHumanRoleToContextRel");
    }
            

    /**
     * @return Роль физического лица НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRoleToContextRel#getHumanRole()
     */
    public static FefuNsiHumanRole.Path<FefuNsiHumanRole> humanRole()
    {
        return _dslPath.humanRole();
    }

    /**
     * @return Связанная роль. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRoleToContextRel#getRoleAssignment()
     */
    public static IRoleAssignmentGen.Path<IRoleAssignment> roleAssignment()
    {
        return _dslPath.roleAssignment();
    }

    public static class Path<E extends FefuNsiHumanRoleToContextRel> extends EntityPath<E>
    {
        private FefuNsiHumanRole.Path<FefuNsiHumanRole> _humanRole;
        private IRoleAssignmentGen.Path<IRoleAssignment> _roleAssignment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Роль физического лица НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRoleToContextRel#getHumanRole()
     */
        public FefuNsiHumanRole.Path<FefuNsiHumanRole> humanRole()
        {
            if(_humanRole == null )
                _humanRole = new FefuNsiHumanRole.Path<FefuNsiHumanRole>(L_HUMAN_ROLE, this);
            return _humanRole;
        }

    /**
     * @return Связанная роль. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRoleToContextRel#getRoleAssignment()
     */
        public IRoleAssignmentGen.Path<IRoleAssignment> roleAssignment()
        {
            if(_roleAssignment == null )
                _roleAssignment = new IRoleAssignmentGen.Path<IRoleAssignment>(L_ROLE_ASSIGNMENT, this);
            return _roleAssignment;
        }

        public Class getEntityClass()
        {
            return FefuNsiHumanRoleToContextRel.class;
        }

        public String getEntityName()
        {
            return "fefuNsiHumanRoleToContextRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
