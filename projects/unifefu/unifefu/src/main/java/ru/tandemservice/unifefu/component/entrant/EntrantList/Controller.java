/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.EntrantList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantList.Controller
{
    public void onClickAddForeignEntrantWizard(IBusinessComponent component)
    {
        EnrollmentCampaign lastEnrollmentCampaign = UniecDAOFacade.getEntrantDAO().getLastEnrollmentCampaign();
        if (lastEnrollmentCampaign == null)
        {
            return;
        }
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.unifefu.component.wizard.FefuForeignOnlineEntrantSearchStep"));
    }
}
