/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public class Model extends AbstractListExtractPubModel<FefuOrderContingentStuDPOListExtract>
{
    private FefuAdditionalProfessionalEducationProgram _dpoProgram;

    public FefuAdditionalProfessionalEducationProgram getDpoProgram()
    {
        return _dpoProgram;
    }

    public void setDpoProgram(FefuAdditionalProfessionalEducationProgram dpoProgram)
    {
        _dpoProgram = dpoProgram;
    }
}