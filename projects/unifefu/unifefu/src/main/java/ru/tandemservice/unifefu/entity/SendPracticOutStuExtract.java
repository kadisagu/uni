/* $Id$ */
package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IPracticeOutExtract;
import ru.tandemservice.unifefu.entity.gen.SendPracticOutStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О направлении на практику в пределах ОУ
 */
public class SendPracticOutStuExtract extends SendPracticOutStuExtractGen implements IPracticeOutExtract
{
    @Override
    public Date getBeginDate()
    {
        return getPracticeBeginDate();
    }

    @Override
    public Date getEndDate()
    {
        return getPracticeEndDate();
    }

    @Override
    public String getPracticePlace()
    {
        return getPracticeExtOrgUnit() != null ? getPracticeExtOrgUnit().getTitle() : null;
    }
}