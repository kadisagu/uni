/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.FefuEduInstitutionStep;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.unifefu.entity.EntrantFefuExt;

/**
 * @author Nikolay Fedorovskih
 * @since 16.05.2013
 */
public class DAO extends ru.tandemservice.uniec.component.wizard.EduInstitutionStep.DAO
{
    @Override
    protected void prepareOnlineEntrantEduInstitution(ru.tandemservice.uniec.component.wizard.EduInstitutionStep.Model model)
    {
        super.prepareOnlineEntrantEduInstitution(model);
        EntrantFefuExt entrantFefuExt = getByNaturalId(new EntrantFefuExt.NaturalId(model.getEntrant()));
        if (entrantFefuExt != null && !StringUtils.isEmpty(entrantFefuExt.getEduInstitutionStr()))
        {
            ((Model) model).setEduInstitutionStr(entrantFefuExt.getEduInstitutionStr());
        }
        PersonEduInstitution eduInstitution = model.getPersonEduInstitution();
        if (eduInstitution != null && (eduInstitution.getSeriaApplication() == null && eduInstitution.getNumberApplication() == null))
        {
            eduInstitution.setSeriaApplication(eduInstitution.getSeria());
            eduInstitution.setNumberApplication(eduInstitution.getNumber());
        }
    }

    @Override
    public void update(org.tandemframework.shared.person.base.bo.Person.ui.EduInstitutionAddEdit.Model model)
    {
        super.update(model);
        PersonEduInstitution eduInstitution = model.getPersonEduInstitution();
        if (eduInstitution.getSeriaApplication() == null && eduInstitution.getNumberApplication() == null)
        {
            eduInstitution.setSeriaApplication(eduInstitution.getSeria());
            eduInstitution.setNumberApplication(eduInstitution.getNumber());
            update(eduInstitution);
        }

    }
}