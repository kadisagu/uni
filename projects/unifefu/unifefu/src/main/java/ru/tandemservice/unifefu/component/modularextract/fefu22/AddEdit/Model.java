/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu22.AddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public class Model  extends CommonModularStudentExtractAddEditModel<FefuOrderContingentStuDPOExtract>
{
    private ISelectModel _studentCustomStateCIModel;
    private ISelectModel _studentStatusModel;
    private IUploadFile _uploadFile;

    public ISelectModel getStudentCustomStateCIModel()
    {
        return _studentCustomStateCIModel;
    }

    public void setStudentCustomStateCIModel(ISelectModel studentCustomStateCIModel)
    {
        _studentCustomStateCIModel = studentCustomStateCIModel;
    }

    public ISelectModel getStudentStatusModel()
    {
        return _studentStatusModel;
    }

    public void setStudentStatusModel(ISelectModel studentStatusModel)
    {
        _studentStatusModel = studentStatusModel;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

}