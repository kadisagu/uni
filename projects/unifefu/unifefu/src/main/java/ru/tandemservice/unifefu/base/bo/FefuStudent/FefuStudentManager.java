/**
 *$Id: FefuStudentManager.java 38584 2014-10-08 11:05:36Z azhebko $
 */
package ru.tandemservice.unifefu.base.bo.FefuStudent;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuStudent.logic.*;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.archivalList.FefuArchivalStudentSearchListDSHandler;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.list.FefuStudentSearchListDSHandler;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.orgUnitList.FefuOrgUnitStudentSearchListDSHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 30.08.12
 */
@Configuration
public class FefuStudentManager extends BusinessObjectManager
{
    public static final long BASE_VPO = 0L;
    public static final long BASE_SPO = 1L;

    public static final long ARCHIVAL_NO = 0L;
    public static final long ARCHIVAL_YES = 1L;

    public static final long TYPE_MATCH_PARTIAL = 0L;
    public static final long TYPE_MATCH_FULL = 1L;

    public static final String FEFU_ARCHIVAL_DS = "archivalDS";
    public static final String FEFU_ARCHIVAL = "archival";
    public static final String FEFU_TYPE_MATCH_DS = "typeMatchDS";
    public static final String FEFU_TYPE_MATCH = "typeMatch";

    public static final String FEFU_STUDENT_FIO_DS = "fefuStudentListDS";
    public static final String FEFU_STUDENT_FIO = "fefuStudentFio";

    public static final String FEFU_IS_LEARNING_DS = "fefuIsLearningDS";
    public static final String FEFU_LEARN_UVC = "learnUVC";
    public static final String FEFU_LEARN_FVO = "learnFVO";
    public static final long FEFU_IS_LEARNING_YES = 1L;

    public static FefuStudentManager instance()
    {
        return instance(FefuStudentManager.class);
    }

    @Bean
    public IFefuStudentVipDataDao fefuStudentVipDataDao()
    {
        return new FefuStudentVipDataDao();
    }

    @Bean
    public IFefuMoveStudentInjectModifierDao printInjectModifierDao()
    {
        return new FefuMoveStudentInjectModifierDao();
    }

    @Bean
    public IDefaultSearchDataSourceHandler fefuStudentPaymentsDSHandler()
    {
        return new FefuStudentPaymentsDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> archivalDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(ARCHIVAL_YES, "Да")
                .addRecord(ARCHIVAL_NO, "Нет");
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> fefuStudentListDSHandler()
    {
        EntityComboDataSourceHandler handler = new FefuStudentFIOListDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                DataWrapper archival = context.get(FefuStudentManager.FEFU_ARCHIVAL);
                if (null != archival)
                {
                    dql.where(eq(property(alias, Student.archival()), value(archival.getId().equals(FefuStudentManager.ARCHIVAL_YES))));
                }


            }
        };
        return handler.order(Student.person().identityCard().fullFio()).pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentListExtSearchListDSHandler()
    {
        return new FefuStudentSearchListDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentOrgUnitListExtSearchListDSHandler()
    {
        return new FefuOrgUnitStudentSearchListDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentArchivalListSearchListDSHandler()
    {
        return new FefuArchivalStudentSearchListDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> baseEduDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(BASE_VPO, "baseEdu.optionTitle.vpo")
                .addRecord(BASE_SPO, "baseEdu.optionTitle.spo");
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> typeMatchDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
               .addRecord(TYPE_MATCH_PARTIAL, "Частичное совпадение статусов")
                .addRecord(TYPE_MATCH_FULL, "Полное совпадение статусов");
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> yesDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(FEFU_IS_LEARNING_YES, "Да");
    }

    public List<DataWrapper> getBaseEduOptions()
    {
        List<DataWrapper> wrapperList = new ArrayList<>();
        wrapperList.add(getBaseEduOption(BASE_VPO));
        wrapperList.add(getBaseEduOption(BASE_SPO));
        return wrapperList;
    }

    public DataWrapper getBaseEduOption(Long idOption)
    {
        if (idOption.equals(BASE_VPO))
            return new DataWrapper(BASE_VPO, getPropertySafe("baseEdu.optionTitle.vpo"));
        else
            return new DataWrapper(BASE_SPO, getPropertySafe("baseEdu.optionTitle.spo"));
    }

    public String getIdentityCards(Person person)
    {
        List<IdentityCard> icList = DataAccessServices.dao().getList(IdentityCard.class, IdentityCard.person(), person);
        if (icList.size() == 1)
            return person.getFullFio();

        String lastName = person.getIdentityCard().getLastName();
        String firstName = person.getIdentityCard().getFirstName();
        String middleName = person.getIdentityCard().getMiddleName();
        String lastNameStr = "";
        String fioStr = "";
        boolean fioChange = false;

        Collections.sort(icList, (o1, o2) -> o2.getCreationDate().compareTo(o1.getCreationDate()));

        List<IdentityCard> iCardList = new ArrayList<>();

        for (IdentityCard ic : icList)
        {
            if (!firstName.equals(ic.getFirstName()) || (ic.getMiddleName() != null && !ic.getMiddleName().equals(middleName)) || (ic.getMiddleName() == null && middleName != null))
            {
                iCardList.add(ic);
                fioChange = true;
            }
            else if (!lastName.equals(ic.getLastName()))
            {
                iCardList.add(ic);
            }
        }
        for (IdentityCard ic : iCardList)
        {
            if (fioChange)
                fioStr = StringUtils.isEmpty(fioStr) ? ic.getFullFio() : fioStr + "; " + ic.getFullFio();
            else
                lastNameStr = StringUtils.isEmpty(lastNameStr) ? ic.getLastName() : lastNameStr + ", " + ic.getLastName();
        }

        if (StringUtils.isEmpty(fioStr))
            return lastName + (StringUtils.isEmpty(lastNameStr) ? " " : " (" + lastNameStr+ ") ") + firstName + " " + (middleName == null ? "" : middleName);
        else
            return person.getIdentityCard().getFullFio() + " (" + fioStr + ")";
    }
}
