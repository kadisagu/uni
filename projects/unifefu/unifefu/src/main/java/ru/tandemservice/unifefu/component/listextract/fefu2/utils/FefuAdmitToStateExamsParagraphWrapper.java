/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu2.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 18.03.2013
 */
public class FefuAdmitToStateExamsParagraphWrapper implements Comparable<FefuAdmitToStateExamsParagraphWrapper>
{
    private final CompensationType _compensationType;
    private final ListStudentExtract _firstExtract;

    public FefuAdmitToStateExamsParagraphWrapper(CompensationType compensationType, ListStudentExtract firstExtract)
    {
        _compensationType = compensationType;
        _firstExtract = firstExtract;
    }

    private final List<FefuAdmitToStateExamsParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<>();

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public List<FefuAdmitToStateExamsParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FefuAdmitToStateExamsParagraphWrapper that = (FefuAdmitToStateExamsParagraphWrapper) o;

        return _compensationType.equals(that._compensationType);
    }

    @Override
    public int hashCode()
    {
        return _compensationType.hashCode();
    }

    @Override
    public int compareTo(FefuAdmitToStateExamsParagraphWrapper o)
    {
        return _compensationType.getCode().compareTo(o.getCompensationType().getCode());
    }
}