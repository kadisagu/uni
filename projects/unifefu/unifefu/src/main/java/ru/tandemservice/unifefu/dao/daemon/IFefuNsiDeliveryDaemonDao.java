/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 09.02.2015
 */
public interface IFefuNsiDeliveryDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IFefuNsiDeliveryDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IFefuNsiDeliveryDaemonDao> instance = new SpringBeanCache<>(IFefuNsiDeliveryDaemonDao.class.getName());

    /**
     * Возвращает список идентификаторов подготовленных, но не ещё отправленных (впервые, либо по транспортной ошибке) пакетов в НСИ,
     * отсортированных по времени добавления.
     *
     * @return - список идентификаторов неотправленных в НСИ пакетов
     */
    List<Long> getPreparedNSIPackageIds(Long logRowToReDeliver);

    /**
     * Производит отправку подготовленного пакета в НСИ.
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doDeliverPackageToNSI(Long logRowId);
}