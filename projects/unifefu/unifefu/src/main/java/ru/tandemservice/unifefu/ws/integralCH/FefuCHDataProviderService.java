/* $Id$ */
package ru.tandemservice.unifefu.ws.integralCH;

/**
 * @author Ekaterina Zvereva
 * @since 16.03.2015
 */
/* $Id$ */


import ru.tandemservice.unifefu.base.bo.FefuCH.FefuCHManager;
import ru.tandemservice.unifefu.ws.integralCH.entity.RequestAnswer;
import ru.tandemservice.unifefu.ws.integralCH.entity.RequestData;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 13.03.2015
 */
@WebService(serviceName = "fefuCHDataProviderService")
public class FefuCHDataProviderService
{
    @WebMethod(action = "getScheduleData")
    public List<RequestAnswer> getScheduleData(@WebParam(name = "requestData", targetNamespace = "http://integralCH.ws.unifefu.tandemservice.ru/") RequestData requestData)
    {
        return FefuCHManager.instance().dao().getScheduleData(requestData);
    }

}