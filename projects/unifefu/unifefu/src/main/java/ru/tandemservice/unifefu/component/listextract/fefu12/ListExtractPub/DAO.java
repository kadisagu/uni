/* $Id$ */

package ru.tandemservice.unifefu.component.listextract.fefu12.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;


/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public class DAO extends AbstractListExtractPubDAO<FefuStuffCompensationStuListExtract, Model> implements IDAO
{
	@Override
	public void prepare(Model model)
	{
		super.prepare(model);
		model.setCompensationSum(MoneyFormatter.ruMoneyFormatter(2).format(model.getExtract().getCompensationSum()));
		model.setImmediateSum(MoneyFormatter.ruMoneyFormatter(2).format(model.getExtract().getImmediateSum()));
	}
}
