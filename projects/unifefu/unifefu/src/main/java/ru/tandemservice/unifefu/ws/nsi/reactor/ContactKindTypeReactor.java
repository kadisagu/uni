/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiContactType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.ContactKindType;

/**
 * @author Dmitry Seleznev
 * @since 25.08.2013
 */
public class ContactKindTypeReactor extends SimpleCatalogEntityNewReactor<ContactKindType, FefuNsiContactType>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return false;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<FefuNsiContactType> getEntityClass()
    {
        return FefuNsiContactType.class;
    }

    @Override
    public Class<ContactKindType> getNSIEntityClass()
    {
        return ContactKindType.class;
    }

    @Override
    public ContactKindType getCatalogElementRetrieveRequestObject(String guid)
    {
        ContactKindType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createContactKindType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public ContactKindType createEntity(CoreCollectionUtils.Pair<FefuNsiContactType, FefuNsiIds> entityPair)
    {
        ContactKindType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createContactKindType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setContactKindID(getUserCodeForNSIChecked(entityPair.getX().getUserCode()));
        nsiEntity.setContactKindName(entityPair.getX().getTitle());
        nsiEntity.setContactKindType(entityPair.getX().getContactInfoType());
        return nsiEntity;
    }

    @Override
    public FefuNsiContactType createEntity(ContactKindType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getContactKindName()) return null;

        FefuNsiContactType entity = new FefuNsiContactType();
        entity.setTitle(nsiEntity.getContactKindName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(FefuNsiContactType.class));
        entity.setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getContactKindID()));
        entity.setContactInfoType(nsiEntity.getContactKindType());
        return entity;
    }

    @Override
    public FefuNsiContactType updatePossibleDuplicateFields(ContactKindType nsiEntity, CoreCollectionUtils.Pair<FefuNsiContactType, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuNsiContactType.title().s()))
                entityPair.getX().setTitle(nsiEntity.getContactKindName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuNsiContactType.userCode().s()))
                entityPair.getX().setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getContactKindID()));

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuNsiContactType.contactInfoType().s()))
                entityPair.getX().setContactInfoType(nsiEntity.getContactKindType());
            return entityPair.getX();
        }
        return null;
    }

    @Override
    public ContactKindType updateNsiEntityFields(ContactKindType nsiEntity, FefuNsiContactType entity)
    {
        nsiEntity.setContactKindName(entity.getTitle());
        nsiEntity.setContactKindID(getUserCodeForNSIChecked(entity.getUserCode()));
        nsiEntity.setContactKindType(entity.getContactInfoType());
        return nsiEntity;
    }
}