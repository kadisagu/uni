/* $Id$ */
package ru.tandemservice.unifefu.component.order.EnrollmentOrderPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.EnrollmentStepEdit.FefuEcOrderEnrollmentStepEdit;

/**
 * @author Nikolay Fedorovskih
 * @since 31.07.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        ru.tandemservice.uniec.component.order.EnrollmentOrderPub.Model baseModel = component.getModel(component.getName());
        getDao().prepare(getModel(component), baseModel.getOrder());
    }

    public void onClickEditEnrollmentStep(IBusinessComponent component)
    {
        ru.tandemservice.uniec.component.order.EnrollmentOrderPub.Model baseModel = component.getModel(component.getName());
        component.getController().activateInRoot(component, new ComponentActivator(FefuEcOrderEnrollmentStepEdit.class.getSimpleName(), new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, baseModel.getOrder().getId())
        ));
    }

    public void openISBLink(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (null == model.getEnrOrderExt()) return;

        String directumOrderId = model.getEnrOrderExt().getDirectumOrderId();

        StringBuilder linkFile = new StringBuilder("Version=ISB7\nSystemCode=dvfu\nComponentType=8\nID=");
        linkFile.append(null != directumOrderId ? directumOrderId : "").append("\nViewCode=");

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
                .contentType(DatabaseFile.CONTENT_TYPE_SOME_DATA)
                .fileName((null != directumOrderId ? directumOrderId : "document") + ".isb")
                .document(linkFile.toString().getBytes()), true);
    }
}