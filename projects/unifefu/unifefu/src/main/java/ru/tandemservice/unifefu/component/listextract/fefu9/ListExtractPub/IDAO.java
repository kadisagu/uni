/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu9.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuFormativeTransferStuListExtract, Model>
{
}
