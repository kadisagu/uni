/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.LogRegistryList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.NSISync.NSISyncManager;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;

import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 28.01.2015
 */
public class NSISyncLogRegistryListUI extends UIPresenter
{

    /*private Long _nsiCatalogId;
    private FefuNsiCatalogType _nsiCatalog;*/
    private Date _requestDateFrom;
    private Date _requestDateTo;
    private String _messageGuid;
    private IdentifiableWrapper _eventType;
    private IdentifiableWrapper _operationType;
    private IdentifiableWrapper _messageStatus;
    private FefuNsiCatalogType _catalogType;
    private String _entityGuid;
    private String _anyXmlBodyGuid;
    private boolean _filtersVisible = false;

    @Override
    public void onComponentRefresh()
    {
        //TODO _nsiCatalog = DataAccessServices.dao().getNotNull(FefuNsiCatalogType.class, _nsiCatalogId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (NSISyncLogRegistryList.NSI_CATALOG_LOG_DS.equals(dataSource.getName()))
        {
            dataSource.put(NSISyncLogRegistryList.NSI_CATALOG, _catalogType);
            dataSource.put(NSISyncLogRegistryList.REQUEST_DATE_FROM_GUID, _requestDateFrom);
            dataSource.put(NSISyncLogRegistryList.REQUEST_DATE_TO_GUID, _requestDateTo);
            dataSource.put(NSISyncLogRegistryList.MESSAGE_GUID, _messageGuid);
            dataSource.put(NSISyncLogRegistryList.EVENT_TYPE, _eventType);
            dataSource.put(NSISyncLogRegistryList.OPERATION_TYPE, _operationType);
            dataSource.put(NSISyncLogRegistryList.MESSAGE_STATUS, _messageStatus);
            dataSource.put(NSISyncLogRegistryList.ENTITY_GUID, _entityGuid);
            dataSource.put(NSISyncLogRegistryList.ANY_XML_BODY_GUID, _anyXmlBodyGuid);
        }
    }

    /*public void onClickForceDaemonStart()
    {
        FefuNsiDaemonDao.DAEMON.wakeUpDaemon();
    } */

    public String getMessageStatusColored()
    {
        FefuNsiLogRow logRow = _uiConfig.getDataSource(NSISyncLogRegistryList.NSI_CATALOG_LOG_DS).getCurrent();
        return null != logRow ? logRow.getStatusStrColored() : null;
    }

    public void onClickSearch()
    {
        //TODO getSettings().save();
    }

    public void onClickClear()
    {
        _requestDateFrom = null;
        _requestDateTo = null;
        _messageGuid = null;
        _eventType = null;
        _operationType = null;
        _messageStatus = null;
        _entityGuid = null;
        _anyXmlBodyGuid = null;
        _catalogType = null;
        onClickSearch();
    }

    public void onClickShowHideFilters()
    {
        _filtersVisible = !_filtersVisible;
    }

    public void onSendMessageAgain()
    {
        FefuNsiLogRow srcLogRow = DataAccessServices.dao().getNotNull(FefuNsiLogRow.class, getListenerParameterAsLong());

        if (!FefuNsiLogRow.OUT_EVENT_TYPE.equals(srcLogRow.getEventType().toLowerCase()) && -1 == srcLogRow.getMessageStatus())
        {
            NsiReactorUtils.reExecuteOBAction(getListenerParameterAsLong());
        } else
        {
            NsiReactorUtils.executeNSIAction(getListenerParameterAsLong(), true);
        }
    }

    public void onGetMessagePack()
    {
        NSISyncManager.instance().dao().getNsiCatalogLogRowPackFile(getListenerParameterAsLong());
    }

    public Date getRequestDateFrom()
    {
        return _requestDateFrom;
    }

    public void setRequestDateFrom(Date requestDateFrom)
    {
        _requestDateFrom = requestDateFrom;
    }

    public Date getRequestDateTo()
    {
        return _requestDateTo;
    }

    public void setRequestDateTo(Date requestDateTo)
    {
        _requestDateTo = requestDateTo;
    }

    public String getMessageGuid()
    {
        return _messageGuid;
    }

    public void setMessageGuid(String messageGuid)
    {
        _messageGuid = messageGuid;
    }

    public IdentifiableWrapper getEventType()
    {
        return _eventType;
    }

    public void setEventType(IdentifiableWrapper eventType)
    {
        _eventType = eventType;
    }

    public IdentifiableWrapper getOperationType()
    {
        return _operationType;
    }

    public void setOperationType(IdentifiableWrapper operationType)
    {
        _operationType = operationType;
    }

    public IdentifiableWrapper getMessageStatus()
    {
        return _messageStatus;
    }

    public void setMessageStatus(IdentifiableWrapper messageStatus)
    {
        _messageStatus = messageStatus;
    }

    public FefuNsiCatalogType getCatalogType()
    {
        return _catalogType;
    }

    public void setCatalogType(FefuNsiCatalogType catalogType)
    {
        _catalogType = catalogType;
    }

    public String getEntityGuid()
    {
        return _entityGuid;
    }

    public void setEntityGuid(String entityGuid)
    {
        _entityGuid = entityGuid;
    }

    public String getAnyXmlBodyGuid()
    {
        return _anyXmlBodyGuid;
    }

    public void setAnyXmlBodyGuid(String anyXmlBodyGuid)
    {
        _anyXmlBodyGuid = anyXmlBodyGuid;
    }

    public boolean isFiltersVisible()
    {
        return _filtersVisible;
    }

    public void setFiltersVisible(boolean filtersVisible)
    {
        _filtersVisible = filtersVisible;
    }
}