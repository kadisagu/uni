/**
 * EDocumentVersionUpdate.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class EDocumentVersionUpdate  implements java.io.Serializable {
    private java.lang.Integer EDocumentID;

    private java.lang.Integer verNum;

    private java.lang.String editor;

    private byte[] document;

    public EDocumentVersionUpdate() {
    }

    public EDocumentVersionUpdate(
           java.lang.Integer EDocumentID,
           java.lang.Integer verNum,
           java.lang.String editor,
           byte[] document) {
           this.EDocumentID = EDocumentID;
           this.verNum = verNum;
           this.editor = editor;
           this.document = document;
    }


    /**
     * Gets the EDocumentID value for this EDocumentVersionUpdate.
     * 
     * @return EDocumentID
     */
    public java.lang.Integer getEDocumentID() {
        return EDocumentID;
    }


    /**
     * Sets the EDocumentID value for this EDocumentVersionUpdate.
     * 
     * @param EDocumentID
     */
    public void setEDocumentID(java.lang.Integer EDocumentID) {
        this.EDocumentID = EDocumentID;
    }


    /**
     * Gets the verNum value for this EDocumentVersionUpdate.
     * 
     * @return verNum
     */
    public java.lang.Integer getVerNum() {
        return verNum;
    }


    /**
     * Sets the verNum value for this EDocumentVersionUpdate.
     * 
     * @param verNum
     */
    public void setVerNum(java.lang.Integer verNum) {
        this.verNum = verNum;
    }


    /**
     * Gets the editor value for this EDocumentVersionUpdate.
     * 
     * @return editor
     */
    public java.lang.String getEditor() {
        return editor;
    }


    /**
     * Sets the editor value for this EDocumentVersionUpdate.
     * 
     * @param editor
     */
    public void setEditor(java.lang.String editor) {
        this.editor = editor;
    }


    /**
     * Gets the document value for this EDocumentVersionUpdate.
     * 
     * @return document
     */
    public byte[] getDocument() {
        return document;
    }


    /**
     * Sets the document value for this EDocumentVersionUpdate.
     * 
     * @param document
     */
    public void setDocument(byte[] document) {
        this.document = document;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EDocumentVersionUpdate)) return false;
        EDocumentVersionUpdate other = (EDocumentVersionUpdate) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.EDocumentID==null && other.getEDocumentID()==null) || 
             (this.EDocumentID!=null &&
              this.EDocumentID.equals(other.getEDocumentID()))) &&
            ((this.verNum==null && other.getVerNum()==null) || 
             (this.verNum!=null &&
              this.verNum.equals(other.getVerNum()))) &&
            ((this.editor==null && other.getEditor()==null) || 
             (this.editor!=null &&
              this.editor.equals(other.getEditor()))) &&
            ((this.document==null && other.getDocument()==null) || 
             (this.document!=null &&
              java.util.Arrays.equals(this.document, other.getDocument())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEDocumentID() != null) {
            _hashCode += getEDocumentID().hashCode();
        }
        if (getVerNum() != null) {
            _hashCode += getVerNum().hashCode();
        }
        if (getEditor() != null) {
            _hashCode += getEditor().hashCode();
        }
        if (getDocument() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDocument());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDocument(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EDocumentVersionUpdate.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">EDocumentVersionUpdate"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EDocumentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "EDocumentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("verNum");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "VerNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("editor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "Editor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("document");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "Document"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
