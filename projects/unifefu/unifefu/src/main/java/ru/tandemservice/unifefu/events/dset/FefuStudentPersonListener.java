package ru.tandemservice.unifefu.events.dset;

import org.hibernate.Session;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.dao.daemon.FEFUAllStudExportDaemonDAO;
import ru.tandemservice.unifefu.entity.ws.MdbViewPerson;
import ru.tandemservice.unifefu.entity.ws.MdbViewStudent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vnekrasov
 */
public class FefuStudentPersonListener extends ParamTransactionCompleteListener<Boolean>
{

    @SuppressWarnings("unchecked")
    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, Person.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, Person.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, Student.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, Student.class, this);
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        List<Long> personIds = new ArrayList<>();
        List<Long> studentIds = new ArrayList<>();

        for (Long id : params)
        {
            IEntityMeta meta = EntityRuntime.getMeta(id);
            if (meta.getEntityClass().equals(Person.class))
            {
                personIds.add(id);
            }
            else if (meta.getEntityClass().equals(Student.class))
            {
                studentIds.add(id);
            }
        }

        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(MdbViewStudent.class, "s")
                //.joinPath(DQLJoinType.inner, MdbViewStudent.mdbViewPerson().person().fromAlias("s"), "p")
                .column(property(MdbViewStudent.mdbViewPerson().person().id().fromAlias("s")))
                .where(in(property(MdbViewStudent.student().id().fromAlias("s")), studentIds));

        List<Long> additionalIds = eBuilder.createStatement(session).list();
        personIds.addAll(additionalIds);

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(MdbViewPerson.class);
        deleteBuilder.where(in(property(MdbViewPerson.person().id()), personIds));
        deleteBuilder.createStatement(session).execute();

        FEFUAllStudExportDaemonDAO.DAEMON.registerAfterCompleteWakeUp(session);
        return true;
    }

    @Override
    public void afterCompletion(Session session, int status, Collection<Long> params, Boolean beforeCompletionResult)
    {

    }

}
