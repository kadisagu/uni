/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu22.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubController;
import ru.tandemservice.unifefu.entity.FefuPerformConditionCourseTransferListExtract;

/**
 * @author Igor Belanov
 * @since 30.06.2016
 */
public class Controller extends AbstractListParagraphPubController<FefuPerformConditionCourseTransferListExtract, IDAO, Model>
{
}
