/* $Id$ */
package ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.OuSettingsEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.TrOrgUnitSettingsFefuExt;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuSettingsEdit.TrBrsCoefficientOuSettingsEditUI;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;

/**
 * @author Nikolay Fedorovskih
 * @since 14.07.2014
 */
public class TrBrsCoefficientOuSettingsEditClickApply extends NamedUIAction
{
    public TrBrsCoefficientOuSettingsEditClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        final TrOrgUnitSettings ouSettings = ((TrBrsCoefficientOuSettingsEditUI) presenter).getOu();
        TrBrsCoefficientOuSettingsEditExtUI addon = (TrBrsCoefficientOuSettingsEditExtUI) presenter.getConfig().getAddon(TrBrsCoefficientOuSettingsEditExt.ADDON_NAME);
        final TrOrgUnitSettingsFefuExt ouSettingsFefuExt = addon.getOuSettingsExt();
        DataAccessServices.dao().doInTransaction(session -> {
            ouSettingsFefuExt.setTrOrgUnitSettings(ouSettings);
            session.saveOrUpdate(ouSettings);
            session.saveOrUpdate(ouSettingsFefuExt);
            return null;
        });
        presenter.deactivate();
    }
}