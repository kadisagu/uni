//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.03.20 at 04:27:21 PM YEKT 
//


package ru.tandemservice.unifefu.ws.nsi.datagram;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


/**
 * Элемент справочника ЭКК
 * 
 * <p>Java class for EKKType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EKKType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="EKKID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EKKNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EKKActive" type="{http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0}NlBool" minOccurs="0"/>
 *         &lt;element name="HumanLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanMiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanBirthdate" type="{http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0}NlDate" minOccurs="0"/>
 *         &lt;element name="IdentityCardSeries" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdentityCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HumanID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Human" type="{http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0}HumanType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EKKType", propOrder = {

})
public class EKKType {

    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "EKKID")
    protected String ekkid;
    @XmlElement(name = "EKKNumber")
    protected String ekkNumber;
    @XmlElement(name = "EKKActive")
    protected String ekkActive;
    @XmlElement(name = "HumanLastName")
    protected String humanLastName;
    @XmlElement(name = "HumanFirstName")
    protected String humanFirstName;
    @XmlElement(name = "HumanMiddleName")
    protected String humanMiddleName;
    @XmlElement(name = "HumanBirthdate")
    protected String humanBirthdate;
    @XmlElement(name = "IdentityCardSeries")
    protected String identityCardSeries;
    @XmlElement(name = "IdentityCardNumber")
    protected String identityCardNumber;
    @XmlElement(name = "HumanID")
    protected EKKType.HumanID humanID;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<>();

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the ekkid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEKKID() {
        return ekkid;
    }

    /**
     * Sets the value of the ekkid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEKKID(String value) {
        this.ekkid = value;
    }

    /**
     * Gets the value of the ekkNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEKKNumber() {
        return ekkNumber;
    }

    /**
     * Sets the value of the ekkNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEKKNumber(String value) {
        this.ekkNumber = value;
    }

    /**
     * Gets the value of the ekkActive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEKKActive() {
        return ekkActive;
    }

    /**
     * Sets the value of the ekkActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEKKActive(String value) {
        this.ekkActive = value;
    }

    /**
     * Gets the value of the humanLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanLastName() {
        return humanLastName;
    }

    /**
     * Sets the value of the humanLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanLastName(String value) {
        this.humanLastName = value;
    }

    /**
     * Gets the value of the humanFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanFirstName() {
        return humanFirstName;
    }

    /**
     * Sets the value of the humanFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanFirstName(String value) {
        this.humanFirstName = value;
    }

    /**
     * Gets the value of the humanMiddleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanMiddleName() {
        return humanMiddleName;
    }

    /**
     * Sets the value of the humanMiddleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanMiddleName(String value) {
        this.humanMiddleName = value;
    }

    /**
     * Gets the value of the humanBirthdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHumanBirthdate() {
        return humanBirthdate;
    }

    /**
     * Sets the value of the humanBirthdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHumanBirthdate(String value) {
        this.humanBirthdate = value;
    }

    /**
     * Gets the value of the identityCardSeries property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentityCardSeries() {
        return identityCardSeries;
    }

    /**
     * Sets the value of the identityCardSeries property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentityCardSeries(String value) {
        this.identityCardSeries = value;
    }

    /**
     * Gets the value of the identityCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentityCardNumber() {
        return identityCardNumber;
    }

    /**
     * Sets the value of the identityCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentityCardNumber(String value) {
        this.identityCardNumber = value;
    }

    /**
     * Gets the value of the humanID property.
     * 
     * @return
     *     possible object is
     *     {@link EKKType.HumanID }
     *     
     */
    public EKKType.HumanID getHumanID() {
        return humanID;
    }

    /**
     * Sets the value of the humanID property.
     * 
     * @param value
     *     allowed object is
     *     {@link EKKType.HumanID }
     *     
     */
    public void setHumanID(EKKType.HumanID value) {
        this.humanID = value;
    }

    /**
     * Gets the value of the oid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="Human" type="{http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0}HumanType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class HumanID {

        @XmlElement(name = "Human")
        protected HumanType human;

        /**
         * Gets the value of the human property.
         * 
         * @return
         *     possible object is
         *     {@link HumanType }
         *     
         */
        public HumanType getHuman() {
            return human;
        }

        /**
         * Sets the value of the human property.
         * 
         * @param value
         *     allowed object is
         *     {@link HumanType }
         *     
         */
        public void setHuman(HumanType value) {
            this.human = value;
        }

    }

}
