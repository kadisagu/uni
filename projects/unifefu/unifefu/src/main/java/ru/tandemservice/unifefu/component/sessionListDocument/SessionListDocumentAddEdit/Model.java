/*$Id$*/
package ru.tandemservice.unifefu.component.sessionListDocument.SessionListDocumentAddEdit;

/**
 * @author DMITRY KNYAZEV
 * @since 25.08.2014
 */
public class Model extends ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentAddEdit.Model
{
    public boolean isEditForm(){
        return !isAddForm();
    }
}
