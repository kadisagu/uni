/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrphanBenefitList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.unifefu.entity.ws.FefuOrphanSettings;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Dmitry Seleznev
 * @since 14.04.2014
 */
@Configuration
public class FefuSettingsOrphanBenefitList extends BusinessComponentManager
{
    public static final String SETTINGS_BENEFIT_DS = "settingsBenefitDS";
    public static final String TOGGLE_COLUMN = "toggleColumn";
    public static final String ORPHAN_BENEFIT_PROPERTY = "orphanBenefit";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SETTINGS_BENEFIT_DS, getSettingsDirectumDS(), settingsDirectumDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getSettingsDirectumDS()
    {
        return columnListExtPointBuilder(SETTINGS_BENEFIT_DS)
                .addColumn(textColumn(DataWrapper.TITLE, DataWrapper.TITLE).order())
                .addColumn(toggleColumn(TOGGLE_COLUMN, ORPHAN_BENEFIT_PROPERTY).toggleOnListener("onClickChangeOrphanBenefit").toggleOffListener("onClickChangeOrphanBenefit"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> settingsDirectumDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<Object[]> benefits = new DQLSelectBuilder().fromEntity(Benefit.class, "b")
                        .joinEntity("b", DQLJoinType.left, FefuOrphanSettings.class, "os", eq(property(Benefit.id().fromAlias("b")), property(FefuOrphanSettings.benefit().id().fromAlias("os"))))
                        .createStatement(context.getSession()).list();

                List<DataWrapper> result = new ArrayList<>();

                for(Object[] item : benefits)
                {
                    Benefit benefit = (Benefit)item[0];
                    FefuOrphanSettings settings = (FefuOrphanSettings) item[1];
                    DataWrapper wrapper = new DataWrapper(benefit.getId(), benefit.getTitle(), benefit);
                    wrapper.setProperty(ORPHAN_BENEFIT_PROPERTY, null != settings && settings.isOrphanBenefit());
                    result.add(wrapper);
                }

                return ListOutputBuilder.get(input, result).build();
            }
        };
    }
}
