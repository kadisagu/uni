package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x3x3_14to15 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.13"),
				 new ScriptDependency("org.tandemframework.shared", "1.3.3"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.3.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность entrantFefuExt

		// создано свойство agreeWithPrivacyNotice
        if (!tool.columnExists("entrantfefuext_t", "agreewithprivacynotice_p"))
		{
			// создать колонку
			tool.createColumn("entrantfefuext_t", new DBColumn("agreewithprivacynotice_p", DBType.BOOLEAN));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность onlineEntrantFefuExt

		// создано свойство agreeWithPrivacyNotice
        if (!tool.columnExists("onlineentrantfefuext_t", "agreewithprivacynotice_p"))
		{
			// создать колонку
			tool.createColumn("onlineentrantfefuext_t", new DBColumn("agreewithprivacynotice_p", DBType.BOOLEAN));
		}
    }
}