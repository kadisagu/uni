package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.FefuCompensationTypeTransferStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «О переводе на другую основу оплаты обучения (вакантные бюджетные места)»
 */
public class FefuCompensationTypeTransferStuListExtract extends FefuCompensationTypeTransferStuListExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getTransferDate();
    }
}