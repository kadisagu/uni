package ru.tandemservice.unifefu.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Печатная форма расписания (расширение ДВФУ)
 */
public class SchedulePrintFormFefuExt extends SchedulePrintFormFefuExtGen
{
    @EntityDSLSupport
    public String getFormWithTerrTitle()
    {
        return getSppSchedulePrintForm().getFormativeOrgUnit().getTitle() + " (" + getTerritorialOrgUnit().getTerritorialTitle() + ")";
    }
}
