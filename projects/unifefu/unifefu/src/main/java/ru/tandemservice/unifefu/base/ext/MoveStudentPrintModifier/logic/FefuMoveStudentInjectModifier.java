/* $Id$ */
package ru.tandemservice.unifefu.base.ext.MoveStudentPrintModifier.logic;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.MoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.entity.FefuCaseCISetting2CustomStateRel;
import ru.tandemservice.unifefu.entity.FefuStudentCustomStateCISetting;
import ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination;
import ru.tandemservice.unifefu.entity.OrgUnitFefuExt;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 11.07.2013
 */
public class FefuMoveStudentInjectModifier extends MoveStudentInjectModifier implements IMoveStudentInjectModifier
{
    private static final Map<String, String[]> CUSTOM_LEARNED_MALE_SEX_CASES_ARRAY = new HashMap<>();
    private static final Map<String, String[]> CUSTOM_LEARNED_FEMALE_SEX_CASES_ARRAY = new HashMap<>();

    static
    {
        CUSTOM_LEARNED_MALE_SEX_CASES_ARRAY.put("custom_learned", CommonExtractPrint.DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.get("learned"));
        CUSTOM_LEARNED_MALE_SEX_CASES_ARRAY.put("custom_learned_past", CommonExtractPrint.DIFFERENT_WORD_MALE_SEX_CASES_ARRAY.get("learned_past"));

        CUSTOM_LEARNED_FEMALE_SEX_CASES_ARRAY.put("custom_learned", CommonExtractPrint.DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.get("learned"));
        CUSTOM_LEARNED_FEMALE_SEX_CASES_ARRAY.put("custom_learned_past", CommonExtractPrint.DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY.get("learned_past"));
    }

    @Override
    public void modularExtractModifier(RtfInjectModifier modifier, ModularStudentExtract extract)
    {
        initFefuCustomStateLearned(modifier, extract, "custom_learned");
        super.modularExtractModifier(modifier, extract);
    }

    @Override
    public void modularExtractTableModifier(RtfTableModifier tableModifier, ModularStudentExtract extract)
    {
        commonExtractTableModifier(tableModifier, extract);
        super.modularExtractTableModifier(tableModifier, extract);
    }

    @Override
    public void modularOrderTableModifier(RtfTableModifier tableModifier, StudentModularOrder order)
    {
        commonModularOrderTableModifier(tableModifier, order);
        super.modularOrderTableModifier(tableModifier, order);
    }

    @Override
    public void individualOrderTableModifier(RtfTableModifier modifier, StudentModularOrder order, ModularStudentExtract extract)
    {
        commonModularOrderTableModifier(modifier, order);
        super.individualOrderTableModifier(modifier, order, extract);
    }

    @Override
    public void listExtractModifier(RtfInjectModifier modifier, ListStudentExtract extract)
    {
        Student student = extract.getEntity();
        CommonExtractPrint.initFefuGroup(modifier, "groupInternal_G", student.getGroup(), student.getEducationOrgUnit().getDevelopForm(), " группы ");
        super.listExtractModifier(modifier, extract);
    }

    @Override
    public void listExtractTableModifier(RtfTableModifier tableModifier, ListStudentExtract extract)
    {
        commonExtractTableModifier(tableModifier, extract);
        super.listExtractTableModifier(tableModifier, extract);
    }

    @Override
    public void listOrderTableModifier(RtfTableModifier tableModifier, StudentListOrder order)
    {
        commonListOrderTableModifier(tableModifier, order);
        super.listOrderTableModifier(tableModifier, order);
    }

    private void commonListOrderTableModifier(RtfTableModifier tableModifier, StudentListOrder order)
    {
        commonAbstractOrderTableModifier(tableModifier, order);
    }

    private void commonModularOrderTableModifier(RtfTableModifier tableModifier, StudentModularOrder order)
    {
        commonAbstractOrderTableModifier(tableModifier, order);
    }

    private void commonAbstractOrderTableModifier(RtfTableModifier tableModifier, AbstractStudentOrder order)
    {
        AbstractStudentExtract extract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(order.getId());
        if (extract != null)
        {
            String orderTypeCode = "";
            if (order instanceof StudentListOrder)
            {
                orderTypeCode = order.getType().getCode();
            }
            else if (order instanceof StudentModularOrder)
            {
                orderTypeCode = extract.getType().getCode();
            }

            commonTableModifier(tableModifier, extract, orderTypeCode);
        }

        if (null == tableModifier.getValue("FEFU_PRIMARY_VISA"))
            tableModifier.put("FEFU_PRIMARY_VISA", new String[][]{});
    }

    private void commonExtractTableModifier(RtfTableModifier tableModifier, AbstractStudentExtract extract)
    {
        String orderTypeCode;
        if (extract instanceof ModularStudentExtract)
        {
            orderTypeCode = extract.getType().getCode();
        }
        else
        {
            orderTypeCode = extract.getParagraph() != null ? extract.getParagraph().getOrder().getType().getCode() : "";
        }

        if (!orderTypeCode.isEmpty())
        {
            commonTableModifier(tableModifier, extract, orderTypeCode);
        }
    }

    private void commonTableModifier(RtfTableModifier tableModifier, AbstractStudentExtract extract, String orderTypeCode)
    {
        String eduLevelStageCode = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getRoot().getCode();
        String[][] fefuPrimaryVisa = FefuStudentManager.instance().printInjectModifierDao().getVisaSignerExtract(orderTypeCode, eduLevelStageCode);

        if (fefuPrimaryVisa.length != 0)
        {
            tableModifier.put("FEFU_PRIMARY_VISA", fefuPrimaryVisa);
        }
        else
        {
            OrgUnit orgUnit = extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit();
            if (!TopOrgUnit.getInstance().equals(extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit()))
                orgUnit = extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit();

            OrgUnitFefuExt orgUnitFefuExt = DataAccessServices.dao().getByNaturalId(new OrgUnitFefuExt.NaturalId(orgUnit));

            if (null != orgUnitFefuExt && null != orgUnitFefuExt.getEmployeePostPossibleVisa())
            {
                EmployeePostPossibleVisa visa = orgUnitFefuExt.getEmployeePostPossibleVisa();
                tableModifier.put("FEFU_PRIMARY_VISA", new String[][]{{visa.getTitle(), visa.getEntity().getPerson().getIdentityCard().getIof()}});
            }
            else
            {
                fefuSignerVisaPostTitle(tableModifier, orgUnit, null, true);
            }
        }
    }

    public static void commonExtractSignerTableModifier(RtfTableModifier tableModifier)
    {
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        EmployeePost employeePost = (principalContext instanceof EmployeePost) ? ((EmployeePost) principalContext) : null;

        fefuSignerVisaPostTitle(tableModifier, null, employeePost, false);
    }

    private static void fefuSignerVisaPostTitle(RtfTableModifier tableModifier, OrgUnit orgUnit, EmployeePost employeePost, boolean visa)
    {
        if (null == orgUnit)
        {
            if (null != employeePost) orgUnit = employeePost.getOrgUnit();
        }
        else
        {
            if (null == employeePost) employeePost = (EmployeePost) orgUnit.getHead();
        }

        String postTitle = null;
        String employeePostTitle = null;

        if (null != orgUnit && visa)
        {
            OrgUnitFefuExt orgUnitFefuExt = DataAccessServices.dao().getByNaturalId(new OrgUnitFefuExt.NaturalId(orgUnit));
            if (null != orgUnitFefuExt && null != orgUnitFefuExt.getEmployeePostPossibleVisa())
            {
                EmployeePostPossibleVisa post = orgUnitFefuExt.getEmployeePostPossibleVisa();
                postTitle = post.getTitle();
                employeePostTitle = post.getEntity().getPerson().getIdentityCard().getIof();
            }
        }

        if (null != employeePost)
        {
            if (null == postTitle || null == employeePostTitle)
            {
                Post post = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost();
                postTitle = null != post.getNominativeCaseTitle() ? post.getNominativeCaseTitle() : post.getTitle();
                postTitle += " " + (null != employeePost.getOrgUnit().getGenitiveCaseTitle() ? employeePost.getOrgUnit().getGenitiveCaseTitle() : employeePost.getOrgUnit().getFullTitle());
                employeePostTitle = employeePost.getPerson().getIdentityCard().getIof();
            }

            if (visa)
                tableModifier.put("FEFU_PRIMARY_VISA", new String[][]{{postTitle, employeePostTitle}});
            else
                tableModifier.put("FEFU_EXTRACT_SIGNER_VISA", new String[][]{{postTitle, employeePostTitle}});
        }
        else
        {
            tableModifier.put("FEFU_EXTRACT_SIGNER_VISA", new String[][]{});
        }
    }

    private static void initFefuCustomStateLearned(RtfInjectModifier modifier, ModularStudentExtract extract, String label)
    {
        DQLSelectBuilder customStateBuilder = new DQLSelectBuilder()
                .fromEntity(StudentCustomState.class, "cs")
                .where(eq(property(StudentCustomState.student().fromAlias("cs")), value(extract.getEntity())))
                .column(property(StudentCustomState.customState().fromAlias("cs")));

        List<StudentCustomStateCI> studList = DataAccessServices.dao().getList(customStateBuilder);

        boolean isMale = extract.getEntity().getPerson().getIdentityCard().getSex().isMale();
        CommonExtractPrint.injectDifferentWords(modifier, isMale ? CUSTOM_LEARNED_MALE_SEX_CASES_ARRAY : CUSTOM_LEARNED_FEMALE_SEX_CASES_ARRAY);

        boolean contain = false;

        List<GrammaCase> cases = new ArrayList<>(DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.keySet());
        List<FefuStudentCustomStateCISetting> settingList = DataAccessServices.dao().getList(FefuStudentCustomStateCISetting.class);

        for (FefuStudentCustomStateCISetting setting : settingList)
        {
            if (!contain)
            {
                DQLSelectBuilder customStateDecBuilder = new DQLSelectBuilder()
                        .fromEntity(FefuCaseCISetting2CustomStateRel.class, "rel")
                        .where(eq(property(FefuCaseCISetting2CustomStateRel.setting().fromAlias("rel")), value(setting)))
                        .column(property(FefuCaseCISetting2CustomStateRel.customState().fromAlias("rel")));

                List<StudentCustomStateCI> decList = DataAccessServices.dao().getList(customStateDecBuilder);

                contain = studList.size() == decList.size() && studList.containsAll(decList);

                if (contain)
                {
                    List<FefuStudentCustomStateDeclination> declinationList = DataAccessServices.dao().getList(FefuStudentCustomStateDeclination.class, FefuStudentCustomStateDeclination.setting(), setting);

                    for (FefuStudentCustomStateDeclination d : declinationList)
                    {
                        String printLabel = d.isPast() ? label + "_past" : label;
                        for (String casePostfix : UniRtfUtil.CASE_POSTFIX)
                        {
                            int numberPostfix = UniRtfUtil.CASE_POSTFIX.indexOf(casePostfix);
                            GrammaCase currentGrammaCase = cases.get(numberPostfix);
                            InflectorVariant inflectorVariant = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(currentGrammaCase);

                            if (d.getValue() != null && d.getInflectorVariant().getCode().equals(inflectorVariant.getCode()) && isMale == d.isMale())
                                modifier.put(printLabel + casePostfix, d.getValue());
                        }
                    }
                }
            }
        }
    }
}