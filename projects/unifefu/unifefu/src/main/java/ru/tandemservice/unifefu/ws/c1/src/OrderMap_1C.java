/* $Id$ */
package ru.tandemservice.unifefu.ws.c1.src;

import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.HashMap;
import java.util.Map;

import static ru.tandemservice.unifefu.ws.c1.src.OrderType_1C.*;

/**
 * @author Nikolay Fedorovskih
 * @since 16.12.2013
 */
public class OrderMap_1C
{
    // Коды приказов для соответствующих типов выписок (в большинстве случаев код приказа - пустая строка)

    // Кроме кода приказа, каждому типу приказа Ф4 или Ф5 прописывается соответствующий вид расчета.
    // !!! Но есть приказы, которые еще и надбавку старосте делают - для надбавок старостам собственный вид расчета, не привязанный к типу приказа.
    // Виды расчетов для приказов по начислениям/прекращениям выплат
    public static final String NONE_PAYMENT_CODE = ""; // Пусто

    public static final String ACAD_GRANT_PAYMENT_CODE = "10"; // Академическая стипендия
    public static final String ACAD_GRANT_PREMIUM_PAYMENT_CODE = "12"; // Надбавка к академической стипендии
    public static final String ACAD_GRANT_ONCE_RAISE_PAYMENT_CODE = "13"; // Однократное повышение государственной академической стипендии

    public static final String SOCIAL_GRANT_PAYMENT_CODE = "20"; // Социальная стипендия

    public static final String FIN_AID_PAYMENT_CODE = "30"; // Материальная помощь

    // Надбавка старосте не привязана к типу приказа! Если выписка имплементирует IGroupManagerPaymentExtract, то уйдет пакет Ф4 с этим кодом выплаты
    public static final String GROUP_CAPTAIN_PREMIUM_PAYMENT_CODE = "40"; // Надбавка к академической стипендии (старосте группы)

    //public static final String FULL_STATE_MAINTENANCE = null/*"50"*/; // Начисления соответствующих выплат, предусмотренных в рамках полного государственного обеспечения

    public static final String EXPECTANT_BENEFIT = "60"; // Пособие по беременности и родам
    public static final String EXPECTANT_BENEFIT_ONETIME = "61"; // Единовременное пособие по беременности и родам

    private static final String INTERNAL_ENROLLMENT_CODE = "__enrollment__";

    public static final Map<String, OrderCode_1C[]> ORDER_CODES_MAP = new HashMap<>(77);

    static
    {
        // !!! Для абитуриентских приказов прописывается один код на все типы! Они все по умолчанию считаются как Ф1.
        ORDER_CODES_MAP.put(INTERNAL_ENROLLMENT_CODE, _codes(ENROLLMENT));

        // Сборные

        // Ф2 + Ф4 (особенно костыльный случай)
        // !!! Внимание !!! Костыль !!! Тут в зависимости от галочек в приказе может быть выплата как EXPECTANT_BENEFIT, так и EXPECTANT_BENEFIT_ONETIME
        // TODO переделать архитектуру под возможность одним приказом разные выплаты начислять - это и надбавок старостам касается
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.WEEKEND_PREGNANCY_MODULAR_ORDER, new OrderCode_1C[]{
                new OrderCode_1C(TRANSFER, null),
                new OrderCode_1C(ASSIGN_PAYMENT, EXPECTANT_BENEFIT)}); // О предоставлении отпуска по беременности и родам - weekendPregnancyStuExtract

        // Ф1
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.RESTORATION_COURSE_MODULAR_ORDER, _codes(ENROLLMENT)); // О восстановлении - restorationCourseStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.RESTORATION_ADMIT_TO_DIPLOMA_DEFEND_ABSCENCE_MODULAR_ORDER, _codes(ENROLLMENT)); // О восстановлении и допуске к защите ВКР (неявка на защиту) - restorationAdmitToDiplomaDefStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.RESTORATION_ADMIT_TO_REPEAT_DIPLOMA_DEFEND_MODULAR_ORDER, _codes(ENROLLMENT)); // О восстановлении и допуске к защите ВКР (повторная защита) - restorationAdmitToDiplomaDefExtStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.RESTORATION_ADMIT_TO_DIPLOMA_MODULAR_ORDER, _codes(ENROLLMENT)); // О восстановлении и допуске к подготовке ВКР - restorationAdmitToDiplomaStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.RESTORATION_ADMIT_ABSCENCE_STATE_EXAMS_MODULAR_ORDER, _codes(ENROLLMENT)); // О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ) - restorationAdmitStateExamsStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.RESTORATION_ADMIT_REPEAT_STATE_EXAMS_MODULAR_ORDER, _codes(ENROLLMENT)); // О восстановлении и повторном допуске к сдаче государственного экзамена (пересдача ГЭ) - restorationAdmitStateExamsExtStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_TRANSFER_ENROLLMENT_MODULAR_ORDER, _codes(ENROLLMENT)); // О зачислении в порядке перевода - fefuEduTransferEnrolmentStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_ENROLL_TO_SECOND_AND_NEXT_COURSE_MODULAR_ORDER, _codes(ENROLLMENT)); // О зачислении на второй и последующий курс - fefuEduEnrolmentToSecondAndNextCourseStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_BUDGET_COMPENSATION_TYPE_MODULAR_ORDER, _codes(ENROLLMENT)); // О переводе на вакантное бюджетное место - transferCompTypeExtStuExtract

        // Ф2
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.WEEKEND_OUT_MODULAR_ORDER, _codes(TRANSFER));  // О выходе из академического отпуска - weekendOutStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.WEEKEND_PREGNANCY_OUT_MODULAR_ORDER, _codes(TRANSFER)); // О выходе из отпуска по беременности и родам - weekendPregnancyOutStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.WEEKEND_CHILD_OUT_VARIANT_2_MODULAR_ORDER, _codes(TRANSFER)); // О выходе из отпуска по уходу за ребенком - weekendChildCareOutStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_GROUP_MODULAR_ORDER, _codes(TRANSFER)); // О переводе из группы в группу - transferGroupStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_BETWEEN_TERRITORIAL_MODULAR_ORDER, _codes(TRANSFER));// О переводе между территориальными подразделениями - transferBetweenTerritorialStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_DEV_CONDITIONS_AND_PERIOD_MODULAR_ORDER, _codes(TRANSFER)); // О переводе на другие условия освоения и срок освоения - transferDevCondExtStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_DEV_CODTIDIONS_MODULAR_ORDER, _codes(TRANSFER)); // О переводе на другие условия освоения - transferDevCondStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_TRANSFER_DEVELOP_FORM_MODULAR_ORDER, _codes(TRANSFER)); // О переводе на другую форму освоения - fefuTransferDevFormStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_COURSE_MODULAR_ORDER, _codes(TRANSFER)); // О переводе с курса на следующий курс - transferCourseStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_COURSE_CHANGE_TERRITORIAL_MODULAR_ORDER, _codes(TRANSFER)); // О переводе с курса на следующий курс со сменой территориального подразделения - transferCourseWithChangeTerrOuStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_PROFILE_MODULAR_ORDER, _codes(TRANSFER)); // О переводе со специализации на специализации (с профиля на профиль) - transferProfileExtStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_PROFILE_GROUP_REASSIGN_MODULAR_ORDER, _codes(TRANSFER)); // О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу - transferProfileGroupExtStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_EDU_LEVEL_MODULAR_ORDER, _codes(TRANSFER)); // О переводе со специальности на специальность - transferEduLevelStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_FORMATIVE_CHANGE_MODULAR_ORDER, _codes(TRANSFER)); // О переводе со специальности на специальность, из одной школы в другую - transferFormativeStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_TERRITORIAL_MODULAR_ORDER, _codes(TRANSFER)); // О переводе со специальности на специальность, из филиала в филиал - transferTerritorialExtStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_TERRITORIAL_CHANVE_MODULAR_ORDER, _codes(TRANSFER)); // О переводе со специальности на специальность, из школы в филиал - transferTerritorialStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.WEEKENT_VARIANT_2_MODULAR_ORDER, _codes(TRANSFER)); // О предоставлении академического отпуска - weekendStuExtractExt
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.WEEKEND_CHILD_ONE_AND_HALF_MODULAR_ORDER, _codes(TRANSFER)); // О предоставлении отпуска по уходу за ребенком до 1,5 лет - weekendChildOneAndHalfStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.WEEKEND_CHILD_THREE_MODULAR_ORDER, _codes(TRANSFER)); // О предоставлении отпуска по уходу за ребенком до 3 лет - weekendChildThreeStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.GROUP_ASSIGN_MODULAR_ORDER, _codes(TRANSFER)); // О распределении в группу - groupAssignStuExtract

        // Ф3
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.GIVE_DIPLOMA_MODULAR_ORDER, _codes(EXCLUDE)); // О присвоении степени, выдаче диплома и отчислении в связи с окончанием университета - giveDiplomaStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_EXCLUDE_MODULAR_ORDER, _codes(EXCLUDE)); // Об отчислении - fefuExcludeStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.EXCLUDE_GP_DEFENCE_MODULAR_ORDER, _codes(EXCLUDE)); // Об отчислении (ВКР) - excludeGPDefenceFailStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.EXCLUDE_STATE_EXAM_MODULAR_ORDER, _codes(EXCLUDE)); // Об отчислении (государственный экзамен) - excludeStateExamStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.EXCLUDE_UNACCEPTED_TO_GP_DEFENCE_MODULAR_ORDER, _codes(EXCLUDE)); // Об отчислении (недопуск к ВКР) - excludeUnacceptedToGPDefenceStuExtract

        // Ф4
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_SOC_GRANT_RESUMPTION_MODULAR_ORDER, _codes(ASSIGN_PAYMENT, SOCIAL_GRANT_PAYMENT_CODE)); // О возобновлении выплаты социальной стипендии - fefuSocGrantResumptionStuExtract
        //ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_FULL_STATE_MAINTENANCE_ENROLLMENT_MODULAR_ORDER, _codes(ASSIGN_PAYMENT, FULL_STATE_MAINTENANCE)); // О зачислении на полное государственное обеспечение - fullStateMaintenanceEnrollmentStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_ACAD_GRANT_ASSIGN_MODULAR_ORDER, _codes(ASSIGN_PAYMENT, ACAD_GRANT_PAYMENT_CODE)); // О назначении академической стипендии - fefuAcadGrantAssignStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_ACAD_GRANT_ASSIGN_ENROLLMENT_MODULAR_ORDER, _codes(ASSIGN_PAYMENT, ACAD_GRANT_PAYMENT_CODE)); // О назначении академической стипендии (вступительные испытания) - fefuAcadGrantAssignStuEnrolmentExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.ACAD_GRANT_BONUS_ASSIGN_MODULAR_ORDER, _codes(ASSIGN_PAYMENT, ACAD_GRANT_PREMIUM_PAYMENT_CODE)); // О назначении надбавки к государственной академической стипендии - acadGrantBonusAssignStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.SOC_GRANT_ASSIGN_MODULAR_ORDER, _codes(ASSIGN_PAYMENT, SOCIAL_GRANT_PAYMENT_CODE)); // О назначении социальной стипендии - socGrantAssignStuExtract
        //ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_FULL_STATE_MAINTENANCE_MODULAR_ORDER, _codes(ASSIGN_PAYMENT, FULL_STATE_MAINTENANCE)); // О полном государственном обеспечении - fullStateMaintenanceStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.GRANT_RISE_MODULAR_ORDER, _codes(ASSIGN_PAYMENT, ACAD_GRANT_ONCE_RAISE_PAYMENT_CODE)); // Об однократном повышении государственной академической стипендии - grantRiseStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.MAT_AID_ASSIGN_MODULAR_ORDER, _codes(ASSIGN_PAYMENT, FIN_AID_PAYMENT_CODE)); // Об оказании материальной помощи - matAidAssignStuExtract

        // Ф5
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.ACAD_GRANT_PAYMENT_STOP_MODULAR_ORDER, _codes(TAKE_OFF_PAYMENT, ACAD_GRANT_PAYMENT_CODE)); // О прекращении выплаты академической стипендии - acadGrantPaymentStopStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.ACAD_GRANT_BONUS_PAYMENT_STOP_MODULAR_ORDER, _codes(TAKE_OFF_PAYMENT, ACAD_GRANT_PREMIUM_PAYMENT_CODE)); // О прекращении выплаты надбавки к государственной академической стипендии - acadGrantBonusPaymentStopStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.SOC_GRANT_PAYMENT_STOP_MODULAR_ORDER, _codes(TAKE_OFF_PAYMENT, SOCIAL_GRANT_PAYMENT_CODE)); // О прекращении выплаты социальной стипендии - socGrantPaymentStopStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_REEDUCATION_MODULAR_ORDER, _codes(TAKE_OFF_PAYMENT, NONE_PAYMENT_CODE)); // О предоставлении повторного года обучения - fefuReEducationStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.REEDUCATION_FGOS_MODULAR_ORDER, _codes(TAKE_OFF_PAYMENT, NONE_PAYMENT_CODE)); // О предоставлении повторного года обучения с переходом на ФГОС - reEducationFGOSStuExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_SOC_GRANT_STOP_MODULAR_ORDER, _codes(TAKE_OFF_PAYMENT, SOCIAL_GRANT_PAYMENT_CODE)); // О приостановлении выплаты социальной стипендии - fefuSocGrantStopStuExtract

        // Списочные
        // Ф2
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.SET_SPECIALIZATION_AND_REASSIGN_GROUP_LIST_EXTRACT, _codes(TRANSFER)); // О закреплении за специализациями  и перераспределении в группы (списочный) - splitStudentsGroupStuListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.GROUP_TRANSFER_LIST_EXTRACT, _codes(TRANSFER)); // О переводе из группы в группу (списочный) - groupTransferExtStuListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_TRANSFER_COURSE_LIST_EXTRACT, _codes(TRANSFER)); // О переводе с курса на курс (списочный) - fefuCourseTransferStuListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TERRITORIAL_COURSE_TRANSFER_LIST_EXTRACT, _codes(TRANSFER)); // О переводе с курса на следующий курс (по направлениям подготовки) со сменой территориального подразделения - territorialCourseTransferStuListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.ASSIGN_TO_GROUPS_FIRST_COURSE_STUDENTS_LIST_EXTRACT, _codes(TRANSFER)); // О распределении студентов 1 курса в группы - splitFirstCourseStudentsGroupStuListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.TRANSFER_TERRITORIAL_VARIANT_2_LIST_EXTRACT, _codes(TRANSFER)); // О переводе студентов территориального подразделения - territorialTransferExtStuListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_TRANSFER_FORMATIVE_LIST_EXTRACT, _codes(TRANSFER)); // Об изменении формирующего подразделения (списочный) - fefuFormativeTransferStuListExtract

        // Ф3
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_GIVE_GENERAL_DIPLOMA_WITH_DISMISS_LIST_EXTRACT, _codes(EXCLUDE)); // О выдаче диплома и присвоении квалификации - fefuGiveDiplomaWithDismissStuListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_GIVE_DIPLOMA_WITH_HONOURS_WITH_DISMISS_LIST_EXTRACT, _codes(EXCLUDE)); // О выдаче диплома с отличием и присвоении квалификации - fefuGiveDiplomaSuccessWithDismissStuListExtract

        // Ф4
        //ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_FULL_STATE_MAINTENANCE_ENROLLMENT_LIST_EXTRACT, _codes(ASSIGN_PAYMENT, FULL_STATE_MAINTENANCE)); // О зачислении на полное государственное обеспечение (списочный) - fullStateMaintenanceEnrollmentStuListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FEFU_ACAD_GRANT_ASSIGN_ENROLLMENT_LIST_EXTRACT, _codes(ASSIGN_PAYMENT, ACAD_GRANT_PAYMENT_CODE)); // О назначении академической стипендии (вступительные испытания) (списочный) - fefuAcadGrantAssignStuEnrolmentListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.ACAD_GRANT_ASSIGN_LIST_EXTRACT, _codes(ASSIGN_PAYMENT, ACAD_GRANT_PAYMENT_CODE)); // О назначении академической стипендии (списочный) - acadGrantAssignStuListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.SOC_GRANT_ASSIGN_LIST_EXTRACT, _codes(ASSIGN_PAYMENT, SOCIAL_GRANT_PAYMENT_CODE)); // О назначении социальной стипендии (списочный) - socGrantAssignStuListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.ACAD_GRANT_RISE_LIST_EXTRACT, _codes(ASSIGN_PAYMENT, ACAD_GRANT_ONCE_RAISE_PAYMENT_CODE)); // Об однократном повышении государственной академической стипендии - grantRiseStuListExtract
        ORDER_CODES_MAP.put(StudentExtractTypeCodes.FIN_AID_ASSIGN_LIST_EXTRACT, _codes(ASSIGN_PAYMENT, FIN_AID_PAYMENT_CODE)); // Об оказании материальной помощи - finAidAssignStuListExtract
    }

    public static OrderCode_1C get(Object extract, OrderType_1C orderType)
    {
        OrderCode_1C[] codes;
        if (extract instanceof EnrollmentExtract)
            codes = ORDER_CODES_MAP.get(INTERNAL_ENROLLMENT_CODE);
        else
            codes = ORDER_CODES_MAP.get(((IAbstractExtract) extract).getType().getCode());

        if (codes != null)
        {
            for (OrderCode_1C code : codes)
            {
                if (code.getType() == orderType)
                    return code;
            }
        }
        return null;
    }

    private static OrderCode_1C[] _codes(OrderType_1C type, String paymentCode)
    {
        return new OrderCode_1C[]{new OrderCode_1C(type, paymentCode)};
    }

    private static OrderCode_1C[] _codes(OrderType_1C type)
    {
        return _codes(type, null);
    }
}