package ru.tandemservice.unifefu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Дополнительный статус студента (справочник)"
 * Имя сущности : studentCustomStateCI
 * Файл data.xml : unifefu-catalog.data.xml
 */
public interface StudentCustomStateCICodes
{
    /** Константа кода (code) элемента : Допущен к сдаче ГЭ (title) */
    String DOPUTSHEN_K_SDACHE_G_E = "admittedToStateExams";
    /** Константа кода (code) элемента : Допущен к защите ВКР (title) */
    String DOPUTSHEN_K_ZATSHITE_V_K_R = "admittedToDiploma";
    /** Константа кода (code) элемента : Каникулы (title) */
    String KANIKULY = "vacation";
    /** Константа кода (code) элемента : Диплом с отличием (title) */
    String DIPLOM_S_OTLICHIEM = "diplomaSuccess";
    /** Константа кода (code) элемента : Целевой прием (title) */
    String TSELEVOY_PRIEM = "targetReception";
    /** Константа кода (code) элемента : Допущен к ГИА (title) */
    String DOPUTSHEN_K_G_I_A = "admittedToGIA";
    /** Константа кода (code) элемента : Условный перевод (title) */
    String USLOVNYY_PEREVOD = "conditionalTransfer";

    Set<String> CODES = ImmutableSet.of(DOPUTSHEN_K_SDACHE_G_E, DOPUTSHEN_K_ZATSHITE_V_K_R, KANIKULY, DIPLOM_S_OTLICHIEM, TSELEVOY_PRIEM, DOPUTSHEN_K_G_I_A, USLOVNYY_PEREVOD);
}
