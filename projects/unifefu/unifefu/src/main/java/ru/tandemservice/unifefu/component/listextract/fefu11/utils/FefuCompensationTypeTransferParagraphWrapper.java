/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu11.utils;

import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class FefuCompensationTypeTransferParagraphWrapper implements Comparable<FefuCompensationTypeTransferParagraphWrapper>
{
    private Date _transferDate;
    private StudentCategory _studentCategory;
    private DevelopForm _developForm;

    private List<FefuCompensationTypeTransferParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<>();

    public FefuCompensationTypeTransferParagraphWrapper(Date transferDate, StudentCategory studentCategory, DevelopForm developForm)
    {
        _transferDate = transferDate;
        _studentCategory = studentCategory;
        _developForm = developForm;
    }

    public Date getTransferDate()
    {
        return _transferDate;
    }

    public void setTransferDate(Date transferDate)
    {
        _transferDate = transferDate;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public void setStudentCategory(StudentCategory studentCategory)
    {
        _studentCategory = studentCategory;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public List<FefuCompensationTypeTransferParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public void setParagraphPartWrapperList(List<FefuCompensationTypeTransferParagraphPartWrapper> paragraphPartWrapperList)
    {
        _paragraphPartWrapperList = paragraphPartWrapperList;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuCompensationTypeTransferParagraphWrapper))
            return false;

        FefuCompensationTypeTransferParagraphWrapper that = (FefuCompensationTypeTransferParagraphWrapper) o;

        return _studentCategory.equals(that.getStudentCategory());
    }

    @Override
    public int compareTo(FefuCompensationTypeTransferParagraphWrapper o)
    {
        if (StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(o.getStudentCategory().getCode())) return -1;
        else if (StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(o.getStudentCategory().getCode())) return 1;
        else return 0;
    }
}
