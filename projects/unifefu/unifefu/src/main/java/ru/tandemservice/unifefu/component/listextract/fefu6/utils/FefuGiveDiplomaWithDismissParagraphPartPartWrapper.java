/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nvankov
 * @since 7/2/13
 */
public class FefuGiveDiplomaWithDismissParagraphPartPartWrapper implements Comparable<FefuGiveDiplomaWithDismissParagraphPartPartWrapper>
{
    private final EducationLevelsHighSchool _educationLevelsHighSchool;
    private final ListStudentExtract _firstExtract;

    public FefuGiveDiplomaWithDismissParagraphPartPartWrapper(EducationLevelsHighSchool educationLevelsHighSchool, ListStudentExtract firstExtract)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
        _firstExtract = firstExtract;
    }

    private final List<Person> _personList = new ArrayList<>();

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuGiveDiplomaWithDismissParagraphPartPartWrapper))
            return false;

        FefuGiveDiplomaWithDismissParagraphPartPartWrapper that = (FefuGiveDiplomaWithDismissParagraphPartPartWrapper) o;

        if(_educationLevelsHighSchool.getEducationLevel().getLevelType().isSpecialization() ||
                _educationLevelsHighSchool.getEducationLevel().getLevelType().isProfile() ||
                that.getEducationLevelsHighSchool().getEducationLevel().getLevelType().isSpecialization() ||
                that.getEducationLevelsHighSchool().getEducationLevel().getLevelType().isProfile())
            return _educationLevelsHighSchool.equals(that.getEducationLevelsHighSchool());
        else
            return true;
    }

    @Override
    public int hashCode()
    {
        return _educationLevelsHighSchool.hashCode();
    }

    @Override
    public int compareTo(FefuGiveDiplomaWithDismissParagraphPartPartWrapper o)
    {
        StructureEducationLevels levelType1 = _educationLevelsHighSchool.getEducationLevel().getLevelType();
        StructureEducationLevels levelType2 = o.getEducationLevelsHighSchool().getEducationLevel().getLevelType();
        if(!levelType1.isProfile() && !levelType1.isSpecialization() && (levelType2.isProfile() || levelType2.isSpecialization())) return -1;
        else if((levelType1.isProfile() || levelType1.isSpecialization()) && !levelType2.isProfile() && !levelType2.isSpecialization()) return 1;
        else if(levelType1.isSpecialization() && levelType2.isProfile()) return -1;
        else if(levelType1.isProfile() && levelType2.isSpecialization()) return 1;
        else if((levelType1.isProfile() && levelType2.isProfile()) || (levelType1.isSpecialization() && levelType2.isSpecialization()))
        {
            if(!_educationLevelsHighSchool.equals(o.getEducationLevelsHighSchool()))
            {
                return _educationLevelsHighSchool.getTitle().compareTo(o.getEducationLevelsHighSchool().getTitle());
            }
            else
            {
                return 0;
            }
        }
        else return 0;
    }
}
