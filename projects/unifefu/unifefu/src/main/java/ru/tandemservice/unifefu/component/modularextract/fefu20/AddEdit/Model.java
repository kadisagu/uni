/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu20.AddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOExtract;


/**
 * @author Ekaterina Zvereva
 * @since 20.01.2015
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuEnrollStuDPOExtract>
{
    private ISelectModel _dpoProgramModel;
    private IUploadFile _uploadFile;
    private StudentStatus _statusNew;

    public ISelectModel getDpoProgramModel()
    {
        return _dpoProgramModel;
    }

    public void setDpoProgramModel(ISelectModel dpoProgramModel)
    {
        _dpoProgramModel = dpoProgramModel;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

    public StudentStatus getStatusNew()
    {
        return _statusNew;
    }

    public void setStatusNew(StudentStatus statusNew)
    {
        _statusNew = statusNew;
    }

}