/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu11;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class FefuCompensationTypeTransferStuListExtractPrint implements IPrintFormCreator<FefuCompensationTypeTransferStuListExtract>, IListParagraphPrintFormCreator<FefuCompensationTypeTransferStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuCompensationTypeTransferStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuCompensationTypeTransferStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);

        Group group = firstExtract.getEntity().getGroup();
        EducationOrgUnit educationOrgUnit = firstExtract.getEntity().getEducationOrgUnit();

        CommonExtractPrint.initFefuGroup(injectModifier, "intoFefuGroup", group, educationOrgUnit.getDevelopForm(), " группы ");
        injectModifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getTransferDate()));

        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuCompensationTypeTransferStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuCompensationTypeTransferStuListExtract firstExtract)
    {
    }
}