/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.entrant.FefuEntrantContractList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 01.04.2013
 */
public interface IDAO extends IUniDao<Model>
{
    /**
     * Удаляет договор выранного напрвления приема
     * @param directionId - id выбранного направления приема
     */
    public void deleteContract(Long directionId);

    /**
     * Изменяте факт оплаты аванса договора выбранного направления приема
     * @param directionId - id выбранного направления приема
     */
    public void updateAdvancePayed(Long directionId);
}