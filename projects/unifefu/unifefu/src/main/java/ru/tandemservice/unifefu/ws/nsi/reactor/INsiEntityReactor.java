/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import java.util.List;
import java.util.Set;

/**
 * @author Dmitry Seleznev
 * @since 08.08.2013
 */
public interface INsiEntityReactor<T>
{
    /**
     * Возвращает пустую сущность НСИ для запроса полного перечня элементов соответствующего справочника
     *
     * @return - Пустая сущность НСИ
     */
    T getCatalogElementRetrieveRequestObject(String guid);

    /**
     * Производит полную синхронизацию справочника с НСИ
     */
    void synchronizeFullCatalogWithNsi();

    /**
     * Добавляет новые и обновляет добавленные ранее в НСИ элементы справочника по идентификаторам ОБ
     *
     * @param entityToAddIdsSet - набор идентификаторов ОБ
     */
    void insertOrUpdateNewItemsAtNSI(Set<Long> entityToAddIdsSet);

    /**
     * Удаляет сущности из НСИ по идентификаторам ОБ
     *
     * @param entityToAddIdsSet - набор идентификаторов ОБ
     */
    void deleteItemsFromNSI(Set<Long> entityToAddIdsSet);

    /**
     * Возвращает подготовленный пакет сущностей НСИ, сгенерированных по сущностям ОБ.
     *
     * @param itemsToRetrieve - сущности НСИ с идентификаторами, которые требуется получить.
     *                        Если данный параметр не задан, то вернётся полный перечень элементов справочника.
     *                        Если данный параметр имеет только пакеты без идентификаторов, то вернётся полный
     *                        перечень элементов справочника.
     * @return - перечень элементов справочника в ОБ
     */
    List<Object> retrieve(List<INsiEntity> itemsToRetrieve);

    /**
     * Создаёт новые сущности, либо обновляет существующие в ОБ по идентификаторам НСИ.
     *
     * @param itemsToInsert - сущности НСИ с идентификаторами, которые требуется создать.
     * @return - перечень элементов справочника в ОБ
     */
    List<Object> insert(List<INsiEntity> itemsToInsert) throws NSIProcessingErrorException, NSIProcessingWarnException;

    /**
     * Обновляет сущности в ОБ по идентификаторам НСИ.
     *
     * @param itemsToUpdate - сущности НСИ с идентификаторами, которые требуется обновить.
     * @return - перечень элементов справочника в ОБ
     */
    List<Object> update(List<INsiEntity> itemsToUpdate) throws NSIProcessingErrorException, NSIProcessingWarnException;

    /**
     * Удаляет сущности в ОБ по идентификаторам НСИ.
     *
     * @param itemsToDelete - сущности НСИ с идентификаторами, которые требуется удалить.
     * @return - перечень элементов справочника в ОБ
     */
    List<Object> delete(List<INsiEntity> itemsToDelete) throws NSIProcessingErrorException, NSIProcessingWarnException;
}