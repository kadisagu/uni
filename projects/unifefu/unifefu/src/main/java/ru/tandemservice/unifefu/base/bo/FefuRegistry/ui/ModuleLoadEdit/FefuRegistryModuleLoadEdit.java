/* $Id: FefuRegistryModuleLoadEdit.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.ModuleLoadEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 05.02.14
 * Time: 12:43
 */

@Configuration
public class FefuRegistryModuleLoadEdit extends BusinessComponentManager
{
    public static final String EPP_CONTROL_TYPE_DS = "eppControlTypeDS";
    public static final String EPP_GRADE_SCALE_DS = "eppGradeScaleDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EPP_CONTROL_TYPE_DS, eppControlTypeDSHandler()))
                .addDataSource(selectDS(EPP_GRADE_SCALE_DS, eppGradeScaleDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eppControlTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppFControlActionType.class).order(EppFControlActionType.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eppGradeScaleDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppGradeScale.class).order(EppGradeScale.title());
    }
}
