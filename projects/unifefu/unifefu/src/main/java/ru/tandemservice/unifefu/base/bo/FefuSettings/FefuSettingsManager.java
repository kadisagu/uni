/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuSettings.logic.FefuSettingsDao;
import ru.tandemservice.unifefu.base.bo.FefuSettings.logic.IFefuSettingsDao;

/**
 * @author Dmitry Seleznev
 * @since 10.07.2013
 */
@Configuration
public class FefuSettingsManager extends BusinessObjectManager
{
    public static FefuSettingsManager instance()
    {
        return instance(FefuSettingsManager.class);
    }

    @Bean
    public IFefuSettingsDao dao()
    {
        return new FefuSettingsDao();
    }
}