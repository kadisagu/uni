/**
 *$Id$
 */
package ru.tandemservice.unifefu.dao.student;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.student.IAcademicGrantSizeDAO;
import ru.tandemservice.unifefu.entity.FefuStudentPaymentsAndBonusesSizeSetting;

/**
 * @author Alexander Zhebko
 * @since 12.03.2013
 */
public class FefuAcademicGrantSizeDAO extends UniBaseDao implements IAcademicGrantSizeDAO
{
    public static FefuStudentPaymentsAndBonusesSizeSetting getGrantSettings(UniBaseDao dao)
    {
        return new DQLSelectBuilder().fromEntity(FefuStudentPaymentsAndBonusesSizeSetting.class, "s").
                createStatement(dao.getComponentSession()).uniqueResult();
    }

    @Override
    public Integer getDefaultGrantSize()
    {
        FefuStudentPaymentsAndBonusesSizeSetting settings = getGrantSettings(this);
        return settings == null ? null : settings.getDefaultGrantSize();
    }

    @Override
    public Integer getDefaultGroupManagerBonusSize()
    {
        FefuStudentPaymentsAndBonusesSizeSetting settings = getGrantSettings(this);
        return settings == null ? null : settings.getDefaultGroupManagerBonusSize();
    }

    @Override
    public Integer getDefaultSocialGrantSize()
    {
        FefuStudentPaymentsAndBonusesSizeSetting settings = getGrantSettings(this);
        return settings == null ? null : settings.getDefaultSocialGrantSize();
    }
}