/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu16.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuExcludeStuDPOListExtract, Model>
{
}