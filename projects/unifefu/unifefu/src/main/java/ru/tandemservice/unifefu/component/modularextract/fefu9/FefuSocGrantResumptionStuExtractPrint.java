/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu9;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class FefuSocGrantResumptionStuExtractPrint implements IPrintFormCreator<FefuSocGrantResumptionStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuSocGrantResumptionStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("orderNumber", extract.getSocGrantOrder());
        modifier.put("orderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getSocGrantOrderDate()));
        modifier.put("beginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPayResumeDate()));
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}