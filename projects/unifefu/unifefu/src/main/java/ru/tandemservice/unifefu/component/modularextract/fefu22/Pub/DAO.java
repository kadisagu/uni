/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu22.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;

/**
 * @author Ekaterina Zvereva
 * @since 22.01.2015
 */
public class DAO extends ModularStudentExtractPubDAO<FefuOrderContingentStuDPOExtract, Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.getExtract().isIndividual())
            model.setPrintFormFileName((String) getProperty(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.P_FILE_NAME,
                                                            FefuOrderToPrintFormRelation.L_ORDER,model.getExtract().getParagraph().getOrder()));
        model.setDpoProgramOld((FefuAdditionalProfessionalEducationProgram)getProperty(FefuAdditionalProfessionalEducationProgramForStudent.class, FefuAdditionalProfessionalEducationProgramForStudent.L_PROGRAM,
                            FefuAdditionalProfessionalEducationProgramForStudent.L_STUDENT, model.getExtract().getEntity()));
    }

}