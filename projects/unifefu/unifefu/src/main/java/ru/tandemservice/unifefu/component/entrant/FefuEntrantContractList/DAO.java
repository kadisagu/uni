/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.entrant.FefuEntrantContractList;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.unifefu.entity.FefuEntrantContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 01.04.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    public void prepareListDataSource(Model model)
    {
        StaticListDataSource<ViewWrapper<RequestedEnrollmentDirection>> dataSource = new StaticListDataSource<>();

        Map<RequestedEnrollmentDirection, FefuEntrantContract> contractMap = new HashMap<>();
        for (FefuEntrantContract contract : getList(FefuEntrantContract.class, FefuEntrantContract.requestedEnrollmentDirection().entrantRequest().entrant().id(), model.getId()))
        {
            contractMap.put(contract.getRequestedEnrollmentDirection(), contract);
        }

        FefuEntrantContract contract;
        boolean hasContract;

        List<ViewWrapper<RequestedEnrollmentDirection>> wrappers = new ArrayList<>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "d")
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("d")), DQLExpressions.value(model.getId())))
                .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.compensationType().code().fromAlias("d")), DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)));

        for (RequestedEnrollmentDirection direction : builder.createStatement(getSession()).<RequestedEnrollmentDirection>list())
        {
            ViewWrapper<RequestedEnrollmentDirection> wrapper = new ViewWrapper<>(direction);

            hasContract = (contract = contractMap.get(direction)) != null;

            wrapper.setViewProperty("contractNumber", hasContract ? contract.getContractNumber() : null);
            wrapper.setViewProperty("contractDate", hasContract ? contract.getContractDate() : null);
            wrapper.setViewProperty("contractPayer", hasContract ? contract.getContractPayer() : null);
            wrapper.setViewProperty("contractTerminated", hasContract ? contract.isContractTerminated() : null);
            wrapper.setViewProperty("deleteDisabled", !hasContract);

            wrappers.add(wrapper);
        }

        dataSource.setRowList(wrappers);

        model.setDataSource(dataSource);
    }

    @Override
    public void deleteContract(Long directionId)
    {
        FefuEntrantContract contract = get(FefuEntrantContract.class, FefuEntrantContract.requestedEnrollmentDirection().id(), directionId);
        delete(contract);
    }

    @Override
    public void updateAdvancePayed(Long directionId)
    {
        FefuEntrantContract contract = get(FefuEntrantContract.class, FefuEntrantContract.requestedEnrollmentDirection().id(), directionId);
        contract.setContractTerminated(!contract.isContractTerminated());
        update(contract);
    }
}