/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e64.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.e64.AddEdit.Model;

/**
 * @author Alexey Lopatin
 * @since 27.08.2013
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e64.AddEdit.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if(model.isAddForm())
        {
            model.getExtract().setPassStateExam(true);
        }
    }

    @Override
    public void update(Model model)
    {
        if (!model.getExtract().getPassStateExam())
        {
            model.getExtract().setPluralForStateExam(false);
        }
        super.update(model);
    }
}