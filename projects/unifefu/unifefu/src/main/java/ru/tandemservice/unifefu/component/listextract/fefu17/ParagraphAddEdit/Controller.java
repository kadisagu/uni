/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu17.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.01.2015
 */
public class Controller extends AbstractListParagraphAddEditController<FefuTransfStuDPOListExtract, IDAO, Model>
{
        public void onChangeOldDPOProgram(IBusinessComponent component)
        {
            getDao().changeProgramOld(getModel(component));
        }
}