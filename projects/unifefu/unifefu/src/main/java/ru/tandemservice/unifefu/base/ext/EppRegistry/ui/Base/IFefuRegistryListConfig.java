/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EppRegistry.ui.Base;

/**
 * @author Irina Ugfeld
 * @since 15.03.2016
 */
public interface IFefuRegistryListConfig {
    String EPP_REGISTRY_STRUCTURE_DS = "eppRegistryStructureDS";
    String EPP_F_CONTROL_ACTION_TYPE_DS = "eppFControlActionTypeDS";
    String SUBJECT_INDEX_DS = "subjectIndexDS";
    String EDU_ORG_UNIT_DS = "eduOrgUnitDS";
}