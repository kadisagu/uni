package ru.tandemservice.unifefu.events.dset;

import org.hibernate.Session;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.unifefu.dao.daemon.FEFUMdbViewOrdersDaemonDAO;
import ru.tandemservice.unifefu.entity.MdbViewPrikazy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 */
public class FefuOrderChangeListener extends ParamTransactionCompleteListener<Boolean>
{

    @SuppressWarnings("unchecked")
    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, AbstractStudentExtract.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, AbstractStudentExtract.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, AbstractStudentOrder.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, AbstractStudentOrder.class, this);
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        List<Long> extractIds = new ArrayList<>();
        List<Long> orderIds = new ArrayList<>();

        for (Long id : params)
        {
            IEntityMeta meta = EntityRuntime.getMeta(id);
            if (isExtract(meta))
            {
                extractIds.add(id);
            }
            else if(isOrder(meta))
            {
                orderIds.add(id);
            }

        }

        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(MdbViewPrikazy.class, "p")
                .column(property(MdbViewPrikazy.extract().id().fromAlias("p")));
        eBuilder.where(in(property(MdbViewPrikazy.order().id().fromAlias("p")), orderIds));

        List<Long> additionalIds = eBuilder.createStatement(session).list();
        extractIds.addAll(additionalIds);

        DQLDeleteBuilder deleteBuilder1 = new DQLDeleteBuilder(MdbViewPrikazy.class);
        deleteBuilder1.where(in(property(MdbViewPrikazy.extract().id()), extractIds));
        deleteBuilder1.createStatement(session).execute();
        session.flush();

        FEFUMdbViewOrdersDaemonDAO.DAEMON.registerAfterCompleteWakeUp(session);
        return true;
    }

    @Override
    public void afterCompletion(Session session, int status, Collection<Long> params, Boolean beforeCompletionResult)
    {

    }

    private boolean isExtract(IEntityMeta entity)
    {
       if (entity.getEntityClass().equals(AbstractStudentExtract.class))
             return true;

        if (entity.getParent()==null)
             return false;

        if (entity.getParent().getEntityClass().equals(AbstractStudentExtract.class))
             return true;

        if (entity.getParent().getParent()==null)
             return false;

        if (entity.getParent().getParent().getEntityClass().equals(AbstractStudentExtract.class))
             return true;

        if (entity.getParent().getParent().getParent()==null)
             return false;

        if (entity.getParent().getParent().getParent().getEntityClass().equals(AbstractStudentExtract.class))
             return true;

       return false;
    }

    private boolean isOrder(IEntityMeta entity)
        {
           if (entity.getEntityClass().equals(AbstractStudentOrder.class))
                 return true;

            if (entity.getParent()==null)
                 return false;

            if (entity.getParent().getEntityClass().equals(AbstractStudentOrder.class))
                 return true;

            if (entity.getParent().getParent()==null)
                 return false;

            if (entity.getParent().getParent().getEntityClass().equals(AbstractStudentOrder.class))
                 return true;

            if (entity.getParent().getParent().getParent()==null)
                 return false;

            if (entity.getParent().getParent().getParent().getEntityClass().equals(AbstractStudentOrder.class))
                 return true;

           return false;
        }
}
