package ru.tandemservice.unifefu.base.ext.TrJournal.ui.StructureView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureView.TrJournalStructureViewUI;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: amakarova
 * Date: 13.11.13
 */
public class TrJournalStructureViewExtUI extends UIAddon
{
    public TrJournalStructureViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        TrJournalStructureViewUI presenter = getPresenter();
        final Map<Long, FefuRow> rowMap = new HashMap<>();
        StaticListDataSource<TrJournalStructureViewUI.Row> dataSource = presenter.getDataSource();
        List<TrJournalStructureViewUI.Row> rows = presenter.getDataSource().getRowList();
        Double allWeight = 0.0;
        for (TrJournalStructureViewUI.Row row : rows)
        {
            if (row.getEntity() instanceof TrJournalEvent)
            {
                for (TrBrsCoefficientValue value : DataAccessServices.dao().getList(TrBrsCoefficientValue.class, TrBrsCoefficientValue.owner().s(), row.getEntity()))
                {
                    if (value != null && value.getDefinition().getUserCode().equals(FefuBrs.WEIGHT_EVENT_CODE))
                        allWeight += (Double) value.getValue();
                }
            }
        }
        for (TrJournalStructureViewUI.Row row : rows)
        {
            if (row.getEntity() instanceof TrJournalEvent)
            {
                TrJournalEvent event = (TrJournalEvent) row.getEntity();
                Map<String, TrBrsCoefficientValue> values = new HashMap<>();
                for (TrBrsCoefficientValue value : DataAccessServices.dao().getList(TrBrsCoefficientValue.class, TrBrsCoefficientValue.owner().s(), event))
                {
                    values.put(value.getDefinition().getUserCode(), value);
                }
                Double weight = !values.containsKey(FefuBrs.WEIGHT_EVENT_CODE) ? 0.0 : (Double) values.get(FefuBrs.WEIGHT_EVENT_CODE).getValue();
                String sValueWeight = String.valueOf(weight);
                Double normWeight = allWeight != 0.0 ? weight / allWeight * 100 : 0.0;
                String sNormWeight = String.valueOf(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(normWeight));
                String max = !values.containsKey(FefuBrs.MAX_POINTS_CODE) ? "" : values.get(FefuBrs.MAX_POINTS_CODE).getValue().toString();
                String min = !values.containsKey(FefuBrs.MIN_POINTS_CODE) ? "" : values.get(FefuBrs.MIN_POINTS_CODE).getValue().toString();
                String dateBegin = "";
                String dateEnd = "";
                TrEduGroupEvent groupEvent = getTrEduGroupEvent(event);
                if (groupEvent != null && groupEvent.getScheduleEvent() != null)
                {
                    dateBegin = groupEvent.getScheduleEvent().getBeginDateStr();
                    dateEnd = groupEvent.getScheduleEvent().getEndDateStr();
                }
                FefuRow newRow = new FefuRow(sValueWeight, sNormWeight, max, min, dateBegin, dateEnd);
                rowMap.put(row.getId(), newRow);
            }
        }

        dataSource.addColumn(new SimpleColumn("Весовой коэфф.", "getValueWeight")
        {
            @Override
            public String getContent(IEntity entity)
            {
                FefuRow curRow = rowMap.get(entity.getId());
                if (curRow != null)
                    return curRow.getValueWeight();
                return "";
            }
        }.setAlign("right").setHeaderAlign("center").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вес (%)", "getNormWeight")
        {
            @Override
            public String getContent(IEntity entity)
            {
                FefuRow curRow = rowMap.get(entity.getId());
                if (curRow != null)
                    return curRow.getNormWeight();
                return "";
            }
        }.setAlign("right").setHeaderAlign("center").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Макс. балл", "getValueMax")
        {
            @Override
            public String getContent(IEntity entity)
            {
                FefuRow curRow = rowMap.get(entity.getId());
                if (curRow != null)
                    return curRow.getValueMax();
                return "";
            }
        }.setAlign("right").setHeaderAlign("center").setOrderable(false).setVisible(true));
        dataSource.addColumn(new SimpleColumn("Мин. балл", "getValueMin")
        {
            @Override
            public String getContent(IEntity entity)
            {
                FefuRow curRow = rowMap.get(entity.getId());
                if (curRow != null)
                    return curRow.getValueMin();
                return "";
            }
        }.setAlign("right").setHeaderAlign("center").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата проведения", "getDateBegin")
        {
            @Override
            public String getContent(IEntity entity)
            {
                FefuRow curRow = rowMap.get(entity.getId());
                if (curRow != null)
                    return curRow.getDateBegin();
                return "";
            }
        }.setHeaderAlign("center").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Конечная дата выставления оценки", "getDateEnd")
        {
            @Override
            public String getContent(IEntity entity)
            {
                FefuRow curRow = rowMap.get(entity.getId());
                if (curRow != null)
                    return curRow.getDateEnd();
                return "";
            }
        }.setHeaderAlign("center").setOrderable(false));
    }

    private class FefuRow
    {
        public String getValueWeight()
        {
            return valueWeight;
        }

        public String getNormWeight()
        {
            return normWeight;
        }

        public String getValueMax()
        {
            return valueMax;
        }

        public String getValueMin()
        {
            return valueMin;
        }

        private final String valueWeight;
        private final String normWeight;
        private final String valueMax;
        private final String valueMin;

        public String getDateBegin()
        {
            return dateBegin;
        }

        public String getDateEnd()
        {
            return dateEnd;
        }

        private final String dateBegin;
        private final String dateEnd;

        public FefuRow(String valueWeight, String normWeight, String valueMax, String valueMin, String dateBegin, String dateEnd)
        {
            this.valueWeight = valueWeight;
            this.normWeight = normWeight;
            this.valueMax = valueMax;
            this.valueMin = valueMin;
            this.dateBegin = dateBegin;
            this.dateEnd = dateEnd;
        }
    }

    public TrEduGroupEvent getTrEduGroupEvent(TrJournalEvent event)
    {
        DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ge")
                .column("ge").top(1)
                .where(DQLExpressions.eq(property(TrEduGroupEvent.journalEvent().fromAlias("ge")), value(event)));

        return query.createStatement(getSession()).uniqueResult();
    }
}