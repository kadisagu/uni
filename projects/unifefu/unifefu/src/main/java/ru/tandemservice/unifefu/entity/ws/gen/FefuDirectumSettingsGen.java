package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка параметров Directum для типа приказа по студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuDirectumSettingsGen extends EntityBase
 implements INaturalIdentifiable<FefuDirectumSettingsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings";
    public static final String ENTITY_NAME = "fefuDirectumSettings";
    public static final int VERSION_HASH = 325160127;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_EXTRACT_TYPE = "studentExtractType";
    public static final String L_FEFU_DIRECTUM_ORDER_TYPE = "fefuDirectumOrderType";
    public static final String P_DISMISS = "dismiss";
    public static final String P_RESTORE = "restore";
    public static final String P_GRANT_MAT_AID = "grantMatAid";
    public static final String P_SOCIAL = "social";
    public static final String P_PRACTICE = "practice";
    public static final String P_GROUP_MANAGER = "groupManager";
    public static final String P_PENALTY = "penalty";
    public static final String P_MAGISTER_PROGRAM_FIXING = "magisterProgramFixing";
    public static final String P_PAYMENTS = "payments";
    public static final String P_STATE_FORMULAR = "stateFormular";
    public static final String P_CHECK_MEDICAL_DOCS = "checkMedicalDocs";
    public static final String P_SIGNER = "signer";

    private StudentExtractType _studentExtractType;     // Тип приказа
    private FefuDirectumOrderType _fefuDirectumOrderType;     // Вид приказа в Directum
    private boolean _dismiss;     // Приказ на отчисление с связи с окон.обуч.(выпуск)
    private boolean _restore;     // Приказ на восстановление студентов
    private boolean _grantMatAid;     // Приказ о назначении стипендии / материальной помощи / восстановлению стипендии
    private boolean _social;     // Приказ о пособии по беременности / академическом отпуске с приостановлением выплаты стипендии / по студентам-сиротам (ДМП)
    private boolean _practice;     // Приказ по практикам
    private boolean _groupManager;     // Приказ о назначении / снятии старост групп
    private boolean _penalty;     // О наложении взыскания
    private boolean _magisterProgramFixing;     // Приказ о закреплении за магистерской программой
    private boolean _payments;     // Приказ по выплатам (СПО)
    private boolean _stateFormular;     // Документ гос образца (ДПО)
    private boolean _checkMedicalDocs;     // Проверка мед. справок / заключений ВК
    private String _signer;     // Подписывает

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     */
    @NotNull
    public StudentExtractType getStudentExtractType()
    {
        return _studentExtractType;
    }

    /**
     * @param studentExtractType Тип приказа. Свойство не может быть null.
     */
    public void setStudentExtractType(StudentExtractType studentExtractType)
    {
        dirty(_studentExtractType, studentExtractType);
        _studentExtractType = studentExtractType;
    }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     */
    @NotNull
    public FefuDirectumOrderType getFefuDirectumOrderType()
    {
        return _fefuDirectumOrderType;
    }

    /**
     * @param fefuDirectumOrderType Вид приказа в Directum. Свойство не может быть null.
     */
    public void setFefuDirectumOrderType(FefuDirectumOrderType fefuDirectumOrderType)
    {
        dirty(_fefuDirectumOrderType, fefuDirectumOrderType);
        _fefuDirectumOrderType = fefuDirectumOrderType;
    }

    /**
     * @return Приказ на отчисление с связи с окон.обуч.(выпуск). Свойство не может быть null.
     */
    @NotNull
    public boolean isDismiss()
    {
        return _dismiss;
    }

    /**
     * @param dismiss Приказ на отчисление с связи с окон.обуч.(выпуск). Свойство не может быть null.
     */
    public void setDismiss(boolean dismiss)
    {
        dirty(_dismiss, dismiss);
        _dismiss = dismiss;
    }

    /**
     * @return Приказ на восстановление студентов. Свойство не может быть null.
     */
    @NotNull
    public boolean isRestore()
    {
        return _restore;
    }

    /**
     * @param restore Приказ на восстановление студентов. Свойство не может быть null.
     */
    public void setRestore(boolean restore)
    {
        dirty(_restore, restore);
        _restore = restore;
    }

    /**
     * @return Приказ о назначении стипендии / материальной помощи / восстановлению стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isGrantMatAid()
    {
        return _grantMatAid;
    }

    /**
     * @param grantMatAid Приказ о назначении стипендии / материальной помощи / восстановлению стипендии. Свойство не может быть null.
     */
    public void setGrantMatAid(boolean grantMatAid)
    {
        dirty(_grantMatAid, grantMatAid);
        _grantMatAid = grantMatAid;
    }

    /**
     * @return Приказ о пособии по беременности / академическом отпуске с приостановлением выплаты стипендии / по студентам-сиротам (ДМП). Свойство не может быть null.
     */
    @NotNull
    public boolean isSocial()
    {
        return _social;
    }

    /**
     * @param social Приказ о пособии по беременности / академическом отпуске с приостановлением выплаты стипендии / по студентам-сиротам (ДМП). Свойство не может быть null.
     */
    public void setSocial(boolean social)
    {
        dirty(_social, social);
        _social = social;
    }

    /**
     * @return Приказ по практикам. Свойство не может быть null.
     */
    @NotNull
    public boolean isPractice()
    {
        return _practice;
    }

    /**
     * @param practice Приказ по практикам. Свойство не может быть null.
     */
    public void setPractice(boolean practice)
    {
        dirty(_practice, practice);
        _practice = practice;
    }

    /**
     * @return Приказ о назначении / снятии старост групп. Свойство не может быть null.
     */
    @NotNull
    public boolean isGroupManager()
    {
        return _groupManager;
    }

    /**
     * @param groupManager Приказ о назначении / снятии старост групп. Свойство не может быть null.
     */
    public void setGroupManager(boolean groupManager)
    {
        dirty(_groupManager, groupManager);
        _groupManager = groupManager;
    }

    /**
     * @return О наложении взыскания. Свойство не может быть null.
     */
    @NotNull
    public boolean isPenalty()
    {
        return _penalty;
    }

    /**
     * @param penalty О наложении взыскания. Свойство не может быть null.
     */
    public void setPenalty(boolean penalty)
    {
        dirty(_penalty, penalty);
        _penalty = penalty;
    }

    /**
     * @return Приказ о закреплении за магистерской программой. Свойство не может быть null.
     */
    @NotNull
    public boolean isMagisterProgramFixing()
    {
        return _magisterProgramFixing;
    }

    /**
     * @param magisterProgramFixing Приказ о закреплении за магистерской программой. Свойство не может быть null.
     */
    public void setMagisterProgramFixing(boolean magisterProgramFixing)
    {
        dirty(_magisterProgramFixing, magisterProgramFixing);
        _magisterProgramFixing = magisterProgramFixing;
    }

    /**
     * @return Приказ по выплатам (СПО). Свойство не может быть null.
     */
    @NotNull
    public boolean isPayments()
    {
        return _payments;
    }

    /**
     * @param payments Приказ по выплатам (СПО). Свойство не может быть null.
     */
    public void setPayments(boolean payments)
    {
        dirty(_payments, payments);
        _payments = payments;
    }

    /**
     * @return Документ гос образца (ДПО). Свойство не может быть null.
     */
    @NotNull
    public boolean isStateFormular()
    {
        return _stateFormular;
    }

    /**
     * @param stateFormular Документ гос образца (ДПО). Свойство не может быть null.
     */
    public void setStateFormular(boolean stateFormular)
    {
        dirty(_stateFormular, stateFormular);
        _stateFormular = stateFormular;
    }

    /**
     * @return Проверка мед. справок / заключений ВК. Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckMedicalDocs()
    {
        return _checkMedicalDocs;
    }

    /**
     * @param checkMedicalDocs Проверка мед. справок / заключений ВК. Свойство не может быть null.
     */
    public void setCheckMedicalDocs(boolean checkMedicalDocs)
    {
        dirty(_checkMedicalDocs, checkMedicalDocs);
        _checkMedicalDocs = checkMedicalDocs;
    }

    /**
     * @return Подписывает.
     */
    @Length(max=255)
    public String getSigner()
    {
        return _signer;
    }

    /**
     * @param signer Подписывает.
     */
    public void setSigner(String signer)
    {
        dirty(_signer, signer);
        _signer = signer;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuDirectumSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setStudentExtractType(((FefuDirectumSettings)another).getStudentExtractType());
                setFefuDirectumOrderType(((FefuDirectumSettings)another).getFefuDirectumOrderType());
            }
            setDismiss(((FefuDirectumSettings)another).isDismiss());
            setRestore(((FefuDirectumSettings)another).isRestore());
            setGrantMatAid(((FefuDirectumSettings)another).isGrantMatAid());
            setSocial(((FefuDirectumSettings)another).isSocial());
            setPractice(((FefuDirectumSettings)another).isPractice());
            setGroupManager(((FefuDirectumSettings)another).isGroupManager());
            setPenalty(((FefuDirectumSettings)another).isPenalty());
            setMagisterProgramFixing(((FefuDirectumSettings)another).isMagisterProgramFixing());
            setPayments(((FefuDirectumSettings)another).isPayments());
            setStateFormular(((FefuDirectumSettings)another).isStateFormular());
            setCheckMedicalDocs(((FefuDirectumSettings)another).isCheckMedicalDocs());
            setSigner(((FefuDirectumSettings)another).getSigner());
        }
    }

    public INaturalId<FefuDirectumSettingsGen> getNaturalId()
    {
        return new NaturalId(getStudentExtractType(), getFefuDirectumOrderType());
    }

    public static class NaturalId extends NaturalIdBase<FefuDirectumSettingsGen>
    {
        private static final String PROXY_NAME = "FefuDirectumSettingsNaturalProxy";

        private Long _studentExtractType;
        private Long _fefuDirectumOrderType;

        public NaturalId()
        {}

        public NaturalId(StudentExtractType studentExtractType, FefuDirectumOrderType fefuDirectumOrderType)
        {
            _studentExtractType = ((IEntity) studentExtractType).getId();
            _fefuDirectumOrderType = ((IEntity) fefuDirectumOrderType).getId();
        }

        public Long getStudentExtractType()
        {
            return _studentExtractType;
        }

        public void setStudentExtractType(Long studentExtractType)
        {
            _studentExtractType = studentExtractType;
        }

        public Long getFefuDirectumOrderType()
        {
            return _fefuDirectumOrderType;
        }

        public void setFefuDirectumOrderType(Long fefuDirectumOrderType)
        {
            _fefuDirectumOrderType = fefuDirectumOrderType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuDirectumSettingsGen.NaturalId) ) return false;

            FefuDirectumSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudentExtractType(), that.getStudentExtractType()) ) return false;
            if( !equals(getFefuDirectumOrderType(), that.getFefuDirectumOrderType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudentExtractType());
            result = hashCode(result, getFefuDirectumOrderType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudentExtractType());
            sb.append("/");
            sb.append(getFefuDirectumOrderType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuDirectumSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuDirectumSettings.class;
        }

        public T newInstance()
        {
            return (T) new FefuDirectumSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentExtractType":
                    return obj.getStudentExtractType();
                case "fefuDirectumOrderType":
                    return obj.getFefuDirectumOrderType();
                case "dismiss":
                    return obj.isDismiss();
                case "restore":
                    return obj.isRestore();
                case "grantMatAid":
                    return obj.isGrantMatAid();
                case "social":
                    return obj.isSocial();
                case "practice":
                    return obj.isPractice();
                case "groupManager":
                    return obj.isGroupManager();
                case "penalty":
                    return obj.isPenalty();
                case "magisterProgramFixing":
                    return obj.isMagisterProgramFixing();
                case "payments":
                    return obj.isPayments();
                case "stateFormular":
                    return obj.isStateFormular();
                case "checkMedicalDocs":
                    return obj.isCheckMedicalDocs();
                case "signer":
                    return obj.getSigner();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentExtractType":
                    obj.setStudentExtractType((StudentExtractType) value);
                    return;
                case "fefuDirectumOrderType":
                    obj.setFefuDirectumOrderType((FefuDirectumOrderType) value);
                    return;
                case "dismiss":
                    obj.setDismiss((Boolean) value);
                    return;
                case "restore":
                    obj.setRestore((Boolean) value);
                    return;
                case "grantMatAid":
                    obj.setGrantMatAid((Boolean) value);
                    return;
                case "social":
                    obj.setSocial((Boolean) value);
                    return;
                case "practice":
                    obj.setPractice((Boolean) value);
                    return;
                case "groupManager":
                    obj.setGroupManager((Boolean) value);
                    return;
                case "penalty":
                    obj.setPenalty((Boolean) value);
                    return;
                case "magisterProgramFixing":
                    obj.setMagisterProgramFixing((Boolean) value);
                    return;
                case "payments":
                    obj.setPayments((Boolean) value);
                    return;
                case "stateFormular":
                    obj.setStateFormular((Boolean) value);
                    return;
                case "checkMedicalDocs":
                    obj.setCheckMedicalDocs((Boolean) value);
                    return;
                case "signer":
                    obj.setSigner((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentExtractType":
                        return true;
                case "fefuDirectumOrderType":
                        return true;
                case "dismiss":
                        return true;
                case "restore":
                        return true;
                case "grantMatAid":
                        return true;
                case "social":
                        return true;
                case "practice":
                        return true;
                case "groupManager":
                        return true;
                case "penalty":
                        return true;
                case "magisterProgramFixing":
                        return true;
                case "payments":
                        return true;
                case "stateFormular":
                        return true;
                case "checkMedicalDocs":
                        return true;
                case "signer":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentExtractType":
                    return true;
                case "fefuDirectumOrderType":
                    return true;
                case "dismiss":
                    return true;
                case "restore":
                    return true;
                case "grantMatAid":
                    return true;
                case "social":
                    return true;
                case "practice":
                    return true;
                case "groupManager":
                    return true;
                case "penalty":
                    return true;
                case "magisterProgramFixing":
                    return true;
                case "payments":
                    return true;
                case "stateFormular":
                    return true;
                case "checkMedicalDocs":
                    return true;
                case "signer":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentExtractType":
                    return StudentExtractType.class;
                case "fefuDirectumOrderType":
                    return FefuDirectumOrderType.class;
                case "dismiss":
                    return Boolean.class;
                case "restore":
                    return Boolean.class;
                case "grantMatAid":
                    return Boolean.class;
                case "social":
                    return Boolean.class;
                case "practice":
                    return Boolean.class;
                case "groupManager":
                    return Boolean.class;
                case "penalty":
                    return Boolean.class;
                case "magisterProgramFixing":
                    return Boolean.class;
                case "payments":
                    return Boolean.class;
                case "stateFormular":
                    return Boolean.class;
                case "checkMedicalDocs":
                    return Boolean.class;
                case "signer":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuDirectumSettings> _dslPath = new Path<FefuDirectumSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuDirectumSettings");
    }
            

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#getStudentExtractType()
     */
    public static StudentExtractType.Path<StudentExtractType> studentExtractType()
    {
        return _dslPath.studentExtractType();
    }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#getFefuDirectumOrderType()
     */
    public static FefuDirectumOrderType.Path<FefuDirectumOrderType> fefuDirectumOrderType()
    {
        return _dslPath.fefuDirectumOrderType();
    }

    /**
     * @return Приказ на отчисление с связи с окон.обуч.(выпуск). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isDismiss()
     */
    public static PropertyPath<Boolean> dismiss()
    {
        return _dslPath.dismiss();
    }

    /**
     * @return Приказ на восстановление студентов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isRestore()
     */
    public static PropertyPath<Boolean> restore()
    {
        return _dslPath.restore();
    }

    /**
     * @return Приказ о назначении стипендии / материальной помощи / восстановлению стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isGrantMatAid()
     */
    public static PropertyPath<Boolean> grantMatAid()
    {
        return _dslPath.grantMatAid();
    }

    /**
     * @return Приказ о пособии по беременности / академическом отпуске с приостановлением выплаты стипендии / по студентам-сиротам (ДМП). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isSocial()
     */
    public static PropertyPath<Boolean> social()
    {
        return _dslPath.social();
    }

    /**
     * @return Приказ по практикам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isPractice()
     */
    public static PropertyPath<Boolean> practice()
    {
        return _dslPath.practice();
    }

    /**
     * @return Приказ о назначении / снятии старост групп. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isGroupManager()
     */
    public static PropertyPath<Boolean> groupManager()
    {
        return _dslPath.groupManager();
    }

    /**
     * @return О наложении взыскания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isPenalty()
     */
    public static PropertyPath<Boolean> penalty()
    {
        return _dslPath.penalty();
    }

    /**
     * @return Приказ о закреплении за магистерской программой. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isMagisterProgramFixing()
     */
    public static PropertyPath<Boolean> magisterProgramFixing()
    {
        return _dslPath.magisterProgramFixing();
    }

    /**
     * @return Приказ по выплатам (СПО). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isPayments()
     */
    public static PropertyPath<Boolean> payments()
    {
        return _dslPath.payments();
    }

    /**
     * @return Документ гос образца (ДПО). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isStateFormular()
     */
    public static PropertyPath<Boolean> stateFormular()
    {
        return _dslPath.stateFormular();
    }

    /**
     * @return Проверка мед. справок / заключений ВК. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isCheckMedicalDocs()
     */
    public static PropertyPath<Boolean> checkMedicalDocs()
    {
        return _dslPath.checkMedicalDocs();
    }

    /**
     * @return Подписывает.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#getSigner()
     */
    public static PropertyPath<String> signer()
    {
        return _dslPath.signer();
    }

    public static class Path<E extends FefuDirectumSettings> extends EntityPath<E>
    {
        private StudentExtractType.Path<StudentExtractType> _studentExtractType;
        private FefuDirectumOrderType.Path<FefuDirectumOrderType> _fefuDirectumOrderType;
        private PropertyPath<Boolean> _dismiss;
        private PropertyPath<Boolean> _restore;
        private PropertyPath<Boolean> _grantMatAid;
        private PropertyPath<Boolean> _social;
        private PropertyPath<Boolean> _practice;
        private PropertyPath<Boolean> _groupManager;
        private PropertyPath<Boolean> _penalty;
        private PropertyPath<Boolean> _magisterProgramFixing;
        private PropertyPath<Boolean> _payments;
        private PropertyPath<Boolean> _stateFormular;
        private PropertyPath<Boolean> _checkMedicalDocs;
        private PropertyPath<String> _signer;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#getStudentExtractType()
     */
        public StudentExtractType.Path<StudentExtractType> studentExtractType()
        {
            if(_studentExtractType == null )
                _studentExtractType = new StudentExtractType.Path<StudentExtractType>(L_STUDENT_EXTRACT_TYPE, this);
            return _studentExtractType;
        }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#getFefuDirectumOrderType()
     */
        public FefuDirectumOrderType.Path<FefuDirectumOrderType> fefuDirectumOrderType()
        {
            if(_fefuDirectumOrderType == null )
                _fefuDirectumOrderType = new FefuDirectumOrderType.Path<FefuDirectumOrderType>(L_FEFU_DIRECTUM_ORDER_TYPE, this);
            return _fefuDirectumOrderType;
        }

    /**
     * @return Приказ на отчисление с связи с окон.обуч.(выпуск). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isDismiss()
     */
        public PropertyPath<Boolean> dismiss()
        {
            if(_dismiss == null )
                _dismiss = new PropertyPath<Boolean>(FefuDirectumSettingsGen.P_DISMISS, this);
            return _dismiss;
        }

    /**
     * @return Приказ на восстановление студентов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isRestore()
     */
        public PropertyPath<Boolean> restore()
        {
            if(_restore == null )
                _restore = new PropertyPath<Boolean>(FefuDirectumSettingsGen.P_RESTORE, this);
            return _restore;
        }

    /**
     * @return Приказ о назначении стипендии / материальной помощи / восстановлению стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isGrantMatAid()
     */
        public PropertyPath<Boolean> grantMatAid()
        {
            if(_grantMatAid == null )
                _grantMatAid = new PropertyPath<Boolean>(FefuDirectumSettingsGen.P_GRANT_MAT_AID, this);
            return _grantMatAid;
        }

    /**
     * @return Приказ о пособии по беременности / академическом отпуске с приостановлением выплаты стипендии / по студентам-сиротам (ДМП). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isSocial()
     */
        public PropertyPath<Boolean> social()
        {
            if(_social == null )
                _social = new PropertyPath<Boolean>(FefuDirectumSettingsGen.P_SOCIAL, this);
            return _social;
        }

    /**
     * @return Приказ по практикам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isPractice()
     */
        public PropertyPath<Boolean> practice()
        {
            if(_practice == null )
                _practice = new PropertyPath<Boolean>(FefuDirectumSettingsGen.P_PRACTICE, this);
            return _practice;
        }

    /**
     * @return Приказ о назначении / снятии старост групп. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isGroupManager()
     */
        public PropertyPath<Boolean> groupManager()
        {
            if(_groupManager == null )
                _groupManager = new PropertyPath<Boolean>(FefuDirectumSettingsGen.P_GROUP_MANAGER, this);
            return _groupManager;
        }

    /**
     * @return О наложении взыскания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isPenalty()
     */
        public PropertyPath<Boolean> penalty()
        {
            if(_penalty == null )
                _penalty = new PropertyPath<Boolean>(FefuDirectumSettingsGen.P_PENALTY, this);
            return _penalty;
        }

    /**
     * @return Приказ о закреплении за магистерской программой. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isMagisterProgramFixing()
     */
        public PropertyPath<Boolean> magisterProgramFixing()
        {
            if(_magisterProgramFixing == null )
                _magisterProgramFixing = new PropertyPath<Boolean>(FefuDirectumSettingsGen.P_MAGISTER_PROGRAM_FIXING, this);
            return _magisterProgramFixing;
        }

    /**
     * @return Приказ по выплатам (СПО). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isPayments()
     */
        public PropertyPath<Boolean> payments()
        {
            if(_payments == null )
                _payments = new PropertyPath<Boolean>(FefuDirectumSettingsGen.P_PAYMENTS, this);
            return _payments;
        }

    /**
     * @return Документ гос образца (ДПО). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isStateFormular()
     */
        public PropertyPath<Boolean> stateFormular()
        {
            if(_stateFormular == null )
                _stateFormular = new PropertyPath<Boolean>(FefuDirectumSettingsGen.P_STATE_FORMULAR, this);
            return _stateFormular;
        }

    /**
     * @return Проверка мед. справок / заключений ВК. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#isCheckMedicalDocs()
     */
        public PropertyPath<Boolean> checkMedicalDocs()
        {
            if(_checkMedicalDocs == null )
                _checkMedicalDocs = new PropertyPath<Boolean>(FefuDirectumSettingsGen.P_CHECK_MEDICAL_DOCS, this);
            return _checkMedicalDocs;
        }

    /**
     * @return Подписывает.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings#getSigner()
     */
        public PropertyPath<String> signer()
        {
            if(_signer == null )
                _signer = new PropertyPath<String>(FefuDirectumSettingsGen.P_SIGNER, this);
            return _signer;
        }

        public Class getEntityClass()
        {
            return FefuDirectumSettings.class;
        }

        public String getEntityName()
        {
            return "fefuDirectumSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
