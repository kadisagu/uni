package ru.tandemservice.unifefu.entity.blackboard.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse2PpsEntryRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь ЭУК с преподавателем
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class BbCourse2PpsEntryRelGen extends EntityBase
 implements INaturalIdentifiable<BbCourse2PpsEntryRelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.blackboard.BbCourse2PpsEntryRel";
    public static final String ENTITY_NAME = "bbCourse2PpsEntryRel";
    public static final int VERSION_HASH = 781862266;
    private static IEntityMeta ENTITY_META;

    public static final String L_BB_COURSE = "bbCourse";
    public static final String L_PPS_ENTRY = "ppsEntry";

    private BbCourse _bbCourse;     // Электронный учебный курс (ЭУК) в системе Blackboard
    private PpsEntry _ppsEntry;     // Запись в реестре ППС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     */
    @NotNull
    public BbCourse getBbCourse()
    {
        return _bbCourse;
    }

    /**
     * @param bbCourse Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     */
    public void setBbCourse(BbCourse bbCourse)
    {
        dirty(_bbCourse, bbCourse);
        _bbCourse = bbCourse;
    }

    /**
     * @return Запись в реестре ППС. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getPpsEntry()
    {
        return _ppsEntry;
    }

    /**
     * @param ppsEntry Запись в реестре ППС. Свойство не может быть null.
     */
    public void setPpsEntry(PpsEntry ppsEntry)
    {
        dirty(_ppsEntry, ppsEntry);
        _ppsEntry = ppsEntry;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof BbCourse2PpsEntryRelGen)
        {
            if (withNaturalIdProperties)
            {
                setBbCourse(((BbCourse2PpsEntryRel)another).getBbCourse());
                setPpsEntry(((BbCourse2PpsEntryRel)another).getPpsEntry());
            }
        }
    }

    public INaturalId<BbCourse2PpsEntryRelGen> getNaturalId()
    {
        return new NaturalId(getBbCourse(), getPpsEntry());
    }

    public static class NaturalId extends NaturalIdBase<BbCourse2PpsEntryRelGen>
    {
        private static final String PROXY_NAME = "BbCourse2PpsEntryRelNaturalProxy";

        private Long _bbCourse;
        private Long _ppsEntry;

        public NaturalId()
        {}

        public NaturalId(BbCourse bbCourse, PpsEntry ppsEntry)
        {
            _bbCourse = ((IEntity) bbCourse).getId();
            _ppsEntry = ((IEntity) ppsEntry).getId();
        }

        public Long getBbCourse()
        {
            return _bbCourse;
        }

        public void setBbCourse(Long bbCourse)
        {
            _bbCourse = bbCourse;
        }

        public Long getPpsEntry()
        {
            return _ppsEntry;
        }

        public void setPpsEntry(Long ppsEntry)
        {
            _ppsEntry = ppsEntry;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof BbCourse2PpsEntryRelGen.NaturalId) ) return false;

            BbCourse2PpsEntryRelGen.NaturalId that = (NaturalId) o;

            if( !equals(getBbCourse(), that.getBbCourse()) ) return false;
            if( !equals(getPpsEntry(), that.getPpsEntry()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBbCourse());
            result = hashCode(result, getPpsEntry());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBbCourse());
            sb.append("/");
            sb.append(getPpsEntry());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends BbCourse2PpsEntryRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) BbCourse2PpsEntryRel.class;
        }

        public T newInstance()
        {
            return (T) new BbCourse2PpsEntryRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "bbCourse":
                    return obj.getBbCourse();
                case "ppsEntry":
                    return obj.getPpsEntry();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "bbCourse":
                    obj.setBbCourse((BbCourse) value);
                    return;
                case "ppsEntry":
                    obj.setPpsEntry((PpsEntry) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "bbCourse":
                        return true;
                case "ppsEntry":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "bbCourse":
                    return true;
                case "ppsEntry":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "bbCourse":
                    return BbCourse.class;
                case "ppsEntry":
                    return PpsEntry.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<BbCourse2PpsEntryRel> _dslPath = new Path<BbCourse2PpsEntryRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "BbCourse2PpsEntryRel");
    }
            

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2PpsEntryRel#getBbCourse()
     */
    public static BbCourse.Path<BbCourse> bbCourse()
    {
        return _dslPath.bbCourse();
    }

    /**
     * @return Запись в реестре ППС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2PpsEntryRel#getPpsEntry()
     */
    public static PpsEntry.Path<PpsEntry> ppsEntry()
    {
        return _dslPath.ppsEntry();
    }

    public static class Path<E extends BbCourse2PpsEntryRel> extends EntityPath<E>
    {
        private BbCourse.Path<BbCourse> _bbCourse;
        private PpsEntry.Path<PpsEntry> _ppsEntry;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2PpsEntryRel#getBbCourse()
     */
        public BbCourse.Path<BbCourse> bbCourse()
        {
            if(_bbCourse == null )
                _bbCourse = new BbCourse.Path<BbCourse>(L_BB_COURSE, this);
            return _bbCourse;
        }

    /**
     * @return Запись в реестре ППС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2PpsEntryRel#getPpsEntry()
     */
        public PpsEntry.Path<PpsEntry> ppsEntry()
        {
            if(_ppsEntry == null )
                _ppsEntry = new PpsEntry.Path<PpsEntry>(L_PPS_ENTRY, this);
            return _ppsEntry;
        }

        public Class getEntityClass()
        {
            return BbCourse2PpsEntryRel.class;
        }

        public String getEntityName()
        {
            return "bbCourse2PpsEntryRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
