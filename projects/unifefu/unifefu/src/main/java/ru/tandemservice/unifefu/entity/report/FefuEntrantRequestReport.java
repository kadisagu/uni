package ru.tandemservice.unifefu.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.IFefuEntrantRequestReport;
import ru.tandemservice.unifefu.entity.report.gen.*;

/**
 * Отчет по заявлениям абитуриентов (ДВФУ)
 */
public abstract class FefuEntrantRequestReport extends FefuEntrantRequestReportGen implements IFefuEntrantRequestReport
{
    @EntityDSLSupport
    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}