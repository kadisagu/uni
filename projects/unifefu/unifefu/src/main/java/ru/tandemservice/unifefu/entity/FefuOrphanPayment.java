package ru.tandemservice.unifefu.entity;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Выплаты детям-сиротам
 */
public class FefuOrphanPayment extends FefuOrphanPaymentGen
{
    // В полях с суммой хранится значение в сотых долях копейки
    final static int CURRENCY_ABBREVIATION_PART = 10000;

    public Double getPaymentSumInRuble()
    {
        if(null == getPaymentSum()) return null;
        return getPaymentSum() / Integer.valueOf(CURRENCY_ABBREVIATION_PART).doubleValue();
    }

    public void setPaymentSumInRuble(Double rubles)
    {
        if(null == rubles) setPaymentSum(null);
        setPaymentSum(Double.valueOf(rubles*CURRENCY_ABBREVIATION_PART).longValue());
    }

    public IdentifiableWrapper getPaymentMonthWrapper()
    {
        if(null == getPaymentMonth()) return null;
        return new IdentifiableWrapper(getPaymentMonth().longValue(), CommonBaseDateUtil.getMonthNameDeclined(getPaymentMonth(), GrammaCase.NOMINATIVE));
    }

    public void setPaymentMonthWrapper(IdentifiableWrapper paymentMonthWrapper)
    {
        if(null == paymentMonthWrapper)
            setPaymentMonth(null);
        else
            setPaymentMonth(paymentMonthWrapper.getId().intValue());
    }
}