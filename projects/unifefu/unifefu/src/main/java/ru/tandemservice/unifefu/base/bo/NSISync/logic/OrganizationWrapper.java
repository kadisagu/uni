/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.logic;

import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuTopOrgUnit;
import ru.tandemservice.unifefu.ws.nsi.datagram.OrganizationType;

/**
 * @author Dmitry Seleznev
 * @since 09.04.2014
 */
public class OrganizationWrapper
{
    public static final String ROOT_ELEMENT = "ROOT";

    private String _id;
    private String _title;
    private String _parentId;
    private FefuTopOrgUnit _fefuTopOrgUnit;
    private OrganizationType _organizationType;
    private FefuNsiIds _nsiIds;
    private String _guid;

    public OrganizationWrapper(String title, String parentId)
    {
        _id = String.valueOf(System.currentTimeMillis());
        _title = title;
        _parentId = parentId;
    }

    public OrganizationWrapper(FefuTopOrgUnit fefuTopOrgUnit, FefuNsiIds nsiIds)
    {
        _fefuTopOrgUnit = fefuTopOrgUnit;
        _title = fefuTopOrgUnit.getOrgUnit().getTitle();
        _id = fefuTopOrgUnit.getOrgUnit().getId().toString();
        if (null != fefuTopOrgUnit.getOrgUnit().getParent())
            _parentId = fefuTopOrgUnit.getOrgUnit().getParent().getId().toString();
        else _parentId = ROOT_ELEMENT;
        if (null != nsiIds) _guid = nsiIds.getGuid();
        _nsiIds = nsiIds;
    }

    public OrganizationWrapper(OrganizationType organizationType)
    {
        _organizationType = organizationType;
        _title = organizationType.getOrganizationName();
        _id = organizationType.getID();
        _guid = organizationType.getID();
        if (null != _organizationType.getOrganizationUpper())
            _parentId = _organizationType.getOrganizationUpper().getOrganization().getID();
        else _parentId = ROOT_ELEMENT;
    }

    public boolean isNsiWrapper()
    {
        return null != _organizationType;
    }

    @Override
    public int hashCode()
    {
        return getTitle().trim().toUpperCase().hashCode();
    }

    public String getId()
    {
        return _id;
    }

    public void setId(String id)
    {
        _id = id;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public String getParentId()
    {
        return _parentId;
    }

    public void setParentId(String parentId)
    {
        _parentId = parentId;
    }

    public FefuTopOrgUnit getFefuTopOrgUnit()
    {
        return _fefuTopOrgUnit;
    }

    public void setFefuTopOrgUnit(FefuTopOrgUnit fefuTopOrgUnit)
    {
        _fefuTopOrgUnit = fefuTopOrgUnit;
    }

    public OrganizationType getOrganizationType()
    {
        return _organizationType;
    }

    public void setOrganizationType(OrganizationType organizationType)
    {
        _organizationType = organizationType;
    }

    public FefuNsiIds getNsiIds()
    {
        return _nsiIds;
    }

    public void setNsiIds(FefuNsiIds nsiIds)
    {
        _nsiIds = nsiIds;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }
}