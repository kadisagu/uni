/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.GenericHandler;
import javax.xml.rpc.handler.MessageContext;
import javax.xml.rpc.handler.soap.SOAPMessageContext;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;

/**
 * @author Dmitry Seleznev
 * @since 26.09.2014
 */
public class NsiSecurityHandler extends GenericHandler
{
    @Override
    public boolean handleRequest(MessageContext context)
    {
        try
        {
            SOAPMessage msg = ((SOAPMessageContext) context).getMessage();
            MimeHeaders hd = msg.getMimeHeaders();
            String authorization = new String(Base64Coder.encode("CROCNSI\\ob:1qaz!QAZ".getBytes()));
            hd.addHeader("Authorization", "Basic " + authorization);
            return true;
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return super.handleRequest(context);
    }

    @Override
    public QName[] getHeaders()
    {
        return new QName[0];
    }
}