/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.gradebook;

import ru.tandemservice.unifefu.ws.blackboard.BBConstants;
import ru.tandemservice.unifefu.ws.blackboard.BBContextHelper;
import ru.tandemservice.unifefu.ws.blackboard.HeaderHandlerResolver;
import ru.tandemservice.unifefu.ws.blackboard.PortTypeInitializer;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.GradebookWS;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.GradebookWSPortType;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.ObjectFactory;

/**
 * @author Nikolay Fedorovskih
 * @since 17.03.2014
 */
public class BBGradebookInitializer extends PortTypeInitializer<GradebookWSPortType, ObjectFactory>
{
    private GradebookWS _gradebookWS;

    public BBGradebookInitializer(HeaderHandlerResolver headerHandlerResolver)
    {
        super(BBConstants.GRADEBOOOK_WS, headerHandlerResolver);
        _gradebookWS = new GradebookWS(BBContextHelper.getWSDL_resource_URL("Gradebook.wsdl"));
        _gradebookWS.setHandlerResolver(headerHandlerResolver);
    }

    @Override
    protected GradebookWSPortType initPortType()
    {
        return _gradebookWS.getGradebookWSSOAP11PortHttp();
    }

    @Override
    protected ObjectFactory initObjectFactory()
    {
        return new ObjectFactory();
    }
}