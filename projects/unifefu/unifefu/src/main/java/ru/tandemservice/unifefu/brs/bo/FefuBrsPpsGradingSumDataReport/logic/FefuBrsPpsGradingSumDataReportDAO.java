/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.FefuStudentMarkHistory;
import ru.tandemservice.unifefu.entity.catalog.FefuBrsDocTemplate;
import ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAction;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 12/16/13
 */
public class FefuBrsPpsGradingSumDataReportDAO extends BaseModifyAggregateDAO implements IFefuBrsPpsGradingSumDataReportDAO
{
    @Override
    public FefuBrsPpsGradingSumDataReport createReport(FefuBrsPpsGradingSumDataReportParams reportParams)
    {
        FefuBrsPpsGradingSumDataReport report = new FefuBrsPpsGradingSumDataReport();
        report.setOrgUnit(reportParams.getOrgUnit());
        report.setFormingDate(new Date());
        if(null != reportParams.getFormativeOrgUnit() && !reportParams.getFormativeOrgUnit().isEmpty())
            report.setFormativeOrgUnit(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getFormativeOrgUnit(), OrgUnit.title().s()), ", "));
        report.setYearPart(reportParams.getYearPart().getTitle());
        if(null != reportParams.getTutorOrgUnit() && !reportParams.getTutorOrgUnit().isEmpty())
            report.setTutorOrgUnit(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getTutorOrgUnit(), OrgUnit.title().s()), ", "));
        if(FefuBrsReportManager.CUT_REPORT_CURRENT_ID.equals(reportParams.getCutReport().getId()))
        {
            report.setCutReport("по состоянию на дату");
            report.setCheckDate(reportParams.getCheckDate());
        }
        else
            report.setCutReport("все");
        DatabaseFile content = new DatabaseFile();
        content.setContent(print(reportParams));
        content.setFilename("fefuPpsGradingSumDataReport");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        baseCreate(content);
        report.setContent(content);
        baseCreate(report);

        return report;
    }

    private byte[] print(FefuBrsPpsGradingSumDataReportParams reportParams)
    {
        FefuBrsDocTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(FefuBrsDocTemplate.class, "fefuBrsPpsGradingSumDataReport");
        RtfDocument document = new RtfReader().read(templateDocument.getContent());
        RtfInjectModifier modifier = new RtfInjectModifier();
        String formativeOU = "";
        if(null != reportParams.getFormativeOrgUnit() && !reportParams.getFormativeOrgUnit().isEmpty())
        {
            formativeOU = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getFormativeOrgUnit(), OrgUnit.title().s()), ", ");
        }
        String tutorOU = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getTutorOrgUnit(), OrgUnit.title().s()), ", ");
        String eduYearPart = reportParams.getYearPart().getTitle();
        String cutReport;
        String checkDate = "";
        if(FefuBrsReportManager.CUT_REPORT_CURRENT_ID.equals(reportParams.getCutReport().getId()))
        {
            cutReport = "по состоянию на дату";
            checkDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(reportParams.getCheckDate());
        }
        else
            cutReport = "все";

        modifier.put("reportParams", (!StringUtils.isEmpty(formativeOU) ? formativeOU + ", " : "" ) +  (!StringUtils.isEmpty(tutorOU) ? tutorOU + ", " : "" )
                + eduYearPart + ", учитывать оценки - " + cutReport + (FefuBrsReportManager.CUT_REPORT_CURRENT_ID.equals(reportParams.getCutReport().getId()) ? ", дата построения " + checkDate : ""));
        modifier.modify(document);
        fillTable(document, reportParams);
        return RtfUtil.toByteArray(document);
    }

    private void fillTable(RtfDocument document, final FefuBrsPpsGradingSumDataReportParams reportParams)
    {
        List<OrgUnit> formativeOUs = getFormativeOUParam(reportParams);

        Map<TrJournal, Map<EppGroupType, List<TrEventAction>>> journalActionsMap = Maps.newHashMap();
        Map<TrJournal, Map<EppRealEduGroup, List<TrEduGroupEventStudent>>> journalMarksMap = Maps.newHashMap();

        Map<PpsEntryByEmployeePost, Map<EppRealEduGroup, List<TrJournal>>> ppsJournalsMap = Maps.newHashMap();
        Map<EppRealEduGroup, List<EppRealEduGroup4LoadTypeRow>> groupStudentMap = Maps.newHashMap();

        // КМ
        DQLSelectBuilder eventsBuilder = getEventsBuilder(reportParams, formativeOUs);
        for(TrEventAction event : createStatement(eventsBuilder).<TrEventAction>list())
        {
            TrJournal journal = event.getJournalModule().getJournal();
            EppGroupType groupType = event.getType();
            if(!journalActionsMap.containsKey(journal))
                journalActionsMap.put(journal, Maps.<EppGroupType, List<TrEventAction>>newHashMap());
            if(!journalActionsMap.get(journal).containsKey(groupType))
                journalActionsMap.get(journal).put(groupType, Lists.<TrEventAction>newArrayList());
            if(!journalActionsMap.get(journal).get(groupType).contains(event))
                journalActionsMap.get(journal).get(groupType).add(event);
        }

        // ОЦЕНКИ
//        TrEventAction.class, "ea"
        DQLSelectBuilder marksBuilder = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "m");
        marksBuilder.fetchPath(DQLJoinType.inner, TrEduGroupEventStudent.event().group().fromAlias("m"), "eduGroup");
        marksBuilder.fetchPath(DQLJoinType.inner, TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("m"), "journal");
        marksBuilder.where(in(property("m", TrEduGroupEventStudent.event().journalEvent()), getEventsBuilder(reportParams, formativeOUs).buildQuery()));
        marksBuilder.where(isNotNull(property("m", TrEduGroupEventStudent.gradeAsLong())));
        marksBuilder.where(isNull(property("m", TrEduGroupEventStudent.studentWpe().removalDate())));
        if(FefuBrsReportManager.CUT_REPORT_CURRENT_ID.equals(reportParams.getCutReport().getId()))
        {
            marksBuilder.where(le(property("m", TrEduGroupEventStudent.event().scheduleEvent().durationBegin()), valueTimestamp(reportParams.getCheckDate())));
            marksBuilder.where(exists(new DQLSelectBuilder().fromEntity(FefuStudentMarkHistory.class, "h")
                    .where(eq(property("h", FefuStudentMarkHistory.trEduGroupEventStudent().id()), property("m", TrEduGroupEventStudent.id())))
                    .where(le(property("h", FefuStudentMarkHistory.markDate()), valueTimestamp(reportParams.getCheckDate())))
                    .buildQuery()));
        }


        for(TrEduGroupEventStudent mark : createStatement(marksBuilder).<TrEduGroupEventStudent>list())
        {
            EppRealEduGroup eduGroup = mark.getEvent().getGroup();
            TrJournal journal = mark.getJournal();
            if(!journalMarksMap.containsKey(journal))
                journalMarksMap.put(journal, Maps.<EppRealEduGroup, List<TrEduGroupEventStudent>>newHashMap());
            if(!journalMarksMap.get(journal).containsKey(eduGroup))
                journalMarksMap.get(journal).put(eduGroup, Lists.<TrEduGroupEventStudent>newArrayList());
            if(!journalMarksMap.get(journal).get(eduGroup).contains(mark))
                journalMarksMap.get(journal).get(eduGroup).add(mark);
        }

        // Студенты
        DQLSelectBuilder studentsBuilder = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "st");
        studentsBuilder.fetchPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.group().fromAlias("st"), "eduGroup");
        if(null != formativeOUs && !formativeOUs.isEmpty())
            studentsBuilder.where(in(property("st", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().educationOrgUnit().formativeOrgUnit()), formativeOUs));
        studentsBuilder.where(isNull(property("st", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().removalDate())));
        studentsBuilder.where(exists(
                new DQLSelectBuilder().fromEntity(TrJournalGroup.class, "jg")
                        .where(eq(property("jg", TrJournalGroup.group().id()), property("st", EppRealEduGroup4LoadTypeRow.group().id())))
                        .buildQuery()));
        for(EppRealEduGroup4LoadTypeRow student : createStatement(studentsBuilder).<EppRealEduGroup4LoadTypeRow>list())
        {
            EppRealEduGroup eduGroup = student.getGroup();

            if(!groupStudentMap.containsKey(eduGroup))
                groupStudentMap.put(eduGroup, Lists.<EppRealEduGroup4LoadTypeRow>newArrayList());
            if(!groupStudentMap.get(eduGroup).contains(student))
                groupStudentMap.get(eduGroup).add(student);
        }


        // Преподаватели
        DQLSelectBuilder ppsBuilder = new DQLSelectBuilder().fromEntity(TrJournalGroup.class, "jg");
        ppsBuilder.joinEntity("jg", DQLJoinType.inner, PpsEntryByEmployeePost.class, "pps",
                exists(
                        new DQLSelectBuilder().fromEntity(EppPpsCollectionItem.class, "item").
                                where(eq(property("item", EppPpsCollectionItem.pps().id()), property("pps", PpsEntryByEmployeePost.id()))).
                                where(eq(property("item", EppPpsCollectionItem.list().id()), property("jg", TrJournalGroup.group().id()))).
                                buildQuery()));
        ppsBuilder.column("jg");
        ppsBuilder.column("pps");
        ppsBuilder.fetchPath(DQLJoinType.inner, TrJournalGroup.journal().fromAlias("jg"), "journal");
        ppsBuilder.fetchPath(DQLJoinType.inner, TrJournalGroup.group().type().fromAlias("jg"), "grouptype");

        DQLSelectBuilder subEduGroupBuilder = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "st");
        subEduGroupBuilder.where(eq(property("st", EppRealEduGroup4LoadTypeRow.group().id()), property("jg", TrJournalGroup.group().id())));
        subEduGroupBuilder.where(isNull(property("st", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().removalDate())));
        if(null != formativeOUs && !formativeOUs.isEmpty())
            subEduGroupBuilder.where(in(property("st", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().educationOrgUnit().formativeOrgUnit()), formativeOUs));

        ppsBuilder.where(exists(subEduGroupBuilder.buildQuery()));

        if(null != reportParams.getTutorOrgUnit() && !reportParams.getTutorOrgUnit().isEmpty())
            ppsBuilder.where(in(property("jg", TrJournalGroup.journal().registryElementPart().registryElement().owner()), reportParams.getTutorOrgUnit()));

        for (Object row : createStatement(ppsBuilder).list())
        {
            TrJournalGroup journalGroup = (TrJournalGroup) ((Object[]) row)[0];
            TrJournal journal = journalGroup.getJournal();
            EppRealEduGroup eduGroup = journalGroup.getGroup();
            PpsEntryByEmployeePost pps = (PpsEntryByEmployeePost) ((Object[]) row)[1];

            if (!ppsJournalsMap.containsKey(pps))
                ppsJournalsMap.put(pps, Maps.<EppRealEduGroup, List<TrJournal>>newHashMap());
            if (!ppsJournalsMap.get(pps).containsKey(eduGroup))
                ppsJournalsMap.get(pps).put(eduGroup, Lists.<TrJournal>newArrayList());
            if (!ppsJournalsMap.get(pps).get(eduGroup).contains(journal))
                ppsJournalsMap.get(pps).get(eduGroup).add(journal);
        }

        List<PpsEntryByEmployeePost> ppsList = Lists.newArrayList(ppsJournalsMap.keySet());
        if(ppsList.isEmpty()) throw new ApplicationException("Нет данных для отчета");

        ppsList.sort(CommonCollator.comparing(PersonRole::getFullFio, true));

        List<String[]> tableData = Lists.newArrayList();

        int eventsAll = 0;
        int marksAll = 0;

        for(PpsEntryByEmployeePost pps : ppsList)
        {
            if(null != ppsJournalsMap.get(pps) && !ppsJournalsMap.get(pps).isEmpty())
            {
                int events = 0;
                int marks = 0;
                boolean hasData = false;
                Map<EppRealEduGroup, List<TrJournal>> ppsGroupJournalMap = ppsJournalsMap.get(pps);
                for(Map.Entry<EppRealEduGroup, List<TrJournal>> groupEntry : ppsGroupJournalMap.entrySet())
                {
                    if(null != groupEntry.getValue() && !groupEntry.getValue().isEmpty())
                    {
                        EppRealEduGroup eduGroup = groupEntry.getKey();
                        EppGroupType groupType = eduGroup.getType();
                        for(TrJournal journal : groupEntry.getValue())
                        {
                            if(null != journalActionsMap.get(journal) && !journalActionsMap.get(journal).isEmpty()
                                    && null != journalActionsMap.get(journal).get(groupType) && !journalActionsMap.get(journal).get(groupType).isEmpty()
                                    && null != groupStudentMap.get(eduGroup) && !groupStudentMap.get(eduGroup).isEmpty())
                            {
                                hasData = true;
                                List<TrEventAction> actions = journalActionsMap.get(journal).get(groupType);
                                List<EppRealEduGroup4LoadTypeRow> students = groupStudentMap.get(eduGroup);
                                events += actions.size() * students.size();

                                List<TrEduGroupEventStudent> studentsMarks = null;
                                if(null != journalMarksMap.get(journal) && !journalMarksMap.get(journal).isEmpty()
                                        && null != journalMarksMap.get(journal).get(eduGroup) && !journalMarksMap.get(journal).get(eduGroup).isEmpty())
                                    studentsMarks = journalMarksMap.get(journal).get(eduGroup);
                                if(null != studentsMarks)
                                    marks += studentsMarks.size();
                            }
                        }
                    }
                }

                if(hasData)
                {
                    List<String> row = Lists.newArrayList();
                    row.add(pps.getFullFio());
                    row.add(String.valueOf(events));
                    row.add(String.valueOf(marks));
                    row.add(String.valueOf(0 != events ? (marks*100/events) : 0));
                    tableData.add(row.toArray(new String[row.size()]));

                    eventsAll += events;
                    marksAll += marks;
                }
            }
        }

        if(tableData.isEmpty()) throw new ApplicationException("Нет данных для отчета");

        List<String> totalRow = Lists.newArrayList();
        totalRow.add("Итого");
        totalRow.add(String.valueOf(eventsAll));
        totalRow.add(String.valueOf(marksAll));
        totalRow.add(String.valueOf(0 != eventsAll ? (marksAll*100/eventsAll) : 0));
        tableData.add(totalRow.toArray(new String[totalRow.size()]));

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", tableData.toArray(new String[0][0]));
        tableModifier.modify(document);

//        throw new ApplicationException("test");
    }

    private DQLSelectBuilder getEventsBuilder(FefuBrsPpsGradingSumDataReportParams reportParams, List<OrgUnit> formativeOUs)
    {
        DQLSelectBuilder eventsBuilder = new DQLSelectBuilder().fromEntity(TrEventAction.class, "ea");
        eventsBuilder.fetchPath(DQLJoinType.inner, TrEventAction.journalModule().journal().fromAlias("ea"), "journal");
        eventsBuilder.fetchPath(DQLJoinType.inner, TrEventAction.type().fromAlias("ea"), "type");

        DQLSelectBuilder subEduGroupBuilder = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "st");
        subEduGroupBuilder.where(eq(property("st", EppRealEduGroup4LoadTypeRow.group().id()), property("jg", TrJournalGroup.group().id())));
        subEduGroupBuilder.where(isNull(property("st", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().removalDate())));
        if(null != formativeOUs && !formativeOUs.isEmpty())
            subEduGroupBuilder.where(in(property("st", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().educationOrgUnit().formativeOrgUnit()), formativeOUs));

        eventsBuilder.where(exists(
                new DQLSelectBuilder().fromEntity(TrJournalGroup.class, "jg")
                        .where(eq(property("jg", TrJournalGroup.journal().id()), property("ea", TrEventAction.journalModule().journal().id())))
                        .where(exists(subEduGroupBuilder.buildQuery()))
                        .buildQuery()));
        if(null != reportParams.getTutorOrgUnit() && !reportParams.getTutorOrgUnit().isEmpty())
            eventsBuilder.where(in(property("ea", TrEventAction.journalModule().journal().registryElementPart().registryElement().owner()), reportParams.getTutorOrgUnit()));
        if(FefuBrsReportManager.CUT_REPORT_CURRENT_ID.equals(reportParams.getCutReport().getId()))
        {
            eventsBuilder.where(exists(new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ee")
                    .where(eq(property("ee", TrEduGroupEvent.journalEvent().id()), property("ea", TrEventAction.id())))
                    .where(le(property("ee", TrEduGroupEvent.scheduleEvent().durationBegin()), valueTimestamp(reportParams.getCheckDate())))
                    .buildQuery()));
        }
        eventsBuilder.where(eq(property("ea", TrEventAction.journalModule().journal().yearPart()), value(reportParams.getYearPart())));
        return eventsBuilder;
    }

    private List<OrgUnit> getFormativeOUParam(FefuBrsPpsGradingSumDataReportParams reportParams)
    {
        if(null != reportParams.getFormativeOrgUnit() && !reportParams.getFormativeOrgUnit().isEmpty())
            return reportParams.getFormativeOrgUnit();

        DQLSelectBuilder formOUBuilder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou");
        formOUBuilder.where(DQLExpressions.exists(
                new DQLSelectBuilder().
                        fromEntity(OrgUnitToKindRelation.class, "rel").
                        where(DQLExpressions.eq(DQLExpressions.property("rel", OrgUnitToKindRelation.orgUnitKind().code()), DQLExpressions.value(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))).
                        where(DQLExpressions.eq(DQLExpressions.property("ou", OrgUnit.id()), DQLExpressions.property("rel", OrgUnitToKindRelation.orgUnit().id()))).buildQuery()));

        return createStatement(formOUBuilder).list();
    }
}
