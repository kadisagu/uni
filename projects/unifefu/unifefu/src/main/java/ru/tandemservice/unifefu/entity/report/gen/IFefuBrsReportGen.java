package ru.tandemservice.unifefu.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.entity.report.IFefuBrsReport;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unifefu.entity.report.IFefuBrsReport;

/**
 * Отчет по БРС в проекте ДВФУ
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IFefuBrsReportGen extends InterfaceStubBase
 implements IFefuBrsReport{
    public static final int VERSION_HASH = 779336658;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_CONTENT = "content";
    public static final String P_YEAR_PART = "yearPart";

    private OrgUnit _orgUnit;
    private DatabaseFile _content;
    private String _yearPart;


    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    @NotNull

    public DatabaseFile getContent()
    {
        return _content;
    }

    public void setContent(DatabaseFile content)
    {
        _content = content;
    }

    @Length(max=255)

    public String getYearPart()
    {
        return _yearPart;
    }

    public void setYearPart(String yearPart)
    {
        _yearPart = yearPart;
    }

    private static final Path<IFefuBrsReport> _dslPath = new Path<IFefuBrsReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unifefu.entity.report.IFefuBrsReport");
    }
            

    /**
     * @return Подразделение, на котором был построе отчет. Для глобалных не указывается..
     * @see ru.tandemservice.unifefu.entity.report.IFefuBrsReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.IFefuBrsReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.unifefu.entity.report.IFefuBrsReport#getYearPart()
     */
    public static PropertyPath<String> yearPart()
    {
        return _dslPath.yearPart();
    }

    public static class Path<E extends IFefuBrsReport> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<String> _yearPart;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение, на котором был построе отчет. Для глобалных не указывается..
     * @see ru.tandemservice.unifefu.entity.report.IFefuBrsReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.IFefuBrsReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.unifefu.entity.report.IFefuBrsReport#getYearPart()
     */
        public PropertyPath<String> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new PropertyPath<String>(IFefuBrsReportGen.P_YEAR_PART, this);
            return _yearPart;
        }

        public Class getEntityClass()
        {
            return IFefuBrsReport.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unifefu.entity.report.IFefuBrsReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
