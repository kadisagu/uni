/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.OriginalsVerificationPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationReport;


/**
 * @author nvankov
 * @since 7/16/13
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
})
public class FefuEcReportOriginalsVerificationPubUI extends UIPresenter
{
    // fields

    private Long _reportId;
    private FefuEntrantOriginalsVerificationReport _report;

    // Presenter listeners

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(FefuEntrantOriginalsVerificationReport.class, _reportId);

    }

    // Listeners

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", _reportId)
                .parameter("extension", "rtf")
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report);
        deactivate();
    }

    // Getters & Setters

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public FefuEntrantOriginalsVerificationReport getReport()
    {
        return _report;
    }

    public void setReport(FefuEntrantOriginalsVerificationReport report)
    {
        _report = report;
    }

    public boolean getNotAllEnrollmentDirections()
    {
        return !_report.isAllEnrollmentDirections();
    }
}
