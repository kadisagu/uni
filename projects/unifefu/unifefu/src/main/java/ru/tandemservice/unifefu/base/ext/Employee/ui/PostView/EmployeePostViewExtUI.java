/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Employee.ui.PostView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI;
import ru.tandemservice.unifefu.entity.ws.FefuEmployeePostExt;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 09.07.2014
 */
public class EmployeePostViewExtUI extends UIAddon
{
    private FefuEmployeePostExt _extension;

    public EmployeePostViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        FefuEmployeePostExt ext = DataAccessServices.dao().getByNaturalId(new FefuEmployeePostExt.NaturalId(((EmployeePostViewUI) getPresenter()).getEmployeePost()));
        setExtension(ext);
    }

    public FefuEmployeePostExt getExtension()
    {
        return _extension;
    }

    public void setExtension(FefuEmployeePostExt extension)
    {
        _extension = extension;
    }
}