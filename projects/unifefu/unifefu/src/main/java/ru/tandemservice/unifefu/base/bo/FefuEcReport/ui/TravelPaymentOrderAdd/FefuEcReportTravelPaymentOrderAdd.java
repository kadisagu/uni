/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.TravelPaymentOrderAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.EnrCampaignFormativeOrgUnitDSHandler;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 04.09.2013
 */
@Configuration
public class FefuEcReportTravelPaymentOrderAdd extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_PARAM = EnrCampaignFormativeOrgUnitDSHandler.ENROLLMENT_CAMPAIGN_PROP;
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String ENTRANT_ENROLLMENT_ORDER_TYPE_DS = "entrantEnrollmentOrderTypeDS";
    public static final String ENTRANT_ENROLLMENT_ORDER_TYPE_PARAM = "entrantEnrollmentOrderTypeParam";
    public static final String ENROLLMENT_ORDER_DS = "enrollmentOrderDS";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(ENTRANT_ENROLLMENT_ORDER_TYPE_DS, getName(),
                                                                         EntrantEnrollmentOrderType.defaultSelectDSHandler(getName())
                                                                                 .customize(getEntrantEnrollmentOrderTypeCustomizer())))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(ENROLLMENT_ORDER_DS, getName(),
                                                                         EnrollmentOrder.defaultSelectDSHandler(getName())
                                                                                 .customize(getEnrollmentOrderCustomizer())))
                .create();
    }

    private static EntityComboDataSourceHandler.IQueryCustomizer getEntrantEnrollmentOrderTypeCustomizer()
    {
        return (alias, dql, context, filter) -> {
            dql.where(existsByExpr(EnrollmentOrderType.class, "rel",
                                   and(eq(property("rel", EnrollmentOrderType.enrollmentCampaign()), value((EnrollmentCampaign) context.get(ENROLLMENT_CAMPAIGN_PARAM))),
                                       eq(property("rel", EnrollmentOrderType.used()), value(Boolean.TRUE)),
                                       eq(property("rel", EnrollmentOrderType.entrantEnrollmentOrderType()), property(alias))
                                   )));
            return dql;
        };
    }

    private static EntityComboDataSourceHandler.IQueryCustomizer getEnrollmentOrderCustomizer()
    {
        return (alias, dql, context, filter) -> {
            dql.where(eq(property(alias, EnrollmentOrder.enrollmentCampaign()), value((EnrollmentCampaign) context.get(ENROLLMENT_CAMPAIGN_PARAM))));
            CommonBaseFilterUtil.applySelectFilter(dql, alias, EnrollmentOrder.type(),  context.get(ENTRANT_ENROLLMENT_ORDER_TYPE_PARAM));
            return dql;
        };
    }
}