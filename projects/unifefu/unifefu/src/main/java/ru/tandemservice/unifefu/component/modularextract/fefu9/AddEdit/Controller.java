/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu9.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuSocGrantResumptionStuExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        if(getModel(component).isAddForm())
        {
            onClickChangeOrderDateAndNumber(component);
        }
    }

    public void onClickChangeOrderDateAndNumber(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getSocGrantExtract() != null)
        {
            model.getExtract().setSocGrantOrderDate(model.getSocGrantExtract().getParagraph().getOrder().getCommitDate());
            model.getExtract().setSocGrantOrder(model.getSocGrantExtract().getParagraph().getOrder().getNumber());

        }
    }
}