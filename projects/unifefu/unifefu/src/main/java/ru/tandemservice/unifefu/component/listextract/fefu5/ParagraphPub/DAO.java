/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu5.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuCourseTransferStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class DAO extends AbstractListParagraphPubDAO<FefuCourseTransferStuListExtract, Model> implements IDAO
{
}
