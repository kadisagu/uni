/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu14.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.SendPracticInnerStuExtract;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;

/**
 * @author nvankov
 * @since 6/20/13
 */
public class Model extends ModularStudentExtractPubModel<SendPracticInnerStuExtract>
{
    private String _preventAccidentsIC;
    private String _practiceHeader;
    private String _responsForRecieveCash;

    public String getPreventAccidentsIC()
    {
        return _preventAccidentsIC;
    }

    public void setPreventAccidentsIC(String preventAccidentsIC)
    {
        _preventAccidentsIC = preventAccidentsIC;
    }

    public String getPracticeHeader()
    {
        return _practiceHeader;
    }

    public void setPracticeHeader(String practiceHeader)
    {
        _practiceHeader = practiceHeader;
    }

    public String getResponsForRecieveCash()
    {
        return _responsForRecieveCash;
    }

    public void setResponsForRecieveCash(String responsForRecieveCash)
    {
        _responsForRecieveCash = responsForRecieveCash;
    }
}
