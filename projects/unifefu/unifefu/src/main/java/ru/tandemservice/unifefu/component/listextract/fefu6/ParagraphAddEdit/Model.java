/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6.ParagraphAddEdit;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.dao.IEducationLevelModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.IExtEducationLevelModel;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract;

import java.util.List;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class Model extends AbstractListParagraphAddEditModel<FefuGiveDiplomaWithDismissStuListExtract> implements IEducationLevelModel, IExtEducationLevelModel, IGroupModel
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private StudentCustomStateCI _studentCustomStateCI;
    private DevelopPeriod _developPeriod;
    private boolean _printDiplomaQualification;

    private List<CompensationType> _compensationTypeList;
    private ISelectModel _groupListModel;
    private ISelectModel _developPeriodListModel;

    private boolean _courseAndGroupDisabled;

    public StudentCustomStateCI getStudentCustomStateCI()
    {
        return _studentCustomStateCI;
    }

    public void setStudentCustomStateCI(StudentCustomStateCI studentCustomStateCI)
    {
        _studentCustomStateCI = studentCustomStateCI;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    public boolean isCourseAndGroupDisabled()
    {
        return _courseAndGroupDisabled;
    }

    public void setCourseAndGroupDisabled(boolean courseAndGroupDisabled)
    {
        _courseAndGroupDisabled = courseAndGroupDisabled;
    }

    public boolean isPrintDiplomaQualification()
    {
        return _printDiplomaQualification;
    }

    public void setPrintDiplomaQualification(boolean printDiplomaQualification)
    {
        _printDiplomaQualification = printDiplomaQualification;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getFormativeOrgUnit() : null;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getTerritorialOrgUnit() : null;
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return null;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getDevelopForm() : null;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getDevelopCondition() : null;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return getGroup() != null ? getGroup().getEducationOrgUnit().getDevelopTech() : null;
    }

    @Override
    public EducationLevels getParentEduLevel()
    {
        return getGroup() != null ? EducationOrgUnitUtil.getParentLevel(getGroup().getEducationOrgUnit().getEducationLevelHighSchool()) : null;
    }
}
