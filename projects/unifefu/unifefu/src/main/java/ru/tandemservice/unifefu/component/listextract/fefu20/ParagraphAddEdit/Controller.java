/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;

import java.util.Calendar;

/**
 * @author Andrey Andreev
 * @since 13.01.2016
 */
public class Controller extends AbstractListParagraphAddEditController<FefuAdmittedToGIAListExtract, IDAO, Model>
{

    public void onSubmitYear(IBusinessComponent component)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        if (((ru.tandemservice.unifefu.component.listextract.fefu20.ParagraphAddEdit.Model) component.getModel()).getYear() < Calendar.getInstance().get(Calendar.YEAR))
            errCollector.add("Год должен быть больше или равен текущему.", "year");
    }

}