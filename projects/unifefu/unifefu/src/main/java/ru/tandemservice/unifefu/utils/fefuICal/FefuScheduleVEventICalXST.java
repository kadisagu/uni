/* $Id$ */
package ru.tandemservice.unifefu.utils.fefuICal;

import java.util.Date;

/**
 * @author nvankov
 * @since 9/16/13
 */
public class FefuScheduleVEventICalXST
{
    private Long _pairId;
    private String _uidPrefix;
    private Date _startDate;
    private Date _endDate;
    private String _periodGroup;
    private String _subject;
    private String _teacher;
    private String _lectureRoom;

    public Long getPairId()
    {
        return _pairId;
    }

    public void setPairId(Long pairId)
    {
        _pairId = pairId;
    }

    public String getUidPrefix()
    {
        return _uidPrefix;
    }

    public void setUidPrefix(String uidPrefix)
    {
        _uidPrefix = uidPrefix;
    }

    public Date getStartDate()
    {
        return _startDate;
    }

    public void setStartDate(Date startDate)
    {
        _startDate = startDate;
    }

    public Date getEndDate()
    {
        return _endDate;
    }

    public void setEndDate(Date endDate)
    {
        _endDate = endDate;
    }

    public String getSubject()
    {
        return _subject;
    }

    public void setSubject(String subject)
    {
        _subject = subject;
    }

    public String getTeacher()
    {
        return _teacher;
    }

    public void setTeacher(String teacher)
    {
        _teacher = teacher;
    }

    public String getLectureRoom()
    {
        return _lectureRoom;
    }

    public void setLectureRoom(String lectureRoom)
    {
        _lectureRoom = lectureRoom;
    }

    public String getPeriodGroup()
    {
        return _periodGroup;
    }

    public void setPeriodGroup(String periodGroup)
    {
        _periodGroup = periodGroup;
    }
}
