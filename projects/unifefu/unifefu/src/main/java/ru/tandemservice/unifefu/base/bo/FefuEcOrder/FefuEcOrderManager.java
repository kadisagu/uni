/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.meta.BaseComponentMeta;
import org.tandemframework.core.component.ComponentActivator;
import ru.tandemservice.uniec.base.bo.EcOrder.util.EcOrderUtil;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.logic.FefuEcOrderDAO;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.logic.IFefuEcOrderDAO;

/**
 * @author Nikolay Fedorovskih
 * @since 17.07.2013
 */
@Configuration
public class FefuEcOrderManager extends BusinessObjectManager
{
    public static FefuEcOrderManager instance()
    {
        return instance(FefuEcOrderManager.class);
    }

    @Bean
    public IFefuEcOrderDAO dao()
    {
        return new FefuEcOrderDAO();
    }

    public static Class getParAddEditPresenter(EntrantEnrollmentOrderType orderType)
    {
        if (orderType != null)
        {
            ComponentActivator parAddEditComponent = new ComponentActivator(EcOrderUtil.getParagraphAddEditComponent(orderType));
            BaseComponentMeta meta = (BaseComponentMeta) parAddEditComponent.getComponentMeta().getCafMetaObject();
            return meta.getPresenterClass();
        }
        return null;
    }
}