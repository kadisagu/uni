/* $Id $ */
package ru.tandemservice.unifefu.base.ext.SessionTransfer.ui.OutsidePub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsidePub.SessionTransferOutsidePubUI;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument;

/**
 * @author Rostuncev Savva
 * @since 20.03.2014
 */

public class SessionTransferOutsidePubExtUI extends UIAddon
{
    public SessionTransferOutsidePubExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onPrintStudentsReport()
    {
        String COMPONENT_NAME = "ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTabPrintRtf";
        SessionTransferOutsideDocument presenter = ((SessionTransferOutsidePubUI) getPresenter()).getDocument();
        getActivationBuilder().asRegion(COMPONENT_NAME)
                .parameter("sessionTransferDocumentId", presenter.getId())
                .activate();
    }
}
