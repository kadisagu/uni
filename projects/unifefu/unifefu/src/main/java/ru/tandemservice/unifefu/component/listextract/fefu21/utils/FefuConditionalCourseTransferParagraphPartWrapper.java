/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu21.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Belanov
 * @since 01.07.2016
 */
public class FefuConditionalCourseTransferParagraphPartWrapper implements Comparable<FefuConditionalCourseTransferParagraphPartWrapper>
{
    private final EducationLevelsHighSchool _educationLevelsHighSchool;
    private final ListStudentExtract _firstExtract;
    private final List<Person> _personList = new ArrayList<>();

    public FefuConditionalCourseTransferParagraphPartWrapper(EducationLevelsHighSchool educationLevelsHighSchool, ListStudentExtract firstExtract)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
        _firstExtract = firstExtract;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuConditionalCourseTransferParagraphPartWrapper))
            return false;

        FefuConditionalCourseTransferParagraphPartWrapper that = (FefuConditionalCourseTransferParagraphPartWrapper) o;

        if (_educationLevelsHighSchool.getEducationLevel().getLevelType().isSpecialization() ||
                _educationLevelsHighSchool.getEducationLevel().getLevelType().isProfile() ||
                that.getEducationLevelsHighSchool().getEducationLevel().getLevelType().isSpecialization() ||
                that.getEducationLevelsHighSchool().getEducationLevel().getLevelType().isProfile())
            return _educationLevelsHighSchool.equals(that.getEducationLevelsHighSchool());
        else
            return true;
    }

    @Override
    public int hashCode()
    {
        return _educationLevelsHighSchool.hashCode();
    }

    @Override
    public int compareTo(FefuConditionalCourseTransferParagraphPartWrapper o)
    {
        StructureEducationLevels levelType1 = _educationLevelsHighSchool.getEducationLevel().getLevelType();
        StructureEducationLevels levelType2 = o.getEducationLevelsHighSchool().getEducationLevel().getLevelType();
        if (!levelType1.isProfile() && !levelType1.isSpecialization() && (levelType2.isProfile() || levelType2.isSpecialization()))
            return -1;
        else if ((levelType1.isProfile() || levelType1.isSpecialization()) && !levelType2.isProfile() && !levelType2.isSpecialization())
            return 1;
        else if (levelType1.isSpecialization() && levelType2.isProfile()) return -1;
        else if (levelType1.isProfile() && levelType2.isSpecialization()) return 1;
        else if ((levelType1.isProfile() && levelType2.isProfile()) || (levelType1.isSpecialization() && levelType2.isSpecialization()))
        {
            if (!_educationLevelsHighSchool.equals(o.getEducationLevelsHighSchool()))
            {
                return _educationLevelsHighSchool.getTitle().compareTo(o.getEducationLevelsHighSchool().getTitle());
            } else
            {
                return 0;
            }
        } else return 0;
    }
}
