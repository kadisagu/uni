/* $Id$ */
package ru.tandemservice.unifefu.base.ext.OrgUnit.ui.View;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitViewUI;
import ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.SignatoryEdit.FefuOrgUnitSignatoryEdit;
import ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.SignatoryEdit.FefuOrgUnitSignatoryEditUI;
import ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.Swap.FefuOrgUnitSwap;
import ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.Swap.FefuOrgUnitSwapUI;
import ru.tandemservice.unifefu.base.bo.NSISync.ui.ChangeGuid.NSISyncChangeGuid;
import ru.tandemservice.unifefu.base.bo.NSISync.ui.ChangeGuid.NSISyncChangeGuidUI;
import ru.tandemservice.unifefu.entity.OrgUnitFefuExt;
import ru.tandemservice.unifefu.entity.ws.FefuTopOrgUnit;
import ru.tandemservice.unifefu.utils.NsiIdWrapper;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

/**
 * @author Alexey Lopatin
 * @since 24.09.2013
 */
public class OrgUnitViewExtUI extends UIAddon
{
    private String _signatoryFullTitle;
    private FefuTopOrgUnit _topOrgUnit;
    private NsiIdWrapper _nsiIdWrapper;
    private NsiIdWrapper _topNsiIdWrapper;

    public OrgUnitViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        OrgUnitViewUI presenter = (OrgUnitViewUI) getPresenter();
        OrgUnitFefuExt orgUnitFefuExt = DataAccessServices.dao().getByNaturalId(new OrgUnitFefuExt.NaturalId(presenter.getOrgUnit()));
        _topOrgUnit = DataAccessServices.dao().get(FefuTopOrgUnit.class, FefuTopOrgUnit.orgUnit().id(), presenter.getOrgUnit().getId());

        if (null != orgUnitFefuExt)
        {
            EmployeePostPossibleVisa visa = orgUnitFefuExt.getEmployeePostPossibleVisa();
            _signatoryFullTitle = null == visa ? "" : visa.getEntity().getPerson().getIdentityCard().getIof() + ", " + visa.getTitle();
        }
        _nsiIdWrapper = new NsiIdWrapper(presenter.getOrgUnit().getId());

        if (null != _topOrgUnit)
        {
            _topNsiIdWrapper = new NsiIdWrapper(_topOrgUnit.getId());
        }
    }

    public boolean isTop()
    {
        return null != _topOrgUnit;
    }

    public boolean isTandemAdmin()
    {
        return true; //"tandem".equals(UserContext.getInstance().getPrincipal().getLogin());
    }

    public void onClickEditGuid()
    {
        OrgUnitViewUI presenter = getPresenter();
        getActivationBuilder().asRegionDialog(NSISyncChangeGuid.class)
                .parameter(NSISyncChangeGuidUI.ENTITY_ID, presenter.getOrgUnit().getId())
                .activate();
    }

    public void onClickEditTopGuid()
    {
        getActivationBuilder().asRegionDialog(NSISyncChangeGuid.class)
                .parameter(NSISyncChangeGuidUI.ENTITY_ID, getTopOrgUnit().getId())
                .activate();
    }

    public void onClickSwapOrgUnits()
    {
        OrgUnitViewUI presenter = getPresenter();
        getActivationBuilder().asRegionDialog(FefuOrgUnitSwap.class)
                .parameter(FefuOrgUnitSwapUI.ORG_UNIT_ID, presenter.getOrgUnit().getId())
                .activate();
    }

    public void onClickOrgUnitSignatoryEdit()
    {
        OrgUnitViewUI presenter = (OrgUnitViewUI) getPresenter();

        getActivationBuilder().asRegion(FefuOrgUnitSignatoryEdit.class, presenter.getRegionName())
                .parameter(FefuOrgUnitSignatoryEditUI.ORG_UNIT_ID, presenter.getOrgUnit().getId())
                .activate();
    }

    public String getSignatoryFullTitle()
    {
        return _signatoryFullTitle;
    }

    public void setSignatoryFullTitle(String signatoryFullTitle)
    {
        _signatoryFullTitle = signatoryFullTitle;
    }

    public NsiIdWrapper getNsiIdWrapper()
    {
        return _nsiIdWrapper;
    }

    public void setNsiIdWrapper(NsiIdWrapper nsiIdWrapper)
    {
        _nsiIdWrapper = nsiIdWrapper;
    }

    public FefuTopOrgUnit getTopOrgUnit()
    {
        return _topOrgUnit;
    }

    public void setTopOrgUnit(FefuTopOrgUnit topOrgUnit)
    {
        _topOrgUnit = topOrgUnit;
    }

    public NsiIdWrapper getTopNsiIdWrapper()
    {
        return _topNsiIdWrapper;
    }

    public void setTopNsiIdWrapper(NsiIdWrapper topNsiIdWrapper)
    {
        _topNsiIdWrapper = topNsiIdWrapper;
    }
}