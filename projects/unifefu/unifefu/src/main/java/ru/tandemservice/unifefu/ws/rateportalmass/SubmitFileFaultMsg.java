/**
 * SubmitFileFaultMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportalmass;

public class SubmitFileFaultMsg extends org.apache.axis.AxisFault {
    public java.lang.String submitFileFault;
    public java.lang.String getSubmitFileFault() {
        return this.submitFileFault;
    }

    public SubmitFileFaultMsg() {
    }

    public SubmitFileFaultMsg(java.lang.Exception target) {
        super(target);
    }

    public SubmitFileFaultMsg(java.lang.String message, java.lang.Throwable t) {
        super(message, t);
    }

      public SubmitFileFaultMsg(java.lang.String submitFileFault) {
        this.submitFileFault = submitFileFault;
    }

    /**
     * Writes the exception data to the faultDetails
     */
    public void writeDetails(javax.xml.namespace.QName qname, org.apache.axis.encoding.SerializationContext context) throws java.io.IOException {
        context.serialize(qname, null, submitFileFault);
    }
}
