package ru.tandemservice.unifefu.base.ext.TrJournal.ui.ScheduleEventEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.ScheduleEventEdit.TrJournalScheduleEventEdit;

/**
 * @author amakarova
 *
 */
@Configuration
public class TrJournalScheduleEventEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrJournalScheduleEventEditExtUI.class.getSimpleName();

    @Autowired
    private TrJournalScheduleEventEdit _trJournalScheduleEventEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trJournalScheduleEventEdit.presenterExtPoint())
                .addAddon(uiAddon("unifefuTrJournalScheduleEventEditExtUI", TrJournalScheduleEventEditExtUI.class))
                .create();
    }
}
