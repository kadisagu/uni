/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.vo.FefuSessionListDocumentFilter;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 26.08.2014
 */
public interface IFefuPpsSessionListDocumentDAO extends INeedPersistenceSupport
{
	List<FefuSessionListDocumentWrapper> getSessionListDocumentWrapperList(@NotNull Person person, @NotNull FefuSessionListDocumentFilter filters);

	List<FefuSessionDocumentSlotWrapper> getSessionDocumentSlotWrapperList(@NotNull Person person, @NotNull SessionListDocument document);

	List<FefuSessionSlotMarkData> getSessionListDocumentEditData(@NotNull Person person, @NotNull SessionListDocument document);

	Student getStudent(@NotNull SessionListDocument document);
}
