package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifefu.entity.ws.FefuNsiEntity;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuNsiEntityGen extends EntityBase
 implements INaturalIdentifiable<FefuNsiEntityGen>, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuNsiEntity";
    public static final String ENTITY_NAME = "fefuNsiEntity";
    public static final int VERSION_HASH = 1446231975;
    private static IEntityMeta ENTITY_META;

    public static final String L_GUID = "guid";
    public static final String P_ENTITY_XML = "entityXml";

    private FefuNsiIds _guid;     // Идентификатор сущности в НСИ
    private String _entityXml;     // XML-описание сущности НСИ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор сущности в НСИ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public FefuNsiIds getGuid()
    {
        return _guid;
    }

    /**
     * @param guid Идентификатор сущности в НСИ. Свойство не может быть null и должно быть уникальным.
     */
    public void setGuid(FefuNsiIds guid)
    {
        dirty(_guid, guid);
        _guid = guid;
    }

    /**
     * @return XML-описание сущности НСИ.
     */
    public String getEntityXml()
    {
        initLazyForGet("entityXml");
        return _entityXml;
    }

    /**
     * @param entityXml XML-описание сущности НСИ.
     */
    public void setEntityXml(String entityXml)
    {
        initLazyForSet("entityXml");
        dirty(_entityXml, entityXml);
        _entityXml = entityXml;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuNsiEntityGen)
        {
            if (withNaturalIdProperties)
            {
                setGuid(((FefuNsiEntity)another).getGuid());
            }
            setEntityXml(((FefuNsiEntity)another).getEntityXml());
        }
    }

    public INaturalId<FefuNsiEntityGen> getNaturalId()
    {
        return new NaturalId(getGuid());
    }

    public static class NaturalId extends NaturalIdBase<FefuNsiEntityGen>
    {
        private static final String PROXY_NAME = "FefuNsiEntityNaturalProxy";

        private Long _guid;

        public NaturalId()
        {}

        public NaturalId(FefuNsiIds guid)
        {
            _guid = ((IEntity) guid).getId();
        }

        public Long getGuid()
        {
            return _guid;
        }

        public void setGuid(Long guid)
        {
            _guid = guid;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuNsiEntityGen.NaturalId) ) return false;

            FefuNsiEntityGen.NaturalId that = (NaturalId) o;

            if( !equals(getGuid(), that.getGuid()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getGuid());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getGuid());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuNsiEntityGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuNsiEntity.class;
        }

        public T newInstance()
        {
            return (T) new FefuNsiEntity();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "guid":
                    return obj.getGuid();
                case "entityXml":
                    return obj.getEntityXml();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "guid":
                    obj.setGuid((FefuNsiIds) value);
                    return;
                case "entityXml":
                    obj.setEntityXml((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "guid":
                        return true;
                case "entityXml":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "guid":
                    return true;
                case "entityXml":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "guid":
                    return FefuNsiIds.class;
                case "entityXml":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuNsiEntity> _dslPath = new Path<FefuNsiEntity>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuNsiEntity");
    }
            

    /**
     * @return Идентификатор сущности в НСИ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiEntity#getGuid()
     */
    public static FefuNsiIds.Path<FefuNsiIds> guid()
    {
        return _dslPath.guid();
    }

    /**
     * @return XML-описание сущности НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiEntity#getEntityXml()
     */
    public static PropertyPath<String> entityXml()
    {
        return _dslPath.entityXml();
    }

    public static class Path<E extends FefuNsiEntity> extends EntityPath<E>
    {
        private FefuNsiIds.Path<FefuNsiIds> _guid;
        private PropertyPath<String> _entityXml;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор сущности в НСИ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiEntity#getGuid()
     */
        public FefuNsiIds.Path<FefuNsiIds> guid()
        {
            if(_guid == null )
                _guid = new FefuNsiIds.Path<FefuNsiIds>(L_GUID, this);
            return _guid;
        }

    /**
     * @return XML-описание сущности НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiEntity#getEntityXml()
     */
        public PropertyPath<String> entityXml()
        {
            if(_entityXml == null )
                _entityXml = new PropertyPath<String>(FefuNsiEntityGen.P_ENTITY_XML, this);
            return _entityXml;
        }

        public Class getEntityClass()
        {
            return FefuNsiEntity.class;
        }

        public String getEntityName()
        {
            return "fefuNsiEntity";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
