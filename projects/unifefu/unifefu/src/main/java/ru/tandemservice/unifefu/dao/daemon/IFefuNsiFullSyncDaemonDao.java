/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

/**
 * @author Dmitry Seleznev
 * @since 27.05.2014
 */
public interface IFefuNsiFullSyncDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IFefuNsiFullSyncDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IFefuNsiFullSyncDaemonDao> instance = new SpringBeanCache<>(IFefuNsiFullSyncDaemonDao.class.getName());

    /**
     * Производит принудительную выгрузку данных в НСИ для заданных справочников.
     * Выгрузка производится через FefuNsiDaemonDao, путём регистрации порций элементов на синхронизацию,
     * имитируя сработку листенера на добавление нового элемента в справочник.
     * Берет первые 50 элементов заданного справочника и если время их синхронизации составляет больше месяца назад, или раньше 25 мая 2014 года,
     * то производит отправку в НСИ.
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doFullSyncCatalogsWithNSI();
}