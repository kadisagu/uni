package ru.tandemservice.unifefu.component.report.FefuEntrantResidenceDistrib.List;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.unifefu.entity.report.FefuEntrantResidenceDistrib;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(FefuEntrantResidenceDistrib.ENTITY_CLASS, "report");
        builder.add(MQExpression.eq("report", FefuEntrantResidenceDistrib.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
        new OrderDescriptionRegistry("report").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}
