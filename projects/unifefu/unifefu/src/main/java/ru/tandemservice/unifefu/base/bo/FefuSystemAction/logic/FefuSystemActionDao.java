/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.healthmarketscience.jackcess.Database;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.log4j.*;
import org.hibernate.Session;
import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.process.*;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.util.IPersonMergeListener;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcEntrant.EcEntrantManager;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.ImportContracts.FefuContractImportWrapper;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.MergePersonsAndUpdateGuids.NsiPersonMergeWrapper;
import ru.tandemservice.unifefu.base.vo.ProcessedOnlineEntrantsVO;
import ru.tandemservice.unifefu.dao.daemon.IFefuNsiDaemonDao;
import ru.tandemservice.unifefu.entity.*;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow;
import ru.tandemservice.unifefu.utils.FefuMailSender;
import ru.tandemservice.unifefu.utils.SHAUtils;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanType;
import ru.tandemservice.unifefu.ws.nsi.datagram.XDatagram;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unispp.base.entity.SppScheduleICal;

import javax.validation.constraints.NotNull;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
@SuppressWarnings("deprecation")
public class FefuSystemActionDao extends UniBaseDao implements IFefuSystemActionDao
{
    private final Character CSV_SEPARATOR = ';';
    protected static final Logger logger = Logger.getLogger(FefuSystemActionDao.class);

    @Override
    public void doExportEduInstitutions(File file)
    {
        try
                (BufferedWriter writer = Files.newBufferedWriter(file.toPath(), Charset.forName("UTF-8")))
        {
            List<Long> ids = new DQLSelectBuilder().fromEntity(PersonEduInstitution.class, "e").column(property("e.id"))
                    .where(or(
                            isNotNull(property(PersonEduInstitution.eduInstitution().fromAlias("e"))),
                            gt(property(PersonEduInstitution.yearEnd().fromAlias("e")), value(0)),
                            isNull(property(PersonEduInstitution.mark3().fromAlias("e"))),
                            isNull(property(PersonEduInstitution.mark4().fromAlias("e"))),
                            isNull(property(PersonEduInstitution.mark5().fromAlias("e")))
                    ))
                    .order(property(PersonEduInstitution.creationDate().fromAlias("e")), OrderDirection.desc)
                    .createStatement(getSession()).list();

            BatchUtils.execute(ids, 128, ids1 -> {
                try
                {
                    StringBuilder line;
                    for (final PersonEduInstitution o : getList(PersonEduInstitution.class, "id", ids1))
                    {
                        line = new StringBuilder(77)
                                .append(o.getPerson().getId()).append(CSV_SEPARATOR)
                                .append(o.getEduInstitution() != null ? o.getEduInstitution().getId().toString() : "").append(CSV_SEPARATOR)
                                .append(o.getYearEnd()).append(CSV_SEPARATOR)
                                .append(o.getMark3() == null ? '0' : '1')
                                .append(o.getMark4() == null ? '0' : '1')
                                .append(o.getMark5() == null ? '0' : '1').append(CSV_SEPARATOR)
                                .append(o.getIssuanceDate() == null ? "" : o.getIssuanceDate().getTime()).append(CSV_SEPARATOR)
                                .append(o.getSeria() == null ? "" : o.getSeria()).append(CSV_SEPARATOR)
                                .append(o.getNumber() == null ? "" : o.getNumber());

                        writer.write(line.toString());
                        writer.newLine();
                    }
                }
                catch (final Throwable t)
                {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
                getSession().clear();
            });
        }
        catch (IOException x)
        {
            throw new RuntimeException("IOException in exportEduInstitutions(): %s%n", x);
        }
    }

    private static String getIdentificationKey(String issuanceDateInMSecs, String seria, String number)
    {
        // в рамках персоны идентифицируем нужный документ об окончании ОУ по дате выдаче + серия + номер
        String key = (StringUtils.isEmpty(issuanceDateInMSecs) ? "" : issuanceDateInMSecs + "_") +
                (StringUtils.isEmpty(seria) ? "" : seria + "_") +
                (StringUtils.isEmpty(number) ? "" : number);
        return key.isEmpty() ? null : key;
    }

    @Override
    public String doImportEduInstitutions(File file)
    {
        // Парсим файл
        final Map<Long, Map<String, Object[]>> personIdMap = new HashMap<>(200);
        final Set<Long> eduIds = new HashSet<>(100);
        Map<String, Object[]> item;
        try
                (BufferedReader reader = Files.newBufferedReader(file.toPath(), Charset.forName("UTF-8")))
        {
            String line, marksPresent;
            Long eduInstitutionId, personId;
            String[] parts;
            Integer endYear;
            while ((line = reader.readLine()) != null)
            {
                parts = Arrays.copyOf(line.split(CSV_SEPARATOR.toString()), 7);

                personId = Long.parseLong(parts[0]);
                if ((item = personIdMap.get(personId)) == null)
                    personIdMap.put(personId, item = new HashMap<>());

                eduInstitutionId = !parts[1].isEmpty() ? Long.parseLong(parts[1]) : null;
                if (eduInstitutionId != null)
                    eduIds.add(eduInstitutionId);

                endYear = Integer.parseInt(parts[2]);
                marksPresent = parts[3];

                item.put(getIdentificationKey(parts[4], parts[5], parts[6]), new Object[]{eduInstitutionId, endYear, marksPresent});
            }
        }
        catch (IOException x)
        {
            throw new RuntimeException("IOException in importEduInstitutions(): %s%n", x);
        }

        // Грузим из базы школы, которые в базе есть
        final List<Long> eduInstitutions = new DQLSelectBuilder().fromEntity(EduInstitution.class, "e").column(property("e.id"))
                .where(in(property("e.id"), eduIds))
                .createStatement(getSession()).list();
        eduIds.clear();

        final IDQLStatement stmt = new DQLSelectBuilder().fromEntity(PersonEduInstitution.class, "e")
                .column(property("e"))
                .where(and(
                        isNull(property(PersonEduInstitution.eduInstitution().fromAlias("e"))),
                        eq(property(PersonEduInstitution.yearEnd().fromAlias("e")), value(0)),
                        in(property(PersonEduInstitution.person().id().fromAlias("e")), parameter("ids", PropertyType.LONG))
                ))
                .createStatement(getSession());

        final List<Integer> counterContainer = new ArrayList<>(1);
        counterContainer.add(0);
        BatchUtils.execute(personIdMap.keySet(), 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(final Collection<Long> ids)
            {
                Object[] oldValue;
                Boolean recovered;
                Long oldEduInstId;
                int oldYearEnd;
                String oldMarksPresent, identKey;
                for (final PersonEduInstitution peronEduInstitution : stmt.setParameter("ids", ids).<PersonEduInstitution>list())
                {
                    recovered = false;
                    identKey = getIdentificationKey(peronEduInstitution.getIssuanceDate() != null ? String.valueOf(peronEduInstitution.getIssuanceDate().getTime()) : null,
                                                    peronEduInstitution.getSeria(), peronEduInstitution.getNumber());

                    if ((oldValue = personIdMap.get(peronEduInstitution.getPerson().getId()).remove(identKey)) == null)
                        continue;

                    oldEduInstId = (Long) oldValue[0];
                    oldYearEnd = (Integer) oldValue[1];
                    oldMarksPresent = (String) oldValue[2];

                    if (eduInstitutions.contains(oldEduInstId))
                    {
                        peronEduInstitution.setEduInstitution((EduInstitution) proxy(oldEduInstId));
                        recovered = true;
                    }

                    if (peronEduInstitution.getYearEnd() != oldYearEnd)
                    {
                        peronEduInstitution.setYearEnd(oldYearEnd);
                        recovered = true;
                    }

                    if (peronEduInstitution.getMark3() != null && peronEduInstitution.getMark3() == 0 && oldMarksPresent.charAt(0) == '0')
                    {
                        peronEduInstitution.setMark3(null);
                        recovered = true;
                    }

                    if (peronEduInstitution.getMark4() != null && peronEduInstitution.getMark4() == 0 && oldMarksPresent.charAt(1) == '0')
                    {
                        peronEduInstitution.setMark4(null);
                        recovered = true;
                    }

                    if (peronEduInstitution.getMark5() != null && peronEduInstitution.getMark5() == 0 && oldMarksPresent.charAt(2) == '0')
                    {
                        peronEduInstitution.setMark5(null);
                        recovered = true;
                    }

                    if (recovered)
                    {
                        update(peronEduInstitution);
                        counterContainer.set(0, counterContainer.get(0) + 1);
                    }
                }
            }
        });

        return counterContainer.get(0) + " из " + personIdMap.size();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void makeArchivalEmptyGroupDuplicates()
    {
        // очищаем сессию, т.к.в противном случае объект поднимается не из базы, а из кеша
        // и изменения не сохраняются
        getSession().clear();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Group.class, "g")
                .column(property(Group.id().fromAlias("g")))
                .where(eq(property("g", Group.archival()), value(false)))
                .where(notExists(Student.class, Student.L_GROUP, (Object) property("g")));

        List<Long> groupsEmptyList = getList(builder);

        List<Group> groupsList = getList(Group.class);

        Map<String, List<Long>> groupTitleToIdsListMap = new HashMap<>();
        for (Group group : groupsList)
        {
            List<Long> idsList = groupTitleToIdsListMap.get(group.getTitle());
            if (null == idsList) idsList = new ArrayList<>();
            idsList.add(group.getId());
            groupTitleToIdsListMap.put(group.getTitle(), idsList);
        }

        int deleteCount = 0;
        for (Map.Entry<String, List<Long>> entry : groupTitleToIdsListMap.entrySet())
        {
            if (entry.getValue().size() > 1)
            {
                for (Long groupId : entry.getValue())
                {
                    if (groupsEmptyList.contains(groupId))
                    {
                        System.out.println("Group " + entry.getKey() + " with id=" + groupId + " was sent to archive, because it does not contain any student.");
                        Group grp = get(groupId);
                        UniDaoFacade.getGroupDao().deleteCaptainStudentList(grp);
                        grp.setArchival(true);
                        grp.setArchivingDate(new Date());
                        update(grp);

                        deleteCount++;
                    }
                }
            }
        }
        System.out.println("There are " + deleteCount + " groups were sent to archive.");
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void makeArchivalEmptyGroups()
    {
        // очищаем сессию, т.к.в противном случае объект поднимается не из базы, а из кеша
        // и изменения не сохраняются
        getSession().clear();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Group.class, "g")
                .column("g")
                .where(eq(property("g", Group.archival()), value(false)))
                .where(notExists(Student.class, Student.L_GROUP, (Object) property("g")));

        List<Group> emptyGroupList = getList(builder);

        int deleteCount = 0;
        for (Group grp : emptyGroupList)
        {
            System.out.println("Group with id=" + grp.getId() + " was sent to archive, because it does not contain any student.");
            UniDaoFacade.getGroupDao().deleteCaptainStudentList(grp);

            grp.setArchival(true);
            grp.setArchivingDate(new Date());
            update(grp);

            deleteCount++;
        }

        System.out.println("There are " + deleteCount + " groups were sent to archive.");
    }

    @Override
    public void doArchiveEduPlanVersions(@NotNull Integer eduEndYear, @NotNull EduProgramDuration eduProgramDuration)
    {
        Preconditions.checkNotNull(eduEndYear);
        Preconditions.checkNotNull(eduProgramDuration);
        EppState archivedState = getByNaturalId(new EppState.NaturalId(EppState.STATE_ARCHIVED));
        DQLUpdateBuilder updateEduPlanVersionsDQL = new DQLUpdateBuilder(EppEduPlanVersion.class)
                .set(EppEduPlanVersion.L_STATE, value(archivedState))
                .where(ne(property(EppEduPlanVersion.state()), value(archivedState)))
                .joinPath(DQLJoinType.inner, EppEduPlanVersion.eduPlan(), "ep")
                .where(eq(EppEduPlanVersion.L_EDU_PLAN, "ep.id"))
                .where(eq(property(EppEduPlan.eduEndYear().fromAlias("ep")), value(eduEndYear)))
                .where(eq(property(EppEduPlan.developGrid().developPeriod().eduProgramDuration().fromAlias("ep")), value(eduProgramDuration)));
        updateEduPlanVersionsDQL
                .createStatement(getSession())
                .execute();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void makeArchivalGroupsWithNotActiveStudents()
    {
        // очищаем сессию, т.к.в противном случае объект поднимается не из базы, а из кеша
        // и изменения не сохраняются
        getSession().clear();

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Student.class, "st");
        subBuilder.column(property("st", Student.group().id()));
        subBuilder.where(eq(property("st", Student.status().active()), value(true)));
        subBuilder.where(eq(property("st", Student.archival()), value(false)));
        subBuilder.where(eq(property("st", Student.group().archival()), value(false)));

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Group.class, "g");
        builder.where(notIn(property("g", Group.id()), subBuilder.buildQuery()));
        builder.where(eq(property("g", Group.archival()), value(false)));

        List<Group> groupForArchive = getList(builder);

        int archivedCount = 0;
        for (Group grp : groupForArchive)
        {
            UniDaoFacade.getGroupDao().deleteCaptainStudentList(grp);
            System.out.println("Group with id=" + grp.getId() + " was sent to archive, because it does not contain active and non-archival student.");
            grp.setArchival(true);
            grp.setArchivingDate(new Date());
            getSession().update(grp);

            archivedCount++;
        }

        System.out.println("There are " + archivedCount + " groups were sent to archive.");
    }

    @Override
    public void doAssignForeignStudentsDeclinationSettings()
    {
        List<Student> foreignStudents = new DQLSelectBuilder()
                .fromEntity(Student.class, "s").column("s")
                .where(like(DQLFunctions.upper(property(Student.group().title().fromAlias("s"))), value(CoreStringUtils.escapeLike("!ин"))))
                .createStatement(getSession()).list();

        List<Long> idCardDeclinableIdsSet = new DQLSelectBuilder()
                .fromEntity(IdentityCardDeclinability.class, "icd")
                .column(property(IdentityCardDeclinability.identityCard().id().fromAlias("icd")))
                .createStatement(getSession()).list();

        for (Student student : foreignStudents)
        {
            if (!idCardDeclinableIdsSet.contains(student.getPerson().getIdentityCard().getId()))
            {
                IdentityCardDeclinability declinability = new IdentityCardDeclinability();
                declinability.setIdentityCard(student.getPerson().getIdentityCard());
                idCardDeclinableIdsSet.add(declinability.getIdentityCard().getId());
                declinability.setLastNameNonDeclinable(true);
                declinability.setFirstNameNonDeclinable(true);
                declinability.setMiddleNameNonDeclinable(true);
                save(declinability);
            }
        }
    }

    public void doChangeEntranceYearForArchiveStudents()
    {
        List<Student> archiveStudentsList = new DQLSelectBuilder().fromEntity(Student.class, "s").column("s")
                .where(eq(property(Student.archival().fromAlias("s")), value(Boolean.TRUE)))
                .where(ge(property(Student.entranceYear().fromAlias("s")), value(2000)))
                .createStatement(getSession()).list();

        for (Student student : archiveStudentsList)
        {
            student.setEntranceYear(student.getEntranceYear() - 100);
            update(student);
        }
    }

    public void doFillEduLevelQualifications() throws Exception
    {
        Logger log4j_logger = Logger.getLogger(FefuSystemActionDao.class);
        Appender appender = log4j_logger.getAppender("FefuEduLevelQualificationLogAppender");

        if (null == appender)
        {
            // добавляем, если нет
            final String path = ApplicationRuntime.getAppInstallPath();
            appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} %p - %m%n"), FilenameUtils.concat(path, "tomcat/logs/EduLevelQualificationsFilling.log"));
            appender.setName("FefuEduLevelQualificationLogAppender");
            ((FileAppender) appender).setThreshold(Level.INFO);
            log4j_logger.addAppender(appender);
            log4j_logger.setLevel(Level.INFO);
        }

        log4j_logger.info("Начато заполнение квалификаций для министерских направлений подготовки.");

        List<EducationLevels> levels = new DQLSelectBuilder()
                .fromEntity(EducationLevels.class, "el").column("el")
                .where(isNull(property(EducationLevels.qualification().fromAlias("el"))))
                .createStatement(getSession()).list();

        List<String> updatedQualifications = new ArrayList<>();
        List<String> skippedQualifications = new ArrayList<>();

        for (EducationLevels level : levels)
        {
            if (level.getLevelType().isQualificationRequired())
            {
                EducationLevels parentLevel = level.getParentLevel();
                Qualifications qual = parentLevel.getQualification();

                while (null == qual && null != parentLevel)
                {
                    qual = parentLevel.getQualification();
                    parentLevel = parentLevel.getParentLevel();
                }

                if (null != qual)
                {
                    level.setQualification(qual);
                    update(qual);
                    updatedQualifications.add(level.getDisplayableTitle() + " (квалификация \"" + qual.getTitle() + "\")");
                }
                else
                {
                    skippedQualifications.add(level.getDisplayableTitle());
                }
            }
        }

        Collections.sort(updatedQualifications);
        Collections.sort(skippedQualifications);

        log4j_logger.info("\n================== Обновленные направления подготовки: ==================");
        for (String item : updatedQualifications) log4j_logger.info(item);

        log4j_logger.info("\n================== Пропущенные направления подготовки: ==================");
        for (String item : skippedQualifications) log4j_logger.info(item);

        appender.close();
        log4j_logger.removeAppender(appender);
    }

    public void renderPersonalDataReport(boolean includeStudents, boolean includeEntrants, boolean includeEmployee, boolean includeActiveStudent, boolean includeArchival, final OutputStream stream) throws IOException
    {
        List<FefuPersonalDataWrapper> result = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        if (includeStudents)
        {
            unloadStudentData(result, sdf, false, true);
        }

        if (includeActiveStudent)
        {
            unloadStudentData(result, sdf, true, false);
        }

        if (includeEntrants)
        {
            unloadEntrantsData(result, sdf);
        }

        if (includeEmployee)
        {
            unloadEmployeeData(result, sdf);
        }

        writeTitle(includeStudents, includeEntrants, includeEmployee, includeActiveStudent, stream, sdf);

        writeTableHeader(stream);

        writeTableDataToFile(stream, result);
    }

    /**
     * Выгрузка данных сотрудников
     * @param result
     * @param sdf
     */
    private void unloadEmployeeData(List<FefuPersonalDataWrapper> result, SimpleDateFormat sdf)
    {
        List<Object[]> employeeList = new DQLSelectBuilder()
                .fromEntity(EmployeePost.class, "ep")
                .joinEntity("ep", DQLJoinType.inner, Employee.class, "e", eq(property(EmployeePost.employee().id().fromAlias("ep")), property(Employee.id().fromAlias("e"))))
                .joinEntity("e", DQLJoinType.inner, Person.class, "p", eq(property(Employee.person().id().fromAlias("e")), property(Person.id().fromAlias("p"))))
                .joinEntity("p", DQLJoinType.inner, IdentityCard.class, "ic", eq(property(Person.identityCard().id().fromAlias("p")), property(IdentityCard.id().fromAlias("ic"))))
                .joinEntity("ic", DQLJoinType.left, AddressBase.class, "ad", eq(property(IdentityCard.address().id().fromAlias("ic")), property(AddressBase.id().fromAlias("ad"))))
                .joinEntity("p", DQLJoinType.left, FefuNsiIds.class, "ids", eq(property(Person.id().fromAlias("p")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .column(property(Person.id().fromAlias("p")))
                .column(property(FefuNsiIds.guid().fromAlias("ids")))
                .column(property(FefuNsiIds.guid().fromAlias("ids")))
                .column(property(IdentityCard.lastName().fromAlias("ic")))
                .column(property(IdentityCard.firstName().fromAlias("ic")))
                .column(property(IdentityCard.middleName().fromAlias("ic")))
                .column(property(IdentityCard.sex().title().fromAlias("ic")))
                .column(property(IdentityCard.birthDate().fromAlias("ic")))
                .column(property(IdentityCard.birthPlace().fromAlias("ic")))
                .column(property(IdentityCard.cardType().title().fromAlias("ic")))
                .column(property(IdentityCard.seria().fromAlias("ic")))
                .column(property(IdentityCard.number().fromAlias("ic")))
                .column(property(IdentityCard.issuanceDate().fromAlias("ic")))
                .column(property(IdentityCard.issuancePlace().fromAlias("ic")))
                .column(property(IdentityCard.issuanceCode().fromAlias("ic")))
                .column(property("ad"))
                .column(property(Person.innNumber().fromAlias("p")))
                .column(property(Person.snilsNumber().fromAlias("p")))
                .order(property(IdentityCard.lastName().fromAlias("ic")))
                .order(property(IdentityCard.firstName().fromAlias("ic")))
                .order(property(IdentityCard.middleName().fromAlias("ic")))
                .createStatement(getSession()).list();

        for (Object[] employee : employeeList)
        {
            Long personId = (Long) employee[0];
            String personGuid = (String) employee[1];
            FefuPersonalDataWrapper wrapper = new FefuPersonalDataWrapper(personId, personGuid);
            fillWrapper(wrapper, "Сотрудник", employee, sdf);
            result.add(wrapper);
        }
    }

    /**
     * Выгрузка данных абитуриентов
     * @param result
     * @param sdf
     */
    private void unloadEntrantsData(List<FefuPersonalDataWrapper> result, SimpleDateFormat sdf)
    {
        List<Object[]> entrantList = new DQLSelectBuilder()
                .fromEntity(Entrant.class, "e")
                .joinEntity("e", DQLJoinType.inner, Person.class, "p", eq(property(Entrant.person().id().fromAlias("e")), property(Person.id().fromAlias("p"))))
                .joinEntity("p", DQLJoinType.inner, IdentityCard.class, "ic", eq(property(Person.identityCard().id().fromAlias("p")), property(IdentityCard.id().fromAlias("ic"))))
                .joinEntity("ic", DQLJoinType.left, AddressBase.class, "ad", eq(property(IdentityCard.address().id().fromAlias("ic")), property(AddressBase.id().fromAlias("ad"))))
                .joinEntity("p", DQLJoinType.left, FefuNsiIds.class, "ids", eq(property(Person.id().fromAlias("p")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .column(property(Person.id().fromAlias("p")))
                .column(property(FefuNsiIds.guid().fromAlias("ids")))
                .column(property(FefuNsiIds.guid().fromAlias("ids")))
                .column(property(IdentityCard.lastName().fromAlias("ic")))
                .column(property(IdentityCard.firstName().fromAlias("ic")))
                .column(property(IdentityCard.middleName().fromAlias("ic")))
                .column(property(IdentityCard.sex().title().fromAlias("ic")))
                .column(property(IdentityCard.birthDate().fromAlias("ic")))
                .column(property(IdentityCard.birthPlace().fromAlias("ic")))
                .column(property(IdentityCard.cardType().title().fromAlias("ic")))
                .column(property(IdentityCard.seria().fromAlias("ic")))
                .column(property(IdentityCard.number().fromAlias("ic")))
                .column(property(IdentityCard.issuanceDate().fromAlias("ic")))
                .column(property(IdentityCard.issuancePlace().fromAlias("ic")))
                .column(property(IdentityCard.issuanceCode().fromAlias("ic")))
                .column(property("ad"))
                .column(property(Person.innNumber().fromAlias("p")))
                .column(property(Person.snilsNumber().fromAlias("p")))
                .order(property(IdentityCard.lastName().fromAlias("ic")))
                .order(property(IdentityCard.firstName().fromAlias("ic")))
                .order(property(IdentityCard.middleName().fromAlias("ic")))
                .createStatement(getSession()).list();

        for (Object[] entrant : entrantList)
        {
            Long personId = (Long) entrant[0];
            String personGuid = (String) entrant[1];
            FefuPersonalDataWrapper wrapper = new FefuPersonalDataWrapper(personId, personGuid);
            fillWrapper(wrapper, "Абитуриент", entrant, sdf);
            result.add(wrapper);
        }
    }

    /**
     * Выгрузка данных студентов
     * @param result
     * @param sdf
     * @param onlyActive
     * @param groupByLotusId
     */
    private void unloadStudentData(List<FefuPersonalDataWrapper> result, SimpleDateFormat sdf, boolean onlyActive, boolean groupByLotusId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Student.class, "s");
        if (onlyActive)
        {
            builder.where(eq(property("s", Student.status().active()), value(Boolean.TRUE)));
        }

        builder.joinEntity("s", DQLJoinType.inner, Person.class, "p", eq(property(Student.person().id().fromAlias("s")), property(Person.id().fromAlias("p"))))
                .joinEntity("p", DQLJoinType.inner, IdentityCard.class, "ic", eq(property(Person.identityCard().id().fromAlias("p")), property(IdentityCard.id().fromAlias("ic"))))
                .joinEntity("ic", DQLJoinType.left, AddressBase.class, "ad", eq(property(IdentityCard.address().id().fromAlias("ic")), property(AddressBase.id().fromAlias("ad"))))
                .joinEntity("s", DQLJoinType.left, StudentFefuExt.class, "ex", eq(property(Student.id().fromAlias("s")), property(StudentFefuExt.student().id().fromAlias("ex"))))
                .joinEntity("p", DQLJoinType.left, FefuNsiIds.class, "ids", eq(property(Person.id().fromAlias("p")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .column(property(Person.id().fromAlias("p")))
                .column(property(FefuNsiIds.guid().fromAlias("ids")))
                .column(property(StudentFefuExt.integrationId().fromAlias("ex")))
                .column(property(IdentityCard.lastName().fromAlias("ic")))
                .column(property(IdentityCard.firstName().fromAlias("ic")))
                .column(property(IdentityCard.middleName().fromAlias("ic")))
                .column(property(IdentityCard.sex().title().fromAlias("ic")))
                .column(property(IdentityCard.birthDate().fromAlias("ic")))
                .column(property(IdentityCard.birthPlace().fromAlias("ic")))
                .column(property(IdentityCard.cardType().title().fromAlias("ic")))
                .column(property(IdentityCard.seria().fromAlias("ic")))
                .column(property(IdentityCard.number().fromAlias("ic")))
                .column(property(IdentityCard.issuanceDate().fromAlias("ic")))
                .column(property(IdentityCard.issuancePlace().fromAlias("ic")))
                .column(property(IdentityCard.issuanceCode().fromAlias("ic")))
                .column(property("ad"))
                .column(property(Person.innNumber().fromAlias("p")))
                .column(property(Person.snilsNumber().fromAlias("p")))
                .column(property(Student.educationOrgUnit().fromAlias("s")))
                .column(property(Student.status().title().fromAlias("s")))
                .column(property(Student.compensationType().shortTitle().fromAlias("s")))
                .column("s")
                .order(property(IdentityCard.lastName().fromAlias("ic")))
                .order(property(IdentityCard.firstName().fromAlias("ic")))
                .order(property(IdentityCard.middleName().fromAlias("ic")));

        List<Object[]> studentList = builder.createStatement(getSession()).list();

        if (!groupByLotusId)
        {
            Map<Long, FefuPersonalDataWrapper> wrappersMap = new HashMap<>();
            for (Object[] student : studentList)
            {
                Long personId = (Long) student[0];
                String personGuid = (String) student[1];
                String lotusId = (String) student[2];
                FefuPersonalDataWrapper wrapper = wrappersMap.get(personId);

                if (null != wrapper)
                {
                    if (null != wrapper.getLotusId() && wrapper.getLotusId().length() > 0)
                        wrapper.setLotusId(wrapper.getLotusId() + ", " + lotusId);
                    else
                        wrapper.setLotusId(lotusId);
                }
                else
                {
                    wrapper = new FefuPersonalDataWrapper(personId, personGuid);
                    wrapper.setLotusId(lotusId);
                    fillWrapper(wrapper, "Студент", student, sdf);
                    result.add(wrapper);
                }
                wrappersMap.put(personId, wrapper);
            }
        }
        else
        {
            for (Object[] student : studentList)
            {
                Long personId = (Long) student[0];
                String personGuid = (String) student[1];
                String lotusId = (String) student[2];

                FefuPersonalDataWrapper wrapper = new FefuPersonalDataWrapper(personId, personGuid);
                wrapper.setLotusId(lotusId);
                fillWrapper(wrapper, "Студент", student, sdf);
                result.add(wrapper);
            }
        }
    }

    /**
     *  Создание заголовка файла
     */
    private void writeTitle(boolean includeStudents, boolean includeEntrants, boolean includeEmployee, boolean includeActiveStudent, OutputStream stream, SimpleDateFormat sdf) throws IOException
    {
        final StringBuilder sb = new StringBuilder();
        sb.append("\"Выборка персональных данных по ");
        if (includeStudents) sb.append("студентам");
        if (includeActiveStudent) sb.append("активным студентам (с дублями по ФЛ)");
        if (includeEntrants) sb.append(sb.length() > 40 ? ", " : "").append("абитуриентам");
        if (includeEmployee) sb.append(sb.length() > 40 ? ", " : "").append("сотрудникам");
        sb.append(" на ").append(sdf.format(new Date())).append("\"\n");
        stream.write(sb.toString().getBytes("UTF-8"));
    }

    private void writeTableHeader(OutputStream stream) throws IOException
    {
        final StringBuilder sb = new StringBuilder();
        sb.append("\"GUID\";");
        sb.append("\"Идентификатор персоны\";");
        sb.append("\"Lotus ID\";");
        sb.append("\"Фамилия\";");
        sb.append("\"Имя\";");
        sb.append("\"Отчество\";");
        //sb.append("\"ФИО\";");
        sb.append("\"Пол\";");
        sb.append("\"Дата рождения\";");
            /*sb.append("\"Место рождения\";");
            sb.append("\"Место рождения по ОКАТО\";");
            sb.append("\"Тип удостоверения личности\";");
            sb.append("\"Серия УЛ\";");*/
        sb.append("\"Номер УЛ\";");
            /*sb.append("\"Дата выдачи УЛ\";");
            sb.append("\"Наименование подразделения, выдавшего УЛ\";");
            sb.append("\"Код подразделения, выдавшего УЛ\";");*/
        sb.append("\"Категория персоны\";");
        sb.append("\"Адрес регистрации\";");
        sb.append("\"Школа\";");
        sb.append("\"Специальность\";");
        sb.append("\"Группа\";");
        sb.append("\"Статус учащегося\";");
        sb.append("\"Форма обучения\";");
        sb.append("\"Основа обучения\";");
            /*sb.append("\"ИНН\";");
            sb.append("\"ФНС\";");
            sb.append("\"ПФР (СНИЛС)\";");
            sb.append("\"Научные труды\";");
            sb.append("\"Изобретения\"\n");*/
        sb.append("\"Курс студента\";");
        sb.append("\"Целевой прием\";");
        sb.append("\"Населенный пункт\"\n");
        stream.write(sb.toString().getBytes("UTF-8")); //Cp1251
    }

    private void writeTableDataToFile(OutputStream stream, List<FefuPersonalDataWrapper> result) throws IOException
    {
        for (FefuPersonalDataWrapper wrapper : result)
        {
            final StringBuilder sb = new StringBuilder();
            sb.append("\"").append(wrapper.getPersonGUID()).append("\";");
            sb.append("\"").append(wrapper.getPersonId()).append("\";");
            sb.append("\"").append(null != wrapper.getLotusId() ? wrapper.getLotusId() : "").append("\";");
            sb.append("\"").append(null != wrapper.getLastName() ? wrapper.getLastName() : "").append("\";");
            sb.append("\"").append(null != wrapper.getFirstName() ? wrapper.getFirstName() : "").append("\";");
            sb.append("\"").append(null != wrapper.getMiddleName() ? wrapper.getMiddleName() : "").append("\";");
            //sb.append("\"").append(null != wrapper.getFullName() ? wrapper.getFullName() : "").append("\";");
            sb.append("\"").append(null != wrapper.getSex() ? wrapper.getSex() : "").append("\";");
            sb.append("\"").append(null != wrapper.getBirthDate() ? wrapper.getBirthDate() : "").append("\";");
            /*sb.append("\"").append(null != wrapper.getBirthPlace() ? wrapper.getBirthPlace() : "").append("\";");
            sb.append("\"").append(null != wrapper.getBirthPlaceOKATO() ? wrapper.getBirthPlaceOKATO() : "").append("\";");
            sb.append("\"").append(null != wrapper.getIdentityCardType() ? wrapper.getIdentityCardType() : "").append("\";");
            sb.append("\"").append(null != wrapper.getIdentityCardSeria() ? wrapper.getIdentityCardSeria() : "").append("\";");*/
            sb.append("\"").append(null != wrapper.getIdentityCardNumber() ? wrapper.getIdentityCardNumber() : "").append("\";");
            /*sb.append("\"").append(null != wrapper.getIdentityCardIssuanceDate() ? wrapper.getIdentityCardIssuanceDate() : "").append("\";");
            sb.append("\"").append(null != wrapper.getIdentityCardIssuancePlace() ? wrapper.getIdentityCardIssuancePlace() : "").append("\";");
            sb.append("\"").append(null != wrapper.getIdentityCardIssuancePlaceCode() ? wrapper.getIdentityCardIssuancePlaceCode() : "").append("\";");*/
            sb.append("\"").append(null != wrapper.getPersonCategory() ? wrapper.getPersonCategory() : "").append("\";");
            sb.append("\"").append(null != wrapper.getRegistrationAddress() ? wrapper.getRegistrationAddress() : "").append("\";");
            sb.append("\"").append(null != wrapper.getSchool() ? wrapper.getSchool() : "").append("\";");
            sb.append("\"").append(null != wrapper.getEducationLevel() ? wrapper.getEducationLevel() : "").append("\";");
            sb.append("\"").append(null != wrapper.getGroup() ? wrapper.getGroup() : "").append("\";");
            sb.append("\"").append(null != wrapper.getStatus() ? wrapper.getStatus() : "").append("\";");
            sb.append("\"").append(null != wrapper.getDevelopForm() ? wrapper.getDevelopForm() : "").append("\";");
            sb.append("\"").append(null != wrapper.getCompensationType() ? wrapper.getCompensationType() : "").append("\";");
            /*sb.append("\"").append(null != wrapper.getInn() ? wrapper.getInn() : "").append("\";");
            sb.append("\"").append(null != wrapper.getFns() ? wrapper.getFns() : "").append("\";");
            sb.append("\"").append(null != wrapper.getPfr() ? wrapper.getPfr() : "").append("\";");
            sb.append("\"").append(null == wrapper.getLearnedWork() ? "0" : "1").append("\";");
            sb.append("\"").append(null == wrapper.getInvention() ? "0" : "1").append("\n");*/
            sb.append("\"").append(null != wrapper.getCourse() ? wrapper.getCourse() : "").append("\";");
            sb.append("\"").append(!StringUtils.isEmpty(wrapper.getSpecialPurposeRecruit()) ? wrapper.getSpecialPurposeRecruit() : "0").append("\";");
            sb.append("\"").append(null != wrapper.getRegAddrCity() ? wrapper.getRegAddrCity() : "").append("\"\n");
            stream.write(sb.toString().getBytes("UTF-8"));
        }
    }


    public void renderPersonalDataReportLibrary(boolean includeArchival, final OutputStream stream) throws IOException
    {
        List<FefuPersonalLibraryDataWrapper> result = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        List<Object[]> studentList = new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .joinEntity("s", DQLJoinType.inner, Person.class, "p", eq(property(Student.person().id().fromAlias("s")), property(Person.id().fromAlias("p"))))
                .joinEntity("p", DQLJoinType.inner, IdentityCard.class, "ic", eq(property(Person.identityCard().id().fromAlias("p")), property(IdentityCard.id().fromAlias("ic"))))
                .column(property(Person.id().fromAlias("p")))
                .column(property(IdentityCard.lastName().fromAlias("ic")))
                .column(property(IdentityCard.firstName().fromAlias("ic")))
                .column(property(IdentityCard.middleName().fromAlias("ic")))
                .column(property(IdentityCard.sex().title().fromAlias("ic")))
                .column(property(Student.educationOrgUnit().fromAlias("s")))
                .column(property(Student.status().title().fromAlias("s")))
                .column(property(Student.compensationType().shortTitle().fromAlias("s")))
                .column("s")
                .order(property(IdentityCard.lastName().fromAlias("ic")))
                .order(property(IdentityCard.firstName().fromAlias("ic")))
                .order(property(IdentityCard.middleName().fromAlias("ic")))
                .createStatement(getSession()).list();

        for (Object[] student : studentList)
        {
            Long personId = (Long) student[0];
            FefuPersonalLibraryDataWrapper wrapper = new FefuPersonalLibraryDataWrapper(personId);
            fillLibraryWrapper(wrapper, student);
            result.add(wrapper);
        }

        {
            final StringBuilder sb = new StringBuilder();
            sb.append("\"Выборка данных по студентам на ").append(sdf.format(new Date())).append("\"\n");
            stream.write(sb.toString().getBytes("UTF-8"));
        }
        {
            final StringBuilder sb = new StringBuilder();
            sb.append("\"GUID\";");
            sb.append("\"Фамилия\";");
            sb.append("\"Имя\";");
            sb.append("\"Отчество\";");
            sb.append("\"Курс студента\";");
            sb.append("\"Группа\";");
            sb.append("\"Формирующее подразделение\";");
            sb.append("\"Территориальное подразделение\";");
            sb.append("\"Выпускающее подразделение\";");
            sb.append("\"Направление подготовки\";");
            sb.append("\"Направление/специальность ГОС\";");
            sb.append("\"Год поступления\";");
            sb.append("\"Форма освоения\";");
            sb.append("\"Состояние студента\";");
            sb.append("\"Номер зачетной книжки\"\n");
            stream.write(sb.toString().getBytes("UTF-8")); //Cp1251
        }

        for (FefuPersonalLibraryDataWrapper wrapper : result)
        {
            final StringBuilder sb = new StringBuilder();
            sb.append("\"").append(wrapper.getPersonGUID()).append("\";");
            sb.append("\"").append(null != wrapper.getLastName() ? wrapper.getLastName() : "").append("\";");
            sb.append("\"").append(null != wrapper.getFirstName() ? wrapper.getFirstName() : "").append("\";");
            sb.append("\"").append(null != wrapper.getMiddleName() ? wrapper.getMiddleName() : "").append("\";");
            sb.append("\"").append(null != wrapper.getCourse() ? wrapper.getCourse() : "").append("\";");
            sb.append("\"").append(null != wrapper.getGroup() ? wrapper.getGroup() : "").append("\";");
            sb.append("\"").append(null != wrapper.getFormativeOrgUnit() ? wrapper.getFormativeOrgUnit() : "").append("\";");
            sb.append("\"").append(null != wrapper.getTerritorialOrgUnit() ? wrapper.getTerritorialOrgUnit() : "").append("\";");
            sb.append("\"").append(null != wrapper.getProductiveOrgUnit() ? wrapper.getProductiveOrgUnit() : "").append("\";");
            sb.append("\"").append(null != wrapper.getEducationLevelHS() ? wrapper.getEducationLevelHS() : "").append("\";");
            sb.append("\"").append(null != wrapper.getEducationLevel() ? wrapper.getEducationLevel() : "").append("\";");
            sb.append("\"").append(null != wrapper.getEntranceYear() ? wrapper.getEntranceYear() : "").append("\";");
            sb.append("\"").append(null != wrapper.getDevelopForm() ? wrapper.getDevelopForm() : "").append("\";");
            sb.append("\"").append(null != wrapper.getStatus() ? wrapper.getStatus() : "").append("\";");
            sb.append("\"").append(null != wrapper.getBookNumber() ? wrapper.getBookNumber() : "").append("\"\n");
            stream.write(sb.toString().getBytes("UTF-8"));
        }
    }

    private void fillWrapper(FefuPersonalDataWrapper wrapper, String personCategory, Object[] line, SimpleDateFormat sdf)
    {
        wrapper.setPersonCategory(personCategory);
        wrapper.setLastName((String) line[3]);
        wrapper.setFirstName((String) line[4]);
        wrapper.setMiddleName((String) line[5]);
        wrapper.setSex((String) line[6]);
        if (null != line[7]) wrapper.setBirthDate(sdf.format((Date) line[7]));
        wrapper.setBirthPlace((String) line[8]);
        wrapper.setIdentityCardType((String) line[9]);
        wrapper.setIdentityCardSeria((String) line[10]);
        wrapper.setIdentityCardNumber((String) line[11]);
        if (null != line[12]) wrapper.setIdentityCardIssuanceDate(sdf.format((Date) line[12]));
        wrapper.setIdentityCardIssuancePlace((String) line[13]);
        wrapper.setIdentityCardIssuancePlaceCode((String) line[14]);
        if (null != line[15])
        {
            wrapper.setRegistrationAddress(((AddressBase) line[15]).getTitleWithFlat());
            if (line[15] instanceof AddressDetailed && null != ((AddressDetailed) line[15]).getSettlement())
                wrapper.setRegAddrCity(((AddressDetailed) line[15]).getSettlement().getTitleWithType());
            if (line[15] instanceof AddressString) wrapper.setRegAddrCity(((AddressString) line[15]).getAddress());
        }
        wrapper.setInn((String) line[16]);
        wrapper.setPfr((String) line[17]);

        if (line.length > 18)
        {
            Student student = (Student) line[21];
            EducationOrgUnit eduOrgUnit = (EducationOrgUnit) line[18];
            wrapper.setStatus((String) line[19]);
            wrapper.setCompensationType((String) line[20]);

            boolean nonTerr = eduOrgUnit.getTerritorialOrgUnit().isTop();
            if (nonTerr) wrapper.setSchool(eduOrgUnit.getFormativeOrgUnit().getTitle());
            else wrapper.setSchool(eduOrgUnit.getTerritorialOrgUnit().getTitle());
            wrapper.setDevelopForm(eduOrgUnit.getDevelopForm().getTitle());
            if (null != student.getGroup()) wrapper.setGroup(student.getGroup().getTitle());

            EducationLevels eduLevel = eduOrgUnit.getEducationLevelHighSchool().getEducationLevel();

            final Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(eduLevel);
            personCategory = "Студент (квалификация не указана)";

            if (null != qualification)
            {
                switch (qualification.getCode())
                {
                    case QualificationsCodes.MAGISTR:
                        personCategory = "Студент (магистратура)";
                        break;
                    case QualificationsCodes.BAKALAVR:
                        personCategory = "Студент (бакалавриат)";
                        break;
                    case QualificationsCodes.SPETSIALIST:
                        personCategory = "Студент (специалитет)";
                        break;
                    case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
                        personCategory = "Школьник (СПО)";
                        break;
                    case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
                        personCategory = "Школьник (СПО)";
                        break;
                    default:
                        personCategory = "Студент (квалификация не указана)";
                }
            }

            wrapper.setPersonCategory(personCategory);

            final String title = " «" + eduLevel.getTitle() + "»";
            final StructureEducationLevels levelType = eduLevel.getLevelType();
            final String direction = " " + CommonExtractPrint.getFefuHighEduLevelStr(eduLevel);
            String educationStrDirection;

            if (levelType.isSpecialization()) // специализация
            {
                educationStrDirection = "специальность" + direction + " специализация" + title;
            }
            else if (levelType.isSpecialty()) // специальность
            {
                educationStrDirection = "специальность" + direction;
            }
            else if (levelType.isProfile() && levelType.isBachelor()) // бакалаврский профиль
            {
                educationStrDirection = "направление" + direction + " профиль" + title;
            }
            else if (levelType.isProfile() && levelType.isMaster()) // магистерский профиль
            {
                educationStrDirection = "направление" + direction + " магистерская программа" + title;
            }
            else // направление подготовки
            {
                educationStrDirection = "направление подготовки" + direction;
            }

            wrapper.setEducationLevel(educationStrDirection);
            wrapper.setCourse(student.getCourse().getTitle());
            wrapper.setSpecialPurposeRecruit(student.isTargetAdmission() ? "1" : "");
        }
    }

    private void fillLibraryWrapper(FefuPersonalLibraryDataWrapper wrapper, Object[] line)
    {

        wrapper.setLastName((String) line[1]);
        wrapper.setFirstName((String) line[2]);
        wrapper.setMiddleName((String) line[3]);
        wrapper.setSex((String) line[4]);

        Student student = (Student) line[8];
        EducationOrgUnit eduOrgUnit = (EducationOrgUnit) line[5];
        wrapper.setStatus((String) line[6]);

        wrapper.setCourse(student.getCourse().getTitle());
        wrapper.setGroup(null != student.getGroup() ? student.getGroup().getTitle() : "");

        boolean terr = !eduOrgUnit.getTerritorialOrgUnit().isTop();
        wrapper.setFormativeOrgUnit(eduOrgUnit.getFormativeOrgUnit().getTitle());
        wrapper.setProductiveOrgUnit(eduOrgUnit.getEducationLevelHighSchool().getOrgUnit().getTitle());
        if (terr) wrapper.setTerritorialOrgUnit(eduOrgUnit.getTerritorialOrgUnit().getTitle());
        wrapper.setDevelopForm(eduOrgUnit.getDevelopForm().getTitle());
        if (null != student.getGroup()) wrapper.setGroup(student.getGroup().getTitle());

        EducationLevels eduLevel = eduOrgUnit.getEducationLevelHighSchool().getEducationLevel();

        final String title = " «" + eduLevel.getTitle() + "»";
        final StructureEducationLevels levelType = eduLevel.getLevelType();
        final String direction = " " + CommonExtractPrint.getFefuHighEduLevelStr(eduLevel);
        String educationStrDirection;

        if (levelType.isSpecialization()) // специализация
        {
            educationStrDirection = "специальность" + direction + " специализация" + title;
        }
        else if (levelType.isSpecialty()) // специальность
        {
            educationStrDirection = "специальность" + direction;
        }
        else if (levelType.isProfile() && levelType.isBachelor()) // бакалаврский профиль
        {
            educationStrDirection = "направление" + direction + " профиль" + title;
        }
        else if (levelType.isProfile() && levelType.isMaster()) // магистерский профиль
        {
            educationStrDirection = "направление" + direction + " магистерская программа" + title;
        }
        else // направление подготовки
        {
            educationStrDirection = "направление подготовки" + direction;
        }

        wrapper.setEducationLevel(educationStrDirection);
        wrapper.setEducationLevelHS(eduOrgUnit.getEducationLevelHighSchool().getEducationLevel().getProgramSubjectTitleWithCode());
        wrapper.setEntranceYear(String.valueOf(student.getEntranceYear()));
        wrapper.setBookNumber(student.getBookNumber());
    }

    public void doTestIdmPersonRegistrationService() throws Exception
    {
        String destURL = ApplicationRuntime.getProperty("fefu.idm.service.connectionstr");
        if (null == destURL)
            throw new ApplicationException("Не задан URL сервиса IDM, принимающего запросы на регистрацию новых физ. лиц (см. свойство fefu.idm.service.connectionstr в файлах конфигурации).");

        String shaKey = ApplicationRuntime.getProperty("fefu.idm.service.shakey1");
        if (null == shaKey)
            throw new ApplicationException("Не задан ключ для шифрования контрольной суммы (см. свойство fefu.idm.service.shakey1 в файлах конфигурации).");

        String email = "petrov@p.ru";
        String password = "Asd123456";
        String firstName = "Иван";//"Иван";
        String secondName = "Иванович";//"Иванович";
        String lastName = "Петров";//"Петров";
        String guid = "tandem_guid_is_here";
        String shaChecksum = SHAUtils.getSHAChecksum(shaKey, email, password, lastName, firstName, secondName, guid);

        System.out.println("System action for IDM integration testing has started...");
        System.out.println("email\t= " + email);
        System.out.println("password\t= " + password);
        System.out.println("lastName\t= " + lastName);
        System.out.println("firstName\t= " + firstName);
        System.out.println("secondName\t= " + secondName);
        System.out.println("guidPerson\t= " + guid);
        System.out.println("shaKey\t= " + shaKey);
        System.out.println("hashStr\t= " + shaChecksum);

        HttpClient httpClient = new HttpClient();
        PostMethod post = new PostMethod();
        post.addRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        post.setPath(destURL);

        post.setParameter("email", email);
        post.setParameter("password", password);
        post.setParameter("firstName", firstName);
        post.setParameter("secondName", secondName);
        post.setParameter("lastName", lastName);
        post.setParameter("guidPerson", guid);
        post.setParameter("hashStr", shaChecksum);

        httpClient.executeMethod(post);
        System.out.println("System action for IDM integration testing was finished. The response from IDM is:");
        System.out.println(post.getResponseBodyAsString());
    }

    @Override
    public List<String> doImportContracts(Map<Long, FefuContractImportWrapper> contractsInfoMap)
    {
//        new DQLDeleteBuilder(FefuPracticeContractWithExtOu.class).createStatement(getSession()).execute();
//        new DQLDeleteBuilder(ExternalOrgUnit.class).createStatement(getSession()).execute();
        doFillExtOu(contractsInfoMap);
        checkDuplicates(contractsInfoMap);
        List<String> duplicates = Lists.newArrayList();
        for (FefuContractImportWrapper wrapper : contractsInfoMap.values())
        {
            if (!wrapper.isDublicate())
            {
                FefuPracticeContractWithExtOu contract = new FefuPracticeContractWithExtOu();
                contract.setContractNum(wrapper.getContractNumber());
                contract.setExternalOrgUnit(wrapper.getExternalOrgUnit());
                contract.setContractDate(wrapper.getContractCreateDate());
                contract.setEndDate(wrapper.getContractEndDate());
                save(contract);
            }
            else
                duplicates.add("Договор от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(wrapper.getContractCreateDate()) + " c " + wrapper.getExternalOrgUnit().getTitleWithLegalForm());
        }
        return duplicates;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void fixCompetitionGroups(EnrollmentCampaign enrollmentCampaign)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "ed");
        builder.where(isNull(property("ed", EnrollmentDirection.competitionGroup())));
        builder.where(eq(property("ed", EnrollmentDirection.enrollmentCampaign().id()), value(enrollmentCampaign.getId())));

        List<EnrollmentDirection> directions = builder.createStatement(getSession()).list();

        for (EnrollmentDirection direction : directions)
        {
            CompetitionGroup group = new CompetitionGroup();
            StringBuilder titleBuilder = new StringBuilder(direction.getShortTitle());
            if (null != direction.getEducationOrgUnit().getDevelopForm())
            {
                if (null != direction.getEducationOrgUnit().getDevelopForm().getShortTitle())
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopForm().getShortTitle());
                else
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopForm().getTitle());
            }
            if (null != direction.getEducationOrgUnit().getDevelopCondition())
            {
                if (null != direction.getEducationOrgUnit().getDevelopCondition().getShortTitle())
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopCondition().getShortTitle());
                else
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopCondition().getTitle());
            }
            if (null != direction.getEducationOrgUnit().getDevelopTech())
            {
                if (null != direction.getEducationOrgUnit().getDevelopTech().getShortTitle())
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopTech().getShortTitle());
                else
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopTech().getTitle());
            }
            if (null != direction.getEducationOrgUnit().getDevelopPeriod())
            {
                titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopPeriod().getTitle());
            }
            if (null != direction.getEducationOrgUnit().getFormativeOrgUnit())
            {
                if (null != direction.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle())
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle());
                else
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getFormativeOrgUnit().getTitle());
            }

            if (null != direction.getEducationOrgUnit().getTerritorialOrgUnit())
            {
                titleBuilder.append(", ").append(direction.getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialShortTitle());
            }

            group.setTitle(titleBuilder.toString());
            group.setEnrollmentCampaign(enrollmentCampaign);
            save(group);
            direction.setCompetitionGroup(group);
            saveOrUpdate(direction);
        }
    }

    @Override
    public void doMoveOriginals(EnrollmentCampaign enrollmentCampaign)
    {
        //Достаем все запросы по направлениям в данной приемной кампании
        DQLSelectBuilder requestDirectionsBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "rd");
        requestDirectionsBuilder.where(eq(property("rd", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().id()), value(enrollmentCampaign.getId())));

        List<RequestedEnrollmentDirection> allRequestedEnrollmentDirections = requestDirectionsBuilder.createStatement(getSession()).list();

        //Мап списка выбранных направлений по заявлениям
        Map<Long, List<RequestedEnrollmentDirection>> requestedEnrollmentDirectionsMap = Maps.newHashMap();

        for (RequestedEnrollmentDirection requestedEnrollmentDirection : allRequestedEnrollmentDirections)
        {
            //только "активный" и "к зачислению"
            if (EntrantStateCodes.ACTIVE.equals(requestedEnrollmentDirection.getState().getCode()) ||
                    EntrantStateCodes.TO_BE_ENROLED.equals(requestedEnrollmentDirection.getState().getCode()))
            {
                Long reqKey = requestedEnrollmentDirection.getEntrantRequest().getId();
                if (null == requestedEnrollmentDirectionsMap.get(reqKey))
                    requestedEnrollmentDirectionsMap.put(reqKey, Lists.<RequestedEnrollmentDirection>newArrayList());
                if (!requestedEnrollmentDirectionsMap.get(reqKey).contains(requestedEnrollmentDirection))
                    requestedEnrollmentDirectionsMap.get(reqKey).add(requestedEnrollmentDirection);
            }
        }

        for (Long reqKey : requestedEnrollmentDirectionsMap.keySet())
        {
            List<RequestedEnrollmentDirection> requestedEnrollmentDirections = requestedEnrollmentDirectionsMap.get(reqKey);
            if (requestedEnrollmentDirections.size() < 2) continue;
            //Мап выбранных направлений по направлениям
            Map<Long, List<RequestedEnrollmentDirection>> requestedDirectionsByEnrollmentDirectionsMap = Maps.newHashMap();
            for (RequestedEnrollmentDirection direction : requestedEnrollmentDirections)
            {
                Long dirKey = direction.getEnrollmentDirection().getId();
                if (null == requestedDirectionsByEnrollmentDirectionsMap.get(dirKey))
                    requestedDirectionsByEnrollmentDirectionsMap.put(dirKey, Lists.<RequestedEnrollmentDirection>newArrayList());
                if (!requestedDirectionsByEnrollmentDirectionsMap.get(dirKey).contains(direction))
                    requestedDirectionsByEnrollmentDirectionsMap.get(dirKey).add(direction);
            }

            for (Long dirKey : requestedDirectionsByEnrollmentDirectionsMap.keySet())
            {
                List<RequestedEnrollmentDirection> enrollmentDirections = requestedDirectionsByEnrollmentDirectionsMap.get(dirKey);
                if (enrollmentDirections.size() >= 2)
                {
                    boolean containOriginalOnBudget = false;
                    RequestedEnrollmentDirection contractDirection = null;
                    for (RequestedEnrollmentDirection dir : enrollmentDirections)
                    {
                        if (CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(dir.getCompensationType().getCode()))
                        {
                            contractDirection = dir;
                        }
                        if (dir.isOriginalDocumentHandedIn() && CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(dir.getCompensationType().getCode()))
                            containOriginalOnBudget = true;
                    }
                    //если оригиналы на бюджете перемещаем на договор
                    if (containOriginalOnBudget)
                    {
                        EcEntrantManager.instance().dao().updateOriginalDocuments(contractDirection, true);
                    }
                }
            }
        }
    }

    private void checkDuplicates(Map<Long, FefuContractImportWrapper> contractsInfoMap)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuPracticeContractWithExtOu.class, "ct");
        builder.column(property("ct", FefuPracticeContractWithExtOu.externalOrgUnit().id()));
        builder.column(property("ct", FefuPracticeContractWithExtOu.contractDate()));
        List<Object[]> fefuPracticeContracts = builder.createStatement(getSession()).list();

        HashSet<String> fefuPracticeContractsHashSet = Sets.newHashSet();

        for (Object[] contract : fefuPracticeContracts)
        {
            fefuPracticeContractsHashSet.add(contract[0].toString() + DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) contract[1]));
        }

        for (FefuContractImportWrapper wrapper : contractsInfoMap.values())
        {
            if (fefuPracticeContractsHashSet.contains((wrapper.getExternalOrgUnit().getId().toString() + DateFormatter.DEFAULT_DATE_FORMATTER.format(wrapper.getContractCreateDate()))))
                wrapper.setDublicate(true);
        }
    }

    private void doFillExtOu(Map<Long, FefuContractImportWrapper> contractsInfoMap)
    {
        //ОПФ
        DQLSelectBuilder legalFormBuilder = new DQLSelectBuilder().fromEntity(LegalForm.class, "lf");
        List<LegalForm> legalForms = legalFormBuilder.createStatement(getSession()).list();

        LegalForm defaultLegalForm = DataAccessServices.dao().get(LegalForm.class, LegalForm.code(), Integer.toString(14));

        Map<Long, LegalForm> legalFormMap = Maps.newHashMap();
        for (LegalForm legalForm : legalForms)
        {
            legalFormMap.put(legalForm.getId(), legalForm);
        }

        for (Long key : contractsInfoMap.keySet())
        {
            FefuContractImportWrapper wrapper = contractsInfoMap.get(key);

            String extOuTitle = wrapper.getExternalOrganizationTitle();

            List<String> splittedTitles = Lists.newArrayList(extOuTitle.split(" "));

            Long legalFormId = null;

            for (String str : splittedTitles)
            {
                for (Long keyLFM : legalFormMap.keySet())
                {
                    if (!StringUtils.isEmpty(legalFormMap.get(keyLFM).getShortTitle()))
                    {
                        if (legalFormMap.get(keyLFM).getShortTitle().equals(str))
                        {
                            legalFormId = keyLFM;
                            break;
                        }
                    }
                }
            }
            if (null != legalFormId)
            {
                wrapper.setLegalForm(legalFormMap.get(legalFormId));
            }
            else
            {
                wrapper.setLegalForm(defaultLegalForm);
            }

//            // Если ОПФ в начале то удаляем
//            if (extOuTitle.startsWith(wrapper.getLegalForm().getShortTitle()))
//            {
//                extOuTitle = extOuTitle.substring(wrapper.getLegalForm().getShortTitle().length() + 1);
//                wrapper.setExternalOrganizationTitle(extOuTitle);
//            }
//
//            if (extOuTitle.matches("(^\".{0,}\"$)|(^«.{0,}»$)"))
//            {
//                extOuTitle = extOuTitle.replaceAll("(^[«\"])|([»\"]$)", "");
//                wrapper.setExternalOrganizationTitle(extOuTitle);
//            }
        }

        //Организации
        Map<Long, String> extOuTitles = Maps.newHashMap();

        for (FefuContractImportWrapper wrapper : contractsInfoMap.values())
        {
            extOuTitles.put(wrapper.getFakeId(), wrapper.getExternalOrganizationTitle());
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ExternalOrgUnit.class, "eou");
        builder.where(in(property("eou", ExternalOrgUnit.title()), extOuTitles.values()));
        List<ExternalOrgUnit> externalOrgUnits = builder.createStatement(getSession()).list();

        for (Long key : extOuTitles.keySet())
        {
            String extOuTitle = extOuTitles.get(key);
            for (ExternalOrgUnit externalOrgUnit : externalOrgUnits)
            {
                if (externalOrgUnit.getTitle().equals(extOuTitle))
                {
                    contractsInfoMap.get(key).setExternalOrgUnit(externalOrgUnit);
                }
            }
        }

        List<Long> createOuFakeIds = Lists.newArrayList();
        for (FefuContractImportWrapper wrapper : contractsInfoMap.values())
        {
            if (null == wrapper.getExternalOrgUnit())
            {
                createOuFakeIds.add(wrapper.getFakeId());
            }
        }

        AddressCountry russia = DataAccessServices.dao().get(AddressCountry.class, AddressCountry.code(), IKladrDefines.RUSSIA_COUNTRY_CODE);

        List<ExternalOrgUnit> createdExternalOrgUnitList = Lists.newArrayList();

        for (Long key : createOuFakeIds)
        {
            FefuContractImportWrapper wrapper = contractsInfoMap.get(key);
            for (ExternalOrgUnit extOu : createdExternalOrgUnitList)
            {
                if (extOu.getTitle().equals(wrapper.getExternalOrganizationTitle()) && extOu.getLegalForm().getShortTitle().equals(wrapper.getLegalForm().getShortTitle()))
                    wrapper.setExternalOrgUnit(extOu);
            }
            if (null == wrapper.getExternalOrgUnit())
            {
                AddressRu address = new AddressRu();
                address.setCountry(russia);
                save(address);

                ExternalOrgUnit externalOrgUnit = new ExternalOrgUnit();
                externalOrgUnit.setLegalForm(wrapper.getLegalForm());
                externalOrgUnit.setTitle(wrapper.getExternalOrganizationTitle());
                externalOrgUnit.setShortTitle(wrapper.getExternalOrganizationTitle());
                externalOrgUnit.setCreateDate(new Date());
                externalOrgUnit.setActive(true);
                externalOrgUnit.setLegalAddress(address);
                externalOrgUnit.setComment(wrapper.getExternalOrganizationAddress());
                save(externalOrgUnit);
                wrapper.setExternalOrgUnit(externalOrgUnit);
                createdExternalOrgUnitList.add(externalOrgUnit);
            }
        }
    }

    @Override
    public void export_StudentOrdersList(final Database mdb) throws IOException
    {
        new FefuStudentExtractExporter(this, this.getSession(), mdb).doExport();
    }

    @Override
    public List<ProcessedOnlineEntrantsVO> sendOnlineEntrantNotifications(List<ProcessedOnlineEntrantsVO> onlineEntrants)
    {
        List<ProcessedOnlineEntrantsVO> onlineEntrantsWithoutEmails = Lists.newArrayList();
        for (ProcessedOnlineEntrantsVO onlineEntrant : onlineEntrants)
        {
            if (onlineEntrant.getForeign())
            {
                if (StringUtils.isEmpty(onlineEntrant.getForeignOnlineEntrant().getEmail()))
                {
                    onlineEntrantsWithoutEmails.add(onlineEntrant);
                    continue;
                }

                String text = "Уважаемый Абитуриент!\n" +
                        "\n" +
                        "Ваше заявление с номером " + onlineEntrant.getForeignOnlineEntrant().getPersonalNumber() + ", поданное через " +
                        "Сервис приема заявлений абитуриентов ИТС «Электронный университет» ДВФУ, прошло проверку.\n" +
                        "\n" +
                        "Вам присвоен номер абитуриента " + onlineEntrant.getForeignOnlineEntrant().getEntrant().getPersonalNumber() + ".\n" +
                        "Дальнейшую информацию о ходе приема и необходимых для поступления действиях Вы можете получить в Вашем Личном кабинете на Интернет-портале кампуса ДВФУ, на странице Приемной комиссии на официальном сайте ДВФУ по адресу «http://www.dvfu.ru/web/admissions», а так же обратившись непосредственно в Приемную комиссию ДВФУ по телефону 8 (800) 555−0−888 (звонок по России бесплатный) или по электронной почте priem@dvfu.ru.\n" +
                        "\n" +
                        "Благодарим Вас за использование Сервиса приема заявлений абитуриентов ИТС «Электронный университет» ДВФУ.\n" +
                        "\n" +
                        "По всем вопросам, связанным с работой этого Сервиса, Вы можете обратиться в Приемную комиссию ДВФУ по телефону 8 (800) 555−0−888 (звонок по России бесплатный) или по электронной почте priem@dvfu.ru.\n" +
                        "\n" +
                        "Пожалуйста, не отвечайте на данное письмо.\n" +
                        "\n" +
                        "Приемная комиссия ДВФУ.\n" +
                        "\n" +
                        "\n" +

                        "Dear Applicant,\n" +
                        "\n" +
                        "You have been given the applicant number " + onlineEntrant.getForeignOnlineEntrant().getEntrant().getPersonalNumber() + ".\n" +
                        "Your application with the number " + onlineEntrant.getForeignOnlineEntrant().getPersonalNumber() + ", submitted through the FEFU “Electronic University” Service for online application submission, has been checked.\n" +
                        "\n" +
                        "Further information on the application process and requirements for admission can be obtained from your personal account on the FEFU campus’s Internet portal, on the Admissions Committee page on FEFU’s official website http://www.dvfu.ru/web/admissions, as well as by contacting the FEFU Office of International Admissions by e-mail at interadmission@dvfu.ru\n" +
                        "\n" +
                        "Thank you for your use of the Service for receipt of applications applicants ITS \"Electronic University\" Palo.\n" +
                        "\n" +
                        "In all matters related to this Service, you may contact the FEFU Office of International Admissions by e-mail at preim@dvfu.ru\n" +
                        "\n" +
                        "Please do not reply to this letter.\n" +
                        "\n" +
                        "Sincerely,\n" +
                        "FEFU Office of International Admissions";
                try
                {
                    FefuMailSender.sendEmail(onlineEntrant.getForeignOnlineEntrant().getEmail(), "Уведомление о принятии заявления приемной комиссией ДВФУ / Your application was accepted by the FEFU Admissions", text);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            else
            {
                if (StringUtils.isEmpty(onlineEntrant.getOnlineEntrant().getEntrant().getPerson().getEmail()))
                {
                    onlineEntrantsWithoutEmails.add(onlineEntrant);
                    continue;
                }
                String text = "Уважаемый Абитуриент!\n" +
                        "\n" +
                        "Ваше заявление с номером " + onlineEntrant.getOnlineEntrant().getPersonalNumber() + ", поданное через Сервис приема заявлений абитуриентов ИТС «Электронный университет» ДВФУ, прошло проверку.\n" +
                        "\n" +
                        "Вам присвоен номер абитуриента " + onlineEntrant.getOnlineEntrant().getEntrant().getPersonalNumber() + ".\n" +
                        "Дальнейшую информацию о ходе приема и необходимых для поступления действиях Вы можете получить в Вашем Личном кабинете " +
                        "на Интернет-портале кампуса ДВФУ, на странице Приемной комиссии на официальном сайте ДВФУ по адресу «http://www.dvfu.ru/web/admissions», " +
                        "а так же обратившись непосредственно в Приемную комиссию ДВФУ по телефону 8 (800) 555−0−888 (звонок по России бесплатный) или по электронной почте priem@dvfu.ru.\n" +
                        "\n" +
                        "Благодарим Вас за использование Сервиса приема заявлений абитуриентов ИТС «Электронный университет» ДВФУ.\n" +
                        "\n" +
                        "По всем вопросам, связанным с работой этого Сервиса, Вы можете обратиться в Приемную комиссию ДВФУ по телефону 8 (800) 555−0−888 (звонок по России бесплатный) или по электронной почте priem@dvfu.ru.\n" +
                        "\n" +
                        "Пожалуйста, не отвечайте на данное письмо.\n" +
                        "\n" +
                        "Приемная комиссия ДВФУ.";
                try
                {
                    FefuMailSender.sendEmail(onlineEntrant.getOnlineEntrant().getEntrant().getPerson().getEmail(), "Уведомление о принятии заявления приемной комиссией ДВФУ", text);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return onlineEntrantsWithoutEmails;
    }

    @Override
    public void doSendToArchiveStudents(List<Long> studentIds)
    {
        Calendar calendar = Calendar.getInstance();

        List<Long> nonArchivableStudents = new DQLSelectBuilder().fromEntity(Student.class, "s").column(property(Student.id().fromAlias("s"))).distinct()
                .joinEntity("s", DQLJoinType.left, AbstractStudentExtract.class, "e", eq(property(Student.id().fromAlias("s")), property(AbstractStudentExtract.entity().id().fromAlias("e"))))
                .joinEntity("e", DQLJoinType.left, AbstractStudentParagraph.class, "p", eq(property(AbstractStudentExtract.paragraph().id().fromAlias("e")), property(AbstractStudentParagraph.id().fromAlias("p"))))
                .joinEntity("p", DQLJoinType.left, AbstractStudentOrder.class, "o", eq(property(AbstractStudentParagraph.order().id().fromAlias("p")), property(AbstractStudentOrder.id().fromAlias("o"))))
                .joinEntity("o", DQLJoinType.left, OrderStates.class, "st", eq(property(AbstractStudentOrder.state().id().fromAlias("o")), property(OrderStates.id().fromAlias("st"))))
                .where(in(property(Student.id().fromAlias("s")), studentIds))
                .where(or(
                        eq(property(Student.status().active().fromAlias("s")), value(Boolean.TRUE)),
                        and(
                                isNotNull(property(AbstractStudentExtract.id().fromAlias("e"))),
                                or(
                                        isNull(property(AbstractStudentExtract.paragraph().fromAlias("e"))),
                                        ne(property(OrderStates.code().fromAlias("st")), value(OrderStatesCodes.FINISHED))
                                )
                        )
                )).createStatement(getSession()).list();

        if (nonArchivableStudents.isEmpty())
        {
            new DQLUpdateBuilder(Student.class)
                    .set(Student.finishYear().s(), value(calendar.get(Calendar.YEAR)))
                    .set(Student.archivingDate().s(), valueDate(CoreDateUtils.getDayFirstTimeMoment(calendar.getTime())))
                    .set(Student.archival().s(), value(Boolean.TRUE))
                    .where(in(property(Student.id()), studentIds))
                    .createStatement(getSession()).execute();
        }
        else
        {
            Student student = get(Student.class, nonArchivableStudents.get(0));
            throw new ApplicationException("У студента " + student.getFio() + " активное состояние или остались непроведенные приказы.");
        }

    }

    @Override
    public void doArchivePracticeContract()
    {
        DQLSelectBuilder selectBuilder = new DQLSelectBuilder().fromEntity(FefuPracticeContractWithExtOu.class, "c");
        selectBuilder.column(property("c", FefuPracticeContractWithExtOu.id()));
        selectBuilder.where(isNotNull(property("c", FefuPracticeContractWithExtOu.endDate())));
        selectBuilder.where(lt(property("c", FefuPracticeContractWithExtOu.endDate()), valueTimestamp(new DateTime().minusDays(1).toDate())));
        selectBuilder.where(eq(property("c", FefuPracticeContractWithExtOu.archival()), value(false)));

        new DQLUpdateBuilder(FefuPracticeContractWithExtOu.class)
                .set(FefuPracticeContractWithExtOu.archival().s(), value(Boolean.TRUE))
                .where(in(property(FefuPracticeContractWithExtOu.id()), selectBuilder.buildQuery()))
                .createStatement(getSession()).execute();
    }

    @Override
    public List<FefuImtsaXml> getXmlList(Collection<Long> blockIds)
    {
        List<FefuImtsaXml> xmlList = blockIds == null ? getList(FefuImtsaXml.class) : new DQLSelectBuilder().fromEntity(FefuImtsaXml.class, "x").where(in(property("x", FefuImtsaXml.block().id()), blockIds)).createStatement(getSession()).<FefuImtsaXml>list();
        Collections.sort(xmlList, (o1, o2) -> {
            EppEduPlanVersionBlock block1 = o1.getBlock();
            EppEduPlanVersionBlock block2 = o2.getBlock();

            Long versionId1 = block1.getEduPlanVersion().getId();
            Long versionId2 = block2.getEduPlanVersion().getId();
            if (versionId1.equals(versionId2))
            {
                if (block1.isRootBlock() || block2.isRootBlock())
                {
                    return block1.isRootBlock() ? -1 : 1;
                }

                return block1.getId().compareTo(block2.getId());
            }

            return versionId1.compareTo(versionId2);
        });

        return xmlList;
    }

    @Override
    public void saveOrRewriteImportLog(EppEduPlanVersionBlock block, String error)
    {
        FefuImtsaImportLog log = get(FefuImtsaImportLog.class, FefuImtsaImportLog.block(), block);
        if (log != null)
        {
            delete(log);
            getSession().flush();
            getSession().clear();
        }

        log = new FefuImtsaImportLog(block, error);
        save(log);
    }

    @Override
    public List<FefuImtsaCyclePlanStructureRel> getImtsaCycles(boolean secondGosGeneration)
    {
        return getList(FefuImtsaCyclePlanStructureRel.class, FefuImtsaCyclePlanStructureRel.gos2(), secondGosGeneration);
    }

    @Override
    public List<EppPlanStructure> getParts(boolean secondGosGeneration)
    {
        return getList(EppPlanStructure.class, EppPlanStructure.parent().code(), secondGosGeneration ? EppPlanStructureCodes.GOS_COMPONENTS : EppPlanStructureCodes.FGOS_PARTS);
    }

    @Override
    public void doDeleteAllFefuScheduleICalData()
    {
        DQLDeleteBuilder builder = new DQLDeleteBuilder(SppScheduleICal.class);
        builder.createStatement(getSession()).execute();
    }

    public Set<Long> getOtherOrdersDuplicates()
    {
        List<StudentOtherOrder> otherOrdersList = new DQLSelectBuilder()
                .fromEntity(StudentOtherOrder.class, "e").column("e")
                .createStatement(getSession()).list();

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(StudentOtherOrder.class, "s")
                .column(property(StudentOtherOrder.id().fromAlias("s")));

        List<Object[]> ordersList = new DQLSelectBuilder().fromEntity(AbstractStudentOrder.class, "e")
                .where(notIn(property(AbstractStudentOrder.id().fromAlias("e")), subBuilder.buildQuery()))
                .column(property(AbstractStudentOrder.id().fromAlias("e")))
                .column(property(AbstractStudentOrder.number().fromAlias("e")))
                .column(property(AbstractStudentOrder.commitDate().fromAlias("e")))
                .createStatement(getSession()).list();

        Map<CoreCollectionUtils.Pair<String, Date>, Long> ordersMap = new HashMap<>();
        for (Object[] order : ordersList)
        {
            if (null != order[1] && null != order[2])
                ordersMap.put(new CoreCollectionUtils.Pair<>((String) order[1], (Date) order[2]), (Long) order[0]);
        }

        Set<Long> ordersToBeDeleted = new HashSet<>();
        for (StudentOtherOrder order : otherOrdersList)
        {
            if (null != order.getNumber() && null != order.getCommitDate())
            {
                if (null != ordersMap.get(new CoreCollectionUtils.Pair<>(order.getNumber(), order.getCommitDate())))
                    ordersToBeDeleted.add(order.getId());
            }
        }

        List<StudentOtherOrder> errorOrders = new DQLSelectBuilder().fromEntity(OtherStudentExtract.class, "e")
                .joinEntity("e", DQLJoinType.inner, AbstractStudentParagraph.class, "p", eq(property(OtherStudentExtract.paragraph().id().fromAlias("e")), property(AbstractStudentParagraph.id().fromAlias("p"))))
                .joinEntity("p", DQLJoinType.inner, StudentOtherOrder.class, "o", eq(property(AbstractStudentParagraph.order().id().fromAlias("p")), property(StudentOtherOrder.id().fromAlias("o"))))
                .joinEntity("e", DQLJoinType.inner, StudentFefuExt.class, "ext", eq(property(OtherStudentExtract.entity().id().fromAlias("e")), property(StudentFefuExt.student().id().fromAlias("ext"))))
                .where(isNull(property(StudentFefuExt.integrationId().fromAlias("ext"))))
                .column("o").createStatement(getSession()).list();

        Set<CoreCollectionUtils.Pair<String, String>> orderNumDatesToDelete = new HashSet<>();
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("02-02-04/1-2798", "02.12.2011"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("1244/с", "01.11.2010"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("1373/c", "10.12.2009"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("1690/1", "16.09.2008"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("2042/1", "07.09.2006"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("2062/1", "22.10.2008"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("2164/1", "05.11.2008"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("2612/1", "23.11.2007"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("2640/1", "08.11.2006"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("2705/1", "14.11.2006"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("41-з", "30.01.2008"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("43/с", "24.01.2011"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("Сф-1292", "29.05.2011"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("Сф-1850", "18.08.2010"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("Сф-1851", "18.08.2010"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("Сф-1852", "18.08.2010"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("Сф-1904", "04.08.2009"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("Сф-2529", "13.10.2010"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("Сф-2930", "10.10.2008"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("Сф-504", "26.02.2009"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("УВ-1107", "30.08.2010"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("УВ-1429", "05.12.2005"));
        orderNumDatesToDelete.add(new CoreCollectionUtils.Pair<>("УВ-1475", "10.11.2010"));

        for (StudentOtherOrder order : errorOrders)
        {
            if (null != order.getNumber() && null != order.getCommitDate())
                if (orderNumDatesToDelete.contains(new CoreCollectionUtils.Pair<>(order.getNumber(), DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()))))
                    ordersToBeDeleted.add(order.getId());
        }
        return ordersToBeDeleted;
    }

    public void doDeleteOtherOrdersDuplicates(List<Long> otherOrderIdsToDelete)
    {
        List<StudentOtherOrder> otherOrdersList = new DQLSelectBuilder()
                .fromEntity(StudentOtherOrder.class, "e").column("e")
                .where(in(property(StudentOtherOrder.id().fromAlias("e")), otherOrderIdsToDelete))
                .createStatement(getSession()).list();

        for (StudentOtherOrder order : otherOrdersList)
        {
            System.out.println("Order " + order.getNumber() + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()) + " was deleted");
            MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
        }
        getSession().flush();
    }

    public void doResetPersonSyncDateForFailedStudents()
    {
        Date date = new Date();
        date = CoreDateUtils.add(date, Calendar.DAY_OF_MONTH, -5);

        List<String> logRows = new DQLSelectBuilder().fromEntity(FefuNsiLogRow.class, "e")
                .column(property(FefuNsiLogRow.messageBody().fromAlias("e")))
                .where(eq(property(FefuNsiLogRow.eventType().fromAlias("e")), value("out")))
                .where(ge(property(FefuNsiLogRow.messageTime().fromAlias("e")), valueTimestamp(date)))
                .where(ne(property(FefuNsiLogRow.messageStatus().fromAlias("e")), value(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_ERROR.intValue())))
                .createStatement(getSession()).list();

        Set<String> successGuidsSet = new HashSet<>();
        System.out.println("############# There are " + logRows.size() + " log rows to parse.");

        int i = 0;
        int j = 0;
        for (String logRowStr : logRows)
        {
            String datagramStr = logRowStr.replaceAll("<x-datagram:x-datagram xmlns=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0\" xmlns:p=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Service\" xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:x-datagram=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">", "");
            datagramStr = datagramStr.replaceAll("<x-datagram xmlns=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Service\">", "<x-datagram xmlns=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0\">");
            datagramStr = datagramStr.replaceAll("</x-datagram:x-datagram>", "");

            XDatagram reqDatagram = NsiReactorUtils.fromXml(XDatagram.class, datagramStr.getBytes());
            for (Object item : reqDatagram.getEntityList())
            {
                if (item instanceof HumanType) successGuidsSet.add(((HumanType) item).getID());
            }

            i++;
            if (++j == 10)
            {
                System.out.println("############# There are " + i + " of " + logRows.size() + " log rows were processed.");
                j = 0;
            }
        }

        List<FefuNsiIds> personIdsToCheck = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "ids").column("ids")
                //.column(property(FefuNsiIds.guid().fromAlias("ids"))).column(property(FefuNsiIds.entityId().fromAlias("ids")))
                .where(ge(property(FefuNsiIds.syncTime().fromAlias("ids")), valueTimestamp(date)))
                .where(eq(property(FefuNsiIds.entityType().fromAlias("ids")), value(Person.ENTITY_NAME)))
                .createStatement(getSession()).list();

        Set<Long> nsiIdsToResetSyncTimeSet = new HashSet<>();
        System.out.println("############# There are " + personIdsToCheck.size() + " id rows to parse.");

        i = j = 0;
        for (FefuNsiIds nsiIds : personIdsToCheck)
        {
            if (!successGuidsSet.contains(nsiIds.getGuid())) nsiIdsToResetSyncTimeSet.add(nsiIds.getId());
            i++;
            if (++j == 10)
            {
                System.out.println("############# There are " + i + " of " + logRows.size() + " id rows were processed.");
                j = 0;
            }
        }

        new DQLUpdateBuilder(FefuNsiIds.class)
                .set(FefuNsiIds.P_SYNC_TIME, DQLExpressions.nul(PropertyType.DATE))
                .where(in(property(FefuNsiIds.id()), nsiIdsToResetSyncTimeSet))
                .createStatement(getSession()).execute();
    }

    public void doMergePersonsAndChangeGuidsByCssFile(String fileContent)
    {                                                                         /*
        Set<String> guidSet = new HashSet<>();
        List<FefuNsiIds> tempList = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "ids").column("ids").createStatement(getSession()).list();
        for(FefuNsiIds ids : tempList)
        {
            if(guidSet.contains(ids.getGuid().toLowerCase())) throw new ApplicationException("wtf!!!" + ids.getGuid());
            guidSet.add(ids.getGuid().toLowerCase());
        }
        if(true) throw new ApplicationException("!!!!!!!!!!!");             */

        FefuNsiSyncDAO.logEvent(Level.INFO, "********** MERGE DUPLICATES AND CHANGE GUIDS BY NSI CSS FILE HAVE STARTED **********");

        String[] lines = fileContent.split("\r\n");

        FefuNsiSyncDAO.logEvent(Level.INFO, "********** There are " + (lines.length - 1) + " persons with guids were recieved from file **********");

        List<Object[]> icardList = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "ids")
                .column(property(FefuNsiIds.guid().fromAlias("ids"))).column(property(FefuNsiIds.entityId().fromAlias("ids")))
                .column(property(IdentityCard.lastName().fromAlias("ic"))).column(property(IdentityCard.firstName().fromAlias("ic")))
                .column(property(IdentityCard.middleName().fromAlias("ic"))).column(property(IdentityCard.birthDate().fromAlias("ic")))
                .joinEntity("ids", DQLJoinType.inner, Person.class, "p", eq(property(FefuNsiIds.entityId().fromAlias("ids")), property(Person.id().fromAlias("p"))))
                .joinEntity("p", DQLJoinType.inner, IdentityCard.class, "ic", eq(property(Person.identityCard().id().fromAlias("p")), property(IdentityCard.id().fromAlias("ic"))))
                .createStatement(getSession()).list();

        Map<String, NsiPersonMergeWrapper> personWrappersMap = new HashMap<>();

        for (Object[] item : icardList)
        {
            String guid = ((String) item[0]).toLowerCase();
            if (null != personWrappersMap.get(guid)) System.out.println("!!!!Achtung");
            personWrappersMap.put(guid, new NsiPersonMergeWrapper(item));
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "********** There are " + icardList.size() + " persons with guids were recieved from database **********");


        List<Object[]> personsList = new DQLSelectBuilder().fromEntity(Person.class, "e").column("e").column("ids")
                .joinEntity("e", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(Person.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .createStatement(getSession()).list();

        Map<String, CoreCollectionUtils.Pair<Person, FefuNsiIds>> personMap = new HashMap<>();

        for (Object[] item : personsList)
        {
            String guid = ((FefuNsiIds) item[1]).getGuid().toLowerCase();
            personMap.put(guid, new CoreCollectionUtils.Pair(item[0], item[1]));
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "********** Person Map was filled up **********");


        List<FefuNsiIds> orphanPersonGuids = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "ids").column("ids")
                .joinEntity("ids", DQLJoinType.left, Person.class, "p", eq(property(FefuNsiIds.entityId().fromAlias("ids")), property(Person.id().fromAlias("p"))))
                .where(eq(property(FefuNsiIds.entityType().fromAlias("ids")), value(Person.ENTITY_NAME)))
                .where(isNull(property(Person.id().fromAlias("p"))))
                .createStatement(getSession()).list();

        FefuNsiSyncDAO.logEvent(Level.INFO, "********** There are " + orphanPersonGuids.size() + " orphan person guids were found in database **********");

        for (FefuNsiIds ids : orphanPersonGuids) delete(ids);
        FefuNsiSyncDAO.logEvent(Level.INFO, "********** There are " + orphanPersonGuids.size() + " orphan person guids were deleted from database **********");
        getSession().flush();


        List<String> occupiedNonPersonGuidsList = new ArrayList<>();
        List<String> occupiedNonPersonGuidsListTemp = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e")
                .column(property(FefuNsiIds.guid().fromAlias("e")))
                .where(ne(property(FefuNsiIds.entityType().fromAlias("e")), value(Person.ENTITY_NAME)))
                .createStatement(getSession()).list();

        for (String item : occupiedNonPersonGuidsListTemp)
        {
            occupiedNonPersonGuidsList.add(item.toLowerCase());
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "********** Occupied guids list was filled up **********");


        Map<String, List<NsiPersonMergeWrapper>> mergeMap = new HashMap<>();
        Map<String, String> nameDifferentMap = new LinkedHashMap<>();
        Map<String, String> birthDateDifferentMap = new LinkedHashMap<>();
        List<String> occupiedGuidsList = new ArrayList<>();
        List<String> mergeErrorstList = new ArrayList<>();
        List<String> nonExistList = new ArrayList<>();
        //Set<String> nsiGuidsSet = new HashSet<>();

        for (int i = 1; i < lines.length; i++)
        {
            String[] cells = lines[i].split("\t");
            if (cells.length != 6)
            {
                FefuNsiSyncDAO.logEvent(Level.ERROR, "********** Incorrect columns amount at given css file **********");
                throw new ApplicationException("Количество колонок для всех строк в файле должно быть равным шести.");
            }

            NsiPersonMergeWrapper nsiWrapper = new NsiPersonMergeWrapper(cells);
            NsiPersonMergeWrapper obWrapper = personWrappersMap.get(nsiWrapper.getGuid().toLowerCase());

            if (null == obWrapper) nonExistList.add(nsiWrapper.getGuid());
            else
            {
                if (nsiWrapper.isPersonsNameAreDifferent(obWrapper))
                    nameDifferentMap.put(nsiWrapper.getGuid(), nsiWrapper.getFullFioDifferenceStr(obWrapper));
                if (nsiWrapper.isPersonsBirthDatesAreDifferent(obWrapper))
                    birthDateDifferentMap.put(nsiWrapper.getGuid(), nsiWrapper.getDatesDifferenceStr(obWrapper));

                List<NsiPersonMergeWrapper> wrappersToMerge = mergeMap.get(nsiWrapper.getNsiGuid());
                if (null == wrappersToMerge) wrappersToMerge = new ArrayList<>();
                wrappersToMerge.add(nsiWrapper);
                mergeMap.put(nsiWrapper.getNsiGuid(), wrappersToMerge);
            }

            //nsiGuidsSet.add(nsiWrapper.getNsiGuid());
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "********** All the persons were prepared for guid changing and merging **********");

        int i = 0;
        int j = 0;
        Set<String> alreadyUsedGuidsSet = new HashSet<>();

        for (Map.Entry<String, List<NsiPersonMergeWrapper>> entry : mergeMap.entrySet())
        {
            if (occupiedNonPersonGuidsList.contains(entry.getKey().toLowerCase()))
            {
                occupiedGuidsList.add(entry.getKey().toLowerCase());
                continue;
            }    /*
try{
            FefuNsiIds shitId = getByNaturalId(new FefuNsiIds.NaturalId(entry.getKey()));
            if(null == shitId) shitId = getByNaturalId(new FefuNsiIds.NaturalId(entry.getKey().toLowerCase()));
            if(null == shitId) shitId = getByNaturalId(new FefuNsiIds.NaturalId(entry.getKey().toUpperCase()));
            if(null != shitId) throw new ApplicationException("WTF?" + shitId.getGuid());
}catch (Throwable t)
{
    System.out.println();
}    */

            CoreCollectionUtils.Pair<Person, FefuNsiIds> primaPersonItem = personMap.get(entry.getKey().toLowerCase());
            if (null != primaPersonItem)
            {
                entry.getValue().add(0, new NsiPersonMergeWrapper(primaPersonItem.getX(), primaPersonItem.getY()));
            }

            if (entry.getValue().size() > 1)
            {
                //i++;

                List<Person> dubPersonList = new ArrayList<>();
                List<FefuNsiIds> dubPersonIdsList = new ArrayList<>();
                StringBuilder lineBuilder = new StringBuilder(entry.getKey());

                for (NsiPersonMergeWrapper wrapper : entry.getValue())
                {
                    lineBuilder.append(" | " + wrapper.getPrintableTitle());
                    CoreCollectionUtils.Pair<Person, FefuNsiIds> personItem = personMap.get(wrapper.getGuid().toLowerCase());
                    dubPersonList.add(personItem.getX());
                    dubPersonIdsList.add(personItem.getY());
                }

                FefuNsiSyncDAO.logEvent(Level.INFO, "*** Merge persons: " + lineBuilder.toString());

                try
                {
                    Person person = dubPersonList.get(0);
                    Set<Person> dupPersonSet = new HashSet<>(dubPersonList);
                    dupPersonSet.remove(person);

                    IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(person.getId());
                    IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(person.getIdentityCard().getId());

                    if (null != dubPersonList && dupPersonSet.size() > 0)
                    {
                        for (Person dbl : dupPersonSet)
                        {
                            IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(dbl.getId());
                            IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(dbl.getIdentityCard().getId());
                        }
                        doMergePersonDublicates(person, new ArrayList(dupPersonSet));
                    }

                    for (FefuNsiIds ids : dubPersonIdsList) delete(ids);
                    getSession().flush();

                    if (alreadyUsedGuidsSet.contains(entry.getKey().toLowerCase()))
                    {
                        FefuNsiSyncDAO.logEvent(Level.ERROR, "*** Can not change guid for merged person, because it was already used for other person.");
                        continue;
                    }
                    else
                    {
                        FefuNsiIds newNsiId = new FefuNsiIds();
                        newNsiId.setSyncTime(new Date());
                        newNsiId.setEntityId(person.getId());
                        newNsiId.setEntityType(Person.ENTITY_NAME);
                        newNsiId.setGuid(entry.getKey().toLowerCase());
                        save(newNsiId);
                        FefuNsiSyncDAO.logEvent(Level.INFO, "*** Guid was changed for merged persons to " + entry.getKey());
                        alreadyUsedGuidsSet.add(newNsiId.getGuid().toLowerCase());
                    }

                    FefuNsiSyncDAO.logEvent(Level.INFO, "*** Merge persons success.");

                }
                catch (Exception e)
                {
                    mergeErrorstList.add(lineBuilder.toString());
                    FefuNsiSyncDAO.logEvent(Level.ERROR, "*** Merge persons failed: " + e.getMessage());
                    throw e;
                }
            }
            else if (entry.getValue().size() == 1)
            {
                j++;
                CoreCollectionUtils.Pair<Person, FefuNsiIds> personItem = personMap.get(entry.getValue().get(0).getGuid().toLowerCase());
                CoreCollectionUtils.Pair<Person, FefuNsiIds> origPersonItem = personMap.get(entry.getKey().toLowerCase());
                if (null != origPersonItem) delete(origPersonItem.getY());

                if (alreadyUsedGuidsSet.contains(entry.getKey().toLowerCase()))
                    FefuNsiSyncDAO.logEvent(Level.ERROR, "*** Can not change guid, because it was already used for other person.");
                else
                {
                    delete(personItem.getY());
                    getSession().flush();

                    FefuNsiIds newNsiId = new FefuNsiIds();
                    newNsiId.setSyncTime(new Date());
                    newNsiId.setEntityId(personItem.getX().getId());
                    newNsiId.setEntityType(Person.ENTITY_NAME);
                    newNsiId.setGuid(entry.getKey().toLowerCase());

                    save(newNsiId);
                    FefuNsiSyncDAO.logEvent(Level.INFO, "*** Guid was changed from " + personItem.getY().getGuid() + " to " + entry.getKey());
                    alreadyUsedGuidsSet.add(newNsiId.getGuid().toLowerCase());
                }
            }
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "********** Merging was finished for " + i + " items, " + j + " guids were changed  **********");

        FefuNsiSyncDAO.logEvent(Level.WARN, "********** Some of the guids were not found at database (" + nonExistList.size() + " items): **********");
        for (String guid : nonExistList)
        {
            FefuNsiSyncDAO.logEvent(Level.WARN, guid);
        }

        FefuNsiSyncDAO.logEvent(Level.WARN, "********** Some of the guids were occupied by another entity at database (" + occupiedGuidsList.size() + " items): **********");
        for (String guid : occupiedGuidsList)
        {
            FefuNsiSyncDAO.logEvent(Level.WARN, guid);
        }

        FefuNsiSyncDAO.logEvent(Level.WARN, "********** Some of the names were different at OB and NSI (" + nameDifferentMap.entrySet().size() + " items): **********");
        for (Map.Entry<String, String> entry : nameDifferentMap.entrySet())
        {
            FefuNsiSyncDAO.logEvent(Level.WARN, entry.getKey() + " - " + entry.getValue());
        }

        FefuNsiSyncDAO.logEvent(Level.WARN, "********** Some of the birth dates were different at OB and NSI (" + birthDateDifferentMap.entrySet().size() + " items): **********");
        for (Map.Entry<String, String> entry : birthDateDifferentMap.entrySet())
        {
            FefuNsiSyncDAO.logEvent(Level.WARN, entry.getKey() + " - " + entry.getValue());
        }

        FefuNsiSyncDAO.logEvent(Level.WARN, "********** There were some errors in merging process (" + mergeErrorstList.size() + " items): **********");
        for (String mergeError : mergeErrorstList)
        {
            FefuNsiSyncDAO.logEvent(Level.WARN, mergeError);
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "********** MERGE DUPLICATES AND CHANGE GUIDS BY NSI CSS FILE WAS FINISHED **********");
    }

    private void doMergePersonDublicates(final Person template, final List<Person> dublicates)
    {
        final Session session = this.getSession();
        Person primaryPerson = template;

        // шаблон персоны не может быть дубликатом
        if (dublicates.contains(template))
        {
            throw new RuntimeException("Dublicates list should not contain template person!");
        }


        Set<IdentityCard> cardWithPhotoSet = new HashSet<>();
        if (null != template.getIdentityCard().getPhoto()) cardWithPhotoSet.add(template.getIdentityCard());

        Map<IdentityCard, Integer> filledFieldsCountMap = new HashMap<>();
        filledFieldsCountMap.put(template.getIdentityCard(), getFilledFieldsCount(template.getIdentityCard()));

        // по всем дубликатам
        for (final Person dub : dublicates)
        {
            filledFieldsCountMap.put(dub.getIdentityCard(), getFilledFieldsCount(dub.getIdentityCard()));
            if (null != dub.getIdentityCard().getPhoto()) cardWithPhotoSet.add(dub.getIdentityCard());
        }

        IdentityCard primaryIdentityCard = cardWithPhotoSet.size() == 1 ? cardWithPhotoSet.iterator().next() : null;

        boolean hasEqualFilledFields = false;
        Integer maxFilledFieldsCount = 0;
        IdentityCard maxFieldICard = null;
        Date lastIdentityCardDate = null;
        IdentityCard lastDateICard = null;

        for (Map.Entry<IdentityCard, Integer> entry : filledFieldsCountMap.entrySet())
        {
            if ((cardWithPhotoSet.size() > 1 && cardWithPhotoSet.contains(entry.getKey())) || cardWithPhotoSet.isEmpty())
            {
                if (entry.getValue() == maxFilledFieldsCount) hasEqualFilledFields = true;

                if (entry.getValue() > maxFilledFieldsCount)
                {
                    maxFilledFieldsCount = entry.getValue();
                    maxFieldICard = entry.getKey();
                }

                if (null == lastIdentityCardDate && null != entry.getKey().getIssuanceDate())
                {
                    lastIdentityCardDate = entry.getKey().getIssuanceDate();
                    lastDateICard = entry.getKey();
                }
                else if (null != lastIdentityCardDate && null != entry.getKey().getIssuanceDate())
                {
                    lastIdentityCardDate = entry.getKey().getIssuanceDate();
                    lastDateICard = entry.getKey();
                }
            }
        }

        if (null == primaryIdentityCard && !hasEqualFilledFields && null != maxFieldICard)
            primaryIdentityCard = maxFieldICard;
        if (null == primaryIdentityCard && null != lastDateICard) primaryIdentityCard = lastDateICard;

        if (null != primaryIdentityCard && !template.equals(primaryIdentityCard.getPerson()))
        {
            primaryPerson = primaryIdentityCard.getPerson();
            dublicates.add(template);
            dublicates.remove(primaryPerson);
        }

        // у каждой персоны должен быть принципал (логин+пароль)
        final Person2PrincipalRelation relation = this.get(Person2PrincipalRelation.class, Person2PrincipalRelation.L_PERSON, primaryPerson);
        final Principal principal = relation.getPrincipal();

        final List<PersonRole> processedRoles = new ArrayList<>();

        if (ApplicationRuntime.containsBean(IPersonMergeListener.LIST_BEAN_NAME))
        {
            @SuppressWarnings("unchecked")
            final
            List<IPersonMergeListener> listeners = (List<IPersonMergeListener>) ApplicationRuntime.getBean(IPersonMergeListener.LIST_BEAN_NAME);
            for (final IPersonMergeListener listener : listeners)
            {
                processedRoles.addAll(listener.doMerge(primaryPerson, dublicates));
            }
        }

        // по всем дубликатам
        for (final Person dub : dublicates)
        {
            // по всем категориями персоны
            for (final PersonRole role : this.getList(PersonRole.class, PersonRole.L_PERSON, dub))
            {
                // роль уже была обработала в IPersonMergeListener
                if (processedRoles.contains(role))
                {
                    continue;
                }

                // роль перелинковываем на шаблонную персону
                role.setPerson(primaryPerson);
                role.setPrincipal(principal);
                session.update(role);
            }
        }


        List<Person> fullPersonList = new ArrayList<>();
        fullPersonList.addAll(dublicates);
        fullPersonList.add(primaryPerson);

        // по всем дубликатам
        for (final Person dub : fullPersonList)
        {
            // по всем категориями персоны
            for (final IdentityCard iCard : this.getList(IdentityCard.class, IdentityCard.L_PERSON, dub))
            {
                // роль перелинковываем на шаблонную персону
                iCard.setPerson(primaryPerson);
                session.update(iCard);
            }
        }

        session.flush();

        // сейчас надо удалить дубликаты персон (и вместе с ними принципалов)
        // вызов API функции корректен, так как у этой персоны больше нет ролей
        for (final Person dub : dublicates)
        {
            PersonManager.instance().dao().deletePerson(dub.getId());
        }
    }

    private int getFilledFieldsCount(IdentityCard iCard)
    {
        int filledFieldsCount = 0;
        if (null != iCard.getBirthDate()) filledFieldsCount++;
        if (null != iCard.getBirthPlace()) filledFieldsCount++;
        if (null != iCard.getIssuanceDate()) filledFieldsCount++;
        if (null != iCard.getSeria()) filledFieldsCount++;
        if (null != iCard.getNumber()) filledFieldsCount++;
        if (null != iCard.getIssuancePlace()) filledFieldsCount++;
        if (null != iCard.getIssuanceCode()) filledFieldsCount++;
        if (null != iCard.getAddress()) filledFieldsCount++;
        if (null != iCard.getCitizenship()) filledFieldsCount++;
        if (null != iCard.getNationality()) filledFieldsCount++;
        if (null != iCard.getRegistrationPeriod()) filledFieldsCount++;
        if (null != iCard.getRegistrationPeriod()) filledFieldsCount++;
        if (null != iCard.getRegistrationPeriodFrom()) filledFieldsCount++;
        if (null != iCard.getRegistrationPeriodTo()) filledFieldsCount++;
        return filledFieldsCount;
    }

    public void doRegenBadIdentityCardIdsByFile(String fileContent)
    {
        int startIdx = fileContent.indexOf("********** Some of the guids were occupied by another entity");
        int endIdx = fileContent.indexOf("********** Some of the names were different");

        if (endIdx < startIdx || endIdx < 0)
        {
            throw new ApplicationException("Не нейдены GUID'ы, занятые другими объектами.");
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "********** REGEN GUIDS AND SEND TO NSI HAVE STARTED **********");

        String[] lines = fileContent.substring(startIdx, endIdx).split("\r\n");

        List<String> guidsList = new ArrayList<>();
        for (String line : lines)
        {
            int guidStartIdx = line.indexOf("WARN - ");
            if (guidStartIdx > 0 && line.length() > 10)
            {
                String guid = line.substring(guidStartIdx + 7);
                if (guid.length() > 10) guidsList.add(guid);
            }
        }

        List<FefuNsiIds> occupatedGuidsList = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e").createStatement(getSession()).list();

        //Set<String> occupatedGuidsSet = new HashSet<>();
        Map<String, FefuNsiIds> guidToFefuNsiIdMap = new HashMap<>();
        for (FefuNsiIds nsiIds : occupatedGuidsList)
        {
            //occupatedGuidsSet.add(nsiIds.getGuid());
            guidToFefuNsiIdMap.put(nsiIds.getGuid(), nsiIds);
        }

        int processed = 0;
        for (String guid : guidsList)
        {
            FefuNsiIds nsiIds = guidToFefuNsiIdMap.get(guid);
            if (null != nsiIds)
            {
                String newGuid = UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString();
                while (occupatedGuidsList.contains(newGuid))
                    newGuid = UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString();

                IEntityMeta meta = EntityRuntime.getMeta(nsiIds.getEntityId());
                FefuNsiIds newNsiIds = new FefuNsiIds();
                newNsiIds.update(nsiIds);
                newNsiIds.setGuid(newGuid);

                delete(nsiIds);
                getSession().flush();
                save(newNsiIds);

                IFefuNsiDaemonDao.instance.get().doRegisterEntityEdit(newNsiIds.getEntityId());
                FefuNsiSyncDAO.logEvent(Level.WARN, "*** Regen was executed for " + guid + " -> " + newGuid + " - " + nsiIds.getEntityId() + " - " + (null != meta ? meta.getName() : ""));
                //occupatedGuidsSet.add(newGuid);
                processed++;
            }
            else
            {
                FefuNsiSyncDAO.logEvent(Level.WARN, "*** Regen was ignored for " + guid + " due to its absence in database.");
            }
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "********** REGEN GUIDS AND SEND TO NSI WAS FINISHED FOR " + processed + " of " + guidsList.size() + " ITEMS **********");
    }

    @Override
    public int doRegenerateExtractPrintForms(Collection<Long> idList)
    {
        int counter = 0;
        for (Long id : idList)
        {
            IEntity entity = this.get(id);
            if (entity instanceof ListStudentExtract)
            {
                MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText((ListStudentExtract) entity, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);
            }
            else if (entity instanceof ModularStudentExtract)
            {
                MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText((ModularStudentExtract) entity, MoveStudentDefines.EXTRACT_TEXT_CODE);
            }
            else
            {
                continue;
            }

            counter++;
        }
        return counter;
    }

    @Override
    public void regenerateExtractPrintForms(Collection<String> extractTypes, Date formingDateFrom, Date formingDateTo)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .column("e.id")
                .where(in(property("e", AbstractStudentExtract.type().code()), extractTypes))
                .where(in(property("e", AbstractStudentExtract.paragraph().order().state().code()), UnimoveDefines.CATALOG_ORDER_STATE_FINISHED, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED));

        if (formingDateFrom != null)
            dql.where(ge(property("e", IAbstractExtract.P_CREATE_DATE), valueTimestamp(CoreDateUtils.getDayFirstTimeMoment(formingDateFrom))));

        if (formingDateTo != null)
            dql.where(lt(property("e", IAbstractExtract.P_CREATE_DATE), valueTimestamp(CoreDateUtils.getNextDayFirstTimeMoment(formingDateTo, 1))));

        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                final List<Long> idList = createStatement(dql).list();
                final MutableInt counter = new MutableInt(0);

                state.setMaxValue(idList.size());
                state.setMessage("Перегенерировано 0% выписок");

                long startTime = System.currentTimeMillis();
                // Разбиваем на транзакции - у doRegenerateExtractPrintForms стоит Propagation.REQUIRES_NEW
                BatchUtils.execute(idList, 50, new BatchUtils.Action<Long>()
                {
                    @Override
                    public void execute(final Collection<Long> ids)
                    {
                        // doRegenerateExtractPrintForms - вызываться должен каждый раз в новой траназакции!
                        counter.add(FefuSystemActionManager.instance().dao().doRegenerateExtractPrintForms(ids));
                        state.setMessage("Перегенерировано " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(counter.intValue() * 100d / idList.size()) + "% выписок");
                        state.setCurrentValue(counter.intValue());
                        getSession().clear();
                    }
                });

                return new ProcessResult("Пересохранено " + counter.intValue() + " печатных форм выписок за " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(((double) (System.currentTimeMillis() - startTime)) / 60000d) + " минут.");
            }
        };

        new BackgroundProcessHolder().start("Перегенерация печатных форм выписок", process, ProcessDisplayMode.percent);
    }


    @Override
    public void setCustomStateForStuTargetEnrollment(EnrollmentCampaign enrollmentCampaign, Date startDate, StudentCustomStateCI newStatus)
    {
        //Получаем данные по выпискам
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "extract")
                .joinPath(DQLJoinType.inner, EnrollmentExtract.entity().requestedEnrollmentDirection().fromAlias("extract"), "red")
                .joinPath(DQLJoinType.inner, EnrollmentExtract.studentNew().fromAlias("extract"), "stud")
                .joinPath(DQLJoinType.inner, EnrollmentExtract.paragraph().order().fromAlias("extract"), "o")
                .joinPath(DQLJoinType.left, RequestedEnrollmentDirection.externalOrgUnit().fromAlias("red"), "ext")

                .column("stud")
                .column(property("ext", ExternalOrgUnit.title()))

                .where(eq(property(EnrollmentExtract.entity().targetAdmission().fromAlias("extract")), value(true)))
                .where(eq(property(Student.targetAdmission().fromAlias("stud")), value(true)))
                .where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().enrollmentCampaign().fromAlias("red")), value(enrollmentCampaign)))
                .where(eq(property(EnrollmentOrder.state().code().fromAlias("o")), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
                .where(eq(property(RequestedEnrollmentDirection.state().code().fromAlias("red")), value(UniecDefines.ENTRANT_STATE_ENROLED_CODE)))
                .where(eq(property(Student.entranceYear().fromAlias("stud")), value(enrollmentCampaign.getEducationYear().getIntValue())))
                ;

        for (Object[] item : builder.createStatement(getSession()).<Object[]>list())
        {
            StudentCustomState state = new StudentCustomState();
            state.setCustomState(newStatus);
            state.setBeginDate(startDate);
            state.setDescription((String) item[1]);
            state.setStudent((Student) item[0]);
            if (!UniStudentManger.instance().studentCustomStateDAO().hasMatchStudentCustomState(state))
                saveOrUpdate(state);
        }
    }

    @Override
    public void doSendNotAcceptRegElementToArchive()
    {
        EppState stateArchive = getByCode(EppState.class, EppState.STATE_ARCHIVED);

        new DQLUpdateBuilder(EppRegistryDiscipline.class)
                .set(EppRegistryDiscipline.L_STATE, value(stateArchive))
                .where(eq(property(EppRegistryDiscipline.state().code()), value(EppState.STATE_FORMATIVE)))
                .createStatement(getSession()).execute();

        new DQLUpdateBuilder(EppRegistryPractice.class)
                .set(EppRegistryPractice.L_STATE, value(stateArchive))
                .where(eq(property(EppRegistryPractice.state().code()), value(EppState.STATE_FORMATIVE)))
                .createStatement(getSession()).execute();

        new DQLUpdateBuilder(EppRegistryAttestation.class)
                .set(EppRegistryAttestation.L_STATE, value(stateArchive))
                .where(eq(property(EppRegistryAttestation.state().code()), value(EppState.STATE_FORMATIVE)))
                .createStatement(getSession()).execute();
    }

    /**
     * Удаление выбранных приказов
     *
     * @param ids - список идентификаторов приказов
     */
    @Override
    public void deleteNotCoordinatedOrders(List<Long> ids)
    {
        for (List<Long> idsPart : Lists.partition(ids, 200))
        {
            deleteSelectedOrders(idsPart);

        }


    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    private void deleteSelectedOrders(List<Long> idsPart)
    {
        // Удаляем выписки, привязанные к выбранным приказам
        new DQLDeleteBuilder(AbstractStudentExtract.class)
                .where(in(property(AbstractStudentExtract.paragraph().order().id()), idsPart))
                .createStatement(getSession()).execute();

        //удаляем параграфы приказов
        new DQLDeleteBuilder(AbstractStudentParagraph.class)
                .where(in(property(AbstractStudentParagraph.order().id()), idsPart))
                .createStatement(getSession()).execute();

        //удаляем сами приказы
        new DQLDeleteBuilder(AbstractStudentOrder.class)
                .where(in(property(AbstractStudentExtract.id()), idsPart))
                .createStatement(getSession()).execute();
    }

    @Override
    public void doGenerateGuids()
    {
        List<Class<? extends IEntity>> classList = new ArrayList<>();
        classList.add(StudentCategory.class);
        classList.add(StudentStatus.class);
        classList.add(CompensationType.class);
        classList.add(Course.class);
        classList.add(EducationOrgUnit.class);

        List<IEntity> entityList = new ArrayList<>();
        for(Class<? extends IEntity> iClazz : classList)
            entityList.addAll(new DQLSelectBuilder().fromEntity(iClazz, "e").column(property("e"))
                    .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "ids", eq(property("e", IEntity.P_ID), property(FefuNsiIds.entityId().fromAlias("ids"))))
                    .where(isNull(property("ids"))).createStatement(getSession()).list());

        for(IEntity entity : entityList)
        {
            FefuNsiIds ids = new FefuNsiIds();
            ids.setSyncTime(new Date());
            ids.setEntityId(entity.getId());
            ids.setGuid(NsiReactorUtils.generateGUID(entity.getId()));
            ids.setEntityType(EntityRuntime.getMeta(entity.getId()).getName());
            System.out.println(ids.getEntityId() + " (" + ids.getGuid() + ") - " + ids.getEntityType());
            save(ids);
        }
    }

}
