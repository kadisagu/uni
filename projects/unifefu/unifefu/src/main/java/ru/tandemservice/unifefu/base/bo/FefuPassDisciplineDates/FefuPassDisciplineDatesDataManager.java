/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPassDisciplineDates;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuPassDisciplineDates.logic.FefuPassDisciplineDatesDAO;
import ru.tandemservice.unifefu.base.bo.FefuPassDisciplineDates.logic.IFefuPassDisciplineDatesDAO;

/**
 * @author Nikolay Fedorovskih
 * @since 14.06.2013
 */
@Configuration
public class FefuPassDisciplineDatesDataManager extends BusinessObjectManager
{
    public static FefuPassDisciplineDatesDataManager instance()
    {
        return instance(FefuPassDisciplineDatesDataManager.class);
    }

    @Bean
    public IFefuPassDisciplineDatesDAO dao()
    {
        return new FefuPassDisciplineDatesDAO();
    }
}