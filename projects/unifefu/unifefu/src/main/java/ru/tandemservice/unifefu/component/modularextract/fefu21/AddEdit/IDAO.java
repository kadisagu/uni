/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu21.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuExcludeStuDPOExtract, Model>
{
}