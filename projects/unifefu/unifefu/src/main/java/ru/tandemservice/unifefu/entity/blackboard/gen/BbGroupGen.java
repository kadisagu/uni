package ru.tandemservice.unifefu.entity.blackboard.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse2TrJournalRel;
import ru.tandemservice.unifefu.entity.blackboard.BbGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Группа студентов ЭУК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class BbGroupGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.blackboard.BbGroup";
    public static final String ENTITY_NAME = "bbGroup";
    public static final int VERSION_HASH = 710348220;
    private static IEntityMeta ENTITY_META;

    public static final String L_BB_COURSE2_TR_JOURNAL_REL = "bbCourse2TrJournalRel";
    public static final String L_GROUP = "group";
    public static final String P_TITLE = "title";
    public static final String P_BB_PRIMARY_ID = "bbPrimaryId";

    private BbCourse2TrJournalRel _bbCourse2TrJournalRel;     // Связь ЭУК с реализацией дисциплины
    private Group _group;     // Группа
    private String _title;     // Название
    private String _bbPrimaryId;     // Идентификатор группы в Blackboard

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Связь ЭУК с реализацией дисциплины. Свойство не может быть null.
     */
    @NotNull
    public BbCourse2TrJournalRel getBbCourse2TrJournalRel()
    {
        return _bbCourse2TrJournalRel;
    }

    /**
     * @param bbCourse2TrJournalRel Связь ЭУК с реализацией дисциплины. Свойство не может быть null.
     */
    public void setBbCourse2TrJournalRel(BbCourse2TrJournalRel bbCourse2TrJournalRel)
    {
        dirty(_bbCourse2TrJournalRel, bbCourse2TrJournalRel);
        _bbCourse2TrJournalRel = bbCourse2TrJournalRel;
    }

    /**
     * @return Группа.
     */
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Идентификатор группы в Blackboard. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=16)
    public String getBbPrimaryId()
    {
        return _bbPrimaryId;
    }

    /**
     * @param bbPrimaryId Идентификатор группы в Blackboard. Свойство не может быть null и должно быть уникальным.
     */
    public void setBbPrimaryId(String bbPrimaryId)
    {
        dirty(_bbPrimaryId, bbPrimaryId);
        _bbPrimaryId = bbPrimaryId;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof BbGroupGen)
        {
            setBbCourse2TrJournalRel(((BbGroup)another).getBbCourse2TrJournalRel());
            setGroup(((BbGroup)another).getGroup());
            setTitle(((BbGroup)another).getTitle());
            setBbPrimaryId(((BbGroup)another).getBbPrimaryId());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends BbGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) BbGroup.class;
        }

        public T newInstance()
        {
            return (T) new BbGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "bbCourse2TrJournalRel":
                    return obj.getBbCourse2TrJournalRel();
                case "group":
                    return obj.getGroup();
                case "title":
                    return obj.getTitle();
                case "bbPrimaryId":
                    return obj.getBbPrimaryId();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "bbCourse2TrJournalRel":
                    obj.setBbCourse2TrJournalRel((BbCourse2TrJournalRel) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "bbPrimaryId":
                    obj.setBbPrimaryId((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "bbCourse2TrJournalRel":
                        return true;
                case "group":
                        return true;
                case "title":
                        return true;
                case "bbPrimaryId":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "bbCourse2TrJournalRel":
                    return true;
                case "group":
                    return true;
                case "title":
                    return true;
                case "bbPrimaryId":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "bbCourse2TrJournalRel":
                    return BbCourse2TrJournalRel.class;
                case "group":
                    return Group.class;
                case "title":
                    return String.class;
                case "bbPrimaryId":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<BbGroup> _dslPath = new Path<BbGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "BbGroup");
    }
            

    /**
     * @return Связь ЭУК с реализацией дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroup#getBbCourse2TrJournalRel()
     */
    public static BbCourse2TrJournalRel.Path<BbCourse2TrJournalRel> bbCourse2TrJournalRel()
    {
        return _dslPath.bbCourse2TrJournalRel();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroup#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Идентификатор группы в Blackboard. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroup#getBbPrimaryId()
     */
    public static PropertyPath<String> bbPrimaryId()
    {
        return _dslPath.bbPrimaryId();
    }

    public static class Path<E extends BbGroup> extends EntityPath<E>
    {
        private BbCourse2TrJournalRel.Path<BbCourse2TrJournalRel> _bbCourse2TrJournalRel;
        private Group.Path<Group> _group;
        private PropertyPath<String> _title;
        private PropertyPath<String> _bbPrimaryId;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Связь ЭУК с реализацией дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroup#getBbCourse2TrJournalRel()
     */
        public BbCourse2TrJournalRel.Path<BbCourse2TrJournalRel> bbCourse2TrJournalRel()
        {
            if(_bbCourse2TrJournalRel == null )
                _bbCourse2TrJournalRel = new BbCourse2TrJournalRel.Path<BbCourse2TrJournalRel>(L_BB_COURSE2_TR_JOURNAL_REL, this);
            return _bbCourse2TrJournalRel;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroup#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(BbGroupGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Идентификатор группы в Blackboard. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroup#getBbPrimaryId()
     */
        public PropertyPath<String> bbPrimaryId()
        {
            if(_bbPrimaryId == null )
                _bbPrimaryId = new PropertyPath<String>(BbGroupGen.P_BB_PRIMARY_ID, this);
            return _bbPrimaryId;
        }

        public Class getEntityClass()
        {
            return BbGroup.class;
        }

        public String getEntityName()
        {
            return "bbGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
