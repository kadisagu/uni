/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.SignerDocumentAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeRel;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 27.12.2013
 */
@Configuration
public class FefuSettingsSignerDocumentAddEdit extends BusinessComponentManager
{
    public final static String SIGNER_DS = "signerDS";
    public final static String SIGNER_DOCUMENT_TYPE_LIST_DS = "signerDocumentTypeListDS";
    public static final String SIGNER_DOCUMENT_TYPE_LIST_PARAM = "signerDocumentTypeList";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SIGNER_DS, signerDSHandler()).addColumn(EmployeePostPossibleVisa.POSSIBLE_VISA_FULL_TITLE))
                .addDataSource(searchListDS(SIGNER_DOCUMENT_TYPE_LIST_DS, getSignerDocumentTypeListDS(), signerDocumentTypeListDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> signerDSHandler()
    {
        EntityComboDataSourceHandler comboDataSourceHandler = new EntityComboDataSourceHandler(getName(), EmployeePostPossibleVisa.class);
        comboDataSourceHandler.filter(EmployeePostPossibleVisa.entity().person().identityCard().fullFio());
        comboDataSourceHandler.filter(EmployeePostPossibleVisa.title());
        comboDataSourceHandler.order(EmployeePostPossibleVisa.entity().person().identityCard().fullFio());
        return comboDataSourceHandler;
    }

    @Bean
    public ColumnListExtPoint getSignerDocumentTypeListDS()
    {
        return columnListExtPointBuilder(SIGNER_DOCUMENT_TYPE_LIST_DS)
                .addColumn(textColumn("eduLevelStage", FefuSignerStuDocTypeRel.eduLevelStage().shortTitle()))
                .addColumn(textColumn("signer", FefuSignerStuDocTypeRel.signer().entity().person().identityCard().fullFio()))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> signerDocumentTypeListDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<FefuSignerStuDocTypeRel> signerDocumentTypeList = context.get(SIGNER_DOCUMENT_TYPE_LIST_PARAM);
                return ListOutputBuilder.get(input, signerDocumentTypeList == null ? new ArrayList<>() : signerDocumentTypeList).build();
            }
        };
    }
}
