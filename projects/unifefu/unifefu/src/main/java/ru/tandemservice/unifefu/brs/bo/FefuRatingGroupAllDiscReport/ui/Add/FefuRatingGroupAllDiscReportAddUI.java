package ru.tandemservice.unifefu.brs.bo.FefuRatingGroupAllDiscReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unifefu.brs.base.BaseFefuReportAddUI;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuRatingGroupAllDiscReport.FefuRatingGroupAllDiscReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuRatingGroupAllDiscReport.logic.FefuRatingGroupAllDiscReportParams;
import ru.tandemservice.unifefu.brs.bo.FefuRatingGroupAllDiscReport.ui.View.FefuRatingGroupAllDiscReportView;
import ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport;

import java.util.Collections;
import java.util.Date;

/**
 * User: amakarova
 * Date: 23.12.13
 */
public class FefuRatingGroupAllDiscReportAddUI extends BaseFefuReportAddUI<FefuRatingGroupAllDiscReportParams>
{
    @Override
    protected void fillDefaults(FefuRatingGroupAllDiscReportParams reportSettings)
    {
        super.fillDefaults(reportSettings);
        reportSettings.setFormativeOrgUnit(getCurrentFormativeOrgUnit());
        reportSettings.setCheckDate(new Date());
        reportSettings.setOnlyFilledJournals(FefuBrsReportManager.instance().yesNoItemListExtPoint().getItem(FefuBrsReportManager.YES_ID.toString()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuBrsReportManager.GROUP_DS.equals(dataSource.getName()))
        {
            if (null != getReportSettings().getFormativeOrgUnit())
                dataSource.put(FefuBrsReportManager.PARAM_FORMATIVE_OU, Collections.singletonList(getReportSettings().getFormativeOrgUnit()));
        }
    }

    public void onClickApply()
    {
        FefuRatingGroupAllDiscReport report = FefuRatingGroupAllDiscReportManager.instance().dao().createReport(getReportSettings());
        deactivate();
        _uiActivation.asDesktopRoot(FefuRatingGroupAllDiscReportView.class).parameter(UIPresenter.PUBLISHER_ID, report.getId()).activate();
    }
}