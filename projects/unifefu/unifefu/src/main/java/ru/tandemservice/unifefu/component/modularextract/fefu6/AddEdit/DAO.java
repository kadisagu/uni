/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu6.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.fias.base.bo.util.CountryAutocompleteModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Nationality;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuChangeFioStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 03.09.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuChangeFioStuExtract, Model> implements IDAO
{
    @Override
    protected FefuChangeFioStuExtract createNewInstance()
    {
        return new FefuChangeFioStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setCitizenshipModel(new CountryAutocompleteModel());
        model.setIdentityCardTypeList(getCatalogItemListOrderByCode(IdentityCardType.class));
        model.setNationalityModel(new LazySimpleSelectModel<>(Nationality.class).setRecordLimit(50));
        model.setSexList(getCatalogItemList(Sex.class));

        FefuChangeFioStuExtract extract = model.getExtract();
        Student student = extract.getEntity();

        if (model.isAddForm())
        {
            IdentityCard card = student.getPerson().getIdentityCard();
            extract.setLastNameNew(card.getLastName());
            extract.setFirstNameNew(card.getFirstName());
            extract.setMiddleNameNew(card.getMiddleName());
            extract.setSex(card.getSex());
            extract.setBirthDate(card.getBirthDate());
            extract.setBirthPlace(card.getBirthPlace());
            extract.setNationality(card.getNationality());
            extract.setCitizenship(card.getCitizenship());
        }
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setFirstNameNew(StringUtils.capitalize(model.getExtract().getFirstNameNew()));
        model.getExtract().setLastNameNew(StringUtils.capitalize(model.getExtract().getLastNameNew()));
        model.getExtract().setMiddleNameNew(StringUtils.capitalize(model.getExtract().getMiddleNameNew()));

        //save rollback data
        model.getExtract().setLastActiveIdentityCard(model.getExtract().getEntity().getPerson().getIdentityCard());
        super.update(model);
    }
}