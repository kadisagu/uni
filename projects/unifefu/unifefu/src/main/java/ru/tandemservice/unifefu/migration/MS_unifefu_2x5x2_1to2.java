package ru.tandemservice.unifefu.migration;

import org.apache.commons.collections.map.HashedMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 22.04.2014
 */
public class MS_unifefu_2x5x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<Long, ExtensionsWrapper> orderIdToExtMap = new HashedMap();
        Map<String, ExtensionsWrapper> directumIdToExtMap = new HashedMap();
        initExtensionMaps(tool, orderIdToExtMap, directumIdToExtMap);

        Map<Long, List<DirectumLogWrapper>> orderIdToLogListMap = new HashedMap();
        Map<String, List<DirectumLogWrapper>> directumIdToLogListMap = new HashedMap();
        Map<Long, OrderDirectumIdsWrapper> directumIdWrappersMap = new HashedMap();
        initDirectumLogMaps(tool, orderIdToLogListMap, directumIdToLogListMap, directumIdWrappersMap);

        PreparedStatement extUpdate = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMORDERID_P=?, DIRECTUMTASKID_P=? where ID=?");

        int updatedCount = 0;
        for (Map.Entry<Long, OrderDirectumIdsWrapper> idsEntry : directumIdWrappersMap.entrySet())
        {
            OrderDirectumIdsWrapper idWrapper = idsEntry.getValue();
            ExtensionsWrapper extWrapper = orderIdToExtMap.get(idsEntry.getKey());

            if (null != extWrapper)
            {
                extUpdate.setLong(3, extWrapper.getId());
                if (null != idWrapper.getDirectumOrderId()) extUpdate.setString(1, idWrapper.getDirectumOrderId());
                else extUpdate.setString(1, extWrapper.getDirectumOrderId());
                if (null != idWrapper.getDirectumTaskId()) extUpdate.setString(2, idWrapper.getDirectumTaskId());
                else extUpdate.setString(2, extWrapper.getDirectumTaskId());
                extUpdate.addBatch();
            }

            if (++updatedCount % 256 == 0)
            {
                extUpdate.executeBatch();
                System.out.println("256 Order extensions were updated.");
            }
        }

        extUpdate.executeBatch();
        System.out.println("Last Order extensions were updated.");
    }

    private void initExtensionMaps(DBTool tool, Map<Long, ExtensionsWrapper> orderIdToExtMap, Map<String, ExtensionsWrapper> directumIdToExtMap) throws Exception
    {
        Statement extStmt = tool.getConnection().createStatement();
        extStmt.execute("select ID, ORDER_ID, ORDERID_P, DIRECTUMORDERID_P, DIRECTUMTASKID_P from FEFUSTUDENTORDEREXTENSION_T");
        ResultSet extRs = extStmt.getResultSet();

        while (extRs.next())
        {
            ExtensionsWrapper extWrapper = new ExtensionsWrapper(extRs);
            Long orderId = null != extWrapper.getOrder() ? extWrapper.getOrder() : extWrapper.getOrderId();
            if (null != orderId) orderIdToExtMap.put(orderId, extWrapper);
            if (null != extWrapper.getDirectumOrderId())
                directumIdToExtMap.put(extWrapper.getDirectumOrderId(), extWrapper);
        }
    }

    private void initDirectumLogMaps(DBTool tool, Map<Long, List<DirectumLogWrapper>> orderIdToLogMap, Map<String, List<DirectumLogWrapper>> directumIdToLogMap, Map<Long, OrderDirectumIdsWrapper> directumIdsWrapperMap) throws Exception
    {
        Statement logStmt = tool.getConnection().createStatement();
        logStmt.execute("select dl.ID, dl.ORDER_ID, dl.ORDEREXT_ID, dl.DIRECTUMORDERID_P, dl.DIRECTUMTASKID_P, dl.OPERATIONDATETIME_P, dl.OPERATIONTYPE_P, ext.ORDER_ID, ext.ORDERID_P \n" +
                "from FEFUDIRECTUMLOG_T dl left outer join FEFUSTUDENTORDEREXTENSION_T ext on dl.ORDEREXT_ID=ext.id order by dl.OPERATIONDATETIME_P desc");
        ResultSet logRs = logStmt.getResultSet();

        while (logRs.next())
        {
            DirectumLogWrapper logWrapper = new DirectumLogWrapper(logRs);
            Long orderId = logWrapper.getOrder();
            if (null == orderId && null != logWrapper.getExtOrder()) orderId = logWrapper.getExtOrder();
            if (null == orderId && null != logWrapper.getExtOrderId()) orderId = logWrapper.getExtOrderId();
            List<DirectumLogWrapper> orderLogWrappers = orderIdToLogMap.get(orderId);
            if (null == orderLogWrappers) orderLogWrappers = new ArrayList<>();
            orderLogWrappers.add(logWrapper);
            orderIdToLogMap.put(orderId, orderLogWrappers);

            if (null != logWrapper.getDirectumOrderId())
            {
                List<DirectumLogWrapper> directumIdLogWrappers = directumIdToLogMap.get(logWrapper.getDirectumOrderId());
                if (null == directumIdLogWrappers) directumIdLogWrappers = new ArrayList<>();
                directumIdLogWrappers.add(logWrapper);
                directumIdToLogMap.put(logWrapper.getDirectumOrderId(), directumIdLogWrappers);
            }

            OrderDirectumIdsWrapper directumIdsWrapper = directumIdsWrapperMap.get(orderId);
            if (null == directumIdsWrapper) directumIdsWrapper = new OrderDirectumIdsWrapper(orderId);
            if (null == directumIdsWrapper.getDirectumOrderId() && logWrapper.getOperationType().contains("Создание документа"))
                directumIdsWrapper.setDirectumOrderId(logWrapper.getDirectumOrderId());
            if (null == directumIdsWrapper.getDirectumTaskId() && logWrapper.getOperationType().contains("Создание задачи"))
                directumIdsWrapper.setDirectumTaskId(logWrapper.getDirectumTaskId());
            directumIdsWrapperMap.put(orderId, directumIdsWrapper);
        }
    }

    private class ExtensionsWrapper
    {
        private Long _id;
        private Long _order;
        private Long _orderId;
        private String _directumOrderId;
        private String _directumTaskId;

        private ExtensionsWrapper(ResultSet resultSet) throws SQLException
        {
            _id = resultSet.getLong(1);
            _order = resultSet.getLong(2);
            _orderId = resultSet.getLong(3);
            _directumOrderId = resultSet.getString(4);
            _directumTaskId = resultSet.getString(5);
        }

        private Long getId()
        {
            return _id;
        }

        private void setId(Long id)
        {
            _id = id;
        }

        private Long getOrder()
        {
            return _order;
        }

        private void setOrder(Long order)
        {
            _order = order;
        }

        private Long getOrderId()
        {
            return _orderId;
        }

        private void setOrderId(Long orderId)
        {
            _orderId = orderId;
        }

        private String getDirectumOrderId()
        {
            return _directumOrderId;
        }

        private void setDirectumOrderId(String directumOrderId)
        {
            _directumOrderId = directumOrderId;
        }

        private String getDirectumTaskId()
        {
            return _directumTaskId;
        }

        private void setDirectumTaskId(String directumTaskId)
        {
            _directumTaskId = directumTaskId;
        }
    }

    private class DirectumLogWrapper
    {
        private Long _id;
        private Long _order;
        private Long _extension;
        private String _directumOrderId;
        private String _directumTaskId;
        private Date _operationDateTime;
        private String _operationType;
        private Long _extOrder;
        private Long _extOrderId;

        private DirectumLogWrapper(ResultSet resultSet) throws SQLException
        {
            _id = resultSet.getLong(1);
            _order = resultSet.getLong(2);
            _extension = resultSet.getLong(3);
            _directumOrderId = resultSet.getString(4);
            _directumTaskId = resultSet.getString(5);
            _operationDateTime = resultSet.getDate(6);
            _operationType = resultSet.getString(7);
            _extOrder = resultSet.getLong(8);
            _extOrderId = resultSet.getLong(9);
        }

        private Long getId()
        {
            return _id;
        }

        private void setId(Long id)
        {
            _id = id;
        }

        private Long getOrder()
        {
            return _order;
        }

        private void setOrder(Long order)
        {
            _order = order;
        }

        private Long getExtension()
        {
            return _extension;
        }

        private void setExtension(Long extension)
        {
            _extension = extension;
        }

        private String getDirectumOrderId()
        {
            return _directumOrderId;
        }

        private void setDirectumOrderId(String directumOrderId)
        {
            _directumOrderId = directumOrderId;
        }

        private String getDirectumTaskId()
        {
            return _directumTaskId;
        }

        private void setDirectumTaskId(String directumTaskId)
        {
            _directumTaskId = directumTaskId;
        }

        private Date getOperationDateTime()
        {
            return _operationDateTime;
        }

        private void setOperationDateTime(Date operationDateTime)
        {
            _operationDateTime = operationDateTime;
        }

        private String getOperationType()
        {
            return _operationType;
        }

        private void setOperationType(String operationType)
        {
            _operationType = operationType;
        }

        private Long getExtOrder()
        {
            return _extOrder;
        }

        private void setExtOrder(Long extOrder)
        {
            _extOrder = extOrder;
        }

        private Long getExtOrderId()
        {
            return _extOrderId;
        }

        private void setExtOrderId(Long extOrderId)
        {
            _extOrderId = extOrderId;
        }
    }

    private class OrderDirectumIdsWrapper
    {
        private Long _orderId;
        private String _directumOrderId;
        private String _directumTaskId;

        private OrderDirectumIdsWrapper(Long orderId)
        {
            _orderId = orderId;
        }

        private OrderDirectumIdsWrapper(Long orderId, String directumOrderId, String directumTaskId)
        {
            _orderId = orderId;
            _directumOrderId = directumOrderId;
            _directumTaskId = directumTaskId;
        }

        private Long getOrderId()
        {
            return _orderId;
        }

        private void setOrderId(Long orderId)
        {
            _orderId = orderId;
        }

        private String getDirectumOrderId()
        {
            return _directumOrderId;
        }

        private void setDirectumOrderId(String directumOrderId)
        {
            _directumOrderId = directumOrderId;
        }

        private String getDirectumTaskId()
        {
            return _directumTaskId;
        }

        private void setDirectumTaskId(String directumTaskId)
        {
            _directumTaskId = directumTaskId;
        }
    }
}