/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.spec.KeySpec;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Шифрование и дешифрование строки
 *
 * @author Vlad
 */
public class SymmetricCrypto
{
    private final SecretKey _key = createKey(new byte[]
            {
                    -23, -28, 3, -84, 10, 20, -47, -113
                    , 15, -15, -18, -16, -1, -4, 33, 34
                    , -3, -25, -13, 10, 69, 36, -126, -56
                    , 12, -109, 26, 1, 50, -26, 89, -55
                    , 73, -19, -86, -101, -113, -89, -16, 80
                    , -13, -31, -77, 120, -41, 78, -128, -22
                    , 66, 86, 92, 126, -119, 97, 90, 93
                    , 31, -18, 125, 59, 22, 64, -110, -111
                    , -77, -25, 42, 116, 52, 18, 83, -100
                    , -7, -49, -28, 5, -92, -2, 100, -16
                    , -68, 66, 54, -64, -93, -108, -22, 43
                    , -99, 114, -66, 115, -96, -86, -5, 123
                    , 28, 106, 24, -62, -14, -76, -119, -7
                    , -68, -28, 122, 14, -97, -120, -10, -87
                    , -78, -41, 95, 80, 118, -15, -15, 68
                    , -61, 83, -114, 15, 5, 72, -123, -84
            }
    );

    public String crypt(String text)
    {
        if (StringUtils.isEmpty(text))
            return null;

        byte[] bytes;
        try
        {
            bytes = text.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            return text;
        }

        boolean zip = false;
        if (text.length() > 200)
        {
            // если строка длинная, то можно попробовать ее позиповать
            try
            {
                byte[] zipBytes = zip(bytes);
                if (zipBytes.length < bytes.length)
                {
                    // сжатая строка занимает меьший объем - использовать ее
                    zip = true;
                    bytes = zipBytes;
                }
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        boolean crypted = false;
        if (_key != null)
        {
            // шифруем
            try
            {
                Cipher cipher = Cipher.getInstance("DESede");
                cipher.init(Cipher.ENCRYPT_MODE, _key);
                bytes = cipher.doFinal(bytes);
                crypted = true;
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        // это флаг, где в 1 бите указывается зазиповано или нет и в 0 бите указывается зашифровано или нет
        int prefixIndex = zip ? 2 : 0;
        if (crypted)
            prefixIndex++;
        prefixIndex += 'G'; // используются 4 буквы: G, H, I, J

        return "" + (char) prefixIndex + new String(Base64.encodeBase64(bytes, false));
    }

    public String decrypt(String cryptText)
    {
        if (StringUtils.isEmpty(cryptText))
            return null;

        char prefixChar = cryptText.charAt(0);
        if (prefixChar < 'G' || prefixChar > 'J')
            return cryptText;

        int prefixIndex = prefixChar - 'G';
        boolean zip = (prefixIndex & 2) != 0;
        boolean crypted = (prefixIndex & 1) != 0;

        try
        {
            byte[] bytes = cryptText.substring(1).getBytes();
            bytes = Base64.decodeBase64(bytes);

            if (crypted)
            {
                if (_key == null)
                    throw new RuntimeException("Parameter is crypted but there is not a security key");

                Cipher cipher = Cipher.getInstance("DESede");
                cipher.init(Cipher.DECRYPT_MODE, _key);
                bytes = cipher.doFinal(bytes);
            }

            if (zip)
            {
                bytes = unzip(bytes);
            }

            return new String(bytes, "UTF-8");
        } catch (Exception e)
        {
            throw new RuntimeException("Can't restore parameter", e);
        }
    }

    private static byte[] zip(byte[] bytes) throws Exception
    {
        ByteArrayOutputStream bosCompressed = new ByteArrayOutputStream();
        GZIPOutputStream gos = new GZIPOutputStream(bosCompressed);
        gos.write(bytes);
        gos.close();
        return bosCompressed.toByteArray();
    }

    private static byte[] unzip(byte[] bytes) throws Exception
    {
        InputStream is = new GZIPInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int b;
        while ((b = is.read()) != -1)
        {
            bos.write(b);
        }
        return bos.toByteArray();
    }

    private static SecretKey createKey(String keyString)
    {
        byte[] keyBytes;
        try
        {
            keyBytes = keyString.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException("Can't create secret key", e);
        }
        return createKey(keyBytes);
    }

    private static SecretKey createKey(byte[] keyBytes)
    {
        try
        {
            KeySpec keySpec = new DESedeKeySpec(keyBytes);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
            return keyFactory.generateSecret(keySpec);
        } catch (Exception e)
        {
            throw new RuntimeException("Can't create secret key", e);
        }
    }
}