package ru.tandemservice.unifefu.component.report.EntrantDailyRatingByED.EntrantDailyRatingByEDAdd;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uniec.component.report.EntrantDailyRatingByED.EntrantDailyRatingByEDAdd.Model;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * User: amakarova
 * Date: 22.05.13
 */
@Zlo
public class EntrantDailyRatingByEDReportBuilder extends ru.tandemservice.uniec.component.report.EntrantDailyRatingByED.EntrantDailyRatingByEDAdd.EntrantDailyRatingByEDReportBuilder {

    public EntrantDailyRatingByEDReportBuilder(Model model, Session session) {
        super(model, session);
    }

    @Override
    protected String getRegNumber(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return requestedEnrollmentDirection.getEntrantRequest().getEntrant().getPersonalNumber();
    }
}
