/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.List.FefuWorkGraphList;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraph;

import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
public class FefuWorkGraphDSHandler extends DefaultSearchDataSourceHandler
{
    public FefuWorkGraphDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    public DSOutput execute(DSInput input, ExecutionContext context)
    {
        EppYearEducationProcess pupnag = context.get(FefuWorkGraphList.PUPNAG);
        EppState state = context.get(FefuWorkGraphList.STATE);
        List<DevelopForm> developForms = context.get(FefuWorkGraphList.DEVELOP_FORM);
        List<DevelopCondition> developConditions = context.get(FefuWorkGraphList.DEVELOP_CONDITION);
        List<DevelopTech> developTechs = context.get(FefuWorkGraphList.DEVELOP_TECH);
        List<DevelopGrid> developGrids = context.get(FefuWorkGraphList.DEVELOP_GRID);

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(FefuWorkGraph.class, "wg");

        if (pupnag != null){ dql.where(eq(property("wg", FefuWorkGraph.year()), value(pupnag))); }
        if (state != null){ dql.where(eq(property("wg", FefuWorkGraph.state()), value(state))); }
        if (developForms != null && !developForms.isEmpty()){ dql.where(in(property("wg", FefuWorkGraph.developForm()), developForms)); }
        if (developConditions != null && !developConditions.isEmpty()){ dql.where(in(property("wg", FefuWorkGraph.developCondition()), developConditions)); }
        if (developTechs != null && !developTechs.isEmpty()){ dql.where(in(property("wg", FefuWorkGraph.developTech()), developTechs)); }
        if (developGrids != null && !developGrids.isEmpty()){ dql.where(in(property("wg", FefuWorkGraph.developGrid()), developGrids)); }

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build().ordering(new Comparator<FefuWorkGraph>()
        {
            @Override
            public int compare(FefuWorkGraph w1, FefuWorkGraph w2)
            {
                return w1.getTitle().compareTo(w2.getTitle());
            }
        });
    }
}