/* $Id: IFEFUPersonGuidDaemonDao.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;

/**
 * @author Dmitry Seleznev
 * @since 27.05.2013
 */
public interface IFEFUPersonGuidDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IFEFUPersonGuidDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IFEFUPersonGuidDaemonDao> instance = new SpringBeanCache<>(IFEFUPersonGuidDaemonDao.class.getName());

    /**
     * обновляет таблицу с гуидами персон (студентов и абитуриентов)
     *
     * @return true, если в базе были изменения
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    boolean doActualizePersonGuidsTable();

    /**
     * Создаёт запись в таблице с гуидами для передаваемого идентификатора персоны
     *
     * @param personId - идентификатор персоны
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    FefuNsiIds doActualizePersonGuid(Long personId);

    /**
     * Создаёт фиктивный GUID персоны
     *
     * @param guid - сторонний GUID
     * @return - объект GUID
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    FefuNsiIds doRegisterFictiveGuid(String guid);
}