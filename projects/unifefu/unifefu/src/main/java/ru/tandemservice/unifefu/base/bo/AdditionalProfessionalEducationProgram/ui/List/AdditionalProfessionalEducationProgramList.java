package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.AdditionalProfessionalEducationProgramManager;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.logic.ApeProgramDSHandler;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.logic.Constants.*;

@Configuration
public class AdditionalProfessionalEducationProgramList extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(APE_PROGRAM_DS, apeProgramDS(), apeProgramDSHandler()))
                .addDataSource(selectDS(EDUCATION_ORG_UNIT_DS, AdditionalProfessionalEducationProgramManager.instance().educationOrgUnitDSHandler()).addColumn(EducationOrgUnit.P_TITLE))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, AdditionalProfessionalEducationProgramManager.instance().formativeOrgUnitDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
                .addDataSource(selectDS(TERRITORIAL_ORG_UNIT_DS, AdditionalProfessionalEducationProgramManager.instance().territorialOrgUnitDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
                .addDataSource(selectDS(PRODUCING_ORG_UNIT_DS, AdditionalProfessionalEducationProgramManager.instance().producingOrgUnitDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
                .addDataSource(selectDS(PROGRAM_STATUS_DS, AdditionalProfessionalEducationProgramManager.instance().programStatusDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(PROGRAM_PUBLICATION_STATUS_DS, AdditionalProfessionalEducationProgramManager.instance().programPublicationStatusDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(EDUCATION_LEVEL_DS, AdditionalProfessionalEducationProgramManager.instance().educationLevelDSHandler()).addColumn(TITLE))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> apeProgramDSHandler()
    {
        return new ApeProgramDSHandler(getName());
    }

    @Bean
    public ColumnListExtPoint apeProgramDS()
    {
        return columnListExtPointBuilder(APE_PROGRAM_DS)
                .addColumn(textColumn(TITLE_C, FefuAdditionalProfessionalEducationProgram.title()).order())
                .addColumn(textColumn(FORMATIVE_ORG_UNIT_C, FefuAdditionalProfessionalEducationProgram.formativeOrgUnit().fullTitle()).order())
                .addColumn(textColumn(TERRITORIAL_ORG_UNIT_C, FefuAdditionalProfessionalEducationProgram.territorialOrgUnit().fullTitle()).order())
                .addColumn(textColumn(PROGRAM_OPENING_DATE, FefuAdditionalProfessionalEducationProgram.programOpeningDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn(PROGRAM_CLOSING_DATE, FefuAdditionalProfessionalEducationProgram.programClosingDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn(PRODUCING_ORG_UNIT_C, FefuAdditionalProfessionalEducationProgram.producingOrgUnit().fullTitle()).order())
                .addColumn(textColumn(PROGRAM_STATUS_C, FefuAdditionalProfessionalEducationProgram.programStatus().title()).order())
                .addColumn(textColumn(PROGRAM_PUBLICATION_STATUS_C, FefuAdditionalProfessionalEducationProgram.programPublicationStatus().title()).order())
                .addColumn(textColumn(EDUCATION_LEVEL_C, FefuAdditionalProfessionalEducationProgram.educationLevel().title()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("editFefuApeProgramList"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                                        alert(APE_PROGRAM_DS + ".delete.alert")).permissionKey("deleteFefuApeProgramList"))
                .create();
    }

}
