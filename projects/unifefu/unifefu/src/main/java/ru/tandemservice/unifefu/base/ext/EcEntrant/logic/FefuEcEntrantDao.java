/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EcEntrant.logic;

import ru.tandemservice.uniec.base.bo.EcEntrant.logic.EcEntrantDao;
import ru.tandemservice.uniec.entity.catalog.ProfileKnowledge;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.ProfileExaminationMark;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;

import java.util.List;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 19.06.2013
 */
public class FefuEcEntrantDao extends EcEntrantDao
{
    @Override
    public void saveOrUpdateEntrantRequest(
                                           List<RequestedEnrollmentDirection> selectedList,
                                           List<RequestedEnrollmentDirection> forDeleteList,
                                           boolean addForm,
                                           EntrantRequest entrantRequest,
                                           Integer entrantRequestNumber,
                                           Integer entrantDirectionNumber,
                                           Map<Long, List<ProfileKnowledge>> profileKnowledgeMap,
                                           Map<Long, List<ProfileExaminationMark>> profileMarkMap)
    {
        if (!addForm && entrantRequest.getId() != null)
            UnifefuDaoFacade.getFefuEntrantDAO().checkAccessForTCEmployee(entrantRequest);
        super.saveOrUpdateEntrantRequest(selectedList, forDeleteList, addForm, entrantRequest, entrantRequestNumber, entrantDirectionNumber, profileKnowledgeMap, profileMarkMap);
    }

    @Override
    public void updateOriginalDocuments(RequestedEnrollmentDirection direction, boolean save)
    {
        if (!save)
            UnifefuDaoFacade.getFefuEntrantDAO().checkAccessForTCEmployee(direction.getEntrantRequest());
        super.updateOriginalDocuments(direction, save);
    }

    @Override
    public void deleteRequestedEnrollmentDirection(Long directionId)
    {
        RequestedEnrollmentDirection direction = getNotNull(RequestedEnrollmentDirection.class, directionId);
        UnifefuDaoFacade.getFefuEntrantDAO().checkAccessForTCEmployee(direction.getEntrantRequest());
        super.deleteRequestedEnrollmentDirection(directionId);
    }

    @Override
    public void deleteEntrantRequest(Long entrantRequestId)
    {
        UnifefuDaoFacade.getFefuEntrantDAO().checkAccessForTCEmployee((EntrantRequest)proxy(entrantRequestId));
        super.deleteEntrantRequest(entrantRequestId);
    }
}