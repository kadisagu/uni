/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuImtsaImport;

import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.validator.FileExtensionsValidator;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.FefuImtsaImportLog;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.node.ImtsaFile;

import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
    private Long _id;
    private EppEduPlanVersionBlock _block;
    private boolean _secondGosGeneration;
    private IUploadFile _source;
    private ImtsaFile _result;
    private ISelectModel _cycleSelectModel;
    private ISelectModel _partSelectModel;
    private Map<String, Set<String>> _cycles2PartsMap;
    private Map<String, EppPlanStructure> _cyclesMap;
    private Map<String, EppPlanStructure> _partsMap;
    private Map<String, String> _cycleTitlesMap;
    private boolean _saveConfig;
    private ImportStage _stage;
    private String _errorMessage;
    private FefuImtsaImportLog _log;
    private String _encoding;


    public Long getId(){ return _id; }
    public void setId(Long id){ _id = id; }

    public EppEduPlanVersionBlock getBlock(){ return _block; }
    public void setBlock(EppEduPlanVersionBlock block){ _block = block; }

    public boolean isSecondGosGeneration(){ return _secondGosGeneration; }
    public void setSecondGosGeneration(boolean secondGosGeneration){ _secondGosGeneration = secondGosGeneration; }

    public IUploadFile getSource(){ return _source; }
    public void setSource(IUploadFile source){ _source = source; }

    public ImtsaFile getResult(){ return _result; }
    public void setResult(ImtsaFile result){ _result = result; }

    public ISelectModel getCycleSelectModel(){ return _cycleSelectModel; }
    public void setCycleSelectModel(ISelectModel cycleSelectModel){ _cycleSelectModel = cycleSelectModel; }

    public ISelectModel getPartSelectModel(){ return _partSelectModel; }
    public void setPartSelectModel(ISelectModel partSelectModel){ _partSelectModel = partSelectModel; }

    public Map<String, Set<String>> getCycles2PartsMap(){ return _cycles2PartsMap; }
    public void setCycles2PartsMap(Map<String, Set<String>> cycles2PartsMap){ _cycles2PartsMap = cycles2PartsMap; }

    public Map<String, EppPlanStructure> getCyclesMap(){ return _cyclesMap; }
    public void setCyclesMap(Map<String, EppPlanStructure> cyclesMap){ _cyclesMap = cyclesMap; }

    public Map<String, EppPlanStructure> getPartsMap(){ return _partsMap; }
    public void setPartsMap(Map<String, EppPlanStructure> partsMap){ _partsMap = partsMap; }

    public Map<String, String> getCycleTitlesMap(){ return _cycleTitlesMap; }
    public void setCycleTitlesMap(Map<String, String> cycleTitlesMap){ _cycleTitlesMap = cycleTitlesMap; }

    public boolean isSaveConfig(){ return _saveConfig; }
    public void setSaveConfig(boolean saveConfig){ _saveConfig = saveConfig; }

    public ImportStage getStage(){ return _stage; }
    public void setStage(ImportStage stage){ _stage = stage; }

    public String getErrorMessage(){ return _errorMessage; }
    public void setErrorMessage(String errorMessage){ _errorMessage = errorMessage; }

    public FefuImtsaImportLog getLog(){ return _log; }
    public void setLog(FefuImtsaImportLog log){ _log = log; }

    public String getEncoding(){ return _encoding; }
    public void setEncoding(String encoding){ _encoding = encoding; }


    private String _currentCycle;
    private String _currentPart;

    public String getCurrentCycle(){ return _currentCycle; }
    public void setCurrentCycle(String currentCycle){ _currentCycle = currentCycle; }

    public String getCurrentPart(){ return _currentPart; }
    public void setCurrentPart(String currentPart){ _currentPart = currentPart; }


    public Collection<String> getCycles(){ return _cycles2PartsMap.keySet(); }
    public Collection<String> getParts(){ return _cycles2PartsMap.get(_currentCycle); }

    public String getCycleName()
    {
        return _cycleTitlesMap.containsKey(_currentCycle) ?  _cycleTitlesMap.get(_currentCycle) : _currentCycle;
    }
    public String getPartName(){ return _currentPart.replace(_currentCycle, getCycleName()); }

    public EppPlanStructure getCycleValue(){ return _cyclesMap.get(_currentCycle); }
    public void setCycleValue(EppPlanStructure cycleValue){ _cyclesMap.put(_currentCycle, cycleValue); }

    public EppPlanStructure getPartValue(){ return _partsMap.get(_currentPart); }
    public void setPartValue(EppPlanStructure partValue){ _partsMap.put(_currentPart, partValue); }


    // calculated methods
    public List<Validator> getValidators()
    {
        List<Validator> list = new ArrayList<>();
        list.add(new Required());
        list.add(new FileExtensionsValidator("fileExtensions=xml"));
        return list;
    }


    public static enum ImportStage
    {
        PICK_UP,
        CONFIG,
        ERROR;

        public boolean isError(){ return this == ERROR; }
        public boolean isConfig(){ return this == CONFIG; }
        public boolean isPickUp(){ return this == PICK_UP; }
    }
}