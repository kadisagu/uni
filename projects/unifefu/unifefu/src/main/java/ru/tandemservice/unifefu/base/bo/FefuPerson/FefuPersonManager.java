/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPerson;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuPerson.logic.FefuPersonDao;
import ru.tandemservice.unifefu.base.bo.FefuPerson.logic.IFefuPersonDao;

/**
 * @author Dmitry Seleznev
 * @since 05.12.2014
 */
@Configuration
public class FefuPersonManager extends BusinessObjectManager
{
    public static FefuPersonManager instance()
    {
        return instance(FefuPersonManager.class);
    }

    @Bean
    public IFefuPersonDao dao()
    {
        return new FefuPersonDao();
    }
}