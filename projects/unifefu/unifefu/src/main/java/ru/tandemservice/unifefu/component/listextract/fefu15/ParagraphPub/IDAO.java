/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu15.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuEnrollStuDPOListExtract, Model>
{
}