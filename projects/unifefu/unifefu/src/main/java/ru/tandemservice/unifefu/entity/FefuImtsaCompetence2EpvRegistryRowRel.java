package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * @see ru.tandemservice.unifefu.entity.gen.FefuImtsaCompetence2EpvRegistryRowRelGen
 */
public class FefuImtsaCompetence2EpvRegistryRowRel extends FefuImtsaCompetence2EpvRegistryRowRelGen
{
    public FefuImtsaCompetence2EpvRegistryRowRel()
    {
    }

    public FefuImtsaCompetence2EpvRegistryRowRel(EppEpvRegistryRow registryRow, FefuCompetence fefuCompetence, int number)
    {
        setRegistryRow(registryRow);
        setFefuCompetence(fefuCompetence);
        setNumber(number);
    }

    @Override
    public String getGroupCode()
    {
        return getGroupShortTitle() + "-" + String.valueOf(getGroupNumber());
    }

    @Override
    public String getGroupShortTitle()
    {
        return getFefuCompetence().getEppSkillGroup().getShortTitle();
    }

    @Override
    public int getGroupNumber()
    {
        return getNumber();
    }
}