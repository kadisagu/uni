
package ru.tandemservice.unifefu.ws.blackboard.gradebook.gen;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScoreVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScoreVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="averageScore" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="columnId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="courseId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exempt" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="expansionData" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="firstAttemptId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="grade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="highestAttemptId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="instructorComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastAttemptId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lowestAttemptId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="manualGrade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="manualScore" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="memberId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="schemaGradeValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shortInstructorComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shortStudentComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="studentComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScoreVO", namespace = "http://gradebook.ws.blackboard/xsd", propOrder = {
    "averageScore",
    "columnId",
    "courseId",
    "exempt",
    "expansionData",
    "firstAttemptId",
    "grade",
    "highestAttemptId",
    "id",
    "instructorComments",
    "lastAttemptId",
    "lowestAttemptId",
    "manualGrade",
    "manualScore",
    "memberId",
    "schemaGradeValue",
    "shortInstructorComments",
    "shortStudentComments",
    "status",
    "studentComments",
    "userId"
})
public class ScoreVO {

    @XmlElementRef(name = "averageScore", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<Double> averageScore;
    @XmlElementRef(name = "columnId", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> columnId;
    @XmlElementRef(name = "courseId", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> courseId;
    protected Boolean exempt;
    @XmlElement(nillable = true)
    protected List<String> expansionData;
    @XmlElementRef(name = "firstAttemptId", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> firstAttemptId;
    @XmlElementRef(name = "grade", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> grade;
    @XmlElementRef(name = "highestAttemptId", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> highestAttemptId;
    @XmlElementRef(name = "id", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> id;
    @XmlElementRef(name = "instructorComments", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> instructorComments;
    @XmlElementRef(name = "lastAttemptId", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> lastAttemptId;
    @XmlElementRef(name = "lowestAttemptId", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> lowestAttemptId;
    @XmlElementRef(name = "manualGrade", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> manualGrade;
    @XmlElementRef(name = "manualScore", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<Double> manualScore;
    @XmlElementRef(name = "memberId", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> memberId;
    @XmlElementRef(name = "schemaGradeValue", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> schemaGradeValue;
    @XmlElementRef(name = "shortInstructorComments", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> shortInstructorComments;
    @XmlElementRef(name = "shortStudentComments", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> shortStudentComments;
    protected Integer status;
    @XmlElementRef(name = "studentComments", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> studentComments;
    @XmlElementRef(name = "userId", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> userId;

    /**
     * Gets the value of the averageScore property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getAverageScore() {
        return averageScore;
    }

    /**
     * Sets the value of the averageScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setAverageScore(JAXBElement<Double> value) {
        this.averageScore = ((JAXBElement<Double> ) value);
    }

    /**
     * Gets the value of the columnId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getColumnId() {
        return columnId;
    }

    /**
     * Sets the value of the columnId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setColumnId(JAXBElement<String> value) {
        this.columnId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the courseId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCourseId() {
        return courseId;
    }

    /**
     * Sets the value of the courseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCourseId(JAXBElement<String> value) {
        this.courseId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the exempt property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExempt() {
        return exempt;
    }

    /**
     * Sets the value of the exempt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExempt(Boolean value) {
        this.exempt = value;
    }

    /**
     * Gets the value of the expansionData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expansionData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpansionData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getExpansionData() {
        if (expansionData == null) {
            expansionData = new ArrayList<>();
        }
        return this.expansionData;
    }

    /**
     * Gets the value of the firstAttemptId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFirstAttemptId() {
        return firstAttemptId;
    }

    /**
     * Sets the value of the firstAttemptId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFirstAttemptId(JAXBElement<String> value) {
        this.firstAttemptId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the grade property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGrade() {
        return grade;
    }

    /**
     * Sets the value of the grade property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGrade(JAXBElement<String> value) {
        this.grade = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the highestAttemptId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHighestAttemptId() {
        return highestAttemptId;
    }

    /**
     * Sets the value of the highestAttemptId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHighestAttemptId(JAXBElement<String> value) {
        this.highestAttemptId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setId(JAXBElement<String> value) {
        this.id = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the instructorComments property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInstructorComments() {
        return instructorComments;
    }

    /**
     * Sets the value of the instructorComments property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInstructorComments(JAXBElement<String> value) {
        this.instructorComments = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the lastAttemptId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLastAttemptId() {
        return lastAttemptId;
    }

    /**
     * Sets the value of the lastAttemptId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLastAttemptId(JAXBElement<String> value) {
        this.lastAttemptId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the lowestAttemptId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLowestAttemptId() {
        return lowestAttemptId;
    }

    /**
     * Sets the value of the lowestAttemptId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLowestAttemptId(JAXBElement<String> value) {
        this.lowestAttemptId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the manualGrade property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getManualGrade() {
        return manualGrade;
    }

    /**
     * Sets the value of the manualGrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setManualGrade(JAXBElement<String> value) {
        this.manualGrade = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the manualScore property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getManualScore() {
        return manualScore;
    }

    /**
     * Sets the value of the manualScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setManualScore(JAXBElement<Double> value) {
        this.manualScore = ((JAXBElement<Double> ) value);
    }

    /**
     * Gets the value of the memberId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMemberId() {
        return memberId;
    }

    /**
     * Sets the value of the memberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMemberId(JAXBElement<String> value) {
        this.memberId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the schemaGradeValue property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSchemaGradeValue() {
        return schemaGradeValue;
    }

    /**
     * Sets the value of the schemaGradeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSchemaGradeValue(JAXBElement<String> value) {
        this.schemaGradeValue = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the shortInstructorComments property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShortInstructorComments() {
        return shortInstructorComments;
    }

    /**
     * Sets the value of the shortInstructorComments property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShortInstructorComments(JAXBElement<String> value) {
        this.shortInstructorComments = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the shortStudentComments property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShortStudentComments() {
        return shortStudentComments;
    }

    /**
     * Sets the value of the shortStudentComments property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShortStudentComments(JAXBElement<String> value) {
        this.shortStudentComments = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStatus(Integer value) {
        this.status = value;
    }

    /**
     * Gets the value of the studentComments property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStudentComments() {
        return studentComments;
    }

    /**
     * Sets the value of the studentComments property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStudentComments(JAXBElement<String> value) {
        this.studentComments = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserId(JAXBElement<String> value) {
        this.userId = ((JAXBElement<String> ) value);
    }

}
