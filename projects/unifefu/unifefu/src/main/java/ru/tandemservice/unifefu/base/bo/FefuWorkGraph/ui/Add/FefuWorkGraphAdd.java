/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.Add;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.unifefu.entity.catalog.FefuSchedulePartitionType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 04.10.2013
 */
@Configuration
public class FefuWorkGraphAdd extends BusinessComponentManager
{
    public static final String PUPNAG_DS = "pupnagDS";
    public static final String PARTITION_TYPE_DS = "partitionTypeDS";
    public static final String DEVELOP_COMBINATION_DS = "developCombinationDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PUPNAG_DS, pupnagDSHandler()))
                .addDataSource(selectDS(PARTITION_TYPE_DS, partitionTypeDSHandler()))
                .addDataSource(selectDS(DEVELOP_COMBINATION_DS, developCombinationDSHandler())
                        .addColumn(DevelopCombination.title().s()))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developTechDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developGridDSConfig())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler pupnagDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppYearEducationProcess.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler partitionTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FefuSchedulePartitionType.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler developCombinationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopCombination.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = super.query(alias, filter);
                filter = StringUtils.trimToNull(filter);
                if (filter != null)
                {
                    String space = " ";
                    filter = CoreStringUtils.escapeLike(filter, true);
                    builder.where(or(
                            likeUpper(
                                    DQLFunctions.concat(
                                            property(alias, DevelopCombination.developForm().shortTitle()),
                                            value(space),
                                            property(alias, DevelopCombination.developCondition().shortTitle()),
                                            value(space),
                                            DQLFunctions.coalesce(property(alias, DevelopCombination.developTech().shortTitle()), value("")),
                                            value(space),
                                            property(alias, DevelopCombination.developGrid().title())),
                                    value(filter)),
                            likeUpper(
                                    DQLFunctions.concat(
                                            property(alias, DevelopCombination.developForm().title()),
                                            value(space),
                                            property(alias, DevelopCombination.developCondition().title()),
                                            value(space),
                                            property(alias, DevelopCombination.developTech().title()),
                                            value(space),
                                            property(alias, DevelopCombination.developGrid().title())),
                                    value(filter))));
                }

                return builder;
            }
        };
    }
}