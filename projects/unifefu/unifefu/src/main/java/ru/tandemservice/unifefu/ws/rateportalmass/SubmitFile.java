/**
 * SubmitFile.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportalmass;

public interface SubmitFile extends java.rmi.Remote {
    public ru.tandemservice.unifefu.ws.rateportalmass.SubmitFileResponse submitFile(ru.tandemservice.unifefu.ws.rateportalmass.SubmitFileRequest submitFileParameters) throws java.rmi.RemoteException, ru.tandemservice.unifefu.ws.rateportalmass.SubmitFileFaultMsg;
}
