/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrgUnitAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniOrgUnit.logic.OrgUnitByKindComboDataSourceHandler;

/**
 * @author Dmitry Seleznev
 * @since 30.07.2013
 */
@Configuration
public class FefuSettingsOrgUnitAddEdit extends BusinessComponentManager
{
    public static final String ORDER_TYPE_DS = "orderTypeDS";
    public static final String ORG_UNIT_DS = "orgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ORDER_TYPE_DS, orderTypeDSHandler()))
                .addDataSource(this.selectDS(ORG_UNIT_DS, orgUnitComboDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler orderTypeDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitComboDSHandler()
    {
        return new OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING);
    }
}