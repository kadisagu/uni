/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow;

import java.util.Date;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 14.11.2012
 */
public interface IFefuOrderAutoCommitDao extends INeedPersistenceSupport
{
    /**
     * Проверяет валидность загружаемых из Directum'а данные перед синхронизацией в БД.
     *
     * @param orderDataList - список данных из СЭД Directum
     */
    void validateFefuDirectumOrderData(List<FefuDirectumOrderData> orderDataList);

    /**
     * Производит синхронизацию номеров и дат приказов с СЭД Directum на основе загружаемого файла.
     * Если выставлен атрибут "Провести приказы", то пытается так же провести все приказы,
     * идентификаторы которых содержатся в выгрузке из Directum'а.
     *
     * @param orderDataList - подготовленные к синхронизации данные о приказах (идентификатор, дата, номер),
     *                      полученные из СЭД Directum.
     * @param executor      - исполнитель, по факту - сотрудник, который загружает файл.
     *                      Администратор не наделён правом проведения приказов, поэтому все действия должны осуществляться только
     *                      от имени сотрудника.
     * @param formingDate   - Дата и время запуска синхронизации.
     * @param commit        - Провести приказы
     */
    void doSynchronizeOrders(List<FefuDirectumOrderData> orderDataList, EmployeePost executor, Date formingDate, boolean commit);

    FefuOrderAutoCommitLogRow doSynchronizeSingleOrderData(FefuDirectumOrderData orderData, Long transactionId, EmployeePost executor, String executorStr, Date formingDate, boolean commit, ProcessState state, int orderDataListSize, int currentValue);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    FefuOrderCommitError doCommitOrders(FefuOrderAutoCommitLogRow row, Long orderId, EmployeePost executor, ProcessState state, int orderDataListSize, int currentValue);
}