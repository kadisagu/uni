package ru.tandemservice.unifefu.base.ext.TrOrgUnit.ui.EduGroupList;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.FefuBrsPermittedEduPlanManager;
import ru.tandemservice.unifefu.entity.FefuBrsPermittedWorkPlan;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic.TrOrgUnitEduGroupDSHandler;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.EduGroupList.TrOrgUnitEduGroupList;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.EduGroupList.TrOrgUnitEduGroupListUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;
import ru.tandemservice.unitraining.brs.entity.catalogs.gen.TrBrsCoefficientOwnerTypeGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: amakarova
 */
public class TrOrgUnitEduGroupListExtUI extends UIAddon
{

    private JournalGenStatus status = JournalGenStatus.PERMITTED;

    public TrOrgUnitEduGroupListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        status = JournalGenStatus.PERMITTED;
    }

    public void onClickGroupsJournalGenFefu()
    {
        final Collection<IEntity> selected = ((CheckboxColumn) ((PageableSearchListDataSource) getPresenter().getConfig().getDataSource(TrOrgUnitEduGroupList.TR_ORGUNIT_EDU_GROUP_DS)).getLegacyDataSource().getColumn(TrOrgUnitEduGroupDSHandler.COLUMN_SELECT)).getSelectedObjects();
        if (CollectionUtils.isEmpty(selected))
            throw new ApplicationException("Не выбраны группы для формирования реализаций");

        checkGroupsYearPart(selected);

        EppYearPart yearPart = getGroupsYear(selected);
        Collection<Long> idsGroup = UniBaseDao.ids(selected);
        TrOrgUnitEduGroupListUI presenter = getPresenter();

        if (FefuBrsPermittedEduPlanManager.instance().dao().isDisableCreation())
        {
            String alias = "pwp";
            //list of EppWorkPlan id's only
            DQLSelectBuilder permittedWorkPlanList = new DQLSelectBuilder().fromEntity(FefuBrsPermittedWorkPlan.class, alias)
                    .column(property(alias, FefuBrsPermittedWorkPlan.L_WORK_PLAN));

            List<Long> permittedEduGroupsId = new ArrayList<>(idsGroup.size());
            alias = "egr";
            DQLExecutionContext dqlContext = new DQLExecutionContext(getSession());
            for (Long groupId : idsGroup)
            {
                final Long count = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, alias)
                        .column(property(alias, EppRealEduGroupRow.id()))
                        .where(eq(property(alias, EppRealEduGroupRow.group().id()), value(groupId)))
                        .where(in(property(alias, EppRealEduGroupRow.studentWpePart().studentWpe().sourceRow().workPlan().id()), permittedWorkPlanList.buildQuery()))
                        .createCountStatement(dqlContext).uniqueResult();
                if (count > 0)
                    permittedEduGroupsId.add(groupId);
            }

            if (permittedEduGroupsId.isEmpty())
            {
                status = JournalGenStatus.DISCARD;
                idsGroup = Collections.emptyList();
            } else
            {
                status = JournalGenStatus.PARTIAL;
                idsGroup = permittedEduGroupsId;
            }
        }

        for (Long id : idsGroup)
        {
            EppRealEduGroup4LoadType group = DataAccessServices.dao().get(id);
            boolean isCreate = false;
            List<String> groupTypesCodes = Lists.newArrayList();
            groupTypesCodes.add(EppGroupTypeALTCodes.TYPE_PRACTICE);
            //создаем сначала по практикам
            List<EppRealEduGroup4LoadType> groupsListPractice = getGroupsList(group, EppGroupTypeALTCodes.TYPE_PRACTICE, presenter.getSupport().getSession());
            if (groupsListPractice != null && groupsListPractice.size() > 0)
            {
                // предварительная проверка, что нет практик
                createJournals(yearPart, presenter.getSupport().getSession(), groupsListPractice, groupTypesCodes);
                isCreate = true;
            }
            if (!isCreate)
            {
                // по лабам
                List<EppRealEduGroup4LoadType> groupsListLabs = getGroupsList(group, EppGroupTypeALTCodes.TYPE_LABS, presenter.getSupport().getSession());
                groupTypesCodes.add(EppGroupTypeALTCodes.TYPE_LABS);
                if (groupsListLabs != null && groupsListLabs.size() > 0)
                {
                    // предварительная проверка, что нет практик и лаб
                    createJournals(yearPart, presenter.getSupport().getSession(), groupsListLabs, groupTypesCodes);
                    isCreate = true;
                }
            }
            if (!isCreate)
            {
                // по лекциям
                List<EppRealEduGroup4LoadType> groupsListLec = getGroupsList(group, EppGroupTypeALTCodes.TYPE_LECTURES, presenter.getSupport().getSession());
                groupTypesCodes.add(EppGroupTypeALTCodes.TYPE_LECTURES);
                if (groupsListLec != null && groupsListLec.size() > 0)
                    // предварительная проверка, что нет практик, лаб и лекций
                    createJournals(yearPart, presenter.getSupport().getSession(), groupsListLec, groupTypesCodes);
            }
        }
    }

    private void createJournals(EppYearPart yearPart, Session session, List<EppRealEduGroup4LoadType> groupList, List<String> groupTypes)
    {
        List<String> groupWithoutStudents = Lists.newArrayList();

        for (EppRealEduGroup group : groupList)
        {
            // есть ли реализации
            List<TrJournalGroup> trJournalGroups = new DQLSelectBuilder()
                    .fromEntity(TrJournalGroup.class, "j2g")
                    .where(eq(property(TrJournalGroup.group().fromAlias("j2g")), value(group)))
                    .where(in(property(TrJournalGroup.group().type().code().fromAlias("j2g")), groupTypes))
                    .createStatement(getSession()).list();
            if (trJournalGroups != null && trJournalGroups.size() > 0)
                continue;

            OrgUnit orgUnit;
            final List<EppRealEduGroupRow> studentList = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, "grp")
                    .column(property("grp"))
                    .where(eq(property(EppRealEduGroupRow.group().fromAlias("grp")), value(group)))
                    .createStatement(session).list();
            if (studentList != null && !studentList.isEmpty())
            {
                EppRealEduGroupRow student = studentList.get(0);
                orgUnit = student.getStudentEducationOrgUnit().getFormativeOrgUnit();

                final TrJournal journal = new TrJournal(group.getActivityPart(), group.getSummary().getYearPart());

                TrJournal newJournal = TrJournalManager.instance().dao().doCreateTrJournal(journal, true);
                TrJournalManager.instance().dao().doLinkEduGroups(journal, Collections.singletonList(group));
                doSaveCoeff(newJournal, orgUnit, yearPart);
            } else
                groupWithoutStudents.add(group.getTitle());
        }

        if (!groupWithoutStudents.isEmpty())
        {
            InfoCollector info = ContextLocal.getInfoCollector();
            info.add("В группе «" + StringUtils.join(groupWithoutStudents, "», «") + "» нет студентов.");
        }
    }

    /**
     * Проверяет что все УГС на одну Часть года.
     *
     * @param groupList список УГС
     */
    protected void checkGroupsYearPart(Collection<IEntity> groupList)
    {
        if (groupList.isEmpty())
            return;

        final List<EppRealEduGroup> list = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(EppRealEduGroup.class, "grp").column(property("grp"))
                        .where(in(property(EppRealEduGroup.id().fromAlias("grp")), UniBaseDao.ids(groupList))));

        final EppYearPart yearPart = list.get(0).getSummary().getYearPart();
        for (EppRealEduGroup group : list)
        {
            if (!yearPart.equals(group.getSummary().getYearPart()))
                throw new ApplicationException(TrOrgUnitManager.instance().getProperty("groupsJournalGen.error.different-eppYearPart"));
        }
    }

    protected EppYearPart getGroupsYear(final Collection<IEntity> groupList)
    {
        if (groupList.isEmpty())
            return null;

        return DataAccessServices.dao().getCalculatedValue(session -> new DQLSelectBuilder().fromEntity(EppRealEduGroup.class, "grp")
                .top(1)
                .column(property("grp", EppRealEduGroup.summary().yearPart()))
                .where(in(property(EppRealEduGroup.id().fromAlias("grp")), UniBaseDao.ids(groupList)))
                .createStatement(session).uniqueResult());
    }

    private void doSaveCoeff(TrJournal trJournal, OrgUnit orgUnit, EppYearPart yearPart)
    {
        final TrOrgUnitSettings globalSettings = DataAccessServices.dao().getByNaturalId(new TrOrgUnitSettings.NaturalId(orgUnit, yearPart));

        Map<String, TrBrsCoefficientValue> values = new HashMap<>();
        for (TrBrsCoefficientValue value : DataAccessServices.dao().getList(TrBrsCoefficientValue.class, TrBrsCoefficientValue.owner().s(), globalSettings))
        {
            values.put(value.getDefinition().getUserCode(), value);
        }
        HashMap<TrBrsCoefficientDef, Object> newValues = new HashMap<>();

        if (values.size() > 0)
        {
            for (TrBrsCoefficientDef def : TrBrsCoefficientManager.instance().brsDao().getFilteredCoefficientDefList(trJournal, getOwnerType()))
            {
                if (def.getUserCode().equals(FefuBrs.RANGE_SETOFF_CODE))
                    newValues.put(def, values.get(FefuBrs.RANGE_SETOFF_DEF_CODE).getValue());
                if (def.getUserCode().equals(FefuBrs.RANGE_5_CODE))
                    newValues.put(def, values.get(FefuBrs.RANGE_5_DEF_CODE).getValue());
                if (def.getUserCode().equals(FefuBrs.RANGE_4_CODE))
                    newValues.put(def, values.get(FefuBrs.RANGE_4_DEF_CODE).getValue());
                if (def.getUserCode().equals(FefuBrs.RANGE_3_CODE))
                    newValues.put(def, values.get(FefuBrs.RANGE_3_DEF_CODE).getValue());
            }
            TrBrsCoefficientManager.instance().coefDao().doSaveCoefficients(trJournal, newValues);
        }
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return DataAccessServices.dao().getByNaturalId(new TrBrsCoefficientOwnerTypeGen.NaturalId(TrBrsCoefficientOwnerTypeCodes.JOURNAL));
    }

    private List<EppRealEduGroup4LoadType> getGroupsList(EppRealEduGroup4LoadType group4LoadType, String groupType, Session session)
    {
        DQLSelectBuilder grBuilder = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadType.class, "grp").column(property("grp"))
                .where(eq(property(EppRealEduGroup4LoadType.type().code().fromAlias("grp")), value(groupType)))
                .where(eq(property(EppRealEduGroup4LoadType.summary().fromAlias("grp")), value(group4LoadType.getSummary())))
                .where(eq(property(EppRealEduGroup4LoadType.activityPart().fromAlias("grp")), value(group4LoadType.getActivityPart())))
                .order(property(EppRealEduGroup4LoadType.type().priority().fromAlias("grp")))
                .order(property(EppRealEduGroup4LoadType.title().fromAlias("grp")));
        return grBuilder.createStatement(session).list();
    }

    public JournalGenStatus getStatus()
    {
        return status;
    }

    private enum JournalGenStatus
    {
        PERMITTED(""),
        PARTIAL("Реализации сформированы только по разрешенным учебным планам"),
        DISCARD("Формирование реализаций заблокировано"),;

        private final String message;

        JournalGenStatus(String message)
        {
            this.message = message;
        }

        public String getMessage()
        {
            return message;
        }

        public boolean isPermitted()
        {
            return this.equals(PARTIAL) || this.equals(DISCARD);
        }

    }
}
