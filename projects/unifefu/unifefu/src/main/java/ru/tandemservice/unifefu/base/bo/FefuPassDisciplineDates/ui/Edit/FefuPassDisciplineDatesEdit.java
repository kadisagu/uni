/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPassDisciplineDates.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.transformer.BaseOutputTransformer;
import org.tandemframework.caf.logic.transformer.IOutputTransformer;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.unifefu.base.bo.FefuPassDisciplineDates.logic.DisciplineRowWrapper;
import ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 14.06.2013
 */
@Configuration
public class FefuPassDisciplineDatesEdit extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String SUBJECT_PASS_FORM_DS = "subjectPassFormDS";
    public static final String TECHNICAL_COMMISSIONS_DS = "technicalCommissionsDS";
    public static final String PASS_DISCIPLINE_DS = "passDisciplineDS";

    public static final String ENROLLMENT_CAMPAIGN_PARAM = "enrollmentCampaign";
    public static final String SUBJECT_PASS_FORM_PARAM = "subjectPassForm";
    public static final String DEVELOP_FORM_PARAM = "developForm";
    public static final String TECHNICAL_COMMISSION_PARAM = "technicalCommission";

    private static final String DISCIPLINE_COLUMN = "discipline";
    private static final String PASS_DATE_COLUMN = "passDate";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(selectDS(SUBJECT_PASS_FORM_DS, subjectPassFormDSHandler()))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(selectDS(TECHNICAL_COMMISSIONS_DS, fefuTechnicalCommissionsDSHandler()))
                .addDataSource(searchListDS(PASS_DISCIPLINE_DS, getPassDisciplineDS(), passDisciplineDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getPassDisciplineDS()
    {
        return columnListExtPointBuilder(PASS_DISCIPLINE_DS)
                .addColumn(textColumn(DISCIPLINE_COLUMN, "title"))
                .addColumn(blockColumn(PASS_DATE_COLUMN, "passDateBlockColumn").width("150px"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> passDisciplineDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<DataWrapper> resultList = new ArrayList<>();
                EnrollmentCampaign enrollmentCampaign = context.get(ENROLLMENT_CAMPAIGN_PARAM);
                SubjectPassForm subjectPassForm = context.get(SUBJECT_PASS_FORM_PARAM);
                DevelopForm developForm = context.get(DEVELOP_FORM_PARAM);
                FefuTechnicalCommission technicalCommission = context.get(TECHNICAL_COMMISSION_PARAM);

                if (enrollmentCampaign != null && subjectPassForm != null && developForm != null && technicalCommission != null)
                {
                    // Получаем список дисциплин
                    List<Discipline2RealizationWayRelation> disciplines = new DQLSelectBuilder().fromEntity(Discipline2RealizationWayRelation.class, "e")
                            .where(eq(property(Discipline2RealizationWayRelation.enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)))
                            .order(property(Discipline2RealizationWayRelation.educationSubject().title().fromAlias("e")))
                            .createStatement(context.getSession()).list();

                    // Получаем имеющиеся настройки
                    List<FefuDisciplineDateSetting> settings =
                            new DQLSelectBuilder().fromEntity(FefuDisciplineDateSetting.class, "e").column("e")
                                    .where(and(eq(property(FefuDisciplineDateSetting.discipline().enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)),
                                               eq(property(FefuDisciplineDateSetting.subjectPassForm().fromAlias("e")), value(subjectPassForm)),
                                               eq(property(FefuDisciplineDateSetting.developForm().fromAlias("e")), value(developForm)),
                                               eq(property(FefuDisciplineDateSetting.technicalCommission().fromAlias("e")), value(technicalCommission))))
                                    .createStatement(context.getSession()).list();

                    for (Discipline2RealizationWayRelation discipline : disciplines)
                    {
                        DisciplineRowWrapper row = new DisciplineRowWrapper();
                        row.setDiscipline2RealizationWayRelation(discipline);
                        for (FefuDisciplineDateSetting setting : settings)
                        {
                            if (setting.getDiscipline().getId().equals(discipline.getId()))
                            {
                                row.setFefuDisciplineDateSetting(setting);
                                settings.remove(setting);
                                break;
                            }
                        }

                        DataWrapper rec = new DataWrapper(discipline.getId(), discipline.getTitle(), row);
                        resultList.add(rec);
                    }
                }

                DSOutput output = ListOutputBuilder.get(input, resultList).pageable(false).build();
                output.setCountRecord(Math.max(resultList.size(), 3));
                return output;
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> subjectPassFormDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), SubjectPassForm.class)
                .where(SubjectPassForm.internal(), true);
        handler.setPageable(false);
        handler.outputTransformer(catalogCodeOrderDSTransformer());
        return handler;
    }

    @Bean
    public IOutputTransformer<DSInput, DSOutput> catalogCodeOrderDSTransformer()
    {
        // Сортировка по коду элемента справочника
        return new BaseOutputTransformer<DSInput, DSOutput>("catalogCodeOrderDSTransformer", getName())
        {
            @Override
            public void transformOutput(DSInput dsInput, DSOutput result, ExecutionContext context)
            {
                Collections.sort(result.<ICatalogItem>getRecordList(), CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> fefuTechnicalCommissionsDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), FefuTechnicalCommission.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(eq(property(FefuTechnicalCommission.enrollmentCampaign().fromAlias(alias)), commonValue(context.get(ENROLLMENT_CAMPAIGN_PARAM))));
            }
        };
        handler.setPageable(false);
        handler.order(FefuTechnicalCommission.title());
        return handler;
    }

}