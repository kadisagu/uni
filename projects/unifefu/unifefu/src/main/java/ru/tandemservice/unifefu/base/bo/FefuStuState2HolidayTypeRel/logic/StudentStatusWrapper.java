/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuStuState2HolidayTypeRel.logic;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import javax.annotation.Nullable;

/**
 * @author DMITRY KNYAZEV
 * @since 06.10.2014
 */
public class StudentStatusWrapper extends ViewWrapper<StudentStatus>
{
	public static final String CONSIDER = "consider";


	public StudentStatusWrapper(StudentStatus studentStatus, @Nullable Boolean consider)
	{
		super(studentStatus);
		this.setViewProperty(CONSIDER, consider != null);
	}
}
