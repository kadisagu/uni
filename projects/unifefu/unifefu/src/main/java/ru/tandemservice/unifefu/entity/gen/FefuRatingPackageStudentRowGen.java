package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow;
import ru.tandemservice.unifefu.entity.FefuSendingRatingPackage;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Студент, включенный в пакет отправки рейтингов на портал
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuRatingPackageStudentRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow";
    public static final String ENTITY_NAME = "fefuRatingPackageStudentRow";
    public static final int VERSION_HASH = -447161493;
    private static IEntityMeta ENTITY_META;

    public static final String L_RATING_PACKAGE = "ratingPackage";
    public static final String L_STUDENT = "student";
    public static final String L_NSI_ID = "nsiId";
    public static final String P_FIO = "fio";
    public static final String P_GROUP_NUMBER = "groupNumber";

    private FefuSendingRatingPackage _ratingPackage;     // Пакет, в котором отправлены данные
    private Student _student;     // Студент
    private FefuNsiIds _nsiId;     // Идентификатор НСИ
    private String _fio;     // ФИО студента на момент отправки данных
    private String _groupNumber;     // Номер группы студента на момент отправки данных

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Пакет, в котором отправлены данные. Свойство не может быть null.
     */
    @NotNull
    public FefuSendingRatingPackage getRatingPackage()
    {
        return _ratingPackage;
    }

    /**
     * @param ratingPackage Пакет, в котором отправлены данные. Свойство не может быть null.
     */
    public void setRatingPackage(FefuSendingRatingPackage ratingPackage)
    {
        dirty(_ratingPackage, ratingPackage);
        _ratingPackage = ratingPackage;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Идентификатор НСИ. Свойство не может быть null.
     */
    @NotNull
    public FefuNsiIds getNsiId()
    {
        return _nsiId;
    }

    /**
     * @param nsiId Идентификатор НСИ. Свойство не может быть null.
     */
    public void setNsiId(FefuNsiIds nsiId)
    {
        dirty(_nsiId, nsiId);
        _nsiId = nsiId;
    }

    /**
     * @return ФИО студента на момент отправки данных. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFio()
    {
        return _fio;
    }

    /**
     * @param fio ФИО студента на момент отправки данных. Свойство не может быть null.
     */
    public void setFio(String fio)
    {
        dirty(_fio, fio);
        _fio = fio;
    }

    /**
     * @return Номер группы студента на момент отправки данных.
     */
    @Length(max=255)
    public String getGroupNumber()
    {
        return _groupNumber;
    }

    /**
     * @param groupNumber Номер группы студента на момент отправки данных.
     */
    public void setGroupNumber(String groupNumber)
    {
        dirty(_groupNumber, groupNumber);
        _groupNumber = groupNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuRatingPackageStudentRowGen)
        {
            setRatingPackage(((FefuRatingPackageStudentRow)another).getRatingPackage());
            setStudent(((FefuRatingPackageStudentRow)another).getStudent());
            setNsiId(((FefuRatingPackageStudentRow)another).getNsiId());
            setFio(((FefuRatingPackageStudentRow)another).getFio());
            setGroupNumber(((FefuRatingPackageStudentRow)another).getGroupNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuRatingPackageStudentRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuRatingPackageStudentRow.class;
        }

        public T newInstance()
        {
            return (T) new FefuRatingPackageStudentRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "ratingPackage":
                    return obj.getRatingPackage();
                case "student":
                    return obj.getStudent();
                case "nsiId":
                    return obj.getNsiId();
                case "fio":
                    return obj.getFio();
                case "groupNumber":
                    return obj.getGroupNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "ratingPackage":
                    obj.setRatingPackage((FefuSendingRatingPackage) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "nsiId":
                    obj.setNsiId((FefuNsiIds) value);
                    return;
                case "fio":
                    obj.setFio((String) value);
                    return;
                case "groupNumber":
                    obj.setGroupNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "ratingPackage":
                        return true;
                case "student":
                        return true;
                case "nsiId":
                        return true;
                case "fio":
                        return true;
                case "groupNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "ratingPackage":
                    return true;
                case "student":
                    return true;
                case "nsiId":
                    return true;
                case "fio":
                    return true;
                case "groupNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "ratingPackage":
                    return FefuSendingRatingPackage.class;
                case "student":
                    return Student.class;
                case "nsiId":
                    return FefuNsiIds.class;
                case "fio":
                    return String.class;
                case "groupNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuRatingPackageStudentRow> _dslPath = new Path<FefuRatingPackageStudentRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuRatingPackageStudentRow");
    }
            

    /**
     * @return Пакет, в котором отправлены данные. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow#getRatingPackage()
     */
    public static FefuSendingRatingPackage.Path<FefuSendingRatingPackage> ratingPackage()
    {
        return _dslPath.ratingPackage();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Идентификатор НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow#getNsiId()
     */
    public static FefuNsiIds.Path<FefuNsiIds> nsiId()
    {
        return _dslPath.nsiId();
    }

    /**
     * @return ФИО студента на момент отправки данных. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow#getFio()
     */
    public static PropertyPath<String> fio()
    {
        return _dslPath.fio();
    }

    /**
     * @return Номер группы студента на момент отправки данных.
     * @see ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow#getGroupNumber()
     */
    public static PropertyPath<String> groupNumber()
    {
        return _dslPath.groupNumber();
    }

    public static class Path<E extends FefuRatingPackageStudentRow> extends EntityPath<E>
    {
        private FefuSendingRatingPackage.Path<FefuSendingRatingPackage> _ratingPackage;
        private Student.Path<Student> _student;
        private FefuNsiIds.Path<FefuNsiIds> _nsiId;
        private PropertyPath<String> _fio;
        private PropertyPath<String> _groupNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Пакет, в котором отправлены данные. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow#getRatingPackage()
     */
        public FefuSendingRatingPackage.Path<FefuSendingRatingPackage> ratingPackage()
        {
            if(_ratingPackage == null )
                _ratingPackage = new FefuSendingRatingPackage.Path<FefuSendingRatingPackage>(L_RATING_PACKAGE, this);
            return _ratingPackage;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Идентификатор НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow#getNsiId()
     */
        public FefuNsiIds.Path<FefuNsiIds> nsiId()
        {
            if(_nsiId == null )
                _nsiId = new FefuNsiIds.Path<FefuNsiIds>(L_NSI_ID, this);
            return _nsiId;
        }

    /**
     * @return ФИО студента на момент отправки данных. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow#getFio()
     */
        public PropertyPath<String> fio()
        {
            if(_fio == null )
                _fio = new PropertyPath<String>(FefuRatingPackageStudentRowGen.P_FIO, this);
            return _fio;
        }

    /**
     * @return Номер группы студента на момент отправки данных.
     * @see ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow#getGroupNumber()
     */
        public PropertyPath<String> groupNumber()
        {
            if(_groupNumber == null )
                _groupNumber = new PropertyPath<String>(FefuRatingPackageStudentRowGen.P_GROUP_NUMBER, this);
            return _groupNumber;
        }

        public Class getEntityClass()
        {
            return FefuRatingPackageStudentRow.class;
        }

        public String getEntityName()
        {
            return "fefuRatingPackageStudentRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
