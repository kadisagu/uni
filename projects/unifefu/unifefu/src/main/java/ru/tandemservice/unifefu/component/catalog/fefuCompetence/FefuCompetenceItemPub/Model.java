/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.catalog.fefuCompetence.FefuCompetenceItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubModel;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class Model extends DefaultCatalogItemPubModel<FefuCompetence>
{
}