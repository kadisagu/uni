/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e66.AddEdit;

import ru.tandemservice.movestudent.utils.GroupSelectModel;

/**
 * @author Alexey Lopatin
 * @since 14.11.2013
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e66.AddEdit.DAO
{
    @Override
    public void prepare(final ru.tandemservice.movestudent.component.modularextract.e66.AddEdit.Model model)
    {
        super.prepare(model);
        model.getEduModel().setGroupModel(
                new GroupSelectModel(model.getEduModel(), (Model) model)
                        .dependFromFUTS(true)
                        .extModel((Model) model)
                        .showChildLevels(true)
        );
    }
}
