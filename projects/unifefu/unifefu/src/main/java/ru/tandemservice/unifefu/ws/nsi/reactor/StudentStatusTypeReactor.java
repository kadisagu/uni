/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.StudentStatusType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class StudentStatusTypeReactor extends SimpleCatalogEntityNewReactor<StudentStatusType, StudentStatus>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<StudentStatus> getEntityClass()
    {
        return StudentStatus.class;
    }

    @Override
    public Class<StudentStatusType> getNSIEntityClass()
    {
        return StudentStatusType.class;
    }

    @Override
    public StudentStatusType getCatalogElementRetrieveRequestObject(String guid)
    {
        StudentStatusType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createStudentStatusType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public StudentStatusType createEntity(CoreCollectionUtils.Pair<StudentStatus, FefuNsiIds> entityPair)
    {
        StudentStatusType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createStudentStatusType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setStudentStatusID(entityPair.getX().getCode());
        nsiEntity.setStudentStatusName(entityPair.getX().getTitle());
        nsiEntity.setStudentStatusNameShort(entityPair.getX().getShortTitle());
        nsiEntity.setStudentStatusActive(entityPair.getX().isActive() ? "1" : "0");
        nsiEntity.setStudentStatusUsedinSystem(entityPair.getX().isUsedInSystem() ? "1" : "0");
        nsiEntity.setStudentStatusPriority(String.valueOf(entityPair.getX().getPriority()));
        return nsiEntity;
    }

    @Override
    public StudentStatus createEntity(StudentStatusType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getStudentStatusName()) return null;

        StudentStatus entity = new StudentStatus();
        entity.setTitle(nsiEntity.getStudentStatusName());
        if (null != StringUtils.trimToNull(nsiEntity.getStudentStatusNameShort()))
            entity.setShortTitle(nsiEntity.getStudentStatusNameShort());
        else entity.setShortTitle(nsiEntity.getStudentStatusName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(StudentStatus.class));
        if (null != nsiEntity.getStudentStatusActive())
            entity.setActive("1".equals(nsiEntity.getStudentStatusActive()) ? Boolean.TRUE : Boolean.FALSE);
        if (null != nsiEntity.getStudentStatusUsedinSystem())
            entity.setUsedInSystem("1".equals(nsiEntity.getStudentStatusUsedinSystem()) ? Boolean.TRUE : Boolean.FALSE);
        if (null != nsiEntity.getStudentStatusPriority())
            entity.setPriority(Integer.parseInt(nsiEntity.getStudentStatusPriority()));

        return entity;
    }

    @Override
    public StudentStatus updatePossibleDuplicateFields(StudentStatusType nsiEntity, CoreCollectionUtils.Pair<StudentStatus, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), StudentStatus.title().s()))
                entityPair.getX().setTitle(nsiEntity.getStudentStatusName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), StudentStatus.shortTitle().s()))
                entityPair.getX().setShortTitle(null != StringUtils.trimToNull(nsiEntity.getStudentStatusNameShort()) ? nsiEntity.getStudentStatusNameShort() : nsiEntity.getStudentStatusName());
        }
        return entityPair.getX();
    }

    @Override
    public StudentStatusType updateNsiEntityFields(StudentStatusType nsiEntity, StudentStatus entity)
    {
        nsiEntity.setStudentStatusID(entity.getCode());
        nsiEntity.setStudentStatusName(entity.getTitle());
        nsiEntity.setStudentStatusNameShort(entity.getShortTitle());
        nsiEntity.setStudentStatusActive(entity.isActive() ? "1" : "0");
        nsiEntity.setStudentStatusUsedinSystem(entity.isUsedInSystem() ? "1" : "0");
        nsiEntity.setStudentStatusPriority(String.valueOf(entity.getPriority()));
        return nsiEntity;
    }
}