/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow;

/**
 * @author Dmitry Seleznev
 * @since 15.11.2012
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "syncRow.id")
})
public class FefuOrderAutoCommitViewUI extends UIPresenter
{
    private FefuOrderAutoCommitLogRow _syncRow = new FefuOrderAutoCommitLogRow();

    @Override
    public void onComponentRefresh()
    {
        _syncRow = DataAccessServices.dao().getNotNull(_syncRow.getId());
    }

    // Getters & Setters

    public FefuOrderAutoCommitLogRow getSyncRow()
    {
        return _syncRow;
    }

    public void setSyncRow(FefuOrderAutoCommitLogRow syncRow)
    {
        _syncRow = syncRow;
    }
}