/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.System;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.unifefu.base.bo.Directum.logic.DirectumDAO;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiDaemonDao;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;

import java.io.File;

/**
 * @author Dmitry Seleznev
 * @since 26.09.2014
 */
public class FefuSettingsSystemUI extends UIPresenter
{
    // nsi
    private DataWrapper _nsiEnable;
    private String _nsiUrl;
    private DataWrapper _nsiAuthEnable;
    private String _nsiAuthUser;
    private String _nsiAuthPass1;
    private String _nsiAuthPass2;
    private DataWrapper _nsiIgnoreIdentityCardData;

    // directum
    private DataWrapper _directumEnable;
    private String _directumWSUrl;
    private String _directumBaseUrl;

    @Override
    public void onComponentRefresh()
    {
        DataWrapper yesOption = new DataWrapper(FefuSettingsSystem.YES_ID, "да");
        DataWrapper notOption = new DataWrapper(FefuSettingsSystem.NO_ID, "нет");

        // nsi
        String autoSyncEnabledStr = ApplicationRuntime.getProperty(FefuNsiDaemonDao.NSI_SERVICE_AUTOSYNC);
        _nsiEnable = (null != autoSyncEnabledStr && Boolean.parseBoolean(autoSyncEnabledStr)) ? yesOption : notOption;
        _nsiUrl = ApplicationRuntime.getProperty(FefuNsiSyncDAO.NSI_SERVICE_URL);

        String authEnabledStr = ApplicationRuntime.getProperty(NsiReactorUtils.NSI_SERVICE_AUTH_ENABLED);
        _nsiAuthEnable = (null != authEnabledStr && Boolean.parseBoolean(authEnabledStr)) ? yesOption : notOption;
        _nsiAuthUser = ApplicationRuntime.getProperty(NsiReactorUtils.NSI_SERVICE_AUTH_USERNAME);
        _nsiAuthPass1 = ApplicationRuntime.getProperty(NsiReactorUtils.NSI_SERVICE_AUTH_PASSWORD);
        _nsiAuthPass2 = ApplicationRuntime.getProperty(NsiReactorUtils.NSI_SERVICE_AUTH_PASSWORD);

        String ignoreIdentityCardStr = ApplicationRuntime.getProperty(NsiReactorUtils.NSI_IGNORE_IDENTITY_CARD);
        _nsiIgnoreIdentityCardData = (null != ignoreIdentityCardStr && Boolean.parseBoolean(ignoreIdentityCardStr)) ? yesOption : notOption;

        // directum
        _directumWSUrl = ApplicationRuntime.getProperty(DirectumDAO.DIRECTUM_SERVICE_URL_PROPERTY);
        _directumBaseUrl = ApplicationRuntime.getProperty(DirectumDAO.DIRECTUM_BASE_URL_PROPERTY);
        _directumEnable = StringUtils.isEmpty(_directumWSUrl) ? notOption : yesOption;
    }

    // Getters & Setters

    public DataWrapper getNsiEnable()
    {
        return _nsiEnable;
    }

    public void setNsiEnable(DataWrapper nsiEnable)
    {
        _nsiEnable = nsiEnable;
    }

    public String getNsiUrl()
    {
        return _nsiUrl;
    }

    public void setNsiUrl(String nsiUrl)
    {
        _nsiUrl = nsiUrl;
    }

    public DataWrapper getNsiAuthEnable()
    {
        return _nsiAuthEnable;
    }

    public void setNsiAuthEnable(DataWrapper nsiAuthEnable)
    {
        _nsiAuthEnable = nsiAuthEnable;
    }

    public String getNsiAuthUser()
    {
        return _nsiAuthUser;
    }

    public void setNsiAuthUser(String nsiAuthUser)
    {
        _nsiAuthUser = nsiAuthUser;
    }

    public String getNsiAuthPass1()
    {
        return _nsiAuthPass1;
    }

    public void setNsiAuthPass1(String nsiAuthPass1)
    {
        _nsiAuthPass1 = nsiAuthPass1;
    }

    public String getNsiAuthPass2()
    {
        return _nsiAuthPass2;
    }

    public void setNsiAuthPass2(String nsiAuthPass2)
    {
        _nsiAuthPass2 = nsiAuthPass2;
    }

    public DataWrapper getNsiIgnoreIdentityCardData()
    {
        return _nsiIgnoreIdentityCardData;
    }

    public void setNsiIgnoreIdentityCardData(DataWrapper nsiIgnoreIdentityCardData)
    {
        _nsiIgnoreIdentityCardData = nsiIgnoreIdentityCardData;
    }

    public DataWrapper getDirectumEnable()
    {
        return _directumEnable;
    }

    public void setDirectumEnable(DataWrapper directumEnable)
    {
        _directumEnable = directumEnable;
    }

    public String getDirectumWSUrl()
    {
        return _directumWSUrl;
    }

    public void setDirectumWSUrl(String directumWSUrl)
    {
        _directumWSUrl = directumWSUrl;
    }

    public String getDirectumBaseUrl()
    {
        return _directumBaseUrl;
    }

    public void setDirectumBaseUrl(String directumBaseUrl)
    {
        _directumBaseUrl = directumBaseUrl;
    }


    // Utils

    public boolean isShowNSI()
    {
        return _nsiEnable.getId().equals(FefuSettingsSystem.YES_ID);
    }

    public boolean isShowNSIAuth()
    {
        return _nsiAuthEnable.getId().equals(FefuSettingsSystem.YES_ID);
    }

    public boolean isShowDirectum()
    {
        return _directumEnable.getId().equals(FefuSettingsSystem.YES_ID);
    }


    // Listeners

    public void onClickApply() throws Exception
    {
        PropertiesConfiguration config = new PropertiesConfiguration(new File(ApplicationRuntime.getAppConfigPath(), "app.properties"));

        // ws
        boolean nsiEnable = _nsiEnable.getId().equals(FefuSettingsSystem.YES_ID);
        if (nsiEnable)
        {
            _nsiUrl = StringUtils.trimToEmpty(_nsiUrl);
            _nsiAuthUser = StringUtils.trimToEmpty(_nsiAuthUser);
            _nsiAuthPass1 = StringUtils.trimToEmpty(_nsiAuthPass1);
            _nsiAuthPass2 = StringUtils.trimToEmpty(_nsiAuthPass2);

            if (!_nsiAuthPass1.equals(_nsiAuthPass2))
            {
                ContextLocal.getErrorCollector().add("Пароли, указанные для аутентификации в IIS на стенде НСИ, не совпадают.", "nsiAuthPass1", "nsiAuthPass2");
                return;
            }

            config.setProperty(FefuNsiDaemonDao.NSI_SERVICE_AUTOSYNC, Boolean.TRUE.toString());
            config.setProperty(FefuNsiSyncDAO.NSI_SERVICE_URL, _nsiUrl);
            ApplicationRuntime.getProperties().setProperty(FefuNsiDaemonDao.NSI_SERVICE_AUTOSYNC, Boolean.TRUE.toString());
            ApplicationRuntime.getProperties().setProperty(FefuNsiSyncDAO.NSI_SERVICE_URL, _nsiUrl);

            boolean nsiAuthEnable = _nsiAuthEnable.getId().equals(FefuSettingsSystem.YES_ID);
            if (nsiAuthEnable)
            {
                config.setProperty(NsiReactorUtils.NSI_SERVICE_AUTH_ENABLED, Boolean.TRUE.toString());
                config.setProperty(NsiReactorUtils.NSI_SERVICE_AUTH_USERNAME, _nsiAuthUser);
                config.setProperty(NsiReactorUtils.NSI_SERVICE_AUTH_PASSWORD, _nsiAuthPass1);
                ApplicationRuntime.getProperties().setProperty(NsiReactorUtils.NSI_SERVICE_AUTH_ENABLED, Boolean.TRUE.toString());
                ApplicationRuntime.getProperties().setProperty(NsiReactorUtils.NSI_SERVICE_AUTH_USERNAME, _nsiAuthUser);
                ApplicationRuntime.getProperties().setProperty(NsiReactorUtils.NSI_SERVICE_AUTH_PASSWORD, _nsiAuthPass1);
            } else
            {
                config.clearProperty(NsiReactorUtils.NSI_SERVICE_AUTH_ENABLED);
                config.clearProperty(NsiReactorUtils.NSI_SERVICE_AUTH_USERNAME);
                config.clearProperty(NsiReactorUtils.NSI_SERVICE_AUTH_PASSWORD);
                ApplicationRuntime.getProperties().remove(NsiReactorUtils.NSI_SERVICE_AUTH_ENABLED);
                ApplicationRuntime.getProperties().remove(NsiReactorUtils.NSI_SERVICE_AUTH_USERNAME);
                ApplicationRuntime.getProperties().remove(NsiReactorUtils.NSI_SERVICE_AUTH_PASSWORD);
            }

            boolean nsiIgnoreIdentityCardData = _nsiIgnoreIdentityCardData.getId().equals(FefuSettingsSystem.YES_ID);
            if(nsiIgnoreIdentityCardData)
            {
                config.setProperty(NsiReactorUtils.NSI_IGNORE_IDENTITY_CARD, Boolean.TRUE.toString());
                ApplicationRuntime.getProperties().setProperty(NsiReactorUtils.NSI_IGNORE_IDENTITY_CARD, Boolean.TRUE.toString());
            }
            else
            {
                config.clearProperty(NsiReactorUtils.NSI_IGNORE_IDENTITY_CARD);
                ApplicationRuntime.getProperties().remove(NsiReactorUtils.NSI_IGNORE_IDENTITY_CARD);
            }

            config.getLayout().setBlancLinesBefore(FefuNsiDaemonDao.NSI_SERVICE_AUTOSYNC, 1);
            config.getLayout().setComment(FefuNsiDaemonDao.NSI_SERVICE_AUTOSYNC, "nsi settings");
        } else
        {
            config.clearProperty(FefuNsiDaemonDao.NSI_SERVICE_AUTOSYNC);
            config.clearProperty(FefuNsiSyncDAO.NSI_SERVICE_URL);
            config.clearProperty(NsiReactorUtils.NSI_SERVICE_AUTH_ENABLED);
            config.clearProperty(NsiReactorUtils.NSI_SERVICE_AUTH_USERNAME);
            config.clearProperty(NsiReactorUtils.NSI_SERVICE_AUTH_PASSWORD);
            ApplicationRuntime.getProperties().remove(FefuNsiDaemonDao.NSI_SERVICE_AUTOSYNC);
            ApplicationRuntime.getProperties().remove(FefuNsiSyncDAO.NSI_SERVICE_URL);
            ApplicationRuntime.getProperties().remove(NsiReactorUtils.NSI_SERVICE_AUTH_ENABLED);
            ApplicationRuntime.getProperties().remove(NsiReactorUtils.NSI_SERVICE_AUTH_USERNAME);
            ApplicationRuntime.getProperties().remove(NsiReactorUtils.NSI_SERVICE_AUTH_PASSWORD);
        }

        boolean directumEnable = _directumEnable.getId().equals(FefuSettingsSystem.YES_ID);
        if (directumEnable)
        {
            _directumWSUrl = StringUtils.trimToEmpty(_directumWSUrl);
            _directumBaseUrl = StringUtils.trimToEmpty(_directumBaseUrl);

            config.setProperty(DirectumDAO.DIRECTUM_SERVICE_URL_PROPERTY, _directumWSUrl);
            config.setProperty(DirectumDAO.DIRECTUM_BASE_URL_PROPERTY, _directumBaseUrl);
            ApplicationRuntime.getProperties().setProperty(DirectumDAO.DIRECTUM_SERVICE_URL_PROPERTY, _directumWSUrl);
            ApplicationRuntime.getProperties().setProperty(DirectumDAO.DIRECTUM_BASE_URL_PROPERTY, _directumBaseUrl);

            config.getLayout().setBlancLinesBefore(DirectumDAO.DIRECTUM_SERVICE_URL_PROPERTY, 1);
            config.getLayout().setComment(DirectumDAO.DIRECTUM_SERVICE_URL_PROPERTY, "directum settings");
        } else
        {
            config.clearProperty(DirectumDAO.DIRECTUM_SERVICE_URL_PROPERTY);
            config.clearProperty(DirectumDAO.DIRECTUM_BASE_URL_PROPERTY);
            ApplicationRuntime.getProperties().remove(DirectumDAO.DIRECTUM_SERVICE_URL_PROPERTY);
            ApplicationRuntime.getProperties().remove(DirectumDAO.DIRECTUM_BASE_URL_PROPERTY);
        }

        config.save();
    }
}