package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic;

import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;

public interface IApeProgramForStudentDao
{
    FefuAdditionalProfessionalEducationProgramForStudent getApeProgramForStudentByStudentId(Long studentId);
}
