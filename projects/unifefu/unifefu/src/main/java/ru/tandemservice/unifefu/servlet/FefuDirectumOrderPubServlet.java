/* $Id: FefuDirectumOrderPubServlet.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.servlet;

import com.google.common.collect.Maps;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.core.CoreServices;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.render.RichTableListRenderUtils;
import ru.tandemservice.unifefu.base.bo.Directum.DirectumManager;
import ru.tandemservice.unifefu.base.bo.Directum.ui.ErrorPage.DirectumErrorPage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * @author: Dmitry Seleznev
 * @since: 05.03.2014
 */
public class FefuDirectumOrderPubServlet extends HttpServlet
{
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        // http://localhost:8080/beanservlet/FefuOrder?orderId=115
        // http://localhost:8080/beanservlet/FefuOrder?orderId=868321

        String directumOrderId = req.getParameter("orderId");
        Long orderId = DirectumManager.instance().dao().getOrderIdByDirectumOrderId(directumOrderId);

        if (null == orderId || null == orderId)
        {
            goToErrorPage(resp);
            return;
        }

        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put(IUIPresenter.PUBLISHER_ID, orderId);
        String componentName = RichTableListRenderUtils.getComponentName(null, parameters);
        String link = CoreServices.linkFactoryService().getExternalLink(null, componentName, parameters, null);

        if (null == componentName || null == link) goToErrorPage(resp);

        resp.sendRedirect(link);
    }

    private void goToErrorPage(HttpServletResponse resp) throws ServletException, IOException
    {
        String link = CoreServices.linkFactoryService().getExternalLink(null, DirectumErrorPage.class.getSimpleName(), null, null);
        resp.sendRedirect(link);
    }
}