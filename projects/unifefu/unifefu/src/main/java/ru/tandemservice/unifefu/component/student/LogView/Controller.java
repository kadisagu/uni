/* $Id$ */
package ru.tandemservice.unifefu.component.student.LogView;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author Alexey Lopatin
 * @since 18.10.2013
 */
public class Controller extends ru.tandemservice.uni.component.log.EntityLogViewBase.Controller
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = component.getModel();
        model.setSettings(UniBaseUtils.getDataSettings(component, "logView.student.filter."));
        getDao().prepare(model);
        prepareDataSource(component);
    }
}
