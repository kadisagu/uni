/**
 * GetEDocumentActualVersionRESTResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class GetEDocumentActualVersionRESTResponse  implements java.io.Serializable {
    private byte[] getEDocumentActualVersionRESTResult;

    public GetEDocumentActualVersionRESTResponse() {
    }

    public GetEDocumentActualVersionRESTResponse(
           byte[] getEDocumentActualVersionRESTResult) {
           this.getEDocumentActualVersionRESTResult = getEDocumentActualVersionRESTResult;
    }


    /**
     * Gets the getEDocumentActualVersionRESTResult value for this GetEDocumentActualVersionRESTResponse.
     * 
     * @return getEDocumentActualVersionRESTResult
     */
    public byte[] getGetEDocumentActualVersionRESTResult() {
        return getEDocumentActualVersionRESTResult;
    }


    /**
     * Sets the getEDocumentActualVersionRESTResult value for this GetEDocumentActualVersionRESTResponse.
     * 
     * @param getEDocumentActualVersionRESTResult
     */
    public void setGetEDocumentActualVersionRESTResult(byte[] getEDocumentActualVersionRESTResult) {
        this.getEDocumentActualVersionRESTResult = getEDocumentActualVersionRESTResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetEDocumentActualVersionRESTResponse)) return false;
        GetEDocumentActualVersionRESTResponse other = (GetEDocumentActualVersionRESTResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getEDocumentActualVersionRESTResult==null && other.getGetEDocumentActualVersionRESTResult()==null) || 
             (this.getEDocumentActualVersionRESTResult!=null &&
              java.util.Arrays.equals(this.getEDocumentActualVersionRESTResult, other.getGetEDocumentActualVersionRESTResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetEDocumentActualVersionRESTResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetEDocumentActualVersionRESTResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetEDocumentActualVersionRESTResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetEDocumentActualVersionRESTResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">GetEDocumentActualVersionRESTResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getEDocumentActualVersionRESTResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "GetEDocumentActualVersionRESTResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
