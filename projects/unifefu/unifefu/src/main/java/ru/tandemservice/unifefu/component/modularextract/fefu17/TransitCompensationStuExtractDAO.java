/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu17;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.TransitCompensationStuExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2014
 */
public class TransitCompensationStuExtractDAO extends UniBaseDao implements IExtractComponentDao<TransitCompensationStuExtract>
{

	@Override
	public void doCommit(TransitCompensationStuExtract extract, Map parameters)
	{
		//save print form
		MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);
	}

	@Override
	public void doRollback(TransitCompensationStuExtract extract, Map parameters)
	{
	}
}
