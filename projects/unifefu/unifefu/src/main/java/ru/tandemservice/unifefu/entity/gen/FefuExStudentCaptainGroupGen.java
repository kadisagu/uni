package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuExStudentCaptainGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь групп, у которых были удалены старосты с выписками из приказов и присвоении квалификации
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuExStudentCaptainGroupGen extends EntityBase
 implements INaturalIdentifiable<FefuExStudentCaptainGroupGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuExStudentCaptainGroup";
    public static final String ENTITY_NAME = "fefuExStudentCaptainGroup";
    public static final int VERSION_HASH = 765456157;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";
    public static final String L_EXTRACT = "extract";

    private Group _group;     // Группа
    private AbstractStudentExtract _extract;     // Выписка из приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Выписка из приказа. Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка из приказа. Свойство не может быть null.
     */
    public void setExtract(AbstractStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuExStudentCaptainGroupGen)
        {
            if (withNaturalIdProperties)
            {
                setGroup(((FefuExStudentCaptainGroup)another).getGroup());
                setExtract(((FefuExStudentCaptainGroup)another).getExtract());
            }
        }
    }

    public INaturalId<FefuExStudentCaptainGroupGen> getNaturalId()
    {
        return new NaturalId(getGroup(), getExtract());
    }

    public static class NaturalId extends NaturalIdBase<FefuExStudentCaptainGroupGen>
    {
        private static final String PROXY_NAME = "FefuExStudentCaptainGroupNaturalProxy";

        private Long _group;
        private Long _extract;

        public NaturalId()
        {}

        public NaturalId(Group group, AbstractStudentExtract extract)
        {
            _group = ((IEntity) group).getId();
            _extract = ((IEntity) extract).getId();
        }

        public Long getGroup()
        {
            return _group;
        }

        public void setGroup(Long group)
        {
            _group = group;
        }

        public Long getExtract()
        {
            return _extract;
        }

        public void setExtract(Long extract)
        {
            _extract = extract;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuExStudentCaptainGroupGen.NaturalId) ) return false;

            FefuExStudentCaptainGroupGen.NaturalId that = (NaturalId) o;

            if( !equals(getGroup(), that.getGroup()) ) return false;
            if( !equals(getExtract(), that.getExtract()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getGroup());
            result = hashCode(result, getExtract());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getGroup());
            sb.append("/");
            sb.append(getExtract());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuExStudentCaptainGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuExStudentCaptainGroup.class;
        }

        public T newInstance()
        {
            return (T) new FefuExStudentCaptainGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "group":
                    return obj.getGroup();
                case "extract":
                    return obj.getExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "extract":
                    obj.setExtract((AbstractStudentExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "group":
                        return true;
                case "extract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "group":
                    return true;
                case "extract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "group":
                    return Group.class;
                case "extract":
                    return AbstractStudentExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuExStudentCaptainGroup> _dslPath = new Path<FefuExStudentCaptainGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuExStudentCaptainGroup");
    }
            

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuExStudentCaptainGroup#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Выписка из приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuExStudentCaptainGroup#getExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    public static class Path<E extends FefuExStudentCaptainGroup> extends EntityPath<E>
    {
        private Group.Path<Group> _group;
        private AbstractStudentExtract.Path<AbstractStudentExtract> _extract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuExStudentCaptainGroup#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Выписка из приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuExStudentCaptainGroup#getExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

        public Class getEntityClass()
        {
            return FefuExStudentCaptainGroup.class;
        }

        public String getEntityName()
        {
            return "fefuExStudentCaptainGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
