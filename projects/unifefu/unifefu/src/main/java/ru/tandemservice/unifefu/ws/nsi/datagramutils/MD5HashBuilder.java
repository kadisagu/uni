/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Dmitry Seleznev
 * @since 07.05.2014
 */
public class MD5HashBuilder
{
    private static MessageDigest MESS_DIGESTER;

    private StringBuilder _stringBuilder = new StringBuilder();

    private static MessageDigest getMessageDigester()
    {
        if (null == MESS_DIGESTER)
        {
            try
            {
                MESS_DIGESTER = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e)
            {
            }
        }
        return MESS_DIGESTER;
    }

    public static long getCheckSum(String value)
    {
        return new BigInteger(1, getMessageDigester().digest(value.getBytes())).longValue();
    }

    public void add(String value)
    {
        _stringBuilder.append(value);
    }

    public void add(long value)
    {
        _stringBuilder.append(value);
    }

    public long getCheckSum()
    {
        return getCheckSum(_stringBuilder.toString());
    }
}