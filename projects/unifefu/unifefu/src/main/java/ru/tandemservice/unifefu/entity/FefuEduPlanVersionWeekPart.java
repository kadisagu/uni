package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.unifefu.entity.gen.FefuEduPlanVersionWeekPartGen;

/**
 * Элемент разбиения учебного графика (ДВФУ)
 */
public class FefuEduPlanVersionWeekPart extends FefuEduPlanVersionWeekPartGen
{
    public FefuEduPlanVersionWeekPart()
    {
    }

    public FefuEduPlanVersionWeekPart(FefuEduPlanVersionWeek eduPlanVersionWeek, int partitionElementNumber, EppWeekType weekType)
    {
        this.setEduPlanVersionWeek(eduPlanVersionWeek);
        this.setPartitionElementNumber(partitionElementNumber);
        this.setWeekType(weekType);
    }
}