/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu5.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuCourseTransferStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuCourseTransferStuListExtract, Model>
{
}
