/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu23.AddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 22.01.2015
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuTransfStuDPOExtract>
{
    private List<Course> _courseList;
    private ISelectModel _educationOrgUnitModel;
    private ISelectModel _dpoProgramModel;
    private EducationOrgUnit _educationOrgUnit;
    private IUploadFile _uploadFile;

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public ISelectModel getEducationOrgUnitModel()
    {
        return _educationOrgUnitModel;
    }

    public void setEducationOrgUnitModel(ISelectModel educationOrgUnitModel)
    {
        _educationOrgUnitModel = educationOrgUnitModel;
    }

    public ISelectModel getDpoProgramModel()
    {
        return _dpoProgramModel;
    }

    public void setDpoProgramModel(ISelectModel dpoProgramModel)
    {
        _dpoProgramModel = dpoProgramModel;
    }

    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        _educationOrgUnit = educationOrgUnit;
    }

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

}