/*$Id$*/
package ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.logic;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;

import javax.annotation.Nullable;
import java.util.Collection;

/**
 * @author DMITRY KNYAZEV
 * @since 22.01.2015
 */
public interface IFefuBrsPermittedEduPlanDAO extends ISharedBaseDao
{
    /**
     * Возвращает насторойку "Запретить создание реализаций"
     *
     * @return {@code true} - создание реализаций запрещено, {@code false} - создание реализаций разрешено
     */
    public Boolean isDisableCreation();

    /**
     * Сохраняет значение настройки "Запретить создание реализаций"
     *
     * @param value значение настройки
     */
    public void saveDisableCreation(@Nullable Boolean value);

    /**
     * Сохраняет список РУП как разрешенных для создания реализаций.
     * Не должен содеражать РУП уже добавленных как разрешенные.
     *
     * @param workPlanList список РУП для добавления к разрешенным
     * @throws ClassCastException если не все элементы принадлежат к {@link ru.tandemservice.uniepp.entity.workplan.EppWorkPlan}
     */
    public void saveWorkPlansAsPermitted(Collection<IEntity> workPlanList) throws ClassCastException;
}
