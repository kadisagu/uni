/* $Id$ */
package ru.tandemservice.unifefu.dao;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import ru.tandemservice.movestudent.dao.MoveStudentDao;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unifefu.entity.FefuExStudentCaptainGroup;
import ru.tandemservice.unifefu.entity.FefuInactiveProgramSubjectIndexToCourse;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData;
import ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Nikolay Fedorovskih
 * @since 25.06.2013
 */
public class FefuMoveStudentDAO extends MoveStudentDao implements IFefuMoveStudentDAO
{
    @Override
    public boolean canBeCreatedExtractForStudentWithoutGroup(StudentExtractType extractType)
    {
        return super.canBeCreatedExtractForStudentWithoutGroup(extractType) ||
                extractType.getCode().equals(StudentExtractTypeCodes.FEFU_ENROLL_TO_SECOND_AND_NEXT_COURSE_MODULAR_ORDER) ||
                extractType.getCode().equals(StudentExtractTypeCodes.FEFU_TRANSFER_ENROLLMENT_MODULAR_ORDER) ||
                extractType.getCode().equals(StudentExtractTypeCodes.FEFU_ENROLL_DPO_MODULAR_ORDER) ||
                extractType.getCode().equals(StudentExtractTypeCodes.FEFU_EXCLUDE_DPO_MODULAR_ORDER) ||
                extractType.getCode().equals(StudentExtractTypeCodes.FEFU_DPO_CONTINGENT_MODULAR_ORDER) ||
                extractType.getCode().equals(StudentExtractTypeCodes.FEFU_TRANSFER_DPO_MODULAR_ORDER);
    }

    @Override
    public List<String> getStudentPracticeTypes()
    {
        return new DQLSelectBuilder().fromEntity(FefuStudentPractice.class, "e")
                .column(property(FefuStudentPractice.title().fromAlias("e")))
                .where(eq(property(FefuStudentPractice.isParent().fromAlias("e")), value(true)))
                .order(property(FefuStudentPractice.code().fromAlias("e")))
                .createStatement(getSession()).list();
    }

    @Override
    public List<String> getStudentPracticeKinds()
    {
        return new DQLSelectBuilder().fromEntity(FefuStudentPractice.class, "e")
                .column(property(FefuStudentPractice.title().fromAlias("e")))
                .order(property(FefuStudentPractice.title().fromAlias("e")))
                .createStatement(getSession()).list();
    }

    @Override
    public String getDeclinablePracticeType(String type, GrammaCase grammaCase)
    {
        // Ищем слово в справочнике (да, это жуткий костыль)
        FefuStudentPractice practice = new DQLSelectBuilder().fromEntity(FefuStudentPractice.class, "e")
                .column("e")
                .where(and(
                        eq(property(FefuStudentPractice.title().fromAlias("e")), value(type)),
                        eq(property(FefuStudentPractice.isParent().fromAlias("e")), value(true))
                )).createStatement(getSession()).uniqueResult();
        InflectorVariant inflectorVariant = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(grammaCase);

        return practice != null ? DeclinableManager.instance().dao().getPropertyValue(practice, FefuStudentPractice.title().s(), inflectorVariant) : null;
    }

    @Override
    public List<FefuOrphanPayment> getOrphanPayments(Long extractId)
    {
        return new DQLSelectBuilder().fromEntity(FefuOrphanPayment.class, "p").
                where(eq(property("p", FefuOrphanPayment.extract().id()), value(extractId))).
                order(property("p", FefuOrphanPayment.type().priority())).
                createStatement(getSession()).list();
    }

    @Override
    public void addCustomConditionToGroupSelectBuilder(DQLSelectBuilder builder, String alias)
    {
        builder.joinPath(DQLJoinType.left, Group.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().fromAlias(alias), "si");
        builder.where(or(isNull("si.id"),
                         notExistsByExpr(FefuInactiveProgramSubjectIndexToCourse.class, "inactive", and(
                                 eq(property("inactive", FefuInactiveProgramSubjectIndexToCourse.L_COURSE), property(alias, Group.L_COURSE)),
                                 eq(property("inactive", FefuInactiveProgramSubjectIndexToCourse.L_EDU_PROGRAM_SUBJECT_INDEX), property("si", EduProgramSubject.L_SUBJECT_INDEX))
                         ))
        ));
    }

    @Override
    public void breakLinkCaptainWithGroups(AbstractStudentExtract extract)
    {
        //Получаем список групп, для которых студент является старостой
        List<GroupCaptainStudent> listCaptain = getList(GroupCaptainStudent.class, GroupCaptainStudent.L_STUDENT, extract.getEntity());
        for (GroupCaptainStudent temp : listCaptain)
        {
            FefuExStudentCaptainGroup exCaptain = new FefuExStudentCaptainGroup();
            exCaptain.setExtract(extract);
            exCaptain.setGroup(temp.getGroup());
            save(exCaptain);
            delete(temp);
        }
    }

    @Override
    public void linkStudCaptainWithGroups(AbstractStudentExtract extract)
    {
        List<FefuExStudentCaptainGroup> listValues = getList(FefuExStudentCaptainGroup.class, FefuExStudentCaptainGroup.L_EXTRACT, extract);
        for (FefuExStudentCaptainGroup temp : listValues)
        {
            GroupCaptainStudent captain = new GroupCaptainStudent();
            captain.setGroup(temp.getGroup());
            captain.setStudent(extract.getEntity());
            save(captain);
            delete(temp);
        }
    }

    @Override
    public void addFormativeOrgUnitToBuilder(DQLSelectBuilder builder, OrgUnit formativeOrgUnit)
    {
        return;
    }


    @Override
    public void setStatusEndDateByCode(ModularStudentExtract extract, String statusCode, Date endDate)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "cs")
                .column("cs")
                .where(eq(property("cs", StudentCustomState.student()), value(extract.getEntity())))
                .where(eq(property("cs", StudentCustomState.customState().code()), value(statusCode)))
                .where(or(
                        isNull(property("cs", StudentCustomState.endDate())),
                        ge(property("cs", StudentCustomState.endDate()), valueDate(endDate))
                ));

        StudentCustomState state = builder.createStatement(getSession()).uniqueResult();

        if (state != null)
        {
            StudentCustomStateToExtractRollbackData rollbackData = new StudentCustomStateToExtractRollbackData();
            rollbackData.setExtract(extract);
            rollbackData.setStudentCustomState(state);
            rollbackData.setDateStatusBegin(state.getBeginDate());
            rollbackData.setDateStatusEnd(state.getEndDate());
            DataAccessServices.dao().save(rollbackData);
            state.setEndDate(endDate);
            if (state.getBeginDate() != null && state.getBeginDate().after(endDate))
                state.setBeginDate(endDate);
            UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(state);
        }
    }

    @Override
    public void revertCustomStateDates(ModularStudentExtract extract)
    {
        StudentCustomStateToExtractRollbackData data = get(StudentCustomStateToExtractRollbackData.class, StudentCustomStateToExtractRollbackData.extract(), extract);

        if (data != null)
        {
            StudentCustomState state = data.getStudentCustomState();
            state.setBeginDate(data.getDateStatusBegin());
            state.setEndDate(data.getDateStatusEnd());
            UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(state);

            delete(data);
        }
    }
}