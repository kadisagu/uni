/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.Test1C;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;
import ru.tandemservice.unifefu.ws.c1.dao.Fefu_1C_Daemon;
import ru.tandemservice.unifefu.ws.c1.dao.IFefu_1C_Daemon;

import java.util.Collection;

/**
 * @author Nikolay Fedorovskih
 * @since 13.12.2013
 */
public class FefuSystemActionTest1CUI extends UIPresenter
{
    private Integer _tempFileId;

    private void downloadTempFile(Integer temporaryFileId)
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.PRINT_REPORT)
                .parameters(new ParametersMap().add("id", temporaryFileId).add("zip", Boolean.FALSE).add("extension", "txt"))
                .activate();
    }

    @Override
    public void onComponentPrepareRender()
    {
        if (_tempFileId != null)
        {
            try
            {
                downloadTempFile(_tempFileId);
            }
            finally
            {
                _tempFileId = null;
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuSystemActionTest1C.EXTRACT_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap("fio", "orderNumber"));
        }
    }

    public void onClickStart()
    {
        final AbstractListDataSource<IEntity> ds = ((BaseSearchListDataSource) _uiConfig.getDataSource(FefuSystemActionTest1C.EXTRACT_DS)).getLegacyDataSource();
        final CheckboxColumn column = (CheckboxColumn) ds.getColumn(FefuSystemActionTest1C.CHECKBOX_COLUMN);
        final Collection<Long> ids = UniBaseDao.ids(column.getSelectedObjects());

        if (ids.isEmpty())
            throw new ApplicationException("Необходимо выбрать одну или несколько выписок.");

        long countSuccess = column.getSelectedObjects().stream().filter(e-> null !=e.getProperty(FefuSystemActionTest1C.SYNC_PROP)).count();
        if (countSuccess > 0 && !"ok".equals(getClientParameter()))
        {
            ConfirmInfo confirm = new ConfirmInfo("Вы действительно хотите повторно отправить " + CommonBaseStringUtil.numberWithPostfixCase(countSuccess, "выписку?", "выписки?", "выписок?"), new ClickButtonAction("start", "ok", true));
            TapSupportUtils.displayConfirm(confirm);
            return;
        }


        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                try
                {
                    IFefu_1C_Daemon.instance.get().doTest(ids);
                }
                catch (Fefu_1C_Daemon.TestResultException e)
                {
                    _tempFileId = PrintReportTemporaryStorage.registerTemporaryPrintForm(e.data, "test-1C.txt");
                }
                return null;
            }
        };

        new BackgroundProcessHolder().start("Отправка пакетов в подсистему интеграции БУ/УК (1С)", process, ProcessDisplayMode.unknown);
    }

}