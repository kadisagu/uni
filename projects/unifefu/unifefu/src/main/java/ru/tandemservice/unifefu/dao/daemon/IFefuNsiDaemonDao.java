/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 12.09.2013
 */
public interface IFefuNsiDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IFefuNsiDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IFefuNsiDaemonDao> instance = new SpringBeanCache<>(IFefuNsiDaemonDao.class.getName());

    /**
     * Производит синхронизацию данны, измененных за последние n минут, с момента последнего запуска демона.
     * Так же, по расписанию инициирует синхронизацию с НСИ на предмет изменений, которые по каким-либо причинам не внеслись в НСИ.
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doSyncPersonsWithNSI();

    /**
     * Регистрирует факт добавления новой сущности
     *
     * @param ids - идентификаторы добавленных сущностей
     */
    void doRegisterEntityAdd(Long... ids);

    /**
     * Регистрирует факт изменения сущности
     *
     * @param ids - идентификаторы измененных сущностей
     */
    void doRegisterEntityEdit(Long... ids);

    /**
     * Регистрирует факт удаления сущности
     *
     * @param ids - идентификаторы удалённых сущностей
     */
    void doRegisterEntityDelete(Long... ids);

    /**
     * Возвращает true, если в очереди есть хотя бы одна сущность на добавление, изменение, или удаление
     *
     * @return - true, если в очереди есть хотя бы одна сущность
     */
    boolean isDaemonHasTasksInQueue();

    /**
     * Регистрирует объекты, изменения которых нужно игнорировать, дабы не вызывать их
     * повторное обновление в НСИ после загрузки из той же НСИ
     *
     * @param ids
     */
    void doRegisterEntityToIgnore(Long... ids);

    /**
     * Проверяет, нужно ли игнорировать сущность и не отправлять изменения в НСИ.
     * Нужно, чтобы блокировать отправку изменений пришедших только что из НСИ.
     *
     * @param id - идентификатор сущности
     * @return
     */
    boolean isEntityShouldBeIgnored(Long id);

    /**
     * Создаёт на основе подраздедения организацию ДВФУ
     *
     * @param id - идентификатор подразделения
     */
    void doCreateFefuTopOrgUnit(Long id);

    /**
     * Возвращает список справочников НСИ в порядке приоритета, для которых включена синхронизация в реальном времени
     *
     * @return - список справочников НСИ
     */
    List<FefuNsiCatalogType> getRealTimeSyncNSICatalogsList();
}