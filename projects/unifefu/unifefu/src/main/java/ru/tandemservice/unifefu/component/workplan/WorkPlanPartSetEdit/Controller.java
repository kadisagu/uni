/* $Id: Controller.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.workplan.WorkPlanPartSetEdit;

import org.tandemframework.core.component.IBusinessComponent;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 08.02.14
 * Time: 21:42
 */
public class Controller extends ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit.Controller
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = (Model)this.getModel(component);
        if(null != component.getParentComponent() && "ru.tandemservice.uniepp.component.workplan.WorkPlanWizard".equals(component.getParentComponent().getComponentMeta().getName()))
            model.setWizard(true);
        super.onRefreshComponent(component);
    }

    @Override
    public void onChangePartsInTerm(IBusinessComponent component)
    {
        final Model model = (Model)this.getModel(component);
        final boolean firstTime = (null == model.getPartsInTerm());
        if (firstTime) {
            // если ничего не выбранно - то части должна быть одна (DEV-4089)
            model.setPartsInTerm(Model.getPartsInTerm(1));
        }
        super.onChangePartsInTerm(component);
    }
}
