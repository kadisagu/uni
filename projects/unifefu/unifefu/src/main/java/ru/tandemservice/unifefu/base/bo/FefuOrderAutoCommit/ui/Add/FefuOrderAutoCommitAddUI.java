/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.ui.Add;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic.FefuDirectumOrderData;
import ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic.FefuOrderAutoCommitUtil;

import java.util.Date;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 13.11.2012
 */
public class FefuOrderAutoCommitAddUI extends UIPresenter
{
    private Date _formingDate;
    private IUploadFile _file;
    private EmployeePost _executor;
    private String _executorStr;
    private boolean _commit;

    @Override
    public void onComponentRefresh()
    {
        _formingDate = new Date();
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        if (principalContext instanceof EmployeePost)
        {
            _executor = (EmployeePost) principalContext;
            _executorStr = OrderExecutorSelectModel.getExecutor(_executor);
        }
    }

    // Getters & Setters

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public IUploadFile getFile()
    {
        return _file;
    }

    public void setFile(IUploadFile file)
    {
        _file = file;
    }

    public EmployeePost getExecutor()
    {
        return _executor;
    }

    public void setExecutor(EmployeePost executor)
    {
        _executor = executor;
    }

    public String getExecutorStr()
    {
        return _executorStr;
    }

    public void setExecutorStr(String executorStr)
    {
        _executorStr = executorStr;
    }

    public boolean isCommit()
    {
        return _commit;
    }

    public void setCommit(boolean commit)
    {
        _commit = commit;
    }


    // Listeners

    public void onClickApply() throws Exception
    {
        List<FefuDirectumOrderData> orderDataList = FefuOrderAutoCommitUtil.validateDirectumSyncFileStructureAndPrepareData(_file, _executor);
        FefuOrderAutoCommitUtil.synchronizeOrders(orderDataList, _executor, _formingDate, _commit);
        //if (!UserContext.getInstance().getErrorCollector().hasErrors()) deactivate();
        //FefuOrderAutoCommitManager.instance().dao().doSynchronizeOrders(orderDataList, _executor, _formingDate, _commit);
    }
}