package ru.tandemservice.unifefu.component.listextract.fefu13.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public class DAO extends AbstractListExtractPubDAO<FefuAdditionalAcademGrantStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
    }
}
