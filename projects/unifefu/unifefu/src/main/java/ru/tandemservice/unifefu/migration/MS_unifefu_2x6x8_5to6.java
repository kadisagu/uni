package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x8_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuTransfAcceleratedTimeStuListExtract

		// создана новая сущность
        if(!tool.tableExists("fftrnsfacclrtdtmstlstextrct_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fftrnsfacclrtdtmstlstextrct_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("transferdate_p", DBType.DATE), 
				new DBColumn("course_id", DBType.LONG).setNullable(false), 
				new DBColumn("groupold_id", DBType.LONG).setNullable(false), 
				new DBColumn("groupnew_id", DBType.LONG).setNullable(false), 
				new DBColumn("educationorgunitnew_id", DBType.LONG).setNullable(false), 
				new DBColumn("educationorgunitold_id", DBType.LONG).setNullable(false), 
                new DBColumn("season_p", DBType.createVarchar(255)),
                new DBColumn("planneddate_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuTransfAcceleratedTimeStuListExtract");

		}


    }
}