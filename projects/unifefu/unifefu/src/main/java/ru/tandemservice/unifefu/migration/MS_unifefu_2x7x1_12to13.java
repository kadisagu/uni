package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x1_12to13 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {


		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuOrderToPrintFormRelation

		// сущность стала root, создано обязательное свойство class
		{
			// создать колонку
			tool.createColumn("fefuordertoprintformrelation_t", new DBColumn("discriminator", DBType.SHORT));

			// заполнить discriminator, вытащив код сущности из id
			String functionPrefix = tool.getDataSource().getSqlFunctionPrefix();
			tool.executeUpdate("update fefuordertoprintformrelation_t set discriminator=" + functionPrefix + "getentitycode(id) where discriminator is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefuordertoprintformrelation_t", "discriminator", false);

		}

		// создано обязательное свойство fileType
		{
			// создать колонку
			tool.createColumn("fefuordertoprintformrelation_t", new DBColumn("filetype_p", DBType.createVarchar(255)));

			// задать значение по умолчанию
			java.lang.String defaultFileType = "application/msword";
			tool.executeUpdate("update fefuordertoprintformrelation_t set filetype_p=? where filetype_p is null", defaultFileType);

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefuordertoprintformrelation_t", "filetype_p", false);

		}

		// создано обязательное свойство fileName
		{
			// создать колонку
			tool.createColumn("fefuordertoprintformrelation_t", new DBColumn("filename_p", DBType.createVarchar(255)));

			// задать значение по умолчанию
			java.lang.String defaultFileName = "temp.rtf";
			tool.executeUpdate("update fefuordertoprintformrelation_t set filename_p=? where filename_p is null", defaultFileName);

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefuordertoprintformrelation_t", "filename_p", false);

		}

		// создано обязательное свойство content
		{
			// создать колонку
			tool.createColumn("fefuordertoprintformrelation_t", new DBColumn("content_p", DBType.BLOB));

			// задать значение по умолчанию
			byte[] defaultContent = new byte[0];
			tool.executeUpdate("update fefuordertoprintformrelation_t set content_p=? where content_p is null", defaultContent);

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefuordertoprintformrelation_t", "content_p", false);

		}
        copyDataForOrderTemplate(tool);

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuExtractToPrintFormRelation

        // сущность была удалена
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("ffextrcttprntfrmrltn_t", "fefuprintformrelation_t");

            // удалить таблицу
            tool.dropTable("ffextrcttprntfrmrltn_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("fefuExtractToPrintFormRelation");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuPrintFormRelation

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("fefuprintformrelation_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("fefuPrintFormRelation");

        }


    }

    private void copyDataForOrderTemplate(DBTool tool) throws SQLException
    {
        tool.getStatement().executeUpdate("update ord set ord.filetype_p = f.filetype_p, ord.filename_p = f.filename_p, ord.content_p= f.content_p"+
                                                      " from fefuordertoprintformrelation_t as ord" +
                                                      " inner join fefuprintformrelation_t as f" +
                                                      " on ord.id=f.id");

        tool.deleteRowsFromParentTables("fefuordertoprintformrelation_t", "fefuprintformrelation_t");

    }
}