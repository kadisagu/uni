/* $Id$ */
package ru.tandemservice.unifefu.utils;

import com.google.common.collect.Lists;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.unifefu.dao.student.FefuStudentExtractData;
import ru.tandemservice.unifefu.dao.student.IFefuStudentExtractData;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 11/6/13
 */
public class FefuOrderData
{
    public static Map<String, String> getData(AbstractStudentExtract extract)
    {
        final String methodName = "getData";
        IFefuStudentExtractData fefuStudentExtractData = (IFefuStudentExtractData) ApplicationRuntime.getBean(IFefuStudentExtractData.FEFU_EXTRACT_DATA_BEAN_NAME);
        try
        {
            Method method = fefuStudentExtractData.getClass().getMethod(methodName, new Class[]{extract.getClass()});
            return (Map<String, String>) method.invoke(fefuStudentExtractData, extract);
        }
        catch (NoSuchMethodException e1)
        {
            return fefuStudentExtractData.getBaseData(extract);
        }
        catch (InvocationTargetException | IllegalAccessException e2)
        {
            throw new RuntimeException(e2);
        }
    }

    public static String[][] getMainEduProgrammTableData(List<Map<String, String>> extractsData)
    {
        if(extractsData.isEmpty()) return new String[1][];
        List<String[]> extractTableData = Lists.newArrayList();
        for(Map<String, String> extractData: extractsData)
        {
            List<String> tableData = Lists.newArrayList();
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_COURSE));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_EDU_OU));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_PAYMENT));
            tableData.add(extractData.get(FefuStudentExtractData.BASE_PROP_ORDER_NUM));
            tableData.add(extractData.get(FefuStudentExtractData.BASE_PROP_ORDER_COMMIT_DATE));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_GEN_COMMENT));
            extractTableData.add(tableData.toArray(new String[0]));
        }
        return extractTableData.toArray(new String[0][0]);
    }

    public static String[][] getPartComExtractsTableData(List<Map<String, String>> extractsData)
    {
        if(extractsData.isEmpty()) return new String[1][];
        List<String[]> extractTableData = Lists.newArrayList();
        for(Map<String, String> extractData: extractsData)
        {
            List<String> tableData = Lists.newArrayList();
            tableData.add(extractData.get(FefuStudentExtractData.BASE_PROP_ORDER_COMMIT_DATE));
            tableData.add(extractData.get(FefuStudentExtractData.BASE_PROP_ORDER_NUM));
            tableData.add(extractData.get(FefuStudentExtractData.BASE_PROP_REASON));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_GEN_COMMENT));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_END_DATE));
            extractTableData.add(tableData.toArray(new String[0]));
        }
        return extractTableData.toArray(new String[0][0]);
    }

    public static String[][] getPracticeExtractsTableData(List<Map<String, String>> extractsData)
    {
        if(extractsData.isEmpty()) return new String[1][];
        List<String[]> extractTableData = Lists.newArrayList();
        for(Map<String, String> extractData: extractsData)
        {
            List<String> tableData = Lists.newArrayList();
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_PRACTICE_TYPE));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_PRACTICE_PLACE));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_PRACTICE_PERIOD));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_PRACTICE_HEADER));
            tableData.add(extractData.get(FefuStudentExtractData.BASE_PROP_ORDER_NUM));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_GEN_COMMENT));
            extractTableData.add(tableData.toArray(new String[0]));
        }
        return extractTableData.toArray(new String[0][0]);
    }

    public static String[][] getOrphanPaymentsTableData(List<Map<String, String>> extractsData)
    {
        if(extractsData.isEmpty()) return new String[1][];
        List<String[]> extractTableData = Lists.newArrayList();
        for(Map<String, String> extractData: extractsData)
        {
            List<String> tableData = Lists.newArrayList();
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_COURSE));
            tableData.add(extractData.get(FefuStudentExtractData.BASE_PROP_ORDER_NUM));
            tableData.add(extractData.get(FefuStudentExtractData.BASE_PROP_ORDER_COMMIT_DATE));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_EXTRACT_PAYMENT));
            tableData.add(extractData.get(FefuStudentExtractData.PROP_GEN_COMMENT));
            extractTableData.add(tableData.toArray(new String[0]));
        }
        return extractTableData.toArray(new String[0][0]);
    }
}
