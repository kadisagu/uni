package ru.tandemservice.unifefu.base.ext.TrJournal.ui.AddEvent;

import com.google.common.collect.Lists;
import org.hibernate.Session;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartModuleWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.AddEvent.TrJournalAddEventUI;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: amakarova
 */
public class TrJournalAddEventExtUI extends UIAddon
{
    private Date _date;
    private Date _durationBegin;
    private Date _durationEnd;
    private boolean isDateVisible;

    private ScheduleEvent _schEvent = new ScheduleEvent();

    private TrEduGroupEvent _groupEvent = new TrEduGroupEvent();

    private EppRealEduGroup _eppRealEduGroup4LoadType;

     public TrJournalAddEventExtUI(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    public void onComponentActivate()
    {
        TrJournalAddEventUI presenter = getPresenter();
        setDateVisible(getJournalGroup(presenter.getSupport().getSession(), presenter.getJournal().getId()));
        if (getGroupEvent() != null && getGroupEvent().getScheduleEvent() != null)
        {
            setDate(getGroupEvent().getScheduleEvent().getDurationBegin());
            getSchEvent().setDurationBegin(getGroupEvent().getScheduleEvent().getDurationBegin());
            getSchEvent().setDurationEnd(getGroupEvent().getScheduleEvent().getDurationEnd());
        } else if (isDateVisible()) {
            setDate(new Date());
            setDurationBegin(new Date());
            setDurationEnd(new Date());
        }
    }

    @Override
    public void onComponentRefresh()
    {
        TrJournalAddEventUI presenter = getPresenter();
        presenter.onComponentRefresh();
        setGroupEvent(getTrEduGroupEvent(presenter.getEvent()));

        TrJournal journal = presenter.getJournal();

        EppGroupType groupTypeNew = null;

        // получим список групп
        DQLSelectBuilder groupDQL = new DQLSelectBuilder()
                .fromEntity(TrJournalGroup.class, "rel")
                .column("rel")
                .joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("rel"), "grp")
                .where(eq(property(TrJournalGroup.journal().fromAlias("rel")), value(journal)))
                 ;
        List<TrJournalGroup> relList = groupDQL.createStatement(presenter.getSupport().getSession()).list();
        List<EppRealEduGroup> groups = CommonBaseUtil.getPropertiesList(relList, TrJournalGroup.group().s());

        if (groups != null && groups.size() > 0) {
            groupTypeNew = getLoadTypeFromGroups(groups);
            for (EppRealEduGroup group : groups) {
                if (group.getType().getCode().equals(groupTypeNew.getCode()))
                    setEppRealEduGroup4LoadType(group);
            }
        }

        if (groupTypeNew == null) {
            TrJournalModule journalModule = presenter.getModule();
            Long dicsiplineId = journal.getRegistryElementPart().getId();
            IEppRegElPartWrapper disciplineWrapper = IEppRegistryDAO.instance.get().getRegistryElementPartDataMap(Collections.singleton(dicsiplineId)).get(dicsiplineId);
            Collection<IEppRegElPartModuleWrapper> disciplineModules = disciplineWrapper.getModuleMap().values();
            List<EppGroupTypeALT> groupTypes = IUniBaseDao.instance.get().getList(EppGroupTypeALT.class);

            for (IEppRegElPartModuleWrapper moduleWrapper : disciplineModules) {
                if (moduleWrapper.getItem().equals(journalModule.getModule())) {
                    for (EppGroupTypeALT groupType : groupTypes) {
                        if (moduleWrapper.getLoadAsDouble(groupType.getCode()) > 0) {
                            if (groupType.getCode().equals(EppGroupTypeALTCodes.TYPE_PRACTICE)) {
                                groupTypeNew = groupType;
                             }
                            if (groupType.getCode().equals(EppGroupTypeALTCodes.TYPE_LABS)) {
                                if (groupTypeNew == null || !groupType.getCode().equals(EppGroupTypeALTCodes.TYPE_PRACTICE)) {
                                    groupTypeNew = groupType;
                                }
                            }
                            if (groupType.getCode().equals(EppGroupTypeALTCodes.TYPE_LECTURES)) {
                                if (groupTypeNew == null) {
                                    groupTypeNew = groupType;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (groupTypeNew == null) {
            groupTypeNew = UniDaoFacade.getCoreDao().getCatalogItem(EppGroupTypeALT.class, EppGroupTypeALTCodes.TYPE_LECTURES);
        }
        presenter.getEvent().setType(groupTypeNew);
    }

    public Date getDate()
    {
        return _date;
    }

    public void setDate(Date date)
    {
        this._date = date;
    }

    public Date getDurationBegin() {
        return _durationBegin;
    }

    public void setDurationBegin(Date durationBegin) {
        _durationBegin = durationBegin;
    }

    public Date getDurationEnd() {
        return _durationEnd;
    }

    public void setDurationEnd(Date durationEnd) {
        _durationEnd = durationEnd;
    }

    public TrEduGroupEvent getGroupEvent() {
        return _groupEvent;
    }

    public void setGroupEvent(TrEduGroupEvent groupEvent) {
        _groupEvent = groupEvent;
    }

    public TrEduGroupEvent getTrEduGroupEvent(TrJournalEvent event) {
        DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ge")
                .where(DQLExpressions.eq(property(TrEduGroupEvent.journalEvent().fromAlias("ge")), value(event)));
        List<TrEduGroupEvent> eventList = query.createStatement(getSession()).list();
        if (eventList.size() > 0)
            return eventList.get(0);
        else
            return null;
    }

    public TrEduGroupEvent getGroupEventFromJournal(TrJournalEvent event) {
        DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ge")
                .where(DQLExpressions.eq(property(TrEduGroupEvent.journalEvent().journalModule().journal().fromAlias("ge")), value(event)));
        List<TrEduGroupEvent> eventList = query.createStatement(getSession()).list();
        if (eventList.size() > 0)
            return eventList.get(0);
        else
            return null;
    }

    private EppGroupType getLoadTypeFromGroups(List<EppRealEduGroup> groups)
    {
        List<EppGroupType> groupTypes = Lists.newArrayList();
        for (EppRealEduGroup group : groups) {
            EppGroupType groupType = group.getType();
            groupTypes.add(groupType);
        }
        return getLoadType(groupTypes);
    }

    private EppGroupType getLoadType(List<EppGroupType> groupTypes)
    {
        EppGroupType groupTypeNew = null;
        for (EppGroupType groupType : groupTypes)
        {
            String code = groupType.getCode();

            if (code.equals(EppGroupTypeALTCodes.TYPE_PRACTICE)) return groupType;
            if (code.equals(EppGroupTypeALTCodes.TYPE_LABS)) groupTypeNew = groupType;
            if (code.equals(EppGroupTypeALTCodes.TYPE_LECTURES))
            {
                if (groupTypeNew == null)
                {
                    groupTypeNew = groupType;
                }
            }
        }
        if (null == groupTypeNew)
        {
            for (EppGroupType groupType : groupTypes)
            {
                String code = groupType.getCode();
                if (EppGroupTypeFCACodes.CODES.contains(code)) return groupType;
            }
        }
        return groupTypeNew;
    }

    public ScheduleEvent getSchEvent() {
        return _schEvent;
    }

    public void setSchEvent(ScheduleEvent schEvent) {
        _schEvent = schEvent;
    }

    public EppRealEduGroup getEppRealEduGroup4LoadType() {
        return _eppRealEduGroup4LoadType;
    }

    public void setEppRealEduGroup4LoadType(EppRealEduGroup eppRealEduGroup4LoadType) {
        _eppRealEduGroup4LoadType = eppRealEduGroup4LoadType;
    }

    public boolean isDateVisible() {
        return isDateVisible;
    }

    public void setDateVisible(boolean dateVisible) {
        isDateVisible = dateVisible;
    }

    private boolean getJournalGroup(Session session, Long journalId)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(TrJournalGroup.class, "e")
                .where(eq(property(TrJournalGroup.journal().id().fromAlias("e")), value(journalId)))
                 ;
        return dql.createStatement(session).list().size() > 0;
    }
}
