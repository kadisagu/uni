package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unifefu.entity.FefuStudentMarkHistory;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * История оценки студента в журнале
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuStudentMarkHistoryGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuStudentMarkHistory";
    public static final String ENTITY_NAME = "fefuStudentMarkHistory";
    public static final int VERSION_HASH = 1953054997;
    private static IEntityMeta ENTITY_META;

    public static final String L_TR_EDU_GROUP_EVENT_STUDENT = "trEduGroupEventStudent";
    public static final String P_MARK_DATE = "markDate";
    public static final String P_GRADE_AS_LONG = "gradeAsLong";

    private TrEduGroupEventStudent _trEduGroupEventStudent;     // Запись студента по событию в журнале
    private Date _markDate;     // Дата оценки
    private Long _gradeAsLong;     // Балл

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись студента по событию в журнале.
     */
    public TrEduGroupEventStudent getTrEduGroupEventStudent()
    {
        return _trEduGroupEventStudent;
    }

    /**
     * @param trEduGroupEventStudent Запись студента по событию в журнале.
     */
    public void setTrEduGroupEventStudent(TrEduGroupEventStudent trEduGroupEventStudent)
    {
        dirty(_trEduGroupEventStudent, trEduGroupEventStudent);
        _trEduGroupEventStudent = trEduGroupEventStudent;
    }

    /**
     * @return Дата оценки. Свойство не может быть null.
     */
    @NotNull
    public Date getMarkDate()
    {
        return _markDate;
    }

    /**
     * @param markDate Дата оценки. Свойство не может быть null.
     */
    public void setMarkDate(Date markDate)
    {
        dirty(_markDate, markDate);
        _markDate = markDate;
    }

    /**
     * @return Балл.
     */
    public Long getGradeAsLong()
    {
        return _gradeAsLong;
    }

    /**
     * @param gradeAsLong Балл.
     */
    public void setGradeAsLong(Long gradeAsLong)
    {
        dirty(_gradeAsLong, gradeAsLong);
        _gradeAsLong = gradeAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuStudentMarkHistoryGen)
        {
            setTrEduGroupEventStudent(((FefuStudentMarkHistory)another).getTrEduGroupEventStudent());
            setMarkDate(((FefuStudentMarkHistory)another).getMarkDate());
            setGradeAsLong(((FefuStudentMarkHistory)another).getGradeAsLong());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuStudentMarkHistoryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuStudentMarkHistory.class;
        }

        public T newInstance()
        {
            return (T) new FefuStudentMarkHistory();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "trEduGroupEventStudent":
                    return obj.getTrEduGroupEventStudent();
                case "markDate":
                    return obj.getMarkDate();
                case "gradeAsLong":
                    return obj.getGradeAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "trEduGroupEventStudent":
                    obj.setTrEduGroupEventStudent((TrEduGroupEventStudent) value);
                    return;
                case "markDate":
                    obj.setMarkDate((Date) value);
                    return;
                case "gradeAsLong":
                    obj.setGradeAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "trEduGroupEventStudent":
                        return true;
                case "markDate":
                        return true;
                case "gradeAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "trEduGroupEventStudent":
                    return true;
                case "markDate":
                    return true;
                case "gradeAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "trEduGroupEventStudent":
                    return TrEduGroupEventStudent.class;
                case "markDate":
                    return Date.class;
                case "gradeAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuStudentMarkHistory> _dslPath = new Path<FefuStudentMarkHistory>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuStudentMarkHistory");
    }
            

    /**
     * @return Запись студента по событию в журнале.
     * @see ru.tandemservice.unifefu.entity.FefuStudentMarkHistory#getTrEduGroupEventStudent()
     */
    public static TrEduGroupEventStudent.Path<TrEduGroupEventStudent> trEduGroupEventStudent()
    {
        return _dslPath.trEduGroupEventStudent();
    }

    /**
     * @return Дата оценки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentMarkHistory#getMarkDate()
     */
    public static PropertyPath<Date> markDate()
    {
        return _dslPath.markDate();
    }

    /**
     * @return Балл.
     * @see ru.tandemservice.unifefu.entity.FefuStudentMarkHistory#getGradeAsLong()
     */
    public static PropertyPath<Long> gradeAsLong()
    {
        return _dslPath.gradeAsLong();
    }

    public static class Path<E extends FefuStudentMarkHistory> extends EntityPath<E>
    {
        private TrEduGroupEventStudent.Path<TrEduGroupEventStudent> _trEduGroupEventStudent;
        private PropertyPath<Date> _markDate;
        private PropertyPath<Long> _gradeAsLong;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись студента по событию в журнале.
     * @see ru.tandemservice.unifefu.entity.FefuStudentMarkHistory#getTrEduGroupEventStudent()
     */
        public TrEduGroupEventStudent.Path<TrEduGroupEventStudent> trEduGroupEventStudent()
        {
            if(_trEduGroupEventStudent == null )
                _trEduGroupEventStudent = new TrEduGroupEventStudent.Path<TrEduGroupEventStudent>(L_TR_EDU_GROUP_EVENT_STUDENT, this);
            return _trEduGroupEventStudent;
        }

    /**
     * @return Дата оценки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentMarkHistory#getMarkDate()
     */
        public PropertyPath<Date> markDate()
        {
            if(_markDate == null )
                _markDate = new PropertyPath<Date>(FefuStudentMarkHistoryGen.P_MARK_DATE, this);
            return _markDate;
        }

    /**
     * @return Балл.
     * @see ru.tandemservice.unifefu.entity.FefuStudentMarkHistory#getGradeAsLong()
     */
        public PropertyPath<Long> gradeAsLong()
        {
            if(_gradeAsLong == null )
                _gradeAsLong = new PropertyPath<Long>(FefuStudentMarkHistoryGen.P_GRADE_AS_LONG, this);
            return _gradeAsLong;
        }

        public Class getEntityClass()
        {
            return FefuStudentMarkHistory.class;
        }

        public String getEntityName()
        {
            return "fefuStudentMarkHistory";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
