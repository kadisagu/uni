
package ru.tandemservice.unifefu.ws.blackboard.course.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="courseId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vo" type="{http://course.ws.blackboard/xsd}StaffInfoVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "courseId",
    "vo"
})
@XmlRootElement(name = "saveStaffInfo")
public class SaveStaffInfo {

    @XmlElementRef(name = "courseId", namespace = "http://course.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> courseId;
    @XmlElementRef(name = "vo", namespace = "http://course.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<StaffInfoVO> vo;

    /**
     * Gets the value of the courseId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCourseId() {
        return courseId;
    }

    /**
     * Sets the value of the courseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCourseId(JAXBElement<String> value) {
        this.courseId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the vo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StaffInfoVO }{@code >}
     *     
     */
    public JAXBElement<StaffInfoVO> getVo() {
        return vo;
    }

    /**
     * Sets the value of the vo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StaffInfoVO }{@code >}
     *     
     */
    public void setVo(JAXBElement<StaffInfoVO> value) {
        this.vo = ((JAXBElement<StaffInfoVO> ) value);
    }

}
