/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu21.AddEdit;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.FefuExtractPrintFormManager;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public class DAO extends CommonModularStudentExtractAddEditDAO <FefuExcludeStuDPOExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected FefuExcludeStuDPOExtract createNewInstance()
    {
        return new FefuExcludeStuDPOExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));
    }

    @Override
    public void update(Model model)
    {
        if (!model.getExtract().getEntity().getStudentCategory().isDppListener())
            throw new ApplicationException("Невозможно создать приказ. Данный студент не является слушателем ДПО.");

        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());

        super.update(model);
        if (model.getExtract().isIndividual())
            FefuExtractPrintFormManager.instance().dao().saveOrUpdatePrintForm(model.getExtract().getParagraph().getOrder(), model.getUploadFile());

        //Заносим в выписку данные о предыдущей программе ДПО
        FefuExcludeStuDPOExtract extract = model.getExtract();
        FefuAdditionalProfessionalEducationProgram dpoProgramOld = getProperty(FefuAdditionalProfessionalEducationProgramForStudent.class, FefuAdditionalProfessionalEducationProgramForStudent.L_PROGRAM,
                                                                            FefuAdditionalProfessionalEducationProgramForStudent.L_STUDENT, extract.getEntity());
        if (dpoProgramOld != null)
        {
            extract.setFormativeOrgUnitStr(dpoProgramOld.getFormativeOrgUnit().getFullTitle());
            extract.setTerritorialOrgUnitStr(dpoProgramOld.getTerritorialOrgUnit().getTerritorialFullTitle());
            extract.setEducationLevelHighSchoolStr((String) dpoProgramOld.getEducationOrgUnit().getEducationLevelHighSchool().getProperty(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY));
            extract.setDevelopFormStr(dpoProgramOld.getDevelopForm().getTitle());
            extract.setDevelopConditionStr(dpoProgramOld.getDevelopCondition().getTitle());
            extract.setDevelopTechStr(dpoProgramOld.getDevelopTech().getTitle());
            extract.setDevelopPeriodStr(dpoProgramOld.getDevelopPeriod().getTitle());
        }

        update(extract);

    }
}