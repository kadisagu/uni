/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu7.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaSuccessWithDismissStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class DAO extends AbstractListParagraphPubDAO<FefuGiveDiplomaSuccessWithDismissStuListExtract, Model> implements IDAO
{
}
