/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.student.FefuForeignStudentDataEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

/**
 * @author Alexander Zhebko
 * @since 14.02.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStudentFefuExt(get(StudentFefuExt.class, StudentFefuExt.student().id(), model.getStudentId()));
        if(null == model.getStudentFefuExt())
        {
            model.setStudentFefuExt(new StudentFefuExt());
            model.getStudentFefuExt().setStudent(this.getNotNull(Student.class, model.getStudentId()));
        }
        if (model.getStudentFefuExt().getBaseEdu() != null)
            model.setBaseEdu(FefuStudentManager.instance().getBaseEduOption(model.getStudentFefuExt().getBaseEdu()));
        model.setBaseEduList(FefuStudentManager.instance().getBaseEduOptions());
    }

    @Override
    public void update(Model model)
    {
        StudentFefuExt studentFefuExt = model.getStudentFefuExt();
        if (model.getBaseEdu() != null)
            studentFefuExt.setBaseEdu(model.getBaseEdu().getId());
        else
            studentFefuExt.setBaseEdu(null);
        getSession().saveOrUpdate(studentFefuExt);
    }
}