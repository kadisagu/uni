package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сотрудник технической комиссии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuTechnicalCommissionEmployeeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee";
    public static final String ENTITY_NAME = "fefuTechnicalCommissionEmployee";
    public static final int VERSION_HASH = -304021473;
    private static IEntityMeta ENTITY_META;

    public static final String L_TECHNICAL_COMMISSION = "technicalCommission";
    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String P_ACCESS_LIMITATION = "accessLimitation";

    private FefuTechnicalCommission _technicalCommission;     // Техническая комиссия
    private EmployeePost _employeePost;     // Сотрудник
    private boolean _accessLimitation = true;     // Ограничение прав доступа к абитуриентам

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Техническая комиссия. Свойство не может быть null.
     */
    @NotNull
    public FefuTechnicalCommission getTechnicalCommission()
    {
        return _technicalCommission;
    }

    /**
     * @param technicalCommission Техническая комиссия. Свойство не может быть null.
     */
    public void setTechnicalCommission(FefuTechnicalCommission technicalCommission)
    {
        dirty(_technicalCommission, technicalCommission);
        _technicalCommission = technicalCommission;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Ограничение прав доступа к абитуриентам. Свойство не может быть null.
     */
    @NotNull
    public boolean isAccessLimitation()
    {
        return _accessLimitation;
    }

    /**
     * @param accessLimitation Ограничение прав доступа к абитуриентам. Свойство не может быть null.
     */
    public void setAccessLimitation(boolean accessLimitation)
    {
        dirty(_accessLimitation, accessLimitation);
        _accessLimitation = accessLimitation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuTechnicalCommissionEmployeeGen)
        {
            setTechnicalCommission(((FefuTechnicalCommissionEmployee)another).getTechnicalCommission());
            setEmployeePost(((FefuTechnicalCommissionEmployee)another).getEmployeePost());
            setAccessLimitation(((FefuTechnicalCommissionEmployee)another).isAccessLimitation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuTechnicalCommissionEmployeeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuTechnicalCommissionEmployee.class;
        }

        public T newInstance()
        {
            return (T) new FefuTechnicalCommissionEmployee();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "technicalCommission":
                    return obj.getTechnicalCommission();
                case "employeePost":
                    return obj.getEmployeePost();
                case "accessLimitation":
                    return obj.isAccessLimitation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "technicalCommission":
                    obj.setTechnicalCommission((FefuTechnicalCommission) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "accessLimitation":
                    obj.setAccessLimitation((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "technicalCommission":
                        return true;
                case "employeePost":
                        return true;
                case "accessLimitation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "technicalCommission":
                    return true;
                case "employeePost":
                    return true;
                case "accessLimitation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "technicalCommission":
                    return FefuTechnicalCommission.class;
                case "employeePost":
                    return EmployeePost.class;
                case "accessLimitation":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuTechnicalCommissionEmployee> _dslPath = new Path<FefuTechnicalCommissionEmployee>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuTechnicalCommissionEmployee");
    }
            

    /**
     * @return Техническая комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee#getTechnicalCommission()
     */
    public static FefuTechnicalCommission.Path<FefuTechnicalCommission> technicalCommission()
    {
        return _dslPath.technicalCommission();
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Ограничение прав доступа к абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee#isAccessLimitation()
     */
    public static PropertyPath<Boolean> accessLimitation()
    {
        return _dslPath.accessLimitation();
    }

    public static class Path<E extends FefuTechnicalCommissionEmployee> extends EntityPath<E>
    {
        private FefuTechnicalCommission.Path<FefuTechnicalCommission> _technicalCommission;
        private EmployeePost.Path<EmployeePost> _employeePost;
        private PropertyPath<Boolean> _accessLimitation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Техническая комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee#getTechnicalCommission()
     */
        public FefuTechnicalCommission.Path<FefuTechnicalCommission> technicalCommission()
        {
            if(_technicalCommission == null )
                _technicalCommission = new FefuTechnicalCommission.Path<FefuTechnicalCommission>(L_TECHNICAL_COMMISSION, this);
            return _technicalCommission;
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Ограничение прав доступа к абитуриентам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee#isAccessLimitation()
     */
        public PropertyPath<Boolean> accessLimitation()
        {
            if(_accessLimitation == null )
                _accessLimitation = new PropertyPath<Boolean>(FefuTechnicalCommissionEmployeeGen.P_ACCESS_LIMITATION, this);
            return _accessLimitation;
        }

        public Class getEntityClass()
        {
            return FefuTechnicalCommissionEmployee.class;
        }

        public String getEntityName()
        {
            return "fefuTechnicalCommissionEmployee";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
