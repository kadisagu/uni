package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x11x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.11.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuTrJournalExt

        SQLUpdateQuery upd = new SQLUpdateQuery("tr_journal", "j");
        upd.set("gradescale_id", "ext.gradescale_id");
        upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("fefutrjournalext_t", "ext"), "ext.trjournal_id = j.id");

        int n = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));
        if (n > 0) tool.info(n + " gradeScales moved from fefuTrJournalExt to TrJournal");

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("fefutrjournalext_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("fefuTrJournalExt");
        }
    }
}