/* $Id: JuridicalContactorAddClickSaveOuAction.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.ext.JuridicalContactor.ui.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.Edit.ExternalOrgUnitEditUI;
import org.tandemframework.shared.ctr.base.bo.JuridicalContactor.ui.Add.JuridicalContactorAddUI;
import ru.tandemservice.unifefu.base.ext.ExternalOrgUnit.ui.Edit.ExternalOrgUnitEditExt;
import ru.tandemservice.unifefu.base.ext.ExternalOrgUnit.ui.Edit.ExternalOrgUnitEditExtUI;


/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 12/21/12
 * Time: 7:19 PM
 */
public class JuridicalContactorAddClickSaveOuAction extends NamedUIAction
{
    protected JuridicalContactorAddClickSaveOuAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if(presenter instanceof JuridicalContactorAddUI)
        {
            JuridicalContactorAddUI prsnt = (JuridicalContactorAddUI) presenter;
            if(StringUtils.isEmpty(prsnt.getOuDTO().getShortTitle()))
                        prsnt.getOuDTO().setShortTitle(prsnt.getOuDTO().getTitle());
            prsnt.onClickSaveOu();
        }
    }
}
