/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuOrderContingentStuDPOListExtract, Model>
{
}