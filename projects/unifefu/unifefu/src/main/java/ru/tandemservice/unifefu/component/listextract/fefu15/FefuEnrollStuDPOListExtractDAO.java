/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu15;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.AdditionalProfessionalEducationProgramForStudentManager;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public class FefuEnrollStuDPOListExtractDAO extends UniBaseDao implements IExtractComponentDao<FefuEnrollStuDPOListExtract>
{
    /**
     * Внесение выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doCommit(FefuEnrollStuDPOListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getEduEnrollmentOrderDate());
            extract.setPrevOrderNumber(orderData.getEduEnrollmentOrderNumber());
        }
        orderData.setEduEnrollmentOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setEduEnrollmentOrderNumber(extract.getParagraph().getOrder().getNumber());

        //сохраняем данные выписки
        extract.setStudentStatusOld(student.getStatus());
        extract.getEntity().setStatus(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));

        FefuAdditionalProfessionalEducationProgramForStudent programForStudent =
                AdditionalProfessionalEducationProgramForStudentManager.instance().apeProgramForStudentDao().getApeProgramForStudentByStudentId(student.getId());
        if (programForStudent == null)
        {
            programForStudent = new FefuAdditionalProfessionalEducationProgramForStudent();
            programForStudent.setStudent(student);
        }
        programForStudent.setProgram(extract.getDpoProgramNew());
        saveOrUpdate(programForStudent);
        saveOrUpdate(orderData);
    }

    /**
     * Отмена вносимых выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doRollback(FefuEnrollStuDPOListExtract extract, Map parameters)
    {
        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setEduEnrollmentOrderDate(extract.getPrevOrderDate());
        orderData.setEduEnrollmentOrderNumber(extract.getPrevOrderNumber());

        extract.getEntity().setStatus(extract.getStudentStatusOld());
        FefuAdditionalProfessionalEducationProgramForStudent programForStudent =
                AdditionalProfessionalEducationProgramForStudentManager.instance().apeProgramForStudentDao().getApeProgramForStudentByStudentId(student.getId());
        if (programForStudent != null)
        {
            programForStudent.setProgram(extract.getDpoProgramOld());
            update(programForStudent);
        }
        getSession().saveOrUpdate(orderData);
    }
}