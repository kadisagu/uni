package ru.tandemservice.unifefu.dao.daemon;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.mdbio.AddressIODao;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.PersonFefuExt;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.entity.ws.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 08.02.14
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("deprecation")
public class FEFUAllStudExportDaemonDAO extends UniBaseDao implements IFEFUAllStudExportDaemonDAO
{
    public static final int DAEMON_ITERATION_TIME = 240;
    private static String ADDED_UPDATED_ENTITY_NAME = "";
    private static final Set<Long> ADDED_UPDATED_ENTITY_IDS = Collections.synchronizedSet(new HashSet<Long>());
    private static boolean manual_locked = false;

    public static final SyncDaemon DAEMON = new SyncDaemon(FEFUAllStudExportDaemonDAO.class.getName(), DAEMON_ITERATION_TIME, IFEFUAllStudExportDaemonDAO.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            if (manual_locked) return;

            final IFEFUAllStudExportDaemonDAO dao = IFEFUAllStudExportDaemonDAO.instance.get();

            // отключаем логирование в базу и прочие системные штуки
            final IEventServiceLock eventLock = CoreServices.eventService().lock();
            Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */
            try
            {
                if (ADDED_UPDATED_ENTITY_IDS.isEmpty())
                {
                    dao.doExportStudents();
                } else
                {
                    dao.doExportChildEntities();
                }


            } catch (final Exception t)
            {
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            } finally
            {
                // включаем штуки и логирование обратно
                Debug.resumeLogging();
                eventLock.release();
            }
        }
    };

    public static void lockDaemon()
    {
        manual_locked = true;
    }

    public static void unlockDaemon()
    {
        manual_locked = false;
    }

    public static boolean isLocked()
    {
        return manual_locked;
    }

    public void doExportChildEntities()
    {
        Set<Long> tempSet = new HashSet<>();
        tempSet.addAll(ADDED_UPDATED_ENTITY_IDS);
        ADDED_UPDATED_ENTITY_IDS.clear();

        if (ADDED_UPDATED_ENTITY_NAME.equals(MdbViewAddress.ENTITY_NAME))
        {
            ADDED_UPDATED_ENTITY_NAME = "";
            doFillAddress(tempSet);
        }

        if (ADDED_UPDATED_ENTITY_NAME.equals(MdbViewPersonForeignlanguage.ENTITY_NAME))
        {
            ADDED_UPDATED_ENTITY_NAME = "";
            doFillPersonForeignLanguage(tempSet);
        }

        if (ADDED_UPDATED_ENTITY_NAME.equals(MdbViewPersonEduinstitution.ENTITY_NAME))
        {
            ADDED_UPDATED_ENTITY_NAME = "";
            doFillPersonEduInstitution(tempSet);
        }

        if (ADDED_UPDATED_ENTITY_NAME.equals(MdbViewPersonSportachievement.ENTITY_NAME))
        {
            ADDED_UPDATED_ENTITY_NAME = "";
            doFillPersonSportAchievement(tempSet);
        }

        if (ADDED_UPDATED_ENTITY_NAME.equals(MdbViewPersonBenefit.ENTITY_NAME))
        {
            ADDED_UPDATED_ENTITY_NAME = "";
            doFillPersonBenefit(tempSet);
        }

        if (ADDED_UPDATED_ENTITY_NAME.equals(MdbViewPersonNextofkin.ENTITY_NAME))
        {
            ADDED_UPDATED_ENTITY_NAME = "";
            doFillPersonNextOfKin(tempSet);
        }

        if (ADDED_UPDATED_ENTITY_NAME.equals(MdbViewPersonAdditionalData.ENTITY_NAME))
        {
            ADDED_UPDATED_ENTITY_NAME = "";
            doFillPersonAdditionalData(tempSet);
        }

        if (ADDED_UPDATED_ENTITY_NAME.equals(MdbViewStudentCustomState.ENTITY_NAME))
        {
            ADDED_UPDATED_ENTITY_NAME = "";
            doFillStudentCustomState(tempSet);
        }

        if (ADDED_UPDATED_ENTITY_NAME.equals(MdbViewStudentAdditionalData.ENTITY_NAME))
        {
            ADDED_UPDATED_ENTITY_NAME = "";
            doFillStudentAdditionalData(tempSet);
        }

    }

    public void doRegisterEntity(String entityName, Collection<Long> entityIds)
    {
        ADDED_UPDATED_ENTITY_IDS.clear();
        ADDED_UPDATED_ENTITY_IDS.addAll(entityIds);
        ADDED_UPDATED_ENTITY_NAME = entityName;
    }


    public void doDeleteAllEntities()
    {
        final Session session = this.getSession();

        DQLDeleteBuilder deleteBuilder1 = new DQLDeleteBuilder(MdbViewStudentAdditionalData.class);
        deleteBuilder1.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder2 = new DQLDeleteBuilder(MdbViewStudentCustomState.class);
        deleteBuilder2.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder3 = new DQLDeleteBuilder(MdbViewStudent.class);
        deleteBuilder3.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder4 = new DQLDeleteBuilder(MdbViewPersonAdditionalData.class);
        deleteBuilder4.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder5 = new DQLDeleteBuilder(MdbViewPersonBenefit.class);
        deleteBuilder5.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder6 = new DQLDeleteBuilder(MdbViewPersonSportachievement.class);
        deleteBuilder6.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder7 = new DQLDeleteBuilder(MdbViewPersonForeignlanguage.class);
        deleteBuilder7.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder8 = new DQLDeleteBuilder(MdbViewPersonEduinstitution.class);
        deleteBuilder8.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder9 = new DQLDeleteBuilder(MdbViewPersonNextofkin.class);
        deleteBuilder9.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder10 = new DQLDeleteBuilder(MdbViewPerson.class);
        deleteBuilder10.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder11 = new DQLDeleteBuilder(MdbViewAddress.class);
        deleteBuilder11.createStatement(session).execute();

        session.flush();
        session.clear();
    }

    private void doFillPersonEduInstitution(Collection<Long> ids)
    {
        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(PersonEduInstitution.class, "pe")
                .joinEntity("pe", DQLJoinType.inner, MdbViewPerson.class, "mp",
                        eq(property(MdbViewPerson.person().id().fromAlias("mp")), property(PersonEduInstitution.person().id().fromAlias("pe"))))
                .column(property("pe"))
                .column(property("mp"))
                .where(in(property(PersonEduInstitution.id().fromAlias("pe")), ids));

        List<Object[]> list = eBuilder.createStatement(getSession()).list();
        for (Object[] item : list)
        {
            PersonEduInstitution eduInstitution = (PersonEduInstitution) item[0];
            MdbViewPerson mdbPerson = (MdbViewPerson) item[1];

            MdbViewPersonEduinstitution newData = new MdbViewPersonEduinstitution();

            newData.setMdbViewPerson(mdbPerson);
            newData.setPersonEduInstitution(eduInstitution);

            newData.setEduInstitution(eduInstitution.getEduInstitution());

            newData.setEducationalInstitutionCountry(AddressIODao.getCountryString(eduInstitution.getAddressItem().getCountry()));
            newData.setEducationalInstitutionSettlement(AddressIODao.getSettlementString(eduInstitution.getAddressItem()));
            newData.setEducationalInstitutionTypeKind(eduInstitution.getEduInstitutionKind().getTitle());
            newData.setEducationDocumentType(eduInstitution.getDocumentType().getTitle());
            newData.setEducationLevelStage(eduInstitution.getEducationLevelStage().getTitle());

            newData.setDate(eduInstitution.getIssuanceDate());
            newData.setEmployeeSpeciality(null == eduInstitution.getEmployeeSpeciality() ? null : eduInstitution.getEmployeeSpeciality().getTitle());
            newData.setGraduationHonour(null == eduInstitution.getGraduationHonour() ? null : eduInstitution.getGraduationHonour().getTitle());
            newData.setDiplomaQualification(null == eduInstitution.getDiplomaQualification() ? null : eduInstitution.getDiplomaQualification().getTitle());
            newData.setQualification(eduInstitution.getQualification());
            newData.setMarks((Math.max(0, (null == eduInstitution.getMark5() ? 0 : eduInstitution.getMark5())) + "," + Math.max(0, (null == eduInstitution.getMark4() ? 0 : eduInstitution.getMark4())) + "," + Math.max(0, (null == eduInstitution.getMark3() ? 0 : eduInstitution.getMark3()))));
            newData.setSeria(StringUtils.trimToEmpty(eduInstitution.getSeria()));
            newData.setNumber(StringUtils.trimToEmpty(eduInstitution.getNumber()));

            newData.setRegionCode(eduInstitution.getRegionCode());
            newData.setYearEnd(eduInstitution.getYearEnd());

            save(newData);
        }
    }

    private void export_PersonEduInstitution(Collection<Long> personIds, Map<Long, MdbViewPerson> mdbPersonList)
    {
        final Session session = this.getSession();
        final List<Long> ids = new DQLSelectBuilder().fromEntity(PersonEduInstitution.class, "e").column(DQLExpressions.property("e.id"))
                .where(in(property(PersonEduInstitution.person().id().fromAlias("e")), personIds))
                .createStatement(session).list();
        try
        {
            for (final PersonEduInstitution eduInstitution : FEFUAllStudExportDaemonDAO.this.getList(PersonEduInstitution.class, "id", ids))
            {
                MdbViewPersonEduinstitution newData = new MdbViewPersonEduinstitution();

                newData.setMdbViewPerson(mdbPersonList.get(eduInstitution.getPerson().getId()));
                newData.setPersonEduInstitution(eduInstitution);

                newData.setEduInstitution(eduInstitution.getEduInstitution());

                newData.setEducationalInstitutionCountry(AddressIODao.getCountryString(eduInstitution.getAddressItem().getCountry()));
                newData.setEducationalInstitutionSettlement(AddressIODao.getSettlementString(eduInstitution.getAddressItem()));
                newData.setEducationalInstitutionTypeKind(eduInstitution.getEduInstitutionKind().getTitle());
                newData.setEducationDocumentType(eduInstitution.getDocumentType().getTitle());
                newData.setEducationLevelStage(eduInstitution.getEducationLevelStage().getTitle());

                newData.setDate(eduInstitution.getIssuanceDate());
                newData.setEmployeeSpeciality(null == eduInstitution.getEmployeeSpeciality() ? null : eduInstitution.getEmployeeSpeciality().getTitle());
                newData.setGraduationHonour(null == eduInstitution.getGraduationHonour() ? null : eduInstitution.getGraduationHonour().getTitle());
                newData.setDiplomaQualification(null == eduInstitution.getDiplomaQualification() ? null : eduInstitution.getDiplomaQualification().getTitle());
                newData.setQualification(eduInstitution.getQualification());
                newData.setMarks((Math.max(0, (null == eduInstitution.getMark5() ? 0 : eduInstitution.getMark5())) + "," + Math.max(0, (null == eduInstitution.getMark4() ? 0 : eduInstitution.getMark4())) + "," + Math.max(0, (null == eduInstitution.getMark3() ? 0 : eduInstitution.getMark3()))));
                newData.setSeria(StringUtils.trimToEmpty(eduInstitution.getSeria()));
                newData.setNumber(StringUtils.trimToEmpty(eduInstitution.getNumber()));

                newData.setRegionCode(eduInstitution.getRegionCode());
                newData.setYearEnd(eduInstitution.getYearEnd());

                session.save(newData);
            }
        } catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }

    private void doFillPersonSportAchievement(Collection<Long> ids)
    {
        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(PersonSportAchievement.class, "pa")
                .joinEntity("pa", DQLJoinType.inner, MdbViewPerson.class, "mp",
                        eq(property(MdbViewPerson.person().id().fromAlias("mp")), property(PersonSportAchievement.person().id().fromAlias("pa"))))
                .column(property("pa"))
                .column(property("mp"))
                .where(in(property(PersonSportAchievement.id().fromAlias("pa")), ids));

        List<Object[]> list = eBuilder.createStatement(getSession()).list();
        for (Object[] item : list)
        {
            PersonSportAchievement sportAchievement = (PersonSportAchievement) item[0];
            MdbViewPerson mdbPerson = (MdbViewPerson) item[1];

            MdbViewPersonSportachievement newData = new MdbViewPersonSportachievement();

            newData.setPersonSportAchievement(sportAchievement);
            newData.setMdbViewPerson(mdbPerson);
            newData.setSportType(sportAchievement.getSportKind().getTitle());
            newData.setSportRank(sportAchievement.getSportRank().getTitle());
            newData.setBestachievement(sportAchievement.getBestAchievement());


            save(newData);
        }
    }

    private void export_PersonSportAchievement(Collection<Long> personIds, Map<Long, MdbViewPerson> mdbPersonList)
    {
        final Session session = this.getSession();
        final List<Long> ids = new DQLSelectBuilder().fromEntity(PersonSportAchievement.class, "e").column(DQLExpressions.property("e.id"))
                .where(in(property(PersonSportAchievement.person().id().fromAlias("e")), personIds))
                .createStatement(session).list();
        try
        {
            for (final PersonSportAchievement sportAchievement : FEFUAllStudExportDaemonDAO.this.getList(PersonSportAchievement.class, "id", ids))
            {
                MdbViewPersonSportachievement newData = new MdbViewPersonSportachievement();

                newData.setPersonSportAchievement(sportAchievement);
                newData.setMdbViewPerson(mdbPersonList.get(sportAchievement.getPerson().getId()));
                newData.setSportType(sportAchievement.getSportKind().getTitle());
                newData.setSportRank(sportAchievement.getSportRank().getTitle());
                newData.setBestachievement(sportAchievement.getBestAchievement());
                session.save(newData);
            }
        } catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }

    public void doFillAddress(Collection<Long> ids)
    {
        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(AddressBase.class, "ab")
                .joinEntity("ab", DQLJoinType.left, MdbViewAddress.class, "ma",
                        eq(property(MdbViewAddress.addressBase().id().fromAlias("ma")), property(AddressBase.id().fromAlias("ab"))))
                .joinEntity("ab", DQLJoinType.left, IdentityCard.class, "idc", eq(property(IdentityCard.address().id().fromAlias("idc")), property(AddressBase.id().fromAlias("ab"))))
                .joinEntity("ab", DQLJoinType.left, Person.class, "p",
                        or(eq(property(Person.address().id().fromAlias("p")), property(AddressBase.id().fromAlias("ab"))),
                                eq(property(Person.identityCard().fromAlias("p")), property("idc")))
                )

                .joinEntity("p", DQLJoinType.left, MdbViewPerson.class, "mp",
                        eq(property(MdbViewPerson.person().id().fromAlias("mp")), property(Person.id().fromAlias("p"))))
                .column(property("ab"))
                .column(property("ma"))
                .column(property("p"))
                .column(property("mp"))
                .distinct()
                .where(in(property(AddressBase.id().fromAlias("ab")), ids));

        List<Object[]> list = eBuilder.createStatement(getSession()).list();
        for (Object[] item : list)
        {
            AddressBase addressBase = (AddressBase) item[0];
            MdbViewAddress mdbViewAddress = (MdbViewAddress) item[1];
            //если такого адреса нет, то обновляем данные по персоне
            if (mdbViewAddress == null)
            {
                Person person = (Person) item[2];
                MdbViewPerson mdbViewPerson = (MdbViewPerson) item[3];

                if (mdbViewPerson != null)
                {
                    AddressBase regAddress = person.getIdentityCard().getAddress();
                    AddressBase factAddress = person.getAddress();

                    mdbViewPerson.setAddressRegId(doSaveMdbViewAddress(regAddress));
                    mdbViewPerson.setAddressFactId(doSaveMdbViewAddress(factAddress));
                    saveOrUpdate(mdbViewPerson);
                }
            } else
            //если адрес уже есть в выгрузке - обновляем строку
            {
                mdbViewAddress.setAdress(addressBase.getTitleWithFlat());
                saveOrUpdate(mdbViewAddress);
            }

        }
    }


    public void doFillPersonForeignLanguage(Collection<Long> ids)
    {
        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(PersonForeignLanguage.class, "pfl")
                .joinEntity("pfl", DQLJoinType.inner, MdbViewPerson.class, "mp",
                        eq(property(MdbViewPerson.person().id().fromAlias("mp")), property(PersonForeignLanguage.person().id().fromAlias("pfl"))))
                .column(property("pfl"))
                .column(property("mp"))
                .where(in(property(PersonForeignLanguage.id().fromAlias("pfl")), ids));

        List<Object[]> list = eBuilder.createStatement(getSession()).list();
        for (Object[] item : list)
        {
            PersonForeignLanguage foreignLanguage = (PersonForeignLanguage) item[0];
            MdbViewPerson mdbPerson = (MdbViewPerson) item[1];

            MdbViewPersonForeignlanguage newData = new MdbViewPersonForeignlanguage();
            newData.setPersonForeignLanguage(foreignLanguage);
            newData.setMdbViewPerson(mdbPerson);
            newData.setLanguage(foreignLanguage.getLanguage().getTitle());
            newData.setSkill(null == foreignLanguage.getSkill() ? null : foreignLanguage.getSkill().getTitle());
            save(newData);
        }
    }

    private void export_PersonForeignLanguage(Collection<Long> personIds, Map<Long, MdbViewPerson> mdbPersonList)
    {
        final Session session = this.getSession();
        final List<Long> ids = new DQLSelectBuilder().fromEntity(PersonForeignLanguage.class, "e").column(DQLExpressions.property("e.id"))
                .where(in(property(PersonForeignLanguage.person().id().fromAlias("e")), personIds))
                .createStatement(session).list();
        try
        {
            for (final PersonForeignLanguage foreignLanguage : FEFUAllStudExportDaemonDAO.this.getList(PersonForeignLanguage.class, "id", ids))
            {
                MdbViewPersonForeignlanguage newData = new MdbViewPersonForeignlanguage();
                newData.setPersonForeignLanguage(foreignLanguage);
                newData.setMdbViewPerson(mdbPersonList.get(foreignLanguage.getPerson().getId()));
                newData.setLanguage(foreignLanguage.getLanguage().getTitle());
                newData.setSkill(null == foreignLanguage.getSkill() ? null : foreignLanguage.getSkill().getTitle());
                session.save(newData);
            }
        } catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }

    private void doFillPersonBenefit(Collection<Long> ids)
    {
        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(PersonBenefit.class, "pb")
                .joinEntity("pb", DQLJoinType.inner, MdbViewPerson.class, "mp",
                        eq(property(MdbViewPerson.person().id().fromAlias("mp")), property(PersonBenefit.person().id().fromAlias("pb"))))
                .column(property("pb"))
                .column(property("mp"))
                .where(in(property(PersonBenefit.id().fromAlias("pb")), ids));

        List<Object[]> list = eBuilder.createStatement(getSession()).list();
        for (Object[] item : list)
        {
            PersonBenefit benefit = (PersonBenefit) item[0];
            MdbViewPerson mdbPerson = (MdbViewPerson) item[1];

            MdbViewPersonBenefit newData = new MdbViewPersonBenefit();
            newData.setPersonBenefit(benefit);
            newData.setMdbViewPerson(mdbPerson);
            newData.setBenefit(benefit.getBenefit().getTitle());
            newData.setBasic(benefit.getBasic());
            newData.setDate(benefit.getDate());
            newData.setDocument(benefit.getDocument());

            save(newData);
        }
    }

    private void export_PersonBenefit(Collection<Long> personIds, Map<Long, MdbViewPerson> mdbPersonList)
    {
        final Session session = this.getSession();
        final List<Long> ids = new DQLSelectBuilder().fromEntity(PersonBenefit.class, "e").column(DQLExpressions.property("e.id"))
                .where(in(property(PersonBenefit.person().id().fromAlias("e")), personIds))
                .createStatement(session).list();
        try
        {
            for (final PersonBenefit benefit : FEFUAllStudExportDaemonDAO.this.getList(PersonBenefit.class, "id", ids))
            {
                MdbViewPersonBenefit newData = new MdbViewPersonBenefit();
                newData.setPersonBenefit(benefit);
                newData.setMdbViewPerson(mdbPersonList.get(benefit.getPerson().getId()));
                newData.setBenefit(benefit.getBenefit().getTitle());
                newData.setBasic(benefit.getBasic());
                newData.setDate(benefit.getDate());
                newData.setDocument(benefit.getDocument());
                session.save(newData);
            }
        } catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }

    private void doFillPersonAdditionalData(Collection<Long> ids)
    {
        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(PersonFefuExt.class, "pf")
                .joinEntity("pf", DQLJoinType.inner, MdbViewPerson.class, "mp",
                        eq(property(MdbViewPerson.person().id().fromAlias("mp")), property(PersonFefuExt.person().id().fromAlias("pf"))))
                .column(property("pf"))
                .column(property("mp"))
                .where(in(property(PersonFefuExt.id().fromAlias("pf")), ids));

        List<Object[]> list = eBuilder.createStatement(getSession()).list();
        for (Object[] item : list)
        {
            PersonFefuExt personExt = (PersonFefuExt) item[0];
            MdbViewPerson mdbPerson = (MdbViewPerson) item[1];

            MdbViewPersonAdditionalData newData = new MdbViewPersonAdditionalData();
            newData.setPersonFefuExt(personExt);
            newData.setMdbViewPerson(mdbPerson);
            newData.setCommentVip(personExt.getCommentVip());
            newData.setFioCuratorVip(personExt.getFioCuratorVip());
            newData.setVipDate(personExt.getVipDate());

            save(newData);
        }
    }

    private void export_PersonAdditionalData(Collection<Long> personIds, Map<Long, MdbViewPerson> mdbPersonList)
    {
        final Session session = this.getSession();
        final List<Long> ids = new DQLSelectBuilder().fromEntity(PersonFefuExt.class, "e").column(DQLExpressions.property("e.id"))
                .where(in(property(PersonFefuExt.person().id().fromAlias("e")), personIds))
                .createStatement(session).list();
        try
        {
            for (final PersonFefuExt personExt : FEFUAllStudExportDaemonDAO.this.getList(PersonFefuExt.class, "id", ids))
            {
                MdbViewPersonAdditionalData newData = new MdbViewPersonAdditionalData();
                newData.setPersonFefuExt(personExt);
                newData.setMdbViewPerson(mdbPersonList.get(personExt.getPerson().getId()));
                newData.setCommentVip(personExt.getCommentVip());
                newData.setFioCuratorVip(personExt.getFioCuratorVip());
                newData.setVipDate(personExt.getVipDate());
                session.save(newData);
            }
        } catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }

    private MdbViewAddress doSaveMdbViewAddress(AddressBase address)
    {
        if (address == null)
        {
            return null;
        }

        MdbViewAddress newData = new MdbViewAddress();
        newData.setAddressBase(address);
        newData.setAdress(address.getTitleWithFlat());
        save(newData);

        return newData;
    }

    private void doFillPersonNextOfKin(Collection<Long> ids)
    {
        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(PersonNextOfKin.class, "pk")
                .joinEntity("pk", DQLJoinType.inner, MdbViewPerson.class, "mp",
                        eq(property(MdbViewPerson.person().id().fromAlias("mp")), property(PersonNextOfKin.person().id().fromAlias("pk"))))
                .column(property("pk"))
                .column(property("mp"))
                .where(in(property(PersonNextOfKin.id().fromAlias("pk")), ids));

        List<Object[]> list = eBuilder.createStatement(getSession()).list();
        for (Object[] item : list)
        {
            PersonNextOfKin nextOfKin = (PersonNextOfKin) item[0];
            MdbViewPerson mdbPerson = (MdbViewPerson) item[1];

            MdbViewPersonNextofkin newData = new MdbViewPersonNextofkin();
            newData.setPersonNextOfKin(nextOfKin);
            newData.setMdbViewPerson(mdbPerson);
            newData.setRelationDegree(nextOfKin.getRelationDegree().getTitle());

            newData.setIdentityBirthDate(nextOfKin.getBirthDate());
            newData.setIdentityCode(StringUtils.trimToNull(nextOfKin.getPassportIssuanceCode()));
            newData.setIdentityDate(nextOfKin.getPassportIssuanceDate());
            newData.setIdentityFirstName(StringUtils.trimToNull(nextOfKin.getFirstname()));
            newData.setIdentityLastName(StringUtils.trimToNull(nextOfKin.getLastname()));
            newData.setIdentityMiddleName(StringUtils.trimToNull(nextOfKin.getPatronymic()));
            newData.setIdentityNumber(StringUtils.trimToNull(nextOfKin.getPassportNumber()));
            newData.setIdentityPlace(StringUtils.trimToNull(nextOfKin.getPassportIssuancePlace()));
            newData.setIdentitySeria(StringUtils.trimToNull(nextOfKin.getPassportSeria()));


            AddressBase address = nextOfKin.getAddress();
            newData.setAddressId(doSaveMdbViewAddress(address));

            newData.setEmployeePlace(StringUtils.trimToNull(nextOfKin.getEmploymentPlace()));
            newData.setPhones(StringUtils.trimToNull(nextOfKin.getPhones()));

            save(newData);
        }
    }

    private void export_PersonNextOfKin(Collection<Long> personIds, Map<Long, MdbViewPerson> mdbPersonList)
    {
        final Session session = this.getSession();
        final List<Long> ids = new DQLSelectBuilder().fromEntity(PersonNextOfKin.class, "e").column(DQLExpressions.property("e.id"))
                .where(in(property(PersonNextOfKin.person().id().fromAlias("e")), personIds))
                .createStatement(session).list();
        try
        {
            for (final PersonNextOfKin nextOfKin : FEFUAllStudExportDaemonDAO.this.getList(PersonNextOfKin.class, "id", ids))
            {
                MdbViewPersonNextofkin newData = new MdbViewPersonNextofkin();
                newData.setPersonNextOfKin(nextOfKin);
                newData.setMdbViewPerson(mdbPersonList.get(nextOfKin.getPerson().getId()));
                newData.setRelationDegree(nextOfKin.getRelationDegree().getTitle());

                newData.setIdentityBirthDate(nextOfKin.getBirthDate());
                newData.setIdentityCode(StringUtils.trimToNull(nextOfKin.getPassportIssuanceCode()));
                newData.setIdentityDate(nextOfKin.getPassportIssuanceDate());
                newData.setIdentityFirstName(StringUtils.trimToNull(nextOfKin.getFirstname()));
                newData.setIdentityLastName(StringUtils.trimToNull(nextOfKin.getLastname()));
                newData.setIdentityMiddleName(StringUtils.trimToNull(nextOfKin.getPatronymic()));
                newData.setIdentityNumber(StringUtils.trimToNull(nextOfKin.getPassportNumber()));
                newData.setIdentityPlace(StringUtils.trimToNull(nextOfKin.getPassportIssuancePlace()));
                newData.setIdentitySeria(StringUtils.trimToNull(nextOfKin.getPassportSeria()));

                AddressBase address = nextOfKin.getAddress();
                newData.setAddressId(doSaveMdbViewAddress(address));
                newData.setEmployeePlace(StringUtils.trimToNull(nextOfKin.getEmploymentPlace()));
                newData.setPhones(StringUtils.trimToNull(nextOfKin.getPhones()));


                session.save(newData);
            }
        } catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }


    private void doFillStudentCustomState(Collection<Long> ids)
    {
        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(StudentCustomState.class, "sc")
                .joinEntity("sc", DQLJoinType.inner, MdbViewStudent.class, "ms",
                        eq(property(MdbViewStudent.student().id().fromAlias("ms")), property(StudentCustomState.student().id().fromAlias("sc"))))
                .column(property("sc"))
                .column(property("ms"))
                .where(in(property(StudentCustomState.id().fromAlias("sc")), ids));

        List<Object[]> list = eBuilder.createStatement(getSession()).list();
        for (Object[] item : list)
        {
            StudentCustomState state = (StudentCustomState) item[0];
            MdbViewStudent mdbStudent = (MdbViewStudent) item[1];

            MdbViewStudentCustomState newData = new MdbViewStudentCustomState();
            newData.setStudentCustomState(state);
            newData.setMdbViewStudent(mdbStudent);
            newData.setBeginDate(state.getBeginDate());
            newData.setCustomState(state.getCustomState().getTitle());
            newData.setDescription(state.getDescription());
            newData.setEndDate(state.getEndDate());

            save(newData);
        }
    }

    private void export_StudentCustomState(Collection<Long> studentIds, Map<Long, MdbViewStudent> mdbStudentList)
    {
        final Session session = this.getSession();

        final List<Long> ids = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "e").column(DQLExpressions.property("e.id"))
                .where(in(property(StudentCustomState.student().id().fromAlias("e")), studentIds))
                .createStatement(session).list();
        try
        {
            for (final StudentCustomState state : FEFUAllStudExportDaemonDAO.this.getList(StudentCustomState.class, "id", ids))
            {
                MdbViewStudentCustomState newData = new MdbViewStudentCustomState();
                newData.setStudentCustomState(state);
                newData.setMdbViewStudent(mdbStudentList.get(state.getStudent().getId()));
                newData.setBeginDate(state.getBeginDate());
                newData.setCustomState(state.getCustomState().getTitle());
                newData.setDescription(state.getDescription());
                newData.setEndDate(state.getEndDate());
                session.save(newData);
            }
        } catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }

    private void doFillStudentAdditionalData(Collection<Long> ids)
    {
        DQLSelectBuilder eBuilder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                                    /* [0] */.column(property(Student.id().fromAlias("s")))
                                    /* [1] */.column(property("ext"))
                                    /* [2] */.column(property("ms"))
                                    /* [3] */.column(property("contactData"))
                .joinEntity("s", DQLJoinType.inner, StudentFefuExt.class, "ext",
                        eq(property(Student.id().fromAlias("s")), property(StudentFefuExt.student().id().fromAlias("ext"))))
                .joinEntity("s", DQLJoinType.inner, MdbViewStudent.class, "ms",
                        eq(property(MdbViewStudent.student().id().fromAlias("ms")), property(Student.id().fromAlias("s"))))
                .joinPath(DQLJoinType.left, Student.person().contactData().fromAlias("s"), "contactData")
                .where(in(property(StudentFefuExt.id().fromAlias("ext")), ids));

        List<Object[]> list = eBuilder.createStatement(getSession()).list();
        for (Object[] item : list)
        {
            MdbViewStudent mdbStudent = (MdbViewStudent) item[2];
            Long id = (Long) item[0];
            StudentFefuExt studentFefuExt = (StudentFefuExt) item[1];
            String integrationId = studentFefuExt.getIntegrationId();
            PersonContactData contactData = (PersonContactData) item[3];
            String email = contactData.getEmail();
            String allPhones = contactData.getAllPhones();
            if (integrationId != null || email != null || StringUtils.isNotEmpty(allPhones))
            {
                MdbViewStudentAdditionalData newData = new MdbViewStudentAdditionalData();
                newData.setStudentFefuExt(studentFefuExt);
                newData.setMdbViewStudent(mdbStudent);
                newData.setIntegrationId(integrationId);
                newData.setEmail(email);
                newData.setPhones(allPhones);
                save(newData);
            }
        }
    }

    private void export_StudentAdditionalData(Collection<Long> studentIds, Map<Long, MdbViewStudent> mdbStudentList)
    {
        final Session session = this.getSession();

        final IDQLStatement stmt = new DQLSelectBuilder().fromEntity(Student.class, "s")
                            /* [0] */.column(property(Student.id().fromAlias("s")))
                            /* [1] */.column(property("ext"))
                            /* [2] */.column(caseExpr(
                new IDQLExpression[]{in(property("s.id"), studentIds)},
                new IDQLExpression[]{value(true)}, value(false)))
                            /* [3] */.column(property("contactData"))
                .joinEntity("s", DQLJoinType.left, StudentFefuExt.class, "ext",
                        eq(property(Student.id().fromAlias("s")), property(StudentFefuExt.student().id().fromAlias("ext"))))
                .joinPath(DQLJoinType.left, Student.person().contactData().fromAlias("s"), "contactData")
                .where(in(property(Student.id().fromAlias("s")), studentIds))
                .createStatement(session);
        try
        {
            int c = 0;
            List<Object[]> list = stmt.list();
            for (Object[] item : list)
            {
                c++;
                Long id = (Long) item[0];
                StudentFefuExt studentFefuExt = (StudentFefuExt) item[1];
                String integrationId = studentFefuExt == null ? null : studentFefuExt.getIntegrationId();
                PersonContactData contactData = (PersonContactData) item[3];
                String email = contactData.getEmail();
                String allPhones = contactData.getAllPhones();
                if (integrationId != null || email != null || StringUtils.isNotEmpty(allPhones))
                {
                    MdbViewStudentAdditionalData newData = new MdbViewStudentAdditionalData();
                    newData.setStudentFefuExt(studentFefuExt);
                    newData.setMdbViewStudent(mdbStudentList.get(id));
                    newData.setIntegrationId(integrationId);
                    newData.setEmail(email);
                    newData.setPhones(allPhones);
                    session.save(newData);
                }

            }
        } catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }


    private void doDeleteEmptyPersons()
    {
        final Session session = this.getSession();

        final List<Long> ids = new DQLSelectBuilder().fromEntity(MdbViewPerson.class, "e").column(DQLExpressions.property("e.id"))
                .joinEntity("e", DQLJoinType.left, MdbViewStudent.class, "st", eq(property(MdbViewStudent.mdbViewPerson().id().fromAlias("st")), property("e.id")))
                .where(isNull(property(MdbViewStudent.mdbViewPerson().id().fromAlias("st"))))
                .createStatement(session).list();

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(MdbViewPerson.class);
        deleteBuilder.where(in(property(MdbViewPerson.id()), ids));

        deleteBuilder.createStatement(session).execute();
    }

    private void doExportAdditionalStudents()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();

        final Session session = this.getSession();
        final List<Long> ids = new DQLSelectBuilder().fromEntity(Student.class, "e").column(DQLExpressions.property("e.id"))
                .joinEntity("e", DQLJoinType.left, MdbViewStudent.class, "st", eq(property(MdbViewStudent.student().fromAlias("st")), property("e")))
                .joinEntity("e", DQLJoinType.inner, MdbViewPerson.class, "mp", eq(property(Student.person().id().fromAlias("e")), property(MdbViewPerson.person().id().fromAlias("mp"))))
                .where(isNull(property(MdbViewStudent.student().id().fromAlias("st"))))
                .createStatement(session).list();
        try
        {
            Map<Long, MdbViewStudent> mdbStudentList = new HashMap<>();
            for (final Student student : FEFUAllStudExportDaemonDAO.this.getList(Student.class, "id", ids))
            {
                MdbViewStudent newData = new MdbViewStudent();

                MdbViewPerson mdbViewPerson = new DQLSelectBuilder().fromEntity(MdbViewPerson.class, "e").column(property("e"))
                        .where(eq(property(MdbViewPerson.person().fromAlias("e")), value(student.getPerson())))
                        .createStatement(session).uniqueResult();

                newData.setMdbViewPerson(mdbViewPerson);
                newData.setArchival(student.isArchival());
                newData.setBookNumber(student.getBookNumber());
                newData.setCategory(student.getStudentCategory().getTitle());
                newData.setCompensation(student.getCompensationType().getTitle());
                newData.setCourse(student.getCourse().getIntValue());
                newData.setEduOu(student.getEducationOrgUnit());
                newData.setEntrance(student.getEntranceYear());
                newData.setFinishYear(student.getFinishYear());
                newData.setGroup(student.getGroup());
                newData.setNumber(student.getPerNumber());
                newData.setPersonalFileNumber(student.getPersonalFileNumber());
                newData.setSpecialPurposeRecruit(student.isTargetAdmission());
                newData.setStatus(student.getStatus().getTitle());
                newData.setStudent(student);

                session.save(newData);
                mdbStudentList.put(student.getId(), newData);

            }
            FEFUAllStudExportDaemonDAO.this.export_StudentCustomState(ids, mdbStudentList);
            FEFUAllStudExportDaemonDAO.this.export_StudentAdditionalData(ids, mdbStudentList);
        } catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally
        {
            eventLock.release();
        }
    }


    private void export_Student(Collection<Long> personIds, Map<Long, MdbViewPerson> mdbPersonList)
    {
        final Session session = this.getSession();
        final List<Long> ids = new DQLSelectBuilder().fromEntity(Student.class, "e").column(DQLExpressions.property("e.id"))
                .joinEntity("e", DQLJoinType.left, MdbViewStudent.class, "s", eq(property(Student.id().fromAlias("e")), property(MdbViewStudent.student().id().fromAlias("s"))))
                .where(in(property(Student.person().id().fromAlias("e")), personIds))
                .where(isNull(property(MdbViewStudent.id().fromAlias("s"))))
                .createStatement(session).list();
        try
        {
            Map<Long, MdbViewStudent> mdbStudentList = new HashMap<>();
            for (final Student student : FEFUAllStudExportDaemonDAO.this.getList(Student.class, "id", ids))
            {
                MdbViewStudent newData = new MdbViewStudent();

                newData.setMdbViewPerson(mdbPersonList.get(student.getPerson().getId()));
                newData.setArchival(student.isArchival());
                newData.setBookNumber(student.getBookNumber());
                newData.setCategory(student.getStudentCategory().getTitle());
                newData.setCompensation(student.getCompensationType().getTitle());
                newData.setCourse(student.getCourse().getIntValue());
                newData.setEduOu(student.getEducationOrgUnit());
                newData.setEntrance(student.getEntranceYear());
                newData.setFinishYear(student.getFinishYear());
                newData.setGroup(student.getGroup());
                newData.setNumber(student.getPerNumber());
                newData.setPersonalFileNumber(student.getPersonalFileNumber());
                newData.setSpecialPurposeRecruit(student.isTargetAdmission());
                newData.setStatus(student.getStatus().getTitle());
                newData.setStudent(student);

                session.save(newData);
                mdbStudentList.put(student.getId(), newData);

            }
            FEFUAllStudExportDaemonDAO.this.export_StudentCustomState(ids, mdbStudentList);
            FEFUAllStudExportDaemonDAO.this.export_StudentAdditionalData(ids, mdbStudentList);
        } catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }

    public Collection<Long> getPersonIds(Boolean withBatch)
    {
        Session session = this.getSession();

        DQLSelectBuilder pBuilder = new DQLSelectBuilder().fromEntity(Person.class, "p").column(DQLExpressions.property("p.id"));
        pBuilder.joinEntity("p", DQLJoinType.inner, Student.class, "st", eq(property(Student.person().fromAlias("st")), property("p")));
        pBuilder.joinEntity("p", DQLJoinType.left, MdbViewPerson.class, "m", eq(property(MdbViewPerson.person().fromAlias("m")), property("p")));
        pBuilder.where(isNull(property(MdbViewPerson.person().fromAlias("m"))));
        pBuilder.distinct();
        if (!withBatch)
        {
            return pBuilder.createStatement(session).list();
        } else
        {
            return pBuilder.createStatement(session).setMaxResults(2000).list();
        }
    }

    @Transactional(readOnly = false)
    public void export_Person(final Collection<Long> ids)
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();

        try
        {
            final Session session = this.getSession();
            //final List<Long> ids = getPersonIds(true);

            BatchUtils.execute(ids, 128, ids1 -> {
                try
                {
                    Map<Long, MdbViewPerson> mdbPersonList = new HashMap<>();
                    List<Person> list = new DQLSelectBuilder().fromEntity(Person.class, "p", true).column("p")
                            .where(in("p.id", ids1))
                            .createStatement(getSession()).list();
                    for (final Person person : list)
                    {

                        final IdentityCard identityCard = person.getIdentityCard();

                        final AddressBase regAddress = identityCard.getAddress();
                        final AddressBase factAddress = person.getAddress();

                        MdbViewPerson newData = new MdbViewPerson();

                        newData.setIdentityFirstName(StringUtils.trimToNull(identityCard.getFirstName()));
                        newData.setIdentityLastName(StringUtils.trimToNull(identityCard.getLastName()));
                        newData.setIdentityMiddleName(StringUtils.trimToNull(identityCard.getMiddleName()));
                        newData.setIdentityBirthDate(identityCard.getBirthDate());
                        newData.setIdentityBirthPlace(StringUtils.trimToNull(identityCard.getBirthPlace()));
                        newData.setIdentityType(StringUtils.trimToNull(identityCard.getCardType().getTitle()));
                        newData.setIdentitySeria(StringUtils.trimToNull(identityCard.getSeria()));
                        newData.setIdentityNumber(StringUtils.trimToNull(identityCard.getNumber()));
                        newData.setIdentityDate(identityCard.getIssuanceDate());
                        newData.setIdentitySex(StringUtils.trimToNull(identityCard.getSex().getTitle()));
                        newData.setIdentityCode(identityCard.getIssuanceCode());
                        newData.setIdentityPlace(identityCard.getIssuancePlace());
                        newData.setIdentityCitizen(AddressIODao.getCitizenshipString(identityCard.getCitizenship()));

                        newData.setFamilyStatus(StringUtils.trimToNull(null == person.getFamilyStatus() ? null : person.getFamilyStatus().getTitle()));
                        newData.setChildCount(person.getChildCount());
                        newData.setPensionType(StringUtils.trimToNull(null == person.getPensionType() ? null : person.getPensionType().getTitle()));
                        newData.setPensionIssuanceDate(person.getPensionIssuanceDate());
                        newData.setFlatPresence(StringUtils.trimToNull(null == person.getFlatPresence() ? null : person.getFlatPresence().getTitle()));

                        newData.setServiceLengthYears(Math.max(0, person.getServiceLengthYears()));
                        newData.setServiceLengthMonths(Math.max(0, person.getServiceLengthMonths()));
                        newData.setServiceLengthDays(Math.max(0, person.getServiceLengthDays()));


                        newData.setAddressRegId(doSaveMdbViewAddress(regAddress));
                        newData.setAddressFactId(doSaveMdbViewAddress(factAddress));

                        newData.setPerson(person);
                        newData.setPersonEduInstitution(person.getPersonEduInstitution());

                        newData.setInnNumber(person.getInnNumber());
                        newData.setSnilsNumber(person.getSnilsNumber());

                        session.save(newData);
                        mdbPersonList.put(person.getId(), newData);
                    }
                    FEFUAllStudExportDaemonDAO.this.export_PersonEduInstitution(ids1, mdbPersonList);
                    FEFUAllStudExportDaemonDAO.this.export_PersonSportAchievement(ids1, mdbPersonList);
                    FEFUAllStudExportDaemonDAO.this.export_PersonForeignLanguage(ids1, mdbPersonList);
                    FEFUAllStudExportDaemonDAO.this.export_PersonBenefit(ids1, mdbPersonList);
                    FEFUAllStudExportDaemonDAO.this.export_PersonNextOfKin(ids1, mdbPersonList);
                    FEFUAllStudExportDaemonDAO.this.export_PersonAdditionalData(ids1, mdbPersonList);

                    FEFUAllStudExportDaemonDAO.this.export_Student(ids1, mdbPersonList);


                } catch (final Throwable t)
                {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }

                session.flush();
                session.clear();
            });
        } finally
        {
            // включаем штуки и логирование обратно
            eventLock.release();
        }
    }

    public void doExportStudents()
    {
        this.doDeleteEmptyPersons();
        final Collection<Long> ids = getPersonIds(true);
        this.export_Person(ids);
        this.doExportAdditionalStudents();
    }

}