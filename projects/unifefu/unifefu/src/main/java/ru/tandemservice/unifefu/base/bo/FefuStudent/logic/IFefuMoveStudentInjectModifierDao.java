/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeRel;

/**
 * @author Alexey Lopatin
 * @since 13.01.2014
 */
public interface IFefuMoveStudentInjectModifierDao extends INeedPersistenceSupport
{
    /**
     * Возвращает подписанта приказа: Должность, И.О. Фамилия.
     *
     * @param extractTypeCode код приказа
     * @param eduLevelStageCode код ступени образования
     * @return подписант приказа.
     */
    String[][] getVisaSignerExtract(String extractTypeCode, String eduLevelStageCode);

    /**
     * Возвращает подписанта приказа: Должность, И.О. Фамилия.
     *
     * @param documentTypeCode код приказа
     * @param levelStageCode код ступени образования
     * @return подписант приказа.
     */
    String[][] getVisaSignerDocumentType(String documentTypeCode, String levelStageCode);

    /**
     * Возвращает подписанта приказа: И.О. Фамилия.
     *
     * @param documentTypeCode код приказа
     * @param levelStageCode код ступени образования
     * @return подписант приказа.
     */
    FefuSignerStuDocTypeRel getSignerDocumentType(String documentTypeCode, String levelStageCode);
}
