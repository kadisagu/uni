package ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;

/**
 * @author amakarova
 */
@Configuration
public class FefuListTrJournalDisciplinesReportAdd extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(FefuBrsReportManager.instance().orgUnitDSConfig())
                .addDataSource(FefuBrsReportManager.instance().yearPartDSConfig())
                .addDataSource(FefuBrsReportManager.instance().ppsDSConfig())
                .addDataSource(FefuBrsReportManager.instance().groupDSConfig())
                .addDataSource(FefuBrsReportManager.instance().yesNoDSConfig())
                .addDataSource(FefuBrsReportManager.instance().fcaDSConfig())
                .addDataSource(FefuBrsReportManager.instance().responsibilityOrgUnitDSConfig())
                .create();
    }
}



    