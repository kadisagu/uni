package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionCheckResults;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Результаты проверки версии УП в рамках блока (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEduPlanVersionCheckResultsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEduPlanVersionCheckResults";
    public static final String ENTITY_NAME = "fefuEduPlanVersionCheckResults";
    public static final int VERSION_HASH = -1440553346;
    private static IEntityMeta ENTITY_META;

    public static final String L_BLOCK = "block";
    public static final String P_CHECK_TIME = "checkTime";
    public static final String P_MESSAGE = "message";

    private EppEduPlanVersionBlock _block;     // Блок УП(в)
    private Date _checkTime;     // Время проверки
    private String _message;     // Результат проверки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок УП(в). Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    /**
     * @param block Блок УП(в). Свойство не может быть null.
     */
    public void setBlock(EppEduPlanVersionBlock block)
    {
        dirty(_block, block);
        _block = block;
    }

    /**
     * @return Время проверки. Свойство не может быть null.
     */
    @NotNull
    public Date getCheckTime()
    {
        return _checkTime;
    }

    /**
     * @param checkTime Время проверки. Свойство не может быть null.
     */
    public void setCheckTime(Date checkTime)
    {
        dirty(_checkTime, checkTime);
        _checkTime = checkTime;
    }

    /**
     * @return Результат проверки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getMessage()
    {
        return _message;
    }

    /**
     * @param message Результат проверки. Свойство не может быть null.
     */
    public void setMessage(String message)
    {
        dirty(_message, message);
        _message = message;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEduPlanVersionCheckResultsGen)
        {
            setBlock(((FefuEduPlanVersionCheckResults)another).getBlock());
            setCheckTime(((FefuEduPlanVersionCheckResults)another).getCheckTime());
            setMessage(((FefuEduPlanVersionCheckResults)another).getMessage());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEduPlanVersionCheckResultsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEduPlanVersionCheckResults.class;
        }

        public T newInstance()
        {
            return (T) new FefuEduPlanVersionCheckResults();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "block":
                    return obj.getBlock();
                case "checkTime":
                    return obj.getCheckTime();
                case "message":
                    return obj.getMessage();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "block":
                    obj.setBlock((EppEduPlanVersionBlock) value);
                    return;
                case "checkTime":
                    obj.setCheckTime((Date) value);
                    return;
                case "message":
                    obj.setMessage((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "block":
                        return true;
                case "checkTime":
                        return true;
                case "message":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "block":
                    return true;
                case "checkTime":
                    return true;
                case "message":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "block":
                    return EppEduPlanVersionBlock.class;
                case "checkTime":
                    return Date.class;
                case "message":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEduPlanVersionCheckResults> _dslPath = new Path<FefuEduPlanVersionCheckResults>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEduPlanVersionCheckResults");
    }
            

    /**
     * @return Блок УП(в). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionCheckResults#getBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
    {
        return _dslPath.block();
    }

    /**
     * @return Время проверки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionCheckResults#getCheckTime()
     */
    public static PropertyPath<Date> checkTime()
    {
        return _dslPath.checkTime();
    }

    /**
     * @return Результат проверки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionCheckResults#getMessage()
     */
    public static PropertyPath<String> message()
    {
        return _dslPath.message();
    }

    public static class Path<E extends FefuEduPlanVersionCheckResults> extends EntityPath<E>
    {
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _block;
        private PropertyPath<Date> _checkTime;
        private PropertyPath<String> _message;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок УП(в). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionCheckResults#getBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
        {
            if(_block == null )
                _block = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK, this);
            return _block;
        }

    /**
     * @return Время проверки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionCheckResults#getCheckTime()
     */
        public PropertyPath<Date> checkTime()
        {
            if(_checkTime == null )
                _checkTime = new PropertyPath<Date>(FefuEduPlanVersionCheckResultsGen.P_CHECK_TIME, this);
            return _checkTime;
        }

    /**
     * @return Результат проверки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionCheckResults#getMessage()
     */
        public PropertyPath<String> message()
        {
            if(_message == null )
                _message = new PropertyPath<String>(FefuEduPlanVersionCheckResultsGen.P_MESSAGE, this);
            return _message;
        }

        public Class getEntityClass()
        {
            return FefuEduPlanVersionCheckResults.class;
        }

        public String getEntityName()
        {
            return "fefuEduPlanVersionCheckResults";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
