
package ru.tandemservice.unifefu.ws.blackboard.context.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tandemservice.unifefu.ws.blackboard.context.gen package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DeactivateToolIgnore_QNAME = new QName("http://context.ws.blackboard", "ignore");
    private final static QName _DeactivateToolResponseReturn_QNAME = new QName("http://context.ws.blackboard", "return");
    private final static QName _GetServerVersionUnused_QNAME = new QName("http://context.ws.blackboard", "unused");
    private final static QName _RegisterToolResultVOProxyToolGuid_QNAME = new QName("http://context.ws.blackboard/xsd", "proxyToolGuid");
    private final static QName _LoginToolPassword_QNAME = new QName("http://context.ws.blackboard", "password");
    private final static QName _LoginToolClientVendorId_QNAME = new QName("http://context.ws.blackboard", "clientVendorId");
    private final static QName _LoginToolLoginExtraInfo_QNAME = new QName("http://context.ws.blackboard", "loginExtraInfo");
    private final static QName _LoginToolClientProgramId_QNAME = new QName("http://context.ws.blackboard", "clientProgramId");
    private final static QName _LoginUserid_QNAME = new QName("http://context.ws.blackboard", "userid");
    private final static QName _LoginTicketTicket_QNAME = new QName("http://context.ws.blackboard", "ticket");
    private final static QName _EmulateUserUserToEmulate_QNAME = new QName("http://context.ws.blackboard", "userToEmulate");
    private final static QName _RegisterToolInitialSharedSecret_QNAME = new QName("http://context.ws.blackboard", "initialSharedSecret");
    private final static QName _RegisterToolRegistrationPassword_QNAME = new QName("http://context.ws.blackboard", "registrationPassword");
    private final static QName _RegisterToolDescription_QNAME = new QName("http://context.ws.blackboard", "description");
    private final static QName _GetRequiredEntitlementsMethod_QNAME = new QName("http://context.ws.blackboard", "method");
    private final static QName _CourseIdVOExternalId_QNAME = new QName("http://ws.platform.blackboard/xsd", "externalId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tandemservice.unifefu.ws.blackboard.context.gen
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetRequiredEntitlementsResponse }
     * 
     */
    public GetRequiredEntitlementsResponse createGetRequiredEntitlementsResponse() {
        return new GetRequiredEntitlementsResponse();
    }

    /**
     * Create an instance of {@link DeactivateTool }
     * 
     */
    public DeactivateTool createDeactivateTool() {
        return new DeactivateTool();
    }

    /**
     * Create an instance of {@link LoginToolResponse }
     * 
     */
    public LoginToolResponse createLoginToolResponse() {
        return new LoginToolResponse();
    }

    /**
     * Create an instance of {@link DeactivateToolResponse }
     * 
     */
    public DeactivateToolResponse createDeactivateToolResponse() {
        return new DeactivateToolResponse();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link EmulateUserResponse }
     * 
     */
    public EmulateUserResponse createEmulateUserResponse() {
        return new EmulateUserResponse();
    }

    /**
     * Create an instance of {@link DeactivateToolResultVO }
     * 
     */
    public DeactivateToolResultVO createDeactivateToolResultVO() {
        return new DeactivateToolResultVO();
    }

    /**
     * Create an instance of {@link GetSystemInstallationIdResponse }
     * 
     */
    public GetSystemInstallationIdResponse createGetSystemInstallationIdResponse() {
        return new GetSystemInstallationIdResponse();
    }

    /**
     * Create an instance of {@link RegisterToolResponse }
     * 
     */
    public RegisterToolResponse createRegisterToolResponse() {
        return new RegisterToolResponse();
    }

    /**
     * Create an instance of {@link LoginTool }
     * 
     */
    public LoginTool createLoginTool() {
        return new LoginTool();
    }

    /**
     * Create an instance of {@link LoginTicket }
     * 
     */
    public LoginTicket createLoginTicket() {
        return new LoginTicket();
    }

    /**
     * Create an instance of {@link GetMemberships }
     * 
     */
    public GetMemberships createGetMemberships() {
        return new GetMemberships();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link InitializeVersion2Response }
     * 
     */
    public InitializeVersion2Response createInitializeVersion2Response() {
        return new InitializeVersion2Response();
    }

    /**
     * Create an instance of {@link ExtendSessionLife }
     * 
     */
    public ExtendSessionLife createExtendSessionLife() {
        return new ExtendSessionLife();
    }

    /**
     * Create an instance of {@link RegisterTool }
     * 
     */
    public RegisterTool createRegisterTool() {
        return new RegisterTool();
    }

    /**
     * Create an instance of {@link InitializeResponse }
     * 
     */
    public InitializeResponse createInitializeResponse() {
        return new InitializeResponse();
    }

    /**
     * Create an instance of {@link LoginTicketResponse }
     * 
     */
    public LoginTicketResponse createLoginTicketResponse() {
        return new LoginTicketResponse();
    }

    /**
     * Create an instance of {@link GetRequiredEntitlements }
     * 
     */
    public GetRequiredEntitlements createGetRequiredEntitlements() {
        return new GetRequiredEntitlements();
    }

    /**
     * Create an instance of {@link CourseIdVO }
     * 
     */
    public CourseIdVO createCourseIdVO() {
        return new CourseIdVO();
    }

    /**
     * Create an instance of {@link GetMembershipsResponse }
     * 
     */
    public GetMembershipsResponse createGetMembershipsResponse() {
        return new GetMembershipsResponse();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     * 
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link GetMyMembershipsResponse }
     * 
     */
    public GetMyMembershipsResponse createGetMyMembershipsResponse() {
        return new GetMyMembershipsResponse();
    }

    /**
     * Create an instance of {@link VersionVO }
     * 
     */
    public VersionVO createVersionVO() {
        return new VersionVO();
    }

    /**
     * Create an instance of {@link ExtendSessionLifeResponse }
     * 
     */
    public ExtendSessionLifeResponse createExtendSessionLifeResponse() {
        return new ExtendSessionLifeResponse();
    }

    /**
     * Create an instance of {@link GetServerVersion }
     * 
     */
    public GetServerVersion createGetServerVersion() {
        return new GetServerVersion();
    }

    /**
     * Create an instance of {@link RegisterToolResultVO }
     * 
     */
    public RegisterToolResultVO createRegisterToolResultVO() {
        return new RegisterToolResultVO();
    }

    /**
     * Create an instance of {@link GetServerVersionResponse }
     * 
     */
    public GetServerVersionResponse createGetServerVersionResponse() {
        return new GetServerVersionResponse();
    }

    /**
     * Create an instance of {@link EmulateUser }
     * 
     */
    public EmulateUser createEmulateUser() {
        return new EmulateUser();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "ignore", scope = DeactivateTool.class)
    public JAXBElement<String> createDeactivateToolIgnore(String value) {
        return new JAXBElement<>(_DeactivateToolIgnore_QNAME, String.class, DeactivateTool.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeactivateToolResultVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "return", scope = DeactivateToolResponse.class)
    public JAXBElement<DeactivateToolResultVO> createDeactivateToolResponseReturn(DeactivateToolResultVO value) {
        return new JAXBElement<>(_DeactivateToolResponseReturn_QNAME, DeactivateToolResultVO.class, DeactivateToolResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterToolResultVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "return", scope = RegisterToolResponse.class)
    public JAXBElement<RegisterToolResultVO> createRegisterToolResponseReturn(RegisterToolResultVO value) {
        return new JAXBElement<>(_DeactivateToolResponseReturn_QNAME, RegisterToolResultVO.class, RegisterToolResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "return", scope = GetSystemInstallationIdResponse.class)
    public JAXBElement<String> createGetSystemInstallationIdResponseReturn(String value) {
        return new JAXBElement<>(_DeactivateToolResponseReturn_QNAME, String.class, GetSystemInstallationIdResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "unused", scope = GetServerVersion.class)
    public JAXBElement<VersionVO> createGetServerVersionUnused(VersionVO value) {
        return new JAXBElement<>(_GetServerVersionUnused_QNAME, VersionVO.class, GetServerVersion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard/xsd", name = "proxyToolGuid", scope = RegisterToolResultVO.class)
    public JAXBElement<String> createRegisterToolResultVOProxyToolGuid(String value) {
        return new JAXBElement<>(_RegisterToolResultVOProxyToolGuid_QNAME, String.class, RegisterToolResultVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "password", scope = LoginTool.class)
    public JAXBElement<String> createLoginToolPassword(String value) {
        return new JAXBElement<>(_LoginToolPassword_QNAME, String.class, LoginTool.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "clientVendorId", scope = LoginTool.class)
    public JAXBElement<String> createLoginToolClientVendorId(String value) {
        return new JAXBElement<>(_LoginToolClientVendorId_QNAME, String.class, LoginTool.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "loginExtraInfo", scope = LoginTool.class)
    public JAXBElement<String> createLoginToolLoginExtraInfo(String value) {
        return new JAXBElement<>(_LoginToolLoginExtraInfo_QNAME, String.class, LoginTool.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "clientProgramId", scope = LoginTool.class)
    public JAXBElement<String> createLoginToolClientProgramId(String value) {
        return new JAXBElement<>(_LoginToolClientProgramId_QNAME, String.class, LoginTool.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "password", scope = Login.class)
    public JAXBElement<String> createLoginPassword(String value) {
        return new JAXBElement<>(_LoginToolPassword_QNAME, String.class, Login.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "clientVendorId", scope = Login.class)
    public JAXBElement<String> createLoginClientVendorId(String value) {
        return new JAXBElement<>(_LoginToolClientVendorId_QNAME, String.class, Login.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "userid", scope = Login.class)
    public JAXBElement<String> createLoginUserid(String value) {
        return new JAXBElement<>(_LoginUserid_QNAME, String.class, Login.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "loginExtraInfo", scope = Login.class)
    public JAXBElement<String> createLoginLoginExtraInfo(String value) {
        return new JAXBElement<>(_LoginToolLoginExtraInfo_QNAME, String.class, Login.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "clientProgramId", scope = Login.class)
    public JAXBElement<String> createLoginClientProgramId(String value) {
        return new JAXBElement<>(_LoginToolClientProgramId_QNAME, String.class, Login.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "clientVendorId", scope = LoginTicket.class)
    public JAXBElement<String> createLoginTicketClientVendorId(String value) {
        return new JAXBElement<>(_LoginToolClientVendorId_QNAME, String.class, LoginTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "loginExtraInfo", scope = LoginTicket.class)
    public JAXBElement<String> createLoginTicketLoginExtraInfo(String value) {
        return new JAXBElement<>(_LoginToolLoginExtraInfo_QNAME, String.class, LoginTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "clientProgramId", scope = LoginTicket.class)
    public JAXBElement<String> createLoginTicketClientProgramId(String value) {
        return new JAXBElement<>(_LoginToolClientProgramId_QNAME, String.class, LoginTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "ticket", scope = LoginTicket.class)
    public JAXBElement<String> createLoginTicketTicket(String value) {
        return new JAXBElement<>(_LoginTicketTicket_QNAME, String.class, LoginTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "userid", scope = GetMemberships.class)
    public JAXBElement<String> createGetMembershipsUserid(String value) {
        return new JAXBElement<>(_LoginUserid_QNAME, String.class, GetMemberships.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "return", scope = GetServerVersionResponse.class)
    public JAXBElement<VersionVO> createGetServerVersionResponseReturn(VersionVO value) {
        return new JAXBElement<>(_DeactivateToolResponseReturn_QNAME, VersionVO.class, GetServerVersionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "return", scope = InitializeVersion2Response.class)
    public JAXBElement<String> createInitializeVersion2ResponseReturn(String value) {
        return new JAXBElement<>(_DeactivateToolResponseReturn_QNAME, String.class, InitializeVersion2Response.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "userToEmulate", scope = EmulateUser.class)
    public JAXBElement<String> createEmulateUserUserToEmulate(String value) {
        return new JAXBElement<>(_EmulateUserUserToEmulate_QNAME, String.class, EmulateUser.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "clientVendorId", scope = RegisterTool.class)
    public JAXBElement<String> createRegisterToolClientVendorId(String value) {
        return new JAXBElement<>(_LoginToolClientVendorId_QNAME, String.class, RegisterTool.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "clientProgramId", scope = RegisterTool.class)
    public JAXBElement<String> createRegisterToolClientProgramId(String value) {
        return new JAXBElement<>(_LoginToolClientProgramId_QNAME, String.class, RegisterTool.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "initialSharedSecret", scope = RegisterTool.class)
    public JAXBElement<String> createRegisterToolInitialSharedSecret(String value) {
        return new JAXBElement<>(_RegisterToolInitialSharedSecret_QNAME, String.class, RegisterTool.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "registrationPassword", scope = RegisterTool.class)
    public JAXBElement<String> createRegisterToolRegistrationPassword(String value) {
        return new JAXBElement<>(_RegisterToolRegistrationPassword_QNAME, String.class, RegisterTool.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "description", scope = RegisterTool.class)
    public JAXBElement<String> createRegisterToolDescription(String value) {
        return new JAXBElement<>(_RegisterToolDescription_QNAME, String.class, RegisterTool.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "return", scope = InitializeResponse.class)
    public JAXBElement<String> createInitializeResponseReturn(String value) {
        return new JAXBElement<>(_DeactivateToolResponseReturn_QNAME, String.class, InitializeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://context.ws.blackboard", name = "method", scope = GetRequiredEntitlements.class)
    public JAXBElement<String> createGetRequiredEntitlementsMethod(String value) {
        return new JAXBElement<>(_GetRequiredEntitlementsMethod_QNAME, String.class, GetRequiredEntitlements.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.platform.blackboard/xsd", name = "externalId", scope = CourseIdVO.class)
    public JAXBElement<String> createCourseIdVOExternalId(String value) {
        return new JAXBElement<>(_CourseIdVOExternalId_QNAME, String.class, CourseIdVO.class, value);
    }

}
