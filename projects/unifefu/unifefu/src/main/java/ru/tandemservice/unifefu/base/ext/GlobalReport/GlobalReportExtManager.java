/* $Id$ */
package ru.tandemservice.unifefu.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.ui.ResultsList.FefuDipDocumentReportResultsList;

/**
 * @author Andrey Avetisov
 * @since 29.09.2014
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;
    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add("diplomaList", new GlobalReportDefinition("dip", "diplomaList", FefuDipDocumentReportResultsList.class.getSimpleName()))
                .create();
    }
}
