/* $Id$ */
package ru.tandemservice.unifefu.events.nsi;

import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 23.12.2013
 */
public class NsiEntityInfo
{
    private Long _entityID;
    private Date _changeDate;
    private Long _hash;

    public Long getEntityID()
    {
        return _entityID;
    }

    public void setEntityID(Long entityID)
    {
        _entityID = entityID;
    }

    public Date getChangeDate()
    {
        return _changeDate;
    }

    public void setChangeDate(Date changeDate)
    {
        _changeDate = changeDate;
    }

    public Long getHash()
    {
        return _hash;
    }

    public void setHash(Long hash)
    {
        _hash = hash;
    }
}
