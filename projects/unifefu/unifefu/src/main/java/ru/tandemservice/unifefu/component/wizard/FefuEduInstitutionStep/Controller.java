/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.FefuEduInstitutionStep;

import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;

/**
 * @author Nikolay Fedorovskih
 * @since 16.05.2013
 */
public class Controller extends ru.tandemservice.uniec.component.wizard.EduInstitutionStep.Controller
{
    @Override
    public String getNextStep()
    {
        return IEntrantWizardComponents.PERSONAL_DATA_STEP;
    }
}