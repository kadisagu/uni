/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.TutorTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.BBCourseDSHandler;
import ru.tandemservice.unifefu.base.bo.Blackboard.ui.AddCourseToTrJournal.BlackboardAddCourseToTrJournal;
import ru.tandemservice.unifefu.base.bo.Blackboard.ui.AddCourseToTrJournal.BlackboardAddCourseToTrJournalUI;
import ru.tandemservice.unifefu.base.bo.Blackboard.ui.EditCourseEventRelation.BlackboardEditCourseEventRelation;
import ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.BBGradebookUtils;
import ru.tandemservice.unitraining.base.bo.TrHomePage.TrHomePageManager;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalStructureEdit.TrHomePageJournalStructureEditUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

import java.util.Arrays;

/**
 * @author Nikolay Fedorovskih
 * @since 26.03.2014
 */
public class BlackboardTutorTabUI extends UIPresenter
{
    private boolean _empty;
    private TrJournal _journal;
    private PpsEntry _ppsEntry;

    @Override
    public void onComponentRefresh()
    {
        IPrincipalContext principalContext = _uiConfig.getUserContext().getPrincipalContext();
        Person person = PersonManager.instance().dao().getPerson(principalContext);
        if (principalContext instanceof EmployeePost)
        {
            EmployeePost4PpsEntry ep4ppsEntry = DataAccessServices.dao().get(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.employeePost().id(), principalContext.getId());
            if (null != ep4ppsEntry)
                _ppsEntry = ep4ppsEntry.getPpsEntry();
        }
        else
        {
            _ppsEntry = null;
        }
        _empty = TrHomePageManager.instance().dao().getStructureCount(person, TrHomePageJournalStructureEditUI.ALLOW_STATES) == 0;
        _journal = getSettings().get("journal");
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case BlackboardTutorTab.BB_TR_JOURNAL_DS:
                dataSource.put(BlackboardTutorTab.BB_TR_JOURNAL_PPS_PARAM, getPpsEntry());
                break;
            case BlackboardTutorTab.COURSE_DS:
                if (getPpsEntry() != null)
                {
                    dataSource.put(BBCourseDSHandler.PPS_ENTRY_LIST_FILTER_PARAM, Arrays.asList(getPpsEntry()));
                    dataSource.put(BBCourseDSHandler.TR_JOURNAL_PARAM, getJournal());
                }
                break;
            case BlackboardTutorTab.STRUCTURE_VIEW_DS:
                dataSource.put(BBCourseDSHandler.TR_JOURNAL_PARAM, getJournal());
                break;
        }
    }

    public void onClickAddCourse()
    {
        _uiActivation.asRegionDialog(BlackboardAddCourseToTrJournal.class)
                .parameter(BlackboardAddCourseToTrJournalUI.TR_JOURNAL_CONTEXT_PARAM, getJournal())
                .parameter(BlackboardAddCourseToTrJournalUI.PPS_ENTRY_PARAM, getPpsEntry())
                .activate();
    }

    public void onClickGetGrades()
    {
        BBGradebookUtils.doSyncGradesForJournal(getJournal());
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(BlackboardEditCourseEventRelation.class)
                .parameter(IUIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        BbTrJournalEventRel rel = DataAccessServices.dao().get(BbTrJournalEventRel.class, BbTrJournalEventRel.L_EVENT, getListenerParameterAsLong());
        if (rel != null)
        {
            DataAccessServices.dao().delete(rel);
        }
    }

    public boolean isEmpty()
    {
        return _empty;
    }

    public TrJournal getJournal()
    {
        return _journal;
    }

    public void setJournal(TrJournal journal)
    {
        _journal = journal;
        getSettings().set("journal", journal);
    }

    public PpsEntry getPpsEntry()
    {
        return _ppsEntry;
    }
}