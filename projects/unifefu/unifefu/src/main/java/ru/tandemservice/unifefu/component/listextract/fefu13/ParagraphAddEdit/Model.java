package ru.tandemservice.unifefu.component.listextract.fefu13.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative.AbstractParagraphAddEditAlternativeModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.IExtEducationLevelModel;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;
import ru.tandemservice.unifefu.entity.catalog.FefuPromotedActivityType;

import java.util.Date;
import java.util.List;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public class Model extends AbstractParagraphAddEditAlternativeModel<FefuAdditionalAcademGrantStuListExtract> implements IGroupModel, IExtEducationLevelModel
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private Date _dateStart;
    private Date _dateEnd;
	private Date _matchingDate;
	private String _protocolNumber;
    private FefuPromotedActivityType promotedActivityType;
	private EmployeePost _responsibleForPayments;
	private EmployeePost _responsibleForAgreement;
	private EducationLevelsHighSchool _educationLevelsHighSchool;

	private ISelectModel _groupListModel;
	private ISelectModel _eduLevelHighSchoolListModel;
    private List<CompensationType> _compensationTypeList;
    private List<FefuPromotedActivityType> _promotedActivityTypeList;
	private ISelectModel _employeePostModel;


    //Calculate method
    public String getGrantSizeId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "grantSizeId_" + dataSource.getCurrentEntity().getId();
    }

    public FefuPromotedActivityType getPromotedActivityType()
    {
        return promotedActivityType;
    }

    public void setPromotedActivityType(FefuPromotedActivityType promotedActivityType)
    {
        this.promotedActivityType = promotedActivityType;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public Date getDateStart()
    {
        return _dateStart;
    }

    public void setDateStart(Date dateStart)
    {
        _dateStart = dateStart;
    }

    public Date getDateEnd()
    {
        return _dateEnd;
    }

    public void setDateEnd(Date dateEnd)
    {
        _dateEnd = dateEnd;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }


    public List<FefuPromotedActivityType> getPromotedActivityTypeList()
    {
        return _promotedActivityTypeList;
    }

    public void setPromotedActivityTypeList(List<FefuPromotedActivityType> promotedActivityTypeList)
    {
        _promotedActivityTypeList = promotedActivityTypeList;
    }

    public ISelectModel getEmployeePostModel()
	{
		return _employeePostModel;
	}

	public void setEmployeePostModel(ISelectModel employeePostModel)
	{
		_employeePostModel = employeePostModel;
	}

	public Date getMatchingDate()
	{
		return _matchingDate;
	}

	public void setMatchingDate(Date matchingDate)
	{
		_matchingDate = matchingDate;
	}

	public String getProtocolNumber()
	{
		return _protocolNumber;
	}

	public void setProtocolNumber(String protocolNumber)
	{
		_protocolNumber = protocolNumber;
	}

	public EmployeePost getResponsibleForPayments()
	{
		return _responsibleForPayments;
	}

	public void setResponsibleForPayments(EmployeePost responsibleForPayments)
	{
		_responsibleForPayments = responsibleForPayments;
	}

	public EmployeePost getResponsibleForAgreement()
	{
		return _responsibleForAgreement;
	}

	public void setResponsibleForAgreement(EmployeePost responsibleForAgreement)
	{
		_responsibleForAgreement = responsibleForAgreement;
	}

	public EducationLevelsHighSchool getEducationLevelsHighSchool()
	{
		return _educationLevelsHighSchool;
	}

	public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
	{
		_educationLevelsHighSchool = educationLevelsHighSchool;
	}

	public ISelectModel getEduLevelHighSchoolListModel()
	{
		return _eduLevelHighSchoolListModel;
	}

	public void setEduLevelHighSchoolListModel(ISelectModel eduLevelHighSchoolListModel)
	{
		_eduLevelHighSchoolListModel = eduLevelHighSchoolListModel;
	}

	@Override
	public EducationLevels getParentEduLevel()
	{
		return getEducationLevelsHighSchool() != null ? getEducationLevelsHighSchool().getEducationLevel() : null;
	}
}
