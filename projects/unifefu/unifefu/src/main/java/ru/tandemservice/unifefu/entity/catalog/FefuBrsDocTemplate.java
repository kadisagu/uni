package ru.tandemservice.unifefu.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.unifefu.entity.catalog.gen.*;

/**
 * Шаблоны документов, используемых в рейтинговой системе
 */
public class FefuBrsDocTemplate extends FefuBrsDocTemplateGen
{
    @Override
    public byte[] getContent()
    {
        return CommonBaseUtil.getTemplateContent(this);
    }
}