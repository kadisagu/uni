/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu11.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 19.11.2013
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuCompensationTypeTransferStuListExtract, Model>
{
}
