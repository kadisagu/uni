/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu8.ParagraphAddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 23.10.2013
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuHolidayStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        FefuHolidayStuListExtract firstExtract = model.getFirstExtract();

        // заполняем поля по умолчанию
        if (firstExtract != null)
        {
            model.setCourse(firstExtract.getCourse());
            model.setGroup(firstExtract.getGroup());
            model.setBeginDate(firstExtract.getBeginDate());
            model.setEndDate(firstExtract.getEndDate());
        }

        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setNewStudentCustomStatus(DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), UniFefuDefines.VACATION));
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if(!model.getBeginDate().before(model.getEndDate()))
            errors.add("Дата окончания каникул должна быть больше даты начала.", "beginDate", "endDate");
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
    }

    @Override
    protected FefuHolidayStuListExtract createNewInstance(Model model)
    {
        return new FefuHolidayStuListExtract();
    }

    @Override
    protected void fillExtract(FefuHolidayStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setBeginDate(model.getBeginDate());
        extract.setEndDate(model.getEndDate());
    }
}