package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.unifefu.entity.FefuSignerStuExtractRel;
import ru.tandemservice.unifefu.entity.FefuSignerStuExtractSetting;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь настройки подписантов для приказов со ступенью образования (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuSignerStuExtractRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuSignerStuExtractRel";
    public static final String ENTITY_NAME = "fefuSignerStuExtractRel";
    public static final int VERSION_HASH = 1896066108;
    private static IEntityMeta ENTITY_META;

    public static final String L_SETTING = "setting";
    public static final String L_EDU_LEVEL_STAGE = "eduLevelStage";
    public static final String L_SIGNER = "signer";

    private FefuSignerStuExtractSetting _setting;     // Настройка подписантов для приказов по движению студентов (ДВФУ)
    private StructureEducationLevels _eduLevelStage;     // Ступень образования
    private EmployeePostPossibleVisa _signer;     // Подписант

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Настройка подписантов для приказов по движению студентов (ДВФУ). Свойство не может быть null.
     */
    @NotNull
    public FefuSignerStuExtractSetting getSetting()
    {
        return _setting;
    }

    /**
     * @param setting Настройка подписантов для приказов по движению студентов (ДВФУ). Свойство не может быть null.
     */
    public void setSetting(FefuSignerStuExtractSetting setting)
    {
        dirty(_setting, setting);
        _setting = setting;
    }

    /**
     * @return Ступень образования. Свойство не может быть null.
     */
    @NotNull
    public StructureEducationLevels getEduLevelStage()
    {
        return _eduLevelStage;
    }

    /**
     * @param eduLevelStage Ступень образования. Свойство не может быть null.
     */
    public void setEduLevelStage(StructureEducationLevels eduLevelStage)
    {
        dirty(_eduLevelStage, eduLevelStage);
        _eduLevelStage = eduLevelStage;
    }

    /**
     * @return Подписант. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostPossibleVisa getSigner()
    {
        return _signer;
    }

    /**
     * @param signer Подписант. Свойство не может быть null.
     */
    public void setSigner(EmployeePostPossibleVisa signer)
    {
        dirty(_signer, signer);
        _signer = signer;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuSignerStuExtractRelGen)
        {
            setSetting(((FefuSignerStuExtractRel)another).getSetting());
            setEduLevelStage(((FefuSignerStuExtractRel)another).getEduLevelStage());
            setSigner(((FefuSignerStuExtractRel)another).getSigner());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuSignerStuExtractRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuSignerStuExtractRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuSignerStuExtractRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "setting":
                    return obj.getSetting();
                case "eduLevelStage":
                    return obj.getEduLevelStage();
                case "signer":
                    return obj.getSigner();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "setting":
                    obj.setSetting((FefuSignerStuExtractSetting) value);
                    return;
                case "eduLevelStage":
                    obj.setEduLevelStage((StructureEducationLevels) value);
                    return;
                case "signer":
                    obj.setSigner((EmployeePostPossibleVisa) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "setting":
                        return true;
                case "eduLevelStage":
                        return true;
                case "signer":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "setting":
                    return true;
                case "eduLevelStage":
                    return true;
                case "signer":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "setting":
                    return FefuSignerStuExtractSetting.class;
                case "eduLevelStage":
                    return StructureEducationLevels.class;
                case "signer":
                    return EmployeePostPossibleVisa.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuSignerStuExtractRel> _dslPath = new Path<FefuSignerStuExtractRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuSignerStuExtractRel");
    }
            

    /**
     * @return Настройка подписантов для приказов по движению студентов (ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSignerStuExtractRel#getSetting()
     */
    public static FefuSignerStuExtractSetting.Path<FefuSignerStuExtractSetting> setting()
    {
        return _dslPath.setting();
    }

    /**
     * @return Ступень образования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSignerStuExtractRel#getEduLevelStage()
     */
    public static StructureEducationLevels.Path<StructureEducationLevels> eduLevelStage()
    {
        return _dslPath.eduLevelStage();
    }

    /**
     * @return Подписант. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSignerStuExtractRel#getSigner()
     */
    public static EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa> signer()
    {
        return _dslPath.signer();
    }

    public static class Path<E extends FefuSignerStuExtractRel> extends EntityPath<E>
    {
        private FefuSignerStuExtractSetting.Path<FefuSignerStuExtractSetting> _setting;
        private StructureEducationLevels.Path<StructureEducationLevels> _eduLevelStage;
        private EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa> _signer;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Настройка подписантов для приказов по движению студентов (ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSignerStuExtractRel#getSetting()
     */
        public FefuSignerStuExtractSetting.Path<FefuSignerStuExtractSetting> setting()
        {
            if(_setting == null )
                _setting = new FefuSignerStuExtractSetting.Path<FefuSignerStuExtractSetting>(L_SETTING, this);
            return _setting;
        }

    /**
     * @return Ступень образования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSignerStuExtractRel#getEduLevelStage()
     */
        public StructureEducationLevels.Path<StructureEducationLevels> eduLevelStage()
        {
            if(_eduLevelStage == null )
                _eduLevelStage = new StructureEducationLevels.Path<StructureEducationLevels>(L_EDU_LEVEL_STAGE, this);
            return _eduLevelStage;
        }

    /**
     * @return Подписант. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSignerStuExtractRel#getSigner()
     */
        public EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa> signer()
        {
            if(_signer == null )
                _signer = new EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa>(L_SIGNER, this);
            return _signer;
        }

        public Class getEntityClass()
        {
            return FefuSignerStuExtractRel.class;
        }

        public String getEntityName()
        {
            return "fefuSignerStuExtractRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
