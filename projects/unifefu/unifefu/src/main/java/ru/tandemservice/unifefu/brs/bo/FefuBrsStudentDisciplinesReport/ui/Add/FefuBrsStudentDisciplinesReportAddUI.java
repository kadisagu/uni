/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unifefu.brs.base.BaseFefuReportAddUI;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.FefuBrsStudentDisciplinesReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.logic.FefuBrsStudentDisciplinesReportParams;
import ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.ui.View.FefuBrsStudentDisciplinesReportView;
import ru.tandemservice.unifefu.entity.report.FefuBrsStudentDisciplinesReport;

import java.util.Collections;
import java.util.Date;

/**
 * @author nvankov
 * @since 12/19/13
 */
public class FefuBrsStudentDisciplinesReportAddUI extends BaseFefuReportAddUI<FefuBrsStudentDisciplinesReportParams>
{
    @Override
    protected void fillDefaults(FefuBrsStudentDisciplinesReportParams reportSettings)
    {
        super.fillDefaults(reportSettings);
        reportSettings.setFormativeOrgUnit(getCurrentFormativeOrgUnit());
        reportSettings.setCheckDate(new Date());
        reportSettings.setOnlyFilledJournals(FefuBrsReportManager.instance().yesNoItemListExtPoint().getItem(FefuBrsReportManager.YES_ID.toString()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuBrsReportManager.GROUP_DS.equals(dataSource.getName()))
        {
            if(null != getReportSettings().getFormativeOrgUnit())
                dataSource.put(FefuBrsReportManager.PARAM_FORMATIVE_OU, Collections.singletonList(getReportSettings().getFormativeOrgUnit().getId()));
        }
    }

    public void onClickApply()
    {
        FefuBrsStudentDisciplinesReport report = FefuBrsStudentDisciplinesReportManager.instance().dao().createReport(getReportSettings());
        deactivate();
        _uiActivation.asDesktopRoot(FefuBrsStudentDisciplinesReportView.class).parameter(UIPresenter.PUBLISHER_ID, report.getId()).activate();
    }
}
