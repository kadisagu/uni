/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu16.ParagraphAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.FefuExtractPrintFormManager;
import ru.tandemservice.unifefu.entity.*;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuExcludeStuDPOListExtract, Model>
{

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        super.prepare(model);
        model.setStudentStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));
        model.setDpoProgramModel(new BaseSingleSelectModel()
        {

            @Override
            public ListResult<FefuAdditionalProfessionalEducationProgram> findValues(String filter)
            {
                DQLSelectBuilder builder = createSelectBuilder(filter, null);

                List<FefuAdditionalProfessionalEducationProgram> list = builder.createStatement(getSession()).list();
                return new ListResult<>(list, list.size());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return get(FefuAdditionalProfessionalEducationProgram.class, (Long)primaryKey);
            }

            protected DQLSelectBuilder createSelectBuilder(String filter, Object o)
            {
                OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(FefuAdditionalProfessionalEducationProgram.class, "ape")
                        .predicate(DQLPredicateType.distinct)
                        .where(or(eq(property("ape", FefuAdditionalProfessionalEducationProgram.formativeOrgUnit()), value(orgUnit)),
                                  eq(property("ape", FefuAdditionalProfessionalEducationProgram.territorialOrgUnit()), value(orgUnit))))
                        .column(property("ape"));


                if (o != null)
                {
                    if (o instanceof Long)
                        builder.where(eq(property("ape", FefuAdditionalProfessionalEducationProgram.id()), commonValue(o, PropertyType.LONG)));
                    else if (o instanceof Collection)
                        builder.where(in(property("ape", FefuAdditionalProfessionalEducationProgram.id()), (Collection) o));
                }

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("ape", FefuAdditionalProfessionalEducationProgram.title()), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property("ape", FefuAdditionalProfessionalEducationProgram.title()), OrderDirection.asc);

                return builder;
            }


        });
        final FefuExcludeStuDPOListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            FefuAdditionalProfessionalEducationProgram program = get(FefuAdditionalProfessionalEducationProgramForStudent.class, FefuAdditionalProfessionalEducationProgramForStudent.student(),
                                                                     firstExtract.getEntity()).getProgram();
            model.setDpoProgram(program);
            model.setExcludeDate(firstExtract.getExcludeDate());
        }
        model.setCanAddTemplate(!model.isEditForm() && model.isParagraphOnlyOneInTheOrder());
    }

    @Override
    protected FefuExcludeStuDPOListExtract createNewInstance(Model model)
    {
        return new FefuExcludeStuDPOListExtract();
    }

    @Override
    protected void fillExtract(FefuExcludeStuDPOListExtract extract, Student student, Model model)
    {
        extract.setExcludeDate(model.getExcludeDate());
        extract.setStudentStatusOld(student.getStatus());
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();
        builder.add(MQExpression.or(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().formativeOrgUnit(), orgUnit), MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().territorialOrgUnit(), orgUnit)));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().additional(), true));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.studentCategory().code(), StudentCategoryCodes.STUDENT_CATEGORY_DPP));

        List<Long> studIds = new DQLSelectBuilder().fromEntity(FefuAdditionalProfessionalEducationProgramForStudent.class, "apes")
                .column(property("apes", FefuAdditionalProfessionalEducationProgramForStudent.student().id()))
                .where(eq(property("apes",FefuAdditionalProfessionalEducationProgramForStudent.program()), value(model.getDpoProgram()))).createStatement(getSession()).list();
        builder.add(MQExpression.in(STUDENT_ALIAS, Student.id(), studIds));
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        AbstractStudentOrder order = model.getParagraph().getOrder();
        FefuExtractPrintFormManager.instance().dao().saveOrUpdatePrintForm(order, model.getUploadFileOrder());

    }

    @Override
    protected void appendStudentsWithGroup(MQBuilder builder)
    {
        /* Нужны студенты и без группы*/
    }

}