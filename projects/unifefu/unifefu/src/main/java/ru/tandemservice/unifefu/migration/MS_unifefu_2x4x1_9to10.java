package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x1_9to10 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuEntrantOrderExtension

        //  свойство order стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("fefuentrantorderextension_t", "order_id", true);
            tool.createColumn("fefuentrantorderextension_t", new DBColumn("orderid_p", DBType.LONG));
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuStudentOrderExtension

        //  свойство order стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("fefustudentorderextension_t", "order_id", true);
            tool.createColumn("fefustudentorderextension_t", new DBColumn("orderid_p", DBType.LONG));
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuDirectumLog

        // создано свойство orderExt
        {
            // создать колонку
            tool.createColumn("fefudirectumlog_t", new DBColumn("orderext_id", DBType.LONG));
        }

        // создано свойство enrOrderExt
        {
            // создать колонку
            tool.createColumn("fefudirectumlog_t", new DBColumn("enrorderext_id", DBType.LONG));
        }

        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select dl.ID, sox.ID, eox.ID, aso.ID, aeo.id from FEFUDIRECTUMLOG_T dl" +
                " left outer join FEFUSTUDENTORDEREXTENSION_T sox on dl.ORDER_ID=sox.ORDER_ID" +
                " left outer join FEFUENTRANTORDEREXTENSION_T eox on dl.ENRORDER_ID=eox.ORDER_ID" +
                " left outer join ABSTRACTSTUDENTORDER_T aso on dl.ORDER_ID=aso.ID" +
                " left outer join ABSTRACTENTRANTORDER_T aeo on dl.ENRORDER_ID=aeo.ID" +
                " where sox.id is not null or eox.ID is not null or aso.ID is not null or aeo.ID is not null");
        ResultSet rs = stmt.getResultSet();

        PreparedStatement updateLog = tool.prepareStatement("update FEFUDIRECTUMLOG_T set orderext_id=?, enrorderext_id=? where id=?");
        PreparedStatement updateStudExt = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set order_id=? where id=?");
        PreparedStatement updateEnrExt = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set order_id=? where id=?");

        while (rs.next())
        {
            Object orderExtIdObj = rs.getObject(2);
            Object enrOrderExtIdObj = rs.getObject(3);

            Long orderExtId = null != orderExtIdObj ? ((Number) orderExtIdObj).longValue() : null;
            Long enrOrderExtId = null != enrOrderExtIdObj ? ((Number) enrOrderExtIdObj).longValue() : null;

            if (null != orderExtId)
            {
                updateStudExt.setObject(1, rs.getObject(4));
                updateStudExt.setLong(2, orderExtId);
                updateStudExt.executeUpdate();
            }

            if (null != enrOrderExtId)
            {
                updateEnrExt.setObject(1, rs.getObject(5));
                updateEnrExt.setLong(2, enrOrderExtId);
                updateEnrExt.executeUpdate();
            }

            updateLog.setObject(1, orderExtId);
            updateLog.setObject(2, enrOrderExtId);
            updateLog.setLong(3, rs.getLong(1));
            updateLog.executeUpdate();
        }

        updateStudExt.close();
        updateEnrExt.close();
        updateLog.close();
        stmt.close();
    }
}