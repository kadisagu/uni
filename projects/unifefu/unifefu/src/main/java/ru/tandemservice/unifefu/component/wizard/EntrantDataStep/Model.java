/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.EntrantDataStep;

/**
 * @author Nikolay Fedorovskih
 * @since 03.06.2013
 */
public class Model extends ru.tandemservice.uniec.component.wizard.EntrantDataStep.Model
{
    private ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.Model _diplomaModel = new ru.tandemservice.unifefu.component.entrant.OlympiadDiplomaAddEdit.Model();

    @Override
    public ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.Model getDiplomaModel()
    {
        return _diplomaModel;
    }

    @Override
    public void setDiplomaModel(ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.Model diplomaModel)
    {
        _diplomaModel = diplomaModel;
    }

}