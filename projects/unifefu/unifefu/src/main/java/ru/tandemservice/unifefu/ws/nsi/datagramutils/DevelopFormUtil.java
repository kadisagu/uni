/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.EducationFormType;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 07.05.2014
 */
public class DevelopFormUtil extends SimpleNsiCatalogUtil<EducationFormType, DevelopForm>
{
    public static final String EDUCATION_FORM_GROUP_NSI_FIELD = "EducationFormGroup";

    @Override
    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            super.getNsiFields();
            NSI_FIELDS.add(EDUCATION_FORM_GROUP_NSI_FIELD);
        }
        return NSI_FIELDS;
    }

    @Override
    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            super.getEntityFields();
            ENTITY_FIELDS.add(DevelopForm.group().s());
        }
        return ENTITY_FIELDS;
    }

    @Override
    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            super.getNsiToObFieldsMap();
            NSI_TO_OB_FIELDS_MAP.put(EDUCATION_FORM_GROUP_NSI_FIELD, DevelopForm.group().s());
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    @Override
    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            super.getObToNsiFieldsMap();
            OB_TO_NSI_FIELDS_MAP.put(DevelopForm.group().s(), EDUCATION_FORM_GROUP_NSI_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    public long getNsiDevelopFormGroupHash(EducationFormType nsiEntity, boolean caseInsensitive)
    {
        if (null == nsiEntity.getEducationFormGroup()) return 0;
        if (caseInsensitive) return MD5HashBuilder.getCheckSum(nsiEntity.getEducationFormGroup().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(nsiEntity.getEducationFormGroup());
    }

    @Override
    public boolean isFieldEmpty(EducationFormType nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case EDUCATION_FORM_GROUP_NSI_FIELD:
                return null == nsiEntity.getEducationFormGroup();
            default:
                return super.isFieldEmpty(nsiEntity, fieldName);
        }
    }

    @Override
    public long getNsiFieldHash(EducationFormType nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case EDUCATION_FORM_GROUP_NSI_FIELD:
                return getNsiDevelopFormGroupHash(nsiEntity, caseInsensitive);
            default:
                return super.getNsiFieldHash(nsiEntity, fieldName, caseInsensitive);
        }
    }

    public long getDevelopFormGroupHash(DevelopForm entity, boolean caseInsensitive)
    {
        if (null == entity.getGroup()) return 0;
        if (caseInsensitive) return MD5HashBuilder.getCheckSum(entity.getGroup().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(entity.getGroup());
    }

    @Override
    public long getEntityFieldHash(DevelopForm entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case DevelopForm.P_GROUP:
                return getDevelopFormGroupHash(entity, caseInsensitive);
            default:
                return super.getEntityFieldHash(entity, nsiId, fieldName, caseInsensitive);
        }
    }
}