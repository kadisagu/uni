/* $Id$ */
package ru.tandemservice.unifefu.base.ext.DiplomaIssuance.ui.logic;

import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentDao;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unifefu.entity.DiplomaIssuanceFefuExt;

import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 26.09.2014
 */
public class DipDocumentExtDao extends DipDocumentDao implements IDipDocumentExtDao
{


    @Override
    public void saveDiplomaIssuanceExtension(DiplomaIssuance diplomaIssuance, DiplomaIssuanceFefuExt diplomaIssuanceFefuExt, List<DiplomaContentIssuance> issuanceContentList, Map<Long, DipDiplomaBlank> oldBlankMap)
    {
        saveDiplomaIssuance(diplomaIssuance, issuanceContentList, oldBlankMap);
        if(diplomaIssuanceFefuExt!=null && diplomaIssuanceFefuExt.getRegistrationNumberBook()!=null)
        {
            saveOrUpdate(diplomaIssuanceFefuExt);
        }
    }


}
