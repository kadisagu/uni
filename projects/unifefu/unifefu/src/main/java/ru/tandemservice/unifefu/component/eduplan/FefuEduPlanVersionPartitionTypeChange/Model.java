/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionPartitionTypeChange;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionPartitionType;
import ru.tandemservice.unifefu.entity.catalog.FefuSchedulePartitionType;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 03.06.2013
 */
@Input
(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
    private Long _id;
    private FefuEduPlanVersionPartitionType _versionPartitionType;
    private List<FefuSchedulePartitionType> _partitionTypes;
    private FefuSchedulePartitionType _partitionTypeOld;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public FefuEduPlanVersionPartitionType getVersionPartitionType()
    {
        return _versionPartitionType;
    }

    public void setVersionPartitionType(FefuEduPlanVersionPartitionType versionPartitionType)
    {
        _versionPartitionType = versionPartitionType;
    }

    public List<FefuSchedulePartitionType> getPartitionTypes()
    {
        return _partitionTypes;
    }

    public void setPartitionTypes(List<FefuSchedulePartitionType> partitionTypes)
    {
        _partitionTypes = partitionTypes;
    }

    public FefuSchedulePartitionType getPartitionTypeOld()
    {
        return _partitionTypeOld;
    }

    public void setPartitionTypeOld(FefuSchedulePartitionType partitionTypeOld)
    {
        _partitionTypeOld = partitionTypeOld;
    }
}