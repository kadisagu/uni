/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu11.ParagraphAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract;

import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 19.11.2013
 */
public class Model extends AbstractListParagraphAddEditModel<FefuCompensationTypeTransferStuListExtract> implements IGroupModel
{
    private List<DevelopForm> _developFormList;
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _groupListModel;

    private Course _course;
    private Group _group;
    private Date _protocolDate;
    private Date _transferDate;
    private String _protocolNumbers;
    private DevelopForm _developForm;
    private CompensationType _compensationTypeNew;

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    public void setProtocolDate(Date protocolDate)
    {
        _protocolDate = protocolDate;
    }

    public Date getTransferDate()
    {
        return _transferDate;
    }

    public void setTransferDate(Date transferDate)
    {
        _transferDate = transferDate;
    }

    public String getProtocolNumbers()
    {
        return _protocolNumbers;
    }

    public void setProtocolNumbers(String protocolNumbers)
    {
        _protocolNumbers = protocolNumbers;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public CompensationType getCompensationTypeNew()
    {
        return _compensationTypeNew;
    }

    public void setCompensationTypeNew(CompensationType compensationTypeNew)
    {
        _compensationTypeNew = compensationTypeNew;
    }

    public boolean isAnyOrderParametersCantBeChanged()
    {
        return !isParagraphOnlyOneInTheOrder();
    }
}
