/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu7.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaSuccessWithDismissStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuGiveDiplomaSuccessWithDismissStuListExtract, Model>
{
}
