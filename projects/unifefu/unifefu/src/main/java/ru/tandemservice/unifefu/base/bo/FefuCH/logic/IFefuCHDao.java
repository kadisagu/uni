/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuCH.logic;



import ru.tandemservice.unifefu.ws.integralCH.entity.RequestAnswer;
import ru.tandemservice.unifefu.ws.integralCH.entity.RequestData;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 13.03.2015
 */
public interface IFefuCHDao
{
   List<RequestAnswer> getScheduleData(RequestData data);
}