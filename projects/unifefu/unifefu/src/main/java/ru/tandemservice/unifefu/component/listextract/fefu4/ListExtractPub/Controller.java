/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubController;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class Controller extends AbstractListExtractPubController<FefuAcadGrantAssignStuEnrolmentListExtract, Model, IDAO>
{
}