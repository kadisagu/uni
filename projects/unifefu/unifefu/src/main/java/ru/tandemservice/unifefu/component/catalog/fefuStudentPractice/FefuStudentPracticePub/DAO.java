/* $Id$ */
package ru.tandemservice.unifefu.component.catalog.fefuStudentPractice.FefuStudentPracticePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice;

/**
 * @author Nikolay Fedorovskih
 * @since 25.06.2013
 */
public class DAO extends DefaultCatalogPubDAO<FefuStudentPractice, Model> implements IDAO
{
}