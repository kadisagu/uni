/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unifefu.brs.base.BaseFefuReportAddUI;
import ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.FefuBrsAttestationResultsReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.logic.FefuBrsAttestationResultsReportParams;
import ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.ui.View.FefuBrsAttestationResultsReportView;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.report.FefuBrsAttestationResultsReport;

import java.util.Collections;
import java.util.Date;

/**
 * @author nvankov
 * @since 12/3/13
 */
public class FefuBrsAttestationResultsReportAddUI extends BaseFefuReportAddUI<FefuBrsAttestationResultsReportParams>
{
    @Override
    protected void fillDefaults(FefuBrsAttestationResultsReportParams reportSettings)
    {
        super.fillDefaults(reportSettings);
        reportSettings.setFormativeOrgUnit(getCurrentFormativeOrgUnit());
        reportSettings.setCheckDate(new Date());
        reportSettings.setOnlyFilledJournals(FefuBrsReportManager.instance().yesNoItemListExtPoint().getItem(FefuBrsReportManager.YES_ID.toString()));
        reportSettings.setGroupBy(FefuBrsReportManager.instance().groupByItemListExtPoint().getItem(FefuBrsReportManager.GROUP_BY_GROUP_ID.toString()));
        reportSettings.setAttestation(FefuBrsReportManager.instance().attestationItemListExtPoint().getItem(FefuBrsReportManager.CURRENT_ATTESTATION_ID.toString()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuBrsReportManager.GROUP_DS.equals(dataSource.getName()))
        {
            if(null != getReportSettings().getFormativeOrgUnit())
                dataSource.put(FefuBrsReportManager.PARAM_FORMATIVE_OU, Collections.singletonList(getReportSettings().getFormativeOrgUnit().getId()));
        }
    }

    public boolean getShowCurrentAttestation()
    {
        return null != getReportSettings().getAttestation() && getReportSettings().getAttestation().getId().equals(FefuBrsReportManager.CURRENT_ATTESTATION_ID);

    }

    public void onClickApply()
    {
        FefuBrsAttestationResultsReport report = FefuBrsAttestationResultsReportManager.instance().dao().createReport(getReportSettings());
        deactivate();
        _uiActivation.asDesktopRoot(FefuBrsAttestationResultsReportView.class).parameter(UIPresenter.PUBLISHER_ID, report.getId()).activate();
    }
}
