package ru.tandemservice.unifefu.component.menu.MarkDistribution;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.menu.MarkDistribution.Model;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.unifefu.UniFefuDefines;

import java.util.List;
import java.util.Map;

/**
 * @author amakarova
 * @since 17.05.2013
 */
public class Controller extends ru.tandemservice.uniec.component.menu.MarkDistribution.Controller
{
    public void onClickPrint(IBusinessComponent component)
    {
        String typeList = UniecDefines.SCRIPT_ENROLLMENT_EXAM_LIST;
        // скрипт
        printList(typeList, component);
    }

    public void onClickPrintSitList(IBusinessComponent component)
    {
        String typeList = UniFefuDefines.SCRIPT_ENROLLMENT_SIT_LIST;
        // скрипт
        printList(typeList, component);
    }

    public void onClickPrintSitExamCardList(IBusinessComponent component)
    {
        String typeList = UniFefuDefines.SCRIPT_ENROLLMENT_SIT_EXAM_CARD_LIST;
        // скрипт
        printList(typeList, component);
    }

    public void onClickPrintCipherList(IBusinessComponent component)
    {
        String typeList = UniFefuDefines.SCRIPT_ENROLLMENT_CIPHER_LIST;
        // скрипт
        printList(typeList, component);
    }

    private void printList(String typeList, IBusinessComponent component) {

        Model model = getModel(component);
        if (model.getDiscipline() == null) return; // не печатается
        List<Map<String, Object>> table = getEntrantList(model);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, typeList),
                "typeList", typeList,
                "hsId", model.getEducationLevelsHighSchool() == null ? null : model.getEducationLevelsHighSchool().getId(),
                "formativeOrgUnit", model.getFormativeOrgUnit() == null ? null : model.getFormativeOrgUnit().getTitle(),
                "disciplineTitle", model.getDisciplineTitle(),
                "table", table
        );
    }
}
