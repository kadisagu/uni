/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.validator;

/**
 * Валидатор числа с плавающей точкой: проверяет, что число меньше или равно заданному.
 * @author Alexander Zhebko
 * @since 22.07.2013
 */
public class LEDoubleValidator extends Validator<Double>
{
    private double maxValue;

    public LEDoubleValidator(double maxValue)
    {
        this.maxValue = maxValue;
    }

    @Override
    protected boolean validateValue(Double value)
    {
        return value <= maxValue;
    }

    @Override
    public String getInvalidMessage()
    {
        return "Значение больше " + maxValue + ".";
    }
}