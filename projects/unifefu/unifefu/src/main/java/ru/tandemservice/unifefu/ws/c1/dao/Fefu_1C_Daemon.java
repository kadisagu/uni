/* $Id$ */
package ru.tandemservice.unifefu.ws.c1.dao;

import org.apache.axis.MessageContext;
import org.apache.axis.encoding.SerializationContext;
import org.apache.axis.encoding.Serializer;
import org.apache.axis.server.AxisServer;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.person.base.entity.Person;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import ru.tandemservice.movestudent.component.commons.gradation.*;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.entity.Fefu_1C_log;
import ru.tandemservice.unifefu.entity.Fefu_1C_pack;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unifefu.ws.c1.Data;
import ru.tandemservice.unifefu.ws.c1.TableRecordType;
import ru.tandemservice.unifefu.ws.c1.TandemLocator;
import ru.tandemservice.unifefu.ws.c1.src.OrderCode_1C;
import ru.tandemservice.unifefu.ws.c1.src.OrderMap_1C;
import ru.tandemservice.unifefu.ws.c1.src.OrderType_1C;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimv.IAbstractDocument;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;
import java.io.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 13.12.2013
 */
public class Fefu_1C_Daemon extends UniBaseDao implements IFefu_1C_Daemon
{
    private static final String STUDENT_POST_GUID = ApplicationRuntime.getProperty("fefu.1C_student_post_guid");
    private static final String NONE_GROUP_GUID = ApplicationRuntime.getProperty("fefu.1C_none_group_guid");
    //private static final String ACADEMICAL_VACATION_GROUP_GUID = ApplicationRuntime.getProperty("fefu.1C_academical_vacation_guid");
    //private static final String FREE_ACADEMICAL_VACATION_GROUP_GUID = ApplicationRuntime.getProperty("fefu.1C_free_academical_vacation_guid");


    private static final String OK_RESULT = "0 - ОК";

    public static final String GLOBAL_DAEMON_LOCK = Fefu_1C_Daemon.class.getSimpleName() + ".lock";
    public static final String WS_ADDRESS = ApplicationRuntime.getProperty("fefu.1C_endpointAddress");
    public static final boolean DAEMON_ACTIVATED = "true".equals(ApplicationRuntime.getProperty("fefu.1C_daemon_activate"));

    public static final String WITH_ATTENDANCE = "with_attendance"; // отпуск c посещением занятий
    public static final String WITHOUT_ATTENDANCE = "without_attendance"; // отпуск без посещения занятий

    // Отладочный режим - на сервер 1С ничего не передается
    public static final boolean LOCAL_DEBUG = "true".equals(ApplicationRuntime.getProperty("fefu.1C_local_debug"));

    // Форматы приказов. Нужны, чтобы одна и та же выписка могла попасть в один или разные пакеты в разных ипостасях
    // Например, выписка о назначении стипендии может включать надбавку старосте - для этой надбавки нужен отдельный пакет,
    // со своими данными, хотя и того же типа, что и назначение стипендии.
    // ВНИМАНИЕ! Значения не менять! Они храняться в базе, т.е. по сути это захардкоженый справочник.
    public static final int ENROLLMENT_FORMAT = 1;
    public static final int TRANSFER_FORMAT = 2;
    public static final int EXCLUDE_FORMAT = 3;
    public static final int ASSIGN_PAYMENT_FORMAT = 4;
    public static final int ASSIGN_GROUP_MANAGER_BONUS_FORMAT = 5;
    public static final int TAKEOFF_PAYMENT_FORMAT = 6;

    public static final TandemLocator LOCATOR;

    static
    {
        if (WS_ADDRESS == null || WS_ADDRESS.isEmpty())
        {
            LOCATOR = null;
        }
        else
        {
            LOCATOR = new TandemLocator();
            LOCATOR.setTandemSoap12EndpointAddress(WS_ADDRESS);
        }
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(Fefu_1C_Daemon.class.getName(), 30, GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            try
            {
                if (DAEMON_ACTIVATED)
                {
                    IFefu_1C_Daemon.instance.get().doSynchronization(null, false);
                }
            }
            catch (final Throwable t)
            {
                logger.error(t.getMessage(), t);
                Debug.exception(t.getMessage(), t);
            }
        }
    };

    private void addNewItem(IEntity extract, int format)
    {
        Fefu_1C_log logItem = new Fefu_1C_log();
        logItem.setExtract((IAbstractDocument) extract);
        logItem.setFormat(format);
        save(logItem);
    }

    /**
     * Поддерживается ли выписка. Для отправки в 1С предназначены только конкретные типы выписок.
     */
    private boolean isSupported(IAbstractExtract extract)
    {
        return extract != null && (OrderMap_1C.ORDER_CODES_MAP.containsKey(extract.getType().getCode()) || extract instanceof EnrollmentExtract);
    }

    @Override
    public void addNewItems(Collection<IAbstractExtract> items)
    {
        items.stream().filter(this::isSupported).forEach(extract -> {
            if (extract instanceof IEnrollmentExtract)
                addNewItem(extract, ENROLLMENT_FORMAT);

            // Форма Ф2 - движение (перевод)
            if (extract instanceof ITransferExtract)
                addNewItem(extract, TRANSFER_FORMAT);

            // Форма Ф3 - отчисление
            if (extract instanceof IExcludeExtract)
                addNewItem(extract, EXCLUDE_FORMAT);

            // Форма Ф4 - назначение выплаты
            if (extract instanceof IAssignPaymentExtract)
                addNewItem(extract, ASSIGN_PAYMENT_FORMAT);

            // Отдельно форма Ф4 для начислений старостам
            if (extract instanceof IGroupManagerPaymentExtract)
                addNewItem(extract, ASSIGN_GROUP_MANAGER_BONUS_FORMAT);

            // Форма Ф5 - снятие выплаты
            if (extract instanceof ITakeOffPayment)
                addNewItem(extract, TAKEOFF_PAYMENT_FORMAT);
        });
    }

    private void checkGUID(String guid, String guidName)
    {
        if (guid == null || guid.length() != 36)
            throw new RuntimeException("1C GUID for " + guidName + " is missing in properties or invalid: " + guid);
    }

    @Override
    public void doSynchronization(Collection<Long> extractIds, boolean systemAction)
    {
        if (LOCATOR == null)
            return;

        // Проверяем наличие в пропертях константных 1С-гуидов
        checkGUID(STUDENT_POST_GUID, "student post");
        checkGUID(NONE_GROUP_GUID, "none group");
        //checkGUID(ACADEMICAL_VACATION_GROUP_GUID, "academical vacation");
        //checkGUID(FREE_ACADEMICAL_VACATION_GROUP_GUID, "free academical vacation");


        // Исключить выписки, которые успешно отправлены в 1С
        DQLSelectBuilder successSubBuilder = new DQLSelectBuilder().fromEntity(Fefu_1C_log.class, "l")
                .column(property("l.id"))
                .where(eq("l." + Fefu_1C_log.L_EXTRACT, "log." + Fefu_1C_log.L_EXTRACT))
                .where(eq(property("l", Fefu_1C_log.pack().success()), value(Boolean.TRUE)));

        // Получаем список выписок, которые есть в логе, но до сих пор успешно не синхронизированные.
        // Т.е. это либо вновь добавленные выписки, либо другие, до сих пор успешно не синхронизированные
        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Fefu_1C_log.class, "log")
                .column(DQLFunctions.max(property("log.id")))
//                .where(notExists(successSubBuilder.buildQuery()))
                .group(property("log", Fefu_1C_log.extract().id()))
                .group(property("log", Fefu_1C_log.format()));
        if (!systemAction)
            subBuilder.where(notExists(successSubBuilder.buildQuery()));

        if (extractIds != null)
            subBuilder.where(in("log." + Fefu_1C_log.L_EXTRACT, extractIds));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Fefu_1C_log.class, "e")
                .column(property("e"))
                .where(in("e.id", subBuilder.buildQuery()));

        Collection<Fefu_1C_log> logItems = builder.createStatement(getSession()).list();
        if (logItems.isEmpty())
            return;

        try
        {
            PacksContainer packsContainer = new PacksContainer();

            // Раскладываем выписки по пакетам
            for (Fefu_1C_log logItem : logItems)
            {
                packsContainer.add(logItem);
            }

            // Подстраиваемся под 1С, которые не умеют обрабатывать два пакета с одинаковыми номерами приказов
            fixOrderNumbers(packsContainer);

            // Рассылаем пакеты
            for (SendPackage pack : packsContainer.getPacks())
            {
                pack.doSend();
            }
        }
        catch (ServiceException | IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    private void fixOrderNumbers(PacksContainer packsContainer)
    {
        // Поскольку в 1С не умеют обрабатывать разные пакеты с одним номером приказа, то добавляем постфикс (/2, /3 и т.д.) после номера приказа.
        // Номер + постфикс должны быть уникальны в рамках приказа. Т.е. другой приказ с таким же номером (в новом учебном году) не должен зависеть от
        // старых пакетов - отправляется, как будто он первый.
        Map<IAbstractDocument, List<GroupByOrderPack>> map = new HashMap<>();
        Set<Long> orderIds = new HashSet<>();
        for (SendPackage pack : packsContainer.getPacks())
        {
            if (pack instanceof GroupByOrderPack)
            {
                IAbstractDocument order = ((GroupByOrderPack) pack).getOrder();
                List<GroupByOrderPack> packs = map.get(order);
                if (null == packs)
                {
                    packs = new ArrayList<>();
                    map.put(order, packs);
                    orderIds.add(order.getId());
                }
                packs.add((GroupByOrderPack) pack);
            }
        }

        // Достаем ранее отправленные номера по приказам из этой партии
        Map<Long, List<String>> sentMap = new HashMap<>();
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Fefu_1C_pack.class, "p")
                    /* 0 */.column(property("p", Fefu_1C_pack.order().id()))
                    /* 1 */.column(property("p", Fefu_1C_pack.P_SENT_ORDER_NUMBER))
                .where(in(property("p", Fefu_1C_pack.order().id()), orderIds))
                .where(eq(property("p", Fefu_1C_pack.P_SUCCESS), value(Boolean.TRUE)));

        for (Object[] item : this.<Object[]>getList(builder))
        {
            Long id = (Long) item[0];
            String number = (String) item[1];
            List<String> numbers = sentMap.get(id);
            if (null == numbers)
            {
                numbers = new ArrayList<>();
                sentMap.put(id, numbers);
            }
            numbers.add(number);
        }

        // Составляем новые номера приказов для отправляемых пакетов в соответствии с тем, надо ли дописывать постфикс
        for (Map.Entry<IAbstractDocument, List<GroupByOrderPack>> entry : map.entrySet())
        {
            IAbstractOrder order = (IAbstractOrder) entry.getKey();
            String orderNumber = order.getNumber();

            // Смотрим отправлялись ли ранее пакеты для этого приказа
            List<String> sentNumbers = sentMap.get(order.getId());
            int maxPostfix = 0;
            if (sentNumbers != null)
            {
                maxPostfix = 1;
                // Если отправлялись, достаем максимальный постфикс
                // Длина строки с номером приказа. Нужна, чтобы доставать сразу постфикс из отправленных номеров, содержащих постфикс. +1 - это слеш (типа: номер_приказа/1)
                int numberStrLen = orderNumber.length() + 1;
                for (String sentNumber : sentNumbers)
                {
                    if (sentNumber.length() > numberStrLen && sentNumber.startsWith(sentNumber))
                    {
                        try
                        {
                            int postfix = Integer.decode(sentNumber.substring(numberStrLen));
                            if (postfix > maxPostfix)
                                maxPostfix = postfix;
                        }
                        catch (NumberFormatException e)
                        { /**/ }
                    }
                }
            }
            else
            {
                if (entry.getValue().size() == 1)
                {
                    // Приказ такой отправляется впервые и пакет с таким приказов всего один - постфикс не нужен
                    entry.getValue().get(0).setOrderNumber(orderNumber);
                    continue;
                }
            }

            for (GroupByOrderPack pack : entry.getValue())
            {
                if (0 == maxPostfix)
                {
                    pack.setOrderNumber(orderNumber);
                    maxPostfix++;
                }
                else
                {
                    // Да, начинаться будет с /2
                    pack.setOrderNumber(orderNumber + "/" + ++maxPostfix);
                }
            }
        }
    }

    private abstract class SendPackage
    {
        private List<Fefu_1C_log> _items = new ArrayList<>();

        public void add(Fefu_1C_log item)
        {
            _items.add(item);
        }

        public List<Fefu_1C_log> getItems()
        {
            return _items;
        }

        public void saveResults(Data request, String response) throws IOException
        {
            Fefu_1C_pack pack = new Fefu_1C_pack();
            pack.setDispatchDate(Calendar.getInstance().getTime());
            pack.setRequest(toXML(request));
            pack.setResponse(response);
            pack.setSuccess(OK_RESULT.equalsIgnoreCase(response));
            if (this instanceof GroupByOrderPack)
            {
                pack.setOrder(((GroupByOrderPack) this).getOrder());
                pack.setSentOrderNumber(((GroupByOrderPack) this).getOrderNumber());
            }
            save(pack);

            for (Fefu_1C_log logItem : getItems())
            {
                logItem.setPack(pack);
                saveOrUpdate(logItem);
            }
        }

        public abstract void doSend() throws ServiceException, IOException;
    }

    /**
     * Пакеты, для которых передается номер приказа (раньше это были Ф1, Ф4 и Ф5, а теперь - все)
     */
    private abstract class GroupByOrderPack extends SendPackage
    {
        private IAbstractOrder _order;
        private String _orderNumber;
        private String _orderCode;

        public IAbstractOrder getOrder()
        {
            return _order;
        }

        public void setOrder(IAbstractOrder order)
        {
            _order = order;
        }

        public String getOrderNumber()
        {
            return _orderNumber;
        }

        public void setOrderNumber(String orderNumber)
        {
            _orderNumber = orderNumber;
        }

        public String getOrderCode()
        {
            return _orderCode;
        }

        public void setOrderCode(String orderCode)
        {
            _orderCode = orderCode;
        }

        protected Data createData(List<TableRecordType> table)
        {
            Data data = new Data();
            data.setScholarshipOrderDate((Date) getOrder().getProperty("commitDate"));
            data.setScholarshipOrderNo(getOrderNumber());
            data.setScholarshipOrderCode(getOrderCode());
            data.setTable(table.toArray(new TableRecordType[table.size()]));
            return data;
        }
    }

    private abstract class ScholarshipPack extends GroupByOrderPack
    {
        private Date _startDate;
        private Date _endDate;

        public Date getStartDate()
        {
            return _startDate;
        }

        public void setStartDate(Date startDate)
        {
            _startDate = startDate;
        }

        public Date getEndDate()
        {
            return _endDate;
        }

        public void setEndDate(Date endDate)
        {
            _endDate = endDate;
        }

        @Override
        protected Data createData(List<TableRecordType> table)
        {
            Data data = super.createData(table);
            data.setScholarshipStartDate(getStartDate());
            data.setScholarshipEndDate(getEndDate());
            return data;
        }
    }

    private class EnrollmentPack extends GroupByOrderPack
    {
        @Override
        public void doSend() throws ServiceException, IOException
        {
            if (getItems().isEmpty())
                return;

            List<TableRecordType> table = new ArrayList<>(getItems().size());

            for (int i = 0; i < getItems().size(); i++)
            {
                Fefu_1C_log item = getItems().get(i);
                IEnrollmentExtract extract = getNotNull(item.getExtract().getId());

                try
                {
                    table.add(createTable(extract, getDepartmentGUID(extract.getGroupNew()), null, extract.getEntranceDate(), null, null, STUDENT_POST_GUID));
                }
                catch (InvalidDataException e)
                {
                    // Гуид не найден или что-то еще. Этот студент не попадает в пакет. Отправится в следующий раз, если всё уже будет ОК.
                    Debug.exception(e.getMessage(), e);
                    logger.error(e.getMessage(), e);
                }
            }
            if (table.isEmpty()) return;

            Data data = createData(table);

            String result = !LOCAL_DEBUG ? LOCATOR.getTandemSoap12().employment(data) : OK_RESULT;

            saveResults(data, result);
        }
    }

    private class TransferPack extends GroupByOrderPack
    {
        private String getAttendanceType(StudentStatus studentStatus)
        {
            return UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITH_ATTENDANCE.equals(studentStatus.getCode()) ? WITH_ATTENDANCE : WITHOUT_ATTENDANCE;
        }

        @Override
        public void doSend() throws ServiceException, IOException
        {
            if (getItems().isEmpty())
                return;

            List<TableRecordType> table = new ArrayList<>(getItems().size());

            for (int i = 0; i < getItems().size(); i++)
            {
                Fefu_1C_log item = getItems().get(i);
                ITransferExtract extract = getNotNull(item.getExtract().getId());
                try
                {
                    // Костыль для DEV-5015 п.4
                    // Для приказов об отпуске по беременности и родам и отпуске по уходу за ребенком (до 1,5 лет и до 3 лет) передавать статус "с посещением" или "без посещения" в поле calcKind
                    String calcKind = null;
                    switch (item.getExtract().getType().getCode())
                    {
                        case StudentExtractTypeCodes.WEEKEND_PREGNANCY_MODULAR_ORDER: // О предоставлении отпуска по беременности и родам
                        case StudentExtractTypeCodes.WEEKENT_VARIANT_2_MODULAR_ORDER: // О предоставлении академического отпуска
                            calcKind = WITHOUT_ATTENDANCE;
                            break;

                        case StudentExtractTypeCodes.WEEKEND_CHILD_ONE_AND_HALF_MODULAR_ORDER: // О предоставлении отпуска по уходу за ребенком до 1,5 лет
                        case StudentExtractTypeCodes.WEEKEND_CHILD_THREE_MODULAR_ORDER: // О предоставлении отпуска по уходу за ребенком до 3 лет
                            calcKind = getAttendanceType((StudentStatus) ((AbstractStudentExtract) extract).getProperty("studentStatusNew"));
                            break;

                        case StudentExtractTypeCodes.WEEKEND_PREGNANCY_OUT_MODULAR_ORDER:  // О выходе из отпуска по беременности и родам
                        case StudentExtractTypeCodes.WEEKEND_OUT_MODULAR_ORDER:   // О выходе из академического отпуска
                        case StudentExtractTypeCodes.WEEKEND_CHILD_OUT_VARIANT_2_MODULAR_ORDER:  // О выходе из отпуска по уходу за ребенком
                            calcKind = getAttendanceType((StudentStatus) ((AbstractStudentExtract) extract).getProperty("studentStatusOld"));
                            break;
                    }

                    table.add(createTable(
                            extract,
                            getDepartmentGUID(extract.getGroupNew(), extract, false),
                            getDepartmentGUID(extract.getGroupOld(), extract, true),
                            extract.getTransferOrOrderDate(),
                            null,
                            calcKind,
                            null
                    ));
                }
                catch (InvalidDataException e)
                {
                    // Гуид не найден или что-то еще. Этот студент не попадает в пакет. Отправится в следующий раз, если всё уже будет ОК.
                    Debug.exception(e.getMessage(), e);
                    logger.error(e.getMessage(), e);
                }
            }

            if (table.isEmpty()) return;

            Data data = createData(table);

            String result = !LOCAL_DEBUG ? LOCATOR.getTandemSoap12().transfer(data) : OK_RESULT;

            saveResults(data, result);
        }
    }

    private class ExcludePack extends GroupByOrderPack
    {
        @Override
        public void doSend() throws ServiceException, IOException
        {
            if (getItems().isEmpty())
                return;

//            TableRecordType[] table = new TableRecordType[getItems().size()];
            List<TableRecordType> table = new ArrayList<>(getItems().size());

            for (int i = 0; i < getItems().size(); i++)
            {
                Fefu_1C_log item = getItems().get(i);
                IExcludeExtract extract = getNotNull(item.getExtract().getId());
                try
                {
                    Group group = ((AbstractStudentExtract) extract).getEntity().getGroup();
                    table.add(createTable(extract, null, getDepartmentGUID(group), extract.getExcludeDate(), null, null, null));
                }
                catch (InvalidDataException e)
                {
                    // Гуид не найден или что-то еще. Этот студент не попадает в пакет. Отправится в следующий раз, если всё уже будет ОК.
                    Debug.exception(e.getMessage(), e);
                    logger.error(e.getMessage(), e);
                }
            }
            if (table.isEmpty()) return;

            Data data = createData(table);

            String result = !LOCAL_DEBUG ? LOCATOR.getTandemSoap12().discharge(data) : OK_RESULT;

            saveResults(data, result);
        }
    }

    private class AssignPaymentPack extends ScholarshipPack
    {
        @Override
        public void doSend() throws ServiceException, IOException
        {
            if (getItems().isEmpty())
                return;

            List<TableRecordType> table = new ArrayList<>(getItems().size());

            for (int i = 0; i < getItems().size(); i++)
            {
                Fefu_1C_log item = getItems().get(i);
                IAbstractExtract extract = getNotNull(item.getExtract().getId());
                OrderCode_1C code = OrderMap_1C.get(extract, OrderType_1C.ASSIGN_PAYMENT);
                if (code == null)
                    throw new RuntimeException("Code for extract " + extract.getType().getCode() + " not found for format " + item.getFormat());

                String calcKind;
                BigDecimal amount;
                Group department = ((AbstractStudentExtract) extract).getEntity().getGroup();

                try
                {
                    switch (item.getFormat())
                    {
                        case ASSIGN_PAYMENT_FORMAT:
                            amount = ((IAssignPaymentExtract) extract).getPaymentAmount();
                            calcKind = code.getPaymentCode();

                            // !!! КОСТЫЛЬ для приказа О предоставлении отпуска по беременности и родам
                            // TODO Нужен рефакторинг под то, что один приказ может делать несколько выплат.
                            if (StudentExtractTypeCodes.WEEKEND_PREGNANCY_MODULAR_ORDER.equals(extract.getType().getCode()))
                            {
                                WeekendPregnancyStuExtract zloExtract = (WeekendPregnancyStuExtract) extract;
                                if (!zloExtract.isPayBenefit() && !zloExtract.isPayOnetimeBenefit())
                                    continue; // Вообще никаких выплат


                                if (zloExtract.isPayBenefit() && zloExtract.isPayOnetimeBenefit())
                                {
                                    // Есть обе выплаты, нужно вручную добавить еще одну запись в пакет для единовременной выплаты
                                    // Простая выплата добавиться стандартно
                                    table.add(createTable(extract, null, getDepartmentGUID(department), null, amount, OrderMap_1C.EXPECTANT_BENEFIT_ONETIME, null));
                                }
                                else if (zloExtract.isPayOnetimeBenefit())
                                {
                                    // Выплата только одна и не та, которая по умолчанию, подменяем calcKind
                                    calcKind = OrderMap_1C.EXPECTANT_BENEFIT_ONETIME;
                                }
                            }

                            break;
                        case ASSIGN_GROUP_MANAGER_BONUS_FORMAT:
                            amount = ((IGroupManagerPaymentExtract) extract).getGroupManagerPaymentAmount();
                            calcKind = OrderMap_1C.GROUP_CAPTAIN_PREMIUM_PAYMENT_CODE;
                            break;
                        default:
                            // других нам тут не надо
                            throw new IllegalStateException();
                    }


                    table.add(createTable(extract, null, getDepartmentGUID(department), null, amount, calcKind, null));
                }
                catch (InvalidDataException e)
                {
                    // Гуид не найден или что-то еще.
                    Debug.exception(e.getMessage(), e);
                    logger.error(e.getMessage(), e);
                    // Весь пакет (видимо по конкретному приказу) НЕ будет оправлен.
                    return;
                }
            }

            if (table.isEmpty())
                return;

            Data data = createData(table);

            String result = !LOCAL_DEBUG ? LOCATOR.getTandemSoap12().scholarshipON(data) : OK_RESULT;

            saveResults(data, result);
        }
    }

    private class TakeOffPaymentPack extends ScholarshipPack
    {
        @Override
        public void doSend() throws ServiceException, IOException
        {
            if (getItems().isEmpty())
                return;

            List<TableRecordType> table = new ArrayList<>(getItems().size());

            for (int i = 0; i < getItems().size(); i++)
            {
                Fefu_1C_log item = getItems().get(i);
                IAbstractExtract extract = getNotNull(item.getExtract().getId());
                OrderCode_1C code = OrderMap_1C.get(extract, OrderType_1C.TAKE_OFF_PAYMENT);
                if (code == null)
                    throw new RuntimeException("Code for extract " + extract.getType().getCode() + " not found for format " + item.getFormat());


                Group department = ((AbstractStudentExtract) extract).getEntity().getGroup();
                try
                {
                    table.add(createTable(extract, null, getDepartmentGUID(department), null, null, code.getPaymentCode(), null));
                }
                catch (InvalidDataException e)
                {
                    // Гуид не найден или что-то еще.
                    Debug.exception(e.getMessage(), e);
                    logger.error(e.getMessage(), e);
                    // Весь пакет (видимо по конкретному приказу) НЕ будет оправлен.
                    return;
                }
            }

            if (table.isEmpty()) return;
            Data data = createData(table);

            String result = !LOCAL_DEBUG ? LOCATOR.getTandemSoap12().scholarshipOFF(data) : OK_RESULT;

            saveResults(data, result);
        }
    }

    private class PacksContainer
    {
        // Пакеты с простыми типами Ф1, Ф2, Ф3 разбиваем по 50 выписок в пакете, чтобы не было больших передач.
        private final int MAX_ITEMS_COUNT_IN_PACK = 50;
        private List<SendPackage> _packs = new ArrayList<>();

        public List<SendPackage> getPacks()
        {
            return _packs;
        }

        public void add(Fefu_1C_log item)
        {
            IAbstractExtract extract = (IAbstractExtract) item.getExtract();
            Date startDate = null, endDate = null;
            switch (item.getFormat())
            {
                case ASSIGN_PAYMENT_FORMAT:
                    startDate = ((IAssignPaymentExtract) extract).getPaymentBeginDate();
                    endDate = ((IAssignPaymentExtract) extract).getPaymentEndDate();
                    break;
                case ASSIGN_GROUP_MANAGER_BONUS_FORMAT:
                    if (!((IGroupManagerPaymentExtract) extract).hasGroupManagerBonus())
                        return;
                    startDate = ((IGroupManagerPaymentExtract) extract).getGroupManagerBonusBeginDate();
                    endDate = ((IGroupManagerPaymentExtract) extract).getGroupManagerBonusEndDate();
                    break;
                case TAKEOFF_PAYMENT_FORMAT:
                    startDate = ((ITakeOffPayment) extract).getPaymentTakeOffDate();
                    endDate = null;
                    break;
            }

            SendPackage pack = findPack(item.getFormat(), extract.getParagraph().getOrder(), extract.getType().getCode(), startDate, endDate);
            // До задачи DEV-5015 были типы приказов, группируемые по только типу пакета без указания номера и даты приказа, поэтому использовалось ограничение
            // на количество выписок в таких пакетах MAX_ITEMS_COUNT_IN_PACK. Пока можно этот код оставить, на случай возвращения/появления типов пакетов без указания приказа,
            // которые не будут наследоваться от GroupByOrderPack
            if (pack == null || (pack.getItems().size() >= MAX_ITEMS_COUNT_IN_PACK && !(pack instanceof GroupByOrderPack)))
            {
                switch (item.getFormat())
                {
                    case ENROLLMENT_FORMAT:
                        pack = new EnrollmentPack();
                        break;
                    case TRANSFER_FORMAT:
                        pack = new TransferPack();
                        break;
                    case EXCLUDE_FORMAT:
                        pack = new ExcludePack();
                        break;
                    case ASSIGN_PAYMENT_FORMAT:
                    case ASSIGN_GROUP_MANAGER_BONUS_FORMAT:
                        pack = new AssignPaymentPack();
                        break;
                    case TAKEOFF_PAYMENT_FORMAT:
                        pack = new TakeOffPaymentPack();
                        break;
                    default:
                        throw new IllegalStateException();
                }

                if (pack instanceof GroupByOrderPack)
                {
                    GroupByOrderPack orderPack = (GroupByOrderPack) pack;
                    orderPack.setOrder(extract.getParagraph().getOrder());
                    orderPack.setOrderCode(extract.getType().getCode());

                    if (pack instanceof ScholarshipPack)
                    {
                        ScholarshipPack scholarshipPack = (ScholarshipPack) pack;
                        scholarshipPack.setStartDate(startDate);
                        scholarshipPack.setEndDate(endDate);
                    }
                }

                _packs.add(pack);
            }
            pack.add(item);
        }

        private boolean safeCompare(Object o1, Object o2)
        {
            return (o1 == null && o2 == null) || (o1 != null && o1.equals(o2));
        }

        private SendPackage findPack(int format, IAbstractOrder order, String orderCode, Date startDate, Date endDate)
        {
            Class<? extends SendPackage> clazz;
            switch (format)
            {
                case ENROLLMENT_FORMAT:
                    clazz = EnrollmentPack.class;
                    break;
                case TRANSFER_FORMAT:
                    clazz = TransferPack.class;
                    break;
                case EXCLUDE_FORMAT:
                    clazz = ExcludePack.class;
                    break;
                case ASSIGN_PAYMENT_FORMAT:
                case ASSIGN_GROUP_MANAGER_BONUS_FORMAT:
                case TAKEOFF_PAYMENT_FORMAT:
                    clazz = ScholarshipPack.class;
                    break;
                default:
                    throw new IllegalStateException();
            }

            // Идем с конца списка - ищем подходящий по классу и другим параметрам пакет
            for (int i = _packs.size() - 1; i >= 0; i--)
            {
                SendPackage pack = _packs.get(i);
                if (clazz.isInstance(pack))
                {
                    if (pack instanceof GroupByOrderPack)
                    {
                        GroupByOrderPack orderPack = (GroupByOrderPack) pack;
                        if (!(orderPack.getOrder().equals(order) && safeCompare(orderCode, orderPack.getOrderCode())))
                            continue;

                        if (pack instanceof ScholarshipPack)
                        {
                            ScholarshipPack scholarshipPack = (ScholarshipPack) pack;
                            if (!(safeCompare(startDate, scholarshipPack.getStartDate()) && safeCompare(endDate, scholarshipPack.getEndDate())))
                                continue;
                        }
                    }
                    return pack;
                }
            }
            return null;
        }

    }

    private String getNSI_GUID(Long entityId, String alternativeGUID)
    {
        String nsiGUID = IFefuNsiSyncDAO.instance.get().getNsiIdGuid(entityId);
        return nsiGUID != null ? nsiGUID : alternativeGUID;
    }

    private String getDepartmentGUID(IEntity department)
    {
        return getDepartmentGUID(department, null, false);
    }

    private String getDepartmentGUID(IEntity department, Object extract, boolean oldDepartment)
    {
        /* костыль убран в рамках DEV-5015 п.3
        if (extract != null)
        {
            // Для некоторых типов приказов (пока только Ф2) нужны свои контстантные гуиды подразделений.
            // Например, когда отправляют в академ отпуск - это в 1С перевод в какое-то определенное подразделение, гуид которого у нас задан константой.
            // Аналогично выход из отпуска - переводят из этого подразделения в реальную группу.

            String orderTypeCode = ((IAbstractExtract) extract).getType().getCode();
            String newStatusCode;
            if (!oldDepartment)
            {
                switch (orderTypeCode)
                {
                    case "1.12": // О предоставлении отпуска по беременности и родам
                        // тут вроде нельзя выбрать новое состояние
                    case "1.16": // О предоставлении академического отпуска
                        // тут тоже
                        return ACADEMICAL_VACATION_GROUP_GUID;
                    case "1.59": // О предоставлении отпуска по уходу за ребенком до 1,5 лет
                        newStatusCode = ((WeekendChildOneAndHalfStuExtract) extract).getStudentStatusNew().getCode();
                        return UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITH_ATTENDANCE.equals(newStatusCode) ? FREE_ACADEMICAL_VACATION_GROUP_GUID : ACADEMICAL_VACATION_GROUP_GUID;
                    case "1.94": // О предоставлении отпуска по уходу за ребенком до 3 лет
                        newStatusCode = ((WeekendChildThreeStuExtract) extract).getStudentStatusNew().getCode();
                        return UniDefines.CATALOG_STUDENT_STATUS_CHILD_WITH_ATTENDANCE.equals(newStatusCode) ? FREE_ACADEMICAL_VACATION_GROUP_GUID : ACADEMICAL_VACATION_GROUP_GUID;
                }
            }
            else
            {
                switch (orderTypeCode)
                {
                    case "1.89":  // О выходе из отпуска по беременности и родам
                    case "1.9":   // О выходе из академического отпуска
                    case "1.90":  // О выходе из отпуска по уходу за ребенком
                        return ACADEMICAL_VACATION_GROUP_GUID;
                }
            }
        }
        */

        return department != null ? getNSI_GUID(department.getId(), NONE_GROUP_GUID) : NONE_GROUP_GUID;
    }

    private TableRecordType createTable(Object extract, String departmentGUID, String oldDepartmentGUID, Date studentDate, BigDecimal amount, String calcKind, String postGUID) throws InvalidDataException
    {
        Student student = null;
        Person person;
        if (extract instanceof AbstractStudentExtract)
        {
            student = ((AbstractStudentExtract) extract).getEntity();
            person = student.getPerson();
        }
        else if (extract instanceof EnrollmentExtract)
        {
            person = ((EnrollmentExtract) extract).getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson();
        }
        else
            throw new IllegalStateException();

        TableRecordType record = new TableRecordType();

        String studentGUID = getNSI_GUID(person.getId(), null);
        if (studentGUID == null)
            throw new InvalidDataException("GUID for person id " + person.getId() + " not found");

        record.setStudentID(studentGUID);
        record.setStudentNumber(student != null ? (student.getPersonalFileNumber() != null ? student.getPersonalFileNumber() : "") : "");
        record.setDepartmentOldID(oldDepartmentGUID);
        record.setDepartmentID(departmentGUID);
        record.setPostID(postGUID);
        record.setCalcKind(calcKind);
        record.setStudentDate(studentDate);
        record.setScholarshipAmount(amount != null ? amount : new BigDecimal(0));

        return record;
    }

    private static final QName _qname = new QName(Data.getTypeDesc().getXmlType().getLocalPart());
    private static final Attributes _attrs = new AttributesImpl();
    private static final Serializer _serializer = Data.getSerializer(null, Data.class, Data.getTypeDesc().getXmlType());
    private static final MessageContext _msgContext = new MessageContext(new AxisServer());

    private static byte[] toXML(Data data) throws IOException
    {
        try (StringWriter writer = new StringWriter())
        {
            _serializer.serialize(_qname, _attrs, data, new SerializationContext(writer, _msgContext));
            return writer.toString()
                    .replaceAll(" xmlns[:=].*?\".*?\"", "")
                    .replaceAll(" xsi:type=\".*?\"", "")
                    .getBytes();
        }
    }

    public String formatXML(String unformattedXml)
    {
        try
        {
            final Document document = parseXml(unformattedXml);

            OutputFormat format = new OutputFormat(document);
            format.setLineWidth(85);
            format.setIndenting(true);
            format.setIndent(2);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);

            return out.toString();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    private Document parseXml(String in)
    {
        try
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(in));
            return db.parse(is);
        }
        catch (ParserConfigurationException | SAXException | IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public class TestResultException extends RuntimeException
    {
        public byte[] data;

        private TestResultException(byte[] data)
        {
            this.data = data;
        }
    }

    @Override
    public void doTest(Collection<Long> extractIds) throws TestResultException
    {
        StringBuilder str = new StringBuilder();

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
        str.append("Log of synchronization \n\n\n");

        List<IAbstractExtract> entities = new ArrayList<>(extractIds.size());
        for (Long id : extractIds)
        {
            entities.add(get(id));
        }
        addNewItems(entities);


        Long maxPackId = new DQLSelectBuilder().fromEntity(Fefu_1C_pack.class, "p")
                .column(DQLFunctions.max(property("p.id")))
                .createStatement(getSession())
                .uniqueResult();

        try
        {
            doSynchronization(extractIds, true);
        }
        catch (Throwable e)
        {
            StringWriter writer = new StringWriter();
            try (PrintWriter out = new PrintWriter(writer))
            {
                e.printStackTrace(out);
                out.flush();
            }
            throw new TestResultException(writer.toString().getBytes());
        }

        DQLSelectBuilder packsInLog = new DQLSelectBuilder().fromEntity(Fefu_1C_log.class, "l")
                .column(property("l", Fefu_1C_log.pack().id()))
                .where(in(property("l", Fefu_1C_log.extract()), extractIds));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Fefu_1C_pack.class, "p", true)
                .column(property("p"))
                .where(in("p.id", packsInLog.buildQuery()));

        if (maxPackId != null)
            builder.where(gt(property("p.id"), value(maxPackId)));

        for (Fefu_1C_pack item : builder.createStatement(getSession()).<Fefu_1C_pack>list())
        {
            str.append("Moment: ").append(dateFormat.format(item.getDispatchDate())).append("\n");
            str.append("Request: ").append(formatXML(new String(item.getRequest()))).append("\n");
            str.append("Response: ").append(item.getResponse()).append("\n");
            str.append(item.isSuccess() ? "it's success" : "it's error").append("\n");

            str.append("---------------- \n");
        }

        // Откатываем транзакцию и прямо в эксепшен записываем результат
        throw new TestResultException(str.toString().getBytes());
    }
}