/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.parser;

import org.tandemframework.core.util.cache.SafeMap;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.node.*;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.node.planRow.ImtsaDiscipline;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.node.planRow.ImtsaVariant;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.nodeItem.NodeItem;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.nodeItem.NodeItemAdvanced;

import java.io.InputStream;
import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 03.09.2013
 */
public class SecondGosGenerationImtsaParser extends ImtsaParser
{
    protected SecondGosGenerationImtsaParser(Document document)
    {
        super(document);
    }

    protected SecondGosGenerationImtsaParser(InputStream inputStream)
    {
        super(inputStream);
    }

    @Override
    protected ImtsaCycles parseCycles()
    {
        setStage(ParseStage.CYCLES);
        Node cyclesNode;
        NodeItem cyclesNodeItem;
        if ((cyclesNode = getUniqueNodeByTitleAndParent(ImtsaTitle.getCyclesNodeName(), ImtsaFile.getTitleNodeName())) == null && !FEFU_IMTSA_IMPORT_SIMPLE_MODE)
        {
            return null;
        }

        Map<String, String> abbreviation2TitleMap = new HashMap<>();
        if (cyclesNode != null)
        {
            if ((cyclesNodeItem = getNodeItem(cyclesNode, Collections.singletonList(ImtsaCycles.CYCLES_NODE_VIEW))) == null && !FEFU_IMTSA_IMPORT_SIMPLE_MODE)
            {
                return null;
            }

            if (cyclesNodeItem != null)
            {
                for (NodeItem cycleNodeItem: cyclesNodeItem.getChildNodes())
                {
                    Map<String, Object> attributes = cycleNodeItem.getAttributes();
                    String cycleAbbreviation = (String) attributes.get(ImtsaCycles.getCycleAbbreviationAttribute());
                    String cycleTitle = (String) attributes.get(ImtsaCycles.getCycleTitleAttribute());
                    if ((FEFU_IMTSA_IMPORT_SIMPLE_MODE && cycleAbbreviation != null && cycleTitle != null) || !FEFU_IMTSA_IMPORT_SIMPLE_MODE)
                        abbreviation2TitleMap.put(cycleAbbreviation, cycleTitle);
                }
            }
        }

        ImtsaCycles result = new ImtsaCycles();
        result.setAbbreviation2TitleMap(abbreviation2TitleMap);

        return result;
    }

    @Override
    protected String getDisciplineIdAttribute()
    {
        return ImtsaDiscipline.getIdAttribute();
    }


    @Override
    protected String getDisciplineCycleAttribute()
    {
        return ImtsaDiscipline.getCycleAttribute();
    }

    @Override
    protected String getModifiedId(String id)
    {
        return id
                .replaceAll(ImtsaVariant.getVariantRegExpVariablePartFrom(), ImtsaVariant.getVariantRegExpVariablePartTo())
                .replaceAll(ImtsaVariant.getVariantRegExpVariantFrom(), ImtsaVariant.getVariantRegExpVariantTo());
    }


    @Override
    protected Map<String, Collection<ImtsaPractice>> parsePractices()
    {
        setStage(ParseStage.PRACTICES);
        Map<String, INodeViewAdvanced> nodeViewMap = new HashMap<>();
        nodeViewMap.put(ImtsaFile.getGos2TutoringPracticeGroupNodeName(), ImtsaPractice.GOS2_TUTORING_PRACTICE_GROUP_NODE_VIEW);
        nodeViewMap.put(ImtsaFile.getGos2OtherPracticeGroupNodeName(), ImtsaPractice.GOS2_OTHER_PRACTICE_GROUP_NODE_VIEW);

        Node practicesNode;
        NodeItemAdvanced practicesNodeItem;
        if ((practicesNode = getUniqueNodeByTitleAndParent(ImtsaFile.getGos2SpecialWorkKindsNodeName(), ImtsaFile.getPlanNodeName())) == null || (practicesNodeItem = getNodeItemAdvanced(practicesNode, nodeViewMap)) == null)
        {
            return null;
        }

        Map<String, Collection<ImtsaPractice>> result = new HashMap<>();
        result.put(ImtsaPractice.getGos2TutoringPracticeNodeName(), getPractices(practicesNodeItem.getChildMap().get(ImtsaFile.getGos2TutoringPracticeGroupNodeName()), ImtsaPractice.getGos2TutoringPracticeNodeName()));
        result.put(ImtsaPractice.getGos2OtherPracticeNodeName(), getPractices(practicesNodeItem.getChildMap().get(ImtsaFile.getGos2OtherPracticeGroupNodeName()), ImtsaPractice.getGos2OtherPracticeNodeName()));
        return result;
    }


    private Collection<ImtsaPractice> getPractices(List<NodeItemAdvanced> practiceGroups, String practiceNodeName)
    {
        if (practiceGroups == null)
        {
            return Collections.emptyList();
        }

        Map<String, ImtsaPractice> title2PracticeMap = SafeMap.get(ImtsaPractice::new);

        for (NodeItemAdvanced practiceGroupNodeItem: practiceGroups)
        {
            for (Map.Entry<String, List<NodeItemAdvanced>> practiceEntry: practiceGroupNodeItem.getChildMap().entrySet())
            {
                if (practiceEntry.getKey().equals(practiceNodeName))
                {
                    for (NodeItemAdvanced practiceNodeItem: practiceEntry.getValue())
                    {
                        Map<String, Object> attributes = practiceNodeItem.getAttributes();
                        if (attributes.get(ImtsaPractice.getGos2TermAttribute()) == null && attributes.get(ImtsaPractice.getGos2CourseAttribute()) != null)
                        {
                            registerError("Невозможно сохранить нагрузку практики на курс - пропущено.");
                        }

                        Double weeks = (Double) attributes.get(ImtsaPractice.getGos2WeeksAttribute());
                        if (weeks != null)
                        {
                            String title = (String) attributes.get(ImtsaPractice.getGos2KindAttribute());
                            ImtsaPractice practice = title2PracticeMap.get(title);
                            if (practice.getOwnerCode() == null)
                            {
                                practice.setOwnerCode((String) attributes.get(ImtsaPractice.getGos2ChairCodeAttribute()));
                            }

                            Integer termNumber = (Integer) attributes.get(ImtsaPractice.getGos2TermAttribute());
                            Double termWeeks = practice.getTerm2WeeksMap().get(termNumber);

                            practice.getTerm2WeeksMap().put(termNumber, weeks + (termWeeks == null ? 0.0d : termWeeks));
                        }
                    }
                }
            }
        }

        return title2PracticeMap.values();
    }
}