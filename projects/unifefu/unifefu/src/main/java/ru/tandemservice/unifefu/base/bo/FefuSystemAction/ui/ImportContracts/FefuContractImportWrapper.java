/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.ImportContracts;

import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;

import java.util.Date;

/**
 * @author nvankov
 * @since 4/29/13
 */
public class FefuContractImportWrapper
{
    private Long _fakeId;
    private boolean _dublicate;
    private String _contractNumber;
    private String _externalOrganizationTitle;
    private String _externalOrganizationAddress;
    private Date _contractCreateDate;
    private Date _contractEndDate;
    private ExternalOrgUnit _externalOrgUnit;
    private LegalForm _legalForm;

    public Long getFakeId()
    {
        return _fakeId;
    }

    public void setFakeId(Long fakeId)
    {
        _fakeId = fakeId;
    }

    public boolean isDublicate()
    {
        return _dublicate;
    }

    public void setDublicate(boolean dublicate)
    {
        _dublicate = dublicate;
    }

    public String getContractNumber()
    {
        return _contractNumber;
    }

    public String getExternalOrganizationTitle()
    {
        return _externalOrganizationTitle;
    }

    public void setExternalOrganizationTitle(String externalOrganizationTitle)
    {
        _externalOrganizationTitle = externalOrganizationTitle;
    }

    public String getExternalOrganizationAddress()
    {
        return _externalOrganizationAddress;
    }

    public Date getContractCreateDate()
    {
        return _contractCreateDate;
    }

    public Date getContractEndDate()
    {
        return _contractEndDate;
    }

    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        _externalOrgUnit = externalOrgUnit;
    }

    public LegalForm getLegalForm()
    {
        return _legalForm;
    }

    public void setLegalForm(LegalForm legalForm)
    {
        _legalForm = legalForm;
    }

    public FefuContractImportWrapper(String contractNumber, String externalOrganizationTitle, String externalOrganizationAddress, Date contractCreateDate, Date contractEndDate)
    {
        _contractNumber = contractNumber.trim();
        _externalOrganizationTitle = externalOrganizationTitle.trim();
        _externalOrganizationAddress = externalOrganizationAddress.trim();
        _contractCreateDate = contractCreateDate;
        _contractEndDate = contractEndDate;
        _dublicate = false;
    }
}
