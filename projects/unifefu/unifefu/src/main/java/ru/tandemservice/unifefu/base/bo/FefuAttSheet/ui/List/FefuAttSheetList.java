/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.Pub.FefuAttSheetPub;
import ru.tandemservice.unifefu.entity.SessionAttSheetDocument;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 30.06.2014
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class FefuAttSheetList extends BusinessComponentManager
{
	public static final String ALERT_FORMATTER = "ui:currentItemAlert";
	public static final String PRINT_LISTENER = "onPrintEntityFromList";

	//data sources
	public static final String ATT_SHEET_DS = "attSheetDS";

	// поля фильтра
	public static final String FEFU_REQUEST_DATE_FROM = "formedFrom";
	public static final String FEFU_REQUEST_DATE_TO = "formedTo";
	public static final String FEFU_STUDENT_FIRST_NAME = "personFirstName";
	public static final String FEFU_STUDENT_LAST_NAME = "personLastName";
	public static final String FEFU_DOCUMENT_NUMBER = "docNumber";
	public static final String FEFU_FORMATIVE_ORGUNIT_ID = "formativeOrgUnit";


	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(searchListDS(ATT_SHEET_DS, getAttSheetDS(), attSheetDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint getAttSheetDS()
	{
		return columnListExtPointBuilder(ATT_SHEET_DS)
				.addColumn(publisherColumn("number", SessionAttSheetDocument.P_NUMBER)
						           .businessComponent(FefuAttSheetPub.class)
						           .parameters("ui:addEditFormParameters")
						           .order().permissionKey("viewFefuSessionAttSheetPub")
				)
				.addColumn(textColumn("createDate", SessionAttSheetDocument.formingDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
				.addColumn(textColumn("fio", SessionAttSheetDocument.targetStudent().person().fullFio()).order())
				.addColumn(actionColumn("print").icon("printer").listener(PRINT_LISTENER).displayHeader(false).permissionKey("printFefuSessionAttSheetPub"))
				.addColumn(actionColumn("edit").icon(ActionColumn.EDIT).listener(EDIT_LISTENER).displayHeader(false).permissionKey("editFefuSessionAttSheetPub"))
				.addColumn(actionColumn("delete").icon(ActionColumn.DELETE).listener(DELETE_LISTENER).alert(ALERT_FORMATTER).displayHeader(false).permissionKey("deleteFefuSessionAttSheetPub"))
				.create();
	}

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> attSheetDSHandler()
	{
		return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
		{
			@Override
			protected DSOutput execute(DSInput input, ExecutionContext context)
			{
				String alias = "pack";
				// receive data from filter
				Long orgUnitId = context.get(FEFU_FORMATIVE_ORGUNIT_ID);
				Date requestDateFrom = context.get(FEFU_REQUEST_DATE_FROM);
				Date requestDateTo = context.get(FEFU_REQUEST_DATE_TO);
				String studentFirstName = context.get(FEFU_STUDENT_FIRST_NAME);
				String studentLastName = context.get(FEFU_STUDENT_LAST_NAME);
				String documentNumber = context.get(FEFU_DOCUMENT_NUMBER);

				DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionAttSheetDocument.class, alias)
						.column(alias)
						.order(property(alias, (String) input.getEntityOrder().getKey()), input.getEntityOrder().getDirection())
						.where(eq(property(alias, SessionAttSheetDocument.orgUnit().id()), value(orgUnitId)));

				if (null != requestDateFrom)
				{
					requestDateFrom = CoreDateUtils.getDayFirstTimeMoment(requestDateFrom);
					builder.where(ge(property(alias, SessionAttSheetDocument.formingDate()), valueDate(requestDateFrom)));
				}

				if (null != requestDateTo)
				{
					requestDateTo = CoreDateUtils.getNextDayFirstTimeMoment(requestDateTo, 1);
					builder.where(lt(property(alias, SessionAttSheetDocument.formingDate()), valueDate(requestDateTo)));
				}

				if (null != documentNumber)
				{
					builder.where(likeUpper(property(alias, SessionAttSheetDocument.number()), value(CoreStringUtils.escapeLike(documentNumber))));
				}

				if (null != studentFirstName)
				{
					builder.where(likeUpper(property(alias, SessionAttSheetDocument.targetStudent().person().identityCard().firstName()), value(CoreStringUtils.escapeLike(studentFirstName))));
				}

				if (null != studentLastName)
				{
					builder.where(likeUpper(property(alias, SessionAttSheetDocument.targetStudent().person().identityCard().lastName()), value(CoreStringUtils.escapeLike(studentLastName))));
				}

				return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
			}
		};
	}
}
