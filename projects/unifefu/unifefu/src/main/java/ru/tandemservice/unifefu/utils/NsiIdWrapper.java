/* $Id$ */
package ru.tandemservice.unifefu.utils;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;

/**
 * @author Dmitry Seleznev
 * @since 27.03.2014
 */
public class NsiIdWrapper
{
    private Long _entityId;
    private String _guidsListStr;
    private String _syncDatesListStr;

    public NsiIdWrapper(Long entityId)
    {
        _entityId = entityId;
        if (null != _entityId)
        {
            StringBuilder guidsBuilder = new StringBuilder();
            StringBuilder datesBuilder = new StringBuilder();

            for (FefuNsiIds ids : DataAccessServices.dao().getList(FefuNsiIds.class, FefuNsiIds.entityId(), _entityId))
            {
                guidsBuilder.append(guidsBuilder.length() > 0 ? ", " : "").append(ids.getGuid());
                datesBuilder.append(datesBuilder.length() > 0 ? ", " : "").append(DateFormatter.DATE_FORMATTER_WITH_TIME.format(ids.getSyncTime()));
            }

            _guidsListStr = guidsBuilder.toString();
            _syncDatesListStr = datesBuilder.toString();
        }
    }

    public boolean isVisible()
    {
        return UserContext.getInstance().getPrincipalContext().isAdmin();
    }

    public String getComplexGuidStr()
    {
        return getGuidsListStr() + (null != getSyncDatesListStr() && getSyncDatesListStr().length() > 0 ? "  (" + getSyncDatesListStr() + ")" : "");
    }

    public Long getEntityId()
    {
        return _entityId;
    }

    public void setEntityId(Long entityId)
    {
        _entityId = entityId;
    }

    public String getGuidsListStr()
    {
        return _guidsListStr;
    }

    public void setGuidsListStr(String guidsListStr)
    {
        _guidsListStr = guidsListStr;
    }

    public String getSyncDatesListStr()
    {
        return _syncDatesListStr;
    }

    public void setSyncDatesListStr(String syncDatesListStr)
    {
        _syncDatesListStr = syncDatesListStr;
    }
}