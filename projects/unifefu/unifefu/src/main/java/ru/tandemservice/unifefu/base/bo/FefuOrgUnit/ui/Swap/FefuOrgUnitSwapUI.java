/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.Swap;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuOrgUnit.FefuOrgUnitManager;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 28.03.2014
 */
@Input({@Bind(key = FefuOrgUnitSwapUI.ORG_UNIT_ID, binding = "orgUnitId", required = true)})
public class FefuOrgUnitSwapUI extends UIPresenter
{
    public static final String ORG_UNIT_ID = "orgUnitId";

    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private OrgUnit _orgUnitToSwap;
    private FefuNsiIds _orgUnitNsiId;
    private FefuNsiIds _orgUnitToSwapNsiId;
    private String _statistics;

    @Override
    public void onComponentRefresh()
    {
        _orgUnit = DataAccessServices.dao().getNotNull(OrgUnit.class, _orgUnitId);
        _orgUnitNsiId = IFefuNsiSyncDAO.instance.get().getNsiId(_orgUnitId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuOrgUnitSwap.ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put(FefuOrgUnitSwap.ORG_UNIT_ID, _orgUnitId);
            dataSource.put(FefuOrgUnitSwap.ORG_UNIT_TYPE_ID, _orgUnit.getOrgUnitType().getId());
        }
    }

    public void onChangeOrgUnitToSwap()
    {
        if (null != _orgUnitToSwap)
        {
            List<FefuNsiIds> nsiIds = DataAccessServices.dao().getList(FefuNsiIds.class, FefuNsiIds.entityId(), _orgUnitToSwap.getId());
            if (!nsiIds.isEmpty()) _orgUnitToSwapNsiId = nsiIds.get(0);
            else _orgUnitToSwapNsiId = null;

            int employeeCnt = DataAccessServices.dao().getCount(EmployeePost.class, EmployeePost.orgUnit().id().s(), _orgUnitToSwap.getId());
            int studentFormCnt = DataAccessServices.dao().getCount(Student.class, Student.educationOrgUnit().formativeOrgUnit().id().s(), _orgUnitToSwap.getId());
            int studentTerrCnt = DataAccessServices.dao().getCount(Student.class, Student.educationOrgUnit().territorialOrgUnit().id().s(), _orgUnitToSwap.getId());
            int studentOperCnt = DataAccessServices.dao().getCount(Student.class, Student.educationOrgUnit().operationOrgUnit().id().s(), _orgUnitToSwap.getId());
            int studentGrpCnt = DataAccessServices.dao().getCount(Student.class, Student.educationOrgUnit().groupOrgUnit().id().s(), _orgUnitToSwap.getId());
            int eduOUFormCnt = DataAccessServices.dao().getCount(EducationOrgUnit.class, EducationOrgUnit.formativeOrgUnit().id().s(), _orgUnitToSwap.getId());
            int eduOUTerrCnt = DataAccessServices.dao().getCount(EducationOrgUnit.class, EducationOrgUnit.territorialOrgUnit().id().s(), _orgUnitToSwap.getId());
            int eduOUOperCnt = DataAccessServices.dao().getCount(EducationOrgUnit.class, EducationOrgUnit.operationOrgUnit().id().s(), _orgUnitToSwap.getId());
            int eduOUGrpCnt = DataAccessServices.dao().getCount(EducationOrgUnit.class, EducationOrgUnit.groupOrgUnit().id().s(), _orgUnitToSwap.getId());

            _statistics = "Количество сотрудников: " + employeeCnt + "\n";
            _statistics += "Количество студентов:   " + studentFormCnt + ", " + studentTerrCnt + ", " + studentOperCnt + ", " + studentGrpCnt + " (форм, терр, диспетчерская, деканат)\n";
            _statistics += "Количество напр. подг.: " + eduOUFormCnt + ", " + eduOUTerrCnt + ", " + eduOUOperCnt + ", " + eduOUGrpCnt + " (форм, терр, диспетчерская, деканат)\n";
            _statistics += "При нажатии \"Сохранить\" для текущего подразделения будет изменен GUID (будет взят у подразделения-донорра), GUID подразделения-донора (добавится символ \"_\" в конце), а так же будут переведены все сотрудники подразделения-донора на текущее.";
        }
    }

    public void onClickApply()
    {
        FefuOrgUnitManager.instance().dao().doSwapOrgUnits(_orgUnitId, _orgUnitToSwap.getId());
        deactivate();
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public OrgUnit getOrgUnitToSwap()
    {
        return _orgUnitToSwap;
    }

    public void setOrgUnitToSwap(OrgUnit orgUnitToSwap)
    {
        _orgUnitToSwap = orgUnitToSwap;
    }

    public FefuNsiIds getOrgUnitNsiId()
    {
        return _orgUnitNsiId;
    }

    public void setOrgUnitNsiId(FefuNsiIds orgUnitNsiId)
    {
        _orgUnitNsiId = orgUnitNsiId;
    }

    public FefuNsiIds getOrgUnitToSwapNsiId()
    {
        return _orgUnitToSwapNsiId;
    }

    public void setOrgUnitToSwapNsiId(FefuNsiIds orgUnitToSwapNsiId)
    {
        _orgUnitToSwapNsiId = orgUnitToSwapNsiId;
    }

    public String getStatistics()
    {
        return _statistics;
    }

    public void setStatistics(String statistics)
    {
        _statistics = statistics;
    }
}
