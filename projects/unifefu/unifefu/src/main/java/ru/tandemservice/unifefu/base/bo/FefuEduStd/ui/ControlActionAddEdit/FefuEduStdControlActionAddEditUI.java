/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ControlActionAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel;

/**
 * @author Alexey Lopatin
 * @since 19.01.2015
 */
@Input({
               @Bind(key = FefuEduStdControlActionAddEditUI.EDU_STANDARD_ID, binding = "eduStandard.id", required = true),
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "controlAction2EduStandardRel.id")
       })
public class FefuEduStdControlActionAddEditUI extends UIPresenter
{
    public static final String EDU_STANDARD_ID = "eduStandardId";
    public static final String REL_ID = "controlAction2EduStandardRelId";

    private EppStateEduStandard _eduStandard = new EppStateEduStandard();
    private FefuFControlActionType2EppStateEduStandardRel _controlAction2EduStandardRel = new FefuFControlActionType2EppStateEduStandardRel();

    @Override
    public void onComponentRefresh()
    {
        _eduStandard = DataAccessServices.dao().getNotNull(_eduStandard.getId());
        if (null != _controlAction2EduStandardRel.getId())
        {
            _controlAction2EduStandardRel = DataAccessServices.dao().getNotNull(_controlAction2EduStandardRel.getId());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuEduStdControlActionAddEdit.CONTROL_ACTION_DS.equals(dataSource.getName()))
        {
            dataSource.put(EDU_STANDARD_ID, _eduStandard.getId());
            dataSource.put(REL_ID, _controlAction2EduStandardRel.getId());
        }
    }

    public void onClickApply()
    {
        _controlAction2EduStandardRel.setEduStandard(_eduStandard);
        DataAccessServices.dao().saveOrUpdate(_controlAction2EduStandardRel);
        deactivate();
    }

    public EppStateEduStandard getEduStandard()
    {
        return _eduStandard;
    }

    public void setEduStandard(EppStateEduStandard eduStandard)
    {
        _eduStandard = eduStandard;
    }

    public FefuFControlActionType2EppStateEduStandardRel getControlAction2EduStandardRel()
    {
        return _controlAction2EduStandardRel;
    }

    public void setControlAction2EduStandardRel(FefuFControlActionType2EppStateEduStandardRel controlAction2EduStandardRel)
    {
        _controlAction2EduStandardRel = controlAction2EduStandardRel;
    }
}
