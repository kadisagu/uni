/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.CompetenceAdd;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.FefuEduStdManager;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 01.12.2014
 */
@Input({
                @Bind(key = UIPresenter.PUBLISHER_ID, binding = "eduStandard.id", required = true),
                @Bind(key = FefuEduStdCompetenceAddUI.SPECIALIZATION_ID, binding = "programSpecializationId")
       })
public class FefuEduStdCompetenceAddUI extends UIPresenter
{
    public static final String EDU_STANDARD_ID = "eduStandardId";
    public static final String SPECIALIZATION_ID = "specializationId";
    public static final String EPP_SKILL_GROUP = "eppSkillGroup";

    private EppStateEduStandard _eduStandard = new EppStateEduStandard();
    private EduProgramSpecialization _programSpecialization;
    private EppSkillGroup _eppSkillGroup;
    private List<FefuCompetence> _fefuCompetenceList = Lists.newArrayList();
    private Long _programSpecializationId;

    @Override
    public void onComponentRefresh()
    {
        _eduStandard = DataAccessServices.dao().getNotNull(_eduStandard.getId());
        if (null != _programSpecializationId)
        {
            _programSpecialization = DataAccessServices.dao().getNotNull(_programSpecializationId);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuEduStdCompetenceAdd.FEFU_COMPETENCE_DS.equals(dataSource.getName()))
        {
            dataSource.put(EDU_STANDARD_ID, _eduStandard.getId());
            dataSource.put(EPP_SKILL_GROUP, _eppSkillGroup);
        }
    }

    public void onClickApply()
    {
        FefuEduStdManager.instance().dao().saveFefuCompetence(_eduStandard, _eppSkillGroup.getId(), _fefuCompetenceList);
        deactivate();
    }

    public EppStateEduStandard getEduStandard()
    {
        return _eduStandard;
    }

    public void setEduStandard(EppStateEduStandard eduStandard)
    {
        _eduStandard = eduStandard;
    }

    public EppSkillGroup getEppSkillGroup()
    {
        return _eppSkillGroup;
    }

    public void setEppSkillGroup(EppSkillGroup eppSkillGroup)
    {
        _eppSkillGroup = eppSkillGroup;
    }

    public List<FefuCompetence> getFefuCompetenceList()
    {
        return _fefuCompetenceList;
    }

    public void setFefuCompetenceList(List<FefuCompetence> fefuCompetenceList)
    {
        _fefuCompetenceList = fefuCompetenceList;
    }

    public EduProgramSpecialization getProgramSpecialization()
    {
        return _programSpecialization;
    }

    public void setProgramSpecialization(EduProgramSpecialization programSpecialization)
    {
        _programSpecialization = programSpecialization;
    }

    public Long getProgramSpecializationId()
    {
        return _programSpecializationId;
    }

    public void setProgramSpecializationId(Long programSpecializationId)
    {
        _programSpecializationId = programSpecializationId;
    }
}
