/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu17.ParagraphPub;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.01.2015
 */
public class DAO extends AbstractListParagraphPubDAO<FefuTransfStuDPOListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        String printFormName = getProperty(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.P_FILE_NAME, FefuOrderToPrintFormRelation.L_ORDER, model.getParagraph().getOrder());
        model.setPrintFormFileName(printFormName);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        super.prepareListDataSource(model);
        for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            viewWrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, true);
        }

    }
}