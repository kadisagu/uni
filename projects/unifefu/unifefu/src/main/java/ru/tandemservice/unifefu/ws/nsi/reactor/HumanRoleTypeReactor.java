/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRole;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRole;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanRoleType;

/**
 * @author Dmitry Seleznev
 * @since 12.02.2015
 */
public class HumanRoleTypeReactor extends SimpleCatalogEntityNewReactor<HumanRoleType, FefuNsiHumanRole>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return false;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    protected boolean isShouldBeCheckedForDelete()
    {
        return false;
    }

    @Override
    public Class<FefuNsiHumanRole> getEntityClass()
    {
        return FefuNsiHumanRole.class;
    }

    @Override
    public Class<HumanRoleType> getNSIEntityClass()
    {
        return HumanRoleType.class;
    }

    @Override
    public HumanRoleType getCatalogElementRetrieveRequestObject(String guid)
    {
        // TODO: Имеет смысл переопределить метод получения полного списка ученых степеней физ лица из НСИ, поскольку их может быть очень много.
        HumanRoleType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanRoleType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public HumanRoleType createEntity(CoreCollectionUtils.Pair<FefuNsiHumanRole, FefuNsiIds> entityPair)
    {
        return null; // Мы не можем назначать роли в НСИ
    }

    @Override
    public HumanRoleType updateNsiEntityFields(HumanRoleType nsiEntity, FefuNsiHumanRole entity)
    {
        return null; // Мы не можем изменять набор ролей в НСИ
    }

    @Override
    public FefuNsiHumanRole createEntity(HumanRoleType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getHumanID() || null == nsiEntity.getRoleID()) return null;

        FefuNsiHumanRole entity = new FefuNsiHumanRole();

        if (null != nsiEntity.getHumanID() && null != nsiEntity.getHumanID().getHuman())
        {
            CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonPair(nsiEntity.getHumanID().getHuman().getID());
            if (null != personPair && null != personPair.getX()) entity.setPerson(personPair.getX());
        }

        if (null != nsiEntity.getRoleID() && null != nsiEntity.getRoleID().getRole())
        {
            CoreCollectionUtils.Pair<FefuNsiRole, FefuNsiIds> rolePair = NsiObjectsHolder.getRolePair(nsiEntity.getRoleID().getRole().getID());
            if (null != rolePair && null != rolePair.getX()) entity.setRole(rolePair.getX());
        }

        if (null == entity.getPerson() || null == entity.getRole()) return null;

        return entity;
    }

    @Override
    public CoreCollectionUtils.Pair<FefuNsiHumanRole, FefuNsiIds> getPossibleDuplicatePair(HumanRoleType nsiEntity)
    {
        return _idToEntityMap.get(nsiEntity.getID());
    }

    @Override
    public FefuNsiHumanRole updatePossibleDuplicateFields(HumanRoleType nsiEntity, CoreCollectionUtils.Pair<FefuNsiHumanRole, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getHumanID() || null == nsiEntity.getRoleID()) return null;

        if (null != entityPair)
        {
            if (null != nsiEntity.getHumanID() && null != nsiEntity.getHumanID().getHuman())
            {
                CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonPair(nsiEntity.getHumanID().getHuman().getID());
                if (null != personPair && null != personPair.getX()) entityPair.getX().setPerson(personPair.getX());
            }

            if (null != nsiEntity.getRoleID() && null != nsiEntity.getRoleID().getRole())
            {
                CoreCollectionUtils.Pair<FefuNsiRole, FefuNsiIds> rolePair = NsiObjectsHolder.getRolePair(nsiEntity.getRoleID().getRole().getID());
                if (null != rolePair && null != rolePair.getX()) entityPair.getX().setRole(rolePair.getX());
            }
        }

        return entityPair.getX();
    }
}