/* $Id$ */
package ru.tandemservice.unifefu.eduplan.ext.EppEduPlanVersion;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Dmitry Seleznev
 * @since 10.12.2014
 */
@Configuration
public class EppEduPlanVersionExtManager extends BusinessObjectExtensionManager
{
}