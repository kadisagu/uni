/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu26.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.unifefu.entity.FefuPerformConditionTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuPerformConditionTransferCourseStuExtract>
{
}
