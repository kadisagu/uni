/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu22.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuPerformConditionCourseTransferListExtract;

import static org.tandemframework.hibsupport.builder.expression.MQExpression.eq;

/**
 * @author Igor Belanov
 * @since 30.06.2016
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuPerformConditionCourseTransferListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setCourseAndGroupDisabled(!model.isParagraphOnlyOneInTheOrder());

        FefuPerformConditionCourseTransferListExtract firstExtract = model.getFirstExtract();
        if (firstExtract != null)
        {
            model.setGroup(firstExtract.getEntity().getGroup());
            model.setCourse(firstExtract.getEntity().getGroup().getCourse());
        }

        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourse()));
        builder.add(eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected FefuPerformConditionCourseTransferListExtract createNewInstance(Model model)
    {
        return new FefuPerformConditionCourseTransferListExtract();
    }

    @Override
    protected void fillExtract(FefuPerformConditionCourseTransferListExtract extract, Student student, Model model)
    {
    }
}
