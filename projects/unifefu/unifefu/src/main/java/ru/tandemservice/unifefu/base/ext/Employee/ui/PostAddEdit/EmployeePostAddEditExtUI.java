/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Employee.ui.PostAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEditUI;
import ru.tandemservice.unifefu.entity.ws.FefuEmployeePostExt;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 09.07.2014
 */
public class EmployeePostAddEditExtUI extends UIAddon
{
    private FefuEmployeePostExt _extension;
    private Double _rate;

    public EmployeePostAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        FefuEmployeePostExt ext = DataAccessServices.dao().getByNaturalId(new FefuEmployeePostExt.NaturalId(((EmployeePostAddEditUI) getPresenter()).getEmployeePost()));
        setExtension(ext);

        if (null != getExtension() && null != getExtension().getRate())
        {
            setRate(getExtension().getRate() / 10000.0);
        }
    }

    public FefuEmployeePostExt getExtension()
    {
        return _extension;
    }

    public void setExtension(FefuEmployeePostExt extension)
    {
        _extension = extension;
    }

    public Double getRate()
    {
        return _rate;
    }

    public void setRate(Double rate)
    {
        _rate = rate;
    }
}