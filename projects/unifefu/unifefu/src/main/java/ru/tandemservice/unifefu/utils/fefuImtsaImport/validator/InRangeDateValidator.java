/* $Id$ */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.validator;

import org.tandemframework.core.view.formatter.DateFormatter;

import java.util.Date;

/**
 * @author azhebko
 * @since 22.12.2014
 */
public class InRangeDateValidator extends Validator<Date>
{
    private final Date _from;
    private final Date _to;

    public InRangeDateValidator(Date from, Date to)
    {
        if (from == null && to == null)
            throw new IllegalStateException(); // требуется задать хотя бы одну дату
        _from = from;
        _to = to;
    }

    @Override
    public String getInvalidMessage()
    {
        DateFormatter formatter = DateFormatter.DEFAULT_DATE_FORMATTER;
        if (_from != null && _to != null)
            return "Значение выходит за диапазон " + formatter.format(_from) + " - " + formatter.format(_to) + ".";

        if (_from != null)
            return "Значение меньше " + formatter.format(_from) + ".";

        return "Значение больше " + formatter.format(_to) + ".";
    }

    @Override
    protected boolean validateValue(Date value)
    {
        return  (_from == null || !_from.after(value)) && (_to == null || !_to.before(value));
    }
}