
package ru.tandemservice.unifefu.ws.nsi.server;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceResponseType2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceResponseType2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="callCC" type="{http://www.croc.ru/Schemas/Dvfu/Nsi/Service}callCCType"/>
 *         &lt;element name="callRC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceResponseType2", propOrder = {
    "callCC",
    "callRC"
})
public class ServiceResponseType2 {

    @XmlElement(required = true)
    protected BigInteger callCC;
    protected String callRC;

    /**
     * Gets the value of the callCC property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCallCC() {
        return callCC;
    }

    /**
     * Sets the value of the callCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCallCC(BigInteger value) {
        this.callCC = value;
    }

    /**
     * Gets the value of the callRC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallRC() {
        return callRC;
    }

    /**
     * Sets the value of the callRC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallRC(String value) {
        this.callRC = value;
    }

}
