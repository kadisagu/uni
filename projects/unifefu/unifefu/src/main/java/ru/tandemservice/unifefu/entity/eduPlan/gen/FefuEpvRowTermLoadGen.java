package ru.tandemservice.unifefu.entity.eduPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.unifefu.entity.catalog.FefuLoadType;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEpvRowTermLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Нагрузка строки УПв в семестре (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEpvRowTermLoadGen extends EntityBase
 implements INaturalIdentifiable<FefuEpvRowTermLoadGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.eduPlan.FefuEpvRowTermLoad";
    public static final String ENTITY_NAME = "fefuEpvRowTermLoad";
    public static final int VERSION_HASH = 1141015534;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW_TERM = "rowTerm";
    public static final String L_LOAD_TYPE = "loadType";
    public static final String P_LOAD = "load";
    public static final String P_LOAD_AS_DOUBLE = "loadAsDouble";

    private EppEpvRowTerm _rowTerm;     // Семестр строки УПв
    private FefuLoadType _loadType;     // Вид нагрузки
    private long _load;     // Число часов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Семестр строки УПв. Свойство не может быть null.
     */
    @NotNull
    public EppEpvRowTerm getRowTerm()
    {
        return _rowTerm;
    }

    /**
     * @param rowTerm Семестр строки УПв. Свойство не может быть null.
     */
    public void setRowTerm(EppEpvRowTerm rowTerm)
    {
        dirty(_rowTerm, rowTerm);
        _rowTerm = rowTerm;
    }

    /**
     * @return Вид нагрузки. Свойство не может быть null.
     */
    @NotNull
    public FefuLoadType getLoadType()
    {
        return _loadType;
    }

    /**
     * @param loadType Вид нагрузки. Свойство не может быть null.
     */
    public void setLoadType(FefuLoadType loadType)
    {
        dirty(_loadType, loadType);
        _loadType = loadType;
    }

    /**
     * @return Число часов. Свойство не может быть null.
     */
    @NotNull
    public long getLoad()
    {
        return _load;
    }

    /**
     * @param load Число часов. Свойство не может быть null.
     */
    public void setLoad(long load)
    {
        dirty(_load, load);
        _load = load;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEpvRowTermLoadGen)
        {
            if (withNaturalIdProperties)
            {
                setRowTerm(((FefuEpvRowTermLoad)another).getRowTerm());
                setLoadType(((FefuEpvRowTermLoad)another).getLoadType());
            }
            setLoad(((FefuEpvRowTermLoad)another).getLoad());
        }
    }

    public INaturalId<FefuEpvRowTermLoadGen> getNaturalId()
    {
        return new NaturalId(getRowTerm(), getLoadType());
    }

    public static class NaturalId extends NaturalIdBase<FefuEpvRowTermLoadGen>
    {
        private static final String PROXY_NAME = "FefuEpvRowTermLoadNaturalProxy";

        private Long _rowTerm;
        private Long _loadType;

        public NaturalId()
        {}

        public NaturalId(EppEpvRowTerm rowTerm, FefuLoadType loadType)
        {
            _rowTerm = ((IEntity) rowTerm).getId();
            _loadType = ((IEntity) loadType).getId();
        }

        public Long getRowTerm()
        {
            return _rowTerm;
        }

        public void setRowTerm(Long rowTerm)
        {
            _rowTerm = rowTerm;
        }

        public Long getLoadType()
        {
            return _loadType;
        }

        public void setLoadType(Long loadType)
        {
            _loadType = loadType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuEpvRowTermLoadGen.NaturalId) ) return false;

            FefuEpvRowTermLoadGen.NaturalId that = (NaturalId) o;

            if( !equals(getRowTerm(), that.getRowTerm()) ) return false;
            if( !equals(getLoadType(), that.getLoadType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRowTerm());
            result = hashCode(result, getLoadType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRowTerm());
            sb.append("/");
            sb.append(getLoadType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEpvRowTermLoadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEpvRowTermLoad.class;
        }

        public T newInstance()
        {
            return (T) new FefuEpvRowTermLoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "rowTerm":
                    return obj.getRowTerm();
                case "loadType":
                    return obj.getLoadType();
                case "load":
                    return obj.getLoad();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "rowTerm":
                    obj.setRowTerm((EppEpvRowTerm) value);
                    return;
                case "loadType":
                    obj.setLoadType((FefuLoadType) value);
                    return;
                case "load":
                    obj.setLoad((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "rowTerm":
                        return true;
                case "loadType":
                        return true;
                case "load":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "rowTerm":
                    return true;
                case "loadType":
                    return true;
                case "load":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "rowTerm":
                    return EppEpvRowTerm.class;
                case "loadType":
                    return FefuLoadType.class;
                case "load":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEpvRowTermLoad> _dslPath = new Path<FefuEpvRowTermLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEpvRowTermLoad");
    }
            

    /**
     * @return Семестр строки УПв. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEpvRowTermLoad#getRowTerm()
     */
    public static EppEpvRowTerm.Path<EppEpvRowTerm> rowTerm()
    {
        return _dslPath.rowTerm();
    }

    /**
     * @return Вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEpvRowTermLoad#getLoadType()
     */
    public static FefuLoadType.Path<FefuLoadType> loadType()
    {
        return _dslPath.loadType();
    }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEpvRowTermLoad#getLoad()
     */
    public static PropertyPath<Long> load()
    {
        return _dslPath.load();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEpvRowTermLoad#getLoadAsDouble()
     */
    public static SupportedPropertyPath<Double> loadAsDouble()
    {
        return _dslPath.loadAsDouble();
    }

    public static class Path<E extends FefuEpvRowTermLoad> extends EntityPath<E>
    {
        private EppEpvRowTerm.Path<EppEpvRowTerm> _rowTerm;
        private FefuLoadType.Path<FefuLoadType> _loadType;
        private PropertyPath<Long> _load;
        private SupportedPropertyPath<Double> _loadAsDouble;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Семестр строки УПв. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEpvRowTermLoad#getRowTerm()
     */
        public EppEpvRowTerm.Path<EppEpvRowTerm> rowTerm()
        {
            if(_rowTerm == null )
                _rowTerm = new EppEpvRowTerm.Path<EppEpvRowTerm>(L_ROW_TERM, this);
            return _rowTerm;
        }

    /**
     * @return Вид нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEpvRowTermLoad#getLoadType()
     */
        public FefuLoadType.Path<FefuLoadType> loadType()
        {
            if(_loadType == null )
                _loadType = new FefuLoadType.Path<FefuLoadType>(L_LOAD_TYPE, this);
            return _loadType;
        }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEpvRowTermLoad#getLoad()
     */
        public PropertyPath<Long> load()
        {
            if(_load == null )
                _load = new PropertyPath<Long>(FefuEpvRowTermLoadGen.P_LOAD, this);
            return _load;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuEpvRowTermLoad#getLoadAsDouble()
     */
        public SupportedPropertyPath<Double> loadAsDouble()
        {
            if(_loadAsDouble == null )
                _loadAsDouble = new SupportedPropertyPath<Double>(FefuEpvRowTermLoadGen.P_LOAD_AS_DOUBLE, this);
            return _loadAsDouble;
        }

        public Class getEntityClass()
        {
            return FefuEpvRowTermLoad.class;
        }

        public String getEntityName()
        {
            return "fefuEpvRowTermLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getLoadAsDouble();
}
