/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.orgunit.EppEpvAcceptionTab;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.collections15.Transformer;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.unifefu.dao.eppEduPlan.IImtsaImportDAO;

import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 04.10.2013
 */
public class Controller extends ru.tandemservice.uniepp.component.orgunit.EppEpvAcceptionTab.Controller
{
    public void onClickCreateRegistryElements(IBusinessComponent component)
    {
        if (!"ok".equals(component.getClientParameter()))
        {
            String confirmStr = "Данное действие создает и привязывает для всех отобранных фильтрами строк учебного плана дисциплины, если таковых не было. Вы уверены?";
            ConfirmInfo confirm = new ConfirmInfo(confirmStr,
                    new ClickButtonAction("createRegistryElements", "ok", false));
            TapSupportUtils.displayConfirm(confirm);
            return;
        }

        Model model = (Model) getModel(component);
/*
        Transformer<Model.RowWrapper, EppEpvRegistryRow> rowWrapperToRowTransformer = new Transformer<Model.RowWrapper, EppEpvRegistryRow>(){ @Override public EppEpvRegistryRow transform(Model.RowWrapper o){ return o.getRegistryRow(); }};
        Predicate<EppEpvRegistryRow> hasNoRegistryElementPredicate = new Predicate<EppEpvRegistryRow>(){ @Override public boolean evaluate(EppEpvRegistryRow row){ return row.getRegistryElement() == null; }};
        Transformer<EppEpvRegistryRow, Long> rowToIdTransformer = new Transformer<EppEpvRegistryRow, Long>(){ @Override public Long transform(EppEpvRegistryRow o){ return o.getId(); }};
        Collection<Long> registryRowIds = CollectionUtils.collect(CollectionUtils.select(CollectionUtils.collect(model.getData().getRows(), rowWrapperToRowTransformer), hasNoRegistryElementPredicate), rowToIdTransformer);
*/

        IDAO dao = (IDAO) getDao();
        List<Long> registryRowIds = dao.getRegistryRowIds(model.getOrgUnitId(), model.getSettings());
        IImtsaImportDAO.instance.get().createRegistryElements(registryRowIds, true);
        component.refresh();
    }

    private static final Map<String, List<String>> DATE_FIELD_MAP = new HashMap<>();
    static
    {
        DATE_FIELD_MAP.put(Model.UMU_ID, Arrays.asList("checkedByUMUDateFrom", "checkedByUMUDateTo"));
        DATE_FIELD_MAP.put(Model.CRK_ID, Arrays.asList("checkedByCRKDateFrom", "checkedByCRKDateTo"));
        DATE_FIELD_MAP.put(Model.OOP_ID, Arrays.asList("checkedByOOPDateFrom", "checkedByOOPDateTo"));
    }

    public void onClickCheckedBy(IBusinessComponent component)
    {
        IDataSettings dataSettings = getModel(component).getSettings();
        String id = component.getListenerParameter();
        if (dataSettings.get(id) == null)
        {
            for (String dateField: DATE_FIELD_MAP.get(id))
            {
                dataSettings.set(dateField, null);
            }
        }
    }

    public void onClickCheckedDate(IBusinessComponent component)
    {
        IDataSettings dataSettings = getModel(component).getSettings();
        String id = component.getListenerParameter();
        for (String dateField: DATE_FIELD_MAP.get(id))
        {
            if (dataSettings.get(dateField) != null)
            {
                dataSettings.set(id, Model.YES);
                break;
            }
        }
    }
}
