/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleSession.ui.OrgUnitPrintFormList;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.OrgUnitPrintFormList.SppScheduleSessionOrgUnitPrintFormList;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.OrgUnitPrintFormList.SppScheduleSessionOrgUnitPrintFormListUI;

/**
 * @author Igor Belanov
 * @since 05.09.2016
 */
public class SppScheduleSessionOrgUnitPrintFormListExtUI extends UIAddon
{
    public SppScheduleSessionOrgUnitPrintFormListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleSessionOrgUnitPrintFormList.PRINT_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(getParentPresenter().getSettings().getAsMap(true, "admin"));
        }
    }

    private SppScheduleSessionOrgUnitPrintFormListUI getParentPresenter()
    {
        return getPresenter();
    }
}
