package ru.tandemservice.unifefu.entity.blackboard.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.unifefu.entity.blackboard.BbCourseStudent;
import ru.tandemservice.unifefu.entity.blackboard.BbGroup;
import ru.tandemservice.unifefu.entity.blackboard.BbGroupMembership;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Участник группы Blackboard
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class BbGroupMembershipGen extends EntityBase
 implements INaturalIdentifiable<BbGroupMembershipGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.blackboard.BbGroupMembership";
    public static final String ENTITY_NAME = "bbGroupMembership";
    public static final int VERSION_HASH = -1148406511;
    private static IEntityMeta ENTITY_META;

    public static final String L_BB_GROUP = "bbGroup";
    public static final String L_BB_COURSE_STUDENT = "bbCourseStudent";
    public static final String P_BB_PRIMARY_ID = "bbPrimaryId";
    public static final String L_GROUP_ROW = "groupRow";

    private BbGroup _bbGroup;     // Группа студентов ЭУК
    private BbCourseStudent _bbCourseStudent;     // Студент на курсе
    private String _bbPrimaryId;     // Идентификатор участника группы в Blackboard
    private EppRealEduGroup4LoadTypeRow _groupRow;     // Запись студента в УГС (по виду аудиторной нагрузки)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа студентов ЭУК. Свойство не может быть null.
     */
    @NotNull
    public BbGroup getBbGroup()
    {
        return _bbGroup;
    }

    /**
     * @param bbGroup Группа студентов ЭУК. Свойство не может быть null.
     */
    public void setBbGroup(BbGroup bbGroup)
    {
        dirty(_bbGroup, bbGroup);
        _bbGroup = bbGroup;
    }

    /**
     * @return Студент на курсе. Свойство не может быть null.
     */
    @NotNull
    public BbCourseStudent getBbCourseStudent()
    {
        return _bbCourseStudent;
    }

    /**
     * @param bbCourseStudent Студент на курсе. Свойство не может быть null.
     */
    public void setBbCourseStudent(BbCourseStudent bbCourseStudent)
    {
        dirty(_bbCourseStudent, bbCourseStudent);
        _bbCourseStudent = bbCourseStudent;
    }

    /**
     * @return Идентификатор участника группы в Blackboard. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=16)
    public String getBbPrimaryId()
    {
        return _bbPrimaryId;
    }

    /**
     * @param bbPrimaryId Идентификатор участника группы в Blackboard. Свойство не может быть null и должно быть уникальным.
     */
    public void setBbPrimaryId(String bbPrimaryId)
    {
        dirty(_bbPrimaryId, bbPrimaryId);
        _bbPrimaryId = bbPrimaryId;
    }

    /**
     * @return Запись студента в УГС (по виду аудиторной нагрузки).
     */
    public EppRealEduGroup4LoadTypeRow getGroupRow()
    {
        return _groupRow;
    }

    /**
     * @param groupRow Запись студента в УГС (по виду аудиторной нагрузки).
     */
    public void setGroupRow(EppRealEduGroup4LoadTypeRow groupRow)
    {
        dirty(_groupRow, groupRow);
        _groupRow = groupRow;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof BbGroupMembershipGen)
        {
            if (withNaturalIdProperties)
            {
                setBbGroup(((BbGroupMembership)another).getBbGroup());
                setBbCourseStudent(((BbGroupMembership)another).getBbCourseStudent());
            }
            setBbPrimaryId(((BbGroupMembership)another).getBbPrimaryId());
            setGroupRow(((BbGroupMembership)another).getGroupRow());
        }
    }

    public INaturalId<BbGroupMembershipGen> getNaturalId()
    {
        return new NaturalId(getBbGroup(), getBbCourseStudent());
    }

    public static class NaturalId extends NaturalIdBase<BbGroupMembershipGen>
    {
        private static final String PROXY_NAME = "BbGroupMembershipNaturalProxy";

        private Long _bbGroup;
        private Long _bbCourseStudent;

        public NaturalId()
        {}

        public NaturalId(BbGroup bbGroup, BbCourseStudent bbCourseStudent)
        {
            _bbGroup = ((IEntity) bbGroup).getId();
            _bbCourseStudent = ((IEntity) bbCourseStudent).getId();
        }

        public Long getBbGroup()
        {
            return _bbGroup;
        }

        public void setBbGroup(Long bbGroup)
        {
            _bbGroup = bbGroup;
        }

        public Long getBbCourseStudent()
        {
            return _bbCourseStudent;
        }

        public void setBbCourseStudent(Long bbCourseStudent)
        {
            _bbCourseStudent = bbCourseStudent;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof BbGroupMembershipGen.NaturalId) ) return false;

            BbGroupMembershipGen.NaturalId that = (NaturalId) o;

            if( !equals(getBbGroup(), that.getBbGroup()) ) return false;
            if( !equals(getBbCourseStudent(), that.getBbCourseStudent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBbGroup());
            result = hashCode(result, getBbCourseStudent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBbGroup());
            sb.append("/");
            sb.append(getBbCourseStudent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends BbGroupMembershipGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) BbGroupMembership.class;
        }

        public T newInstance()
        {
            return (T) new BbGroupMembership();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "bbGroup":
                    return obj.getBbGroup();
                case "bbCourseStudent":
                    return obj.getBbCourseStudent();
                case "bbPrimaryId":
                    return obj.getBbPrimaryId();
                case "groupRow":
                    return obj.getGroupRow();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "bbGroup":
                    obj.setBbGroup((BbGroup) value);
                    return;
                case "bbCourseStudent":
                    obj.setBbCourseStudent((BbCourseStudent) value);
                    return;
                case "bbPrimaryId":
                    obj.setBbPrimaryId((String) value);
                    return;
                case "groupRow":
                    obj.setGroupRow((EppRealEduGroup4LoadTypeRow) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "bbGroup":
                        return true;
                case "bbCourseStudent":
                        return true;
                case "bbPrimaryId":
                        return true;
                case "groupRow":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "bbGroup":
                    return true;
                case "bbCourseStudent":
                    return true;
                case "bbPrimaryId":
                    return true;
                case "groupRow":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "bbGroup":
                    return BbGroup.class;
                case "bbCourseStudent":
                    return BbCourseStudent.class;
                case "bbPrimaryId":
                    return String.class;
                case "groupRow":
                    return EppRealEduGroup4LoadTypeRow.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<BbGroupMembership> _dslPath = new Path<BbGroupMembership>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "BbGroupMembership");
    }
            

    /**
     * @return Группа студентов ЭУК. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroupMembership#getBbGroup()
     */
    public static BbGroup.Path<BbGroup> bbGroup()
    {
        return _dslPath.bbGroup();
    }

    /**
     * @return Студент на курсе. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroupMembership#getBbCourseStudent()
     */
    public static BbCourseStudent.Path<BbCourseStudent> bbCourseStudent()
    {
        return _dslPath.bbCourseStudent();
    }

    /**
     * @return Идентификатор участника группы в Blackboard. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroupMembership#getBbPrimaryId()
     */
    public static PropertyPath<String> bbPrimaryId()
    {
        return _dslPath.bbPrimaryId();
    }

    /**
     * @return Запись студента в УГС (по виду аудиторной нагрузки).
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroupMembership#getGroupRow()
     */
    public static EppRealEduGroup4LoadTypeRow.Path<EppRealEduGroup4LoadTypeRow> groupRow()
    {
        return _dslPath.groupRow();
    }

    public static class Path<E extends BbGroupMembership> extends EntityPath<E>
    {
        private BbGroup.Path<BbGroup> _bbGroup;
        private BbCourseStudent.Path<BbCourseStudent> _bbCourseStudent;
        private PropertyPath<String> _bbPrimaryId;
        private EppRealEduGroup4LoadTypeRow.Path<EppRealEduGroup4LoadTypeRow> _groupRow;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа студентов ЭУК. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroupMembership#getBbGroup()
     */
        public BbGroup.Path<BbGroup> bbGroup()
        {
            if(_bbGroup == null )
                _bbGroup = new BbGroup.Path<BbGroup>(L_BB_GROUP, this);
            return _bbGroup;
        }

    /**
     * @return Студент на курсе. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroupMembership#getBbCourseStudent()
     */
        public BbCourseStudent.Path<BbCourseStudent> bbCourseStudent()
        {
            if(_bbCourseStudent == null )
                _bbCourseStudent = new BbCourseStudent.Path<BbCourseStudent>(L_BB_COURSE_STUDENT, this);
            return _bbCourseStudent;
        }

    /**
     * @return Идентификатор участника группы в Blackboard. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroupMembership#getBbPrimaryId()
     */
        public PropertyPath<String> bbPrimaryId()
        {
            if(_bbPrimaryId == null )
                _bbPrimaryId = new PropertyPath<String>(BbGroupMembershipGen.P_BB_PRIMARY_ID, this);
            return _bbPrimaryId;
        }

    /**
     * @return Запись студента в УГС (по виду аудиторной нагрузки).
     * @see ru.tandemservice.unifefu.entity.blackboard.BbGroupMembership#getGroupRow()
     */
        public EppRealEduGroup4LoadTypeRow.Path<EppRealEduGroup4LoadTypeRow> groupRow()
        {
            if(_groupRow == null )
                _groupRow = new EppRealEduGroup4LoadTypeRow.Path<EppRealEduGroup4LoadTypeRow>(L_GROUP_ROW, this);
            return _groupRow;
        }

        public Class getEntityClass()
        {
            return BbGroupMembership.class;
        }

        public String getEntityName()
        {
            return "bbGroupMembership";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
