/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.logic;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.fias.base.entity.AddressStreet;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiDaemonDao;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.NsiPerson;

import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 04.02.2015
 */
public class NSITestDAO extends SharedBaseDao implements INSITestDAO
{
    @Override
    public void doCreateStudent(boolean lockNsiListeners)
    {
        String p1Guid = "aaaaaaaa-1111-bbbb-2222-000000000001"; // Персона 1
        String p2Guid = "aaaaaaaa-1111-bbbb-2222-000000000002"; // Персона 2
        String ic1Guid = "bbbbbbbb-1111-bbbb-2222-000000000001"; // УЛ1
        String ic2Guid = "bbbbbbbb-1111-bbbb-2222-000000000002"; // УЛ2
        String ic3Guid = "bbbbbbbb-1111-bbbb-2222-000000000003"; // УЛ3
        String nsiPerGuid = "cccccccc-1111-bbbb-2222-000000000001"; // Персона из НСИ
        String studentGuid = "dddddddd-1111-bbbb-2222-000000000001"; // Студент

        AddressCountry russia = DataAccessServices.dao().get(AddressCountry.class, AddressCountry.code(), IKladrDefines.RUSSIA_COUNTRY_CODE);
        AddressItem ekater = DataAccessServices.dao().get(AddressItem.class, AddressItem.code(), "6600000100000"); //Екатеринбург
        AddressItem peter = DataAccessServices.dao().get(AddressItem.class, AddressItem.code(), "7800000000000"); //Санкт-Петербург
        AddressStreet lenina = DataAccessServices.dao().get(AddressStreet.class, AddressStreet.codesync(), 66000001000060500L); //ул. Ленина
        AddressStreet aero = DataAccessServices.dao().get(AddressStreet.class, AddressStreet.codesync(), 78000000000010700L); //ул. Аэродромная
        IdentityCardType cardType = getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII);
        Sex male = getCatalogItem(Sex.class, SexCodes.MALE);
        Sex female = getCatalogItem(Sex.class, SexCodes.FEMALE);

        StudentCategory studCategory = getCatalogItem(StudentCategory.class, StudentCategoryCodes.STUDENT_CATEGORY_STUDENT);
        CompensationType compType = getCatalogItem(CompensationType.class, CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT);
        StudentStatus studStatus = getCatalogItem(StudentStatus.class, "1");
        Course course = DevelopGridDAO.getCourseMap().get(3);

        //TODO жуткий хардкод
        Group group = get(Group.class, Group.title(), "М1102мр");

        FefuNsiIds p1NsiId = getByNaturalId(new FefuNsiIds.NaturalId(p1Guid));
        FefuNsiIds p2NsiId = getByNaturalId(new FefuNsiIds.NaturalId(p2Guid));
        FefuNsiIds ic1NsiId = getByNaturalId(new FefuNsiIds.NaturalId(ic1Guid));
        FefuNsiIds ic2NsiId = getByNaturalId(new FefuNsiIds.NaturalId(ic2Guid));
        FefuNsiIds ic3NsiId = getByNaturalId(new FefuNsiIds.NaturalId(ic3Guid));
        FefuNsiIds nsiPerNsiId = getByNaturalId(new FefuNsiIds.NaturalId(nsiPerGuid));
        FefuNsiIds studentNsiId = getByNaturalId(new FefuNsiIds.NaturalId(studentGuid));


        NsiPerson nsiPerson = null == nsiPerNsiId ? null : get(NsiPerson.class, nsiPerNsiId.getEntityId());
        Student student = null == studentNsiId ? null : get(Student.class, studentNsiId.getEntityId());

        if (null != nsiPerson) PersonManager.instance().dao().deletePersonRole(nsiPerson.getId());
        if (null != student) PersonManager.instance().dao().deletePersonRole(student.getId());

        Person p1 = null == p1NsiId ? null : get(Person.class, p1NsiId.getEntityId());
        Person p2 = null == p2NsiId ? null : get(Person.class, p2NsiId.getEntityId());
        IdentityCard ic1 = null == ic1NsiId ? null : get(IdentityCard.class, ic1NsiId.getEntityId());
        IdentityCard ic2 = null == ic2NsiId ? null : get(IdentityCard.class, ic2NsiId.getEntityId());
        IdentityCard ic3 = null == ic3NsiId ? null : get(IdentityCard.class, ic3NsiId.getEntityId());
        nsiPerson = null;
        student = null;

        deleteEntities(lockNsiListeners, p1NsiId, p2NsiId, ic1NsiId, ic2NsiId, ic3NsiId, nsiPerNsiId, studentNsiId);
        getSession().flush();

        ic1 = createIdentityCard(ic1, male, "Дедубликатов", "Дубль", "Физлицович", CoreDateUtils.getDateByYearAndDay(1990, 251), "Владивосток", "8765", "901234", CoreDateUtils.getDateByYearAndDay(2005, 177), "Кем-то выдано", "777-000", cardType, russia, ekater, lenina, "5");
        ic2 = createIdentityCard(ic2, female, "Дедубликатова", "Дубля", "Физлицовична", CoreDateUtils.getDateByYearAndDay(1990, 251), "Владивосток", "4565", "878787", CoreDateUtils.getDateByYearAndDay(2003, 17), "Кем-то выдано", "777-000", cardType, russia, ekater, lenina, "7");
        ic3 = createIdentityCard(ic3, male, "Дедубликатовский", "Дубль", "Физлицовичь", CoreDateUtils.getDateByYearAndDay(1990, 300), "Екатеринбург", "3454", "232323", CoreDateUtils.getDateByYearAndDay(2001, 107), "Некто выдал", "555-001", cardType, russia, peter, aero, "14");
        saveOrUpdateWithNsiListenersLock(ic1.getPhoto(), lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(ic1.getAddress(), lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(ic2.getPhoto(), lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(ic2.getAddress(), lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(ic3.getPhoto(), lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(ic3.getAddress(), lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(ic1, lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(ic2, lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(ic3, lockNsiListeners);

        p1 = createPerson(p1, ic1, "234234234234", "345-345-345 55", russia, peter, aero, "456");
        p2 = createPerson(p2, ic3, "345567777777", "888-555-656 34", russia, ekater, lenina, "44");
        saveOrUpdateWithNsiListenersLock(p1.getAddress(), lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(p1.getContactData(), lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(p2.getAddress(), lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(p2.getContactData(), lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(p1, lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(p2, lockNsiListeners);

        ic1.setPerson(p1);
        ic2.setPerson(p1);
        ic3.setPerson(p2);
        saveOrUpdateWithNsiListenersLock(ic1, lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(ic2, lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(ic3, lockNsiListeners);

        nsiPerson = createNsiPerson(nsiPerson, p1);
        student = createStudent(student, p2, "111111", 2007, studCategory, studStatus, course, group, compType, group.getEducationOrgUnit());
        saveOrUpdateWithNsiListenersLock(nsiPerson, lockNsiListeners);
        saveOrUpdateWithNsiListenersLock(student, lockNsiListeners);

        p1NsiId = refreshNsiId(p1NsiId, p1Guid, p1.getId(), Person.ENTITY_NAME);
        p2NsiId = refreshNsiId(p2NsiId, p2Guid, p2.getId(), Person.ENTITY_NAME);
        ic1NsiId = refreshNsiId(ic1NsiId, ic1Guid, ic1.getId(), IdentityCard.ENTITY_NAME);
        ic2NsiId = refreshNsiId(ic2NsiId, ic2Guid, ic2.getId(), IdentityCard.ENTITY_NAME);
        ic3NsiId = refreshNsiId(ic3NsiId, ic3Guid, ic3.getId(), IdentityCard.ENTITY_NAME);
        nsiPerNsiId = refreshNsiId(nsiPerNsiId, nsiPerGuid, nsiPerson.getId(), NsiPerson.ENTITY_NAME);
        studentNsiId = refreshNsiId(studentNsiId, studentGuid, student.getId(), Student.ENTITY_NAME);
        saveOrUpdate(p1NsiId);
        saveOrUpdate(p2NsiId);
        saveOrUpdate(ic1NsiId);
        saveOrUpdate(ic2NsiId);
        saveOrUpdate(ic3NsiId);
        saveOrUpdate(nsiPerNsiId);
        saveOrUpdate(studentNsiId);
    }

    private IdentityCard createIdentityCard(IdentityCard icard, Sex sex, String lastName, String firstName, String middleName, Date birthDate, String birthPlace, String cardSer, String cardNum, Date issuDate, String issuPlace, String issuCode, IdentityCardType type, AddressCountry country, AddressItem settlement, AddressStreet street, String houseNum)
    {
        if (null == icard) icard = new IdentityCard();

        DatabaseFile photo = null != icard.getPhoto() ? icard.getPhoto() : new DatabaseFile();
        AddressRu icAddr = null != icard.getAddress() ? (AddressRu) icard.getAddress() : new AddressRu();
        icAddr.setCountry(country);
        icAddr.setSettlement(settlement);
        icAddr.setStreet(street);
        icAddr.setHouseNumber(houseNum);

        icard.setSex(sex);
        icard.setCardType(type);
        icard.setPhoto(photo);
        icard.setAddress(icAddr);
        icard.setCitizenship(country);
        icard.setLastName(lastName);
        icard.setFirstName(firstName);
        icard.setMiddleName(middleName);
        icard.setSeria(cardSer);
        icard.setNumber(cardNum);
        icard.setIssuanceDate(issuDate);
        icard.setIssuancePlace(issuPlace);
        icard.setIssuanceCode(issuCode);
        icard.setBirthDate(birthDate);
        icard.setBirthPlace(birthPlace);

        return icard;
    }

    private Person createPerson(Person person, IdentityCard icard, String inn, String snils, AddressCountry country, AddressItem settlement, AddressStreet street, String houseNum)
    {
        if (null == person) person = new Person();

        AddressRu perAddr = null != person.getAddress() ? (AddressRu) person.getAddress() : new AddressRu();
        perAddr.setCountry(country);
        perAddr.setSettlement(settlement);
        perAddr.setStreet(street);
        perAddr.setHouseNumber(houseNum);

        PersonContactData contact = null != person.getContactData() ? person.getContactData() : new PersonContactData();

        person.setIdentityCard(icard);
        person.setAddress(perAddr);
        person.setContactData(contact);
        person.setInnNumber(inn);
        person.setSnilsNumber(snils);

        return person;
    }

    private NsiPerson createNsiPerson(NsiPerson nsiPerson, Person person)
    {
        if (null == nsiPerson) nsiPerson = new NsiPerson();

        nsiPerson.setPerson(person);
        nsiPerson.setComment("Некая персона из НСИ");
        nsiPerson.setCreateDate(new Date());
        return nsiPerson;
    }

    private Student createStudent(Student student, Person person, String perNumber, int entrYear, StudentCategory category, StudentStatus status, Course course, Group group, CompensationType compType, EducationOrgUnit eduOrgUnit)
    {
        if (null == student) student = new Student();

        student.setPerson(person);
        student.setArchival(false);
        student.setPersonalNumber(perNumber);
        student.setEntranceYear(entrYear);
        student.setStatus(status);
        student.setCourse(course);
        student.setStudentCategory(category);
        student.setCompensationType(compType);
        student.setEducationOrgUnit(eduOrgUnit);
        student.setDevelopPeriodAuto(eduOrgUnit.getDevelopPeriod());
        student.setGroup(group);

        return student;
    }

    private FefuNsiIds refreshNsiId(FefuNsiIds nsiIds, String guid, Long entityId, String entityType)
    {
        //if (null == nsiIds)
        {
            nsiIds = new FefuNsiIds();
            nsiIds.setGuid(guid);
        }

        nsiIds.setEntityId(entityId);
        nsiIds.setEntityType(entityType);
        nsiIds.setSyncTime(new Date());

        return nsiIds;
    }

    private void saveOrUpdateWithNsiListenersLock(IEntity entity, boolean lockNsiListeners)
    {
        saveOrUpdate(entity);
        if (lockNsiListeners) FefuNsiDaemonDao.registerNsiEntityInfo(entity);
    }

    private void deleteEntities(boolean lockNsiListeners, IEntity... entities)
    {
        for (IEntity entity : entities)
        {
            if (null == entity) continue;
            delete(entity);
            if (lockNsiListeners) FefuNsiDaemonDao.registerNsiEntityInfo(entity);
        }
    }
}