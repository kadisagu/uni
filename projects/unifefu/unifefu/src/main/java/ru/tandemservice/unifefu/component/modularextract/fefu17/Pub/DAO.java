/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu17.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.component.modularextract.fefu17.utils.TransportKindFormatter;
import ru.tandemservice.unifefu.entity.TransitCompensationStuExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2014
 */
public class DAO extends ModularStudentExtractPubDAO<TransitCompensationStuExtract, Model> implements IDAO
{
	@Override
	public void prepare(Model model)
	{
		super.prepare(model);
		model.setCompensationSum(model.getExtract().getCompensationSum() / 100D);
		model.setTransportKind(new TransportKindFormatter(TransportKindFormatter.SUFFIX_N).format(model.getExtract().getTransportKind()));
	}
}
