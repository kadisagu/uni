/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeRel;
import ru.tandemservice.unifefu.entity.FefuSignerStuExtractRel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 13.01.2014
 */
public class FefuMoveStudentInjectModifierDao extends CommonDAO implements IFefuMoveStudentInjectModifierDao
{
    @Override
    public String[][] getVisaSignerExtract(String orderTypeCode, String eduLevelStageCode)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(FefuSignerStuExtractRel.class, "rel")
                .where(eq(property("rel", FefuSignerStuExtractRel.setting().studentExtractType().code()), value(orderTypeCode)))
                .where(eq(property("rel", FefuSignerStuExtractRel.eduLevelStage().code()), value(eduLevelStageCode)));

        FefuSignerStuExtractRel signer = builder.createStatement(getSession()).uniqueResult();

        if (null == signer)
            return new String[][]{};
        else
            return new String[][]{{signer.getSigner().getTitle(), signer.getSigner().getEntity().getPerson().getIdentityCard().getIof()}};
    }

    @Override
    public String[][] getVisaSignerDocumentType(String documentTypeCode, String levelStageCode)
    {
        FefuSignerStuDocTypeRel signer = getSignerDocumentType(documentTypeCode, levelStageCode);
        if (null == signer)
            return new String[][]{};
        else
            return new String[][]{{signer.getSigner().getTitle(), signer.getSigner().getEntity().getPerson().getIdentityCard().getIof()}};
    }

    @Override
    public FefuSignerStuDocTypeRel getSignerDocumentType(String documentTypeCode, String levelStageCode)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(FefuSignerStuDocTypeRel.class, "rel")
                .where(eq(property("rel", FefuSignerStuDocTypeRel.setting().studentDocumentType().code()), value(documentTypeCode)))
                .where(eq(property("rel", FefuSignerStuDocTypeRel.eduLevelStage().code()), value(levelStageCode)));

        return builder.createStatement(getSession()).uniqueResult();
    }
}
