package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. «Приказ по контингенту ДО/ДПО»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuOrderContingentStuDPOExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract";
    public static final String ENTITY_NAME = "fefuOrderContingentStuDPOExtract";
    public static final int VERSION_HASH = 1230236745;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String L_STUDENT_STATUS_NEW = "studentStatusNew";
    public static final String L_STUDENT_CUSTOM_STATE_NEW = "StudentCustomStateNew";

    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента
    private StudentStatus _studentStatusNew;     // Новое состояние студента
    private StudentCustomStateCI _StudentCustomStateNew;     // Новый дополнительный статус

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Предыдущее состояние студента.
     */
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    /**
     * @param studentStatusNew Новое состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        dirty(_studentStatusNew, studentStatusNew);
        _studentStatusNew = studentStatusNew;
    }

    /**
     * @return Новый дополнительный статус.
     */
    public StudentCustomStateCI getStudentCustomStateNew()
    {
        return _StudentCustomStateNew;
    }

    /**
     * @param StudentCustomStateNew Новый дополнительный статус.
     */
    public void setStudentCustomStateNew(StudentCustomStateCI StudentCustomStateNew)
    {
        dirty(_StudentCustomStateNew, StudentCustomStateNew);
        _StudentCustomStateNew = StudentCustomStateNew;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuOrderContingentStuDPOExtractGen)
        {
            setStudentStatusOld(((FefuOrderContingentStuDPOExtract)another).getStudentStatusOld());
            setStudentStatusNew(((FefuOrderContingentStuDPOExtract)another).getStudentStatusNew());
            setStudentCustomStateNew(((FefuOrderContingentStuDPOExtract)another).getStudentCustomStateNew());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuOrderContingentStuDPOExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuOrderContingentStuDPOExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuOrderContingentStuDPOExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "studentStatusNew":
                    return obj.getStudentStatusNew();
                case "StudentCustomStateNew":
                    return obj.getStudentCustomStateNew();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "studentStatusNew":
                    obj.setStudentStatusNew((StudentStatus) value);
                    return;
                case "StudentCustomStateNew":
                    obj.setStudentCustomStateNew((StudentCustomStateCI) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                        return true;
                case "studentStatusNew":
                        return true;
                case "StudentCustomStateNew":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return true;
                case "studentStatusNew":
                    return true;
                case "StudentCustomStateNew":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "studentStatusOld":
                    return StudentStatus.class;
                case "studentStatusNew":
                    return StudentStatus.class;
                case "StudentCustomStateNew":
                    return StudentCustomStateCI.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuOrderContingentStuDPOExtract> _dslPath = new Path<FefuOrderContingentStuDPOExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuOrderContingentStuDPOExtract");
    }
            

    /**
     * @return Предыдущее состояние студента.
     * @see ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract#getStudentStatusNew()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusNew()
    {
        return _dslPath.studentStatusNew();
    }

    /**
     * @return Новый дополнительный статус.
     * @see ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract#getStudentCustomStateNew()
     */
    public static StudentCustomStateCI.Path<StudentCustomStateCI> StudentCustomStateNew()
    {
        return _dslPath.StudentCustomStateNew();
    }

    public static class Path<E extends FefuOrderContingentStuDPOExtract> extends ModularStudentExtract.Path<E>
    {
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private StudentStatus.Path<StudentStatus> _studentStatusNew;
        private StudentCustomStateCI.Path<StudentCustomStateCI> _StudentCustomStateNew;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Предыдущее состояние студента.
     * @see ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract#getStudentStatusNew()
     */
        public StudentStatus.Path<StudentStatus> studentStatusNew()
        {
            if(_studentStatusNew == null )
                _studentStatusNew = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_NEW, this);
            return _studentStatusNew;
        }

    /**
     * @return Новый дополнительный статус.
     * @see ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract#getStudentCustomStateNew()
     */
        public StudentCustomStateCI.Path<StudentCustomStateCI> StudentCustomStateNew()
        {
            if(_StudentCustomStateNew == null )
                _StudentCustomStateNew = new StudentCustomStateCI.Path<StudentCustomStateCI>(L_STUDENT_CUSTOM_STATE_NEW, this);
            return _StudentCustomStateNew;
        }

        public Class getEntityClass()
        {
            return FefuOrderContingentStuDPOExtract.class;
        }

        public String getEntityName()
        {
            return "fefuOrderContingentStuDPOExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
