package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic;

public class Constants
{
    public static final String STUDENT_ID = "studentId";
    public static final String APE_PROGRAM_DS = "apeProgramDS";

    public static final String INDIVIDUAL_DEVELOP_FORM_DS = "individualDevelopFormDS";
    public static final String INDIVIDUAL_DEVELOP_CONDITION_DS = "individualDevelopConditionDS";
    public static final String INDIVIDUAL_DEVELOP_TECH_DS = "individualDevelopTechDS";
    public static final String EDUCATION_ORG_UNIT = "educationOrgUnit";

    public static final String TITLE = "title";
}
