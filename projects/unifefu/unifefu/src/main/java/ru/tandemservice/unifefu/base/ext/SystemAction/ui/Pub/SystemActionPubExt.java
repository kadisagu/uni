/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
@Configuration
public class SystemActionPubExt extends BusinessComponentExtensionManager
{
    public static final String FEFU_SYSTEM_ACTION_PUB_ADDON_NAME = "fefuSystemActionPubAddon";

    @Autowired
    private SystemActionPub _systemActionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_systemActionPub.presenterExtPoint())
                .addAddon(uiAddon(FEFU_SYSTEM_ACTION_PUB_ADDON_NAME, FefuSystemActionPubAddon.class))
                .create();
    }
}
