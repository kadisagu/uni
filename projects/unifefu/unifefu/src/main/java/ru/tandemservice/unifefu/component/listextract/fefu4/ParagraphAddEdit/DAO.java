/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.ParagraphAddEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.student.IAcademicGrantSizeDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;

import java.util.*;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuAcadGrantAssignStuEnrolmentListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setCompensationTypeDisabled(!model.isParagraphOnlyOneInTheOrder());

        FefuAcadGrantAssignStuEnrolmentListExtract firstExtract = model.getFirstExtract();
        if(null != firstExtract)
        {
            if(null != model.getParagraphId())
            {
                model.setCourse(firstExtract.getCourse());
                model.setGroup(firstExtract.getGroup());
            }
            model.setCompensationType(firstExtract.getCompensationType());
            model.setBeginDate(firstExtract.getBeginDate());
            model.setEndDate(firstExtract.getEndDate());
        }
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected FefuAcadGrantAssignStuEnrolmentListExtract createNewInstance(Model model)
    {
        return new FefuAcadGrantAssignStuEnrolmentListExtract();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void fillExtract(FefuAcadGrantAssignStuEnrolmentListExtract extract, Student student, Model model)
    {
        IValueMapHolder<Long> grantHolder = (IValueMapHolder<Long>) model.getDataSource().getColumn(Controller.GRANT_COLUMN);
        Map<Long, Long> grantMap = (null == grantHolder ? Collections.<Long, Long>emptyMap() : grantHolder.getValueMap());

        IValueMapHolder<Long> groupManagerBonusHolder = (IValueMapHolder<Long>) model.getDataSource().getColumn(Controller.GROUP_MANAGER_BONUS_COLUMN);
        Map<Long, Long> groupManagerBonusMap = (null == grantHolder ? Collections.<Long, Long>emptyMap() : groupManagerBonusHolder.getValueMap());

        Long grant = grantMap.get(student.getId());
        Long groupManagerBonus = groupManagerBonusMap.get(student.getId());

        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
        extract.setBeginDate(model.getBeginDate());
        extract.setEndDate(model.getEndDate());
        extract.setGrantSizeInRuble(grant);
        extract.setGroupManagerBonusSizeInRuble(groupManagerBonus);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        IValueMapHolder<String> grantHolder = (IValueMapHolder<String>) model.getDataSource().getColumn(Controller.GRANT_COLUMN);
        Map<Long, String> grantMap = (null == grantHolder ? Collections.<Long, String>emptyMap() : grantHolder.getValueMap());

        List<ViewWrapper> viewList = new ArrayList<ViewWrapper>((Collection) ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects());

        for (ViewWrapper wrapper : viewList)
        {
            if (grantMap.get(wrapper.getId()) == null)
                errorCollector.add("Поле «" + model.getDataSource().getColumn(Controller.GRANT_COLUMN).getCaption() + "» обязательно для заполнения.", "grantSizeId_" + wrapper.getId());
        }

        if (!model.getBeginDate().before(model.getEndDate()))
            errorCollector.add("Дата окончания должна быть больше даты начала.", "beginDate", "endDate");

        if (errorCollector.hasErrors())
            return;

        super.update(model);
    }

    @Override
    public void prepareGroupManagerList(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        List<ViewWrapper<Student>> studentList = ViewWrapper.getPatchedList(dataSource);
        List<Long> studentIds = new ArrayList<>();
        for (ViewWrapper wrapper : studentList)
        {
            studentIds.add(wrapper.getId());
        }

        List<Long> ids = new DQLSelectBuilder()
                .fromEntity(GroupCaptainStudent.class, "g")
                .column(DQLExpressions.property(GroupCaptainStudent.student().id().fromAlias("g")))
                .where(DQLExpressions.in(DQLExpressions.property(GroupCaptainStudent.student().id().fromAlias("g")), studentIds))
                .createStatement(getSession())
                .list();

        model.setGroupManagerIds(ids);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void fillGrantSizeDefaultValues(Model model)
    {
        Integer defaultGrant = IAcademicGrantSizeDAO.instance.get().getDefaultGrantSize();
        Integer defaultGroupManagerBonus = IAcademicGrantSizeDAO.instance.get().getDefaultGroupManagerBonusSize();

        if (defaultGrant != null || defaultGroupManagerBonus != null)
        {
            DynamicListDataSource dataSource = model.getDataSource();
            List<ViewWrapper<Student>> studentList = ViewWrapper.getPatchedList(dataSource);

            IValueMapHolder<Long> grantHolder = (IValueMapHolder<Long>) model.getDataSource().getColumn(Controller.GRANT_COLUMN);
            Map<Long, Long> grantMap = (null == grantHolder ? Collections.<Long, Long>emptyMap() : grantHolder.getValueMap());

            List<Long> groupManagerIds = model.getGroupManagerIds();
            IValueMapHolder<Long> groupManagerBonusHolder = (IValueMapHolder<Long>) model.getDataSource().getColumn(Controller.GROUP_MANAGER_BONUS_COLUMN);
            Map<Long, Long> groupManagerBonusMap = (null == groupManagerBonusHolder ? Collections.<Long, Long>emptyMap() : groupManagerBonusHolder.getValueMap());

            for (ViewWrapper<Student> wrapper : studentList)
            {
                Long id = wrapper.getId();
                if (!grantMap.containsKey(id) && defaultGrant != null)
                    grantMap.put(id, defaultGrant.longValue());
                if (groupManagerIds.contains(id) && !groupManagerBonusMap.containsKey(id) && defaultGroupManagerBonus != null)
                    groupManagerBonusMap.put(id, defaultGroupManagerBonus.longValue());
            }
        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        super.prepareListDataSource(model);
        prepareGroupManagerList(model);
        fillGrantSizeDefaultValues(model);
    }
}