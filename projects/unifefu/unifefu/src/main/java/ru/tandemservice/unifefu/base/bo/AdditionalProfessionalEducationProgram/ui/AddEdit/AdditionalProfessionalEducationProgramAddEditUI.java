package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;

import java.util.Date;

import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.logic.Constants.*;

@Input({
               @Bind(key = APE_PROGRAM_ID, binding = APE_PROGRAM_ID)
       })
public class AdditionalProfessionalEducationProgramAddEditUI extends UIPresenter
{
    private Long apeProgramId;
    private FefuAdditionalProfessionalEducationProgram program;

    @Override
    public void onComponentRefresh()
    {
        if (apeProgramId == null)
        {
            program = new FefuAdditionalProfessionalEducationProgram();
        }
        else
        {
            program = DataAccessServices.dao().getNotNull(FefuAdditionalProfessionalEducationProgram.class, apeProgramId);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EDUCATION_ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put(FORMATIVE_ORG_UNIT, program.getFormativeOrgUnit());
            dataSource.put(TERRITORIAL_ORG_UNIT, program.getTerritorialOrgUnit());
        }
        else if (TERRITORIAL_ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put(FORMATIVE_ORG_UNIT, program.getFormativeOrgUnit());
        }
    }

    public void onClickApply()
    {
        if (validate().hasErrors())
        {
            return;
        }
        DataAccessServices.dao().saveOrUpdate(program);
        deactivate();
    }

    protected ErrorCollector validate()
    {
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        Date startDate = program.getProgramOpeningDate();
        Date endDate = program.getProgramClosingDate();
        if (startDate != null && endDate != null && endDate.before(startDate))
        {
            errorCollector.add("Дата открытия программы не может быть больше даты закрытия.", "programOpeningDate", "programClosingDate");
        }

        return errorCollector;
    }

    public Long getApeProgramId()
    {
        return apeProgramId;
    }

    public void setApeProgramId(Long apeProgramId)
    {
        this.apeProgramId = apeProgramId;
    }

    public FefuAdditionalProfessionalEducationProgram getProgram()
    {
        return program;
    }

    public void setProgram(FefuAdditionalProfessionalEducationProgram program)
    {
        this.program = program;
    }

    public String getPermissionKey()
    {
        return apeProgramId == null ? "addFefuApeProgramList" : "editFefuApeProgramList";
    }

}
