package ru.tandemservice.unifefu.base.ext.EcDistribution.logic;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.IEcDistributionListPrint;

/**
 * User: newdev
 * Date: 31.07.13
 */
public interface IFefuEcDistributionListPrint extends IEcDistributionListPrint
{
    public static final SpringBeanCache<IFefuEcDistributionListPrint> instance = new SpringBeanCache<>(IFefuEcDistributionListPrint.class.getName());
//
//    /**
//     * Печатает распределение по направлениям
//     *
//     * @param template     шаблон
//     * @param distribution основное распределение
//     * @return rtf-документ
//     */
//    RtfDocument getPrintFormForDistributionPerDirection(byte[] template, EcgDistribution distribution);
//
//    /**
//     * Печатает распределение по конкурсным группам
//     *
//     * @param template     шаблон
//     * @param distribution основное или уточняющее распределение
//     * @return rtf-документ
//     */
//    RtfDocument getPrintFormForDistributionPerCompetitionGroup(byte[] template, IEcgDistribution distribution);

    void setWithoutEnrolled(boolean isWithoutEnrolled);

    boolean isWithoutEnrolled();
}
