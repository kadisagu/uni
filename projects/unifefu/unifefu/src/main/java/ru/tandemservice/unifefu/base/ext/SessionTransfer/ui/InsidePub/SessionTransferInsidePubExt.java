/* $Id $ */
package ru.tandemservice.unifefu.base.ext.SessionTransfer.ui.InsidePub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsidePub.SessionTransferInsidePub;

/**
 * @author Rostuncev Savva
 * @since 20.03.2014
 */
@Configuration
public class SessionTransferInsidePubExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "fefu" + SessionTransferInsidePubExtUI.class.getSimpleName();

    @Autowired
    private SessionTransferInsidePub _sessionTransferInsidePub;


    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_sessionTransferInsidePub.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, SessionTransferInsidePubExtUI.class))
                .create();
    }

}
