/* $Id$ */
package ru.tandemservice.unifefu.component.group.FefuGroupPub;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLStatement;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.uni.component.group.GroupPub.Model;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 08.11.2013
 */
public class DAO extends ru.tandemservice.uni.component.group.GroupPub.DAO implements IDAO
{
    public static final String ORDER_NON_STATE_FINISHED = "orderNonStateFinished";

    @Override
    public void setArchival(Model model, boolean archival, ErrorCollector errorCollector)
    {
        super.setArchival(model, archival, errorCollector);
        if (!archival)
        {
            Group group = model.getGroup();
            List<Student> groupStudents = getList(Student.class, Student.L_GROUP, group);

            for (Student student : groupStudents)
            {
                student.setArchivingDate(null);
            }
        }
        else
        {
            UniDaoFacade.getGroupDao().deleteCaptainStudentList(model.getGroup());
        }
    }

    @SuppressWarnings("unchecked")
        @Override
        public void prepareListDataSource(Model model)
        {
            super.prepareListDataSource(model);
            DynamicListDataSource<Student> dataSource = model.getStudentDataSource();

            List<ViewWrapper<Student>> list = ViewWrapper.<Student>getPatchedList(dataSource);


            DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                            .fromEntity(AbstractStudentExtract.class, "ase")
                            .joinPath(DQLJoinType.left, AbstractStudentExtract.paragraph().fromAlias("ase"), "asep")
                            .joinPath(DQLJoinType.left, AbstractStudentParagraph.order().fromAlias("asep"), "aso")
                            .where(or(
                                    isNull(property(AbstractStudentExtract.paragraph().id().fromAlias("ase"))),
                                    ne(property(AbstractStudentOrder.state().code().fromAlias("aso")), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED))))
                            .where(in(property(AbstractStudentExtract.entity().id().fromAlias("ase")),  UniBaseDao.ids(list)))
                            .column(property(AbstractStudentExtract.entity().id().fromAlias("ase")))
                            .column(DQLFunctions.count(property("ase.id")))
                            .group(property(AbstractStudentExtract.entity().id().fromAlias("ase")));

            IDQLStatement statement = subBuilder.createStatement(getSession());
            Map<Long, Integer> id2count = new HashMap<>();
            for (Object[] row : statement.<Object[]>list())
            {
                Long id = (Long) row[0];
                Number count = (Number) row[1];
                id2count.put(id, count == null ? 0 : count.intValue());
            }

            for (ViewWrapper<Student> viewWrapper : ViewWrapper.<Student>getPatchedList(dataSource))
            {
                Integer value = id2count.get(viewWrapper.getId());
                viewWrapper.setViewProperty(ORDER_NON_STATE_FINISHED, value == null || value == 0 ? "" : "Есть непроведенные приказы");
            }
        }

	@Override
	protected void sortStudentList(DynamicListDataSource<Student> dataSource, List<Student> list)
	{
		super.sortStudentList(dataSource, list);
		if (dataSource.getEntityOrder().getKey().equals(Student.compensationType().shortTitle().s()))
			Collections.sort(list, new FefuGroupStudentListCmp(dataSource.getEntityOrder().getDirection()));
	}
}
