/* $Id: FefuRegistryDAO.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.logic;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.cxf.common.util.StringUtils;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uni.util.SafeSimpleColumn;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppBaseRegElWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartModuleWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementLoadGen;
import ru.tandemservice.uniepp.ui.EduPlanVersionBlockDataSourceGenerator;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.FefuRegistryManager;
import ru.tandemservice.unifefu.base.bo.FefuSettings.ui.ParametersLabor.FefuSettingsParametersLaborUI;
import ru.tandemservice.unifefu.base.vo.FefuLoadVO;
import ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel;
import ru.tandemservice.unifefu.entity.catalog.FefuLoadType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuLoadTypeCodes;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEppRegistryModuleALoadExt;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEppRegistryModuleELoad;
import ru.tandemservice.unispp.base.entity.SppRegElementExt;
import ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 05.02.14
 * Time: 12:40
 */
@Transactional
public class FefuRegistryDAO extends UniBaseDao implements IFefuRegistryDAO
{
    @Override
    public StaticListDataSource<RowWrapper> getRegElementDataSource(EppRegistryElement regElement)
    {
        Map<String, String> loadTypesMap = Maps.newHashMap();
        Map<String, FefuLoadType> interactiveLoadTypeMap = Maps.newHashMap();

        loadTypesMap.put(EppALoadTypeCodes.TYPE_LECTURES, FefuLoadTypeCodes.TYPE_LECTURES_INTER);
        loadTypesMap.put(EppALoadTypeCodes.TYPE_PRACTICE, FefuLoadTypeCodes.TYPE_PRACTICE_INTER);
        loadTypesMap.put(EppALoadTypeCodes.TYPE_LABS, FefuLoadTypeCodes.TYPE_LABS_INTER);

        for (FefuLoadType loadType : DataAccessServices.dao().getList(FefuLoadType.class))
        {
            interactiveLoadTypeMap.put(loadType.getCode(), loadType);
        }

        final IEppRegElWrapper elementWrapper = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(regElement.getId())).get(regElement.getId());
        final Map<String, Double> loadMap = getLoadMap(regElement);

        /**    {moduleId -> {fefuLoadTypeCode -> value}} **/
        Map<Long, Map<String, Number>> interactiveLoadMap = getInteractiveLoadMap(elementWrapper);

        EppELoadType selfWorkLoadType = DataAccessServices.dao().get(EppELoadType.class, EppELoadType.code(), EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);

        /**    {moduleId -> {ELoadTypeCode -> value}} **/
        Map<Long, Map<String, Number>> eLoadMap = getELoadMap(elementWrapper, selfWorkLoadType);


        final List<RowWrapper> rowList = new ArrayList<>();
        final RowWrapper elementRow = new RowWrapper(elementWrapper, null, false);
        rowList.add(elementRow);

        final StaticListDataSource<RowWrapper> ds = new StaticListDataSource<>();

        // кастомизация списка
        ds.setRowCustomizer(new SimpleRowCustomizer<RowWrapper>()
        {
            @Override
            public String getRowStyle(final RowWrapper entity)
            {
                final IEppBaseRegElWrapper w = entity.getEntity();
                if (w instanceof IEppRegElWrapper)
                {
                    return "font-weight:bold;background-color: " + UniDefines.COLOR_BLUE + ";";
                }
                if (w instanceof IEppRegElPartWrapper)
                {
                    return "background-color: " + UniDefines.COLOR_BLUE + ";";
                }
                return "";
            }
        });

        // базовые колонки
        ds.addColumn(UniEppUtils.getStateColumn("item.module"));
        ds.addColumn(new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__TITLE, "title").setClickable(false).setOrderable(false).setTreeColumn(true));
        ds.addColumn(new SafeSimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__OWNER, EppRegistryElementPartModule.module().owner().shortTitle().fromAlias("item")).setClickable(false).setOrderable(false));

        // всего часов, всего трудоемкости
        final HeadColumn h = new HeadColumn("load", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__LOAD);
        h.addColumn(column_load(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_TOTAL_SIZE, EppLoadType.FULL_CODE_TOTAL_HOURS, loadMap));
        h.addColumn(column_load(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_TOTAL_LABOR, EppLoadType.FULL_CODE_LABOR, loadMap));

        // аудиторная нагрузка
        final HeadColumn al = new HeadColumn("aload", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__A_LOAD);
        final List<EppALoadType> loads = DataAccessServices.dao().getList(EppALoadType.class, EppALoadType.P_CODE);
        for (final EppALoadType load : loads)
        {
            al.addColumn(column_load(load, loadMap));
            al.addColumn(column_fefu_load(interactiveLoadTypeMap.get(loadTypesMap.get(load.getCode())), interactiveLoadMap));
        }
        h.addColumn(al.setWidth(1).setHeaderAlign("center"));
        h.addColumn(column_fefu_load(interactiveLoadTypeMap.get(FefuLoadTypeCodes.TYPE_EXAM_HOURS), interactiveLoadMap).setWidth(1).setHeaderAlign("center"));
        h.addColumn(column_e_load(selfWorkLoadType, eLoadMap).setWidth(1).setHeaderAlign("center"));

        ds.addColumn(h.setWidth(1).setHeaderAlign("center"));

        // формы контроля (итоговые и текущие)
        final HeadColumn cc = new HeadColumn("cacts", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__CONTROL_ACTIONS);
        final List<EppControlActionType> actions = DataAccessServices.dao().getList(EppControlActionType.class, EppControlActionType.P_CATALOG_CODE, EppControlActionType.P_CODE);
        for (final EppControlActionType ca : actions)
        {
            final int sz = elementWrapper.getActionSize(ca.getFullCode());
            if (sz > 0)
            {
                cc.addColumn(column_action(ca));
            }
        }
        ds.addColumn(cc.setWidth(1).setHeaderAlign("center"));

        // дейсвтия со строками
        ds.addColumn(new BlockColumn("edit", "", "rowActionsBlock").setWidth(1));

        for (final IEppRegElPartWrapper partWrapper : elementWrapper.getPartMap().values())
        {
            final RowWrapper partRow = new RowWrapper(partWrapper, elementRow, true);
            rowList.add(partRow);

            for (final IEppRegElPartModuleWrapper moduleWrapper : partWrapper.getModuleMap().values())
            {
                final RowWrapper moduleRow = new RowWrapper(moduleWrapper, partRow, true);
                rowList.add(moduleRow);
            }
        }

        ds.setupRows(rowList);
        return ds;
    }

    @Override
    public List<FefuLoadVO> moduleLoadList(Long moduleId, boolean createNewIfLoadNotExist)
    {
        List<FefuLoadVO> resultList = Lists.newArrayList();
        EppRegistryModule module = get(moduleId);

        Map<String, String> loadTypesMap = Maps.newHashMap();

        loadTypesMap.put(EppALoadTypeCodes.TYPE_LECTURES, FefuLoadTypeCodes.TYPE_LECTURES_INTER);
        loadTypesMap.put(EppALoadTypeCodes.TYPE_PRACTICE, FefuLoadTypeCodes.TYPE_PRACTICE_INTER);
        loadTypesMap.put(EppALoadTypeCodes.TYPE_LABS, FefuLoadTypeCodes.TYPE_LABS_INTER);

        List<EppALoadType> loadTypes = DataAccessServices.dao().getList(EppALoadType.class, EppALoadType.code().s());

        List<FefuLoadType> fefuLoadTypes = DataAccessServices.dao().getList(FefuLoadType.class);
        Map<String, FefuLoadType> fefuLoadTypeMap = Maps.newHashMap();
        for (FefuLoadType fefuLoadType : fefuLoadTypes)
        {
            fefuLoadTypeMap.put(fefuLoadType.getCode(), fefuLoadType);
        }


        // аудиторная нагрузка
        DQLSelectBuilder aLoadBuilder = new DQLSelectBuilder().fromEntity(EppRegistryModuleALoad.class, "al");
        aLoadBuilder.where(eq(property("al", EppRegistryModuleALoad.module().id()), value(moduleId)));

        Map<String, EppRegistryModuleALoad> aLoadMap = Maps.newHashMap();
        for (EppRegistryModuleALoad aLoad : createStatement(aLoadBuilder).<EppRegistryModuleALoad>list())
        {
            aLoadMap.put(aLoad.getLoadType().getCode(), aLoad);
        }

        // интерактивная нагрузка
        // часы за экз.
        DQLSelectBuilder fefuExtALoadBuilder = new DQLSelectBuilder().fromEntity(FefuEppRegistryModuleALoadExt.class, "fl");
        fefuExtALoadBuilder.where(eq(property("fl", FefuEppRegistryModuleALoadExt.module().id()), value(moduleId)));

        Map<String, FefuEppRegistryModuleALoadExt> fefuLoadMap = Maps.newHashMap();
        for (FefuEppRegistryModuleALoadExt fefuLoad : createStatement(fefuExtALoadBuilder).<FefuEppRegistryModuleALoadExt>list())
        {
            fefuLoadMap.put(fefuLoad.getLoadType().getCode(), fefuLoad);
        }

        // самостоятельная

        EppELoadType selfWorkLoadType = DataAccessServices.dao().get(EppELoadType.class, EppELoadType.code(), EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);

        DQLSelectBuilder eppELoadBuilder = new DQLSelectBuilder().fromEntity(FefuEppRegistryModuleELoad.class, "el");
        eppELoadBuilder.where(eq(property("el", FefuEppRegistryModuleELoad.module().id()), value(moduleId)));
        eppELoadBuilder.where(eq(property("el", FefuEppRegistryModuleELoad.loadType().id()), value(selfWorkLoadType.getId())));

        Map<String, FefuEppRegistryModuleELoad> eppELoadMap = Maps.newHashMap();
        for (FefuEppRegistryModuleELoad eppELoad : createStatement(eppELoadBuilder).<FefuEppRegistryModuleELoad>list())
        {
            eppELoadMap.put(eppELoad.getLoadType().getCode(), eppELoad);
        }

        for (EppALoadType aLoadType : loadTypes)
        {
            String aLoadTypeCode = aLoadType.getCode();
            EppRegistryModuleALoad aLoad = aLoadMap.get(aLoadTypeCode);
            if (createNewIfLoadNotExist && null == aLoad)
            {
                aLoad = new EppRegistryModuleALoad();
                aLoad.setModule(module);
                aLoad.setLoadType(aLoadType);
            }
            resultList.add(new FefuLoadVO(aLoadType.getId(), aLoadType.getTitle(), aLoad));
            String fefuLoadTypeCode = loadTypesMap.get(aLoadTypeCode);
            if (!StringUtils.isEmpty(fefuLoadTypeCode))
            {
                FefuLoadType fefuLoadType = fefuLoadTypeMap.get(fefuLoadTypeCode);
                if (null != fefuLoadType)
                {
                    FefuEppRegistryModuleALoadExt fefuLoad = fefuLoadMap.get(loadTypesMap.get(aLoadTypeCode));
                    if (createNewIfLoadNotExist && null == fefuLoad)
                    {
                        fefuLoad = new FefuEppRegistryModuleALoadExt();
                        fefuLoad.setModule(module);
                        fefuLoad.setLoadType(fefuLoadType);
                    }
                    resultList.add(new FefuLoadVO(fefuLoadType.getId(), fefuLoadType.getTitle(), fefuLoad));
                }
            }
        }
        FefuLoadType examFefuLoadType = fefuLoadTypeMap.get(FefuLoadTypeCodes.TYPE_EXAM_HOURS);
        if (null != examFefuLoadType)
        {
            FefuEppRegistryModuleALoadExt fefuExamLoad = fefuLoadMap.get(FefuLoadTypeCodes.TYPE_EXAM_HOURS);
            if (createNewIfLoadNotExist && null == fefuExamLoad)
            {
                fefuExamLoad = new FefuEppRegistryModuleALoadExt();
                fefuExamLoad.setModule(module);
                fefuExamLoad.setLoadType(examFefuLoadType);
            }
            resultList.add(new FefuLoadVO(examFefuLoadType.getId(), examFefuLoadType.getTitle(), fefuExamLoad));
        }

        FefuEppRegistryModuleELoad eppRegistryModuleELoad = eppELoadMap.get(EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);
        if (createNewIfLoadNotExist && null == eppRegistryModuleELoad)
        {
            eppRegistryModuleELoad = new FefuEppRegistryModuleELoad();
            eppRegistryModuleELoad.setModule(module);
            eppRegistryModuleELoad.setLoadType(selfWorkLoadType);
        }
        resultList.add(new FefuLoadVO(selfWorkLoadType.getId(), selfWorkLoadType.getTitle(), eppRegistryModuleELoad));

        return resultList;
    }

    @Transactional(readOnly = true)
    @Override
    public List<FefuLoadVO> moduleLoadListReadOnly(Long moduleId)
    {
        return moduleLoadList(moduleId, false);
    }

    @Override
    public void createOrUpdateLoadList(List<FefuLoadVO> fefuLoadVOs)
    {
        for (FefuLoadVO fefuLoadVO : fefuLoadVOs)
        {
            if (null != fefuLoadVO.getALoad())
                saveOrUpdate(fefuLoadVO.getALoad());
            else if (null != fefuLoadVO.getFefuLoad())
                saveOrUpdate(fefuLoadVO.getFefuLoad());
            else if (null != fefuLoadVO.getELoad())
                saveOrUpdate(fefuLoadVO.getELoad());
        }
    }

    public void correctionSizeAndLabor(EppRegistryElement element)
    {
        correctionSizeAndLabor(element, Lists.<EppRegistryElementPart>newArrayList());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void correctionSizeAndLabor(EppRegistryElement element, List<EppRegistryElementPart> ignoreParts)
    {
        Long id = element.getId();
        IEppRegElWrapper elementWrapper = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(id)).get(id);
        if (null == elementWrapper) return;

        List<String> eppALoadTypes = Lists.newArrayList();
        eppALoadTypes.add(EppALoadTypeCodes.TYPE_LECTURES);
        eppALoadTypes.add(EppALoadTypeCodes.TYPE_PRACTICE);
        eppALoadTypes.add(EppALoadTypeCodes.TYPE_LABS);

        List<String> eppELoadTypes = Lists.newArrayList();
        eppELoadTypes.add(EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);

        List<String> eppFefuExamsTypes = Lists.newArrayList();
        eppFefuExamsTypes.add(FefuLoadTypeCodes.TYPE_EXAM_HOURS);

        IDataSettings dataSettings = DataSettingsFacade.getSettings(FefuSettingsParametersLaborUI.DATA_SETTINGS_NAME);
        Long zet = dataSettings.get(FefuSettingsParametersLaborUI.DS_ZET);

        long totalSize = 0;
        long totalLabor = 0;

        for (IEppRegElPartWrapper partWrapper : elementWrapper.getPartMap().values())
        {
            long size = 0;
            EppRegistryElementPart elementPart = partWrapper.getItem();
            if (ignoreParts.contains(elementPart)) continue;

            for (IEppRegElPartModuleWrapper moduleWrapper : partWrapper.getModuleMap().values())
            {
                EppRegistryElementPartModule item = moduleWrapper.getItem();
                EppRegistryModule module = item.getModule();

                List<FefuLoadVO> fefuLoadVOs = FefuRegistryManager.instance().dao().moduleLoadList(module.getId(), true);
                for (FefuLoadVO fefuLoadVO : fefuLoadVOs)
                {
                    EppRegistryModuleALoad aLoad = fefuLoadVO.getALoad();
                    FefuEppRegistryModuleELoad eLoad = fefuLoadVO.getELoad();
                    FefuEppRegistryModuleALoadExt fefuLoad = fefuLoadVO.getFefuLoad();

                    if (null != aLoad)
                    {
                        String code = aLoad.getLoadType().getCode();
                        size = eppALoadTypes.contains(code) ? size + aLoad.getLoad() : size;
                    } else if (null != eLoad)
                    {
                        String code = eLoad.getLoadType().getCode();
                        size = eppELoadTypes.contains(code) ? size + eLoad.getLoad() : size;
                    } else if (null != fefuLoad)
                    {
                        String code = fefuLoad.getLoadType().getCode();
                        size = eppFefuExamsTypes.contains(code) ? size + fefuLoad.getLoad() : size;
                    }
                }
            }
            long labor = zet == null || zet == 0 ? 0 : size / zet;

            elementPart.setSize(size);
            elementPart.setLabor(labor);
            saveOrUpdate(elementPart);

            totalSize += size;
            totalLabor += labor;
        }

        element.setSize(totalSize);
        element.setLabor(totalLabor);
        saveOrUpdate(element);

        Map<String, EppLoadType> loadTypeMap = EppLoadTypeUtils.getLoadTypeMap();
        Map<String, EppRegistryElementLoad> loadMap = IEppRegistryDAO.instance.get().getRegistryElementTotalLoadMap(Collections.singleton(id)).get(id);
        EppELoadType selfWorkLoadType = DataAccessServices.dao().get(EppELoadType.class, EppELoadType.code(), EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);

        if (loadMap.isEmpty())
        {
            for (EppLoadType loadType : loadTypeMap.values())
            {
                String code = loadType.getFullCode();
                EppRegistryElementLoad elementLoad = new EppRegistryElementLoad();

                double value = getRegistryElementTotalLoad(elementWrapper, code, selfWorkLoadType.getId(), loadType.getId(), ignoreParts);
                elementLoad.setLoadAsDouble(value);
                loadMap.put(code, elementLoad);
            }
            Map<Long, Map<String, EppRegistryElementLoad>> disciplineLoadMap = IEppRegistryDAO.instance.get().getRegistryElementTotalLoadMap(Collections.singleton(id));
            disciplineLoadMap.put(id, loadMap);
            IEppRegistryDAO.instance.get().saveRegistryElementTotalLoadMap(disciplineLoadMap);
        } else
        {
            for (EppLoadType loadType : loadTypeMap.values())
            {
                String code = loadType.getFullCode();
                EppRegistryElementLoad elementLoad = loadMap.get(code);
                if (null == elementLoad) continue;

                double value = getRegistryElementTotalLoad(elementWrapper, code, selfWorkLoadType.getId(), elementLoad.getLoadType().getId(), ignoreParts);
                elementLoad.setLoadAsDouble(value);
                saveOrUpdate(elementLoad);
            }
        }
    }

    private double getRegistryElementTotalLoad(IEppRegElWrapper elementWrapper, String code, Long selfWorkLoadTypeId, Long loadTypeId, List<EppRegistryElementPart> ignoreParts)
    {

        if (loadTypeId.equals(selfWorkLoadTypeId))
        {
            double value = 0;
            for (IEppRegElPartWrapper partWrapper : elementWrapper.getPartMap().values())
            {
                if (null != partWrapper)
                {
                    for (IEppRegElPartModuleWrapper moduleWrapper : partWrapper.getModuleMap().values())
                    {
                        EppRegistryElementPartModule partModule = moduleWrapper.getItem();
                        if (ignoreParts.contains(partModule.getPart())) continue;

                        Long moduleId = partModule.getModule().getId();
                        List<FefuLoadVO> fefuLoadVOs = FefuRegistryManager.instance().dao().moduleLoadListReadOnly(moduleId);
                        for (FefuLoadVO fefuLoadVO : fefuLoadVOs)
                        {
                            FefuEppRegistryModuleELoad eLoad = fefuLoadVO.getELoad();
                            if (null != eLoad && loadTypeId.equals(eLoad.getLoadType().getId()))
                            {
                                long load = eLoad.getLoad();
                                value += load < 0 ? 0 : UniEppUtils.wrap(load);
                            }
                        }
                    }
                }
            }
            return value;
        }

        return elementWrapper.getChildrenLoadAsDouble(code);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void doMovePartModule(StaticListDataSource<RowWrapper> dataSource, EppRegistryElement element, final Long id, int direction)
    {
        if (null == id) return;
        try
        {
            Session session = getSession();
            NamedSyncInTransactionCheckLocker.register(session, String.valueOf(element.getId()));

            final PlaneTree flatTree = new PlaneTree((List) dataSource.getRowList());
            final List<RowWrapper> items = (List) flatTree.getFlatTreeObjectsHierarchicalSorted("");

            final RowWrapper row0 = (RowWrapper) CollectionUtils.find(items, object -> id.equals(((RowWrapper) object).getId()));

            if (!(row0.getEntity().getItem() instanceof EppRegistryElementPartModule)) return;
            final EppRegistryElementPartModule m0 = (EppRegistryElementPartModule) row0.getEntity().getItem();

            final RowWrapper row1 = items.get(Math.min(Math.max(0, items.indexOf(row0) + direction), items.size() - 1));
            if (row1.getId().equals(row0.getId())) return;

            if (row1.getEntity().getItem() instanceof EppRegistryElementPartModule)
            {
                final EppRegistryElementPartModule m1 = (EppRegistryElementPartModule) row1.getEntity().getItem();
                if (m0.getPart().equals(m1.getPart()))
                {
                    final int n0 = m0.getNumber(), n1 = m1.getNumber();
                    m0.setNumber(-1);
                    m1.setNumber(-2);
                    session.flush();
                    m0.setNumber(n1);
                    m1.setNumber(n0);
                    session.flush();
                    session.clear();
                }
            } else if (row1.getEntity().getItem() instanceof EppRegistryElementPart)
            {
                EppRegistryElementPart p1 = (EppRegistryElementPart) row1.getEntity().getItem();
                if (direction < 0)
                {
                    // надо взять предыдущую часть
                    p1 = new DQLSelectBuilder()
                            .fromEntity(EppRegistryElementPart.class, "x")
                            .where(eq(property(EppRegistryElementPart.registryElement().fromAlias("x")), value(element)))
                            .where(lt(property(EppRegistryElementPart.number().fromAlias("x")), value(p1.getNumber())))
                            .order(property(EppRegistryElementPart.number().fromAlias("x")), OrderDirection.desc)
                            .createStatement(session).setMaxResults(1).uniqueResult();
                    if (null == p1) return;
                }

                if (p1.equals(m0.getPart())) return;

                new DQLUpdateBuilder(EppRegistryElementPartModule.class)
                        .set(EppRegistryElementPartModule.L_PART, value(p1))
                        .set(EppRegistryElementPartModule.P_NUMBER, value(direction < 0 ? Integer.MAX_VALUE : Integer.MIN_VALUE))
                        .where(eq(property("id"), value(m0.getId())))
                        .createStatement(session).execute();
                session.clear();
            }
        } finally
        {
            IEppRegistryDAO.instance.get().doRecalculateRegistryElementNumbers(
                    DataAccessServices.dao().get(EppRegistryElement.class, element.getId())
            );
        }
    }

    @Override
    public EduProgramKind getEduProgramKind(EppRegistryElement element)
    {
        if (null == element) return null;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuRegistryElement2EduProgramKindRel.class, "r").top(1)
                .column(property("r", FefuRegistryElement2EduProgramKindRel.eduProgramSubject().subjectIndex().programKind()))
                .where(eq(property("r", FefuRegistryElement2EduProgramKindRel.registryElement()), value(element)));

        return builder.createStatement(getSession()).uniqueResult();
    }

    protected AbstractColumn column_action(final EppControlActionType ca)
    {
        return new SafeSimpleColumn(ca.getShortTitle(), ca.getFullCode())
        {
            @Override
            public String getContent(final IEntity entity)
            {
                final int sz = ((RowWrapper) entity).getEntity().getActionSize(ca.getFullCode());
                return (sz > 0 ? String.valueOf(sz) : "");
            }
        }.setVerticalHeader(true).setHeaderAlign("center").setAlign("right").setWidth(1).setClickable(false).setOrderable(false);
    }

    private AbstractColumn column_e_load(final EppELoadType typeTotalSelfwork, final Map<Long, Map<String, Number>> eLoadMap)
    {
        return new SafeSimpleColumn(typeTotalSelfwork.getShortTitle(), typeTotalSelfwork.getCode())
        {
            @Override
            public boolean isRawContent()
            {
                return true;
            }

            @Override
            public String getContent(final IEntity entity)
            {
                final IEppBaseRegElWrapper e = ((RowWrapper) entity).getEntity();
                if (e instanceof IEppRegElPartModuleWrapper)
                {
                    IEppRegElPartModuleWrapper moduleWrapper = (IEppRegElPartModuleWrapper) e;
                    if (null != moduleWrapper.getItem().getModule() &&
                            null != eLoadMap.get(moduleWrapper.getItem().getModule().getId()) &&
                            null != eLoadMap.get(moduleWrapper.getItem().getModule().getId()).get(typeTotalSelfwork.getCode()))
                    {
                        final Double lx = (Double) eLoadMap.get(moduleWrapper.getItem().getModule().getId()).get(typeTotalSelfwork.getCode());
                        return UniEppUtils.formatLoad(lx, true);
                    } else
                    {
                        return null;
                    }
                } else if (e instanceof IEppRegElPartWrapper)
                {
                    final Double lx = getELoadForRegElPart((IEppRegElPartWrapper) e, typeTotalSelfwork, eLoadMap);
                    return UniEppUtils.formatLoad(lx, true);
                } else if (e instanceof IEppRegElWrapper)
                {
                    EppRegistryElementLoad selfWork = DataAccessServices.dao().getByNaturalId(new EppRegistryElementLoadGen.NaturalId(((IEppRegElWrapper) e).getItem(), typeTotalSelfwork));
                    final Double lx = getELoadForRegElement((IEppRegElWrapper) e, typeTotalSelfwork, eLoadMap);

                    if (null != selfWork)
                    {
                        final Double sx = selfWork.getLoadAsDouble();
                        if (!UniEppUtils.eq(null == lx ? 0 : lx, sx))
                        {
                            return UniEppUtils.error(UniEppUtils.formatLoad(sx, false), UniEppUtils.formatLoad(lx, false));
                        } else
                            return UniEppUtils.formatLoad(lx, true);
                    } else return UniEppUtils.formatLoad(lx, true);
                } else return null;
            }
        }.setVerticalHeader(true).setHeaderAlign("center").setAlign("right").setWidth(1).setClickable(false).setOrderable(false);
    }

    private Map<Long, Map<String, Number>> getELoadMap(IEppRegElWrapper elementWrapper, EppELoadType selfWorkLoadType)
    {
        List<Long> modulesIds = Lists.newArrayList();
        for (Map.Entry<Integer, IEppRegElPartWrapper> entry : elementWrapper.getPartMap().entrySet())
        {
            if (null != entry.getValue())
            {
                for (Map.Entry<Integer, IEppRegElPartModuleWrapper> pMEntry : entry.getValue().getModuleMap().entrySet())
                {
                    if (null != pMEntry.getValue())
                        modulesIds.add(pMEntry.getValue().getItem().getModule().getId());
                }
            }
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuEppRegistryModuleELoad.class, "l");
        builder.where(eq(property("l", FefuEppRegistryModuleELoad.loadType().id()), value(selfWorkLoadType.getId())));
        builder.where(in(property("l", FefuEppRegistryModuleELoad.module().id()), modulesIds));

        Map<Long, Map<String, Number>> eLoadMap = Maps.newHashMap();
        for (FefuEppRegistryModuleELoad rowPartLoad : createStatement(builder).<FefuEppRegistryModuleELoad>list())
        {
            Long moduleId = rowPartLoad.getModule().getId();
            String code = rowPartLoad.getLoadType().getCode();

            if (!eLoadMap.containsKey(moduleId))
                eLoadMap.put(moduleId, Maps.<String, Number>newHashMap());
            if (!eLoadMap.get(moduleId).containsKey(code))
                eLoadMap.get(moduleId).put(code, rowPartLoad.getLoadAsDouble());
        }
        return eLoadMap;
    }

    private AbstractColumn column_fefu_load(final FefuLoadType fefuLoadType, final Map<Long, Map<String, Number>> interactiveLoadMap)
    {
        return new SafeSimpleColumn(fefuLoadType.getShortTitle(), fefuLoadType.getCode())
        {
            @Override
            public boolean isRawContent()
            {
                return true;
            }

            @Override
            public String getContent(final IEntity entity)
            {
                final IEppBaseRegElWrapper e = ((RowWrapper) entity).getEntity();
                if (e instanceof IEppRegElPartModuleWrapper)
                {
                    IEppRegElPartModuleWrapper moduleWrapper = (IEppRegElPartModuleWrapper) e;
                    if (null != moduleWrapper.getItem().getModule() &&
                            null != interactiveLoadMap.get(moduleWrapper.getItem().getModule().getId()) &&
                            null != interactiveLoadMap.get(moduleWrapper.getItem().getModule().getId()).get(fefuLoadType.getCode()))
                    {
                        final Double lx = (Double) interactiveLoadMap.get(moduleWrapper.getItem().getModule().getId()).get(fefuLoadType.getCode());
                        return UniEppUtils.formatLoad(lx, true);
                    } else
                    {
                        return null;
                    }
                } else if (e instanceof IEppRegElPartWrapper)
                {
                    final Double lx = getLoadForRegElPart((IEppRegElPartWrapper) e, fefuLoadType, interactiveLoadMap);
                    return UniEppUtils.formatLoad(lx, true);
                } else if (e instanceof IEppRegElWrapper)
                {
                    final Double lx = getLoadForRegElement((IEppRegElWrapper) e, fefuLoadType, interactiveLoadMap);

                    return UniEppUtils.formatLoad(lx, true);
                } else return null;
            }
        }.setVerticalHeader(true).setHeaderAlign("center").setAlign("right").setWidth(1).setClickable(false).setOrderable(false);
    }

    private Double getELoadForRegElement(IEppRegElWrapper e, EppELoadType typeTotalSelfwork, Map<Long, Map<String, Number>> eLoadMap)
    {

        if (null != e.getPartMap())
        {
            List<Long> modulesIds = Lists.newArrayList();
            for (Map.Entry<Integer, IEppRegElPartWrapper> entry : e.getPartMap().entrySet())
            {
                if (null != entry.getValue() && null != entry.getValue().getModuleMap())
                {
                    for (Map.Entry<Integer, IEppRegElPartModuleWrapper> partEntry : entry.getValue().getModuleMap().entrySet())
                    {
                        if (null != entry.getValue() && null != partEntry.getValue().getItem().getModule())
                        {
                            modulesIds.add(partEntry.getValue().getItem().getModule().getId());
                        }
                    }
                }
            }

            String code = typeTotalSelfwork.getCode();
            Double load = null;
            for (Long moduleId : modulesIds)
            {
                if (null != eLoadMap.get(moduleId) && null != eLoadMap.get(moduleId).get(code))
                {
                    if (null == load) load = (Double) eLoadMap.get(moduleId).get(code);
                    else load += (Double) eLoadMap.get(moduleId).get(code);
                }
            }
            return load;
        } else return null;
    }

    private Double getELoadForRegElPart(IEppRegElPartWrapper e, EppELoadType typeTotalSelfwork, Map<Long, Map<String, Number>> eLoadMap)
    {
        if (null != e.getModuleMap())
        {
            List<Long> modulesIds = Lists.newArrayList();
            for (Map.Entry<Integer, IEppRegElPartModuleWrapper> entry : e.getModuleMap().entrySet())
            {
                if (null != entry.getValue() && null != entry.getValue().getItem().getModule())
                {
                    modulesIds.add(entry.getValue().getItem().getModule().getId());
                }
            }

            String code = typeTotalSelfwork.getCode();
            Double load = null;
            for (Long moduleId : modulesIds)
            {
                if (null != eLoadMap.get(moduleId) && null != eLoadMap.get(moduleId).get(code))
                {
                    if (null == load) load = (Double) eLoadMap.get(moduleId).get(code);
                    else load += (Double) eLoadMap.get(moduleId).get(code);
                }
            }
            return load;
        } else return null;
    }


    private Double getLoadForRegElement(IEppRegElWrapper e, FefuLoadType fefuLoadType, Map<Long, Map<String, Number>> interactiveLoadMap)
    {
        if (null != e.getPartMap())
        {
            List<Long> modulesIds = Lists.newArrayList();
            for (Map.Entry<Integer, IEppRegElPartWrapper> entry : e.getPartMap().entrySet())
            {
                if (null != entry.getValue() && null != entry.getValue().getModuleMap())
                {
                    for (Map.Entry<Integer, IEppRegElPartModuleWrapper> partEntry : entry.getValue().getModuleMap().entrySet())
                    {
                        if (null != entry.getValue() && null != partEntry.getValue().getItem().getModule())
                        {
                            modulesIds.add(partEntry.getValue().getItem().getModule().getId());
                        }
                    }
                }
            }

            String code = fefuLoadType.getCode();
            Double load = null;
            for (Long moduleId : modulesIds)
            {
                if (null != interactiveLoadMap.get(moduleId) && null != interactiveLoadMap.get(moduleId).get(code))
                {
                    if (null == load) load = (Double) interactiveLoadMap.get(moduleId).get(code);
                    else load += (Double) interactiveLoadMap.get(moduleId).get(code);
                }
            }
            return load;
        } else return null;
    }

    private Double getLoadForRegElPart(IEppRegElPartWrapper e, FefuLoadType fefuLoadType, Map<Long, Map<String, Number>> interactiveLoadMap)
    {
        if (null != e.getModuleMap())
        {
            List<Long> modulesIds = Lists.newArrayList();
            for (Map.Entry<Integer, IEppRegElPartModuleWrapper> entry : e.getModuleMap().entrySet())
            {
                if (null != entry.getValue() && null != entry.getValue().getItem().getModule())
                {
                    modulesIds.add(entry.getValue().getItem().getModule().getId());
                }
            }

            String code = fefuLoadType.getCode();
            Double load = null;
            for (Long moduleId : modulesIds)
            {
                if (null != interactiveLoadMap.get(moduleId) && null != interactiveLoadMap.get(moduleId).get(code))
                {
                    if (null == load) load = (Double) interactiveLoadMap.get(moduleId).get(code);
                    else load += (Double) interactiveLoadMap.get(moduleId).get(code);
                }
            }
            return load;
        } else return null;
    }

    private Map<Long, Map<String, Number>> getInteractiveLoadMap(IEppRegElWrapper elementWrapper)
    {
        List<Long> modulesIds = Lists.newArrayList();
        for (Map.Entry<Integer, IEppRegElPartWrapper> entry : elementWrapper.getPartMap().entrySet())
        {
            if (null != entry.getValue())
            {
                for (Map.Entry<Integer, IEppRegElPartModuleWrapper> pMEntry : entry.getValue().getModuleMap().entrySet())
                {
                    if (null != pMEntry.getValue())
                        modulesIds.add(pMEntry.getValue().getItem().getModule().getId());
                }
            }
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuEppRegistryModuleALoadExt.class, "l");
        builder.where(in(property("l", FefuEppRegistryModuleALoadExt.module().id()), modulesIds));

        Map<Long, Map<String, Number>> interactiveLoadMap = Maps.newHashMap();
        for (FefuEppRegistryModuleALoadExt rowPartLoad : createStatement(builder).<FefuEppRegistryModuleALoadExt>list())
        {
            Long moduleId = rowPartLoad.getModule().getId();
            String code = rowPartLoad.getLoadType().getCode();

            if (!interactiveLoadMap.containsKey(moduleId))
                interactiveLoadMap.put(moduleId, Maps.<String, Number>newHashMap());
            if (!interactiveLoadMap.get(moduleId).containsKey(code))
                interactiveLoadMap.get(moduleId).put(code, rowPartLoad.getLoadAsDouble());
        }

        return interactiveLoadMap;
    }

    protected Map<String, Double> getLoadMap(final EppRegistryElement regElement)
    {
        final Map<String, EppRegistryElementLoad> tmp = IEppRegistryDAO.instance.get().getRegistryElementTotalLoadMap(Collections.singleton(regElement.getId())).get(regElement.getId());
        final Map<String, Double> loadMap = new HashMap<>(tmp.size());
        for (final Map.Entry<String, EppRegistryElementLoad> e : tmp.entrySet())
        {
            loadMap.put(e.getKey(), e.getValue().getLoadAsDouble());
        }
        loadMap.put(EppLoadType.FULL_CODE_TOTAL_HOURS, regElement.getSizeAsDouble());
        loadMap.put(EppLoadType.FULL_CODE_LABOR, regElement.getLaborAsDouble());
        return loadMap;
    }

    protected AbstractColumn column_load(final EppALoadType load, final Map<String, Double> loadMap)
    {
        return this.column_load(load.getShortTitle(), load.getFullCode(), loadMap);
    }

    protected AbstractColumn column_load(final String title, final String fullCode, final Map<String, Double> loadMap)
    {
        return new SafeSimpleColumn(title, fullCode)
        {
            @Override
            public boolean isRawContent()
            {
                return true;
            }

            @Override
            public String getContent(final IEntity entity)
            {
                final IEppBaseRegElWrapper e = ((RowWrapper) entity).getEntity();
                final double ll = e.getLoadAsDouble(fullCode);

                if (e.getItem() instanceof EppRegistryElement)
                {
                    final Double lx = loadMap.get(fullCode);
                    if (null != lx)
                    {
                        if (!UniEppUtils.eq(lx.doubleValue(), ll))
                        {
                            return UniEppUtils.error(UniEppUtils.formatLoad(lx, false), UniEppUtils.formatLoad(ll, false));
                        }
                    }
                }

                return UniEppUtils.formatLoad(ll, true);
            }
        }.setVerticalHeader(true).setHeaderAlign("center").setAlign("right").setWidth(1).setClickable(false).setOrderable(false);
    }

    public void saveRegistryElementTotalLoadMap(final Map<Long, Map<String, SppRegElementLoadCheck>> newMap)
    {
        validateSppRegElementLoadCheckMap(newMap);
        Session session = getSession();
        NamedSyncInTransactionCheckLocker.register(session, "SppRegElementLoadCheckMap");

        final Map<Long, Map<String, SppRegElementLoadCheck>> currentMap = this.getRegistryElementTotalLoadMap(newMap.keySet());

        // обновляем существующие
        for (final Map.Entry<Long, Map<String, SppRegElementLoadCheck>> currentRegElEntry : currentMap.entrySet())
        {
            final Map<String, SppRegElementLoadCheck> currentRegElMap = currentRegElEntry.getValue();
            final Map<String, SppRegElementLoadCheck> newRegElMap = newMap.remove(currentRegElEntry.getKey());

            for (final Map.Entry<String, SppRegElementLoadCheck> newRegElLoadCheckEntry : newRegElMap.entrySet())
            {
                final SppRegElementLoadCheck newLoad = newRegElLoadCheckEntry.getValue();
                if (null == newLoad)
                {
                    continue; /* no-value */
                }

                SppRegElementLoadCheck dbLoad = currentRegElMap.remove(newRegElLoadCheckEntry.getKey());
                if (null == dbLoad)
                {
                    dbLoad = new SppRegElementLoadCheck();
                }
                dbLoad.update(newLoad);
                session.saveOrUpdate(dbLoad);
            }

            for (final Map.Entry<String, SppRegElementLoadCheck> currentRegElLoadCheckEntry : currentRegElMap.entrySet())
            {
                session.delete(currentRegElLoadCheckEntry.getValue());
            }
        }

        // все удаления должны попасть в базу ДО добавлений
        session.flush();

        // создаем новые
        for (final Map.Entry<Long, Map<String, SppRegElementLoadCheck>> newRegElEntry : newMap.entrySet())
        {
            final Map<String, SppRegElementLoadCheck> newRegElMap = newRegElEntry.getValue();
            for (final Map.Entry<String, SppRegElementLoadCheck> newRegElLoadEntry : newRegElMap.entrySet())
            {

                final SppRegElementLoadCheck load = new SppRegElementLoadCheck();
                load.update(newRegElLoadEntry.getValue());
                session.saveOrUpdate(load);
            }
        }

        session.flush();
    }

    public void saveRegistryElExt(SppRegElementExt newExt)
    {
        Session session = getSession();
        NamedSyncInTransactionCheckLocker.register(session, "SppRegElExt");
        session.saveOrUpdate(newExt);
        session.flush();
    }

    protected void validateSppRegElementLoadCheckMap(final Map<Long, Map<String, SppRegElementLoadCheck>> loadMap)
    {
        final List<EppALoadType> loadTypes = DataAccessServices.dao().getList(EppALoadType.class);
        Session session = getSession();

        for (final Map.Entry<Long, Map<String, SppRegElementLoadCheck>> regElEntry : loadMap.entrySet())
        {
            final EppRegistryElement regEl = (EppRegistryElement) session.load(EppRegistryElement.class, regElEntry.getKey());
            for (final EppALoadType loadType : loadTypes)
            {
                final SppRegElementLoadCheck load = regElEntry.getValue().get(loadType.getFullCode());
                if ((null != load) && (null == load.getId()))
                {
                    load.setEppALoadType(loadType);
                    load.setRegistryElement(regEl);
                }
            }

            for (final Map.Entry<String, SppRegElementLoadCheck> regElLoadEntry : regElEntry.getValue().entrySet())
            {
                final SppRegElementLoadCheck newLoad = regElLoadEntry.getValue();
                if (null == newLoad)
                {
                    continue; /* no-value */
                }

                final String code = newLoad.getEppALoadType().getFullCode();
                if (!regElLoadEntry.getKey().equals(code))
                {
                    throw new IllegalArgumentException("SppRegElementLoadCheck(" + newLoad.getId() + ").load(" + code + ") != " + regElLoadEntry.getKey());
                }

                final Long regElId = newLoad.getRegistryElement().getId();
                if (!regElEntry.getKey().equals(regElId))
                {
                    throw new IllegalArgumentException("SppRegElementLoadCheck(" + newLoad.getId() + ").dsc(" + regElId + ") != " + regElEntry.getKey());
                }
            }
        }
    }

    public Map<Long, Map<String, SppRegElementLoadCheck>> getRegistryElementTotalLoadMap(Collection<Long> disciplineIds)
    {
        final Map<Long, Map<String, SppRegElementLoadCheck>> result = SafeMap.get(HashMap.class);
        if (disciplineIds != null)
        {
            for (List<Long> ids : Iterables.partition(disciplineIds, DQL.MAX_VALUES_ROW_NUMBER)) {
                List<SppRegElementLoadCheck> checkList = getList(SppRegElementLoadCheck.class, SppRegElementLoadCheck.registryElement().id(), ids);
                for (SppRegElementLoadCheck load : checkList) {
                    result.get(load.getRegistryElement().getId()).put(load.getEppALoadType().getFullCode(), load);
                }
            }
        }
        return result;
    }

    @Override
    public void setOwner4ChildModulesOfRegElement(EppRegistryElement element, OrgUnit owner)
    {
        OrgUnit newOwner = element.getOwner();
        if (owner != null && !owner.getId().equals(newOwner.getId()))
        {
            String pm_alias = "pm";
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(EppRegistryElementPartModule.class, pm_alias)
                    .column(property(pm_alias, EppRegistryElementPartModule.module()))
                    .where(eq(property(pm_alias, EppRegistryElementPartModule.module().owner()), value(owner)))
                    .where(eq(property(pm_alias, EppRegistryElementPartModule.part().registryElement()), value(element)));

            List<EppRegistryModule> modules = getList(builder);
            Session session = getSession();
            modules.forEach(module -> {
                module.setOwner(newOwner);
                session.update(module);
            });
        }
    }
}
