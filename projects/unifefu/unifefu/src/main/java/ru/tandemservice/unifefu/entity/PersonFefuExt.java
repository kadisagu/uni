package ru.tandemservice.unifefu.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.gen.PersonFefuExtGen;

import java.util.Date;

/**
 * Перосна (расширение ДВФУ)
 */
public class PersonFefuExt extends PersonFefuExtGen
{
    public PersonFefuExt()
    {

    }

    public PersonFefuExt(Person person)
    {
        setPerson(person);
    }

    public PersonFefuExt(Person person, Date vipDate)
    {
        setPerson(person);
        setVipDate(vipDate);
    }

    /**
     * @return да (<code>vipDate</code>)
     */
    public String getPubFormatVipDate()
    {
        return "да (с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getVipDate()) + ")";
    }
}