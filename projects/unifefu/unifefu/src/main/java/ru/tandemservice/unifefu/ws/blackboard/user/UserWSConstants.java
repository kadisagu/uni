/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.user;

/**
 * @author Nikolay Fedorovskih
 * @since 28.01.2014
 */
public class UserWSConstants
{
    // Filter types
    // called by getUser
    public static final int GET_ALL_USERS_WITH_AVAILABILITY = 1;
    public static final int GET_USER_BY_ID_WITH_AVAILABILITY = 2;
    public static final int GET_USER_BY_BATCH_ID_WITH_AVAILABILITY = 3;
    public static final int GET_USER_BY_COURSE_ID_WITH_AVAILABILITY = 4;
    public static final int GET_USER_BY_GROUP_ID_WITH_AVAILABILITY = 5;
    public static final int GET_USER_BY_NAME_WITH_AVAILABILITY = 6;
    public static final int GET_USER_BY_SYSTEM_ROLE = 7;
    // called by getAddressBookEntry
    public static final int GET_ADDRESS_BOOK_ENTRY_BY_ID = 8;
    public static final int GET_ADDRESS_BOOK_ENTRY_BY_CURRENT_USERID = 9;

    // Errors
    public static final String USERWS001 = "User.WS001"; // Failed to save user
    public static final String USERWS002 = "User.WS002"; // Failed to save an address book entry
    public static final String USERWS003 = "User.WS003"; // Failed to save a observer association
    public static final String USERWS004 = "User.WS004"; // Failed to delete a user
    public static final String USERWS005 = "User.WS005"; // Failed to delete a user by its portal role
    public static final String USERWS006 = "User.WS006"; // Failed to delete a user by portal/institution role
    public static final String USERWS007 = "User.WS007"; // Failed to delete an address book entry
    public static final String USERWS008 = "User.WS008"; // Failed to delete an observer association
    public static final String USERWS009 = "User.WS009"; // Failed to load users
    public static final String USERWS010 = "User.WS010"; // Failed to load address book entries
    public static final String USERWS011 = "User.WS011"; // Failed to load observes
    public static final String USERWS012 = "User.WS012"; // Failed to load the user in session or tool login does have required privilege
    public static final String USERWS013 = "User.WS013"; // Failed to create the user since required fields are not set
    public static final String USERWS014 = "User.WS014"; // Failed to load the user portal roles
    public static final String USERWS016 = "User.WS016"; // Failed to load the portal roles in the system
}