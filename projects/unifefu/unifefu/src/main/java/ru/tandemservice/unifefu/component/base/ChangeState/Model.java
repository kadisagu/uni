package ru.tandemservice.unifefu.component.base.ChangeState;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author ilunin
 * @since 15.10.2014
 */
@Input({@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id")})
public class Model extends ru.tandemservice.uniepp.component.base.ChangeState.Model
{
    public boolean isEduPlanVersion(){
        return getStateObject() instanceof EppEduPlanVersion;
    }

    public boolean isDeclineTransition()
    {
        return getTransition().getCode().equals(EppState.STATE_REJECTED);
    }

}
