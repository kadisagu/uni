/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu6.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuChangeFioStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 03.09.2012
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuChangeFioStuExtract, Model>
{
}