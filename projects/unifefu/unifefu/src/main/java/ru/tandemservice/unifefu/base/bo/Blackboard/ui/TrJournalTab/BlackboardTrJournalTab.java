/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.TrJournalTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.BBCourseDSHandler;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;

/**
 * @author Nikolay Fedorovskih
 * @since 20.03.2014
 */
@Configuration
public class BlackboardTrJournalTab extends BusinessComponentManager
{
    public static final String COURSE_DS = "courseDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(COURSE_DS, getCourseDS(), courseDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getCourseDS()
    {
        return columnListExtPointBuilder(COURSE_DS)
                .addColumn(textColumn(BbCourse.P_TITLE, BbCourse.P_TITLE).order())
                .addColumn(textColumn(BbCourse.P_BB_COURSE_ID, BbCourse.P_BB_COURSE_ID).order())
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("delete.alert", BbCourse.P_BB_COURSE_ID)))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return new BBCourseDSHandler(getName(), false);
    }
}