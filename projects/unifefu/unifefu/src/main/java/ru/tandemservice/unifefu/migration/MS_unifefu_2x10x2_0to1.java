package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x10x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuAdmitToDiplomaStuListExtract

		// создано обязательное свойство notNeedAdmissionToGIA
		{
			// создать колонку
			tool.createColumn("ffadmttdplmstlstextrct_t", new DBColumn("notneedadmissiontogia_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultNotNeedAdmissionToGIA = false;
			tool.executeUpdate("update ffadmttdplmstlstextrct_t set notneedadmissiontogia_p=? where notneedadmissiontogia_p is null", defaultNotNeedAdmissionToGIA);

			// сделать колонку NOT NULL
			tool.setColumnNullable("ffadmttdplmstlstextrct_t", "notneedadmissiontogia_p", false);

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuAdmitToStateExamsStuListExtract

		// создано обязательное свойство notNeedAdmissionToGIA
		{
			// создать колонку
			tool.createColumn("ffadmttsttexmsstlstextrct_t", new DBColumn("notneedadmissiontogia_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultNotNeedAdmissionToGIA = false;
			tool.executeUpdate("update ffadmttsttexmsstlstextrct_t set notneedadmissiontogia_p=? where notneedadmissiontogia_p is null", defaultNotNeedAdmissionToGIA);

			// сделать колонку NOT NULL
			tool.setColumnNullable("ffadmttsttexmsstlstextrct_t", "notneedadmissiontogia_p", false);

		}


    }
}