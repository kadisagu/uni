/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSchedule.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt;
import ru.tandemservice.unispp.base.entity.SppSchedulePrintForm;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 9/27/13
 */
public class FefuSchedulePrintFormDSHandler extends DefaultSearchDataSourceHandler
{
    public final static String PROPERTY_GROUPS = "groups";
    public final static String PROPERTY_CREATE_DATE = "createDate";
    public final static String PROPERTY_SEASON = "season";

    public FefuSchedulePrintFormDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long orgUnitId = context.getNotNull("orgUnitId");
        Long adminId = context.get("admin");
        List<Long> groups = context.get("groups");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SchedulePrintFormFefuExt.class, "pf");
        builder.joinPath(DQLJoinType.inner, SchedulePrintFormFefuExt.sppSchedulePrintForm().fromAlias("pf"), "spf");
        builder.where(eq(property("spf", SppSchedulePrintForm.createOU().id()), value(orgUnitId)));
        builder.column("pf");
        if (null != adminId)
            builder.where(eq(property("pf", SchedulePrintFormFefuExt.admin().id()), value(adminId)));
        if (null != groups && !groups.isEmpty())
        {
            DQLSelectBuilder groupBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
            groupBuilder.column(DQLFunctions.upper(property("g", Group.title())));
            groupBuilder.where(in(property("g", Group.id()), groups));
            List<String> groupsTitles = createStatement(groupBuilder).list();
            List<IDQLExpression> expressions = groupsTitles.stream().map(groupTitle -> like(DQLFunctions.upper(property("spf", SppSchedulePrintForm.groups())), value(CoreStringUtils.escapeLike(groupTitle, true)))).collect(Collectors.toList());
            builder.where(or(expressions.toArray(new IDQLExpression[]{})));
        }

        //return DQLSelectOutputBuilder.get(input, builder, getSession()).order().build();
        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            SchedulePrintFormFefuExt printFormFefuExt = wrapper.getWrapped();
            SppSchedulePrintForm printForm = printFormFefuExt.getSppSchedulePrintForm();
            wrapper.setProperty(PROPERTY_GROUPS, printForm.getGroups());
            wrapper.setProperty(PROPERTY_CREATE_DATE, printForm.getCreateDate());
            wrapper.setProperty(PROPERTY_SEASON, printForm.getSeason().getTitleWithTime());
        }

        return output;
    }
}
