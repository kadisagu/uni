/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.logic;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppBaseRegElWrapper;

/**
 * @author nvankov
 * @since 2/6/14
 */
public class RowWrapper extends ViewWrapper<IEppBaseRegElWrapper>
{
    private final RowWrapper parent;
    private final boolean edit;

    @Override
    public RowWrapper getHierarhyParent() { return this.parent; }

    public RowWrapper(final IEppBaseRegElWrapper i, final RowWrapper parent, boolean edit)
    {
        super(i);
        this.parent = parent;
        this.edit = edit;
    }

    public boolean isEdit()
    {
        return edit;
    }
}