/* $Id: EppExtViewProvider4ElementPart.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.report.externalView;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Victor Nekrasov
 * @since 23.04.2014
 */

public class EppExtViewProvider4ElementPart extends SimpleDQLExternalViewConfig
{
	@Override
	protected DQLSelectBuilder buildDqlQuery()
	{
		DQLSelectBuilder dql = new DQLSelectBuilder()
				.fromEntity(EppRegistryElementPart.class, "elementPart")
				.joinPath(DQLJoinType.left, EppRegistryElementPart.registryElement().fromAlias("elementPart"), "regElement")
				.joinEntity("elementPart", DQLJoinType.left, EppRegistryElementPartFControlAction.class, "cAction",
				            eq(property("elementPart"), property(EppRegistryElementPartFControlAction.part().fromAlias("cAction"))));

		column(dql, property("elementPart", EppRegistryElementPart.id()), "partId").comment("ID части");
		column(dql, property("elementPart", EppRegistryElementPart.number()), "partNumber").comment("Порядковый номер части");
		column(dql, property("regElement", EppRegistryElement.id()), "elementId").comment("ID реестровой записи");

		column(dql, property(EppRegistryElementPart.labor().fromAlias("elementPart")), "labor").comment("Трудоемкость");
		column(dql, property(EppRegistryElementPart.size().fromAlias("elementPart")), "partSize").comment("Часов (всего)");


		column(dql, property("cAction", EppRegistryElementPartFControlAction.controlAction().code()), "actionCode").comment("код формы контроля");
		column(dql, property("cAction", EppRegistryElementPartFControlAction.controlAction().title()), "actionTitle").comment("наименование формы контроля");

		return dql;
	}
}

