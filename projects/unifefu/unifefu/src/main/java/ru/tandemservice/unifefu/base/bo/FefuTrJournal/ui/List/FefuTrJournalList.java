package ru.tandemservice.unifefu.base.bo.FefuTrJournal.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.base.bo.UniOrgUnit.logic.OrgUnitByKindComboDataSourceHandler;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.unifefu.base.bo.FefuTrJournal.FefuTrJournalManager;
import ru.tandemservice.unifefu.base.bo.FefuTrJournal.logic.EducationLevelHighSchoolDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuTrJournal.logic.FefuTrJournalDSHandler;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic.TrOrgUnitJournalDSHandler;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

import java.util.Arrays;

/**
 * @author amakarova
 */
@Configuration
public class FefuTrJournalList extends BusinessComponentManager
{
    public static final String TR_ORGUNIT_JOURNAL_DS = "trOrgUnitJournalDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL_DS = "educationLevelHighSchoolDS";
    public static final String YES_DS = "yesDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(FefuTrJournalList.TR_ORGUNIT_JOURNAL_DS, trOrgUnitJournalDSColumns(), trJournalDSHandler()))
                .addDataSource(FefuTrJournalManager.instance().trOrgUnitDSConfig())
                .addDataSource(FefuTrJournalManager.instance().trOrgUnitYearPartDSConfig())
                .addDataSource(FefuTrJournalManager.instance().trOrgUnitActivityElementPartDSConfig())
                .addDataSource(EppStateManager.instance().eppStateDSConfig())
                .addDataSource(FefuTrJournalManager.instance().trOrgUnitPpsDSConfig())
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitComboDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
                .addDataSource(selectDS(EDUCATION_LEVEL_HIGH_SCHOOL_DS, educationLevelHighSchoolDSHandler()).addColumn(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY))
                .addDataSource(selectDS(YES_DS, yesDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint trOrgUnitJournalDSColumns()
    {
        return this.columnListExtPointBuilder(FefuTrJournalList.TR_ORGUNIT_JOURNAL_DS)
                .addColumn(textColumn(TrJournal.P_NUMBER, TrJournal.number()).order().width("1").create())
                .addColumn(publisherColumn(TrJournal.L_REGISTRY_ELEMENT_PART, TrJournal.registryElementPart().titleWithNumber()).order().create())
                .addColumn(textColumn("durationPeriod", TrJournal.durationPeriodString()).order().width("1").create())
                .addColumn(textColumn("pps", TrOrgUnitJournalDSHandler.COLUMN_PPS).formatter(UniEppUtils.NEW_LINE_FORMATTER).create())
                .addColumn(textColumn(TrJournal.L_RESPONSIBLE, TrJournal.responsible().shortTitle()).create())
                .addColumn(textColumn("groups", FefuTrJournalDSHandler.COLUMN_UGS).formatter(UniEppUtils.NEW_LINE_FORMATTER).create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).create())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trJournalDSHandler()
    {
        return new FefuTrJournalDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitComboDSHandler()
    {
        return new OrgUnitByKindComboDataSourceHandler(getName(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationLevelHighSchoolDSHandler()
    {
        return new EducationLevelHighSchoolDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> yesDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(FefuTrJournalDSHandler.YES_ITEM));
    }
}
