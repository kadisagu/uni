/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.FefuTechnicalCommissionsDataManager;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.List.FefuTechnicalCommissionsList;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

/**
 * @author Nikolay Fedorovskih
 * @since 11.06.2013
 */
@Input({
               @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "technicalCommissionId"),
               @Bind(key = FefuTechnicalCommissionsList.ENROLLMENT_CAMPAIGN_PARAM, binding = "enrollmentCampaignId"),
       })
public class FefuTechnicalCommissionsAddEditUI extends UIPresenter
{
    private FefuTechnicalCommission technicalCommission;
    private Long technicalCommissionId;
    private Long enrollmentCampaignId;

    private boolean isAddForm;

    @Override
    public void onComponentRefresh()
    {
        isAddForm = (getTechnicalCommissionId() == null);
        if (isAddForm)
        {
            EnrollmentCampaign enrollmentCampaign = DataAccessServices.dao().getNotNull(getEnrollmentCampaignId());
            technicalCommission = new FefuTechnicalCommission();
            technicalCommission.setEnrollmentCampaign(enrollmentCampaign);
        }
        else
        {
            technicalCommission = DataAccessServices.dao().getNotNull(getTechnicalCommissionId());
        }
    }

    public void onClickApply()
    {
        FefuTechnicalCommissionsDataManager.instance().dao().saveOrUpdate(technicalCommission);
        deactivate();
    }

    public FefuTechnicalCommission getTechnicalCommission()
    {
        return technicalCommission;
    }

    public boolean getAddForm()
    {
        return isAddForm;
    }

    public void setEnrollmentCampaignId(Long enrollmentCampaignId)
    {
        this.enrollmentCampaignId = enrollmentCampaignId;
    }

    public Long getEnrollmentCampaignId()
    {
        return enrollmentCampaignId;
    }

    public void setTechnicalCommissionId(Long technicalCommissionId)
    {
        this.technicalCommissionId = technicalCommissionId;
    }

    public Long getTechnicalCommissionId()
    {
        return technicalCommissionId;
    }
}