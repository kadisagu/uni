/* $Id$ */
package ru.tandemservice.unifefu.events.nsi;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.sec.entity.*;
import ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRole;
import ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRoleToContextRel;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry V. Seleznev
 * @since 12.02.2015
 */
public class NsiHumanRoleListener extends ParamTransactionCompleteListener<Boolean>
{
    @SuppressWarnings("unchecked")
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, FefuNsiHumanRole.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, FefuNsiHumanRole.class, this);
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        // Поднимаем список изменившихся ролей сотрудников НСИ
        List<Object[]> humanRoleList = new DQLSelectBuilder().fromEntity(FefuNsiHumanRole.class, "e").fromEntity(EmployeePost.class, "ep").column(property("e")).column(property("ep"))
                .where(eq(property(FefuNsiHumanRole.person().id().fromAlias("e")), property(EmployeePost.employee().person().id().fromAlias("ep"))))
                .where(in(property(FefuNsiHumanRole.id().fromAlias("e")), params)).createStatement(session).list();

        // Подготавливаем вспомогательные сеты идентификаторов
        Set<Long> roleIdSet = new HashSet<>();
        Set<Long> emplPostIdSet = new HashSet<>();
        for (Object[] roleItem : humanRoleList)
        {
            emplPostIdSet.add(((EmployeePost) roleItem[1]).getId());
            roleIdSet.add(((FefuNsiHumanRole) roleItem[0]).getRole().getRole().getId());
        }

        // Поднимаем полный список связей роли физ лица с конкретными контекстами (роль у физ лица одна, а сотрудников много, нужно для каждого сотрудника продублировать роли)
        List<FefuNsiHumanRoleToContextRel> relList = new DQLSelectBuilder().fromEntity(FefuNsiHumanRoleToContextRel.class, "e").column(property("e"))
                .where(in(property(FefuNsiHumanRoleToContextRel.humanRole().id().fromAlias("e")), params))
                .createStatement(session).list();

        // Заполняем мап связей. Ключ - пара ID сотрудника и ID роли, значение - FefuNsiHumanRoleToContextRel
        Map<CoreCollectionUtils.Pair<Long, Long>, FefuNsiHumanRoleToContextRel> detailedEmplPostRoleToRPCMap = new HashMap<>();
        for (FefuNsiHumanRoleToContextRel rel : relList)
        {
            detailedEmplPostRoleToRPCMap.put(new CoreCollectionUtils.Pair<>(rel.getRoleAssignment().getPrincipalContext().getId(), rel.getHumanRole().getRole().getRole().getId()), rel);
        }

        // Поднимаем полный список конфигураций ролей
        List<RoleConfig> configList = new DQLSelectBuilder().fromEntity(RoleConfig.class, "e").column(property("e"))
                .where(in(property(RoleConfig.role().id().fromAlias("e")), roleIdSet))
                .createStatement(session).list();

        // Заполняем мап конфигураций рорлей. Ключ - ID роли, значение - RoleConfig
        Map<Long, RoleConfig> configMap = new HashMap<>();
        for (RoleConfig config : configList) configMap.put(config.getRole().getId(), config);


        // Поднимаем список назначенных глобальных ролей сотрудников
        List<IRoleAssignment> graList = new DQLSelectBuilder().fromEntity(IRoleAssignment.class, "e").column(property("e"))
                .where(in(property(RoleAssignmentGlobal.principalContext().id().fromAlias("e")), emplPostIdSet))
                .createStatement(session).list();

        // Заполняем мап контекстов. Ключ - пара ID сотрудника и ID роли, значение - RolePrincipalContext
        Map<CoreCollectionUtils.Pair<Long, Long>, IRoleAssignment> emplPostRoleToGRAMap = new HashMap<>();
        for (IRoleAssignment gra : graList)
        {
            emplPostRoleToGRAMap.put(new CoreCollectionUtils.Pair<>(gra.getPrincipalContext().getId(), gra.getRoleConfig().getRole().getId()), gra);
        }


        // Создаём недостающие контексты сотрудникам физ лица и генерируем связи с этими контекстами, дабы обеспечить каскадную обработку всех связей
        for (Object[] roleItem : humanRoleList)
        {
            FefuNsiHumanRole humanRole = (FefuNsiHumanRole) roleItem[0];
            EmployeePost employeePost = (EmployeePost) roleItem[1];

            CoreCollectionUtils.Pair<Long, Long> pair = new CoreCollectionUtils.Pair<>(employeePost.getId(), humanRole.getRole().getRole().getId());

            IRoleAssignment roleAssignment = emplPostRoleToGRAMap.get(pair);
            RoleConfig roleConfig = configMap.get(humanRole.getRole().getRole().getId());
            if (null == roleAssignment)
            {
                if (roleConfig instanceof RoleConfigGlobal)
                {
                    roleAssignment = new RoleAssignmentGlobal();
                    ((RoleAssignmentGlobal) roleAssignment).setRoleConfig((RoleConfigGlobal) roleConfig);
                    ((RoleAssignmentGlobal) roleAssignment).setPrincipalContext(employeePost);
                } else if (roleConfig instanceof RoleConfigLocalOrgUnit)
                {
                    roleAssignment = new RoleAssignmentLocalOrgUnit();
                    ((RoleAssignmentLocalOrgUnit) roleAssignment).setRoleConfig((RoleConfigLocalOrgUnit) roleConfig);
                    ((RoleAssignmentLocalOrgUnit) roleAssignment).setPrincipalContext(employeePost);
                } else if (roleConfig instanceof RoleConfigTemplateOrgUnit)
                {
                    roleAssignment = new RoleAssignmentTemplateOrgUnit();
                    ((RoleAssignmentTemplateOrgUnit) roleAssignment).setRoleConfig((RoleConfigTemplateOrgUnit) roleConfig);
                    ((RoleAssignmentTemplateOrgUnit) roleAssignment).setOrgUnit(humanRole.getRole().getOrgUnit());
                    ((RoleAssignmentTemplateOrgUnit) roleAssignment).setPrincipalContext(employeePost);
                }
                session.save(roleAssignment);
            }

            FefuNsiHumanRoleToContextRel rel = detailedEmplPostRoleToRPCMap.get(pair);
            if (null == rel)
            {
                rel = new FefuNsiHumanRoleToContextRel();
                rel.setHumanRole(humanRole);
                rel.setRoleAssignment(roleAssignment);
                session.save(rel);
            }
        }

        return true;
    }
}