/* $Id $ */
package ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTab;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.SessionAttSheetDocument;

import static org.tandemframework.hibsupport.dql.DQLExpressions.notInstanceOf;

/**
 * @author Rostuncev Savva
 * @since 19.03.2014
 */

public class DAO extends ru.tandemservice.unisession.component.orgunit.SessionTransferDocListTab.DAO implements IDAO
{
	@Override
	public void applyFilters(final DQLSelectBuilder dql, final String docAlias, final IDataSettings settings)
	{
		dql.where(notInstanceOf(docAlias, SessionAttSheetDocument.class));
		super.applyFilters(dql, docAlias, settings);
	}
}
