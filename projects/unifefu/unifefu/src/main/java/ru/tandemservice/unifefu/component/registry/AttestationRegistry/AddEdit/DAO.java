/* $Id$ */
package ru.tandemservice.unifefu.component.registry.AttestationRegistry.AddEdit;

import com.google.common.collect.Lists;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartModuleWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.FefuRegistryManager;

import java.util.Collections;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 12.11.2014
 */
public class DAO extends ru.tandemservice.uniepp.component.registry.AttestationRegistry.AddEdit.DAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(ru.tandemservice.uniepp.component.registry.base.AddEdit.Model model)
    {
        super.prepare(model);

        EppRegistryElement element = model.getElement();
        if (null == element.getId())
        {
            element.setSize(0);
            element.setLabor(0);
        }

        OrgUnit owner = model.getElement().getOwner();
        if (owner != null)
            ((Model) model).setOldOwner(owner);
    }

    @Override
    public void save(ru.tandemservice.uniepp.component.registry.base.AddEdit.Model<EppRegistryAttestation> model)
    {
        super.save(model);

        EppRegistryElement element =  model.getElement();
        IEppRegElWrapper elementWrapper = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(element.getId())).get(element.getId());
        List<EppRegistryElementPart> ignoreParts = Lists.newArrayList();

        for (IEppRegElPartWrapper partWrapper : elementWrapper.getPartMap().values())
        {
            EppRegistryElementPart part = partWrapper.getItem();
            if (part.getNumber() > element.getParts())
            {
                for (IEppRegElPartModuleWrapper moduleWrapper : partWrapper.getModuleMap().values())
                {
                    EppRegistryElementPartModule partModule = moduleWrapper.getItem();
                    delete(partModule);
                }
                ignoreParts.add(part);
                delete(part);
            }
        }
        FefuRegistryManager.instance().dao().correctionSizeAndLabor(element, ignoreParts);
        FefuRegistryManager.instance().dao().setOwner4ChildModulesOfRegElement(element, ((Model) model).getOldOwner());
    }
}
