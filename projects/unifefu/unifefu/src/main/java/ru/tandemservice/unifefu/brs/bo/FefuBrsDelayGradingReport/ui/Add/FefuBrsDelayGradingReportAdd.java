/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;

/**
 * @author nvankov
 * @since 12/10/13
 */
@Configuration
public class FefuBrsDelayGradingReportAdd extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(FefuBrsReportManager.instance().orgUnitDSConfig())
                .addDataSource(FefuBrsReportManager.instance().yearPartDSConfig())
                .addDataSource(FefuBrsReportManager.instance().ppsDSConfig())
                .addDataSource(FefuBrsReportManager.instance().groupDSConfig())
                .addDataSource(FefuBrsReportManager.instance().responsibilityOrgUnitDSConfig())
                .create();
    }
}



    