/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.ui.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.ui.ResultsList.FefuDipDocumentExcelBuilder;

import java.io.ByteArrayOutputStream;

/**
 * @author Andrey Avetisov
 * @since 30.09.2014
 */
public class DipDocumentReportDAO extends CommonDAO implements IDipDocumentReportDAO
{
    @Override
    public ByteArrayOutputStream buildReport(FefuDipDocumentExcelBuilder builder) throws Exception
    {
        return builder.buildReport();
    }

}
