/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu7.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuAcadGrantAssignStuExtract, Model>
{
}