package ru.tandemservice.unifefu.component.report.EntrantRatingSummary.EntrantRatingSummaryAdd;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uniec.component.report.EntrantRatingSummary.EntrantRatingSummaryAdd.Model;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author amakarova
 * @since 22.05.2013
 */
@Zlo
public class EntrantRatingSummaryReportBuilder extends ru.tandemservice.uniec.component.report.EntrantRatingSummary.EntrantRatingSummaryAdd.EntrantRatingSummaryReportBuilder
{
    public EntrantRatingSummaryReportBuilder(Model model, Session session) {
        super(model, session);
    }

    @Override
    protected String getRegNumber(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return requestedEnrollmentDirection.getEntrantRequest().getEntrant().getPersonalNumber();
    }
}
