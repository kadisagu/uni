/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu21.ParagraphAddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDao;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract;

import java.util.Collection;

/**
 * @author Igor Belanov
 * @since 30.06.2016
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuConditionalCourseTransferListExtract, Model> implements IDAO
{
    @Override
    @SuppressWarnings("Duplicates")
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));

        final EducationLevelsHighSchool eduLevels;
        FefuConditionalCourseTransferListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            if (null != model.getParagraphId())
                model.setCompensationType(firstExtract.getCompensationType());
            model.setGroup(firstExtract.getGroup());
            model.setCourse(firstExtract.getCourse());
            model.setCourseNew(firstExtract.getCourseNew());
            model.setGroupNew(firstExtract.getGroupNew());
            model.setEliminateDate(firstExtract.getEliminateDate());
            eduLevels = firstExtract.getGroup().getEducationOrgUnit().getEducationLevelHighSchool();
        } else
        {
            eduLevels = null;
        }
        model.setCourseAndGroupDisabled(!model.isParagraphOnlyOneInTheOrder());

        model.setGroupListModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getCourse() == null) return ListResult.getEmpty();

                MQBuilder builder = getBuilder(filter);

                return new ListResult<>(builder.getResultList(getSession(), 0, MoveStudentDao.MAX_ROWS), builder.getResultCount(getSession()));
            }

            @SuppressWarnings("deprecation")
            private MQBuilder getBuilder(String filter)
            {
                MQBuilder builder = new MQBuilder(Group.ENTITY_CLASS, "g");
                builder.add(MQExpression.eq("g", Group.L_COURSE, model.getCourse()));
                builder.add(MQExpression.eq("g", Group.P_ARCHIVAL, Boolean.FALSE));
                builder.add(MQExpression.or(
                        MQExpression.eq("g", Group.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getParagraph().getOrder().getOrgUnit()),
                        MQExpression.eq("g", Group.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getParagraph().getOrder().getOrgUnit())
                ));

                if (eduLevels != null) // фильтрация групп по Направлению подготовки группы из первого параграфа
                {
                    Collection<Long> hsIDs = MoveStudentDaoFacade.getMoveStudentDao().getChildSpecializationIDs(eduLevels, model.getParagraph().getOrder().getOrgUnit(), null);
                    hsIDs.add(eduLevels.getId());
                    builder.add(MQExpression.in("g", Group.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + ".id", hsIDs));
                }

                builder.add(MQExpression.like("g", Group.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("g", Group.P_TITLE, OrderDirection.asc);

                return builder;
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                Group group = get((Long) primaryKey);
                if (null != group)
                {
                    MQBuilder builder = getBuilder("");
                    if (builder.getResultList(getSession()).contains(group))
                        return group;
                }
                return null;
            }
        });
        model.setCourseNewListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getCourse() == null) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Course.class, "c")
                        .where(DQLExpressions.gt(DQLExpressions.property(Course.intValue().fromAlias("c")), DQLExpressions.value(model.getCourse().getIntValue())));

                return new ListResult<>(builder.createStatement(getSession()).<Course>list());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                Course course = get((Long) primaryKey);
                if (null != course)
                {
                    if (findValues("").getObjects().contains(course))
                        return course;
                }
                return null;
            }
        });

        model.setGroupNewListModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getCourseNew() == null || model.getGroup() == null) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(Group.class, "gn")
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.course().fromAlias("gn")), DQLExpressions.value(model.getCourseNew())))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.archival().fromAlias("gn")), DQLExpressions.value(Boolean.FALSE)))
                        .where(DQLExpressions.eq(DQLExpressions.property(Group.educationOrgUnit().fromAlias("gn")), DQLExpressions.value(model.getGroup().getEducationOrgUnit())));

                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(Group.title().fromAlias("gn"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > MoveStudentDao.MAX_ROWS)
                    builder.top(MoveStudentDao.MAX_ROWS);

                return new ListResult<>(builder.createStatement(getSession()).<Group>list(), count);
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                Group group = get((Long) primaryKey);
                if (null != group)
                {
                    if (findValues("").getObjects().contains(group))
                        return group;
                }
                return null;
            }
        });
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourse()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected FefuConditionalCourseTransferListExtract createNewInstance(Model model)
    {
        return new FefuConditionalCourseTransferListExtract();
    }

    @Override
    protected void fillExtract(FefuConditionalCourseTransferListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCourseNew(model.getCourseNew());
        extract.setGroupNew(model.getGroupNew());
        extract.setCompensationType(model.getCompensationType());
        extract.setEliminateDate(model.getEliminateDate());
    }
}
