package ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.EditBase;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.IBrsCoefficientRowWrapper;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBaseUI;
import java.util.*;

/**
 *
 */
public class TrBrsCoefficientEditBaseExtUI extends UIAddon
{

    public TrBrsCoefficientEditBaseExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        TrBrsCoefficientEditBaseUI presenter = getPresenter();
        EntityHolder owner = presenter.getOwnerHolder();
        if (owner.getValue() instanceof TrJournal)
        {
            hideUnusedBrsCoefficientRows((TrJournal) owner.getValue(), presenter.getRowList(), true);
            Collections.sort(presenter.getRowList(), (o1, o2) -> o1.getDef().getCode().compareToIgnoreCase(o2.getDef().getCode()));
        }
    }

    public static void hideUnusedBrsCoefficientRows(TrJournal trJournal, List<IBrsCoefficientRowWrapper> rows, boolean hideWeight)
    {
        Set<String> disabledRow = new HashSet<>(5);
        if (hideWeight)
            disabledRow.add(FefuBrs.WEIGHT_JOURNAL_CODE);

        String code;
        if (trJournal.getGradeScale() != null)
            code = trJournal.getGradeScale().getCode();
        else
        {
            EppFControlActionGroup fcaGroup = TrBrsCoefficientManager.instance().coefDao().getFcaGroup(trJournal);
            if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(fcaGroup.getCode()))
                code = EppGradeScaleCodes.SCALE_5;
            else
                code = EppGradeScaleCodes.SCALE_2;
        }

        switch (code)
        {
            case EppGradeScaleCodes.SCALE_5:
                disabledRow.add(FefuBrs.RANGE_SETOFF_CODE);
                break;
            case EppGradeScaleCodes.SCALE_2:
                disabledRow.add(FefuBrs.RANGE_3_CODE);
                disabledRow.add(FefuBrs.RANGE_4_CODE);
                disabledRow.add(FefuBrs.RANGE_5_CODE);
                break;
            default:
                disabledRow.add(FefuBrs.RANGE_3_CODE);
                disabledRow.add(FefuBrs.RANGE_4_CODE);
                disabledRow.add(FefuBrs.RANGE_5_CODE);
                disabledRow.add(FefuBrs.RANGE_SETOFF_CODE);
        }

        for (int i = rows.size() - 1; i >= 0; i--)
        {
            if (disabledRow.contains(rows.get(i).getDef().getUserCode()))
                rows.remove(i);
        }
    }
}
