/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.FefuPracticeContractWithExtOuManager;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.AddEdit.FefuPracticeContractWithExtOuAddEdit;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.RatingEdit.FefuPracticeContractWithExtOuRatingEdit;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;

/**
 * @author nvankov
 * @since 7/12/13
 */
@Input
        ({
                @Bind(key = UIPresenter.PUBLISHER_ID, binding = "practicContractId"),
        })
public class FefuPracticeContractWithExtOuViewUI extends UIPresenter
{
    private Long _practicContractId;
    private FefuPracticeContractWithExtOu _practiceContract;

    public Long getPracticContractId()
    {
        return _practicContractId;
    }

    public void setPracticContractId(Long practicContractId)
    {
        _practicContractId = practicContractId;
    }

    public FefuPracticeContractWithExtOu getPracticeContract()
    {
        return _practiceContract;
    }

    public void setPracticeContract(FefuPracticeContractWithExtOu practiceContract)
    {
        _practiceContract = practiceContract;
    }

    @Override
    public void onComponentRefresh()
    {
        _practiceContract = DataAccessServices.dao().get(FefuPracticeContractWithExtOu.class, FefuPracticeContractWithExtOu.id(), _practicContractId);
    }

    public void onClickEdit()
    {
        _uiActivation.asDesktopRoot(FefuPracticeContractWithExtOuAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, _practiceContract.getId()).activate();
    }

    public void onClickEditRating()
    {
        _uiActivation.asRegionDialog(FefuPracticeContractWithExtOuRatingEdit.class).parameter(UIPresenter.PUBLISHER_ID, _practiceContract.getId()).activate();
    }

    public void onClickUncheck()
    {
        _practiceContract.setChecked(false);
        FefuPracticeContractWithExtOuManager.instance().dao().createOrUpdate(_practiceContract);
    }

    public void onClickCheck()
    {
        _practiceContract.setChecked(true);
        FefuPracticeContractWithExtOuManager.instance().dao().createOrUpdate(_practiceContract);
    }

    public void onClickBackFromArchive()
    {
        _practiceContract.setArchival(false);
        FefuPracticeContractWithExtOuManager.instance().dao().createOrUpdate(_practiceContract);
    }

    public void onClickAddToArchive()
    {
        _practiceContract.setArchival(true);
        FefuPracticeContractWithExtOuManager.instance().dao().createOrUpdate(_practiceContract);
    }


}
