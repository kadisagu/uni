/* $Id$ */
package ru.tandemservice.unifefu.ws.eventsAxis2;

import com.google.common.collect.Lists;
import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nvankov
 * @since 9/19/13
 */
public class FefuSubmitEventsClient
{
    public static final String PORTAL_SERVICE_EVENTS_URL = "fefu.portal.service.events.url";
    private static final String SUBSYSTEM_CODE = "OB";
    private static final String FUNCTION_CODE = "TimeTable";

    private static final Logger LOGGER = LoggerFactory.getLogger(FefuSubmitEventsClient.class);

    public static FefuScheduleICalCommits sendEvents(String inputId, String groupTitle, List<String> currentNsiIds, String iCalendar, FefuScheduleICalCommits vEventsCommits)
    {
        SubmitEventSOAP11ServiceStub.SubmitEventsResponseE response = sendEvents(inputId, groupTitle, currentNsiIds, iCalendar);
        return checkResponse(response.getSubmitEventsResponse(), vEventsCommits);
    }

    public static FefuTeacherScheduleICalCommits sendEvents(String inputId, String groupTitle, List<String> currentNsiIds, String iCalendar, FefuTeacherScheduleICalCommits vEventsCommits)
    {
        SubmitEventSOAP11ServiceStub.SubmitEventsResponseE response = sendEvents(inputId, groupTitle, currentNsiIds, iCalendar);
        return checkTeacherResponse(response.getSubmitEventsResponse(), vEventsCommits);
    }

    private static SubmitEventSOAP11ServiceStub.SubmitEventsResponseE sendEvents(String inputId, String scheduleTitle, List<String> currentNsiIds, String iCalendar)
    {
        if(null == ApplicationRuntime.getProperty(PORTAL_SERVICE_EVENTS_URL))
        {
            throw new ApplicationException("Приложение не настроено для работы с веб-сервисом.", true);
        }

        ArrayList<SubmitEventSOAP11ServiceStub.Recipient> recipients = Lists.newArrayList();
        for(String guid : currentNsiIds)
        {

            SubmitEventSOAP11ServiceStub.Recipient recipient = new SubmitEventSOAP11ServiceStub.Recipient();
            recipient.setId(guid);
            recipient.setType(SubmitEventSOAP11ServiceStub.RecipientType.user);
            recipients.add(recipient);
        }
        SubmitEventSOAP11ServiceStub.Recipient[] recipientArr = recipients.toArray(new SubmitEventSOAP11ServiceStub.Recipient[]{});

        List<SubmitEventSOAP11ServiceStub.Event> events = Lists.newArrayList();
        SubmitEventSOAP11ServiceStub.Event event = new SubmitEventSOAP11ServiceStub.Event();
        event.setInputid(inputId);
        event.setSystemcode(SUBSYSTEM_CODE);
        event.setFunctioncode(FUNCTION_CODE);
        SubmitEventSOAP11ServiceStub.RecipientList recipientList = new SubmitEventSOAP11ServiceStub.RecipientList();
        recipientList.setRecipient(recipientArr);
        event.setRecipients(recipientList);
        event.setIcalendar(iCalendar);
        if(!StringUtils.isEmpty(scheduleTitle))
        {
            event.setComment(scheduleTitle);
        }
        else
        {
            event.setComment("Отмена расписания");
        }
        events.add(event);


        SubmitEventSOAP11ServiceStub.Event[] eventArr = events.toArray(new SubmitEventSOAP11ServiceStub.Event[]{});
        SubmitEventSOAP11ServiceStub.EventsList eventsList = new SubmitEventSOAP11ServiceStub.EventsList();
        eventsList.setEvent(eventArr);

        SubmitEventSOAP11ServiceStub.SubmitEventsRequest request = new SubmitEventSOAP11ServiceStub.SubmitEventsRequest();
        request.setEvents(eventsList);
        SubmitEventSOAP11ServiceStub.SubmitEventsRequestE requestE = new SubmitEventSOAP11ServiceStub.SubmitEventsRequestE();
        requestE.setSubmitEventsRequest(request);
//        SubmitEventSOAP11ServiceStub.SubmitEventsResponse response = requestE.
        SubmitEventSOAP11ServiceStub.SubmitEventsResponseE response;
        try
        {
            SubmitEventSOAP11ServiceStub stub = new SubmitEventSOAP11ServiceStub(SubmitEventSOAP11ServiceStub.ADDRESS);

            int timeOutInMilliSeconds = 10 * 60 * 1000; // 10 minutes
            stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, timeOutInMilliSeconds);
            stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, timeOutInMilliSeconds);

            response = stub.submitEvents(requestE);
        }
        catch (Exception e)
        {
            LOGGER.info("Webservice address - " + SubmitEventSOAP11ServiceStub.ADDRESS);
            if(e instanceof AxisFault && ((AxisFault)e).getMessage().equals("java.net.ConnectException"))
            {
                LOGGER.warn("Connection to Portal webservice could not be established. It means no events could be sent to the FEFU Portal.");
                Debug.exception(e);
                ApplicationException exception = new ApplicationException("Ошибка подключения к веб-сервису. Данные не могут быть отправлены.", e);
                exception.setForceRefresh(true);
                throw exception;
            }
            else if (e instanceof AxisFault)
            {
                LOGGER.error("An AxisFault exception occurred", e);

                AxisFault fault = (AxisFault) e;
                System.out.println("FaultAction = " + fault.getFaultAction());
                System.out.println("FaultNode = " + fault.getFaultNode());
                System.out.println("FaultReason = " + fault.getReason());
                System.out.println("FaultRole = " + fault.getFaultRole());
                System.out.println("FaultString = " + fault.getReason());
                System.out.println("FaultCode = " + fault.getFaultCode());

//                for(Element elem : )
                System.out.println("FaultDetail = " + fault.getFaultDetailElement());
//
//                AxisFault fault = (AxisFault) e;
//                System.out.println("FaultActor = " + fault.getFaultActor());
//                System.out.println("FaultNode = " + fault.getFaultNode());
//                System.out.println("FaultReason = " + fault.getFaultReason());
//                System.out.println("FaultRole = " + fault.getFaultRole());
//                System.out.println("FaultString = " + fault.getFaultString());
//                System.out.println("FaultCode = " + fault.getFaultCode().toString());
//                for(Element elem : fault.getFaultDetails())
//                    System.out.println("FaultDetail = " + elem.toString());

                Debug.exception(e);
                ApplicationException exception = new ApplicationException("Ошибка в работе веб-сервиса или веб-сервис недоступен", e);
                exception.setForceRefresh(true);
                throw exception;
            }
            else
            {
                LOGGER.error("A non AxisFault exception occurred", e);
                Debug.exception(e);
                throw new RuntimeException("A non AxisFault exception occurred", e);
            }
//            e.printStackTrace();
//            throw new ApplicationException("Ошибка в работе веб-сервиса или веб-сервис недоступен", e);
        }
//        SubmitEventSOAP11Service service = new SubmitEventSOAP11ServiceLocator();
//        SubmitEventsRequest request = new SubmitEventsRequest();
//        ;
//        try
//        {
//            SubmitEvents port = service.getSubmitEventsSOAP11Port();
//            response = port.submitEvents(request);
//        }
//        catch (Exception e)
//        {
//
//        }

        return response;
    }

    private static FefuScheduleICalCommits checkResponse(SubmitEventSOAP11ServiceStub.SubmitEventsResponse response, FefuScheduleICalCommits vEventsCommits)
    {
        if(null == response) return vEventsCommits;
        if(null == response.getCommits()) return vEventsCommits;
        if(null == response.getCommits().getCommit()) return vEventsCommits;

        for(SubmitEventSOAP11ServiceStub.Commit commit : response.getCommits().getCommit())
        {
            if (null != commit)
            {
                String[] outputId = commit.getOutputid().split(":");
                String out = outputId[0].replaceAll("\"", "");
                String vEventUid = outputId[2].replaceAll("\"", "");
                if (StringUtils.isEmpty(out))
                    vEventsCommits.getFailedVEvents().add(vEventsCommits.getvEventsMap().get(vEventUid));
                else
                    vEventsCommits.getCommitedVEvents().add(vEventsCommits.getvEventsMap().get(vEventUid));
            }
        }

        return vEventsCommits;
    }

    private static FefuTeacherScheduleICalCommits checkTeacherResponse(SubmitEventSOAP11ServiceStub.SubmitEventsResponse response, FefuTeacherScheduleICalCommits vEventsCommits)
    {
        if(null == response) return vEventsCommits;
        if(null == response.getCommits()) return vEventsCommits;
        if(null == response.getCommits().getCommit()) return vEventsCommits;

        for(SubmitEventSOAP11ServiceStub.Commit commit : response.getCommits().getCommit())
        {
            if (null != commit)
            {
                String[] outputId = commit.getOutputid().split(":");
                String out = outputId[0].replaceAll("\"", "");
                String vEventUid = outputId[2].replaceAll("\"", "");
                if (StringUtils.isEmpty(out))
                    vEventsCommits.getFailedVEvents().add(vEventsCommits.getvEventsMap().get(vEventUid));
                else
                    vEventsCommits.getCommitedVEvents().add(vEventsCommits.getvEventsMap().get(vEventUid));
            }
        }

        return vEventsCommits;
    }
}
