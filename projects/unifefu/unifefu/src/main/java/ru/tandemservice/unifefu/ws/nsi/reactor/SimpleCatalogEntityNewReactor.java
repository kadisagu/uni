/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Level;
import org.tandemframework.common.catalog.entity.IActiveCatalogItem;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.FefuNsiIdWrapper;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.INsiEntityUtil;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.NsiEntityUtilFactory;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 08.08.2013
 */
public abstract class SimpleCatalogEntityNewReactor<T extends INsiEntity, E extends IEntity> implements INsiEntityReactor<T>
{
    // Получаем инструменты для сравнения сущностей НСИ-ОБ
    private INsiEntityUtil UTIL;

    protected Map<String, T> _idToNsiEntityMap = new HashedMap();
    protected Map<Long, List<T>> _hashToNsiEntityListMap = new HashedMap();
    protected Map<String, CoreCollectionUtils.Pair<E, FefuNsiIds>> _idToEntityMap = new HashedMap();
    protected Map<Long, List<CoreCollectionUtils.Pair<E, FefuNsiIds>>> _hashToEntityListMap = new HashedMap();
    protected Map<Class, Set<String>> _usedCodes = new HashMap<>();

    protected Set<CoreCollectionUtils.Pair<IEntity, FefuNsiIds>> _otherEntityToSaveOrUpdateSet = new HashSet<>();
    protected Set<CoreCollectionUtils.Pair<IEntity, FefuNsiIds>> _otherEntityToPostSaveOrUpdateSet = new HashSet<>();

    public INsiEntityUtil getUtil()
    {
        if (null == UTIL) UTIL = NsiEntityUtilFactory.getUtil(getEntityClass());
        return UTIL;
    }

    /**
     * Признак обработки сущностей небольшими пакетами по 50 штук.
     * Актуально для больших справочников.
     *
     * @return - признак обработки сущностей небольшими пакетами
     */
    protected boolean useSmallPackage()
    {
        return false;
    }

    /**
     * Добавление элементов в справочник в ОБ разрешено
     *
     * @return - да/нет
     */
    public abstract boolean isAddItemsToOBAllowed();

    /**
     * Добавление элементов в справочник в НСИ разрешено
     *
     * @return - да/нет
     */
    public abstract boolean isAddItemsToNSIAllowed();

    /**
     * НСИ является мастер-системой, т.е. все правки в уже существующие элементы должны вноситься в подсистеме ОБ
     *
     * @return - да/нет
     */
    public abstract boolean isNsiMasterSystem();

    /**
     * Возвращает класс сущности ОБ
     *
     * @return - класс сущности ОБ
     */
    public abstract Class<E> getEntityClass();

    /**
     * Возвращает класс сущности НСИ
     *
     * @return - класс сущности НСИ
     */
    public abstract Class<T> getNSIEntityClass();

    /**
     * Возвращает имя сущности в мета-описании
     *
     * @return - имя сущности в мета-описании
     */
    public String getEntityName()
    {
        return EntityRuntime.getMeta(getEntityClass()).getName();
    }

    /**
     * Указывает нужно ли проверять объекты на удалябельность. По умолчанию нужно.
     *
     * @return - проверять удалябельность
     */
    protected boolean isShouldBeCheckedForDelete()
    {
        return true;
    }

    /**
     * Возвращает GUID сущности ОБ, либо доставая его из связанного объекта идентификаторов НСИ,
     * либо генерируя на основе идентификатора сущности в ОБ.
     *
     * @param entityPair - Пара значений: сущность ОБ, идентификатор НСИ
     * @return - GUID
     */
    public String getGUID(CoreCollectionUtils.Pair<E, FefuNsiIds> entityPair)
    {
        if (null == entityPair) return null;
        FefuNsiIds nsiIds = entityPair.getY();
        if (null != nsiIds) return nsiIds.getGuid();
        return NsiReactorUtils.generateGUID(entityPair.getX());
    }

    /**
     * Создаёт сущность ОБ на основе сщности НСИ.
     *
     * @param nsiEntity - Сущность НСИ
     * @return - новая сущность ОБ, готовая к сохранению.
     */
    public abstract E createEntity(T nsiEntity);

    /**
     * Создаёт сощность НСИ по переданной сущности ОБ с учетом уже вероятно имеющегося GUID
     *
     * @param entityPair - Пара хначений: сущность ОБ, Идентификатор НСИ
     * @return - Сущность НСИ, готовая к включению в датаграмму для отправки в НСИ
     */
    public abstract T createEntity(CoreCollectionUtils.Pair<E, FefuNsiIds> entityPair);

    /**
     * Возвращает пару (обект ОБ, Идентификатор НСИ) по сущности из НСИ.
     * Первым делом ищем по GUID. Если по GUID не найдено, то ищем по hash объекта
     *
     * @param nsiEntity - Сущность НСИ
     * @return - Пара значений: сущность ОБ, Идентификатор НСИ
     */
    public CoreCollectionUtils.Pair<E, FefuNsiIds> getPossibleDuplicatePair(T nsiEntity)
    {
        CoreCollectionUtils.Pair<E, FefuNsiIds> objectByGuidWrapper = _idToEntityMap.get(nsiEntity.getID());
        CoreCollectionUtils.Pair<E, FefuNsiIds> similarObjectWrapper = null;

        //if (null != objectByGuidWrapper) foundByGuid = true;

        List<CoreCollectionUtils.Pair<E, FefuNsiIds>> similarHashList = _hashToEntityListMap.get(getUtil().getNsiEntityHash(nsiEntity, false, false, true));
        if (null != similarHashList && !similarHashList.isEmpty())
        {
            if (similarHashList.size() == 1) similarObjectWrapper = similarHashList.get(0);
            else
            {
                for (CoreCollectionUtils.Pair<E, FefuNsiIds> item : similarHashList)
                {
                    if (getUtil().isTwoObjectsIdenticalFieldByField(nsiEntity, item.getX(), item.getY()))
                    {
                        similarObjectWrapper = item;
                        break;
                    }
                }
            }
        }

        // Дополнительная проверка
        if (null != objectByGuidWrapper && null != similarObjectWrapper
                && null != objectByGuidWrapper.getY() && null != similarObjectWrapper.getY()
                && !objectByGuidWrapper.getY().getGuid().equals(similarObjectWrapper.getY().getGuid()))
        {
            // TODO: OMG! What we gonna do now?
            // TODO: This can occure, if some duplicates were merged at NSI, but weren't merged at OB
            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  Shit happened. Leave the ship!");
        }

        return (null != objectByGuidWrapper && null != objectByGuidWrapper.getX()) ? objectByGuidWrapper : similarObjectWrapper;
    }

    /**
     * Обновляет ключевые поля сущности ОБ, в соответствии с сущностью НСИ.
     *
     * @param nsiEntity     - сущность НСИ
     * @param entityWrapper - пара значений: сущность ОБ, идентификатор НСИ
     *                      причем ключ может быть идентификатором сущности ОБ, либо её title, либо пользовательским кодом.
     * @return - Обновленная сущность ОБ, готовая к сохранению.
     */
    public abstract E updatePossibleDuplicateFields(T nsiEntity, CoreCollectionUtils.Pair<E, FefuNsiIds> entityWrapper);

    /**
     * Обновляет ключевые поля сущности НСИ, в соответствии с сущностью ОБ.
     *
     * @param nsiEntity - сущность НСИ
     * @param entity    - сущность ОБ
     * @return - Обновленная сущность НСИ, готовая к отправке в НСИ.
     */
    public abstract T updateNsiEntityFields(T nsiEntity, E entity);

    /**
     * Возвращает сущность НСИ, отправка которого в НСИ повлечет возврат полного перечня элементов этого справочника из НСИ.
     *
     * @return - сущность НСИ для запроса полного перечня элементов справочника из НСИ.
     */
    public T getFullCatalogRetrieveRequestObject()
    {
        return getCatalogElementRetrieveRequestObject(null);
    }

    /**
     * Возвращает признак удалённости элемента из НСИ. Если некогда элемент был удалён в НСИ, то чтобы данный элемент
     * не создавался повторно в НСИ, как новый, то ставится признак его удалённости.
     *
     * @param entityPair - пара значений: сущность ОБ, идентификатор НСИ
     * @return - true, если есть FefuNsiIds, связанный с переданным объектом, и у него установлен признак deletedFromNsi
     */
    public boolean isEntityDeletedFromNSI(CoreCollectionUtils.Pair<E, FefuNsiIds> entityPair)
    {
        if (null == entityPair || null == entityPair.getX()) return false;
        if (null == entityPair.getY()) return false;
        return entityPair.getY().isDeletedFromNsi();
    }

    /**
     * Возвращает true, если первоисточником сущности является подсистема ОБ
     *
     * @param entityId - Идентификатор сущности в ОБ
     * @param guid     - GUID для сравнения
     * @return - true, если guid совпадает со сгенерированнм налету
     */
    public boolean isEntitySourceOB(Long entityId, String guid)
    {
        if (null == entityId || null == guid) return false;
        String generatedGuid = NsiReactorUtils.generateGUID(entityId);
        return guid.equals(generatedGuid);
    }

    /**
     * Инициализирует МАП'ы объектов ОБ и их Идентификаторов НСИ, а так же МАП'ы по hash'ам объектов
     *
     * @param entityIds - набор идентификаторов сущностей ОБ. Если не заполнен, то поднимается весь справочник
     */
    protected void initEntityMap(Set<Long> entityIds)
    {
        Set<Long> processedEntitySet = new HashSet<>();
        _idToEntityMap = IFefuNsiSyncDAO.instance.get().getEntityWithNsiIdsMap(getEntityClass(), entityIds);
        _hashToEntityListMap = new HashedMap();

        for (CoreCollectionUtils.Pair<E, FefuNsiIds> entityToGuidPair : _idToEntityMap.values())
        {
            if (null != entityToGuidPair.getX() && !processedEntitySet.contains(entityToGuidPair.getX().getId()))
            {
                Long hash = getUtil().getEntityHash(entityToGuidPair.getX(), entityToGuidPair.getY(), false, false, true);
                List<CoreCollectionUtils.Pair<E, FefuNsiIds>> pairsList = _hashToEntityListMap.get(hash);
                if (null == pairsList) pairsList = new ArrayList<>();
                pairsList.add(entityToGuidPair);
                _hashToEntityListMap.put(hash, pairsList);
                processedEntitySet.add(entityToGuidPair.getX().getId());
            }
        }
    }

    /**
     * Регистрирует новые объекты, созданные в ОБ в процессе синхронизации/добавления/обновления элементов в НСИ
     *
     * @param entityPair - новая пара объектов сущность ОБ, идентификатор НСИ
     */
    protected void registerNewEntityInMaps(CoreCollectionUtils.Pair<E, FefuNsiIds> entityPair)
    {
        if (null == entityPair.getX() || null == entityPair.getY()) return;
        _idToEntityMap.put(entityPair.getX().getId().toString(), entityPair);
        _idToEntityMap.put(entityPair.getY().getGuid(), entityPair);
        Long hash = getUtil().getEntityHash(entityPair.getX(), entityPair.getY(), false, false, true);

        List<CoreCollectionUtils.Pair<E, FefuNsiIds>> entityPairList = _hashToEntityListMap.get(hash);
        if (null == entityPairList) entityPairList = new ArrayList<>();
        else
        {
            List<CoreCollectionUtils.Pair<E, FefuNsiIds>> newEntityPairList = new ArrayList<>();
            for (CoreCollectionUtils.Pair<E, FefuNsiIds> pairFromList : entityPairList)
            {
                if (null != pairFromList.getX() && !pairFromList.getX().getId().equals(entityPair.getX().getId()))
                    newEntityPairList.add(entityPair);
                else newEntityPairList.add(entityPair);
            }
            entityPairList = newEntityPairList;
        }

        _hashToEntityListMap.put(hash, entityPairList);
    }

    /**
     * Заполняет связанные МАП'ы
     *
     * @param entityIds - набор идентификаторов сущностей ОБ. Если не заполнен, то поднимается весь справочник
     */
    protected void prepareRelatedObjectsMap(Set<Long> entityIds)
    {
        if (IActiveCatalogItem.class.isAssignableFrom(getEntityClass()))
        {
            List<String> catalogCodes = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(getEntityClass(), "e").column(DQLExpressions.property("e", ICatalogItem.CATALOG_ITEM_USER_CODE)));
            for (String code : catalogCodes)
            {
                Set<String> usedCatalogCodes = _usedCodes.get(getEntityClass());
                if (null == usedCatalogCodes) usedCatalogCodes = new HashSet<>();
                usedCatalogCodes.add(code);
                _usedCodes.put(getEntityClass(), usedCatalogCodes);
            }
        }
    }

    /**
     * Вполняет первичныую обработку сущностей НСИ для извлечения из БД необходимых объектов
     *
     * @param itemsToProcess - список сущностей НСИ для обработки
     */
    protected void preProcessNsiEntities(List<INsiEntity> itemsToProcess)
    {
        // Необходимо переопределить в случае, если требуется предварительно пробежаться по пакету и поднять из базы
        // все необходимые объекты, например, связанные с адресами физ. лица.
    }

    /**
     * Для обеспечени псевдо-уникальности кода сущнотей НСИ в справочниках ОБ, предлагается в случае его занятости в ОБ
     * подставлять к коду НСИ некий префикс, а при отдаче в НСИ данный префикс убирать.
     *
     * @param clazz    - класс справочника для хранения уникальных пользовательских кодов
     * @param userCode - пользовательский код из НСИ для проверки на уникальность.
     * @return - обработанный пользователский код (либо как есть, либо с префиксом)
     */
    protected String getUserCodeForOBChecked(Class clazz, String userCode)
    {
        if (null == userCode) return null;
        if (null == clazz) clazz = getEntityClass();

        String resultCode = userCode;
        if (_usedCodes.containsKey(clazz) && _usedCodes.get(clazz).contains(userCode))
        {
            resultCode = "nsi." + userCode;
            if (_usedCodes.get(clazz).contains(resultCode)) resultCode += System.currentTimeMillis();
        }

        Set<String> usedCatalogCodesSet = _usedCodes.get(clazz);
        if (null == usedCatalogCodesSet) usedCatalogCodesSet = new HashSet<>();
        usedCatalogCodesSet.add(resultCode);
        _usedCodes.put(clazz, usedCatalogCodesSet);

        return resultCode;
    }

    /**
     * Убирает префикс, обеспечивающий уникальность пользователского кода в ОБ, при его наличии.
     *
     * @param userCode - пользовательский код справочника ОБ
     * @return - обработанный пользователский код
     */
    protected String getUserCodeForNSIChecked(String userCode)
    {
        if (null == userCode) return null;
        return userCode.replaceAll("nsi.", "");
    }

    /**
     * Производит полную синхронизацию справочников в ОБ и НСИ
     */
    @Override
    public void synchronizeFullCatalogWithNsi()
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG HAVE STARTED ==========");

        // Забираем полный список всех элементов справочника из НСИ
        List<T> nsiEntityList = NsiReactorUtils.retrieveObjectsFromNsiBySingleElement(getNSIEntityClass(), getFullCatalogRetrieveRequestObject());

        // Инициализируем мапы с сущностями ОБ
        initEntityMap(null);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + _idToEntityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        // Если для синхронизации нужны дополнительные данные, то инициализируем их
        prepareRelatedObjectsMap(null);

        List<Object> nsiEntityToUpdate = new ArrayList<>(); // Сюда сваливаем все сущности НСИ, которые требуется обновить данными из ОБ
        Set<Long> alreadySynchronizedEntitySet = new HashSet<>(); // Контрольный сет, блокирующий возможность повторного добавления сущности в НСИ, если она обновляется.

        // Проводим синхронизацию всех полученных элементов из НСИ с имеющимися в ОБ
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start local (OB) synchronization of elements for " + getEntityClass().getSimpleName() + " catalog ----------");
        for (T nsiEntity : nsiEntityList)
        {
            // Пытаемся найти соответствующий элементу из НСИ элемент в ОБ. Сначала по GUID'у, а затем по hash
            CoreCollectionUtils.Pair<E, FefuNsiIds> objectToUpdateWrapper = getPossibleDuplicatePair(nsiEntity);

            // Если объект помечен как дубль, то игнорируем его и переходим к следующему
            if (null != nsiEntity.getIsNotConsistent() && 1 == nsiEntity.getIsNotConsistent())
            {
                // Если есть точно такой же объект в ОБ, то нет смысла его отправлять в НСИ. Это родит ещё один дубль, поэтому помечаем его как обработанный.
                if (null != objectToUpdateWrapper)
                    alreadySynchronizedEntitySet.add(objectToUpdateWrapper.getX().getId());
                continue;
            }

            if (isNsiMasterSystem() && null == objectToUpdateWrapper
                    || (null != objectToUpdateWrapper && !alreadySynchronizedEntitySet.contains(objectToUpdateWrapper.getX().getId()))) // Если НСИ - мастер-система, то обновляем элементы в ОБ, но дубли не плодим.
            {
                E modifiedEntity = null;
                if (null != objectToUpdateWrapper) // Если найдены схожие элементы
                {
                    if (getUtil().isEntityChanged(nsiEntity, objectToUpdateWrapper.getX(), objectToUpdateWrapper.getY())) // Сверяем наличие изменений, чтобы не проводить обновление объекта без необходимости
                    {
                        modifiedEntity = updatePossibleDuplicateFields(nsiEntity, objectToUpdateWrapper); // Вносим изменения в сущность ОБ и далее обновляем сущности ОБ
                        FefuNsiIds nsiIds = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiEntities(modifiedEntity, objectToUpdateWrapper.getY(), new FefuNsiIdWrapper(nsiEntity.getID(), getEntityName(), isAddItemsToOBAllowed(), isNsiMasterSystem()), _otherEntityToSaveOrUpdateSet, _otherEntityToPostSaveOrUpdateSet);
                        CoreCollectionUtils.Pair<E, FefuNsiIds> modifiedPair = new CoreCollectionUtils.Pair<>(modifiedEntity, nsiIds);
                        alreadySynchronizedEntitySet.add(modifiedEntity.getId());
                        registerNewEntityInMaps(modifiedPair);
                    } else
                        IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(objectToUpdateWrapper.getX(), objectToUpdateWrapper.getY(), new FefuNsiIdWrapper(nsiEntity.getID(), getEntityName())); // Иначе просто обновляем данные о GUID и времени синхронизации элемента
                } else if (isAddItemsToOBAllowed()) // Если схожих не найдено, и позволено добавление новых элементов в ОБ, то создаём новый элемент и сохраняем его в ОБ
                {
                    modifiedEntity = createEntity(nsiEntity);
                    if (null != modifiedEntity)
                    {
                        FefuNsiIds nsiIds = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiEntities(modifiedEntity, null, new FefuNsiIdWrapper(nsiEntity.getID(), getEntityName(), isAddItemsToOBAllowed(), isNsiMasterSystem()), _otherEntityToSaveOrUpdateSet, _otherEntityToPostSaveOrUpdateSet);
                        CoreCollectionUtils.Pair<E, FefuNsiIds> newPair = new CoreCollectionUtils.Pair<>(modifiedEntity, nsiIds);
                        alreadySynchronizedEntitySet.add(modifiedEntity.getId());
                        registerNewEntityInMaps(newPair);
                    } else
                    {
                        // TODO Что делать, если объект не может быть создан в силу недостаточности данных? Пока не делаем ничего. Кидать эксепшн тут бесполезно
                        //new NSIProcessingErrorException("Could not create new element. Human not found for the given contractor.");
                    }
                }
            } else // Если НСИ - не мастер система, то обновляем элементы в НСИ значениями из ОБ (подготавливаем список для дальнейшего обновления)
            {
                // Если похожий объект в ОБ не найден, значит - не судьба. Это левый элемент и его там быть не должно, а в ОБ он не нужен.
                if (null != objectToUpdateWrapper)
                {
                    // Чтобы повторно не заслать в НСИ элемент, который ранее оттуда был удалён, проверяем соответствующий признак, а так же смотрим, что что-то изменилось
                    if ((null == objectToUpdateWrapper || null == objectToUpdateWrapper.getY()
                            || (objectToUpdateWrapper.getY() != null && !objectToUpdateWrapper.getY().isDeletedFromNsi()))
                            && getUtil().isEntityChanged(nsiEntity, objectToUpdateWrapper.getX(), objectToUpdateWrapper.getY()))
                        nsiEntityToUpdate.add(updateNsiEntityFields(nsiEntity, objectToUpdateWrapper.getX()));
                }
            }

            // Чтобы избежать повторной обработки далее, добавляем в соответствующий сет
            if (null != objectToUpdateWrapper) alreadySynchronizedEntitySet.add(objectToUpdateWrapper.getX().getId());
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Local (OB) synchronization of elements for " + getEntityClass().getSimpleName() + " catalog was finished ----------");

        // Если разрешено добавление новых элементов в НСИ
        if (isAddItemsToNSIAllowed())
        {
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog ----------");

            List<Object> nsiInsertEntityList = new ArrayList<>();
            Set<Long> alreadyPreparedForSendingToNSIEntitySet = new HashSet<>();

            // Бежим по всем сущностям ОБ и выискиваем те, которых нет в НСИ
            for (CoreCollectionUtils.Pair<E, FefuNsiIds> entityPair : _idToEntityMap.values())
            {
                if (null != entityPair.getY() && entityPair.getY().isDeletedFromNsi())
                    continue; // Проверяем, чтобы соответствующий элемент не был удалён ранее из НСИ

                // Если ранее ещё не обрабатывали данную сущность и не отправляли на правки в НСИ
                if (!alreadySynchronizedEntitySet.contains(entityPair.getX().getId())
                        && !alreadyPreparedForSendingToNSIEntitySet.contains(entityPair.getX().getId()))
                {
                    nsiInsertEntityList.add(createEntity(entityPair));
                    alreadyPreparedForSendingToNSIEntitySet.add(entityPair.getX().getId());
                    IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(entityPair.getX(), entityPair.getY(), new FefuNsiIdWrapper(null, getEntityName()));
                }
            }
            // Собственно, производим отправку свежедобавляемых элементов в НСИ
            NsiReactorUtils.executeNSIAction(nsiInsertEntityList, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT, useSmallPackage());
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
        }

        // Если НСИ не является мастер-системой по данному справочнику и есть некие отличия элементов с ОБ, то производим их обновление в НСИ
        if (!isNsiMasterSystem() && nsiEntityToUpdate.size() > 0)
        {
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start update elements at NSI for " + getEntityClass().getSimpleName() + " catalog ----------");
            NsiReactorUtils.executeNSIAction(nsiEntityToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE, useSmallPackage());
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Update elements at NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
        }

        // Изменяем время последней синхронизации для справочника в целом
        IFefuNsiSyncDAO.instance.get().updateNsiCatalogSyncTime(getNSIEntityClass().getSimpleName());
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG WAS FINISHED ==========");
    }

    @Override
    public void insertOrUpdateNewItemsAtNSI(Set<Long> entityToAddIdsSet)
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== INSERT/UPDATE ITEMS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG HAVE STARTED ==========");

        // Инициализируем мапы с сущностями ОБ
        initEntityMap(entityToAddIdsSet);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + _idToEntityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        // Если для синхронизации нужны дополнительные данные, то инициализируем их
        prepareRelatedObjectsMap(entityToAddIdsSet);

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Calculating elements, have to update at NSI, for " + getEntityClass().getSimpleName() + " catalog was finished ----------");

        if (isAddItemsToNSIAllowed())
        {
            List<Object> nsiInsertEntityList = new ArrayList<>();
            List<Object> nsiEntityToUpdate = new ArrayList<>();
            Set<Long> alreadySynchronizedEntitySet = new HashSet<>();
            Set<Long> alreadyPreparedForSendingToNSIEntitySet = new HashSet<>();

            for (Long entityId : entityToAddIdsSet)
            {
                CoreCollectionUtils.Pair<E, FefuNsiIds> entityPair = _idToEntityMap.get(entityId.toString());

                if (null != entityPair && null != entityPair.getY() && entityPair.getY().isDeletedFromNsi())
                    continue; // Проверяем, чтобы соответствующий элемент не был удалён ранее из НСИ

                if (!alreadySynchronizedEntitySet.contains(entityPair.getX().getId())
                        && !alreadyPreparedForSendingToNSIEntitySet.contains(entityPair.getX().getId()))
                {
                    if (null != entityPair.getY()) // Если есть GUID, то считаем элемент присутствующим в НСИ и пытаемся его обновить
                    {
                        nsiEntityToUpdate.add(createEntity(entityPair));
                    } else // Добавляем новый элемент в НСИ. Если такой уже есть в НСИ (по набору ключевых полей), то придёт запрос на дедубликацию.
                    {
                        nsiInsertEntityList.add(createEntity(entityPair));
                    }

                    alreadyPreparedForSendingToNSIEntitySet.add(entityPair.getX().getId());
                    FefuNsiIds nsiIds = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(entityPair.getX(), entityPair.getY(), new FefuNsiIdWrapper(null, getEntityName()));
                    CoreCollectionUtils.Pair<E, FefuNsiIds> modifiedPair = new CoreCollectionUtils.Pair<>(entityPair.getX(), nsiIds);
                    registerNewEntityInMaps(modifiedPair);
                }
            }

            if (nsiInsertEntityList.size() > 0)
            {
                FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog ----------");
                NsiReactorUtils.prepareNSIAction(nsiInsertEntityList, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT, useSmallPackage());
                FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
            }

            if (nsiEntityToUpdate.size() > 0)
            {
                FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start update elements at NSI for " + getEntityClass().getSimpleName() + " catalog ----------");
                NsiReactorUtils.prepareNSIAction(nsiEntityToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE, useSmallPackage());
                FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Update elements at NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
            }
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== INSERT/UPDATE ITEMS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG WAS FINISHED ==========");
    }

    @Override
    public void deleteItemsFromNSI(Set<Long> entityToDelIdsSet)
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM NSI HAVE STARTED ==========");

        initEntityMap(entityToDelIdsSet);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Some nsiIds list (" + _idToEntityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database by their ids ----------");

        Set<String> processedEntityGuids = new HashSet<>();
        List<Object> nsiEntityToDelete = new ArrayList<>();
        List<FefuNsiIds> nsiIdsListToUpdateDeleted = new ArrayList<>();

        for (CoreCollectionUtils.Pair<E, FefuNsiIds> pair : _idToEntityMap.values())
        {
            if (null != pair.getY() && isEntitySourceOB(pair.getY().getEntityId(), pair.getY().getGuid())
                    && !processedEntityGuids.contains(pair.getY().getGuid()))
            {
                nsiEntityToDelete.add(getCatalogElementRetrieveRequestObject(pair.getY().getGuid()));
            } else nsiIdsListToUpdateDeleted.add(pair.getY());
            processedEntityGuids.add(pair.getY().getGuid());
        }

        NsiReactorUtils.prepareNSIAction(nsiEntityToDelete, FefuNsiRequestsProcessor.OPERATION_TYPE_DELETE, useSmallPackage());

        //IFefuNsiSyncDAO.instance.get().updateDeletedFromOBProperty(nsiIdsListToUpdateDeleted); // TODO
        // Две проблемы: либо нужно отслеживать удаленные объекты, либо теряются гуды и нельзя определить, а был ли он вообще в ОБ
        // Пока выгоднее удалять напрочь.
        IFefuNsiSyncDAO.instance.get().deleteRelatedNsiId(entityToDelIdsSet);
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM NSI WAS FINISHED ==========");
    }

    @Override
    public List<Object> retrieve(List<INsiEntity> itemsToRetrieve)
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== RETRIEVE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM OB HAVE STARTED ==========");

        // Подготавливаем сет идентификаторов ОБ для инициализации мапов
        Set<Long> entityIdSetForMapInit = IFefuNsiSyncDAO.instance.get().getEntityIdSetByNsiEntityList(itemsToRetrieve);

        initEntityMap(entityIdSetForMapInit);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + _idToEntityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        prepareRelatedObjectsMap(entityIdSetForMapInit);

        List<Object> result = new ArrayList<>();
        Set<Long> alreadyPreparedItemIdList = new HashSet<>(); // Чтобы повторно не обрабатывать сущность, заполняем сет
        List<CoreCollectionUtils.Pair<E, FefuNsiIds>> pairsToRetrieve = new ArrayList<>();

        // Подготавливаем список пар сущность/идентификатор НСИ для последующего формирования списка сущностей НСИ
        for (INsiEntity nsiEntity : itemsToRetrieve)
        {
            if (null == nsiEntity.getID())
            {
                for (CoreCollectionUtils.Pair<E, FefuNsiIds> pair : _idToEntityMap.values())
                {
                    // Дабы случайно не вернуть номинально удалённую сущность НСИ, но оставленную в ОБ
                    if (null == pair.getY() || (null != pair.getY() && !pair.getY().isDeletedFromNsi()))
                        pairsToRetrieve.add(pair);
                }
            } else
            {
                // TODO: а нужно ли возвращать сущность, которую номинально удалили из НСИ, но нельзя было удалять из ОБ?
                CoreCollectionUtils.Pair<E, FefuNsiIds> pair = _idToEntityMap.get(nsiEntity.getID());
                if (null != pair) pairsToRetrieve.add(pair);
            }
        }

        // Подготавливаем список сущностей НСИ для возврата в ответе
        for (CoreCollectionUtils.Pair<E, FefuNsiIds> pair : pairsToRetrieve)
        {
            if (null != pair.getX() && !alreadyPreparedItemIdList.contains(pair.getX().getId()) && !isEntityDeletedFromNSI(pair))
            {
                // Если идентификатора НСИ у нас ещё нет в списке элементов, то генерим его нелету
                // TODO: неплохо было бы ещё сохранить его в базу, а то при адресации по сгенеренному налету ИД система на сможет найти соответствующий элемент
                if (null == pair.getY())
                {
                    FefuNsiIds dummyId = new FefuNsiIds();
                    dummyId.setGuid(getGUID(pair));
                    dummyId.setEntityId(pair.getX().getId());
                    pair.setY(dummyId);
                }
                result.add(createEntity(pair));
                alreadyPreparedItemIdList.add(pair.getX().getId());
            }
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole requested catalog elements list (" + result.size() + " items) for " + getEntityClass().getSimpleName() + " catalog was sent back ----------");
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== RETRIEVE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM OB WAS FINISHED ==========");

        return result;
    }

    private List<Object> insertOrUpdate(List<INsiEntity> itemsToProcess, String operationType) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB HAVE STARTED ==========");

        // Подготавливаем список консистентных сущностей из НСИ (вдруг там внезапно окажется дубль)
        Set<String> guidsToInsertSet = new HashSet<>();
        List<T> itemsToInsertChecked = new ArrayList<>();
        List<Object> itemsProcessed = new ArrayList<>();

        preProcessNsiEntities(itemsToProcess);

        for (INsiEntity nsiEntity : itemsToProcess)
        {
            // Если сущность помечена как неконсистентная (например, дубль), то её не нужно учитывать
            if (null == nsiEntity.getIsNotConsistent() || 1 != nsiEntity.getIsNotConsistent())
            {
                // Если у какой-то из сущностей в пакете не указан GUID, то обрабатывать такой пакет нельзя
                if (null == nsiEntity.getID())
                {
                    FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing incoming package was failed. There are one or more elements without GUID specified for " + getEntityClass().getSimpleName() + " catalog ----------");
                    FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
                    throw new NSIProcessingErrorException("Could not " + operationType.toLowerCase() + " element without GUID specified. Please, specify GUID for all items in request package.");
                }

                itemsToInsertChecked.add(getNSIEntityClass().cast(nsiEntity));
                itemsProcessed.add(nsiEntity);

                // Добавляем список guid'ов, для которых может понадобиться объединение дублей
                List<String> mergeDuplicatesList = getUtil().getMergeDuplicatesList(nsiEntity);
                if (null != mergeDuplicatesList) guidsToInsertSet.addAll(mergeDuplicatesList);
            }
        }

        // Если после отсеивания невалидных сущностей в пакете не осталось ничего, то обработка пакета теряет смысл
        if (itemsToInsertChecked.isEmpty())
        {
            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing incoming package was failed. There is no any entity guid to " + operationType.toLowerCase() + " in datagram. Could not process an empty request. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
            throw new NSIProcessingErrorException("There is no any entity guid to " + operationType.toLowerCase() + " in datagram. Could not process an empty request.");
        }

        // Готовим список сущностей НСИ, для которых требуется поднять данные из БД.
        // Здесь же учитываются идентификаторы НСИ для дедубликации
        List<INsiEntity> guidEntitySet = new ArrayList<>();
        guidEntitySet.addAll(itemsToProcess);

        for (String duplicateGuid : guidsToInsertSet)
        {
            guidEntitySet.add(getCatalogElementRetrieveRequestObject(duplicateGuid));
        }

        // Подготавливаем сет идентификаторов ОБ для инициализации мапов
        Set<Long> entityIdSet = IFefuNsiSyncDAO.instance.get().getEntityIdSetByNsiEntityList(guidEntitySet);

        // Получаем список GUID'ов неудаляемых объектов (в силу наличия ссылок с других объектов), на случай, если придётся дедублицировать
        List<String> nonDeletableGuids = IFefuNsiSyncDAO.instance.get().getNonDeletableGuidsList(getEntityClass(), entityIdSet);

        // Не указан ни один идентификатор сущности НСИ на удаление, либо запрос предполагает тотальное удаление всех элементов из ОБ
            /*if (null == entityIdSet)
                throw new NSIProcessingErrorException("Could not delete whole entity set. Please, specify entity ids list to delete.");*/

        /*if (entityIdSet.size() == itemsToInsertChecked.size())
        {
            initEntityMap(entityIdSet);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Specified elements list (" + _idToEntityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");
        } else
        TODO: нет полной уверенности, что нам не пришлют дубликат с полным совпадением, но другим guid'ом*/
        {
            initEntityMap(null);
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + _idToEntityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");
        }

        // Инициализируем дополнительные мапы, если требуется
        prepareRelatedObjectsMap(entityIdSet);

        Set<Long> entityDuplicateIdsToDel = new HashSet<>();
        Set<String> alreadyProcessedEntitySet = new HashSet<>();
        List<String> notDeletedDuplicateGuids = new ArrayList<>();
        List<String> nonProcessableGuids = new ArrayList<>();

        for (T nsiEntity : itemsToInsertChecked)
        {
            if (!alreadyProcessedEntitySet.contains(nsiEntity.getID()))
            {
                String newGuid = nsiEntity.getID();
                alreadyProcessedEntitySet.add(nsiEntity.getID());
                List<CoreCollectionUtils.Pair<E, FefuNsiIds>> entityDuplicates = new ArrayList<>();
                CoreCollectionUtils.Pair<E, FefuNsiIds> entityPair = _idToEntityMap.get(nsiEntity.getID());
                if (null != entityPair) entityDuplicates.add(entityPair);

                // Пробегаемся по дублям, если таковые имеются
                List<String> mergeDuplicatesList = getUtil().getMergeDuplicatesList(nsiEntity);
                if (null != mergeDuplicatesList)
                {
                    FefuNsiSyncDAO.logEvent(Level.INFO, "---------- There are some entities needs to be deduplicated (" + mergeDuplicatesList.size() + " items) for " + getEntityClass().getSimpleName() + " catalog ----------");
                    for (String duplicateGuid : mergeDuplicatesList)
                    {
                        CoreCollectionUtils.Pair<E, FefuNsiIds> duplicateEntityPair = _idToEntityMap.get(duplicateGuid);
                        if (null == entityPair && null != duplicateEntityPair) // Не найден элемент с таким guid'ом, но есть элемент с GUID'ом дубликата
                        {
                            entityPair = duplicateEntityPair;
                            //newGuid = duplicateGuid;
                        }
                        if (null != duplicateEntityPair && !entityDuplicates.contains(duplicateEntityPair))
                            entityDuplicates.add(duplicateEntityPair);
                    }
                }

                // Если до сих пор не найден объект в ОБ ни по GUID'у оригинала, ни по GUID'ам дублей, то пытаемся найти по hash
                if (null == entityPair || null == entityPair.getX())
                {
                    entityPair = getPossibleDuplicatePair(nsiEntity);
                }

                if (null != entityPair && null != entityPair.getX())
                {
                    entityDuplicates.remove(entityPair);
                    E updatedEntity = updatePossibleDuplicateFields(nsiEntity, entityPair);
                    if (null != updatedEntity)
                    {
                        FefuNsiIds nsiIds = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiEntities(updatedEntity, entityPair.getY(), new FefuNsiIdWrapper(newGuid, getEntityName(), isAddItemsToOBAllowed(), isNsiMasterSystem()), _otherEntityToSaveOrUpdateSet, _otherEntityToPostSaveOrUpdateSet);
                        CoreCollectionUtils.Pair<E, FefuNsiIds> modifiedPair = new CoreCollectionUtils.Pair<>(updatedEntity, nsiIds);
                        registerNewEntityInMaps(modifiedPair);
                    }
                } else
                {
                    if (!isAddItemsToOBAllowed())
                    {
                        FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Adding new elements to " + getEntityClass().getSimpleName() + " catalog can not be processed, because it prohibited. ----------");
                        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
                        throw new NSIProcessingErrorException("Adding new elements to " + getEntityName() + " catalog is prohibited.");
                    }

                    E newEntity = createEntity(nsiEntity);
                    if (null != newEntity)
                    {
                        FefuNsiIds nsiIds = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiEntities(newEntity, null, new FefuNsiIdWrapper(newGuid, getEntityName(), isAddItemsToOBAllowed(), isNsiMasterSystem()), _otherEntityToSaveOrUpdateSet, _otherEntityToPostSaveOrUpdateSet);
                        CoreCollectionUtils.Pair<E, FefuNsiIds> newPair = new CoreCollectionUtils.Pair<>(newEntity, nsiIds);
                        registerNewEntityInMaps(newPair);
                    } else
                    {
                        nonProcessableGuids.add(nsiEntity.getID());
                    }
                }

                // Формируем сет идентификаторов сущностей ОБ для удаления в процессе дедубликации
                for (CoreCollectionUtils.Pair<E, FefuNsiIds> duplicateToDelPair : entityDuplicates)
                {
                    if (!nonDeletableGuids.contains(duplicateToDelPair.getY().getGuid()))
                    {
                        _idToEntityMap.remove(duplicateToDelPair);
                        entityDuplicateIdsToDel.add(duplicateToDelPair.getX().getId());
                    } else notDeletedDuplicateGuids.add(duplicateToDelPair.getY().getGuid());
                }
            }
        }

        // Если есть невставляемые сущности (в силу отсутствия значений обязательных полей, то возвращаем НСИ ошибку
        if (!nonProcessableGuids.isEmpty())
        {
            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Processing entities for " + getEntityClass().getSimpleName() + " catalog was failed due to some required field values absence. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
            throw new NSIProcessingErrorException("One, or more entities could not be processed due to some required field values absence. Error object GUID's: " + CommonBaseStringUtil.joinNotEmpty(nonProcessableGuids, ", "));
        }

        // Если список неудаляемых объектов не пуст, то возвращаем в НСИ ошибку, не выполняя удаления других объектов
        if (!notDeletedDuplicateGuids.isEmpty())
        {
            StringBuilder errGuids = new StringBuilder();
            for (String guid : notDeletedDuplicateGuids)
            {
                errGuids.append(errGuids.length() > 0 ? ", " : "").append(guid);
            }

            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Deleting entities in process of deduplication for " + getEntityClass().getSimpleName() + " catalog was failed. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");

            throw new NSIProcessingErrorException("One, or more entities can not be deleted, because of other objects relations with the entities have to be deleted. " +
                    "Please, check dependencies for the objects " + CommonBaseStringUtil.joinNotEmpty(notDeletedDuplicateGuids, ", ") + ".");
        }

        // Удаляем дубликаты и связанные с ними идентификаторы НСИ
        IFefuNsiSyncDAO.instance.get().deleteObjectsSetCheckedForDeletability(getEntityClass(), entityDuplicateIdsToDel, isShouldBeCheckedForDelete());
        IFefuNsiSyncDAO.instance.get().deleteRelatedNsiId(entityDuplicateIdsToDel);

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== " + operationType.toUpperCase() + " ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");

        return itemsProcessed;
    }

    @Override
    public List<Object> insert(List<INsiEntity> itemsToInsert) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        return insertOrUpdate(itemsToInsert, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT);
    }

    @Override
    public List<Object> update(List<INsiEntity> itemsToUpdate) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        return insertOrUpdate(itemsToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE);
    }

    @Override
    public List<Object> delete(List<INsiEntity> itemsToDelete) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM OB HAVE STARTED ==========");

        // Подготавливаем сет идентификаторов ОБ для инициализации мапов
        Set<Long> entityIdSet = IFefuNsiSyncDAO.instance.get().getEntityIdSetByNsiEntityList(itemsToDelete);

        // Не указан ни один идентификатор сущности НСИ на удаление, либо запрос предполагает тотальное удаление всех элементов из ОБ
        if (null == entityIdSet)
        {
            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- There are no any GUID specified for the delete operation. Whole entity set could not be deleted ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM OB WAS FINISHED ==========");
            throw new NSIProcessingErrorException("Could not delete whole entity set. Please, specify entity ids list to delete.");
        }

        // Если единственным поставщиком данных для НСИ является ОБ, то удаление внешними подсистемами в нём элементов запрещено
        if (!isAddItemsToOBAllowed())
        {
            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Deleting elements is prohibited for the " + getEntityClass().getSimpleName() + " catalog. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM OB WAS FINISHED ==========");
            throw new NSIProcessingErrorException("Could not delete any element from whole entity set. Delete operation is prohibited for the " + getEntityClass().getSimpleName() + " catalog.");
        }

        // Инициализируем мапы объектов и связанных с ними GUID'ов
        initEntityMap(entityIdSet);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + _idToEntityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        // Инициализируем дополнительные мапы, если требуется
        // prepareRelatedObjectsMap(entityIdSetForMapInit);

        // Получаем список GUID'ов неудаляемых объектов (в силу наличия ссылок с других объектов)
        List<String> nonDeletableGuids = isShouldBeCheckedForDelete() ? IFefuNsiSyncDAO.instance.get().getNonDeletableGuidsList(getEntityClass(), entityIdSet) : Collections.emptyList();

        // Если список неудаляемых объектов не пуст, то возвращаем в НСИ ошибку, не выполняя удаления других объектов
        if (!nonDeletableGuids.isEmpty())
        {
            StringBuilder errGuids = new StringBuilder();
            for (String guid : nonDeletableGuids)
            {
                errGuids.append(errGuids.length() > 0 ? ", " : "").append(guid);
            }

            FefuNsiSyncDAO.logEvent(Level.ERROR, "---------- Deleting requested list (" + itemsToDelete.size() + " items) for " + getEntityClass().getSimpleName() + " catalog was failed. ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM OB WAS FINISHED ==========");

            throw new NSIProcessingErrorException("One, or more entities can not be deleted, because of other objects relations with the entities have to be deleted. " +
                    "Please, check dependencies for the objects " + CommonBaseStringUtil.joinNotEmpty(nonDeletableGuids, ", ") + ".");
        }

        // Подготавливаем список объектов реально обработанных и GUID'ов, не найденных в ОБ
        List<String> strangeGuids = new ArrayList<>();
        List<Object> deletedObjectsList = new ArrayList<>();
        for (INsiEntity nsiEntity : itemsToDelete)
        {
            if (null != _idToEntityMap.get(nsiEntity.getID()))
            {
                deletedObjectsList.add(getCatalogElementRetrieveRequestObject(nsiEntity.getID()));
            } else strangeGuids.add(nsiEntity.getID());
        }

        // Удаляем удалябельные объекты и связанные с ними идентификаторы НСИ
        IFefuNsiSyncDAO.instance.get().deleteObjectsSetCheckedForDeletability(getEntityClass(), entityIdSet, isShouldBeCheckedForDelete());
        IFefuNsiSyncDAO.instance.get().deleteRelatedNsiId(entityIdSet);

        // Выдаём предупреждение, если среди переданных GUID'ов имеются не найденные в ОБ. Удалять нечего.
        if (!strangeGuids.isEmpty())
        {
            FefuNsiSyncDAO.logEvent(Level.WARN, "---------- Deleting requested list (" + deletedObjectsList.size() + " items) for " + getEntityClass().getSimpleName() + " catalog was executed with warnings ----------");
            FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM OB WAS FINISHED ==========");
            throw new NSIProcessingWarnException("One, or more entities were not found at OB by given GUID's, so they were ignored: " + CommonBaseStringUtil.joinNotEmpty(strangeGuids, ", "), deletedObjectsList);
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Deleting requested elements list (" + deletedObjectsList.size() + " items) for " + getEntityClass().getSimpleName() + " catalog was executed successfully ----------");
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM OB WAS FINISHED ==========");

        return deletedObjectsList;
    }
}