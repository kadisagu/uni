/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPerson.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.shared.person.base.entity.Person;

/**
 * @author Dmitry Seleznev
 * @since 05.12.2014
 */
public interface IFefuPersonDao extends INeedPersistenceSupport
{
    void updateFefuLogin(Person person, String login, AuthenticationType authenticationType);
}