/**
 * FefuDirectumService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directumtest;

public interface FefuDirectumService extends java.rmi.Remote {
    public java.lang.String updateOrderRequisites(java.lang.String ID, java.lang.String orderNumber, java.lang.String orderDate, java.lang.String directumScanUrl) throws java.rmi.RemoteException;
    public java.lang.String commitOrderWithSignedScanUrl(java.lang.String ID, java.lang.String orderNumber, java.lang.String orderDate, java.lang.String directumScanUrl) throws java.rmi.RemoteException;
    public java.lang.String commitOrder(java.lang.String ID, java.lang.String orderNumber, java.lang.String orderDate) throws java.rmi.RemoteException;
    public java.lang.String sendToRevision(java.lang.String ID) throws java.rmi.RemoteException;
    public java.lang.String updateOrderWithSignedScanUrl(java.lang.String ID, java.lang.String directumScanUrl) throws java.rmi.RemoteException;
    public java.lang.String rejectOrder(java.lang.String ID) throws java.rmi.RemoteException;
}
