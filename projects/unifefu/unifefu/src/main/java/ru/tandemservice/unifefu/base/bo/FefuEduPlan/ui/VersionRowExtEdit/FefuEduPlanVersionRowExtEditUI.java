/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.VersionRowExtEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.logic.IFefuEduPlanDAO;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 29.10.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class FefuEduPlanVersionRowExtEditUI extends UIPresenter
{
    private Long _id;
    private Map<Term, Map<ICatalogItem, Double>> _dataMap;
    private ICatalogItem _currentType;
    private Term _currentTerm;
    private List<Long> _unmodifiableTypes;
    private Collection<Term> _terms;

    public Long getId(){ return _id; }
    public void setId(Long id){ _id = id; }

    public ICatalogItem getCurrentType(){ return _currentType; }
    public void setCurrentType(ICatalogItem currentType){ _currentType = currentType; }

    public Term getCurrentTerm(){ return _currentTerm; }
    public void setCurrentTerm(Term currentTerm){ _currentTerm = currentTerm; }

    public Collection<Term> getTerms(){ return _terms; }
    public void setTerms(Collection<Term> terms){ _terms = terms; }


    public Double getValue(){ return _dataMap.get(_currentTerm).get(_currentType); }
    public void setValue(Double value){ _dataMap.get(_currentTerm).put(_currentType, value); }

    public Collection<ICatalogItem> getTypes(){ return _dataMap.values().iterator().next().keySet(); }

    public String getFieldId(){ return String.valueOf(_currentTerm.getId()) + "_" + _currentType.getId();  }


    public boolean isCellDisabled()
    {
        return _currentType instanceof EppLoadType;
    }

    @Override
    public void onComponentRefresh()
    {
        IFefuEduPlanDAO dao = FefuEduPlanManager.instance().dao();
        _dataMap = dao.getEpvRowExtLoadDataMap(_id);
        _terms = _dataMap.keySet();
    }

    public void onClickApply()
    {
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        FefuEduPlanManager.instance().dao().updateEpvRowExtLoad(_id, _dataMap, errorCollector);

        if (errorCollector.hasErrors())
        {
            return;
        }

        deactivate();
    }
}