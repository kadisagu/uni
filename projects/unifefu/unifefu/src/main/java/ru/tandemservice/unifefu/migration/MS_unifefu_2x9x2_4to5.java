/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * @author Andrey Andreev
 * @since 17.02.2016
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x9x2_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuAbstractCompetence2EpvRegistryRowRel

        // создана новая сущность
        if (!tool.tableExists("ffabstrctcmptnc2epvrgstryrwr_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("ffabstrctcmptnc2epvrgstryrwr_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_b2292ae"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("registryrow_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuAbstractCompetence2EpvRegistryRowRel");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuEpvRowCompetence2EppStateEduStandardRel
        if (!tool.tableExists("ffepvrwcmptnc2eppsttedstndrd_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("ffepvrwcmptnc2eppsttedstndrd_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_fa321cde"),
                                      new DBColumn("edustandardrel_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuEpvRowCompetence2EppStateEduStandardRel");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuImtsaCompetence2EpvRegistryRowRel
        if (!tool.tableExists("ffimtscmptnc2epvrgstryrwrl_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("ffimtscmptnc2epvrgstryrwrl_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_438f838e"),
                                      new DBColumn("fefucompetence_id", DBType.LONG).setNullable(false),
                                      new DBColumn("number_p", DBType.INTEGER).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuImtsaCompetence2EpvRegistryRowRel");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuCompetence2EppStateEduStandardRel

        // удаление свойства "Направленность"
        tool.dropColumn("ffcmptnc2eppsttedstndrdrl_t", "programspecialization_id");

        // создано обязательное свойство groupNumber
        if (!tool.table("ffcmptnc2eppsttedstndrdrl_t").columnExists("groupnumber_p"))
        {
            // создать колонку
            tool.createColumn("ffcmptnc2eppsttedstndrdrl_t", new DBColumn("groupnumber_p", DBType.INTEGER));

            // заполнение нового поля
            fillNumbers2EppStateEduStandardRel(tool);

            // сделать колонку NOT NULL
            tool.setColumnNullable("ffcmptnc2eppsttedstndrdrl_t", "groupnumber_p", false);
        }

        // Заполнение новых таблиц
        if (tool.tableExists("ffcmptnc2epvrgstryrwrl_t") && tool.tableExists("ffcmptnc2edplnvrsnblckrl_t"))
            fillFefuCompetence2EpvRegistryRowRels(tool);

        // Удаление старых сущностей
        // сущность fefuCompetence2EpvRegistryRowRel

        // сущность была удалена
        if (tool.tableExists("ffcmptnc2epvrgstryrwrl_t"))
        {
            // удалить таблицу
            tool.dropTable("ffcmptnc2epvrgstryrwrl_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("fefuCompetence2EpvRegistryRowRel");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuCompetence2EduPlanVersionBlockRel

        // сущность была удалена
        if (tool.tableExists("ffcmptnc2edplnvrsnblckrl_t"))
        {
            // удалить таблицу
            tool.dropTable("ffcmptnc2edplnvrsnblckrl_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("fefuCompetence2EduPlanVersionBlockRel");
        }
    }

    /**
     * Заполняет свойство Номер таблицы связей стандарт-компетенция
     */
    private void fillNumbers2EppStateEduStandardRel(DBTool tool) throws Exception
    {
        final MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater(
                "update ffcmptnc2eppsttedstndrdrl_t set groupnumber_p=? where id=?", DBType.INTEGER, DBType.LONG);

        final List<Object[]> rows = tool.executeQuery(
                processor(Long.class, Long.class, Long.class, Integer.class),
                "select rel.edustandard_id, cmp.eppskillgroup_id, rel.id, rel.number_p"
                        + " from ffcmptnc2eppsttedstndrdrl_t rel"
                        + " join fefucompetence_t cmp on cmp.id=rel.fefucompetence_id"
        );

        rows.stream()
                .collect(Collectors.groupingBy(
                        row -> (Long) row[0],
                        Collectors.mapping(row -> row, Collectors.groupingBy(
                                row -> (Long) row[1],
                                Collectors.mapping(row -> new CoreCollectionUtils.Pair<>((Long) row[2], (Integer) row[3]), Collectors.toList())
                        ))
                ))
                .entrySet()
                .forEach(std -> std.getValue().entrySet()
                        .forEach(grp -> {
                                     List<CoreCollectionUtils.Pair<Long, Integer>> rels = grp.getValue();
                                     rels.sort((r1, r2) -> Integer.compare(r1.getY(), r2.getY()));
                                     for (int i = 0; i < rels.size(); )
                                     {
                                         CoreCollectionUtils.Pair<Long, Integer> rel = rels.get(i);
                                         i++;
                                         updater.addBatch(i, rel.getX());
                                     }
                                 }
                        ));
        updater.executeUpdate(tool);
    }

    /**
     * Заполняет таблицы связей компетенций со строками УПв из старых таблиц связей строка-компетенция и блок-компетенция
     */
    private void fillFefuCompetence2EpvRegistryRowRels(DBTool tool) throws Exception
    {
        short imtsaChildCode = tool.entityCodes().get("fefuImtsaCompetence2EpvRegistryRowRel");
        short stdChildCode = tool.entityCodes().get("fefuEpvRowCompetence2EppStateEduStandardRel");
        final MigrationUtils.BatchInsertBuilder insertAbsRel = new MigrationUtils.BatchInsertBuilder(MigrationUtils.DISCRIMINATOR, "registryrow_id");
        final MigrationUtils.BatchInsertBuilder imtsaRelUpdater = new MigrationUtils.BatchInsertBuilder(imtsaChildCode, "fefucompetence_id", "number_p");
        final MigrationUtils.BatchInsertBuilder stdRelUpdater = new MigrationUtils.BatchInsertBuilder(stdChildCode, "edustandardrel_id");

        //Элемент реестра, Компетенция, Код компетенции, Блок Упв в котором есть это элемент реестра
        final List<Object[]> rowRelRaws = tool.executeQuery(
                processor(Long.class, Long.class, String.class, Long.class, Long.class),
                "select regrel.registryrow_id, regrel.fefucompetence_id, cmp.code_p, rowrel.owner_id, pln.parent_id"
                        + " from ffcmptnc2epvrgstryrwrl_t regrel"
                        + " join fefucompetence_t cmp on cmp.id=regrel.fefucompetence_id"
                        + " join epp_epvrow_base_t rowrel on rowrel.id=regrel.registryrow_id"
                        + " join epp_eduplan_verblock_t blck on blck.id=rowrel.owner_id"
                        + " join epp_eduplan_ver_t ver on ver.id=blck.eduplanversion_id"
                        + " join epp_eduplan_t pln on pln.id=ver.eduplan_id"
        );

        //Номера компетенций в группе из блоков
        Map<CoreCollectionUtils.Pair<Long, Long>, Integer> numberByBlockAndCmp = getNumberByBlockAndCmpMap(tool);
        //Связи компетенций со стандартом
        Map<CoreCollectionUtils.Pair<Long, Long>, Long> stdRelByCmpAndStdMap = getStdRelByCmpAndStdMap(tool);
        //Стандарты по строкам УПв
//        Map<Long, Long> stdByRowMap = getStdByRowMap(tool);

        for (Object[] row : rowRelRaws)
        {
            Long rowId = (Long) row[0];
            Long cmpId = (Long) row[1];
            String cmpCode = (String) row[2];
            Long blockId = (Long) row[3];

            if (cmpCode.contains("imp"))//Импортированная компетенция. Связь импортированной компетенции со строки УПв
            {
                Integer number = numberByBlockAndCmp.get(new CoreCollectionUtils.Pair<>(blockId, cmpId));//Номер берется из связи с блоком.
                if (number != null)
                {
                    Long id = insertAbsRel.addRow(imtsaChildCode, rowId);
                    imtsaRelUpdater.addRowWithId(id, cmpId, number);
                }

                continue;
            }

            Long stdId = (Long) row[4];//stdByRowMap.get(rowId);
            if (stdId == null) continue;
            Long stdRelId = stdRelByCmpAndStdMap.get(new CoreCollectionUtils.Pair<>(cmpId, stdId));
            if (stdRelId != null) //Компетенция из стандарта.
            {
                Long id = insertAbsRel.addRow(stdChildCode, rowId);
                stdRelUpdater.addRowWithId(id, stdRelId);
            }
        }

        insertAbsRel.executeInsert(tool, "ffabstrctcmptnc2epvrgstryrwr_t");
        imtsaRelUpdater.executeInsert(tool, "ffimtscmptnc2epvrgstryrwrl_t");
        stdRelUpdater.executeInsert(tool, "ffepvrwcmptnc2eppsttedstndrd_t");
    }


    private Map<CoreCollectionUtils.Pair<Long, Long>, Integer> getNumberByBlockAndCmpMap(DBTool tool) throws Exception
    {
        final List<Object[]> blockRelRaws = tool.executeQuery(
                processor(Long.class, Long.class, Integer.class),
                "select block_id, fefucompetence_id, number_p from ffcmptnc2edplnvrsnblckrl_t");

        return blockRelRaws.stream()
                .collect(Collectors.toMap(
                        row -> new CoreCollectionUtils.Pair<>((Long) row[0], (Long) row[1]),
                        row -> (Integer) row[2],
                        (n1, n2) -> n1));
    }

    private Map<CoreCollectionUtils.Pair<Long, Long>, Long> getStdRelByCmpAndStdMap(DBTool tool) throws Exception
    {
        final List<Object[]> stdRelRaws = tool.executeQuery(
                processor(Long.class, Long.class, Long.class),
                "select fefucompetence_id, edustandard_id, id from ffcmptnc2eppsttedstndrdrl_t");

        return stdRelRaws.stream()
                .collect(Collectors.toMap(
                        row -> new CoreCollectionUtils.Pair<>((Long) row[0], (Long) row[1]),
                        row -> (Long) row[2]));
    }

    private Map<Long, Long> getStdByRowMap(DBTool tool) throws Exception
    {
        final List<Object[]> row2StdRaws = tool.executeQuery(
                processor(Long.class, Long.class),
                "select regrel.registryrow_id, pln.parent_id"
                        + " from ffcmptnc2epvrgstryrwrl_t regrel"
                        + " join epp_epvrow_base_t rowrel on rowrel.id=regrel.registryrow_id"
                        + " join epp_eduplan_verblock_t blck on blck.id=rowrel.owner_id"
                        + " join epp_eduplan_ver_t ver on ver.id=blck.eduplanversion_id"
                        + " join epp_eduplan_t pln on pln.id=ver.eduplan_id"
                        + " where pln.parent_id is not null"
        );

        return row2StdRaws.stream().collect(Collectors.toMap(row -> (Long) row[0], row -> (Long) row[1], (l1, l2) -> l1));
    }
}