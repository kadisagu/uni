/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.CatalogsList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.document.provider.SimpleDocumentRenderer;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unifefu.base.bo.NSISync.NSISyncManager;
import ru.tandemservice.unifefu.base.bo.NSISync.ui.ExecuteFromFile.NSISyncExecuteFromFile;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiDaemonDao;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiDeliveryDaemonDao;
import ru.tandemservice.unifefu.dao.daemon.IFefuNsiDaemonDao;

import java.io.OutputStream;

/**
 * @author Dmitry Seleznev
 * @since 12.09.2013
 */
public class NSISyncCatalogsListUI extends UIPresenter
{
    private boolean _autoRefreshDisabled = false;
    private int _timerInterval = 7000;

    public String getSyncIcon()
    {
        Boolean disabled = (Boolean) ((DataWrapper) getConfig().getDataSource(NSISyncCatalogsList.NSI_CATALOGS_LIST_DS).getCurrent()).getProperty(NSISyncCatalogsList.SYNC_DISABLED);
        if (null != disabled && disabled) return "reject";
        return "daemon";
    }

    public void onClickSyncNow()
    {
        if (FefuNsiDaemonDao.isEntityInQueueForSync(getListenerParameterAsLong()))
            FefuNsiDaemonDao.removeCatalogToSync(getListenerParameterAsLong());
        else if (FefuNsiDaemonDao.isEntitySyncInProcess(getListenerParameterAsLong()))
        {
            //TODO isEntitySyncInProcess
        } else
        {
            FefuNsiDaemonDao.registerCatalogToSyncAndWakeUpDaemon(getListenerParameterAsLong());
        }
    }

    public void onClickChangeAutoSync()
    {
        NSISyncManager.instance().dao().updateNsiCatalogAutosync(getListenerParameterAsLong());
    }

    public void onClickChangeRealTimeSync()
    {
        NSISyncManager.instance().dao().updateNsiCatalogRealTimeSync(getListenerParameterAsLong());
    }

    // TODO: Do not forget to kill this method
    public void onClickGetOrgstructDifferences() throws Exception
    {
        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(), true); }
        catch (Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    public void onClickForceDaemonStart()
    {
        FefuNsiDaemonDao.DAEMON.wakeUpDaemon();
    }

    public boolean isDaemonHasTasksInQueue()
    {
        return IFefuNsiDaemonDao.instance.get().isDaemonHasTasksInQueue();
    }

    public boolean isDaemonLockedByTransportError()
    {
        return FefuNsiDeliveryDaemonDao.isLockedByTransportError();
    }

    public void onClickUnlockDelivery()
    {
        FefuNsiDeliveryDaemonDao.unlockAfterTransportError();
        FefuNsiDeliveryDaemonDao.DAEMON.wakeUpDaemon();
    }

    public IDocumentRenderer buildDocumentRenderer()
    {
        return new SimpleDocumentRenderer(null)
        {
            @Override public String getContentType() { return DatabaseFile.CONTENT_TYPE_TEXT_CSV; }
            @Override public String getFilename() { return "nsiToObOrgstructDifferences.csv"; }
            @Override public void render(OutputStream stream) throws Exception
            { NSISyncManager.instance().dao().prepareOrgstructDifferences(stream); }
        };
    }

    public void onChangeAutoRefresh()
    {
        _autoRefreshDisabled = !_autoRefreshDisabled;
        if (_autoRefreshDisabled) _timerInterval = 86400000;
        else _timerInterval = 7000;
    }

    public void onClickExecuteFromFile()
    {
        _autoRefreshDisabled = true;
        _uiActivation.asRegionDialog(NSISyncExecuteFromFile.class).activate();
    }

    public boolean isAutoRefreshDisabled()
    {
        return _autoRefreshDisabled;
    }

    public void setAutoRefreshDisabled(boolean autoRefreshDisabled)
    {
        _autoRefreshDisabled = autoRefreshDisabled;
    }

    public int getTimerInterval()
    {
        return _timerInterval;
    }

    public void setTimerInterval(int timerInterval)
    {
        _timerInterval = timerInterval;
    }
}