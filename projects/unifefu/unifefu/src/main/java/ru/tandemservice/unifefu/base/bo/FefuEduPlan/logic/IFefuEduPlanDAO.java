/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.logic;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.util.FefuEpvRowWrapper;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEpvCheckState;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
public interface IFefuEduPlanDAO extends ISharedBaseDao, INeedPersistenceSupport
{
    /**
     * Возвращает отображение семестров на часы нагрузки по типам.
     * @param rowId id строки
     * @return отображение семестров на часы нагрузки по типам
     */
    public Map<Term, Map<ICatalogItem, Double>> getEpvRowExtLoadDataMap(Long rowId);

    /**
     * Сохраняет нагрузку строки плана.
     * @param rowId id строки
     * @param dataMap отображение семестров на часы нагрузки по типам
     * @param errorCollector коллектор ошибок
     */
    public void updateEpvRowExtLoad(Long rowId, Map<Term, Map<ICatalogItem, Double>> dataMap, ErrorCollector errorCollector);

    /**
     * Возвращает справочник датасорсов дисциплин и мероприятий блоков УПв.
     * @param versionId id УПв
     * @return справочник датасорсов дисциплин и мероприятий блоков УПв
     */
    public Map<EppEduPlanVersionBlock, PairKey<StaticListDataSource<FefuEpvRowWrapper>, StaticListDataSource<FefuEpvRowWrapper>>> getEduPlanVersionBlockDataSourceMap(Long versionId);

    /**
     * Сохраняет оригинальный файл ИМЦА.
     * @param block блок УПв
     * @param source файл
     * @param fileName имя файла
     * @param encoding кодировка
     * @param number номер версии
     */
    public void saveImtsaXml(EppEduPlanVersionBlock block, IUploadFile source, String fileName, String encoding, int number);

    /**
     * Состояние проверки УПв. Возвращает новое, если не было.
     * @param versionId id УПв
     * @return состояние проверки УПв
     */
    public FefuEpvCheckState getEpvCheckState(Long versionId);

    /**
     * Копирует учебный график из другой УПв.
     * @param source УПв, откуда копируется график
     * @param target УПв, куда копируется график
     * @param courses курсы, которые будут заменены в учебном графике; должен быть указан хотя бы один
     */
    void doCopyEduPlanVersionSchedule(@NotNull EppEduPlanVersion source, @NotNull EppEduPlanVersion target, @NotNull Collection<Course> courses);

    /**
     * Осуществляет различные проверки для блоков УПв.
     * 1. Проверка общих параметров:
     *      1) "Нормативный срок освоения образовательной программы для очной формы"
     *      2) "Общая трудоемкость образовательной программы для очной формы освоения, в ЗЕТ"
     *      3) "Общая трудоемкость образовательной программы для очной формы освоения, реализуемой за один учебный год, в ЗЕТ"
     *      4) "Общая трудоемкость образовательной программы для очно-заочной и заочной форм освоения, реализуемой за один учебный год, в ЗЕТ"
     *      5) "Максимальное количество часов, отводимых на занятия лекционного типа в целом по блокам, в процентах"
     *      6) "Минимальная трудоемкость дисциплин по выбору, в ЗЕТ
     * 2. Проверка компетенций
     *      - проверяем наличие в блоке УПв перечня всех заданных в ГОС компетенций
     * 3. Проверка трудоемкости
     *
     * @param block Блок УП(в)
     * @param params Проверять:
     *               [0] - общие параметры ГОС
     *               [1] - наличие в блоке УПв перечня всех заданных в ГОС компетенций
     *               [2] - наличие в УПв заданных в ГОС элементов и сумму трудоемкостей (попадание их в диапазон, заданный во ФГОС)
     *               [3] - мероприятия УПв
     *               [4] - дополнительные параметры
     */
    void doCheckEduPlanVersion(EppEduPlanVersionBlock block, boolean ... params);

    /**
     * Возвращает мап для таблицы "Сводные данные по бюджету времени"
     * @param version УПв
     * @return { courseNumber -> { partNumber -> { rowCode -> value }}}
     */
    Map<Integer, Map<Integer, Map<String, Double>>> getEpvSummaryBudgetDataMap(EppEduPlanVersion version);
}