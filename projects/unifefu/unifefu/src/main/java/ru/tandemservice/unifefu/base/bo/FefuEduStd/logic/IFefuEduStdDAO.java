package ru.tandemservice.unifefu.base.bo.FefuEduStd.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 02.12.2014
 */
public interface IFefuEduStdDAO
{
    /**
     * Возвращает блок трудоемкости для ГОСа по выбранной квалификации.
     *
     * @param eduStandard   ГОС
     * @param qualification Разрешенная квалификация
     * @return Блок трудоемкости.
     */
    List<FefuEppStateEduStandardLaborBlocks> getEduStandardLaborBlockRows(EppStateEduStandard eduStandard, EduProgramQualification qualification);

    /**
     * Пересчитывает сумму корневых строк текущего блока
     * для минимального и максимального ЗЕТ
     *
     * @param currentBlock текущий блок
     */
    void recalculateBlockMinAndMaxNumbers(FefuEppStateEduStandardLaborBlocks currentBlock);

    /**
     * Сохранение компетенций.
     *
     * @param eduStandard           ГОС
     * @param programSpecialization Направленность ГОСа (null - общий блок)
     * @param fefuCompetenceList    Список компетенций
     */
    void saveFefuCompetence(EppStateEduStandard eduStandard, Long skillGroupId, List<FefuCompetence> fefuCompetenceList);

    /**
     * Повышает номер компетенции в ГОС
     *
     * @param entityId id текущей связи компетенции с ГОС
     */
    void doChangeCompetencePriorityUp(Long entityId);

    /**
     * Понижает номер компетенции в ГОС
     *
     * @param entityId id текущей связи компетенции с ГОС
     */
    void doChangeCompetencePriorityDown(Long entityId);

    /**
     * Выстроить элементы компетенций по порядку, начиная с 1 (в рамках выбранного ГОСа).
     *
     * @param eduStandard           ГОС
     * @param programSpecialization Направленность ГОСа (null - общий блок)
     */
    void doReMoveFefuCompetence(EppStateEduStandard eduStandard, EduProgramSpecialization programSpecialization);

    /**
     * @param stateEduStandardId ГОС
     * @return возвращает мапу номеров компетенций в ГОСе (id компетенции, номер компетенции)
     */
    Map<Long, Integer> getCompetenceNumbersMap(Long stateEduStandardId);
}
