/* $Id: FefuRegistryExtLoadTab.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.ExtLoadTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 05.02.14
 * Time: 12:42
 */
@Configuration
public class FefuRegistryExtLoadTab extends BusinessComponentManager
{
    public static final String EDU_PROGRAM_DELETE_LISTENER = "onClickDeleteEduProgram";

    public static final String EDU_PROGRAM_DS = "eduProgramDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EDU_PROGRAM_DS, eduProgramDS(), eduProgramDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduProgramDS()
    {
        return columnListExtPointBuilder(EDU_PROGRAM_DS)
                .addColumn(textColumn(FefuRegistryElement2EduProgramKindRel.L_EDU_PROGRAM_SUBJECT, FefuRegistryElement2EduProgramKindRel.eduProgramSubject().titleWithCode()))
                .addColumn(textColumn(FefuRegistryElement2EduProgramKindRel.eduProgramSubject().subjectIndex().s(), FefuRegistryElement2EduProgramKindRel.eduProgramSubject().subjectIndex().title()))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, EDU_PROGRAM_DELETE_LISTENER).alert("message:eduProgramDS.delete.alert").disabled("ui:disabledEduProgramKind"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduProgramDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), FefuRegistryElement2EduProgramKindRel.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                EppRegistryElement element = context.get(FefuRegistryExtLoadTabUI.PARAM_REG_ELEMENT);
                dql.where(eq(property(alias, FefuRegistryElement2EduProgramKindRel.registryElement()), value(element)));
            }
        };
        return handler
                .order(FefuRegistryElement2EduProgramKindRel.eduProgramSubject().subjectCode())
                .order(FefuRegistryElement2EduProgramKindRel.eduProgramSubject().title())
                .pageable(false);
    }
}
