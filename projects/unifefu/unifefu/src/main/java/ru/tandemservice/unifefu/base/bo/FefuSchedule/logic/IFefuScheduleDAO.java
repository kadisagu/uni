/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSchedule.logic;

import ru.tandemservice.unifefu.base.vo.FefuSchedulePrintDataVO;
import ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.vo.SppScheduleGroupPrintVO;

/**
 * @author nvankov
 * @since 9/2/13
 */
public interface IFefuScheduleDAO
{
    SppScheduleGroupPrintVO prepareScheduleGroupPrintVO(SppSchedule schedule);

    SchedulePrintFormFefuExt savePrintForm(FefuSchedulePrintDataVO scheduleData);
}
