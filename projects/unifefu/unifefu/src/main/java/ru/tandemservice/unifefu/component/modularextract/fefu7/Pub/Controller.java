/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu7.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubController;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class Controller extends ModularStudentExtractPubController<FefuAcadGrantAssignStuExtract, IDAO, Model>
{
}