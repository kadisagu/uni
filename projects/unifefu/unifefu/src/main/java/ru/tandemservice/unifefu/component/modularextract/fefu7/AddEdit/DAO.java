/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu7.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.uni.dao.student.IAcademicGrantSizeDAO;
import ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract;
import ru.tandemservice.movestudent.utils.SessionPartModel;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuAcadGrantAssignStuExtract, Model> implements IDAO
{
    @Override
    protected FefuAcadGrantAssignStuExtract createNewInstance()
    {
        return new FefuAcadGrantAssignStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setPartModel(new SessionPartModel());

        Integer defaultGroupManagerBonusSize = IAcademicGrantSizeDAO.instance.get().getDefaultGroupManagerBonusSize();
        Integer defaultGrantSize = IAcademicGrantSizeDAO.instance.get().getDefaultGrantSize();

        if (model.isAddForm())
        {
            long count = new DQLSelectBuilder()
                    .fromEntity(GroupCaptainStudent.class, "g")
                    .where(DQLExpressions.eq(DQLExpressions.property(GroupCaptainStudent.student().fromAlias("g")), DQLExpressions.value(model.getExtract().getEntity())))
                    .createCountStatement(new DQLExecutionContext(getSession()))
                    .uniqueResult();

            model.getExtract().setGroupManagerBonus(count > 0);

            model.setGrantSize(defaultGrantSize == null ? 0.0d : defaultGrantSize);
            model.setGroupManagerBonusSize(defaultGroupManagerBonusSize == null ? 0.0d : defaultGroupManagerBonusSize);

        } else
        {
            model.setGrantSize(model.getExtract().getGrantSize());

            Double groupManagerBonusSize = model.getExtract().getGroupManagerBonusSize();
            if (groupManagerBonusSize == null)
            {
                groupManagerBonusSize = defaultGroupManagerBonusSize == null ? 0.0d : Double.valueOf(defaultGroupManagerBonusSize);
            }
            model.setGroupManagerBonusSize(groupManagerBonusSize);
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if (model.getExtract().getBeginDate().getTime() >= model.getExtract().getEndDate().getTime())
            errors.add("Дата окончания выплаты должна быть позже даты начала", "beginDate", "endDate");

        if (model.getExtract().isGroupManagerBonus() && model.getExtract().getGroupManagerBonusBeginDate().getTime() >= model.getExtract().getGroupManagerBonusEndDate().getTime())
            errors.add("Дата окончания выплаты надбавки должна быть позже даты начала", "groupManagerBonusBeginDate", "groupManagerBonusEndDate");
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setGrantSize(model.getGrantSize());
        if (model.getExtract().isGroupManagerBonus())
        {
            model.getExtract().setGroupManagerBonusSize(model.getGroupManagerBonusSize());

        } else
        {
            model.getExtract().setGroupManagerBonusSize(null);
            model.getExtract().setGroupManagerBonusBeginDate(null);
            model.getExtract().setGroupManagerBonusEndDate(null);
        }

        super.update(model);
    }
}