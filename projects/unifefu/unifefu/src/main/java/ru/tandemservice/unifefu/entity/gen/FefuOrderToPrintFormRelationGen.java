package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь приказа с печатной формой(для приказов ДПО/ДО)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuOrderToPrintFormRelationGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation";
    public static final String ENTITY_NAME = "fefuOrderToPrintFormRelation";
    public static final int VERSION_HASH = -38330544;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String P_FILE_TYPE = "fileType";
    public static final String P_FILE_NAME = "fileName";
    public static final String P_CONTENT = "content";

    private AbstractStudentOrder _order;     // Приказ
    private String _fileType;     // Тип файла
    private String _fileName;     // Имя файла
    private byte[] _content;     // Сохраненная печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AbstractStudentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrder(AbstractStudentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Тип файла. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFileType()
    {
        return _fileType;
    }

    /**
     * @param fileType Тип файла. Свойство не может быть null.
     */
    public void setFileType(String fileType)
    {
        dirty(_fileType, fileType);
        _fileType = fileType;
    }

    /**
     * @return Имя файла. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFileName()
    {
        return _fileName;
    }

    /**
     * @param fileName Имя файла. Свойство не может быть null.
     */
    public void setFileName(String fileName)
    {
        dirty(_fileName, fileName);
        _fileName = fileName;
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     */
    @NotNull
    public byte[] getContent()
    {
        initLazyForGet("content");
        return _content;
    }

    /**
     * @param content Сохраненная печатная форма. Свойство не может быть null.
     */
    public void setContent(byte[] content)
    {
        initLazyForSet("content");
        dirty(_content, content);
        _content = content;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuOrderToPrintFormRelationGen)
        {
            setOrder(((FefuOrderToPrintFormRelation)another).getOrder());
            setFileType(((FefuOrderToPrintFormRelation)another).getFileType());
            setFileName(((FefuOrderToPrintFormRelation)another).getFileName());
            setContent(((FefuOrderToPrintFormRelation)another).getContent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuOrderToPrintFormRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuOrderToPrintFormRelation.class;
        }

        public T newInstance()
        {
            return (T) new FefuOrderToPrintFormRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "fileType":
                    return obj.getFileType();
                case "fileName":
                    return obj.getFileName();
                case "content":
                    return obj.getContent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((AbstractStudentOrder) value);
                    return;
                case "fileType":
                    obj.setFileType((String) value);
                    return;
                case "fileName":
                    obj.setFileName((String) value);
                    return;
                case "content":
                    obj.setContent((byte[]) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "fileType":
                        return true;
                case "fileName":
                        return true;
                case "content":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "fileType":
                    return true;
                case "fileName":
                    return true;
                case "content":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return AbstractStudentOrder.class;
                case "fileType":
                    return String.class;
                case "fileName":
                    return String.class;
                case "content":
                    return byte[].class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuOrderToPrintFormRelation> _dslPath = new Path<FefuOrderToPrintFormRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuOrderToPrintFormRelation");
    }
            

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation#getOrder()
     */
    public static AbstractStudentOrder.Path<AbstractStudentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Тип файла. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation#getFileType()
     */
    public static PropertyPath<String> fileType()
    {
        return _dslPath.fileType();
    }

    /**
     * @return Имя файла. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation#getFileName()
     */
    public static PropertyPath<String> fileName()
    {
        return _dslPath.fileName();
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation#getContent()
     */
    public static PropertyPath<byte[]> content()
    {
        return _dslPath.content();
    }

    public static class Path<E extends FefuOrderToPrintFormRelation> extends EntityPath<E>
    {
        private AbstractStudentOrder.Path<AbstractStudentOrder> _order;
        private PropertyPath<String> _fileType;
        private PropertyPath<String> _fileName;
        private PropertyPath<byte[]> _content;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation#getOrder()
     */
        public AbstractStudentOrder.Path<AbstractStudentOrder> order()
        {
            if(_order == null )
                _order = new AbstractStudentOrder.Path<AbstractStudentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Тип файла. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation#getFileType()
     */
        public PropertyPath<String> fileType()
        {
            if(_fileType == null )
                _fileType = new PropertyPath<String>(FefuOrderToPrintFormRelationGen.P_FILE_TYPE, this);
            return _fileType;
        }

    /**
     * @return Имя файла. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation#getFileName()
     */
        public PropertyPath<String> fileName()
        {
            if(_fileName == null )
                _fileName = new PropertyPath<String>(FefuOrderToPrintFormRelationGen.P_FILE_NAME, this);
            return _fileName;
        }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation#getContent()
     */
        public PropertyPath<byte[]> content()
        {
            if(_content == null )
                _content = new PropertyPath<byte[]>(FefuOrderToPrintFormRelationGen.P_CONTENT, this);
            return _content;
        }

        public Class getEntityClass()
        {
            return FefuOrderToPrintFormRelation.class;
        }

        public String getEntityName()
        {
            return "fefuOrderToPrintFormRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
