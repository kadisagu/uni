package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x0_11to12 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuFakePpsEntry
        if (!tool.tableExists("fefufakeppsentry_t"))
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefufakeppsentry_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("vacancyname_p", DBType.createVarchar(255)).setNullable(false),
                new DBColumn("orgunit_id", DBType.LONG).setNullable(false),
				new DBColumn("post_id", DBType.LONG).setNullable(false),
				new DBColumn("staffrateinteger_p", DBType.INTEGER).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuFakePpsEntry");
		}

        short recordOwnerEntityCode;

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuLoadRecordOwner
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefuloadrecordowner_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("pps_id", DBType.LONG), 
				new DBColumn("fakepps_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			recordOwnerEntityCode = tool.entityCodes().ensure("fefuLoadRecordOwner");
		}

        Map<Long, Long> load2eduGroupMap = new HashMap<>();

        Map<Long, Long> load2ppsMap = new HashMap<>();
        Map<Long, Long> firstCourseLoad2ppsMap = new HashMap<>();
        Map<Long, Long> nonEduLoad2ppsMap = new HashMap<>();

        Map<Long, Long> pps2recordOwnerMap = new HashMap<>();


        Statement stmt = tool.getConnection().createStatement();
        ResultSet loadRs = stmt.executeQuery("select egl.id, ci.list_id, ci.pps_id " +
                "from fefuedugroupppsdistr_t egl " +
                "inner join epp_pps_collection_t ci " +
                "on ci.id = egl.ppsgroup_id ");

        while (loadRs.next())
        {
            Long loadId = loadRs.getLong(1);
            Long eduGroupId = loadRs.getLong(2);
            Long ppsId = loadRs.getLong(3);

            load2eduGroupMap.put(loadId, eduGroupId);
            load2ppsMap.put(loadId, ppsId);
        }

        ResultSet firstCourseRs = stmt.executeQuery("select fc.id, fc.pps_id " +
                "from fefufirstcourseimedugroup_t fc ");

        while (firstCourseRs.next())
        {
            Long firstCourseLoadId = firstCourseRs.getLong(1);
            Long ppsId = firstCourseRs.getLong(2);

            firstCourseLoad2ppsMap.put(firstCourseLoadId, ppsId);
        }

        ResultSet nonEduLoadRs = stmt.executeQuery("select nel.id, nel.pps_id " +
                "from fefunoneduload_t nel ");

        while (nonEduLoadRs.next())
        {
            Long nonEduLoadId = nonEduLoadRs.getLong(1);
            Long ppsId = nonEduLoadRs.getLong(2);

            nonEduLoad2ppsMap.put(nonEduLoadId, ppsId);
        }

        Set<Long> ppsIds = new HashSet();
        ppsIds.addAll(load2ppsMap.values());
        ppsIds.addAll(firstCourseLoad2ppsMap.values());
        ppsIds.addAll(nonEduLoad2ppsMap.values());

        PreparedStatement recordOwnerPs = tool.prepareStatement("insert into fefuloadrecordowner_t (id, discriminator, pps_id, fakepps_id) values (?, ? ,? , ?)");
        for (Long ppsId: ppsIds)
        {
            Long recordOwnerId = EntityIDGenerator.generateNewId(recordOwnerEntityCode);

            recordOwnerPs.setLong(1, recordOwnerId);
            recordOwnerPs.setShort(2, recordOwnerEntityCode);
            recordOwnerPs.setLong(3, ppsId);
            recordOwnerPs.setNull(4, Types.BIGINT);

            recordOwnerPs.execute();

            pps2recordOwnerMap.put(ppsId, recordOwnerId);
        }

		// создано обязательное свойство owner
        // создано обязательное свойство eduGroup
		{
			// создать колонку
			tool.createColumn("fefuedugroupppsdistr_t", new DBColumn("owner_id", DBType.LONG));
			tool.createColumn("fefuedugroupppsdistr_t", new DBColumn("edugroup_id", DBType.LONG));

            PreparedStatement update = tool.prepareStatement("update fefuedugroupppsdistr_t set owner_id=?, edugroup_id=? where id=?");
            for (Long loadId: load2ppsMap.keySet())
            {
                update.setLong(1, pps2recordOwnerMap.get(load2ppsMap.get(loadId)));
                update.setLong(2, load2eduGroupMap.get(loadId));
                update.setLong(3, loadId);

                update.execute();
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefuedugroupppsdistr_t", "owner_id", false);
            tool.setColumnNullable("fefuedugroupppsdistr_t", "edugroup_id", false);
		}

		// создано обязательное свойство owner
		{
			// создать колонку
			tool.createColumn("fefufirstcourseimedugroup_t", new DBColumn("owner_id", DBType.LONG));

            PreparedStatement update = tool.prepareStatement("update fefufirstcourseimedugroup_t set owner_id=? where id=?");
            for (Long firstCourseLoadId: firstCourseLoad2ppsMap.keySet())
            {
                update.setLong(1, pps2recordOwnerMap.get(firstCourseLoad2ppsMap.get(firstCourseLoadId)));
                update.setLong(2, firstCourseLoadId);

                update.execute();
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefufirstcourseimedugroup_t", "owner_id", false);
		}

		// создано обязательное свойство owner
		{
			// создать колонку
			tool.createColumn("fefunoneduload_t", new DBColumn("owner_id", DBType.LONG));

            PreparedStatement update = tool.prepareStatement("update fefunoneduload_t set owner_id=? where id=?");
            for (Long nonEduLoadId: nonEduLoad2ppsMap.keySet())
            {
                update.setLong(1, pps2recordOwnerMap.get(nonEduLoad2ppsMap.get(nonEduLoadId)));
                update.setLong(2, nonEduLoadId);

                update.execute();
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefunoneduload_t", "owner_id", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEduGroupPpsDistr
		// удалено свойство ppsGroup
		{
			// удалить колонку
			tool.dropColumn("fefuedugroupppsdistr_t", "ppsgroup_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuFirstCourseImEduGroup
		// удалено свойство pps
		{
			// удалить колонку
			tool.dropColumn("fefufirstcourseimedugroup_t", "pps_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuNonEduLoad
		// удалено свойство pps
		{
			// удалить колонку
			tool.dropColumn("fefunoneduload_t", "pps_id");
		}
    }
}