/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.membership;

/**
 * @author Nikolay Fedorovskih
 * @since 28.01.2014
 */
public class CourseMembershipWSConstants
{
    public static final int BY_CRS_ID = 2; // Filter based on Course Id
    public static final int BY_CRS_ID_AND_ROLE_ID = 7; // Filter based on Course Id's AND role Id's
    public static final int BY_CRS_ID_AND_USER_ID = 6; // Filter based on User Id's AND course Id's
    public static final int BY_CRS_MEM_ID = 3; // Filter based on CourseMembership Id's
    public static final int BY_GRP_ID = 4; // Filter based on CourseMembership Id's
    public static final int BY_ID = 1; // Filter based on Membership Id
    public static final int BY_USER_ID = 5; // Filter based on User Id's

    public static final String INSTRUCTOR_ROLE = "P";
    public static final String STUDENT_ROLE = "S";

    public static final String COURSEMEMWS101 = "Membership.WS101"; // Failed to save or update memberships
    public static final String COURSEMEMWS102 = "Membership.WS102"; // Failed to delete memberships
    public static final String COURSEMEMWS103 = "Membership.WS103"; // Failed to load memberships
}