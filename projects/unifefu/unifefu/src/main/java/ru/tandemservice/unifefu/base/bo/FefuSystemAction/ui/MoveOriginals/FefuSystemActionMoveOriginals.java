/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.MoveOriginals;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author nvankov
 * @since 5/22/13
 */
@Configuration
public class FefuSystemActionMoveOriginals extends BusinessComponentManager
{
    public final static String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, enrollmentCampaignDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrollmentCampaignDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EnrollmentCampaign.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", EnrollmentCampaign.useCompetitionGroup()), DQLExpressions.value(Boolean.TRUE)));
                ep.dqlBuilder.order(DQLExpressions.property("e", EnrollmentCampaign.educationYear().intValue()), OrderDirection.desc);
            }
        };
    }
}
