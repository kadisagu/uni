package ru.tandemservice.unifefu.component.modularextract.fefu1.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/17/12
 * Time: 4:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuChangeDateStateExaminationStuExtract>
{
    private Integer _year;
    private List<String> _seasons;
    private String _season;

    public Integer getYear()
    {
        return _year;
    }

    public void setYear(Integer year)
    {
        _year = year;
    }

    public List<String> getSeasons()
    {
        return _seasons;
    }

    public void setSeasons(List<String> seasons)
    {
        _seasons = seasons;
    }

    public String getSeason()
    {
        return _season;
    }

    public void setSeason(String season)
    {
        _season = season;
    }
}

