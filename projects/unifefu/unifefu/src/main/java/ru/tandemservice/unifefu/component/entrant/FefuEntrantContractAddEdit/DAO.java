/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.entrant.FefuEntrantContractAddEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.unifefu.entity.FefuEntrantContract;

/**
 * @author Alexander Zhebko
 * @since 02.04.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        FefuEntrantContract contract = get(FefuEntrantContract.class, FefuEntrantContract.requestedEnrollmentDirection().id(), model.getDirectionId());
        model.setContract(contract == null ? new FefuEntrantContract() : contract);
    }

    @Override
    public void update(Model model)
    {
        FefuEntrantContract contract = model.getContract();
        if (contract.getRequestedEnrollmentDirection() == null)
        {
            contract.setRequestedEnrollmentDirection(getNotNull(RequestedEnrollmentDirection.class, model.getDirectionId()));
        }

        saveOrUpdate(contract);
    }
}