/* $Id$ */
package ru.tandemservice.unifefu.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

/**
 * @author Andrey Avetisov
 * @since 29.09.2014
 */
public class UniFefuGlobalReportListAddon extends UIAddon
{
    public static final String NAME = "uniFefuGlobalReportListAddon";

    public UniFefuGlobalReportListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}
