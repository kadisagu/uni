/**
 *$Id: DAO.java 38584 2014-10-08 11:05:36Z azhebko $
 */
package ru.tandemservice.unifefu.component.student.StudentPub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.entity.PersonFefuExt;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.entity.gen.PersonFefuExtGen;
import ru.tandemservice.unifefu.entity.gen.StudentFefuExtGen;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.utils.NsiIdWrapper;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;

/**
 * @author Alexander Shaburov
 * @since 30.08.12
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        Student student = model.getStudent();
        EducationLevels educationLevel = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
        // берем, если есть, объекты расширения в ДВФУ
        StudentFefuExt studentFefuExt = getByNaturalId(new StudentFefuExtGen.NaturalId(student));
        PersonFefuExt personFefuExt = getByNaturalId(new PersonFefuExtGen.NaturalId(student.getPerson()));

        FefuNsiIds nsiId = IFefuNsiSyncDAO.instance.get().getNsiId(student.getPerson().getId());
        if (null != nsiId && nsiId.isHasFormFilled())
            model.getBaseModel().setHasFormFilled(true);

        if (null == educationLevel.getQualification())
            educationLevel = EducationOrgUnitUtil.getParentLevel(educationLevel);

        model.setStudentFefuExt(studentFefuExt);
        model.setPersonFefuExt(personFefuExt);

        model.setStudentFefuExtIntegrationId(studentFefuExt != null ? studentFefuExt.getIntegrationId() : null);
        model.setStudentAddress1Str(studentFefuExt != null ? studentFefuExt.getOldAddress1Str() : null);
        model.setStudentAddress2Str(studentFefuExt != null ? studentFefuExt.getOldAddress2Str() : null);

        model.setVipBlockVisible(personFefuExt != null && personFefuExt.getVipDate() != null);
        model.setBaseEduTitle(studentFefuExt != null && studentFefuExt.getBaseEdu() != null ? FefuStudentManager.instance().getBaseEduOption(studentFefuExt.getBaseEdu()).getTitle() : "");
        model.setQualificationTitle(educationLevel.getQualificationTitle());
	    model.setSpecialDegreeTitle(educationLevel.getEduProgramSubject() instanceof EduProgramSubject2009 ? ((EduProgramSubject2009) educationLevel.getEduProgramSubject()).getSpecialDegreeTitle() : null);

        model.setNsiIdWrapper(new NsiIdWrapper(model.getStudent().getId()));
    }
}