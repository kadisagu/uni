/*$Id$*/
package ru.tandemservice.unifefu.component.pps.PpsSessionBulletinList;

/**
 * @author DMITRY KNYAZEV
 * @since 07.09.2015
 */
public interface IDAO extends ru.tandemservice.unisession.component.pps.PpsSessionBulletinList.IDAO
{

    void doPrintBulletin(Long id);
}
