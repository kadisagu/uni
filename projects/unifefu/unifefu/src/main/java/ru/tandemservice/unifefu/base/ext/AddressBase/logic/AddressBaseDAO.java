/* $Id$ */
package ru.tandemservice.unifefu.base.ext.AddressBase.logic;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact;
import ru.tandemservice.unifefu.events.nsi.AddressListener;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 12.01.2015
 */
public class AddressBaseDAO extends org.tandemframework.shared.fias.base.bo.AddressBase.logic.AddressBaseDAO
{
    @Override
    public void saveOrUpdateAddressWithoutEntityUpdate(IEntity entity, AddressBase addressDTO, String propertyPath, Boolean nullWithoutSettlement)
    {
        Long oldAddressId = null;
        AddressBase oldAddr = (AddressBase) entity.getProperty(propertyPath);
        if (null != oldAddr) oldAddressId = oldAddr.getId();
        Set<String> reservedGuidsSet = new HashSet<>();


        if (null != oldAddressId)
        {
            List<String> guidsList = new DQLSelectBuilder().fromEntity(FefuNsiPersonContact.class, "e").column(property(FefuNsiIds.guid().fromAlias("ids")))
                    .joinEntity("e", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(FefuNsiPersonContact.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                    .where(eq(property(FefuNsiPersonContact.address().id().fromAlias("e")), value(oldAddressId)))
                    .order(property(FefuNsiIds.syncTime().fromAlias("ids")), OrderDirection.desc)
                    .top(1).createStatement(getSession()).list();

            reservedGuidsSet.addAll(guidsList);
        }

        super.saveOrUpdateAddressWithoutEntityUpdate(entity, addressDTO, propertyPath, nullWithoutSettlement);

        getSession().flush();

        if (null != oldAddressId)
        {
            AddressBase newAddr = (AddressBase) entity.getProperty(propertyPath);
            if (null != newAddr && !oldAddressId.equals(newAddr.getId()))
            {
                if(!reservedGuidsSet.isEmpty()) AddressListener.NEW_ADDR_GUID_INHERITANCE_MAP.put(newAddr.getId(), reservedGuidsSet.iterator().next());
            }
        }
    }
}