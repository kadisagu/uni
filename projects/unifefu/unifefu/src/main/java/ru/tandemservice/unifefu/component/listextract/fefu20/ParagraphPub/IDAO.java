/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;

/**
 * @author Andrey Andreev
 * @since 13.01.2016
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuAdmittedToGIAListExtract, Model>
{
}
