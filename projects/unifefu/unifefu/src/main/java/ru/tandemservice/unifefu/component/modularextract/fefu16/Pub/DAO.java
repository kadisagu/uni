/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu16.Pub;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.movestudent.component.modularextract.e86.utils.EmployeePostDecl;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author nvankov
 * @since 11/20/13
 */
public class DAO extends ModularStudentExtractPubDAO<FullStateMaintenanceStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        DQLSelectBuilder paymentsBuilder = new DQLSelectBuilder().fromEntity(FefuOrphanPayment.class, "op");
        paymentsBuilder.where(DQLExpressions.eq(DQLExpressions.property("op", FefuOrphanPayment.extract().id()), DQLExpressions.value(model.getExtract().getId())));
        paymentsBuilder.order(property("op", FefuOrphanPayment.paymentYear()));
        paymentsBuilder.order(property("op", FefuOrphanPayment.paymentMonth()));
        paymentsBuilder.order(property("op", FefuOrphanPayment.type().priority()));
        List<FefuOrphanPayment> payments = paymentsBuilder.createStatement(getSession()).list();
        List<String> paymentsInfo = Lists.newArrayList();
        for (FefuOrphanPayment payment : payments)
        {
            String datePeriod;
            if (model.getExtract().isPaymentsForPeriod())
            {
                datePeriod = " (с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(payment.getPeriodStartDate()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(payment.getPeriodEndDate()) + ")";
            }
            else
            {
                datePeriod = " (" + CommonBaseDateUtil.getMonthNameDeclined(payment.getPaymentMonth(), GrammaCase.NOMINATIVE) + " " + payment.getPaymentYear() + " г.)";
            }

            String paymentInfo = payment.getType().getTitle() + " - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(payment.getPaymentSumInRuble()) + " руб." + datePeriod;
            paymentsInfo.add(paymentInfo);
        }
        model.setPayments(StringUtils.join(paymentsInfo, "; "));
        if (model.getExtract().isAgreePayments())
        {
            model.setResponsibleForPayments(EmployeePostDecl.getEmployeeStrInst(model.getExtract().getResponsibleForPayments()));
            model.setResponsibleForAgreement(EmployeePostDecl.getEmployeeStrDat(model.getExtract().getResponsibleForAgreement()));
        }
        else
        {
            model.setResponsibleForPayments(EmployeePostDecl.getEmployeeStrDat(model.getExtract().getResponsibleForPayments()));
        }
    }
}
