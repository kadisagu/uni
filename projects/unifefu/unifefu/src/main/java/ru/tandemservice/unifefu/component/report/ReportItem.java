/* $Id$ */
package ru.tandemservice.unifefu.component.report;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.entrant.gen.EntrantIndividualProgressGen;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.unifefu.entity.FefuEntrantContract;

import java.util.*;

/**
 * @author Ekaterina Zvereva
 * @since 29.07.2014
 */
public class ReportItem
{
    private final RequestedEnrollmentDirection _requestedDirection;
    private List<RequestedEnrollmentDirection> _otherRequestedDirection;
    private double _sumMark = 0;
    private Set<EntrantIndividualProgress> _individualProgresses;
    private int _individualProgressSumMark = 0;
    private List<PersonBenefit> _benefits;
    private Map<Long, ChosenEntranceDiscipline> _chosenDisciplinesByWay;
    private ChosenEntranceDiscipline[] chosenDistribution;
    private Map<Discipline2RealizationWayRelation, List<SubjectPassForm>> _passFormsByDiscipline;
    private RequestedEnrollmentDirection _origReqDir;
    private boolean passFormEge = false;
    private boolean passFormInner = false;
    private boolean recommended = false;
    private boolean preliminary = false;
    private List<FefuEntrantContract> _contracts;
    private List<EntrantEnrolmentRecommendation> _enrolmentRecommendations;


    public ReportItem(RequestedEnrollmentDirection requestedDirection)
    {
        _requestedDirection = requestedDirection;
    }

    public RequestedEnrollmentDirection getRequestedDirection()
    {
        return _requestedDirection;
    }

    public List<RequestedEnrollmentDirection> getOtherRequestedDirection()
    {
        return _otherRequestedDirection;
    }

    public void setOtherRequestedDirection(List<RequestedEnrollmentDirection> otherRequestedDirection)
    {
        _otherRequestedDirection = otherRequestedDirection;
    }

    public Set<EntrantIndividualProgress> getIndividualProgresses()
    {
        return _individualProgresses;
    }

    public void setIndividualProgresses(Set<EntrantIndividualProgress> individualProgresses)
    {
        _individualProgresses = individualProgresses;
        if (_individualProgresses != null)
            _individualProgressSumMark = _individualProgresses.stream().mapToInt(EntrantIndividualProgressGen::getMark).sum();
    }

    public int getIndividualProgressSumMark()
    {
        return _individualProgressSumMark;
    }

    public double getFinalSumMark()
    {
        return _requestedDirection.getEnrollmentDirection().getEnrollmentCampaign().isUseIndividualProgressInAmountMark() ?
                _sumMark + _individualProgressSumMark : _sumMark;
    }

    public double getSumMark()
    {
        return _sumMark;
    }

    public void setSumMark(double sumMark)
    {
        _sumMark = sumMark;
    }

    public List<PersonBenefit> getBenefits()
    {
        return _benefits;
    }

    public void setBenefits(List<PersonBenefit> benefits)
    {
        this._benefits = benefits;
    }

    public Map<Long, ChosenEntranceDiscipline> getChosenDisciplinesByWay()
    {
        return _chosenDisciplinesByWay;
    }

    public void setChosenDisciplinesByWay(Map<Long, ChosenEntranceDiscipline> chosenDisciplinesByWay)
    {
        _chosenDisciplinesByWay = chosenDisciplinesByWay;
    }

    public ChosenEntranceDiscipline[] getChosenDistribution()
    {
        return chosenDistribution;
    }

    public void setChosenDistribution(ChosenEntranceDiscipline[] chosenDistribution)
    {
        this.chosenDistribution = chosenDistribution;
    }

    public Map<Discipline2RealizationWayRelation, List<SubjectPassForm>> getPassFormsByDiscipline()
    {
        return _passFormsByDiscipline;
    }

    public void setPassFormsByDiscipline(Map<Discipline2RealizationWayRelation, List<SubjectPassForm>> passFormsByDiscipline)
    {
        _passFormsByDiscipline = passFormsByDiscipline;
    }

    public RequestedEnrollmentDirection getOrigReqDir()
    {
        return _origReqDir;
    }

    public void setOrigReqDir(RequestedEnrollmentDirection origReqDir)
    {
        this._origReqDir = origReqDir;
    }

    public void setPassFormEge(boolean passFormEge)
    {
        this.passFormEge = passFormEge;
    }

    public void setPassFormInner(boolean passFormInner)
    {
        this.passFormInner = passFormInner;
    }

    public String getPassForm(String passFormEgeStr, String passFormInnerStr)
    {
        return (passFormEge ? passFormEgeStr : "") + ((passFormEge && passFormInner) ? "/ " : "") + (passFormInner ? passFormInnerStr : "");
    }

    public boolean isPreliminary()
    {
        return preliminary;
    }

    public void setPreliminary(boolean preliminary)
    {
        this.preliminary = preliminary;
    }

    public boolean isRecommended()
    {
        return recommended;
    }

    public void setRecommended(boolean recommended)
    {
        this.recommended = recommended;
    }

    public List<FefuEntrantContract> getContracts()
    {
        return _contracts;
    }

    public void setContracts(List<FefuEntrantContract> contracts)
    {
        _contracts = contracts;
    }

    public List<EntrantEnrolmentRecommendation> getEnrolmentRecommendations()
    {
        return _enrolmentRecommendations;
    }

    public void setEnrolmentRecommendations(List<EntrantEnrolmentRecommendation> enrolmentRecommendations)
    {
        _enrolmentRecommendations = enrolmentRecommendations;
    }

    //---------
    public static class ItemComparator implements Comparator<ReportItem>
    {
        private Map<CompetitionKind, Integer> _competitionKindPriorities;
        private Map<Integer, List<Long>> _entranceDisciplineSortedByPriority;

        /**
         * @param competitionKindPriorities приоритеты видов конкурса
         */
        public ItemComparator(Map<CompetitionKind, Integer> competitionKindPriorities, Map<Integer, List<Long>> entranceDisciplineSortedByPriority)
        {
            _competitionKindPriorities = competitionKindPriorities;
            _entranceDisciplineSortedByPriority = entranceDisciplineSortedByPriority;
        }

        @Override
        public int compare(ReportItem o1, ReportItem o2)
        {
            // получаем выбранные направления
            RequestedEnrollmentDirection r1 = o1.getRequestedDirection();
            RequestedEnrollmentDirection r2 = o2.getRequestedDirection();

            int result;

            // сравнить по приоритетам видов конкурса
            result = compareCompetitionKind(r1, r2);
            if (result != 0) return result;

            // Сумма баллов
            result = Double.compare(o2.getFinalSumMark(), o1.getFinalSumMark());
            if (result != 0) return result;
            if (r1.getEnrollmentDirection().getEnrollmentCampaign().isUseIndividualProgressInAmountMark())
            {
                result = Double.compare(o2.getSumMark(), o1.getSumMark());
                if (result != 0) return result;
            }

            // по баллам за дисциплину с наивысшим приоритетом
            result = compateChusens(o1, o2);
            if (result != 0) return result;

            // преимущественное право
            result = Boolean.compare(CollectionUtils.isEmpty(o1.getEnrolmentRecommendations()), CollectionUtils.isEmpty(o2.getEnrolmentRecommendations()));
            if (result != 0) return result;

            // льготы
            result = Boolean.compare(CollectionUtils.isEmpty(o1.getBenefits()), CollectionUtils.isEmpty(o2.getBenefits()));
            if (result != 0) return result;

            // все осмысленное кончилось, теперь по фио
            return CommonCollator.RUSSIAN_COLLATOR.compare(r1.getEntrantRequest().getEntrant().getFio(), r2.getEntrantRequest().getEntrant().getFio());
        }


        /**
         * по баллам за дисциплину с наивысшим приоритетом
         */
        public int compateChusens(ReportItem o1, ReportItem o2)
        {
            if (_entranceDisciplineSortedByPriority == null || _entranceDisciplineSortedByPriority.size() == 0)
                return 0;

            Map<Long, ChosenEntranceDiscipline> chosens1 = o1.getChosenDisciplinesByWay();
            Map<Long, ChosenEntranceDiscipline> chosens2 = o2.getChosenDisciplinesByWay();

            if (chosens1 == null || chosens1.isEmpty())
            {
                if (chosens2 == null || chosens2.isEmpty()) return 0;
                else return -1;
            }
            else if (chosens2 == null || chosens2.isEmpty()) return 1;

            int result;
            for (Map.Entry<Integer, List<Long>> entry : _entranceDisciplineSortedByPriority.entrySet())
            {
                List<Long> disciplines = entry.getValue();
                ChosenEntranceDiscipline cho1 = null;
                ChosenEntranceDiscipline cho2 = null;

                for (Long discipline : disciplines)
                {
                    if (cho1 == null) cho1 = chosens1.get(discipline);
                    if (cho2 == null) cho2 = chosens2.get(discipline);
                    if (cho1 != null && cho2 != null) break;
                }

                if (cho1 == null)
                {
                    if (cho2 == null) continue;
                    else return -1;
                }
                else if (cho2 == null) return 1;

                result = Double.compare(cho2.getFinalMark() == null ? 0 : cho2.getFinalMark(),
                                        cho1.getFinalMark() == null ? 0 : cho1.getFinalMark());
                if (result != 0) return result;
            }

            return 0;
        }

        /**
         * Сравнение по приоритетам "Видов конкурса"
         * Для "Целевого приема" приоритет берется по ключу null
         */
        public int compareCompetitionKind(RequestedEnrollmentDirection r1, RequestedEnrollmentDirection r2)
        {

            Integer priority1 = _competitionKindPriorities.get(r1.isTargetAdmission()? null: r1.getCompetitionKind());
            Integer priority2 = _competitionKindPriorities.get(r2.isTargetAdmission()? null: r2.getCompetitionKind());
            return Integer.compare(priority1, priority2);
        }
    }
}