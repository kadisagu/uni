/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.EmployeeSpeciality;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.NsiDatagramUtil;
import ru.tandemservice.unifefu.ws.nsi.datagram.AcademicRankType;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanAcademicRankType;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanType;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.ScienceDiplomaWrapper;

/**
 * @author Dmitry Seleznev
 * @since 26.01.2015
 */
public class HumanAcademicRankTypeReactor extends SimpleCatalogEntityNewReactor<HumanAcademicRankType, PersonAcademicStatus>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return false;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<PersonAcademicStatus> getEntityClass()
    {
        return PersonAcademicStatus.class;
    }

    @Override
    public Class<HumanAcademicRankType> getNSIEntityClass()
    {
        return HumanAcademicRankType.class;
    }

    @Override
    public HumanAcademicRankType getCatalogElementRetrieveRequestObject(String guid)
    {
        // TODO: Имеет смысл переопределить метод получения полного списка ученых степеней физ лица из НСИ, поскольку их может быть очень много.
        HumanAcademicRankType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanAcademicRankType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public HumanAcademicRankType createEntity(CoreCollectionUtils.Pair<PersonAcademicStatus, FefuNsiIds> entityPair)
    {
        HumanAcademicRankType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanAcademicRankType();

        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setHumanAcademicRankDate(NsiDatagramUtil.formatDate(entityPair.getX().getDate()));
        nsiEntity.setHumanAcademicDiplomaNumber(NsiDatagramUtil.formatScienceDiploma(entityPair.getX()));
        nsiEntity.setHumanAcademicRankOrganization(entityPair.getX().getIssuancePlace());
        //nsiEntity.setHumanAcademicOKSOID(); we store this param as a string, not as a link

        CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonIdToEntityMap().get(String.valueOf(entityPair.getX().getPerson().getId()));
        if (null != personPair && null != personPair.getY())
        {
            HumanAcademicRankType.HumanID humanId = new HumanAcademicRankType.HumanID();
            HumanType human = new HumanType();
            human.setID(personPair.getY().getGuid());
            humanId.setHuman(human);
            nsiEntity.setHumanID(humanId);
        }

        if (null != entityPair.getX().getAcademicStatus())
        {
            CoreCollectionUtils.Pair<ScienceStatus, FefuNsiIds> degreePair = NsiObjectsHolder.getScienceStatusMap().get(String.valueOf(entityPair.getX().getAcademicStatus().getId()));
            if (null != degreePair && null != degreePair.getY())
            {
                HumanAcademicRankType.AcademicRankID rankId = new HumanAcademicRankType.AcademicRankID();
                AcademicRankType rank = new AcademicRankType();
                rank.setID(degreePair.getY().getGuid());
                rankId.setAcademicRank(rank);
                nsiEntity.setAcademicRankID(rankId);
            }
        }

        return nsiEntity;
    }

    @Override
    public PersonAcademicStatus createEntity(HumanAcademicRankType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getHumanID()) return null;

        PersonAcademicStatus entity = new PersonAcademicStatus();
        entity.setDate(NsiDatagramUtil.parseDate(nsiEntity.getHumanAcademicRankDate()));
        entity.setIssuancePlace(nsiEntity.getHumanAcademicRankOrganization());

        if (null != StringUtils.trimToNull(nsiEntity.getHumanAcademicDiplomaNumber()))
        {
            ScienceDiplomaWrapper wrapper = NsiDatagramUtil.parseScienceDiplomaWrapper(nsiEntity.getHumanAcademicDiplomaNumber());
            if (null != wrapper)
            {
                entity.setSeria(wrapper.getSeria());
                entity.setNumber(wrapper.getNumber());
                entity.setIssuanceDate(wrapper.getDate());
            }
        }

        if (null != nsiEntity.getHumanID() && null != nsiEntity.getHumanID().getHuman())
        {
            CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonIdToEntityMap().get(nsiEntity.getHumanID().getHuman().getID());
            if (null != personPair && null != personPair.getX()) entity.setPerson(personPair.getX());
        }

        if (null != nsiEntity.getAcademicRankID() && null != nsiEntity.getAcademicRankID().getAcademicRank())
        {
            CoreCollectionUtils.Pair<ScienceStatus, FefuNsiIds> degreePair = NsiObjectsHolder.getScienceStatusMap().get(nsiEntity.getAcademicRankID().getAcademicRank().getID());
            if (null != degreePair && null != degreePair.getX()) entity.setAcademicStatus(degreePair.getX());
        }

        if (null != nsiEntity.getHumanAcademicOKSOID() && null != nsiEntity.getHumanAcademicOKSOID().getOKSO())
        {
            CoreCollectionUtils.Pair<EmployeeSpeciality, FefuNsiIds> specialityPair = NsiObjectsHolder.getEmployeeSpecialityMap().get(nsiEntity.getHumanAcademicOKSOID().getOKSO().getID());
            if (null != specialityPair && null != specialityPair.getX())
                entity.setScienceSpeciality(specialityPair.getX().getTitle());
        }

        return entity;
    }

    @Override
    public PersonAcademicStatus updatePossibleDuplicateFields(HumanAcademicRankType nsiEntity, CoreCollectionUtils.Pair<PersonAcademicStatus, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), PersonAcademicStatus.date().s()))
                entityPair.getX().setDate(NsiDatagramUtil.parseDate(nsiEntity.getHumanAcademicRankDate()));

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), PersonAcademicStatus.issuancePlace().s()))
                entityPair.getX().setIssuancePlace(nsiEntity.getHumanAcademicRankOrganization());

            if (null != StringUtils.trimToNull(nsiEntity.getHumanAcademicDiplomaNumber()))
            {
                ScienceDiplomaWrapper wrapper = NsiDatagramUtil.parseScienceDiplomaWrapper(nsiEntity.getHumanAcademicDiplomaNumber());
                if (null != wrapper)
                {
                    if ((null == entityPair.getX().getSeria() ^ null == wrapper.getSeria()) || !entityPair.getX().getSeria().trim().toLowerCase().equals(wrapper.getSeria().trim().toLowerCase()))
                        entityPair.getX().setSeria(wrapper.getSeria());

                    if ((null == entityPair.getX().getNumber() ^ null == wrapper.getNumber()) || !entityPair.getX().getNumber().trim().toLowerCase().equals(wrapper.getNumber().trim().toLowerCase()))
                        entityPair.getX().setNumber(wrapper.getNumber());

                    if ((null == entityPair.getX().getIssuanceDate() ^ null == wrapper.getDate()) || entityPair.getX().getIssuanceDate().getTime() != wrapper.getDate().getTime())
                        entityPair.getX().setIssuanceDate(wrapper.getDate());
                }
            }

            if (null != nsiEntity.getHumanID() && null != nsiEntity.getHumanID().getHuman())
            {
                CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonIdToEntityMap().get(nsiEntity.getHumanID().getHuman().getID());
                if (null != personPair && null != personPair.getX()) entityPair.getX().setPerson(personPair.getX());
            }

            if (null != nsiEntity.getAcademicRankID() && null != nsiEntity.getAcademicRankID().getAcademicRank())
            {
                CoreCollectionUtils.Pair<ScienceStatus, FefuNsiIds> degreePair = NsiObjectsHolder.getScienceStatusMap().get(nsiEntity.getAcademicRankID().getAcademicRank().getID());
                if (null != degreePair && null != degreePair.getX())
                    entityPair.getX().setAcademicStatus(degreePair.getX());
            }

            if (null != nsiEntity.getHumanAcademicOKSOID() && null != nsiEntity.getHumanAcademicOKSOID().getOKSO())
            {
                CoreCollectionUtils.Pair<EmployeeSpeciality, FefuNsiIds> specialityPair = NsiObjectsHolder.getEmployeeSpecialityMap().get(nsiEntity.getHumanAcademicOKSOID().getOKSO().getID());
                if (null != specialityPair && null != specialityPair.getX())
                    entityPair.getX().setScienceSpeciality(specialityPair.getX().getTitle());
            }
        }

        return entityPair.getX();
    }

    @Override
    public HumanAcademicRankType updateNsiEntityFields(HumanAcademicRankType nsiEntity, PersonAcademicStatus entity)
    {
        nsiEntity.setHumanAcademicRankDate(NsiDatagramUtil.formatDate(entity.getDate()));
        nsiEntity.setHumanAcademicDiplomaNumber(NsiDatagramUtil.formatScienceDiploma(entity));
        nsiEntity.setHumanAcademicRankOrganization(entity.getIssuancePlace());
        //nsiEntity.setHumanAcademicOKSOID(); we store this param as a string, not as a link

        CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonIdToEntityMap().get(String.valueOf(entity.getPerson().getId()));
        if (null != personPair && null != personPair.getY())
        {
            HumanAcademicRankType.HumanID humanId = new HumanAcademicRankType.HumanID();
            HumanType human = new HumanType();
            human.setID(personPair.getY().getGuid());
            humanId.setHuman(human);
            nsiEntity.setHumanID(humanId);
        }

        if (null != entity.getAcademicStatus())
        {
            CoreCollectionUtils.Pair<ScienceStatus, FefuNsiIds> degreePair = NsiObjectsHolder.getScienceStatusMap().get(String.valueOf(entity.getAcademicStatus().getId()));
            if (null != degreePair && null != degreePair.getY())
            {
                HumanAcademicRankType.AcademicRankID rankId = new HumanAcademicRankType.AcademicRankID();
                AcademicRankType rank = new AcademicRankType();
                rank.setID(degreePair.getY().getGuid());
                rankId.setAcademicRank(rank);
                nsiEntity.setAcademicRankID(rankId);
            }
        }

        return nsiEntity;
    }
}