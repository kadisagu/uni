/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu12.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 13.05.2013
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuEduTransferEnrolmentStuExtract, Model>
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected FefuEduTransferEnrolmentStuExtract createNewInstance()
    {
        return new FefuEduTransferEnrolmentStuExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        CommonExtractModel eduModel = MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract(), true);

        if (!model.isAddForm())
            model.setDebtsList(MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(model.getExtract()));

        if (model.getDebtsList().isEmpty())
            model.getDebtsList().add(new StuExtractToDebtRelation());

        if (model.isAddForm())
        {
            // Заполняем значения по умолчания - текущие данные студента
            Student student = model.getExtract().getEntity();
            EducationOrgUnit orgunit = student.getEducationOrgUnit();

            eduModel.setFormativeOrgUnit(orgunit.getFormativeOrgUnit());
            eduModel.setTerritorialOrgUnit(orgunit.getTerritorialOrgUnit());
            eduModel.setCourse(student.getCourse());
            eduModel.setGroup(student.getGroup());
            eduModel.setEducationLevelsHighSchool(orgunit.getEducationLevelHighSchool());
            eduModel.setDevelopForm(orgunit.getDevelopForm());
            eduModel.setDevelopCondition(orgunit.getDevelopCondition());
            eduModel.setDevelopTech(orgunit.getDevelopTech());
            eduModel.setDevelopPeriod(orgunit.getDevelopPeriod());
            eduModel.setCompensationType(student.getCompensationType());
        }
        model.setEduModel(eduModel);
        model.setStudentStatusNewStr(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE).getTitle());
    }

    @Override
    public void update(Model model)
    {
        //save rollback data
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        super.update(model);

        if (!model.getExtract().isLiquidateEduPlanDifference())
        {
            model.getExtract().setLiquidationDeadlineDate(null);
        }

        for (StuExtractToDebtRelation rel : model.getDebtsList())
        {
            if (model.getExtract().isLiquidateEduPlanDifference())
            {
                rel.setExtract(model.getExtract());
                saveOrUpdate(rel);
            }
            else if (null != rel.getId())
            {
                delete(rel);
            }
        }

        for (StuExtractToDebtRelation rel : model.getDebtsToDel())
        {
            if (null != rel.getId())
                delete(rel);
        }
    }

}