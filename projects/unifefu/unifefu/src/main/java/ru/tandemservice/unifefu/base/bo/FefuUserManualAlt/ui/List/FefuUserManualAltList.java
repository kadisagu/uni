/* $Id: FefuUserManualAltList.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuUserManualAlt.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.unifefu.base.bo.FefuUserManualAlt.logic.FefuUserManualAltUtil;
import ru.tandemservice.unifefu.entity.FefuUserManual;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Victor Nekrasov
 * @since 28.04.2014
 */
@Configuration
public class FefuUserManualAltList extends BusinessComponentManager
{
    public static final String FEFU_USER_MANUAL_ALT_SEARCH_DS = "fefuUserManualAltSearchDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(FEFU_USER_MANUAL_ALT_SEARCH_DS, fefuUserManualAltSearchDSColumns(), fefuUserManualAltSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint fefuUserManualAltSearchDSColumns()
    {
        return columnListExtPointBuilder(FEFU_USER_MANUAL_ALT_SEARCH_DS)
                .addColumn(textColumn("title", FefuUserManual.title()).order())
                .addColumn(dateColumn("editDate", FefuUserManual.editDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn("comment",FefuUserManual.comment()).order())
                .addColumn(actionColumn("download", new Icon("template_save"), "onClickDownload"))// прав на скачивание нет, т.к. в списке отображаются только те элементы на которые у пользователя есть права
                .addColumn(actionColumn("edit", new Icon("template_add"), "onClickEdit").permissionKey("fefuUserManualAlt_edit, fefuUserManualAlt_admin"))
                .addColumn(actionColumn("delete", new Icon("delete"), "onClickDelete").alert(alert("fefuUserManualAltSearchDS.delete.alert", FefuUserManual.title().s())).permissionKey("fefuUserManualAlt_delete, fefuUserManualAlt_admin").disabled("ui:deleteDisabled"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> fefuUserManualAltSearchDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), FefuUserManual.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final String title = context.get("title");
                boolean userManual_edit = CoreServices.securityService().check(CoreServices.securityService().getCommonSecurityObject(), UserContext.getInstance().getPrincipalContext(), "fefuUserManualAlt_edit");
                boolean userManual_admin = CoreServices.securityService().check(CoreServices.securityService().getCommonSecurityObject(), UserContext.getInstance().getPrincipalContext(), "fefuUserManualAlt_admin");
                if (userManual_edit || userManual_admin)
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuUserManual.class, "b").column(property("b"));
                    if (title != null)
                        builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property("b", FefuUserManual.title())), DQLExpressions.value(CoreStringUtils.escapeLike(title, true))));

                    if (input.getEntityOrder().getColumnName().equals("comment"))
                    {
                        final List<FefuUserManual> resultList = builder.createStatement(context.getSession()).list();
                        Collections.sort(resultList, new EntityComparator<>(input.getEntityOrder()));
                        return ListOutputBuilder.get(input, resultList).pageable(true).build();
                    }

                    new DQLOrderDescriptionRegistry(FefuUserManual.class, "b").applyOrder(builder, input.getEntityOrder());
                    return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
                }

                List<Long> acceptCodeList = new ArrayList<>();
                for (FefuUserManual userManual : new DQLSelectBuilder().fromEntity(FefuUserManual.class, "b").createStatement(context.getSession()).<FefuUserManual>list())
                    if (CoreServices.securityService().check(CoreServices.securityService().getCommonSecurityObject(), UserContext.getInstance().getPrincipalContext(), FefuUserManualAltUtil.getPermissionKeyDownload(userManual)))
                        acceptCodeList.add(userManual.getId());

                final List<FefuUserManual> resultList = new ArrayList<>();
                BatchUtils.execute(acceptCodeList, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuUserManual.class, "b").column(property("b"))
                            .where(in(property(FefuUserManual.id().fromAlias("b")), elements));
                    if (title != null)
                        builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property("b", FefuUserManual.title())), DQLExpressions.value(CoreStringUtils.escapeLike(title, true))));
                    resultList.addAll(builder.createStatement(context.getSession()).<FefuUserManual>list());
                });

                Collections.sort(resultList, new EntityComparator<>(input.getEntityOrder()));
                return ListOutputBuilder.get(input, resultList).pageable(true).build();
            }
        };
    }
}
