/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu15.AddEdit;

import org.joda.time.LocalDate;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;

/**
 * @author nvankov
 * @since 11/15/13
 */
public class Controller extends CommonModularStudentExtractAddEditController<FullStateMaintenanceEnrollmentStuExtract, IDAO, Model>
{
    public void onPaymentTypeChange(IBusinessComponent component)
    {
        Model model = getModel(component);
        int index = component.getListenerParameter();
        FefuOrphanPayment payment = model.getPayments().get(index);
        FefuOrphanPaymentType type = model.getPayments().get(index).getType();
        if(null != type)
            payment.setPrint(type.getPrint());
    }

    public void onClickAddPayment(IBusinessComponent component)
    {
        Model model = getModel(component);
        addPayment(model);
    }

    private void addPayment(Model model)
    {
        FefuOrphanPayment newPayment = new FefuOrphanPayment();
        newPayment.setExtract(model.getExtract());
        model.getPayments().add(newPayment);
    }

    public void onClickDeletePayment(IBusinessComponent component)
    {
        Model model = getModel(component);
        int index = component.getListenerParameter();
        model.getPayments().remove(index);
        if(model.getPayments().isEmpty()) addPayment(model);

    }
}
