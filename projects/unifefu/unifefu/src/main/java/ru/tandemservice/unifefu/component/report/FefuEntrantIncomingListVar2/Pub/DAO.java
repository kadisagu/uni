/* $Id$ */
package ru.tandemservice.unifefu.component.report.FefuEntrantIncomingListVar2.Pub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report;

/**
 * @author Igor Belanov
 * @since 25.07.2016
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReport(getNotNull(FefuEntrantIncomingListVar2Report.class, model.getReport().getId()));
    }
}
