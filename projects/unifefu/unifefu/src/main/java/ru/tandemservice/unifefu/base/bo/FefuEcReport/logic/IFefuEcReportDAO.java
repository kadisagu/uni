/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unifefu.base.vo.FefuEntrantOriginalsVerificationReportVO;

/**
 * @author Nikolay Fedorovskih
 * @since 10.07.2013
 */
public interface IFefuEcReportDAO extends INeedPersistenceSupport
{
    Long createReport(IFefuEcReportBuilder reportBuilder, IFefuEntrantReport report);

    EnrollmentDirection getSelectedEnrollmentDirection(FefuEntrantOriginalsVerificationReportVO reportVO);
}