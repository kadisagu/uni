package ru.tandemservice.unifefu.component.student.FefuLotusStudentDataEdit;

import org.tandemframework.core.component.Input;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

import java.util.Date;

/**
 * @author amakarova
 * @since 14.10.2013
 */
@Input(keys = "studentId", bindings = "studentId")
public class Model
{
    private Long _studentId;
    private StudentFefuExt _studentFefuExt;
    private boolean _checkedLotus;
    private Date _checkedLotusDate;

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public StudentFefuExt getStudentFefuExt()
    {
        return _studentFefuExt;
    }

    public void setStudentFefuExt(StudentFefuExt studentFefuExt)
    {
        _studentFefuExt = studentFefuExt;
    }

    public boolean isCheckedLotus() {
        return _checkedLotus;
    }

    public void setCheckedLotus(final boolean checkedLotus) {
        _checkedLotus = checkedLotus;
    }

    public Date getCheckedLotusDate() {
        return _checkedLotusDate;
    }

    public void setCheckedLotusDate(Date checkedLotusDate) {
        _checkedLotusDate = checkedLotusDate;
    }
}