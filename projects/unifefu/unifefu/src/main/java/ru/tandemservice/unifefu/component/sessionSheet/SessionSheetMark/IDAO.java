/*$Id$*/
package ru.tandemservice.unifefu.component.sessionSheet.SessionSheetMark;

import ru.tandemservice.uni.dao.IUniBaseDao;

/**
 * @author DMITRY KNYAZEV
 * @since 08.09.2015
 */
public interface IDAO extends ru.tandemservice.unisession.component.sessionSheet.SessionSheetMark.IDAO, IUniBaseDao
{

}
