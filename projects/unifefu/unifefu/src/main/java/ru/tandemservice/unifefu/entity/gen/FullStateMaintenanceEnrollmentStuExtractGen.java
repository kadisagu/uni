package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О зачислении на полное государственное обеспечение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FullStateMaintenanceEnrollmentStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract";
    public static final String ENTITY_NAME = "fullStateMaintenanceEnrollmentStuExtract";
    public static final int VERSION_HASH = -1128505856;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENROLL_DATE = "enrollDate";
    public static final String P_HAS_PAYMENTS = "hasPayments";
    public static final String P_TOTAL_PAYMENT_SUM = "totalPaymentSum";
    public static final String P_PAYMENT_END_DATE = "paymentEndDate";

    private Date _enrollDate;     // Дата зачисления
    private boolean _hasPayments;     // Начислить выплаты
    private Long _totalPaymentSum;     // Итоговая сумма выплат (в сотых долях копейки)
    private Date _paymentEndDate;     // Дата окончания выплаты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата зачисления.
     */
    public Date getEnrollDate()
    {
        return _enrollDate;
    }

    /**
     * @param enrollDate Дата зачисления.
     */
    public void setEnrollDate(Date enrollDate)
    {
        dirty(_enrollDate, enrollDate);
        _enrollDate = enrollDate;
    }

    /**
     * @return Начислить выплаты. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasPayments()
    {
        return _hasPayments;
    }

    /**
     * @param hasPayments Начислить выплаты. Свойство не может быть null.
     */
    public void setHasPayments(boolean hasPayments)
    {
        dirty(_hasPayments, hasPayments);
        _hasPayments = hasPayments;
    }

    /**
     * @return Итоговая сумма выплат (в сотых долях копейки).
     */
    public Long getTotalPaymentSum()
    {
        return _totalPaymentSum;
    }

    /**
     * @param totalPaymentSum Итоговая сумма выплат (в сотых долях копейки).
     */
    public void setTotalPaymentSum(Long totalPaymentSum)
    {
        dirty(_totalPaymentSum, totalPaymentSum);
        _totalPaymentSum = totalPaymentSum;
    }

    /**
     * @return Дата окончания выплаты.
     */
    public Date getPaymentEndDate()
    {
        return _paymentEndDate;
    }

    /**
     * @param paymentEndDate Дата окончания выплаты.
     */
    public void setPaymentEndDate(Date paymentEndDate)
    {
        dirty(_paymentEndDate, paymentEndDate);
        _paymentEndDate = paymentEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FullStateMaintenanceEnrollmentStuExtractGen)
        {
            setEnrollDate(((FullStateMaintenanceEnrollmentStuExtract)another).getEnrollDate());
            setHasPayments(((FullStateMaintenanceEnrollmentStuExtract)another).isHasPayments());
            setTotalPaymentSum(((FullStateMaintenanceEnrollmentStuExtract)another).getTotalPaymentSum());
            setPaymentEndDate(((FullStateMaintenanceEnrollmentStuExtract)another).getPaymentEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FullStateMaintenanceEnrollmentStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FullStateMaintenanceEnrollmentStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FullStateMaintenanceEnrollmentStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    return obj.getEnrollDate();
                case "hasPayments":
                    return obj.isHasPayments();
                case "totalPaymentSum":
                    return obj.getTotalPaymentSum();
                case "paymentEndDate":
                    return obj.getPaymentEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    obj.setEnrollDate((Date) value);
                    return;
                case "hasPayments":
                    obj.setHasPayments((Boolean) value);
                    return;
                case "totalPaymentSum":
                    obj.setTotalPaymentSum((Long) value);
                    return;
                case "paymentEndDate":
                    obj.setPaymentEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                        return true;
                case "hasPayments":
                        return true;
                case "totalPaymentSum":
                        return true;
                case "paymentEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    return true;
                case "hasPayments":
                    return true;
                case "totalPaymentSum":
                    return true;
                case "paymentEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    return Date.class;
                case "hasPayments":
                    return Boolean.class;
                case "totalPaymentSum":
                    return Long.class;
                case "paymentEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FullStateMaintenanceEnrollmentStuExtract> _dslPath = new Path<FullStateMaintenanceEnrollmentStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FullStateMaintenanceEnrollmentStuExtract");
    }
            

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract#getEnrollDate()
     */
    public static PropertyPath<Date> enrollDate()
    {
        return _dslPath.enrollDate();
    }

    /**
     * @return Начислить выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract#isHasPayments()
     */
    public static PropertyPath<Boolean> hasPayments()
    {
        return _dslPath.hasPayments();
    }

    /**
     * @return Итоговая сумма выплат (в сотых долях копейки).
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract#getTotalPaymentSum()
     */
    public static PropertyPath<Long> totalPaymentSum()
    {
        return _dslPath.totalPaymentSum();
    }

    /**
     * @return Дата окончания выплаты.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract#getPaymentEndDate()
     */
    public static PropertyPath<Date> paymentEndDate()
    {
        return _dslPath.paymentEndDate();
    }

    public static class Path<E extends FullStateMaintenanceEnrollmentStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _enrollDate;
        private PropertyPath<Boolean> _hasPayments;
        private PropertyPath<Long> _totalPaymentSum;
        private PropertyPath<Date> _paymentEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract#getEnrollDate()
     */
        public PropertyPath<Date> enrollDate()
        {
            if(_enrollDate == null )
                _enrollDate = new PropertyPath<Date>(FullStateMaintenanceEnrollmentStuExtractGen.P_ENROLL_DATE, this);
            return _enrollDate;
        }

    /**
     * @return Начислить выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract#isHasPayments()
     */
        public PropertyPath<Boolean> hasPayments()
        {
            if(_hasPayments == null )
                _hasPayments = new PropertyPath<Boolean>(FullStateMaintenanceEnrollmentStuExtractGen.P_HAS_PAYMENTS, this);
            return _hasPayments;
        }

    /**
     * @return Итоговая сумма выплат (в сотых долях копейки).
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract#getTotalPaymentSum()
     */
        public PropertyPath<Long> totalPaymentSum()
        {
            if(_totalPaymentSum == null )
                _totalPaymentSum = new PropertyPath<Long>(FullStateMaintenanceEnrollmentStuExtractGen.P_TOTAL_PAYMENT_SUM, this);
            return _totalPaymentSum;
        }

    /**
     * @return Дата окончания выплаты.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract#getPaymentEndDate()
     */
        public PropertyPath<Date> paymentEndDate()
        {
            if(_paymentEndDate == null )
                _paymentEndDate = new PropertyPath<Date>(FullStateMaintenanceEnrollmentStuExtractGen.P_PAYMENT_END_DATE, this);
            return _paymentEndDate;
        }

        public Class getEntityClass()
        {
            return FullStateMaintenanceEnrollmentStuExtract.class;
        }

        public String getEntityName()
        {
            return "fullStateMaintenanceEnrollmentStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
