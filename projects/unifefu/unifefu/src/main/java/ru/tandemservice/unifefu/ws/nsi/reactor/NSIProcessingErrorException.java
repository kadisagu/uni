/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

/**
 * @author Dmitry Seleznev
 * @since 19.12.2013
 */
public class NSIProcessingErrorException extends Exception
{
    public NSIProcessingErrorException(String message)
    {
        super(message);
    }
}