/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.RelDegreeType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class RelDegreeTypeReactor extends SimpleCatalogEntityNewReactor<RelDegreeType, RelationDegree>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<RelationDegree> getEntityClass()
    {
        return RelationDegree.class;
    }

    @Override
    public Class<RelDegreeType> getNSIEntityClass()
    {
        return RelDegreeType.class;
    }

    @Override
    public RelDegreeType getCatalogElementRetrieveRequestObject(String guid)
    {
        RelDegreeType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createRelDegreeType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public RelDegreeType createEntity(CoreCollectionUtils.Pair<RelationDegree, FefuNsiIds> entityPair)
    {
        RelDegreeType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createRelDegreeType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setRelDegreeID(getUserCodeForNSIChecked(entityPair.getX().getUserCode()));
        nsiEntity.setRelDegreeName(entityPair.getX().getTitle());
        return nsiEntity;
    }

    @Override
    public RelationDegree createEntity(RelDegreeType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getRelDegreeName()) return null;

        RelationDegree entity = new RelationDegree();
        entity.setTitle(nsiEntity.getRelDegreeName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(RelationDegree.class));
        entity.setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getRelDegreeID()));
        return entity;
    }

    @Override
    public RelationDegree updatePossibleDuplicateFields(RelDegreeType nsiEntity, CoreCollectionUtils.Pair<RelationDegree, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), RelationDegree.title().s()))
                entityPair.getX().setTitle(nsiEntity.getRelDegreeName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), RelationDegree.userCode().s()))
                entityPair.getX().setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getRelDegreeID()));
        }
        return entityPair.getX();
    }

    @Override
    public RelDegreeType updateNsiEntityFields(RelDegreeType nsiEntity, RelationDegree entity)
    {
        nsiEntity.setRelDegreeName(entity.getTitle());
        nsiEntity.setRelDegreeID(getUserCodeForNSIChecked(entity.getUserCode()));
        return nsiEntity;
    }
}