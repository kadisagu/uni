
package ru.tandemservice.unifefu.component.listextract.fefu13.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative.IAbstractParagraphAddEditAlternativeDAO;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public interface IDAO extends IAbstractParagraphAddEditAlternativeDAO<FefuAdditionalAcademGrantStuListExtract, Model>
{
}
