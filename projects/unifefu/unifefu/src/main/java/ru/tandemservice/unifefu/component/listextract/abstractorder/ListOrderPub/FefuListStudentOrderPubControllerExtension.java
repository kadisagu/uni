/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.abstractorder.ListOrderPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.IListStudentOrderPubControllerExtension;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.unifefu.base.bo.Directum.DirectumManager;
import ru.tandemservice.unifefu.base.bo.Directum.logic.DirectumDAO;
import ru.tandemservice.unifefu.base.bo.Directum.ui.OrderLogView.DirectumOrderLogView;
import ru.tandemservice.unifefu.base.bo.Directum.ui.Send.DirectumSend;
import ru.tandemservice.unifefu.entity.ws.FefuStudentOrderExtension;
import ru.tandemservice.unifefu.ws.directum.FefuDirectumClient;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;

/**
 * @author Dmitry Seleznev
 * @since 18.07.2013
 */
public class FefuListStudentOrderPubControllerExtension implements IListStudentOrderPubControllerExtension
{
    @Override
    public void doSendToCoordinationAdditionalAction(IBusinessComponent component, AbstractStudentOrder order)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
            errCollector.add("Нельзя отправить приказ на согласование, так как он уже на согласовании.");

        /*if (StringUtils.isEmpty(order.getNumber()))
            errCollector.add("Нельзя отправить приказ на согласование без номера.");

        if (order.getCommitDate() == null)
            errCollector.add("Нельзя отправить приказ на согласование без даты приказа.");*/

        if (!errCollector.hasErrors())
        {
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(DirectumSend.class.getSimpleName(), new ParametersMap()
                            .add("orderId", order.getId()))
            );
        }
    }

    @Override
    public void doExecuteAdditionalAction(IBusinessComponent component, AbstractStudentOrder order)
    {
        component.getController().activateInRoot(component, new ComponentActivator(DirectumOrderLogView.class.getSimpleName(), new ParametersMap()
                        .add("orderId", order.getId()))
        );
    }

    // DEV-4592
    public boolean isDirectumScanUrlAvailable(Long orderId)
    {
        if (null != orderId)
        {
            FefuStudentOrderExtension ext = DataAccessServices.dao().get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.order().id(), orderId);
            return (null != ext && null != ext.getDirectumScanUrl());
        }
        return false;
    }

    // DEV-4592
    public String getDirectumScanUrl(Long orderId)
    {
        if (null != orderId)
        {
            FefuStudentOrderExtension ext = DataAccessServices.dao().get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.order().id(), orderId);
            return (null != ext && null != ext.getDirectumScanUrl()) ? ext.getDirectumScanUrl() : null;
        }
        return null;
    }

    // DEV-4697
    public boolean isDirectumTaskIdAvailable(Long orderId)
    {
        if (null != orderId)
        {
            FefuStudentOrderExtension ext = DataAccessServices.dao().get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.order().id(), orderId);
            return (null != ext && null != ext.getDirectumTaskId());
        }
        return false;
    }

    // DEV-4697
    public void onClickGetDirectumTaskId(Long orderId)
    {
        if (null != orderId)
        {
            FefuStudentOrderExtension ext = DataAccessServices.dao().get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.order().id(), orderId);
            String directumTaskId = (null != ext && null != ext.getDirectumTaskId()) ? ext.getDirectumTaskId() : null;
            StringBuilder linkFile = new StringBuilder("Version=ISB7\nSystemCode=dvfu\nComponentType=9\nID=");
            linkFile.append(null != directumTaskId ? directumTaskId : "").append("\nViewCode=");

            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
                    .contentType(DatabaseFile.CONTENT_TYPE_SOME_DATA)
                    .fileName((null != directumTaskId ? directumTaskId : "document") + ".isb")
                    .document(linkFile.toString().getBytes()), true);
        }
    }

    // DEV-4697
    public String getDirectumTaskUrl(Long orderId)
    {
        if (null != orderId)
        {
            AbstractStudentOrder order = DataAccessServices.dao().get(AbstractStudentOrder.class, orderId);
            if (null != order)
            {
                FefuStudentOrderExtension ext = DataAccessServices.dao().get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.order().id(), order.getId());
                return DirectumDAO.getDirectumBaseUrl() + ((null != ext && null != ext.getDirectumTaskId()) ? ext.getDirectumTaskId() : "");
            }
        }
        return null;
    }

    // DEV-4725
    public void onClickDirectumScanPdfPrint(Long orderId)
    {
        if (null != orderId)
        {
            FefuStudentOrderExtension ext = DataAccessServices.dao().get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.order().id(), orderId);
            if (null != ext && null != ext.getDirectumScanUrl())
            {
                String directumScanUrlID = ext.getDirectumScanUrl();
                // парсим ссылку на скан-копию Directum из расширений приказов, получаем ID скан-копии, отдаем клиенту pdf-документ
                directumScanUrlID = StringUtils.trimToNull((StringUtils.isNumeric(directumScanUrlID) ? directumScanUrlID : StringUtils.substringAfter(directumScanUrlID, "id=")));
                if (null != directumScanUrlID)
                {
                    String fileName = "DirectumScanOrder orderId" + directumScanUrlID + ".pdf";
                    BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(DatabaseFile.CONTENT_TYPE_APPLICATION_PDF).fileName(fileName).document(FefuDirectumClient.getOrderScan(directumScanUrlID)), true);
                }
            }
        }
    }

    public boolean isDirectumSendingOrderToAccept(Long orderId)
    {
        return DirectumManager.instance().dao().checkDirectumSendingOrderToAccept(orderId, false);
    }

    public boolean isValidateOrder(Long orderId)
    {
        return DirectumManager.instance().dao().checkDirectumSendingOrderToAccept(orderId, true);
    }

    public void onDeleteFromQueue(Long orderId)
    {
        DirectumManager.instance().dao().doDeleteSendingOrder(orderId);
    }
}