/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu14.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract;


/**
 * @author Ekaterina Zvereva
 * @since 12.11.2014
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuTransfAcceleratedTimeStuListExtract, Model>
{
}