/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppSchedule;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.logic.FefuScheduleEventsDAO;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.ISppScheduleEventsDAO;

/**
 * @author nvankov
 * @since 2/14/14
 */

@Configuration
public class SppScheduleExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public ISppScheduleEventsDAO eventsDao()
    {
        return new FefuScheduleEventsDAO();
    }
}
