/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e63;

import ru.tandemservice.movestudent.entity.ExcludeUnacceptedToGPDefenceStuExtract;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 21.11.2014
 */
public class ExcludeUnacceptedToGPDefenceStuExtractDao extends ru.tandemservice.movestudent.component.modularextract.e63.ExcludeUnacceptedToGPDefenceStuExtractDao
{

    @Override
    public void doCommit(ExcludeUnacceptedToGPDefenceStuExtract extract, Map parameters)
    {
        super.doCommit(extract, parameters);
        UnifefuDaoFacade.getFefuMoveStudentDAO().setStatusEndDateByCode(extract, UniFefuDefines.ADMITTED_TO_STATE_EXAMES, extract.getExcludeDate());
    }

    @Override
    public void doRollback(ExcludeUnacceptedToGPDefenceStuExtract extract, Map parameters)
    {
        super.doRollback(extract, parameters);

        UnifefuDaoFacade.getFefuMoveStudentDAO().revertCustomStateDates(extract);
    }
}