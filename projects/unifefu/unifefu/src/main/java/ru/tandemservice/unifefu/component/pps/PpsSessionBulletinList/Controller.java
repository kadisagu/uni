/*$Id$*/
package ru.tandemservice.unifefu.component.pps.PpsSessionBulletinList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

/**
 * @author DMITRY KNYAZEV
 * @since 07.09.2015
 */
public class Controller extends ru.tandemservice.unisession.component.pps.PpsSessionBulletinList.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        final ru.tandemservice.unisession.component.pps.PpsSessionBulletinList.Model model = this.getModel(component);
        final DynamicListDataSource<SessionBulletinDocument> dataSource = model.getDataSource();
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать"));
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        final IDAO dao = (IDAO) this.getDao();
        dao.doPrintBulletin(component.<Long>getListenerParameter());
    }
}
