/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.axis.AxisFault;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import ru.tandemservice.uniec_fis.util.IFisService;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow;
import ru.tandemservice.unifefu.ws.nsi.*;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.datagram.DepartmentType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;
import ru.tandemservice.unifefu.ws.nsi.datagram.ObjectFactory;
import ru.tandemservice.unifefu.ws.nsi.datagram.XDatagram;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;
import ru.tandemservice.unifefu.ws.nsi.servertest.ServiceSoapImplServiceLocator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.HandlerRegistry;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 09.08.2013
 */
public class NsiReactorUtils
{
    public static final int PACKAGE_LENGTH = 200;
    public static final int SMALL_PACKAGE_LENGTH = 50;
    public static final String OB_SUBSYSTEM_CODE = "OB";
    public static final ObjectFactory NSI_OBJECT_FACTORY = new ObjectFactory();
    public static final int[] SINGLE_NSI_REQUEST_TRY_SCHEDULE = new int[]{60000, 60000, 300000, 300000, 3600000, 14400000, 68400000};

    public static final String NSI_SERVICE_AUTH_ENABLED = "fefu.nsi.iis.auth.enabled";
    public static final String NSI_SERVICE_AUTH_USERNAME = "fefu.nsi.iis.auth.username";
    public static final String NSI_SERVICE_AUTH_PASSWORD = "fefu.nsi.iis.auth.password";
    public static final String NSI_IGNORE_IDENTITY_CARD = "fefu.nsi.identityCard.ignore";

    public static String generateGUID(IEntity entity)
    {
        return UUID.nameUUIDFromBytes(String.valueOf(entity.getId()).getBytes()).toString();
    }

    public static String generateGUID(Long entityId)
    {
        return UUID.nameUUIDFromBytes(String.valueOf(entityId).getBytes()).toString();
    }

    /**
     * Общий метод для получения списка элементов из НСИ для конкретного справочника, либо конкретного одиночного элемента справочника НСИ по типу, либо GUID'у.
     *
     * @param elementToRetrieve - Некоторый элемент НСИ для запроса. Это может быть как совсем пустой элемент определенного типа
     *                          (вернётся полный список элементов соответствующего справочника),
     *                          так и один элемент определенного типа, в котором заполнен только GUID
     *                          (вернётся элемент, соответствующий GUID'у)
     * @return - список элементов, полученных из НСИ
     * @throws RuntimeException - В случае ошибок получения элементов будет пробрасываться эксепшн
     */
    public static <T> List<T> retrieveObjectsFromNsiBySingleElement(Class<T> clazz, T elementToRetrieve) throws RuntimeException
    {
        if (null == elementToRetrieve) return new ArrayList<>();
        return retrieveObjectsFromNSIByElementsList(clazz, Collections.singletonList(elementToRetrieve));
    }

    /**
     * Общий метод для получения списка элементов из НСИ.
     *
     * @param elementsToRetrieve - Некоторые элементы НСИ для запроса. Это могут быть как совсем пустые элементы определенных типов
     *                           (вернётся полный список элементов соответствующих справочников),
     *                           так элементы определенного типа, в которых заполнены только GUID'ы
     *                           (вернутся элементы, соответствующих GUID'ам)
     * @return - список элементов, полученных из НСИ
     * @throws RuntimeException - В случае ошибок получения элементов будет пробрасываться эксепшн
     */
    public static <T> List<T> retrieveObjectsFromNSIByElementsList(final Class<T> clazz, List<T> elementsToRetrieve) throws RuntimeException
    {
        return retrieveObjectsFromNSIByElementsList(clazz, elementsToRetrieve, PACKAGE_LENGTH);
    }

    /**
     * Подготавливает адаптер для работы с сервисами НСИ.
     * Способен учитывать настройку простой аутентификации на IIS стендов НСИ.
     *
     * @return - адаптер к вэб-сервисам НСИ
     */
    public static ServiceSoap_Service getServiceProvider()
    {
        ServiceSoap_Service service = new ServiceSoap_ServiceLocator();

        String authEnabledStr = ApplicationRuntime.getProperty(NSI_SERVICE_AUTH_ENABLED);
        if (null != authEnabledStr && Boolean.parseBoolean(authEnabledStr))
        {
            String authUsername = ApplicationRuntime.getProperty(NSI_SERVICE_AUTH_USERNAME);
            String authPassword = ApplicationRuntime.getProperty(NSI_SERVICE_AUTH_PASSWORD);

            if (null != authUsername && null != authPassword)
            {
                HandlerRegistry registry = service.getHandlerRegistry();
                QName servicePort = new QName("http://www.qa.com", "ServiceSoap12");
                List handlerChain = registry.getHandlerChain(servicePort);
                HandlerInfo info = new HandlerInfo();
                info.setHandlerClass(NsiSecurityHandler.class);
                handlerChain.add(info);

            }
        }

        return service;
    }

    /**
     * Возвращает настройку "Игнорировать сведения об УЛ из НСИ".
     */
    public static boolean isIdentityCardIgnore()
    {
        String ignoreIdentityCardStr = ApplicationRuntime.getProperty(NSI_IGNORE_IDENTITY_CARD);
        return null != ignoreIdentityCardStr && Boolean.parseBoolean(ignoreIdentityCardStr);
    }

    /**
     * Общий метод для получения списка элементов из НСИ.
     *
     * @param elementsToRetrieve - Некоторые элементы НСИ для запроса. Это могут быть как совсем пустые элементы определенных типов
     *                           (вернётся полный список элементов соответствующих справочников),
     *                           так элементы определенного типа, в которых заполнены только GUID'ы
     *                           (вернутся элементы, соответствующих GUID'ам)
     * @param packageLength      - размер порции элементов, одновременно высылаемых запросом к НСИ
     * @return - список элементов, полученных из НСИ
     * @throws RuntimeException - В случае ошибок получения элементов будет пробрасываться эксепшн
     */
    public static <T> List<T> retrieveObjectsFromNSIByElementsList(final Class<T> clazz, List<T> elementsToRetrieve, int packageLength) throws RuntimeException
    {
        final ServiceSoap_Service service = getServiceProvider();
        final List<T> result = new ArrayList<>();

        /*TODO del*/
        /*FileWriter out1 = null;
        try{
            out1 = new FileWriter("f:/posts.xml"); //TODO del
        }catch (Exception e) {}
        final FileWriter out = null != out1 ? out1 : null;*/
        /*TODO del*/

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Retrieving elements for " + clazz.getSimpleName() + " from NSI ----------");
        BatchUtils.execute(elementsToRetrieve, packageLength, new BatchUtils.Action<T>()
        {
            String parentMessageId = null;

            @Override
            public void execute(final Collection<T> elementsPortion)
            {
                Long rowId = null;
                Integer portionCount = null;
                try
                {
                    ServiceSoap_PortType port = service.getServiceSoap12();
                    ServiceRequestType request = new ServiceRequestType();
                    RoutingHeaderType header = new RoutingHeaderType();
                    ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
                    XDatagram xDatagram = NSI_OBJECT_FACTORY.createXDatagram();
                    xDatagram.getEntityList().addAll(elementsPortion);

                    ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
                    MessageElement datagramOut = new SOAPBodyElement(inStream);

                    header.setOperationType(RoutingHeaderTypeOperationType.fromValue("retrieve"));
                    header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                    header.setSourceId("OB");
                    if (null == parentMessageId) parentMessageId = header.getMessageId();
                    else header.setParentId(parentMessageId);

                    request.setRoutingHeader(header);
                    datagram.set_any(new MessageElement[]{datagramOut});
                    request.setDatagram(datagram);

                    rowId = IFefuNsiSyncDAO.instance.get().doInitLogNSIOutcomingAction(header, xDatagram, null, null);
                    rowId = IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, header, xDatagram, null, null, null);
                    ServiceResponseType response = port.retrieve(request);

                    if (null != response.getDatagram())
                    {
                        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;
                        //if (null != out) out.write(response.getDatagram().get_any()[0].toString()); //TODO del

                        if (null != respMsg && response.getCallCC().getValue().intValue() == 0)
                        {
                            portionCount = 0;
                            XDatagram respDatagram = fromXml(XDatagram.class, respMsg.getAsString().getBytes());
                            IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, null, respDatagram, response, null, null);
                            for (Object retrievedObject : respDatagram.getEntityList())
                            {
                                if (clazz.isInstance(retrievedObject))
                                {
                                    result.add((T) retrievedObject);
                                    portionCount++;
                                }
                            }
                        }
                    } else
                    {
                        StringBuilder err = new StringBuilder("||||||||| Empty Datagram was received while retrieving elements from NSI:\n");
                        err.append("\t\tCallCC = ").append(response.getCallCC().getValue().intValue());
                        err.append("\t\tCallRC = ").append(response.getCallRC());
                        FefuNsiSyncDAO.logEvent(Level.ERROR, err.toString());
                        IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, header, xDatagram, null, new UnsupportedOperationException(response.getCallRC()), err.toString());
                    }

                } catch (final Throwable t)
                {
                    StringBuilder err = new StringBuilder("||||||||| Some ERROR has occured while retrieving elements from NSI:\n");
                    if (t instanceof AxisFault)
                    {
                        err.append("\t\tFaultMessage = ").append(t.getMessage()).append("\n");
                        err.append("\t\tFaultString = ").append(((AxisFault) t).getFaultString()).append("\n");
                        err.append("\t\tFaultReason = ").append(((AxisFault) t).getFaultReason()).append("\n");
                        err.append("\t\tFaultDump = ").append(((AxisFault) t).dumpToString()).append("\n");
                    } else err.append(t.getMessage());

                    IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, null, null, null, t, err.toString());
                    FefuNsiSyncDAO.logEvent(Level.ERROR, err.toString());
                    return;
                }

                FefuNsiSyncDAO.logEvent(Level.INFO, portionCount + (elementsPortion.size() == 1 ? " " : " more ") + "elements were retrieved from NSI");
            }
        });
        //try { out.close(); } catch (Exception e){} // TODO


        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Retrieving elements for " + clazz.getSimpleName() + " from NSI was finished ----------");
        return result;
    }

    public static void prepareNSIAction(Collection<Object> elementsToProcess, final String actionType, boolean useSmallPackage) throws RuntimeException
    {
        if (null == elementsToProcess || 0 == elementsToProcess.size()) return;

        final ServiceSoap_Service service = getServiceProvider();

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Prepare " + actionType + " for elements at NSI ----------");
        BatchUtils.execute(elementsToProcess, useSmallPackage ? SMALL_PACKAGE_LENGTH : PACKAGE_LENGTH, new BatchUtils.Action<Object>()
        {
            String parentMessageId = null;

            @Override
            public void execute(final Collection<Object> elementsPortion)
            {
                Long rowId = null;
                Integer portionCount = elementsPortion.size();
                try
                {
                    ServiceSoap_PortType port = service.getServiceSoap12();
                    ServiceRequestType request = new ServiceRequestType();
                    RoutingHeaderType header = new RoutingHeaderType();
                    ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
                    XDatagram xDatagram = NSI_OBJECT_FACTORY.createXDatagram();
                    xDatagram.getEntityList().addAll(elementsPortion);

                    ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
                    MessageElement datagramOut = new SOAPBodyElement(inStream);

                    header.setOperationType(RoutingHeaderTypeOperationType.fromValue(actionType));
                    header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                    header.setSourceId("OB");

                    if (null == parentMessageId) parentMessageId = header.getMessageId();
                    else header.setParentId(parentMessageId);

                    request.setRoutingHeader(header);
                    datagram.set_any(new MessageElement[]{datagramOut});
                    request.setDatagram(datagram);

                    // Логгируем пакет для дальнейшей обработки демоном, занимающимся отправкой пакетов в НСИ
                    //IFefuNsiSyncDAO.instance.get().doLogNSIOutcomingAction(xDatagram.getEntityList().get(0), header, xDatagram, null, null, null, null, false);
                    rowId = IFefuNsiSyncDAO.instance.get().doInitLogNSIOutcomingAction(header, xDatagram, null, null);
                } catch (final Throwable t)
                {
                    StringBuilder err = new StringBuilder("||||||||| Some ERROR has occured while preparing package for execute at NSI:\n");
                    if (t instanceof AxisFault)
                    {
                        err.append("\t\tFaultMessage = ").append(t.getMessage()).append("\n");
                        err.append("\t\tFaultString = ").append(((AxisFault) t).getFaultString()).append("\n");
                        err.append("\t\tFaultReason = ").append(((AxisFault) t).getFaultReason()).append("\n");
                        err.append("\t\tFaultDump = ").append(((AxisFault) t).dumpToString()).append("\n");
                    } else err.append(t.getMessage());

                    IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, null, null, null, t, err.toString());
                    FefuNsiSyncDAO.logEvent(Level.ERROR, err.toString());
                    throw CoreExceptionUtils.getRuntimeException(t);
                }

                FefuNsiSyncDAO.logEvent(Level.INFO, portionCount + (elementsPortion.size() == 1 ? " " : " more ") + "elements were prepared for execute at NSI");
            }
        });

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Preparing " + actionType + " for elements at NSI was finished ----------");
    }

    public static void executeNSIAction(Collection<Object> elementsToProcess, final String actionType, boolean useSmallPackage) throws RuntimeException
    {
        final ServiceSoap_Service service = getServiceProvider();

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Executing " + actionType + " for elements at NSI ----------");
        BatchUtils.execute(elementsToProcess, useSmallPackage ? SMALL_PACKAGE_LENGTH : PACKAGE_LENGTH, new BatchUtils.Action<Object>()
        {
            String parentMessageId = null;

            @Override
            public void execute(final Collection<Object> elementsPortion)
            {
                Long rowId = null;
                Integer portionCount = elementsPortion.size();
                try
                {
                    ServiceSoap_PortType port = service.getServiceSoap12();
                    ServiceRequestType request = new ServiceRequestType();
                    RoutingHeaderType header = new RoutingHeaderType();
                    ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
                    XDatagram xDatagram = NSI_OBJECT_FACTORY.createXDatagram();
                    xDatagram.getEntityList().addAll(elementsPortion);

                    ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
                    MessageElement datagramOut = new SOAPBodyElement(inStream);

                    header.setOperationType(RoutingHeaderTypeOperationType.fromValue(actionType));
                    header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                    header.setSourceId("OB");

                    if (null == parentMessageId) parentMessageId = header.getMessageId();
                    else header.setParentId(parentMessageId);

                    request.setRoutingHeader(header);
                    datagram.set_any(new MessageElement[]{datagramOut});
                    request.setDatagram(datagram);

                    rowId = IFefuNsiSyncDAO.instance.get().doInitLogNSIOutcomingAction(header, xDatagram, null, null);
                    rowId = IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, header, xDatagram, null, null, null);
                    ServiceResponseType response = null;

                    int tryCount = 0;
                    boolean retry = true;
                    while (tryCount < SINGLE_NSI_REQUEST_TRY_SCHEDULE.length && retry)
                    {
                        try
                        {
                            retry = false;
                            if (FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT.equals(actionType))
                                response = port.insert(request);
                            else if (FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE.equals(actionType))
                                response = port.update(request);
                            else if (FefuNsiRequestsProcessor.OPERATION_TYPE_DELETE.equals(actionType))
                                response = port.delete(request);
                        } catch (Exception e)
                        {
                            if (e instanceof AxisFault)
                            {
                                if (null != ((AxisFault) e).getFaultString() && ((AxisFault) e).getFaultString().contains("java.net.SocketTimeoutException"))
                                {
                                    FefuNsiSyncDAO.logEvent(Level.INFO, "!!!!!! TIMEOUT (" + (tryCount + 1) + ") !!!!!!");
                                    Thread.sleep(SINGLE_NSI_REQUEST_TRY_SCHEDULE[tryCount]);
                                    request.getRoutingHeader().setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                                    retry = true;
                                    tryCount++;
                                } else throw e;
                            } else throw e;
                        }
                    }

                    {
                        StringBuilder err = new StringBuilder("||||||||| Response from NSI:\n");
                        err.append("\t\tCallCC = ").append(response.getCallCC().getValue().intValue());
                        err.append("\t\tCallRC = ").append(response.getCallRC());
                        if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_ERROR.equals(response.getCallCC().getValue()))
                        {
                            FefuNsiSyncDAO.logEvent(Level.ERROR, err.toString());
                            IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, header, xDatagram, response, new UnsupportedOperationException(response.getCallRC()), err.toString());
                        } else
                            IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, header, xDatagram, response, null, err.toString());
                    }

                } catch (final Throwable t)
                {
                    StringBuilder err = new StringBuilder("||||||||| Some ERROR has occured while processing elements at NSI:\n");
                    if (t instanceof AxisFault)
                    {
                        err.append("\t\tFaultMessage = ").append(t.getMessage()).append("\n");
                        err.append("\t\tFaultString = ").append(((AxisFault) t).getFaultString()).append("\n");
                        err.append("\t\tFaultReason = ").append(((AxisFault) t).getFaultReason()).append("\n");
                        err.append("\t\tFaultDump = ").append(((AxisFault) t).dumpToString()).append("\n");
                    } else err.append(t.getMessage());

                    IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, null, null, null, t, err.toString());
                    FefuNsiSyncDAO.logEvent(Level.ERROR, err.toString());
                    throw CoreExceptionUtils.getRuntimeException(t);
                }

                FefuNsiSyncDAO.logEvent(Level.INFO, portionCount + (elementsPortion.size() == 1 ? " " : " more ") + "elements were processed at NSI");
            }
        });

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Executing " + actionType + " for elements at NSI was finished ----------");
    }

    public static void executeNSIAction(Long srcLogRowId, boolean manual) throws RuntimeException
    {
        FefuNsiLogRow srcLogRow = DataAccessServices.dao().getNotNull(FefuNsiLogRow.class, srcLogRowId);

        final ServiceSoap_Service service = getServiceProvider();
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- " + (manual ? "Re-" : "") + "Executing " + srcLogRow.getOperationType() + " for elements at NSI." + (manual ? (" Src message GUID = " + srcLogRow.getMessageId()) : "") + " ----------");

        Long rowId = null;
        int entityCount = 0;

        try
        {
            ServiceSoap_PortType port = service.getServiceSoap12();
            ServiceRequestType request = new ServiceRequestType();
            RoutingHeaderType header = new RoutingHeaderType();
            ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();

            ByteArrayInputStream inStream = new ByteArrayInputStream(srcLogRow.getMessageBody().getBytes());
            MessageElement datagramOut = new SOAPBodyElement(inStream);

            XDatagram reqDatagram = fromXml(XDatagram.class, srcLogRow.getMessageBody().getBytes());
            entityCount = reqDatagram.getEntityList().size();

            header.setOperationType(RoutingHeaderTypeOperationType.fromValue(srcLogRow.getOperationType()));
            header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
            header.setParentId(srcLogRow.getMessageId());
            header.setSourceId(srcLogRow.getSourceId());

            request.setRoutingHeader(header);
            datagram.set_any(new MessageElement[]{datagramOut});
            request.setDatagram(datagram);

            String comment = manual ? ("Повторная отправка пакета " + srcLogRow.getMessageId()) : null;
            rowId = IFefuNsiSyncDAO.instance.get().doInitLogNSIOutcomingAction(header, reqDatagram, comment, srcLogRowId);
            ServiceResponseType response = null;

            try
            {
                if (FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT.equals(srcLogRow.getOperationType()))
                    response = port.insert(request);
                else if (FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE.equals(srcLogRow.getOperationType()))
                    response = port.update(request);
                else if (FefuNsiRequestsProcessor.OPERATION_TYPE_DELETE.equals(srcLogRow.getOperationType()))
                    response = port.delete(request);
            } catch (Exception e)
            {
                if (e instanceof AxisFault)
                {
                    if (null != ((AxisFault) e).getFaultString() && ((AxisFault) e).getFaultString().contains("java.net.UnknownHostException"))
                    {
                        FefuNsiSyncDAO.logEvent(Level.INFO, "!!!!!! Unknown host !!!!!!\n" + e.getMessage());
                        IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, header, reqDatagram, response, e, "Unknown host (see details at SoapFault)");
                    } else if (null != ((AxisFault) e).getFaultString() && ((AxisFault) e).getFaultString().contains("java.net.SocketTimeoutException"))
                    {
                        FefuNsiSyncDAO.logEvent(Level.INFO, "!!!!!! TIMEOUT (" + (srcLogRow.getAttemptsCount() + 1) + ") !!!!!!");
                        IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, header, reqDatagram, response, e, "Timeout exception (see details at SoapFault)");
                        //TODO вынести в демон Thread.sleep(SINGLE_NSI_REQUEST_TRY_SCHEDULE[tryCount]);
                        //request.getRoutingHeader().setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                    } else //throw e;
                    {
                        FefuNsiSyncDAO.logEvent(Level.INFO, "!!!!!! SOME ERROR !!!!!!\n" + e.getMessage());
                        IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, header, reqDatagram, response, e, "Some error (see details at SoapFault)");
                    }
                } else throw e;
            }

            if (null != response)
            {
                StringBuilder err = new StringBuilder("||||||||| Response from NSI:\n");
                err.append("\t\tCallCC = ").append(response.getCallCC().getValue().intValue());
                err.append("\t\tCallRC = ").append(response.getCallRC());
                if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_ERROR.equals(response.getCallCC().getValue()))
                {
                    FefuNsiSyncDAO.logEvent(Level.ERROR, err.toString());
                    IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, header, reqDatagram, response, new UnsupportedOperationException(response.getCallRC()), err.toString());
                } else
                    IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, header, reqDatagram, response, null, err.toString());
            }

        } catch (final Throwable t)
        {
            StringBuilder err = new StringBuilder("||||||||| Some ERROR has occured while processing elements at NSI:\n");
            if (t instanceof AxisFault)
            {
                err.append("\t\tFaultMessage = ").append(t.getMessage()).append("\n");
                err.append("\t\tFaultString = ").append(((AxisFault) t).getFaultString()).append("\n");
                err.append("\t\tFaultReason = ").append(((AxisFault) t).getFaultReason()).append("\n");
                err.append("\t\tFaultDump = ").append(((AxisFault) t).dumpToString()).append("\n");
            } else err.append(t.getMessage());

            IFefuNsiSyncDAO.instance.get().doUpdateLogNSIOutcomingAction(rowId, null, null, null, t, err.toString());
            FefuNsiSyncDAO.logEvent(Level.ERROR, err.toString());
            throw CoreExceptionUtils.getRuntimeException(t);
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, entityCount + " elements were processed at NSI");

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- " + (manual ? "Re-" : "") + "Executing " + srcLogRow.getOperationType() + " for elements at NSI was finished." + (manual ? (" Src message GUID = " + srcLogRow.getMessageId()) : "") + " ----------");
    }

    public static void reExecuteOBAction(Long srcLogRowId) throws RuntimeException
    {
        FefuNsiLogRow srcLogRow = DataAccessServices.dao().getNotNull(FefuNsiLogRow.class, srcLogRowId);

        ServiceSoapImplServiceLocator service = new ServiceSoapImplServiceLocator();

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Re-Executing " + srcLogRow.getOperationType() + " for elements at OB. Src message GUID = " + srcLogRow.getMessageId() + " ----------");

        try
        {
            ru.tandemservice.unifefu.ws.nsi.servertest.ServiceRequestType request = new ru.tandemservice.unifefu.ws.nsi.servertest.ServiceRequestType();
            ru.tandemservice.unifefu.ws.nsi.servertest.RoutingHeaderType header = new ru.tandemservice.unifefu.ws.nsi.servertest.RoutingHeaderType();
            ru.tandemservice.unifefu.ws.nsi.servertest.ServiceRequestTypeDatagram datagram = new ru.tandemservice.unifefu.ws.nsi.servertest.ServiceRequestTypeDatagram();
            System.out.println("------ re-executing headers created");

            String messageBody = srcLogRow.getMessageBody();

            //System.out.println(messageBody);

            int idx = messageBody.indexOf(":x-datagram");
            if (idx > 0)
            {
                idx = messageBody.indexOf(">", idx);
                messageBody = messageBody.substring(idx + 1);
                idx = messageBody.indexOf("x-datagram>");
                if (idx > 0) idx = messageBody.substring(0, idx).lastIndexOf("</");
                if (idx > 0) messageBody = messageBody.substring(0, idx);
                messageBody = "<x-datagram xmlns=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0\">" + messageBody + "</x-datagram>";
                System.out.println("------ re-executing body correction was executed");

                //System.out.println(messageBody);
            }
            System.out.println("------ re-executing body correction finished");

            ByteArrayInputStream inStream = new ByteArrayInputStream(messageBody.getBytes());
            MessageElement datagramOut = new SOAPBodyElement(inStream);

            header.setSourceId(srcLogRow.getSourceId() + " (повторная обработка)");
            header.setOperationType(srcLogRow.getOperationType());
            header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
            header.setParentId(srcLogRow.getMessageId());
            request.setRoutingHeader(header);

            System.out.println("------ re-executing datagram injected");
            datagram.set_any(new MessageElement[]{datagramOut});
            request.setDatagram(datagram);

            try
            {
                System.out.println("------ re-executing WS Address =  " + service.getServiceSoapPortAddress());
                if (FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT.equals(srcLogRow.getOperationType()))
                    service.getServiceSoapPort().insert(request);
                else if (FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE.equals(srcLogRow.getOperationType()))
                    service.getServiceSoapPort().update(request);
                else if (FefuNsiRequestsProcessor.OPERATION_TYPE_DELETE.equals(srcLogRow.getOperationType()))
                    service.getServiceSoapPort().delete(request);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        } catch (final Throwable t)
        {
            t.printStackTrace();
            throw CoreExceptionUtils.getRuntimeException(t);
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Re-Executing " + srcLogRow.getOperationType() + " for elements at OB was finished. Src message GUID = " + srcLogRow.getMessageId() + " ----------");
    }

    public static ServiceRequestType prepareNsiRequest(RoutingHeaderTypeOperationType operationType)
    {
        String messageGUID = UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString();
        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        header.setSourceId(OB_SUBSYSTEM_CODE);
        header.setOperationType(operationType);
        header.setMessageId(messageGUID);
        request.setRoutingHeader(header);
        return request;
    }

    public static <T> List<T> getObjectsByType(ServiceSoap_PortType port, ServiceRequestType request, final Class<T> clazz)
    {
        try
        {
            ServiceResponseType response = port.retrieve(request);

            if (null != response.getDatagram())
            {
                MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

                if (null != respMsg && response.getCallCC().getValue().intValue() == 1)
                {
                    List<T> result = new ArrayList<>();
                    XDatagram respDatagram = fromXml(XDatagram.class, respMsg.getAsString().getBytes());

                    for (Object item : respDatagram.getEntityList())
                    {
                        if (item.getClass().getName().equals(clazz.getName()))
                        {
                            result.add(clazz.cast(item));
                        }
                    }

                    return result;
                }
            } else
            {
                System.out.println("CallCC = " + response.getCallCC().getValue().intValue());
                System.out.println("CallRC = " + response.getCallRC());
            }
        } catch (final Throwable t)
        {
            if (t instanceof AxisFault)
            {
                System.out.println("FaultString = " + ((AxisFault) t).getFaultString());
                System.out.println("FaultReason =  " + ((AxisFault) t).getFaultReason());
                System.out.println("FaultMessage = " + ((AxisFault) t).getMessage());
                System.out.println("FaultDump = " + ((AxisFault) t).dumpToString());
            }
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        return null;
    }

    public static <T> T fromXml(final Class<T> clazz, final byte[] xml)
    {
        try
        {
            final JAXBContext jc = JAXBContext.newInstance(clazz.getPackage().getName());
            final Unmarshaller u = jc.createUnmarshaller();
            final Object object = u.unmarshal(new ByteArrayInputStream(xml));

            if (object instanceof JAXBElement)
            {
                JAXBElement element = ((JAXBElement) object);
                if ("error".equalsIgnoreCase(element.getName().getLocalPart()))
                {
                    throw new IFisService.FisError(element, element.getValue().toString()); // корневой тег с ошибкой
                }
            }

            return clazz.cast(object);
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static byte[] toXml(final Object root)
    {
        try
        {
            final JAXBContext jc = JAXBContext.newInstance(root.getClass());
            final Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            final ByteArrayOutputStream ba = new ByteArrayOutputStream(8192);
            m.marshal(root, ba);
            return ba.toByteArray();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static Map<String, FefuNsiIds> getFefuNsiIdsMap(List<? extends IEntity> entityList)
    {
        Map<String, FefuNsiIds> result = new HashMap<>();
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                .where(in(property(FefuNsiIds.entityId().fromAlias("e")), getEntityIds(entityList)));
        List<FefuNsiIds> nsiIdsList = DataAccessServices.dao().getList(builder);
        for (FefuNsiIds id : nsiIdsList) result.put(id.getGuid(), id);
        return result;
    }

    public static List<Long> getEntityIds(List<? extends IEntity> entityList)
    {
        List<Long> result = new ArrayList<>();
        for (IEntity entity : entityList) result.add(entity.getId());
        return result;
    }

    public static final boolean isShouldBeUpdated(Map<String, ? extends IEntity> entityMap, String nsiEntityStr, String entityStr)
    {
        if (null == nsiEntityStr) return false;
        if (null == nsiEntityStr && null == entityStr) return false;
        String str1 = nsiEntityStr.trim().toUpperCase();
        String str2 = null != entityStr ? entityStr.trim().toUpperCase() : "";
        if (!str1.equals(str2)) return true;
        if (null != entityMap && entityMap.containsKey(str1)) return false;
        return false;
    }

    public static boolean isShouldBeUpdated(String nsiEntityStr, String entityStr, boolean nsiIsMasterSystem)
    {
        if (null == nsiEntityStr && null == entityStr) return false;
        if (nsiIsMasterSystem && null == nsiEntityStr) return false;
        if (!nsiIsMasterSystem && null == entityStr) return false;
        String str1 = null != nsiEntityStr ? nsiEntityStr.trim().toUpperCase() : "";
        String str2 = null != entityStr ? entityStr.trim().toUpperCase() : "";
        return !str1.equals(str2);
    }

    /**
     * Возвращает отформатированный XML-документ.
     *
     * @param src - неформатированный xml-документ
     * @return - отформатированный xml-документ
     * @throws Exception - ну, мало ли что может произойти
     */
    public static String getXmlFormatted(String src) throws Exception
    {
        if (null == src || src.trim().length() == 0) return null;

        try
        {
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(src));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(is);

            DOMSource domSource = new DOMSource(doc);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.ENCODING, "Utf8");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            StringWriter sw = new StringWriter();
            StreamResult sr = new StreamResult(sw);
            transformer.transform(domSource, sr);
            String result = sw.toString();
            return result;
        } catch (Exception e)
        {

        }
        return null;
    }

    public static void printMappedOrgStructure(List<INsiEntity> entityList)
    {
        OrgStructureWrapper orgStructureWrapper = new OrgStructureWrapper();
        orgStructureWrapper.useSeparators = true;
        orgStructureWrapper.printGuids = true;

        prepareActualDepartmentsStructure(entityList, orgStructureWrapper);
        prepareOBOrgStructure(orgStructureWrapper);
        printMappedOrgStructureRecursive(null, null, orgStructureWrapper, 0);

        orgStructureWrapper.outputBuilder.append("\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.guidToDepartmentMap.values().size() + " departments received.\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.idToOrgUnitMap.values().size() + " orgUnits loaded from database.\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.mappedByGuidCnt + " mapped by guid\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.mappedByTitleCnt + " mapped by title\n");
        orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.alreadyMappedOrgUnitsSet.size() + " mapped\n");

        orgStructureWrapper.outputBuilder.append("\n---------------------- Unmapped OB org units: \n");
        for (Long unusedId : orgStructureWrapper.unmappedOrgUnitIds)
        {
            orgStructureWrapper.outputBuilder.append(unusedId);
            OrgUnit orgUnit = orgStructureWrapper.idToOrgUnitMap.get(unusedId);
            if (null != orgUnit) orgStructureWrapper.outputBuilder.append(" ").append(orgUnit.getTitle());
            orgStructureWrapper.outputBuilder.append("\n");
        }

        System.out.println(orgStructureWrapper.outputBuilder.toString());
    }

    public static void prepareActualDepartmentsStructure(List<INsiEntity> entityList, OrgStructureWrapper orgStructureWrapper)
    {
        Set<String> actualRootDepartmentsSet = new HashSet<>();
        actualRootDepartmentsSet.add("двфу");
        actualRootDepartmentsSet.add("декрет");
        actualRootDepartmentsSet.add("двфу обособленные подразделения");

        Set<String> departmentsToIgnoreSet = new HashSet<>();
        departmentsToIgnoreSet.add("неучебные подразделения стар");
        departmentsToIgnoreSet.add("неучебные подразделения");
        departmentsToIgnoreSet.add("учебные подразделения");
        departmentsToIgnoreSet.add("школы");
        departmentsToIgnoreSet.add("двфу обособленные подразделения");
        departmentsToIgnoreSet.add("обособленные подразделения");

        Collections.sort(entityList, new Comparator<INsiEntity>()
        {
            @Override
            public int compare(INsiEntity o1, INsiEntity o2)
            {
                if (null == o1.getTitle() && null == o2.getTitle()) return 0;
                if (null == o1.getTitle() && null != o2.getTitle()) return 1;
                if (null != o1.getTitle() && null == o2.getTitle()) return -1;
                return o1.getTitle().toLowerCase().compareTo(o2.getTitle().toLowerCase());
            }
        });

        for (INsiEntity nsiEntity : entityList)
        {
            DepartmentType dep = (DepartmentType) nsiEntity;
            String parentDepGuid = null != dep.getDepartmentUpper() ? dep.getDepartmentUpper().getDepartment().getID() : null;

            if (null != dep.getDepartmentName())
            {
                if (dep.getDepartmentName().trim().length() > orgStructureWrapper.maxDepartmentTitleLength)
                    orgStructureWrapper.maxDepartmentTitleLength = dep.getDepartmentName().trim().length();

                orgStructureWrapper.guidToDepartmentMap.put(dep.getID(), dep);
                List<String> childDeps = orgStructureWrapper.guidToChildGuidsMap.get(parentDepGuid);
                if (null == childDeps) childDeps = new ArrayList<>();
                childDeps.add(dep.getID());
                orgStructureWrapper.guidToChildGuidsMap.put(parentDepGuid, childDeps);

                /*DepartmentType parentDepartment = null != dep.getDepartmentUpper() ? dep.getDepartmentUpper().getDepartment() : null;
                if(null != parentDepartment)
                {
                    if(parentDepartment.getDepartmentName().trim().toLowerCase())


                } TODO */
            }
        }

        //TODO del
        for (DepartmentType dep : orgStructureWrapper.guidToDepartmentMap.values())
        {
            StringBuilder depTitle = new StringBuilder();
            if (null != StringUtils.trimToNull(dep.getDepartmentName()))
            {
                depTitle.append(dep.getDepartmentName());
            }

            if (null != dep && null != dep.getDepartmentUpper())
            {
                DepartmentType parentDep = orgStructureWrapper.guidToDepartmentMap.get(dep.getDepartmentUpper().getDepartment().getID());
                if (null != parentDep)
                {
                    if (null != parentDep.getTitle() && departmentsToIgnoreSet.contains(parentDep.getTitle().trim().toLowerCase()))
                    {
                        parentDep = null != parentDep.getDepartmentUpper() ? parentDep.getDepartmentUpper().getDepartment() : null;
                    }
                }

                if (null != parentDep) depTitle.append(parentDep.getDepartmentName());
            }

/*                if (null != orgStructureWrapper.titleWithParentToGuidMap.get(depTitle.toString()))
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");*/

            String depTitleStr = depTitle.toString().replaceAll(" ", "").trim().toLowerCase();
            //orgStructureWrapper.titleWithParentToGuidMap.put(depTitleStr, dep.getID());
            orgStructureWrapper.titleWithParentToGuidMapDel.put(dep.getID(), depTitleStr);
        }

        if (null != orgStructureWrapper.guidToChildGuidsMap.get(null))
        {
            List<String> actualRootGuidsList = new ArrayList<>();
            for (String guid : orgStructureWrapper.guidToChildGuidsMap.get(null))
            {
                if (null != orgStructureWrapper.guidToDepartmentMap.get(guid) && null != orgStructureWrapper.guidToDepartmentMap.get(guid).getTitle()
                        //&& (null == orgStructureWrapper.guidToDepartmentMap.get(guid).getIsArchive() || !"1".equals(orgStructureWrapper.guidToDepartmentMap.get(guid).getIsArchive())) //tODO del
                        && actualRootDepartmentsSet.contains(orgStructureWrapper.guidToDepartmentMap.get(guid).getDepartmentName().trim().toLowerCase()))
                {
                    actualRootGuidsList.add(guid);
                }
            }
            orgStructureWrapper.guidToChildGuidsMap.put(null, actualRootGuidsList);
        }
    }

    public static void printNSIOrgStructure(List<INsiEntity> entityList)
    {
        OrgStructureWrapper orgStructureWrapper = new OrgStructureWrapper();
        prepareActualDepartmentsStructure(entityList, orgStructureWrapper);
        printDepartmentsRecursive(null, orgStructureWrapper, 0);
    }

    public static void prepareOBOrgStructure(OrgStructureWrapper orgStructureWrapper)
    {
        List<Object[]> orgUnitAndGuidList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou").column("ou").column(property(FefuNsiIds.guid().fromAlias("ids")))
                        .joinEntity("ou", DQLJoinType.left, FefuNsiIds.class, "ids", eq(property(OrgUnit.id().fromAlias("ou")), property(FefuNsiIds.entityId().fromAlias("ids"))))
        );

        Collections.sort(orgUnitAndGuidList, new Comparator<Object[]>()
        {
            @Override
            public int compare(Object[] o1, Object[] o2)
            {
                if (null == ((OrgUnit) o1[0]).getTitle() && null == ((OrgUnit) o2[0]).getTitle()) return 0;
                if (null == ((OrgUnit) o1[0]).getTitle() && null != ((OrgUnit) o2[0]).getTitle()) return 1;
                if (null != ((OrgUnit) o1[0]).getTitle() && null == ((OrgUnit) o2[0]).getTitle()) return -1;
                return ((OrgUnit) o1[0]).getTitle().toLowerCase().compareTo(((OrgUnit) o2[0]).getTitle().toLowerCase());
            }
        });

        for (Object[] item : orgUnitAndGuidList)
        {
            String guid = (String) item[1];
            OrgUnit orgUnit = (OrgUnit) item[0];
            orgStructureWrapper.idToOrgUnitMap.put(orgUnit.getId(), orgUnit);
            orgStructureWrapper.unmappedOrgUnitIds.add(orgUnit.getId());

            if (null != orgUnit.getParent())
            {
                List<Long> childOrgUnitIdsList = orgStructureWrapper.idToChildIdsMap.get(orgUnit.getParent().getId());
                if (null == childOrgUnitIdsList) childOrgUnitIdsList = new ArrayList<>();
                childOrgUnitIdsList.add(orgUnit.getId());
                orgStructureWrapper.idToChildIdsMap.put(orgUnit.getParent().getId(), childOrgUnitIdsList);
            }

            if (null != guid)
            {
                orgStructureWrapper.guidToOrgUnitIdMap.put(guid, orgUnit.getId());
                orgStructureWrapper.orgUnitIdToGuidMap.put(orgUnit.getId(), guid);
            }

            String titleForHash = (null != orgUnit.getParent() ? orgUnit.getParent().getTitle().toLowerCase() : "") + orgUnit.getTitle().toLowerCase();
            orgStructureWrapper.titleWithParentHashToIdMap.put(titleForHash.hashCode(), orgUnit.getId());

            StringBuilder orgUnitWithParentTitle = new StringBuilder(orgUnit.getTitle());
            if (null != orgUnit.getHierarhyParent())
            {
                orgUnitWithParentTitle.append(orgUnit.getParent() instanceof TopOrgUnit ? "двфу" : orgUnit.getParent().getTitle());
            }
            String orgUnitWithParentTitleStr = orgUnitWithParentTitle.toString().replaceAll(" ", "").trim().toLowerCase();
            orgStructureWrapper.titleWithParentToIdMap.put(orgUnitWithParentTitleStr, orgUnit.getId());

            String orgTitle = orgUnit.getTitle().toLowerCase().replaceAll("двфу", "").replaceAll(" ", "").trim();
            Integer titleCount = orgStructureWrapper.titleToCountMap.get(orgTitle);
            if (null == titleCount)
            {
                titleCount = 1;
                orgStructureWrapper.titleToIdMap.put(orgTitle, orgUnit.getId());
            } else
            {
                orgStructureWrapper.titleToIdMap.remove(orgTitle);
                titleCount++;
            }
            orgStructureWrapper.titleToCountMap.put(orgTitle, titleCount);

            // Добавляем ключи уникальности: в рамках одного подразделения не должно быть более одного подращлеления с одинаковыми названиями
            // Эту уникальность нужно отслеживать в процессе изменений оргструктуры через механизмы интеграции
            String uniqueKey = null != orgUnit.getParent() ? String.valueOf(orgUnit.getParent().getId()) : "";
            uniqueKey += orgUnit.getTitle().toLowerCase();
            orgStructureWrapper.uniqueKeySet.add(uniqueKey);
        }
    }

    public static void printOBOrgStructure()
    {
        OrgStructureWrapper orgStructureWrapper = new OrgStructureWrapper();
        prepareOBOrgStructure(orgStructureWrapper);
        printOrgUnitRecursive(OrgUnitManager.instance().dao().getTopOrgUnitId(), orgStructureWrapper, 0);
    }

    private static void printOrgUnitRecursive(Long parentId, OrgStructureWrapper orgStructureWrapper, int level)
    {
        OrgUnit orgUnit = orgStructureWrapper.idToOrgUnitMap.get(parentId);
        if (null != orgUnit)
        {
            for (int i = 0; i < level; i++) System.out.print("    ");
            System.out.println((null == orgStructureWrapper.orgUnitIdToGuidMap.get(orgUnit.getId()) ? "^" : "") + StringUtils.trimToNull(orgUnit.getTitle()));
        }

        List<Long> childOrgUnitIds = orgStructureWrapper.idToChildIdsMap.get(orgUnit.getId());
        if (null != childOrgUnitIds)
        {
            for (Long childId : childOrgUnitIds)
            {
                printOrgUnitRecursive(childId, orgStructureWrapper, level + 1);
            }
        }
    }

    private static void printDepartmentsRecursive(String parentGuid, OrgStructureWrapper orgStructureWrapper, int level)
    {
        if (null != parentGuid)
        {
            DepartmentType dep = orgStructureWrapper.guidToDepartmentMap.get(parentGuid);
            if (null != dep)
            {
                for (int i = 0; i < level - 1; i++) System.out.print("    ");
                System.out.println(StringUtils.trimToNull(dep.getDepartmentName()));
            }
        }

        List<String> childDeps = orgStructureWrapper.guidToChildGuidsMap.get(parentGuid);
        if (null != childDeps)
        {
            for (String childGuid : childDeps)
            {
                printDepartmentsRecursive(childGuid, orgStructureWrapper, level + 1);
            }
        }
    }

    public static List<String> getRootDepartmentsForPartialOrgstructure(OrgStructureWrapper orgStructureWrapper)
    {
        List<String> rootDepartmentsList = new ArrayList<>();
        for (Map.Entry<String, DepartmentType> entry : orgStructureWrapper.guidToDepartmentMap.entrySet())
        {
            DepartmentType dep = entry.getValue();
            DepartmentType parentDep = null != dep.getDepartmentUpper() ? dep.getDepartmentUpper().getDepartment() : null;

            if (null == parentDep || !orgStructureWrapper.guidToDepartmentMap.containsKey(parentDep.getID()))
                rootDepartmentsList.add(dep.getID());
        }
        return rootDepartmentsList;
    }

    public static void printMappedOrgStructureRecursive(String parentGuid, Long parentId, OrgStructureWrapper orgStructureWrapper, int level)
    {
        boolean orgUnitFound = false;
        DepartmentType dep = orgStructureWrapper.guidToDepartmentMap.get(parentGuid);
        if (null != dep)
        {
            orgStructureWrapper.resultDepartmentGuidsOrdered.add(parentGuid);

            if (orgStructureWrapper.printGuids)
                orgStructureWrapper.outputBuilder.append(parentGuid).append(orgStructureWrapper.useSeparators ? "|" : " ");

            for (int i = 0; i < level - 1; i++) orgStructureWrapper.outputBuilder.append("    ");
            orgStructureWrapper.outputBuilder.append(StringUtils.trimToNull(dep.getDepartmentName()));
            if (orgStructureWrapper.useSeparators) orgStructureWrapper.outputBuilder.append("|");

            //TODO del
                /*if(null != orgStructureWrapper.titleWithParentToGuidMapDel.get(parentGuid))
                System.out.print(" (" + orgStructureWrapper.titleWithParentToGuidMapDel.get(parentGuid) + ")");*/

            Long orgUnitId = orgStructureWrapper.guidToOrgUnitIdMap.get(parentGuid);
            if (null != orgUnitId && null != orgStructureWrapper.idToOrgUnitMap.get(orgUnitId))
            {
                orgUnitFound = true;
                String foundGuid = orgStructureWrapper.orgUnitIdToGuidMap.get(orgUnitId);
                orgStructureWrapper.resultGuidToOrgUnitMap.put(parentGuid, orgUnitId);

                if (!orgStructureWrapper.useSeparators)
                {
                    for (int i = 0; i < orgStructureWrapper.maxDepartmentTitleLength - dep.getDepartmentName().trim().length(); i++)
                        orgStructureWrapper.outputBuilder.append(" ");
                }

                //for (int i = 0; i < level - 1; i++) System.out.print("    ");
                orgStructureWrapper.outputBuilder.append(orgStructureWrapper.idToOrgUnitMap.get(orgUnitId).getTitle());
                if (orgStructureWrapper.useSeparators) orgStructureWrapper.outputBuilder.append("|");
                if (null != foundGuid && !parentGuid.equals(foundGuid) && orgStructureWrapper.printGuids)
                    orgStructureWrapper.outputBuilder.append("[").append(foundGuid).append("]");

                orgStructureWrapper.alreadyMappedOrgUnitsSet.add(orgUnitId);
                orgStructureWrapper.mappedByGuidCnt++;
                orgStructureWrapper.unmappedOrgUnitIds.remove(orgUnitId);
            }

            if (!orgUnitFound)
            {
                String titleForSeek = orgStructureWrapper.titleWithParentToGuidMapDel.get(parentGuid);
                if (null != titleForSeek)
                {
                    //System.out.print(" -------- " + titleForSeek  + " ---------");
                    Long orgUnitByTitleId = orgStructureWrapper.titleWithParentToIdMap.get(titleForSeek);
                    if (null != orgUnitByTitleId && !orgStructureWrapper.alreadyMappedOrgUnitsSet.contains(orgUnitByTitleId) && !orgStructureWrapper.orgUnitIdToGuidMap.containsKey(orgUnitByTitleId))
                    {
                        OrgUnit orgUnitByTitle = orgStructureWrapper.idToOrgUnitMap.get(orgUnitByTitleId);
                        if (null != orgUnitByTitle)
                        {
                            orgUnitFound = true;
                            String foundGuid = orgStructureWrapper.orgUnitIdToGuidMap.get(orgUnitId);
                            orgStructureWrapper.resultGuidToOrgUnitMap.put(parentGuid, orgUnitByTitleId);

                            if (!orgStructureWrapper.useSeparators)
                            {
                                for (int i = 0; i < orgStructureWrapper.maxDepartmentTitleLength - dep.getDepartmentName().trim().length(); i++)
                                    orgStructureWrapper.outputBuilder.append(" ");
                            }
                            //for (int i = 0; i < level - 1; i++) System.out.print("    ");
                            orgStructureWrapper.outputBuilder.append("* " + orgUnitByTitle.getTitle());

                            if (orgStructureWrapper.useSeparators) orgStructureWrapper.outputBuilder.append("|");
                            if (null != foundGuid && !parentGuid.equals(foundGuid) && orgStructureWrapper.printGuids)
                                orgStructureWrapper.outputBuilder.append("[").append(foundGuid).append("]");

                            orgStructureWrapper.alreadyMappedOrgUnitsSet.add(orgUnitByTitleId);
                            orgStructureWrapper.mappedByTitleCnt++;
                            orgStructureWrapper.unmappedOrgUnitIds.remove(orgUnitByTitleId);
                        }
                    }
                }
            }

            if (!orgUnitFound)
            {
                String titleForSeek = dep.getDepartmentName().toLowerCase().replaceAll("двфу", "").replaceAll(" ", "").trim();

                if (null != titleForSeek)
                {
                    Integer titleCount = orgStructureWrapper.titleToCountMap.get(titleForSeek);
                    if (null != titleCount && 1 == titleCount)
                    {
                        Long orgUnitByTitleId = orgStructureWrapper.titleToIdMap.get(titleForSeek);
                        if (null != orgUnitByTitleId && !orgStructureWrapper.alreadyMappedOrgUnitsSet.contains(orgUnitByTitleId) && !orgStructureWrapper.orgUnitIdToGuidMap.containsKey(orgUnitByTitleId))
                        {
                            OrgUnit orgUnitByTitle = orgStructureWrapper.idToOrgUnitMap.get(orgUnitByTitleId);
                            if (null != orgUnitByTitle)
                            {
                                String foundGuid = orgStructureWrapper.orgUnitIdToGuidMap.get(orgUnitId);
                                orgStructureWrapper.resultGuidToOrgUnitMap.put(parentGuid, orgUnitByTitleId);

                                if (!orgStructureWrapper.useSeparators)
                                {
                                    for (int i = 0; i < orgStructureWrapper.maxDepartmentTitleLength - dep.getDepartmentName().trim().length(); i++)
                                        orgStructureWrapper.outputBuilder.append(" ");
                                }

                                //for (int i = 0; i < level - 1; i++) System.out.print("    ");
                                orgStructureWrapper.outputBuilder.append("* " + orgUnitByTitle.getTitle());

                                if (orgStructureWrapper.useSeparators) orgStructureWrapper.outputBuilder.append("|");
                                if (null != foundGuid && !parentGuid.equals(foundGuid) && orgStructureWrapper.printGuids)
                                    orgStructureWrapper.outputBuilder.append("[").append(foundGuid).append("]");

                                orgStructureWrapper.alreadyMappedOrgUnitsSet.add(orgUnitByTitleId);
                                orgStructureWrapper.mappedByTitleCnt++;
                                orgStructureWrapper.unmappedOrgUnitIds.remove(orgUnitByTitleId);
                            }
                        }
                    }
                }
            }
        }

        OrgUnit orgUnit = orgStructureWrapper.idToOrgUnitMap.get(parentId);
        if (null != orgUnit && null == parentGuid)
        {
            if (!orgStructureWrapper.useSeparators)
            {
                for (int i = 0; i < orgStructureWrapper.maxDepartmentTitleLength; i++)
                    orgStructureWrapper.outputBuilder.append(" ");
            } else
            {
                if (orgStructureWrapper.printGuids) orgStructureWrapper.outputBuilder.append("|");
                orgStructureWrapper.outputBuilder.append("|");
            }

            for (int i = 0; i < level - 1; i++) orgStructureWrapper.outputBuilder.append("    ");
            orgStructureWrapper.outputBuilder.append(orgUnit.getTitle());
        }

        orgStructureWrapper.outputBuilder.append("\n");

        if (null == parentId || null != parentGuid)
        {
            List<String> childDeps = orgStructureWrapper.guidToChildGuidsMap.get(parentGuid);
            if (null != childDeps)
            {
                for (String childGuid : childDeps)
                {
                    printMappedOrgStructureRecursive(childGuid, null, orgStructureWrapper, level + 1);
                }
            }
        }

        if (null == parentGuid && null == parentId) parentId = OrgUnitManager.instance().dao().getTopOrgUnitId();

        List<Long> childOrgUnits = orgStructureWrapper.idToChildIdsMap.get(parentId);
        if (null != childOrgUnits)
        {
            for (Long childId : orgStructureWrapper.idToChildIdsMap.get(parentId))
            {
                if (!orgStructureWrapper.alreadyMappedOrgUnitsSet.contains(childId))
                    printMappedOrgStructureRecursive(null, childId, orgStructureWrapper, level + 1);
            }
        }
    }

    public static class OrgStructureWrapper
    {
        // Department Maps
        public Map<String, DepartmentType> guidToDepartmentMap = new HashMap<>();
        public Map<String, List<String>> guidToChildGuidsMap = new HashMap<>();
        //public Map<String, String> titleWithParentToGuidMap = new HashMap<>();
        public Map<String, String> titleWithParentToGuidMapDel = new HashMap<>(); //TODO del

        public Map<Long, OrgUnit> idToOrgUnitMap = new HashMap<>();
        public Map<String, Long> guidToOrgUnitIdMap = new HashMap<>();
        public Map<Long, String> orgUnitIdToGuidMap = new HashMap<>();
        public Map<Long, List<Long>> idToChildIdsMap = new HashMap<>();
        public Map<Integer, Long> titleWithParentHashToIdMap = new HashMap<>();
        public Map<String, Long> titleWithParentToIdMap = new HashMap<>();

        public Map<String, Long> titleToIdMap = new HashMap<>();
        public Map<String, Integer> titleToCountMap = new HashMap<>();

        public int mappedByGuidCnt = 0;
        public int mappedByTitleCnt = 0;
        public int maxDepartmentTitleLength = 0;
        public Set<Long> alreadyMappedOrgUnitsSet = new HashSet<>();

        public Set<Long> unmappedOrgUnitIds = new HashSet<>();

        public StringBuilder outputBuilder = new StringBuilder();
        public boolean useSeparators = false;
        public boolean printGuids = false;

        public Map<String, Long> resultGuidToOrgUnitMap = new HashMap<>();
        public List<String> resultDepartmentGuidsOrdered = new ArrayList<>();
        public Set<String> uniqueKeySet = new HashSet<>();
    }


    public static byte[] getEmulatedResponseDatagramElementsFromFile(String filePath)
    {
        ByteArrayOutputStream out = null;
        InputStream input = null;
        try
        {
            try
            {
                int data = 0;
                out = new ByteArrayOutputStream();
                input = new BufferedInputStream(new FileInputStream(filePath));
                while ((data = input.read()) != -1) out.write(data);
                return out.toByteArray();
                //NsiReactorUtils.fromXml(XDatagram.class, _orgstructureFile);
            } catch (FileNotFoundException ex)
            {
                throw new ApplicationException("Файл '" + filePath + "' не найден.");
            } finally
            {
                if (null != input) input.close();
                if (null != out) out.close();
            }
        } catch (IOException ioEx)
        {
            ioEx.printStackTrace();
            throw new ApplicationException("Невозмоэно прочитать файл '" + filePath + "'.");
        }
    }

    public static <T> List<T> getObjectsFromNSIByFile(final Class<T> clazz, byte[] orgstructFile)
    {
        List<T> result = new ArrayList<>();
        XDatagram datagram = fromXml(XDatagram.class, orgstructFile);

        for (Object retrievedObject : datagram.getEntityList())
        {
            if (clazz.isInstance(retrievedObject))
            {
                result.add((T) retrievedObject);
            }
        }

        return result;
    }
}