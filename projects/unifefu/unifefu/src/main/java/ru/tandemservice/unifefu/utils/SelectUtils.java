/* $Id$ */
package ru.tandemservice.unifefu.utils;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;

import java.util.Arrays;

/**
 * @author Nikolay Fedorovskih
 * @since 01.07.2013
 */
public class SelectUtils
{
    public static final Long NO_ID = 0L;
    public static final Long YES_ID = 1L;

    public static ISelectModel getYesNoSelectModel()
    {
        return new StaticSelectModel("id", "title",
                                     Arrays.asList(new IdentifiableWrapper(YES_ID, "Да"), new IdentifiableWrapper(NO_ID, "Нет")));
    }

    public static Boolean getBooleanFromYesNoSelectValue(Object value)
    {
        if (value == null)
            return null;
        IdentifiableWrapper identifiable = (IdentifiableWrapper) value;
        if (identifiable.getId().equals(NO_ID))
            return false;
        if (identifiable.getId().equals(YES_ID))
            return true;

        throw new RuntimeException("Unknown id from yes|no select.");
    }
}