/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu15.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract;

/**
 * @author nvankov
 * @since 11/15/13
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FullStateMaintenanceEnrollmentStuExtract, Model>
{
}
