/* $Id$ */
package ru.tandemservice.unifefu.component.group.GroupList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 26.11.2014
 */
public class Controller extends ru.tandemservice.uni.component.group.GroupList.Controller
{

    @Override
    protected DynamicListDataSource<Group> createNewDataSource(IBusinessComponent component)
    {
        //добавляем столбец с количеством студентов в акад.отпуске
        DynamicListDataSource<Group> dataSource = super.createNewDataSource(component);
        List<AbstractColumn> columns = dataSource.getColumns();
        AbstractColumn column = dataSource.getColumn(Group.P_ACTIVE_STUDENT_COUNT);
        int startPos = column == null ? 11 : column.getNumber();

        columns.add(++startPos, new SimpleColumn("Академический отпуск", Model.P_VACATION_STUDENT_COUNT).setClickable(false).setOrderable(false));
        return dataSource;
    }
}
