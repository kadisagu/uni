/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;

/**
 * @author nvankov
 * @since 12/16/13
 */
@Configuration
public class FefuBrsSumDataReportAdd extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(FefuBrsReportManager.instance().orgUnitDSConfig())
                .addDataSource(FefuBrsReportManager.instance().territorialOrgUnitDSConfig())
                .addDataSource(FefuBrsReportManager.instance().yearPartDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(FefuBrsReportManager.instance().groupDSConfig())
                .addDataSource(FefuBrsReportManager.instance().eduLvlDSConfig())
                .create();
    }


}



    