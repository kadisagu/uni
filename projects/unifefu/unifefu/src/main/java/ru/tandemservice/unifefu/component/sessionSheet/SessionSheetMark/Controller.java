/*$Id$*/
package ru.tandemservice.unifefu.component.sessionSheet.SessionSheetMark;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import ru.tandemservice.unifefu.entity.SessionSheetMarkBy;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

/**
 * @author DMITRY KNYAZEV
 * @since 08.09.2015
 */
public class Controller extends ru.tandemservice.unisession.component.sessionSheet.SessionSheetMark.Controller
{

    @Override
    public void onClickApply(IBusinessComponent component)
    {
        final Model model = (Model) this.getModel(component);
        final IDAO dao = (IDAO) this.getDao();

        final SessionMark previous = dao.get(SessionMark.class, SessionMark.slot().s(), model.getSlot());
        SessionSheetMarkBy markBy = null;
        final SessionMarkCatalogItem previousMark = previous == null ? null : previous.getValueItem();
        final SessionMarkCatalogItem mark = model.getMark();
        if (previousMark != null || mark != null)
        {
            if (previousMark == null || mark == null || !previousMark.equals(mark))
            {
                final SessionSheetDocument sheet = model.getSheet();
                if (sheet.getId() != null)
                {
                    markBy = dao.get(SessionSheetMarkBy.class, SessionSheetMarkBy.sessionDocument(), sheet);
                }
                if (markBy == null)
                {
                    markBy = new SessionSheetMarkBy();
                    markBy.setSessionDocument(sheet);
                }
                IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
                markBy.setPrincipalContext(principalContext);
            }
        }

        dao.update(model);
        dao.saveOrUpdate(markBy);
        this.deactivate(component);
    }
}
