/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.IdentityCardStep;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.unifefu.dao.daemon.IFEFUPasswordRegistratorDaemonDao;
import ru.tandemservice.unifefu.dao.daemon.IFEFUPersonGuidDaemonDao;
import ru.tandemservice.unifefu.entity.EntrantFefuExt;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;
import ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.utils.SymmetricCrypto;

/**
 * @author Nikolay Fedorovskih
 * @since 15.05.2013
 */
public class DAO extends ru.tandemservice.uniec.component.wizard.IdentityCardStep.DAO
{
    @Override
    public void prepare(ru.tandemservice.uniec.component.wizard.IdentityCardStep.Model model)
    {
        super.prepare(model);
        IdentityCard identityCard = model.getEntrantModel().getIdentityCard();
        OnlineEntrant onlineEntrant = model.getEntrantModel().getOnlineEntrant();
        if (onlineEntrant.getId() != null && onlineEntrant.getPassportCitizenship() != null)
        {
            identityCard.setCitizenship(onlineEntrant.getPassportCitizenship());
        }
        else
        {
            identityCard.setCitizenship(identityCard.getCardType().getCitizenshipDefault());
        }
    }

    private void createEntrantFefuExt(Entrant entrant, OnlineEntrant onlineEntrant)
    {
        EntrantFefuExt entrantFefuExt = getByNaturalId(new EntrantFefuExt.NaturalId(entrant));
        if (entrantFefuExt == null)
        {
            entrantFefuExt = new EntrantFefuExt();
            entrantFefuExt.setEntrant(entrant);
        }
        entrantFefuExt.setParticipateInSecondWaveUSE(onlineEntrant.getParticipateInSecondWaveUSE());
        entrantFefuExt.setParticipateInEntranceTests(onlineEntrant.getParticipateInEntranceTests());
        entrantFefuExt.setEduInstitutionStr(onlineEntrant.getEduInstitutionStr());

        OnlineEntrantFefuExt olEntrantExt = getByNaturalId(new OnlineEntrantFefuExt.NaturalId(onlineEntrant));
        if (null != olEntrantExt)
            entrantFefuExt.setAgreeWithPrivacyNotice(olEntrantExt.getAgreeWithPrivacyNotice());

        saveOrUpdate(entrantFefuExt);
    }

    @Override
    public void createEntrant(ru.tandemservice.uniec.component.wizard.IdentityCardStep.Model m)
    {
        String password = null;

        super.createEntrant(m);
        Model model = (Model) m;
        Entrant entrant = model.getEntrantModel().getEntrant();
        OnlineEntrant onlineEntrant = model.getEntrantModel().getOnlineEntrant();
        Principal principal = get(Person2PrincipalRelation.class, Person2PrincipalRelation.L_PERSON, entrant.getPerson()).getPrincipal();

        if (onlineEntrant.getId() != null)
        {
            createEntrantFefuExt(entrant, onlineEntrant);

            OnlineEntrantFefuExt olEntrantExt = getByNaturalId(new OnlineEntrantFefuExt.NaturalId(onlineEntrant));
            if (null != olEntrantExt)
            {
                password = new SymmetricCrypto().decrypt(olEntrantExt.getPasswordEncrypted());
            }

            Person person = model.getEntrantModel().getPerson();
            person.getContactData().setEmail(onlineEntrant.getEmail());
            update(person.getContactData());

            // Сразу добавляем информацию о ближайшем родственнике. В мастере этот шаг скрыт для ДВФУ.
            addEntrantNextOfKin(person, onlineEntrant);
        }

        if (null != model.getForeignOnlineEntrantId())
        {

            FefuForeignOnlineEntrant foreignOnlineEntrant = get(model.getForeignOnlineEntrantId());
            foreignOnlineEntrant.setEntrant(entrant);
            update(foreignOnlineEntrant);

            if (!StringUtils.isEmpty(foreignOnlineEntrant.getEmail()))
            {
                Person person = entrant.getPerson();
                person.getContactData().setEmail(foreignOnlineEntrant.getEmail());
                update(person.getContactData());
            }

            password = new SymmetricCrypto().decrypt(foreignOnlineEntrant.getPasswordEncrypted());
        }

        if (password == null)
        {
            IdentityCard icard = entrant.getPerson().getIdentityCard();
            password = StringUtils.capitalize(CoreStringUtils.transliterate(icard.getLastName()).toLowerCase().replaceAll("\'", "")) + (StringUtils.isEmpty(icard.getNumber()) ? "" : icard.getNumber());
            if (password.length() < 8)
            {
                password += RussianDateFormatUtils.getDayString(icard.getBirthDate(), true);
                password += RussianDateFormatUtils.getMonthString(icard.getBirthDate(), true);
                password += RussianDateFormatUtils.getYearString(icard.getBirthDate(), false);
            }
        }

        PersonManager.instance().dao().updatePersonLogin(entrant.getPerson().getId(), principal.getLogin(), password);
        FefuNsiIds nsiId = IFEFUPersonGuidDaemonDao.instance.get().doActualizePersonGuid(entrant.getPerson().getId());
        IFEFUPasswordRegistratorDaemonDao.instance.get().doRegisterNSIRequest(nsiId, entrant.getPerson(), password);
    }

    @Override
    public void createBySimilar(Entrant entrant, Long basisPersonId, OnlineEntrant onlineEntrant)
    {
        super.createBySimilar(entrant, basisPersonId, onlineEntrant);
        createEntrantFefuExt(entrant, onlineEntrant);
    }

    private void addEntrantNextOfKin(Person person, OnlineEntrant onlineEntrant)
    {
        if (onlineEntrant.getNextOfKinRelationDegree() != null)
        {
            PersonNextOfKin personNextOfKin = new PersonNextOfKin();
            personNextOfKin.setRelationDegree(onlineEntrant.getNextOfKinRelationDegree());
            personNextOfKin.setFirstname(onlineEntrant.getNextOfKinFirstName());
            personNextOfKin.setLastname(onlineEntrant.getNextOfKinLastName());
            personNextOfKin.setPatronymic(onlineEntrant.getNextOfKinMiddleName());
            personNextOfKin.setEmploymentPlace(onlineEntrant.getNextOfKinWorkPlace());
            personNextOfKin.setPost(onlineEntrant.getNextOfKinWorkPost());
            personNextOfKin.setPhones(onlineEntrant.getNextOfKinPhone());
            personNextOfKin.setPerson(person);
            save(personNextOfKin);
        }
    }
}