/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.EntrantRequestAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

/**
 * @author Nikolay Fedorovskih
 * @since 14.06.2013
 */
public class Model extends ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Model
{
    private FefuTechnicalCommission technicalCommission;
    private ISelectModel technicalCommissionsSelectModel;
    private FefuTechnicalCommission oldTechnicalCommission;

    public FefuTechnicalCommission getTechnicalCommission()
    {
        return technicalCommission;
    }

    public void setTechnicalCommission(FefuTechnicalCommission technicalCommission)
    {
        this.technicalCommission = technicalCommission;
    }

    public ISelectModel getTechnicalCommissionsSelectModel()
    {
        return technicalCommissionsSelectModel;
    }

    public void setTechnicalCommissionsSelectModel(ISelectModel technicalCommissionsSelectModel)
    {
        this.technicalCommissionsSelectModel = technicalCommissionsSelectModel;
    }

    public FefuTechnicalCommission getOldTechnicalCommission()
    {
        return oldTechnicalCommission;
    }

    public void setOldTechnicalCommission(FefuTechnicalCommission oldTechnicalCommission)
    {
        this.oldTechnicalCommission = oldTechnicalCommission;
    }
}