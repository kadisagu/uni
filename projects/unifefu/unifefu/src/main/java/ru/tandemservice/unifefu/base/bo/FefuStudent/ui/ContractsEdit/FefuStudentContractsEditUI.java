/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.ui.ContractsEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.FefuStudentContract;

/**
 * @author Alexey Lopatin
 * @since 20.12.2013
 */
@Input({@Bind(key = "contractId", binding = "contractId", required = true)})
public class FefuStudentContractsEditUI extends UIPresenter
{
    private Long _contractId;
    private FefuStudentContract _contract;

    @Override
    public void onComponentRefresh()
    {
        _contract = DataAccessServices.dao().get(_contractId);
    }

    // Listeners

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_contract);
        deactivate();
    }

    // Getters & Setters

    public Long getContractId()
    {
        return _contractId;
    }

    public void setContractId(Long contractId)
    {
        _contractId = contractId;
    }

    public FefuStudentContract getContract()
    {
        return _contract;
    }

    public void setContract(FefuStudentContract contract)
    {
        _contract = contract;
    }
}
