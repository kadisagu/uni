/* $Id$ */
package ru.tandemservice.unifefu.tapestry.formatter;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.BaseRawFormatter;

/**
 * Форматтер для вывода xml-текста из нескольких строк с переносами в списках и инфо-таблицах.
 * Учитывает особенность отрисовки тэга разрыва строки <<br/>>
 *
 * Заменяет встречающиеся в передаваемой строке "\n" на переносы html
 *
 * @author Dmitry Seleznev
 * @since 22.08.2012
 */
public class FefuCrazyXmlFormatter extends BaseRawFormatter<String>
{
    public static final FefuCrazyXmlFormatter INSTANCE = new FefuCrazyXmlFormatter();

    @Override
    public String format(String source)
    {
        if (StringUtils.isEmpty(source)) { return ""; }

        source = BaseRawFormatter.encode(source.trim());
        final StringBuilder sb = new StringBuilder();

        final String[] lines = source.split("\n");
        for (final String line : lines)
        {
            if (StringUtils.isEmpty(line)) { sb.append("<div>&nbsp;</div>"); }
            else { sb.append("<div><nobr>").append(line.replaceAll(" ", "&nbsp;")).append("</nobr></div>"); }
        }

        return sb.toString();
    }
}