/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

/**
 * @author Dmitry Seleznev
 * @since 18.02.2014
 */
public interface IFEFUPortalStatusesDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IFEFUPortalStatusesDaemonDao.class.getName() + ".global-lock";
        final SpringBeanCache<IFEFUPortalStatusesDaemonDao> instance = new SpringBeanCache<>(IFEFUPortalStatusesDaemonDao.class.getName());

        /**
         * Посылает зарегистрированные, но не отправленные уведомления в портал ДВФУ
         */
        @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
        void doSendPortalStatuses();

	void saveLogData(FEFUPortalStatusesDaemonDao.RequestPack pack);
}
