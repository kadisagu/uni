/* $Id: Model.java 38584 2014-10-08 11:05:36Z azhebko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.listextract.fefu1.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.component.listextract.e41.utils.EmployeePostVO;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.SendPracticeOutStuListExtract;

import java.util.Date;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 13.02.2013
 */
public class Model extends AbstractListParagraphAddEditModel<SendPracticeOutStuListExtract> implements IGroupModel
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private String _practiceType;
    private String _practiceKind;
    private String _practiceDuration;
    private Date _practiceBeginDate;
    private Date _practiceEndDate;
    private Date _attestationDate;
    private boolean _outClassTime = false;
    private boolean _doneEduPlan = false;
    private boolean _donePractice = false;
    private boolean _provideFundsAccordingToEstimates = false;
    private String _estimateNum;
    private Course _practiceCourse;
    private EmployeePostVO _preventAccidentsIC;
    private String _preventAccidentsICStr;
    private EmployeePostVO _responsForRecieveCash;
    private String _responsForRecieveCashStr;

    private ISelectModel _groupListModel;
    private List<CompensationType> _compensationTypeList;
    private List<String> _practiceTypeList;
    private ISelectModel _practiceKindModel;
    private ISelectModel _employeePostModel;
    private ISelectModel _externalOrgUnitModel;
    private ISelectModel _practiceContractModel;

    public String getPracticeType()
    {
        return _practiceType;
    }

    public void setPracticeType(String _practiceType)
    {
        this._practiceType = _practiceType;
    }

    public String getUpdateIds()
    {
        return getPracticeHeaderOutId() + "," + getPracticeContractId();
    }

    public String getPracticeExternalOrgUnitId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "practiceExternalOrgUnitId_" + dataSource.getCurrentEntity().getId();
    }

    //Calculate method
    public String getPracticeHeaderInnerId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "practiceHeaderInnerId_" + dataSource.getCurrentEntity().getId();
    }

    //Calculate method
    public String getPracticeHeaderInnerStrId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "practiceHeaderInnerStrId_" + dataSource.getCurrentEntity().getId();
    }

    //Calculate method
    public String getPracticeFactAddressId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "practiceFactAddressId_" + dataSource.getCurrentEntity().getId();
    }

    //Calculate method
    public String getPracticeHeaderOutId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "practiceHeaderOutId_" + dataSource.getCurrentEntity().getId();
    }

    //Calculate method
    public String getPracticeContractId()
    {
        DynamicListDataSource dataSource = this.getDataSource();
        return "practiceContractId_" + dataSource.getCurrentEntity().getId();
    }



    public boolean getStudentDisabled()
    {
        Boolean disabled = (Boolean) getDataSource().getCurrentValueEntity().getProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED);
        return null != disabled ? disabled : false ;
    }

    public Course getCourse()
    {
        return _course;

    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public String getPracticeKind()
    {
        return _practiceKind;
    }

    public void setPracticeKind(String practiceKind)
    {
        _practiceKind = practiceKind;
    }

    public String getPracticeDuration()
    {
        return _practiceDuration;
    }

    public void setPracticeDuration(String practiceDuration)
    {
        _practiceDuration = practiceDuration;
    }

    public Date getPracticeBeginDate()
    {
        return _practiceBeginDate;
    }

    public void setPracticeBeginDate(Date practiceBeginDate)
    {
        _practiceBeginDate = practiceBeginDate;
    }

    public Date getPracticeEndDate()
    {
        return _practiceEndDate;
    }

    public void setPracticeEndDate(Date practiceEndDate)
    {
        _practiceEndDate = practiceEndDate;
    }

    public Date getAttestationDate()
    {
        return _attestationDate;
    }

    public void setAttestationDate(Date attestationDate)
    {
        _attestationDate = attestationDate;
    }

    public boolean isOutClassTime()
    {
        return _outClassTime;
    }

    public void setOutClassTime(boolean outClassTime)
    {
        _outClassTime = outClassTime;
    }

    public boolean isDoneEduPlan()
    {
        return _doneEduPlan;
    }

    public void setDoneEduPlan(boolean doneEduPlan)
    {
        _doneEduPlan = doneEduPlan;
    }

    public Course getPracticeCourse()
    {
        return _practiceCourse;
    }

    public void setPracticeCourse(Course practiceCourse)
    {
        _practiceCourse = practiceCourse;
    }

    public EmployeePostVO getPreventAccidentsIC()
    {
        return _preventAccidentsIC;
    }

    public void setPreventAccidentsIC(EmployeePostVO preventAccidentsIC)
    {
        _preventAccidentsIC = preventAccidentsIC;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public List<String> getPracticeTypeList()
    {
        return _practiceTypeList;
    }

    public void setPracticeTypeList(List<String> _practiceTypeList)
    {
        this._practiceTypeList = _practiceTypeList;
    }

    public ISelectModel getPracticeKindModel()
    {
        return _practiceKindModel;
    }

    public void setPracticeKindModel(ISelectModel _practiceKindModel)
    {
        this._practiceKindModel = _practiceKindModel;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }

    public ISelectModel getExternalOrgUnitModel()
    {
        return _externalOrgUnitModel;
    }

    public void setExternalOrgUnitModel(ISelectModel externalOrgUnitModel)
    {
        _externalOrgUnitModel = externalOrgUnitModel;
    }

    public EmployeePostVO getResponsForRecieveCash()
    {
        return _responsForRecieveCash;
    }

    public void setResponsForRecieveCash(EmployeePostVO responsForRecieveCash)
    {
        _responsForRecieveCash = responsForRecieveCash;
    }

    public boolean getProvideFundsAccordingToEstimates()
    {
        return _provideFundsAccordingToEstimates;
    }

    public void setProvideFundsAccordingToEstimates(boolean provideFundsAccordingToEstimates)
    {
        _provideFundsAccordingToEstimates = provideFundsAccordingToEstimates;
    }

    public String getEstimateNum()
    {
        return _estimateNum;
    }

    public void setEstimateNum(String estimateNum)
    {
        _estimateNum = estimateNum;
    }

    public ISelectModel getPracticeContractModel()
    {
        return _practiceContractModel;
    }

    public void setPracticeContractModel(ISelectModel practiceContractModel)
    {
        _practiceContractModel = practiceContractModel;
    }

    public boolean isDonePractice()
    {
        return _donePractice;
    }

    public void setDonePractice(boolean donePractice)
    {
        _donePractice = donePractice;
    }

    public String getPreventAccidentsICStr()
    {
        return _preventAccidentsICStr;
    }

    public void setPreventAccidentsICStr(String preventAccidentsICStr)
    {
        _preventAccidentsICStr = preventAccidentsICStr;
    }

    public String getResponsForRecieveCashStr()
    {
        return _responsForRecieveCashStr;
    }

    public void setResponsForRecieveCashStr(String responsForRecieveCashStr)
    {
        _responsForRecieveCashStr = responsForRecieveCashStr;
    }
}
