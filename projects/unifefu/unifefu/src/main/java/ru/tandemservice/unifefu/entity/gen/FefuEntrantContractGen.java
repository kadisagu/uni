package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.unifefu.entity.FefuEntrantContract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Договор ДВФУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEntrantContractGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEntrantContract";
    public static final String ENTITY_NAME = "fefuEntrantContract";
    public static final int VERSION_HASH = -1292003075;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUESTED_ENROLLMENT_DIRECTION = "requestedEnrollmentDirection";
    public static final String P_CONTRACT_NUMBER = "contractNumber";
    public static final String P_CONTRACT_DATE = "contractDate";
    public static final String P_CONTRACT_PAYER = "contractPayer";
    public static final String P_CONTRACT_TERMINATED = "contractTerminated";

    private RequestedEnrollmentDirection _requestedEnrollmentDirection;     // Выбранное направление подготовки
    private String _contractNumber;     // Номер договора
    private Date _contractDate;     // Дата договора
    private String _contractPayer;     // Плательщик по договору
    private boolean _contractTerminated;     // Договор расторгнут

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выбранное направление подготовки. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
    {
        return _requestedEnrollmentDirection;
    }

    /**
     * @param requestedEnrollmentDirection Выбранное направление подготовки. Свойство не может быть null и должно быть уникальным.
     */
    public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        dirty(_requestedEnrollmentDirection, requestedEnrollmentDirection);
        _requestedEnrollmentDirection = requestedEnrollmentDirection;
    }

    /**
     * @return Номер договора. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getContractNumber()
    {
        return _contractNumber;
    }

    /**
     * @param contractNumber Номер договора. Свойство не может быть null и должно быть уникальным.
     */
    public void setContractNumber(String contractNumber)
    {
        dirty(_contractNumber, contractNumber);
        _contractNumber = contractNumber;
    }

    /**
     * @return Дата договора. Свойство не может быть null.
     */
    @NotNull
    public Date getContractDate()
    {
        return _contractDate;
    }

    /**
     * @param contractDate Дата договора. Свойство не может быть null.
     */
    public void setContractDate(Date contractDate)
    {
        dirty(_contractDate, contractDate);
        _contractDate = contractDate;
    }

    /**
     * @return Плательщик по договору. Свойство не может быть null.
     */
    @NotNull
    public String getContractPayer()
    {
        return _contractPayer;
    }

    /**
     * @param contractPayer Плательщик по договору. Свойство не может быть null.
     */
    public void setContractPayer(String contractPayer)
    {
        dirty(_contractPayer, contractPayer);
        _contractPayer = contractPayer;
    }

    /**
     * @return Договор расторгнут. Свойство не может быть null.
     */
    @NotNull
    public boolean isContractTerminated()
    {
        return _contractTerminated;
    }

    /**
     * @param contractTerminated Договор расторгнут. Свойство не может быть null.
     */
    public void setContractTerminated(boolean contractTerminated)
    {
        dirty(_contractTerminated, contractTerminated);
        _contractTerminated = contractTerminated;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEntrantContractGen)
        {
            setRequestedEnrollmentDirection(((FefuEntrantContract)another).getRequestedEnrollmentDirection());
            setContractNumber(((FefuEntrantContract)another).getContractNumber());
            setContractDate(((FefuEntrantContract)another).getContractDate());
            setContractPayer(((FefuEntrantContract)another).getContractPayer());
            setContractTerminated(((FefuEntrantContract)another).isContractTerminated());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEntrantContractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEntrantContract.class;
        }

        public T newInstance()
        {
            return (T) new FefuEntrantContract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "requestedEnrollmentDirection":
                    return obj.getRequestedEnrollmentDirection();
                case "contractNumber":
                    return obj.getContractNumber();
                case "contractDate":
                    return obj.getContractDate();
                case "contractPayer":
                    return obj.getContractPayer();
                case "contractTerminated":
                    return obj.isContractTerminated();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "requestedEnrollmentDirection":
                    obj.setRequestedEnrollmentDirection((RequestedEnrollmentDirection) value);
                    return;
                case "contractNumber":
                    obj.setContractNumber((String) value);
                    return;
                case "contractDate":
                    obj.setContractDate((Date) value);
                    return;
                case "contractPayer":
                    obj.setContractPayer((String) value);
                    return;
                case "contractTerminated":
                    obj.setContractTerminated((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "requestedEnrollmentDirection":
                        return true;
                case "contractNumber":
                        return true;
                case "contractDate":
                        return true;
                case "contractPayer":
                        return true;
                case "contractTerminated":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "requestedEnrollmentDirection":
                    return true;
                case "contractNumber":
                    return true;
                case "contractDate":
                    return true;
                case "contractPayer":
                    return true;
                case "contractTerminated":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "requestedEnrollmentDirection":
                    return RequestedEnrollmentDirection.class;
                case "contractNumber":
                    return String.class;
                case "contractDate":
                    return Date.class;
                case "contractPayer":
                    return String.class;
                case "contractTerminated":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEntrantContract> _dslPath = new Path<FefuEntrantContract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEntrantContract");
    }
            

    /**
     * @return Выбранное направление подготовки. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantContract#getRequestedEnrollmentDirection()
     */
    public static RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
    {
        return _dslPath.requestedEnrollmentDirection();
    }

    /**
     * @return Номер договора. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantContract#getContractNumber()
     */
    public static PropertyPath<String> contractNumber()
    {
        return _dslPath.contractNumber();
    }

    /**
     * @return Дата договора. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantContract#getContractDate()
     */
    public static PropertyPath<Date> contractDate()
    {
        return _dslPath.contractDate();
    }

    /**
     * @return Плательщик по договору. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantContract#getContractPayer()
     */
    public static PropertyPath<String> contractPayer()
    {
        return _dslPath.contractPayer();
    }

    /**
     * @return Договор расторгнут. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantContract#isContractTerminated()
     */
    public static PropertyPath<Boolean> contractTerminated()
    {
        return _dslPath.contractTerminated();
    }

    public static class Path<E extends FefuEntrantContract> extends EntityPath<E>
    {
        private RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> _requestedEnrollmentDirection;
        private PropertyPath<String> _contractNumber;
        private PropertyPath<Date> _contractDate;
        private PropertyPath<String> _contractPayer;
        private PropertyPath<Boolean> _contractTerminated;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выбранное направление подготовки. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantContract#getRequestedEnrollmentDirection()
     */
        public RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection> requestedEnrollmentDirection()
        {
            if(_requestedEnrollmentDirection == null )
                _requestedEnrollmentDirection = new RequestedEnrollmentDirection.Path<RequestedEnrollmentDirection>(L_REQUESTED_ENROLLMENT_DIRECTION, this);
            return _requestedEnrollmentDirection;
        }

    /**
     * @return Номер договора. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantContract#getContractNumber()
     */
        public PropertyPath<String> contractNumber()
        {
            if(_contractNumber == null )
                _contractNumber = new PropertyPath<String>(FefuEntrantContractGen.P_CONTRACT_NUMBER, this);
            return _contractNumber;
        }

    /**
     * @return Дата договора. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantContract#getContractDate()
     */
        public PropertyPath<Date> contractDate()
        {
            if(_contractDate == null )
                _contractDate = new PropertyPath<Date>(FefuEntrantContractGen.P_CONTRACT_DATE, this);
            return _contractDate;
        }

    /**
     * @return Плательщик по договору. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantContract#getContractPayer()
     */
        public PropertyPath<String> contractPayer()
        {
            if(_contractPayer == null )
                _contractPayer = new PropertyPath<String>(FefuEntrantContractGen.P_CONTRACT_PAYER, this);
            return _contractPayer;
        }

    /**
     * @return Договор расторгнут. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEntrantContract#isContractTerminated()
     */
        public PropertyPath<Boolean> contractTerminated()
        {
            if(_contractTerminated == null )
                _contractTerminated = new PropertyPath<Boolean>(FefuEntrantContractGen.P_CONTRACT_TERMINATED, this);
            return _contractTerminated;
        }

        public Class getEntityClass()
        {
            return FefuEntrantContract.class;
        }

        public String getEntityName()
        {
            return "fefuEntrantContract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
