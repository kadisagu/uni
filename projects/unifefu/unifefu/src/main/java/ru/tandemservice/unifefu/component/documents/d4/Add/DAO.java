/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d4.Add;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeRel;

/**
 * @author Alexey Lopatin
 * @since 16.09.2013
 */
public class DAO extends ru.tandemservice.uni.component.documents.d4.Add.DAO
{
    @Override
    public void prepare(ru.tandemservice.uni.component.documents.d4.Add.Model model)
    {
        super.prepare(model);
        model.setTrainingOfficersActive(true);

        String levelStageCode = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getRoot().getCode();
        FefuSignerStuDocTypeRel signer = FefuStudentManager.instance().printInjectModifierDao().getSignerDocumentType(model.getStudentDocumentType().getCode(), levelStageCode);

        model.setRectorAltStr(signer.getSigner().getEntity().getPerson().getIdentityCard().getIof());
    }
}
