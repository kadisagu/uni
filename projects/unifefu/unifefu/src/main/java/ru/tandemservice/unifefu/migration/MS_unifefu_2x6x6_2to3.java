package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x6_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDirectumSettings

		// создано обязательное свойство checkMedicalDocs
		{
			// создать колонку
			tool.createColumn("fefudirectumsettings_t", new DBColumn("checkmedicaldocs_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultCheckMedicalDocs = Boolean.FALSE;
			tool.executeUpdate("update fefudirectumsettings_t set checkmedicaldocs_p=? where checkmedicaldocs_p is null", defaultCheckMedicalDocs);

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefudirectumsettings_t", "checkmedicaldocs_p", false);

		}


    }
}