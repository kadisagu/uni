/**
 * SubmitNotificationsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.portal._2014_04_06;

public interface SubmitNotificationsService extends javax.xml.rpc.Service
{
    public java.lang.String getSubmitNotificationsPortAddress();

    public SubmitNotifications getSubmitNotificationsPort() throws javax.xml.rpc.ServiceException;

    public SubmitNotifications getSubmitNotificationsPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}