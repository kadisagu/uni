/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.axis.AxisFault;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.*;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow;
import ru.tandemservice.unifefu.entity.FefuSendingRatingPackage;
import ru.tandemservice.unifefu.entity.FefuStudentMarkHistory;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;
import ru.tandemservice.unifefu.ws.rateportal.*;
import ru.tandemservice.unifefu.ws.rateportalmass.Commit;
import ru.tandemservice.unifefu.ws.rateportalmass.*;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsIRatingValueFormatter;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 18.02.2014
 */
public class FEFUPortalStatusesDaemonDao extends UniBaseDao implements IFEFUPortalStatusesDaemonDao
{
	private static final int PERSON_MAX_NUMBER_IN_PACKAGE = 10;

	public static final String CRITERIA_SEPARATOR = "<br/> ";
	public static final String DELETED_MARK_SIGN = "-";
	public static final String SENDING_STATUS = "==== Отправка на портал пакета с рейтингами студентов, ID = ";
	public static final String COMMITS_IS_NULL = "Commits is \"NULL\"";

	public static final String STATUS_PACK_SYSTEM_CODE = "WR";
	public static final String STATUS_PACK_FUNCTION_CODE = "StudentRate";
	public static final String STATUS_PACK_CONTENT_TYPE = "text/xml";
	public static final String FILE_DOCUMENT_CODE = "StudentRate";

	public static final String GROUP_TITLE_WITHOUT_GROUP = "Без группы";

	private static final ObjectFactory FACTORY = new ObjectFactory();

	protected static final Logger log4j_logger = Logger.getLogger(FEFUPortalStatusesDaemonDao.class);
	public static final String XML_HEADER_REGEXP = "\\<\\?xml version=\"1\\.0\" encoding=\"UTF-8\" standalone=\"yes\"\\?\\>";

	public static void logEvent(Level loggingLevel, String message)
	{
		// проверяем, есть ли уже appender
		Appender appender = log4j_logger.getAppender("PortalStatusesAppender");

		if (null != appender)
		{
			log4j_logger.log(loggingLevel, message);
		}

		if (null == appender)
		{
			try
			{
				// добавляем, если нет
				final String path = ApplicationRuntime.getAppInstallPath();
				Calendar cal = CoreDateUtils.createCalendar(new Date());
				String logFileName = "PortalStatus_" + cal.get(Calendar.YEAR) + "_" + (cal.get(Calendar.MONTH) + 1) + "_" + cal.get(Calendar.DAY_OF_MONTH) + ".log";
				try
				{
					appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} %p - %m%n"), FilenameUtils.concat(path, "tomcat/logs/" + logFileName));
					appender.setName("PortalStatusesAppender");
					((FileAppender) appender).setThreshold(Level.INFO);
					log4j_logger.addAppender(appender);
				}
				catch (IOException ex)
				{
					//
				}

				log4j_logger.setLevel(Level.INFO);
				if (null != appender)
				{
					log4j_logger.log(loggingLevel, message);
				}
			}
			finally
			{
				// и отцепляем, если добавляли
				if (null != appender)
				{
					log4j_logger.removeAppender(appender);
				}
			}
		}
	}

	public static final SyncDaemon DAEMON = new SyncDaemon(FEFUPortalStatusesDaemonDao.class.getName(), 1440, IFEFUPortalStatusesDaemonDao.GLOBAL_DAEMON_LOCK)
	{
		@Override
		protected void main()
		{
			final IFEFUPortalStatusesDaemonDao dao = IFEFUPortalStatusesDaemonDao.instance.get();

			try
			{
				dao.doSendPortalStatuses();
			}
			catch (final Throwable t)
			{
				Debug.exception(t.getMessage(), t);
				this.logger.warn(t.getMessage(), t);
			}

		}
	};

	@Override
	public void doSendPortalStatuses()
	{
		if (null == ApplicationRuntime.getProperty(SubmitFileServiceLocator.PORTAL_SUBMIT_STATUS_SERVICE_URL))
			return;

		// Время запуска демона
		long startTime = System.currentTimeMillis();
		long lastOperationTime = System.currentTimeMillis();
		logEvent(Level.INFO, "||||| Демон отправки рейтингов на портал ДВФУ начал работу");

		Map<String, CoreCollectionUtils.Pair<StudentType, Set<FefuRatingPackageStudentRow>>> preparedStudentRatingPacks = prepareStudentRatingPacks();
		logEvent(Level.INFO, String.format("Подготовка данных по рейтингам студентов заняла %d мсек", (System.currentTimeMillis() - lastOperationTime)));
		lastOperationTime = System.currentTimeMillis();
		List<RequestPack> requestList = prepareRequestList(preparedStudentRatingPacks);
		logEvent(Level.INFO, String.format("Подготовка пакетов по рейтингам студентов для отправки на портал заняла %d мсек", (System.currentTimeMillis() - lastOperationTime)));
		lastOperationTime = System.currentTimeMillis();

		SubmitFileServiceLocator locator = new SubmitFileServiceLocator();
		for (RequestPack requestPack : requestList)
		{
			FileMetadata fileMetadata = new FileMetadata();

			Calendar calendar = new GregorianCalendar();
			calendar.setTime(new Date());
			fileMetadata.setCreatedate(calendar);
			fileMetadata.setModifydate(calendar);
			fileMetadata.setInputid(NsiReactorUtils.generateGUID(new Date().getTime()));
			fileMetadata.setComment("Рейтинг студента на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(calendar.getTime()));
			fileMetadata.setFilename("Текущий рейтинг студента на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(calendar.getTime()));
			fileMetadata.setContenttype(STATUS_PACK_CONTENT_TYPE);
			fileMetadata.setSystemcode(STATUS_PACK_SYSTEM_CODE);
			fileMetadata.setFunctioncode(STATUS_PACK_FUNCTION_CODE);

			fileMetadata.setDocumentcode(FILE_DOCUMENT_CODE);


			ru.tandemservice.unifefu.ws.rateportalmass.File requestFile = new File();
			requestFile.setFilemetadata(fileMetadata);
			requestFile.setData(requestPack.getFefuSendingRatingPackage().getSendingData().getBytes());

			SubmitFileRequest request = new SubmitFileRequest();
			request.setFile(requestFile);

			FefuSendingRatingPackage ratingPackage = requestPack.getFefuSendingRatingPackage();
			// set package sending time
			ratingPackage.setSendingTime(new Date());
			// try to send file
			SendingResult result = executeSingleRequestPost(locator, request);
			// set sending result
			ratingPackage.setSendingResult(result.getSendingResult());
			// set external id (may be null)
			ratingPackage.setExternalId(result.getExternalId());
			//save log data in new transaction
			IFEFUPortalStatusesDaemonDao.instance.get().saveLogData(requestPack);
		}
		logEvent(Level.INFO, String.format("Отправка данных по рейтингам студентов на портал заняла %d мсек", (System.currentTimeMillis() - lastOperationTime)));
		logEvent(Level.INFO, String.format("||||| Полное время работы демона отправки рейтингов составило %d  мсек", (System.currentTimeMillis() - startTime)));
	}

	private SendingResult executeSingleRequestPost(SubmitFileServiceLocator locator, SubmitFileRequest request)
	{
		try
		{
			logEvent(Level.INFO, SENDING_STATUS + request.getFile().getFilemetadata().getInputid());
			SubmitFileResponse response = locator.getSubmitFileSOAP11Port().submitFile(request);
			ru.tandemservice.unifefu.ws.rateportalmass.Commit[] resCommits = response.getCommits();

			if(resCommits == null)
				throw new ApplicationException(COMMITS_IS_NULL);

			for (ru.tandemservice.unifefu.ws.rateportalmass.Commit commit : resCommits)
			{
				logEvent(Level.INFO, String.format("%s %s  -  Успешно (ИД статуса на портале: %s)",SENDING_STATUS,request.getFile().getFilemetadata().getInputid(), commit.getOutputid()));
			}
			return new SendingResult(resCommits);
		}
		catch (Exception e)
		{
			logEvent(Level.ERROR,  String.format("%s %s  -  ОШИБКА !!!!!", SENDING_STATUS ,request.getFile().getFilemetadata().getInputid()));
			logEvent(Level.ERROR, e.getMessage());
			e.printStackTrace(System.err);
			return new SendingResult(e);
		}
	}

	private List<RequestPack> prepareRequestList(Map<String, CoreCollectionUtils.Pair<StudentType, Set<FefuRatingPackageStudentRow>>> studentRatingPacks)
	{
		List<RequestPack> requestList = new ArrayList<>();
		Set<FefuRatingPackageStudentRow> allStudentRows = new HashSet<>();
		StringBuilder data = new StringBuilder("");
		int counter = 0;
		for (Map.Entry<String, CoreCollectionUtils.Pair<StudentType, Set<FefuRatingPackageStudentRow>>> entry : studentRatingPacks.entrySet())
		{
			if (counter > 0)
				data.append(new String(NsiReactorUtils.toXml(entry.getValue().getX())).replaceFirst(XML_HEADER_REGEXP, ""));
			else
				data.append(new String(NsiReactorUtils.toXml(entry.getValue().getX())).replaceFirst(XML_HEADER_REGEXP, XML_HEADER_REGEXP + "\n<students>"));

			allStudentRows.addAll(entry.getValue().getY());
			if(++counter >= PERSON_MAX_NUMBER_IN_PACKAGE)
			{
				data.append("</students>");
				FefuSendingRatingPackage ratingPackage = new FefuSendingRatingPackage();
				ratingPackage.setSendingData(data.toString());
				data.setLength(0);
				requestList.add(new RequestPack(allStudentRows, ratingPackage));
				allStudentRows = new HashSet<>();
				counter = 0;
			}
		}
		if(counter > 0)
		{
			data.append("</students>");
			FefuSendingRatingPackage ratingPackage = new FefuSendingRatingPackage();
			ratingPackage.setSendingData(data.toString());
			requestList.add(new RequestPack(allStudentRows, ratingPackage));
		}
		return requestList;
	}

	private Map<String, CoreCollectionUtils.Pair<StudentType, Set<FefuRatingPackageStudentRow>>> prepareStudentRatingPacks()
	{
		long lastOperationTime = System.currentTimeMillis();

		// Кэш для рейтингов. Ключ - TrJournal.id, значение - рейтинг студентов по журналу
		Map<Long, IBrsDao.ICurrentRatingCalc> ratingMap = new HashMap<>();

		// Кэш дополнительных атрибутов журнала. Ключ - TrJournal.id, значение - мап (ключ - код параметра) всех дополнительных атрибутов журнала
		Map<Long, Map<String, ISessionBrsDao.IRatingAdditParamDef>> additParamDefMap = new HashMap<>();

		// Подготавливаем Мап с фио преподавателей для групп журнала. Ключ TrJournalGroup.id, значение - ФИО преподавателей через запятую
		Map<Long, String> tutorsMap = prepareTutorsMap();
		logEvent(Level.INFO, String.format("1. Подготовка данных по преподавателям в журналах текущего учебного года: %d  мсек", (System.currentTimeMillis() - lastOperationTime)));
		lastOperationTime = System.currentTimeMillis();

		// Подготавливаем Мап событий студента. Ключ - EppStudentWorkPlanElement.id, событие студента
		Map<Long, List<TrEduGroupEventStudent>> studentToEventsMap = prepareStudentEventsMap();
		logEvent(Level.INFO, String.format("2. Подготовка данных по событиям студентов в журналах текущего учебного года: %d мсек", System.currentTimeMillis() - lastOperationTime));
		lastOperationTime = System.currentTimeMillis();

		// Подготавливаем Мап оценок. Ключ - пара(EppStudentWorkPlanElement.id, TrEduGroupEvent.id), значение - пара(оценка, дата оценки)
		// Дата оценки берется из истории оценок! - из последней (по дате) записи в истории.
		Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Date>> markActualMap = prepareMarkActualMap();
		// Подготавливаем Мап истории оценок. Ключ - пара(EppStudentWorkPlanElement.id, TrEduGroupEvent.id), значение - иторическая оценка
		//Map<CoreCollectionUtils.Pair<Long, Long>, List<FefuStudentMarkHistory>> markHistoryMap = prepareMarkHistoryMap();
		logEvent(Level.INFO, String.format("3. Подготовка оценок в журналах текущего учебного года: %d мсек", (System.currentTimeMillis() - lastOperationTime)));
		lastOperationTime = System.currentTimeMillis();


		// Подготавливаем Мап коэффициентов БРС для КМ. Ключ - TrJournalEvent.id, значение - список коэффициентов БРС для КМ журнала
		Map<Long, List<TrBrsCoefficientValue>> brsCoeffsMap = prepareJournalEventBrsCoefficients();
		logEvent(Level.INFO, String.format("4. Подготовка данных по коэффициентам БРС в событиях журналов текущего учебного года: %d мсек", (System.currentTimeMillis() - lastOperationTime)));
		lastOperationTime = System.currentTimeMillis();

		//Подготавливаем Мап коэффициентов БРС для журнала. Ключ - TrJournal.id, значение - список коэффициентов БРС для журнала
		Map<Long, List<TrBrsCoefficientValue>> journalCoeffsMap = prepareJournalBrsCoefficients();
		logEvent(Level.INFO, String.format("5. Подготовка данных по коэффициентам БРС в журналах текущего учебного года: %d мсек", (System.currentTimeMillis() - lastOperationTime)));
		lastOperationTime = System.currentTimeMillis();


		// ПОСТРОЕНИЕ ПАКЕТОВ

		// Получаем список всех студентов УГС, включенных в журнал (в реализацию дисциплины)
		Multimap<FefuNsiIds, TrJournalGroupStudent> studentSlotsMap = getStudentSlotsMap();
		logEvent(Level.INFO, String.format("6. Получение полного списка студентов в журналах текущего учебного года: %d мсек", (System.currentTimeMillis() - lastOperationTime)));

		// Результирующий Мап с подготовленными пакетами статусов
		Map<String, CoreCollectionUtils.Pair<StudentType, Set<FefuRatingPackageStudentRow>>> studentRatingPacksMap = new HashMap<>();

		// Мап для курсов. Ключ - TrJournalGroup.id, значение - курс
		Map<Long, CourseType> courseMap = new HashMap<>();

		logEvent(Level.INFO, "7. Обработка списка студентов в журналах текущего учебного года: ");
		// мап хранящий перечень наименований групп одной персоны
		Map<CoreCollectionUtils.Pair<String, String>, GroupType> personsGroupsMap = new HashMap<>();
		int studentCounter = 0;
		for (Map.Entry<FefuNsiIds, Collection<TrJournalGroupStudent>> entry : studentSlotsMap.asMap().entrySet())
		{
			lastOperationTime = System.currentTimeMillis();
			studentCounter++;
            List<TrJournalGroupStudent> journals = new ArrayList<>(entry.getValue());
            Collections.sort(journals,new JournalsComparator());

            for (TrJournalGroupStudent journalStudent : journals)
			{
                EppStudentWorkPlanElement slot = journalStudent.getStudentWpe();
				TrJournalGroup group = journalStudent.getGroup();
				Student student = slot.getStudent();
				TrJournal journal = group.getJournal();
				// Получаем закэшированный набор дополнительных атрибутов журнала, либо поднимаем его из базы и кэшируем
				Map<String, ISessionBrsDao.IRatingAdditParamDef> journalAdditParamDefMap = additParamDefMap.get(journal.getId());
				if (null == journalAdditParamDefMap)
				{
					try
					{
						journalAdditParamDefMap = TrBrsCoefficientManager.instance().brsDao().getRatingAdditionalParamDefinitions(journal);
						additParamDefMap.put(journal.getId(), journalAdditParamDefMap);
					}
					catch (Exception e)
					{
						logEvent(Level.ERROR, "    !!! " + e.getMessage());
						logEvent(Level.ERROR, String.format("\tСтудент %s, guid=%s журнал=%s",student.getPerson().getFullFio(), entry.getKey(), journal.getTitle()));
						log4j_logger.trace("----", e);
						e.printStackTrace(System.err);
					}
				}

				String guid = entry.getKey().getGuid();

				StudentType studentPortal;
				GroupType groupPortal;
				Set<FefuRatingPackageStudentRow> studentRow;

				if (!studentRatingPacksMap.containsKey(guid))
				{
					studentRow = new HashSet<>();
					studentPortal = FACTORY.createStudentType();
					studentPortal.setRatingtitle("Текущий рейтинг студента на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
					studentPortal.setName(student.getPerson().getFullFio());

					studentPortal.setGroups(FACTORY.createGroupsType());

					groupPortal = FEFUPortalStatusesDaemonDao.createStudentType(student);
					studentPortal.getGroups().getGroup().add(groupPortal);
					personsGroupsMap.put(new CoreCollectionUtils.Pair<>(guid, groupPortal.getName()), groupPortal);
					//В соответсвии с DEV-5300 п.3
					//if (studentPortal.getName().contains("Лопатина")) studentPortal.setPersonGuid("00000000-0000-0000-4444-000000000000");
					//else
					studentPortal.setPersonGuid(guid);
				}
				else
				{
					studentPortal = studentRatingPacksMap.get(guid).getX();
					studentRow = studentRatingPacksMap.get(guid).getY();

					CoreCollectionUtils.Pair<String, String> key = new CoreCollectionUtils.Pair<>(guid, FEFUPortalStatusesDaemonDao.getGroupTitle(student));
					groupPortal = personsGroupsMap.get(key);
					if (groupPortal == null)
					{
						groupPortal = FEFUPortalStatusesDaemonDao.createStudentType(student);
						studentPortal.getGroups().getGroup().add(groupPortal);
						personsGroupsMap.put(key, groupPortal);
					}
				}
				CourseType coursePortal = courseMap.get(slot.getId());

				if (null == coursePortal)
				{
					coursePortal = FACTORY.createCourseType();
					coursePortal.setName(journal.getRegistryElementPart().getRegistryElement().getTitle());
					if (null != tutorsMap.get(group.getId()))
						coursePortal.setTeacherName(tutorsMap.get(group.getId()));

					// Получаем закэшированный рейтинг студентов по журналу, либо поднимаем его из базы и кэшируем
					IBrsDao.ICurrentRatingCalc rating = ratingMap.get(journal.getId());
					if (null == rating)
					{
						try
						{
							rating = TrBrsCoefficientManager.instance().brsDao().getCalculatedRating(journal);
						}
						catch (Exception e)
						{
							logEvent(Level.ERROR, "    !!! " + e.getMessage());
							logEvent(Level.ERROR, String.format("\tСтудент %s, guid=%s курс=%s",student.getPerson().getFullFio(), studentPortal.getPersonGuid(), coursePortal.getName()));
							log4j_logger.trace("----", e);
							e.printStackTrace(System.err);
						}
					}

					ratingMap.put(journal.getId(), rating);

					if (null != rating)
					{
						IBrsDao.IStudentCurrentRatingData studentRating = rating.getCurrentRating(slot);
						ISessionBrsDao.IRatingValue ratValue = studentRating.getRatingValue();
						if (null != ratValue)
						{
							coursePortal.setRatingCurrent(null != ratValue.getMessage() ? ratValue.getMessage() : (BrsIRatingValueFormatter.instance.format(ratValue) + "%"));
						}

						coursePortal.setRatingFinal(getRightValue(studentRating.getRatingAdditParam(FefuBrs.TOTAL_COLUMN_KEY), "%"));
						coursePortal.setResult(getRightValue(studentRating.getRatingAdditParam(FefuBrs.MARK_COLUMN_KEY), null));
						coursePortal.setResultCurrent(getRightValue(studentRating.getRatingAdditParam(FefuBrs.MARK_CURRENT_COLUMN_KEY), null));
						coursePortal.setUrl(getRightValue(studentRating.getRatingAdditParam(FefuBrs.CURRENT_GROUP_RATING_COLUMN_KEY), null));
						coursePortal.setHasRequiredPoints(getRightBooleanValue(studentRating.getRatingAdditParam(FefuBrs.VALID_TOTAL_COLUMN_KEY)));
						coursePortal.setHasRequiredPointsCurrent(getRightBooleanValue(studentRating.getRatingAdditParam(FefuBrs.VALID_CURRENT_COLUMN_KEY)));
					}

					if (null != journalCoeffsMap.get(journal.getId()))
					{
						List<TrBrsCoefficientValue> coefficientValueList = journalCoeffsMap.get(journal.getId());
						if (null != coefficientValueList)
						{
							StringBuilder criteriaBuilder = new StringBuilder();
							boolean isNotFirst = false;
							for (TrBrsCoefficientValue brsCoefficientValue : coefficientValueList)
							{
								if (!"0".equals(brsCoefficientValue.getValueStr()))
								{
									criteriaBuilder.append(isNotFirst ? CRITERIA_SEPARATOR : "").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(brsCoefficientValue.getValueAsDouble())).append("% ").append(brsCoefficientValue.getDefinition().getShortTitle());
									isNotFirst = true;
								}
							}
							coursePortal.setCriteria(criteriaBuilder.toString());
						}
					}

					groupPortal.getCourse().add(coursePortal);
					courseMap.put(slot.getId(), coursePortal);
				}

				if (studentToEventsMap.containsKey(slot.getId()))
				{
					Collections.sort(studentToEventsMap.get(slot.getId()), new Comparator<TrEduGroupEventStudent>()
					{
						@Override
						public int compare(TrEduGroupEventStudent o1, TrEduGroupEventStudent o2)
						{
							ScheduleEvent e1 = o1.getEvent().getScheduleEvent();
							ScheduleEvent e2 = o2.getEvent().getScheduleEvent();
							if(e1 == null && e2 == null) return 0;
							if(e1 != null && e2 != null) return e1.getDurationBegin().compareTo(e2.getDurationBegin());
							if(e1 != null) return 1;
							else return -1;
						}
					});
					for (TrEduGroupEventStudent eventStud : studentToEventsMap.get(slot.getId()))
					{
						TaskType taskPortal = FACTORY.createTaskType();
						taskPortal.setName(eventStud.getEvent().getJournalEvent().getTheme());
						taskPortal.setMarkDue(DateFormatter.DEFAULT_DATE_FORMATTER.format(eventStud.getEvent().getDeadlineDate()));
						taskPortal.setTypeName(eventStud.getEvent().getJournalEvent().getType().getTitle());

						Date dueDate = null != eventStud.getEvent().getScheduleEvent() ? eventStud.getEvent().getScheduleEvent().getDurationBegin() : null;
						taskPortal.setDue(DateFormatter.DEFAULT_DATE_FORMATTER.format(dueDate));

						List<TrBrsCoefficientValue> coefficientValueList = brsCoeffsMap.get(eventStud.getEvent().getJournalEvent().getId());
						if (null != coefficientValueList)
						{
							for (TrBrsCoefficientValue brsCoefficientValue : coefficientValueList)
							{
								if (FefuBrs.MIN_POINTS_CODE.equals(brsCoefficientValue.getDefinition().getUserCode()))
								{
									taskPortal.setRequiredPoints(brsCoefficientValue.getValueStr());
								}
								if (FefuBrs.MAX_POINTS_CODE.equals(brsCoefficientValue.getDefinition().getUserCode()))
								{
									taskPortal.setMaxPoints(brsCoefficientValue.getValueStr());
								}
								if (FefuBrs.WEIGHT_EVENT_CODE.equals(brsCoefficientValue.getDefinition().getUserCode()))
								{
									taskPortal.setWeight(brsCoefficientValue.getValueStr());
								}
							}
						}

						CoreCollectionUtils.Pair<Long, Long> key = new CoreCollectionUtils.Pair<>(slot.getId(), eventStud.getEvent().getId());
						CoreCollectionUtils.Pair<Double, Date> actualMark = markActualMap.get(key);

						if (actualMark != null)
						{
							MarkType markPortal = FACTORY.createMarkType();
							if (actualMark.getX() != null)
							{
								//заполняем поле оценки и дату оценки
								markPortal.setValue(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(actualMark.getX()));
								markPortal.setMdate(DateFormatter.DEFAULT_DATE_FORMATTER.format(actualMark.getY()));
							}
							else
							{
								// если есть история но нет оценки ставим "-"
								markPortal.setValue(DELETED_MARK_SIGN);
								// дату не ставим
								markPortal.setMdate(null);
							}
							taskPortal.getMark().add(markPortal);
						}

						coursePortal.getTask().add(taskPortal);
					}
				}
				studentRow.add(prepareStudentRow(entry.getKey(), student));
				studentRatingPacksMap.put(guid, new CoreCollectionUtils.Pair<>(studentPortal, studentRow));
			}
			logEvent(Level.INFO, String.format("\tПодготовка данных по студенту #%d:%s заняла %d мсек", studentCounter, entry.getKey().getGuid(), System.currentTimeMillis() - lastOperationTime ));
		}
		return studentRatingPacksMap;
	}

	private String getRightValue(ISessionBrsDao.IRatingValue value, String measureUnit)
	{
		if (null == value) return null;
		return null != value.getMessage() ? value.getMessage() : (DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(value.getValue()) + (null != measureUnit ? measureUnit : ""));
	}

	private String getRightBooleanValue(ISessionBrsDao.IRatingValue value)
	{
		if (null == value) return null;
		return FefuBrs.TRUE_ROW_VALUE.equals(value) ? "1" : (FefuBrs.FALSE_ROW_VALUE.equals(value) ? "0" : "-");
	}

	private Map<Long, String> prepareTutorsMap()
	{
		DQLSelectBuilder pps = new DQLSelectBuilder()
				.fromEntity(EppPpsCollectionItem.class, "rel")
				.column(property(TrJournalGroup.id().fromAlias("jRel")))
				.column(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("rel")))
				.predicate(DQLPredicateType.distinct)
				.joinEntity("rel", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "grp", eq(property(EppPpsCollectionItem.list().fromAlias("rel")), property("grp")))
				.joinEntity("grp", DQLJoinType.inner, TrJournalGroup.class, "jRel", eq(property(TrJournalGroup.group().fromAlias("jRel")), property("grp")))
				.where(eq(property(TrJournalGroup.journal().yearPart().year().educationYear().current().fromAlias("jRel")), value(Boolean.TRUE)))
				.order(property(TrJournalGroup.id().fromAlias("jRel")))
				.order(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("rel")));

		DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(pps);

		int idIdx = dql.column(property(TrJournalGroup.id().fromAlias("jRel")));
		int fioIdx = dql.column(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("rel")));

		Map<Long, String> tutorsMap = new HashMap<>();

		for (Object[] item : scrollRows(dql.getDql().createStatement(getSession())))
		{
			Long grpId = (Long) item[idIdx];
			String tutors = tutorsMap.get(grpId);
			if (null == tutors) tutors = (String) item[fioIdx];
			else tutors += ", " + item[fioIdx];
			tutorsMap.put(grpId, tutors);
		}
		return tutorsMap;
	}

	private Map<Long, List<TrEduGroupEventStudent>> prepareStudentEventsMap()
	{
		DQLSelectBuilder eventDQL = new DQLSelectBuilder()
				.fromEntity(TrEduGroupEventStudent.class, "event").column("event")
				.where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().yearPart().year().educationYear().current().fromAlias("event")), value(Boolean.TRUE)));
		final List<TrEduGroupEventStudent> events = IUniBaseDao.instance.get().getList(eventDQL);
		Map<Long, List<TrEduGroupEventStudent>> studentToEventsMap = new HashMap<>();
		for (TrEduGroupEventStudent event : events)
		{
			Long studentSlotId = event.getStudentWpe().getId();
			List<TrEduGroupEventStudent> studEventsList = studentToEventsMap.get(studentSlotId);
			if (null == studEventsList) studEventsList = new ArrayList<>();
			studEventsList.add(event);
			studentToEventsMap.put(studentSlotId, studEventsList);
		}

		return studentToEventsMap;
	}

	private Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Date>> prepareMarkActualMap()
	{
		// Получение наборов слот в журнале-событие-оценка-дата_оценки
		// Берутся только те оценки, у которых есть история.
		// Дата оценки берется из последней (по дате) записи в истории

        //  select
        //    m.event_id,
        //    m.studentSlot_id,
        //   m.gradeAsLong_p,
        //    h.mark_date
        //    from
        //    (
        //        select
        //        hist.trEduGroupEventStudent_id stud_id,
        //        max(hist.markDate_p) mark_date
        //        from FEFUSTUDENTMARKHISTORY_T hist
        //        group by hist.trEduGroupEventStudent_id
        //    ) h
        //    join tr_edugrp_event_student m on m.id = h.stud_id

		DQLSelectBuilder histDQL = new DQLSelectBuilder().fromEntity(FefuStudentMarkHistory.class, "hist")
				.column(property("hist", FefuStudentMarkHistory.L_TR_EDU_GROUP_EVENT_STUDENT), "stud_id")
				.column(DQLFunctions.max(property("hist", FefuStudentMarkHistory.P_MARK_DATE)), "mark_date")
				.group(property("hist", FefuStudentMarkHistory.L_TR_EDU_GROUP_EVENT_STUDENT));

		DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
				new DQLSelectBuilder().fromDataSource(histDQL.buildQuery(), "h")
						.joinEntity("h", DQLJoinType.inner, TrEduGroupEventStudent.class, "m",
						            eq(property("h.stud_id"), property("m.id")))
		);

		int eventColumnIdx = dql.column(property("m", TrEduGroupEventStudent.event().id()));
		int slotColumnIdx = dql.column(property("m", TrEduGroupEventStudent.studentWpe().id()));
		int markColumnIdx = dql.column(property("m", TrEduGroupEventStudent.P_GRADE_AS_LONG));
		int markDateColumnIdx = dql.column("h.mark_date");

		Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Date>> markActualMap = new HashMap<>();

		Iterable<Object[]> rows = scrollRows(dql.getDql().createStatement(getSession()));
		for (Object[] item : rows)
		{
			Long eventId = (Long) item[eventColumnIdx];
			Long slotId = (Long) item[slotColumnIdx];
			Long markAsLong = (Long) item[markColumnIdx];
			Double mark = (markAsLong != null) ? (markAsLong * 0.01d) : null;
			Date markDate = (Date) item[markDateColumnIdx];

			CoreCollectionUtils.Pair<Long, Long> key = new CoreCollectionUtils.Pair<>(slotId, eventId);
			CoreCollectionUtils.Pair<Double, Date> value = new CoreCollectionUtils.Pair<>(mark, markDate);

			markActualMap.put(key, value);
		}
		return markActualMap;
	}

	private Map<Long, List<TrBrsCoefficientValue>> prepareJournalEventBrsCoefficients()
	{
		// Поднимаем коэффициенты для КМ в рамках журнала
		List<TrBrsCoefficientValue> brsCoeffsList = new DQLSelectBuilder().fromEntity(TrBrsCoefficientValue.class, "e").column("e")
				.joinEntity("e", DQLJoinType.inner, TrJournalEvent.class, "ev", eq(property(TrBrsCoefficientValue.owner().id().fromAlias("e")), property(TrJournalEvent.id().fromAlias("ev"))))
				.where(eq(property(TrJournalEvent.journalModule().journal().yearPart().year().educationYear().current().fromAlias("ev")), value(Boolean.TRUE)))
				.createStatement(getSession()).list();

		Map<Long, List<TrBrsCoefficientValue>> brsCoeffsMap = new HashMap<>();

		for (TrBrsCoefficientValue brsCoefficientValue : brsCoeffsList)
		{
			Long ownerId = brsCoefficientValue.getOwner().getId();
			List<TrBrsCoefficientValue> ownerBrsCoeffsList = brsCoeffsMap.get(ownerId);
			if (null == ownerBrsCoeffsList) ownerBrsCoeffsList = new ArrayList<>();
			ownerBrsCoeffsList.add(brsCoefficientValue);
			brsCoeffsMap.put(ownerId, ownerBrsCoeffsList);
		}

		return brsCoeffsMap;
	}

	private Map<Long, List<TrBrsCoefficientValue>> prepareJournalBrsCoefficients()
	{

		// Поднимаем коэффициенты для журнала
		List<TrBrsCoefficientValue> journalCoeffsList = new DQLSelectBuilder().fromEntity(TrBrsCoefficientValue.class, "e").column("e")
				.joinEntity("e", DQLJoinType.inner, TrJournal.class, "j", eq(property(TrBrsCoefficientValue.owner().id().fromAlias("e")), property(TrJournal.id().fromAlias("j"))))
				.where(eq(property(TrJournal.yearPart().year().educationYear().current().fromAlias("j")), value(Boolean.TRUE)))
				.createStatement(getSession()).list();

		Map<Long, List<TrBrsCoefficientValue>> journalCoeffsMap = new HashMap<>();

		for (TrBrsCoefficientValue brsCoefficientValue : journalCoeffsList)
		{
			Long ownerId = brsCoefficientValue.getOwner().getId();
			List<TrBrsCoefficientValue> ownerBrsCoeffsList = journalCoeffsMap.get(ownerId);
			if (null == ownerBrsCoeffsList) ownerBrsCoeffsList = new ArrayList<>();
			ownerBrsCoeffsList.add(brsCoefficientValue);
			journalCoeffsMap.put(ownerId, ownerBrsCoeffsList);
		}

		return journalCoeffsMap;
	}

	private Multimap<FefuNsiIds, TrJournalGroupStudent> getStudentSlotsMap()
	{
		DQLSelectBuilder stuSlotDQL = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e")
				.joinPath(DQLJoinType.inner, TrJournalGroupStudent.studentWpe().student().person().fromAlias("e"), "p")
				.joinEntity("p", DQLJoinType.inner, FefuNsiIds.class, "nsi", eq(property(Person.id().fromAlias("p")), property(FefuNsiIds.entityId().fromAlias("nsi"))))
				.where(eq(property(TrJournalGroupStudent.group().journal().yearPart().year().educationYear().current().fromAlias("e")), value(Boolean.TRUE)));

		DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(stuSlotDQL);

		int nsiColumnIdx = dql.column("nsi");
		int journalGroupColumnIdx = dql.column("e");

		Multimap<FefuNsiIds, TrJournalGroupStudent> studentSlotMap = ArrayListMultimap.create();

		for (Object[] item : scrollRows(dql.getDql().createStatement(getSession())))
		{
			FefuNsiIds nsiIds = (FefuNsiIds) item[nsiColumnIdx];
			studentSlotMap.put(nsiIds, (TrJournalGroupStudent) item[journalGroupColumnIdx]);
		}

		return studentSlotMap;
	}

	private FefuRatingPackageStudentRow prepareStudentRow(FefuNsiIds nsiIds, Student student)
	{
		FefuRatingPackageStudentRow row = new FefuRatingPackageStudentRow();
		row.setNsiId(nsiIds);
		row.setStudent(student);
		row.setFio(student.getPerson().getFullFio());
		row.setGroupNumber(FEFUPortalStatusesDaemonDao.getGroupTitle(student));
		return row;
	}

	private static GroupType createStudentType(Student student)
	{
		GroupType groupType = FACTORY.createGroupType();
		groupType.setName(FEFUPortalStatusesDaemonDao.getGroupTitle(student));
		return groupType;
	}
	private static String getGroupTitle( Student student)
	{
		return (null != student.getGroup() ? student.getGroup().getTitle() : GROUP_TITLE_WITHOUT_GROUP );
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	public void saveLogData(RequestPack pack)
	{
		save(pack.getFefuSendingRatingPackage());
		for (FefuRatingPackageStudentRow row : pack.getFefuRatingPackageStudentRows())
		{
			save(row);
		}
	}

    class RequestPack
	{
		private final FefuSendingRatingPackage _fefuSendingRatingPackage;
		private final Set<FefuRatingPackageStudentRow> _fefuRatingPackageStudentRows;

		private RequestPack(Set<FefuRatingPackageStudentRow> fefuRatingPackageStudentRows, FefuSendingRatingPackage fefuSendingRatingPackage)
		{
			this._fefuRatingPackageStudentRows = fefuRatingPackageStudentRows;
			this._fefuSendingRatingPackage = fefuSendingRatingPackage;
			for(FefuRatingPackageStudentRow row : fefuRatingPackageStudentRows)
			{
				row.setRatingPackage(_fefuSendingRatingPackage);
			}
		}

		public Set<FefuRatingPackageStudentRow> getFefuRatingPackageStudentRows()
		{
			return _fefuRatingPackageStudentRows;
		}

		public FefuSendingRatingPackage getFefuSendingRatingPackage()
		{
			return _fefuSendingRatingPackage;
		}
	}

	private class SendingResult
	{
		private static final String ANSWER_IS_NULL = "Answer is \"NULL\"";
		private static final String SEPARATOR = ";";
		public static final String FAULT_STRING_REGEXP = "\\<faultstring\\>(.*)\\</faultstring\\>";
		private final String _externalId;
		private final String _sendingResult;

		private SendingResult(@NotNull Exception exp)
		{
			StringBuilder sb = new StringBuilder("Exception ClassName: ");
			if (exp.getClass().getEnclosingClass() == null)
				sb.append(exp.getClass().getName());
			else
				sb.append(exp.getClass().getEnclosingClass().getName());

			sb.append(";\nMessage:");
			if(exp.getMessage() != null)
				sb.append(" ").append(exp.getMessage()).append(";");

			if(exp instanceof AxisFault)
			{
				sb.append("\nFaultString: ");
				Pattern pattern = Pattern.compile(FAULT_STRING_REGEXP);
				Matcher matcher = pattern.matcher(((AxisFault) exp).getFaultString());
				while (matcher.find())
				{
					sb.append(matcher.group()).append(";");
				}
			}

			this._sendingResult = sb.toString();
			this._externalId = null;
		}

		private SendingResult(@NotNull ru.tandemservice.unifefu.ws.rateportalmass.Commit[] commits)
		{
			StringBuilder answer = new StringBuilder("");
			StringBuilder externalId = new StringBuilder("");
			int count = 0;
			for(Commit commit: commits)
			{
				if(commit.getAnswer() != null)
					answer.append(commit.getAnswer());
				else
					answer.append(ANSWER_IS_NULL);

				if(commit.getOutputid() != null)
					externalId.append(commit.getOutputid());

				if(++count < commits.length)
				{
					answer.append(SEPARATOR);
					externalId.append(SEPARATOR);
				}

			}
			this._externalId = externalId.toString();
			this._sendingResult = answer.toString();
		}

		public String getExternalId()
		{
			return _externalId;
		}

		public String getSendingResult()
		{
			return _sendingResult;
		}
	}

    private final class JournalsComparator implements Comparator<TrJournalGroupStudent> {

        @Override
        public int compare(TrJournalGroupStudent o1, TrJournalGroupStudent o2) {

			final EppWorkPlanRow sourceRow1 = o1.getStudentWpe().getSourceRow();
			final EppWorkPlanRow sourceRow2 = o2.getStudentWpe().getSourceRow();
			final int term1 = sourceRow1 == null ? 0 : sourceRow1.getWorkPlan().getWorkPlan().getTerm().getIntValue();
			final int term2 = sourceRow2 == null ? 0 : sourceRow2.getWorkPlan().getWorkPlan().getTerm().getIntValue();
			if (term1 != term2)
				return Integer.compare(term2, term1);

			String title1 = o1.getGroup().getJournal().getRegistryElementPart().getRegistryElement().getTitle();
			String title2 = o2.getGroup().getJournal().getRegistryElementPart().getRegistryElement().getTitle();
			return title1.compareTo(title2);
		}
    }
}