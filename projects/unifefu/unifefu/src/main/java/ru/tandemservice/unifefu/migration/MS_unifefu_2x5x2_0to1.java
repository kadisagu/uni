/* $Id: MS_unifefu_2x5x2_0to1.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Denis Perminov
 * @since 21.04.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x2_0to1  extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // перезаполняем колонку с задачей Directum лог
        tool.getStatement().executeUpdate("update fdl set fdl.DIRECTUMTASKID_P=replace(fdl.DIRECTUMTASKID_P,';','')" +
                " from FEFUDIRECTUMLOG_T as fdl" +
                " where fdl.DIRECTUMTASKID_P is not null");
        // перезаполняем колонку с задачей студенческого расширения
        tool.getStatement().executeUpdate("update soe set soe.DIRECTUMTASKID_P=replace(soe.DIRECTUMTASKID_P,';','')" +
                " from FEFUSTUDENTORDEREXTENSION_T as soe" +
                " where soe.DIRECTUMTASKID_P is not null");
        // перезаполняем колонку с задачей студенческого расширения
        tool.getStatement().executeUpdate("update eoe set eoe.DIRECTUMTASKID_P=replace(eoe.DIRECTUMTASKID_P,';','')" +
                " from FEFUENTRANTORDEREXTENSION_T as eoe" +
                " where eoe.DIRECTUMTASKID_P is not null");
    }
}
