/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRole;
import ru.tandemservice.unifefu.ws.nsi.datagram.RoleType;
import ru.tandemservice.unifefu.ws.nsi.datagram.SubsystemType;

/**
 * @author Dmitry Seleznev
 * @since 11.02.2015
 */
public class RoleTypeReactor extends SimpleCatalogEntityNewReactor<RoleType, FefuNsiRole>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<FefuNsiRole> getEntityClass()
    {
        return FefuNsiRole.class;
    }

    @Override
    public Class<RoleType> getNSIEntityClass()
    {
        return RoleType.class;
    }

    @Override
    public RoleType getCatalogElementRetrieveRequestObject(String guid)
    {
        RoleType RoleType = NsiReactorUtils.NSI_OBJECT_FACTORY.createRoleType();
        if (null != guid) RoleType.setID(guid);
        return RoleType;
    }

    @Override
    public RoleType createEntity(CoreCollectionUtils.Pair<FefuNsiRole, FefuNsiIds> entityPair)
    {
        RoleType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createRoleType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setRoleName(entityPair.getX().getName());

        RoleType.SubsystemID subsystemID = NsiReactorUtils.NSI_OBJECT_FACTORY.createRoleTypeSubsystemID();
        SubsystemType subsystem = NsiReactorUtils.NSI_OBJECT_FACTORY.createSubsystemType();
        subsystem.setID(NsiObjectsHolder.getObSubsystemGuid());
        subsystemID.setSubsystem(subsystem);
        nsiEntity.setSubsystemID(subsystemID);

        return nsiEntity;
    }

    @Override
    public RoleType updateNsiEntityFields(RoleType nsiEntity, FefuNsiRole entity)
    {
        nsiEntity.setRoleName(entity.getName());
        return nsiEntity;
    }

    @Override
    public FefuNsiRole createEntity(RoleType nsiEntity)
    {
        return null; // Мы не можем принимать роли и создавать их откуда-то из вне
    }

    @Override
    public FefuNsiRole updatePossibleDuplicateFields(RoleType nsiEntity, CoreCollectionUtils.Pair<FefuNsiRole, FefuNsiIds> entityPair)
    {
        return null; // Мы не можем принимать роли и создавать их откуда-то из вне
    }
}