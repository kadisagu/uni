/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.e35;

import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudent.component.listextract.e35.utils.SplitFirstCourseStudentsGroupParagraphPartWrapper;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

/**
 * @author Alexey Lopatin
 * @since 10.09.2013
 */
public class SplitFirstCourseStudentsGroupStuListOrderPrint extends ru.tandemservice.movestudent.component.listextract.e35.SplitFirstCourseStudentsGroupStuListOrderPrint
{
    @Override
    public void additionalParagraphPartInjectModifier(RtfInjectModifier paragraphPartInjectModifier, SplitFirstCourseStudentsGroupParagraphPartWrapper paragraphPartWrapper)
    {
        StructureEducationLevels levels = paragraphPartWrapper.getEducationLevelsHighSchool().getEducationLevel().getLevelType();
        RtfString fefuEducationStrProfile = new RtfString();

        if (levels.isSpecialization() || levels.isProfile())
        {
            fefuEducationStrProfile.append(levels.isProfile() ? levels.isBachelor() ? "Профиль «" : "Магистерская программа «" : "Специализация «");
            fefuEducationStrProfile.append(paragraphPartWrapper.getEducationLevelsHighSchool().getTitle() + "»").par().append(IRtfData.TAB);
        }
        paragraphPartInjectModifier.put("fefuEducationStrProfile", fefuEducationStrProfile);
    }
}
