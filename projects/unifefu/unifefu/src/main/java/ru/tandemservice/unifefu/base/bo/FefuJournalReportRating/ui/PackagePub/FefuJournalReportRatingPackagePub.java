/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuJournalReportRating.ui.PackagePub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 29.04.2014
 */
@Configuration
public class FefuJournalReportRatingPackagePub extends BusinessComponentManager
{
    // data sources
    public static final String PACKAGE_PUB_DS = "packagePubDS";
    public static final String FEFU_PACKAGE_ID = "packageId";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PACKAGE_PUB_DS, getPackagePubDS(), packagePubDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getPackagePubDS()
    {
	    return columnListExtPointBuilder(PACKAGE_PUB_DS)
	            .addColumn(textColumn("fio", FefuRatingPackageStudentRow.fio()).order())
	            .addColumn(textColumn("groupNumber", FefuRatingPackageStudentRow.groupNumber()).order())
	            .addColumn(textColumn("nsiID", FefuRatingPackageStudentRow.nsiId().guid()).order())
	            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> packagePubDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute( DSInput input, ExecutionContext context)
            {
	            Long packageId = context.get(FEFU_PACKAGE_ID);
	            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuRatingPackageStudentRow.class, "row")
			            .column("row")
			            .where(eq(property("row", FefuRatingPackageStudentRow.ratingPackage()), value(packageId)))
			            .order(property("row", (String)input.getEntityOrder().getKey()), input.getEntityOrder().getDirection());

	            return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }
}
