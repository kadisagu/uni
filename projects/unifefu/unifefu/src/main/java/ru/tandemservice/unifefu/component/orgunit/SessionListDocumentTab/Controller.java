/*$Id$*/
package ru.tandemservice.unifefu.component.orgunit.SessionListDocumentTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisession.entity.document.SessionListDocument;
import ru.tandemservice.unisession.entity.document.SessionSimpleDocument;

/**
 * @author DMITRY KNYAZEV
 * @since 07.09.2015
 */
public class Controller extends ru.tandemservice.unisession.component.orgunit.SessionListDocumentTab.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        final ru.tandemservice.unisession.component.orgunit.SessionListDocumentTab.Model model = this.getModel(component);
        final DynamicListDataSource<SessionListDocument> dataSource = model.getDataSource();
        dataSource.addColumn(new SimpleColumn("Дата\n сдачи", SessionSimpleDocument.deadlineDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(true), 5);
    }
}
