/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionBlockDeveloperAddEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.FefuDeveloper;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getDeveloperId() == null)
        {
            FefuDeveloper developer = new FefuDeveloper();
            developer.setBlock(get(EppEduPlanVersionBlock.class, model.getBlockId()));
            model.setDeveloper(developer);

        } else
        {
            model.setDeveloper(get(FefuDeveloper.class, model.getDeveloperId()));
        }
    }
}