/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubController;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public class Controller extends AbstractListExtractPubController<FefuOrderContingentStuDPOListExtract, Model, IDAO>
{
}