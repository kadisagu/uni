/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.entrant.FefuEntrantContractAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unifefu.entity.FefuEntrantContract;

/**
 * @author Alexander Zhebko
 * @since 02.04.2013
 */
@Input( {
    @Bind(key = "directionId", binding = "directionId")
} )
public class Model
{
    private Long _directionId;
    private FefuEntrantContract _contract;

    public Long getDirectionId()
    {
        return _directionId;
    }

    public void setDirectionId(Long directionId)
    {
        _directionId = directionId;
    }

    public FefuEntrantContract getContract()
    {
        return _contract;
    }

    public void setContract(FefuEntrantContract contract)
    {
        _contract = contract;
    }
}