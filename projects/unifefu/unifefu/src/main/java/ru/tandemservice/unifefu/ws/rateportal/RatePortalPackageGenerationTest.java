/* $Id$ */
package ru.tandemservice.unifefu.ws.rateportal;

import org.apache.axis.MessageContext;
import org.apache.axis.encoding.SerializationContext;
import org.apache.axis.server.AxisServer;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;

import javax.xml.namespace.QName;
import java.io.StringWriter;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 17.02.2014
 */
public class RatePortalPackageGenerationTest
{
    private static final ObjectFactory FACTORY = new ObjectFactory();

    public static void main(String[] args)
    {
        StudentType student = FACTORY.createStudentType();

        student.setName("Волкова Ксения Михайловна");
        //student.setLotusId("2010001801");
        student.setPersonGuid("00000000-0000-0000-4444-000000000000");

        GroupsType groups = FACTORY.createGroupsType();
        groups.group = new ArrayList<>();
        student.setGroups(groups);

        GroupType group = FACTORY.createGroupType();
        group.setName("Б5314б(кит)");
        groups.group.add(group);

        CourseType course = FACTORY.createCourseType();
        course.task = new ArrayList<>();
        course.setName("Иностранный язык второй (китайский)");
        course.setTeacherName("Квашина Юлия Александровна");
        course.setCriteria("удовл >= 61%, хор >= 76%, отл. >= 86%");
        course.setRatingFinal("77.0%");
        course.setRatingCurrent("77.0%");
        course.setHasRequiredPoints("1");
        course.setHasRequiredPointsCurrent("1");
        course.setResult("хор.");
        course.setResultCurrent("хор.");
        //course.setUrl("blank");
        group.getCourse().add(course);

        TaskType task = FACTORY.createTaskType();
        task.mark = new ArrayList<>();
        task.setName("Домашняя работа (февраль)");
        task.setDue(DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        task.setWeight("7");
        task.setMaxPoints("15");
        task.setTypeName("Практическое задание");
        task.setRequiredPoints("9");
        task.setMarkDue(DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        course.task.add(task);

        MarkType mark = FACTORY.createMarkType();
        mark.setMdate("22.04.2013");
        mark.setValue("10");
        task.mark.add(mark);

        System.out.println(new String(NsiReactorUtils.toXml(student)));

        List<Status> statusList = new ArrayList<>();
        RecipientList recipientList = new RecipientList();
        recipientList.setRecipient(new Recipient[]{new Recipient("00000000-0000-0000-4444-000000000000", RecipientType.user)});

        Status status = new Status();
        status.setSystemcode("WR");
        status.setFunctioncode("StudentRate");
        status.setInputid(NsiReactorUtils.generateGUID(new Date().getTime()));
        status.setRecipients(recipientList);

        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        status.setActualdate(calendar);

        status.setContenttype("text/xml");
        status.setComment("Рейтинг Иванова Иван Иваныча");
        status.setStatus("Текущий рейтинг Иванова Иван Иваныча");
        status.setDetail(new String(NsiReactorUtils.toXml(student)));

        statusList.add(status);

        StatusList statusList1 = new StatusList();
        statusList1.setStatus(statusList.toArray(new Status[]{}));
        SubmitStatusRequest request = new SubmitStatusRequest();
        request.setStatuses(statusList1);

        SubmitStatusSOAP11SyncServiceLocator locator = new SubmitStatusSOAP11SyncServiceLocator();

        /*Attributes attrs = new AttributesImpl();
        MessageContext msgContext = new MessageContext(new AxisServer());
        QName qname = new QName(SubmitStatusRequest.getTypeDesc().getXmlType().getLocalPart());
        org.apache.axis.encoding.Serializer serializer = SubmitStatusRequest.getSerializer(null, SubmitStatusRequest.class, SubmitStatusRequest.getTypeDesc().getXmlType());

        try (StringWriter writer = new StringWriter())
        {
            serializer.serialize(qname, attrs, request, new SerializationContext(writer, msgContext));

            System.out.println(NsiReactorUtils.getXmlFormatted(new String(writer.toString()
                    .replaceAll(" xmlns[:=].*?\".*?\"", "")
                    .replaceAll(" xsi:type=\".*?\"", "")
                    .getBytes())));
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        System.out.println();*/

        try
        {
            SubmitStatusResponse response = locator.getSubmitStatusSOAP11SyncPort().submitStatus(request);
            CommitList commitList = response.getCommits();
            Commit[] resCommits = commitList.getCommit();
            for (Commit commit : resCommits)
            {
                System.out.println(commit.getRecipientId() + " - " + commit.getAnswer());
            }
        }
        catch (Exception e)
        {
            //http://dvfu-forms:9080/SubmitStatusComponent/SubmitStatus
            //Не судьба
            e.printStackTrace();
        }
    }
}