/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu21.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract;

/**
 * @author Igor Belanov
 * @since 30.06.2016
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuConditionalCourseTransferListExtract, Model>
{
}
