/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.logic;

import ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport;

import java.io.ByteArrayOutputStream;

/**
 * @author nvankov
 * @since 12/16/13
 */
public interface IFefuBrsPpsGradingSumDataReportDAO
{
    FefuBrsPpsGradingSumDataReport createReport(FefuBrsPpsGradingSumDataReportParams reportSettings);
}
