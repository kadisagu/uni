/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu19.Pub;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Ekaterina Zvereva
 * @since 18.11.2014
 */
public class DAO  extends ModularStudentExtractPubDAO<FefuTransfAcceleratedTimeStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

    }
}