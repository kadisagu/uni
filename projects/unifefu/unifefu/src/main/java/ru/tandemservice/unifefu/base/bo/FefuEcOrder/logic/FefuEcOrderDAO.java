/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.IEnrollmentExtractFactory;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderToBasicRel;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.FefuEcOrderManager;
import ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt;

import java.util.*;

/**
 * @author Nikolay Fedorovskih
 * @since 26.07.2013
 */
@Transactional
public class FefuEcOrderDAO extends CommonDAO implements IFefuEcOrderDAO
{
    @Override
    public void saveOrderAndParagraphs(EnrollmentOrder order, EnrollmentOrderFefuExt orderExt, Course enrollmentCourse,
                                       EmployeePost executor, EnrollmentOrderBasic basic, List<PreliminaryEnrollmentStudent> selectedPreStudentList)
    {
        // Проверка уникальности номера приказа
        EcOrderManager.instance().dao().checkOrderNumber(order);

        // Затираем ненужные параметры, если вдруг их вводили, а потом меняли тип приказа
        EnrollmentOrderType enrOrderType = EcOrderManager.instance().dao().getEnrollmentOrderType(order.getEnrollmentCampaign(), order.getType());
        if (!enrOrderType.isUseEnrollmentDate()) order.setEnrollmentDate(null);
        if (!enrOrderType.isBasic()) order.setOrderBasicText(null);
        if (!enrOrderType.isCommand()) order.setOrderText(null);
        if (!enrOrderType.isReasonAndBasic()) order.setReason(null);

        // исполнитель
        order.setExecutor(OrderExecutorSelectModel.getExecutor(executor));

        // сохраняем приказ
        save(order);

        // сохраняем расширение ДВФУ для приказа
        orderExt.setEnrollmentOrder(order);
        save(orderExt);

        // сохраняем связь основания приказа с приказом
        if (basic != null && order.getReason() != null)
        {
            EnrollmentOrderToBasicRel rel = new EnrollmentOrderToBasicRel();
            rel.setBasic(basic);
            rel.setOrder(order);
            save(rel);
        }

        getSession().flush();

        // создавать презентер без активации компонента не очень хорошо, наверное.. Но другого решения не нашел.
        IEnrollmentExtractFactory extractFactory;
        Class parAddEditPresenter = FefuEcOrderManager.getParAddEditPresenter(order.getType());
        try
        {
            extractFactory = (IEnrollmentExtractFactory) parAddEditPresenter.newInstance();
        }
        catch (IllegalAccessException | InstantiationException exception)
        {
            throw new RuntimeException(exception);
        }

        // Разбиваем абитуриентов по НПП
        Map<EducationOrgUnit, List<PreliminaryEnrollmentStudent>> map = new HashMap<>();
        for (PreliminaryEnrollmentStudent preStudent : selectedPreStudentList)
        {
            List<PreliminaryEnrollmentStudent> eduList = map.get(preStudent.getEducationOrgUnit());
            if (eduList == null)
            {
                eduList = new ArrayList<>();
                map.put(preStudent.getEducationOrgUnit(), eduList);
            }
            eduList.add(preStudent);
        }

        for (EducationOrgUnit educationOrgUnit : getSortedList(map.keySet()))
        {
            EcOrderManager.instance().dao().saveOrUpdateEnrollmentParagraph(
                    order,
                    null, // надо создать новый параграф
                    extractFactory,
                    false, // создаем параграф на НПП
                    map.get(educationOrgUnit),
                    null, // старосты нет
                    educationOrgUnit.getFormativeOrgUnit(),
                    educationOrgUnit.getEducationLevelHighSchool(),
                    educationOrgUnit.getDevelopForm(),
                    enrollmentCourse,
                    null // группа не указана
            );
        }
    }

    private List<EducationOrgUnit> getSortedList(Set<EducationOrgUnit> set)
    {
        List<EducationOrgUnit> result = new ArrayList<>(set);
        Collections.sort(result, new Comparator<EducationOrgUnit>()
        {
            @Override
            public int compare(EducationOrgUnit o1, EducationOrgUnit o2)
            {
                String title1 = o1.getEducationLevelHighSchool().getEducationLevel().getTitle();
                String title2 = o2.getEducationLevelHighSchool().getEducationLevel().getTitle();
                return title1.compareToIgnoreCase(title2);
            }
        });
        return result;
    }
}