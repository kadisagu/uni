package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «Об отчислении слушателей ДПО/ДО»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuExcludeStuDPOListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuExcludeStuDPOListExtract";
    public static final String ENTITY_NAME = "fefuExcludeStuDPOListExtract";
    public static final int VERSION_HASH = -1442175367;
    private static IEntityMeta ENTITY_META;

    public static final String P_EXCLUDE_DATE = "excludeDate";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";

    private Date _excludeDate;     // Дата отчисления
    private StudentStatus _studentStatusOld;     // Предыдущее состояние студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     */
    @NotNull
    public Date getExcludeDate()
    {
        return _excludeDate;
    }

    /**
     * @param excludeDate Дата отчисления. Свойство не может быть null.
     */
    public void setExcludeDate(Date excludeDate)
    {
        dirty(_excludeDate, excludeDate);
        _excludeDate = excludeDate;
    }

    /**
     * @return Предыдущее состояние студента.
     */
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Предыдущее состояние студента.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuExcludeStuDPOListExtractGen)
        {
            setExcludeDate(((FefuExcludeStuDPOListExtract)another).getExcludeDate());
            setStudentStatusOld(((FefuExcludeStuDPOListExtract)another).getStudentStatusOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuExcludeStuDPOListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuExcludeStuDPOListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuExcludeStuDPOListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return obj.getExcludeDate();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    obj.setExcludeDate((Date) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                        return true;
                case "studentStatusOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return true;
                case "studentStatusOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "excludeDate":
                    return Date.class;
                case "studentStatusOld":
                    return StudentStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuExcludeStuDPOListExtract> _dslPath = new Path<FefuExcludeStuDPOListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuExcludeStuDPOListExtract");
    }
            

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuExcludeStuDPOListExtract#getExcludeDate()
     */
    public static PropertyPath<Date> excludeDate()
    {
        return _dslPath.excludeDate();
    }

    /**
     * @return Предыдущее состояние студента.
     * @see ru.tandemservice.unifefu.entity.FefuExcludeStuDPOListExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    public static class Path<E extends FefuExcludeStuDPOListExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<Date> _excludeDate;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчисления. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuExcludeStuDPOListExtract#getExcludeDate()
     */
        public PropertyPath<Date> excludeDate()
        {
            if(_excludeDate == null )
                _excludeDate = new PropertyPath<Date>(FefuExcludeStuDPOListExtractGen.P_EXCLUDE_DATE, this);
            return _excludeDate;
        }

    /**
     * @return Предыдущее состояние студента.
     * @see ru.tandemservice.unifefu.entity.FefuExcludeStuDPOListExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

        public Class getEntityClass()
        {
            return FefuExcludeStuDPOListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuExcludeStuDPOListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
