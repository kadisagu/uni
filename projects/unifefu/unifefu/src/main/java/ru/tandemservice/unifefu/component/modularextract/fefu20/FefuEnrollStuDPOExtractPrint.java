/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu20;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOExtract;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;

/**
 * @author Ekaterina Zvereva
 * @since 20.01.2015
 */
public class FefuEnrollStuDPOExtractPrint implements IPrintFormCreator<FefuEnrollStuDPOExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuEnrollStuDPOExtract extract)
    {
        if (extract.isIndividual())
        {
            byte[] rel = UniDaoFacade.getCoreDao().getProperty(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.P_CONTENT, FefuOrderToPrintFormRelation.L_ORDER, extract.getParagraph().getOrder());
            if (rel != null)
                return new RtfReader().read(rel);
        }

        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("fefuDpoProgram", extract.getDpoProgramNew().getTitle());
        modifier.put("enrollmentDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEnrollDate()));

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}