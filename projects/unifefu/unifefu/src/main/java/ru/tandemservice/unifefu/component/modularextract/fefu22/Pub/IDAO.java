/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu22.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 22.01.2015
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuOrderContingentStuDPOExtract, Model>
{
}