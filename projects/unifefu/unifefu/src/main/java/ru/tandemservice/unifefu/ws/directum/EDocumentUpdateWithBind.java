/**
 * EDocumentUpdateWithBind.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class EDocumentUpdateWithBind  implements java.io.Serializable {
    private java.lang.String XMLPackage;

    private byte[][] documents;

    private java.lang.String objectType;

    private java.lang.String objectKey;

    private java.lang.String ISCode;

    public EDocumentUpdateWithBind() {
    }

    public EDocumentUpdateWithBind(
           java.lang.String XMLPackage,
           byte[][] documents,
           java.lang.String objectType,
           java.lang.String objectKey,
           java.lang.String ISCode) {
           this.XMLPackage = XMLPackage;
           this.documents = documents;
           this.objectType = objectType;
           this.objectKey = objectKey;
           this.ISCode = ISCode;
    }


    /**
     * Gets the XMLPackage value for this EDocumentUpdateWithBind.
     * 
     * @return XMLPackage
     */
    public java.lang.String getXMLPackage() {
        return XMLPackage;
    }


    /**
     * Sets the XMLPackage value for this EDocumentUpdateWithBind.
     * 
     * @param XMLPackage
     */
    public void setXMLPackage(java.lang.String XMLPackage) {
        this.XMLPackage = XMLPackage;
    }


    /**
     * Gets the documents value for this EDocumentUpdateWithBind.
     * 
     * @return documents
     */
    public byte[][] getDocuments() {
        return documents;
    }


    /**
     * Sets the documents value for this EDocumentUpdateWithBind.
     * 
     * @param documents
     */
    public void setDocuments(byte[][] documents) {
        this.documents = documents;
    }


    /**
     * Gets the objectType value for this EDocumentUpdateWithBind.
     * 
     * @return objectType
     */
    public java.lang.String getObjectType() {
        return objectType;
    }


    /**
     * Sets the objectType value for this EDocumentUpdateWithBind.
     * 
     * @param objectType
     */
    public void setObjectType(java.lang.String objectType) {
        this.objectType = objectType;
    }


    /**
     * Gets the objectKey value for this EDocumentUpdateWithBind.
     * 
     * @return objectKey
     */
    public java.lang.String getObjectKey() {
        return objectKey;
    }


    /**
     * Sets the objectKey value for this EDocumentUpdateWithBind.
     * 
     * @param objectKey
     */
    public void setObjectKey(java.lang.String objectKey) {
        this.objectKey = objectKey;
    }


    /**
     * Gets the ISCode value for this EDocumentUpdateWithBind.
     * 
     * @return ISCode
     */
    public java.lang.String getISCode() {
        return ISCode;
    }


    /**
     * Sets the ISCode value for this EDocumentUpdateWithBind.
     * 
     * @param ISCode
     */
    public void setISCode(java.lang.String ISCode) {
        this.ISCode = ISCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EDocumentUpdateWithBind)) return false;
        EDocumentUpdateWithBind other = (EDocumentUpdateWithBind) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.XMLPackage==null && other.getXMLPackage()==null) || 
             (this.XMLPackage!=null &&
              this.XMLPackage.equals(other.getXMLPackage()))) &&
            ((this.documents==null && other.getDocuments()==null) || 
             (this.documents!=null &&
              java.util.Arrays.equals(this.documents, other.getDocuments()))) &&
            ((this.objectType==null && other.getObjectType()==null) || 
             (this.objectType!=null &&
              this.objectType.equals(other.getObjectType()))) &&
            ((this.objectKey==null && other.getObjectKey()==null) || 
             (this.objectKey!=null &&
              this.objectKey.equals(other.getObjectKey()))) &&
            ((this.ISCode==null && other.getISCode()==null) || 
             (this.ISCode!=null &&
              this.ISCode.equals(other.getISCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getXMLPackage() != null) {
            _hashCode += getXMLPackage().hashCode();
        }
        if (getDocuments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDocuments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDocuments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getObjectType() != null) {
            _hashCode += getObjectType().hashCode();
        }
        if (getObjectKey() != null) {
            _hashCode += getObjectKey().hashCode();
        }
        if (getISCode() != null) {
            _hashCode += getISCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EDocumentUpdateWithBind.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">EDocumentUpdateWithBind"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("XMLPackage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "XMLPackage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documents");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "Documents"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "base64Binary"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("objectType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "ObjectType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("objectKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "ObjectKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "ISCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
