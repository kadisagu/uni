package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.unifefu.entity.gen.FefuTransferDevFormStuExtractGen;

import java.util.Date;

/**
 * О переводе на другую форму освоения
 */
public class FefuTransferDevFormStuExtract extends FefuTransferDevFormStuExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getTransferDate();
    }
}