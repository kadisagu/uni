/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.catalog.fefuCompetence.FefuCompetenceAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<FefuCompetence, Model>
{
}