/*$Id$*/
package ru.tandemservice.unifefu.component.group.GroupGlobalList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 08.10.2014
 */
public class Controller extends ru.tandemservice.uni.component.group.GroupGlobalList.Controller
{
	@Override
	protected DynamicListDataSource<Group> createNewDataSource(IBusinessComponent component)
	{
		DynamicListDataSource<Group> dataSource = super.createNewDataSource(component);
		List<AbstractColumn> columns = dataSource.getColumns();
		AbstractColumn column = dataSource.getColumn(Group.P_ACTIVE_STUDENT_COUNT );
        if (column != null)
            column.setOrderable(true);
		int startPos = column == null ? 11 : column.getNumber();

		columns.add(++startPos, new SimpleColumn("Академический отпуск", new String[]{Model.P_VACATION_STUDENT_COUNT}).setClickable(false).setOrderable(false));
		return dataSource;
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        initDefaultFilter((Model)getModel(component));
    }

    @Override
    public void onClickClear(IBusinessComponent component)
    {
        super.onClickClear(component);
        initDefaultFilter((Model) getModel(component));
    }

    public void initDefaultFilter(ru.tandemservice.uni.component.group.GroupGlobalList.Model model)
    {
        if (model.getSettings().get(Model.FEFU_EDU_LEVEL_TYPE_FILTER_NAME) == null)
        {
            List<StructureEducationLevels> levelsList = new ArrayList<>(2);
            levelsList.add(DataAccessServices.dao().get(StructureEducationLevels.class, StructureEducationLevels.P_CODE, StructureEducationLevelsCodes.HIGH_GOS3_GROUP));
            levelsList.add(DataAccessServices.dao().get(StructureEducationLevels.class, StructureEducationLevels.P_CODE, StructureEducationLevelsCodes.HIGH_GOS2_GROUP));
            model.getSettings().set(Model.FEFU_EDU_LEVEL_TYPE_FILTER_NAME, levelsList);
        }
    }
}
