/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu13.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;

/**
 * @author nvankov
 * @since 6/20/13
 */
public interface IDAO extends IModularStudentExtractPubDAO<SendPracticOutStuExtract, Model>
{
}
