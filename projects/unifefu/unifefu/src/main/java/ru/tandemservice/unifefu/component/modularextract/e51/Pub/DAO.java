/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e51.Pub;

import ru.tandemservice.movestudent.component.modularextract.e51.Pub.Model;

/**
 * @author Ekaterina Zvereva
 * @since 30.12.2014
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e51.Pub.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setChangesNothing(false);
    }
}
