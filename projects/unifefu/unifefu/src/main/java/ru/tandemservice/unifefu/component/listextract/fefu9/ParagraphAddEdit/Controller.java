/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu9.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class Controller extends AbstractListParagraphAddEditController<FefuFormativeTransferStuListExtract, IDAO, Model>
{
    @Override
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("status", "Состояние", Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(EDU_LVL_HS_COLUMN_TITLE, Student.educationOrgUnit().educationLevelHighSchool().configurableTitle().s()).setClickable(false));
        dataSource.setOrder("status", OrderDirection.asc);
    }
}
