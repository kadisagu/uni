/**
 *$Id$
 */
package ru.tandemservice.unifefu.tapestry.richTableList;

import org.tandemframework.core.view.list.column.SimpleColumn;

/**
 * @author Alexander Zhebko
 * @since 06.06.2013
 */
public final class FefuWeekTypeBlockColumn extends SimpleColumn
{
    private final int _columnIdx;      // порядковый номер колонки (счет с нуля)
    private final long _columnId;      // идентификатор колонки

    public FefuWeekTypeBlockColumn(long columnId, int columnIdx, String caption)
    {
        super(caption, null);
        this.setOrderable(false);
        this.setClickable(false);
        this._columnIdx = columnIdx;
        this._columnId = columnId;
    }

    public int getColumnIdx()
    {
        return this._columnIdx;
    }

    public Long getColumnId()
    {
        return this._columnId;
    }
}
