/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.context;

import ru.tandemservice.unifefu.ws.blackboard.BBConstants;
import ru.tandemservice.unifefu.ws.blackboard.BBContextHelper;
import ru.tandemservice.unifefu.ws.blackboard.HeaderHandlerResolver;
import ru.tandemservice.unifefu.ws.blackboard.PortTypeInitializer;
import ru.tandemservice.unifefu.ws.blackboard.context.gen.ContextWS;
import ru.tandemservice.unifefu.ws.blackboard.context.gen.ContextWSPortType;
import ru.tandemservice.unifefu.ws.blackboard.context.gen.ObjectFactory;

/**
 * @author Nikolay Fedorovskih
 * @since 16.03.2014
 */
public class BBContextInitializer extends PortTypeInitializer<ContextWSPortType, ObjectFactory>
{
    private ContextWS _contextWS;

    public BBContextInitializer(HeaderHandlerResolver headerHandlerResolver)
    {
        super(BBConstants.CONTEXT_WS, headerHandlerResolver);
        _contextWS = new ContextWS(BBContextHelper.getWSDL_resource_URL("Context.wsdl"));
        _contextWS.setHandlerResolver(headerHandlerResolver);
    }

    @Override
    protected ContextWSPortType initPortType()
    {
        return _contextWS.getContextWSSOAP11PortHttp();
    }

    @Override
    protected ObjectFactory initObjectFactory()
    {
        return new ObjectFactory();
    }
}