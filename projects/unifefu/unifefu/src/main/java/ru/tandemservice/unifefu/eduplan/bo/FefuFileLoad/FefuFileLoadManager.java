/*$Id$*/
package ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad.logic.FefuFileLoadDao;
import ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad.logic.IFefuFileLoadDao;

/**
 * @author DMITRY KNYAZEV
 * @since 15.01.2015
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class FefuFileLoadManager extends BusinessObjectManager
{
	public static FefuFileLoadManager instance()
	{
		return instance(FefuFileLoadManager.class);
	}

	@Bean
	public IFefuFileLoadDao dao()
	{
		return new FefuFileLoadDao();
	}
}
