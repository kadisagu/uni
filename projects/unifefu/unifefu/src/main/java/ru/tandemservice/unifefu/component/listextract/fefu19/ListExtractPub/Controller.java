/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubController;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 15.05.2015
 */
public class Controller extends AbstractListExtractPubController<FefuTransfSpecialityStuListExtract, Model, IDAO>
{
}
