/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsReport.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportMeta;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;

/**
 * @author nvankov
 * @since 12/3/13
 */
public class FefuBrsReportListUI extends UIPresenter
{
    public void onClickView()
    {
        IReportMeta meta = FefuBrsReportManager.instance().fefuBrsReportListExtPoint().<Long>getUniqueCodeMappingSource().getUniqueCode2ItemMap().get(_uiSupport.getListenerParameterAsLong());
        _uiActivation.asDesktopRoot(meta.getComponentClass()).activate();
    }
}
