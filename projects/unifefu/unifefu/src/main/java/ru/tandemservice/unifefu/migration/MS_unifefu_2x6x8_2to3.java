/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Alexey Lopatin
 * @since 22.10.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x8_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.8")
                };
    }

    @Override
    public ScriptDependency[] getBeforeDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("uniepp", "2.6.8", 1)
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();
        ResultSet src = stmt.executeQuery("select id, student_id from sessionattsheetslot_t");

        while (src.next())
        {
            Long id = src.getLong(1);
            Long studentId = src.getLong(2);

            int i = 0;
            if (studentId != 0)
            {
                Statement stmt2 = tool.getConnection().createStatement();
                stmt2.execute("select count (id) from epp_student_wpe_caction_t where id = " + studentId);
                ResultSet src2 = stmt2.getResultSet();

                while (src2.next())
                {
                    i = src2.getInt(1);
                }
            }

            if (i == 0)
            {
                tool.prepareStatement("update sessionattsheetslot_t set student_id = null where id = " + id).execute();
            }
        }
    }
}