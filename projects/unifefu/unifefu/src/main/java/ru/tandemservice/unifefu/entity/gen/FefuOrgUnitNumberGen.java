package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.entity.FefuOrgUnitNumber;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Код подразделения для использования в документах студентов (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuOrgUnitNumberGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuOrgUnitNumber";
    public static final String ENTITY_NAME = "fefuOrgUnitNumber";
    public static final int VERSION_HASH = -1290517044;
    private static IEntityMeta ENTITY_META;

    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String P_NUMBER = "number";

    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private OrgUnit _territorialOrgUnit;     // Территориальное подразделение
    private short _number;     // Номер (код) подразделения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение. Свойство не может быть null.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подразделение. Свойство не может быть null.
     */
    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Номер (код) подразделения. Свойство не может быть null.
     */
    @NotNull
    public short getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер (код) подразделения. Свойство не может быть null.
     */
    public void setNumber(short number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuOrgUnitNumberGen)
        {
            setFormativeOrgUnit(((FefuOrgUnitNumber)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((FefuOrgUnitNumber)another).getTerritorialOrgUnit());
            setNumber(((FefuOrgUnitNumber)another).getNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuOrgUnitNumberGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuOrgUnitNumber.class;
        }

        public T newInstance()
        {
            return (T) new FefuOrgUnitNumber();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((OrgUnit) value);
                    return;
                case "number":
                    obj.setNumber((Short) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "territorialOrgUnit":
                    return OrgUnit.class;
                case "number":
                    return Short.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuOrgUnitNumber> _dslPath = new Path<FefuOrgUnitNumber>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuOrgUnitNumber");
    }
            

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrgUnitNumber#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrgUnitNumber#getTerritorialOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Номер (код) подразделения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrgUnitNumber#getNumber()
     */
    public static PropertyPath<Short> number()
    {
        return _dslPath.number();
    }

    public static class Path<E extends FefuOrgUnitNumber> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private OrgUnit.Path<OrgUnit> _territorialOrgUnit;
        private PropertyPath<Short> _number;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrgUnitNumber#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrgUnitNumber#getTerritorialOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new OrgUnit.Path<OrgUnit>(L_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Номер (код) подразделения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrgUnitNumber#getNumber()
     */
        public PropertyPath<Short> number()
        {
            if(_number == null )
                _number = new PropertyPath<Short>(FefuOrgUnitNumberGen.P_NUMBER, this);
            return _number;
        }

        public Class getEntityClass()
        {
            return FefuOrgUnitNumber.class;
        }

        public String getEntityName()
        {
            return "fefuOrgUnitNumber";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
