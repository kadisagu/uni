/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.FefuTechnicalCommissionsDataManager;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.AddEdit.FefuTechnicalCommissionsAddEdit;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.AddEditEmployee.FefuTechnicalCommissionsAddEditEmployee;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.ui.AddEditEmployee.FefuTechnicalCommissionsAddEditEmployeeUI;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

/**
 * @author Nikolay Fedorovskih
 * @since 11.06.2013
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "technicalCommissionId", required = true)
})
public class FefuTechnicalCommissionsViewUI extends UIPresenter
{
    private Long technicalCommissionId;
    private FefuTechnicalCommission technicalCommission;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuTechnicalCommissionsView.EMPLOYEE_POST_DS.equals(dataSource.getName()))
            dataSource.put(FefuTechnicalCommissionsView.TECHNICAL_COMMISSION_PARAM, getTechnicalCommission());
    }

    @Override
    public void onComponentRefresh()
    {
        technicalCommission = DataAccessServices.dao().getNotNull(getTechnicalCommissionId());
    }

    public void onClickEdit()
    {
        _uiActivation.asDesktopRoot(FefuTechnicalCommissionsAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getTechnicalCommission().getId())
                .activate();
    }

    public void onClickAddEmployee()
    {
        _uiActivation.asDesktopRoot(FefuTechnicalCommissionsAddEditEmployee.class)
                .parameter(UIPresenter.PUBLISHER_ID, getTechnicalCommission().getId())
                .activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asDesktopRoot(FefuTechnicalCommissionsAddEditEmployee.class)
                .parameter(FefuTechnicalCommissionsAddEditEmployeeUI.TC_EMPLOYEE_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDelete()
    {
        FefuTechnicalCommissionsDataManager.instance().dao().delete(getTechnicalCommission());
        deactivate();
    }

    public void onDeleteEntityFromList()
    {
        FefuTechnicalCommissionsDataManager.instance().dao().delete(getListenerParameterAsLong());
    }

    public FefuTechnicalCommission getTechnicalCommission()
    {
        return technicalCommission;
    }

    public void setTechnicalCommissionId(Long technicalCommissionId)
    {
        this.technicalCommissionId = technicalCommissionId;
    }

    public Long getTechnicalCommissionId()
    {
        return technicalCommissionId;
    }
}