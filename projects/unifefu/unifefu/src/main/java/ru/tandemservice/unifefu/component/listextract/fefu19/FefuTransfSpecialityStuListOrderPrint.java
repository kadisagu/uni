/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.component.listextract.fefu19.utils.FefuTransfSpecialityStuParagraphPartWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu19.utils.FefuTransfSpecialityStuParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unifefu.entity.gen.StudentFefuExtGen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 15.05.2015
 */
public class FefuTransfSpecialityStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<FefuTransfSpecialityStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        List<FefuTransfSpecialityStuParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts);

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, preparedParagraphsStructure, order);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }


    protected List<FefuTransfSpecialityStuParagraphWrapper> prepareParagraphsStructure(List<FefuTransfSpecialityStuListExtract> extracts)
    {
        int ind;
        List<FefuTransfSpecialityStuParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        for (FefuTransfSpecialityStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = student.getPerson();


            EducationOrgUnit educationOrgUnitNew = extract.getEducationOrgUnitNew();
            EducationOrgUnit educationOrgUnitOld = extract.getEducationOrgUnitOld();

            EducationLevels parentLevel = EducationOrgUnitUtil.getParentLevel(educationOrgUnitNew.getEducationLevelHighSchool());
            Qualifications qualification = parentLevel.getQualification();

            if (null == qualification)
            {
                EducationLevels tmpLevel = parentLevel.getParentLevel();
                while (null != tmpLevel && null == qualification)
                {
                    qualification = tmpLevel.getQualification();
                    tmpLevel = tmpLevel.getParentLevel();
                }
            }


            StudentFefuExt studentExt = DataAccessServices.dao().getByNaturalId(new StudentFefuExtGen.NaturalId(student));
            Long eduLevel = studentExt != null? studentExt.getBaseEdu(): null;


            FefuTransfSpecialityStuParagraphWrapper paragraphWrapper = new FefuTransfSpecialityStuParagraphWrapper(
					student.getCourse(), extract.getGroupOld(), extract.getGroupNew(), student.getCompensationType(),
					educationOrgUnitOld.getEducationLevelHighSchool(), parentLevel, educationOrgUnitOld.getFormativeOrgUnit(), educationOrgUnitOld.getTerritorialOrgUnit(), educationOrgUnitOld.getDevelopForm(),
					educationOrgUnitOld.getDevelopCondition(), educationOrgUnitOld.getDevelopTech(), educationOrgUnitOld.getDevelopPeriod(), qualification, extract, eduLevel);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else paragraphWrapper = paragraphWrapperList.get(ind);

            FefuTransfSpecialityStuParagraphPartWrapper paragraphPartWrapper = new FefuTransfSpecialityStuParagraphPartWrapper(educationOrgUnitNew.getEducationLevelHighSchool(), qualification, extract);

            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);

            paragraphPartWrapper.getPersonList().add(person);
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<FefuTransfSpecialityStuParagraphWrapper> paragraphWrappers, StudentListOrder order)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (FefuTransfSpecialityStuParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_TRANSFER_SPECIALIZATION_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());
                CommonExtractPrint.injectReason(paragraphInjectModifier, order.getReason(), order.getReasonComment());
                CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());

                boolean isNeedDevCondition = !paragraphWrapper.getDevelopCondition().getCode().equals(DevelopConditionCodes.FULL_PERIOD);

                StringBuilder developCondition = new StringBuilder(" по ").append(CommonExtractPrint.getDevelopConditionStr_G(paragraphWrapper.getDevelopCondition()))
                        .append(" (").append(paragraphWrapper.getDevelopPeriod().getTitle()).append(") основной образовательной программе");
                paragraphInjectModifier.put("fefuDevelopCondition", isNeedDevCondition? developCondition.toString():"");

                paragraphInjectModifier.put("techRemote",
                             DevelopTechCodes.DISTANCE.equals(paragraphWrapper.getDevelopTech().getCode())? CommonExtractPrint.WITH_REMOTE_EDU_TECH : "");

                //базовое образование
                String eduLevel = (paragraphWrapper.getBaseEdu()) == null ? "" :(" на базе " + (paragraphWrapper.getBaseEdu().equals(FefuStudentManager.BASE_VPO) ? "высшего образования" : "среднего профессионального образования"));
                paragraphInjectModifier.put("fefuBaseEduLevel", isNeedDevCondition? eduLevel:"");

                parNumber++;
                paragraphInjectModifier.put("parNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                paragraphInjectModifier.put("paragraphNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");

                String code = paragraphWrapper.getDevelopForm().getCode();
                if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "очной");
                else if (DevelopFormCodes.CORESP_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "заочной");
                else if (DevelopFormCodes.PART_TIME_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "очно-заочной");
                else if (DevelopFormCodes.EXTERNAL_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "экстернату");
                else if (DevelopFormCodes.APPLICANT_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "самостоятельному обучению и итоговой аттестации");

                Student firstStudent = paragraphWrapper.getFirstExtract().getEntity();
                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, firstStudent.getCompensationType(), "", false);

                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit(), "formativeOrgUnitStr", "");

                paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());
                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "groupInternal_G", paragraphWrapper.getGroup(), paragraphWrapper.getDevelopForm(), " группы ");
                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "groupNew", paragraphWrapper.getGroupNew(), paragraphWrapper.getDevelopForm(), " в группу ");

                EducationLevels specialityOld = paragraphWrapper.getEducationLevelsOld();
                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, specialityOld, new String[]{"fefuEducationStrDirectionOld"}, true);

                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, paragraphWrapper.getParentEduLevelNew(), new String[]{"fefuEducationStrDirectionNew"}, false);

                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(paragraphWrapper.getGroup()), "fefuShortFastExtendedOptionalText");

                paragraphInjectModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<FefuTransfSpecialityStuParagraphPartWrapper> paragraphPartWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);

        // 2. Если нашли, то вместо него вставляем все подпараграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            for (FefuTransfSpecialityStuParagraphPartWrapper paragraphPartWrapper : paragraphPartWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_TRANSFER_SPECIALIZATION_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, paragraphPartWrapper.getFirstExtract());

                Qualifications qualification = paragraphPartWrapper.getQualification();
                String qualificationCode = null != qualification ? qualification.getCode() : null;
                if (null != qualificationCode && !FefuTransfSpecialityStuListExtractPrint.PROCESSED_QUALIFICATION_CODES.contains(qualificationCode))
                    qualificationCode = null;

                RtfString profileTitle = new RtfString();
                profileTitle.par();
                if (paragraphPartWrapper.getEducationLevel().getEducationLevel().isProfileOrSpecialization())
                     profileTitle.append(IRtfData.TAB).append(StringUtils.capitalize(FefuTransfSpecialityStuListExtractPrint.PROFILE_STRING_CASES_ARRAY.get(qualificationCode)[0])).append(" «")
                            .append(paragraphPartWrapper.getEducationLevel().getTitle()).append("»").par();

                paragraphPartInjectModifier.put("profileNew", profileTitle);

                List<Person> personList = paragraphPartWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int j = 0;

                for (; j < personList.size() - 1; j++)
                {
                    rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio()).par();
                }

                rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio());

                paragraphPartInjectModifier.put("STUDENT_LIST", rtfString);
                paragraphPartInjectModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}