/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu22.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuOrderContingentStuDPOExtract, Model>
{
}