/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.EnrollmentOrderStatisticsAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.ITitledEntity;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportParam;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportParam;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.FefuEcReportManager;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.EnrollmentOrderStatisticsReportBuilder;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.IFefuEcReportBuilder;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.IFefuEcReportDAO;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.EnrollmentOrderStatisticsPub.FefuEcReportEnrollmentOrderStatisticsPub;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 09.09.2013
 */
public class FefuEcReportEnrollmentOrderStatisticsAddUI extends UIPresenter
{
    private FefuEnrollmentOrderStatisticsReport _report = new FefuEnrollmentOrderStatisticsReport();
    private IReportParam<List<StudentCategory>> _studentCategory = new ReportParam<>();
    private IReportParam<List<Qualifications>> _qualification = new ReportParam<>();
    private IReportParam<CompensationType> _compensationType = new ReportParam<>();
    private IReportParam<List<OrgUnit>> _formativeOrgUnit = new ReportParam<>();
    private IReportParam<List<OrgUnit>> _territorialOrgUnit = new ReportParam<>();
    private IReportParam<List<EducationLevelsHighSchool>> _direction = new ReportParam<>();
    private IReportParam<List<DevelopForm>> _developForm = new ReportParam<>();
    private IReportParam<List<DevelopCondition>> _developCondition = new ReportParam<>();
    private IReportParam<List<DevelopTech>> _developTech = new ReportParam<>();
    private IReportParam<List<DevelopPeriod>> _developPeriod = new ReportParam<>();
    private ITitledEntity _includeForeignPerson = FefuEcReportEnrollmentOrderStatisticsAdd.BOOLEAN_NO;

    @Override
    public void onComponentRefresh()
    {
        EnrollmentCampaign ec = UnifefuDaoFacade.getFefuEntrantDAO().getLastEnrollmentCampaign();
        _report.setEnrollmentCampaign(ec);
        _report.setDateFrom(ec != null ? ec.getStartDate() : CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())));
        _report.setDateTo(new Date());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(FefuEcReportEnrollmentOrderStatisticsAdd.ENROLLMENT_CAMPAIGN_PARAM, _report.getEnrollmentCampaign());
        if (FefuEcReportEnrollmentOrderStatisticsAdd.DIRECTION_DS.equals(dataSource.getName()))
        {
            dataSource.put(FefuEcReportEnrollmentOrderStatisticsAdd.QUALIFICATION_LIST_PARAM, _qualification.getDataIfActive());
            dataSource.put(FefuEcReportEnrollmentOrderStatisticsAdd.FORMATIVE_ORGUNIT_LIST_PARAM, _formativeOrgUnit.getDataIfActive());
            dataSource.put(FefuEcReportEnrollmentOrderStatisticsAdd.TERRITORIAL_ORGUNIT_LIST_PARAM, _territorialOrgUnit.getDataIfActive());
        }
    }

    @SuppressWarnings("unchecked")
    private String getTitledListStr(IReportParam param, String titleProperty)
    {
        return param.isActive() ? CommonBaseStringUtil.join(((IReportParam<List<IEntity>>) param).getData(), titleProperty, "; ") : null;
    }

    private String getTitledListStr(IReportParam param)
    {
        return getTitledListStr(param, "title");
    }

    public void onClickApply()
    {
        final IFefuEcReportDAO dao = FefuEcReportManager.instance().dao();

        _report.setFormingDate(new Date());
        _report.setStudentCategoryStr(getTitledListStr(_studentCategory));
        _report.setQualificationStr(getTitledListStr(_qualification));
        _report.setFormativeOrgUnitStr(getTitledListStr(_formativeOrgUnit));
        _report.setTerritorialOrgUnitStr(getTitledListStr(_territorialOrgUnit));
        _report.setCompensationType(_compensationType.getDataIfActive());
        _report.setDirectionStr(getTitledListStr(_direction, EducationLevelsHighSchool.P_FULL_TITLE));
        _report.setDevelopFormStr(getTitledListStr(_developForm));
        _report.setDevelopConditionStr(getTitledListStr(_developCondition));
        _report.setDevelopTechStr(getTitledListStr(_developTech));
        _report.setDevelopPeriodStr(getTitledListStr(_developPeriod));
        _report.setIncludeForeignPerson(FefuEcReportEnrollmentOrderStatisticsAdd.BOOLEAN_YES.getId().equals(_includeForeignPerson.getId()));

        IFefuEcReportBuilder reportBuilder = new EnrollmentOrderStatisticsReportBuilder(
                _report,
                _studentCategory.getDataIfActive(),
                _qualification.getDataIfActive(),
                _formativeOrgUnit.getDataIfActive(),
                _territorialOrgUnit.getDataIfActive(),
                _direction.getDataIfActive(),
                _developForm.getDataIfActive(),
                _developCondition.getDataIfActive(),
                _developTech.getDataIfActive(),
                _developPeriod.getDataIfActive()
        );
        dao.createReport(reportBuilder, _report);

        deactivate();

        _uiActivation.asDesktopRoot(FefuEcReportEnrollmentOrderStatisticsPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, _report.getId())
                .activate();
    }

    public FefuEnrollmentOrderStatisticsReport getReport()
    {
        return _report;
    }

    public IReportParam<List<StudentCategory>> getStudentCategory()
    {
        return _studentCategory;
    }

    public IReportParam<List<Qualifications>> getQualification()
    {
        return _qualification;
    }

    public IReportParam<CompensationType> getCompensationType()
    {
        return _compensationType;
    }

    public IReportParam<List<OrgUnit>> getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public IReportParam<List<OrgUnit>> getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public IReportParam<List<EducationLevelsHighSchool>> getDirection()
    {
        return _direction;
    }

    public IReportParam<List<DevelopForm>> getDevelopForm()
    {
        return _developForm;
    }

    public IReportParam<List<DevelopCondition>> getDevelopCondition()
    {
        return _developCondition;
    }

    public IReportParam<List<DevelopTech>> getDevelopTech()
    {
        return _developTech;
    }

    public IReportParam<List<DevelopPeriod>> getDevelopPeriod()
    {
        return _developPeriod;
    }

    public ITitledEntity getIncludeForeignPerson()
    {
        return _includeForeignPerson;
    }

    public void setIncludeForeignPerson(ITitledEntity includeForeignPerson)
    {
        _includeForeignPerson = includeForeignPerson;
    }
}