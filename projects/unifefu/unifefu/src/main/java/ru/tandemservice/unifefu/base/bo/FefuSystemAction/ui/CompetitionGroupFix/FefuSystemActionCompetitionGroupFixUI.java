/* $Id: FefuSystemActionCompetitionGroupFixUI.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.CompetitionGroupFix;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 14.05.13
 * Time: 11:27
 */
public class FefuSystemActionCompetitionGroupFixUI extends UIPresenter
{
    private EnrollmentCampaign _enrollmentCampaign;

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public void onClickApply()
    {
        FefuSystemActionManager.instance().dao().fixCompetitionGroups(_enrollmentCampaign);
        deactivate();
    }
}
