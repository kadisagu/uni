/**
 * ResourceList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportal;

public class ResourceList  implements java.io.Serializable {
    private ru.tandemservice.unifefu.ws.rateportal.Resource[] resource;

    public ResourceList() {
    }

    public ResourceList(
           ru.tandemservice.unifefu.ws.rateportal.Resource[] resource) {
           this.resource = resource;
    }


    /**
     * Gets the resource value for this ResourceList.
     * 
     * @return resource
     */
    public ru.tandemservice.unifefu.ws.rateportal.Resource[] getResource() {
        return resource;
    }


    /**
     * Sets the resource value for this ResourceList.
     * 
     * @param resource
     */
    public void setResource(ru.tandemservice.unifefu.ws.rateportal.Resource[] resource) {
        this.resource = resource;
    }

    public ru.tandemservice.unifefu.ws.rateportal.Resource getResource(int i) {
        return this.resource[i];
    }

    public void setResource(int i, ru.tandemservice.unifefu.ws.rateportal.Resource _value) {
        this.resource[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResourceList)) return false;
        ResourceList other = (ResourceList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.resource==null && other.getResource()==null) || 
             (this.resource!=null &&
              java.util.Arrays.equals(this.resource, other.getResource())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResource() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResource());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResource(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResourceList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "ResourceList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resource");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resource"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resource"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
