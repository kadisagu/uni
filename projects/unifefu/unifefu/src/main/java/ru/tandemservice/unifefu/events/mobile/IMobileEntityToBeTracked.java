/* $Id$ */
package ru.tandemservice.unifefu.events.mobile;

import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.*;

/**
 * @author Dmitry Seleznev
 * @since 26.08.2014
 */
public interface IMobileEntityToBeTracked
{
    public static final Class[] ENTITY_TYPES_TO_BE_TRACKED = new Class[]
            {
                    Principal.class,
                    Person.class,
                    IdentityCard.class,
                    Student.class,
                    EppRegistryElement.class,
                    EppRegistryElementPart.class,
                    EppRegistryElementPartModule.class,
                    EppRegistryElementPartFControlAction.class,
                    EppRegistryElementLoad.class,
                    EppRegistryModuleALoad.class,
                    EppStudentWorkPlanElement.class,
                    EppStudentWpeALoad.class,
                    EppStudentWpeCAction.class,
                    SppSchedule.class,
                    SppScheduleWeekRow.class,
                    SppSchedulePeriod.class,
                    SppSchedulePeriodFix.class,
                    SppScheduleSeason.class,
                    TrEduGroupEvent.class,
                    TrEduGroupEventStudent.class,
                    TrJournal.class,
                    TrJournalModule.class,
                    TrJournalEvent.class,
                    TrJournalGroup.class,
                    TrJournalGroupStudent.class,
                    SessionSlotLinkMark.class
            };
}