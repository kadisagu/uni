package ru.tandemservice.unifefu.entity;

import com.google.common.collect.ComparisonChain;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;
import ru.tandemservice.unifefu.entity.gen.*;

import java.util.Comparator;

/**
 * @see ru.tandemservice.unifefu.entity.gen.FefuAbstractCompetence2EpvRegistryRowRelGen
 */
public abstract class FefuAbstractCompetence2EpvRegistryRowRel extends FefuAbstractCompetence2EpvRegistryRowRelGen
{

    /**
     * @return Компетенция
     */
    public abstract FefuCompetence getFefuCompetence();

    public abstract String getGroupCode();

    /**
     * @return Сокращенное название группы компетенций
     */
    public abstract String getGroupShortTitle();

    /**
     * @return номер в группе компетенций
     */
    public abstract int getGroupNumber();

    /**
     * Компаратор по группе компетенций и по номеру в группе
     */
    public static final Comparator<FefuAbstractCompetence2EpvRegistryRowRel> COMPARATOR_BY_CODE =
            (o1, o2) -> ComparisonChain.start()
                    .compare(o1.getGroupShortTitle(), o2.getGroupShortTitle())
                    .compare(o1.getGroupNumber(), o2.getGroupNumber())
                    .result();
}