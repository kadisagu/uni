/* $Id$ */
package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAssignPaymentExtract;
import ru.tandemservice.movestudent.component.commons.gradation.IGroupManagerPaymentExtract;
import ru.tandemservice.unifefu.entity.gen.FefuAcadGrantAssignStuEnrolmentListExtractGen;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Проект приказа «О назначении академической стипендии (вступительные испытания)»
 */
public class FefuAcadGrantAssignStuEnrolmentListExtract extends FefuAcadGrantAssignStuEnrolmentListExtractGen implements IAssignPaymentExtract, IGroupManagerPaymentExtract
{
    // В полях с суммой хранится значение в сотых долях копейки
    private final static int CURRENCY_ABBREVIATION_DIGITS = 4;
    private final static long CURRENCY_ABBREVIATION_PART = (long) Math.pow(10D, CURRENCY_ABBREVIATION_DIGITS);


    public Long getGrantSizeInRuble()
    {
        return getGrantSize() / CURRENCY_ABBREVIATION_PART;
    }

    public Long getGroupManagerBonusSizeInRuble()
    {
        return getGroupManagerBonusSize() != null ? getGroupManagerBonusSize() / CURRENCY_ABBREVIATION_PART : null;
    }

    public void setGrantSizeInRuble(Long grandSizeInRuble)
    {
        setGrantSize(grandSizeInRuble * CURRENCY_ABBREVIATION_PART);
    }

    public void setGroupManagerBonusSizeInRuble(Long bonusSizeInRuble)
    {
        setGroupManagerBonusSize(bonusSizeInRuble == null ? null : bonusSizeInRuble * CURRENCY_ABBREVIATION_PART);
    }

    @Override
    public Date getGroupManagerBonusBeginDate()
    {
        return getBeginDate();
    }

    @Override
    public Date getGroupManagerBonusEndDate()
    {
        return getEndDate();
    }

    @Override
    public boolean hasGroupManagerBonus()
    {
        return getGroupManagerBonusSize() != null;
    }

    @Override
    public BigDecimal getPaymentAmount()
    {
        return new BigDecimal(getGrantSize()).movePointLeft(CURRENCY_ABBREVIATION_DIGITS);
    }

    @Override
    public BigDecimal getGroupManagerPaymentAmount()
    {
        return getGroupManagerBonusSize() != null ? new BigDecimal(getGroupManagerBonusSize()).movePointLeft(CURRENCY_ABBREVIATION_DIGITS) : null;
    }

    @Override
    public Date getPaymentBeginDate()
    {
        return getBeginDate();
    }

    @Override
    public Date getPaymentEndDate()
    {
        return getEndDate();
    }
}