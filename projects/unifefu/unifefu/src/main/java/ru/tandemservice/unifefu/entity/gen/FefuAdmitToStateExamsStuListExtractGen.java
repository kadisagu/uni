package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О допуске к сдаче государственных экзаменов»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuAdmitToStateExamsStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract";
    public static final String ENTITY_NAME = "fefuAdmitToStateExamsStuListExtract";
    public static final int VERSION_HASH = -271784329;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_NOT_NEED_ADMISSION_TO_G_I_A = "notNeedAdmissionToGIA";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private boolean _notNeedAdmissionToGIA = false;     // Без допуска к ГИА

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotNeedAdmissionToGIA()
    {
        return _notNeedAdmissionToGIA;
    }

    /**
     * @param notNeedAdmissionToGIA Без допуска к ГИА. Свойство не может быть null.
     */
    public void setNotNeedAdmissionToGIA(boolean notNeedAdmissionToGIA)
    {
        dirty(_notNeedAdmissionToGIA, notNeedAdmissionToGIA);
        _notNeedAdmissionToGIA = notNeedAdmissionToGIA;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuAdmitToStateExamsStuListExtractGen)
        {
            setCourse(((FefuAdmitToStateExamsStuListExtract)another).getCourse());
            setGroup(((FefuAdmitToStateExamsStuListExtract)another).getGroup());
            setCompensationType(((FefuAdmitToStateExamsStuListExtract)another).getCompensationType());
            setNotNeedAdmissionToGIA(((FefuAdmitToStateExamsStuListExtract)another).isNotNeedAdmissionToGIA());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuAdmitToStateExamsStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuAdmitToStateExamsStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuAdmitToStateExamsStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "notNeedAdmissionToGIA":
                    return obj.isNotNeedAdmissionToGIA();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "notNeedAdmissionToGIA":
                    obj.setNotNeedAdmissionToGIA((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "notNeedAdmissionToGIA":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "notNeedAdmissionToGIA":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "notNeedAdmissionToGIA":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuAdmitToStateExamsStuListExtract> _dslPath = new Path<FefuAdmitToStateExamsStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuAdmitToStateExamsStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract#isNotNeedAdmissionToGIA()
     */
    public static PropertyPath<Boolean> notNeedAdmissionToGIA()
    {
        return _dslPath.notNeedAdmissionToGIA();
    }

    public static class Path<E extends FefuAdmitToStateExamsStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Boolean> _notNeedAdmissionToGIA;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract#isNotNeedAdmissionToGIA()
     */
        public PropertyPath<Boolean> notNeedAdmissionToGIA()
        {
            if(_notNeedAdmissionToGIA == null )
                _notNeedAdmissionToGIA = new PropertyPath<Boolean>(FefuAdmitToStateExamsStuListExtractGen.P_NOT_NEED_ADMISSION_TO_G_I_A, this);
            return _notNeedAdmissionToGIA;
        }

        public Class getEntityClass()
        {
            return FefuAdmitToStateExamsStuListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuAdmitToStateExamsStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
