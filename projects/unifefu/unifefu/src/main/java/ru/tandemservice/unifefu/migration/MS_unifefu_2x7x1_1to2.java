package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuBrsSumDataReport

		// изменен тип свойства formativeOrgUnit
		{
			// изменить тип колонки
			tool.changeColumnType("fefubrssumdatareport_t", "formativeorgunit_p", DBType.createVarchar(1000));

		}

		// изменен тип свойства eduLevel
		{
			// изменить тип колонки
			tool.changeColumnType("fefubrssumdatareport_t", "edulevel_p", DBType.createVarchar(3000));

		}


    }
}