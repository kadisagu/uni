package ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unifefu.brs.base.BaseFefuBrsReportParams;

import java.util.List;

/**
 * @author amakarova
 */
public class FefuListTrJournalDisciplinesReportParams extends BaseFefuBrsReportParams
{
    private OrgUnit _formativeOrgUnit;
    private List<PpsEntry> _ppsList;
    private List<Group> _groupList;
    private DataWrapper _onlyFilledJournals;
    private DataWrapper _fca;

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public List<PpsEntry> getPpsList()
    {
        return _ppsList;
    }

    public void setPpsList(List<PpsEntry> ppsList)
    {
        _ppsList = ppsList;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public DataWrapper getOnlyFilledJournals()
    {
        return _onlyFilledJournals;
    }

    public void setOnlyFilledJournals(DataWrapper onlyFilledJournals)
    {
        _onlyFilledJournals = onlyFilledJournals;
    }

    public DataWrapper getFca()
    {
        return _fca;
    }

    public void setFca(DataWrapper fca)
    {
        _fca = fca;
    }
}
