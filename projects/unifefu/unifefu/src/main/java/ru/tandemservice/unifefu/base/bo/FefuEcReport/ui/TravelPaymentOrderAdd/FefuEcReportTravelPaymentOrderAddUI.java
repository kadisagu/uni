/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.TravelPaymentOrderAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.FefuEcReportManager;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 04.09.2013
 */
public class FefuEcReportTravelPaymentOrderAddUI extends UIPresenter
{
    private EnrollmentCampaign _enrollmentCampaign;
    private List<EntrantEnrollmentOrderType> _entrantEnrollmentOrderTypeList;
    private List<EnrollmentOrder> _enrollmentOrderList;
    private Double _minSumMark4AdditionalAcademGrant3 = 230.0;
    private Double _minSumMark4AdditionalAcademGrant4 = 320.0;
    private Double _minMark = 70.0;
    private boolean _includeForeignPerson;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(UnifefuDaoFacade.getFefuEntrantDAO().getLastEnrollmentCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(FefuEcReportTravelPaymentOrderAdd.ENROLLMENT_CAMPAIGN_PARAM, _enrollmentCampaign);
        dataSource.put(FefuEcReportTravelPaymentOrderAdd.ENTRANT_ENROLLMENT_ORDER_TYPE_PARAM, _entrantEnrollmentOrderTypeList);
    }

    public void onClickApply()
    {
        byte[] report = FefuEcReportManager.instance().fefuTravelPaymentOrderReportDAO()
                .buildReport(_enrollmentCampaign, _entrantEnrollmentOrderTypeList, _enrollmentOrderList,
                             _minSumMark4AdditionalAcademGrant3, _minSumMark4AdditionalAcademGrant4, _minMark, _includeForeignPerson);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("TravelPaymentOrder.rtf").document(report), true);
        deactivate();
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public List<EnrollmentOrder> getEnrollmentOrderList()
    {
        return _enrollmentOrderList;
    }

    public void setEnrollmentOrderList(List<EnrollmentOrder> enrollmentOrderList)
    {
        _enrollmentOrderList = enrollmentOrderList;
    }

    public List<EntrantEnrollmentOrderType> getEntrantEnrollmentOrderTypeList()
    {
        return _entrantEnrollmentOrderTypeList;
    }

    public void setEntrantEnrollmentOrderTypeList(List<EntrantEnrollmentOrderType> entrantEnrollmentOrderTypeList)
    {
        _entrantEnrollmentOrderTypeList = entrantEnrollmentOrderTypeList;
    }

    public boolean isIncludeForeignPerson()
    {
        return _includeForeignPerson;
    }

    public void setIncludeForeignPerson(boolean includeForeignPerson)
    {
        _includeForeignPerson = includeForeignPerson;
    }

    public Double getMinMark()
    {
        return _minMark;
    }

    public void setMinMark(Double minMark)
    {
        _minMark = minMark;
    }

    public Double getMinSumMark4AdditionalAcademGrant3()
    {
        return _minSumMark4AdditionalAcademGrant3;
    }

    public void setMinSumMark4AdditionalAcademGrant3(Double minSumMark4AdditionalAcademGrant3)
    {
        _minSumMark4AdditionalAcademGrant3 = minSumMark4AdditionalAcademGrant3;
    }

    public Double getMinSumMark4AdditionalAcademGrant4()
    {
        return _minSumMark4AdditionalAcademGrant4;
    }

    public void setMinSumMark4AdditionalAcademGrant4(Double minSumMark4AdditionalAcademGrant4)
    {
        _minSumMark4AdditionalAcademGrant4 = minSumMark4AdditionalAcademGrant4;
    }
}