/* $Id$ */
package ru.tandemservice.unifefu.ws.foreignOnlineEntrants;

import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.FefuForeignOnlineEntrantManager;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.dao.daemon.IFEFUPasswordRegistratorDaemonDao;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;
import ru.tandemservice.unifefu.ws.FefuOnlineEntrantData;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author nvankov
 * @since 4/1/13
 */

@WebService(serviceName="fefuForeignEntrantsService")
public class FefuForeignOnlineEntrantsService
{
    @XmlType(name="foreignEntrant")
    public static class ForeignOnlineEntrant
    {
        @XmlAttribute public Long userId; // Регистрационный номер
        // Раздел 1. Персональные данные

        // Фамилия, имя, отчество на русском языке / Family Name, Given Name In Russian
        @XmlAttribute public String firstNameRu;
        @XmlAttribute public String middleNameRu;
        @XmlAttribute public String lastNameRu;
        // 2 Фамилия, имя на английском языке / Family Name, Given Name In English
        @XmlAttribute public String firstNameEn;
        @XmlAttribute public String lastNameEn;
        @XmlAttribute public String fioNative; // 3 Фамилия, имя на родном языке / Family Name, Given Name In Native Language
        @XmlAttribute public String country; // 4. Страна / Country of current residence

        @XmlAttribute public Date birthDate; // 5. Дата рождения (день, месяц, год) / Date of Birth (day, month, year)
        @XmlAttribute public String birthPlace; // 6. Место рождения (город, провинция) / Place of Birth (city, province)
        @XmlAttribute public String maritalStatus; // 7. Семейное положение / Marital Status

        // Раздел 2. Контактные данные

        @XmlAttribute public String addressNative; // 8. Домашний адрес на родине (на родном языке) / Permanent Home Address, Telephone (in Native Language)
        @XmlAttribute public String addressEn; // 9. Домашний адрес на родине (на английском языке) / Permanent Home Address, Telephone (in English)
        @XmlAttribute public String phoneHome; // 10. "Телефон на родине" / "Phone at home"
        @XmlAttribute public String addressVlad; // 11. Адрес во Владивостоке / Address in Vladivostok
        @XmlAttribute public String phoneVlad; // 12. Телефон во Владивостоке / Telephone in Vladivostok
        @XmlAttribute public String email; // 13. E-mail / E-mail

        // Раздел 3. Образование

        @XmlAttribute public String schoolWithLocation; // 14. Образовательное учреждение / School with location
        @XmlAttribute public String enteringDateStr; // 15. Год поступления / Entering date
        @XmlAttribute public String graduationDateStr; // 16. Год окончания / Graduation date
        @XmlAttribute public String foreignLang; // 17. Какими иностранными языками владеете / What foreign languages do you know?

        // Раздел 4. Последнее место работы

        @XmlAttribute public String workPlace; // 18. Место работы / Job
        @XmlAttribute public String occupation; // 19. Cпециальность / Occupation
        // сроки работы / working period
        @XmlAttribute public Date workDateFrom; // 20. c / from
        @XmlAttribute public Date workDateTo; // 21. по / to

        // Раздел 5. Контактное лицо
        @XmlAttribute public String contactPerson; // 22. Контактное лицо, телефон, e-mail / Emergency Person, Telephone, E-mail

        // Раздел 6. Источник информации о вузе и общежитие
        @XmlAttribute public String fefuInfo; // 23. Как Вы узнали о ДВФУ? / How did you learn about FEFU?
        @XmlAttribute public String needDormitory; // 24. Нуждаетесь ли Вы в общежитии? / Do you need a room in the dormitory?

        // Раздел 7.
        @XmlAttribute public String eduProgrammAndSpec; //25. Программа обучения в ДВФУ (бакалавриат, магистратура, аспирантура, стажировка, прочее) с указанием специальности (направления) или кафедры / FEFU Educational Program (Bachelor’s, Master’s, Post-graduate, Internship, other) and specialty that you would like to study
    }

    @WebMethod
    public void createOrUpdateForeignOnlineEntrant(@WebParam(name="foreignOnlineEntrant")ForeignOnlineEntrant foreignEntrant)
    {
        FefuForeignOnlineEntrantManager.instance().dao().webServiceCreateOrUpdate(foreignEntrant);
    }

    @WebMethod
    public boolean isExistForeignOnlineEntrant(@WebParam(name="userId")long userId)
    {
        FefuForeignOnlineEntrant foreignOnlineEntrant = getFefuForeignOnlineEntrant(userId);

        return null != foreignOnlineEntrant;
    }

    @WebMethod
    public boolean isCanDeleteForeignOnlineEntrant(@WebParam(name="userId")long userId)
    {
        return FefuForeignOnlineEntrantManager.instance().dao().canDeleteEntrant(userId);
    }

    @WebMethod
    public ForeignOnlineEntrant getForeignOnlineEntrant(@WebParam(name="userId")long userId)
    {
        return FefuForeignOnlineEntrantManager.instance().dao().webServiceGetForeignOnlineEntrant(userId);
    }

    @WebMethod
    public Integer getOnlineEntrantPersonalNumber(@WebParam(name="userId")long userId)
    {
        FefuForeignOnlineEntrant foreignOnlineEntrant = getFefuForeignOnlineEntrant(userId);
        return null != foreignOnlineEntrant ? foreignOnlineEntrant.getPersonalNumber() : null;
    }

    private FefuForeignOnlineEntrant getFefuForeignOnlineEntrant(@WebParam(name="userId")long userId)
    {
        return FefuForeignOnlineEntrantManager.instance().dao().getFefuForeignOnlineEntrant(userId);
    }

    @WebMethod
    public void sendOnlineEntrantPassword(@WebParam(name="userId")long userId, @WebParam(name="passwordEncrypted")String passwordEncrypted)
    {
        IFEFUPasswordRegistratorDaemonDao.instance.get().updateOnlineEntrantExtPassword(userId, passwordEncrypted);
    }

    @WebMethod
    public void createOrUpdateOnlineEntrant(long userId, FefuOnlineEntrantData entrantData)
    {
        UnifefuDaoFacade.getOnlineEntrantDAO().createOrUpdateFefuOnlineEntrant(userId, entrantData);
    }

    @WebMethod
    public FefuOnlineEntrantData getEntrantData(long userId)
    {
        return UnifefuDaoFacade.getOnlineEntrantDAO().getFefuOnlineEntrantData(userId);
    }

    @WebMethod
    public boolean deleteForeignOnlineEntrant(@WebParam(name="userId")long userId)
    {
        return FefuForeignOnlineEntrantManager.instance().dao().deleteForeignOnlineEntrantWS(userId);
    }

    @WebMethod
    public boolean isForeignOnlineEntrantAccepted(@WebParam(name="userId")long userId)
    {
        return FefuForeignOnlineEntrantManager.instance().dao().isForeignOnlineEntrantAccepted(userId);
    }

    @WebMethod
    public String getForeignDocumentCopiesName(@WebParam(name="userId")long userId)
    {
        return FefuForeignOnlineEntrantManager.instance().dao().getForeignDocumentCopiesName(userId);
    }

    @WebMethod
    public byte[] getForeignDocumentCopies(@WebParam(name="userId")long userId)
    {
        return FefuForeignOnlineEntrantManager.instance().dao().getForeignDocumentCopies(userId);
    }

    @WebMethod
    public void deleteForeignDocumentCopies(@WebParam(name="userId")long userId)
    {
        FefuForeignOnlineEntrantManager.instance().dao().deleteForeignDocumentCopies(userId);
    }

    @WebMethod
    public void saveForeignDocumentCopies(@WebParam(name="userId")long userId, @WebParam(name="zipArchive")byte[] zipArchive, @WebParam(name="documentCopiesName")String documentCopiesName)
    {
        FefuForeignOnlineEntrantManager.instance().dao().saveForeignDocumentCopies(userId, zipArchive, documentCopiesName);
    }

}
