/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author Alexey Lopatin
 * @since 20.12.2014
 */
public interface IFefuEduPlanPrintDAO extends INeedPersistenceSupport
{
    /**
     * Формирует печатную форму для версии УП
     *
     * @param block Блок УП(в)
     * @return xls
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    byte[] formingEduPlanVersionReport(EppEduPlanVersionBlock block);
}
