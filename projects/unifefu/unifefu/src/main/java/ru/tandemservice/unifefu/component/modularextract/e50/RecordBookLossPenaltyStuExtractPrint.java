/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e50;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.entity.RecordBookLossPenaltyStuExtract;

/**
 * @author Ekaterina Zvereva
 * @since 30.12.2014
 */
public class RecordBookLossPenaltyStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e50.RecordBookLossPenaltyStuExtractPrint
{
    private static String WITH_REPRIMAND_TEXT = " объявить выговор за утерю зачетной книжки.\n Дирекции ";

    @Override
    public void additionalModify(RtfInjectModifier modifier, RecordBookLossPenaltyStuExtract extract)
    {
        modifier.put("fefuReprimand", extract.isWithoutReprimand()? "": WITH_REPRIMAND_TEXT);
        if (extract.isWithoutReprimand())
            modifier.put("formativeOrgUnitStrWithTerritorial_G", "");
    }
}
