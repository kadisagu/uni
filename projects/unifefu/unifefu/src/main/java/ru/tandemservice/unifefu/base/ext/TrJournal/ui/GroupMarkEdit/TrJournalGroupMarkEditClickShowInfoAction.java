/* $Id: TrJournalGroupMarkEditClickShowInfoAction.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.ext.TrJournal.ui.GroupMarkEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import ru.tandemservice.unifefu.base.bo.FefuTrJournal.ui.MarkInfo.FefuTrJournalMarkInfo;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditUI;

public class TrJournalGroupMarkEditClickShowInfoAction extends NamedUIAction
{
    protected TrJournalGroupMarkEditClickShowInfoAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        TrJournalGroupMarkEditUI prsnt = (TrJournalGroupMarkEditUI) presenter;
        prsnt.getActivationBuilder().asRegion(FefuTrJournalMarkInfo.class, "dialog")
                .parameter(IUIPresenter.PUBLISHER_ID, prsnt.getListenerParameterAsLong())
                .activate();
        }
}
