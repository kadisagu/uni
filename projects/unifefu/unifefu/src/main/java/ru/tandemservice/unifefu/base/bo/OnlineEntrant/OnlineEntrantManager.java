/* $Id$ */
package ru.tandemservice.unifefu.base.bo.OnlineEntrant;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Ekaterina Zvereva
 * @since 01.06.2015
 */
@Configuration
public class OnlineEntrantManager extends BusinessObjectManager
{

    public static OnlineEntrantManager instance()
    {
        return instance(OnlineEntrantManager.class);
    }
}