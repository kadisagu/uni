/* $Id$ */

package ru.tandemservice.unifefu.component.listextract.fefu12.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public class Model extends AbstractListExtractPubModel<FefuStuffCompensationStuListExtract>
{
	private String compensationSum;
	private String immediateSum;

	public String getCompensationSum()
	{
		return compensationSum;
	}

	public void setCompensationSum(String compensationSum)
	{
		this.compensationSum = compensationSum;
	}

	public String getImmediateSum()
	{
		return immediateSum;
	}

	public void setImmediateSum(String immediateSum)
	{
		this.immediateSum = immediateSum;
	}
}
