/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import ru.tandemservice.unifefu.ws.nsi.datagram.DegreeType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import javax.xml.namespace.QName;

/**
 * @author Dmitry Seleznev
 * @since 08.05.2014
 */
public class NsiLocalAccessServiceDegreeTest extends NsiLocalAccessBaseTest
{
    public static final String[][] RETRIEVE_WHOLE_PARAMS = new String[][]{{null}};
    public static final String[][] RETRIEVE_NON_EXISTS_PARAMS = new String[][]{{"aaaaaaaa-6a03-3221-90ad-83af3fcd7b64"}};
    public static final String[][] RETRIEVE_SINGLE_PARAMS = new String[][]{{"ef19ccdf-d2f9-11e0-aff7-001a4be8a71c"}};
    public static final String[][] RETRIEVE_FEW_PARAMS = new String[][]{{"f272ea5f-3fc6-11e1-85f3-001b245d68a8"}, {"ef19ccd2-d2f9-11e0-aff7-001a4be8a71c"}, {"aaaaaaaa-6a03-3221-90ad-83af3fcd7b64"}, {"2d073c98-9bdd-36fb-bfcf-b20fc2a2b1a2"}};

    public static final String[][] INSERT_NEW_PARAMS = new String[][]{{"90800349-619a-11e0-a335-xxxxxxxxxxxX", "Доктор тестовых наук", "3"}};
    public static final String[][] INSERT_NEW_ANALOG_PARAMS = new String[][]{{"90800349-619a-11e0-a335-xxxxxxxxxxxY", "Доктор филологических наук", "18"}};
    public static final String[][] INSERT_NEW_GUID_EXISTS_PARAMS = new String[][]{{"90800349-619a-11e0-a335-xxxxxxxxxxxY", "Доктор филологических наук1", "188"}};
    public static final String[][] INSERT_NEW_MERGE_GUID_EXISTS_PARAMS = new String[][]{{"90800349-619a-11e0-a335-xxxxxxxxxxxZ", "Доктор филологических наук1", "188", "90800349-619a-11e0-a335-xxxxxxxxxxxZ; 90800349-619a-11e0-a335-xxxxxxxxxxxY; 90800349-619a-11e0-a335-xxxxxxxxxx31"}};
    public static final String[] INSERT_MASS_TEMPLATE_PARAMS = new String[]{"Доктор для тестирования", "mass"};

    public static final String[][] DELETE_EMPTY_PARAMS = new String[][]{{null}};
    public static final String[][] DELETE_NON_EXISTS_PARAMS = new String[][]{{"ef19ccf5-d2f9-11e0-aff7-001a4be8a71X"}};
    public static final String[][] DELETE_SINGLE_PARAMS = new String[][]{{"ef19ccf5-d2f9-11e0-aff7-001a4be8a71c"}};
    public static final String[][] DELETE_NON_DELETABLE_PARAMS = new String[][]{{"ef19ccf5-d2f9-11e0-aff7-001a4be8a71c"}};
    public static final String[][] DELETE_MASS_PARAMS = new String[][]{{"ef19ccf5-d2f9-11e0-aff7-001a4be8a71c"}};

    @Override
    protected INsiEntity createNsiEntity(String[] fieldValues)
    {
        DegreeType entity = NsiLocalAccessServiceTestUtil.FACTORY.createDegreeType();
        if (null != fieldValues)
        {
            if (fieldValues.length > 0) entity.setID(fieldValues[0]);
            if (fieldValues.length > 1) entity.setDegreeName(fieldValues[1]);
            if (fieldValues.length > 2) entity.setDegreeID(fieldValues[2]);
            if (fieldValues.length > 3 && null != fieldValues[3])
                entity.getOtherAttributes().put(new QName("mergeDuplicates"), fieldValues[3]);
        }
        return entity;
    }
}