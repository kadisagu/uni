/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.FefuAttSheetManager;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.AddEdit.FefuAttSheetAddEdit;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.AddEdit.FefuAttSheetAddEditUI;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.Pub.FefuAttSheetPubUI;
import ru.tandemservice.unifefu.entity.SessionAttSheetDocument;

/**
 * @author DMITRY KNYAZEV
 * @since 30.06.2014
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitId")})
@SuppressWarnings("UnusedDeclaration")
public class FefuAttSheetListUI extends UIPresenter
{
	private Long orgUnitId;

	private PageableSearchListDataSource _contentDS;

	@Override
	public void onComponentRefresh()
	{
		_contentDS = getConfig().getDataSource(FefuAttSheetList.ATT_SHEET_DS);
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		if (FefuAttSheetList.ATT_SHEET_DS.equals(dataSource.getName()))
		{
			dataSource.put(FefuAttSheetList.FEFU_FORMATIVE_ORGUNIT_ID, getOrgUnitId());
			dataSource.put(FefuAttSheetList.FEFU_REQUEST_DATE_FROM, getSettings().get(FefuAttSheetList.FEFU_REQUEST_DATE_FROM));
			dataSource.put(FefuAttSheetList.FEFU_REQUEST_DATE_TO, getSettings().get(FefuAttSheetList.FEFU_REQUEST_DATE_TO));
			dataSource.put(FefuAttSheetList.FEFU_DOCUMENT_NUMBER, getSettings().get(FefuAttSheetList.FEFU_DOCUMENT_NUMBER));
			dataSource.put(FefuAttSheetList.FEFU_STUDENT_FIRST_NAME, getSettings().get(FefuAttSheetList.FEFU_STUDENT_FIRST_NAME));
			dataSource.put(FefuAttSheetList.FEFU_STUDENT_LAST_NAME, getSettings().get(FefuAttSheetList.FEFU_STUDENT_LAST_NAME));
		}
	}

	public void onClickAddEdit()
	{
		_uiActivation.asRegion(FefuAttSheetAddEdit.class)
				.parameter(FefuAttSheetAddEditUI.DOCUMENT_ID, null)
				.parameter(FefuAttSheetAddEditUI.ORG_UNIT_ID, getOrgUnitId())
				.activate();
	}

	public void onPrintEntityFromList()
	{
		String COMPONENT_NAME = "ru.tandemservice.unifefu.component.orgunit.AttSheetPrintRtf";
		getActivationBuilder().asRegion(COMPONENT_NAME)
				.parameter("sessionTransferDocumentId", getListenerParameter())
				.activate();
	}

	public void onEditEntityFromList()
	{
		_uiActivation.asRegion(FefuAttSheetAddEdit.class)
				.parameter(FefuAttSheetAddEditUI.DOCUMENT_ID, getListenerParameterAsLong())
				.parameter(FefuAttSheetAddEditUI.ORG_UNIT_ID, getOrgUnitId())
				.activate();
	}

	public void onDeleteEntityFromList()
	{
		SessionAttSheetDocument document = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
		FefuAttSheetManager.instance().dao().deleteAttSheetDocument(document);
	}

	public ParametersMap getAddEditFormParameters()
	{
		return ParametersMap.createWith(FefuAttSheetPubUI.ORGUNIT_ID_PARAM, getOrgUnitId());
	}

	public String getCurrentItemAlert()
	{
		return "Удалить аттестационный лист \"" + getCurrentNumber() + "\"?";
	}

	public String getCurrentNumber()
	{
		SessionAttSheetDocument document = _contentDS.getCurrent();
		return document.getNumber();
	}

	public Long getOrgUnitId()
	{
		return orgUnitId;
	}

	public void setOrgUnitId(Long orgUnitId)
	{
		this.orgUnitId = orgUnitId;
	}
}
