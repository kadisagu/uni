/* $Id$ */
package ru.tandemservice.unifefu.component.order.list.StudentListOrderPrint;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.order.list.StudentListOrderPrint.Model;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 7/5/13
 */
public class DAO extends ru.tandemservice.movestudent.component.order.list.StudentListOrderPrint.DAO
{
    private String getCapitalizedFirstChar(String s)
    {
        if (StringUtils.isNotEmpty(s))
            return StringUtils.capitalize(s.substring(0, 1));
        return "";
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        // Получаем ФИО студента из первой выписки в приказе
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .top(1)
                .column(property("i", IdentityCard.P_LAST_NAME))
                .column(property("i", IdentityCard.P_FIRST_NAME))
                .column(property("i", IdentityCard.P_MIDDLE_NAME))
                .joinPath(DQLJoinType.inner, AbstractStudentExtract.paragraph().fromAlias("e"), "p")
                .joinPath(DQLJoinType.inner, AbstractStudentExtract.entity().person().identityCard().fromAlias("e"), "i")
                .where(eq(property("p", IAbstractParagraph.L_ORDER), value(model.getOrder())))
                .order(property("p", IAbstractParagraph.P_NUMBER))
                .order(property("e", IAbstractExtract.P_NUMBER));

        Object[] fio = dql.createStatement(getSession()).uniqueResult();
        String studentFio = null;
        if (fio != null)
        {
            studentFio = fio[0] + " " + getCapitalizedFirstChar((String) fio[1]) + getCapitalizedFirstChar((String) fio[2]);
        }
        model.setFileName((null != studentFio ? (studentFio + " ") : "Order ") + "orderId" + String.valueOf(model.getOrder().getId()));
    }

    @Override
    protected void setBarcodeAndAddNumOfPages(Model model)
    {
        model.setBarcode(UnifefuDaoFacade.getUnifefuDAO().getListOrderBarCode(model.getOrder()));
        model.setAddNumOfPages(true);
    }
}
