/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu5.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuCourseTransferStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuCourseTransferStuListExtract, Model>
{
}
