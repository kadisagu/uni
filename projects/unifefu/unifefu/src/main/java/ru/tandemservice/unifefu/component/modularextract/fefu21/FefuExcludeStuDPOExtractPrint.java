/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu21;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOExtract;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public class FefuExcludeStuDPOExtractPrint implements IPrintFormCreator<FefuExcludeStuDPOExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuExcludeStuDPOExtract extract)
    {
        if (extract.isIndividual())
        {
            byte[] rel = UniDaoFacade.getCoreDao().getProperty(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.P_CONTENT, FefuOrderToPrintFormRelation.L_ORDER, extract.getParagraph().getOrder());
            if (rel != null)
                return new RtfReader().read(rel);
        }

        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        FefuAdditionalProfessionalEducationProgramForStudent progStudent = DataAccessServices.dao().get(FefuAdditionalProfessionalEducationProgramForStudent.class,
                                            FefuAdditionalProfessionalEducationProgramForStudent.student(), extract.getEntity());
        modifier.put("fefuDpoProgram", progStudent != null && progStudent.getProgram() != null ? progStudent.getProgram().getTitle() : "");
        modifier.put("excludeDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getExcludeDate()));
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}