/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import org.apache.axis.AxisFault;

import javax.xml.rpc.ServiceException;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceMainTest
{
    public static void main(String[] args) throws Exception
    {
        try
        {
            //NsiLocalAccessServiceAgreementTest test = new NsiLocalAccessServiceAgreementTest();
            //test.retrieve(NsiLocalAccessServiceAgreementTest.RETRIEVE_WHOLE_PARAMS);
            //test.insert(NsiLocalAccessServiceAgreementTest.INSERT_NEW_PARAMS);



            //NsiLocalAccessServiceLanguageLevelTest test = new NsiLocalAccessServiceLanguageLevelTest();

            //test.retrieve(NsiLocalAccessServiceLanguageLevelTest.RETRIEVE_WHOLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceLanguageLevelTest.RETRIEVE_NON_EXISTS_PARAMS);
            //test.retrieve(NsiLocalAccessServiceLanguageLevelTest.RETRIEVE_SINGLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceLanguageLevelTest.RETRIEVE_FEW_PARAMS);

            //test.insert(NsiLocalAccessServiceLanguageLevelTest.INSERT_NEW_PARAMS);
            //test.insert(NsiLocalAccessServiceLanguageLevelTest.INSERT_NEW_PARAMS1);
            //test.insert(NsiLocalAccessServiceLanguageLevelTest.INSERT_NEW_ANALOG_PARAMS);
            //test.insert(NsiLocalAccessServiceLanguageLevelTest.INSERT_NEW_GUID_EXISTS_PARAMS);
            //test.insert(NsiLocalAccessServiceLanguageLevelTest.INSERT_NEW_MERGE_GUID_EXISTS_PARAMS);
            //test.insertMass(1000, NsiLocalAccessServiceLanguageLevelTest.INSERT_MASS_TEMPLATE_PARAMS);

            //test.delete(NsiLocalAccessServiceLanguageLevelTest.DELETE_EMPTY_PARAMS);
            //test.delete(NsiLocalAccessServiceLanguageLevelTest.DELETE_NON_EXISTS_PARAMS);
            //test.delete(NsiLocalAccessServiceLanguageLevelTest.DELETE_SINGLE_PARAMS);
            //test.delete(NsiLocalAccessServiceLanguageLevelTest.DELETE_NON_DELETABLE_PARAMS);
            //test.delete(NsiLocalAccessServiceLanguageLevelTest.DELETE_MASS_PARAMS);



            //NsiLocalAccessServiceLanguageTest test = new NsiLocalAccessServiceLanguageTest();

            //test.retrieve(NsiLocalAccessServiceLanguageTest.RETRIEVE_WHOLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceLanguageTest.RETRIEVE_NON_EXISTS_PARAMS);
            //test.retrieve(NsiLocalAccessServiceLanguageTest.RETRIEVE_SINGLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceLanguageTest.RETRIEVE_FEW_PARAMS);

            //test.insert(NsiLocalAccessServiceLanguageTest.INSERT_NEW_PARAMS);
            //test.insert(NsiLocalAccessServiceLanguageTest.INSERT_NEW_PARAMS1);
            //test.insert(NsiLocalAccessServiceLanguageTest.INSERT_NEW_ANALOG_PARAMS);
            //test.insert(NsiLocalAccessServiceLanguageTest.INSERT_NEW_GUID_EXISTS_PARAMS);
            //test.insert(NsiLocalAccessServiceLanguageTest.INSERT_NEW_MERGE_GUID_EXISTS_PARAMS);
            //test.insertMass(1000, NsiLocalAccessServiceLanguageTest.INSERT_MASS_TEMPLATE_PARAMS);

            //test.delete(NsiLocalAccessServiceLanguageTest.DELETE_EMPTY_PARAMS);
            //test.delete(NsiLocalAccessServiceLanguageTest.DELETE_NON_EXISTS_PARAMS);
            //test.delete(NsiLocalAccessServiceLanguageTest.DELETE_SINGLE_PARAMS);
            //test.delete(NsiLocalAccessServiceLanguageTest.DELETE_NON_DELETABLE_PARAMS);
            //test.delete(NsiLocalAccessServiceLanguageTest.DELETE_MASS_PARAMS);



            //NsiLocalAccessServiceRelDegreeTest test = new NsiLocalAccessServiceRelDegreeTest();

            //test.retrieve(NsiLocalAccessServiceRelDegreeTest.RETRIEVE_WHOLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceRelDegreeTest.RETRIEVE_NON_EXISTS_PARAMS);
            //test.retrieve(NsiLocalAccessServiceRelDegreeTest.RETRIEVE_SINGLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceRelDegreeTest.RETRIEVE_FEW_PARAMS);

            //test.insert(NsiLocalAccessServiceRelDegreeTest.INSERT_NEW_PARAMS);
            //test.insert(NsiLocalAccessServiceRelDegreeTest.INSERT_NEW_ANALOG_PARAMS);
            //test.insert(NsiLocalAccessServiceRelDegreeTest.INSERT_NEW_GUID_EXISTS_PARAMS);
            //test.insert(NsiLocalAccessServiceRelDegreeTest.INSERT_NEW_MERGE_GUID_EXISTS_PARAMS);
            //test.insertMass(1000, NsiLocalAccessServiceRelDegreeTest.INSERT_MASS_TEMPLATE_PARAMS);

            //test.delete(NsiLocalAccessServiceRelDegreeTest.DELETE_EMPTY_PARAMS);
            //test.delete(NsiLocalAccessServiceRelDegreeTest.DELETE_NON_EXISTS_PARAMS);
            //test.delete(NsiLocalAccessServiceRelDegreeTest.DELETE_SINGLE_PARAMS);
            //test.delete(NsiLocalAccessServiceRelDegreeTest.DELETE_NON_DELETABLE_PARAMS);
            //test.delete(NsiLocalAccessServiceRelDegreeTest.DELETE_MASS_PARAMS);


            //NsiLocalAccessServiceCompensationTypeTest test = new NsiLocalAccessServiceCompensationTypeTest();

            //test.retrieve(NsiLocalAccessServiceCompensationTypeTest.RETRIEVE_WHOLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceCompensationTypeTest.RETRIEVE_NON_EXISTS_PARAMS);
            //test.retrieve(NsiLocalAccessServiceCompensationTypeTest.RETRIEVE_SINGLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceCompensationTypeTest.RETRIEVE_FEW_PARAMS);

            //test.insert(NsiLocalAccessServiceCompensationTypeTest.INSERT_NEW_PARAMS);
            //test.insert(NsiLocalAccessServiceCompensationTypeTest.INSERT_NEW_ANALOG_PARAMS);
            //test.insert(NsiLocalAccessServiceCompensationTypeTest.INSERT_NEW_GUID_EXISTS_PARAMS);
            //test.insert(NsiLocalAccessServiceCompensationTypeTest.INSERT_NEW_MERGE_GUID_EXISTS_PARAMS);
            //test.insertMass(1000, NsiLocalAccessServiceCompensationTypeTest.INSERT_MASS_TEMPLATE_PARAMS);

            //test.delete(NsiLocalAccessServiceCompensationTypeTest.DELETE_EMPTY_PARAMS);
            //test.delete(NsiLocalAccessServiceCompensationTypeTest.DELETE_NON_EXISTS_PARAMS);
            //test.delete(NsiLocalAccessServiceCompensationTypeTest.DELETE_SINGLE_PARAMS);
            //test.delete(NsiLocalAccessServiceCompensationTypeTest.DELETE_NON_DELETABLE_PARAMS);
            //test.delete(NsiLocalAccessServiceCompensationTypeTest.DELETE_MASS_PARAMS);


            //NsiLocalAccessServiceDevelopPeriodTest test = new NsiLocalAccessServiceDevelopPeriodTest();

            //test.retrieve(NsiLocalAccessServiceDevelopPeriodTest.RETRIEVE_WHOLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDevelopPeriodTest.RETRIEVE_NON_EXISTS_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDevelopPeriodTest.RETRIEVE_SINGLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDevelopPeriodTest.RETRIEVE_FEW_PARAMS);

            //test.insert(NsiLocalAccessServiceDevelopPeriodTest.INSERT_NEW_PARAMS);
            //test.insert(NsiLocalAccessServiceDevelopPeriodTest.INSERT_NEW_ANALOG_PARAMS);
            //test.insert(NsiLocalAccessServiceDevelopPeriodTest.INSERT_NEW_GUID_EXISTS_PARAMS);
            //test.insert(NsiLocalAccessServiceDevelopPeriodTest.INSERT_NEW_MERGE_GUID_EXISTS_PARAMS);
            //test.insertMass(1000, NsiLocalAccessServiceDevelopPeriodTest.INSERT_MASS_TEMPLATE_PARAMS);

            //test.delete(NsiLocalAccessServiceDevelopPeriodTest.DELETE_EMPTY_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopPeriodTest.DELETE_NON_EXISTS_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopPeriodTest.DELETE_SINGLE_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopPeriodTest.DELETE_NON_DELETABLE_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopPeriodTest.DELETE_MASS_PARAMS);


            //NsiLocalAccessServiceDevelopTechTest test = new NsiLocalAccessServiceDevelopTechTest();

            //test.retrieve(NsiLocalAccessServiceDevelopTechTest.RETRIEVE_WHOLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDevelopTechTest.RETRIEVE_NON_EXISTS_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDevelopTechTest.RETRIEVE_SINGLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDevelopTechTest.RETRIEVE_FEW_PARAMS);

            //test.insert(NsiLocalAccessServiceDevelopTechTest.INSERT_NEW_PARAMS);
            //test.insert(NsiLocalAccessServiceDevelopTechTest.INSERT_NEW_ANALOG_PARAMS);
            //test.insert(NsiLocalAccessServiceDevelopTechTest.INSERT_NEW_GUID_EXISTS_PARAMS);
            //test.insert(NsiLocalAccessServiceDevelopTechTest.INSERT_NEW_MERGE_GUID_EXISTS_PARAMS);
            //test.insertMass(1000, NsiLocalAccessServiceDevelopTechTest.INSERT_MASS_TEMPLATE_PARAMS);

            //test.delete(NsiLocalAccessServiceDevelopTechTest.DELETE_EMPTY_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopTechTest.DELETE_NON_EXISTS_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopTechTest.DELETE_SINGLE_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopTechTest.DELETE_NON_DELETABLE_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopTechTest.DELETE_MASS_PARAMS);


            //NsiLocalAccessServiceDevelopConditionTest test = new NsiLocalAccessServiceDevelopConditionTest();

            //test.retrieve(NsiLocalAccessServiceDevelopConditionTest.RETRIEVE_WHOLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDevelopConditionTest.RETRIEVE_NON_EXISTS_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDevelopConditionTest.RETRIEVE_SINGLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDevelopConditionTest.RETRIEVE_FEW_PARAMS);

            //test.insert(NsiLocalAccessServiceDevelopConditionTest.INSERT_NEW_PARAMS);
            //test.insert(NsiLocalAccessServiceDevelopConditionTest.INSERT_NEW_ANALOG_PARAMS);
            //test.insert(NsiLocalAccessServiceDevelopConditionTest.INSERT_NEW_GUID_EXISTS_PARAMS);
            //test.insert(NsiLocalAccessServiceDevelopConditionTest.INSERT_NEW_MERGE_GUID_EXISTS_PARAMS);
            //test.insertMass(1000, NsiLocalAccessServiceDevelopConditionTest.INSERT_MASS_TEMPLATE_PARAMS);

            //test.delete(NsiLocalAccessServiceDevelopConditionTest.DELETE_EMPTY_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopConditionTest.DELETE_NON_EXISTS_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopConditionTest.DELETE_SINGLE_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopConditionTest.DELETE_NON_DELETABLE_PARAMS);
            //test.delete(NsiLocalAccessServiceDevelopConditionTest.DELETE_MASS_PARAMS);


            //NsiLocalAccessServiceEducationFormTest test = new NsiLocalAccessServiceEducationFormTest();

            //test.retrieve(NsiLocalAccessServiceEducationFormTest.RETRIEVE_WHOLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceEducationFormTest.RETRIEVE_NON_EXISTS_PARAMS);
            //test.retrieve(NsiLocalAccessServiceEducationFormTest.RETRIEVE_SINGLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceEducationFormTest.RETRIEVE_FEW_PARAMS);

            //test.insert(NsiLocalAccessServiceEducationFormTest.INSERT_NEW_PARAMS);
            //test.insert(NsiLocalAccessServiceEducationFormTest.INSERT_NEW_ANALOG_PARAMS);
            //test.insert(NsiLocalAccessServiceEducationFormTest.INSERT_NEW_GUID_EXISTS_PARAMS);
            //test.insert(NsiLocalAccessServiceEducationFormTest.INSERT_NEW_MERGE_GUID_EXISTS_PARAMS);
            //test.insertMass(1000, NsiLocalAccessServiceEducationFormTest.INSERT_MASS_TEMPLATE_PARAMS);

            //test.delete(NsiLocalAccessServiceEducationFormTest.DELETE_EMPTY_PARAMS);
            //test.delete(NsiLocalAccessServiceEducationFormTest.DELETE_NON_EXISTS_PARAMS);
            //test.delete(NsiLocalAccessServiceEducationFormTest.DELETE_SINGLE_PARAMS);
            //test.delete(NsiLocalAccessServiceEducationFormTest.DELETE_NON_DELETABLE_PARAMS);
            //test.delete(NsiLocalAccessServiceEducationFormTest.DELETE_MASS_PARAMS);


            //NsiLocalAccessServiceDegreeTest test = new NsiLocalAccessServiceDegreeTest();

            //test.retrieve(NsiLocalAccessServiceDegreeTest.RETRIEVE_WHOLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDegreeTest.RETRIEVE_NON_EXISTS_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDegreeTest.RETRIEVE_SINGLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceDegreeTest.RETRIEVE_FEW_PARAMS);

            //test.insert(NsiLocalAccessServiceDegreeTest.INSERT_NEW_PARAMS);
            //test.insert(NsiLocalAccessServiceDegreeTest.INSERT_NEW_ANALOG_PARAMS);
            //test.insert(NsiLocalAccessServiceDegreeTest.INSERT_NEW_GUID_EXISTS_PARAMS);
            //test.insert(NsiLocalAccessServiceDegreeTest.INSERT_NEW_MERGE_GUID_EXISTS_PARAMS);
            //test.insertMass(1000, NsiLocalAccessServiceDegreeTest.INSERT_MASS_TEMPLATE_PARAMS);

            //test.delete(NsiLocalAccessServiceDegreeTest.DELETE_EMPTY_PARAMS);
            //test.delete(NsiLocalAccessServiceDegreeTest.DELETE_NON_EXISTS_PARAMS);
            //test.delete(NsiLocalAccessServiceDegreeTest.DELETE_SINGLE_PARAMS);
            //test.delete(NsiLocalAccessServiceDegreeTest.DELETE_NON_DELETABLE_PARAMS);
            //test.delete(NsiLocalAccessServiceDegreeTest.DELETE_MASS_PARAMS);


            NsiLocalAccessServiceAcademicRankTest test = new NsiLocalAccessServiceAcademicRankTest();

            test.retrieve(NsiLocalAccessServiceAcademicRankTest.RETRIEVE_WHOLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceAcademicRankTest.RETRIEVE_NON_EXISTS_PARAMS);
            //test.retrieve(NsiLocalAccessServiceAcademicRankTest.RETRIEVE_SINGLE_PARAMS);
            //test.retrieve(NsiLocalAccessServiceAcademicRankTest.RETRIEVE_FEW_PARAMS);

            //test.insert(NsiLocalAccessServiceAcademicRankTest.INSERT_NEW_PARAMS);
            //test.insert(NsiLocalAccessServiceAcademicRankTest.INSERT_NEW_ANALOG_PARAMS);
            //test.insert(NsiLocalAccessServiceAcademicRankTest.INSERT_NEW_GUID_EXISTS_PARAMS);
            //test.insert(NsiLocalAccessServiceAcademicRankTest.INSERT_NEW_MERGE_GUID_EXISTS_PARAMS);
            //test.insertMass(1000, NsiLocalAccessServiceAcademicRankTest.INSERT_MASS_TEMPLATE_PARAMS);

            //test.delete(NsiLocalAccessServiceAcademicRankTest.DELETE_EMPTY_PARAMS);
            //test.delete(NsiLocalAccessServiceAcademicRankTest.DELETE_NON_EXISTS_PARAMS);
            //test.delete(NsiLocalAccessServiceAcademicRankTest.DELETE_SINGLE_PARAMS);
            //test.delete(NsiLocalAccessServiceAcademicRankTest.DELETE_NON_DELETABLE_PARAMS);
            //test.delete(NsiLocalAccessServiceAcademicRankTest.DELETE_MASS_PARAMS);
        } catch (Exception e)
        {
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            throw e;
        }
    }
}