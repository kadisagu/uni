package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь элемента реестра с образовательной программой (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuRegistryElement2EduProgramKindRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel";
    public static final String ENTITY_NAME = "fefuRegistryElement2EduProgramKindRel";
    public static final int VERSION_HASH = -1724341388;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_ELEMENT = "registryElement";
    public static final String L_EDU_PROGRAM_SUBJECT = "eduProgramSubject";

    private EppRegistryElement _registryElement;     // Элемент реестра
    private EduProgramSubject _eduProgramSubject;     // Направление подготовки профессионального образования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    /**
     * @param registryElement Элемент реестра. Свойство не может быть null.
     */
    public void setRegistryElement(EppRegistryElement registryElement)
    {
        dirty(_registryElement, registryElement);
        _registryElement = registryElement;
    }

    /**
     * @return Направление подготовки профессионального образования. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getEduProgramSubject()
    {
        return _eduProgramSubject;
    }

    /**
     * @param eduProgramSubject Направление подготовки профессионального образования. Свойство не может быть null.
     */
    public void setEduProgramSubject(EduProgramSubject eduProgramSubject)
    {
        dirty(_eduProgramSubject, eduProgramSubject);
        _eduProgramSubject = eduProgramSubject;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuRegistryElement2EduProgramKindRelGen)
        {
            setRegistryElement(((FefuRegistryElement2EduProgramKindRel)another).getRegistryElement());
            setEduProgramSubject(((FefuRegistryElement2EduProgramKindRel)another).getEduProgramSubject());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuRegistryElement2EduProgramKindRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuRegistryElement2EduProgramKindRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuRegistryElement2EduProgramKindRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registryElement":
                    return obj.getRegistryElement();
                case "eduProgramSubject":
                    return obj.getEduProgramSubject();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registryElement":
                    obj.setRegistryElement((EppRegistryElement) value);
                    return;
                case "eduProgramSubject":
                    obj.setEduProgramSubject((EduProgramSubject) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registryElement":
                        return true;
                case "eduProgramSubject":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registryElement":
                    return true;
                case "eduProgramSubject":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registryElement":
                    return EppRegistryElement.class;
                case "eduProgramSubject":
                    return EduProgramSubject.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuRegistryElement2EduProgramKindRel> _dslPath = new Path<FefuRegistryElement2EduProgramKindRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuRegistryElement2EduProgramKindRel");
    }
            

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel#getRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> registryElement()
    {
        return _dslPath.registryElement();
    }

    /**
     * @return Направление подготовки профессионального образования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel#getEduProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> eduProgramSubject()
    {
        return _dslPath.eduProgramSubject();
    }

    public static class Path<E extends FefuRegistryElement2EduProgramKindRel> extends EntityPath<E>
    {
        private EppRegistryElement.Path<EppRegistryElement> _registryElement;
        private EduProgramSubject.Path<EduProgramSubject> _eduProgramSubject;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel#getRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> registryElement()
        {
            if(_registryElement == null )
                _registryElement = new EppRegistryElement.Path<EppRegistryElement>(L_REGISTRY_ELEMENT, this);
            return _registryElement;
        }

    /**
     * @return Направление подготовки профессионального образования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel#getEduProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> eduProgramSubject()
        {
            if(_eduProgramSubject == null )
                _eduProgramSubject = new EduProgramSubject.Path<EduProgramSubject>(L_EDU_PROGRAM_SUBJECT, this);
            return _eduProgramSubject;
        }

        public Class getEntityClass()
        {
            return FefuRegistryElement2EduProgramKindRel.class;
        }

        public String getEntityName()
        {
            return "fefuRegistryElement2EduProgramKindRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
