/* $Id$ */
package ru.tandemservice.unifefu.base.ext.DiplomaIssuance;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unidip.base.bo.DipDocument.logic.IDipDocumentDao;
import ru.tandemservice.unifefu.base.ext.DiplomaIssuance.ui.logic.DipDocumentExtDao;


/**
 * @author Andrey Avetisov
 * @since 26.09.2014
 */
@Configuration
public class DiplomaIssuanceExtManager extends BusinessObjectExtensionManager
{

    @Bean
    @BeanOverride
    public IDipDocumentDao dao()
    {
        return new DipDocumentExtDao();
    }
}
