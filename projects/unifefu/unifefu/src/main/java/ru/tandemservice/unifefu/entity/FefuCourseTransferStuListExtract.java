package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.gen.FefuCourseTransferStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «О переводе с курса на следующий курс»
 */
public class FefuCourseTransferStuListExtract extends FefuCourseTransferStuListExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getBeginDate();
    }

    @Override
    public Group getGroupOld()
    {
        return getGroup();
    }
}