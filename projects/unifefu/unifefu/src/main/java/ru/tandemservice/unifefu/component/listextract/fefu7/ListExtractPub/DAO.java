/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu7.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaSuccessWithDismissStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class DAO extends AbstractListExtractPubDAO<FefuGiveDiplomaSuccessWithDismissStuListExtract, Model> implements IDAO
{
}
