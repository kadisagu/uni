/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu23;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public class FefuTransfStuDPOExtractPrint implements IPrintFormCreator<FefuTransfStuDPOExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuTransfStuDPOExtract extract)
    {
        if (extract.isIndividual())
        {
            byte[] rel = UniDaoFacade.getCoreDao().getProperty(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.P_CONTENT, FefuOrderToPrintFormRelation.L_ORDER, extract.getParagraph().getOrder());
            if (rel != null)
                return new RtfReader().read(rel);
        }

        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("fefuDpoProgram", extract.getDpoProgramNew().getTitle());
        modifier.put("fefuIndividualPlane", extract.isIndividualPlan()? " (по индивидуальному плану)" : "");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}