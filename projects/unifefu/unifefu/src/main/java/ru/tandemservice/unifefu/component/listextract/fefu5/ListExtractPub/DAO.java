/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu5.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuCourseTransferStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class DAO extends AbstractListExtractPubDAO<FefuCourseTransferStuListExtract, Model> implements IDAO
{
}
