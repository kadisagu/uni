/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu3;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.component.listextract.fefu3.utils.FefuAdmitToDiplomaParagraphPartWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu3.utils.FefuAdmitToDiplomaParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 19.03.2013
 */
public class FefuAdmitToDiplomaStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<FefuAdmitToDiplomaStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, extracts.get(0), prepareParagraphsStructure(extracts));

        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    protected List<FefuAdmitToDiplomaParagraphWrapper> prepareParagraphsStructure(List<FefuAdmitToDiplomaStuListExtract> extracts)
    {
        List<FefuAdmitToDiplomaParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        int ind;

        for (FefuAdmitToDiplomaStuListExtract extract: extracts)
        {
            Student student = extract.getEntity();
            CompensationType compensationType = student.getCompensationType();
            EducationLevelsHighSchool educationLevelsHighSchool = student.getEducationOrgUnit().getEducationLevelHighSchool();

            FefuAdmitToDiplomaParagraphWrapper paragraphWrapper = new FefuAdmitToDiplomaParagraphWrapper(compensationType, extract);
            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            FefuAdmitToDiplomaParagraphPartWrapper paragraphPartWrapper = new FefuAdmitToDiplomaParagraphPartWrapper(educationLevelsHighSchool, extract);
            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);

            Person person = student.getPerson();

            if (!paragraphPartWrapper.getPersonList().contains(person))
                paragraphPartWrapper.getPersonList().add(person);
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, FefuAdmitToDiplomaStuListExtract firstExtract, List<FefuAdmitToDiplomaParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            String courseStr = firstExtract.getCourse().getTitle();
            Group group = firstExtract.getGroup();
            EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();

            int parNumber = 0;
            for (FefuAdmitToDiplomaParagraphWrapper paragraphWrapper: paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_ADMIT_TO_DIPLOMA_DEFEND_WORK_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());

                // добавим изменения в шаблон в зависимости от флага "без допуска к ГИА"
                if (firstExtract.isNotNeedAdmissionToGIA()) {
                    paragraphInjectModifier.put("fefuEduPlanCompleted_A", "выполнивших содержание учебного плана, ");
                    paragraphInjectModifier.put("fefuAdmitedToGia_A", "");
                } else {
                    paragraphInjectModifier.put("fefuEduPlanCompleted_A", "");
                    paragraphInjectModifier.put("fefuAdmitedToGia_A", "допущенных к государственной итоговой аттестации");
                }

                paragraphInjectModifier.put("course", courseStr);
                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "groupInternal_G", group, educationOrgUnit.getDevelopForm(), " группы ");
                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, educationOrgUnit.getDevelopForm(), "");
                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, educationOrgUnit.getEducationLevelHighSchool().getEducationLevel(), new String[]{"fefuEducationStrDirection"}, false);
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, educationOrgUnit, "formativeOrgUnitStr", "");
                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, paragraphWrapper.getCompensationType(), "", false);

                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() == 1 ? "" : String.valueOf(++parNumber) + ". ");
                paragraphInjectModifier.put("paragraphNumberConditional", paragraphWrappers.size() == 1 ? "" : String.valueOf(parNumber) + ". ");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(group), "fefuShortFastExtendedOptionalText");
                modifyReason(paragraphInjectModifier, firstExtract);

                paragraphInjectModifier.put("gosExams", firstExtract.getPluralForGosExam() == null ? "" :
                        firstExtract.getPluralForGosExam() ? " и сдавших государственные экзамены" : " и сдавших государственный экзамен");

                paragraphInjectModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<FefuAdmitToDiplomaParagraphPartWrapper> paragraphPartWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (FefuAdmitToDiplomaParagraphPartWrapper paragraphPartWrapper: paragraphPartWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_ADMIT_TO_DIPLOMA_DEFEND_WORK_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, paragraphPartWrapper.getFirstExtract());
                modifyFefuEducationStrProfile(paragraphPartInjectModifier, paragraphPartWrapper.getEducationLevelsHighSchool());

                List<Person> personList = paragraphPartWrapper.getPersonList();
                RtfString rtfString = new RtfString();

                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);

                int i = 0;

                for (; i < personList.size() - 1; i++)
                {
                    rtfString.append(String.valueOf(i + 1)).append(". ").append(personList.get(i).getFullFio()).par();
                }

                rtfString.append(String.valueOf(i + 1)).append(". ").append(personList.get(i).getFullFio());

                paragraphPartInjectModifier.put("STUDENT_LIST", rtfString);
                paragraphPartInjectModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected static void modifyFefuEducationStrProfile(RtfInjectModifier modifier, EducationLevelsHighSchool educationLevelsHighSchool)
    {
        StructureEducationLevels levels = educationLevelsHighSchool.getEducationLevel().getLevelType();
        RtfString fefuEducationStrProfile = new RtfString();
        if (levels.isSpecialization() || levels.isProfile())
        {
            fefuEducationStrProfile.append(levels.isProfile() ? levels.isBachelor() ? "Профиль «" : "Магистерская программа «" : "Специализация «");
            fefuEducationStrProfile.append(educationLevelsHighSchool.getTitle() + "»").par();
        }
        modifier.put("fefuEducationStrProfile", fefuEducationStrProfile);
    }

    protected static void modifyReason(RtfInjectModifier modifier, AbstractStudentExtract extract)
    {
        StudentOrderReasons reason = ((StudentListOrder) extract.getParagraph().getOrder()).getReason();
        if (null != reason)
        {
            String reasonPrint = reason.getPrintTitle() == null ? reason.getTitle() : reason.getPrintTitle();
            modifier.put("reason", reasonPrint);
            modifier.put("Reason", StringUtils.capitalize(reasonPrint));
            modifier.put("reasonPrint", reasonPrint);
            modifier.put("ReasonPrint", StringUtils.capitalize(reasonPrint));
            modifier.put("reasonTitle", reason.getTitle());
            modifier.put("ReasonTitle", StringUtils.capitalize(reason.getTitle()));
        } else
        {
            modifier.put("reason", "");
            modifier.put("Reason", "");
            modifier.put("reasonPrint", "");
            modifier.put("ReasonPrint", "");
            modifier.put("reasonTitle", "");
            modifier.put("ReasonTitle", "");
        }
    }
}