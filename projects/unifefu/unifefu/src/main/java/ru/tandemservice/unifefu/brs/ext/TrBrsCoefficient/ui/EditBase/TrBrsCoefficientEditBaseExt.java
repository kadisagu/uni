package ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.EditBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBase;

/**
 * Created with IntelliJ IDEA.
 * User: newdev
 * Date: 03.02.14
 * Time: 16:42
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class TrBrsCoefficientEditBaseExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrBrsCoefficientEditBaseExtUI.class.getSimpleName();

    @Autowired
    private TrBrsCoefficientEditBase _trBrsCoefficientEditBase;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trBrsCoefficientEditBase.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrBrsCoefficientEditBaseExtUI.class))
                .create();
    }
}