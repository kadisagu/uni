/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 11.07.2013
 */
public class CompetitionGroupDSHandler extends EntityComboDataSourceHandler
{
    public static final String ENROLLMENT_CAMPAIGN_PROP = EnrCampaignFormativeOrgUnitDSHandler.ENROLLMENT_CAMPAIGN_PROP;
    public static final String QUALIFICATIONS_PROP = "qualifications";
    public static final String FORMATIVE_ORGUNIT_PROP = "formativeOrgUnit";
    public static final String TERRITORIAL_ORGUNIT_PROP = "territorialOrgUnit";

    public CompetitionGroupDSHandler(String ownerId)
    {
        super(ownerId, CompetitionGroup.class);
        this.order(CompetitionGroup.title());
        this.filter(CompetitionGroup.title());
    }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        super.applyWhereConditions(alias, dql, context);
        EnrollmentCampaign enrollmentCampaign = context.get(ENROLLMENT_CAMPAIGN_PROP);
        OrgUnit formativeOrgUnit = context.get(FORMATIVE_ORGUNIT_PROP);
        OrgUnit territorialOrgUnit = context.get(TERRITORIAL_ORGUNIT_PROP);
        List<Qualifications> qualifications = context.get(QUALIFICATIONS_PROP);

        DQLSelectBuilder directionsFilter = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "dir")
                .column(property(EnrollmentDirection.competitionGroup().id().fromAlias("dir")))
                .predicate(DQLPredicateType.distinct)
                .where(eq(
                        property(EnrollmentDirection.enrollmentCampaign().fromAlias("dir")),
                        value(enrollmentCampaign)
                ));

        if (formativeOrgUnit != null)
            directionsFilter.where(eq(
                    property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().fromAlias("dir")),
                    value(formativeOrgUnit)
            ));

        if (territorialOrgUnit != null)
            directionsFilter.where(eq(
                    property(EnrollmentDirection.educationOrgUnit().territorialOrgUnit().fromAlias("dir")),
                    value(territorialOrgUnit)
            ));

        if (qualifications != null)
            directionsFilter.where(in(
                    property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("dir")),
                    qualifications
            ));

        dql.where(and(
                eq(
                        property(CompetitionGroup.enrollmentCampaign().fromAlias(alias)),
                        value(enrollmentCampaign)),
                in(
                        property(CompetitionGroup.id().fromAlias(alias)),
                        directionsFilter.buildQuery())
        ));
    }
}