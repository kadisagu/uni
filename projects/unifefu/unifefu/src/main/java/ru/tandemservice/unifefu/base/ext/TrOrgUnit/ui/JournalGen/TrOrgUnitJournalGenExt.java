package ru.tandemservice.unifefu.base.ext.TrOrgUnit.ui.JournalGen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unifefu.base.ext.TrOrgUnit.ui.JournalAdd.TrOrgUnitJournalAddClickApply;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalGen.TrOrgUnitJournalGen;

/**
 * User: newdev
 */
@Configuration
public class TrOrgUnitJournalGenExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrOrgUnitJournalGenExtUI.class.getSimpleName();

    @Autowired
    private TrOrgUnitJournalGen _trOrgUnitJournalGen;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trOrgUnitJournalGen.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrOrgUnitJournalGenExtUI.class))
                .addAction(new TrOrgUnitJournalGenClickApply("onClickApply"))
                .create();
    }
}

