/* $Id: EppExtViewProvider4RegElement.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.report.externalView;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Victor Nekrasov
 * @since 23.04.2014
 */

public class EppExtViewProvider4RegElement extends SimpleDQLExternalViewConfig
{
	@Override
	protected DQLSelectBuilder buildDqlQuery()
	{
		DQLSelectBuilder dql = new DQLSelectBuilder()
				.fromEntity(EppRegistryElement.class, "regElement")
				.joinPath(DQLJoinType.left, EppRegistryElement.owner().fromAlias("regElement"), "owner")
				.joinPath(DQLJoinType.left, OrgUnit.parent().fromAlias("owner"), "parent")
				.joinPath(DQLJoinType.left, OrgUnit.parent().fromAlias("parent"), "nextParent")
				.joinEntity("regElement", DQLJoinType.left, EppRegistryElementLoad.class, "load",
				            eq(property("regElement"), property(EppRegistryElementLoad.registryElement().fromAlias("load"))));

		column(dql, property("regElement", EppRegistryElement.id()), "elementId").comment("ID реестровой записи");
		column(dql, property("regElement", EppRegistryElement.number()), "elementNumber").comment("Номер реестровой записи (из интерфейса)");
		column(dql, property("regElement", EppRegistryElement.state().id()), "stateId").comment("ID Состояния");
		column(dql, property("regElement", EppRegistryElement.title()), "title").comment("Наименование (из поля \"Название\" в интерфейсе)");
		column(dql, property("owner", OrgUnit.title()), "ownerTitle").comment("Наименование читающего подразделения");
		column(dql, property("parent", OrgUnit.title()), "parentTitle").comment("Наименование вышестоящего подразделения");
		column(dql, property("nextParent", OrgUnit.title()), "nextParentTitle").comment("Наименование следующего вышестоящего подразделения");

		column(dql, property(EppRegistryElement.parent().code().fromAlias("regElement")), "registryStructureCode").comment("код структуры реестра");
		column(dql, property(EppRegistryElement.parent().title().fromAlias("regElement")), "registryStructureTitle").comment("наименование структуры реестра");

		column(dql, property(EppRegistryElement.parts().fromAlias("regElement")), "parts").comment("Число частей");
		column(dql, property(EppRegistryElement.labor().fromAlias("regElement")), "labor").comment("Трудоемкость");
		column(dql, property(EppRegistryElement.size().fromAlias("regElement")), "elementSize").comment("Часов (всего*100)");

		column(dql, property(EppRegistryElementLoad.loadType().code().fromAlias("load")), "loadTypeCode").comment("код типа нагрузки");
		column(dql, property(EppRegistryElementLoad.loadType().title().fromAlias("load")), "loadTypeTitle").comment("наименование типа нагрузки");
		column(dql, property(EppRegistryElementLoad.load().fromAlias("load")), "loadValue").comment("часов нагрузки (*100)");

		return dql;
	}
}

