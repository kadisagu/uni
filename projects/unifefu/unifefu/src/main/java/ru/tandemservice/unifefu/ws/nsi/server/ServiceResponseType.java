
package ru.tandemservice.unifefu.ws.nsi.server;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="callCC" type="{http://www.croc.ru/Schemas/Dvfu/Nsi/Service}callCCType"/>
 *         &lt;element name="callRC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="routingHeader" type="{http://www.croc.ru/Schemas/Dvfu/Nsi/Service}RoutingHeaderType" minOccurs="0"/>
 *         &lt;element name="datagram" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;any/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceResponseType", propOrder = {
    "callCC",
    "callRC",
    "routingHeader",
    "datagram"
})
public class ServiceResponseType {

    @XmlElement(required = true)
    protected BigInteger callCC;
    protected String callRC;
    protected RoutingHeaderType routingHeader;
    protected ServiceResponseType.Datagram datagram;

    /**
     * Gets the value of the callCC property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCallCC() {
        return callCC;
    }

    /**
     * Sets the value of the callCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCallCC(BigInteger value) {
        this.callCC = value;
    }

    /**
     * Gets the value of the callRC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallRC() {
        return callRC;
    }

    /**
     * Sets the value of the callRC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallRC(String value) {
        this.callRC = value;
    }

    /**
     * Gets the value of the routingHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RoutingHeaderType }
     *     
     */
    public RoutingHeaderType getRoutingHeader() {
        return routingHeader;
    }

    /**
     * Sets the value of the routingHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoutingHeaderType }
     *     
     */
    public void setRoutingHeader(RoutingHeaderType value) {
        this.routingHeader = value;
    }

    /**
     * Gets the value of the datagram property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceResponseType.Datagram }
     *     
     */
    public ServiceResponseType.Datagram getDatagram() {
        return datagram;
    }

    /**
     * Sets the value of the datagram property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceResponseType.Datagram }
     *     
     */
    public void setDatagram(ServiceResponseType.Datagram value) {
        this.datagram = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;any/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "content"
    })
    public static class Datagram {

        @XmlMixed
        @XmlAnyElement(lax = true)
        protected List<Object> content;

        public Datagram()
        {
        }

        public Datagram(List<Object> content)
        {
            this.content = content;
        }

        /**
         * Gets the value of the content property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the content property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContent().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * {@link String }
         * 
         * 
         */
        public List<Object> getContent() {
            if (content == null) {
                content = new ArrayList<>();
            }
            return this.content;
        }

    }

}
