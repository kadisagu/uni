/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.TransferDataAboutTargetReception;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;

import java.util.Date;


/**
 * @author Zvereva Ekaterina
 * @since 22.08.2014
 */
public class FefuSystemActionTransferDataAboutTargetReceptionUI extends UIPresenter
{
    private EnrollmentCampaign _enrollmentCampaign;
    private StudentCustomStateCI _customStateCI;
    private Date _startDate;
    private StudentCustomState _newCustomState;

    @Override
    public void onComponentRefresh()
    {
        _customStateCI = IUniBaseDao.instance.get().getCatalogItem(StudentCustomStateCI.class, UniFefuDefines.TARGET_RECEPTION);
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public StudentCustomStateCI getCustomStateCI()
    {
        return _customStateCI;
    }

    public void setCustomStateCI(StudentCustomStateCI customState)
    {
        _customStateCI = customState;
    }

    public Date getStartDate()
    {
        return _startDate;
    }

    public void setStartDate(Date startDate)
    {
        _startDate = startDate;
    }

    public StudentCustomState getNewCustomState()
    {
        return _newCustomState;
    }

    public void setNewCustomState(StudentCustomState newCustomState)
    {
        _newCustomState = newCustomState;
    }

    public void onClickTransferDataStart()
    {
        FefuSystemActionManager.instance().dao().setCustomStateForStuTargetEnrollment(getEnrollmentCampaign(), getStartDate(), getCustomStateCI());
        deactivate();
    }

}