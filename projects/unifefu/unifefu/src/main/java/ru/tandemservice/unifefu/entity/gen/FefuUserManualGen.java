package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifefu.entity.FefuUserManual;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Руководство пользователя (инструкция ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuUserManualGen extends EntityBase
 implements INaturalIdentifiable<FefuUserManualGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuUserManual";
    public static final String ENTITY_NAME = "fefuUserManual";
    public static final int VERSION_HASH = 1127085109;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_FILE_NAME = "fileName";
    public static final String P_PATH = "path";
    public static final String P_DOCUMENT = "document";
    public static final String P_EDIT_DATE = "editDate";
    public static final String P_COMMENT = "comment";

    private String _code;     // Код
    private String _title;     // Название
    private String _fileName;     // Имя файла
    private String _path;     // Путь в classpath до документа по умолчанию
    private byte[] _document;     // Документ в zip сохраненный в базе
    private Date _editDate;     // Дата редактирования
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Имя файла. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFileName()
    {
        return _fileName;
    }

    /**
     * @param fileName Имя файла. Свойство не может быть null.
     */
    public void setFileName(String fileName)
    {
        dirty(_fileName, fileName);
        _fileName = fileName;
    }

    /**
     * @return Путь в classpath до документа по умолчанию.
     */
    @Length(max=255)
    public String getPath()
    {
        return _path;
    }

    /**
     * @param path Путь в classpath до документа по умолчанию.
     */
    public void setPath(String path)
    {
        dirty(_path, path);
        _path = path;
    }

    /**
     * @return Документ в zip сохраненный в базе.
     */
    public byte[] getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ в zip сохраненный в базе.
     */
    public void setDocument(byte[] document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Дата редактирования.
     */
    public Date getEditDate()
    {
        return _editDate;
    }

    /**
     * @param editDate Дата редактирования.
     */
    public void setEditDate(Date editDate)
    {
        dirty(_editDate, editDate);
        _editDate = editDate;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuUserManualGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((FefuUserManual)another).getCode());
            }
            setTitle(((FefuUserManual)another).getTitle());
            setFileName(((FefuUserManual)another).getFileName());
            setPath(((FefuUserManual)another).getPath());
            setDocument(((FefuUserManual)another).getDocument());
            setEditDate(((FefuUserManual)another).getEditDate());
            setComment(((FefuUserManual)another).getComment());
        }
    }

    public INaturalId<FefuUserManualGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<FefuUserManualGen>
    {
        private static final String PROXY_NAME = "FefuUserManualNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuUserManualGen.NaturalId) ) return false;

            FefuUserManualGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuUserManualGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuUserManual.class;
        }

        public T newInstance()
        {
            return (T) new FefuUserManual();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "fileName":
                    return obj.getFileName();
                case "path":
                    return obj.getPath();
                case "document":
                    return obj.getDocument();
                case "editDate":
                    return obj.getEditDate();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "fileName":
                    obj.setFileName((String) value);
                    return;
                case "path":
                    obj.setPath((String) value);
                    return;
                case "document":
                    obj.setDocument((byte[]) value);
                    return;
                case "editDate":
                    obj.setEditDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "fileName":
                        return true;
                case "path":
                        return true;
                case "document":
                        return true;
                case "editDate":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "fileName":
                    return true;
                case "path":
                    return true;
                case "document":
                    return true;
                case "editDate":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "fileName":
                    return String.class;
                case "path":
                    return String.class;
                case "document":
                    return byte[].class;
                case "editDate":
                    return Date.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuUserManual> _dslPath = new Path<FefuUserManual>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuUserManual");
    }
            

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Имя файла. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getFileName()
     */
    public static PropertyPath<String> fileName()
    {
        return _dslPath.fileName();
    }

    /**
     * @return Путь в classpath до документа по умолчанию.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getPath()
     */
    public static PropertyPath<String> path()
    {
        return _dslPath.path();
    }

    /**
     * @return Документ в zip сохраненный в базе.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getDocument()
     */
    public static PropertyPath<byte[]> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Дата редактирования.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getEditDate()
     */
    public static PropertyPath<Date> editDate()
    {
        return _dslPath.editDate();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends FefuUserManual> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<String> _fileName;
        private PropertyPath<String> _path;
        private PropertyPath<byte[]> _document;
        private PropertyPath<Date> _editDate;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(FefuUserManualGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FefuUserManualGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Имя файла. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getFileName()
     */
        public PropertyPath<String> fileName()
        {
            if(_fileName == null )
                _fileName = new PropertyPath<String>(FefuUserManualGen.P_FILE_NAME, this);
            return _fileName;
        }

    /**
     * @return Путь в classpath до документа по умолчанию.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getPath()
     */
        public PropertyPath<String> path()
        {
            if(_path == null )
                _path = new PropertyPath<String>(FefuUserManualGen.P_PATH, this);
            return _path;
        }

    /**
     * @return Документ в zip сохраненный в базе.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getDocument()
     */
        public PropertyPath<byte[]> document()
        {
            if(_document == null )
                _document = new PropertyPath<byte[]>(FefuUserManualGen.P_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Дата редактирования.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getEditDate()
     */
        public PropertyPath<Date> editDate()
        {
            if(_editDate == null )
                _editDate = new PropertyPath<Date>(FefuUserManualGen.P_EDIT_DATE, this);
            return _editDate;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unifefu.entity.FefuUserManual#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(FefuUserManualGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return FefuUserManual.class;
        }

        public String getEntityName()
        {
            return "fefuUserManual";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
