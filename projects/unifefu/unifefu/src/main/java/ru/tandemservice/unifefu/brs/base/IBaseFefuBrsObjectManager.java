/* $Id$ */
package ru.tandemservice.unifefu.brs.base;

/**
 * @author Nikolay Fedorovskih
 * @since 30.05.2014
 */
public interface IBaseFefuBrsObjectManager
{
    String getPermissionKey();
}