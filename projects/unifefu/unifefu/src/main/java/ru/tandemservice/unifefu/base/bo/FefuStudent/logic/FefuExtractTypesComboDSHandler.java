/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unifefu.base.vo.FefuEntrantOriginalsVerificationReportVO;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;


import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author nvankov
 * @since 7/17/13
 */
public class FefuExtractTypesComboDSHandler extends SimpleTitledComboDataSourceHandler
{

    public FefuExtractTypesComboDSHandler(String ownerId)
    {
        super(ownerId);
        _filtered = true;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long studentId = context.get("studentId");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuStudentPayment.class, "p");
        builder.column(property("p", FefuStudentPayment.extract().type()));
        builder.where(eq(property("p", FefuStudentPayment.extract().entity().id()), value(studentId)));

        setOrderByProperty(FefuStudentPayment.extract().type().title().s());
        setFilterByProperty(FefuStudentPayment.extract().type().title().s());
        context.put(UIDefines.COMBO_OBJECT_LIST, createStatement(builder).list());
        return super.execute(input, context);
    }
}
