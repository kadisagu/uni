/* $Id$ */
package ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.OuSettingsPub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.TrOrgUnitSettingsFefuExt;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuSettingsPub.TrBrsCoefficientOuSettingsPubUI;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;

/**
 * @author Nikolay Fedorovskih
 * @since 14.07.2014
 */
public class TrBrsCoefficientOuSettingsPubExtUI extends UIAddon
{
    private TrOrgUnitSettingsFefuExt _ouSettingsExt;

    public TrBrsCoefficientOuSettingsPubExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        TrBrsCoefficientOuSettingsPubUI presenter = getPresenter();
        TrOrgUnitSettings ouSettings = presenter.getOuSettings();
        if (ouSettings != null)
        {
            _ouSettingsExt = DataAccessServices.dao().getByNaturalId(
                    new TrOrgUnitSettingsFefuExt.NaturalId(presenter.getOuSettings())
            );
        }
    }

    public void onChangeYearPart()
    {
        TrBrsCoefficientOuSettingsPubUI presenter = getPresenter();
        presenter.onChangeYearPart();
        onComponentRefresh();
    }

    public boolean isBlockTutorEdit()
    {
        return _ouSettingsExt != null && _ouSettingsExt.isBlockTutorEdit();
    }
}