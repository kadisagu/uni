/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EppRegistry;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtensionBuilder;
import org.tandemframework.caf.ui.config.datasource.column.IHeaderColumnBuilder;
import org.tandemframework.caf.ui.config.datasource.column.TextDSColumn;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.DisciplineList.EppRegistryDisciplineList;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.base.ext.EppRegistry.logic.FefuRegistryBaseDSHandler;
import ru.tandemservice.unifefu.base.ext.EppRegistry.ui.Base.FefuRegistryListAddon;


import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Irina Ugfeld
 * @since 15.03.2016
 */
@Configuration
public class EppRegistryExtManager extends BusinessObjectExtensionManager {

    public static EppRegistryExtManager instance()
    {
        return instance(EppRegistryExtManager.class);
    }


    public IDefaultComboDataSourceHandler getEppRegistryStructureDSHandler(String eppRegistryStructureCode) {
        return new EntityComboDataSourceHandler(getName(), EppRegistryStructure.class)
            .customize((alias, dql, context, filter) -> {
                dql.joinPath(DQLJoinType.left, EppRegistryStructure.parent().fromAlias(alias), "p");
                dql.where(or(
                        eq(property(alias, EppRegistryStructure.code()), value(eppRegistryStructureCode)),
                        eq(property("p", EppRegistryStructure.code()), value(eppRegistryStructureCode))
                ));
                return dql;
            })
                .filter(EppRegistryStructure.title())
                .order(EppRegistryStructure.code());
    }

    @Bean
    public IDefaultComboDataSourceHandler getEppFControlActionTypeDSHandler() {
        return new EntityComboDataSourceHandler(this.getName(), EppFControlActionType.class)
                .order(EppFControlActionType.title())
                .filter(EppFControlActionType.title());
    }


    @Bean
    public IDefaultComboDataSourceHandler getSubjectIndexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
                .where(EduProgramSubjectIndex.programKind(), FefuRegistryListAddon.SETTING_EDU_PROGRAM_KIND)
                .order(EduProgramSubjectIndex.priority())
                .filter(EduProgramSubjectIndex.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler getEduOrgUnitDSHandler() {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
                .where(EduProgramSubject.subjectIndex(), FefuRegistryListAddon.SETTING_SUBJECT_INDEX)
                .where(EduProgramSubject.subjectIndex().programKind(), FefuRegistryListAddon.SETTING_EDU_PROGRAM_KIND)
                .filter(EduProgramSubject.subjectCode())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.subjectCode())
                .order(EduProgramSubject.title());
    }

    public ColumnListExtension getElementListDSColumnExtension(ColumnListExtPoint baseColumns) {
        IColumnListExtensionBuilder extPointBuilder = columnListExtensionBuilder(baseColumns);

        List<EppFControlActionType> fControlActionTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppFControlActionType.class);
        IHeaderColumnBuilder fControlHeadColumn = headerColumn("columnTitle.fControl");

        fControlActionTypes.forEach(c -> {
            TextDSColumn column = textColumn(c.getShortTitle(), "fefu." + c.getFullCode()).verticalHeader("true").create();
            column.setLabel(c.getShortTitle());
            fControlHeadColumn.addSubColumn(column);
        });

        return extPointBuilder
                .addAllBefore(EppRegistryDisciplineList.COLUMN_EDIT)
                .addColumn(fControlHeadColumn)
                .addColumn(textColumn("columnTitle.workPlanCount", FefuRegistryBaseDSHandler.FEFU_WORK_PLAN_COUNT).verticalHeader("true"))
                .addColumn(textColumn("columnTitle.eduPlanVersionCount", FefuRegistryBaseDSHandler.FEFU_WORK_PLAN_COUNT).verticalHeader("true"))
                .addColumn(textColumn("columnTitle.comment", EppRegistryElement.P_COMMENT))
                .create();
    }
}