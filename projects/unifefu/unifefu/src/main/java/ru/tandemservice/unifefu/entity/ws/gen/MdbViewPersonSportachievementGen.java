package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.PersonSportAchievement;
import ru.tandemservice.unifefu.entity.ws.MdbViewPerson;
import ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Спортивное достижение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewPersonSportachievementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement";
    public static final String ENTITY_NAME = "mdbViewPersonSportachievement";
    public static final int VERSION_HASH = -1882903271;
    private static IEntityMeta ENTITY_META;

    public static final String L_MDB_VIEW_PERSON = "mdbViewPerson";
    public static final String L_PERSON_SPORT_ACHIEVEMENT = "personSportAchievement";
    public static final String P_SPORT_TYPE = "sportType";
    public static final String P_SPORT_RANK = "sportRank";
    public static final String P_BESTACHIEVEMENT = "bestachievement";

    private MdbViewPerson _mdbViewPerson;     // Персона
    private PersonSportAchievement _personSportAchievement;     // Спортивное достижение
    private String _sportType;     // Вид спорта
    private String _sportRank;     // Спортивные разряды и звания
    private String _bestachievement;     // Лучшее достижение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона. Свойство не может быть null.
     */
    @NotNull
    public MdbViewPerson getMdbViewPerson()
    {
        return _mdbViewPerson;
    }

    /**
     * @param mdbViewPerson Персона. Свойство не может быть null.
     */
    public void setMdbViewPerson(MdbViewPerson mdbViewPerson)
    {
        dirty(_mdbViewPerson, mdbViewPerson);
        _mdbViewPerson = mdbViewPerson;
    }

    /**
     * @return Спортивное достижение. Свойство не может быть null.
     */
    @NotNull
    public PersonSportAchievement getPersonSportAchievement()
    {
        return _personSportAchievement;
    }

    /**
     * @param personSportAchievement Спортивное достижение. Свойство не может быть null.
     */
    public void setPersonSportAchievement(PersonSportAchievement personSportAchievement)
    {
        dirty(_personSportAchievement, personSportAchievement);
        _personSportAchievement = personSportAchievement;
    }

    /**
     * @return Вид спорта. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSportType()
    {
        return _sportType;
    }

    /**
     * @param sportType Вид спорта. Свойство не может быть null.
     */
    public void setSportType(String sportType)
    {
        dirty(_sportType, sportType);
        _sportType = sportType;
    }

    /**
     * @return Спортивные разряды и звания. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSportRank()
    {
        return _sportRank;
    }

    /**
     * @param sportRank Спортивные разряды и звания. Свойство не может быть null.
     */
    public void setSportRank(String sportRank)
    {
        dirty(_sportRank, sportRank);
        _sportRank = sportRank;
    }

    /**
     * @return Лучшее достижение.
     */
    @Length(max=255)
    public String getBestachievement()
    {
        return _bestachievement;
    }

    /**
     * @param bestachievement Лучшее достижение.
     */
    public void setBestachievement(String bestachievement)
    {
        dirty(_bestachievement, bestachievement);
        _bestachievement = bestachievement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewPersonSportachievementGen)
        {
            setMdbViewPerson(((MdbViewPersonSportachievement)another).getMdbViewPerson());
            setPersonSportAchievement(((MdbViewPersonSportachievement)another).getPersonSportAchievement());
            setSportType(((MdbViewPersonSportachievement)another).getSportType());
            setSportRank(((MdbViewPersonSportachievement)another).getSportRank());
            setBestachievement(((MdbViewPersonSportachievement)another).getBestachievement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewPersonSportachievementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewPersonSportachievement.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewPersonSportachievement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mdbViewPerson":
                    return obj.getMdbViewPerson();
                case "personSportAchievement":
                    return obj.getPersonSportAchievement();
                case "sportType":
                    return obj.getSportType();
                case "sportRank":
                    return obj.getSportRank();
                case "bestachievement":
                    return obj.getBestachievement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mdbViewPerson":
                    obj.setMdbViewPerson((MdbViewPerson) value);
                    return;
                case "personSportAchievement":
                    obj.setPersonSportAchievement((PersonSportAchievement) value);
                    return;
                case "sportType":
                    obj.setSportType((String) value);
                    return;
                case "sportRank":
                    obj.setSportRank((String) value);
                    return;
                case "bestachievement":
                    obj.setBestachievement((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mdbViewPerson":
                        return true;
                case "personSportAchievement":
                        return true;
                case "sportType":
                        return true;
                case "sportRank":
                        return true;
                case "bestachievement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mdbViewPerson":
                    return true;
                case "personSportAchievement":
                    return true;
                case "sportType":
                    return true;
                case "sportRank":
                    return true;
                case "bestachievement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mdbViewPerson":
                    return MdbViewPerson.class;
                case "personSportAchievement":
                    return PersonSportAchievement.class;
                case "sportType":
                    return String.class;
                case "sportRank":
                    return String.class;
                case "bestachievement":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewPersonSportachievement> _dslPath = new Path<MdbViewPersonSportachievement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewPersonSportachievement");
    }
            

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement#getMdbViewPerson()
     */
    public static MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
    {
        return _dslPath.mdbViewPerson();
    }

    /**
     * @return Спортивное достижение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement#getPersonSportAchievement()
     */
    public static PersonSportAchievement.Path<PersonSportAchievement> personSportAchievement()
    {
        return _dslPath.personSportAchievement();
    }

    /**
     * @return Вид спорта. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement#getSportType()
     */
    public static PropertyPath<String> sportType()
    {
        return _dslPath.sportType();
    }

    /**
     * @return Спортивные разряды и звания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement#getSportRank()
     */
    public static PropertyPath<String> sportRank()
    {
        return _dslPath.sportRank();
    }

    /**
     * @return Лучшее достижение.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement#getBestachievement()
     */
    public static PropertyPath<String> bestachievement()
    {
        return _dslPath.bestachievement();
    }

    public static class Path<E extends MdbViewPersonSportachievement> extends EntityPath<E>
    {
        private MdbViewPerson.Path<MdbViewPerson> _mdbViewPerson;
        private PersonSportAchievement.Path<PersonSportAchievement> _personSportAchievement;
        private PropertyPath<String> _sportType;
        private PropertyPath<String> _sportRank;
        private PropertyPath<String> _bestachievement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement#getMdbViewPerson()
     */
        public MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
        {
            if(_mdbViewPerson == null )
                _mdbViewPerson = new MdbViewPerson.Path<MdbViewPerson>(L_MDB_VIEW_PERSON, this);
            return _mdbViewPerson;
        }

    /**
     * @return Спортивное достижение. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement#getPersonSportAchievement()
     */
        public PersonSportAchievement.Path<PersonSportAchievement> personSportAchievement()
        {
            if(_personSportAchievement == null )
                _personSportAchievement = new PersonSportAchievement.Path<PersonSportAchievement>(L_PERSON_SPORT_ACHIEVEMENT, this);
            return _personSportAchievement;
        }

    /**
     * @return Вид спорта. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement#getSportType()
     */
        public PropertyPath<String> sportType()
        {
            if(_sportType == null )
                _sportType = new PropertyPath<String>(MdbViewPersonSportachievementGen.P_SPORT_TYPE, this);
            return _sportType;
        }

    /**
     * @return Спортивные разряды и звания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement#getSportRank()
     */
        public PropertyPath<String> sportRank()
        {
            if(_sportRank == null )
                _sportRank = new PropertyPath<String>(MdbViewPersonSportachievementGen.P_SPORT_RANK, this);
            return _sportRank;
        }

    /**
     * @return Лучшее достижение.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonSportachievement#getBestachievement()
     */
        public PropertyPath<String> bestachievement()
        {
            if(_bestachievement == null )
                _bestachievement = new PropertyPath<String>(MdbViewPersonSportachievementGen.P_BESTACHIEVEMENT, this);
            return _bestachievement;
        }

        public Class getEntityClass()
        {
            return MdbViewPersonSportachievement.class;
        }

        public String getEntityName()
        {
            return "mdbViewPersonSportachievement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
