package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x8_11to12 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuAdditionalProfessionalEducationProgramForStudent

        // сущность была удалена
        if (tool.tableExists("fefu_ape_program_for_student_t"))
        {
            // удалить таблицу
            tool.dropTable("fefu_ape_program_for_student_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("fefuAdditionalProfessionalEducationProgramForStudent");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuAdditionalProfessionalEducationProgram

        // сущность была удалена
        if (tool.tableExists("fefu_ape_program_t"))
        {

            // удалить таблицу
            tool.dropTable("fefu_ape_program_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("fefuAdditionalProfessionalEducationProgram");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuAdditionalProfessionalEducationProgramForStudentIndividualParameters

        // сущность была удалена
        if (tool.tableExists("fefu_ape_program_ind_params_t"))
        {
            // удалить таблицу
            tool.dropTable("fefu_ape_program_ind_params_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("fefuAdditionalProfessionalEducationProgramForStudentIndividualParameters");
        }

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuProgramStatusApe

		// сущность была удалена
        if (tool.tableExists("fefu_c_program_status_ape_t"))
		{
					// удалить таблицу
			tool.dropTable("fefu_c_program_status_ape_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuProgramStatusApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuProgramPublicationStatusApe

		// сущность была удалена
        if (tool.tableExists("fefu_c_pr_public_status_ape_t"))
		{
			// удалить таблицу
			tool.dropTable("fefu_c_pr_public_status_ape_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuProgramPublicationStatusApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuIssuedDocumentApe

		// сущность была удалена
        if (tool.tableExists("fefu_c_issued_document_ape_t"))
		{

			// удалить таблицу
			tool.dropTable("fefu_c_issued_document_ape_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuIssuedDocumentApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuIndividualDevelopTechApe

		// сущность была удалена
        if (tool.tableExists("fefu_c_ind_develop_tech_ape_t"))
		{

			// удалить таблицу
			tool.dropTable("fefu_c_ind_develop_tech_ape_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuIndividualDevelopTechApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuIndividualDevelopFormApe

		// сущность была удалена
        if (tool.tableExists("fefu_c_ind_develop_form_ape_t"))
		{

			// удалить таблицу
			tool.dropTable("fefu_c_ind_develop_form_ape_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuIndividualDevelopFormApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuIndividualDevelopConditionApe

		// сущность была удалена
        if (tool.tableExists("fefu_c_ind_develop_cond_ape_t"))
		{

			// удалить таблицу
			tool.dropTable("fefu_c_ind_develop_cond_ape_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuIndividualDevelopConditionApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDiplomaTypeApe

		// сущность была удалена
        if (tool.tableExists("fefu_c_diploma_type_ape_t"))
		{

			// удалить таблицу
			tool.dropTable("fefu_c_diploma_type_ape_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuDiplomaTypeApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDevelopTechApe

		// сущность была удалена
        if (tool.tableExists("fefu_c_develop_tech_ape_t"))
		{

			// удалить таблицу
			tool.dropTable("fefu_c_develop_tech_ape_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuDevelopTechApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDevelopPeriodApe

		// сущность была удалена
        if (tool.tableExists("fefu_c_develop_period_ape_t"))
		{

			// удалить таблицу
			tool.dropTable("fefu_c_develop_period_ape_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuDevelopPeriodApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDevelopFormApe

		// сущность была удалена
        if (tool.tableExists("fefu_c_develop_form_ape_t"))
		{

			// удалить таблицу
			tool.dropTable("fefu_c_develop_form_ape_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuDevelopFormApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDevelopConditionApe

		// сущность была удалена
        if (tool.tableExists("fefu_c_develop_condition_ape_t"))
		{

			// удалить таблицу
			tool.dropTable("fefu_c_develop_condition_ape_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuDevelopConditionApe");

		}
    }
}