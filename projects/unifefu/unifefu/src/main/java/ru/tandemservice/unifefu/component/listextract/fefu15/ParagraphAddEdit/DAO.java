/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu15.ParagraphAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.FefuExtractPrintFormManager;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuEnrollStuDPOListExtract, Model> implements IDAO
{
    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();
        builder.add(MQExpression.or(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().formativeOrgUnit(), orgUnit), MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().territorialOrgUnit(), orgUnit)));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().additional(), true));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.studentCategory().code(), StudentCategoryCodes.STUDENT_CATEGORY_DPP));
    }



    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        super.prepare(model);
        model.setStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
        model.setDpoProgramModel(new BaseSingleSelectModel()
        {

            @Override
            public ListResult<FefuAdditionalProfessionalEducationProgram> findValues(String filter)
            {
                DQLSelectBuilder builder = createSelectBuilder(filter, null);

                List<FefuAdditionalProfessionalEducationProgram> list = builder.createStatement(getSession()).list();
                return new ListResult<>(list, list.size());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return get(FefuAdditionalProfessionalEducationProgram.class, (Long)primaryKey);
            }

            protected DQLSelectBuilder createSelectBuilder(String filter, Object o)
            {
                OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(FefuAdditionalProfessionalEducationProgram.class, "ape")
                        .predicate(DQLPredicateType.distinct)
                        .where(or(eq(property("ape", FefuAdditionalProfessionalEducationProgram.formativeOrgUnit()), value(orgUnit)),
                                  eq(property("ape", FefuAdditionalProfessionalEducationProgram.territorialOrgUnit()), value(orgUnit))))
                        .column(property("ape"));


                if (o != null)
                {
                    if (o instanceof Long)
                        builder.where(eq(property("ape", FefuAdditionalProfessionalEducationProgram.id()), commonValue(o, PropertyType.LONG)));
                    else if (o instanceof Collection)
                        builder.where(in(property("ape", FefuAdditionalProfessionalEducationProgram.id()), (Collection) o));
                }

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("ape", FefuAdditionalProfessionalEducationProgram.title()), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property("ape", FefuAdditionalProfessionalEducationProgram.title()), OrderDirection.asc);

                return builder;
            }


        });
        final FefuEnrollStuDPOListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            model.setDpoProgramNew(firstExtract.getDpoProgramNew());
            model.setEnrollDate(firstExtract.getEnrollDate());
        }
        model.setCanAddTemplate(!model.isEditForm() && model.isParagraphOnlyOneInTheOrder());
    }


    @Override
    protected FefuEnrollStuDPOListExtract createNewInstance(Model model)
    {
        return new FefuEnrollStuDPOListExtract();
    }

    @Override
    protected void fillExtract(FefuEnrollStuDPOListExtract extract, Student student, Model model)
    {
        extract.setDpoProgramNew(model.getDpoProgramNew());
        extract.setEnrollDate(model.getEnrollDate());
        extract.setStudentStatusOld(student.getStatus());

        FefuAdditionalProfessionalEducationProgramForStudent studProgram = get(FefuAdditionalProfessionalEducationProgramForStudent.class,
                                                          FefuAdditionalProfessionalEducationProgramForStudent.student(), student);
        if (studProgram != null)
            extract.setDpoProgramOld(studProgram.getProgram());

    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        AbstractStudentOrder order = model.getParagraph().getOrder();
        FefuExtractPrintFormManager.instance().dao().saveOrUpdatePrintForm(order, model.getUploadFileOrder());

    }

    @Override
    protected void appendStudentsWithGroup(MQBuilder builder)
    {
        /* Нужны студенты и без группы*/
    }
}