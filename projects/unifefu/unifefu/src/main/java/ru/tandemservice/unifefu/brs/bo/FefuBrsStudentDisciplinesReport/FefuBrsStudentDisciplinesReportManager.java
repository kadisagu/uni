/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.brs.base.IBaseFefuBrsObjectManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.logic.FefuBrsStudentDisciplinesReportDAO;
import ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.logic.IFefuBrsStudentDisciplinesReportDAO;

/**
 * @author nvankov
 * @since 12/19/13
 */
@Configuration
public class FefuBrsStudentDisciplinesReportManager extends BusinessObjectManager implements IBaseFefuBrsObjectManager
{
    @Override
    public String getPermissionKey()
    {
        return "fefuBrsStudentDisciplinesReportPermissionKey";
    }

    public static FefuBrsStudentDisciplinesReportManager instance()
    {
        return instance(FefuBrsStudentDisciplinesReportManager.class);
    }

    @Bean
    public IFefuBrsStudentDisciplinesReportDAO dao()
    {
        return new FefuBrsStudentDisciplinesReportDAO();
    }
}



    