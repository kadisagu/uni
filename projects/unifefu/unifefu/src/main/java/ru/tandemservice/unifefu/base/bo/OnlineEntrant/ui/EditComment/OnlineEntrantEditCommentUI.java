/* $Id$ */
package ru.tandemservice.unifefu.base.bo.OnlineEntrant.ui.EditComment;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt;

/**
 * @author Ekaterina Zvereva
 * @since 01.06.2015
 */
@Input({
        @Bind(key="entrantId", binding = "entrantId", required = true)
        })
public class OnlineEntrantEditCommentUI extends UIPresenter
{
    private Long _entrantId;
    private OnlineEntrantFefuExt _entrantExt;

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }

    @Override
    public void onComponentRefresh()
    {
        _entrantExt = DataAccessServices.dao().get(OnlineEntrantFefuExt.class, OnlineEntrantFefuExt.onlineEntrant().id(), _entrantId);

    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_entrantExt);
        deactivate();
    }
}