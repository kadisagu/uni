/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu9;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class FefuSocGrantResumptionStuExtractDao extends UniBaseDao implements IExtractComponentDao<FefuSocGrantResumptionStuExtract>
{
    public void doCommit(FefuSocGrantResumptionStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        FefuStudentPayment payment = new FefuStudentPayment();
        payment.setExtract(extract);
        payment.setContinueOrder(true);
        payment.setStartDate(extract.getPayResumeDate());
        payment.setInfluencedExtract(extract.getSocGrantExtract());
        payment.setInfluencedOrderNum(extract.getSocGrantOrder());
        payment.setInfluencedOrderDate(extract.getSocGrantOrderDate());
        if(!StringUtils.isEmpty(extract.getSocGrantOrder()) && null != extract.getSocGrantOrderDate())
            payment.setComment("Возобновление выплаты социальной стипендии по приказу №" + extract.getSocGrantOrder() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getSocGrantOrderDate()));
        if(null != extract.getReason())
            payment.setReason(extract.getReason().getTitle());
        save(payment);
    }

    public void doRollback(FefuSocGrantResumptionStuExtract extract, Map parameters)
    {

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuStudentPayment.class);
        deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(FefuStudentPayment.extract()), DQLExpressions.value(extract)));
        deleteBuilder.createStatement(getSession()).execute();

    }
}