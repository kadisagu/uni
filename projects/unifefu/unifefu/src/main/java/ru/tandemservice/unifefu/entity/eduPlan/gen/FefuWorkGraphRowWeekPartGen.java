package ru.tandemservice.unifefu.entity.eduPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRowWeek;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRowWeekPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ГУП (Часть недели строки курса)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuWorkGraphRowWeekPartGen extends EntityBase
 implements INaturalIdentifiable<FefuWorkGraphRowWeekPartGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRowWeekPart";
    public static final String ENTITY_NAME = "fefuWorkGraphRowWeekPart";
    public static final int VERSION_HASH = -502904479;
    private static IEntityMeta ENTITY_META;

    public static final String L_WEEK = "week";
    public static final String P_PART = "part";
    public static final String L_TYPE = "type";

    private FefuWorkGraphRowWeek _week;     // Неделя строки курса
    private int _part;     // Номер части недели
    private EppWeekType _type;     // Тип недели

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Неделя строки курса. Свойство не может быть null.
     */
    @NotNull
    public FefuWorkGraphRowWeek getWeek()
    {
        return _week;
    }

    /**
     * @param week Неделя строки курса. Свойство не может быть null.
     */
    public void setWeek(FefuWorkGraphRowWeek week)
    {
        dirty(_week, week);
        _week = week;
    }

    /**
     * @return Номер части недели. Свойство не может быть null.
     */
    @NotNull
    public int getPart()
    {
        return _part;
    }

    /**
     * @param part Номер части недели. Свойство не может быть null.
     */
    public void setPart(int part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return Тип недели. Свойство не может быть null.
     */
    @NotNull
    public EppWeekType getType()
    {
        return _type;
    }

    /**
     * @param type Тип недели. Свойство не может быть null.
     */
    public void setType(EppWeekType type)
    {
        dirty(_type, type);
        _type = type;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuWorkGraphRowWeekPartGen)
        {
            if (withNaturalIdProperties)
            {
                setWeek(((FefuWorkGraphRowWeekPart)another).getWeek());
                setPart(((FefuWorkGraphRowWeekPart)another).getPart());
            }
            setType(((FefuWorkGraphRowWeekPart)another).getType());
        }
    }

    public INaturalId<FefuWorkGraphRowWeekPartGen> getNaturalId()
    {
        return new NaturalId(getWeek(), getPart());
    }

    public static class NaturalId extends NaturalIdBase<FefuWorkGraphRowWeekPartGen>
    {
        private static final String PROXY_NAME = "FefuWorkGraphRowWeekPartNaturalProxy";

        private Long _week;
        private int _part;

        public NaturalId()
        {}

        public NaturalId(FefuWorkGraphRowWeek week, int part)
        {
            _week = ((IEntity) week).getId();
            _part = part;
        }

        public Long getWeek()
        {
            return _week;
        }

        public void setWeek(Long week)
        {
            _week = week;
        }

        public int getPart()
        {
            return _part;
        }

        public void setPart(int part)
        {
            _part = part;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuWorkGraphRowWeekPartGen.NaturalId) ) return false;

            FefuWorkGraphRowWeekPartGen.NaturalId that = (NaturalId) o;

            if( !equals(getWeek(), that.getWeek()) ) return false;
            if( !equals(getPart(), that.getPart()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getWeek());
            result = hashCode(result, getPart());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getWeek());
            sb.append("/");
            sb.append(getPart());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuWorkGraphRowWeekPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuWorkGraphRowWeekPart.class;
        }

        public T newInstance()
        {
            return (T) new FefuWorkGraphRowWeekPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "week":
                    return obj.getWeek();
                case "part":
                    return obj.getPart();
                case "type":
                    return obj.getType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "week":
                    obj.setWeek((FefuWorkGraphRowWeek) value);
                    return;
                case "part":
                    obj.setPart((Integer) value);
                    return;
                case "type":
                    obj.setType((EppWeekType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "week":
                        return true;
                case "part":
                        return true;
                case "type":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "week":
                    return true;
                case "part":
                    return true;
                case "type":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "week":
                    return FefuWorkGraphRowWeek.class;
                case "part":
                    return Integer.class;
                case "type":
                    return EppWeekType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuWorkGraphRowWeekPart> _dslPath = new Path<FefuWorkGraphRowWeekPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuWorkGraphRowWeekPart");
    }
            

    /**
     * @return Неделя строки курса. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRowWeekPart#getWeek()
     */
    public static FefuWorkGraphRowWeek.Path<FefuWorkGraphRowWeek> week()
    {
        return _dslPath.week();
    }

    /**
     * @return Номер части недели. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRowWeekPart#getPart()
     */
    public static PropertyPath<Integer> part()
    {
        return _dslPath.part();
    }

    /**
     * @return Тип недели. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRowWeekPart#getType()
     */
    public static EppWeekType.Path<EppWeekType> type()
    {
        return _dslPath.type();
    }

    public static class Path<E extends FefuWorkGraphRowWeekPart> extends EntityPath<E>
    {
        private FefuWorkGraphRowWeek.Path<FefuWorkGraphRowWeek> _week;
        private PropertyPath<Integer> _part;
        private EppWeekType.Path<EppWeekType> _type;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Неделя строки курса. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRowWeekPart#getWeek()
     */
        public FefuWorkGraphRowWeek.Path<FefuWorkGraphRowWeek> week()
        {
            if(_week == null )
                _week = new FefuWorkGraphRowWeek.Path<FefuWorkGraphRowWeek>(L_WEEK, this);
            return _week;
        }

    /**
     * @return Номер части недели. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRowWeekPart#getPart()
     */
        public PropertyPath<Integer> part()
        {
            if(_part == null )
                _part = new PropertyPath<Integer>(FefuWorkGraphRowWeekPartGen.P_PART, this);
            return _part;
        }

    /**
     * @return Тип недели. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRowWeekPart#getType()
     */
        public EppWeekType.Path<EppWeekType> type()
        {
            if(_type == null )
                _type = new EppWeekType.Path<EppWeekType>(L_TYPE, this);
            return _type;
        }

        public Class getEntityClass()
        {
            return FefuWorkGraphRowWeekPart.class;
        }

        public String getEntityName()
        {
            return "fefuWorkGraphRowWeekPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
