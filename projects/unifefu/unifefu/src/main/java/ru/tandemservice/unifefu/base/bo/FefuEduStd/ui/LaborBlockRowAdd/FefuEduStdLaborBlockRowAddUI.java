/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.LaborBlockRowAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.FefuEduStdManager;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ParametersTab.FefuEduStdParametersTabUI;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 02.12.2014
 */
@Input({
               @Bind(key = FefuEduStdLaborBlockRowAddUI.EDU_STANDARD_ID, binding = "eduStandard.id", required = true),
               @Bind(key = FefuEduStdLaborBlockRowAddUI.QUALIFICATION_ID, binding = "qualification.id", required = true)
       })
public class FefuEduStdLaborBlockRowAddUI extends UIPresenter
{
    public static final String EDU_STANDARD_ID = "eduStandardId";
    public static final String QUALIFICATION_ID = "qualificationId";

    private EppStateEduStandard _eduStandard = new EppStateEduStandard();
    private EduProgramQualification _qualification = new EduProgramQualification();

    private FefuEppStateEduStandardLaborBlocks _block = new FefuEppStateEduStandardLaborBlocks();
    private List<HSelectOption> _parentList = Collections.emptyList();
    private List<HSelectOption> _valueList = Collections.emptyList();

    @Override
    public void onComponentRefresh()
    {
        _eduStandard = DataAccessServices.dao().getNotNull(_eduStandard.getId());
        _qualification = DataAccessServices.dao().getNotNull(_qualification.getId());

        List<FefuEppStateEduStandardLaborBlocks> blockList = FefuEduStdManager.instance().dao().getEduStandardLaborBlockRows(_eduStandard, _qualification);
        _parentList = HierarchyUtil.listHierarchyNodesWithParents(blockList, FefuEduStdParametersTabUI.LABOR_BLOCK_COMPARATOR, true);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppPlanStructure4SubjectIndex.class, "rel")
                .column(property("rel", EppPlanStructure4SubjectIndex.eppPlanStructure())).distinct()
                .where(eq(property("rel", EppPlanStructure4SubjectIndex.eduProgramSubjectIndex().id()), value(_eduStandard.getProgramSubject().getSubjectIndex().getId())));

        List<EppPlanStructure> structureList = DataAccessServices.dao().getList(builder);
        List<HSelectOption> valueList = HierarchyUtil.listHierarchyNodesWithParents(structureList, true);

        // убираем все, кроме листьев
        Set<Long> parentIds = new HashSet<>();
        for (HSelectOption value : valueList)
        {
            EppPlanStructure parent = ((EppPlanStructure) value.getObject()).getParent();
            if (null != parent) parentIds.add(parent.getId());
        }

        for (HSelectOption value : valueList)
        {
            EppPlanStructure planStructure = (EppPlanStructure) value.getObject();
            value.setCanBeSelected(!parentIds.contains(planStructure.getId()));
        }
        _valueList = valueList;
    }

    public void onClickApply()
    {
        _block.setEduStandard(_eduStandard);
        _block.setQualification(_qualification);

        DataAccessServices.dao().saveOrUpdate(_block);
        FefuEduStdManager.instance().dao().recalculateBlockMinAndMaxNumbers(_block);
        deactivate();
    }

    public EppStateEduStandard getEduStandard()
    {
        return _eduStandard;
    }

    public void setEduStandard(EppStateEduStandard eduStandard)
    {
        _eduStandard = eduStandard;
    }

    public EduProgramQualification getQualification()
    {
        return _qualification;
    }

    public void setQualification(EduProgramQualification qualification)
    {
        _qualification = qualification;
    }

    public FefuEppStateEduStandardLaborBlocks getBlock()
    {
        return _block;
    }

    public void setBlock(FefuEppStateEduStandardLaborBlocks block)
    {
        _block = block;
    }

    public List<HSelectOption> getParentList()
    {
        return _parentList;
    }

    public void setParentList(List<HSelectOption> parentList)
    {
        _parentList = parentList;
    }

    public List<HSelectOption> getValueList()
    {
        return _valueList;
    }

    public void setValueList(List<HSelectOption> valueList)
    {
        _valueList = valueList;
    }
}
