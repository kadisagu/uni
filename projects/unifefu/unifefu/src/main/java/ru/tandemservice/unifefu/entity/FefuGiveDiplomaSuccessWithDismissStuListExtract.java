package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.unifefu.entity.gen.FefuGiveDiplomaSuccessWithDismissStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «О присвоении квалификации, выдаче диплома с отличием и отчислении в связи с окончанием университета»
 */
public class FefuGiveDiplomaSuccessWithDismissStuListExtract extends FefuGiveDiplomaSuccessWithDismissStuListExtractGen implements IFefuGiveDiplomaWithDismissStuListExtract, IExcludeExtract
{
    @Override
    public Date getExcludeDate()
    {
        return getBeginDate();
    }
}