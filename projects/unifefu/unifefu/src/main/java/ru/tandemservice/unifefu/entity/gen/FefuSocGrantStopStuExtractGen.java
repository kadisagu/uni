package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О приостановлении выплаты социальной стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuSocGrantStopStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract";
    public static final String ENTITY_NAME = "fefuSocGrantStopStuExtract";
    public static final int VERSION_HASH = 1966922192;
    private static IEntityMeta ENTITY_META;

    public static final String L_SOC_GRANT_EXTRACT = "socGrantExtract";
    public static final String P_SOC_GRANT_ORDER = "socGrantOrder";
    public static final String P_SOC_GRANT_ORDER_DATE = "socGrantOrderDate";
    public static final String P_PAY_STOP_DATE = "payStopDate";

    private AbstractStudentExtract _socGrantExtract;     // Выписка о назначении социальной стипендии
    private String _socGrantOrder;     // Номер приказа о назначении социальной стипендии
    private Date _socGrantOrderDate;     // Дата приказа о назначении социальной стипендии
    private Date _payStopDate;     // Дата прекращения выплаты социальной стипендии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка о назначении социальной стипендии.
     */
    public AbstractStudentExtract getSocGrantExtract()
    {
        return _socGrantExtract;
    }

    /**
     * @param socGrantExtract Выписка о назначении социальной стипендии.
     */
    public void setSocGrantExtract(AbstractStudentExtract socGrantExtract)
    {
        dirty(_socGrantExtract, socGrantExtract);
        _socGrantExtract = socGrantExtract;
    }

    /**
     * @return Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSocGrantOrder()
    {
        return _socGrantOrder;
    }

    /**
     * @param socGrantOrder Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    public void setSocGrantOrder(String socGrantOrder)
    {
        dirty(_socGrantOrder, socGrantOrder);
        _socGrantOrder = socGrantOrder;
    }

    /**
     * @return Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getSocGrantOrderDate()
    {
        return _socGrantOrderDate;
    }

    /**
     * @param socGrantOrderDate Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     */
    public void setSocGrantOrderDate(Date socGrantOrderDate)
    {
        dirty(_socGrantOrderDate, socGrantOrderDate);
        _socGrantOrderDate = socGrantOrderDate;
    }

    /**
     * @return Дата прекращения выплаты социальной стипендии. Свойство не может быть null.
     */
    @NotNull
    public Date getPayStopDate()
    {
        return _payStopDate;
    }

    /**
     * @param payStopDate Дата прекращения выплаты социальной стипендии. Свойство не может быть null.
     */
    public void setPayStopDate(Date payStopDate)
    {
        dirty(_payStopDate, payStopDate);
        _payStopDate = payStopDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuSocGrantStopStuExtractGen)
        {
            setSocGrantExtract(((FefuSocGrantStopStuExtract)another).getSocGrantExtract());
            setSocGrantOrder(((FefuSocGrantStopStuExtract)another).getSocGrantOrder());
            setSocGrantOrderDate(((FefuSocGrantStopStuExtract)another).getSocGrantOrderDate());
            setPayStopDate(((FefuSocGrantStopStuExtract)another).getPayStopDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuSocGrantStopStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuSocGrantStopStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuSocGrantStopStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    return obj.getSocGrantExtract();
                case "socGrantOrder":
                    return obj.getSocGrantOrder();
                case "socGrantOrderDate":
                    return obj.getSocGrantOrderDate();
                case "payStopDate":
                    return obj.getPayStopDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    obj.setSocGrantExtract((AbstractStudentExtract) value);
                    return;
                case "socGrantOrder":
                    obj.setSocGrantOrder((String) value);
                    return;
                case "socGrantOrderDate":
                    obj.setSocGrantOrderDate((Date) value);
                    return;
                case "payStopDate":
                    obj.setPayStopDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                        return true;
                case "socGrantOrder":
                        return true;
                case "socGrantOrderDate":
                        return true;
                case "payStopDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    return true;
                case "socGrantOrder":
                    return true;
                case "socGrantOrderDate":
                    return true;
                case "payStopDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "socGrantExtract":
                    return AbstractStudentExtract.class;
                case "socGrantOrder":
                    return String.class;
                case "socGrantOrderDate":
                    return Date.class;
                case "payStopDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuSocGrantStopStuExtract> _dslPath = new Path<FefuSocGrantStopStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuSocGrantStopStuExtract");
    }
            

    /**
     * @return Выписка о назначении социальной стипендии.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract#getSocGrantExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> socGrantExtract()
    {
        return _dslPath.socGrantExtract();
    }

    /**
     * @return Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract#getSocGrantOrder()
     */
    public static PropertyPath<String> socGrantOrder()
    {
        return _dslPath.socGrantOrder();
    }

    /**
     * @return Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract#getSocGrantOrderDate()
     */
    public static PropertyPath<Date> socGrantOrderDate()
    {
        return _dslPath.socGrantOrderDate();
    }

    /**
     * @return Дата прекращения выплаты социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract#getPayStopDate()
     */
    public static PropertyPath<Date> payStopDate()
    {
        return _dslPath.payStopDate();
    }

    public static class Path<E extends FefuSocGrantStopStuExtract> extends ModularStudentExtract.Path<E>
    {
        private AbstractStudentExtract.Path<AbstractStudentExtract> _socGrantExtract;
        private PropertyPath<String> _socGrantOrder;
        private PropertyPath<Date> _socGrantOrderDate;
        private PropertyPath<Date> _payStopDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка о назначении социальной стипендии.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract#getSocGrantExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> socGrantExtract()
        {
            if(_socGrantExtract == null )
                _socGrantExtract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_SOC_GRANT_EXTRACT, this);
            return _socGrantExtract;
        }

    /**
     * @return Номер приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract#getSocGrantOrder()
     */
        public PropertyPath<String> socGrantOrder()
        {
            if(_socGrantOrder == null )
                _socGrantOrder = new PropertyPath<String>(FefuSocGrantStopStuExtractGen.P_SOC_GRANT_ORDER, this);
            return _socGrantOrder;
        }

    /**
     * @return Дата приказа о назначении социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract#getSocGrantOrderDate()
     */
        public PropertyPath<Date> socGrantOrderDate()
        {
            if(_socGrantOrderDate == null )
                _socGrantOrderDate = new PropertyPath<Date>(FefuSocGrantStopStuExtractGen.P_SOC_GRANT_ORDER_DATE, this);
            return _socGrantOrderDate;
        }

    /**
     * @return Дата прекращения выплаты социальной стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract#getPayStopDate()
     */
        public PropertyPath<Date> payStopDate()
        {
            if(_payStopDate == null )
                _payStopDate = new PropertyPath<Date>(FefuSocGrantStopStuExtractGen.P_PAY_STOP_DATE, this);
            return _payStopDate;
        }

        public Class getEntityClass()
        {
            return FefuSocGrantStopStuExtract.class;
        }

        public String getEntityName()
        {
            return "fefuSocGrantStopStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
