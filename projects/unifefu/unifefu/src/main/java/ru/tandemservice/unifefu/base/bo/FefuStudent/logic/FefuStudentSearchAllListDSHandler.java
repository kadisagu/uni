/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IEducationOrgUnitContextHandler;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author Alexey Lopatin
 * @since 03.11.2013
 */
public class FefuStudentSearchAllListDSHandler extends FefuStudentSearchListOrderNonStateFinishedDSHandler
{
    public FefuStudentSearchAllListDSHandler(String ownerId)
    {
        super(ownerId);
        _orderDescriptionRegistry.setOrders(Student.person(), new OrderDescription("idCard", new String[]{IdentityCard.P_FULL_FIO}));
    }

    public static void studentListFilter(DQLSelectBuilder builder, String alias, ExecutionContext context)
    {
        List<Long> studentIdList = context.get(FefuStudentManager.FEFU_STUDENT_FIO);
        DataWrapper archival = context.get(FefuStudentManager.FEFU_ARCHIVAL);

        if (null != studentIdList && !studentIdList.isEmpty())
        {
            builder.where(in(property(alias, Student.id()), studentIdList));
        }
        if (null != archival)
        {
             builder.where(eq(property(alias, Student.archival()), value(archival.getId().equals(FefuStudentManager.ARCHIVAL_YES))));
        }
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);
        studentListFilter(builder, alias, context);
    }

    @Override
    public IEducationOrgUnitContextHandler getContextHandler(ExecutionContext context)
    {
        return null;
    }

    @Override
    public void applyCustomStateFilter(List<StudentCustomStateCI> studentCustomStateCIList, DQLSelectBuilder builder, ExecutionContext context)
    {
        if (studentCustomStateCIList.isEmpty()) return;
        DataWrapper filter = context.get(FefuStudentManager.FEFU_TYPE_MATCH);
        if (filter == null || filter.getId().equals(FefuStudentManager.TYPE_MATCH_PARTIAL))
            super.applyCustomStateFilter(studentCustomStateCIList, builder, context);
        else applyFullCustomState(studentCustomStateCIList, builder);
    }


    /**
     * Полное совпадение доп.статусов со списком в фильтре
     * @param studentCustomStateCIList - список доп.статусов
     * @param builder
     */
    private void applyFullCustomState(List<StudentCustomStateCI> studentCustomStateCIList, DQLSelectBuilder builder)
    {
        Date currentDate = new Date();
        int size = studentCustomStateCIList.size();

        //Подзапрос для получения студентов, которые имеют какой-либо активный статус
        // не из списка фильтрации - нам они гарантированно не нужны
        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "cs")
                .column(property(StudentCustomState.student().fromAlias("cs")))
                .where(notIn(property(StudentCustomState.customState().fromAlias("cs")), studentCustomStateCIList))
                .where(or(
                        isNull(property("cs", StudentCustomState.P_END_DATE)),
                        ge(property("cs", StudentCustomState.P_END_DATE), value(CoreDateUtils.getDayFirstTimeMoment(currentDate), PropertyType.DATE))
                ))
                .where(or(
                        isNull(property("cs", StudentCustomState.P_BEGIN_DATE)),
                        lt(property("cs", StudentCustomState.P_BEGIN_DATE), value(CoreDateUtils.getNextDayFirstTimeMoment(currentDate, 1), PropertyType.DATE))
                ));

        // Студенты, имеющие активные статусы только из списка фильтрации,
        // при этом количество статусов совпадает с размером списка
        DQLSelectBuilder subBuilder2 = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "stu")
                .column(property(StudentCustomState.student().fromAlias("stu")))
                .where(notIn(property(StudentCustomState.student().fromAlias("stu")), subBuilder.buildQuery()))
                .where(or(
                        isNull(property("stu", StudentCustomState.P_END_DATE)),
                        ge(property("stu", StudentCustomState.P_END_DATE), value(CoreDateUtils.getDayFirstTimeMoment(currentDate), PropertyType.DATE))
                ))
                .where(or(
                        isNull(property("stu", StudentCustomState.P_BEGIN_DATE)),
                        lt(property("stu", StudentCustomState.P_BEGIN_DATE), value(CoreDateUtils.getNextDayFirstTimeMoment(currentDate, 1), PropertyType.DATE))
                ))
                .group(property(StudentCustomState.student().id().fromAlias("stu")))
                .having(eq(count(DQLPredicateType.distinct, property(StudentCustomState.customState().id().fromAlias("stu"))), value(size)));

        builder.where(in(property(Student.id().fromAlias("s")), subBuilder2.buildQuery()));

    }
}
