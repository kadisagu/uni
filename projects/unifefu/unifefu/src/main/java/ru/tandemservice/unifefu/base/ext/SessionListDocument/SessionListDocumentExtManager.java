package ru.tandemservice.unifefu.base.ext.SessionListDocument;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author avedernikov
 * @since 02.08.2016
 */
@Configuration
public class SessionListDocumentExtManager extends BusinessObjectExtensionManager
{
}
