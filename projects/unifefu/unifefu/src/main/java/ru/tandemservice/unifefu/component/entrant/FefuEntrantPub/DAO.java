/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.FefuEntrantPub;

import ru.tandemservice.uniec.component.entrant.EntrantPub.Model;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.OlympiadDiplomaFefuExt;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 03.06.2013
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantPub.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        UnifefuDaoFacade.getFefuEntrantDAO().checkAccessForTCEmployee(model.getEntrant());
    }

    @Override
    protected List<OlympiadDiploma> getDiplomaList(Entrant entrant)
    {
        // Патчим список олимпиад
        List<OlympiadDiploma> result = super.getDiplomaList(entrant);
        for (OlympiadDiploma diploma : result)
        {
            OlympiadDiplomaFefuExt ext = getByNaturalId(new OlympiadDiplomaFefuExt.NaturalId(diploma));
            // Надо выводить значение из справочника вместо названия, которое в базовом объекте хранится
            if (ext != null)
                diploma.setOlympiad(ext.getEntrantOlympiad().getTitle());
        }
        return result;
    }
}