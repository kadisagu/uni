/* $Id$ */
package ru.tandemservice.unifefu.base.ext.UniStudent;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.FefuEduProgramEducationOrgUnitAddon;

import java.util.Collection;
import java.util.HashSet;

/**
 * @author Nikolay Fedorovskih
 * @since 17.10.2013
 */
@Configuration
public class UniStudentExtManager extends BusinessObjectExtensionManager
{
    public static final String PROP_DEVELOP_FORM = "developForm";
    public static final String BASE_EDU_DS = "baseEduDS";
    public static final String PROP_BASE_EDU = "baseEdu";

    public static void configEduUtil(UniEduProgramEducationOrgUnitAddon util, String settingsKey)
    {
        util.configSettings(settingsKey);
        util.configUseFilters(FefuEduProgramEducationOrgUnitAddon.FEFU_FILTERS);

        Collection<UniEduProgramEducationOrgUnitAddon.Filters> multiselectFilters = new HashSet<>(FefuEduProgramEducationOrgUnitAddon.FEFU_FILTER_SET);
        multiselectFilters.remove(FefuEduProgramEducationOrgUnitAddon.FEFU_INCLUDE_SPECIALIZATIONS_AND_PROFILES);
        util.configMultiselectFilters(multiselectFilters.toArray(new UniEduProgramEducationOrgUnitAddon.Filters[multiselectFilters.size()]));
    }
}