/* $Id$ */
package ru.tandemservice.unifefu.ws.eventsAxis2;

import ru.tandemservice.unifefu.utils.fefuICal.FefuScheduleVEventICalXST;
import ru.tandemservice.unifefu.utils.fefuICal.FefuTeacherScheduleVEventICalXST;

import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 9/20/13
 */
public class FefuScheduleICalCommits
{

    private Map<String, FefuScheduleVEventICalXST> _vEventsMap;
    private List<FefuScheduleVEventICalXST> _commitedVEvents;
    private List<FefuScheduleVEventICalXST> _failedVEvents;
    private boolean _requestFailed = false;

    public Map<String, FefuScheduleVEventICalXST> getvEventsMap()
    {
        return _vEventsMap;
    }

    public void setvEventsMap(Map<String, FefuScheduleVEventICalXST> vEventsMap)
    {
        _vEventsMap = vEventsMap;
    }

    public List<FefuScheduleVEventICalXST> getCommitedVEvents()
    {
        return _commitedVEvents;
    }

    public void setCommitedVEvents(List<FefuScheduleVEventICalXST> commitedVEvents)
    {
        _commitedVEvents = commitedVEvents;
    }

    public List<FefuScheduleVEventICalXST> getFailedVEvents()
    {
        return _failedVEvents;
    }

    public void setFailedVEvents(List<FefuScheduleVEventICalXST> failedVEvents)
    {
        _failedVEvents = failedVEvents;
    }

    public boolean isRequestFailed()
    {
        return _requestFailed;
    }

    public void setRequestFailed(boolean requestFailed)
    {
        _requestFailed = requestFailed;
    }
}
