package ru.tandemservice.unifefu.dao.print;


import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.print.SessionBulletinPrintDAO;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;


/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 11.12.13
 * Time: 16:21
 * To change this template use File | Settings | File Templates.
 */
public class FefuSessionBulletinPrintDAO extends SessionBulletinPrintDAO
{
    @Override
    protected void printAdditionalInfo(final BulletinPrintInfo printInfo, MarkAmountData markAmountData)
    {
        final SessionBulletinDocument bulletin = printInfo.getBulletin();
        final RtfDocument document = printInfo.getDocument();
        final TreeSet<String> eduTitles = new TreeSet<>();
        final TreeSet<String> formTitles = new TreeSet<>();
        for (final EppStudentWpeCAction slot : printInfo.getContentMap().keySet())
        {
            final EppRealEduGroup4ActionTypeRow groupInfo = printInfo.getContentMap().get(slot).getEduGroupRow();
            if (null != groupInfo)
            {
                eduTitles.add(geDirectionWithParentStr(groupInfo.getStudentEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel()));
                formTitles.add(groupInfo.getStudentEducationOrgUnit().getDevelopForm().getTitle());
            }
        }
        final RtfInjectModifier modifier = new RtfInjectModifier();
        final EppRegistryElementPart registryElement = bulletin.getGroup().getActivityPart();
        modifier.put("eduOksoOrgUnitTitle", StringUtils.join(eduTitles.iterator(), ", "));
        modifier.put("eduForm", StringUtils.join(formTitles.iterator(), ", "));
        modifier.put("load", registryElement.getLaborAsDouble().toString());

        final EppControlActionType caType = bulletin.getGroup().getActionType();

        if (caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT))
        {
            modifier.put("controlForm", "Курсовой проект");
        } else if (caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK))
        {
            modifier.put("controlForm", "Курсовая работа");
        } else if (caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM) || caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM))
        {
            modifier.put("controlForm", "Экзамен");
        } else if (caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF) || caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF))
        {
            modifier.put("controlForm", "Зачет");
        } else
        {
            modifier.put("controlForm", caType.getTitle().toLowerCase());
        }

        modifier.modify(document);


        final List<SessionMarkGradeValueCatalogItem> markList = Lists.reverse(this.getGradeScaleMarkList(bulletin));

        final String[][] markTableData = new String[Math.max(3, 2 + markList.size())][2];
        markTableData[0][0] = "Число студентов на экзамене (зачете)";
        markTableData[0][1] = "";


        int row = 1;
        for (final SessionMarkGradeValueCatalogItem mark : markList)
        {
            if (row == 1)
                markTableData[row][0] = "Из них\t\tполучивших «" + mark.getTitle().toLowerCase() + "»";
            else
                markTableData[row][0] = "\t\tполучивших «" + mark.getTitle().toLowerCase() + "»";
            markTableData[row][1] = "";
            row++;
        }

        markTableData[row][0] = "Число студентов, не явившихся на экзамен (зачет)";
        markTableData[row][1] = "";

        boolean doPrintMarkAmounts = markAmountData.getTotalMarkAmount() != 0;
        if (doPrintMarkAmounts)
        {

            markTableData[0][1] = String.valueOf(markAmountData.getTotalGradeAmount());


            row = 1;
            for (final SessionMarkGradeValueCatalogItem mark : markList)
            {
                Integer count = markAmountData.getMarkAmountMap().get(mark);
                markTableData[row][1] = count == null ? "0" : String.valueOf(count);
                row++;
            }
            markTableData[row][1] = String.valueOf(markAmountData.getNotAppearCount());
        }

        new RtfTableModifier().put("TM", markTableData).modify(document);
    }

    @Override
    protected List<ColumnType> prepareColumnList(final BulletinPrintInfo printInfo)
    {
        if (printInfo.isUseCurrentRating() || printInfo.isUsePoints())
            return super.prepareColumnList(printInfo);
        return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.MARK,
                ColumnType.DATE,
                ColumnType.EMPTY);
    }

    @Override
    protected List<ColumnType> getTemplateColumnList(final BulletinPrintInfo printInfo)
    {
        if (printInfo.isUseCurrentRating() || printInfo.isUsePoints())
            return super.getTemplateColumnList(printInfo);
        return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.GROUP,
                ColumnType.MARK,
                ColumnType.DATE,
                ColumnType.EMPTY);
    }

    private String geDirectionWithParentStr(EducationLevels educationLevel)
    {
        final StructureEducationLevels levelType = educationLevel.getLevelType();
        final String title = " «" + educationLevel.getTitle() + "»";
        final String direction = " " + CommonExtractPrint.getFefuHighEduLevelStr(educationLevel);
        String educationStrDirection;

        if (levelType.isSpecialization()) // специализация
        {
            educationStrDirection = UniRtfUtil.SPECIALITY_CASES[0] + direction + " " + CommonExtractPrint.SPECIALIZATION_CASES[0] + title;
        } else if (levelType.isSpecialty()) // специальность
        {
            educationStrDirection = UniRtfUtil.SPECIALITY_CASES[0] + direction;
        } else if (levelType.isProfile() && levelType.isBachelor()) // бакалаврский профиль
        {
            educationStrDirection = CommonExtractPrint.DIRECTION_CASES[0] + direction + " " + CommonExtractPrint.PROFILE_CASES[0] + title;
        } else if (levelType.isProfile() && levelType.isMaster()) // магистерский профиль
        {
            educationStrDirection = CommonExtractPrint.DIRECTION_CASES[0] + direction + " " + CommonExtractPrint.MASTER_PROGRAM_CASES[0] + title;
        } else // направление подготовки
        {
            educationStrDirection = UniRtfUtil.EDU_DIRECTION_CASES[0] + direction;
        }
        return educationStrDirection;
    }

    @Override
    protected String[] fillTableRow(BulletinPrintRow printRow, List<ColumnType> columns, int rowNumber)
    {
        final String[] strings = super.fillTableRow(printRow, columns, rowNumber);
        {
            final SessionDocument document = printRow.getSlot().getDocument();
            if (document instanceof SessionBulletinDocument)
            {
                final SessionBulletinDocument bulletinDocument = (SessionBulletinDocument) document;
                final SessionObject session = bulletinDocument.getSessionObject();
                final Student student = printRow.getSlot().getActualStudent();

                final SessionStudentNotAllowedForBulletin notAllowedForBulletin = getByNaturalId(new SessionStudentNotAllowedForBulletin.NaturalId(bulletinDocument, student));
                final SessionStudentNotAllowed notAllowedForSession = getByNaturalId(new SessionStudentNotAllowed.NaturalId(session, student));
                if (isNotAllowed(notAllowedForBulletin) || isNotAllowed(notAllowedForSession))
                {
                    final int i = columns.indexOf(ColumnType.MARK);
                    if (i >= 0)
                        strings[i] = "недоп.";
                }
            }
        }
        return strings;
    }

    private boolean isNotAllowed(@Nullable ISessionStudentNotAllowed notAllowedDoc)
    {
        return notAllowedDoc != null && notAllowedDoc.getRemovalDate() == null;
    }
}
