/* $Id: FefuRegistryModuleLoadEditUI.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.ModuleLoadEdit;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.FefuRegistryManager;
import ru.tandemservice.unifefu.base.vo.FefuLoadVO;

import ru.tandemservice.unifefu.entity.catalog.codes.FefuLoadTypeCodes;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEppRegistryModuleALoadExt;


import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 05.02.14
 * Time: 12:44
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "moduleId", required = true),
        @Bind(key = FefuRegistryModuleLoadEditUI.PART_MODULE_ID, binding = FefuRegistryModuleLoadEditUI.PART_MODULE_ID)
})
public class FefuRegistryModuleLoadEditUI extends UIPresenter
{
    public static final String PART_MODULE_ID = "partModuleId";

    private Long _moduleId;
    private Long _partModuleId;
    private EppRegistryModule _module;
    private EppRegistryElementPartModule _partModule;
    private List<FefuLoadVO> _fefuLoadVOs;
    private FefuLoadVO _currentFefuLoadVO;

    private EppRegistryElementPartFControlAction _currentEppControlAction;
    private Map<EppFControlActionType, EppRegistryElementPartFControlAction> _selection = Collections.emptyMap();

    @Override
    public void onComponentRender()
    {
        _uiSupport.setPageTitle("Распределение доп. нагрузки учебного модуля «" + _module.getTitle() + "»");

    }

    @Override
    public void onComponentRefresh()
    {
        _module = DataAccessServices.dao().getNotNull(_moduleId);
        if (_partModuleId != null)
            _partModule = DataAccessServices.dao().get(_partModuleId);
        _fefuLoadVOs = FefuRegistryManager.instance().dao().moduleLoadList(_moduleId, true);
        if (null != getPart())
        {
            setSelection(IEppRegistryDAO.instance.get().getRegistryElementPartControlActions(getPart()));
        }
    }

    public void onClickApply()
    {
        List<FefuLoadVO> modifiedFefuLoadVOs = Lists.newArrayList();

        Map<String, String> loadTypesMap = Maps.newHashMap();

        loadTypesMap.put(FefuLoadTypeCodes.TYPE_LECTURES_INTER, EppALoadTypeCodes.TYPE_LECTURES);
        loadTypesMap.put(FefuLoadTypeCodes.TYPE_PRACTICE_INTER, EppALoadTypeCodes.TYPE_PRACTICE);
        loadTypesMap.put(FefuLoadTypeCodes.TYPE_LABS_INTER, EppALoadTypeCodes.TYPE_LABS);

        Map<String, FefuLoadVO> aLoadMap = Maps.newHashMap();
        for(FefuLoadVO fefuLoadVO : _fefuLoadVOs)
        {
            if(null != fefuLoadVO.getALoad() && null != fefuLoadVO.getALoad().getLoadAsDouble())
                aLoadMap.put(fefuLoadVO.getALoad().getLoadType().getCode(), fefuLoadVO);
        }

        for(FefuLoadVO fefuLoadVO : _fefuLoadVOs)
        {
            if (null != fefuLoadVO.getFefuLoad() && !FefuLoadTypeCodes.TYPE_EXAM_HOURS.equals(fefuLoadVO.getFefuLoad().getLoadType().getCode()))
            {
                FefuEppRegistryModuleALoadExt fefuLoad = fefuLoadVO.getFefuLoad();
                FefuLoadVO aLoad = aLoadMap.get(loadTypesMap.get(fefuLoad.getLoadType().getCode()));
                if (null == aLoad.getALoad())
                {
                    if (fefuLoad.getLoad() > 0)
                        _uiSupport.error("Нагрузка «" + fefuLoad.getLoadType().getTitle() + "» не может быть больше нуля", fefuLoadVO.getId().toString(), aLoad.getId().toString());
                }
                else
                {
                    if (fefuLoad.getLoad() > aLoad.getALoad().getLoad())
                        _uiSupport.error("Нагрузка «" + fefuLoad.getLoadType().getTitle() + "» не может быть больше нагрузки «" + aLoad.getALoad().getLoadType().getTitle() + "»", fefuLoadVO.getId().toString(), aLoad.getId().toString());
                }
            }
            modifiedFefuLoadVOs.add(fefuLoadVO);
        }
        if(getUserContext().getErrorCollector().hasErrors()) return;

        FefuRegistryManager.instance().dao().createOrUpdateLoadList(modifiedFefuLoadVOs);
        if (null != getPart())
        {
            FefuRegistryManager.instance().dao().correctionSizeAndLabor(getPart().getRegistryElement());
            IEppRegistryDAO.instance.get().doSaveRegistryElementPartControlActions(getPart(), getEppControlActionList());
        }
        deactivate();
    }

    public Long getModuleId()
    {
        return _moduleId;
    }

    public void setModuleId(Long moduleId)
    {
        _moduleId = moduleId;
    }

    public Long getPartModuleId()
    {
        return _partModuleId;
    }

    public void setPartModuleId(Long partModuleId)
    {
        _partModuleId = partModuleId;
    }

    public EppRegistryElementPartModule getPartModule()
    {
        return _partModule;
    }

    public void setPartModule(EppRegistryElementPartModule partModule)
    {
        _partModule = partModule;
    }

    public List<FefuLoadVO> getFefuLoadVOs()
    {
        return _fefuLoadVOs;
    }

    public void setFefuLoadVOs(List<FefuLoadVO> fefuLoadVOs)
    {
        _fefuLoadVOs = fefuLoadVOs;
    }

    public FefuLoadVO getCurrentFefuLoadVO()
    {
        return _currentFefuLoadVO;
    }

    public void setCurrentFefuLoadVO(FefuLoadVO currentFefuLoadVO)
    {
        _currentFefuLoadVO = currentFefuLoadVO;
    }

    public boolean isFirstPartModule()
    {
        return getPartModule() != null && getPartModule().getNumber() != 1;
    }

    public EppRegistryElementPart getPart()
    {
        return getPartModule() == null ? null : getPartModule().getPart();
    }

    public Map<EppFControlActionType, EppRegistryElementPartFControlAction> getSelection()
    {
        return _selection;
    }

    public void setSelection(Map<EppFControlActionType, EppRegistryElementPartFControlAction> selection)
    {
        Map<EppFControlActionType, EppRegistryElementPartFControlAction> map = new LinkedHashMap<>(selection.size());
        List<EppFControlActionType> keys = new ArrayList<>(selection.keySet());
        Collections.sort(keys, ITitled.TITLED_COMPARATOR);
        for (EppFControlActionType key : keys)
        {
            map.put(key, selection.get(key));
        }
        _selection = map;
    }

    public List<EppRegistryElementPartFControlAction> getEppControlActionList()
    {
        return new ArrayList<>(getSelection().values());
    }

    public List<EppFControlActionType> getEppControlTypeList()
    {
        return new ArrayList<>(getSelection().keySet());
    }

    public void setEppControlTypeList(List<EppFControlActionType> eppControlTypeList)
    {
        Map<EppFControlActionType, EppRegistryElementPartFControlAction> result = new HashMap<>(eppControlTypeList.size());
        if (null != getPart())
        {
            Map<EppFControlActionType, EppRegistryElementPartFControlAction> map = getSelection();
            for (EppFControlActionType actionType : eppControlTypeList)
            {
                EppRegistryElementPartFControlAction r = map.get(actionType);
                if (null == r)
                {
                    r = new EppRegistryElementPartFControlAction(getPart(), actionType);
                }
                result.put(r.getControlAction(), r);
            }
        }
        setSelection(result);
    }

    public EppRegistryElementPartFControlAction getCurrentEppControlAction()
    {
        return _currentEppControlAction;
    }

    public void setCurrentEppControlAction(EppRegistryElementPartFControlAction currentEppControlAction)
    {
        _currentEppControlAction = currentEppControlAction;
    }
}
