/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu6;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.unifefu.entity.FefuChangeFioStuExtract;


/**
 * @author Dmitry Seleznev
 * @since 03.09.2012
 */
public class FefuChangeFioStuExtractPrint implements IPrintFormCreator<FefuChangeFioStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuChangeFioStuExtract extract)
    {
        /*
        Дима, я в рамках борьбы с ворнингами нашла новый метод и удалила его, потому что в слое Уни писать свой injectModifier не надо (:
        Для того, что ты делал, хватило стандартных функций RtfString
        Если их не хватит, можно расширить то, что она умеет
         */

        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        RtfString fioRtf = new RtfString();
        RtfString fioExtRtf = new RtfString();
        
        IdentityCard oldIdentityCard = extract.getEntity().getPerson().getIdentityCard();

        if (!extract.getLastNameNew().equals(oldIdentityCard.getLastName()))
        {
            fioRtf.append("фамилии");
            fioExtRtf.append("фамилию на ").boldBegin().append(extract.getLastNameNew()).boldEnd();
        }

        if (!extract.getFirstNameNew().equals(oldIdentityCard.getFirstName()))
        {
            fioRtf.append(fioRtf.toList().size() > 0 ? ", " : "").append("имени");
            fioExtRtf.append(fioExtRtf.toList().size() > 0 ? ", " : "");
            fioExtRtf.append("имя на ").boldBegin().append(extract.getFirstNameNew()).boldEnd();
        }

        if (null != extract.getMiddleNameNew() && (null == oldIdentityCard.getMiddleName() || !extract.getMiddleNameNew().equals(oldIdentityCard.getMiddleName())))
        {
            fioRtf.append(fioRtf.toList().size() > 0 ? ", " : "").append("отчества");
            fioExtRtf.append(fioExtRtf.toList().size() > 0 ? ", " : "");
            fioExtRtf.append("отчество на ").boldBegin().append(extract.getMiddleNameNew()).boldEnd();
        }

        //if (fioExtRtf.toList().size() > 0 && (null != modifier.getStringValue("reason") && !"".equals(modifier.getStringValue("reason"))))
        //    fioExtRtf.append(" ");

        modifier.put("fioString_G", fioRtf);
        modifier.put("fioStringExt", fioExtRtf);

        if (null != extract.getReasonAddition()) modifier.put("reasonAddition", " " + extract.getReasonAddition());
        else modifier.put("reasonAddition", "");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}