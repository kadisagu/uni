/* $Id$ */
package ru.tandemservice.unifefu.ws.c1.src;

/**
 * @author Nikolay Fedorovskih
 * @since 16.12.2013
 */
public enum OrderType_1C
{
    ENROLLMENT,
    TRANSFER,
    EXCLUDE,
    ASSIGN_PAYMENT,
    TAKE_OFF_PAYMENT
}