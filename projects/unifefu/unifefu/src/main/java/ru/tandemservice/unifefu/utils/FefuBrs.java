/* $Id$ */
package ru.tandemservice.unifefu.utils;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.dao.IBrsScriptDao;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import java.util.List;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 20.02.2014
 */
public class FefuBrs
{
    // Доп. колонки
    public static final String TOTAL_COLUMN_KEY = "total"; // Всего
    public static final String MARK_COLUMN_KEY = "mark"; // Текстовая оценка
    public static final String MARK_CURRENT_COLUMN_KEY = "markCurrent"; // Текстовая оценка на текущую дату
    public static final String VALID_CURRENT_COLUMN_KEY = "validCurrent"; // Факт сдачи на текущую дату всех КМ из реализации с заданным минимальным баллом (да/нет)
    public static final String VALID_TOTAL_COLUMN_KEY = "validTotal";  // Факт сдачи всех КМ из реализации с заданным минимальным баллом (да/нет)
    public static final String CURRENT_GROUP_RATING_COLUMN_KEY = "currentGroupRating"; // Текущий рейтинг студента в рамках группы

    // Доп. строки - строки статистики
    public static final String VALID_ADDIT_PARAM = "valid"; // аттестовано
    public static final String MEDIAN_ADDIT_PARAM = "median"; // медиана
    public static final String MEDIUM_ADDIT_PARAM = "medium"; // среднее

    public static final ISessionBrsDao.IRatingValue TRUE_ROW_VALUE = BrsScriptUtils.ratingValue(1.0d, Boolean.TRUE.toString());
    public static final ISessionBrsDao.IRatingValue FALSE_ROW_VALUE = BrsScriptUtils.ratingValue(0.0d, Boolean.FALSE.toString());

    public static final String TEXT_MARK_5 = "отл";
    public static final String TEXT_MARK_4 = "хор";
    public static final String TEXT_MARK_3 = "удовл";
    public static final String TEXT_MARK_EXAM_FAIL = "неуд";

    public static final String TEXT_MARK_SETOFF = "зач";
    public static final String TEXT_MARK_SETOFF_FULL = "зачтено";
    public static final String TEXT_MARK_SETOFF_FAIL_FULL = "не зачтено";

    public static final String RANGE_3_CODE = "range_to_3";
    public static final String RANGE_3_DEF_CODE = "range_to_3_def";
    public static final String RANGE_4_CODE = "range_to_4";
    public static final String RANGE_4_DEF_CODE = "range_to_4_def";
    public static final String RANGE_5_CODE = "range_to_5";
    public static final String RANGE_5_DEF_CODE = "range_to_5_def";
    public static final String RANGE_SETOFF_CODE = "range_to_setoff";
    public static final String RANGE_SETOFF_DEF_CODE = "range_to_setoff_def";

    public static final String WEIGHT_JOURNAL_CODE = "weight_journal";
    public static final String WEIGHT_EVENT_CODE = "weight_event";
    public static final String MAX_POINTS_CODE = "max_points";
    public static final String MIN_POINTS_CODE = "min_points";

    public static final String EXAM_ACTION_TYPE_TITLE = "Экзамен";
    public static final String SETOFF_ACTION_TYPE_TITLE = "Зачет";

    public static class EventParams
    {
        private Double min;
        private Double max;
        private Double weight;
        private boolean current;

        public EventParams(boolean current)
        {
            this.current = current;
        }

        public EventParams(Double min, Double max, Double weight, boolean current)
        {
            this.min = min;
            this.max = max;
            this.weight = weight;
            this.current = current;
        }

        public Double getMin()
        {
            return min;
        }

        public void setMin(Double min)
        {
            this.min = min;
        }

        public Double getMax()
        {
            return max;
        }

        public void setMax(Double max)
        {
            this.max = max;
        }

        public Double getWeight()
        {
            return weight;
        }

        public void setWeight(Double weight)
        {
            this.weight = weight;
        }

        public boolean isCurrent()
        {
            return current;
        }

        public void setCurrent(boolean current)
        {
            this.current = current;
        }
    }

    public static class StudentRatingCalcResult
    {
        public final double ratingCurrent;
        public final double ratingTotal;

        public StudentRatingCalcResult(double ratingCurrent, double ratingTotal)
        {
            this.ratingCurrent = ratingCurrent;
            this.ratingTotal = ratingTotal;
        }
    }

    public static class ExtStudentRatingCalcResult extends StudentRatingCalcResult
    {
        public final boolean validCurrent;
        public final boolean validTotal;
        public final String textMarkCurrent;
        public final String textMarkTotal;

        public ExtStudentRatingCalcResult(double ratingCurrent, double ratingTotal, boolean validCurrent, boolean validTotal, String textMarkCurrent, String textMarkTotal)
        {
            super(ratingCurrent, ratingTotal);
            this.validCurrent = validCurrent;
            this.validTotal = validTotal;
            this.textMarkCurrent = textMarkCurrent;
            this.textMarkTotal = textMarkTotal;
        }
    }

    public static interface IGetStudentRatingScript
    {
        <EK, MK> StudentRatingCalcResult calcStudentRating(Map<EK, MK> marks, Map<EK, EventParams> events, double sum_weight_current, double sum_weight_total, Map<Double, String> rangeMap, boolean extRatingData);
    }

    public static interface IExtCurrentRatingCalc extends IBrsDao.ICurrentRatingCalc
    {
        /**
         * @return количество аттестованных на текущую дату
         */
        int getValidCurrentCount();
    }

    public static IBrsScriptDao.IBrsScriptWrapper<IBrsJournalScriptFunctions> getGlobalScript(EppYearPart yearPart)
    {
        return IBrsScriptDao.instance.get().buildDefaultGlobalScriptWrapper(yearPart, IBrsJournalScriptFunctions.class);
    }

    public static String getJournalScaleDescription(boolean isExam, TrJournal journal)
    {
        if (journal.getGradeScale() != null)
            isExam = journal.getGradeScale().getCode().equals(EppGradeScaleCodes.SCALE_5);
        String markSetting = "";
        List<TrBrsCoefficientValue> coefficientValues = DataAccessServices.dao().getList(TrBrsCoefficientValue.class, TrBrsCoefficientValue.L_OWNER, journal);
        if (isExam)
        {
            String range3 = "";
            String range4 = "";
            String range5 = "";
            for (TrBrsCoefficientValue value : coefficientValues)
            {
                if (value != null)
                {
                    String strValue = String.valueOf(value.getValue());
                    switch (value.getDefinition().getUserCode())
                    {
                        case FefuBrs.RANGE_3_CODE:
                            range3 = strValue;
                            break;
                        case FefuBrs.RANGE_4_CODE:
                            range4 = strValue;
                            break;
                        case FefuBrs.RANGE_5_CODE:
                            range5 = strValue;
                            break;
                    }
                }
            }
            markSetting = FefuBrs.TEXT_MARK_3 + " >= " + range3 + "%, " +
                    FefuBrs.TEXT_MARK_4 + " >= " + range4 + "%, " +
                    FefuBrs.TEXT_MARK_5 + " >= " + range5 + "%";
        }
        else
        {
            for (TrBrsCoefficientValue value : coefficientValues)
            {
                if (value != null && value.getDefinition().getUserCode().equals(FefuBrs.RANGE_SETOFF_CODE))
                {
                    markSetting = "зачтено >= " + String.valueOf(value.getValue()) + "%";
                    break;
                }
            }
        }

        return markSetting;
    }
}