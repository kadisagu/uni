package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.AdditionalProfessionalEducationProgramManager;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.logic.Constants.*;

@Configuration
public class AdditionalProfessionalEducationProgramAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, AdditionalProfessionalEducationProgramManager.instance().formativeOrgUnitDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
                .addDataSource(selectDS(TERRITORIAL_ORG_UNIT_DS, territorialOrgUnitDSHandler()).addColumn(OrgUnit.P_TERRITORIAL_FULL_TITLE))
                .addDataSource(selectDS(EDUCATION_ORG_UNIT_DS, AdditionalProfessionalEducationProgramManager.instance().educationOrgUnitDSHandler()).addColumn(EducationOrgUnit.P_TITLE))
                .addDataSource(selectDS(PRODUCING_ORG_UNIT_DS, AdditionalProfessionalEducationProgramManager.instance().producingOrgUnitDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
                .addDataSource(selectDS(DIPLOMA_QUALIFICATION_DS, AdditionalProfessionalEducationProgramManager.instance().diplomaQualificationDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(DIPLOMA_TYPE_DS, AdditionalProfessionalEducationProgramManager.instance().diplomaTypeDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(PROGRAM_STATUS_DS, AdditionalProfessionalEducationProgramManager.instance().programStatusDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(PROGRAM_PUBLICATION_STATUS_DS, AdditionalProfessionalEducationProgramManager.instance().programPublicationStatusDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(EDUCATION_LEVEL_DS, AdditionalProfessionalEducationProgramManager.instance().educationLevelDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(DEVELOP_FORM_DS, AdditionalProfessionalEducationProgramManager.instance().developFormDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(DEVELOP_CONDITION_DS, AdditionalProfessionalEducationProgramManager.instance().developConditionDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(DEVELOP_TECH_DS, AdditionalProfessionalEducationProgramManager.instance().developTechDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(DEVELOP_PERIOD_DS, AdditionalProfessionalEducationProgramManager.instance().developPeriodDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(ISSUED_DOCUMENT_DS, AdditionalProfessionalEducationProgramManager.instance().issuedDocumentDSHandler()).addColumn(TITLE))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> territorialOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou").column(property(EducationOrgUnit.territorialOrgUnit().id().fromAlias("eou")));
                Object formativeOrgUnit = context.get(FORMATIVE_ORG_UNIT);
                builder.where(eq(
                        property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), commonValue(formativeOrgUnit)));

                dql.where(in(property(OrgUnit.id().fromAlias("e")), builder.buildQuery()));
            }
        };
    }

}
