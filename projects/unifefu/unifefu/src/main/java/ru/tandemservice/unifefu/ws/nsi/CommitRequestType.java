/**
 * CommitRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.nsi;

public class CommitRequestType  implements java.io.Serializable {
    private ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType routingHeader;

    private ru.tandemservice.unifefu.ws.nsi.CommitDataType confirmation;

    public CommitRequestType() {
    }

    public CommitRequestType(
           ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType routingHeader,
           ru.tandemservice.unifefu.ws.nsi.CommitDataType confirmation) {
           this.routingHeader = routingHeader;
           this.confirmation = confirmation;
    }


    /**
     * Gets the routingHeader value for this CommitRequestType.
     * 
     * @return routingHeader
     */
    public ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType getRoutingHeader() {
        return routingHeader;
    }


    /**
     * Sets the routingHeader value for this CommitRequestType.
     * 
     * @param routingHeader
     */
    public void setRoutingHeader(ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType routingHeader) {
        this.routingHeader = routingHeader;
    }


    /**
     * Gets the confirmation value for this CommitRequestType.
     * 
     * @return confirmation
     */
    public ru.tandemservice.unifefu.ws.nsi.CommitDataType getConfirmation() {
        return confirmation;
    }


    /**
     * Sets the confirmation value for this CommitRequestType.
     * 
     * @param confirmation
     */
    public void setConfirmation(ru.tandemservice.unifefu.ws.nsi.CommitDataType confirmation) {
        this.confirmation = confirmation;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CommitRequestType)) return false;
        CommitRequestType other = (CommitRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.routingHeader==null && other.getRoutingHeader()==null) || 
             (this.routingHeader!=null &&
              this.routingHeader.equals(other.getRoutingHeader()))) &&
            ((this.confirmation==null && other.getConfirmation()==null) || 
             (this.confirmation!=null &&
              this.confirmation.equals(other.getConfirmation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRoutingHeader() != null) {
            _hashCode += getRoutingHeader().hashCode();
        }
        if (getConfirmation() != null) {
            _hashCode += getConfirmation().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CommitRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "CommitRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("routingHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "routingHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "RoutingHeaderType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("confirmation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "confirmation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "commitDataType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
