package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x0_14to15 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefu_1C_log

		//  свойство dispatchDate стало необязательным
		{
			// сделать колонку NULL
			tool.setColumnNullable("fefu_1c_log_t", "dispatchdate_p", true);

		}

		//  свойство request стало необязательным
		{
			// сделать колонку NULL
			tool.setColumnNullable("fefu_1c_log_t", "request_p", true);

		}


    }
}