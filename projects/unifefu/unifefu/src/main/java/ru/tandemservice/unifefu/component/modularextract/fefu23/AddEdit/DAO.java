/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu23.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.FefuExtractPrintFormManager;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Ekaterina Zvereva
 * @since 22.01.2015
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuTransfStuDPOExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected FefuTransfStuDPOExtract createNewInstance()
    {
        return new FefuTransfStuDPOExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setEducationOrgUnitModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                DQLSelectBuilder builder = createBuilder("", primaryKey);
                return builder.createStatement(getSession()).uniqueResult();
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = createBuilder(filter, null);
                return new ListResult<>(builder.createStatement(getSession()).list());
            }

            private DQLSelectBuilder createBuilder(String filter, Object primaryKey)
            {
                FefuAdditionalProfessionalEducationProgramForStudent studProg =
                        get(FefuAdditionalProfessionalEducationProgramForStudent.class, FefuAdditionalProfessionalEducationProgramForStudent.student(), model.getExtract().getEntity());
                EducationOrgUnit studEOU = studProg.getProgram().getEducationOrgUnit();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou")
                        .column(property("eou"))
                        .distinct()
                        .where(eq(property("eou", EducationOrgUnit.formativeOrgUnit()), value(studEOU.getFormativeOrgUnit())))
                        .where(eq(property("eou", EducationOrgUnit.territorialOrgUnit()), value(studEOU.getTerritorialOrgUnit())))
                        .where(eq(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().additional()), value(true)));

                if (primaryKey != null)
                {
                    if (primaryKey instanceof Long)
                        builder.where(eq(property("eou"), value((Long) primaryKey)));
                    if (primaryKey instanceof Collection)
                        builder.where(in(property("eou", FefuAdditionalProfessionalEducationProgram.educationOrgUnit().id()), (Collection) primaryKey));
                }
                return builder;
            }
        });

        model.setDpoProgramModel(new BaseSingleSelectModel()
        {

            @Override
            public ListResult<FefuAdditionalProfessionalEducationProgram> findValues(String filter)
            {
                DQLSelectBuilder builder = createSelectBuilder(filter, null);

                List<FefuAdditionalProfessionalEducationProgram> list = builder.createStatement(getSession()).list();
                return new ListResult<>(list, list.size());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return get(FefuAdditionalProfessionalEducationProgram.class, (Long)primaryKey);
            }

            private DQLSelectBuilder createSelectBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(FefuAdditionalProfessionalEducationProgram.class, "ape")
                        .predicate(DQLPredicateType.distinct)
                        .column(property("ape"));


                if (o != null)
                {
                    if (o instanceof Long)
                        builder.where(eq(property("ape", FefuAdditionalProfessionalEducationProgram.id()), commonValue(o, PropertyType.LONG)));
                    else if (o instanceof Collection)
                        builder.where(in(property("ape", FefuAdditionalProfessionalEducationProgram.id()), (Collection) o));
                }

                builder.where(eq(property("ape", FefuAdditionalProfessionalEducationProgram.educationOrgUnit()), value(model.getEducationOrgUnit())));

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("ape", FefuAdditionalProfessionalEducationProgram.title()), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property("ape", FefuAdditionalProfessionalEducationProgram.title()), OrderDirection.asc);

                return builder;
            }


        });

        if(model.isAddForm())
        {
            EducationOrgUnit program = new DQLSelectBuilder().fromEntity(FefuAdditionalProfessionalEducationProgramForStudent.class, "apes")
                    .column(property(FefuAdditionalProfessionalEducationProgramForStudent.program().educationOrgUnit().fromAlias("apes")))
                    .where(eq(property("apes", FefuAdditionalProfessionalEducationProgramForStudent.student()), value(model.getExtract().getEntity())))
                    .createStatement(getSession()).uniqueResult();

            model.setEducationOrgUnit(program);
            model.getExtract().setCourseNew(model.getExtract().getEntity().getCourse());
        }
        else
        {
            model.setEducationOrgUnit(model.getExtract().getDpoProgramNew().getEducationOrgUnit());
        }
    }


    @Override
    public void update(Model model)
    {
        if (!model.getExtract().getEntity().getStudentCategory().isDppListener())
            throw new ApplicationException("Невозможно создать приказ. Данный студент не является слушателем ДПО.");

        model.getExtract().setCourseOld(model.getExtract().getEntity().getCourse());
        FefuAdditionalProfessionalEducationProgram dpoProgramOld = getProperty(FefuAdditionalProfessionalEducationProgramForStudent.class, FefuAdditionalProfessionalEducationProgramForStudent.L_PROGRAM,
                                                                               FefuAdditionalProfessionalEducationProgramForStudent.L_STUDENT, model.getExtract().getEntity());
        if (dpoProgramOld != null)
            model.getExtract().setDpoProgramOld(dpoProgramOld);

        super.update(model);
        if (model.getExtract().isIndividual())
            FefuExtractPrintFormManager.instance().dao().saveOrUpdatePrintForm(model.getExtract().getParagraph().getOrder(), model.getUploadFile());

        //Заносим в выписку данные о предыдущей программе ДПО
        FefuTransfStuDPOExtract extract = model.getExtract();

        if (dpoProgramOld != null)
        {
            extract.setFormativeOrgUnitStr(dpoProgramOld.getFormativeOrgUnit().getFullTitle());
            extract.setTerritorialOrgUnitStr(dpoProgramOld.getTerritorialOrgUnit().getTerritorialFullTitle());
            extract.setEducationLevelHighSchoolStr((String) dpoProgramOld.getEducationOrgUnit().getEducationLevelHighSchool().getProperty(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY));
            extract.setDevelopFormStr(dpoProgramOld.getDevelopForm().getTitle());
            extract.setDevelopConditionStr(dpoProgramOld.getDevelopCondition().getTitle());
            extract.setDevelopTechStr(dpoProgramOld.getDevelopTech().getTitle());
            extract.setDevelopPeriodStr(dpoProgramOld.getDevelopPeriod().getTitle());
        }
        update(extract);
    }

}