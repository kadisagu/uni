/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuMobile.logic;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unifefu.ws.mobile.entity.*;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2014
 */
public interface IFefuMobileDao extends IUniBaseDao
{
    void doRegisterChangedEntities(List<Long> entityIds, boolean delete);

    void doRegisterDeletedEntities(Long entityId);

    List<Long> getNewAndUpdatedEntityIdsList(String entityName);

    List<Long> getDeletedEntityIdsList(String entityName);

    void doCommitEntityByIdList(List<Long> entityIdsList);


    List<EduYear> getEduYearsList();

    EduYear getCurrentEduYear();


    List<Long> getActiveStudentPersonsList();

    List<ru.tandemservice.unifefu.ws.mobile.entity.Person> getPersonsByIdList(List<Long> personIdsList);

    ru.tandemservice.unifefu.ws.mobile.entity.Person getPersonById(Long personId);


    List<Long> getActiveStudentIdsList();

    List<ru.tandemservice.unifefu.ws.mobile.entity.Student> getStudentsByIdList(List<Long> studentIdsList);

    ru.tandemservice.unifefu.ws.mobile.entity.Student getStudentById(Long studentId);


    List<Long> getActiveStudent2SemestersIdsList();

    List<Student2Semester> getStudent2SemestersByIdList(List<Long> student2SemesterIds);


    List<Long> getActiveSubjectIdsList();

    List<Subject> getSubjectsByIdList(List<Long> subjectIds);


    List<Long> getActualPeriodsIdsList();

    List<Period> getPeriodsByIdList(List<Long> periodIds);


    List<Long> getActualResultsIdsList();

    List<Result> getResultsByIdList(List<Long> resultIds);
}