/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.OnlineEntrantNotifications;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;
import ru.tandemservice.unifefu.base.vo.ProcessedOnlineEntrantsVO;

import java.util.Collection;
import java.util.List;

/**
 * @author nvankov
 * @since 6/17/13
 */
public class FefuSystemActionOnlineEntrantNotificationsUI extends UIPresenter
{
    private List<ProcessedOnlineEntrantsVO> _onlineEntrantsWithoutEmails;
    private ProcessedOnlineEntrantsVO _currentEntrantWithoutEmail;

    public List<ProcessedOnlineEntrantsVO> getOnlineEntrantsWithoutEmails()
    {
        return _onlineEntrantsWithoutEmails;
    }

    public void setOnlineEntrantsWithoutEmails(List<ProcessedOnlineEntrantsVO> onlineEntrantsWithoutEmails)
    {
        _onlineEntrantsWithoutEmails = onlineEntrantsWithoutEmails;
    }

    public BaseSearchListDataSource getOnlineEntrantDS()
    {
        return _uiConfig.getDataSource(FefuSystemActionOnlineEntrantNotifications.ONLINE_ENTRANT_DS);
    }


    @Override
    public void onComponentRefresh()
    {
        if(null == _uiSettings.get("enrollmentCampaign"))
        {
            EnrollmentCampaign currentCampaign = DataAccessServices.dao().get(EnrollmentCampaign.class, EnrollmentCampaign.educationYear().current(), true);
            if(null != currentCampaign)
            {
                EnrollmentCampaign nextCampaign = DataAccessServices.dao().get(
                        EnrollmentCampaign.class,
                        EnrollmentCampaign.educationYear().intValue(),
                        currentCampaign.getEducationYear().getIntValue() + 1);
                if(null != nextCampaign) _uiSettings.set("enrollmentCampaign", nextCampaign);
                else _uiSettings.set("enrollmentCampaign", currentCampaign);
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuSystemActionOnlineEntrantNotifications.ONLINE_ENTRANT_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "enrollmentCampaign", "processDateFrom", "processDateTo", "entrantType"));
        }
    }

    public Boolean getNotSendEmails()
    {
        if(null == _onlineEntrantsWithoutEmails) return Boolean.FALSE;
        return !_onlineEntrantsWithoutEmails.isEmpty();
    }

    public String getNotSendEmailsMessage()
    {
        StringBuilder message = new StringBuilder();
        message.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>Не отправлены сообщения, так как не указан E-mail, следующим онлайн-абитуриентам: </td></tr>");

        for(ProcessedOnlineEntrantsVO onlineEntrant : _onlineEntrantsWithoutEmails)
        {
            message.append("<tr><td>" +  onlineEntrant.getTitle() + " (" + onlineEntrant.getType() + ")</td></tr>");
        }
        message.append("</table>");
        return message.toString();
    }

    public void onClickSend()
    {
        Collection selected = getOnlineEntrantDS().getOptionColumnSelectedObjects("select");
        if(selected.isEmpty())
            throw new ApplicationException("Для отправки уведомлений о принятии заявлений необходимо выбрать хотя бы одного онлайн-абитуриента");

        List<ProcessedOnlineEntrantsVO> onlineEntrantsList = Lists.newArrayList(selected);

        _onlineEntrantsWithoutEmails = FefuSystemActionManager.instance().dao().sendOnlineEntrantNotifications(onlineEntrantsList);
    }
}
