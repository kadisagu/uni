/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu15.ParagraphPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubController;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.FefuExtractPrintFormManager;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.ui.Edit.FefuExtractPrintFormEdit;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public class Controller extends AbstractListParagraphPubController<FefuEnrollStuDPOListExtract, IDAO, Model>
{
    public void onClickEditPrintForm(IBusinessComponent component)
    {
        Model model = getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(FefuExtractPrintFormEdit.class.getSimpleName(),
                                   new ParametersMap().add("objectId", model.getParagraph().getOrder().getId())));
    }

    @Override
    public void onClickDeleteParagraph(IBusinessComponent component)
    {
        if (getModel(component).getParagraph().getOrder().getParagraphCount() == 1)
            FefuExtractPrintFormManager.instance().dao().deletePrintForm(getModel(component).getParagraph().getOrder());

        super.onClickDeleteParagraph(component);

    }


}