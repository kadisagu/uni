/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.EntrantRequestStep;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.EntrantRequestAddEditUtil;

/**
 * @author Nikolay Fedorovskih
 * @since 05.06.2013
 */
public class Controller extends ru.tandemservice.uniec.component.wizard.EntrantRequestStep.Controller
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        // Патчим колонку с выбранными направлениями подготовки.
        // Вместо EducationLevelsHighSchool.P_DISPLAYABLE_TITLE нам надо выводить EducationLevelsHighSchool.P_FULL_TITLE (DEV-3052)
        DynamicListDataSource<RequestedEnrollmentDirection> selectedDirsDS = getModel(component).getSelectedRequestedEnrollmentDirectionDataSource();
        AbstractColumn dirsColumn = selectedDirsDS.getColumn(EntrantRequestAddEditUtil.REQUESTED_DIRECTIONS_COLUMN);
        String[] fields = (String[])dirsColumn.getKey();
        for (int i = fields.length - 1; i >= 0; i--)
        {
            if (fields[i].equals(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE))
            {
                fields[i] = EducationLevelsHighSchool.P_FULL_TITLE;
                dirsColumn.setKey(fields);
                break;
            }
        }
    }

    protected void validateFields(IBusinessComponent component)
    {
        super.validateFields(component);
        Model model = (Model) getModel(component);
        if (model.getTechnicalCommission() == null)
            component.getUserContext().getErrorCollector().add("Поле «Техническая комиссия» обязательно для заполнения.", "technicalCommission");
    }
}