/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrgUnitNumberAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.FefuOrgUnitNumber;

/**
 * @author Nikolay Fedorovskih
 * @since 12.08.2013
 */
@Input({
               @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitNumberId"),
       })
public class FefuSettingsOrgUnitNumberAddEditUI extends UIPresenter
{
    private Long orgUnitNumberId;
    private FefuOrgUnitNumber orgUnitNumber;
    private boolean isAddForm;

    @Override
    public void onComponentRefresh()
    {
        isAddForm = (orgUnitNumberId == null);
        if (isAddForm)
            orgUnitNumber = new FefuOrgUnitNumber();
        else
            orgUnitNumber = DataAccessServices.dao().getNotNull(FefuOrgUnitNumber.class, orgUnitNumberId);
    }

    // Listeners

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(orgUnitNumber);
        deactivate();
    }

    // Getters & Setters

    public boolean isAddForm()
    {
        return isAddForm;
    }

    public Long getOrgUnitNumberId()
    {
        return orgUnitNumberId;
    }

    public void setOrgUnitNumberId(Long orgUnitNumberId)
    {
        this.orgUnitNumberId = orgUnitNumberId;
    }

    public FefuOrgUnitNumber getOrgUnitNumber()
    {
        return orgUnitNumber;
    }
}