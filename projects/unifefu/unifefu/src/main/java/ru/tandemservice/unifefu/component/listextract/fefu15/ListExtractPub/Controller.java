/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu15.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubController;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public class Controller extends AbstractListExtractPubController<FefuEnrollStuDPOListExtract, Model, IDAO>
{
}