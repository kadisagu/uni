/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EcDistribution.ui.AddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.AddEdit.EcDistributionAddEdit;

/**
 * @author nvankov
 * @since 7/9/13
 */
@Configuration
public class EcDistributionAddEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + EcDistributionAddEditExtUI.class.getSimpleName();

    @Autowired
    private EcDistributionAddEdit _ecDistributionAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_ecDistributionAddEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EcDistributionAddEditExtUI.class))
                .create();
    }
}
