/* $Id$ */
package ru.tandemservice.unifefu.dao.student;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unifefu.entity.*;

import java.util.Map;

/**
 * @author nvankov
 * @since 11/5/13
 */
public class FefuStudentExtractData implements IFefuStudentExtractData
{
    public static final String BASE_PROP_ORDER_NUM = "orderNum";
    public static final String BASE_PROP_ORDER_COMMIT_DATE = "orderCommitDate";
    public static final String BASE_PROP_ORDER_COMMIT_DATE_SYSTEM = "orderCommitDateSystem";
    public static final String BASE_PROP_ORDER_EXECUTOR = "orderExecutor";
    public static final String BASE_PROP_REASON = "reason";
    public static final String PROP_GEN_COMMENT = "genComment";
    public static final String PROP_EXTRACT_COURSE = "course";
    public static final String PROP_EXTRACT_EDU_OU = "eduOU";
    public static final String PROP_EXTRACT_PAYMENT = "payment";
    public static final String PROP_EXTRACT_END_DATE = "endDate";
    public static final String PROP_EXTRACT_PRACTICE_PERIOD = "practicePeriod";
    public static final String PROP_EXTRACT_PRACTICE_TYPE = "practiceType";
    public static final String PROP_EXTRACT_PRACTICE_PLACE = "practicePlace";
    public static final String PROP_EXTRACT_PRACTICE_HEADER = "practiceHeader";

    @Override
    public Map<String, String> getBaseData(AbstractStudentExtract extract)
    {
        Map<String, String> data = Maps.newHashMap();
        if (null != extract.getParagraph() && null != extract.getParagraph().getOrder())
        {
            AbstractStudentOrder order = extract.getParagraph().getOrder();

            data.put(BASE_PROP_ORDER_NUM, order.getNumber());
            data.put(BASE_PROP_ORDER_EXECUTOR, order.getExecutor());
            data.put(BASE_PROP_ORDER_COMMIT_DATE, DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
            data.put(BASE_PROP_ORDER_COMMIT_DATE_SYSTEM, DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDateSystem()));
        }
        data.put(PROP_GEN_COMMENT, extract.getComment());

        if(extract instanceof ModularStudentExtract)
        {
            data.put(BASE_PROP_REASON, ((ModularStudentExtract) extract).getReasonFullTitle());
        }
        else if(extract instanceof OtherStudentExtract)
        {
            data.put(BASE_PROP_REASON, ((OtherStudentExtract) extract).getReasonFullTitle());
        }
        else
        {
            if (null != extract.getParagraph() && null != extract.getParagraph().getOrder())
            {
                if(extract.getParagraph().getOrder() instanceof StudentListOrder)
                {
                    data.put(BASE_PROP_REASON, ((StudentListOrder) extract.getParagraph().getOrder()).getReasonFullTitle());

                }
            }
        }
        return data;
    }
//    +  № приказа - Дата

//    Курс - Институт, факультет, специальность - размер стипендии - Для заметок (Примечания, Поощрение/взыскание)
//    Вид практики - Место - Форма собств. - Город - Сроки - Руководитель (от ДВФУ)
//    Причина - Дата снятия (приказ снятия с должности старосты)

    //
    // Сборные (индивидуальные) приказы
    //

    //    <!-- 1. О предоставлении академического отпуска -->
    @Override
    public Map<String, String> getData(WeekendStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
//        Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 2. О восстановлении -->
    @Override
    public Map<String, String> getData(RestorationStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
//        Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 3. О выходе из академического отпуска -->
    @Override
    public Map<String, String> getData(WeekendOutStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - размер стипендии (если восстановлена)
        addCourseWithEduOUStr(data, extract);
        if(extract.isPaymentResume())
        {
            data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantSum()));
        }
        return data;
    }

    // <!-- 4. О зачислении -->
    @Override
    public Map<String, String> getData(EduEnrolmentStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 5. О переводе на другую форму освоения -->
    @Override
    public Map<String, String> getData(TransferDevFormStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 6. О переводе на другую основу оплаты обучения -->
    @Override
    public Map<String, String> getData(TransferCompTypeStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 7. О переводе на другое направление подготовки (специальность) -->
    @Override
    public Map<String, String> getData(TransferEduTypeStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 8. Об изменении пункта обучения -->
    @Override
    public Map<String, String> getData(TransferStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 9. Об отчислении -->
    @Override
    public Map<String, String> getData(ExcludeStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 10. О смене фамилии (имени) -->
    @Override
    public Map<String, String> getData(ChangeFioStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 11. Об отмене приказа -->
    @Override
    public Map<String, String> getData(RevertOrderStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 12. О предоставлении отпуска по беременности и родам -->
    @Override
    public Map<String, String> getData(WeekendPregnancyStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 13. О предоставлении отпуска по уходу за ребенком -->
    @Override
    public Map<String, String> getData(WeekendChildStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 14. О зачислении, расширенный вариант -->
    @Override
    public Map<String, String> getData(EduEnrolmentStuExtractExt extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 15. О зачислении в порядке перевода -->
    @Override
    public Map<String, String> getData(EduEnrAsTransferStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 16. О предоставлении академического отпуска, расширенный вариант -->
    @Override
    public Map<String, String> getData(WeekendStuExtractExt extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 17. О восстановлении, расширенный вариант -->
    @Override
    public Map<String, String> getData(RestorationStuExtractExt extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 18. О переводе, расширенный вариант -->
    @Override
    public Map<String, String> getData(TransferStuExtractExt extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 19. Об отстранении от занятий -->
    @Override
    public Map<String, String> getData(DischargingStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 20. О продлении экзаменационной сессии -->
    @Override
    public Map<String, String> getData(SessionProlongStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 21. О продлении отпуска по уходу за ребенком -->
    @Override
    public Map<String, String> getData(ProlongWeekndChildStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 22. О назначении академической стипендии -->
    @Override
    public Map<String, String> getData(AcadGrantAssignStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 23. О назначении социальной стипендии -->
    @Override
    public Map<String, String> getData(SocGrantAssignStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - размер стипендии
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantSize()));
        return data;
    }

    // <!-- 24. О приостановлении выплаты социальной стипендии -->
    @Override
    public Map<String, String> getData(SocGrantStopStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 25. О возобновлении выплаты социальной стипендии -->
    @Override
    public Map<String, String> getData(SocGrantResumptionStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 26. Об отчислении, расширенный вариант -->
    @Override
    public Map<String, String> getData(ExcludeStuExtractExt extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 27. Об отчислении в связи с переводом -->
    @Override
    public Map<String, String> getData(ExcludeTransfStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 28. О выдаче дубликата зачетной книжки -->
    @Override
    public Map<String, String> getData(GiveBookDuplicateStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 29. О выдаче дубликата студенческого билета -->
    @Override
    public Map<String, String> getData(GiveCardDuplicateStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 30. О выдаче дубликата диплома -->
    @Override
    public Map<String, String> getData(GiveDiplomaDuplicateStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 31. О выдаче дубликата приложения к диплому -->
    @Override
    public Map<String, String> getData(GiveDiplAppDuplicateStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 32. О допуске к занятиям -->
    @Override
    public Map<String, String> getData(AdmissionStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 33. О назначении выплаты пособий -->
    @Override
    public Map<String, String> getData(AssignBenefitStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!--   34. О смене имени -->
    @Override
    public Map<String, String> getData(ChangeFirstNameStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!--  35. О смене отчества -->
    @Override
    public Map<String, String> getData(ChangeMiddleNameStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 36. О повторном обучении -->
    @Override
    public Map<String, String> getData(ReEducationStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 37. О продлении академического отпуска -->
    @Override
    public Map<String, String> getData(ProlongWeekendStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 38. О выходе из отпуска по уходу за ребенком -->
    @Override
    public Map<String, String> getData(WeekendChildOutStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 39. О скидке по оплате за обучение -->
    @Override
    public Map<String, String> getData(PaymentDiscountStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 40 О ВЫДАЧЕ ДИПЛОМА О НЕПОЛНОМ ВЫСШЕМ ОБРАЗОВАНИИ ПРОФЕССИОНАЛЬНОМ ОБРАЗОВАНИИ-->
    @Override
    public Map<String, String> getData(GiveDiplIncompleteStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 41. О досрочной сдаче сессии -->
    @Override
    public Map<String, String> getData(PrescheduleSessionPassStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 42. О переносе сроков сдачи защиты дипломной работы -->
    @Override
    public Map<String, String> getData(ChangePassDiplomaWorkPeriodStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 43. О переносе сроков сдачи государственного экзамена -->
    @Override
    public Map<String, String> getData(ChangeDateStateExaminationStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 44. О повторной защите дипломной работы -->
    @Override
    public Map<String, String> getData(RepeatProtectDiplomaWorkStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 45. О повторной сдаче государственного экзамена -->
    @Override
    public Map<String, String> getData(RepeatPassStateExamStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 46. О свободном посещении занятий -->
    @Override
    public Map<String, String> getData(FreeStudiesAttendanceStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 47. О назначении старостой учебной группы -->
    @Override
    public Map<String, String> getData(AddGroupManagerStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 48. Об освобождении от обязанностей старосты учебной группы -->
    @Override
    public Map<String, String> getData(RemoveGroupManagerStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - Дата снятия
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_END_DATE, DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getRemovalDate()));
        return data;
    }

    // <!-- 49. О дисциплинарном взыскании -->
    @Override
    public Map<String, String> getData(DisciplinaryPenaltyStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 50. О наложении взыскания за утерю зачетной книжки -->
    @Override
    public Map<String, String> getData(RecordBookLossPenaltyStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 51. О наложении взыскания за утерю студенческого билета -->
    @Override
    public Map<String, String> getData(StudentCardLossPenaltyStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 52. О допуске к сдаче государственного экзамена -->
    @Override
    public Map<String, String> getData(AdmitToStateExamsStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 53. О предоставлении каникул -->
    @Override
    public Map<String, String> getData(HolidayStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 54. О переносе итоговой государственной аттестации -->
    @Override
    public Map<String, String> getData(ChangePassStateExamsStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 55. Об установлении индивидуального срока сдачи сессии(продление сессии) -->
    @Override
    public Map<String, String> getData(SessionIndividualStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 56. О переводе на индивидуальный план обучения -->
    @Override
    public Map<String, String> getData(TransferIndPlanStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 57. О допуске к защите выпускной квалификационной работы(после ГЭ) -->
    @Override
    public Map<String, String> getData(AdmitToDiplomaStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 58. О переводе на индивидуальный график обучения -->
    @Override
    public Map<String, String> getData(TransferIndGraphStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 59. О предоставлении отпуска по уходу за ребенком до 1,5 лет -->
    @Override
    public Map<String, String> getData(WeekendChildOneAndHalfStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 60. О предоставлении повторного года обучения с переходом на ФГОС -->
    @Override
    public Map<String, String> getData(ReEducationFGOSStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 61. О восстановлении -->
    @Override
    public Map<String, String> getData(RestorationCourseStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 62. Об отчислении (государственный экзамен) -->
    @Override
    public Map<String, String> getData(ExcludeStateExamStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 63. Об отчислении (недопуск к ВКР) -->
    @Override
    public Map<String, String> getData(ExcludeUnacceptedToGPDefenceStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 64. Об отчислении (ВКР) -->
    @Override
    public Map<String, String> getData(ExcludeGPDefenceFailStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 65. О переводе из группы в группу -->
    @Override
    public Map<String, String> getData(TransferGroupStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 66. О переводе с курса на следующий курс -->
    @Override
    public Map<String, String> getData(TransferCourseStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 67. О переводе со специальности на специальность (с направления на направление) -->
    @Override
    public Map<String, String> getData(TransferEduLevelStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 68. О переводе со специальности на специальность (с направления на направление) со сменой формирующего подразделения -->
    @Override
    public Map<String, String> getData(TransferFormativeStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 69. О переводе со специальности на специальность (с направления на направление) со сменой территориального подразделения -->
    @Override
    public Map<String, String> getData(TransferTerritorialStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 70. О переводе со специальности на специальность (с направления на направление) между территориальными подразделениями -->
    @Override
    public Map<String, String> getData(TransferTerritorialExtStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 71. О переводе на другие условия освоения и срок освоения -->
    @Override
    public Map<String, String> getData(TransferDevCondExtStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 72. О переводе на другие условия освоения -->
    @Override
    public Map<String, String> getData(TransferDevCondStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 73. О восстановлении и повторном допуске к сдаче государственного экзамена (пересдача ГЭ) -->
    @Override
    public Map<String, String> getData(RestorationAdmitStateExamsExtStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 74. О закреплении за специализацией (профилем) и перераспеределнии в группу -->
    @Override
    public Map<String, String> getData(TransferProfileStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 75. О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ) -->
    @Override
    public Map<String, String> getData(RestorationAdmitStateExamsStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 76. О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу -->
    @Override
    public Map<String, String> getData(TransferProfileGroupExtStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 77. О восстановлении и допуске к подготовке ВКР -->
    @Override
    public Map<String, String> getData(RestorationAdmitToDiplomaStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 78. О переводе на другую основу оплаты обучения (вакантное бюджетное место) -->
    @Override
    public Map<String, String> getData(TransferCompTypeExtStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 79. О переводе со специализации на специализации (с профиля на профиль) -->
    @Override
    public Map<String, String> getData(TransferProfileExtStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 80. Об изменении категории обучаемого -->
    @Override
    public Map<String, String> getData(ChangeCategoryStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 81. О восстановлении и допуске к защите ВКР (неявка на защиту) -->
    @Override
    public Map<String, String> getData(RestorationAdmitToDiplomaDefStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 82. О пересдаче дисциплин учебного плана -->
    @Override
    public Map<String, String> getData(RePassDiscStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 83. О восстановлении и допуске к защите ВКР (повторная защита) -->
    @Override
    public Map<String, String> getData(RestorationAdmitToDiplomaDefExtStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 84. О пересдаче дисциплин учебного плана комиссии -->
    @Override
    public Map<String, String> getData(RePassDiscComissStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 85. О выдаче диплома и присвоении квалификации -->
    @Override
    public Map<String, String> getData(GiveDiplomaStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 86. Об оказании материальной помощи -->
    @Override
    public Map<String, String> getData(MatAidAssignStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 87. Об однократном повышении государственной академической стипендии -->
    @Override
    public Map<String, String> getData(GrantRiseStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - размер стипендии
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantRiseAmount()));
        return data;
    }

    // <!-- 88. О закреплении за специализацией (профилем) -->
    @Override
    public Map<String, String> getData(ProfileAssignStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 89. О выходе из отпуска по беременности и родам -->
    @Override
    public Map<String, String> getData(WeekendPregnancyOutStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 90. О выходе из отпуска по уходу за ребенком -->
    @Override
    public Map<String, String> getData(WeekendChildCareOutStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 91. О назначении надбавки к государственной академической стипендии -->
    @Override
    public Map<String, String> getData(AcadGrantBonusAssignStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - размер стипендии
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantBonusSize()));
        return data;
    }

    // <!-- 93. О распределении в группу -->
    @Override
    public Map<String, String> getData(GroupAssignStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 92. О переводе с курса на следующий курс со сменой территориального подразделения -->
    @Override
    public Map<String, String> getData(TransferCourseWithChangeTerrOuStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    // <!-- 94. О предоставлении отпуска по уходу за ребенком до 3 лет -->
    @Override
    public Map<String, String> getData(WeekendChildThreeStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 95. Об установлении индивидуального срока сдачи сессии(досрочная сдача сессии) -->
    @Override
    public Map<String, String> getData(SessionIndividualAheadStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 96. О допуске к защите выпускной квалификационной работы -->
    @Override
    public Map<String, String> getData(AdmitToDiplomaWithoutExamStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 97. О приеме гражданства Российской Федерации -->
    @Override
    public Map<String, String> getData(AdoptRussianCitizenshipStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 98. Об аттестации практик студентов на основании академической справки -->
    @Override
    public Map<String, String> getData(AttestationPracticStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - Вид практики - Сроки
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PRACTICE_PERIOD, extract.getPracticeDurationStr() + " нед.");
        return data;
    }

    // <!-- 99. О прекращении выплаты академической стипендии -->
    @Override
    public Map<String, String> getData(AcadGrantPaymentStopStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 100. О прекращении выплаты социальной стипендии -->
    @Override
    public Map<String, String> getData(SocGrantPaymentStopStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 101. О прекращении выплаты надбавки к государственной академической стипендии  -->
    @Override
    public Map<String, String> getData(AcadGrantBonusPaymentStopStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // <!-- 102. Об аттестации практик студентов имеющих стаж работы по профилю обучения -->
    @Override
    public Map<String, String> getData(AttestationPracticWithServiceRecStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - Вид практики - Место - Сроки
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PRACTICE_TYPE, extract.getPracticeType());
        data.put(PROP_EXTRACT_PRACTICE_PLACE, extract.getPracticePlace());
        data.put(PROP_EXTRACT_PRACTICE_PERIOD, extract.getPracticeDurationStr() + " нед.");
        return data;
    }

    // <!-- 103. Об аттестации практик студентов, прошедших стажировку -->
    @Override
    public Map<String, String> getData(AttestationPracticWithProbationStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - Вид практики - Место - Сроки
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PRACTICE_TYPE, extract.getPracticeType());
        data.put(PROP_EXTRACT_PRACTICE_PLACE, extract.getPracticePlace());
        data.put(PROP_EXTRACT_PRACTICE_PERIOD, DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeBeginDate()) + "-" +
                DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeEndDate()) + ", " + extract.getPracticeDurationStr() + " нед.");
        return data;
    }

    // <!-- 104. О переводе между территориальными подразделениями -->
    @Override
    public Map<String, String> getData(TransferBetweenTerritorialStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    //    <!-- fefu1. О переносе сдачи государственного экзамена-->
    @Override
    public Map<String, String> getData(FefuChangeDateStateExaminationStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu2. О переносе защиты выпускной квалификационной работы-->
    @Override
    public Map<String, String> getData(FefuChangePassDiplomaWorkPeriodStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu3. О предоставлении повторного года обучения -->
    @Override
    public Map<String, String> getData(FefuReEducationStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu4. Об отчислении -->
    @Override
    public Map<String, String> getData(FefuExcludeStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu5. О переводе на другую форму освоения -->
    @Override
    public Map<String, String> getData(FefuTransferDevFormStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    //    <!-- fefu6. Об изменении фамилии, имени, отчества -->
    @Override
    public Map<String, String> getData(FefuChangeFioStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu7. О назначении академической стипендии -->
    @Override
    public Map<String, String> getData(FefuAcadGrantAssignStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - размер стипендии
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantSize()));
        return data;
    }

    //    <!-- fefu8. О приостановлении выплаты социальной стипендии -->
    @Override
    public Map<String, String> getData(FefuSocGrantStopStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu9. О возобновлении выплаты социальной стипендии -->
    @Override
    public Map<String, String> getData(FefuSocGrantResumptionStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu10. О зачислении на второй и последующий курс -->
    @Override
    public Map<String, String> getData(FefuEduEnrolmentToSecondAndNextCourseStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    //    <!-- fefu11. О назначении академической стипендии (вступительные испытания) -->
    @Override
    public Map<String, String> getData(FefuAcadGrantAssignStuEnrolmentExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Курс - Институт, факультет, специальность - размер стипендии
        addCourseWithEduOUStr(data, extract);
        String payment = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantSize());
        if(extract.hasGroupManagerBonus())
            payment += " + за стар. " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGroupManagerBonusSize());
        data.put(PROP_EXTRACT_PAYMENT, payment);
        return data;
    }

    //    <!-- fefu12. О зачислении в порядке перевода -->
    @Override
    public Map<String, String> getData(FefuEduTransferEnrolmentStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность
        addCourseWithEduOUNew(data, extract);
        return data;
    }

    //    <!-- fefu13. О направлении на практику за пределами ОУ-->
    @Override
    public Map<String, String> getData(SendPracticOutStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - Вид практики - Место - Сроки - Руководитель (от ДВФУ)
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PRACTICE_TYPE, extract.getPracticeKind());
        String practicePlace = extract.getPracticeExtOrgUnit().getTitle();
        if(!StringUtils.isEmpty(extract.getPracticePlace()))
            practicePlace += ", " + extract.getPracticePlace();
        else
            practicePlace += ", " + extract.getPracticeExtOrgUnit().getFactAddress().getTitleWithFlat();
        data.put(PROP_EXTRACT_PRACTICE_PLACE, practicePlace);
        data.put(PROP_EXTRACT_PRACTICE_PERIOD, DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeBeginDate()) + "-" +
                DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeEndDate()) + ", " + extract.getPracticeDurationStr() + " нед.");
        if(!StringUtils.isEmpty(extract.getPracticeHeaderInnerStr()))
            data.put(PROP_EXTRACT_PRACTICE_HEADER, extract.getPracticeHeaderInnerStr());
        else
            data.put(PROP_EXTRACT_PRACTICE_HEADER, getFioNominativeStr(extract.getPracticeHeaderInner(), extract.getPracticeHeaderInnerDegree()));
        return data;
    }

    //    <!-- fefu14. О направлении на практику в пределах ОУ-->
    @Override
    public Map<String, String> getData(SendPracticInnerStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - Вид практики - Место - Форма собств. - Город - Сроки - Руководитель (от ДВФУ)
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PRACTICE_TYPE, extract.getPracticeKind());
        String practicePlace = "";
        if(!StringUtils.isEmpty(extract.getPracticeOrgUnitStr()))
            practicePlace += extract.getPracticeOrgUnitStr();
        else
            practicePlace += extract.getPracticeOrgUnit().getTitle() + ", " + extract.getPracticeOrgUnit().getAddress().getTitleWithFlat();
        data.put(PROP_EXTRACT_PRACTICE_PLACE, practicePlace);
        data.put(PROP_EXTRACT_PRACTICE_PERIOD, DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeBeginDate()) + "-" +
                DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeEndDate()) + ", " + extract.getPracticeDurationStr() + " нед.");
        if(!StringUtils.isEmpty(extract.getPracticeHeaderInnerStr()))
            data.put(PROP_EXTRACT_PRACTICE_HEADER, extract.getPracticeHeaderInnerStr());
        else
            data.put(PROP_EXTRACT_PRACTICE_HEADER, getFioNominativeStr(extract.getPracticeHeader(), extract.getPracticeHeaderDegree()));

        return data;
    }


    //    <!-- fefu15. О зачислении на полное государственное обеспечение-->
    @Override
    public Map<String, String> getData(FullStateMaintenanceEnrollmentStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        data.put(PROP_EXTRACT_COURSE, extract.getCourseStr());
        if(null != extract.getTotalPaymentSumInRuble())
            data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getTotalPaymentSumInRuble()));
        return data;
    }

    //    <!-- fefu16. О полном государственном обеспечении-->
    @Override
    public Map<String, String> getData(FullStateMaintenanceStuExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        data.put(PROP_EXTRACT_COURSE, extract.getCourseStr());
        if(null != extract.getTotalPaymentSumInRuble())
            data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getTotalPaymentSumInRuble()));
        return data;
    }

    //    <!-- fefu20. О зачислении слушателей ДПО-->
    @Override
    public Map<String, String> getData(FefuEnrollStuDPOExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu21.Об отчислении слушателей ДПО-->
    @Override
    public Map<String, String> getData(FefuExcludeStuDPOExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu22. Приказ по контингенту ДПО/ДО-->
    @Override
    public Map<String, String> getData(FefuOrderContingentStuDPOExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu23. О переводе слушателей ДПО-->
    @Override
    public Map<String, String> getData(FefuTransfStuDPOExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //
    // Списочные приказы
    //

    //    <!-- 2. ПЕРЕВОД С КУРСА НА КУРС -->
    @Override
    public Map<String, String> getData(CourseTransferStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourseNew().getTitle());
        data.put(PROP_EXTRACT_EDU_OU, extract.getFormativeOrgUnitStr() + " (" + extract.getTerritorialOrgUnitStr() + "), " + extract.getEducationLevelHighSchoolStr());
        return data;
    }

    //    <!-- 12. ПЕРЕВОД С КУРСА НА КУРС СТУДЕНТОВ, ИМЕЮЩИХ ЗАДОЛЖЕННОСТИ -->
    @Override
    public Map<String, String> getData(CourseTransferDebtorStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourseNew().getTitle());
        data.put(PROP_EXTRACT_EDU_OU, extract.getFormativeOrgUnitStr() + " (" + extract.getTerritorialOrgUnitStr() + "), " + extract.getEducationLevelHighSchoolStr());
        return data;
    }

    //    <!-- 3. СЧИТАТЬ НА ПРЕЖНЕМ КУРСЕ -->
    @Override
    public Map<String, String> getData(CourseNoChangeStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourse().getTitle());
        data.put(PROP_EXTRACT_EDU_OU, extract.getFormativeOrgUnitStr() + " (" + extract.getTerritorialOrgUnitStr() + "), " + extract.getEducationLevelHighSchoolStr());
        return data;
    }

    //    <!-- 4. ОТЧИСЛЕНИЕ -->
    @Override
    public Map<String, String> getData(ExcludeStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 15. ОТЧИСЛЕНИЕ ДЛЯ ОДНОЙ ГРУППЫ -->
    @Override
    public Map<String, String> getData(ExcludeSingGrpStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 14. ОТЧИСЛЕНИЕ ДЛЯ ОДНОЙ ГРУППЫ, ДИПЛОМ С ОТЛИЧИЕМ -->
    @Override
    public Map<String, String> getData(ExcludeSingGrpSuccStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 16. ОТЧИСЛЕНИЕ ДЛЯ ОДНОЙ ГРУППЫ (КАФЕДРАЛЬНЫЙ) -->
    @Override
    public Map<String, String> getData(ExcludeSingGrpCathStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 5. ДОПУСК К ГОСУДАРСТВЕННОЙ ИТОГОВОЙ АТТЕСТАЦИИ -->
    @Override
    public Map<String, String> getData(AdmitToStateExamsStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 6. О ВЫДАЧЕ ДИПЛОМА И ПРИСВОЕНИИ КВАЛИФИКАЦИИ -->
    @Override
    public Map<String, String> getData(GiveDiplStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 7. О ВЫДАЧЕ ДИПЛОМА С ОТЛИЧИЕМ И ПРИСВОЕНИИ КВАЛИФИКАЦИИ -->
    @Override
    public Map<String, String> getData(GiveDiplSuccessStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 8. О ПРЕДОСТАВЛЕНИИ КАНИКУЛ -->
    @Override
    public Map<String, String> getData(HolidayStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 9. О ВЫПУСКЕ СТУДЕНТОВ С ОТЛИЧИЕМ -->
    @Override
    public Map<String, String> getData(GraduateSuccessStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 10. О ВЫПУСКЕ СТУДЕНТОВ -->
    @Override
    public Map<String, String> getData(GraduateStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 11. ОБ ИСКЛЮЧЕНИИ СТУДЕНТОВ, НЕ ВЫПОЛНИВШИХ УЧЕБНЫЙ ПЛАН -->
    @Override
    public Map<String, String> getData(GradExcludeStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 18. О РАЗДЕЛЕНИИ СТУДЕНТОВ ПО СПЕЦИАЛИЗАЦИЯМ -->
    @Override
    public Map<String, String> getData(SplitStudentsStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourseStr());
        data.put(PROP_EXTRACT_EDU_OU, getEduOu(extract, extract.getEducationOrgUnitNew()));
        return data;
    }

    //    <!-- 19. О ПЕРЕВОДЕ НА УСКОРЕННОЕ (СОКРАЩЕННОЕ) ОБУЧЕНИЕ (ОБ ИЗМЕНЕНИИ УСЛОВИЯ ОСВОЕНИЯ) -->
    @Override
    public Map<String, String> getData(TransferDevConditionStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourse().getTitle());
        data.put(PROP_EXTRACT_EDU_OU, getEduOu(extract, extract.getEducationOrgUnitNew()));
        return data;
    }

    //    <!-- 13. О ПЕРЕВОДЕ СТУДЕНТОВ ТЕРРИТОРИАЛЬНОГО ПОДРАЗДЕЛЕНИЯ -->
    @Override
    public Map<String, String> getData(TerritorialTransferStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourse().getTitle());
        data.put(PROP_EXTRACT_EDU_OU, getEduOu(extract, extract.getEducationOrgUnitNew()));
        return data;
    }

    //    <!-- 17. О допуске к защите дипломной работы -->
    @Override
    public Map<String, String> getData(AdmitToPassDiplomaWorkStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 20. О назначении рецензентов -->
    @Override
    public Map<String, String> getData(SetReviewerStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 21. О смене научных руководителей -->
    @Override
    public Map<String, String> getData(ChangeScientificAdviserExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 22. О смене рецензентов -->
    @Override
    public Map<String, String> getData(ChangeReviewerStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 23. Об изменении темы дипломной работы -->
    @Override
    public Map<String, String> getData(ChangeDiplomaWorkTopicStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 24. Об утверждении тем дипломных работ и назначении научных руководителей -->
    @Override
    public Map<String, String> getData(SetDiplomaWorkTopicAndScientificAdviserStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 25. О закреплении за специализациями (профилями) и перераспределении в группу -->
    @Override
    public Map<String, String> getData(SplitStudentsGroupStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourseStr());
        data.put(PROP_EXTRACT_EDU_OU, getEduOu(extract, extract.getEducationOrgUnitNew()));
        return data;
    }

    //    <!-- 26. О допуске к сдаче государственного экзамена -->
    @Override
    public Map<String, String> getData(AdmitToStateExamsExtStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 27. О допуске к защите выпускной квалификационной работы -->
    @Override
    public Map<String, String> getData(AdmitToPassDiplomaWorkExtStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 28. О назначении старостами учебных групп -->
    @Override
    public Map<String, String> getData(AddGroupManagerStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 29. О выдаче диплома и присвоении квалификации (расширенный) -->
    @Override
    public Map<String, String> getData(GiveDiplExtStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 30. О выдаче диплома с отличием и присвоении квалификации (расширенный) -->
    @Override
    public Map<String, String> getData(GiveDiplSuccessExtStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 31. О переводе с курса на следующий курс (по направлениям подготовки) -->
    @Override
    public Map<String, String> getData(CourseTransferExtStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourseNew().getTitle());
        data.put(PROP_EXTRACT_EDU_OU, getEduOu(extract, null));
        return data;
    }

    //    <!-- 32. О назначении академической стипендии -->
    @Override
    public Map<String, String> getData(AcadGrantAssignStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - размер стипендии
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantSizeRuble()));
        return data;
    }

    //    <!-- 33. О назначении социальной стипендии -->
    @Override
    public Map<String, String> getData(SocGrantAssignStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - размер стипендии
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantSize()));
        return data;
    }

    //    <!-- 34. Об оказании материальной помощи -->
    @Override
    public Map<String, String> getData(FinAidAssignStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- 35. О распределении студентов 1 курса в группы -->
    @Override
    public Map<String, String> getData(SplitFirstCourseStudentsGroupStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourseStr());
        data.put(PROP_EXTRACT_EDU_OU, extract.getFormativeOrgUnitStr() + " (" + extract.getTerritorialOrgUnitStr() + "), " + extract.getEducationLevelsHighSchool().getDisplayableTitle());
        return data;
    }

    //    <!-- 36. Об однократном повышении государственной академической стипендии -->
    @Override
    public Map<String, String> getData(GrantRiseStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - размер стипендии
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantRiseAmount()));
        return data;
    }

    //    <!-- 37. О переводе с курса на следующий курс (по направлениям подготовки) со сменой территориального подразделения -->
    @Override
    public Map<String, String> getData(TerritorialCourseTransferStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourseNew().getTitle());
        data.put(PROP_EXTRACT_EDU_OU, getEduOu(extract, extract.getEducationOrgUnitNew()));

        return data;
    }

    //    <!-- 38. О закреплении за специализациями (профилями) -->
    @Override
    public Map<String, String> getData(SplitStudentsExtStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourseStr());
        data.put(PROP_EXTRACT_EDU_OU, getEduOu(extract, extract.getEducationOrgUnitNew()));
        return data;
    }

    //    <!-- 39. О назначении надбавки к государственной академической стипендии -->
    @Override
    public Map<String, String> getData(AcadGrantBonusAssignStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - размер стипендии
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantBonusSize()));
        return data;
    }

    //    <!-- 40. О переводе из группы в группу -->
    @Override
    public Map<String, String> getData(GroupTransferExtStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourseStr());
        data.put(PROP_EXTRACT_EDU_OU, extract.getFormativeOrgUnitStr() + " (" + extract.getTerritorialOrgUnitStr() + "), " + extract.getEducationLevelHighSchoolStr());
        return data;
    }

    //    <!-- 41. О направлении на практику в пределах ОУ -->
    @Override
    public Map<String, String> getData(SendPracticeInnerStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - Вид практики - Место - Форма собств. - Город - Сроки - Руководитель (от ДВФУ)
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PRACTICE_TYPE, extract.getPracticeKind());
        String practicePlace = "";
        if(!StringUtils.isEmpty(extract.getPracticeOrgUnitStr()))
            practicePlace += extract.getPracticeOrgUnitStr();
        else
            practicePlace += extract.getPracticeOrgUnit().getTitle() + ", " + extract.getPracticeOrgUnit().getAddress().getTitleWithFlat();
        data.put(PROP_EXTRACT_PRACTICE_PLACE, practicePlace);
        data.put(PROP_EXTRACT_PRACTICE_PERIOD, DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeBeginDate()) + "-" +
                DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeEndDate()) + ", " + extract.getPracticeDurationStr() + " нед.");
        if(!StringUtils.isEmpty(extract.getPracticeHeaderInnerStr()))
            data.put(PROP_EXTRACT_PRACTICE_HEADER, extract.getPracticeHeaderInnerStr());
        else
            data.put(PROP_EXTRACT_PRACTICE_HEADER, getFioNominativeStr(extract.getPracticeHeader(), extract.getPracticeHeaderDegree()));
        return data;
    }

    //    <!-- 42. О переводе студентов территориального подразделения -->
    @Override
    public Map<String, String> getData(TerritorialTransferExtStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourseStr());
        data.put(PROP_EXTRACT_EDU_OU, getEduOu(extract, extract.getEducationOrgUnitNew()));
        return data;
    }

    //    <!-- fefu1. О направлении на практику за пределами ОУ -->
    @Override
    public Map<String, String> getData(SendPracticeOutStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - Вид практики - Место - Форма собств. - Город - Сроки - Руководитель (от ДВФУ)
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PRACTICE_TYPE, extract.getPracticeKind());
        String practicePlace = extract.getPracticeExtOrgUnit().getTitle();
        if(!StringUtils.isEmpty(extract.getPracticePlace()))
            practicePlace += ", " + extract.getPracticePlace();
        else
            practicePlace += ", " + extract.getPracticeExtOrgUnit().getFactAddress().getTitleWithFlat();
        data.put(PROP_EXTRACT_PRACTICE_PLACE, practicePlace);
        data.put(PROP_EXTRACT_PRACTICE_PERIOD, DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeBeginDate()) + "-" +
                DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPracticeEndDate()) + ", " + extract.getPracticeDurationStr() + " нед.");
        if(!StringUtils.isEmpty(extract.getPracticeHeaderInnerStr()))
            data.put(PROP_EXTRACT_PRACTICE_HEADER, extract.getPracticeHeaderInnerStr());
        else
            data.put(PROP_EXTRACT_PRACTICE_HEADER, getFioNominativeStr(extract.getPracticeHeaderInner(), extract.getPracticeHeaderInnerDegree()));
        return data;
    }

    //    <!-- fefu2. О допуске к сдаче государственных экзаменов -->
    @Override
    public Map<String, String> getData(FefuAdmitToStateExamsStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu3. О допуске к защите выпускной квалификационной работы -->
    @Override
    public Map<String, String> getData(FefuAdmitToDiplomaStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu4. О назначении академической стипендии (вступительные испытания) -->
    @Override
    public Map<String, String> getData(FefuAcadGrantAssignStuEnrolmentListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность - размер стипендии
        addCourseWithEduOUStr(data, extract);
        data.put(PROP_EXTRACT_PAYMENT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getGrantSize()));
        return data;
    }

    //    <!-- fefu5. О переводе с курса на следующий курс -->
    @Override
    public Map<String, String> getData(FefuCourseTransferStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //   Новые : Курс - Институт, факультет, специальность -
        data.put(PROP_EXTRACT_COURSE, extract.getCourseNew().getTitle());
        data.put(PROP_EXTRACT_EDU_OU, getEduOu(extract, null));
        return data;
    }

    //    <!-- fefu6. О присвоении квалификации, выдаче диплома и отчислении в связи с окончанием университета -->
    @Override
    public Map<String, String> getData(FefuGiveDiplomaWithDismissStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu7. О присвоении квалификации, выдаче диплома с отличием и отчислении в связи с окончанием университета -->
    @Override
    public Map<String, String> getData(FefuGiveDiplomaSuccessWithDismissStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu8. О предоставлении каникул (ДВФУ) -->
    @Override
    public Map<String, String> getData(FefuHolidayStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu10. О зачислении на полное государственное обеспечение (списочный) -->
    @Override
    public Map<String, String> getData(FullStateMaintenanceEnrollmentStuListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        return data;
    }

    //   <!-- fefu15. О зачислении слушателей ДПО-->
    @Override
    public Map<String, String> getData(FefuEnrollStuDPOListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu16. Об отчислении слушателей ДПО-->
    @Override
    public Map<String, String> getData(FefuExcludeStuDPOListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu18. Приказ по контингенту ДПО/ДО-->
    @Override
    public Map<String, String> getData(FefuOrderContingentStuDPOListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    //    <!-- fefu17. О переводе слушателей ДПО-->
    @Override
    public Map<String, String> getData(FefuTransfStuDPOListExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        addCourseWithEduOUStr(data, extract);
        return data;
    }

    // Прочие приказы
    @Override
    public Map<String, String> getData(OtherStudentExtract extract)
    {
        Map<String, String> data = getBaseData(extract);
        //    Курс - Институт, факультет, специальность
        if (extract.getCourse() != null)
            data.put(PROP_EXTRACT_COURSE, extract.getCourse().getTitle());
        StringBuilder builder = new StringBuilder();
        if (extract.getFaculty() != null)
        {
            builder.append(extract.getFaculty());
            if (extract.getSpeciality() != null)
                builder.append(", ");
        }
        builder.append(extract.getSpeciality() != null? extract.getSpeciality():"");

          builder.append(extract.getProfile()!= null? (builder.length()>0? ", ":"") + extract.getProfile():"");
        data.put(PROP_EXTRACT_EDU_OU, builder.toString());
        return data;
    }

    private void addCourseWithEduOUStr(Map<String, String> data, AbstractStudentExtract extract)
    {
        data.put(PROP_EXTRACT_COURSE, extract.getCourseStr());
        data.put(PROP_EXTRACT_EDU_OU, extract.getFormativeOrgUnitStr() + " (" + extract.getTerritorialOrgUnitStr() + "), " + extract.getEducationLevelHighSchoolStr());
    }

    private void addCourseWithEduOUNew(Map<String, String> data, CommonStuExtract extract)
    {
        if(null != extract.getCourseNew())
            data.put(PROP_EXTRACT_COURSE, extract.getCourseNew().getTitle());
        else
            data.put(PROP_EXTRACT_COURSE, extract.getCourseStr());
        EducationOrgUnit eduOU = extract.getEducationOrgUnitNew();
        if(null != eduOU)
        {
            data.put(PROP_EXTRACT_EDU_OU, eduOU.getFormativeOrgUnit().getShortTitle() + " (" + eduOU.getTerritorialOrgUnit().getTerritorialShortTitle() + "), " + eduOU.getEducationLevelHighSchool().getDisplayableTitle());
        }
        else
            data.put(PROP_EXTRACT_EDU_OU, extract.getFormativeOrgUnitStr() + " (" + extract.getTerritorialOrgUnitStr() + "), " + extract.getEducationLevelHighSchoolStr());
    }

    private String getEduOu(AbstractStudentExtract extract, EducationOrgUnit educationOrgUnit)
    {
        if(null != educationOrgUnit)
        {
            return educationOrgUnit.getFormativeOrgUnit().getShortTitle() + " (" + educationOrgUnit.getTerritorialOrgUnit().getTerritorialShortTitle() + "), " + educationOrgUnit.getEducationLevelHighSchool().getDisplayableTitle();
        }
        else
            return extract.getFormativeOrgUnitStr() + " (" + extract.getTerritorialOrgUnitStr() + "), " + extract.getEducationLevelHighSchoolStr();
    }

    private String getFioNominativeStr(EmployeePost employeePost, PersonAcademicDegree prevDegree)
    {
        StringBuilder fioStrBuilder = new StringBuilder();

        fioStrBuilder.append(getDegreeShortTitleWithDots(prevDegree));

        String practiceHeaderPostNT = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        String practiceHeaderNominative = !StringUtils.isEmpty(practiceHeaderPostNT) ? practiceHeaderPostNT :
                employeePost.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(practiceHeaderNominative))
            fioStrBuilder.append(StringUtils.isEmpty(fioStrBuilder.toString()) ? "" : " ").append(practiceHeaderNominative.toLowerCase());

        fioStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(employeePost.getPerson().getIdentityCard(), GrammaCase.NOMINATIVE));

        return fioStrBuilder.toString();
    }

    private String getDegreeShortTitleWithDots(PersonAcademicDegree prevDegree)
    {
        String degreeShortTitleWithDots = "";
        if (null != prevDegree)
        {
            String[] degreeTitle = prevDegree.getAcademicDegree().getTitle().toLowerCase().split(" ");
            StringBuilder shortDegreeTitle = new StringBuilder();
            for (String deg : Lists.newArrayList(degreeTitle))
            {
                shortDegreeTitle.append(deg.charAt(0)).append(".");
            }
            degreeShortTitleWithDots += shortDegreeTitle;
        }
        return degreeShortTitleWithDots;
    }
}
