package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О зачислении в порядке перевода
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEduTransferEnrolmentStuExtractGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract";
    public static final String ENTITY_NAME = "fefuEduTransferEnrolmentStuExtract";
    public static final int VERSION_HASH = 2022475031;
    private static IEntityMeta ENTITY_META;

    public static final String P_FROM_SCHOOL = "fromSchool";
    public static final String P_LIQUIDATE_EDU_PLAN_DIFFERENCE = "liquidateEduPlanDifference";
    public static final String P_LIQUIDATION_DEADLINE_DATE = "liquidationDeadlineDate";
    public static final String P_PREV_ENTRY_DATE = "prevEntryDate";

    private String _fromSchool;     // Отчислен из ОУ
    private boolean _liquidateEduPlanDifference;     // Ликвидировать разницу в учебных планах
    private Date _liquidationDeadlineDate;     // Срок ликвидации разницы в учебных планах
    private Date _prevEntryDate;     // Предыдущая дата зачисления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Отчислен из ОУ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFromSchool()
    {
        return _fromSchool;
    }

    /**
     * @param fromSchool Отчислен из ОУ. Свойство не может быть null.
     */
    public void setFromSchool(String fromSchool)
    {
        dirty(_fromSchool, fromSchool);
        _fromSchool = fromSchool;
    }

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     */
    @NotNull
    public boolean isLiquidateEduPlanDifference()
    {
        return _liquidateEduPlanDifference;
    }

    /**
     * @param liquidateEduPlanDifference Ликвидировать разницу в учебных планах. Свойство не может быть null.
     */
    public void setLiquidateEduPlanDifference(boolean liquidateEduPlanDifference)
    {
        dirty(_liquidateEduPlanDifference, liquidateEduPlanDifference);
        _liquidateEduPlanDifference = liquidateEduPlanDifference;
    }

    /**
     * @return Срок ликвидации разницы в учебных планах.
     */
    public Date getLiquidationDeadlineDate()
    {
        return _liquidationDeadlineDate;
    }

    /**
     * @param liquidationDeadlineDate Срок ликвидации разницы в учебных планах.
     */
    public void setLiquidationDeadlineDate(Date liquidationDeadlineDate)
    {
        dirty(_liquidationDeadlineDate, liquidationDeadlineDate);
        _liquidationDeadlineDate = liquidationDeadlineDate;
    }

    /**
     * @return Предыдущая дата зачисления.
     */
    public Date getPrevEntryDate()
    {
        return _prevEntryDate;
    }

    /**
     * @param prevEntryDate Предыдущая дата зачисления.
     */
    public void setPrevEntryDate(Date prevEntryDate)
    {
        dirty(_prevEntryDate, prevEntryDate);
        _prevEntryDate = prevEntryDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuEduTransferEnrolmentStuExtractGen)
        {
            setFromSchool(((FefuEduTransferEnrolmentStuExtract)another).getFromSchool());
            setLiquidateEduPlanDifference(((FefuEduTransferEnrolmentStuExtract)another).isLiquidateEduPlanDifference());
            setLiquidationDeadlineDate(((FefuEduTransferEnrolmentStuExtract)another).getLiquidationDeadlineDate());
            setPrevEntryDate(((FefuEduTransferEnrolmentStuExtract)another).getPrevEntryDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEduTransferEnrolmentStuExtractGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEduTransferEnrolmentStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuEduTransferEnrolmentStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "fromSchool":
                    return obj.getFromSchool();
                case "liquidateEduPlanDifference":
                    return obj.isLiquidateEduPlanDifference();
                case "liquidationDeadlineDate":
                    return obj.getLiquidationDeadlineDate();
                case "prevEntryDate":
                    return obj.getPrevEntryDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "fromSchool":
                    obj.setFromSchool((String) value);
                    return;
                case "liquidateEduPlanDifference":
                    obj.setLiquidateEduPlanDifference((Boolean) value);
                    return;
                case "liquidationDeadlineDate":
                    obj.setLiquidationDeadlineDate((Date) value);
                    return;
                case "prevEntryDate":
                    obj.setPrevEntryDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "fromSchool":
                        return true;
                case "liquidateEduPlanDifference":
                        return true;
                case "liquidationDeadlineDate":
                        return true;
                case "prevEntryDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "fromSchool":
                    return true;
                case "liquidateEduPlanDifference":
                    return true;
                case "liquidationDeadlineDate":
                    return true;
                case "prevEntryDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "fromSchool":
                    return String.class;
                case "liquidateEduPlanDifference":
                    return Boolean.class;
                case "liquidationDeadlineDate":
                    return Date.class;
                case "prevEntryDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEduTransferEnrolmentStuExtract> _dslPath = new Path<FefuEduTransferEnrolmentStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEduTransferEnrolmentStuExtract");
    }
            

    /**
     * @return Отчислен из ОУ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract#getFromSchool()
     */
    public static PropertyPath<String> fromSchool()
    {
        return _dslPath.fromSchool();
    }

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract#isLiquidateEduPlanDifference()
     */
    public static PropertyPath<Boolean> liquidateEduPlanDifference()
    {
        return _dslPath.liquidateEduPlanDifference();
    }

    /**
     * @return Срок ликвидации разницы в учебных планах.
     * @see ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract#getLiquidationDeadlineDate()
     */
    public static PropertyPath<Date> liquidationDeadlineDate()
    {
        return _dslPath.liquidationDeadlineDate();
    }

    /**
     * @return Предыдущая дата зачисления.
     * @see ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract#getPrevEntryDate()
     */
    public static PropertyPath<Date> prevEntryDate()
    {
        return _dslPath.prevEntryDate();
    }

    public static class Path<E extends FefuEduTransferEnrolmentStuExtract> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<String> _fromSchool;
        private PropertyPath<Boolean> _liquidateEduPlanDifference;
        private PropertyPath<Date> _liquidationDeadlineDate;
        private PropertyPath<Date> _prevEntryDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Отчислен из ОУ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract#getFromSchool()
     */
        public PropertyPath<String> fromSchool()
        {
            if(_fromSchool == null )
                _fromSchool = new PropertyPath<String>(FefuEduTransferEnrolmentStuExtractGen.P_FROM_SCHOOL, this);
            return _fromSchool;
        }

    /**
     * @return Ликвидировать разницу в учебных планах. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract#isLiquidateEduPlanDifference()
     */
        public PropertyPath<Boolean> liquidateEduPlanDifference()
        {
            if(_liquidateEduPlanDifference == null )
                _liquidateEduPlanDifference = new PropertyPath<Boolean>(FefuEduTransferEnrolmentStuExtractGen.P_LIQUIDATE_EDU_PLAN_DIFFERENCE, this);
            return _liquidateEduPlanDifference;
        }

    /**
     * @return Срок ликвидации разницы в учебных планах.
     * @see ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract#getLiquidationDeadlineDate()
     */
        public PropertyPath<Date> liquidationDeadlineDate()
        {
            if(_liquidationDeadlineDate == null )
                _liquidationDeadlineDate = new PropertyPath<Date>(FefuEduTransferEnrolmentStuExtractGen.P_LIQUIDATION_DEADLINE_DATE, this);
            return _liquidationDeadlineDate;
        }

    /**
     * @return Предыдущая дата зачисления.
     * @see ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract#getPrevEntryDate()
     */
        public PropertyPath<Date> prevEntryDate()
        {
            if(_prevEntryDate == null )
                _prevEntryDate = new PropertyPath<Date>(FefuEduTransferEnrolmentStuExtractGen.P_PREV_ENTRY_DATE, this);
            return _prevEntryDate;
        }

        public Class getEntityClass()
        {
            return FefuEduTransferEnrolmentStuExtract.class;
        }

        public String getEntityName()
        {
            return "fefuEduTransferEnrolmentStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
