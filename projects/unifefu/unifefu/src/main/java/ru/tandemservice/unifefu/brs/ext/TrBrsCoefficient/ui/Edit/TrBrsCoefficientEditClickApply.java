package ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.Edit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.IBrsCoefficientRowWrapper;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.Edit.TrBrsCoefficientEditUI;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBaseUI;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: newdev
 * Date: 22.12.13
 * Time: 18:31
 * To change this template use File | Settings | File Templates.
 */
public class TrBrsCoefficientEditClickApply extends NamedUIAction {

    protected TrBrsCoefficientEditClickApply(String name) {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if(presenter instanceof TrBrsCoefficientEditUI)
        {
            Map<String, Object> values = new HashMap<>();
            TrBrsCoefficientEditUI prsntr = (TrBrsCoefficientEditUI) presenter;
            TrBrsCoefficientEditBaseUI brsCoefEditComponent = (TrBrsCoefficientEditBaseUI) prsntr.getSupport().getChildrenUI().entrySet().iterator().next().getValue();
            for (IBrsCoefficientRowWrapper row : brsCoefEditComponent.getRowList()) {
                if (row.getDef().getUserCode().startsWith("range_to"))
                    values.put(row.getDef().getUserCode(), row.getValue());
            }
            if (values.size() > 0) {
                Double point5, point4, point3;
                if (prsntr.getOwner() instanceof TrJournal) {
                    TrJournal trJournal = (TrJournal) prsntr.getOwner();
                    EppFControlActionGroup fControlActionGroup = TrBrsCoefficientManager.instance().coefDao().getFcaGroup(trJournal);
                    boolean isExam = EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(fControlActionGroup.getCode());
                    if (isExam) {
                        point5 = getCoefValue(FefuBrs.RANGE_5_CODE, values);
                        point4 = getCoefValue(FefuBrs.RANGE_4_CODE, values);
                        point3 = getCoefValue(FefuBrs.RANGE_3_CODE, values);
                        if (point3 != null && point4 != null && point5 !=null) {
                            if (point5 <= point4)
                                throw new ApplicationException("Неправильно указаны диапазоны. 'Отл' должен быть больше чем 'Хор'");
                            if (point5 <= point3)
                                throw new ApplicationException("Неправильно указаны диапазоны. 'Отл' должен быть больше чем 'Удовл'");
                            if (point4 <= point3)
                                throw new ApplicationException("Неправильно указаны диапазоны. 'Хор' должен быть больше чем 'Удовл'");
                        }
                    }
                } else {
                    point5 = getCoefValue(FefuBrs.RANGE_5_DEF_CODE, values);
                    point4 = getCoefValue(FefuBrs.RANGE_4_DEF_CODE, values);
                    point3 = getCoefValue(FefuBrs.RANGE_3_DEF_CODE, values);
                    if (point3 != null && point4 != null && point5 !=null) {
                        if (point5 <= point4)
                            throw new ApplicationException("Неправильно указаны диапазоны. 'Отл' должен быть больше чем 'Хор'");
                        if (point5 <= point3)
                            throw new ApplicationException("Неправильно указаны диапазоны. 'Отл' должен быть больше чем 'Удовл'");
                        if (point4 <= point3)
                            throw new ApplicationException("Неправильно указаны диапазоны. 'Хор' должен быть больше чем 'Удовл'");
                    }
                }
            }
            prsntr.onClickApply();
        }
    }

    private Double getCoefValue(String coef, Map<String, Object> values) {
        if (values.get(coef) instanceof Double)
            return (Double) values.get(coef);
        else if (values.get(coef) instanceof Long)
            return ((Long) values.get(coef)).doubleValue();
        return 0.0;
    }
}