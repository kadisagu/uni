/* $Id$ */

package ru.tandemservice.unifefu.component.listextract.fefu12.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative.IAbstractParagraphAddEditAlternativeDAO;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public interface IDAO extends IAbstractParagraphAddEditAlternativeDAO<FefuStuffCompensationStuListExtract, Model>
{
}
