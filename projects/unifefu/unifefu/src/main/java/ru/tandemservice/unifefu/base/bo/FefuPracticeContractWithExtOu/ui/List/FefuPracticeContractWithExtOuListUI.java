/* $Id: FefuPracticeContractWithExtOuListUI.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.List;

import org.tandemframework.caf.report.ExcelListDataSourcePrinter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.FefuPracticeContractWithExtOuManager;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.AddEdit.FefuPracticeContractWithExtOuAddEdit;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.View.FefuPracticeContractWithExtOuView;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/28/13
 * Time: 6:01 PM
 */
public class FefuPracticeContractWithExtOuListUI extends UIPresenter
{

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if("jurCtrDS".equals(dataSource.getName()))
        {
            dataSource.put("externalOrgUnit", _uiSettings.getEntityId("externalOrgUnit"));
            dataSource.put("showAllWoExtOuId", Boolean.TRUE);
        }
        if(FefuPracticeContractWithExtOuList.PRACTICE_CONTRACT_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "contractNum", "internalNum", "contractDateFrom", "contractDateTo", "extOuTitle", "headerExtOu", "archival", "checked","abbreviatedNameOrganization","actualAddress","legalAddress"));
        }
    }

    public BaseSearchListDataSource getPracticeContractDS()
    {
        return _uiConfig.getDataSource(FefuPracticeContractWithExtOuList.PRACTICE_CONTRACT_DS);
    }

    public String getCurrentItemDeleteAlert()
    {
        FefuPracticeContractWithExtOu contract = getPracticeContractDS().getCurrent();
        return "Вы действительно хотите удалить договор №" + contract.getContractNum() + " за " + DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getContractDate());
    }

    public void onAddContract()
    {
        _uiActivation.asDesktopRoot(FefuPracticeContractWithExtOuAddEdit.class).activate();
    }

    public void onClickView()
    {
        _uiActivation.asDesktopRoot(FefuPracticeContractWithExtOuView.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onChangeChecked()
    {
        FefuPracticeContractWithExtOu contract = getPracticeContractDS().getRecordById(getListenerParameterAsLong());
        if(contract.isChecked())
            contract.setChecked(false);
        else
            contract.setChecked(true);
        DataAccessServices.dao().update(contract);
    }

    public void onChangeArchival()
    {
        FefuPracticeContractWithExtOu contract = getPracticeContractDS().getRecordById(getListenerParameterAsLong());
        if(contract.isArchival())
            contract.setArchival(false);
        else
            contract.setArchival(true);
        DataAccessServices.dao().update(contract);
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asDesktopRoot(FefuPracticeContractWithExtOuAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public ExcelListDataSourcePrinter getExcelPrinter()
    {
        return new ExcelListDataSourcePrinter(getPracticeContractDS().getLabel(), getPracticeContractDS().getLegacyDataSource()).setupPrintAll();
    }
}
