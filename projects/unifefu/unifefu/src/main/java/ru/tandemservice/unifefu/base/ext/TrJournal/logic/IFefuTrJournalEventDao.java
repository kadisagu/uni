package ru.tandemservice.unifefu.base.ext.TrJournal.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;

public interface IFefuTrJournalEventDao extends ITrJournalEventDao
{
    /**
     * Копирование события в журнале
     *
     * @param sourceEventId - id исходного события
     * @return id нового события
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Long copyEventWithTheme(Long sourceEventId);

    /**
     * Создание событий для групп угс
     *
     * @param journalEventId событие в журнале
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void createEduGroupEventsForJournalEvent(Long journalEventId);
}
