package ru.tandemservice.unifefu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.commonbase.base.entity.IDeclinable;
import ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Виды практик студентов (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuStudentPracticeGen extends EntityBase
 implements IDeclinable, INaturalIdentifiable<FefuStudentPracticeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice";
    public static final String ENTITY_NAME = "fefuStudentPractice";
    public static final int VERSION_HASH = 1774245901;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_IS_PARENT = "isParent";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private boolean _isParent = false;     // Элемент верхнего уровня
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Элемент верхнего уровня. Свойство не может быть null.
     */
    @NotNull
    public boolean isIsParent()
    {
        return _isParent;
    }

    /**
     * @param isParent Элемент верхнего уровня. Свойство не может быть null.
     */
    public void setIsParent(boolean isParent)
    {
        dirty(_isParent, isParent);
        _isParent = isParent;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuStudentPracticeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((FefuStudentPractice)another).getCode());
            }
            setIsParent(((FefuStudentPractice)another).isIsParent());
            setTitle(((FefuStudentPractice)another).getTitle());
        }
    }

    public INaturalId<FefuStudentPracticeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<FefuStudentPracticeGen>
    {
        private static final String PROXY_NAME = "FefuStudentPracticeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuStudentPracticeGen.NaturalId) ) return false;

            FefuStudentPracticeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuStudentPracticeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuStudentPractice.class;
        }

        public T newInstance()
        {
            return (T) new FefuStudentPractice();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "isParent":
                    return obj.isIsParent();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "isParent":
                    obj.setIsParent((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "isParent":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "isParent":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "isParent":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuStudentPractice> _dslPath = new Path<FefuStudentPractice>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuStudentPractice");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Элемент верхнего уровня. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice#isIsParent()
     */
    public static PropertyPath<Boolean> isParent()
    {
        return _dslPath.isParent();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends FefuStudentPractice> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _isParent;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(FefuStudentPracticeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Элемент верхнего уровня. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice#isIsParent()
     */
        public PropertyPath<Boolean> isParent()
        {
            if(_isParent == null )
                _isParent = new PropertyPath<Boolean>(FefuStudentPracticeGen.P_IS_PARENT, this);
            return _isParent;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuStudentPractice#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FefuStudentPracticeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return FefuStudentPractice.class;
        }

        public String getEntityName()
        {
            return "fefuStudentPractice";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
