/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.validator;

/**
 * Валидатор числа с плавающей точкой: проверяет, что число больше или равно заданному.
 * @author Alexander Zhebko
 * @since 22.07.2013
 */
public class GEDoubleValidator extends Validator<Double>
{
    private final double minValue;

    public GEDoubleValidator(double minValue)
    {
        this.minValue = minValue;
    }

    @Override
    protected boolean validateValue(Double value)
    {
        return value >= minValue;
    }

    @Override
    public String getInvalidMessage()
    {
        return "Значение меньше " + minValue + ".";
    }
}