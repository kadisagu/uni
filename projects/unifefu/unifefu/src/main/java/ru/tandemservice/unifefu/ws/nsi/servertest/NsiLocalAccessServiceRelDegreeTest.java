/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import ru.tandemservice.unifefu.ws.nsi.datagram.RelDegreeType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import javax.xml.namespace.QName;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceRelDegreeTest extends NsiLocalAccessBaseTest
{
    public static final String[][] RETRIEVE_WHOLE_PARAMS = new String[][]{{null}};
    public static final String[][] RETRIEVE_NON_EXISTS_PARAMS = new String[][]{{"aaaaaaaa-3202-3c23-8a77-073a1de4f525"}};
    public static final String[][] RETRIEVE_SINGLE_PARAMS = new String[][]{{"cc5d937a-3202-3c23-8a77-073a1de4f525"}};
    public static final String[][] RETRIEVE_FEW_PARAMS = new String[][]{{"cc5d937a-3202-3c23-8a77-073a1de4f525"}, {"73e83fd0-ba09-33de-a0b6-f8ffb1a5ad66"}, {"aaaaaaaa-3202-3c23-8a77-073a1de4f525"}, {"1b8a033b-9b5d-37c6-a12c-922f35afd4e0"}};

    public static final String[][] INSERT_NEW_PARAMS = new String[][]{{"cc5d937a-3202-3c23-8a77-xxxxxxxxxxxX", "Мужик", "77"}};
    public static final String[][] INSERT_NEW_ANALOG_PARAMS = new String[][]{{"cc5d937a-3202-3c23-8a77-xxxxxxxxxxxY", "Пасынок", "42"}};
    public static final String[][] INSERT_NEW_GUID_EXISTS_PARAMS = new String[][]{{"cc5d937a-3202-3c23-8a77-xxxxxxxxxxxY", "Пасынок3", "423"}};
    public static final String[][] INSERT_NEW_MERGE_GUID_EXISTS_PARAMS = new String[][]{{"cc5d937a-3202-3c23-8a77-xxxxxxxxxxxZ", "Пасынок4", "423", "cc5d937a-3202-3c23-8a77-xxxxxxxxxxxZ; cc5d937a-3202-3c23-8a77-xxxxxxxxxxxY; 90800349-619a-11e0-a335-xxxxxxxxxx40"}};
    public static final String[] INSERT_MASS_TEMPLATE_PARAMS = new String[]{"Тест"};

    public static final String[][] DELETE_EMPTY_PARAMS = new String[][]{{null}};
    public static final String[][] DELETE_NON_EXISTS_PARAMS = new String[][]{{"422e2e35-89ac-3c20-8d5b-9225e0e24dfX"}};
    public static final String[][] DELETE_SINGLE_PARAMS = new String[][]{{"cc5d937a-3202-3c23-8a77-xxxxxxxxxxxX"}};
    public static final String[][] DELETE_NON_DELETABLE_PARAMS = new String[][]{{"422e2e35-89ac-3c20-8d5b-9225e0e24df0"}};
    public static final String[][] DELETE_MASS_PARAMS = new String[][]{{"422e2e35-89ac-3c20-8d5b-9225e0e24df0"}};

    @Override
    protected INsiEntity createNsiEntity(String[] fieldValues)
    {
        RelDegreeType entity = NsiLocalAccessServiceTestUtil.FACTORY.createRelDegreeType();
        if (null != fieldValues)
        {
            if (fieldValues.length > 0) entity.setID(fieldValues[0]);
            if (fieldValues.length > 1) entity.setRelDegreeName(fieldValues[1]);
            if (fieldValues.length > 2) entity.setRelDegreeID(fieldValues[2]);
            if (fieldValues.length > 3 && null != fieldValues[3])
                entity.getOtherAttributes().put(new QName("mergeDuplicates"), fieldValues[3]);
        }
        return entity;
    }
}