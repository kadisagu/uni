/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SessionDocument;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexey Lopatin
 * @since 12.05.2015
 */
@Configuration
public class SessionDocumentExtManager extends BusinessObjectExtensionManager
{
}