/* $Id$ */
package ru.tandemservice.unifefu.brs.base;

import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;

import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 23.05.2014
 */
@Input({
               @Bind(key = BaseFefuReportAddUI.YEAR_PART_BIND_PARAM, binding = BaseFefuReportAddUI.YEAR_PART_BIND_PARAM)
       })
public abstract class BaseFefuReportAddUI<PARAMS extends BaseFefuBrsReportParams> extends BaseBrsReportPresenter
{
    public static final String YEAR_PART_BIND_PARAM = "yearPart";
    private EppYearPart _yearPart;
    private boolean _isOrgUnitResponsibility;

    public EppYearPart getYearPart()
    {
        return _yearPart;
    }

    public void setYearPart(EppYearPart yearPart)
    {
        _yearPart = yearPart;
    }

    private PARAMS _reportSettings;

    @SuppressWarnings("unchecked")
    public BaseFefuReportAddUI()
    {
        try
        {
            Class<PARAMS> paramsClass = (Class<PARAMS>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            _reportSettings = paramsClass.newInstance();
        }
        catch (InstantiationException | IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onComponentRefresh()
    {
        // Является ли орг.юнит, на котором строится отчет ответственным подразделение.
        // Он таким является, если он есть в настройке "Подразделения, работающие с УП и РУП" с признаком "Может быть читающим" и не является выпускающим.
        _isOrgUnitResponsibility = getOrgUnitId() != null &&
                IUniBaseDao.instance.get().existsEntity(EppTutorOrgUnit.class, EppTutorOrgUnit.L_ORG_UNIT, getOrgUnitId()) &&
                !UniDaoFacade.getOrgstructDao().isOrgUnitHasKind(getOrgUnit(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING);

        fillDefaults(getReportSettings());
    }

    protected void fillDefaults(PARAMS reportSettings)
    {
        reportSettings.setYearPart(getYearPart() != null ?  getYearPart() : FefuBrsReportManager.instance().dao().lastYearPart());
        reportSettings.setOrgUnit(getOrgUnit());
        reportSettings.setResponsibilityOrgUnit(getResponsibilityOrgUnit());
    }

    public PARAMS getReportSettings()
    {
        return _reportSettings;
    }

    public OrgUnit getResponsibilityOrgUnit()
    {
        return _isOrgUnitResponsibility ? getOrgUnit() : null;
    }

    public Validator getFormativeOrgUnitValidator()
    {
        return _isOrgUnitResponsibility ? null : new Required();
    }

    public OrgUnit getCurrentFormativeOrgUnit()
    {
        return getOrgUnit() != null && !_isOrgUnitResponsibility ? FefuBrsReportManager.instance().dao().getParentFormativeOrgUnit(getOrgUnit()) : null;
    }

    public List<OrgUnit> getCurrentFormativeOrgUnitAsList()
    {
        return getCurrentFormativeOrgUnit() != null ? Arrays.asList(getCurrentFormativeOrgUnit()) : null;
    }

}