/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19.ParagraphAddEdit;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 15.05.2015
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuTransfSpecialityStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setCompensationTypeList(UniDaoFacade.getCoreDao().getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));


        FefuTransfSpecialityStuListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            if (null != model.getParagraphId())
            {
//                model.setCompensationType(firstExtract.getEntity().getCompensationType());
                model.setEducationLevelsHighSchool(firstExtract.getEducationOrgUnitNew().getEducationLevelHighSchool());
            }
            model.setGroup(firstExtract.getEntity().getGroup());
            model.setCourse(firstExtract.getEntity().getGroup().getCourse());
            model.setDevelopForm(firstExtract.getEntity().getGroup().getEducationOrgUnit().getDevelopForm());
            model.setDevelopCondition(firstExtract.getEntity().getGroup().getEducationOrgUnit().getDevelopCondition());
            model.setDevelopTech(firstExtract.getEntity().getGroup().getEducationOrgUnit().getDevelopTech());
            model.setDevelopPeriod(firstExtract.getEntity().getGroup().getEducationOrgUnit().getDevelopPeriod());
            model.setOldEduLevelHighSchool(firstExtract.getEntity().getGroup().getEducationOrgUnit().getEducationLevelHighSchool());

            model.setGroupNew(firstExtract.getGroupNew());
            model.setParentEduLevel(EducationOrgUnitUtil.getParentLevel(model.getGroupNew().getEducationOrgUnit().getEducationLevelHighSchool()));

        }

        model.setSpecializationsListModel(new ExtEducationLevelsHighSchoolSelectModel(model, model).dependFromFUTS(true).showParentLevel(true));
        model.setGroupNewListModel(
                new GroupSelectModel(model, model)
                        .hideInactiveProgramSubject(true)
                        .dependFromFUTS(true)
        );

    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (null != model.getDevelopForm())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developForm().s(), model.getDevelopForm()));
        if (null != model.getDevelopCondition())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developCondition().s(), model.getDevelopCondition()));
        if (null != model.getDevelopTech())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developTech().s(), model.getDevelopTech()));
        if (null != model.getDevelopPeriod())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().developPeriod().s(), model.getDevelopPeriod()));
        if (null != model.getCompensationType())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.compensationType().s(), model.getCompensationType()));
    }


    @Override
    protected void fillExtract(FefuTransfSpecialityStuListExtract extract, Student student, Model model)
    {
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
        extract.setGroupOld(student.getGroup());
        extract.setGroupNew(model.getGroupNew());
        EducationOrgUnit newOrgUnit = UniDaoFacade.getEducationLevelDao().getEducationOrgUnit(model);
        if (newOrgUnit == null)
            throw new ApplicationException("Не найдены параметры обучения студента, удовлетворяющие заданным условиям");
        extract.setEducationOrgUnitNew(newOrgUnit);
        extract.setCaptainTransfer(model.isCaptainTransfer());
    }
    @Override
    protected FefuTransfSpecialityStuListExtract createNewInstance(Model model)
    {
        return new FefuTransfSpecialityStuListExtract();
    }

}