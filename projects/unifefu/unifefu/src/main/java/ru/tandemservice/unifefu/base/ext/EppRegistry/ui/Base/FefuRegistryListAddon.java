/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EppRegistry.ui.Base;

import org.tandemframework.caf.report.ExcelListDataSourcePrinter;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractList;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractListUI;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AttestationList.EppRegistryAttestationListUI;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.DisciplineList.EppRegistryDisciplineListUI;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.PracticeList.EppRegistryPracticeListUI;
import ru.tandemservice.unifefu.base.ext.EppRegistry.logic.print.FefuRegistryExcelListDataSourcePrinter;

/**
 * @author Irina Ugfeld
 * @since 15.03.2016
 */
public class FefuRegistryListAddon extends UIAddon {
    public static final String NAME = "fefuRegistryListAddon";

    public static final String SETTING_EPP_REGISTRY_STRUCTURE = "eppRegistryStructure";
    public static final String SETTING_EPP_F_CONTROL_ACTION_TYPE = "eppFControlActionType";
    public static final String SETTING_EDU_PROGRAM_KIND = "eduProgramKind";
    public static final String SETTING_SUBJECT_INDEX = "subjectIndex";
    public static final String SETTING_EDU_ORG_UNIT = "eduOrgUnit";
    public static final String SETTING_LABOR_ON_HOUR = "laborOnHour";
    public static final String SETTING_LABOR_AS_DOUBLE = "laborAsDouble";
    public static final String SETTING_PARTS = "parts";
    public static final String SETTING_SEND_TO_UMU = "sendToUMU";


    private ISelectModel sendToUMUModel;


    public FefuRegistryListAddon(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    public ExcelListDataSourcePrinter getExcelPrinter() {
        String fileName = "Записи реестра";
        if (getPresenter() instanceof EppRegistryDisciplineListUI)
            fileName += " дисциплин";
        else if (getPresenter() instanceof EppRegistryAttestationListUI)
            fileName += " мероприятий ИГА";
        else if (getPresenter() instanceof EppRegistryPracticeListUI)
            fileName += " практик";

        return new FefuRegistryExcelListDataSourcePrinter(fileName, ((BaseSearchListDataSource) getPresenterConfig()
                .getDataSource(EppRegistryAbstractList.ELEMENT_DS))
                .getLegacyDataSource()).setupPrintAll();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(SETTING_EPP_REGISTRY_STRUCTURE, _presenter.getSettings().get(SETTING_EPP_REGISTRY_STRUCTURE));
        dataSource.put(SETTING_EPP_F_CONTROL_ACTION_TYPE, _presenter.getSettings().get(SETTING_EPP_F_CONTROL_ACTION_TYPE));
        dataSource.put(SETTING_EDU_PROGRAM_KIND, _presenter.getSettings().get(SETTING_EDU_PROGRAM_KIND));
        dataSource.put(SETTING_SUBJECT_INDEX, _presenter.getSettings().get(SETTING_SUBJECT_INDEX));
        dataSource.put(SETTING_EDU_ORG_UNIT, _presenter.getSettings().get(SETTING_EDU_ORG_UNIT));
        dataSource.put(SETTING_LABOR_ON_HOUR, _presenter.getSettings().get(SETTING_LABOR_ON_HOUR));
        dataSource.put(SETTING_LABOR_AS_DOUBLE, _presenter.getSettings().get(SETTING_LABOR_AS_DOUBLE));
        dataSource.put(SETTING_PARTS, _presenter.getSettings().get(SETTING_PARTS));
        dataSource.put(SETTING_SEND_TO_UMU, _presenter.getSettings().get(SETTING_SEND_TO_UMU));

    }

    public boolean isNotFromOrgUnit() {
        return !((EppRegistryAbstractListUI) getPresenter()).isFromOrgUnit();
    }

    public ISelectModel getSendToUMUModel() {
        return sendToUMUModel;
    }

    public void setSendToUMUModel(ISelectModel sendToUMUModel) {
        this.sendToUMUModel = sendToUMUModel;
    }
}