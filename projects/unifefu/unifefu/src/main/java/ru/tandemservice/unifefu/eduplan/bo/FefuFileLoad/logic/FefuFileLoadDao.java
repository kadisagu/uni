/*$Id$*/
package ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad.logic;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.thoughtworks.xstream.XStream;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.plan.data.*;

import javax.annotation.Nullable;
import java.io.InputStream;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 15.01.2015
 */
public class FefuFileLoadDao extends UniBaseDao implements IFefuFileLoadDao
{
	@Override
	public void saveEduBlockFromXML(InputStream stream, Long baseBlockId)
	{
		XStream xStream = getXStream();
		EduPlanVersionBlockWrapper blockWrapper = (EduPlanVersionBlockWrapper) xStream.fromXML(stream);

		EppEduPlanVersionBlock block = getNotNull(baseBlockId);

		//check bock equals
		if (block instanceof EppEduPlanVersionRootBlock != blockWrapper.isRootBlock())
		{
			final String rootBlockMes = "«Основной блок версии УП»";
			final String specializationBlockMes = "«Блок направленности в версии УП»";
			String baseBlockType;
			String blockType;
			if (blockWrapper.isRootBlock())
			{
				baseBlockType = specializationBlockMes;
				blockType = rootBlockMes;
			}
			else
			{
				baseBlockType = rootBlockMes;
				blockType = specializationBlockMes;
			}
			throw new ApplicationException(String.format("Нельзя сохранить %s в %s. Выбирете соответсвующий блок", blockType, baseBlockType));
		}

		//create and save
		Multimap<Long, Long> rowTree = blockWrapper.getRowTree();
		Map<Long, EppEpvRow> blockRows = blockWrapper.getBlockRows();
		Map<Long, EppEpvRow> rootBlockRows = blockWrapper.getRootBlockRows();

		//find all rows first level
		Set<Long> topLvlRows = new HashSet<>();
		topLvlRows.addAll(blockRows.keySet());
		topLvlRows.addAll(rootBlockRows.keySet());
		topLvlRows.removeAll(rowTree.values());

		EppEduPlanVersionRootBlock rootBlock = getRootBlock(block);
		for (Long rowId : topLvlRows)
		{
			EppEpvRow row;
			if (blockWrapper.isRootRow(rowId))
			{
				row = rootBlockRows.get(rowId);
				row.setOwner(rootBlock);
			}
			else
			{
				row = blockRows.get(rowId);
				row.setOwner(block);
			}
			//assign id
			saveOrUpdate(row);
			if (row instanceof EppEpvTermDistributedRow)
			{
				saveRowTerms(blockWrapper, rowId, (EppEpvTermDistributedRow) row);
				saveRowLoad(blockWrapper, rowId, (EppEpvTermDistributedRow) row);
			}
			//save child rows
			Collection<Long> childRows = blockWrapper.getChildRows(rowId);
			if (!childRows.isEmpty())
				saveChildEppEpvRows(childRows, row, block, rootBlock, blockWrapper);
		}
	}

	private void saveRowLoad(EduPlanVersionBlockWrapper blockWrapper, Long rowId, EppEpvTermDistributedRow row)
	{
		Collection<EppEpvRowLoad> rowLoadList = blockWrapper.getRowLoads(rowId);
		if (!rowLoadList.isEmpty())
			for (EppEpvRowLoad rowLoad : rowLoadList)
			{
				rowLoad.setRow(row);
				saveOrUpdate(rowLoad);
			}
	}

	private void saveRowTerms(EduPlanVersionBlockWrapper blockWrapper, Long rowId, EppEpvTermDistributedRow row)
	{
		Collection<EpvRowTermWrapper> rowTermsWrappers = blockWrapper.getRowTerms(rowId);
		if (!rowTermsWrappers.isEmpty())
			for (EpvRowTermWrapper rowTermWrapper : rowTermsWrappers)
			{
				EppEpvRowTerm rowTerm = rowTermWrapper.getRowTerm();
				rowTerm.setRow(row);
				saveOrUpdate(rowTerm);

				List<EppEpvRowTermAction> rowTermActionList = rowTermWrapper.getRowTermActions();
				for (EppEpvRowTermAction rowTermAction : rowTermActionList)
				{
					rowTermAction.setRowTerm(rowTerm);
					saveOrUpdate(rowTermAction);
				}

				List<EppEpvRowTermLoad> rowTermLoadList = rowTermWrapper.getRowTermLoads();
				for (EppEpvRowTermLoad rowTermLoad : rowTermLoadList)
				{
					rowTermLoad.setRowTerm(rowTerm);
					saveOrUpdate(rowTermLoad);
				}
			}
	}

	@Override
	public String createXMLFromEduBlock(Long blockId)
	{
		List<EppEpvRow> eppEpvRowList;
		List<EppEpvRow> eppRootEpvRowList = null;

		EppEduPlanVersionBlock block = getNotNull(blockId);

		String alias = "er";
		eppEpvRowList = new DQLSelectBuilder().fromEntity(EppEpvRow.class, alias)
				.where(DQLExpressions.eq(DQLExpressions.property(alias, EppEpvRow.L_OWNER), value(blockId)))
				.createStatement(getSession()).list();

		if (block instanceof EppEduPlanVersionSpecializationBlock)
		{
			EppEduPlanVersionRootBlock versionRootBlock = getRootBlock(block);
			eppRootEpvRowList = new DQLSelectBuilder().fromEntity(EppEpvRow.class, alias)
					.where(DQLExpressions.eq(DQLExpressions.property(alias, EppEpvRow.L_OWNER), value(versionRootBlock.getId())))
					.createStatement(getSession()).list();
		}

        //key-id строки, value-список ссылающихся на него строк
		Multimap<Long, Long> rowTree = HashMultimap.create();
		Map<Long, EppEpvRow> blockRows = prepareRows(rowTree, eppEpvRowList);
		Map<Long, EppEpvRow> rootBlockRows = prepareRows(rowTree, eppRootEpvRowList);
		EppEduPlanVersionBlock newBlock = copyEntity(block);

		Set<EppEpvRow> rows = new HashSet<>();
		rows.addAll(eppEpvRowList);
		if (eppRootEpvRowList != null) rows.addAll(eppRootEpvRowList);

		Set<EppEpvTermDistributedRow> loadRows = new HashSet<>();
		for (EppEpvRow row : rows)
		{
			if (row instanceof EppEpvTermDistributedRow) loadRows.add((EppEpvTermDistributedRow) row);
		}
		Multimap<Long, EpvRowTermWrapper> rowTerms = getEppEpvRowTerm(loadRows);
		Multimap<Long, EppEpvRowLoad> rowLoads = getEppEpvRowLoad(loadRows);

		XStream xStream = getXStream();
		return xStream.toXML(new EduPlanVersionBlockWrapper(copyMap(blockRows), copyMap(rootBlockRows), rowTree, newBlock, rowTerms, rowLoads));
	}

	private XStream getXStream()
	{
		XStream xstream = new XStream();
		xstream.setMode(XStream.NO_REFERENCES);
		return xstream;
	}

	private Multimap<Long, EpvRowTermWrapper> getEppEpvRowTerm(Collection<EppEpvTermDistributedRow> loadRows)
	{
		String alias = "ert";
		List<EppEpvRowTerm> rowTermList = new DQLSelectBuilder().fromEntity(EppEpvRowTerm.class, alias)
				.where(in(property(alias, EppEpvRowTerm.row()), loadRows))
				.createStatement(getSession()).list();

		//key = rowId, value =List<EppEpvRowTerm>
		Multimap<Long, EpvRowTermWrapper> resultMap = ArrayListMultimap.create();
		for (EppEpvRowTerm rowTerm : rowTermList)
		{

			List<EppEpvRowTermAction> rowTermActionList = getList(EppEpvRowTermAction.class, EppEpvRowTermAction.L_ROW_TERM, rowTerm);
			List<EppEpvRowTermLoad> rowTermLoadList = getList(EppEpvRowTermLoad.class, EppEpvRowTermLoad.L_ROW_TERM, rowTerm);

			List<EppEpvRowTermAction> resRowTermActionList = new ArrayList<>();
			for (EppEpvRowTermAction termAction : rowTermActionList)
			{
				EppEpvRowTermAction newRowTermAction = copyEntity(termAction);
				newRowTermAction.setRowTerm(null);
				newRowTermAction.setId(null);
				resRowTermActionList.add(newRowTermAction);
			}
			List<EppEpvRowTermLoad> resRowTermLoadList = new ArrayList<>();
			for (EppEpvRowTermLoad termLoad : rowTermLoadList)
			{
				EppEpvRowTermLoad newRowTermLoad = copyEntity(termLoad);
				newRowTermLoad.setRowTerm(null);
				newRowTermLoad.setId(null);
				resRowTermLoadList.add(newRowTermLoad);
			}

			Long rowId = rowTerm.getRow().getId();
			EppEpvRowTerm newRowTerm = copyEntity(rowTerm);
			newRowTerm.setId(null);
			newRowTerm.setRow(null);
			resultMap.put(rowId, new EpvRowTermWrapper(newRowTerm, resRowTermActionList, resRowTermLoadList));
		}
		return resultMap;
	}

	private Multimap<Long, EppEpvRowLoad> getEppEpvRowLoad(Collection<EppEpvTermDistributedRow> loadRows)
	{

		String alias = "erl";
		List<EppEpvRowLoad> rowLoadList = new DQLSelectBuilder().fromEntity(EppEpvRowLoad.class, alias)
				.where(in(property(alias, EppEpvRowLoad.row()), loadRows))
				.createStatement(getSession()).list();

		//key = rowId, value =List<EppEpvRowLoad>
		Multimap<Long, EppEpvRowLoad> resultMap = ArrayListMultimap.create();
		for (EppEpvRowLoad rowLoad : rowLoadList)
		{
			Long rowId = rowLoad.getRow().getId();
			EppEpvRowLoad newRowLoad = copyEntity(rowLoad);
			newRowLoad.setId(null);
			newRowLoad.setRow(null);
			resultMap.put(rowId, newRowLoad);
		}
		return resultMap;
	}

	private void saveChildEppEpvRows(Collection<Long> topLvlRows, EppEpvRow parentRow, EppEduPlanVersionBlock block, EppEduPlanVersionRootBlock rootBlock, EduPlanVersionBlockWrapper blockWrapper)
	{
		for (Long rowId : topLvlRows)
		{
			EppEpvRow row;
			if (blockWrapper.isRootRow(rowId))
			{
				row = blockWrapper.getRootBlockRows().get(rowId);
				row.setOwner(rootBlock);
			}
			else
			{
				row = blockWrapper.getBlockRows().get(rowId);
				row.setOwner(block);
			}
			row.setHierarhyParent(parentRow);
			//assign id
			saveOrUpdate(row);

			if (row instanceof EppEpvTermDistributedRow)
			{
				saveRowTerms(blockWrapper, rowId, (EppEpvTermDistributedRow) row);
				saveRowLoad(blockWrapper, rowId, (EppEpvTermDistributedRow) row);
			}

			//recursive save child rows
			Collection<Long> childRows = blockWrapper.getChildRows(rowId);
			if (!childRows.isEmpty())
				saveChildEppEpvRows(childRows, row, block, rootBlock, blockWrapper);
		}
	}


	private EppEduPlanVersionRootBlock getRootBlock(EppEduPlanVersionBlock block)
	{
		if (block instanceof EppEduPlanVersionRootBlock) return (EppEduPlanVersionRootBlock) block;

		return DataAccessServices.dao().getUnique(EppEduPlanVersionRootBlock.class, EppEduPlanVersionRootBlock.L_EDU_PLAN_VERSION, block.getEduPlanVersion());
	}

	private Map<Long, EppEpvRow> prepareRows(Multimap<Long, Long> rowTree, @Nullable List<EppEpvRow> eppEpvRowList)
	{
		Map<Long, EppEpvRow> map = new HashMap<>();
		if (eppEpvRowList == null || eppEpvRowList.isEmpty()) return map;

		for (EppEpvRow row : eppEpvRowList)
		{
			checkEppEpvRow(row);
			Long rowId = row.getId();
			if (row.getHierarhyParent() != null)
			{
				Long parentId = row.getHierarhyParent().getId();
				rowTree.put(parentId, rowId);
			}
			map.put(rowId, row);
		}
		return map;
	}

	private void checkEppEpvRow(EppEpvRow row)
	{
		if (!(row instanceof EppEpvGroupReRow) && !(row instanceof EppEpvStructureRow) && !(row instanceof EppEpvGroupImRow) && !(row instanceof EppEpvRegistryRow))
			throw new IllegalArgumentException("Строка УП имеет недопустимый тип: " + row.toString());
	}

	private EppEpvRow clearRowCopy(EppEpvRow row)
	{
		EppEpvRow newRow = copyEntity(row);
		newRow.setOwner(null); //replace EppEduPlanVersionBlock
		newRow.setHierarhyParent(null); //replace new parent
		newRow.setId(null);
		return newRow;
	}

	private Map<Long, EppEpvRow> copyMap(Map<Long, EppEpvRow> srcMap)
	{
		Map<Long, EppEpvRow> res = new HashMap<>();
		for (Map.Entry<Long, EppEpvRow> entry : srcMap.entrySet())
		{
			Long id = entry.getKey();
			EppEpvRow epvRow = clearRowCopy(entry.getValue());
			res.put(id, epvRow);
		}
		return res;
	}

	private <T extends EntityBase> T copyEntity(T entity)
	{
		EntityBase copy = null;
		try
		{
			copy = entity.getClass().newInstance();
			copy.update(entity);
		}
		catch (InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
		//noinspection unchecked
		return (T) copy;
	}
}
