/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsReport.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportMeta;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 12/3/13
 */
public class FefuBrsReportDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public FefuBrsReportDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Map<IReportMeta, Long> map = FefuBrsReportManager.instance().fefuBrsReportListExtPoint().<Long>getUniqueCodeMappingSource().getItem2UniqueCodeMap();

        List<IReportMeta> list = new ArrayList<>(map.keySet());

        Collections.sort(list, ITitled.TITLED_COMPARATOR);

        List<IIdentifiableWrapper> result = new ArrayList<>();

        for (IReportMeta meta : list)
            result.add(new IdentifiableWrapper(map.get(meta), meta.getTitle()));

        DSOutput output = ListOutputBuilder.get(input, result).pageable(false).build();
        output.setCountRecord(list.size());
        return output;
    }
}
