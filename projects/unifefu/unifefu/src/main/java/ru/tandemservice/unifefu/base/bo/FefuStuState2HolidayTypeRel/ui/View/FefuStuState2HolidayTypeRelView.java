/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuStuState2HolidayTypeRel.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.base.bo.FefuStuState2HolidayTypeRel.logic.StudentStatusWrapper;
import ru.tandemservice.unifefu.entity.StudentStatus2VacRel;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author DMITRY KNYAZEV
 * @since 03.10.2014
 */
@Configuration
public class FefuStuState2HolidayTypeRelView extends BusinessComponentManager
{
	public static final String STUDENT_STATUS_DS = "studentStatusDS";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(searchListDS(STUDENT_STATUS_DS, studentStateDSColumns(), studentStateDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint studentStateDSColumns()
	{
		return columnListExtPointBuilder(STUDENT_STATUS_DS)
				.addColumn(textColumn("title", StudentStatus.title()).width("200px"))
				.addColumn(toggleColumn("consider", StudentStatusWrapper.CONSIDER)
						           .toggleOnListener("onConsiderOn").toggleOnLabel("Да")
						           .toggleOffListener("onConsiderOff").toggleOffLabel("Нет"))
				.create();
	}

	@Bean
	public IDefaultSearchDataSourceHandler studentStateDSHandler()
	{
		return new DefaultSearchDataSourceHandler(getName())
		{
			@Override
			public DSOutput execute(DSInput input, ExecutionContext context)
			{
				DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentStatus.class, "ss")
						.column("ss")
						.column(property("ssv", StudentStatus2VacRel.P_CONSIDER))
						.joinEntity("ss", DQLJoinType.left, StudentStatus2VacRel.class, "ssv", eq(property("ss.id"), property("ssv", StudentStatus2VacRel.L_STATUS)))
						.order(property("ss", StudentStatus.P_PRIORITY));
				List<Object[]> list = builder.createStatement(context.getSession()).list();
				List<StudentStatusWrapper> studentStatusWrappedList = new ArrayList<>(list.size());
				for (Object[] item : list)
				{
					StudentStatus status = (StudentStatus) item[0];
					Boolean consider = (Boolean) item[1];
					studentStatusWrappedList.add(new StudentStatusWrapper(status, consider));
				}
				return ListOutputBuilder.get(input.setCountRecord(studentStatusWrappedList.size()), studentStatusWrappedList).build();
			}
		};
	}
}
