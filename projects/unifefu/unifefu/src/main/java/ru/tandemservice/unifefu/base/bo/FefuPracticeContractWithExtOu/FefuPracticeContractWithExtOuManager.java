/* $Id: FefuPracticeContractWithExtOuManager.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.ctr.base.bo.JuridicalContactor.logic.ExternalOrgUnitContactorComboDSHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.logic.FefuPracticeContractWithExtOuDAO;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.logic.IFefuPracticeContractWithExtOuDAO;

import java.util.Collections;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/28/13
 * Time: 6:00 PM
 */
@Configuration
public class FefuPracticeContractWithExtOuManager extends BusinessObjectManager
{
    public static FefuPracticeContractWithExtOuManager instance()
    {
        return instance(FefuPracticeContractWithExtOuManager.class);
    }

    @Bean
    public IFefuPracticeContractWithExtOuDAO dao()
    {
        return new FefuPracticeContractWithExtOuDAO();
    }

    @Bean
    public IDefaultComboDataSourceHandler extOuComboDSHandler()
    {
        return new ExternalOrgUnitContactorComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler jurCtrComboDSHandler()
    {
        return (IDefaultComboDataSourceHandler) new DefaultComboDataSourceHandler(getName(), JuridicalContactor.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long extOuId = context.get("externalOrgUnit");
                Boolean showAllWoExtOuId = context.getBoolean("showAllWoExtOuId", Boolean.FALSE);
                if(null == extOuId && !showAllWoExtOuId)
                {
                    return ListOutputBuilder.get(input, Collections.emptyList()).build();
                }
                else
                {
                    return super.execute(input, context);
                }
            }

            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                Long extOuId = ep.context.get("externalOrgUnit");

                if(null != extOuId)
                {
                    ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(JuridicalContactor.externalOrgUnit().id().fromAlias("e")), DQLExpressions.value(extOuId)));
                }
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property(JuridicalContactor.active().fromAlias("e")), DQLExpressions.value(true)));
            }

        }.setFilterByProperty(JuridicalContactor.person().identityCard().fullFio().s());
    }

    public final static Long YES = 1L;
    public final static Long NO = 2L;

    @Bean
    public ItemListExtPoint<DataWrapper> booleanOptionsExtPoint()
    {
        return itemList(DataWrapper.class).
                add(YES.toString(), new DataWrapper(YES, "ui.yes")).
                add(NO.toString(), new DataWrapper(NO, "ui.no")).
                create();
    }

    @Bean
    public IDefaultComboDataSourceHandler booleanComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(booleanOptionsExtPoint());
    }
}
