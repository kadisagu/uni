/**
 * ReferencesUpdateResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class ReferencesUpdateResponse  implements java.io.Serializable {
    private java.lang.String[] referencesUpdateResult;

    public ReferencesUpdateResponse() {
    }

    public ReferencesUpdateResponse(
           java.lang.String[] referencesUpdateResult) {
           this.referencesUpdateResult = referencesUpdateResult;
    }


    /**
     * Gets the referencesUpdateResult value for this ReferencesUpdateResponse.
     * 
     * @return referencesUpdateResult
     */
    public java.lang.String[] getReferencesUpdateResult() {
        return referencesUpdateResult;
    }


    /**
     * Sets the referencesUpdateResult value for this ReferencesUpdateResponse.
     * 
     * @param referencesUpdateResult
     */
    public void setReferencesUpdateResult(java.lang.String[] referencesUpdateResult) {
        this.referencesUpdateResult = referencesUpdateResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReferencesUpdateResponse)) return false;
        ReferencesUpdateResponse other = (ReferencesUpdateResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.referencesUpdateResult==null && other.getReferencesUpdateResult()==null) || 
             (this.referencesUpdateResult!=null &&
              java.util.Arrays.equals(this.referencesUpdateResult, other.getReferencesUpdateResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReferencesUpdateResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getReferencesUpdateResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getReferencesUpdateResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReferencesUpdateResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">ReferencesUpdateResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referencesUpdateResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "ReferencesUpdateResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
