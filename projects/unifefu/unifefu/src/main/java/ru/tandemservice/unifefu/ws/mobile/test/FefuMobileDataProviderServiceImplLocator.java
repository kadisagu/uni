/**
 * FefuMobileDataProviderServiceImplLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.mobile.test;

public class FefuMobileDataProviderServiceImplLocator extends org.apache.axis.client.Service implements ru.tandemservice.unifefu.ws.mobile.test.FefuMobileDataProviderServiceImpl {

    public FefuMobileDataProviderServiceImplLocator() {
    }


    public FefuMobileDataProviderServiceImplLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FefuMobileDataProviderServiceImplLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for FefuMobileDataProviderServicePort
    private java.lang.String FefuMobileDataProviderServicePort_address = "http://localhost:8080/services/FefuMobileDataProviderService";

    public java.lang.String getFefuMobileDataProviderServicePortAddress() {
        return FefuMobileDataProviderServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String FefuMobileDataProviderServicePortWSDDServiceName = "FefuMobileDataProviderServicePort";

    public java.lang.String getFefuMobileDataProviderServicePortWSDDServiceName() {
        return FefuMobileDataProviderServicePortWSDDServiceName;
    }

    public void setFefuMobileDataProviderServicePortWSDDServiceName(java.lang.String name) {
        FefuMobileDataProviderServicePortWSDDServiceName = name;
    }

    public ru.tandemservice.unifefu.ws.mobile.test.FefuMobileDataProviderService getFefuMobileDataProviderServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(FefuMobileDataProviderServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getFefuMobileDataProviderServicePort(endpoint);
    }

    public ru.tandemservice.unifefu.ws.mobile.test.FefuMobileDataProviderService getFefuMobileDataProviderServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ru.tandemservice.unifefu.ws.mobile.test.FefuMobileDataProviderServiceSoapBindingStub _stub = new ru.tandemservice.unifefu.ws.mobile.test.FefuMobileDataProviderServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getFefuMobileDataProviderServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setFefuMobileDataProviderServicePortEndpointAddress(java.lang.String address) {
        FefuMobileDataProviderServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ru.tandemservice.unifefu.ws.mobile.test.FefuMobileDataProviderService.class.isAssignableFrom(serviceEndpointInterface)) {
                ru.tandemservice.unifefu.ws.mobile.test.FefuMobileDataProviderServiceSoapBindingStub _stub = new ru.tandemservice.unifefu.ws.mobile.test.FefuMobileDataProviderServiceSoapBindingStub(new java.net.URL(FefuMobileDataProviderServicePort_address), this);
                _stub.setPortName(getFefuMobileDataProviderServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("FefuMobileDataProviderServicePort".equals(inputPortName)) {
            return getFefuMobileDataProviderServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "FefuMobileDataProviderServiceImpl");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "FefuMobileDataProviderServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("FefuMobileDataProviderServicePort".equals(portName)) {
            setFefuMobileDataProviderServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
