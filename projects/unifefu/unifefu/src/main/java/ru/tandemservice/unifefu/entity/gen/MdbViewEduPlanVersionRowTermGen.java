package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm;
import ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * mdbViewEduplanversionRowTerm
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewEduPlanVersionRowTermGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm";
    public static final String ENTITY_NAME = "mdbViewEduPlanVersionRowTerm";
    public static final int VERSION_HASH = 565887094;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW_VERSION_ID = "rowVersionId";
    public static final String P_ROW_PATH = "rowPath";
    public static final String P_TERM = "term";
    public static final String P_WEEKS = "weeks";
    public static final String P_SIZE = "size";
    public static final String P_LABOR = "labor";
    public static final String P_EAUDIT = "eaudit";
    public static final String P_ALECT = "alect";
    public static final String P_APRACT = "apract";
    public static final String P_ALAB = "alab";
    public static final String P_ESELF = "eself";
    public static final String P_ACTIONS = "actions";

    private MdbViewEppEduPlanVersion _rowVersionId;     // Версия учебного плана
    private String _rowPath; 
    private Integer _term; 
    private String _weeks; 
    private String _size; 
    private String _labor; 
    private String _eaudit; 
    private String _alect; 
    private String _apract; 
    private String _alab; 
    private String _eself; 
    private String _actions; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     */
    @NotNull
    public MdbViewEppEduPlanVersion getRowVersionId()
    {
        return _rowVersionId;
    }

    /**
     * @param rowVersionId Версия учебного плана. Свойство не может быть null.
     */
    public void setRowVersionId(MdbViewEppEduPlanVersion rowVersionId)
    {
        dirty(_rowVersionId, rowVersionId);
        _rowVersionId = rowVersionId;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getRowPath()
    {
        return _rowPath;
    }

    /**
     * @param rowPath 
     */
    public void setRowPath(String rowPath)
    {
        dirty(_rowPath, rowPath);
        _rowPath = rowPath;
    }

    /**
     * @return 
     */
    public Integer getTerm()
    {
        return _term;
    }

    /**
     * @param term 
     */
    public void setTerm(Integer term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getWeeks()
    {
        return _weeks;
    }

    /**
     * @param weeks 
     */
    public void setWeeks(String weeks)
    {
        dirty(_weeks, weeks);
        _weeks = weeks;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getSize()
    {
        return _size;
    }

    /**
     * @param size 
     */
    public void setSize(String size)
    {
        dirty(_size, size);
        _size = size;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getLabor()
    {
        return _labor;
    }

    /**
     * @param labor 
     */
    public void setLabor(String labor)
    {
        dirty(_labor, labor);
        _labor = labor;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getEaudit()
    {
        return _eaudit;
    }

    /**
     * @param eaudit 
     */
    public void setEaudit(String eaudit)
    {
        dirty(_eaudit, eaudit);
        _eaudit = eaudit;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getAlect()
    {
        return _alect;
    }

    /**
     * @param alect 
     */
    public void setAlect(String alect)
    {
        dirty(_alect, alect);
        _alect = alect;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getApract()
    {
        return _apract;
    }

    /**
     * @param apract 
     */
    public void setApract(String apract)
    {
        dirty(_apract, apract);
        _apract = apract;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getAlab()
    {
        return _alab;
    }

    /**
     * @param alab 
     */
    public void setAlab(String alab)
    {
        dirty(_alab, alab);
        _alab = alab;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getEself()
    {
        return _eself;
    }

    /**
     * @param eself 
     */
    public void setEself(String eself)
    {
        dirty(_eself, eself);
        _eself = eself;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getActions()
    {
        return _actions;
    }

    /**
     * @param actions 
     */
    public void setActions(String actions)
    {
        dirty(_actions, actions);
        _actions = actions;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewEduPlanVersionRowTermGen)
        {
            setRowVersionId(((MdbViewEduPlanVersionRowTerm)another).getRowVersionId());
            setRowPath(((MdbViewEduPlanVersionRowTerm)another).getRowPath());
            setTerm(((MdbViewEduPlanVersionRowTerm)another).getTerm());
            setWeeks(((MdbViewEduPlanVersionRowTerm)another).getWeeks());
            setSize(((MdbViewEduPlanVersionRowTerm)another).getSize());
            setLabor(((MdbViewEduPlanVersionRowTerm)another).getLabor());
            setEaudit(((MdbViewEduPlanVersionRowTerm)another).getEaudit());
            setAlect(((MdbViewEduPlanVersionRowTerm)another).getAlect());
            setApract(((MdbViewEduPlanVersionRowTerm)another).getApract());
            setAlab(((MdbViewEduPlanVersionRowTerm)another).getAlab());
            setEself(((MdbViewEduPlanVersionRowTerm)another).getEself());
            setActions(((MdbViewEduPlanVersionRowTerm)another).getActions());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewEduPlanVersionRowTermGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewEduPlanVersionRowTerm.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewEduPlanVersionRowTerm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "rowVersionId":
                    return obj.getRowVersionId();
                case "rowPath":
                    return obj.getRowPath();
                case "term":
                    return obj.getTerm();
                case "weeks":
                    return obj.getWeeks();
                case "size":
                    return obj.getSize();
                case "labor":
                    return obj.getLabor();
                case "eaudit":
                    return obj.getEaudit();
                case "alect":
                    return obj.getAlect();
                case "apract":
                    return obj.getApract();
                case "alab":
                    return obj.getAlab();
                case "eself":
                    return obj.getEself();
                case "actions":
                    return obj.getActions();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "rowVersionId":
                    obj.setRowVersionId((MdbViewEppEduPlanVersion) value);
                    return;
                case "rowPath":
                    obj.setRowPath((String) value);
                    return;
                case "term":
                    obj.setTerm((Integer) value);
                    return;
                case "weeks":
                    obj.setWeeks((String) value);
                    return;
                case "size":
                    obj.setSize((String) value);
                    return;
                case "labor":
                    obj.setLabor((String) value);
                    return;
                case "eaudit":
                    obj.setEaudit((String) value);
                    return;
                case "alect":
                    obj.setAlect((String) value);
                    return;
                case "apract":
                    obj.setApract((String) value);
                    return;
                case "alab":
                    obj.setAlab((String) value);
                    return;
                case "eself":
                    obj.setEself((String) value);
                    return;
                case "actions":
                    obj.setActions((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "rowVersionId":
                        return true;
                case "rowPath":
                        return true;
                case "term":
                        return true;
                case "weeks":
                        return true;
                case "size":
                        return true;
                case "labor":
                        return true;
                case "eaudit":
                        return true;
                case "alect":
                        return true;
                case "apract":
                        return true;
                case "alab":
                        return true;
                case "eself":
                        return true;
                case "actions":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "rowVersionId":
                    return true;
                case "rowPath":
                    return true;
                case "term":
                    return true;
                case "weeks":
                    return true;
                case "size":
                    return true;
                case "labor":
                    return true;
                case "eaudit":
                    return true;
                case "alect":
                    return true;
                case "apract":
                    return true;
                case "alab":
                    return true;
                case "eself":
                    return true;
                case "actions":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "rowVersionId":
                    return MdbViewEppEduPlanVersion.class;
                case "rowPath":
                    return String.class;
                case "term":
                    return Integer.class;
                case "weeks":
                    return String.class;
                case "size":
                    return String.class;
                case "labor":
                    return String.class;
                case "eaudit":
                    return String.class;
                case "alect":
                    return String.class;
                case "apract":
                    return String.class;
                case "alab":
                    return String.class;
                case "eself":
                    return String.class;
                case "actions":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewEduPlanVersionRowTerm> _dslPath = new Path<MdbViewEduPlanVersionRowTerm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewEduPlanVersionRowTerm");
    }
            

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getRowVersionId()
     */
    public static MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> rowVersionId()
    {
        return _dslPath.rowVersionId();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getRowPath()
     */
    public static PropertyPath<String> rowPath()
    {
        return _dslPath.rowPath();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getTerm()
     */
    public static PropertyPath<Integer> term()
    {
        return _dslPath.term();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getWeeks()
     */
    public static PropertyPath<String> weeks()
    {
        return _dslPath.weeks();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getSize()
     */
    public static PropertyPath<String> size()
    {
        return _dslPath.size();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getLabor()
     */
    public static PropertyPath<String> labor()
    {
        return _dslPath.labor();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getEaudit()
     */
    public static PropertyPath<String> eaudit()
    {
        return _dslPath.eaudit();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getAlect()
     */
    public static PropertyPath<String> alect()
    {
        return _dslPath.alect();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getApract()
     */
    public static PropertyPath<String> apract()
    {
        return _dslPath.apract();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getAlab()
     */
    public static PropertyPath<String> alab()
    {
        return _dslPath.alab();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getEself()
     */
    public static PropertyPath<String> eself()
    {
        return _dslPath.eself();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getActions()
     */
    public static PropertyPath<String> actions()
    {
        return _dslPath.actions();
    }

    public static class Path<E extends MdbViewEduPlanVersionRowTerm> extends EntityPath<E>
    {
        private MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> _rowVersionId;
        private PropertyPath<String> _rowPath;
        private PropertyPath<Integer> _term;
        private PropertyPath<String> _weeks;
        private PropertyPath<String> _size;
        private PropertyPath<String> _labor;
        private PropertyPath<String> _eaudit;
        private PropertyPath<String> _alect;
        private PropertyPath<String> _apract;
        private PropertyPath<String> _alab;
        private PropertyPath<String> _eself;
        private PropertyPath<String> _actions;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getRowVersionId()
     */
        public MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> rowVersionId()
        {
            if(_rowVersionId == null )
                _rowVersionId = new MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion>(L_ROW_VERSION_ID, this);
            return _rowVersionId;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getRowPath()
     */
        public PropertyPath<String> rowPath()
        {
            if(_rowPath == null )
                _rowPath = new PropertyPath<String>(MdbViewEduPlanVersionRowTermGen.P_ROW_PATH, this);
            return _rowPath;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getTerm()
     */
        public PropertyPath<Integer> term()
        {
            if(_term == null )
                _term = new PropertyPath<Integer>(MdbViewEduPlanVersionRowTermGen.P_TERM, this);
            return _term;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getWeeks()
     */
        public PropertyPath<String> weeks()
        {
            if(_weeks == null )
                _weeks = new PropertyPath<String>(MdbViewEduPlanVersionRowTermGen.P_WEEKS, this);
            return _weeks;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getSize()
     */
        public PropertyPath<String> size()
        {
            if(_size == null )
                _size = new PropertyPath<String>(MdbViewEduPlanVersionRowTermGen.P_SIZE, this);
            return _size;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getLabor()
     */
        public PropertyPath<String> labor()
        {
            if(_labor == null )
                _labor = new PropertyPath<String>(MdbViewEduPlanVersionRowTermGen.P_LABOR, this);
            return _labor;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getEaudit()
     */
        public PropertyPath<String> eaudit()
        {
            if(_eaudit == null )
                _eaudit = new PropertyPath<String>(MdbViewEduPlanVersionRowTermGen.P_EAUDIT, this);
            return _eaudit;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getAlect()
     */
        public PropertyPath<String> alect()
        {
            if(_alect == null )
                _alect = new PropertyPath<String>(MdbViewEduPlanVersionRowTermGen.P_ALECT, this);
            return _alect;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getApract()
     */
        public PropertyPath<String> apract()
        {
            if(_apract == null )
                _apract = new PropertyPath<String>(MdbViewEduPlanVersionRowTermGen.P_APRACT, this);
            return _apract;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getAlab()
     */
        public PropertyPath<String> alab()
        {
            if(_alab == null )
                _alab = new PropertyPath<String>(MdbViewEduPlanVersionRowTermGen.P_ALAB, this);
            return _alab;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getEself()
     */
        public PropertyPath<String> eself()
        {
            if(_eself == null )
                _eself = new PropertyPath<String>(MdbViewEduPlanVersionRowTermGen.P_ESELF, this);
            return _eself;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRowTerm#getActions()
     */
        public PropertyPath<String> actions()
        {
            if(_actions == null )
                _actions = new PropertyPath<String>(MdbViewEduPlanVersionRowTermGen.P_ACTIONS, this);
            return _actions;
        }

        public Class getEntityClass()
        {
            return MdbViewEduPlanVersionRowTerm.class;
        }

        public String getEntityName()
        {
            return "mdbViewEduPlanVersionRowTerm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
