/* $Id$ */
package ru.tandemservice.unifefu.dao.mdbio;

import com.healthmarketscience.jackcess.Database;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.io.IOException;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 17.04.2013
 */
public interface IFefuOrdersIOAddDataDao
{
    SpringBeanCache<IFefuOrdersIOAddDataDao> instance = new SpringBeanCache<>(IFefuOrdersIOAddDataDao.class.getName());

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Map<String, Long> doImport_LotusOrdersData(Database mdb, final String dataBaseFileName, final String tableName, final String extractTypeCategoryCode) throws IOException;
}
