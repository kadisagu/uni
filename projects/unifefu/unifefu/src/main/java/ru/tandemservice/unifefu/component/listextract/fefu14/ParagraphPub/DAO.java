/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu14.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 12.11.2014
 */
public class DAO extends AbstractListParagraphPubDAO<FefuTransfAcceleratedTimeStuListExtract, Model> implements IDAO
{
}