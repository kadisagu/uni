/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu26.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuPerformConditionTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuPerformConditionTransferCourseStuExtract, Model>
{
}
