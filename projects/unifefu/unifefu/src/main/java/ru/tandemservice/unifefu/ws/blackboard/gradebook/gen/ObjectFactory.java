
package ru.tandemservice.unifefu.ws.blackboard.gradebook.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tandemservice.unifefu.ws.blackboard.gradebook.gen package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DeleteAttemptsCourseId_QNAME = new QName("http://gradebook.ws.blackboard", "courseId");
    private final static QName _GradebookTypeFilterTitle_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "title");
    private final static QName _GetGradebookColumnsFilter_QNAME = new QName("http://gradebook.ws.blackboard", "filter");
    private final static QName _ColumnVOScaleId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "scaleId");
    private final static QName _ColumnVOId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "id");
    private final static QName _ColumnVODescription_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "description");
    private final static QName _ColumnVOCalculationType_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "calculationType");
    private final static QName _ColumnVOGradebookTypeId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "gradebookTypeId");
    private final static QName _ColumnVOCourseId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "courseId");
    private final static QName _ColumnVOColumnDisplayName_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "columnDisplayName");
    private final static QName _ColumnVOAnalysisUrl_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "analysisUrl");
    private final static QName _ColumnVODescriptionType_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "descriptionType");
    private final static QName _ColumnVOColumnName_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "columnName");
    private final static QName _ColumnVOContentId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "contentId");
    private final static QName _ColumnVOAggregationModel_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "aggregationModel");
    private final static QName _GetRequiredEntitlementsMethod_QNAME = new QName("http://gradebook.ws.blackboard", "method");
    private final static QName _GetServerVersionResponseReturn_QNAME = new QName("http://gradebook.ws.blackboard", "return");
    private final static QName _ScoreVOManualScore_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "manualScore");
    private final static QName _ScoreVOUserId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "userId");
    private final static QName _ScoreVOShortStudentComments_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "shortStudentComments");
    private final static QName _ScoreVOLowestAttemptId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "lowestAttemptId");
    private final static QName _ScoreVOMemberId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "memberId");
    private final static QName _ScoreVOSchemaGradeValue_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "schemaGradeValue");
    private final static QName _ScoreVOGrade_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "grade");
    private final static QName _ScoreVOLastAttemptId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "lastAttemptId");
    private final static QName _ScoreVOManualGrade_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "manualGrade");
    private final static QName _ScoreVOAverageScore_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "averageScore");
    private final static QName _ScoreVOStudentComments_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "studentComments");
    private final static QName _ScoreVOShortInstructorComments_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "shortInstructorComments");
    private final static QName _ScoreVOColumnId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "columnId");
    private final static QName _ScoreVOHighestAttemptId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "highestAttemptId");
    private final static QName _ScoreVOInstructorComments_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "instructorComments");
    private final static QName _ScoreVOFirstAttemptId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "firstAttemptId");
    private final static QName _GradingSchemaSymbolVOSymbol_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "symbol");
    private final static QName _GradingSchemaSymbolVOGradingSchemaId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "gradingSchemaId");
    private final static QName _AttemptFilterGradeId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "gradeId");
    private final static QName _AttemptVOStudentSubmission_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "studentSubmission");
    private final static QName _AttemptVOStudentSubmissionTextType_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "studentSubmissionTextType");
    private final static QName _AttemptVOStatus_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "status");
    private final static QName _AttemptVOInstructorNotes_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "instructorNotes");
    private final static QName _AttemptVODisplayGrade_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "displayGrade");
    private final static QName _AttemptVOGroupAttemptId_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "groupAttemptId");
    private final static QName _AttemptVOFeedbackToUser_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "feedbackToUser");
    private final static QName _GradebookTypeVODisplayedDescription_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "displayedDescription");
    private final static QName _GradebookTypeVODisplayedTitle_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "displayedTitle");
    private final static QName _GetServerVersionUnused_QNAME = new QName("http://gradebook.ws.blackboard", "unused");
    private final static QName _GradingSchemaVOScaleType_QNAME = new QName("http://gradebook.ws.blackboard/xsd", "scaleType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tandemservice.unifefu.ws.blackboard.gradebook.gen
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeleteGradingSchemasResponse }
     * 
     */
    public DeleteGradingSchemasResponse createDeleteGradingSchemasResponse() {
        return new DeleteGradingSchemasResponse();
    }

    /**
     * Create an instance of {@link GetGradebookColumns }
     * 
     */
    public GetGradebookColumns createGetGradebookColumns() {
        return new GetGradebookColumns();
    }

    /**
     * Create an instance of {@link GetGradebookTypesResponse }
     * 
     */
    public GetGradebookTypesResponse createGetGradebookTypesResponse() {
        return new GetGradebookTypesResponse();
    }

    /**
     * Create an instance of {@link DeleteGradesResponse }
     * 
     */
    public DeleteGradesResponse createDeleteGradesResponse() {
        return new DeleteGradesResponse();
    }

    /**
     * Create an instance of {@link InitializeGradebookWS }
     * 
     */
    public InitializeGradebookWS createInitializeGradebookWS() {
        return new InitializeGradebookWS();
    }

    /**
     * Create an instance of {@link DeleteGrades }
     * 
     */
    public DeleteGrades createDeleteGrades() {
        return new DeleteGrades();
    }

    /**
     * Create an instance of {@link ColumnVO }
     * 
     */
    public ColumnVO createColumnVO() {
        return new ColumnVO();
    }

    /**
     * Create an instance of {@link ScoreVO }
     * 
     */
    public ScoreVO createScoreVO() {
        return new ScoreVO();
    }

    /**
     * Create an instance of {@link DeleteGradebookTypesResponse }
     * 
     */
    public DeleteGradebookTypesResponse createDeleteGradebookTypesResponse() {
        return new DeleteGradebookTypesResponse();
    }

    /**
     * Create an instance of {@link GradingSchemaFilter }
     * 
     */
    public GradingSchemaFilter createGradingSchemaFilter() {
        return new GradingSchemaFilter();
    }

    /**
     * Create an instance of {@link GetGradebookTypes }
     * 
     */
    public GetGradebookTypes createGetGradebookTypes() {
        return new GetGradebookTypes();
    }

    /**
     * Create an instance of {@link GetGradingSchemas }
     * 
     */
    public GetGradingSchemas createGetGradingSchemas() {
        return new GetGradingSchemas();
    }

    /**
     * Create an instance of {@link UpdateColumnAttributeResponse }
     * 
     */
    public UpdateColumnAttributeResponse createUpdateColumnAttributeResponse() {
        return new UpdateColumnAttributeResponse();
    }

    /**
     * Create an instance of {@link ScoreFilter }
     * 
     */
    public ScoreFilter createScoreFilter() {
        return new ScoreFilter();
    }

    /**
     * Create an instance of {@link SaveColumns }
     * 
     */
    public SaveColumns createSaveColumns() {
        return new SaveColumns();
    }

    /**
     * Create an instance of {@link GetServerVersion }
     * 
     */
    public GetServerVersion createGetServerVersion() {
        return new GetServerVersion();
    }

    /**
     * Create an instance of {@link DeleteGradebookTypes }
     * 
     */
    public DeleteGradebookTypes createDeleteGradebookTypes() {
        return new DeleteGradebookTypes();
    }

    /**
     * Create an instance of {@link DeleteAttempts }
     * 
     */
    public DeleteAttempts createDeleteAttempts() {
        return new DeleteAttempts();
    }

    /**
     * Create an instance of {@link SaveAttempts }
     * 
     */
    public SaveAttempts createSaveAttempts() {
        return new SaveAttempts();
    }

    /**
     * Create an instance of {@link GradebookTypeFilter }
     * 
     */
    public GradebookTypeFilter createGradebookTypeFilter() {
        return new GradebookTypeFilter();
    }

    /**
     * Create an instance of {@link InitializeGradebookWSResponse }
     * 
     */
    public InitializeGradebookWSResponse createInitializeGradebookWSResponse() {
        return new InitializeGradebookWSResponse();
    }

    /**
     * Create an instance of {@link SaveGradebookTypesResponse }
     * 
     */
    public SaveGradebookTypesResponse createSaveGradebookTypesResponse() {
        return new SaveGradebookTypesResponse();
    }

    /**
     * Create an instance of {@link SaveGradebookTypes }
     * 
     */
    public SaveGradebookTypes createSaveGradebookTypes() {
        return new SaveGradebookTypes();
    }

    /**
     * Create an instance of {@link SaveGradingSchemasResponse }
     * 
     */
    public SaveGradingSchemasResponse createSaveGradingSchemasResponse() {
        return new SaveGradingSchemasResponse();
    }

    /**
     * Create an instance of {@link DeleteAttemptsResponse }
     * 
     */
    public DeleteAttemptsResponse createDeleteAttemptsResponse() {
        return new DeleteAttemptsResponse();
    }

    /**
     * Create an instance of {@link GetGradesResponse }
     * 
     */
    public GetGradesResponse createGetGradesResponse() {
        return new GetGradesResponse();
    }

    /**
     * Create an instance of {@link GetRequiredEntitlements }
     * 
     */
    public GetRequiredEntitlements createGetRequiredEntitlements() {
        return new GetRequiredEntitlements();
    }

    /**
     * Create an instance of {@link DeleteColumnsResponse }
     * 
     */
    public DeleteColumnsResponse createDeleteColumnsResponse() {
        return new DeleteColumnsResponse();
    }

    /**
     * Create an instance of {@link GetServerVersionResponse }
     * 
     */
    public GetServerVersionResponse createGetServerVersionResponse() {
        return new GetServerVersionResponse();
    }

    /**
     * Create an instance of {@link DeleteGradingSchemas }
     * 
     */
    public DeleteGradingSchemas createDeleteGradingSchemas() {
        return new DeleteGradingSchemas();
    }

    /**
     * Create an instance of {@link GetRequiredEntitlementsResponse }
     * 
     */
    public GetRequiredEntitlementsResponse createGetRequiredEntitlementsResponse() {
        return new GetRequiredEntitlementsResponse();
    }

    /**
     * Create an instance of {@link GradingSchemaSymbolVO }
     * 
     */
    public GradingSchemaSymbolVO createGradingSchemaSymbolVO() {
        return new GradingSchemaSymbolVO();
    }

    /**
     * Create an instance of {@link SaveGrades }
     * 
     */
    public SaveGrades createSaveGrades() {
        return new SaveGrades();
    }

    /**
     * Create an instance of {@link AttemptFilter }
     * 
     */
    public AttemptFilter createAttemptFilter() {
        return new AttemptFilter();
    }

    /**
     * Create an instance of {@link VersionVO }
     * 
     */
    public VersionVO createVersionVO() {
        return new VersionVO();
    }

    /**
     * Create an instance of {@link SaveGradingSchemas }
     * 
     */
    public SaveGradingSchemas createSaveGradingSchemas() {
        return new SaveGradingSchemas();
    }

    /**
     * Create an instance of {@link SaveGradesResponse }
     * 
     */
    public SaveGradesResponse createSaveGradesResponse() {
        return new SaveGradesResponse();
    }

    /**
     * Create an instance of {@link ColumnFilter }
     * 
     */
    public ColumnFilter createColumnFilter() {
        return new ColumnFilter();
    }

    /**
     * Create an instance of {@link UpdateColumnAttribute }
     * 
     */
    public UpdateColumnAttribute createUpdateColumnAttribute() {
        return new UpdateColumnAttribute();
    }

    /**
     * Create an instance of {@link GetAttemptsResponse }
     * 
     */
    public GetAttemptsResponse createGetAttemptsResponse() {
        return new GetAttemptsResponse();
    }

    /**
     * Create an instance of {@link AttemptVO }
     * 
     */
    public AttemptVO createAttemptVO() {
        return new AttemptVO();
    }

    /**
     * Create an instance of {@link GetGrades }
     * 
     */
    public GetGrades createGetGrades() {
        return new GetGrades();
    }

    /**
     * Create an instance of {@link SaveColumnsResponse }
     * 
     */
    public SaveColumnsResponse createSaveColumnsResponse() {
        return new SaveColumnsResponse();
    }

    /**
     * Create an instance of {@link GetGradebookColumnsResponse }
     * 
     */
    public GetGradebookColumnsResponse createGetGradebookColumnsResponse() {
        return new GetGradebookColumnsResponse();
    }

    /**
     * Create an instance of {@link GradebookTypeVO }
     * 
     */
    public GradebookTypeVO createGradebookTypeVO() {
        return new GradebookTypeVO();
    }

    /**
     * Create an instance of {@link DeleteColumns }
     * 
     */
    public DeleteColumns createDeleteColumns() {
        return new DeleteColumns();
    }

    /**
     * Create an instance of {@link GetAttempts }
     * 
     */
    public GetAttempts createGetAttempts() {
        return new GetAttempts();
    }

    /**
     * Create an instance of {@link GradingSchemaVO }
     * 
     */
    public GradingSchemaVO createGradingSchemaVO() {
        return new GradingSchemaVO();
    }

    /**
     * Create an instance of {@link GetGradingSchemasResponse }
     * 
     */
    public GetGradingSchemasResponse createGetGradingSchemasResponse() {
        return new GetGradingSchemasResponse();
    }

    /**
     * Create an instance of {@link SaveAttemptsResponse }
     * 
     */
    public SaveAttemptsResponse createSaveAttemptsResponse() {
        return new SaveAttemptsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = DeleteAttempts.class)
    public JAXBElement<String> createDeleteAttemptsCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, DeleteAttempts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = SaveAttempts.class)
    public JAXBElement<String> createSaveAttemptsCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, SaveAttempts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "title", scope = GradebookTypeFilter.class)
    public JAXBElement<String> createGradebookTypeFilterTitle(String value) {
        return new JAXBElement<>(_GradebookTypeFilterTitle_QNAME, String.class, GradebookTypeFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ColumnFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "filter", scope = GetGradebookColumns.class)
    public JAXBElement<ColumnFilter> createGetGradebookColumnsFilter(ColumnFilter value) {
        return new JAXBElement<>(_GetGradebookColumnsFilter_QNAME, ColumnFilter.class, GetGradebookColumns.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = GetGradebookColumns.class)
    public JAXBElement<String> createGetGradebookColumnsCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, GetGradebookColumns.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = SaveGradebookTypes.class)
    public JAXBElement<String> createSaveGradebookTypesCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, SaveGradebookTypes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = DeleteGrades.class)
    public JAXBElement<String> createDeleteGradesCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, DeleteGrades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "scaleId", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVOScaleId(String value) {
        return new JAXBElement<>(_ColumnVOScaleId_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "id", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVOId(String value) {
        return new JAXBElement<>(_ColumnVOId_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "description", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVODescription(String value) {
        return new JAXBElement<>(_ColumnVODescription_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "calculationType", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVOCalculationType(String value) {
        return new JAXBElement<>(_ColumnVOCalculationType_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "gradebookTypeId", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVOGradebookTypeId(String value) {
        return new JAXBElement<>(_ColumnVOGradebookTypeId_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "courseId", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVOCourseId(String value) {
        return new JAXBElement<>(_ColumnVOCourseId_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "columnDisplayName", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVOColumnDisplayName(String value) {
        return new JAXBElement<>(_ColumnVOColumnDisplayName_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "analysisUrl", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVOAnalysisUrl(String value) {
        return new JAXBElement<>(_ColumnVOAnalysisUrl_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "descriptionType", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVODescriptionType(String value) {
        return new JAXBElement<>(_ColumnVODescriptionType_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "columnName", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVOColumnName(String value) {
        return new JAXBElement<>(_ColumnVOColumnName_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "contentId", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVOContentId(String value) {
        return new JAXBElement<>(_ColumnVOContentId_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "aggregationModel", scope = ColumnVO.class)
    public JAXBElement<String> createColumnVOAggregationModel(String value) {
        return new JAXBElement<>(_ColumnVOAggregationModel_QNAME, String.class, ColumnVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "method", scope = GetRequiredEntitlements.class)
    public JAXBElement<String> createGetRequiredEntitlementsMethod(String value) {
        return new JAXBElement<>(_GetRequiredEntitlementsMethod_QNAME, String.class, GetRequiredEntitlements.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = DeleteGradingSchemas.class)
    public JAXBElement<String> createDeleteGradingSchemasCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, DeleteGradingSchemas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "return", scope = GetServerVersionResponse.class)
    public JAXBElement<VersionVO> createGetServerVersionResponseReturn(VersionVO value) {
        return new JAXBElement<>(_GetServerVersionResponseReturn_QNAME, VersionVO.class, GetServerVersionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "manualScore", scope = ScoreVO.class)
    public JAXBElement<Double> createScoreVOManualScore(Double value) {
        return new JAXBElement<>(_ScoreVOManualScore_QNAME, Double.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "id", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOId(String value) {
        return new JAXBElement<>(_ColumnVOId_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "userId", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOUserId(String value) {
        return new JAXBElement<>(_ScoreVOUserId_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "shortStudentComments", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOShortStudentComments(String value) {
        return new JAXBElement<>(_ScoreVOShortStudentComments_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "lowestAttemptId", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOLowestAttemptId(String value) {
        return new JAXBElement<>(_ScoreVOLowestAttemptId_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "memberId", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOMemberId(String value) {
        return new JAXBElement<>(_ScoreVOMemberId_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "schemaGradeValue", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOSchemaGradeValue(String value) {
        return new JAXBElement<>(_ScoreVOSchemaGradeValue_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "grade", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOGrade(String value) {
        return new JAXBElement<>(_ScoreVOGrade_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "lastAttemptId", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOLastAttemptId(String value) {
        return new JAXBElement<>(_ScoreVOLastAttemptId_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "manualGrade", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOManualGrade(String value) {
        return new JAXBElement<>(_ScoreVOManualGrade_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "averageScore", scope = ScoreVO.class)
    public JAXBElement<Double> createScoreVOAverageScore(Double value) {
        return new JAXBElement<>(_ScoreVOAverageScore_QNAME, Double.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "studentComments", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOStudentComments(String value) {
        return new JAXBElement<>(_ScoreVOStudentComments_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "shortInstructorComments", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOShortInstructorComments(String value) {
        return new JAXBElement<>(_ScoreVOShortInstructorComments_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "columnId", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOColumnId(String value) {
        return new JAXBElement<>(_ScoreVOColumnId_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "highestAttemptId", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOHighestAttemptId(String value) {
        return new JAXBElement<>(_ScoreVOHighestAttemptId_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "courseId", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOCourseId(String value) {
        return new JAXBElement<>(_ColumnVOCourseId_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "instructorComments", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOInstructorComments(String value) {
        return new JAXBElement<>(_ScoreVOInstructorComments_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "firstAttemptId", scope = ScoreVO.class)
    public JAXBElement<String> createScoreVOFirstAttemptId(String value) {
        return new JAXBElement<>(_ScoreVOFirstAttemptId_QNAME, String.class, ScoreVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = SaveGrades.class)
    public JAXBElement<String> createSaveGradesCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, SaveGrades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "id", scope = GradingSchemaSymbolVO.class)
    public JAXBElement<String> createGradingSchemaSymbolVOId(String value) {
        return new JAXBElement<>(_ColumnVOId_QNAME, String.class, GradingSchemaSymbolVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "symbol", scope = GradingSchemaSymbolVO.class)
    public JAXBElement<String> createGradingSchemaSymbolVOSymbol(String value) {
        return new JAXBElement<>(_GradingSchemaSymbolVOSymbol_QNAME, String.class, GradingSchemaSymbolVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "gradingSchemaId", scope = GradingSchemaSymbolVO.class)
    public JAXBElement<String> createGradingSchemaSymbolVOGradingSchemaId(String value) {
        return new JAXBElement<>(_GradingSchemaSymbolVOGradingSchemaId_QNAME, String.class, GradingSchemaSymbolVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "id", scope = AttemptFilter.class)
    public JAXBElement<String> createAttemptFilterId(String value) {
        return new JAXBElement<>(_ColumnVOId_QNAME, String.class, AttemptFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "gradeId", scope = AttemptFilter.class)
    public JAXBElement<String> createAttemptFilterGradeId(String value) {
        return new JAXBElement<>(_AttemptFilterGradeId_QNAME, String.class, AttemptFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = SaveGradingSchemas.class)
    public JAXBElement<String> createSaveGradingSchemasCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, SaveGradingSchemas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GradebookTypeFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "filter", scope = GetGradebookTypes.class)
    public JAXBElement<GradebookTypeFilter> createGetGradebookTypesFilter(GradebookTypeFilter value) {
        return new JAXBElement<>(_GetGradebookColumnsFilter_QNAME, GradebookTypeFilter.class, GetGradebookTypes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = GetGradebookTypes.class)
    public JAXBElement<String> createGetGradebookTypesCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, GetGradebookTypes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "title", scope = GradingSchemaFilter.class)
    public JAXBElement<String> createGradingSchemaFilterTitle(String value) {
        return new JAXBElement<>(_GradebookTypeFilterTitle_QNAME, String.class, GradingSchemaFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GradingSchemaFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "filter", scope = GetGradingSchemas.class)
    public JAXBElement<GradingSchemaFilter> createGetGradingSchemasFilter(GradingSchemaFilter value) {
        return new JAXBElement<>(_GetGradebookColumnsFilter_QNAME, GradingSchemaFilter.class, GetGradingSchemas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = GetGradingSchemas.class)
    public JAXBElement<String> createGetGradingSchemasCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, GetGradingSchemas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = UpdateColumnAttribute.class)
    public JAXBElement<String> createUpdateColumnAttributeCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, UpdateColumnAttribute.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "id", scope = AttemptVO.class)
    public JAXBElement<String> createAttemptVOId(String value) {
        return new JAXBElement<>(_ColumnVOId_QNAME, String.class, AttemptVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "studentSubmission", scope = AttemptVO.class)
    public JAXBElement<String> createAttemptVOStudentSubmission(String value) {
        return new JAXBElement<>(_AttemptVOStudentSubmission_QNAME, String.class, AttemptVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "gradeId", scope = AttemptVO.class)
    public JAXBElement<String> createAttemptVOGradeId(String value) {
        return new JAXBElement<>(_AttemptFilterGradeId_QNAME, String.class, AttemptVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "studentSubmissionTextType", scope = AttemptVO.class)
    public JAXBElement<String> createAttemptVOStudentSubmissionTextType(String value) {
        return new JAXBElement<>(_AttemptVOStudentSubmissionTextType_QNAME, String.class, AttemptVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "status", scope = AttemptVO.class)
    public JAXBElement<String> createAttemptVOStatus(String value) {
        return new JAXBElement<>(_AttemptVOStatus_QNAME, String.class, AttemptVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "studentComments", scope = AttemptVO.class)
    public JAXBElement<String> createAttemptVOStudentComments(String value) {
        return new JAXBElement<>(_ScoreVOStudentComments_QNAME, String.class, AttemptVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "instructorNotes", scope = AttemptVO.class)
    public JAXBElement<String> createAttemptVOInstructorNotes(String value) {
        return new JAXBElement<>(_AttemptVOInstructorNotes_QNAME, String.class, AttemptVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "grade", scope = AttemptVO.class)
    public JAXBElement<String> createAttemptVOGrade(String value) {
        return new JAXBElement<>(_ScoreVOGrade_QNAME, String.class, AttemptVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "displayGrade", scope = AttemptVO.class)
    public JAXBElement<String> createAttemptVODisplayGrade(String value) {
        return new JAXBElement<>(_AttemptVODisplayGrade_QNAME, String.class, AttemptVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "groupAttemptId", scope = AttemptVO.class)
    public JAXBElement<String> createAttemptVOGroupAttemptId(String value) {
        return new JAXBElement<>(_AttemptVOGroupAttemptId_QNAME, String.class, AttemptVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "feedbackToUser", scope = AttemptVO.class)
    public JAXBElement<String> createAttemptVOFeedbackToUser(String value) {
        return new JAXBElement<>(_AttemptVOFeedbackToUser_QNAME, String.class, AttemptVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ScoreFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "filter", scope = GetGrades.class)
    public JAXBElement<ScoreFilter> createGetGradesFilter(ScoreFilter value) {
        return new JAXBElement<>(_GetGradebookColumnsFilter_QNAME, ScoreFilter.class, GetGrades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = GetGrades.class)
    public JAXBElement<String> createGetGradesCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, GetGrades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "title", scope = GradebookTypeVO.class)
    public JAXBElement<String> createGradebookTypeVOTitle(String value) {
        return new JAXBElement<>(_GradebookTypeFilterTitle_QNAME, String.class, GradebookTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "id", scope = GradebookTypeVO.class)
    public JAXBElement<String> createGradebookTypeVOId(String value) {
        return new JAXBElement<>(_ColumnVOId_QNAME, String.class, GradebookTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "displayedDescription", scope = GradebookTypeVO.class)
    public JAXBElement<String> createGradebookTypeVODisplayedDescription(String value) {
        return new JAXBElement<>(_GradebookTypeVODisplayedDescription_QNAME, String.class, GradebookTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "description", scope = GradebookTypeVO.class)
    public JAXBElement<String> createGradebookTypeVODescription(String value) {
        return new JAXBElement<>(_ColumnVODescription_QNAME, String.class, GradebookTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "courseId", scope = GradebookTypeVO.class)
    public JAXBElement<String> createGradebookTypeVOCourseId(String value) {
        return new JAXBElement<>(_ColumnVOCourseId_QNAME, String.class, GradebookTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "displayedTitle", scope = GradebookTypeVO.class)
    public JAXBElement<String> createGradebookTypeVODisplayedTitle(String value) {
        return new JAXBElement<>(_GradebookTypeVODisplayedTitle_QNAME, String.class, GradebookTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = DeleteColumns.class)
    public JAXBElement<String> createDeleteColumnsCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, DeleteColumns.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttemptFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "filter", scope = GetAttempts.class)
    public JAXBElement<AttemptFilter> createGetAttemptsFilter(AttemptFilter value) {
        return new JAXBElement<>(_GetGradebookColumnsFilter_QNAME, AttemptFilter.class, GetAttempts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = GetAttempts.class)
    public JAXBElement<String> createGetAttemptsCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, GetAttempts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "id", scope = ScoreFilter.class)
    public JAXBElement<String> createScoreFilterId(String value) {
        return new JAXBElement<>(_ColumnVOId_QNAME, String.class, ScoreFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "columnId", scope = ScoreFilter.class)
    public JAXBElement<String> createScoreFilterColumnId(String value) {
        return new JAXBElement<>(_ScoreVOColumnId_QNAME, String.class, ScoreFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = SaveColumns.class)
    public JAXBElement<String> createSaveColumnsCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, SaveColumns.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "unused", scope = GetServerVersion.class)
    public JAXBElement<VersionVO> createGetServerVersionUnused(VersionVO value) {
        return new JAXBElement<>(_GetServerVersionUnused_QNAME, VersionVO.class, GetServerVersion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard", name = "courseId", scope = DeleteGradebookTypes.class)
    public JAXBElement<String> createDeleteGradebookTypesCourseId(String value) {
        return new JAXBElement<>(_DeleteAttemptsCourseId_QNAME, String.class, DeleteGradebookTypes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "title", scope = GradingSchemaVO.class)
    public JAXBElement<String> createGradingSchemaVOTitle(String value) {
        return new JAXBElement<>(_GradebookTypeFilterTitle_QNAME, String.class, GradingSchemaVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "id", scope = GradingSchemaVO.class)
    public JAXBElement<String> createGradingSchemaVOId(String value) {
        return new JAXBElement<>(_ColumnVOId_QNAME, String.class, GradingSchemaVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "description", scope = GradingSchemaVO.class)
    public JAXBElement<String> createGradingSchemaVODescription(String value) {
        return new JAXBElement<>(_ColumnVODescription_QNAME, String.class, GradingSchemaVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "scaleType", scope = GradingSchemaVO.class)
    public JAXBElement<String> createGradingSchemaVOScaleType(String value) {
        return new JAXBElement<>(_GradingSchemaVOScaleType_QNAME, String.class, GradingSchemaVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gradebook.ws.blackboard/xsd", name = "courseId", scope = GradingSchemaVO.class)
    public JAXBElement<String> createGradingSchemaVOCourseId(String value) {
        return new JAXBElement<>(_ColumnVOCourseId_QNAME, String.class, GradingSchemaVO.class, value);
    }

}
