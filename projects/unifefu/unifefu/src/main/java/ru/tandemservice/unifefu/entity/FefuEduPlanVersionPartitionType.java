package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unifefu.entity.catalog.FefuSchedulePartitionType;
import ru.tandemservice.unifefu.entity.gen.FefuEduPlanVersionPartitionTypeGen;

/**
 * Тип разбиения для УПв
 */
public class FefuEduPlanVersionPartitionType extends FefuEduPlanVersionPartitionTypeGen
{
    public FefuEduPlanVersionPartitionType()
    {
    }

    public FefuEduPlanVersionPartitionType(EppEduPlanVersion version, FefuSchedulePartitionType partitionType)
    {
        this.setVersion(version);
        this.setPartitionType(partitionType);
    }
}