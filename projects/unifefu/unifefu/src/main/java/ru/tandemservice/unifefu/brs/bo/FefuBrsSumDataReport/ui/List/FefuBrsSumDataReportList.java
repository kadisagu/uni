/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unifefu.brs.base.FefuBrsReportListHandler;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.ui.View.FefuBrsSumDataReportView;
import ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport;

/**
 * @author nvankov
 * @since 12/16/13
 */
@Configuration
public class FefuBrsSumDataReportList extends BusinessComponentManager
{
    public static final String REPORT_DS = "reportDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(FefuBrsReportManager.instance().yearPartDSConfig())
                .addDataSource(searchListDS(REPORT_DS, reportDS(), reportDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint reportDS() {
        return this.columnListExtPointBuilder(REPORT_DS)
                .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")))
                .addColumn(publisherColumn("date", FefuBrsSumDataReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).businessComponent(FefuBrsSumDataReportView.class).order())
                .addColumn(textColumn("formativeOrgUnit", FefuBrsSumDataReport.formativeOrgUnit()))
                .addColumn(textColumn("yearPart", FefuBrsSumDataReport.yearPart()))
                .addColumn(textColumn("eduLevel", FefuBrsSumDataReport.eduLevel()))
                .addColumn(textColumn("developForm", FefuBrsSumDataReport.developForm()))
                .addColumn(textColumn("group", FefuBrsSumDataReport.group()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                        .alert(FormattedMessage.with().template("reportDS.delete.alert").parameter(FefuBrsSumDataReport.formingDateStr()).create())
                )
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler reportDSHandler()
    {
        return new FefuBrsReportListHandler(getName(), FefuBrsSumDataReport.class);
    }
}



    