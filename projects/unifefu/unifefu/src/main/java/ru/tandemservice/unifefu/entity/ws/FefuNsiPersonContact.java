package ru.tandemservice.unifefu.entity.ws;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiContactType;
import ru.tandemservice.unifefu.entity.ws.gen.FefuNsiPersonContactGen;

/**
 * @see ru.tandemservice.unifefu.entity.ws.gen.FefuNsiPersonContactGen
 */
public class FefuNsiPersonContact extends FefuNsiPersonContactGen
{
    public FefuNsiPersonContact()
    {
    }

    public FefuNsiPersonContact(Person person, FefuNsiContactType type, String field, String value, String comment)
    {
        setType(type);
        setField(field);
        setField3(value);
        setDescription(value);
        setPerson(person);
        setComment(comment);
        setTypeStr("Телефон");
    }

    public void update(String value)
    {
        setField3(value);
        setDescription(value);
    }

    public String getDisplayableContactType()
    {
        return (null != getType() ? getType().getTitle() : "Контакт") + " (НСИ)";
    }
}