/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.VersionCheckStateTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEpvCheckState;

import java.util.Date;

/**
 * @author Alexander Zhebko
 * @since 06.12.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "versionId"))
public class FefuEduPlanVersionCheckStateTabUI extends UIPresenter
{
    private Long _versionId;
    private FefuEpvCheckState _checkState;

    public Long getVersionId(){ return _versionId; }
    public void setVersionId(Long versionId){ _versionId = versionId; }

    public FefuEpvCheckState getCheckState(){ return _checkState; }
    public void setCheckState(FefuEpvCheckState checkState){ _checkState = checkState; }

    // calculated
    public boolean isCheckByUMUVisible(){ return !_checkState.isCheckedByUMU(); }
    public boolean isUncheckByUMUVisible(){ return _checkState.isCheckedByUMU(); }

    public boolean isCheckByCRKVisible(){ return !_checkState.isCheckedByCRK(); }
    public boolean isUncheckByCRKVisible(){ return _checkState.isCheckedByCRK(); }

    public boolean isCheckByOOPVisible(){ return !_checkState.isCheckedByOOP(); }
    public boolean isUncheckByOOPVisible(){ return _checkState.isCheckedByOOP(); }

    public boolean isAccepted(){return _checkState.getVersion().getState().getCode().equals(EppState.STATE_ACCEPTED);}
    public boolean isRejected(){return _checkState.getVersion().getState().getCode().equals(EppState.STATE_REJECTED);}

    public boolean isEduPlanAccepted(){return _checkState.getVersion().getEduPlan().getState().getCode().equals(EppState.STATE_ACCEPTED);}

    @Override
    public void onComponentRefresh()
    {
        _checkState = FefuEduPlanManager.instance().dao().getEpvCheckState(_versionId);
    }


    // listeners
    public void onClickCheckByUMU()
    {
        _checkState.setCheckedByUMU(true);
        _checkState.setCheckedByUMUDate(new Date());
        FefuEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }

    public void onClickUncheckByUMU()
    {
        _checkState.setCheckedByUMU(false);
        _checkState.setCheckedByUMUDate(null);
        FefuEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }

    public void onClickCheckByCRK()
    {
        _checkState.setCheckedByCRK(true);
        _checkState.setCheckedByCRKDate(new Date());
        FefuEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }

    public void onClickUncheckByCRK()
    {
        _checkState.setCheckedByCRK(false);
        _checkState.setCheckedByCRKDate(null);
        FefuEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }

    public void onClickCheckByOOP()
    {
        _checkState.setCheckedByOOP(true);
        _checkState.setCheckedByOOPDate(new Date());
        FefuEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }

    public void onClickUncheckByOOP()
    {
        _checkState.setCheckedByOOP(false);
        _checkState.setCheckedByOOPDate(null);
        FefuEduPlanManager.instance().dao().saveOrUpdate(_checkState);
    }
}