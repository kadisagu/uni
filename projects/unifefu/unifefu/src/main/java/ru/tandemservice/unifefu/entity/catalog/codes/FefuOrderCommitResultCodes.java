package ru.tandemservice.unifefu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Результат авто-проведения приказа"
 * Имя сущности : fefuOrderCommitResult
 * Файл data.xml : unifefu-templates.data.xml
 */
public interface FefuOrderCommitResultCodes
{
    /** Константа кода (code) элемента : Успешно (title) */
    String SUCCESS = "success";
    /** Константа кода (code) элемента : Пропущено (title) */
    String SKIPPED = "skipped";
    /** Константа кода (code) элемента : Ошибка идентификатора (title) */
    String ID_ERROR = "idError";
    /** Константа кода (code) элемента : Ошибка синхронизации номера и даты (title) */
    String CREDENTIALS_ERROR = "credentialsError";
    /** Константа кода (code) элемента : Ошибка проведения (title) */
    String COMMIT_ERROR = "commitError";

    Set<String> CODES = ImmutableSet.of(SUCCESS, SKIPPED, ID_ERROR, CREDENTIALS_ERROR, COMMIT_ERROR);
}
