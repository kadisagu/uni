/* $Id$ */
package ru.tandemservice.unifefu.dao.mdbio;

import com.healthmarketscience.jackcess.Database;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.io.IOException;

/**
 * @author Andrey Avetisov
 * @since 29.04.2015
 */
public interface IFefuImtsaCompetenceIODao
{
    SpringBeanCache<IFefuImtsaCompetenceIODao> instance = new SpringBeanCache<>(IFefuImtsaCompetenceIODao.class.getName());

    /**
     * Выбирает из mdb-файла уникальные сочетания Группы компетенций и названия компетенции
     * и импортирует их в базу. Если строка с такой группой и названием есть в базе,
     * то импорта этой строки не происходит.
     * @param mdb файл из которого импортируетсю компетенции
     * @return строка содержащая информацию о том, сколько компетенций было импортировано,
     * а сколько пропущено
     * @throws IOException
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    String doImportImtsaCompetence(Database mdb) throws IOException;
}
