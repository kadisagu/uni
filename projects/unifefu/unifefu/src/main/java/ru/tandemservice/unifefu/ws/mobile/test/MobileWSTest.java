/* $Id$ */
package ru.tandemservice.unifefu.ws.mobile.test;

import ru.tandemservice.unifefu.ws.directumtest.SecurityHandler;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.HandlerRegistry;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 19.06.2014
 */
public class MobileWSTest
{
    private static final int AUTO_TAKE_ELEMENTS_AMOUNT = 1000;

    public static void main(String[] args)
    {
        FefuMobileDataProviderServiceImplLocator locator = new FefuMobileDataProviderServiceImplLocator();
        try
        {
            HandlerRegistry registry = locator.getHandlerRegistry();
            QName servicePort = new QName("http://www.qa.com", "FefuMobileDataProviderServicePort");
            List handlerChain = registry.getHandlerChain(servicePort);
            HandlerInfo info = new HandlerInfo();
            info.setHandlerClass(SecurityHandler.class);
            handlerChain.add(info);

            //testGetCurrentEduYear(locator);
            //testGetEduYearList(locator);

            //testGetActiveStudentPersonsList(locator);
            //testGetNewOrUpdatedPersonsList(locator);
            //testGetDeletedPersonsList(locator);
            //testGetPersonById(locator);
            //testGetPersonsByIdList(locator);
            //testCommitEntityList(locator);

            //testGetActiveStudentList(locator);
            //testGetNewOrUpdatedStudentList(locator);
            //testCommitEntityList(locator);
            //testGetStudentById(locator);
            //testGetStudentsByIdList(locator);

            //testGetSubjectsIdList(locator);
            //testGetSubjectById(locator);
            //testGetSubjectsByIdList(locator);

            //testGetStudent2SemestersIdList(locator);
            //testGetStudent2SemesterById(locator);
            //testGetStudent2SubjectsByIdList(locator);

            //testGetPeriodsIdList(locator);
            //testGetNewOrUpdatedPeriodsList(locator);
            testGetDeletedPeriodsList(locator);
            //testGetPeriodById(locator);
            //testGetPeriodsByIdList(locator);

            //testGetResultsIdList(locator);
            //testGetResultById(locator);
            //testGetResultsByIdList(locator, true);

        } catch (Exception e)
        {

        }
    }

    private static void testGetEduYearList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        EduYear[] eduYearList = locator.getFefuMobileDataProviderServicePort().getEduYearsList();
        System.out.println("Next education years were received:");
        for (EduYear eduYear : eduYearList)
            System.out.println(eduYear.getId() + ", " + eduYear.getTitle() + ", " + eduYear.getStart_year());
    }

    private static void testGetCurrentEduYear(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        EduYear eduYear = locator.getFefuMobileDataProviderServicePort().getCurrentEduYear();
        System.out.println("Current edu year is: " + eduYear.getId() + ", " + eduYear.getTitle() + ", " + eduYear.getStart_year());
    }

    private static void testGetActiveStudentPersonsList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] personIdsList = locator.getFefuMobileDataProviderServicePort().getStudentPersonsIdsList();
        System.out.println("Next persons are active:");
        for (long id : personIdsList) System.out.println(id);
    }

    private static void testGetNewOrUpdatedPersonsList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] personIdsList = locator.getFefuMobileDataProviderServicePort().getNewAndUpdatedPersonsIdsList();
        System.out.println("Next persons were added or updated:");
        for (long id : personIdsList) System.out.println(id);
    }

    private static void testGetDeletedPersonsList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] personIdsList = locator.getFefuMobileDataProviderServicePort().getDeletedPersonsIdsList();
        System.out.println("Next persons were deleted:");
        for (long id : personIdsList) System.out.println(id);
    }

    private static void testGetPersonById(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        Person person = locator.getFefuMobileDataProviderServicePort().getPerson(1415881279264008053L); //1439152455563093877

        StringBuilder personStr = new StringBuilder();
        personStr.append(person.getId()).append(", ");
        personStr.append(person.getUsername()).append(", ");
        personStr.append(person.getLastname()).append(" ");
        personStr.append(person.getFirstname()).append(" ");
        personStr.append(person.getMiddlename()).append(", ");
        personStr.append(null == person.getSex() ? "" : ("0".equals(person.getSex()) ? "М" : "Ж")).append(", ");
        personStr.append(person.getBirthday());

        System.out.println(personStr.toString());
    }

    private static void testGetPersonsByIdList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] personIds = new long[]{1412170533584815989L, 1412183241239611253L, 1412188048904996725L, 1412268793932597109L, 1413003926462580597L,
                1413012739987130229L, 1414459164864292725L, 1414460821531535221L, 1414468046482319221L, 1414946967095093109L, 1415881136137578357L,
                1439056270608312181L, 1439143291801577333L, 1441496289883203445L, 1441511774327678837L, 1441524828056594293L, 1415881279264008053L};
        Person[] persons = locator.getFefuMobileDataProviderServicePort().getPersonsByIdList(personIds);

        for (Person person : persons)
        {
            StringBuilder studStr = new StringBuilder();
            studStr.append(person.getId()).append(", ");
            studStr.append(person.getUsername()).append(", ");
            studStr.append(person.getLastname()).append(" ");
            studStr.append(person.getFirstname()).append(" ");
            studStr.append(person.getMiddlename()).append(", ");
            studStr.append(null == person.getSex() ? "" : ("0".equals(person.getSex()) ? "М" : "Ж")).append(", ");
            studStr.append(person.getBirthday());
            System.out.println(studStr.toString());
        }
    }

    private static void testGetActiveStudentList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] studentIdsList = locator.getFefuMobileDataProviderServicePort().getStudentsIdsList();
        System.out.println("Next students are active:");
        for (long id : studentIdsList) System.out.println(id);
    }

    private static void testGetNewOrUpdatedStudentList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] studentIdsList = locator.getFefuMobileDataProviderServicePort().getNewAndUpdatedStudentsIdsList();
        System.out.println("Next students were added or updated:");
        for (long id : studentIdsList) System.out.println(id);
    }

    private static void testCommitEntityList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        String result = locator.getFefuMobileDataProviderServicePort().commitEntityByIdList(new long[]{1477498837979949595L});
        System.out.println("Commit result is \"" + result + "\"");
    }

    private static void testGetStudentById(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        Student student = locator.getFefuMobileDataProviderServicePort().getStudent(1443303995391617563L); //1443308141716974107

        StringBuilder studStr = new StringBuilder();
        studStr.append(student.getId()).append(", ");
        studStr.append(student.getPerson_id()).append(", ");
        studStr.append(student.getAcademic_group_id());
        System.out.println(studStr.toString());
    }

    private static void testGetStudentsByIdList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] studentIds = new long[]{1415886031664196123L, 1415886031667341851L, 1415886031670487579L, 1415886031673633307L, 1415886031676779035L,
                1415886031679924763L, 1415886031683070491L, 1415886031686216219L, 1415886031689361947L, 1415886031692507675L, 1415886031695653403L,
                1415886031698799131L, 1443303995391617563L, 1415886031701944859L, 1415886031705090587L, 1415886031708236315L};
        Student[] students = locator.getFefuMobileDataProviderServicePort().getStudentsByIdList(studentIds);

        for (Student stud : students)
        {
            StringBuilder studStr = new StringBuilder();
            studStr.append(stud.getId()).append(", ");
            studStr.append(stud.getPerson_id()).append(", ");
            studStr.append(stud.getAcademic_group_id()).append(" (");
            if (null != stud.getAcademic_group_title()) studStr.append(stud.getAcademic_group_title()).append(", ");
            if (null != stud.getAcademic_group_title()) studStr.append(stud.getAcademic_group_org_unit_title());
            studStr.append("), ").append(stud.getEdu_level_title()).append(", ");
            studStr.append(stud.getEdu_level_short_title());
            System.out.println(studStr.toString());
        }
    }

    private static void testGetSubjectsIdList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] subjectIdsList = locator.getFefuMobileDataProviderServicePort().getSubjectsIdsList();
        System.out.println("Next subjects are active:");
        for (long id : subjectIdsList) System.out.println(id);
    }

    private static void testGetSubjectById(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        Subject subject = locator.getFefuMobileDataProviderServicePort().getSubject(1460056028398232946L); //1460009219182306674

        StringBuilder subjStr = new StringBuilder();
        subjStr.append(subject.getId()).append(", ");
        subjStr.append(subject.getFullname()).append(", ");
        subjStr.append(subject.getShortname()).append(" ");

        if (null != subject.getSemesters() && subject.getSemesters().length > 0)
        {
            for (Semester semester : subject.getSemesters())
            {
                subjStr.append("\n\t\t\t");
                subjStr.append(semester.getNum()).append(". ");
                subjStr.append("Лекции: ").append(semester.getLectures()).append(", ");
                subjStr.append("Лабораторные: ").append(semester.getLabs()).append(", ");
                subjStr.append("Практики: ").append(semester.getPractices()).append(", ");
                subjStr.append("Экзамен: ").append(semester.getExam()).append(", ");
                subjStr.append("Зачёт: ").append(semester.getCredit()).append(", ");
            }
        }

        System.out.println(subjStr.toString());
    }

    private static void testGetSubjectsByIdList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] subjectIds = new long[]{1460056028398232946L, 1415886031667341851L, 1415886031670487579L, 1460009219182306674L, 1415886031676779035L,
                1415886031679924763L, 1415886031683070491L, 1460097010008335730L, 1415886031689361947L, 1415886031692507675L, 1415886031695653403L,
                1459916804628948338L, 1443303995391617563L, 1415886031701944859L, 1460090864694470002L, 1415886031708236315L, 1459963573639325042L,
                1459910596490700146L, 1459963646580368754L, 1459963647983363442L, 1459963647089976690L, 1459979336553080178L, 1460021083324818802L};

        Subject[] subjectsByIdList = locator.getFefuMobileDataProviderServicePort().getSubjectsByIdList(subjectIds); //1460009219182306674

        for (Subject subject : subjectsByIdList)
        {
            StringBuilder subjStr = new StringBuilder();
            subjStr.append(subject.getId()).append(", ");
            subjStr.append(subject.getFullname()).append(", ");
            subjStr.append(subject.getShortname()).append(" ");

            if (null != subject.getSemesters() && subject.getSemesters().length > 0)
            {
                for (Semester semester : subject.getSemesters())
                {
                    subjStr.append("\n\t\t\t");
                    subjStr.append(semester.getNum()).append(". ");
                    subjStr.append("Лекции: ").append(semester.getLectures()).append(", ");
                    subjStr.append("Лабораторные: ").append(semester.getLabs()).append(", ");
                    subjStr.append("Практики: ").append(semester.getPractices()).append(", ");
                    subjStr.append("Самостоятельные: ").append(semester.getSeminars()).append(", ");
                    subjStr.append("Экзамен: ").append(semester.getExam()).append(", ");
                    subjStr.append("Зачёт: ").append(semester.getCredit()).append(", ");
                }
            }

            System.out.println(subjStr.toString());
        }
    }


    private static void testGetStudent2SemestersIdList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] student2SemesterIdsList = locator.getFefuMobileDataProviderServicePort().getStudent2SemestersIdsList();
        System.out.println("Next student2subjects are active:");
        for (long id : student2SemesterIdsList) System.out.println(id);
    }

    private static void testGetStudent2SemesterById(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        Student2Semester student2Semester = locator.getFefuMobileDataProviderServicePort().getStudent2Semester(1465417694351730547L);

        StringBuilder subjStr = new StringBuilder();
        subjStr.append(student2Semester.getId()).append(", ");
        subjStr.append(student2Semester.getStudent_id()).append(", ");
        subjStr.append(student2Semester.getSemester_id()).append(", ");
        subjStr.append(student2Semester.getEdu_year_id()).append(", ");
        subjStr.append(student2Semester.getSem_number());

        System.out.println(subjStr.toString());
    }

    private static void testGetStudent2SubjectsByIdList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] student2subjectIds = new long[]{1477603574581618547L, 1477603574517655411L, 1477603574484100979L, 1477603574437963635L, 1477601462683494259L,
                1477601461718804339L, 1477601461683152755L, 1477601461646452595L, 1477601461617092467L, 1477601461581440883L, 1477601461551032179L,
                1477601460562225011L, 1477601460524476275L, 1477601460495116147L, 1477601460448978803L, 1477601460414375795L, 1477601460375578483L,
                1472331835337930611L, 1471923975269447539L, 1471923520833386355L, 1471692538426488691L, 1465501857514792819L, 1465501857477044083L};
        long[] arr = new long[4000];
        long[] whole = locator.getFefuMobileDataProviderServicePort().getStudent2SemestersIdsList();
        for (int i = 0; i < 4000; i++) arr[i] = whole[i];
        Student2Semester[] student2SubjectsByIdList = locator.getFefuMobileDataProviderServicePort().getStudent2SemestersByIdList(arr/*student2subjectIds*/);

        for (Student2Semester student2Semester : student2SubjectsByIdList)
        {
            StringBuilder subjStr = new StringBuilder();
            subjStr.append(student2Semester.getId()).append(", ");
            subjStr.append(student2Semester.getStudent_id()).append(", ");
            subjStr.append(student2Semester.getSemester_id()).append(", ");
            subjStr.append(student2Semester.getEdu_year_id()).append(", ");
            subjStr.append(student2Semester.getSem_number());

            System.out.println(subjStr.toString());
        }
    }


    private static void testGetPeriodsIdList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] periodsIdsList = locator.getFefuMobileDataProviderServicePort().getPeriodsIdsList();
        System.out.println("Next periods are active:");
        for (long id : periodsIdsList) System.out.println(id);
    }

    private static void testGetNewOrUpdatedPeriodsList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] periodIdsList = locator.getFefuMobileDataProviderServicePort().getNewAndUpdatedPeriodIdsList();
        System.out.println("Next periods were added or updated:");
        for (long id : periodIdsList) System.out.println(id);
    }

    private static void testGetDeletedPeriodsList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] periodIdsList = locator.getFefuMobileDataProviderServicePort().getDeletedPeriodIdsList();
        System.out.println("Next periods were deleted:");
        for (long id : periodIdsList) System.out.println(id);
    }

    private static void testGetPeriodById(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        Period period = locator.getFefuMobileDataProviderServicePort().getPeriod(1479290621702758474L); //1477598270380895179l

        StringBuilder periodStr = new StringBuilder();
        periodStr.append(period.getId()).append(", ");
        periodStr.append(period.getAcademic_group_id()).append(", ");
        periodStr.append(period.getType()).append(", ");
        periodStr.append("[").append(period.getDate()).append("], ");
        periodStr.append(period.getDate_start()).append(" - ");
        periodStr.append(period.getDate_end()).append(", ");
        periodStr.append(period.getWeek()).append(", ");
        periodStr.append(period.getDay()).append(", ");
        periodStr.append(period.getPosition()).append(", ");
        periodStr.append(period.getTime_start()).append(" - ");
        periodStr.append(period.getTime_end()).append(", ");
        periodStr.append(period.getPlace()).append(", ");
        periodStr.append(period.getLecturer()).append(", ");
        periodStr.append(period.getSubject_id()).append(", ");
        periodStr.append(period.getSubject_title()).append(", ");
        periodStr.append(period.getSubject_short_title());
        System.out.println(periodStr.toString());
    }

    private static void testGetPeriodsByIdList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] periodIds = new long[]{1472460013707525067L, 1477598270380895179L, 1477598431549123531L, 1477598525826591691L, 1477598604062944203L,
                1477598640761007051L, 1415886031683070491L, 1460097010008335730L, 1415886031689361947L, 1415886031692507675L, 1415886031695653403L,
                1459916804628948338L, 1443303995391617563L, 1415886031701944859L, 1460090864694470002L, 1415886031708236315L, 1459963573639325042L,
                1459910596490700146L, 1459963646580368754L, 1477598688312880075L, 1479290621702758474L, 1459979336553080178L, 1460021083324818802L};

        Period[] periodsByIdList = locator.getFefuMobileDataProviderServicePort().getPeriodsByIdList(periodIds);

        for (Period period : periodsByIdList)
        {
            StringBuilder periodStr = new StringBuilder();
            periodStr.append(period.getId()).append(", ");
            periodStr.append(period.getAcademic_group_id()).append(", ");
            periodStr.append(period.getType()).append(", ");
            periodStr.append("[").append(period.getDate()).append("], ");
            periodStr.append(period.getDate_start()).append(" - ");
            periodStr.append(period.getDate_end()).append(", ");
            periodStr.append(period.getWeek()).append(", ");
            periodStr.append(period.getDay()).append(", ");
            periodStr.append(period.getPosition()).append(", ");
            periodStr.append(period.getTime_start()).append(" - ");
            periodStr.append(period.getTime_end()).append(", ");
            periodStr.append(period.getPlace()).append(", ");
            periodStr.append(period.getLecturer()).append(", ");
            periodStr.append(period.getSubject_id()).append(", ");
            periodStr.append(period.getSubject_title()).append(", ");
            periodStr.append(period.getSubject_short_title());
            System.out.println(periodStr.toString());
        }
    }


    private static void testGetResultsIdList(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        long[] resultsIdsList = locator.getFefuMobileDataProviderServicePort().getResultsIdsList();
        System.out.println("Next results are active:");
        for (long id : resultsIdsList) System.out.println(id);
    }

    private static void testGetResultById(FefuMobileDataProviderServiceImplLocator locator) throws Exception
    {
        Result result = locator.getFefuMobileDataProviderServicePort().getResult(1484469026992746128L); //1472659850752228405l
        System.out.println(printResult(result, null).toString());

        //TODO del
        Result result1 = locator.getFefuMobileDataProviderServicePort().getResult(1484475422812466229L);
        System.out.println("\n\n");
        System.out.println(printResult(result, null).toString());
        System.out.println(printResult(result1, null).toString());
    }

    private static void testGetResultsByIdList(FefuMobileDataProviderServiceImplLocator locator, boolean takeReal) throws Exception
    {
        long[] resultIds = new long[]{1460056028398232946L, 1415886031667341851L, 1415886031670487579L, 1460009219182306674L, 1415886031676779035L,
                1415886031679924763L, 1415886031683070491L, 1460097010008335730L, 1415886031689361947L, 1415886031692507675L, 1415886031695653403L,
                1459916804628948338L, 1443303995391617563L, 1415886031701944859L, 1460090864694470002L, 1415886031708236315L, 1459963573639325042L,
                1459910596490700146L, 1459963646580368754L, 1459963647983363442L, 1459963647089976690L, 1459979336553080178L, 1460021083324818802L};

        if (takeReal)
        {
            resultIds = new long[AUTO_TAKE_ELEMENTS_AMOUNT];
            long[] whole = locator.getFefuMobileDataProviderServicePort().getResultsIdsList();
            for (int i = 0; i < Math.min(whole.length, AUTO_TAKE_ELEMENTS_AMOUNT); i++) resultIds[i] = whole[i];
        }

        Result[] resultsByIdList = locator.getFefuMobileDataProviderServicePort().getResultsByIdList(resultIds);
        for (Result result : resultsByIdList) System.out.println(printResult(result, null).toString());
    }

    private static StringBuilder printResult(Result result, StringBuilder resourceBuilder)
    {
        if (null == resourceBuilder) resourceBuilder = new StringBuilder();
        resourceBuilder.append("[").append(result.getId()).append("], ");
        resourceBuilder.append(result.getStudent_id()).append(", ");
        resourceBuilder.append(result.getSubject_id()).append(", ");
        resourceBuilder.append(result.getEdu_year_id()).append(", ");
        resourceBuilder.append(result.getSemester()).append(", ");
        resourceBuilder.append(null != result.getCredit() ? ("зачёт: " + result.getCredit()) : "-").append(", ");
        resourceBuilder.append(null != result.getExam() ? ("экзамен: " + result.getExam()) : "-").append(", ");
        resourceBuilder.append(null != result.getDiff_credit() ? ("диф. зачёт: " + result.getDiff_credit()) : "-").append(", ");
        resourceBuilder.append(result.getResult()).append(", ");
        resourceBuilder.append(result.getMax_result()).append(", ");

        if (null != result.getMilestones() && result.getMilestones().length > 0)
        {
            for (Milestone milestone : result.getMilestones())
            {
                resourceBuilder.append("\n\t\t\t");
                resourceBuilder.append(milestone.getTitle()).append(", ");
                resourceBuilder.append(milestone.getValue());
            }
        }

        if (null != result.getWork_results() && result.getWork_results().length > 0)
        {
            for (WorkResult workResult : result.getWork_results())
            {
                resourceBuilder.append("\n\t\t\t");
                resourceBuilder.append(workResult.getDate()).append(", ");
                resourceBuilder.append(workResult.getTitle()).append(", ");
                resourceBuilder.append(null != workResult.getValue() ? workResult.getValue() : "-").append(", ");
                resourceBuilder.append(null != workResult.getMax_value() ? workResult.getMax_value() : "-");
            }
        }

        return resourceBuilder;
    }
}