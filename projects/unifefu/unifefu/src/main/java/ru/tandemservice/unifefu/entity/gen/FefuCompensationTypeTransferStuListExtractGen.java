package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О переводе на другую основу оплаты обучения (вакантные бюджетные места)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuCompensationTypeTransferStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract";
    public static final String ENTITY_NAME = "fefuCompensationTypeTransferStuListExtract";
    public static final int VERSION_HASH = 1673330860;
    private static IEntityMeta ENTITY_META;

    public static final String P_PROTOCOL_DATE = "protocolDate";
    public static final String P_PROTOCOL_NUMBERS = "protocolNumbers";
    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_COMPENSATION_TYPE_NEW = "compensationTypeNew";
    public static final String L_COMPENSATION_TYPE_OLD = "compensationTypeOld";

    private Date _protocolDate;     // Дата протоколов
    private String _protocolNumbers;     // Номера протоколов
    private Date _transferDate;     // Дата перевода
    private DevelopForm _developForm;     // Форма освоения
    private CompensationType _compensationTypeNew;     // Новый вид возмещения затрат
    private CompensationType _compensationTypeOld;     // Старый вид возмещения затрат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата протоколов. Свойство не может быть null.
     */
    @NotNull
    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    /**
     * @param protocolDate Дата протоколов. Свойство не может быть null.
     */
    public void setProtocolDate(Date protocolDate)
    {
        dirty(_protocolDate, protocolDate);
        _protocolDate = protocolDate;
    }

    /**
     * @return Номера протоколов. Свойство не может быть null.
     */
    @NotNull
    @Length(max=512)
    public String getProtocolNumbers()
    {
        return _protocolNumbers;
    }

    /**
     * @param protocolNumbers Номера протоколов. Свойство не может быть null.
     */
    public void setProtocolNumbers(String protocolNumbers)
    {
        dirty(_protocolNumbers, protocolNumbers);
        _protocolNumbers = protocolNumbers;
    }

    /**
     * @return Дата перевода. Свойство не может быть null.
     */
    @NotNull
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата перевода. Свойство не может быть null.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationTypeNew()
    {
        return _compensationTypeNew;
    }

    /**
     * @param compensationTypeNew Новый вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeNew(CompensationType compensationTypeNew)
    {
        dirty(_compensationTypeNew, compensationTypeNew);
        _compensationTypeNew = compensationTypeNew;
    }

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationTypeOld()
    {
        return _compensationTypeOld;
    }

    /**
     * @param compensationTypeOld Старый вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeOld(CompensationType compensationTypeOld)
    {
        dirty(_compensationTypeOld, compensationTypeOld);
        _compensationTypeOld = compensationTypeOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuCompensationTypeTransferStuListExtractGen)
        {
            setProtocolDate(((FefuCompensationTypeTransferStuListExtract)another).getProtocolDate());
            setProtocolNumbers(((FefuCompensationTypeTransferStuListExtract)another).getProtocolNumbers());
            setTransferDate(((FefuCompensationTypeTransferStuListExtract)another).getTransferDate());
            setDevelopForm(((FefuCompensationTypeTransferStuListExtract)another).getDevelopForm());
            setCompensationTypeNew(((FefuCompensationTypeTransferStuListExtract)another).getCompensationTypeNew());
            setCompensationTypeOld(((FefuCompensationTypeTransferStuListExtract)another).getCompensationTypeOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuCompensationTypeTransferStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuCompensationTypeTransferStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuCompensationTypeTransferStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    return obj.getProtocolDate();
                case "protocolNumbers":
                    return obj.getProtocolNumbers();
                case "transferDate":
                    return obj.getTransferDate();
                case "developForm":
                    return obj.getDevelopForm();
                case "compensationTypeNew":
                    return obj.getCompensationTypeNew();
                case "compensationTypeOld":
                    return obj.getCompensationTypeOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    obj.setProtocolDate((Date) value);
                    return;
                case "protocolNumbers":
                    obj.setProtocolNumbers((String) value);
                    return;
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "compensationTypeNew":
                    obj.setCompensationTypeNew((CompensationType) value);
                    return;
                case "compensationTypeOld":
                    obj.setCompensationTypeOld((CompensationType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                        return true;
                case "protocolNumbers":
                        return true;
                case "transferDate":
                        return true;
                case "developForm":
                        return true;
                case "compensationTypeNew":
                        return true;
                case "compensationTypeOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    return true;
                case "protocolNumbers":
                    return true;
                case "transferDate":
                    return true;
                case "developForm":
                    return true;
                case "compensationTypeNew":
                    return true;
                case "compensationTypeOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    return Date.class;
                case "protocolNumbers":
                    return String.class;
                case "transferDate":
                    return Date.class;
                case "developForm":
                    return DevelopForm.class;
                case "compensationTypeNew":
                    return CompensationType.class;
                case "compensationTypeOld":
                    return CompensationType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuCompensationTypeTransferStuListExtract> _dslPath = new Path<FefuCompensationTypeTransferStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuCompensationTypeTransferStuListExtract");
    }
            

    /**
     * @return Дата протоколов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getProtocolDate()
     */
    public static PropertyPath<Date> protocolDate()
    {
        return _dslPath.protocolDate();
    }

    /**
     * @return Номера протоколов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getProtocolNumbers()
     */
    public static PropertyPath<String> protocolNumbers()
    {
        return _dslPath.protocolNumbers();
    }

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getCompensationTypeNew()
     */
    public static CompensationType.Path<CompensationType> compensationTypeNew()
    {
        return _dslPath.compensationTypeNew();
    }

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getCompensationTypeOld()
     */
    public static CompensationType.Path<CompensationType> compensationTypeOld()
    {
        return _dslPath.compensationTypeOld();
    }

    public static class Path<E extends FefuCompensationTypeTransferStuListExtract> extends ListStudentExtract.Path<E>
    {
        private PropertyPath<Date> _protocolDate;
        private PropertyPath<String> _protocolNumbers;
        private PropertyPath<Date> _transferDate;
        private DevelopForm.Path<DevelopForm> _developForm;
        private CompensationType.Path<CompensationType> _compensationTypeNew;
        private CompensationType.Path<CompensationType> _compensationTypeOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата протоколов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getProtocolDate()
     */
        public PropertyPath<Date> protocolDate()
        {
            if(_protocolDate == null )
                _protocolDate = new PropertyPath<Date>(FefuCompensationTypeTransferStuListExtractGen.P_PROTOCOL_DATE, this);
            return _protocolDate;
        }

    /**
     * @return Номера протоколов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getProtocolNumbers()
     */
        public PropertyPath<String> protocolNumbers()
        {
            if(_protocolNumbers == null )
                _protocolNumbers = new PropertyPath<String>(FefuCompensationTypeTransferStuListExtractGen.P_PROTOCOL_NUMBERS, this);
            return _protocolNumbers;
        }

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(FefuCompensationTypeTransferStuListExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Новый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getCompensationTypeNew()
     */
        public CompensationType.Path<CompensationType> compensationTypeNew()
        {
            if(_compensationTypeNew == null )
                _compensationTypeNew = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE_NEW, this);
            return _compensationTypeNew;
        }

    /**
     * @return Старый вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract#getCompensationTypeOld()
     */
        public CompensationType.Path<CompensationType> compensationTypeOld()
        {
            if(_compensationTypeOld == null )
                _compensationTypeOld = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE_OLD, this);
            return _compensationTypeOld;
        }

        public Class getEntityClass()
        {
            return FefuCompensationTypeTransferStuListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuCompensationTypeTransferStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
