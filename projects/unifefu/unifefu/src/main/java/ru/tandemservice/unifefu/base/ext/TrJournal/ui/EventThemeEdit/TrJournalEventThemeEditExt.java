package ru.tandemservice.unifefu.base.ext.TrJournal.ui.EventThemeEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unifefu.base.bo.FefuTrJournal.FefuTrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.EventThemeEdit.TrJournalEventThemeEdit;

/**
 * User: amakarova
 * Date: 12.12.13
 */
@Configuration
public class TrJournalEventThemeEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrJournalEventThemeEditExtUI.class.getSimpleName();

    @Autowired
    private TrJournalEventThemeEdit _trJournalEventThemeEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trJournalEventThemeEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrJournalEventThemeEditExtUI.class))
                .addAction(new TrJournalEventThemeEditClickApplyAction("onClickApply"))
                .addDataSource(selectDS(FefuTrJournalManager.TR_EVENT_ACTION_TYPES_DS, FefuTrJournalManager.instance().actionTypeDSHandler()))
                .addDataSource(selectDS(FefuTrJournalManager.TR_EVENT_ADDON_TYPES_DS, FefuTrJournalManager.instance().addonTypeDSHandler()))
                .create();
    }
}