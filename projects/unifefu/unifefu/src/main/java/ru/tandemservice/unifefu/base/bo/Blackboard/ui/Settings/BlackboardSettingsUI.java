/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.Settings;

import org.springframework.util.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.unifefu.ws.blackboard.BBConnector;
import ru.tandemservice.unifefu.ws.blackboard.BBContextHelper;
import ru.tandemservice.unifefu.ws.blackboard.BBSettingsVO;
import ru.tandemservice.unifefu.ws.blackboard.context.gen.RegisterToolResultVO;
import ru.tandemservice.unifefu.ws.blackboard.course.BBCourseUtils;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.CourseVO;
import ru.tandemservice.unifefu.ws.blackboard.membership.BBMembershipUtils;
import ru.tandemservice.unifefu.ws.blackboard.membership.gen.CourseMembershipVO;
import ru.tandemservice.unifefu.ws.blackboard.user.BBUserUtils;
import ru.tandemservice.unifefu.ws.blackboard.user.gen.*;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.*;

/**
 * @author Nikolay Fedorovskih
 * @since 05.02.2014
 */
public class BlackboardSettingsUI extends UIPresenter
{
    private BBSettingsVO _sets;
    private String _log;

    public static String testLogin()
    {
        try
        {
            BBContextHelper.getInstance().reInit();
            return BBConnector.login() ? "OK - success" : "FAIL - unknown error";
        }
        catch (SOAPFaultException e)
        {
            return "FAIL - " + e.getMessage();
        }
    }

    public static String testEmulate()
    {
        try
        {
            BBContextHelper.getInstance().reInit();
            return BBConnector.emulateUser() ? "OK - success" : "FAIL - unknown error";
        }
        catch (SOAPFaultException e)
        {
            return "FAIL - " + e.getMessage();
        }
    }

    public String testStudyCourse()
    {
        try
        {
            CourseVO courseVO = BBCourseUtils.findCourseByCourseId(_sets.getBbStudyCourseId());
            return courseVO != null ? "OK - курс найден" : "FAIL - курс с id \"" + _sets.getBbStudyCourseId() + "\" не найден";
        }
        catch (SOAPFaultException e)
        {
            return "FAIL - " + e.getMessage();
        }
    }

    public static String doRegisterTool(BBSettingsVO settingsVO)
    {
        try
        {
            RegisterToolResultVO registerRes = BBConnector.registerProxy(settingsVO);
            return registerRes.isStatus() ? "OK - success" : "FAIL - " + org.springframework.util.StringUtils.collectionToDelimitedString(registerRes.getFailureErrors(), ", ");
        }
        catch (SOAPFaultException e)
        {
            return "FAIL - " + e.getMessage();
        }
    }

    @Override
    public void onComponentRefresh()
    {
        setSets(BBSettingsVO.load(true));
    }

    public void onClickSave()
    {
        BBSettingsVO.saveSettings(getSets(), true);
    }

    public void onClickClear()
    {
        getSets().setDefaults();
    }

    public void onClickCheckAccess()
    {
        StringBuilder log = new StringBuilder();
        try
        {
            BBSettingsVO.setCachedVO(_sets);

/*
            UserVO userVO = BBUserUtils.findUserByUserName("nikitina.eyu"); // 37b24159-4bbd-47df-9883-932dd9d6c6c7
            log.append("User id: ").append(userVO.getUserBatchUid().getValue()).append("\n");

            Collection<CourseMembershipVO> membershipVOs = BBMembershipUtils.findCourseMembershipsByUserId(userVO.getId().getValue());
            List<String> ids = new ArrayList<>();
            for (CourseMembershipVO vo : membershipVOs)
            {
                ids.add(vo.getCourseId().getValue());
            }

            for (CourseVO vo : BBCourseUtils.findCoursesById(ids))
            {
                log.append(vo.getName().getValue()).append(" : ");
                log.append(vo.getCourseId().getValue()).append(" : ");
                log.append(vo.getDescription().getValue()).append("\n");
            }

            if (true)
                return;


            final CourseVO courseVO = BBCourseUtils.findCourseByCourseIdNotNull("0000000-00063-DOVPDYU-001");
            log.append("Course primary id: ").append(courseVO.getId().getValue()).append("\n");

//            UserVO userVO = BBUserUtils.findUser("e9435e2c-de50-3bc1-9ff5-1c61eec89208");
//            log.append("User id: ").append(userVO.getId().getValue()).append("\n");
//
//            CourseMembershipVO courseMembershipVO = BBMembershipUtils.getCourseMembership(courseVO.getId().getValue(), userVO.getId().getValue(), CourseMembershipWSConstants.STUDENT_ROLE);
//            log.append("Membership id: ").append(courseMembershipVO.getId().getValue()).append("\n");


            List<ColumnVO> columnVOList = BBContextHelper.getInstance().doGradebookRequest(new IBBRequest<GradebookWSPortType, ObjectFactory, List<ColumnVO>>()
            {
                @Override
                public List<ColumnVO> invoke(GradebookWSPortType gradebookWSPortType, ObjectFactory of)
                {
                    ColumnFilter columnFilter = of.createColumnFilter();
                    columnFilter.setFilterType(GradebookWSConstants.GET_COLUMN_BY_COURSE_ID);
                    columnFilter.setExternalGrade(false);
                    return gradebookWSPortType.getGradebookColumns(courseVO.getId().getValue(), columnFilter);
                }
            });

            String colId = null;
            {
                int i = 1;
                for (ColumnVO columnVO : columnVOList)
                {
                    log.append("Column ").append(i++).append(": ").append(columnVO.getColumnName().getValue()).append("\n")
                            .append("     Id: ").append(columnVO.getId().getValue()).append("\n")
                            .append("     GradebookTypeId: ").append(columnVO.getGradebookTypeId().getValue()).append("\n")
                            .append("     ColumnDisplayName: ").append(columnVO.getColumnDisplayName().getValue()).append("\n")
                            .append("     CalculationType: ").append(columnVO.getCalculationType().getValue()).append("\n")
                            .append("     AggregationModel: ").append(columnVO.getAggregationModel().getValue()).append("\n")
                            .append("     AnalysisUrl: ").append(columnVO.getAnalysisUrl().getValue()).append("\n")
                            .append("     ExpansionData: ").append(UniStringUtils.joinWithSeparator(", ", columnVO.getExpansionData().toArray(new String[]{}))).append("\n")
                            .append("     Weight: ").append(columnVO.getWeight()).append("\n")
                            .append("     MultipleAttempts: ").append(columnVO.getMultipleAttempts()).append("\n");

                    if (columnVO.getColumnName().getValue().equalsIgnoreCase("Тест 1"))
                        colId = columnVO.getId().getValue();
                }
            }


            {
                log.append("\n").append("Gradebook types").append("\n");
                List<GradebookTypeVO> list = BBContextHelper.getInstance().doGradebookRequest(new IBBRequest<GradebookWSPortType, ObjectFactory, List<GradebookTypeVO>>()
                {
                    @Override
                    public List<GradebookTypeVO> invoke(GradebookWSPortType gradebookWSPortType, ObjectFactory of)
                    {
                        GradebookTypeFilter filter = of.createGradebookTypeFilter();
                        filter.setFilterType(GradebookWSConstants.GET_GRADEBOOK_TYPE_BY_COURSE_ID);
                        return gradebookWSPortType.getGradebookTypes(courseVO.getId().getValue(), filter);
                    }
                });

                int i = 1;
                for (GradebookTypeVO vo : list)
                {
                    log.append("Type ").append(i++).append(": ").append(vo.getTitle().getValue()).append("\n")
                            .append("     Id: ").append(vo.getId().getValue()).append("\n")
                            .append("     DisplayedTitle: ").append(vo.getDisplayedTitle().getValue()).append("\n")
                            .append("     Description: ").append(vo.getDescription().getValue()).append("\n")
                            .append("     DisplayedDescription: ").append(vo.getDisplayedDescription().getValue()).append("\n")
                            .append("     Weight: ").append(vo.getWeight()).append("\n");
                }
            }

            {
                log.append("\n").append("Gradebook schemas").append("\n");
                List<GradingSchemaVO> list = BBContextHelper.getInstance().doGradebookRequest(new IBBRequest<GradebookWSPortType, ObjectFactory, List<GradingSchemaVO>>()
                {
                    @Override
                    public List<GradingSchemaVO> invoke(GradebookWSPortType gradebookWSPortType, ObjectFactory of)
                    {
                        GradingSchemaFilter filter = of.createGradingSchemaFilter();
                        filter.setFilterType(GradebookWSConstants.GET_GRADING_SCHEMA_BY_COURSE_ID);
                        return gradebookWSPortType.getGradingSchemas(courseVO.getId().getValue(), filter);
                    }
                });

                int i = 1;
                for (GradingSchemaVO vo : list)
                {
                    log.append("Schema ").append(i++).append(": ").append(vo.getTitle().getValue()).append("\n")
                            .append("     Id: ").append(vo.getId().getValue()).append("\n")
                            .append("     Description: ").append(vo.getDescription().getValue()).append("\n")
                            .append("     ScaleType: ").append(vo.getScaleType().getValue()).append("\n")
                            .append("     Version: ").append(vo.getVersion()).append("\n")
                            .append("     ExpansionData: ").append(UniStringUtils.joinWithSeparator(", ", vo.getExpansionData().toArray(new String[]{}))).append("\n")
                            .append("     isNumeric: ").append(vo.isNumeric()).append("\n")
                            .append("     isPercentage: ").append(vo.isPercentage()).append("\n")
                            .append("     isTabular: ").append(vo.isTabular()).append("\n")
                            .append("     isUserDefined: ").append(vo.isUserDefined()).append("\n")
                            ;

                    for (GradingSchemaSymbolVO schemaSymbolVO :  vo.getSymbols())
                    {
                        log.append("     schemaSymbolVO: ").append(schemaSymbolVO.getSymbol().getValue()).append("\n")
                                .append("          AbsoluteTranslation: ").append(schemaSymbolVO.getAbsoluteTranslation()).append("\n")
                                .append("          LowerBound: ").append(schemaSymbolVO.getLowerBound()).append("\n")
                                .append("          UpperBound: ").append(schemaSymbolVO.getUpperBound()).append("\n")
                                .append("          ExpansionData: ").append(schemaSymbolVO.getExpansionData()).append("\n")
                                ;
                    }
                }
            }

            {
                log.append("\n").append("Scores ").append("\n");
                List<ScoreVO> list = BBContextHelper.getInstance().doGradebookRequest(new IBBRequest<GradebookWSPortType, ObjectFactory, List<ScoreVO>>()
                {
                    @Override
                    public List<ScoreVO> invoke(GradebookWSPortType gradebookWSPortType, ObjectFactory of)
                    {
                        ScoreFilter filter = of.createScoreFilter();
                        filter.setFilterType(GradebookWSConstants.GET_SCORE_BY_COURSE_ID);
                        return gradebookWSPortType.getGrades(courseVO.getId().getValue(), filter);
                    }
                });

                int i = 1;
                for (ScoreVO scoreVO : list)
                {
                    if (scoreVO == null)
                        continue;

                    log.append("Score for user id").append(i++).append(": ").append(scoreVO.getUserId() != null ? scoreVO.getUserId().getValue() : "null").append("\n")
                            .append("     ColumnId: ").append(scoreVO.getColumnId().getValue()).append("\n")
                            .append("     MemberId: ").append(scoreVO.getMemberId().getValue()).append("\n")
                            .append("     Grade: ").append(scoreVO.getGrade().getValue()).append("\n")
                            .append("     SchemaGradeValue: ").append(scoreVO.getSchemaGradeValue().getValue()).append("\n")
                            .append("     Status: ").append(scoreVO.getStatus()).append("\n")
                            .append("     AverageScore: ").append(scoreVO.getAverageScore().getValue()).append("\n")
                            .append("     FirstAttemptId: ").append(scoreVO.getFirstAttemptId().getValue()).append("\n")
                            .append("     LowestAttemptId: ").append(scoreVO.getLowestAttemptId().getValue()).append("\n")
                            .append("     HighestAttemptId: ").append(scoreVO.getHighestAttemptId().getValue()).append("\n")
                            .append("     ShortInstructorComments: ").append(scoreVO.getShortInstructorComments().getValue()).append("\n")
                            .append("     InstructorComments: ").append(scoreVO.getInstructorComments().getValue()).append("\n")
                            .append("     ShortStudentComments: ").append(scoreVO.getShortStudentComments().getValue()).append("\n")
                            .append("     StudentComments: ").append(scoreVO.getStudentComments().getValue()).append("\n")
                            .append("     ManualGrade: ").append(scoreVO.getManualGrade().getValue()).append("\n")
                            .append("     ManualScore: ").append(scoreVO.getManualScore().getValue()).append("\n")
                            .append("     Exempt: ").append(scoreVO.isExempt()).append("\n")
                            .append("     ExpansionData: ").append(UniStringUtils.joinWithSeparator(", ", scoreVO.getExpansionData().toArray(new String[]{}))).append("\n")
                            ;
                }


            }


*/
//            List<UserVO> list = BBContextHelper.getInstance().doUserRequest(new IBBRequest<UserWSPortType, ru.tandemservice.unifefu.ws.blackboard.user.gen.ObjectFactory, List<UserVO>>()
//            {
//                @Override
//                public List<UserVO> invoke(UserWSPortType userWSPortType, ru.tandemservice.unifefu.ws.blackboard.user.gen.ObjectFactory of)
//                {
//                    UserFilter userFilter = of.createUserFilter();
//                    userFilter.setFilterType(UserWSConstants.GET_USER_BY_NAME_WITH_AVAILABILITY);
//                    userFilter.getName().add("nikitina.eyu");
//                    return userWSPortType.getUser(userFilter);
//                }
//            });
//            log.append(list.get(0).getUserBatchUid().getValue());

            log.append("LoginTool:\t\t").append(testLogin()).append("\n");
            log.append("Emulate user:\t\t").append(testEmulate()).append("\n");
            log.append("Курс «e-Learning...»:\t").append(testStudyCourse()).append("\n");
            log.append("ContextWS:\t\tversion ").append(BBConnector.getContextWSVersion()).append("\n");
            log.append("UserWS:\t\t\tversion ").append(BBConnector.getUserWSVersion()).append("\n");
            log.append("CourseWS:\t\tversion ").append(BBConnector.getCourseWSVersion()).append("\n");
            log.append("CourseMembershipWS:\tversion ").append(BBConnector.getCourseMembershipWSVersion()).append("\n");
            log.append("GradebookWS:\t\tversion ").append(BBConnector.getGradebookWSVersion()).append("\n");
            log.append("System Installation Id:\t").append(BBConnector.getSystemInstallationId()).append("\n");
        }
        finally
        {
            BBSettingsVO.setCachedVO(null);
            setLog(log.toString());
        }
    }

    public void onClickRegisterTool()
    {
        setLog("RegisterTool: " + doRegisterTool(_sets) + "\n");
    }

    public BBSettingsVO getSets()
    {
        return _sets;
    }

    public void setSets(BBSettingsVO sets)
    {
        _sets = sets;
    }

    public String getRequiredToolMethods()
    {
        return StringUtils.collectionToDelimitedString(getSets().getRequiredToolMethods(), "\n");
    }

    public void setRequiredToolMethods(String value)
    {
        _sets.setRequiredToolMethods(Arrays.asList(StringUtils.delimitedListToStringArray(value, "\n")));
    }

    public String getLog()
    {
        return _log;
    }

    public void setLog(String log)
    {
        _log = log;
    }
}