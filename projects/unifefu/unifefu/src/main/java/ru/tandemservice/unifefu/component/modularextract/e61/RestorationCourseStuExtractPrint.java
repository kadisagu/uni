/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e61;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.entity.RestorationCourseStuExtract;

/**
 * @author Alexey Lopatin
 * @since 10.12.2013
 */
public class RestorationCourseStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e61.RestorationCourseStuExtractPrint
{
    @Override
    public void additionalModify(RtfInjectModifier modifier, RestorationCourseStuExtract extract)
    {
        modifier.put("fefuRestorationTerm", extract.isPrintRestorationDateAndTerm() && extract.getRestorationTerm() != null ? " (" + extract.getRestorationTerm().getTitle() + " семестр)" : "");
        modifier.put("fefuRestorationDate", extract.isPrintRestorationDateAndTerm() ? " с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getRestorationDate()) : "");
    }
}
