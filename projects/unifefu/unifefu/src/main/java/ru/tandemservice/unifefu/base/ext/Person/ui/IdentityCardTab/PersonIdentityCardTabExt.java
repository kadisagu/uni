/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Person.ui.IdentityCardTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.Person.ui.IdentityCardTab.PersonIdentityCardTab;

/**
 * @author Dmitry Seleznev
 * @since 09.10.2012
 */
@Configuration
public class PersonIdentityCardTabExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + PersonIdentityCardTabExtUI.class.getSimpleName();

    @Autowired
    private PersonIdentityCardTab _personIdentityCardTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_personIdentityCardTab.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, PersonIdentityCardTabExtUI.class))
                .create();
    }
}