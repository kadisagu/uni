/**
 * FefuMobileDataProviderService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.mobile.test;

public interface FefuMobileDataProviderService extends java.rmi.Remote {
    public long[] getDeletedSubjectIdsList() throws java.rmi.RemoteException;
    public java.lang.String commitEntityByIdList(long[] entityIdList) throws java.rmi.RemoteException;
    public long[] getStudent2SemestersIdsList() throws java.rmi.RemoteException;
    public long[] getNewAndUpdatedPeriodIdsList() throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Result[] getResultsByIdList(long[] resultIdList) throws java.rmi.RemoteException;
    public long[] getDeletedStudentsIdsList() throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Student2Semester getStudent2Semester(java.lang.Long studentToSemesterId) throws java.rmi.RemoteException;
    public long[] getResultsIdsList() throws java.rmi.RemoteException;
    public long[] getSubjectsIdsList() throws java.rmi.RemoteException;
    public long[] getNewAndUpdatedResultIdsList() throws java.rmi.RemoteException;
    public long[] getDeletedPeriodIdsList() throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Student[] getStudentsByIdList(long[] studentIdList) throws java.rmi.RemoteException;
    public long[] getDeletedStudent2SemesterIdsList() throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.EduYear[] getEduYearsList() throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Period getPeriod(java.lang.Long periodId) throws java.rmi.RemoteException;
    public long[] getNewAndUpdatedSubjectIdsList() throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Subject[] getSubjectsByIdList(long[] subjectIdList) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Subject getSubject(java.lang.Long subjectId) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Student getStudent(java.lang.Long studentId) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Result getResult(java.lang.Long resultId) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Person[] getPersonsByIdList(long[] personIdList) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.EduYear getCurrentEduYear() throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Student2Semester[] getStudent2SemestersByIdList(long[] student2SemesterIdList) throws java.rmi.RemoteException;
    public long[] getDeletedResultIdsList() throws java.rmi.RemoteException;
    public long[] getDeletedPersonsIdsList() throws java.rmi.RemoteException;
    public long[] getStudentsIdsList() throws java.rmi.RemoteException;
    public long[] getNewAndUpdatedPersonsIdsList() throws java.rmi.RemoteException;
    public long[] getStudentPersonsIdsList() throws java.rmi.RemoteException;
    public long[] getNewAndUpdatedStudent2SemesterIdsList() throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Person getPerson(java.lang.Long personId) throws java.rmi.RemoteException;
    public long[] getPeriodsIdsList() throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.mobile.test.Period[] getPeriodsByIdList(long[] periodIdList) throws java.rmi.RemoteException;
    public long[] getNewAndUpdatedStudentsIdsList() throws java.rmi.RemoteException;
}
