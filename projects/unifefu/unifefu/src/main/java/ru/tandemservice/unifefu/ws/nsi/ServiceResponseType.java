/**
 * ServiceResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.nsi;

public class ServiceResponseType  implements java.io.Serializable {
    private ru.tandemservice.unifefu.ws.nsi.CallCCType callCC;

    private java.lang.String callRC;

    private ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType routingHeader;

    private ru.tandemservice.unifefu.ws.nsi.ServiceResponseTypeDatagram datagram;

    public ServiceResponseType() {
    }

    public ServiceResponseType(
           ru.tandemservice.unifefu.ws.nsi.CallCCType callCC,
           java.lang.String callRC,
           ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType routingHeader,
           ru.tandemservice.unifefu.ws.nsi.ServiceResponseTypeDatagram datagram) {
           this.callCC = callCC;
           this.callRC = callRC;
           this.routingHeader = routingHeader;
           this.datagram = datagram;
    }


    /**
     * Gets the callCC value for this ServiceResponseType.
     * 
     * @return callCC
     */
    public ru.tandemservice.unifefu.ws.nsi.CallCCType getCallCC() {
        return callCC;
    }


    /**
     * Sets the callCC value for this ServiceResponseType.
     * 
     * @param callCC
     */
    public void setCallCC(ru.tandemservice.unifefu.ws.nsi.CallCCType callCC) {
        this.callCC = callCC;
    }


    /**
     * Gets the callRC value for this ServiceResponseType.
     * 
     * @return callRC
     */
    public java.lang.String getCallRC() {
        return callRC;
    }


    /**
     * Sets the callRC value for this ServiceResponseType.
     * 
     * @param callRC
     */
    public void setCallRC(java.lang.String callRC) {
        this.callRC = callRC;
    }


    /**
     * Gets the routingHeader value for this ServiceResponseType.
     * 
     * @return routingHeader
     */
    public ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType getRoutingHeader() {
        return routingHeader;
    }


    /**
     * Sets the routingHeader value for this ServiceResponseType.
     * 
     * @param routingHeader
     */
    public void setRoutingHeader(ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType routingHeader) {
        this.routingHeader = routingHeader;
    }


    /**
     * Gets the datagram value for this ServiceResponseType.
     * 
     * @return datagram
     */
    public ru.tandemservice.unifefu.ws.nsi.ServiceResponseTypeDatagram getDatagram() {
        return datagram;
    }


    /**
     * Sets the datagram value for this ServiceResponseType.
     * 
     * @param datagram
     */
    public void setDatagram(ru.tandemservice.unifefu.ws.nsi.ServiceResponseTypeDatagram datagram) {
        this.datagram = datagram;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceResponseType)) return false;
        ServiceResponseType other = (ServiceResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.callCC==null && other.getCallCC()==null) || 
             (this.callCC!=null &&
              this.callCC.equals(other.getCallCC()))) &&
            ((this.callRC==null && other.getCallRC()==null) || 
             (this.callRC!=null &&
              this.callRC.equals(other.getCallRC()))) &&
            ((this.routingHeader==null && other.getRoutingHeader()==null) || 
             (this.routingHeader!=null &&
              this.routingHeader.equals(other.getRoutingHeader()))) &&
            ((this.datagram==null && other.getDatagram()==null) || 
             (this.datagram!=null &&
              this.datagram.equals(other.getDatagram())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCallCC() != null) {
            _hashCode += getCallCC().hashCode();
        }
        if (getCallRC() != null) {
            _hashCode += getCallRC().hashCode();
        }
        if (getRoutingHeader() != null) {
            _hashCode += getRoutingHeader().hashCode();
        }
        if (getDatagram() != null) {
            _hashCode += getDatagram().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "ServiceResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("callCC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "callCC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "callCCType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("callRC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "callRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("routingHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "routingHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "RoutingHeaderType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datagram");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "datagram"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", ">ServiceResponseType>datagram"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
