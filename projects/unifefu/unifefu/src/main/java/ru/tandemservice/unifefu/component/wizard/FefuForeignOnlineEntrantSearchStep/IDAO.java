/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.FefuForeignOnlineEntrantSearchStep;

import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;

/**
 * @author nvankov
 * @since 6/24/13
 */
public interface IDAO extends IUniDao<Model>
{
    Entrant findEntrant(Model model);
}
