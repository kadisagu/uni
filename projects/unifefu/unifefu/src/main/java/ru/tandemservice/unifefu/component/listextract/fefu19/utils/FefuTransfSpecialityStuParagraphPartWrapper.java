/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 18.05.2015
 */
public class FefuTransfSpecialityStuParagraphPartWrapper implements Comparable<FefuTransfSpecialityStuParagraphPartWrapper>
{
    private final EducationLevelsHighSchool _educationLevel;
    private final Qualifications _qualification;
    private final ListStudentExtract _firstExtract;


    public FefuTransfSpecialityStuParagraphPartWrapper(EducationLevelsHighSchool educationLevel, Qualifications qualification, ListStudentExtract firstExtract)
    {
        _educationLevel = educationLevel;
        _qualification = qualification;
        _firstExtract = firstExtract;
    }

    private final List<Person> _personList = new ArrayList<>();

    public EducationLevelsHighSchool getEducationLevel()
    {
        return _educationLevel;
    }

    public Qualifications getQualification()
    {
        return _qualification;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuTransfSpecialityStuParagraphPartWrapper))
            return false;

        FefuTransfSpecialityStuParagraphPartWrapper that = (FefuTransfSpecialityStuParagraphPartWrapper) o;

        return _educationLevel.equals(that.getEducationLevel());
    }

    @Override
    public int hashCode()
    {
        return _educationLevel.hashCode();
    }

    @Override
    public int compareTo(FefuTransfSpecialityStuParagraphPartWrapper o)
    {
        if (!_educationLevel.getEducationLevel().isProfileOrSpecialization() && o.getEducationLevel().getEducationLevel().isProfileOrSpecialization())
            return -1;
        else if (_educationLevel.getEducationLevel().isProfileOrSpecialization() && !o.getEducationLevel().getEducationLevel().isProfileOrSpecialization())
            return 1;
        return _educationLevel.getFullTitle().compareTo(o.getEducationLevel().getFullTitle());

    }
}