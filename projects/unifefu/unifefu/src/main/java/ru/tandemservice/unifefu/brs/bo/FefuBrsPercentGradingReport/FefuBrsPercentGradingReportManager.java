/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.brs.base.IBaseFefuBrsObjectManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport.logic.FefuBrsPercentGradingReportDAO;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport.logic.IFefuBrsPercentGradingReportDAO;

/**
 * @author nvankov
 * @since 12/10/13
 */
@Configuration
public class FefuBrsPercentGradingReportManager extends BusinessObjectManager implements IBaseFefuBrsObjectManager
{
    @Override
    public String getPermissionKey()
    {
        return "fefuBrsPercentGradingReportPermissionKey";
    }

    public static FefuBrsPercentGradingReportManager instance()
    {
        return instance(FefuBrsPercentGradingReportManager.class);
    }

    @Bean
    public IFefuBrsPercentGradingReportDAO dao()
    {
        return new FefuBrsPercentGradingReportDAO();
    }
}



    