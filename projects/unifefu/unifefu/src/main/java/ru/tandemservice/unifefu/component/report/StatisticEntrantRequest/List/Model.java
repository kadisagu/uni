// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.report.StatisticEntrantRequest.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.entity.report.StatisticEntrantRequestReport;

import java.util.List;

/**
 * @author amakarova
 * @since 13.06.2013
 */

public class Model implements IEnrollmentCampaignSelectModel
{
    private IDataSettings _settings;
    private DynamicListDataSource<StatisticEntrantRequestReport> _dataSource;
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    // IEnrollmentCampaignSelectModel

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) _settings.get(ENROLLMENT_CAMPAIGN_FILTER_NAME);
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _settings.set(ENROLLMENT_CAMPAIGN_FILTER_NAME, enrollmentCampaign);
    }

    // Getters & Setters

    @Override
    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource<StatisticEntrantRequestReport> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StatisticEntrantRequestReport> dataSource)
    {
        _dataSource = dataSource;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }
}
