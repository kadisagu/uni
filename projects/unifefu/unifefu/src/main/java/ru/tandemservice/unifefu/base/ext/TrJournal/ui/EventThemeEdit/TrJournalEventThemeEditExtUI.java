package ru.tandemservice.unifefu.base.ext.TrJournal.ui.EventThemeEdit;

import org.hibernate.Session;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.EventThemeEdit.TrJournalEventThemeEditUI;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAction;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: amakarova
 * Date: 12.12.13
 */

public class TrJournalEventThemeEditExtUI extends UIAddon
{

    private Date _date;
    private TrEduGroupEvent _groupEvent = new TrEduGroupEvent();
    private ScheduleEvent _schEvent = new ScheduleEvent();
    private Map<Integer, ScheduleBellEntry> entryMap = new HashMap<>();
    private List<UniplacesPlace> placeList;
    boolean isEventAction;
    boolean isDateVisible;

    public TrJournalEventThemeEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentActivate()
    {
        TrJournalEventThemeEditUI presenter = getPresenter();
        setGroupEvent(getTrEduGroupEvent(presenter.getEvent()));
        setDateVisible(getJournalGroup(presenter.getSupport().getSession(), presenter.getEvent().getJournalModule().getJournal()));
        if (getGroupEvent() != null && getGroupEvent().getScheduleEvent() != null)
        {
            setPlaceList(CommonBaseUtil.<UniplacesPlace>getPropertiesList(IUniBaseDao.instance.get().getList(ScheduleEventPlace.class, ScheduleEventPlace.scheduleEvent().s(), getGroupEvent().getScheduleEvent()), ScheduleEventPlace.place().s()));
            setDate(getGroupEvent().getScheduleEvent().getDurationBegin());
            getSchEvent().setDurationBegin(getGroupEvent().getScheduleEvent().getDurationBegin());
            getSchEvent().setDurationEnd(getGroupEvent().getScheduleEvent().getDurationEnd());
            setDateVisible(true);
        }
        else if (isDateVisible())
        {
            setDate(new Date());
        }
        setEventAction(presenter.getEvent() instanceof TrEventAction);
    }

    public TrJournalEvent getJournalEvent()
    {
        TrJournalEventThemeEditUI presenter = getPresenter();
        return presenter.getEvent();
    }

    @Override
    public void onComponentRefresh()
    {
        TrJournalEventThemeEditUI presenter = getPresenter();
        presenter.setDisabled(!EppState.STATE_FORMATIVE.equals(presenter.getHolder().refresh().getJournalModule().getJournal().getState().getCode()));
    }

    public TrEduGroupEvent getTrEduGroupEvent(TrJournalEvent event)
    {
        DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ge")
                .column("ge").top(1)
                .where(DQLExpressions.eq(property(TrEduGroupEvent.journalEvent().fromAlias("ge")), value(event)));

        return query.createStatement(getSession()).uniqueResult();
    }

    public Date getDate()
    {
        return _date;
    }

    public void setDate(Date date)
    {
        this._date = date;
    }

    public TrEduGroupEvent getGroupEvent()
    {
        return _groupEvent;
    }

    public void setGroupEvent(TrEduGroupEvent groupEvent)
    {
        this._groupEvent = groupEvent;
    }

    public ScheduleEvent getSchEvent()
    {
        return _schEvent;
    }

    public void setSchEvent(ScheduleEvent schEvent)
    {
        this._schEvent = schEvent;
    }

    public List<UniplacesPlace> getPlaceList()
    {
        return placeList;
    }

    public void setPlaceList(List<UniplacesPlace> placeList)
    {
        this.placeList = placeList;
    }

    public String checkDate()
    {
        final Date moduleBeginDate = getGroupEvent().getJournalEvent().getJournalModule().getModuleBeginDate();
        boolean startError = null != moduleBeginDate && CoreDateUtils.getDayFirstTimeMoment(moduleBeginDate).after(getSchEvent().getDurationBegin());
        final Date moduleEndDate = getGroupEvent().getJournalEvent().getJournalModule().getModuleEndDate();
        boolean endError = null != moduleEndDate && CoreDateUtils.getDayLastTimeMoment(moduleEndDate).before(getSchEvent().getDurationEnd());
        if (startError && endError)
            return "Вы ввели дату начала события до даты начала обучения по модулю, и дату окончания события после окончания обучения по модулю. Сохранить событие с этими датами?";
        else if (startError)
            return "Вы ввели дату начала события до даты начала обучения по модулю. Сохранить событие с такой датой начала?";
        else if (endError)
            return "Вы ввели дату окончания события после окончания обучения по модулю. Сохранить событие с такой датой окончания?";
        return null;
    }

    public boolean isVisible()
    {
        return false;
    }

    public boolean isEventAction()
    {
        return isEventAction;
    }

    public void setEventAction(boolean isEventAction)
    {
        this.isEventAction = isEventAction;
    }

    public boolean isDateVisible()
    {
        return isDateVisible;
    }

    public void setDateVisible(boolean dateVisible)
    {
        isDateVisible = dateVisible;
    }

    private boolean getJournalGroup(Session session, TrJournal journal)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(TrJournalGroup.class, "e")
                .column("e.id")
                .where(eq(property(TrJournalGroup.journal().fromAlias("e")), value(journal)));
        return dql.createCountStatement(new DQLExecutionContext(session)).<Long>uniqueResult() > 0L;
    }
}
