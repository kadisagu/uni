package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x2_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewPerson

		// создано свойство personGuid
		{
			// создать колонку
			tool.createColumn("mdbviewperson_t", new DBColumn("personguid_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность mdbViewStudent

		// создано свойство studentGuid
		{
			// создать колонку
			tool.createColumn("mdbviewstudent_t", new DBColumn("studentguid_p", DBType.createVarchar(255)));

		}

		// создано свойство formativeOrgUnitGuid
		{
			// создать колонку
			tool.createColumn("mdbviewstudent_t", new DBColumn("formativeorgunitguid_p", DBType.createVarchar(255)));

		}

		// создано свойство outOrgUnitGuid
		{
			// создать колонку
			tool.createColumn("mdbviewstudent_t", new DBColumn("outorgunitguid_p", DBType.createVarchar(255)));

		}

		// создано свойство educationFormGuid
		{
			// создать колонку
			tool.createColumn("mdbviewstudent_t", new DBColumn("educationformguid_p", DBType.createVarchar(255)));

		}

		// создано свойство educationDirectionGuid
		{
			// создать колонку
			tool.createColumn("mdbviewstudent_t", new DBColumn("educationdirectionguid_p", DBType.createVarchar(255)));

		}

		// создано свойство qualificationGuid
		{
			// создать колонку
			tool.createColumn("mdbviewstudent_t", new DBColumn("qualificationguid_p", DBType.createVarchar(255)));

		}

		// создано свойство developTechGuid
		{
			// создать колонку
			tool.createColumn("mdbviewstudent_t", new DBColumn("developtechguid_p", DBType.createVarchar(255)));

		}

		// создано свойство developConditionGuid
		{
			// создать колонку
			tool.createColumn("mdbviewstudent_t", new DBColumn("developconditionguid_p", DBType.createVarchar(255)));

		}

		// создано свойство compensationTypeGuid
		{
			// создать колонку
			tool.createColumn("mdbviewstudent_t", new DBColumn("compensationtypeguid_p", DBType.createVarchar(255)));

		}


    }
}