/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.SearchStep;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uniec.component.wizard.SearchStep.Model;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;

/**
 * @author Nikolay Fedorovskih
 * @since 17.06.2013
 */
public class DAO extends ru.tandemservice.uniec.component.wizard.SearchStep.DAO
{
    @Override
    protected void setOnlineEntrantListCondition(MQBuilder builder, String filter, Model model, String alias)
    {
        super.setOnlineEntrantListCondition(builder, filter, model, alias);

        // Показать только еще незарегистрированных онлайн-абитуриентоа
        builder.add(MQExpression.isNull(alias, OnlineEntrant.entrant()));
    }
}