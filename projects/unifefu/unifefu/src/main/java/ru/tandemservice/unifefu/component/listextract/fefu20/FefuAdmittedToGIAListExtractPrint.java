/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.Arrays;

/**
 * @author Andrey Andreev
 * @since 13.01.2016
 */
public class FefuAdmittedToGIAListExtractPrint implements IPrintFormCreator<FefuAdmittedToGIAListExtract>, IListParagraphPrintFormCreator<FefuAdmittedToGIAListExtract>, IStudentListParagraphPrintFormatter
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuAdmittedToGIAListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);

        Student student = extract.getEntity();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();

        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");
        CommonExtractPrint.modifyEducationStr(modifier, educationOrgUnit);
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(
                modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(student.getGroup()), "fefuShortFastExtendedOptionalText");

        int ind = Arrays.asList(ApplicationRuntime.getProperty("seasonTerm_N").split(";")).indexOf(extract.getSeason());
        String season = ind > -1 ? Arrays.asList(ApplicationRuntime.getProperty("seasonTerm_P").split(";")).get(ind) : "______";
        modifier.put("seasonTerm_P", season);

        modifier.put("having", student.getPerson().getIdentityCard().getSex().isMale() ? "имеющего" : "имеющую");
        modifier.put("year", String.valueOf(extract.getYear()));

        modifier.modify(document);

        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuAdmittedToGIAListExtract firstExtract)
    {
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuAdmittedToGIAListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuAdmittedToGIAListExtract firstExtract)
    {
        return null;
    }


    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        return "\\par " + extractNumber + ".  " + student.getPerson().getFullFio();
    }
}