/**
 * FefuDirectumServiceImplLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directumtest;

public class FefuDirectumServiceImplLocator extends org.apache.axis.client.Service implements ru.tandemservice.unifefu.ws.directumtest.FefuDirectumServiceImpl {

    public FefuDirectumServiceImplLocator() {
    }


    public FefuDirectumServiceImplLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FefuDirectumServiceImplLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for FefuDirectumServicePort
    private java.lang.String FefuDirectumServicePort_address = "http://localhost:8080/services/FefuDirectumService";

    public java.lang.String getFefuDirectumServicePortAddress() {
        return FefuDirectumServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String FefuDirectumServicePortWSDDServiceName = "FefuDirectumServicePort";

    public java.lang.String getFefuDirectumServicePortWSDDServiceName() {
        return FefuDirectumServicePortWSDDServiceName;
    }

    public void setFefuDirectumServicePortWSDDServiceName(java.lang.String name) {
        FefuDirectumServicePortWSDDServiceName = name;
    }

    public ru.tandemservice.unifefu.ws.directumtest.FefuDirectumService getFefuDirectumServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(FefuDirectumServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getFefuDirectumServicePort(endpoint);
    }

    public ru.tandemservice.unifefu.ws.directumtest.FefuDirectumService getFefuDirectumServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ru.tandemservice.unifefu.ws.directumtest.FefuDirectumServiceImplSoapBindingStub _stub = new ru.tandemservice.unifefu.ws.directumtest.FefuDirectumServiceImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getFefuDirectumServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setFefuDirectumServicePortEndpointAddress(java.lang.String address) {
        FefuDirectumServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ru.tandemservice.unifefu.ws.directumtest.FefuDirectumService.class.isAssignableFrom(serviceEndpointInterface)) {
                ru.tandemservice.unifefu.ws.directumtest.FefuDirectumServiceImplSoapBindingStub _stub = new ru.tandemservice.unifefu.ws.directumtest.FefuDirectumServiceImplSoapBindingStub(new java.net.URL(FefuDirectumServicePort_address), this);
                _stub.setPortName(getFefuDirectumServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("FefuDirectumServicePort".equals(inputPortName)) {
            return getFefuDirectumServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://directum.ws.unifefu.tandemservice.ru/", "FefuDirectumServiceImpl");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://directum.ws.unifefu.tandemservice.ru/", "FefuDirectumServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("FefuDirectumServicePort".equals(portName)) {
            setFefuDirectumServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
