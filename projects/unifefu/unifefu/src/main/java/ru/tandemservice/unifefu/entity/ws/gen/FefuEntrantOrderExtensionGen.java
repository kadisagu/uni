package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение абитуриентского приказа о зачислении для хранения данных Directum
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEntrantOrderExtensionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension";
    public static final String ENTITY_NAME = "fefuEntrantOrderExtension";
    public static final int VERSION_HASH = 319934820;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String P_ORDER_ID = "orderId";
    public static final String P_DIRECTUM_ORDER_ID = "directumOrderId";
    public static final String P_DIRECTUM_NUMBER = "directumNumber";
    public static final String P_DIRECTUM_DATE = "directumDate";
    public static final String P_DIRECTUM_SCAN_URL = "directumScanUrl";
    public static final String P_DIRECTUM_TASK_ID = "directumTaskId";

    private EnrollmentOrder _order;     // Приказ о зачислении абитуриентов
    private Long _orderId;     // Идентификатор приказа в ОБ
    private String _directumOrderId;     // Идентификатор документа из Directum
    private String _directumNumber;     // Номер приказа из Directum
    private Date _directumDate;     // Дата приказа из Directum
    private String _directumScanUrl;     // Скан-копия приказа в Directum
    private String _directumTaskId;     // Идентификатор задачи в Directum

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ о зачислении абитуриентов.
     */
    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ о зачислении абитуриентов.
     */
    public void setOrder(EnrollmentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Идентификатор приказа в ОБ.
     */
    public Long getOrderId()
    {
        return _orderId;
    }

    /**
     * @param orderId Идентификатор приказа в ОБ.
     */
    public void setOrderId(Long orderId)
    {
        dirty(_orderId, orderId);
        _orderId = orderId;
    }

    /**
     * @return Идентификатор документа из Directum.
     */
    @Length(max=255)
    public String getDirectumOrderId()
    {
        return _directumOrderId;
    }

    /**
     * @param directumOrderId Идентификатор документа из Directum.
     */
    public void setDirectumOrderId(String directumOrderId)
    {
        dirty(_directumOrderId, directumOrderId);
        _directumOrderId = directumOrderId;
    }

    /**
     * @return Номер приказа из Directum.
     */
    @Length(max=255)
    public String getDirectumNumber()
    {
        return _directumNumber;
    }

    /**
     * @param directumNumber Номер приказа из Directum.
     */
    public void setDirectumNumber(String directumNumber)
    {
        dirty(_directumNumber, directumNumber);
        _directumNumber = directumNumber;
    }

    /**
     * @return Дата приказа из Directum.
     */
    public Date getDirectumDate()
    {
        return _directumDate;
    }

    /**
     * @param directumDate Дата приказа из Directum.
     */
    public void setDirectumDate(Date directumDate)
    {
        dirty(_directumDate, directumDate);
        _directumDate = directumDate;
    }

    /**
     * @return Скан-копия приказа в Directum.
     */
    @Length(max=255)
    public String getDirectumScanUrl()
    {
        return _directumScanUrl;
    }

    /**
     * @param directumScanUrl Скан-копия приказа в Directum.
     */
    public void setDirectumScanUrl(String directumScanUrl)
    {
        dirty(_directumScanUrl, directumScanUrl);
        _directumScanUrl = directumScanUrl;
    }

    /**
     * @return Идентификатор задачи в Directum.
     */
    @Length(max=255)
    public String getDirectumTaskId()
    {
        return _directumTaskId;
    }

    /**
     * @param directumTaskId Идентификатор задачи в Directum.
     */
    public void setDirectumTaskId(String directumTaskId)
    {
        dirty(_directumTaskId, directumTaskId);
        _directumTaskId = directumTaskId;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEntrantOrderExtensionGen)
        {
            setOrder(((FefuEntrantOrderExtension)another).getOrder());
            setOrderId(((FefuEntrantOrderExtension)another).getOrderId());
            setDirectumOrderId(((FefuEntrantOrderExtension)another).getDirectumOrderId());
            setDirectumNumber(((FefuEntrantOrderExtension)another).getDirectumNumber());
            setDirectumDate(((FefuEntrantOrderExtension)another).getDirectumDate());
            setDirectumScanUrl(((FefuEntrantOrderExtension)another).getDirectumScanUrl());
            setDirectumTaskId(((FefuEntrantOrderExtension)another).getDirectumTaskId());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEntrantOrderExtensionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEntrantOrderExtension.class;
        }

        public T newInstance()
        {
            return (T) new FefuEntrantOrderExtension();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "orderId":
                    return obj.getOrderId();
                case "directumOrderId":
                    return obj.getDirectumOrderId();
                case "directumNumber":
                    return obj.getDirectumNumber();
                case "directumDate":
                    return obj.getDirectumDate();
                case "directumScanUrl":
                    return obj.getDirectumScanUrl();
                case "directumTaskId":
                    return obj.getDirectumTaskId();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((EnrollmentOrder) value);
                    return;
                case "orderId":
                    obj.setOrderId((Long) value);
                    return;
                case "directumOrderId":
                    obj.setDirectumOrderId((String) value);
                    return;
                case "directumNumber":
                    obj.setDirectumNumber((String) value);
                    return;
                case "directumDate":
                    obj.setDirectumDate((Date) value);
                    return;
                case "directumScanUrl":
                    obj.setDirectumScanUrl((String) value);
                    return;
                case "directumTaskId":
                    obj.setDirectumTaskId((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "orderId":
                        return true;
                case "directumOrderId":
                        return true;
                case "directumNumber":
                        return true;
                case "directumDate":
                        return true;
                case "directumScanUrl":
                        return true;
                case "directumTaskId":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "orderId":
                    return true;
                case "directumOrderId":
                    return true;
                case "directumNumber":
                    return true;
                case "directumDate":
                    return true;
                case "directumScanUrl":
                    return true;
                case "directumTaskId":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return EnrollmentOrder.class;
                case "orderId":
                    return Long.class;
                case "directumOrderId":
                    return String.class;
                case "directumNumber":
                    return String.class;
                case "directumDate":
                    return Date.class;
                case "directumScanUrl":
                    return String.class;
                case "directumTaskId":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEntrantOrderExtension> _dslPath = new Path<FefuEntrantOrderExtension>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEntrantOrderExtension");
    }
            

    /**
     * @return Приказ о зачислении абитуриентов.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getOrder()
     */
    public static EnrollmentOrder.Path<EnrollmentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Идентификатор приказа в ОБ.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getOrderId()
     */
    public static PropertyPath<Long> orderId()
    {
        return _dslPath.orderId();
    }

    /**
     * @return Идентификатор документа из Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getDirectumOrderId()
     */
    public static PropertyPath<String> directumOrderId()
    {
        return _dslPath.directumOrderId();
    }

    /**
     * @return Номер приказа из Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getDirectumNumber()
     */
    public static PropertyPath<String> directumNumber()
    {
        return _dslPath.directumNumber();
    }

    /**
     * @return Дата приказа из Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getDirectumDate()
     */
    public static PropertyPath<Date> directumDate()
    {
        return _dslPath.directumDate();
    }

    /**
     * @return Скан-копия приказа в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getDirectumScanUrl()
     */
    public static PropertyPath<String> directumScanUrl()
    {
        return _dslPath.directumScanUrl();
    }

    /**
     * @return Идентификатор задачи в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getDirectumTaskId()
     */
    public static PropertyPath<String> directumTaskId()
    {
        return _dslPath.directumTaskId();
    }

    public static class Path<E extends FefuEntrantOrderExtension> extends EntityPath<E>
    {
        private EnrollmentOrder.Path<EnrollmentOrder> _order;
        private PropertyPath<Long> _orderId;
        private PropertyPath<String> _directumOrderId;
        private PropertyPath<String> _directumNumber;
        private PropertyPath<Date> _directumDate;
        private PropertyPath<String> _directumScanUrl;
        private PropertyPath<String> _directumTaskId;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ о зачислении абитуриентов.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getOrder()
     */
        public EnrollmentOrder.Path<EnrollmentOrder> order()
        {
            if(_order == null )
                _order = new EnrollmentOrder.Path<EnrollmentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Идентификатор приказа в ОБ.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getOrderId()
     */
        public PropertyPath<Long> orderId()
        {
            if(_orderId == null )
                _orderId = new PropertyPath<Long>(FefuEntrantOrderExtensionGen.P_ORDER_ID, this);
            return _orderId;
        }

    /**
     * @return Идентификатор документа из Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getDirectumOrderId()
     */
        public PropertyPath<String> directumOrderId()
        {
            if(_directumOrderId == null )
                _directumOrderId = new PropertyPath<String>(FefuEntrantOrderExtensionGen.P_DIRECTUM_ORDER_ID, this);
            return _directumOrderId;
        }

    /**
     * @return Номер приказа из Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getDirectumNumber()
     */
        public PropertyPath<String> directumNumber()
        {
            if(_directumNumber == null )
                _directumNumber = new PropertyPath<String>(FefuEntrantOrderExtensionGen.P_DIRECTUM_NUMBER, this);
            return _directumNumber;
        }

    /**
     * @return Дата приказа из Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getDirectumDate()
     */
        public PropertyPath<Date> directumDate()
        {
            if(_directumDate == null )
                _directumDate = new PropertyPath<Date>(FefuEntrantOrderExtensionGen.P_DIRECTUM_DATE, this);
            return _directumDate;
        }

    /**
     * @return Скан-копия приказа в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getDirectumScanUrl()
     */
        public PropertyPath<String> directumScanUrl()
        {
            if(_directumScanUrl == null )
                _directumScanUrl = new PropertyPath<String>(FefuEntrantOrderExtensionGen.P_DIRECTUM_SCAN_URL, this);
            return _directumScanUrl;
        }

    /**
     * @return Идентификатор задачи в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension#getDirectumTaskId()
     */
        public PropertyPath<String> directumTaskId()
        {
            if(_directumTaskId == null )
                _directumTaskId = new PropertyPath<String>(FefuEntrantOrderExtensionGen.P_DIRECTUM_TASK_ID, this);
            return _directumTaskId;
        }

        public Class getEntityClass()
        {
            return FefuEntrantOrderExtension.class;
        }

        public String getEntityName()
        {
            return "fefuEntrantOrderExtension";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
