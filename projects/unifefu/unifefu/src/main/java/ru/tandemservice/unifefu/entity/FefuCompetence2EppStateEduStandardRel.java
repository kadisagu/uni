package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Связь компетенции с ГОС (ДВФУ)
 */
public class FefuCompetence2EppStateEduStandardRel extends FefuCompetence2EppStateEduStandardRelGen
{

    public String getCompetenceGroupCode(){
        return getFefuCompetence().getGroupCode(getGroupNumber());
    }
}