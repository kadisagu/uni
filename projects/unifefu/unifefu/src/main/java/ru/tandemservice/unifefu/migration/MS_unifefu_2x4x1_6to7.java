package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x1_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuScheduleSessionPrintForm

        // создана новая сущность
        {
            if (!tool.tableExists("fefuschedulesessionprintform_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefuschedulesessionprintform_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("createdate_p", DBType.TIMESTAMP),
                        new DBColumn("groups_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("term_p", DBType.LONG).setNullable(false),
                        new DBColumn("eduyear_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("headersoop_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("edulevels_p", DBType.createVarchar(255)),
                        new DBColumn("courses_p", DBType.createVarchar(255)),
                        new DBColumn("session_p", DBType.createVarchar(255)),
                        new DBColumn("formativeorgunit_id", DBType.LONG).setNullable(false),
                        new DBColumn("territorialorgunit_id", DBType.LONG).setNullable(false),
                        new DBColumn("developform_id", DBType.LONG),
                        new DBColumn("season_id", DBType.LONG).setNullable(false),
                        new DBColumn("chief_id", DBType.LONG),
                        new DBColumn("adminoop_id", DBType.LONG),
                        new DBColumn("chiefumu_id", DBType.LONG),
                        new DBColumn("content_id", DBType.LONG).setNullable(false),
                        new DBColumn("createou_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuScheduleSessionPrintForm");

        }


    }
}