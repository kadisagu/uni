/* $Id: FefuRegistryModuleLoadTabUI.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.ModuleLoadTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.ModuleLoadEdit.FefuRegistryModuleLoadEdit;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 05.02.14
 * Time: 12:44
 */

@Input({
        @Bind(key= UIPresenter.PUBLISHER_ID, binding="moduleId", required = true)
})
public class FefuRegistryModuleLoadTabUI extends UIPresenter
{
    private Long _moduleId;

    public Long getModuleId()
    {
        return _moduleId;
    }

    public void setModuleId(Long moduleId)
    {
        _moduleId = moduleId;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuRegistryModuleLoadTab.LOAD_DS.equals(dataSource.getName()))
        {
            dataSource.put("moduleId", _moduleId);
        }
    }

    public void onClickEditExtLoad()
    {
        _uiActivation.asRegionDialog(FefuRegistryModuleLoadEdit.class).parameter(UIPresenter.PUBLISHER_ID, _moduleId).activate();
    }
}
