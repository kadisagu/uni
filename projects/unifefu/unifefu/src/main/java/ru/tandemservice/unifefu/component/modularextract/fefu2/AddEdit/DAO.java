package ru.tandemservice.unifefu.component.modularextract.fefu2.AddEdit;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract;

import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/20/12
 * Time: 1:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuChangePassDiplomaWorkPeriodStuExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setYear(model.getExtract().getYear() == 0 ? null : model.getExtract().getYear());
        model.setSeasons(Arrays.asList((ApplicationRuntime.getProperty("seasons_A")).split(";")));
        model.setSeason(model.getSeasons().contains(model.getExtract().getSeason()) ? model.getExtract().getSeason() : null);
    }

    @Override
    protected FefuChangePassDiplomaWorkPeriodStuExtract createNewInstance()
    {
        return new FefuChangePassDiplomaWorkPeriodStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setYear(model.getYear());
        model.getExtract().setSeason(model.getSeason());
        super.update(model);
    }
}
