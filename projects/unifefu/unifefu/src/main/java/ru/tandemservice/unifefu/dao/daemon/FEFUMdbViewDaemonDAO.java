package ru.tandemservice.unifefu.dao.daemon;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.unifefu.entity.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Savva
 * @since 10.02.14
 */
public class FEFUMdbViewDaemonDAO extends UniBaseDao implements IFEFUMdbViewDaemonDAO
{
    public static final int DAEMON_ITERATION_TIME = 1440;
    final SpringBeanCache<IFEFUMdbViewDaemonDAO> instance = new SpringBeanCache<>(IFEFUMdbViewDaemonDAO.class.getName());

    private static boolean sync_locked = false;

    public static final SyncDaemon DAEMON = new SyncDaemon(IFEFUMdbViewDaemonDAO.class.getName(), DAEMON_ITERATION_TIME, IFEFUMdbViewDaemonDAO.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            if (sync_locked) return;

            // отключаем логирование в базу и прочие системные штуки
            final IEventServiceLock eventLock = CoreServices.eventService().lock();
            Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */

            final IFEFUMdbViewDaemonDAO dao = IFEFUMdbViewDaemonDAO.instance.get();
            sync_locked = true;

            try
            {
                dao.doExportMdbView(dao.exist(true));
            }
            catch (final Exception t)
            {
                sync_locked = false;
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            }
            finally
            {
                // включаем штуки и логирование обратно
                Debug.resumeLogging();
                eventLock.release();
            }
            sync_locked = false;
        }
    };

    private final Map<DevelopGrid, String> developGridMetaMap = SafeMap.get(key -> {
        List<String> courses = new DQLSelectBuilder()
                .fromEntity(DevelopGridTerm.class, "dgt")
                .column(property(DevelopGridTerm.course().code().fromAlias("dgt")))
                .where(eq(property(DevelopGridTerm.developGrid().fromAlias("dgt")), value(key)))
                .order(property(DevelopGridTerm.term().intValue().fromAlias("dgt")))
                .createStatement(getSession())
                .list();

        return StringUtils.join(courses, "");
    });

    @Transactional
    public void clearEntity()
    {
//        new DQLDeleteBuilder(MdbViewEduplanversionRow.class).createStatement(getSession()).execute();
//        new DQLDeleteBuilder(MdbViewEppEduplanversionBlock.class).createStatement(getSession()).execute();
//        new DQLDeleteBuilder(MdbViewEduplanversionRowTerm.class).createStatement(getSession()).execute();
//        new DQLDeleteBuilder(MdbViewEppDevelopGridMeta.class).createStatement(getSession()).execute();
        new DQLDeleteBuilder(MdbViewEppEduPlanVersion.class).createStatement(getSession()).execute(); //за счет каскадного удаления чистятся все остальные таблицы
    }

    public Collection<Long> epvIds2 = null;

    public Collection<Long> exist(Boolean b)
    {
        if (b)
        {
            epvIds2 = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanVersion.class, "e")
                    .joinEntity("e", DQLJoinType.left, MdbViewEppEduPlanVersion.class, "ex",
                                eq(
                                        property("e", EppEduPlanVersion.id()),
                                        property("ex", MdbViewEppEduPlanVersion.eppEduPlanVersion().id())
                                ))
                    .column(property("e.id"))
                    .where(isNull(property(MdbViewEppEduPlanVersion.eppEduPlanVersion().fromAlias("ex"))))
                    .createStatement(this.getSession()).setMaxResults(2000).list();
        }
        else
        {
            epvIds2 = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanVersion.class, "e")
                    .joinEntity("e", DQLJoinType.left, MdbViewEppEduPlanVersion.class, "ex",
                                eq(
                                        property("e", EppEduPlanVersion.id()),
                                        property("ex", MdbViewEppEduPlanVersion.eppEduPlanVersion().id())
                                ))
                    .column(property("e.id"))
                    .where(isNull(property(MdbViewEppEduPlanVersion.eppEduPlanVersion().fromAlias("ex"))))
                    .createStatement(this.getSession()).list();
        }
        return epvIds2;
    }

    @Override
    public void doExportMdbView(Collection<Long> epvIds)
    {
        if (epvIds.size() != 0)
        {
            final Map<String, EppControlActionType> actionTypeMap = EppEduPlanVersionDataDAO.getActionTypeMap();
            Collection<Long> epvBlockIds = CommonBaseEntityUtil.getIdList(this.getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion().id(), epvIds));
            final Map<Long, IEppEpvBlockWrapper> blockDataMap = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(epvBlockIds, true); //eduplanversion_row

            Map<EduProgramForm, DevelopForm> programFormMap = new HashMap<>();
            for (DevelopForm developForm : this.getList(DevelopForm.class))
                if (developForm.getProgramForm() != null)
                    programFormMap.put(developForm.getProgramForm(), developForm);

            Map<EduProgramTrait, DevelopTech> programTraitMap = new HashMap<>();
            for (DevelopTech developTech : this.getList(DevelopTech.class))
                if (developTech.getProgramTrait() != null || developTech.getCode().equals(DevelopTechCodes.GENERAL))
                    programTraitMap.put(developTech.getProgramTrait(), developTech);

            for (final EppEduPlanVersion ver : FEFUMdbViewDaemonDAO.this.getList(
                    EppEduPlanVersion.class,
                    "id", epvIds,
                    new String[]{
                            EppEduPlanVersion.eduPlan().number().s(),
                            EppEduPlanVersion.number().s()
                    }
            ))
            {
                MdbViewEppEduPlanVersion mdbViewEppEduplanversion = new MdbViewEppEduPlanVersion();
                mdbViewEppEduplanversion.setEppEduPlanVersion(ver);

                if (ver.getEduPlan() instanceof EppEduPlanProf)
                {
                    System.out.print(ver.getEduPlan().getTitle());
                    EducationLevelsHighSchool educationLevelsHighSchool = getEduPlanEduLevelsHighSchool(ver.getEduPlan());
                    System.out.println();

                    if (null != educationLevelsHighSchool)
                    {
                        mdbViewEppEduplanversion.setEducationLevels(educationLevelsHighSchool.getEducationLevel());
                        mdbViewEppEduplanversion.setEducationLevelsHighSchool(educationLevelsHighSchool);


            /*mdbViewEppEduplanversion.setEducationLevels(ver.getEduPlan().getParent().getEducationLevel());
            mdbViewEppEduplanversion.setEducationLevelsHighSchool(ver.getEduPlan().getEducationLevelHighSchool());*/

                        mdbViewEppEduplanversion.setStdNumber(null != ver.getEduPlan().getParent() ? ver.getEduPlan().getParent().getNumber() : null);

                        DevelopForm developForm = programFormMap.get(ver.getEduPlan().getProgramForm());
                        DevelopTech developTech = programTraitMap.get(ver.getEduPlan().getProgramTrait());

                        mdbViewEppEduplanversion.setPlanDevelopForm(developForm.getCode() + " " + developForm.getTitle());
                        mdbViewEppEduplanversion.setPlanDevelopCond(ver.getEduPlan().getDevelopCondition().getCode() + " " + ver.getEduPlan().getDevelopCondition().getTitle());
                        mdbViewEppEduplanversion.setPlanDevelopTech(developTech.getCode() + " " + developTech.getTitle());
                        mdbViewEppEduplanversion.setPlanDevelopGrid(ver.getEduPlan().getDevelopGrid().getCode() + " " + ver.getEduPlan().getDevelopGrid().getTitle());
                        mdbViewEppEduplanversion.setPlanNumber(ver.getEduPlan().getNumber());
                        mdbViewEppEduplanversion.setVersionNumber(ver.getNumber());
                        mdbViewEppEduplanversion.setVersionTitle(ver.getTitlePostfix());
                        save(mdbViewEppEduplanversion);


                        final List<EppEduPlanVersionBlock> blocks = new DQLSelectBuilder()
                                .fromEntity(EppEduPlanVersionBlock.class, "e").column(property("e"))
                                .where(eq(property(EppEduPlanVersionBlock.eduPlanVersion().id().fromAlias("e")), value(ver.getId())))
                                .order(property(EppEduPlanVersionBlock.eduPlanVersion().id().fromAlias("e")))
                                .createStatement(getSession()).list();

                        for (final EppEduPlanVersionBlock block : blocks)
                        {
                            MdbViewEppEduPlanVersionBlock mdbViewEppEduplanversionBlock = new MdbViewEppEduPlanVersionBlock();
                            mdbViewEppEduplanversionBlock.setMdbViewEppEduPlanVersion(mdbViewEppEduplanversion);
                            System.out.print(ver.getEduPlan().getTitle());
                            EducationLevelsHighSchool educationLevelsHighSchool1 = getBlockEduLevelsHighSchool(block);
                            System.out.println();

                            if (null != educationLevelsHighSchool1)
                            {
                                mdbViewEppEduplanversionBlock.setEducationLevelsHighSchool(educationLevelsHighSchool1);
                                getSession().save(mdbViewEppEduplanversionBlock);
                            }
                        }


                        String rowTypeName;
                        String rowTypeParams;
                        for (EppEduPlanVersionBlock block : blocks)
                        {
                            IEppEpvBlockWrapper wrapper = blockDataMap.get(block.getId());
                            for (final IEppEpvRowWrapper row : wrapper.getRowMap().values())
                            {
                                final Set<Integer> activeTermSet = row.getActiveTermSet();

                                // параметры записи
                                {
                                    rowTypeName = rowTypeParams = "";
                                    if (row.getRow() instanceof EppEpvGroupReRow)
                                    {
                                        rowTypeName = ROW_TYPE_CODE_GROUP;
                                    }
                                    else if (row.getRow() instanceof EppEpvGroupImRow)
                                    {
                                        rowTypeName = (((EppEpvGroupImRow) row.getRow()).getSize() > 0 ? ROW_TYPE_CODE_SELECTION : ROW_TYPE_CODE_SELECTION_EMPTY);
                                        rowTypeParams = String.valueOf(((EppEpvGroupImRow) row.getRow()).getSize());
                                    }
                                    else if (row.getRow() instanceof EppEpvStructureRow)
                                    {
                                        rowTypeName = ROW_TYPE_CODE_STRUCTURE;
                                        rowTypeParams = ((EppEpvStructureRow) row.getRow()).getValue().getCode() + " " + ((EppEpvStructureRow) row.getRow()).getValue().getTitle();

                                    }
                                    else if (row.getRow() instanceof EppEpvRegistryRow)
                                    {
                                        rowTypeName = ROW_TYPE_CODE_REGISTRY_ELEMENT;
                                        rowTypeParams = ((EppEpvRegistryRow) row.getRow()).getRegistryElementType().getCode() + " " + ((EppEpvRegistryRow) row.getRow()).getRegistryElementType().getTitle();
                                    }
                                }

                                MdbViewEduPlanVersionRow mdbViewEduplanversionRow = new MdbViewEduPlanVersionRow();
                                mdbViewEduplanversionRow.setRowVersion(mdbViewEppEduplanversion);
                                System.out.print(ver.getEduPlan().getTitle());
                                EducationLevelsHighSchool educationLevelsHighSchool1 = getBlockEduLevelsHighSchool(row.getRow().getOwner());
                                System.out.println();

                                if (null != educationLevelsHighSchool1)
                                {
                                    mdbViewEduplanversionRow.setRowEduhs(educationLevelsHighSchool1);
                                }
                                else
                                {
                                    mdbViewEduplanversionRow.setRowEduhs(educationLevelsHighSchool);
                                }
                                mdbViewEduplanversionRow.setRowOwner((row.getRow() instanceof EppEpvRegistryRow) ? ((EppEpvRegistryRow) row.getRow()).getRegistryElementOwner() : null);
                                mdbViewEduplanversionRow.setRowPath(row.getHierarhyIndex());
                                mdbViewEduplanversionRow.setRowDscType(rowTypeName);
                                mdbViewEduplanversionRow.setRowDscRel(rowTypeParams);
                                mdbViewEduplanversionRow.setRowIndex(row.getSelfIndexPart());
                                mdbViewEduplanversionRow.setRowTitle(StringUtils.trimToEmpty(row.getTitle()));
                                mdbViewEduplanversionRow.setRowTerms(StringUtils.join(activeTermSet, ", "));
                                getSession().save(mdbViewEduplanversionRow);


                                // добавляем распределение
                                if (row.getRow() instanceof EppEpvTermDistributedRow)
                                {
                                    activeTermSet.add(0);

                                    final Integer[] terms = activeTermSet.toArray(new Integer[activeTermSet.size()]);
                                    Arrays.sort(terms);

                                    for (final Integer term : terms)
                                    {
                                        // формы контроля (только для семестров)
                                        Set<String> actions = null;
                                        if (term > 0)
                                        {
                                            actions = new TreeSet<>();
                                            for (final Map.Entry<String, EppControlActionType> a : actionTypeMap.entrySet())
                                            {
                                                if ((a.getValue() instanceof EppFControlActionType) && (row.getActionSize(term, a.getKey()) > 0))
                                                {
                                                    actions.add(a.getValue().getAbbreviation());
                                                }
                                            }
                                        }

                                        MdbViewEduPlanVersionRowTerm mdbViewEduplanversionRowTerm = new MdbViewEduPlanVersionRowTerm();
                                        mdbViewEduplanversionRowTerm.setRowVersionId(mdbViewEppEduplanversion);
                                        mdbViewEduplanversionRowTerm.setRowPath(row.getHierarhyIndex());
                                        mdbViewEduplanversionRowTerm.setTerm(term);
                                        mdbViewEduplanversionRowTerm.setWeeks(UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppLoadType.FULL_CODE_WEEKS), false));
                                        mdbViewEduplanversionRowTerm.setSize(UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppLoadType.FULL_CODE_TOTAL_HOURS), false));
                                        mdbViewEduplanversionRowTerm.setLabor(UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppLoadType.FULL_CODE_LABOR), false));
                                        mdbViewEduplanversionRowTerm.setEaudit(UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppELoadType.FULL_CODE_AUDIT), false));
                                        mdbViewEduplanversionRowTerm.setAlect(UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_LECTURES), false));
                                        mdbViewEduplanversionRowTerm.setApract(UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_PRACTICE), false));
                                        mdbViewEduplanversionRowTerm.setAlab(UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_LABS), false));
                                        mdbViewEduplanversionRowTerm.setEself(UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppELoadType.FULL_CODE_SELFWORK), false));
                                        mdbViewEduplanversionRowTerm.setActions((null == actions ? "" : StringUtils.join(actions, ", ")));

                                        getSession().save(mdbViewEduplanversionRowTerm);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        System.out.println("skip: " + ver.getFullTitle());
                    }
                }
                DQLSelectBuilder ignoreEpvIdBuilder = new DQLSelectBuilder()
                        .fromEntity(FefuEduPlanVersionWeek.class, "w")
                        .column(property(FefuEduPlanVersionWeek.version().id().fromAlias("w")))
                        .predicate(DQLPredicateType.distinct);

                for (MdbViewEppEduPlanVersion version : new DQLSelectBuilder().fromEntity(MdbViewEppEduPlanVersion.class, "v").where(notIn(property(MdbViewEppEduPlanVersion.eppEduPlanVersion().id().fromAlias("v")), ignoreEpvIdBuilder.buildQuery())).createStatement(getSession()).<MdbViewEppEduPlanVersion>list())
                {
                    MdbViewEppDevelopGridMeta mdbViewEppDevelopGridMeta = new MdbViewEppDevelopGridMeta();
                    mdbViewEppDevelopGridMeta.setMdbViewEppEduPlanVersion(version);
                    mdbViewEppDevelopGridMeta.setDevelopGridMeta(developGridMetaMap.get(version.getEppEduPlanVersion().getEduPlan().getDevelopGrid()));
                    getSession().save(mdbViewEppDevelopGridMeta);

                }
            }
        }
    }

    private EducationLevelsHighSchool getBlockEduLevelsHighSchool(EppEduPlanVersionBlock block)
    {

        if (block instanceof EppEduPlanVersionRootBlock) return null;

        EppEduPlanVersionSpecializationBlock specializationBlock = (EppEduPlanVersionSpecializationBlock) block;
        EppEduPlanProf eduPlanProf = (EppEduPlanProf) specializationBlock.getEduPlanVersion().getEduPlan(); // ClassCasException если не проф. образование

        if (eduPlanProf.getProgramQualification() == null) return null;

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EducationLevelsHighSchool.class, "lhs")
                .column(property("lhs"))
                .joinPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().fromAlias("lhs"), "l")
                .where(eq(property("l", EducationLevels.eduProgramSpecialization()), value(specializationBlock.getProgramSpecialization())))
                .where(eq(property("lhs", EducationLevelsHighSchool.assignedQualification()), value(eduPlanProf.getProgramQualification())));

        if (eduPlanProf instanceof EppEduPlanHigherProf) {
            dql.where(eq(property("lhs", EducationLevelsHighSchool.programOrientation()), value(((EppEduPlanHigherProf) eduPlanProf).getProgramOrientation())));
        }

        List<EducationLevelsHighSchool> educationLevelsHighSchoolList = dql.createStatement(this.getSession()).list();

        if (educationLevelsHighSchoolList.size() > 1)
        {
            System.out.println();
            for (EducationLevelsHighSchool item : educationLevelsHighSchoolList)
                System.out.println("              " + item.getId() + " - " + item.getFullTitleExtended());
            return educationLevelsHighSchoolList.get(0);
        }

        if (educationLevelsHighSchoolList.size() != 1) System.out.print(" ----- !!!!!!");
        if (educationLevelsHighSchoolList.size() != 1) return null;

        return educationLevelsHighSchoolList.iterator().next();

    }

    private EducationLevelsHighSchool getEduPlanEduLevelsHighSchool(EppEduPlan eduPlan)
    {
        EppEduPlanProf eduPlanProf = (EppEduPlanProf) eduPlan; // ClassCasException если не проф. образование

        if (eduPlanProf.getProgramQualification() == null) return null;

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EducationLevelsHighSchool.class, "lhs")
                .column(property("lhs"))
                .joinPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().fromAlias("lhs"), "l")
                .where(eq(property("l", EducationLevels.eduProgramSubject()), value(eduPlanProf.getProgramSubject())))
                .where(eq(property("lhs", EducationLevelsHighSchool.assignedQualification()), value(eduPlanProf.getProgramQualification())));

        if (eduPlan instanceof EppEduPlanHigherProf) {
            dql.where(eq(property("lhs", EducationLevelsHighSchool.programOrientation()), value(((EppEduPlanHigherProf) eduPlan).getProgramOrientation())));
        }

        List<EducationLevelsHighSchool> educationLevelsHighSchoolList = dql.createStatement(this.getSession()).list();

        if (educationLevelsHighSchoolList.size() > 1)
        {
            System.out.println();
            for (EducationLevelsHighSchool item : educationLevelsHighSchoolList)
                System.out.println("              " + item.getId() + " - " + item.getFullTitleExtended());
            return educationLevelsHighSchoolList.get(0);
        }

        if (educationLevelsHighSchoolList.size() != 1) System.out.print(" ----- !!!!!!");
        if (educationLevelsHighSchoolList.size() != 1) return null;

        return educationLevelsHighSchoolList.iterator().next();
    }

    private static final String ROW_TYPE_CODE_GROUP = "g";
    private static final String ROW_TYPE_CODE_STRUCTURE = "s";
    private static final String ROW_TYPE_CODE_SELECTION = "v";
    private static final String ROW_TYPE_CODE_SELECTION_EMPTY = "x";
    private static final String ROW_TYPE_CODE_REGISTRY_ELEMENT = "r";
}