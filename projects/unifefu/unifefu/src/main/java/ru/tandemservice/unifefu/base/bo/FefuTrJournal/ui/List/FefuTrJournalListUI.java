package ru.tandemservice.unifefu.base.bo.FefuTrJournal.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unifefu.base.bo.FefuTrJournal.logic.FefuTrJournalDSHandler;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.Edit.TrJournalEdit;

import java.util.Map;

/**
 * @author amakarova
 */

public class FefuTrJournalListUI extends UIPresenter {

    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    public OrgUnitHolder getOrgUnitHolder() { return this.orgUnitHolder; }
//    public CommonPostfixPermissionModelBase getSecModel() { return this.getOrgUnitHolder().getSecModel(); }

    @Override
    public void onBeforeDataSourceFetch(final IUIDataSource dataSource)
    {
        if (FefuTrJournalList.TR_ORGUNIT_JOURNAL_DS.equals(dataSource.getName()))
        {
            Map<String, Object> settingMap = _uiSettings.getAsMap(
                    FefuTrJournalDSHandler.PARAM_ORG_UNIT,
                    FefuTrJournalDSHandler.PARAM_NUMBER,
                    FefuTrJournalDSHandler.PARAM_PERIOD_DATE_FROM,
                    FefuTrJournalDSHandler.PARAM_PERIOD_DATE_TO,
                    FefuTrJournalDSHandler.PARAM_REG_ELEMENT,
                    FefuTrJournalDSHandler.PARAM_RESPONSIBLE,
                    FefuTrJournalDSHandler.PARAM_TUTOR,
                    FefuTrJournalDSHandler.PARAM_STATE,
                    FefuTrJournalDSHandler.PARAM_YEAR_PART,
                    FefuTrJournalDSHandler.PARAM_FORMATIVE_ORG_UNIT,
                    FefuTrJournalDSHandler.PARAM_EDUCATIONLEVEL_HIGH_SCHOOL,
                    FefuTrJournalDSHandler.PARAM_FIND_IN_CHILD
            );
            dataSource.putAll(settingMap);
        }
        else if (FefuTrJournalList.EDUCATION_LEVEL_HIGH_SCHOOL_DS.equals(dataSource.getName()))
        {
            dataSource.put(FefuTrJournalDSHandler.PARAM_FORMATIVE_ORG_UNIT, _uiSettings.get(FefuTrJournalDSHandler.PARAM_FORMATIVE_ORG_UNIT));
        }
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(TrJournalEdit.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
        .activate();
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
        _uiSupport.setRefreshScheduled(true);
    }

    protected Long getYearPartId() {
        final EppYearPart part = this.getYearPart();
        return (null == part ? null : part.getId());
    }

    public boolean isOrgUnitFilterClear()
    {
        return _uiSettings.get(FefuTrJournalDSHandler.PARAM_ORG_UNIT) == null;
    }

    protected EppYearPart getYearPart() {
        return (EppYearPart)this._uiSettings.get(FefuTrJournalDSHandler.PARAM_YEAR_PART);
    }
}
