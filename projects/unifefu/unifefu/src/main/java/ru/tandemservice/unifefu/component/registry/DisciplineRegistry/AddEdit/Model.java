/* $Id$ */
package ru.tandemservice.unifefu.component.registry.DisciplineRegistry.AddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author Andrey Andreev
 * @since 24.02.2016
 */
@Input({@Bind(key = "fromOrgUnit", binding = "fromOrgUnit")})
public class Model extends ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit.Model
{
    private boolean fromOrgUnit;

    private OrgUnit _oldOwner;

    public OrgUnit getOldOwner()
    {
        return _oldOwner;
    }

    public void setOldOwner(OrgUnit oldOwner)
    {
        _oldOwner = oldOwner;
    }

    public void setFromOrgUnit(boolean fromOrgUnit)
    {
        this.fromOrgUnit = fromOrgUnit;
    }

    public boolean isOwnerNotEditable()
    {
        return isEditForm() && (fromOrgUnit || isReadOnly());
    }
}