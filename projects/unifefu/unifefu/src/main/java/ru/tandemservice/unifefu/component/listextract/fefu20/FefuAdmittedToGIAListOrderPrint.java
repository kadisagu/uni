/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20;

import com.google.common.collect.Iterables;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.component.listextract.fefu20.utils.FefuAdmittedToGIAListParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 13.01.2016
 */
public class FefuAdmittedToGIAListOrderPrint implements IPrintFormCreator<StudentListOrder>
{

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<FefuAdmittedToGIAListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        injectAllParagraphs(document, extracts);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }


    protected void injectAllParagraphs(final RtfDocument document, List<FefuAdmittedToGIAListExtract> extracts)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult paragraphSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (paragraphSearchResult.isFound())
        {
            RtfDocument template = new RtfReader().read(
                    MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_ADMIT_TO_STATE_ATTESTATION_LIST_EXTRACT), 1));

            // FIXME Ну вот как это читать? Стримы сделали не для того, чтобы их пихать куда ни попадя
            Collector<FefuAdmittedToGIAListExtract, ?, Map<EducationLevelsHighSchool, List<FefuAdmittedToGIAListExtract>>> subParagraphCollector =
                    Collectors.groupingBy(
                            extract -> extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool(),
                            Collectors.mapping(extract -> extract, Collectors.toList())
                    );

            Map<FefuAdmittedToGIAListParagraphWrapper, Map<EducationLevelsHighSchool, List<FefuAdmittedToGIAListExtract>>> paragraphsMap =
                    extracts.stream().collect(Collectors.groupingBy(FefuAdmittedToGIAListParagraphWrapper::new, TreeMap::new, subParagraphCollector));

            final List<IRtfElement> paragraphList = new ArrayList<>();
            int[] index = {1};
            boolean notOneParagraph = paragraphsMap.entrySet().size() != 1;
            paragraphsMap.entrySet().forEach(
                    entry -> paragraphList.add(injectParagraph(template.getClone(), index[0]++, notOneParagraph, entry.getKey(), entry.getValue()))
            );

            paragraphSearchResult.getElementList().remove(paragraphSearchResult.getIndex());
            paragraphSearchResult.getElementList().addAll(paragraphSearchResult.getIndex(), paragraphList);
        }
    }


    protected IRtfElement injectParagraph(RtfDocument document, int index, boolean printIndex, FefuAdmittedToGIAListParagraphWrapper paragraph,
                                          Map<EducationLevelsHighSchool, List<FefuAdmittedToGIAListExtract>> extracts)
    {
        RtfInjectModifier modifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph.getFirstExtract().getParagraph(), paragraph.getFirstExtract());

        StudentListOrder order = (StudentListOrder) paragraph.getParagraph().getOrder();
        CommonExtractPrint.injectReason(modifier, order.getReason(), order.getReasonComment());

        FefuAdmittedToGIAListExtract firstExtract = extracts.values().iterator().next().iterator().next();
        Student firstStudent = firstExtract.getEntity();
        Group group = firstStudent.getGroup();
        EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();

        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");
        CommonListExtractPrint.injectStudentsCategory(modifier, firstStudent.getStudentCategory());
        CommonListExtractPrint.injectCommonListExtractData(modifier, firstExtract);
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(
                modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(firstStudent.getGroup()), "fefuShortFastExtendedOptionalText");
        CommonExtractPrint.initFefuGroup(modifier, "groupInternal_G", firstStudent.getGroup(), educationOrgUnit.getDevelopForm(), " группы ");
        CommonExtractPrint.modifyEducationStr(
                modifier, educationOrgUnit.getEducationLevelHighSchool().getEducationLevel(), new String[]{"fefuEducationStrDirection"}, false);

        modifier.put("parNumberConditional", printIndex ? String.valueOf(index) + ". " : "");
        modifier.put("year", String.valueOf(firstExtract.getYear()));

        int ind = Arrays.asList(ApplicationRuntime.getProperty("seasonTerm_N").split(";")).indexOf(firstExtract.getSeason());
        String season = ind > -1 ? Arrays.asList(ApplicationRuntime.getProperty("seasonTerm_P").split(";")).get(ind) : "______";
        modifier.put("seasonTerm_P", season);

        final RtfSearchResult subParagraphSearchResult = UniRtfUtil.findRtfMark(document, CommonExtractPrint.PARAGRAPH_CONTENT);
        if (subParagraphSearchResult.isFound())
        {
            byte[] subParagraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_ADMIT_TO_STATE_ATTESTATION_LIST_EXTRACT), 3);
            RtfDocument subParagraph = new RtfReader().read(subParagraphTemplate);

            final List<IRtfElement> subParagraphList = new ArrayList<>();
            extracts.entrySet().forEach(
                    entry -> subParagraphList.add(injectSubParagraphs(subParagraph.getClone(), entry.getKey(), entry.getValue())));

            subParagraphSearchResult.getElementList().remove(subParagraphSearchResult.getIndex());
            subParagraphSearchResult.getElementList().addAll(subParagraphSearchResult.getIndex(), subParagraphList);
        }

        modifier.modify(document);

        IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
        rtfGroup.setElementList(document.getElementList());

        return rtfGroup;
    }

    protected IRtfElement injectSubParagraphs(RtfDocument document, EducationLevelsHighSchool educationLevelsHighSchool, List<FefuAdmittedToGIAListExtract> extracts)
    {
        RtfInjectModifier modifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, Iterables.getFirst(extracts, null));

        modifyFefuEducationStrProfile(modifier, educationLevelsHighSchool);

        final RtfString studentsList = new RtfString();
        final int[] index = {1};
        extracts.stream()
                .map(extract -> extract.getEntity().getFullFio())
                .sorted(CommonCollator.RUSSIAN_COLLATOR::compare)
                .forEach(fio -> studentsList.par().append(IRtfData.TAB).append(String.valueOf(index[0]++)).append(". ").append(fio));
        modifier.put("STUDENT_LIST", studentsList);

        modifier.modify(document);

        IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
        rtfGroup.setElementList(document.getElementList());

        return rtfGroup;
    }

    protected static void modifyFefuEducationStrProfile(RtfInjectModifier modifier, EducationLevelsHighSchool educationLevelsHighSchool)
    {
        StructureEducationLevels levels = educationLevelsHighSchool.getEducationLevel().getLevelType();
        RtfString fefuEducationStrProfile = new RtfString();
        if (levels.isSpecialization() || levels.isProfile())
        {
            fefuEducationStrProfile
                    .par()
                    .append(IRtfData.TAB)
                    .append(levels.isProfile() ? levels.isBachelor() ? "Профиль «" : "Магистерская программа «" : "Специализация «")
                    .append(educationLevelsHighSchool.getTitle() + "»");
        }
        modifier.put("fefuEducationStrProfile", fefuEducationStrProfile);
    }
}