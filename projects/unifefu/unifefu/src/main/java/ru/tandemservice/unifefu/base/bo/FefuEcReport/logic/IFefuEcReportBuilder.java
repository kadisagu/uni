/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;

/**
 * @author Nikolay Fedorovskih
 * @since 09.09.2013
 */
public interface IFefuEcReportBuilder
{
    DatabaseFile getContent(Session session);
}