/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.membership;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unifefu.ws.blackboard.BBContextHelper;
import ru.tandemservice.unifefu.ws.blackboard.BBSettingsVO;
import ru.tandemservice.unifefu.ws.blackboard.course.BBCourseUtils;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.CourseVO;
import ru.tandemservice.unifefu.ws.blackboard.membership.gen.CourseMembershipVO;
import ru.tandemservice.unifefu.ws.blackboard.membership.gen.GroupMembershipVO;
import ru.tandemservice.unifefu.ws.blackboard.membership.gen.MembershipFilter;
import ru.tandemservice.unifefu.ws.blackboard.membership.gen.ObjectFactory;
import ru.tandemservice.unifefu.ws.blackboard.user.gen.UserVO;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 17.03.2014
 */
public class BBMembershipUtils
{
    /**
     * Получение участника курса (препод или студент)
     *
     * @param coursePrimaryId идентификатор курса (примари)
     * @param userId          идентификатор юзера
     * @param roleId          тип роли (P - препод, S - студент)
     * @return участник
     */
    public static CourseMembershipVO getCourseMembership(final String coursePrimaryId, final String userId, final String roleId)
    {
        return BBContextHelper.getInstance().doMembershipRequest((courseMembershipWSPortType, of) -> {
            MembershipFilter membershipFilter = of.createMembershipFilter();
            membershipFilter.setFilterType(CourseMembershipWSConstants.BY_CRS_ID_AND_USER_ID);
            membershipFilter.getCourseIds().add(coursePrimaryId);
            membershipFilter.getUserIds().add(userId);
            List<CourseMembershipVO> list = courseMembershipWSPortType.getCourseMembership(null, membershipFilter);

            if (list != null)
            {
                for (CourseMembershipVO vo : list)
                {
                    if (vo.getRoleId().getValue().equals(roleId))
                        return vo;
                }
            }
            return null;
        });
    }

    /**
     * Получение списка учатников курсов по идентификатору пользователя BB.
     * Метод нужен, чтобы получать список курсов, на которых учится/преподает конкретный юзер.
     *
     * @param userId идентификатор юзера
     * @return список {@link CourseMembershipVO}
     */
    public static Collection<CourseMembershipVO> findCourseMembershipsByUserId(final String userId)
    {
        return BBContextHelper.getInstance().doMembershipRequest((courseMembershipWSPortType, of) -> {
            MembershipFilter membershipFilter = of.createMembershipFilter();
            membershipFilter.setFilterType(CourseMembershipWSConstants.BY_USER_ID);
            membershipFilter.getUserIds().add(userId);
            return courseMembershipWSPortType.getCourseMembership(null, membershipFilter);
        });
    }

    /**
     * Сохранение участников курса (зачисление на курс)
     *
     * @param coursePrimaryId  идентификатор курса (примари)
     * @param membershipVOList новые участники курса
     */
    public static List<String> saveCourseMemberships(final String coursePrimaryId, final List<CourseMembershipVO> membershipVOList)
    {
        List<String> result = BBContextHelper.getInstance().doMembershipRequest(
                (courseMembershipWSPortType, of) -> courseMembershipWSPortType.saveCourseMembership(coursePrimaryId, membershipVOList));

        if (result == null || result.size() != membershipVOList.size())
            throw new IllegalStateException();

        return result;
    }

    /**
     * Сохранение в BB списка студентов группы какого-то курса
     *
     * @param coursePrimaryId       идентификатор курса (примари)
     * @param groupMembershipVOList список участников группы
     * @return успешно ли создались новые участники группы
     */
    public static List<String> saveGroupMemberships(final String coursePrimaryId, final List<GroupMembershipVO> groupMembershipVOList)
    {
        List<String> result = BBContextHelper.getInstance().doMembershipRequest(
                (courseMembershipWSPortType, of) -> courseMembershipWSPortType.saveGroupMembership(coursePrimaryId, groupMembershipVOList));

        if (result == null || result.size() != groupMembershipVOList.size()) {
            throw new IllegalStateException("CourseMembershipWSPortType.saveGroupMembership() return invalid result. May be group membership already exists in BB.");
        }

        return result;
    }

    /**
     * Сохранение в ВВ участника группы
     *
     * @param coursePrimaryId    идентификатор курса (примари)
     * @param courseMembershipId идентификатор участника круса
     * @param groupId идентификатор группы
     * @return идентификатор созданного участника группы, полученный из ВВ
     */
    public static String saveGroupMembership(String coursePrimaryId, String courseMembershipId, String groupId)
    {
        GroupMembershipVO vo = initGroupMembershipVO(courseMembershipId, groupId);
        return saveGroupMemberships(coursePrimaryId, Collections.singletonList(vo)).get(0);
    }

    /**
     * Создание в Blackboard участника курса
     *
     * @param coursePrimaryId идентификатор курса (примари)
     * @param userId          идентификатор юзера
     * @param roleId          тип роли (P - препод, S - студент)
     * @return идентификатор созданного участника курса
     */
    public static String saveCourseMembership(String coursePrimaryId, String userId, String roleId)
    {
        CourseMembershipVO vo = initCourseMembershipVO(coursePrimaryId, userId, roleId);
        return saveCourseMemberships(coursePrimaryId, Collections.singletonList(vo)).get(0);
    }

    /**
     * Создание объекта CourseMembershipVO на базе курса, юзера и его роли. В Blackboard объект не отправляется!
     *
     * @param coursePrimaryId идентификатор курса (примари)
     * @param userId          идентификатор юзера
     * @param roleId          тип роли
     * @return сабж
     */
    public static CourseMembershipVO initCourseMembershipVO(String coursePrimaryId, String userId, String roleId)
    {
        ObjectFactory of = BBContextHelper.getInstance().getMembershipInitializer().getObjectFactory();
        CourseMembershipVO vo = of.createCourseMembershipVO();
        vo.setRoleId(of.createCourseMembershipVORoleId(roleId));
        vo.setAvailable(of.createCourseMembershipVOAvailable(true));
        vo.setCourseId(of.createCourseMembershipVOCourseId(coursePrimaryId));
        vo.setUserId(of.createCourseMembershipVOUserId(userId));
        return vo;
    }

    /**
     * Создание объекта GroupMembershipVO (участник группы) на базе участника курса и группы, в которую его надо зачислить
     *
     * @param courseMembershipId идентификатор участника круса (примари)
     * @param groupId            идентификатор группы
     * @return созданный объект
     */
    public static GroupMembershipVO initGroupMembershipVO(String courseMembershipId, String groupId)
    {
        ObjectFactory of = BBContextHelper.getInstance().getMembershipInitializer().getObjectFactory();
        GroupMembershipVO vo = of.createGroupMembershipVO();
        vo.setCourseMembershipId(of.createGroupMembershipVOCourseMembershipId(courseMembershipId));
        vo.setGroupId(of.createGroupMembershipVOGroupId(groupId));
        return vo;
    }

    /**
     * Удаление участников группы
     *
     * @param coursePrimaryId    идентификатор курса (примари)
     * @param groupMembershipIds список идентификаторов участников группы
     * @return успех или провал
     */
    public static boolean deleteGroupMemberships(final String coursePrimaryId, final List<String> groupMembershipIds)
    {
        List<String> result = BBContextHelper.getInstance().doMembershipRequest(
                (courseMembershipWSPortType, of) -> courseMembershipWSPortType.deleteGroupMembership(coursePrimaryId, groupMembershipIds));
        return result != null && result.size() == groupMembershipIds.size();
    }

    /**
     * Удаление участников курса
     *
     * @param coursePrimaryId     идентификатор курса (примари)
     * @param courseMembershipIds список идентификаторов участников группы
     * @return успех или провал
     */
    public static boolean deleteCourseMemberships(final String coursePrimaryId, final List<String> courseMembershipIds)
    {
        List<String> result = BBContextHelper.getInstance().doMembershipRequest(
                (courseMembershipWSPortType, of) -> courseMembershipWSPortType.deleteCourseMembership(coursePrimaryId, courseMembershipIds));
        return result != null && result.size() == courseMembershipIds.size();
    }

    /**
     * Проверка того, что преподаватель зачислен на курс "e-Learning: Изучение Blackboard" в качестве студента.
     * Если не зачислен, то сразу зачисляем.
     *
     * @param userVO юзер
     */
    public static void checkInstructor(UserVO userVO)
    {
        final BBSettingsVO settingsVO = BBSettingsVO.getCachedVO();
        if (StringUtils.isEmpty(settingsVO.getBbStudyCourseId()))
            throw new ApplicationException("В настройках не задан идентификатор курса «e-Learning: Изучение Blackboard».");

        final CourseVO courseVO = BBCourseUtils.findCourseByCourseId(settingsVO.getBbStudyCourseId());
        if (courseVO == null)
            throw new ApplicationException("Курс «e-Learning: Изучение Blackboard» с идентификатором " + settingsVO.getBbStudyCourseId() + " не найден в системе Blackboard.");

        // Проверяем, что преподаватель зачислен на курс изучения BB в качестве студента
        CourseMembershipVO membershipVO = BBMembershipUtils.getCourseMembership(courseVO.getId().getValue(), userVO.getId().getValue(), CourseMembershipWSConstants.STUDENT_ROLE);

        if (membershipVO == null)
        {
            // Если не зачислен или зачислен не как студент, то зачисляем в качестве студента
            saveCourseMembership(courseVO.getId().getValue(), userVO.getId().getValue(), CourseMembershipWSConstants.STUDENT_ROLE);
        }
    }
}