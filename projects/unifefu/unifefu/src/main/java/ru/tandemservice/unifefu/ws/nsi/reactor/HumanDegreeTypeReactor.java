/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceBranch;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.NsiDatagramUtil;
import ru.tandemservice.unifefu.ws.nsi.datagram.DegreeType;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanDegreeType;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanType;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.ScienceDiplomaWrapper;

/**
 * @author Dmitry Seleznev
 * @since 14.01.2015
 */
public class HumanDegreeTypeReactor extends SimpleCatalogEntityNewReactor<HumanDegreeType, PersonAcademicDegree>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return false;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<PersonAcademicDegree> getEntityClass()
    {
        return PersonAcademicDegree.class;
    }

    @Override
    public Class<HumanDegreeType> getNSIEntityClass()
    {
        return HumanDegreeType.class;
    }

    @Override
    public HumanDegreeType getCatalogElementRetrieveRequestObject(String guid)
    {
        // TODO: Имеет смысл переопределить метод получения полного списка ученых степеней физ лица из НСИ, поскольку их может быть очень много.
        HumanDegreeType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanDegreeType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public HumanDegreeType createEntity(CoreCollectionUtils.Pair<PersonAcademicDegree, FefuNsiIds> entityPair)
    {
        HumanDegreeType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanDegreeType();

        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setHumanDegreeDegreeDate(NsiDatagramUtil.formatDate(entityPair.getX().getDate()));
        nsiEntity.setHumanDegreeDiplomaNumber(NsiDatagramUtil.formatScienceDiploma(entityPair.getX()));
        nsiEntity.setHumanDegreeOrganization(entityPair.getX().getIssuancePlace());
        nsiEntity.setHumanDegreeScienceID(null != entityPair.getX().getScienceBranch() ? entityPair.getX().getScienceBranch().getTitle() : null);
        nsiEntity.setHumanDegreeDSID(entityPair.getX().getCouncil());

        CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonIdToEntityMap().get(String.valueOf(entityPair.getX().getPerson().getId()));
        if (null != personPair && null != personPair.getY())
        {
            HumanDegreeType.HumanID humanId = new HumanDegreeType.HumanID();
            HumanType human = new HumanType();
            human.setID(personPair.getY().getGuid());
            humanId.setHuman(human);
            nsiEntity.setHumanID(humanId);
        }

        if (null != entityPair.getX().getAcademicDegree())
        {
            CoreCollectionUtils.Pair<ScienceDegree, FefuNsiIds> degreePair = NsiObjectsHolder.getScienceDegreeMap().get(String.valueOf(entityPair.getX().getAcademicDegree().getId()));
            if (null != degreePair && null != degreePair.getY())
            {
                HumanDegreeType.DegreeID degreeId = new HumanDegreeType.DegreeID();
                DegreeType degree = new DegreeType();
                degree.setID(degreePair.getY().getGuid());
                degreeId.setDegree(degree);
                nsiEntity.setDegreeID(degreeId);
            }
        }

        return nsiEntity;
    }

    @Override
    public PersonAcademicDegree createEntity(HumanDegreeType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getHumanID()) return null;

        PersonAcademicDegree entity = new PersonAcademicDegree();
        entity.setDate(NsiDatagramUtil.parseDate(nsiEntity.getHumanDegreeDegreeDate()));
        entity.setIssuancePlace(nsiEntity.getHumanDegreeOrganization());
        entity.setCouncil(nsiEntity.getHumanDegreeDSID());

        if (null != StringUtils.trimToNull(nsiEntity.getHumanDegreeDiplomaNumber()))
        {
            ScienceDiplomaWrapper wrapper = NsiDatagramUtil.parseScienceDiplomaWrapper(nsiEntity.getHumanDegreeDiplomaNumber());
            if (null != wrapper)
            {
                entity.setSeria(wrapper.getSeria());
                entity.setNumber(wrapper.getNumber());
                entity.setIssuanceDate(wrapper.getDate());
            }
        }

        if (null != nsiEntity.getHumanID() && null != nsiEntity.getHumanID().getHuman())
        {
            CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonIdToEntityMap().get(nsiEntity.getHumanID().getHuman().getID());
            if (null != personPair && null != personPair.getX()) entity.setPerson(personPair.getX());
        }

        if (null != nsiEntity.getDegreeID() && null != nsiEntity.getDegreeID().getDegree())
        {
            CoreCollectionUtils.Pair<ScienceDegree, FefuNsiIds> degreePair = NsiObjectsHolder.getScienceDegreeMap().get(nsiEntity.getDegreeID().getDegree().getID());
            if (null != degreePair && null != degreePair.getX()) entity.setAcademicDegree(degreePair.getX());
        }

        if (null != StringUtils.trimToNull(nsiEntity.getHumanDegreeScienceID()))
        {
            CoreCollectionUtils.Pair<ScienceBranch, FefuNsiIds> scBranchPair = NsiObjectsHolder.getScienceBranchMap().get(nsiEntity.getHumanDegreeScienceID().trim().toUpperCase());
            if (null == scBranchPair)
            {
                ScienceBranch scienceBranch = new ScienceBranch();
                scienceBranch.setTitle(nsiEntity.getHumanDegreeScienceID().trim());
                scienceBranch.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(ScienceBranch.class));
                _otherEntityToSaveOrUpdateSet.add(new CoreCollectionUtils.Pair<IEntity, FefuNsiIds>(scienceBranch, null));

                scBranchPair = new CoreCollectionUtils.Pair(scienceBranch, null);
                NsiObjectsHolder.getScienceBranchMap().put(nsiEntity.getHumanDegreeScienceID().trim().toUpperCase(), scBranchPair);
            }

            entity.setScienceBranch(scBranchPair.getX());
        }

        return entity;
    }

    @Override
    public PersonAcademicDegree updatePossibleDuplicateFields(HumanDegreeType nsiEntity, CoreCollectionUtils.Pair<PersonAcademicDegree, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), PersonAcademicDegree.date().s()))
                entityPair.getX().setDate(NsiDatagramUtil.parseDate(nsiEntity.getHumanDegreeDegreeDate()));

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), PersonAcademicDegree.issuancePlace().s()))
                entityPair.getX().setIssuancePlace(nsiEntity.getHumanDegreeOrganization());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), PersonAcademicDegree.council().s()))
                entityPair.getX().setCouncil(nsiEntity.getHumanDegreeDSID());

            if (null != StringUtils.trimToNull(nsiEntity.getHumanDegreeDiplomaNumber()))
            {
                ScienceDiplomaWrapper wrapper = NsiDatagramUtil.parseScienceDiplomaWrapper(nsiEntity.getHumanDegreeDiplomaNumber());
                if (null != wrapper)
                {
                    if ((null == entityPair.getX().getSeria() ^ null == wrapper.getSeria()) || !entityPair.getX().getSeria().trim().toLowerCase().equals(wrapper.getSeria().trim().toLowerCase()))
                        entityPair.getX().setSeria(wrapper.getSeria());

                    if ((null == entityPair.getX().getNumber() ^ null == wrapper.getNumber()) || !entityPair.getX().getNumber().trim().toLowerCase().equals(wrapper.getNumber().trim().toLowerCase()))
                        entityPair.getX().setNumber(wrapper.getNumber());

                    if ((null == entityPair.getX().getIssuanceDate() ^ null == wrapper.getDate()) || entityPair.getX().getIssuanceDate().getTime() != wrapper.getDate().getTime())
                        entityPair.getX().setIssuanceDate(wrapper.getDate());
                }
            }

            if (null != nsiEntity.getHumanID() && null != nsiEntity.getHumanID().getHuman())
            {
                CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonIdToEntityMap().get(nsiEntity.getHumanID().getHuman().getID());
                if (null != personPair && null != personPair.getX()) entityPair.getX().setPerson(personPair.getX());
            }

            if (null != nsiEntity.getDegreeID() && null != nsiEntity.getDegreeID().getDegree())
            {
                CoreCollectionUtils.Pair<ScienceDegree, FefuNsiIds> degreePair = NsiObjectsHolder.getScienceDegreeMap().get(nsiEntity.getDegreeID().getDegree().getID());
                if (null != degreePair && null != degreePair.getX())
                    entityPair.getX().setAcademicDegree(degreePair.getX());
            }

            if (null != StringUtils.trimToNull(nsiEntity.getHumanDegreeScienceID()))
            {
                CoreCollectionUtils.Pair<ScienceBranch, FefuNsiIds> scBranchPair = NsiObjectsHolder.getScienceBranchMap().get(nsiEntity.getHumanDegreeScienceID().trim().toUpperCase());
                if (null == scBranchPair)
                {
                    ScienceBranch scienceBranch = new ScienceBranch();
                    scienceBranch.setTitle(nsiEntity.getHumanDegreeScienceID().trim());
                    scienceBranch.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(ScienceBranch.class));
                    _otherEntityToSaveOrUpdateSet.add(new CoreCollectionUtils.Pair<IEntity, FefuNsiIds>(scienceBranch, null));

                    scBranchPair = new CoreCollectionUtils.Pair(scienceBranch, null);
                    NsiObjectsHolder.getScienceBranchMap().put(nsiEntity.getHumanDegreeScienceID().trim().toUpperCase(), scBranchPair);
                }

                entityPair.getX().setScienceBranch(scBranchPair.getX());
            }
        }

        return entityPair.getX();
    }

    @Override
    public HumanDegreeType updateNsiEntityFields(HumanDegreeType nsiEntity, PersonAcademicDegree entity)
    {
        nsiEntity.setHumanDegreeDegreeDate(NsiDatagramUtil.formatDate(entity.getDate()));
        nsiEntity.setHumanDegreeDiplomaNumber(NsiDatagramUtil.formatScienceDiploma(entity));
        nsiEntity.setHumanDegreeOrganization(entity.getIssuancePlace());
        nsiEntity.setHumanDegreeScienceID(null != entity.getScienceBranch() ? entity.getScienceBranch().getTitle() : null);
        nsiEntity.setHumanDegreeDSID(entity.getCouncil());

        CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonIdToEntityMap().get(String.valueOf(entity.getPerson().getId()));
        if (null != personPair && null != personPair.getY())
        {
            HumanDegreeType.HumanID humanId = new HumanDegreeType.HumanID();
            HumanType human = new HumanType();
            human.setID(personPair.getY().getGuid());
            humanId.setHuman(human);
            nsiEntity.setHumanID(humanId);
        }

        if (null != entity.getAcademicDegree())
        {
            CoreCollectionUtils.Pair<ScienceDegree, FefuNsiIds> degreePair = NsiObjectsHolder.getScienceDegreeMap().get(String.valueOf(entity.getAcademicDegree().getId()));
            if (null != degreePair && null != degreePair.getY())
            {
                HumanDegreeType.DegreeID degreeId = new HumanDegreeType.DegreeID();
                DegreeType degree = new DegreeType();
                degree.setID(degreePair.getY().getGuid());
                degreeId.setDegree(degree);
                nsiEntity.setDegreeID(degreeId);
            }
        }

        return nsiEntity;
    }
}