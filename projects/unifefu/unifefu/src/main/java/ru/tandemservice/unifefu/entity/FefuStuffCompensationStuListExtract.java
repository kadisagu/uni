package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Проект приказа «О выплате компенсации взамен одежды, обуви, мягкого инвентаря и оборудования (при выпуске)»
 */
public class FefuStuffCompensationStuListExtract extends FefuStuffCompensationStuListExtractGen
{
	public Double getImmediateSumAsDouble()
	{
		return getImmediateSum() / 100D;
	}

	public Double getCompensationSumAsDouble()
	{
		return getCompensationSum() / 100D;
	}

	public void setImmediateSumFromDouble(Double source)
	{
		setImmediateSum(new Double(source * 100).longValue());
	}

	public void setCompensationSumFromDouble(Double source)
	{
		setCompensationSum(new Double(source * 100).longValue()) ;
	}

}