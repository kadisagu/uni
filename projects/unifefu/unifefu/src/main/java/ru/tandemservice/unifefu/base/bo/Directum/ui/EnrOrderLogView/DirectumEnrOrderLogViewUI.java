/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.EnrOrderLogView;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension;

/**
 * @author Dmitry Seleznev
 * @since 05.08.2013
 */
@State({
        @Bind(key = "orderId", binding = "orderId")
})
public class DirectumEnrOrderLogViewUI extends UIPresenter
{
    private Long _orderId;
    private EnrollmentOrder _order;
    private AbstractEntrantExtract _firstExtract;
    private FefuEntrantOrderExtension _ext;

    @Override
    public void onComponentRefresh()
    {
        _order = DataAccessServices.dao().getNotNull(EnrollmentOrder.class, _orderId);
        if (_order.getParagraphCount() > 0 && _order.getParagraphList().get(0).getExtractCount() > 0)
            _firstExtract = (AbstractEntrantExtract) _order.getParagraphList().get(0).getExtractList().get(0);
        _ext = DataAccessServices.dao().get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.order(), _order);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DirectumEnrOrderLogView.ORDER_LOG_DS.equals(dataSource.getName()))
        {
            dataSource.put(DirectumEnrOrderLogView.ORDER_ID_PARAM, _orderId);
        }
    }

    public ISecured getSecurityObject()
    {
        return _order;
    }

    public String getSecurityKey()
    {
        return "viewDirectumLogs_enrollmentOrder";
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrollmentOrder order)
    {
        _order = order;
    }

    public AbstractEntrantExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public void setFirstExtract(AbstractEntrantExtract firstExtract)
    {
        _firstExtract = firstExtract;
    }

    public FefuEntrantOrderExtension getExt()
    {
        return _ext;
    }

    public void setExt(FefuEntrantOrderExtension ext)
    {
        _ext = ext;
    }
}