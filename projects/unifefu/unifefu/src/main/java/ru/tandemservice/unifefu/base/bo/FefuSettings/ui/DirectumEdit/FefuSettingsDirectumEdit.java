/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.DirectumEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Dmitry Seleznev
 * @since 11.07.2013
 */
@Configuration
public class FefuSettingsDirectumEdit extends BusinessComponentManager
{
    public final static String SIGNER_DS = "signerDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SIGNER_DS, signerComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler signerComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).filtered(true);
    }
}