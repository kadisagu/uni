package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Dmitry Seleznev
 * @since 29.05.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.6.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("update FEFUNSILOGROW_T set MESSAGESTATUS_P=? where MESSAGECOMMENT_P like '%CallCC = 2%' and MESSAGESTATUS_P <> ?", 2, 2);
    }
}