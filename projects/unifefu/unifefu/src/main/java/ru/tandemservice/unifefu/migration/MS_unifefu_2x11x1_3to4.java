package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.unispp.migration.MS_unispp_2x11x1_5to6;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x11x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.11.1")
		};
    }

	@Override
	public ScriptDependency[] getBeforeDependencies()
	{
		return new ScriptDependency[]
				{
						MigrationUtils.createScriptDependency(MS_unispp_2x11x1_5to6.class)
				};
	}

    @Override
    public void run(DBTool tool) throws Exception
    {
		ISQLTranslator translator = tool.getDialect().getSQLTranslator();

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppSchedulePrintForm

		{
			// подразделение из которого мы зашли записывалось в groupOU.
			// после MS_unispp_2x11x1_5to6 там теперь formativeOU.
			// перенесём в createOU продукта (чтобы всё было как в продукте)
			SQLUpdateQuery upd1 = new SQLUpdateQuery("sppscheduleprintform_t", "spf");
			upd1.set("createou_id", "formativeorgunit_id");
			int n = tool.executeUpdate(translator.toSql(upd1));
			tool.info("schedule printform: product groupOU > product createOU (affected rows: " + n + ")");

			// formativeOU ушло в продукт
			SQLUpdateQuery upd2 = new SQLUpdateQuery("sppscheduleprintform_t", "spf");
			upd2.set("formativeorgunit_id", "spfe.formativeorgunit_id");
			upd2.getUpdatedTableFrom().innerJoin(SQLFrom.table("scheduleprintformfefuext_t", "spfe"), "spf.id=spfe.sppscheduleprintform_id");
			n = tool.executeUpdate(translator.toSql(upd2));
			tool.info("schedule printform: fefu formativeOU > product formativeOU (affected rows: " + n + ")");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность schedulePrintFormFefuExt

		// удалено свойство formativeOrgUnit
		{
			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "formativeorgunit_id");
		}
    }
}
