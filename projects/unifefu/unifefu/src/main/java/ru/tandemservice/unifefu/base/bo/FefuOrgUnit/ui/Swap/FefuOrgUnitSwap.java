/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.Swap;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author Dmitry Seleznev
 * @since 28.03.2014
 */
@Configuration
public class FefuOrgUnitSwap extends BusinessComponentManager
{
    public final static String ORG_UNIT_DS = "orgUnitDS";
    public final static String ORG_UNIT_ID = "orgUnitId";
    public final static String ORG_UNIT_TYPE_ID = "orgUnitTypeId";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(OrgUnit.titleWithParent().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orgUnitDSHandler()
    {
        EntityComboDataSourceHandler comboDataSourceHandler = new EntityComboDataSourceHandler(getName(), OrgUnit.class);
        comboDataSourceHandler.where(OrgUnit.archival(), Boolean.FALSE);
        comboDataSourceHandler.where(OrgUnit.orgUnitType().id(), ORG_UNIT_TYPE_ID);
        comboDataSourceHandler.filter(OrgUnit.title());
        comboDataSourceHandler.order(OrgUnit.title());
        return comboDataSourceHandler;
    }
}