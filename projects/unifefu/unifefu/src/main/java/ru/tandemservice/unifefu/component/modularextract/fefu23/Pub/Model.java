/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu23.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 23.01.2015
 */
public class Model extends ModularStudentExtractPubModel<FefuTransfStuDPOExtract>
{
    private String _printFormFileName;
    private FefuAdditionalProfessionalEducationProgram _dpoProgramOld;

    public FefuAdditionalProfessionalEducationProgram getDpoProgramOld()
    {
        return _dpoProgramOld;
    }

    public void setDpoProgramOld(FefuAdditionalProfessionalEducationProgram dpoProgramOld)
    {
        _dpoProgramOld = dpoProgramOld;
    }

    public String getPrintFormFileName()
    {
        return _printFormFileName;
    }

    public void setPrintFormFileName(String printFormFileName)
    {
        _printFormFileName = printFormFileName;
    }

    public boolean isShowPrintForm()
    {
        return getExtract().getState().getCode().equals("1") || (isIndividualOrder() && getExtract().getParagraph().getOrder().getState().getCode().equals("1"));

    }
}