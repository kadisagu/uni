/* $Id$ */
package ru.tandemservice.unifefu.base.ext.ExternalOrgUnit.ui.WizardStep1Add;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

/**
 * @author nvankov
 * @since 8/13/13
 */
public class ExternalOrgUnitWizardStep1AddExtUI extends UIAddon
{
    private boolean _editShortTitle = false;

    public ExternalOrgUnitWizardStep1AddExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public boolean getEditShortTitle()
    {
        return _editShortTitle;
    }

    public void setEditShortTitle(boolean editShortTitle)
    {
        _editShortTitle = editShortTitle;
    }
}
