/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu14.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubController;
import ru.tandemservice.unifefu.entity.SendPracticInnerStuExtract;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;

/**
 * @author nvankov
 * @since 6/20/13
 */
public class Controller extends ModularStudentExtractPubController<SendPracticInnerStuExtract, IDAO, Model>
{
}
