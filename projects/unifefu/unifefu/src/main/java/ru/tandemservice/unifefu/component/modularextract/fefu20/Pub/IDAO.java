/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu20.Pub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 20.01.2015
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuEnrollStuDPOExtract, Model>
{
    @Override
    void doSendToCoordination(Model model, IPersistentPersonable initiator);
}