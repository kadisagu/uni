/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e81.AddEdit;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.component.modularextract.e81.AddEdit.Model;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.unifefu.UniFefuDefines;

/**
 * @author nvankov
 * @since 3/27/13
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e81.AddEdit.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentCustomStateCI(DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), UniFefuDefines.ADMITTED_TO_DIPLOMA));
    }
}
