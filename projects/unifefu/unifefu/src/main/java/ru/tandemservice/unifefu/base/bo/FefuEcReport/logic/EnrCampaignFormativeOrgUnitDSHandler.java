/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 11.07.2013
 */
public class EnrCampaignFormativeOrgUnitDSHandler extends EntityComboDataSourceHandler
{
    public static final String ENROLLMENT_CAMPAIGN_PROP = "enrollmentCampaign";

    public EnrCampaignFormativeOrgUnitDSHandler(String ownerId)
    {
        super(ownerId, OrgUnit.class);
        this.order(OrgUnit.title());
        this.filter(OrgUnit.title());
    }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        super.applyWhereConditions(alias, dql, context);
        final EnrollmentCampaign enrCamp = context.get(ENROLLMENT_CAMPAIGN_PROP);

        dql.where(in(
                property(OrgUnit.id().fromAlias(alias)),
                new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "d")
                        .column(property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().id().fromAlias("d")))
                        .where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("d")), value(enrCamp)))
                        .buildQuery()
        ));
    }
}