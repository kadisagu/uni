/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.listextract.e32;

import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.movestudent.entity.AcadGrantAssignStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 01.10.2012
 */
public class AcadGrantAssignStuListExtractDao extends ru.tandemservice.movestudent.component.listextract.e32.AcadGrantAssignStuListExtractDao
{
    @Override
    public void doCommit(AcadGrantAssignStuListExtract extract, Map parameters)
    {
        super.doCommit(extract, parameters);

        StudentListOrder listOrder = (StudentListOrder) extract.getParagraph().getOrder();

        //выплата
        FefuStudentPayment payment = new FefuStudentPayment();
        payment.setExtract(extract);
        payment.setStartDate(extract.getBeginDate());
        payment.setStopDate(extract.getEndDate());
        payment.setPaymentSum(extract.getGrantSizeRuble());
        if(null != listOrder.getReason())
            payment.setReason(listOrder.getReason().getTitle());
        save(payment);

        if(extract.getGroupManagerBonusSizeRuble() != null)
        {
            FefuStudentPayment groupManagerPayment = new FefuStudentPayment();
            groupManagerPayment.setExtract(extract);
            groupManagerPayment.setStopOrPauseOrder(true);
            groupManagerPayment.setStartDate(extract.getGroupManagerBonusBeginDate());
            groupManagerPayment.setStopDate(extract.getGroupManagerBonusEndDate());
            groupManagerPayment.setPaymentSum(extract.getGroupManagerBonusSizeRuble());
            if(null != listOrder.getReason())
                groupManagerPayment.setReason(listOrder.getReason().getTitle());
            groupManagerPayment.setComment("Надбавка старосте");
            save(groupManagerPayment);
        }
    }

    @Override
    public void doRollback(AcadGrantAssignStuListExtract extract, Map parameters)
    {
        super.doRollback(extract, parameters);

        // Удаляем выплату
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuStudentPayment.class);
        deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(FefuStudentPayment.extract()), DQLExpressions.value(extract)));
        deleteBuilder.createStatement(getSession()).execute();
     }
}
