/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic;

import java.util.UUID;

/**
 * @author Dmitry Seleznev
 * @since 27.08.2013
 */
public class FefuPersonalLibraryDataWrapper
{
    private String _personGUID;
    private String _lastName;
    private String _firstName;
    private String _middleName;
    private String _sex;
    private String _course;
    private String _group;
    private String _formativeOrgUnit;
    private String _territorialOrgUnit;
    private String _productiveOrgUnit;
    private String _educationLevelHS;
    private String _educationLevel;
    private String _entranceYear;
    private String _developForm;
    private String _status;
    private String _bookNumber;

    public FefuPersonalLibraryDataWrapper(long personId)
    {
        _personGUID = getGUIDGenerated(personId);
    }

    private String getGUIDGenerated(long personId)
    {
        StringBuilder result = new StringBuilder("{");
        result.append(UUID.nameUUIDFromBytes(String.valueOf(personId).getBytes()));
        return result.append("}").toString();
    }

    public String getFullName()
    {
        return _lastName + " " + _firstName + (null != _middleName ? (" " + _middleName) : "");
    }

    public String getPersonGUID()
    {
        return _personGUID;
    }

    public void setPersonGUID(String personGUID)
    {
        _personGUID = personGUID;
    }

    public String getLastName()
    {
        return _lastName;
    }

    public void setLastName(String lastName)
    {
        _lastName = lastName;
    }

    public String getFirstName()
    {
        return _firstName;
    }

    public void setFirstName(String firstName)
    {
        _firstName = firstName;
    }

    public String getMiddleName()
    {
        return _middleName;
    }

    public void setMiddleName(String middleName)
    {
        _middleName = middleName;
    }

    public String getSex()
    {
        return _sex;
    }

    public void setSex(String sex)
    {
        _sex = sex;
    }

    public String getCourse()
    {
        return _course;
    }

    public void setCourse(String course)
    {
        _course = course;
    }

    public String getGroup()
    {
        return _group;
    }

    public void setGroup(String group)
    {
        _group = group;
    }

    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public String getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(String territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public String getProductiveOrgUnit()
    {
        return _productiveOrgUnit;
    }

    public void setProductiveOrgUnit(String productiveOrgUnit)
    {
        _productiveOrgUnit = productiveOrgUnit;
    }

    public String getEducationLevelHS()
    {
        return _educationLevelHS;
    }

    public void setEducationLevelHS(String educationLevelHS)
    {
        _educationLevelHS = educationLevelHS;
    }

    public String getEducationLevel()
    {
        return _educationLevel;
    }

    public void setEducationLevel(String educationLevel)
    {
        _educationLevel = educationLevel;
    }

    public String getEntranceYear()
    {
        return _entranceYear;
    }

    public void setEntranceYear(String entranceYear)
    {
        _entranceYear = entranceYear;
    }

    public String getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(String developForm)
    {
        _developForm = developForm;
    }

    public String getStatus()
    {
        return _status;
    }

    public void setStatus(String status)
    {
        _status = status;
    }

    public String getBookNumber()
    {
        return _bookNumber;
    }

    public void setBookNumber(String bookNumber)
    {
        _bookNumber = bookNumber;
    }
}
