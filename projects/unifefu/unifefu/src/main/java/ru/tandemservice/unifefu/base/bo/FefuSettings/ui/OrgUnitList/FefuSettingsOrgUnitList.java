/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrgUnitList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 29.07.2013
 */
@Configuration
public class FefuSettingsOrgUnitList extends BusinessComponentManager
{
    public static final String SETTING_ORGUNIT_DS = "settingsSearchListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SETTING_ORGUNIT_DS, getSettingsOrgUnitDS(), settingsOrgUnitDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getSettingsOrgUnitDS()
    {
        IMergeRowIdResolver merger = entity -> ((FefuOrgUnitDirectumExtension) entity).getOrgUnit().getId().toString();

        return columnListExtPointBuilder(SETTING_ORGUNIT_DS)
                .addColumn(textColumn("title", FefuOrgUnitDirectumExtension.orgUnit().fullTitle()).merger(merger))
                .addColumn(textColumn("orderType", FefuOrgUnitDirectumExtension.fefuDirectumOrderType().shortTitle()))
                .addColumn(textColumn("orgUnitCode", FefuOrgUnitDirectumExtension.orgUnitCode()))
                .addColumn(textColumn("orgUnitTitle", FefuOrgUnitDirectumExtension.orgUnitTitle()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("settingsSearchListDS.delete.alert"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> settingsOrgUnitDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(FefuOrgUnitDirectumExtension.class, "e").column("e")
                        .order(DQLExpressions.property(FefuOrgUnitDirectumExtension.orgUnit().fullTitle().fromAlias("e")))
                        .order(DQLExpressions.property(FefuOrgUnitDirectumExtension.fefuDirectumOrderType().shortTitle().fromAlias("e")));
                List<FefuOrgUnitDirectumExtension> recordList = builder.createStatement(context.getSession()).list();
                return ListOutputBuilder.get(input, recordList).build();
            }
        };
    }
}