package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x6_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuSchedulePrintForm

		// сущность была удалена
		{
			// удалить таблицу
			tool.dropTable("fefuscheduleprintform_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("fefuSchedulePrintForm");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность schedulePrintFormFefuExt

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("scheduleprintformfefuext_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("createdate_p", DBType.TIMESTAMP), 
				new DBColumn("groups_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("eduyear_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("grouporgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("developform_id", DBType.LONG), 
				new DBColumn("course_id", DBType.LONG).setNullable(false), 
				new DBColumn("season_id", DBType.LONG).setNullable(false), 
				new DBColumn("bells_id", DBType.LONG).setNullable(false), 
				new DBColumn("chief_id", DBType.LONG), 
				new DBColumn("content_id", DBType.LONG).setNullable(false), 
				new DBColumn("createou_id", DBType.LONG).setNullable(false), 
				new DBColumn("edustartdate_p", DBType.DATE), 
				new DBColumn("adminphonenum_p", DBType.createVarchar(255)), 
				new DBColumn("term_p", DBType.LONG).setNullable(false), 
				new DBColumn("formativeorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("territorialorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("edulevel_id", DBType.LONG), 
				new DBColumn("admin_id", DBType.LONG), 
				new DBColumn("chiefumu_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("schedulePrintFormFefuExt");

		}


    }
}