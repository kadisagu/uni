package ru.tandemservice.unifefu.entity;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Руководство пользователя (инструкция ДВФУ)
 */
public class FefuUserManual extends FefuUserManualGen implements ITemplateDocument
{
    @Override
        public byte[] getContent()
        {
            return CommonBaseUtil.getTemplateContent(this);
        }
}