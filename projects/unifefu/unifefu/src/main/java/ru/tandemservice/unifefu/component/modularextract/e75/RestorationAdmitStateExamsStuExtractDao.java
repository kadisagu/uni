/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e75;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.RestorationAdmitStateExamsStuExtract;
import ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.unifefu.UniFefuDefines;

import java.util.Map;

/**
 * @author nvankov
 * @since 3/28/13
 */
public class RestorationAdmitStateExamsStuExtractDao extends ru.tandemservice.movestudent.component.modularextract.e75.RestorationAdmitStateExamsStuExtractDao
{
    @Override
    public void doCommit(RestorationAdmitStateExamsStuExtract extract, Map parameters)
    {
        super.doCommit(extract, parameters);
        //создать статус
        StudentCustomStateCI customStateCI = DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), UniFefuDefines.ADMITTED_TO_STATE_EXAMES);
        StudentCustomState studentCustomState = new StudentCustomState();
        studentCustomState.setCustomState(customStateCI);
        studentCustomState.setStudent(extract.getEntity());
        studentCustomState.setBeginDate(extract.getEntranceDate());
        UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(studentCustomState);
        StudentCustomStateToExtractRelation extractRelation = new StudentCustomStateToExtractRelation();
        extractRelation.setStudentCustomState(studentCustomState);
        extractRelation.setExtract(extract);
        save(extractRelation);
    }

    @Override
    public void doRollback(RestorationAdmitStateExamsStuExtract extract, Map parameters)
    {
        super.doRollback(extract, parameters);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateToExtractRelation.class, "r").
                column(DQLExpressions.property("r", StudentCustomStateToExtractRelation.studentCustomState().id())).
                where(DQLExpressions.eq(DQLExpressions.property("r", StudentCustomStateToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(StudentCustomState.class).
                where(DQLExpressions.in(DQLExpressions.property(StudentCustomState.id()), builder.buildQuery()));
        deleteBuilder.createStatement(getSession()).execute();
    }
}
