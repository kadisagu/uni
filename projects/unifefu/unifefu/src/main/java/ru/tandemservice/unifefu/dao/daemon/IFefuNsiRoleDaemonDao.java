/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

/**
 * @author Dmitry Seleznev
 * @since 11.02.2015
 */
public interface IFefuNsiRoleDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IFefuNsiRoleDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IFefuNsiRoleDaemonDao> instance = new SpringBeanCache<>(IFefuNsiRoleDaemonDao.class.getName());

    /**
     * Производит синхронизацию ролей с НСИ.
     * Берет порцию из 100 ролей и генерирует по ним связанные объекты ролей НСИ. Далее данные отсылаются в НСИ.
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doSyncRolesWithNSI();
}