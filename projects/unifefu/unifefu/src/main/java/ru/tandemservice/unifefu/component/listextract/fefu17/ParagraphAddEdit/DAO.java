/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu17.ParagraphAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.CustomStateUtil;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.FefuExtractPrintFormManager;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOListExtract;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 27.01.2015
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuTransfStuDPOListExtract, Model> implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setCourseListNew(DevelopGridDAO.getCourseList());
        model.setEducationOrgUnitModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                DQLSelectBuilder builder = createBuilder("", primaryKey);
                return builder.createStatement(getSession()).uniqueResult();
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = createBuilder(filter, null);
                return new ListResult<>(builder.createStatement(getSession()).list());
            }

            private DQLSelectBuilder createBuilder(String filter, Object primaryKey)
            {

                OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou")
                        .column(property("eou"))
                        .where(or(eq(property("eou", EducationOrgUnit.formativeOrgUnit()), value(orgUnit)), eq(property("eou", EducationOrgUnit.territorialOrgUnit()), value(orgUnit))))
                        .where(eq(property("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().additional()), value(true)));

                if (primaryKey != null)
                {
                    if (primaryKey instanceof Long)
                        builder.where(eq(property("eou"), value((Long) primaryKey)));
                    if (primaryKey instanceof Collection)
                        builder.where(in(property("eou", EducationOrgUnit.id()), (Collection) primaryKey));
                }
                return builder;
            }
        });

        model.setDpoProgramModel(new BaseSingleSelectModel()
        {

            @Override
            public ListResult<FefuAdditionalProfessionalEducationProgram> findValues(String filter)
            {
                DQLSelectBuilder builder = createSelectBuilder(filter, null);

                List<FefuAdditionalProfessionalEducationProgram> list = builder.createStatement(getSession()).list();
                return new ListResult<>(list, list.size());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return get(FefuAdditionalProfessionalEducationProgram.class, (Long)primaryKey);
            }

            private DQLSelectBuilder createSelectBuilder(String filter, Object o)
            {
                OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(FefuAdditionalProfessionalEducationProgram.class, "ape")
                        .predicate(DQLPredicateType.distinct)
                        .where(or(eq(property("ape", FefuAdditionalProfessionalEducationProgram.formativeOrgUnit()), value(orgUnit)),
                                  eq(property("ape", FefuAdditionalProfessionalEducationProgram.territorialOrgUnit()), value(orgUnit))))
                        .column(property("ape"));


                if (o != null)
                {
                    if (o instanceof Long)
                        builder.where(eq(property("ape", FefuAdditionalProfessionalEducationProgram.id()), commonValue(o, PropertyType.LONG)));
                    else if (o instanceof Collection)
                        builder.where(in(property("ape", FefuAdditionalProfessionalEducationProgram.id()), (Collection) o));
                }

                builder.where(eq(property("ape", FefuAdditionalProfessionalEducationProgram.educationOrgUnit()), value(model.getEducationOrgUnitNew())));


                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("ape", FefuAdditionalProfessionalEducationProgram.title()), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property("ape", FefuAdditionalProfessionalEducationProgram.title()), OrderDirection.asc);

                return builder;
            }


        });
        model.setDpoProgramOldModel(new BaseSingleSelectModel()
        {

            @Override
            public ListResult<FefuAdditionalProfessionalEducationProgram> findValues(String filter)
            {
                DQLSelectBuilder builder = createSelectBuilder(filter, null);

                List<FefuAdditionalProfessionalEducationProgram> list = builder.createStatement(getSession()).list();
                return new ListResult<>(list, list.size());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return get(FefuAdditionalProfessionalEducationProgram.class, (Long)primaryKey);
            }

            protected DQLSelectBuilder createSelectBuilder(String filter, Object o)
            {
                OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(FefuAdditionalProfessionalEducationProgram.class, "ape")
                        .predicate(DQLPredicateType.distinct)
                        .where(or(eq(property("ape", FefuAdditionalProfessionalEducationProgram.formativeOrgUnit()), value(orgUnit)),
                                  eq(property("ape", FefuAdditionalProfessionalEducationProgram.territorialOrgUnit()), value(orgUnit))))
                        .column(property("ape"));


                if (o != null)
                {
                    if (o instanceof Long)
                        builder.where(eq(property("ape", FefuAdditionalProfessionalEducationProgram.id()), commonValue(o, PropertyType.LONG)));
                    else if (o instanceof Collection)
                        builder.where(in(property("ape", FefuAdditionalProfessionalEducationProgram.id()), (Collection) o));
                }

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("ape", FefuAdditionalProfessionalEducationProgram.title()), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property("ape", FefuAdditionalProfessionalEducationProgram.title()), OrderDirection.asc);

                return builder;
            }


        });

        final FefuTransfStuDPOListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            model.setCourseNew(firstExtract.getEntity().getCourse());
            model.setTransferDate(firstExtract.getTransferDate());
            model.setCourseOld(firstExtract.getCourseOld());
            model.setDpoProgramNew(firstExtract.getDpoProgramNew());
            model.setDpoProgramOld(firstExtract.getDpoProgramOld());
        }
        if (model.getDpoProgramOld() != null)
            model.setEducationOrgUnitNew(model.getDpoProgramOld().getEducationOrgUnit());

        model.setCanAddTemplate(!model.isEditForm() && model.isParagraphOnlyOneInTheOrder());

    }
    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        OrgUnit orgUnit = model.getParagraph().getOrder().getOrgUnit();
        builder.add(MQExpression.or(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().formativeOrgUnit(), orgUnit), MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().territorialOrgUnit(), orgUnit)));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().additional(), true));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.studentCategory().code(), StudentCategoryCodes.STUDENT_CATEGORY_DPP));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.course(), model.getCourseOld()));

        List<Long> studIds = new DQLSelectBuilder().fromEntity(FefuAdditionalProfessionalEducationProgramForStudent.class, "apes")
                .column(property("apes", FefuAdditionalProfessionalEducationProgramForStudent.student().id()))
                .where(eq(property("apes",FefuAdditionalProfessionalEducationProgramForStudent.program()), value(model.getDpoProgramOld()))).createStatement(getSession()).list();
        builder.add(MQExpression.in(STUDENT_ALIAS, Student.id(), studIds));

        if (model.getStudentCustomStateCIs() != null && !model.getStudentCustomStateCIs().isEmpty())
        {
            CustomStateUtil.addCustomStatesFilter(builder, STUDENT_ALIAS, model.getStudentCustomStateCIs());
        }

    }

    @Override
    protected FefuTransfStuDPOListExtract createNewInstance(Model model)
    {
        return new FefuTransfStuDPOListExtract();
    }

    @Override
    protected void fillExtract(FefuTransfStuDPOListExtract extract, Student student, Model model)
    {
        extract.setDpoProgramNew(model.getDpoProgramNew());
        extract.setTransferDate(model.getTransferDate());
        extract.setDpoProgramOld(model.getDpoProgramOld());
        extract.setCourseOld(model.getCourseOld());
        extract.setCourseNew(model.getCourseNew());
        extract.setIndividualPlan(model.getIndividualPlan());

    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        AbstractStudentOrder order = model.getParagraph().getOrder();
        FefuExtractPrintFormManager.instance().dao().saveOrUpdatePrintForm(order, model.getUploadFileOrder());
    }

    public void changeProgramOld(Model model)
    {
        model.setEducationOrgUnitNew(model.getDpoProgramOld() != null? model.getDpoProgramOld().getEducationOrgUnit():null);
    }

    @Override
    protected void appendStudentsWithGroup(MQBuilder builder)
    {
        /* Нужны студенты и без группы*/
    }

}