package ru.tandemservice.unifefu.base.vo;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Date;
import java.util.List;

public class FefuEntrantOriginalsVerificationReportVO
{
    private EnrollmentCampaign _enrollmentCampaign; // Приемная кампания - параметр обязательный, по умолчанию последняя добавленная (стандартно как везде в абитуриенте)public EnrollmentCampaign getEnrollmentCampaign()
    private Date _from; // Заявления с - параметр обязательный, по умолчанию ставится 01.01.текущий_годpublic Date getFrom()
    private Date _to; // Заявления по - параметр обязательный, по умолчанию ставится текущая дата, дата должна быть не меньше чем в "Заявления с"public Date getTo()
    private CompensationType _compensationType; // • Вид возмещения затрат - параметр обязательный, селект, элементы справочника (бюджет, по договору)public CompensationType getCompensationType()
    private List<StudentCategory> _studentCategoryList; // • Категория поступающего - параметр не обязательный, со множественным выбором, элементы справочника (студент, слушатель, второе высшее)public List<StudentCategory> getStudentCategoryList()
    private boolean _allEnrollmentDirections; // По всем направлениям/специальностям - флаг, необязат., по умолчанию включен (если включен, то фильтры ниже не показываются вообще (сбрасываются), кроме фильтров "Вид возмещения затрат" и "Категория поступающего"; если выключен, то показываются все, в том числе для выбора конкретного направления приема)public boolean isAllEnrollmentDirections()
    private OrgUnit _formativeOrgUnit; //  • Формирующее подразделение - с поиском и подстановкой (обязат. фильтр, если доступен), только такие формирующие, для которых есть направления приема в выбранной приемной кампании;public OrgUnit getFormattiveOrgUnit()
    private OrgUnit _territorialOrgUnit; // • Территориальное подразделение - с поиском и подстановкой (необязат. фильтр), только такие территориальные, для которых есть направления приема в выбранной приемной кампании для выбранного выше форм-го подр.public OrgUnit getTerritorialOrgUnit()
    private EducationLevelsHighSchool _educationLevelsHighSchool; // • Направление подготовки (специальность) - название уровня вуза (200100.65 Педиатрия), только такие уровни вуза, для которых есть в выбранной приемной кампании направления приема для выбранных выше форм-го и терр-го подр.public EnrollmentDirection getEnrollmentDirection()
    private DevelopForm _developForm; // • Форма освоения - селект (обязат. фильтр, если доступен), только такие формы освоения, для которых есть в выбранной приемной кампании направления приема для выбранных выше форм-го и терр-го подр., направ-я вузаpublic DevelopForm getDevelopForm()
    private DevelopCondition _developCondition; // • Условие освоения - селект (обязат. фильтр, если доступен), только такие .... и так понятноpublic DevelopCondition getDevelopCondition()
    private DevelopTech _developTech; // • Технология освоения - селект (обязат. фильтр, если доступен), только такие .... и так понятноpublic DevelopTech getDevelopTech()
    private DevelopPeriod _developPeriod; // • Срок освоенияpublic DevelopPeriod getDevelopPeriod()
    private List<Qualifications> _qualifications; // Квалификация
    private EnrollmentDirection _enrollmentDirection;
    private boolean _orderByOriginals; // Выделить абитуриентов с оригиналами документов
    private boolean _onlyWithOriginals; // Не включать абитуриентов без оригиналов документов
    private boolean _includeForeignPerson; // Учитывать иностранных граждан
    private boolean _includeEntrantWithBenefit; // Выводить абитуриентов, имеющих льготы
    private boolean _includeEntrantTargetAdmission; // Выводить абитуриентов, поступающих по целевому приему

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getFrom()
    {
        return _from;
    }

    public void setFrom(Date from)
    {
        _from = from;
    }

    public Date getTo()
    {
        return _to;
    }

    public void setTo(Date to)
    {
        _to = to;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public boolean isAllEnrollmentDirections()
    {
        return _allEnrollmentDirections;
    }

    public void setAllEnrollmentDirections(boolean allEnrollmentDirections)
    {
        _allEnrollmentDirections = allEnrollmentDirections;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        _developCondition = developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        _developTech = developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        _developPeriod = developPeriod;
    }

    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        _enrollmentDirection = enrollmentDirection;
    }

    public List<Qualifications> getQualifications()
    {
        return _qualifications;
    }

    public void setQualifications(List<Qualifications> qualifications)
    {
        _qualifications = qualifications;
    }

    public boolean isOrderByOriginals()
    {
        return _orderByOriginals;
    }

    public void setOrderByOriginals(boolean orderByOriginals)
    {
        _orderByOriginals = orderByOriginals;
    }

    public boolean isOnlyWithOriginals()
    {
        return _onlyWithOriginals;
    }

    public void setOnlyWithOriginals(boolean onlyWithOriginals)
    {
        _onlyWithOriginals = onlyWithOriginals;
    }

    public boolean isIncludeForeignPerson()
    {
        return _includeForeignPerson;
    }

    public void setIncludeForeignPerson(boolean includeForeignPerson)
    {
        _includeForeignPerson = includeForeignPerson;
    }

    public boolean isIncludeEntrantWithBenefit()
    {
        return _includeEntrantWithBenefit;
    }

    public void setIncludeEntrantWithBenefit(boolean includeEntrantWithBenefit)
    {
        _includeEntrantWithBenefit = includeEntrantWithBenefit;
    }

    public boolean isIncludeEntrantTargetAdmission()
    {
        return _includeEntrantTargetAdmission;
    }

    public void setIncludeEntrantTargetAdmission(boolean includeEntrantTargetAdmission)
    {
        _includeEntrantTargetAdmission = includeEntrantTargetAdmission;
    }
}