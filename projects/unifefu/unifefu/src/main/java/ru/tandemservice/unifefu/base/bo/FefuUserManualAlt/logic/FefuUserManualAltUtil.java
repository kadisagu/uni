/* $Id: FefuUserManualAltUtil.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuUserManualAlt.logic;

import ru.tandemservice.unifefu.entity.FefuUserManual;

/**
 * @author Victor Nekrasov
  * @since 28.04.2014
 */
public class FefuUserManualAltUtil
{
    /**
     * @return "fefuUserManualAlt_documentDownload_" + code
     */
    public static String getPermissionKeyDownload(FefuUserManual userManual)
    {
        return "fefuUserManualAlt_documentDownload_" + userManual.getCode();
    }
}
