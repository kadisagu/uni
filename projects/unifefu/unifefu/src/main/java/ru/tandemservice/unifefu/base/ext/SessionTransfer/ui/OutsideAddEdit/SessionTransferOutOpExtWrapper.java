/* $Id $ */
package ru.tandemservice.unifefu.base.ext.SessionTransfer.ui.OutsideAddEdit;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.SessionTransferOutsideOperationExt;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutOpWrapper;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Rostuncev Savva
 * @since 27.03.2014
 */

public class SessionTransferOutOpExtWrapper
{
    public int id;
    public Integer _workTimeDisc;

    public SessionTransferOutOpExtWrapper()
    {
        this.id = System.identityHashCode(this);
    }

    public SessionTransferOutOpExtWrapper(SessionTransferOutOpWrapper other)
    {
        setId(other.getId());
        List<SessionTransferOutsideOperationExt> sessionTransferOutsideOperationExts = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(SessionTransferOutsideOperationExt.class, "a")
		        .column("a")
		        .top(1)
                .where(eq(property(SessionTransferOutsideOperationExt.sessionTransferOutsideOperation().discipline().fromAlias("a")), value(other.getSourceDiscipline())))
                .where(eq(property(SessionTransferOutsideOperationExt.sessionTransferOutsideOperation().controlAction().id().fromAlias("a")), value(other.getSourceControlAction().getId())))
                .where(eq(property(SessionTransferOutsideOperationExt.sessionTransferOutsideOperation().mark().fromAlias("a")), value(other.getSourceMark())))
                .where(eq(property(SessionTransferOutsideOperationExt.sessionTransferOutsideOperation().targetMark().slot().studentWpeCAction().id().fromAlias("a")), value(other.getEppSlot().getId())))
//                .where(eq(property(SessionTransferOutsideOperationExt.sessionTransferOutsideOperation().targetMark().value().id().fromAlias("a")), value(other.getMark())))
		        .joinPath(DQLJoinType.inner, SessionTransferOutsideOperationExt.sessionTransferOutsideOperation().targetMark().fromAlias("a"), "tm")
                .joinEntity("a", DQLJoinType.inner, SessionSlotMarkGradeValue.class, "gv", eq(property("gv.id"), property("tm.id")))
                .where(eq(property("gv", SessionSlotMarkGradeValue.value()), value(other.getMark())))
                .where(like(property(SessionTransferOutsideOperationExt.sessionTransferOutsideOperation().comment().fromAlias("a")), value(other.getComment()))));

        if (!sessionTransferOutsideOperationExts.isEmpty())
        {
            setWorkTimeDisc(sessionTransferOutsideOperationExts.get(0).getWorkTimeDisc());
        }
    }

    public SessionTransferOutOpExtWrapper(SessionTransferOutOpExtWrapper other)
    {
        if (other != null)
        {
            setId(other.getId());
            setWorkTimeDisc(other.getWorkTimeDisc());
        }
    }

    public Integer getWorkTimeDisc()
    {
        return _workTimeDisc;
    }

    public void setWorkTimeDisc(Integer _workTimeDisc)
    {
        this._workTimeDisc = _workTimeDisc;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }
}
