package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuChangePassDiplomaWorkPeriodStuExtract

        // создано обязательное свойство afterStateExam
        if (!tool.columnExists("ffchngpssdplmwrkprdstextrct_t", "afterstateexam_p"))
        {
            // создать колонку
            tool.createColumn("ffchngpssdplmwrkprdstextrct_t", new DBColumn("afterstateexam_p", DBType.BOOLEAN));


            // задать значение по умолчанию
            java.lang.Boolean defaultAfterStateExam = false;        // TODO: задайте NOT NULL значение!
            tool.executeUpdate("update ffchngpssdplmwrkprdstextrct_t set afterstateexam_p=? where afterstateexam_p is null", defaultAfterStateExam);

            // сделать колонку NOT NULL
            tool.setColumnNullable("ffchngpssdplmwrkprdstextrct_t", "afterstateexam_p", false);

        }

        // создано обязательное свойство pluralForStateExam
        if (!tool.columnExists("ffchngpssdplmwrkprdstextrct_t", "pluralforstateexam_p"))
        {
            // создать колонку
            tool.createColumn("ffchngpssdplmwrkprdstextrct_t", new DBColumn("pluralforstateexam_p", DBType.BOOLEAN));


            // задать значение по умолчанию
            java.lang.Boolean defaultPluralForStateExam = false;        // TODO: задайте NOT NULL значение!
            tool.executeUpdate("update ffchngpssdplmwrkprdstextrct_t set pluralforstateexam_p=? where pluralforstateexam_p is null", defaultPluralForStateExam);

            // сделать колонку NOT NULL
            tool.setColumnNullable("ffchngpssdplmwrkprdstextrct_t", "pluralforstateexam_p", false);

        }


    }
}