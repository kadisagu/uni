/* $Id$ */
package ru.tandemservice.unifefu.component.group.CaptainStudentAssign;

import ru.tandemservice.uni.component.group.CaptainStudentAssign.Model;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.exception.ApplicationException;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 18.12.2013
 */
public class DAO extends ru.tandemservice.uni.component.group.CaptainStudentAssign.DAO
{
    @Override
    public void countCaptainStudentValidate(Model model)
    {
        // DEV-4545
        List<Student> captainList = UniDaoFacade.getGroupDao().getCaptainStudentList(model.getGroup());
        if (model.getCaptainStudentOldList().size() != captainList.size())
            throw new ApplicationException("Староста уже назначен другим пользователем.");
    }
}
