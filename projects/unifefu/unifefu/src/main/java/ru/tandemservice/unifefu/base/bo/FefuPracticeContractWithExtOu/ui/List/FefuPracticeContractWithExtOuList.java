/* $Id: FefuPracticeContractWithExtOuList.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.employeebase.base.bo.EmployeePost.EmployeePostManager;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.FefuPracticeContractWithExtOuManager;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.logic.FefuPracticeContractWithExtOuDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.View.FefuPracticeContractWithExtOuView;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/28/13
 * Time: 6:00 PM
 */
@Configuration
public class FefuPracticeContractWithExtOuList extends BusinessComponentManager
{
    public final static String PRACTICE_CONTRACT_DS = "practiceContractDS";

    @Bean
    public ColumnListExtPoint practiceContractCL()
    {
        return columnListExtPointBuilder(PRACTICE_CONTRACT_DS)
                .addColumn(publisherColumn("contractNum", FefuPracticeContractWithExtOu.contractNum()).businessComponent(FefuPracticeContractWithExtOuView.class).order().required(true))
                .addColumn(textColumn("internalNum", FefuPracticeContractWithExtOu.internalNum()).order())
                .addColumn(textColumn("contractDate", FefuPracticeContractWithExtOu.contractDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn("endDate", FefuPracticeContractWithExtOu.endDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn("externalOrgUnit", FefuPracticeContractWithExtOu.externalOrgUnit().title()).order())
                .addColumn(textColumn("externalOrgUnitLegalAddress", FefuPracticeContractWithExtOu.externalOrgUnit().legalAddress().title()))
                .addColumn(textColumn("externalOrgUnitFactAddress", FefuPracticeContractWithExtOu.externalOrgUnit().factAddress().title()))
                .addColumn(textColumn("headerExtOu", FefuPracticeContractWithExtOu.headerExtOu().fullTitle()).order())
                .addColumn(booleanColumn("additionalPromises", FefuPracticeContractWithExtOu.hasAdditionalPromises()))
                .addColumn(toggleColumn("checked", FefuPracticeContractWithExtOu.checked()).
                        toggleOffListener("onChangeChecked").toggleOffLabel("Не проверено").
                        toggleOnListener("onChangeChecked").toggleOnLabel("Проверено").permissionKey("fefuPracticeContractWithExtOuListCheck").required(true))
                .addColumn(toggleColumn("archival", FefuPracticeContractWithExtOu.archival()).
                        toggleOffListener("onChangeArchival").toggleOffLabel("Не в архиве").
                        toggleOnListener("onChangeArchival").toggleOnLabel("В архиве").permissionKey("fefuPracticeContractWithExtOuListArchive").required(true))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("fefuPracticeContractWithExtOuListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("ui:currentItemDeleteAlert").permissionKey("fefuPracticeContractWithExtOuListDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRACTICE_CONTRACT_DS, practiceContractCL(), practiceContractDSHandler()))
                .addDataSource(selectDS("extOuDS", FefuPracticeContractWithExtOuManager.instance().extOuComboDSHandler()).addColumn(ExternalOrgUnit.legalFormWithTitle().s()).addColumn(ExternalOrgUnit.legalAddress().titleWithFlat().s()))
                .addDataSource(selectDS("jurCtrDS", FefuPracticeContractWithExtOuManager.instance().jurCtrComboDSHandler()).addColumn(JuridicalContactor.person().fullFio().s()).addColumn(JuridicalContactor.post().s()))
                .addDataSource(selectDS("booleanDS", FefuPracticeContractWithExtOuManager.instance().booleanComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler practiceContractDSHandler()
    {
        return new FefuPracticeContractWithExtOuDSHandler(getName());
    }
}
