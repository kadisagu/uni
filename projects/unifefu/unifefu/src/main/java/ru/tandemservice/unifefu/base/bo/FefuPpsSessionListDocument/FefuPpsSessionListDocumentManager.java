/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.logic.FefuPpsSessionListDocumentDAO;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.logic.IFefuPpsSessionListDocumentDAO;

/**
 * @author DMITRY KNYAZEV
 * @since 26.08.2014
 */
@Configuration
public class FefuPpsSessionListDocumentManager extends BusinessObjectManager
{
	public static FefuPpsSessionListDocumentManager instance()
	{
		return instance(FefuPpsSessionListDocumentManager.class);
	}

	@Bean
	public IFefuPpsSessionListDocumentDAO dao()
	{
		return new FefuPpsSessionListDocumentDAO();
	}
}
