/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import ru.tandemservice.unifefu.ws.nsi.datagram.DevelopPeriodType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import javax.xml.namespace.QName;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceDevelopPeriodTest extends NsiLocalAccessBaseTest
{
    public static final String[][] RETRIEVE_WHOLE_PARAMS = new String[][]{{null}};
    public static final String[][] RETRIEVE_NON_EXISTS_PARAMS = new String[][]{{"aaaaaaaa-9f92-30c5-a055-d41c98e60f0f"}};
    public static final String[][] RETRIEVE_SINGLE_PARAMS = new String[][]{{"82200fb5-9f92-30c5-a055-d41c98e60f0f"}};
    public static final String[][] RETRIEVE_FEW_PARAMS = new String[][]{{"82200fb5-9f92-30c5-a055-d41c98e60f0f"}, {"b90f3911-e93d-3e01-b67f-42389331878b"}, {"aaaaaaaa-8d45-3209-be25-ec921dcdffa3"}, {"d7a1b7cd-35d1-3266-ba94-c8df29db6bc4"}};

    public static final String[][] INSERT_NEW_PARAMS = new String[][]{{"82200fb5-9f92-30c5-a055-xxxxxxxxxxxX", "10 лет 1 мес.", "25", "10", "-1"}};
    public static final String[][] INSERT_NEW_ANALOG_PARAMS = new String[][]{{"82200fb5-9f92-30c5-a055-xxxxxxxxxxxY", "1 год 10 мес.", "12", "2", "12"}};
    public static final String[][] INSERT_NEW_GUID_EXISTS_PARAMS = new String[][]{{"82200fb5-9f92-30c5-a055-xxxxxxxxxxxY", "1 год 10 мес. 1", "12", "5", "12"}};
    public static final String[][] INSERT_NEW_MERGE_GUID_EXISTS_PARAMS = new String[][]{{"82200fb5-9f92-30c5-a055-xxxxxxxxxxxZ", "1 год 10 мес. 2", "12", "7", "12", "82200fb5-9f92-30c5-a055-xxxxxxxxxxxZ; 82200fb5-9f92-30c5-a055-xxxxxxxxxxxY; 90800349-619a-11e0-a335-xxxxxxxxxx39"}};
    public static final String[] INSERT_MASS_TEMPLATE_PARAMS = new String[]{"Тест", "Т"};

    public static final String[][] DELETE_EMPTY_PARAMS = new String[][]{{null}};
    public static final String[][] DELETE_NON_EXISTS_PARAMS = new String[][]{{"add6eb96-818e-3949-8bb7-cd8c954955bX"}};
    public static final String[][] DELETE_SINGLE_PARAMS = new String[][]{{"add6eb96-818e-3949-8bb7-cd8c954955bc"}};
    public static final String[][] DELETE_NON_DELETABLE_PARAMS = new String[][]{{"add6eb96-818e-3949-8bb7-cd8c954955bc"}};
    public static final String[][] DELETE_MASS_PARAMS = new String[][]{{"add6eb96-818e-3949-8bb7-cd8c954955bc"}};

    @Override
    protected INsiEntity createNsiEntity(String[] fieldValues)
    {
        DevelopPeriodType entity = NsiLocalAccessServiceTestUtil.FACTORY.createDevelopPeriodType();
        if (null != fieldValues)
        {
            if (fieldValues.length > 0) entity.setID(fieldValues[0]);
            if (fieldValues.length > 1) entity.setDevelopPeriodName(fieldValues[1]);
            if (fieldValues.length > 2) entity.setDevelopPeriodID(fieldValues[2]);
            if (fieldValues.length > 3) entity.setDevelopPeriodLastCourse(null != fieldValues[3] ? fieldValues[3] : "0");
            if (fieldValues.length > 4) entity.setDevelopPeriodPriority(null != fieldValues[4] ? fieldValues[4] : "-1");
            if (fieldValues.length > 5 && null != fieldValues[5])
                entity.getOtherAttributes().put(new QName("mergeDuplicates"), fieldValues[5]);
        }
        return entity;
    }
}