/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.EducationFormType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class EducationFormTypeReactor extends SimpleCatalogEntityNewReactor<EducationFormType, DevelopForm>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<DevelopForm> getEntityClass()
    {
        return DevelopForm.class;
    }

    @Override
    public Class<EducationFormType> getNSIEntityClass()
    {
        return EducationFormType.class;
    }

    @Override
    public EducationFormType getCatalogElementRetrieveRequestObject(String guid)
    {
        EducationFormType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createEducationFormType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public EducationFormType createEntity(CoreCollectionUtils.Pair<DevelopForm, FefuNsiIds> entityPair)
    {
        EducationFormType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createEducationFormType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setEducationFormID(entityPair.getX().getCode());
        nsiEntity.setEducationFormName(entityPair.getX().getTitle());
        nsiEntity.setEducationFormNameShort(entityPair.getX().getShortTitle());
        nsiEntity.setEducationFormGroup(entityPair.getX().getGroup());
        return nsiEntity;
    }

    @Override
    public DevelopForm createEntity(EducationFormType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getEducationFormName()) return null;

        DevelopForm entity = new DevelopForm();
        entity.setTitle(nsiEntity.getEducationFormName());
        if (null != StringUtils.trimToNull(nsiEntity.getEducationFormNameShort()))
            entity.setShortTitle(nsiEntity.getEducationFormNameShort());
        else entity.setShortTitle(nsiEntity.getEducationFormName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(DevelopForm.class));
        entity.setGroup(nsiEntity.getEducationFormGroup());

        return entity;
    }

    @Override
    public DevelopForm updatePossibleDuplicateFields(EducationFormType nsiEntity, CoreCollectionUtils.Pair<DevelopForm, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), DevelopForm.title().s()))
                entityPair.getX().setTitle(nsiEntity.getEducationFormName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), DevelopForm.shortTitle().s()))
                entityPair.getX().setShortTitle(null != StringUtils.trimToNull(nsiEntity.getEducationFormNameShort()) ? nsiEntity.getEducationFormNameShort() : nsiEntity.getEducationFormName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), DevelopForm.group().s()))
                entityPair.getX().setGroup(nsiEntity.getEducationFormGroup());
        }
        return entityPair.getX();
    }

    @Override
    public EducationFormType updateNsiEntityFields(EducationFormType nsiEntity, DevelopForm entity)
    {
        nsiEntity.setEducationFormID(entity.getCode());
        nsiEntity.setEducationFormName(entity.getTitle());
        nsiEntity.setEducationFormNameShort(entity.getShortTitle());
        nsiEntity.setEducationFormGroup(entity.getGroup());
        return nsiEntity;
    }
}