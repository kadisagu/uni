/* $Id$ */
package ru.tandemservice.unifefu.dao.mdbio;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.FourKey;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.movestudent.entity.StudentOtherOrder;
import ru.tandemservice.movestudent.entity.StudentOtherParagraph;
import ru.tandemservice.movestudent.entity.StudentReasonToTypeRel;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Dmitry Seleznev
 * @since 17.04.2013
 */
public class FefuOrdersIOAddDataDao extends BaseIODao implements IFefuOrdersIOAddDataDao
{
    private static final SimpleDateFormat DATE_PARSER = new SimpleDateFormat("dd.MM.yyyy");
    private static final Set<FourKey<String, String, Date, String>> ORDERS_SET = new HashSet<>();

    private static final OrderStates COMMITED_ORDER_STATE = UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED);
    private static final ExtractStates COMMITED_EXTRACT_STATE = UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED);

    public static StudentExtractType MAIN_EXTRACT_TYPE_CATEGORY = null;
    public static StudentExtractType DOP_EXTRACT_TYPE_CATEGORY = null;
    public static StudentExtractType SIR_EXTRACT_TYPE_CATEGORY = null;
    public static StudentExtractType PRAK_EXTRACT_TYPE_CATEGORY = null;
    public static StudentExtractType OBS_EXTRACT_TYPE_CATEGORY = null;

    private static Map<String, StudentExtractType> PARENT_CATEGORY_MAP = new HashMap<>();
    private static Map<String, StudentExtractType> MAIN_EXTRACT_TYPES_MAP = new HashMap<>();
    private static Map<String, StudentExtractType> DOP_EXTRACT_TYPES_MAP = new HashMap<>();
    private static Map<String, StudentExtractType> SIR_EXTRACT_TYPES_MAP = new HashMap<>();
    private static Map<String, StudentExtractType> PRAK_EXTRACT_TYPES_MAP = new HashMap<>();
    private static Map<String, StudentExtractType> OBS_EXTRACT_TYPES_MAP = new HashMap<>();

    private static Map<String, Map<String, StudentExtractType>> EXTRACT_CATEGORY_TO_EXTRACT_TYPES_MAP = new HashMap<>();
    private static Map<String, StudentOrderReasons> REASONS_MAP = new HashMap<>();

    private static final Map<String, Course> COURSES_MAP = new HashMap<>();
    private static final Map<String, Term> TERMS_MAP = new HashMap<>();

    public void initConstants()
    {
        MAIN_EXTRACT_TYPE_CATEGORY = UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_BASIC_EDU_PROGRAM_OTHER_ORDER);
        DOP_EXTRACT_TYPE_CATEGORY = UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_ADDITIONAL_EDU_PROGRAM_OTHER_ORDER);
        SIR_EXTRACT_TYPE_CATEGORY = UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_ORPHAN_GRANT_OTHER_ORDER);
        PRAK_EXTRACT_TYPE_CATEGORY = UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_PRACTICE_OTHER_ORDER);
        OBS_EXTRACT_TYPE_CATEGORY = UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_ENCOURAGE_OTHER_ORDER);

        for (Course course : DevelopGridDAO.getCourseMap().values())
        {
            COURSES_MAP.put(course.getTitle(), course);
        }

        for (Term term : DevelopGridDAO.getTermMap().values())
        {
            TERMS_MAP.put(term.getTitle(), term);
        }

        for (StudentOrderReasons reason : UniDaoFacade.getCoreDao().getCatalogItemList(StudentOrderReasons.class))
        {
            REASONS_MAP.put(reason.getTitle(), reason);
        }

        for (StudentExtractType exType : UniDaoFacade.getCoreDao().getCatalogItemList(StudentExtractType.class, StudentExtractType.parent().s(), MAIN_EXTRACT_TYPE_CATEGORY))
        {
            MAIN_EXTRACT_TYPES_MAP.put(exType.getTitle(), exType);
        }

        for (StudentExtractType exType : UniDaoFacade.getCoreDao().getCatalogItemList(StudentExtractType.class, StudentExtractType.parent().s(), DOP_EXTRACT_TYPE_CATEGORY))
        {
            DOP_EXTRACT_TYPES_MAP.put(exType.getTitle(), exType);
        }

        for (StudentExtractType exType : UniDaoFacade.getCoreDao().getCatalogItemList(StudentExtractType.class, StudentExtractType.parent().s(), SIR_EXTRACT_TYPE_CATEGORY))
        {
            SIR_EXTRACT_TYPES_MAP.put(exType.getTitle(), exType);
        }

        for (StudentExtractType exType : UniDaoFacade.getCoreDao().getCatalogItemList(StudentExtractType.class, StudentExtractType.parent().s(), PRAK_EXTRACT_TYPE_CATEGORY))
        {
            PRAK_EXTRACT_TYPES_MAP.put(exType.getTitle(), exType);
        }

        for (StudentExtractType exType : UniDaoFacade.getCoreDao().getCatalogItemList(StudentExtractType.class, StudentExtractType.parent().s(), OBS_EXTRACT_TYPE_CATEGORY))
        {
            OBS_EXTRACT_TYPES_MAP.put(exType.getTitle(), exType);
        }

        EXTRACT_CATEGORY_TO_EXTRACT_TYPES_MAP.put(MAIN_EXTRACT_TYPE_CATEGORY.getCode(), MAIN_EXTRACT_TYPES_MAP);
        EXTRACT_CATEGORY_TO_EXTRACT_TYPES_MAP.put(DOP_EXTRACT_TYPE_CATEGORY.getCode(), DOP_EXTRACT_TYPES_MAP);
        EXTRACT_CATEGORY_TO_EXTRACT_TYPES_MAP.put(SIR_EXTRACT_TYPE_CATEGORY.getCode(), SIR_EXTRACT_TYPES_MAP);
        EXTRACT_CATEGORY_TO_EXTRACT_TYPES_MAP.put(PRAK_EXTRACT_TYPE_CATEGORY.getCode(), PRAK_EXTRACT_TYPES_MAP);
        EXTRACT_CATEGORY_TO_EXTRACT_TYPES_MAP.put(OBS_EXTRACT_TYPE_CATEGORY.getCode(), OBS_EXTRACT_TYPES_MAP);

        PARENT_CATEGORY_MAP.put(MAIN_EXTRACT_TYPE_CATEGORY.getCode(), MAIN_EXTRACT_TYPE_CATEGORY);
        PARENT_CATEGORY_MAP.put(DOP_EXTRACT_TYPE_CATEGORY.getCode(), DOP_EXTRACT_TYPE_CATEGORY);
        PARENT_CATEGORY_MAP.put(SIR_EXTRACT_TYPE_CATEGORY.getCode(), SIR_EXTRACT_TYPE_CATEGORY);
        PARENT_CATEGORY_MAP.put(PRAK_EXTRACT_TYPE_CATEGORY.getCode(), PRAK_EXTRACT_TYPE_CATEGORY);
        PARENT_CATEGORY_MAP.put(OBS_EXTRACT_TYPE_CATEGORY.getCode(), OBS_EXTRACT_TYPE_CATEGORY);
    }

    @Override
    public Map<String, Long> doImport_LotusOrdersData(Database mdb, final String dataBaseFileName, final String tableName, final String extractTypeCategoryCode) throws IOException
    {
        initConstants();

        Table table = mdb.getTable(tableName);
        if (table == null)
            throw new ApplicationException("Таблица «" + tableName + "» отсутствует в базе данных (см. файл " + dataBaseFileName + ").");

        final Session session = this.getSession();
        final Map<String, StudentFefuExt> extensionsMap = new HashMap<>();
        final Map<String, List<StudentFefuExt>> duplicatesMap = new HashMap<>();

        List<Object[]> ordersList = new DQLSelectBuilder()
                .fromEntity(OtherStudentExtract.class, "se")
                .joinEntity("se", DQLJoinType.inner, StudentOtherParagraph.class, "sp", eq(property(OtherStudentExtract.paragraph().id().fromAlias("se")), property(StudentOtherParagraph.id().fromAlias("sp"))))
                .joinEntity("sp", DQLJoinType.inner, StudentOtherOrder.class, "so", eq(property(StudentOtherParagraph.order().id().fromAlias("sp")), property(StudentOtherOrder.id().fromAlias("so"))))
                .joinEntity("se", DQLJoinType.inner, StudentFefuExt.class, "stex", eq(property(OtherStudentExtract.entity().id().fromAlias("se")), property(StudentFefuExt.student().id().fromAlias("stex"))))
                .column(property(StudentFefuExt.integrationId().fromAlias("stex")))
                .column(property(StudentOtherOrder.number().fromAlias("so")))
                .column(property(StudentOtherOrder.commitDate().fromAlias("so")))
                .column(property(OtherStudentExtract.comment().fromAlias("se")))
                .order(property(OtherStudentExtract.entity().id().fromAlias("se")))
                .createStatement(getSession()).list();

        for (Object[] item : ordersList)
        {
            ORDERS_SET.add(new FourKey<>((String) item[0], (String) item[1], (Date) item[2], (String) item[3]));
        }

        List<StudentFefuExt> extensions = new DQLSelectBuilder().fromEntity(StudentFefuExt.class, "fe").column("fe")
                .createStatement(session).list();

        for (StudentFefuExt ext : extensions)
        {
            if (!extensionsMap.containsKey(ext.getIntegrationId()))
                extensionsMap.put(ext.getIntegrationId(), ext);
            else
            {
                List<StudentFefuExt> duplicatesList = duplicatesMap.get(ext.getIntegrationId());
                if (null == duplicatesList) duplicatesList = new ArrayList<>();
                duplicatesList.add(ext);
                duplicatesMap.put(ext.getIntegrationId(), duplicatesList);
            }
        }

        BatchUtils.execute(table, 32, rows -> {
            for (final Map<String, Object> row : rows)
            {
                String id = StringUtils.trimToNull((String) row.get("cuid"));
                String orderNumber = StringUtils.trimToNull((String) row.get("prik_no"));
                String orderDateStr = StringUtils.trimToNull((String) row.get("prik_date"));
                String comment = StringUtils.trimToNull((String) row.get("zametki"));

                // Хак для учета приказов по сиротам, там какой-то кретин назвал соответствующие поля по-другому
                if (null == id) id = StringUtils.trimToNull((String) row.get("siruid"));
                if (null == orderNumber) orderNumber = StringUtils.trimToNull((String) row.get("sirprik"));
                if (null == orderDateStr) orderDateStr = StringUtils.trimToNull((String) row.get("sirdate"));
                if (null == comment) comment = StringUtils.trimToNull((String) row.get("sirnotes"));
                if (null == comment) comment = StringUtils.trimToNull((String) row.get("prim"));
                if (null == comment) comment = StringUtils.trimToNull((String) row.get("poos"));

                if (null == orderDateStr && null != orderNumber)
                {
                    String orderNumberNew = orderNumber.replaceAll("оть", "от");
                    String[] orderCredentials = orderNumberNew.split("от");
                    if (orderCredentials.length < 2) orderCredentials = orderNumberNew.split("  ");

                    if (orderCredentials.length > 1)
                    {
                        orderNumber = orderCredentials[0].trim();
                        orderDateStr = orderCredentials[1].trim();
                    }
                }

                if (null == id)
                {
                    log4j_logger.error("SKIPPED: There is no student id specified for the order No " + orderNumber + " " + orderDateStr + " from database " + dataBaseFileName + ".");
                    continue;
                }

                Date orderDate = null;

                if (null != orderDateStr)
                {
                    try
                    {
                        orderDate = DATE_PARSER.parse(orderDateStr);
                        if (CoreDateUtils.getYear(orderDate) < 1950 || CoreDateUtils.getYear(orderDate) > 2050)
                        {
                            log4j_logger.error("Date for Order No " + orderNumber + " " + orderDateStr + " was changed to null, because year less than SQL server can operate with (lotus student ID = " + id + "). Database file name is " + dataBaseFileName);
                            orderDate = null;
                        }
                    }
                    catch (ParseException ex)
                    {
                        log4j_logger.error("Date for Order No " + orderNumber + " " + orderDateStr + " is invalid and can't be parsed (lotus student ID = " + id + "). Database file name is " + dataBaseFileName);
                    }
                }

                if (ORDERS_SET.contains(new FourKey<>(id, orderNumber, orderDate, comment)))
                {
                    log4j_logger.warn("SKIPPED: Order No " + orderNumber + " " + orderDateStr + " was skipped, because order with the same number and date for this student (lotus ID = " + id + ") already exists in database " + dataBaseFileName + ". Comment = '" + comment + "'.");
                    continue;
                }

                try
                {
                    final StudentFefuExt studentFefuExt = extensionsMap.get(id);
                    if (null == studentFefuExt)
                    {
                        log4j_logger.error("SKIPPED: Order No " + orderNumber + " " + orderDateStr + " was skipped, because there is no student with lotus ID = " + id + " in the OB database. Comment = '" + comment + "'.");
                        continue;
                    }

                    List<StudentFefuExt> extList = duplicatesMap.get(id);
                    if (null == extList)
                        extList = new ArrayList<>();
                    extList.add(studentFefuExt);

                    for (StudentFefuExt singleExt : extList)
                    {
                        singleExt = (StudentFefuExt) session.load(StudentFefuExt.class, singleExt.getId());
                        createMainOrder(singleExt.getStudent(), id, orderNumber, orderDate, row, extractTypeCategoryCode);
                    }

                }
                catch (final Throwable t)
                {
                    log4j_logger.fatal("Error with processing: table = " + tableName + ", database file name = " + dataBaseFileName + " [cuid = " + id + ", prik_no = " + orderNumber + ", prik_date = " + DateFormatter.DEFAULT_DATE_FORMATTER.format(orderDate) + "]");
                    log4j_logger.fatal(t.getMessage(), t);
                    throw new IllegalArgumentException("Студент cuid=" + id + ", приказ № " + orderNumber + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(orderDate) + ": Произошла ошибка обработки данных для импорта.");
                }
            }

            System.err.print(".");
            session.flush();
            session.clear();
        });

        return new HashMap<>();
    }

    private StudentOtherOrder createMainOrder(String orderNumber, Date orderDate)
    {
        StudentOtherOrder order = new StudentOtherOrder();

        order.setCreateDate(new Date());
        order.setCommitDate(orderDate);
        order.setNumber(orderNumber);
        order.setCommitDateSystem(orderDate);
        order.setState(COMMITED_ORDER_STATE);
        return order;
    }

    private void createMainOrder(Student student, String integrationId, String orderNumber, Date orderDate, Map<String, Object> row, String extractTypeCategoryCode)
    {
        String extractTypeStr = StringUtils.trimToNull((String) row.get("kateg_prik"));
        if (null == extractTypeStr) extractTypeStr = StringUtils.trimToNull((String) row.get("sircateg"));
        if (null == extractTypeStr) extractTypeStr = "ПРИКАЗ БЕЗ ТИПА";
        StudentExtractType extractType = EXTRACT_CATEGORY_TO_EXTRACT_TYPES_MAP.get(extractTypeCategoryCode).get(extractTypeStr);
        if (null == extractType)
        {
            extractType = new StudentExtractType();
            extractType.setParent(PARENT_CATEGORY_MAP.get(extractTypeCategoryCode));
            extractType.setCode(String.valueOf(System.currentTimeMillis()));
            extractType.setTitle(extractTypeStr);
            extractType.setActive(true);
            save(extractType);
            getSession().flush();

            EXTRACT_CATEGORY_TO_EXTRACT_TYPES_MAP.get(extractTypeCategoryCode).put(extractTypeStr, extractType);
        }
        else if (null == extractType.getId())
        {
            save(extractType);
            getSession().flush();
        }

        // Получение значений колонок для основных приказов
        String courseStr = StringUtils.trimToNull((String) row.get("kurs"));
        if (null == courseStr) courseStr = StringUtils.trimToNull((String) row.get("siryear"));

        Course course = null;
        if (null != courseStr)
        {
            course = COURSES_MAP.get(courseStr);
            if (null == course)
                log4j_logger.error("There is no course in the Tandem University Database, like «" + courseStr + "», presented in the Lotus database. Course was saved as null.");
        }

        String termStr = StringUtils.trimToNull((String) row.get("sem"));
        if (null == termStr) termStr = StringUtils.trimToNull((String) row.get("sirsem"));

        Term term = null;
        if (null != termStr)
        {
            term = TERMS_MAP.get(termStr);
            if (null == term)
                log4j_logger.error("There is no term in the Tandem University Database, like «" + termStr + "», presented in the Lotus database. Term was saved as null.");
        }

        String institute = StringUtils.trimToNull((String) row.get("institute_new"));
        String faculty = StringUtils.trimToNull((String) row.get("school_new"));
        String speciality = StringUtils.trimToNull((String) row.get("major_new"));
        String profile = StringUtils.trimToNull((String) row.get("submajor_new"));
        String grantSize = StringUtils.trimToNull((String) row.get("stipen"));
        String comment = StringUtils.trimToNull((String) row.get("zametki"));

        // Получение значений колонок для приказов по доп образованию
        if (null == institute) institute = StringUtils.trimToNull((String) row.get("institute"));
        if (null == faculty) faculty = StringUtils.trimToNull((String) row.get("school"));
        if (null == speciality) speciality = StringUtils.trimToNull((String) row.get("major"));
        if (null == profile) profile = StringUtils.trimToNull((String) row.get("submajor"));
        String dopEduLevel = StringUtils.trimToNull((String) row.get("dop_op"));

        // Получение значений колонок для приказов по сиротам
        String lastName = StringUtils.trimToNull((String) row.get("sirsurname"));
        String firstName = StringUtils.trimToNull((String) row.get("sirname"));
        String middleName = StringUtils.trimToNull((String) row.get("sirlastname"));
        if (null == grantSize) grantSize = StringUtils.trimToNull((String) row.get("sirsumma"));
        if (null == comment) comment = StringUtils.trimToNull((String) row.get("sirnotes"));
        String fio = (null != lastName ? lastName : "") + (null == firstName ? "" : (" " + firstName)) + (null == middleName ? "" : (" " + middleName));

        // Получение значений колонок для приказов по практикам
        String practiceType = StringUtils.trimToNull((String) row.get("vid"));
        String practicePlace = StringUtils.trimToNull((String) row.get("mesto"));
        String practiceOwnershipType = StringUtils.trimToNull((String) row.get("forma"));
        String practiceSettlement = StringUtils.trimToNull((String) row.get("gorod"));
        String practicePeriod = StringUtils.trimToNull((String) row.get("sroki"));
        String practiceSupervisor = StringUtils.trimToNull((String) row.get("ruk"));
        if (null == comment) comment = StringUtils.trimToNull((String) row.get("prim"));

        // Получение значений колонок для приказов по общественной работе
        String encouragelty = StringUtils.trimToNull((String) row.get("poos"));
        String encourageltyEndDateStr = StringUtils.trimToNull((String) row.get("data_sn"));
        if (null == comment) comment = encouragelty;
        Date encourageltyEndDate = null;
        if (null != encourageltyEndDateStr)
        {
            try
            {
                encourageltyEndDate = DATE_PARSER.parse(encourageltyEndDateStr);
            }
            catch (ParseException ex)
            {
                // nop
            }

            if (encourageltyEndDate == null || CoreDateUtils.getYear(encourageltyEndDate) < 1950)
                log4j_logger.error("Encouragement / penalty end date could not be parsed for Order No " + orderNumber + " " +
                                           (orderDate != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(orderDate) : "") + ".");
        }

        String reasonStr = StringUtils.trimToNull((String) row.get("pric"));
        StudentOrderReasons reason = REASONS_MAP.get(reasonStr);
        if (null != reasonStr && null == reason)
        {
            reason = new StudentOrderReasons();
            reason.setCode(String.valueOf(System.currentTimeMillis()));
            reason.setTitle(reasonStr);
            reason.setCommentRequired(false);
            reason.setCommentable(false);
            save(reason);

            StudentReasonToTypeRel reasonToTypeRel = new StudentReasonToTypeRel();
            reasonToTypeRel.setSecond(extractType);
            reasonToTypeRel.setFirst(reason);
            save(reasonToTypeRel);

            getSession().flush();

            REASONS_MAP.put(reasonStr, reason);
        }

        StudentOtherOrder order = createMainOrder(orderNumber, orderDate);
        StudentOtherParagraph paragraph = new StudentOtherParagraph();
        paragraph.setOrder(order);
        paragraph.setNumber(1);

        OtherStudentExtract extract = new OtherStudentExtract();

        extract.setReason(reason);
        extract.setEntity(student);
        extract.setType(extractType);
        extract.setState(COMMITED_EXTRACT_STATE);
        extract.setParagraph(paragraph);
        extract.setCreateDate(new Date());
        extract.setCommitted(true);
        extract.setNumber(1);

        extract.setGroupStr("-");
        extract.setStudentStatusStr("-");
        extract.setPersonalNumberStr("-");
        extract.setCompensationTypeStr("-");
        extract.setStudentTitle(!StringUtils.isEmpty(fio) ? fio : student.getPerson().getFullFio());
        extract.setStudentTitleStr(!StringUtils.isEmpty(fio) ? fio : student.getPerson().getFullFio());
        extract.setCourseStr(null != courseStr ? courseStr : "-");
        extract.setFormativeOrgUnitStr("-");
        extract.setTerritorialOrgUnitStr("-");
        extract.setEducationLevelHighSchoolStr("-");
        extract.setDevelopFormStr("-");
        extract.setDevelopConditionStr("-");
        extract.setDevelopTechStr("-");
        extract.setDevelopPeriodStr("-");

        extract.setCourse(course);
        extract.setTerm(term);
        extract.setInstitute(institute);
        extract.setFaculty(faculty);
        extract.setSpeciality(speciality);
        extract.setProfile(profile);
        extract.setGrantSize(grantSize);
        extract.setDopEduLevel(dopEduLevel);
        extract.setPracticeType(practiceType);
        extract.setPracticePlace(practicePlace);
        extract.setPracticeOwnershipType(practiceOwnershipType);
        extract.setPracticeSettlement(practiceSettlement);
        extract.setPracticePeriod(practicePeriod);
        extract.setPracticeSupervisor(practiceSupervisor);
        extract.setComment(comment);


        save(order);
        save(paragraph);
        save(extract);

        order.setState(COMMITED_ORDER_STATE);
        extract.setState(COMMITED_EXTRACT_STATE);
        update(order);
        update(extract);

        ORDERS_SET.add(new FourKey<>(integrationId, orderNumber, orderDate, comment));
    }
}