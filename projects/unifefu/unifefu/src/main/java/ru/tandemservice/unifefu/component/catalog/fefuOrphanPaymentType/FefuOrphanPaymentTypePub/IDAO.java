/* $Id$ */
package ru.tandemservice.unifefu.component.catalog.fefuOrphanPaymentType.FefuOrphanPaymentTypePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;

/**
 * @author nvankov
 * @since 11/18/13
 */
public interface IDAO extends IDefaultCatalogPubDAO<FefuOrphanPaymentType, Model>
{
    void updatePriorityUp(Long paymentTypeId);

    void updatePriorityDown(Long paymentTypeId);
}
