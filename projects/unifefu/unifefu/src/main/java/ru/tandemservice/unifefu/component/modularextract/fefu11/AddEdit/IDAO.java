/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu11.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 25.04.2013
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuAcadGrantAssignStuEnrolmentExtract, Model>
{
}