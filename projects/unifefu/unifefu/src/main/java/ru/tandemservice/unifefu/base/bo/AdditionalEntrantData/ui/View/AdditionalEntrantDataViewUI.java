/* $Id$ */
package ru.tandemservice.unifefu.base.bo.AdditionalEntrantData.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.unifefu.base.bo.AdditionalEntrantData.AdditionalEntrantDataManager;
import ru.tandemservice.unifefu.base.bo.AdditionalEntrantData.ui.Edit.AdditionalEntrantDataEdit;
import ru.tandemservice.unifefu.entity.EntrantFefuExt;

/**
 * @author Nikolay Fedorovskih
 * @since 06.06.2013
 */
@State({
               @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrantId", required = true)
       })
public class AdditionalEntrantDataViewUI extends UIPresenter
{
    private Long entrantId;
    private EntrantFefuExt entrantFefuExt;

    @Override
    public void onComponentRefresh()
    {
        setEntrantFefuExt(AdditionalEntrantDataManager.instance().dao().getEntrantFefuExt(getEntrantId()));
    }

    public void onClickEditInfo()
    {
        _uiActivation.asCurrent(AdditionalEntrantDataEdit.class).activate();
    }

    public Long getEntrantId()
    {
        return entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        this.entrantId = entrantId;
    }

    public EntrantFefuExt getEntrantFefuExt()
    {
        return entrantFefuExt;
    }

    public void setEntrantFefuExt(EntrantFefuExt entrantFefuExt)
    {
        this.entrantFefuExt = entrantFefuExt;
    }
}