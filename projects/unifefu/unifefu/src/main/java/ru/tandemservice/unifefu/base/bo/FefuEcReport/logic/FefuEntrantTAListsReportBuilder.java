/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.FefuEcReportManager;
import ru.tandemservice.unifefu.base.vo.FefuEntrantTAListsReportVO;
import ru.tandemservice.unifefu.entity.FefuEntrantContract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 8/8/13
 */
public class FefuEntrantTAListsReportBuilder implements IFefuEcReportBuilder
{
    private FefuEntrantTAListsReportVO reportVO;
    private Session session;
    Map<RequestedEnrollmentDirection, FefuEntrantContract> fefuEntrantContractMap = new HashMap<>();
    Map<Long, Boolean> enrolledMap = new HashMap<>();

    private Map<EnrollmentDirection, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>> direction2disciplinesMap;
    //private Map<EnrollmentDirection, EnrollmentDirectionExamSetData> direction2examSetDetailMap;
    private EntrantDataUtil entrantDataUtil;

    public FefuEntrantTAListsReportBuilder(FefuEntrantTAListsReportVO reportVO)
    {
        this.reportVO = reportVO;
    }

    @Override
    public DatabaseFile getContent(Session session)
    {
        this.session = session;
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        initMaps();

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "fefuEntrantTAListsReport");

        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        List<IRtfElement> elementList = document.getElementList();
        RtfTable formativeOrgUnitTemplate = (RtfTable) UniRtfUtil.findElement(elementList, "T1");
        int formativeOrgUnitIndex = elementList.indexOf(formativeOrgUnitTemplate);
        elementList.remove(formativeOrgUnitIndex);
        elementList.remove(formativeOrgUnitIndex);

        RtfTable tableTemplate = (RtfTable) UniRtfUtil.findElement(elementList, "T2");
        int tableTemplateIndex = elementList.indexOf(tableTemplate);
        elementList.remove(tableTemplateIndex);

        injectTables(formativeOrgUnitTemplate, tableTemplate, elementList, tableTemplateIndex, getData());

        return RtfUtil.toByteArray(document);
    }

    @SuppressWarnings("deprecation")
    private void initMaps()
    {
        // fetch data
        CompensationType compensationType = DataAccessServices.dao().get(CompensationType.class, CompensationType.code(), CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT);
        direction2disciplinesMap = ExamSetUtil.getDirection2disciplinesMap(session, reportVO.getEnrollmentCampaign(), reportVO.getStudentCategoryList(), compensationType);
        //direction2examSetDetailMap = ExamSetUtil.getDirectionExamSetDataMap(session, reportVO.getEnrollmentCampaign(), reportVO.getStudentCategoryList(), compensationType);
        entrantDataUtil = new EntrantDataUtil(session, reportVO.getEnrollmentCampaign(), getRequestedEnrollmentDirectionsMQBuilder());

        DQLSelectBuilder dirbuilder = getRequestedEnrollmentDirectionsBuilder();
        dirbuilder.column(property("red", RequestedEnrollmentDirection.id()));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuEntrantContract.class, "ctr");
        builder.where(in(property("ctr", FefuEntrantContract.requestedEnrollmentDirection().id()), dirbuilder.buildQuery()));
        List<FefuEntrantContract> contracts = builder.createStatement(session).list();
        for (FefuEntrantContract contract : contracts)
        {
            fefuEntrantContractMap.put(contract.getRequestedEnrollmentDirection(), contract);
        }

        DQLSelectBuilder enrolledBuilder = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "pre")
                .joinPath(DQLJoinType.inner, PreliminaryEnrollmentStudent.requestedEnrollmentDirection().fromAlias("pre"), "r")
                .column(property("r", RequestedEnrollmentDirection.id())) // [0]
                .column(property("pre", PreliminaryEnrollmentStudent.compensationType().code())) // [1]
                .where(in(property("r", RequestedEnrollmentDirection.id()), getRequestedEnrollmentDirectionsBuilder().buildQuery()))
                .where(eq(property("r", RequestedEnrollmentDirection.state().code()), value(EntrantStateCodes.ENROLED)))
                .where(eq(property("r", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().id()),
                          property("pre", PreliminaryEnrollmentStudent.educationOrgUnit().id())));
        for (Object[] item : enrolledBuilder.createStatement(session).<Object[]>list())
        {
            enrolledMap.put((Long) item[0], CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(item[1]));
        }
    }

    private void injectTables(RtfTable formativeOrgUnitTemplate, RtfTable tableTemplate, List<IRtfElement> elementList, int startIndex, Map<FormOU, Map<EnrollmentDirection, List<EntrantData>>> map)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<FormOU> ous = new ArrayList<>(map.keySet());
        Collections.sort(ous, (o1, o2) -> {
            if (o1.getFormativeOrgUnit().equals(o2.getFormativeOrgUnit()))
            {
                return o1.getTerritorialOrgUnit().getTitle().compareTo(o2.getTerritorialOrgUnit().getTitle());
            }
            else
            {
                return o1.getFormativeOrgUnit().getTitle().compareTo(o2.getFormativeOrgUnit().getTitle());
            }
        });
        for (FormOU ou : ous)
        {
            RtfTable formOUtable = formativeOrgUnitTemplate.getClone();
            elementList.add(startIndex++, formOUtable);
            RtfString ouTitle = new RtfString().append(ou.getTitle());
            new RtfInjectModifier().put("T1", ouTitle).modify(Collections.<IRtfElement>singletonList(formOUtable));

            Map<EnrollmentDirection, List<EntrantData>> dirMap = map.get(ou);
            List<EnrollmentDirection> enrollmentDirections = new ArrayList<>(dirMap.keySet());
            Collections.sort(enrollmentDirections, (o1, o2) -> o1.getEducationOrgUnit().getEducationLevelHighSchool().getTitle().compareTo(o2.getEducationOrgUnit().getEducationLevelHighSchool().getTitle()));
            for (EnrollmentDirection direction : enrollmentDirections)
            {
                List<EntrantData> entrantDatas = dirMap.get(direction);
                RtfTable table = tableTemplate.getClone();
                elementList.add(startIndex++, table);
                RtfString param = new RtfString().append(direction.getEducationOrgUnit().getEducationLevelHighSchool().getTitle());
                new RtfInjectModifier().put("PARAM", param).modify(Collections.<IRtfElement>singletonList(table));

                List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structure = direction2disciplinesMap.get(direction);
                Integer parts = null;
                if(null != structure && !structure.isEmpty())
                    parts = structure.size();
                tableModifier.put("T2", new TableRtfRowIntercepter(parts)).put("T2", getTable(entrantDatas));
                tableModifier.modify(elementList);
            }
        }
    }

    private String[][] getTable(List<EntrantData> entrantDatas)
    {
        List<String[]> rows = Lists.newArrayList();
        Collections.sort(entrantDatas, CommonCollator.<EntrantData>
                comparing(d -> d.getEntrant().getFio()).thenComparingLong(d -> d.getEntrant().getId()));

        for (EntrantData data : entrantDatas)
        {
            MainRow mainRow = new MainRow(data, entrantDatas.indexOf(data) + 1);
            rows.add(mainRow.toStringArray());
        }
        if (rows.isEmpty())
            rows.add(new String[]{""});
        return rows.toArray(new String[][]{});
    }


    @SuppressWarnings("unchecked")
    private Map<FormOU, Map<EnrollmentDirection, List<EntrantData>>> getData()
    {
        Map<FormOU, Map<EnrollmentDirection, List<EntrantData>>> result = new LinkedHashMap<>();

        List<RequestedEnrollmentDirection> requestedEnrollmentDirections = getRequestedEnrollmentDirectionsBuilder().createStatement(session).list();

        DQLSelectBuilder enrExtractBuilder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "ext");
        enrExtractBuilder.where(in(property("ext", EnrollmentExtract.entity().requestedEnrollmentDirection()),
                getRequestedEnrollmentDirectionsBuilder().buildQuery()));

        enrExtractBuilder.where(and(
                or(
                        isNotNull(property("ext", EnrollmentExtract.paragraph().order().number())),
                        not(eq(property("ext", EnrollmentExtract.paragraph().order().number()), value("")))
                        ),
                isNotNull(property("ext", EnrollmentExtract.paragraph().order().commitDate()))));
//                eq(property("ext", EnrollmentExtract.paragraph().order().state().code()), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)));

        List<EnrollmentExtract> extracts = enrExtractBuilder.createStatement(session).list();
        Map<RequestedEnrollmentDirection, List<EnrollmentExtract>> extractMap = Maps.newHashMap();

        for (EnrollmentExtract extract : extracts)
        {
            RequestedEnrollmentDirection requestedEnrollmentDirection = extract.getEntity().getRequestedEnrollmentDirection();
            if(!extractMap.containsKey(requestedEnrollmentDirection))
                extractMap.put(requestedEnrollmentDirection, Lists.<EnrollmentExtract>newArrayList());
            if(!extractMap.get(requestedEnrollmentDirection).contains(extract))
                extractMap.get(requestedEnrollmentDirection).add(extract);
        }

        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections)
        {
            FormOU formOU = new FormOU(requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit(), requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getTerritorialOrgUnit());
            EnrollmentDirection direction = requestedEnrollmentDirection.getEnrollmentDirection();
            EntrantData entrantData = new EntrantData(requestedEnrollmentDirection, extractMap.get(requestedEnrollmentDirection));
            if (!result.containsKey(formOU))
                result.put(formOU, Maps.<EnrollmentDirection, List<EntrantData>>newHashMap());
            if (!result.get(formOU).containsKey(requestedEnrollmentDirection.getEnrollmentDirection()))
                result.get(formOU).put(direction, Lists.<EntrantData>newArrayList());
            if (!result.get(formOU).get(direction).contains(entrantData))
                result.get(formOU).get(direction).add(entrantData);
        }
        return result;
    }

    private MQBuilder getRequestedEnrollmentDirectionsMQBuilder()
    {

        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        directionBuilder.addJoinFetch("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        directionBuilder.addJoinFetch("request", EntrantRequest.L_ENTRANT, "entrant");
        directionBuilder.addJoinFetch("entrant", Entrant.L_PERSON, "person");
        directionBuilder.addJoinFetch("person", Person.L_IDENTITY_CARD, "identityCard");
        directionBuilder.addJoinFetch("r", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "enrollDir");
        directionBuilder.addJoinFetch("enrollDir", EnrollmentDirection.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE + "." + CompensationType.P_CODE, CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT));
        directionBuilder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, reportVO.getFrom(), reportVO.getTo()));
        directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));

        if (reportVO.isStudentCategoryActive() && !reportVO.getStudentCategoryList().isEmpty())
            directionBuilder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, reportVO.getStudentCategoryList()));
        if (FefuEcReportManager.ENROLLMENT_CAMP_STAGE_DOCUMENTS.equals(reportVO.getEnrollmentCampaignStage().getId()) )
        {
            directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_STATE + "." + EntrantState.P_CODE, EntrantStateCodes.TAKE_DOCUMENTS_AWAY));
        }
        else if (FefuEcReportManager.ENROLLMENT_CAMP_STAGE_EXAMS.equals(reportVO.getEnrollmentCampaignStage().getId()))
        {
            List<String> codes = Arrays.asList(
                    EntrantStateCodes.TAKE_DOCUMENTS_AWAY,
                    EntrantStateCodes.OUT_OF_COMPETITION,
                    EntrantStateCodes.ACTIVE
            );
            directionBuilder.add(MQExpression.notIn("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_STATE + "." + EntrantState.P_CODE, codes));
        }
        else if(FefuEcReportManager.ENROLLMENT_CAMP_STAGE_ENROLLMENT.equals(reportVO.getEnrollmentCampaignStage().getId()))
        {
            directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.L_STATE + "." + EntrantState.P_CODE, EntrantStateCodes.TAKE_DOCUMENTS_AWAY));
            directionBuilder.add(MQExpression.exists(new MQBuilder(PreliminaryEnrollmentStudent.ENTITY_CLASS, "p")
                    .add(MQExpression.eqProperty("r", RequestedEnrollmentDirection.id().s(), "p", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().id().s()))));
        }
        else
        {
            throw new RuntimeException("Unknown report stage!");
        }

        if (FefuEcReportManager.YES.equals(reportVO.getIncludeForeignPerson().getId()))
        {
            directionBuilder.add(MQExpression.notEq("identityCard", IdentityCard.citizenship().code(), IKladrDefines.RUSSIA_COUNTRY_CODE));
        }
        else
        {
            directionBuilder.add(MQExpression.eq("identityCard", IdentityCard.citizenship().code(), IKladrDefines.RUSSIA_COUNTRY_CODE));
        }
        if (FefuEcReportManager.YES.equals(reportVO.getContractCanceled().getId()))
        {
            directionBuilder.add(MQExpression.exists(new MQBuilder(FefuEntrantContract.ENTITY_CLASS, "c")
                    .add(MQExpression.eqProperty("r", RequestedEnrollmentDirection.id().s(), "c", FefuEntrantContract.requestedEnrollmentDirection().id().s()))
                    .add(MQExpression.eq("c", FefuEntrantContract.P_CONTRACT_TERMINATED, Boolean.TRUE))));
        }
        else
        {
            directionBuilder.add(MQExpression.exists(new MQBuilder(FefuEntrantContract.ENTITY_CLASS, "c")
                    .add(MQExpression.eqProperty("r", RequestedEnrollmentDirection.id().s(), "c", FefuEntrantContract.requestedEnrollmentDirection().id().s()))
                    .add(MQExpression.eq("c", FefuEntrantContract.P_CONTRACT_TERMINATED, Boolean.FALSE))
                    .add(MQExpression.or(
                            MQExpression.isNotNull("c", FefuEntrantContract.P_CONTRACT_NUMBER),
                            MQExpression.notEq("c", FefuEntrantContract.P_CONTRACT_NUMBER, "")
                    ))));
        }

        if (FefuEcReportManager.NO.equals(reportVO.getIncludeEnrolled().getId()))
        {
            directionBuilder.add(MQExpression.notEq("entrant", Entrant.state().code(), EntrantStateCodes.ENROLED));
        }

        if (reportVO.isQualificationActive() && !reportVO.getQualifications().isEmpty())
            directionBuilder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification(), reportVO.getQualifications()));
        if (reportVO.isFormativeOrgUnitActive() && !reportVO.getFormativeOrgUnitList().isEmpty())
            directionBuilder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.formativeOrgUnit(), reportVO.getFormativeOrgUnitList()));
        if (reportVO.isTerritorialOrgUnitActive() && !reportVO.getTerritorialOrgUnitList().isEmpty())
            directionBuilder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.territorialOrgUnit(), reportVO.getTerritorialOrgUnitList()));
        if (reportVO.isDevelopFormActive() && !reportVO.getDevelopFormList().isEmpty())
            directionBuilder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.developForm(), reportVO.getDevelopFormList()));
        if (reportVO.isDevelopConditionActive() && !reportVO.getDevelopConditionList().isEmpty())
            directionBuilder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.developCondition(), reportVO.getDevelopConditionList()));
        if (reportVO.isDevelopTechActive() && !reportVO.getDevelopTechList().isEmpty())
            directionBuilder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.developTech(), reportVO.getDevelopTechList()));
        if (reportVO.isDevelopPeriodActive() && !reportVO.getDevelopPeriodList().isEmpty())
            directionBuilder.add(MQExpression.in("educationOrgUnit", EducationOrgUnit.developPeriod(), reportVO.getDevelopPeriodList()));

        return directionBuilder;
    }


    private DQLSelectBuilder getRequestedEnrollmentDirectionsBuilder()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red");
        builder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().id()), value(reportVO.getEnrollmentCampaign().getId())));
        builder.where(betweenDays(RequestedEnrollmentDirection.regDate().fromAlias("red"), reportVO.getFrom(), reportVO.getTo()));
        if (reportVO.isStudentCategoryActive() && !reportVO.getStudentCategoryList().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.studentCategory().id()), EntityUtils.getIdsFromEntityList(reportVO.getStudentCategoryList())));
        builder.where(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().archival()), value(false)));
        builder.where(eq(property("red", RequestedEnrollmentDirection.compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)));
        if (FefuEcReportManager.ENROLLMENT_CAMP_STAGE_DOCUMENTS.equals(reportVO.getEnrollmentCampaignStage().getId()))
        {
            builder.where(not(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().state().code()), value(EntrantStateCodes.TAKE_DOCUMENTS_AWAY))));
        }
        else if (FefuEcReportManager.ENROLLMENT_CAMP_STAGE_EXAMS.equals(reportVO.getEnrollmentCampaignStage().getId()))
        {
            List<String> codes = Arrays.asList(
                    EntrantStateCodes.TAKE_DOCUMENTS_AWAY,
                    EntrantStateCodes.OUT_OF_COMPETITION,
                    EntrantStateCodes.ACTIVE
            );
            builder.where(notIn(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().state().code()), codes));
        }
        else if(FefuEcReportManager.ENROLLMENT_CAMP_STAGE_ENROLLMENT.equals(reportVO.getEnrollmentCampaignStage().getId()))
        {
            builder.where(not(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().state().code()), value(EntrantStateCodes.TAKE_DOCUMENTS_AWAY))));
            builder.where(exists(new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, "c")
                    .where(eq(
                            property("red", RequestedEnrollmentDirection.id()),
                            property("c", PreliminaryEnrollmentStudent.requestedEnrollmentDirection().id()))).buildQuery()));
        }
        else
        {
            throw new RuntimeException("Unknown report stage!");
        }
        if (FefuEcReportManager.YES.equals(reportVO.getIncludeForeignPerson().getId()))
        {
            builder.where(not(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE))));
        }
        else
        {
            builder.where(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
        }
        if (FefuEcReportManager.YES.equals(reportVO.getContractCanceled().getId()))
        {
            builder.where(exists(new DQLSelectBuilder().fromEntity(FefuEntrantContract.class, "c")
                    .where(eq(
                            property("red", RequestedEnrollmentDirection.id()),
                            property("c", FefuEntrantContract.requestedEnrollmentDirection().id())))
                    .where(eq(property("c", FefuEntrantContract.contractTerminated()), value(true)))
                    .buildQuery()));
        }
        else
        {
            builder.where(exists(new DQLSelectBuilder().fromEntity(FefuEntrantContract.class, "c")
                    .where(eq(
                            property("red", RequestedEnrollmentDirection.id()),
                            property("c", FefuEntrantContract.requestedEnrollmentDirection().id())))
                    .where(or(
                            isNotNull(property("c", FefuEntrantContract.contractNumber())),
                            not(eq(property("c", FefuEntrantContract.contractNumber()), value("")))))
                    .where(eq(property("c", FefuEntrantContract.contractTerminated()), value(false))).buildQuery()));
        }

        if (FefuEcReportManager.NO.equals(reportVO.getIncludeEnrolled().getId()))
            builder.where(ne(property("red", RequestedEnrollmentDirection.state().code()), value(EntrantStateCodes.ENROLED)));

        if (reportVO.isQualificationActive() && !reportVO.getQualifications().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification()), reportVO.getQualifications()));
        if (reportVO.isFormativeOrgUnitActive() && !reportVO.getFormativeOrgUnitList().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit()), reportVO.getFormativeOrgUnitList()));
        if (reportVO.isTerritorialOrgUnitActive() && !reportVO.getTerritorialOrgUnitList().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().territorialOrgUnit()), reportVO.getTerritorialOrgUnitList()));
        if (reportVO.isDevelopFormActive() && !reportVO.getDevelopFormList().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm()), reportVO.getDevelopFormList()));
        if (reportVO.isDevelopConditionActive() && !reportVO.getDevelopConditionList().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition()), reportVO.getDevelopConditionList()));
        if (reportVO.isDevelopTechActive() && !reportVO.getDevelopTechList().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developTech()), reportVO.getDevelopTechList()));
        if (reportVO.isDevelopPeriodActive() && !reportVO.getDevelopPeriodList().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developPeriod()), reportVO.getDevelopPeriodList()));

        return builder;
    }

    private class FormOU
    {
        private OrgUnit _formativeOrgUnit;
        private OrgUnit _territorialOrgUnit;

        private FormOU(OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit)
        {
            _formativeOrgUnit = formativeOrgUnit;
            _territorialOrgUnit = territorialOrgUnit;
        }

        private OrgUnit getFormativeOrgUnit()
        {
            return _formativeOrgUnit;
        }

        private void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
        {
            _formativeOrgUnit = formativeOrgUnit;
        }

        private OrgUnit getTerritorialOrgUnit()
        {
            return _territorialOrgUnit;
        }

        private void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
        {
            _territorialOrgUnit = territorialOrgUnit;
        }

        private String getTitle()
        {
            return _formativeOrgUnit.getTitle() + "(" + _territorialOrgUnit.getTerritorialTitle() + ")";
        }

        @Override
        public int hashCode()
        {
            return _formativeOrgUnit.hashCode() + _territorialOrgUnit.hashCode();
        }

        @Override
        public boolean equals(Object obj)
        {
            return _formativeOrgUnit.equals(((FormOU) obj).getFormativeOrgUnit()) && _territorialOrgUnit.equals(((FormOU) obj).getTerritorialOrgUnit());
        }
    }

    private class EntrantData
    {
        private Entrant _entrant;
        private RequestedEnrollmentDirection _requestedEnrollmentDirection;
        private FefuEntrantContract _contract;
        private Map<Discipline2RealizationWayRelation, Double> _markMap;
        private String _orders;

        public EntrantData(RequestedEnrollmentDirection requestedEnrollmentDirection, List<EnrollmentExtract> extracts)
        {
            _requestedEnrollmentDirection = requestedEnrollmentDirection;
            _entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
            _contract = fefuEntrantContractMap.get(_requestedEnrollmentDirection);
            _markMap = entrantDataUtil.getMarkMap(_requestedEnrollmentDirection);

            if(extracts != null && !extracts.isEmpty())
            {
                List<EnrollmentOrder> orders = Lists.newArrayList();
                for(EnrollmentExtract extract : extracts)
                {
                    if(!orders.contains(extract.getOrder()))
                        orders.add(extract.getOrder());
                }

                List<String> orderTitles = Lists.newArrayList();
                for(EnrollmentOrder order : orders)
                {
                    orderTitles.add("Приказ " + (order.getNumber() == null ? "" : " №" + order.getNumber()) + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCreateDate()));
                }
                _orders = StringUtils.join(orderTitles, ", ");
            }
        }

        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _requestedEnrollmentDirection;
        }

        public Entrant getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Entrant entrant)
        {
            _entrant = entrant;
        }

        public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            _requestedEnrollmentDirection = requestedEnrollmentDirection;
        }

        private FefuEntrantContract getContract()
        {
            return _contract;
        }

        private void setContract(FefuEntrantContract contract)
        {
            _contract = contract;
        }

        private Map<Discipline2RealizationWayRelation, Double> getMarkMap()
        {
            return _markMap;
        }

        private String getOrders()
        {
            return _orders;
        }
    }

    private class MainRow
    {
        private EntrantData _entrantData;
        private int numColumn;

        public MainRow(EntrantData entrantData, int i)
        {
            _entrantData = entrantData;
            numColumn = i;
        }

        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _entrantData.getRequestedEnrollmentDirection();
        }

        @SuppressWarnings("deprecation")
        String[] toStringArray()
        {
            List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structure = direction2disciplinesMap.get(_entrantData.getRequestedEnrollmentDirection().getEnrollmentDirection());
            int marks = 1;
            if (null != structure && structure.size() > 0)
                marks = structure.size();
            int columns = marks + 7;
            int i = 0;
            String[] result = new String[columns];
            result[i++] = String.valueOf(numColumn);
            result[i++] = _entrantData.getEntrant().getPersonalNumber();
            result[i++] = _entrantData.getEntrant().getPerson().getFullFio();

            if(null != structure)
            {
                if (structure.isEmpty())
                    i++;
                else
                {
                    Set<ChosenEntranceDiscipline> chosenSet = entrantDataUtil.getChosenEntranceDisciplineSet(_entrantData.getRequestedEnrollmentDirection());
                    ChosenEntranceDiscipline[] distribution = MarkDistributionUtil.getNearestDistribution(chosenSet, structure);
                    for (int k = 0; k < structure.size(); k++)
                    {
                        ChosenEntranceDiscipline disc = distribution[k];
                        Double mark = disc == null ? null : _entrantData.getMarkMap().get(disc.getEnrollmentCampaignDiscipline());
                        result[i++] = mark == null ? "x" : (mark == -1.0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark));
                    }
                }
            }
            else
            {
                i++;
            }

            result[i++] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(entrantDataUtil.getFinalMark(_entrantData.getRequestedEnrollmentDirection()));
            if (null != _entrantData.getContract())
                result[i++] = _entrantData.getContract().getContractNumber();
            else i++;
            result[i++] = _entrantData.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn() ? "п" : "к";
            String comment = "";
            if (null != _entrantData.getContract() && _entrantData.getContract().isContractTerminated())
                 comment += "Расторгнут";
            if(!StringUtils.isEmpty(_entrantData.getOrders()))
            {
                comment += StringUtils.isEmpty(comment) ? "" : ", ";
                comment += _entrantData.getOrders();
            }

            String compensationTypeStr = "";
            Boolean budjet = enrolledMap.get(_entrantData.getRequestedEnrollmentDirection().getId());
            if (budjet != null)
                compensationTypeStr = budjet ? "бюджет" : "договор";

            if (!StringUtils.isEmpty(compensationTypeStr))
            {
                comment += (StringUtils.isEmpty(comment) ? "" : ", ") + compensationTypeStr;
            }

            result[i++] = comment;
            return result;
        }
    }

    class TableRtfRowIntercepter extends RtfRowIntercepterBase
    {
        private Integer _parts;

        TableRtfRowIntercepter(Integer parts)
        {
            _parts = parts;
        }

        @Override
        public void beforeModify(RtfTable table, int currentRowIndex)
        {
            if (null != _parts)
            {// разбиваем колонку по количеству вступительных испытаний в наборе
                int[] parts = new int[_parts];
                Arrays.fill(parts, 1);

                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 3, (newCell, index) -> {
                    RtfString string = new RtfString();
                    string.append(String.valueOf(index + 1) + " экз.");
                    newCell.setElementList(string.toList());
                }, parts);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 3, null, parts);
            }
        }
    }

//    protected String getCategoryForDir(RequestedEnrollmentDirection requestedEnrollmentDirection)
//    {
//        if (reportVO.isIncludeEntrantTargetAdmission())
//        {
//            ExternalOrgUnit orgUnit = requestedEnrollmentDirection.getExternalOrgUnit();
//            if (orgUnit != null)
//            {
//                if(!StringUtils.isEmpty(orgUnit.getShortTitle()))
//                    return orgUnit.getShortTitle();
//                else
//                    return orgUnit.getTitle();
//            }
//            else
//                return "";
//        } else {
//            CompetitionKind competitionKind = requestedEnrollmentDirection.getCompetitionKind();
//            if (competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION)
//                    || competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
//                return competitionKind.getShortTitle();
//            else
//                return "";
//        }
//    }
}
