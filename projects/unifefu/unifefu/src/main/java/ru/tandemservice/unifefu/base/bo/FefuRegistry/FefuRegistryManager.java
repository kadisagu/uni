/* $Id: FefuRegistryManager.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.logic.FefuModuleLoadDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.logic.FefuRegistryDAO;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.logic.IFefuRegistryDAO;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 05.02.14
 * Time: 12:39
 */

@Configuration
public class FefuRegistryManager extends BusinessObjectManager
{
    public static FefuRegistryManager instance()
    {
        return instance(FefuRegistryManager.class);
    }

    @Bean
    public IFefuRegistryDAO dao()
    {
        return new FefuRegistryDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler fefuModuleLoadDSHandler()
    {
        return new FefuModuleLoadDSHandler(getName());
    }
}
