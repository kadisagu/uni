/* $Id$ */
package ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.Pub.TrBrsCoefficientPub;

/**
 * @author Nikolay Fedorovskih
 * @since 13.02.2014
 */
@Configuration
public class TrBrsCoefficientPubExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrBrsCoefficientPubExtUI.class.getSimpleName();

    @Autowired
    private TrBrsCoefficientPub _trBrsCoefficientPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trBrsCoefficientPub.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrBrsCoefficientPubExtUI.class))
                .create();
    }
}