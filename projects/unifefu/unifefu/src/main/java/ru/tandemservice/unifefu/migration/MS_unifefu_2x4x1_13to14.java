package ru.tandemservice.unifefu.migration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.unifefu.entity.ws.FefuStudentOrderExtension;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x1_13to14 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuScheduleBells

        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select ID, TITLE_P from FEFUSCHEDULEBELLS_T");
        ResultSet rs = stmt.getResultSet();

        Map<String, List<Long>> titleIdsMap = Maps.newHashMap();

        while (rs.next())
        {
            Long id = rs.getLong(1);
            String title = rs.getString(2);
            if(!titleIdsMap.containsKey(title))
                titleIdsMap.put(title, Lists.<Long>newArrayList());
            if(!titleIdsMap.get(title).contains(id))
                titleIdsMap.get(title).add(id);
        }

        PreparedStatement update = tool.prepareStatement("update FEFUSCHEDULEBELLS_T set TITLE_P=? where id=?");

        for(Map.Entry<String, List<Long>> entry : titleIdsMap.entrySet())
        {
            if(entry.getValue().size() > 1)
            {
                String title = entry.getKey();
                int i = 1;
                for(Long id : entry.getValue())
                {
                    update.setString(1, title + " (" + i++ + ")");
                    update.setLong(2, id);
                    update.executeUpdate();
                }
            }
        }

        update.close();
        stmt.close();

    }
}