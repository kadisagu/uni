package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;
import ru.tandemservice.unifefu.entity.gen.*;

/** @see ru.tandemservice.unifefu.entity.gen.FefuEpvRowCompetence2EppStateEduStandardRelGen */
public class FefuEpvRowCompetence2EppStateEduStandardRel extends FefuEpvRowCompetence2EppStateEduStandardRelGen
{
    public FefuEpvRowCompetence2EppStateEduStandardRel()
    {
    }

    public FefuEpvRowCompetence2EppStateEduStandardRel(FefuCompetence2EppStateEduStandardRel eduStandardRel, EppEpvRegistryRow registryRow)
    {
        setRegistryRow(registryRow);
        setEduStandardRel(eduStandardRel);
    }

    @Override
    public FefuCompetence getFefuCompetence()
    {
        return this.getEduStandardRel().getFefuCompetence();
    }

    @Override
    public String getGroupCode()
    {
        return getGroupShortTitle() + "-" + String.valueOf(getGroupNumber());
    }

    @Override
    public String getGroupShortTitle()
    {
        return getFefuCompetence().getEppSkillGroup().getShortTitle();
    }

    @Override
    public int getGroupNumber()
    {
        return getEduStandardRel().getGroupNumber();
    }
}