/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.logic;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author DMITRY KNYAZEV
 * @since 29.08.2014
 */
public class FefuSessionSlotMarkData implements ISessionMarkDAO.MarkData
{
	private Date _performDate;
	private Double _points;
	private SessionMarkCatalogItem _markValue;
	private String _comment;
	private final SessionDocumentSlot _documentSlot;
	private final ISelectModel _markModel;

	public FefuSessionSlotMarkData(@NotNull SessionDocumentSlot documentSlot, @NotNull ISelectModel markModel)
	{
		_documentSlot = documentSlot;
		_markModel = markModel;
	}

	public FefuSessionSlotMarkData(@NotNull SessionDocumentSlot documentSlot, @NotNull ISelectModel markModel, @Nullable SessionMark mark)
	{
		this(documentSlot, markModel);
		if (mark != null)
		{
			setPerformDate(mark.getPerformDate());
			setPoints(mark.getPoints());
			setMarkValue(mark.getValueItem());
			setComment(mark.getComment());

		}
	}

	@Override
	public Date getPerformDate()
	{
		return _performDate;
	}

	@Override
	public Double getPoints()
	{
		return _points;
	}

	@Override
	public SessionMarkCatalogItem getMarkValue()
	{
		return _markValue;
	}

	@Override
	public String getComment()
	{
		return _comment;
	}

	public void setPerformDate(Date performDate)
	{
		_performDate = performDate;
	}

	public void setPoints(Double points)
	{
		_points = points;
	}

	public void setMarkValue(SessionMarkCatalogItem markValue)
	{
		_markValue = markValue;
	}

	public void setComment(String comment)
	{
		_comment = comment;
	}

	public SessionDocumentSlot getDocumentSlot()
	{
		return _documentSlot;
	}

	public ISelectModel getMarkModel()
	{
		return _markModel;
	}
}