package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuNsiHumanRoleToContextRel

        // Если миграция выполнялась ранее в бранче, то забить.
        if(tool.columnExists("fefunsihumanroletocontextrel_t", "roleassignment_id")) return;

		// удалено свойство rolePrincipalContext
		{
			// почистить таблицу
			tool.executeUpdate("delete from fefunsihumanroletocontextrel_t");
            tool.dropColumn("fefunsihumanroletocontextrel_t", "roleprincipalcontext_id");
		}

		// создано обязательное свойство roleAssignment
		{
			// создать колонку
			tool.createColumn("fefunsihumanroletocontextrel_t", new DBColumn("roleassignment_id", DBType.LONG));

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefunsihumanroletocontextrel_t", "roleassignment_id", false);
		}
    }
}