package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О зачислении на полное государственное обеспечение»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FullStateMaintenanceEnrollmentStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract";
    public static final String ENTITY_NAME = "fullStateMaintenanceEnrollmentStuListExtract";
    public static final int VERSION_HASH = -900289271;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_ENROLL_DATE = "enrollDate";
    public static final String P_PAYMENT_END_DATE = "paymentEndDate";
    public static final String P_PROTOCOL_NUM_AND_DATE = "protocolNumAndDate";
    public static final String P_START_PAYMENT_PERIOD_DATE = "startPaymentPeriodDate";
    public static final String L_RESPONSIBLE_FOR_PAYMENTS = "responsibleForPayments";
    public static final String L_RESPONSIBLE_FOR_AGREEMENT = "responsibleForAgreement";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Date _enrollDate;     // Дата зачисления
    private Date _paymentEndDate;     // Дата окончания выплат
    private String _protocolNumAndDate;     // Протокол
    private Date _startPaymentPeriodDate;     // Начисление выплат за период с
    private EmployeePost _responsibleForPayments;     // Ответственный за начисление выплат
    private EmployeePost _responsibleForAgreement;     // Ответственный за согласование начислений выплат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Дата зачисления.
     */
    public Date getEnrollDate()
    {
        return _enrollDate;
    }

    /**
     * @param enrollDate Дата зачисления.
     */
    public void setEnrollDate(Date enrollDate)
    {
        dirty(_enrollDate, enrollDate);
        _enrollDate = enrollDate;
    }

    /**
     * @return Дата окончания выплат.
     */
    public Date getPaymentEndDate()
    {
        return _paymentEndDate;
    }

    /**
     * @param paymentEndDate Дата окончания выплат.
     */
    public void setPaymentEndDate(Date paymentEndDate)
    {
        dirty(_paymentEndDate, paymentEndDate);
        _paymentEndDate = paymentEndDate;
    }

    /**
     * @return Протокол. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getProtocolNumAndDate()
    {
        return _protocolNumAndDate;
    }

    /**
     * @param protocolNumAndDate Протокол. Свойство не может быть null.
     */
    public void setProtocolNumAndDate(String protocolNumAndDate)
    {
        dirty(_protocolNumAndDate, protocolNumAndDate);
        _protocolNumAndDate = protocolNumAndDate;
    }

    /**
     * @return Начисление выплат за период с.
     */
    public Date getStartPaymentPeriodDate()
    {
        return _startPaymentPeriodDate;
    }

    /**
     * @param startPaymentPeriodDate Начисление выплат за период с.
     */
    public void setStartPaymentPeriodDate(Date startPaymentPeriodDate)
    {
        dirty(_startPaymentPeriodDate, startPaymentPeriodDate);
        _startPaymentPeriodDate = startPaymentPeriodDate;
    }

    /**
     * @return Ответственный за начисление выплат.
     */
    public EmployeePost getResponsibleForPayments()
    {
        return _responsibleForPayments;
    }

    /**
     * @param responsibleForPayments Ответственный за начисление выплат.
     */
    public void setResponsibleForPayments(EmployeePost responsibleForPayments)
    {
        dirty(_responsibleForPayments, responsibleForPayments);
        _responsibleForPayments = responsibleForPayments;
    }

    /**
     * @return Ответственный за согласование начислений выплат.
     */
    public EmployeePost getResponsibleForAgreement()
    {
        return _responsibleForAgreement;
    }

    /**
     * @param responsibleForAgreement Ответственный за согласование начислений выплат.
     */
    public void setResponsibleForAgreement(EmployeePost responsibleForAgreement)
    {
        dirty(_responsibleForAgreement, responsibleForAgreement);
        _responsibleForAgreement = responsibleForAgreement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FullStateMaintenanceEnrollmentStuListExtractGen)
        {
            setCourse(((FullStateMaintenanceEnrollmentStuListExtract)another).getCourse());
            setGroup(((FullStateMaintenanceEnrollmentStuListExtract)another).getGroup());
            setCompensationType(((FullStateMaintenanceEnrollmentStuListExtract)another).getCompensationType());
            setEnrollDate(((FullStateMaintenanceEnrollmentStuListExtract)another).getEnrollDate());
            setPaymentEndDate(((FullStateMaintenanceEnrollmentStuListExtract)another).getPaymentEndDate());
            setProtocolNumAndDate(((FullStateMaintenanceEnrollmentStuListExtract)another).getProtocolNumAndDate());
            setStartPaymentPeriodDate(((FullStateMaintenanceEnrollmentStuListExtract)another).getStartPaymentPeriodDate());
            setResponsibleForPayments(((FullStateMaintenanceEnrollmentStuListExtract)another).getResponsibleForPayments());
            setResponsibleForAgreement(((FullStateMaintenanceEnrollmentStuListExtract)another).getResponsibleForAgreement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FullStateMaintenanceEnrollmentStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FullStateMaintenanceEnrollmentStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FullStateMaintenanceEnrollmentStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "enrollDate":
                    return obj.getEnrollDate();
                case "paymentEndDate":
                    return obj.getPaymentEndDate();
                case "protocolNumAndDate":
                    return obj.getProtocolNumAndDate();
                case "startPaymentPeriodDate":
                    return obj.getStartPaymentPeriodDate();
                case "responsibleForPayments":
                    return obj.getResponsibleForPayments();
                case "responsibleForAgreement":
                    return obj.getResponsibleForAgreement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "enrollDate":
                    obj.setEnrollDate((Date) value);
                    return;
                case "paymentEndDate":
                    obj.setPaymentEndDate((Date) value);
                    return;
                case "protocolNumAndDate":
                    obj.setProtocolNumAndDate((String) value);
                    return;
                case "startPaymentPeriodDate":
                    obj.setStartPaymentPeriodDate((Date) value);
                    return;
                case "responsibleForPayments":
                    obj.setResponsibleForPayments((EmployeePost) value);
                    return;
                case "responsibleForAgreement":
                    obj.setResponsibleForAgreement((EmployeePost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "enrollDate":
                        return true;
                case "paymentEndDate":
                        return true;
                case "protocolNumAndDate":
                        return true;
                case "startPaymentPeriodDate":
                        return true;
                case "responsibleForPayments":
                        return true;
                case "responsibleForAgreement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "enrollDate":
                    return true;
                case "paymentEndDate":
                    return true;
                case "protocolNumAndDate":
                    return true;
                case "startPaymentPeriodDate":
                    return true;
                case "responsibleForPayments":
                    return true;
                case "responsibleForAgreement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "enrollDate":
                    return Date.class;
                case "paymentEndDate":
                    return Date.class;
                case "protocolNumAndDate":
                    return String.class;
                case "startPaymentPeriodDate":
                    return Date.class;
                case "responsibleForPayments":
                    return EmployeePost.class;
                case "responsibleForAgreement":
                    return EmployeePost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FullStateMaintenanceEnrollmentStuListExtract> _dslPath = new Path<FullStateMaintenanceEnrollmentStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FullStateMaintenanceEnrollmentStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getEnrollDate()
     */
    public static PropertyPath<Date> enrollDate()
    {
        return _dslPath.enrollDate();
    }

    /**
     * @return Дата окончания выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getPaymentEndDate()
     */
    public static PropertyPath<Date> paymentEndDate()
    {
        return _dslPath.paymentEndDate();
    }

    /**
     * @return Протокол. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getProtocolNumAndDate()
     */
    public static PropertyPath<String> protocolNumAndDate()
    {
        return _dslPath.protocolNumAndDate();
    }

    /**
     * @return Начисление выплат за период с.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getStartPaymentPeriodDate()
     */
    public static PropertyPath<Date> startPaymentPeriodDate()
    {
        return _dslPath.startPaymentPeriodDate();
    }

    /**
     * @return Ответственный за начисление выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getResponsibleForPayments()
     */
    public static EmployeePost.Path<EmployeePost> responsibleForPayments()
    {
        return _dslPath.responsibleForPayments();
    }

    /**
     * @return Ответственный за согласование начислений выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getResponsibleForAgreement()
     */
    public static EmployeePost.Path<EmployeePost> responsibleForAgreement()
    {
        return _dslPath.responsibleForAgreement();
    }

    public static class Path<E extends FullStateMaintenanceEnrollmentStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Date> _enrollDate;
        private PropertyPath<Date> _paymentEndDate;
        private PropertyPath<String> _protocolNumAndDate;
        private PropertyPath<Date> _startPaymentPeriodDate;
        private EmployeePost.Path<EmployeePost> _responsibleForPayments;
        private EmployeePost.Path<EmployeePost> _responsibleForAgreement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getEnrollDate()
     */
        public PropertyPath<Date> enrollDate()
        {
            if(_enrollDate == null )
                _enrollDate = new PropertyPath<Date>(FullStateMaintenanceEnrollmentStuListExtractGen.P_ENROLL_DATE, this);
            return _enrollDate;
        }

    /**
     * @return Дата окончания выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getPaymentEndDate()
     */
        public PropertyPath<Date> paymentEndDate()
        {
            if(_paymentEndDate == null )
                _paymentEndDate = new PropertyPath<Date>(FullStateMaintenanceEnrollmentStuListExtractGen.P_PAYMENT_END_DATE, this);
            return _paymentEndDate;
        }

    /**
     * @return Протокол. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getProtocolNumAndDate()
     */
        public PropertyPath<String> protocolNumAndDate()
        {
            if(_protocolNumAndDate == null )
                _protocolNumAndDate = new PropertyPath<String>(FullStateMaintenanceEnrollmentStuListExtractGen.P_PROTOCOL_NUM_AND_DATE, this);
            return _protocolNumAndDate;
        }

    /**
     * @return Начисление выплат за период с.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getStartPaymentPeriodDate()
     */
        public PropertyPath<Date> startPaymentPeriodDate()
        {
            if(_startPaymentPeriodDate == null )
                _startPaymentPeriodDate = new PropertyPath<Date>(FullStateMaintenanceEnrollmentStuListExtractGen.P_START_PAYMENT_PERIOD_DATE, this);
            return _startPaymentPeriodDate;
        }

    /**
     * @return Ответственный за начисление выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getResponsibleForPayments()
     */
        public EmployeePost.Path<EmployeePost> responsibleForPayments()
        {
            if(_responsibleForPayments == null )
                _responsibleForPayments = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_FOR_PAYMENTS, this);
            return _responsibleForPayments;
        }

    /**
     * @return Ответственный за согласование начислений выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract#getResponsibleForAgreement()
     */
        public EmployeePost.Path<EmployeePost> responsibleForAgreement()
        {
            if(_responsibleForAgreement == null )
                _responsibleForAgreement = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_FOR_AGREEMENT, this);
            return _responsibleForAgreement;
        }

        public Class getEntityClass()
        {
            return FullStateMaintenanceEnrollmentStuListExtract.class;
        }

        public String getEntityName()
        {
            return "fullStateMaintenanceEnrollmentStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
