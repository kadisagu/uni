/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.DeleteNotCoordinatedOrders;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.commons.ExtractTypeSelectModel;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;

import java.util.*;

/**
 * @author Ekaterina Zvereva
 * @since 16.01.2015
 */
public class FefuSystemActionDeleteNotCoordinatedOrdersUI extends UIPresenter
{
    private Map<Long, Boolean> _selectedMap;
    private ISelectModel _ordersTypesModel;

    public Map<Long, Boolean> getSelectedMap()
    {
        return _selectedMap;
    }

    public void setSelectedMap(Map<Long, Boolean> selectedMap)
    {
        _selectedMap = selectedMap;
    }

    public ISelectModel getOrdersTypesModel()
    {
        return _ordersTypesModel;
    }

    public void setOrdersTypesModel(ISelectModel ordersTypesModel)
    {
        _ordersTypesModel = ordersTypesModel;
    }

    public boolean isOrderSelected()
    {
        PageableSearchListDataSource ordersDS = _uiConfig.getDataSource(FefuSystemActionDeleteNotCoordinatedOrders.ORDERS_DS);
        Long orderId = ((IEntity) ordersDS.getCurrent()).getId();

        if (_selectedMap.containsKey(orderId))
            return _selectedMap.get(orderId);

        return false;
    }

    public void setOrderSelected(boolean isSelected)
    {
        PageableSearchListDataSource ordersDS = _uiConfig.getDataSource(FefuSystemActionDeleteNotCoordinatedOrders.ORDERS_DS);
        Long orderId = ((IEntity) ordersDS.getCurrent()).getId();
        _selectedMap.put(orderId, isSelected);
    }

    @Override
    public void onComponentRefresh()
    {
        setOrdersTypesModel(new ExtractTypeSelectModel().hierarchical(true));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuSystemActionDeleteNotCoordinatedOrders.ORDERS_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "ordersDateFrom", "ordersDateTo", FefuSystemActionDeleteNotCoordinatedOrders.ORDERS_TYPES_DS));
        }
    }


    public void onClickDelete()
    {
        Collection selected = ((PageableSearchListDataSource) getConfig().getDataSource(FefuSystemActionDeleteNotCoordinatedOrders.ORDERS_DS)).getOptionColumnSelectedObjects("select");
        if (selected.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы один приказ.");

        FefuSystemActionManager.instance().dao().deleteNotCoordinatedOrders(new ArrayList<Long>(selected));

    }
}