/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleDaily.ui.PrintFormAdd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.base.ext.SppScheduleDaily.logic.FefuScheduleDailyComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.PrintFormAdd.SppScheduleDailyPrintFormAdd;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;

/**
 * @author Igor Belanov
 * @since 07.09.2016
 */
@Configuration
public class SppScheduleDailyPrintFormAddExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "fefu" + SppScheduleDailyPrintFormAddExtUI.class.getSimpleName();

    public static final String TERRITORIAL_OU_DS = "territorialOrgUnitDS";
    public static final String TERM_DS = "termDS";

    @Autowired
    private SppScheduleDailyPrintFormAdd _sppScheduleDailyPrintFormAdd;

    @Bean
    public PresenterExtension presenterExtension() {
        return presenterExtensionBuilder(_sppScheduleDailyPrintFormAdd.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, SppScheduleDailyPrintFormAddExtUI.class))
                .addDataSource(selectDS(TERRITORIAL_OU_DS, SppScheduleDailyManager.instance().sppGroupComboDSHandler()).addColumn("title", OrgUnit.territorialFullTitle().s()))
                .addDataSource(selectDS(TERM_DS, SppScheduleManager.instance().termComboDSHandler()))
                .replaceDataSource(selectDS(SppScheduleDailyPrintFormAdd.SEASON_DS, schedulesDailyComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleDailySeason>()
                {
                    @Override
                    public String format(SppScheduleDailySeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .replaceDataSource(selectDS(SppScheduleDailyPrintFormAdd.SCHEDULES_DS, schedulesDailyComboDSHandler()).addColumn("group", SppScheduleDaily.group().title().s()).addColumn("schedule", SppScheduleDaily.title().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler schedulesDailyComboDSHandler()
    {
        return new FefuScheduleDailyComboDSHandler(getName());
    }
}
