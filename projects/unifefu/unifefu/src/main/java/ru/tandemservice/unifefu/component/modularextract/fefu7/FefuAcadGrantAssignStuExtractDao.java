/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu7;

import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 9/5/12
 */
public class FefuAcadGrantAssignStuExtractDao extends UniBaseDao implements IExtractComponentDao<FefuAcadGrantAssignStuExtract>
{
    public void doCommit(FefuAcadGrantAssignStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);

        } else
        {
            extract.setPrevOrderDate(orderData.getAcadGrantAssignmentOrderDate());
            extract.setPrevOrderNumber(orderData.getAcadGrantAssignmentOrderNumber());
            extract.setPrevBeginDate(orderData.getAcadGrantPaymentDateFrom());
            extract.setPrevEndDate(orderData.getAcadGrantPaymentDateTo());
        }

        orderData.setAcadGrantAssignmentOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setAcadGrantAssignmentOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setAcadGrantPaymentDateFrom(extract.getBeginDate());
        orderData.setAcadGrantPaymentDateTo(extract.getEndDate());
        getSession().saveOrUpdate(orderData);

        //выплата
        FefuStudentPayment payment = new FefuStudentPayment();
        payment.setExtract(extract);
        payment.setStartDate(extract.getBeginDate());
        payment.setStopDate(extract.getEndDate());
        payment.setPaymentSum(extract.getGrantSize());
        if(null != extract.getReason())
            payment.setReason(extract.getReason().getTitle());
        save(payment);

        if(extract.isGroupManagerBonus())
        {
            FefuStudentPayment groupManagerPayment = new FefuStudentPayment();
            groupManagerPayment.setExtract(extract);
            groupManagerPayment.setStartDate(extract.getGroupManagerBonusBeginDate());
            groupManagerPayment.setStopDate(extract.getGroupManagerBonusEndDate());
            groupManagerPayment.setPaymentSum(extract.getGroupManagerBonusSize());
            if(null != extract.getReason())
                groupManagerPayment.setReason(extract.getReason().getTitle());
            groupManagerPayment.setComment("Надбавка старосте");
            save(groupManagerPayment);
        }
    }

    public void doRollback(FefuAcadGrantAssignStuExtract extract, Map parameters)
    {
        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }

        orderData.setAcadGrantAssignmentOrderDate(extract.getPrevOrderDate());
        orderData.setAcadGrantAssignmentOrderNumber(extract.getPrevOrderNumber());
        orderData.setAcadGrantPaymentDateFrom(extract.getPrevBeginDate());
        orderData.setAcadGrantPaymentDateTo(extract.getPrevEndDate());
        getSession().saveOrUpdate(orderData);

        // Удаляем выплату
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuStudentPayment.class);
        deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(FefuStudentPayment.extract()), DQLExpressions.value(extract)));
        deleteBuilder.createStatement(getSession()).execute();
     }
}