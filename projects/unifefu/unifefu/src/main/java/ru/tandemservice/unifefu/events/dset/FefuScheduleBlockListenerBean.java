package ru.tandemservice.unifefu.events.dset;

import com.google.common.collect.Lists;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class FefuScheduleBlockListenerBean implements IDSetEventListener
{
    private static boolean _jobEnable;

    public FefuScheduleBlockListenerBean()
    {
        _jobEnable = false;
    }

    public static void enable(boolean jobEnable)
    {
        _jobEnable = jobEnable;
    }

    @SuppressWarnings("unchecked")
    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeInsert, SppSchedule.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, SppSchedule.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, SppSchedule.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeInsert, SppScheduleSession.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, SppScheduleSession.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, SppScheduleSession.class, this);
    }

    @Override
    public void onEvent(DSetEvent event)
    {
        if(!_jobEnable) return;
        final Number count = getBuilder(event).column(DQLFunctions.count(property("x.id")))
            .createStatement(event.getContext())
            .uniqueResult();
        if ((null != count) && (count.intValue() > 0))
        {
            final List<Long> ids = getBuilder(event).column(property("x.id")).createStatement(event.getContext()).list();
            List<String> locks = Lists.newArrayList();
            final IEntityMeta entityMeta = event.getMultitude().getEntityMeta();
            for (Long id : ids)
            {
                if(entityMeta.getEntityClass().equals(SppSchedule.class))
                    locks.add("fefu.schedule.ws.export.lock_" + id);
                else if(entityMeta.getEntityClass().equals(SppScheduleSession.class))
                    locks.add("fefu.schedule.session.ws.export.lock_" + id);
            }
            NamedSyncInTransactionCheckLocker.register(event.getContext().getTransaction(), locks, 300);
        }
    }

    private DQLSelectBuilder getBuilder(DSetEvent event)
    {
        final IEntityMeta entityMeta = event.getMultitude().getEntityMeta();

        return new DQLSelectBuilder().fromEntity(entityMeta.getEntityClass(), "x")
                .where(in(property("x.id"), event.getMultitude().getInExpression()));
    }
}
