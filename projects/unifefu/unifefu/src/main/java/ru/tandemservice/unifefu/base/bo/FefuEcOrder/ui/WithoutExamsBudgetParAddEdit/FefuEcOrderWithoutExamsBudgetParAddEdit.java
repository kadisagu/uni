/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.WithoutExamsBudgetParAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;


/**
 * @author Nikolay Fedorovskih
 * @since 30.07.2013
 */
@Configuration
public class FefuEcOrderWithoutExamsBudgetParAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EcOrderManager.PRE_STUDENT_DS, EcOrderManager.instance().preStudentDS(), EcOrderManager.instance().preStudentDSHandler()))
                .create();
    }
}