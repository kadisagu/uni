package ru.tandemservice.unifefu.entity.catalog;

import ru.tandemservice.unifefu.entity.catalog.gen.*;

import java.util.Arrays;
import java.util.Collection;

/**
 * Виды деятельности, поощряемые дополнительной стипендией
 */
public class FefuPromotedActivityType extends FefuPromotedActivityTypeGen
{
    @Override
    public Collection<String> getDeclinableProperties() {
        return Arrays.asList(P_TITLE);
    }
}