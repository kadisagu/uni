package ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport.logic;

import ru.tandemservice.unifefu.entity.report.FefuListTrJournalDisciplinesReport;

import java.io.ByteArrayOutputStream;

/**
 * @author amakarova
 */
public interface IFefuListTrJournalDisciplinesReportDAO
{
    FefuListTrJournalDisciplinesReport createReport(FefuListTrJournalDisciplinesReportParams reportSettings);
}
