package ru.tandemservice.unifefu.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport;
import ru.tandemservice.unifefu.entity.report.FefuEntrantRequestReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Статистика зачисленных по приказам (ДВФУ, Форма №16)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEnrollmentOrderStatisticsReportGen extends FefuEntrantRequestReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport";
    public static final String ENTITY_NAME = "fefuEnrollmentOrderStatisticsReport";
    public static final int VERSION_HASH = 525746817;
    private static IEntityMeta ENTITY_META;

    public static final String P_STUDENT_CATEGORY_STR = "studentCategoryStr";
    public static final String P_QUALIFICATION_STR = "qualificationStr";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_FORMATIVE_ORG_UNIT_STR = "formativeOrgUnitStr";
    public static final String P_TERRITORIAL_ORG_UNIT_STR = "territorialOrgUnitStr";
    public static final String P_DIRECTION_STR = "directionStr";
    public static final String P_DEVELOP_FORM_STR = "developFormStr";
    public static final String P_DEVELOP_CONDITION_STR = "developConditionStr";
    public static final String P_DEVELOP_TECH_STR = "developTechStr";
    public static final String P_DEVELOP_PERIOD_STR = "developPeriodStr";
    public static final String P_INCLUDE_FOREIGN_PERSON = "includeForeignPerson";

    private String _studentCategoryStr;     // Категория поступающего
    private String _qualificationStr;     // Квалификация
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _formativeOrgUnitStr;     // Формирующее подр.
    private String _territorialOrgUnitStr;     // Территориальное подр.
    private String _directionStr;     // Направления подготовки (специальности)
    private String _developFormStr;     // Форма освоения
    private String _developConditionStr;     // Условие освоения
    private String _developTechStr;     // Технология освоения
    private String _developPeriodStr;     // Срок освоения
    private Boolean _includeForeignPerson;     // Учитывать иностранных граждан

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryStr()
    {
        return _studentCategoryStr;
    }

    /**
     * @param studentCategoryStr Категория поступающего.
     */
    public void setStudentCategoryStr(String studentCategoryStr)
    {
        dirty(_studentCategoryStr, studentCategoryStr);
        _studentCategoryStr = studentCategoryStr;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationStr()
    {
        return _qualificationStr;
    }

    /**
     * @param qualificationStr Квалификация.
     */
    public void setQualificationStr(String qualificationStr)
    {
        dirty(_qualificationStr, qualificationStr);
        _qualificationStr = qualificationStr;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnitStr()
    {
        return _formativeOrgUnitStr;
    }

    /**
     * @param formativeOrgUnitStr Формирующее подр..
     */
    public void setFormativeOrgUnitStr(String formativeOrgUnitStr)
    {
        dirty(_formativeOrgUnitStr, formativeOrgUnitStr);
        _formativeOrgUnitStr = formativeOrgUnitStr;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnitStr()
    {
        return _territorialOrgUnitStr;
    }

    /**
     * @param territorialOrgUnitStr Территориальное подр..
     */
    public void setTerritorialOrgUnitStr(String territorialOrgUnitStr)
    {
        dirty(_territorialOrgUnitStr, territorialOrgUnitStr);
        _territorialOrgUnitStr = territorialOrgUnitStr;
    }

    /**
     * @return Направления подготовки (специальности).
     */
    public String getDirectionStr()
    {
        return _directionStr;
    }

    /**
     * @param directionStr Направления подготовки (специальности).
     */
    public void setDirectionStr(String directionStr)
    {
        dirty(_directionStr, directionStr);
        _directionStr = directionStr;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormStr()
    {
        return _developFormStr;
    }

    /**
     * @param developFormStr Форма освоения.
     */
    public void setDevelopFormStr(String developFormStr)
    {
        dirty(_developFormStr, developFormStr);
        _developFormStr = developFormStr;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionStr()
    {
        return _developConditionStr;
    }

    /**
     * @param developConditionStr Условие освоения.
     */
    public void setDevelopConditionStr(String developConditionStr)
    {
        dirty(_developConditionStr, developConditionStr);
        _developConditionStr = developConditionStr;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTechStr()
    {
        return _developTechStr;
    }

    /**
     * @param developTechStr Технология освоения.
     */
    public void setDevelopTechStr(String developTechStr)
    {
        dirty(_developTechStr, developTechStr);
        _developTechStr = developTechStr;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriodStr()
    {
        return _developPeriodStr;
    }

    /**
     * @param developPeriodStr Срок освоения.
     */
    public void setDevelopPeriodStr(String developPeriodStr)
    {
        dirty(_developPeriodStr, developPeriodStr);
        _developPeriodStr = developPeriodStr;
    }

    /**
     * @return Учитывать иностранных граждан.
     */
    public Boolean getIncludeForeignPerson()
    {
        return _includeForeignPerson;
    }

    /**
     * @param includeForeignPerson Учитывать иностранных граждан.
     */
    public void setIncludeForeignPerson(Boolean includeForeignPerson)
    {
        dirty(_includeForeignPerson, includeForeignPerson);
        _includeForeignPerson = includeForeignPerson;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuEnrollmentOrderStatisticsReportGen)
        {
            setStudentCategoryStr(((FefuEnrollmentOrderStatisticsReport)another).getStudentCategoryStr());
            setQualificationStr(((FefuEnrollmentOrderStatisticsReport)another).getQualificationStr());
            setCompensationType(((FefuEnrollmentOrderStatisticsReport)another).getCompensationType());
            setFormativeOrgUnitStr(((FefuEnrollmentOrderStatisticsReport)another).getFormativeOrgUnitStr());
            setTerritorialOrgUnitStr(((FefuEnrollmentOrderStatisticsReport)another).getTerritorialOrgUnitStr());
            setDirectionStr(((FefuEnrollmentOrderStatisticsReport)another).getDirectionStr());
            setDevelopFormStr(((FefuEnrollmentOrderStatisticsReport)another).getDevelopFormStr());
            setDevelopConditionStr(((FefuEnrollmentOrderStatisticsReport)another).getDevelopConditionStr());
            setDevelopTechStr(((FefuEnrollmentOrderStatisticsReport)another).getDevelopTechStr());
            setDevelopPeriodStr(((FefuEnrollmentOrderStatisticsReport)another).getDevelopPeriodStr());
            setIncludeForeignPerson(((FefuEnrollmentOrderStatisticsReport)another).getIncludeForeignPerson());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEnrollmentOrderStatisticsReportGen> extends FefuEntrantRequestReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEnrollmentOrderStatisticsReport.class;
        }

        public T newInstance()
        {
            return (T) new FefuEnrollmentOrderStatisticsReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "studentCategoryStr":
                    return obj.getStudentCategoryStr();
                case "qualificationStr":
                    return obj.getQualificationStr();
                case "compensationType":
                    return obj.getCompensationType();
                case "formativeOrgUnitStr":
                    return obj.getFormativeOrgUnitStr();
                case "territorialOrgUnitStr":
                    return obj.getTerritorialOrgUnitStr();
                case "directionStr":
                    return obj.getDirectionStr();
                case "developFormStr":
                    return obj.getDevelopFormStr();
                case "developConditionStr":
                    return obj.getDevelopConditionStr();
                case "developTechStr":
                    return obj.getDevelopTechStr();
                case "developPeriodStr":
                    return obj.getDevelopPeriodStr();
                case "includeForeignPerson":
                    return obj.getIncludeForeignPerson();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "studentCategoryStr":
                    obj.setStudentCategoryStr((String) value);
                    return;
                case "qualificationStr":
                    obj.setQualificationStr((String) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "formativeOrgUnitStr":
                    obj.setFormativeOrgUnitStr((String) value);
                    return;
                case "territorialOrgUnitStr":
                    obj.setTerritorialOrgUnitStr((String) value);
                    return;
                case "directionStr":
                    obj.setDirectionStr((String) value);
                    return;
                case "developFormStr":
                    obj.setDevelopFormStr((String) value);
                    return;
                case "developConditionStr":
                    obj.setDevelopConditionStr((String) value);
                    return;
                case "developTechStr":
                    obj.setDevelopTechStr((String) value);
                    return;
                case "developPeriodStr":
                    obj.setDevelopPeriodStr((String) value);
                    return;
                case "includeForeignPerson":
                    obj.setIncludeForeignPerson((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentCategoryStr":
                        return true;
                case "qualificationStr":
                        return true;
                case "compensationType":
                        return true;
                case "formativeOrgUnitStr":
                        return true;
                case "territorialOrgUnitStr":
                        return true;
                case "directionStr":
                        return true;
                case "developFormStr":
                        return true;
                case "developConditionStr":
                        return true;
                case "developTechStr":
                        return true;
                case "developPeriodStr":
                        return true;
                case "includeForeignPerson":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentCategoryStr":
                    return true;
                case "qualificationStr":
                    return true;
                case "compensationType":
                    return true;
                case "formativeOrgUnitStr":
                    return true;
                case "territorialOrgUnitStr":
                    return true;
                case "directionStr":
                    return true;
                case "developFormStr":
                    return true;
                case "developConditionStr":
                    return true;
                case "developTechStr":
                    return true;
                case "developPeriodStr":
                    return true;
                case "includeForeignPerson":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "studentCategoryStr":
                    return String.class;
                case "qualificationStr":
                    return String.class;
                case "compensationType":
                    return CompensationType.class;
                case "formativeOrgUnitStr":
                    return String.class;
                case "territorialOrgUnitStr":
                    return String.class;
                case "directionStr":
                    return String.class;
                case "developFormStr":
                    return String.class;
                case "developConditionStr":
                    return String.class;
                case "developTechStr":
                    return String.class;
                case "developPeriodStr":
                    return String.class;
                case "includeForeignPerson":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEnrollmentOrderStatisticsReport> _dslPath = new Path<FefuEnrollmentOrderStatisticsReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEnrollmentOrderStatisticsReport");
    }
            

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getStudentCategoryStr()
     */
    public static PropertyPath<String> studentCategoryStr()
    {
        return _dslPath.studentCategoryStr();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getQualificationStr()
     */
    public static PropertyPath<String> qualificationStr()
    {
        return _dslPath.qualificationStr();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getFormativeOrgUnitStr()
     */
    public static PropertyPath<String> formativeOrgUnitStr()
    {
        return _dslPath.formativeOrgUnitStr();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getTerritorialOrgUnitStr()
     */
    public static PropertyPath<String> territorialOrgUnitStr()
    {
        return _dslPath.territorialOrgUnitStr();
    }

    /**
     * @return Направления подготовки (специальности).
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getDirectionStr()
     */
    public static PropertyPath<String> directionStr()
    {
        return _dslPath.directionStr();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getDevelopFormStr()
     */
    public static PropertyPath<String> developFormStr()
    {
        return _dslPath.developFormStr();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getDevelopConditionStr()
     */
    public static PropertyPath<String> developConditionStr()
    {
        return _dslPath.developConditionStr();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getDevelopTechStr()
     */
    public static PropertyPath<String> developTechStr()
    {
        return _dslPath.developTechStr();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getDevelopPeriodStr()
     */
    public static PropertyPath<String> developPeriodStr()
    {
        return _dslPath.developPeriodStr();
    }

    /**
     * @return Учитывать иностранных граждан.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getIncludeForeignPerson()
     */
    public static PropertyPath<Boolean> includeForeignPerson()
    {
        return _dslPath.includeForeignPerson();
    }

    public static class Path<E extends FefuEnrollmentOrderStatisticsReport> extends FefuEntrantRequestReport.Path<E>
    {
        private PropertyPath<String> _studentCategoryStr;
        private PropertyPath<String> _qualificationStr;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _formativeOrgUnitStr;
        private PropertyPath<String> _territorialOrgUnitStr;
        private PropertyPath<String> _directionStr;
        private PropertyPath<String> _developFormStr;
        private PropertyPath<String> _developConditionStr;
        private PropertyPath<String> _developTechStr;
        private PropertyPath<String> _developPeriodStr;
        private PropertyPath<Boolean> _includeForeignPerson;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getStudentCategoryStr()
     */
        public PropertyPath<String> studentCategoryStr()
        {
            if(_studentCategoryStr == null )
                _studentCategoryStr = new PropertyPath<String>(FefuEnrollmentOrderStatisticsReportGen.P_STUDENT_CATEGORY_STR, this);
            return _studentCategoryStr;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getQualificationStr()
     */
        public PropertyPath<String> qualificationStr()
        {
            if(_qualificationStr == null )
                _qualificationStr = new PropertyPath<String>(FefuEnrollmentOrderStatisticsReportGen.P_QUALIFICATION_STR, this);
            return _qualificationStr;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getFormativeOrgUnitStr()
     */
        public PropertyPath<String> formativeOrgUnitStr()
        {
            if(_formativeOrgUnitStr == null )
                _formativeOrgUnitStr = new PropertyPath<String>(FefuEnrollmentOrderStatisticsReportGen.P_FORMATIVE_ORG_UNIT_STR, this);
            return _formativeOrgUnitStr;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getTerritorialOrgUnitStr()
     */
        public PropertyPath<String> territorialOrgUnitStr()
        {
            if(_territorialOrgUnitStr == null )
                _territorialOrgUnitStr = new PropertyPath<String>(FefuEnrollmentOrderStatisticsReportGen.P_TERRITORIAL_ORG_UNIT_STR, this);
            return _territorialOrgUnitStr;
        }

    /**
     * @return Направления подготовки (специальности).
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getDirectionStr()
     */
        public PropertyPath<String> directionStr()
        {
            if(_directionStr == null )
                _directionStr = new PropertyPath<String>(FefuEnrollmentOrderStatisticsReportGen.P_DIRECTION_STR, this);
            return _directionStr;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getDevelopFormStr()
     */
        public PropertyPath<String> developFormStr()
        {
            if(_developFormStr == null )
                _developFormStr = new PropertyPath<String>(FefuEnrollmentOrderStatisticsReportGen.P_DEVELOP_FORM_STR, this);
            return _developFormStr;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getDevelopConditionStr()
     */
        public PropertyPath<String> developConditionStr()
        {
            if(_developConditionStr == null )
                _developConditionStr = new PropertyPath<String>(FefuEnrollmentOrderStatisticsReportGen.P_DEVELOP_CONDITION_STR, this);
            return _developConditionStr;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getDevelopTechStr()
     */
        public PropertyPath<String> developTechStr()
        {
            if(_developTechStr == null )
                _developTechStr = new PropertyPath<String>(FefuEnrollmentOrderStatisticsReportGen.P_DEVELOP_TECH_STR, this);
            return _developTechStr;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getDevelopPeriodStr()
     */
        public PropertyPath<String> developPeriodStr()
        {
            if(_developPeriodStr == null )
                _developPeriodStr = new PropertyPath<String>(FefuEnrollmentOrderStatisticsReportGen.P_DEVELOP_PERIOD_STR, this);
            return _developPeriodStr;
        }

    /**
     * @return Учитывать иностранных граждан.
     * @see ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport#getIncludeForeignPerson()
     */
        public PropertyPath<Boolean> includeForeignPerson()
        {
            if(_includeForeignPerson == null )
                _includeForeignPerson = new PropertyPath<Boolean>(FefuEnrollmentOrderStatisticsReportGen.P_INCLUDE_FOREIGN_PERSON, this);
            return _includeForeignPerson;
        }

        public Class getEntityClass()
        {
            return FefuEnrollmentOrderStatisticsReport.class;
        }

        public String getEntityName()
        {
            return "fefuEnrollmentOrderStatisticsReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
