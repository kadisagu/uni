/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu5.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.unifefu.entity.FefuTransferDevFormStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 29.08.2012
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuTransferDevFormStuExtract, IDAO, Model>
{
    public void onChangeGroup(IBusinessComponent component)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().handleGroupChange(getModel(component).getEduModel());
    }
}