package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Неделя учебного графика (ДВФУ)
 */
public class FefuEduPlanVersionWeek extends FefuEduPlanVersionWeekGen
{
    public FefuEduPlanVersionWeek()
    {
    }

    public FefuEduPlanVersionWeek(EppEduPlanVersion version, Course course, EppWeek week)
    {
        this.setVersion(version);
        this.setCourse(course);
        this.setWeek(week);
    }
}