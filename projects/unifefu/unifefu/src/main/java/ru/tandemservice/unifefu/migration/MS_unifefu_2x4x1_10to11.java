package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.unifefu.entity.ws.FefuStudentOrderExtension;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 24.01.2014
 */
public class MS_unifefu_2x4x1_10to11 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select ID, OPERATIONTYPE_P, DIRECTUMID_P, ORDER_ID from FEFUDIRECTUMLOG_T where ORDER_ID is not null and ORDEREXT_ID is null");
        ResultSet rs = stmt.getResultSet();

        //Map<Long, String> orderIdToDirectumIdMap = new HashMap<>();
        Map<Long, Long> orderIdToExtIdMap = new HashMap<>();
        List<Wrapper> wrappers = new ArrayList<>();

        short discriminator = tool.entityCodes().get(FefuStudentOrderExtension.ENTITY_NAME);
        PreparedStatement insert = tool.prepareStatement("insert into FEFUSTUDENTORDEREXTENSION_T (id, discriminator, order_id, directumid_p, directumnumber_p, directumdate_p, orderid_p) values(?, ?, ?, ?, ?, ?, ?)");

        while (rs.next())
        {
            Long id = rs.getLong(1);
            String operationType = rs.getString(2);
            String directumId = rs.getString(3);
            Long orderId = rs.getLong(4);

            wrappers.add(new Wrapper(id, orderId));

            if (null != operationType && operationType.contains("задач"))
            {
                //orderIdToDirectumIdMap.put(orderId, directumId);
                Long extId = EntityIDGenerator.generateNewId(discriminator);
                insert.setLong(1, extId);
                insert.setShort(2, discriminator);
                insert.setLong(3, orderId);
                insert.setString(4, directumId);
                insert.setString(5, null);
                insert.setDate(6, null);
                insert.setLong(7, orderId);
                insert.executeUpdate();
                orderIdToExtIdMap.put(orderId, extId);
            }
        }

        PreparedStatement update = tool.prepareStatement("update FEFUDIRECTUMLOG_T set ORDEREXT_ID=? where id=?");
        for (Wrapper wrapper : wrappers)
        {
            Long extId = orderIdToExtIdMap.get(wrapper.getOrderId());
            if (null != extId)
            {
                update.setLong(1, extId);
                update.setLong(2, wrapper.getId());
                update.executeUpdate();
            }
        }

        insert.close();
        update.close();
        stmt.close();
    }

    private class Wrapper
    {
        private Long _id;
        private Long _orderId;

        private Wrapper(Long id, Long orderId)
        {
            _id = id;
            _orderId = orderId;
        }

        private Long getId()
        {
            return _id;
        }

        private Long getOrderId()
        {
            return _orderId;
        }
    }
}