/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.ImtsaReImport;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.base.ext.SystemAction.ui.Pub.FefuSystemActionPubAddon;

import java.util.Collection;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
public class FefuEduPlanImtsaReImportUI extends UIPresenter
{
    public void onClickReImportImtsa()
    {
        Collection<IEntity> selected = ((BaseSearchListDataSource) getConfig().getDataSource(FefuEduPlanImtsaReImport.DS_EDU_PLAN_VERSION)).getOptionColumnSelectedObjects("selected");
        if (!"ok".equals(_uiSupport.getClientParameter()))
        {
            if (selected.isEmpty())
                throw new ApplicationException("Нужно отметить не меньше одной версии УП.");

            String confirmStr = getConfig().getProperty("ui.reImportImtsaMessage");
            ConfirmInfo confirm = new ConfirmInfo(confirmStr,
                    new ClickButtonAction("reImportImtsa", "ok", false));
            TapSupportUtils.displayConfirm(confirm);

            return;

        }

        Collection<Long> versionIds = CommonDAO.ids(selected);
        Collection<Long> blockIds = CommonDAO.ids(FefuEduPlanManager.instance().dao().getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion().id(), versionIds));

        FefuSystemActionPubAddon.doReImportImtsa(blockIds, true);
    }

    public void onClickSearch()
    {
        _uiSettings.save();
    }

    public void onClickClear()
    {
        _uiSettings.clear();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(FefuEduPlanImtsaReImport.DS_EDU_PLAN_VERSION))
        {
            dataSource.putAll(getSettings().getAsMap(
                    FefuEduPlanImtsaReImport.BIND_NUMBER,
                    FefuEduPlanImtsaReImport.BIND_PROGRAM_SUBJECT,
                    FefuEduPlanImtsaReImport.BIND_PROGRAM_FORM,
                    FefuEduPlanImtsaReImport.BIND_DEVELOP_CONDITION,
                    FefuEduPlanImtsaReImport.BIND_PROGRAM_TRAIT,
                    FefuEduPlanImtsaReImport.BIND_DEVELOP_GRID
            ));
        }
    }
}