/* $Id$ */
package ru.tandemservice.unifefu.component.menu.OnlineEntrantList;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt;
import ru.tandemservice.unifefu.utils.SelectUtils;

/**
 * @author Nikolay Fedorovskih
 * @since 01.07.2013
 */
public class DAO extends ru.tandemservice.uniec.component.menu.OnlineEntrantList.DAO
{
    @Override
    public void prepare(ru.tandemservice.uniec.component.menu.OnlineEntrantList.Model model)
    {
        super.prepare(model);
        ((Model) model).setYesNoSelect(SelectUtils.getYesNoSelectModel());
    }

    @Override
    protected MQBuilder createDataSourceBuilder(ru.tandemservice.uniec.component.menu.OnlineEntrantList.Model model)
    {
        MQBuilder builder = super.createDataSourceBuilder(model);
        IDataSettings settings = model.getSettings();

        Boolean requestRegistered = SelectUtils.getBooleanFromYesNoSelectValue(settings.get("requestRegistered"));
        Boolean hasAttachment = SelectUtils.getBooleanFromYesNoSelectValue(settings.get("hasAttachment"));

        if (requestRegistered != null)
        {
            builder.add(requestRegistered ?
                                MQExpression.isNotNull("onlineEntrant", OnlineEntrant.entrant().s()) :
                                MQExpression.isNull("onlineEntrant", OnlineEntrant.entrant().s()));
        }

        if (hasAttachment != null)
        {
            builder.add(hasAttachment ?
                                MQExpression.isNotNull("onlineEntrant", OnlineEntrant.documentCopies().s()) :
                                MQExpression.isNull("onlineEntrant", OnlineEntrant.documentCopies().s()));
        }

        return builder;
    }

    @Override
    public void prepareListDataSource(ru.tandemservice.uniec.component.menu.OnlineEntrantList.Model model)
    {
        super.prepareListDataSource(model);

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            OnlineEntrant entrant = (OnlineEntrant) wrapper.getEntity();
            OnlineEntrantFefuExt entrantExt = DataAccessServices.dao().get(OnlineEntrantFefuExt.class, OnlineEntrantFefuExt.onlineEntrant(), entrant);
            if (entrantExt != null)
                wrapper.setViewProperty(OnlineEntrantFefuExt.P_COMMENT, entrantExt.getComment());
        }
    }
}