/* $Id$ */
package ru.tandemservice.unifefu.events.nsi;

import org.hibernate.Session;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiContactType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact;
import ru.tandemservice.unifefu.ws.nsi.reactor.ContactTypeReactor;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry V. Seleznev
 * @since 09.01.2015
 */
public class ContactDataListener extends ParamTransactionCompleteListener<Boolean>
{
    public static Set<Long> JUST_CHANGED_CONTACT_ID_SET = new HashSet<>();

    @SuppressWarnings("unchecked")
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, PersonContactData.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, PersonContactData.class, this);
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        Set<Long> preProcessedContactIdList = new HashSet<>();
        for (Long id : params)
        {
            if (!JUST_CHANGED_CONTACT_ID_SET.contains(id)) preProcessedContactIdList.add(id);
            JUST_CHANGED_CONTACT_ID_SET.remove(id);
        }

        if (preProcessedContactIdList.isEmpty()) return true;

        FefuNsiContactType mobileType = DataAccessServices.dao().get(FefuNsiContactType.class, FefuNsiContactType.userCode(), ContactTypeReactor.CONTACT_KIND_CODE_MOBILE_PHONE);
        FefuNsiContactType phoneType = DataAccessServices.dao().get(FefuNsiContactType.class, FefuNsiContactType.userCode(), ContactTypeReactor.CONTACT_KIND_CODE_PHONE);
        FefuNsiContactType workType = DataAccessServices.dao().get(FefuNsiContactType.class, FefuNsiContactType.userCode(), ContactTypeReactor.CONTACT_KIND_CODE_WORK_PHONE);

        if (null == mobileType || null == phoneType || null == workType)
            return true; // Похоже, что не синхронизирован справочник видов контактной информации.

        Map<Long, Person> dataIdToPersonMap = new HashMap<>();

        List<Object[]> personDataList = new DQLSelectBuilder().fromEntity(Person.class, "e")
                .column(property(Person.contactData().id().fromAlias("e"))).column(property("e"))
                .where(in(property(Person.contactData().id().fromAlias("e")), preProcessedContactIdList)).createStatement(session).list();

        for (Object[] item : personDataList)
        {
            dataIdToPersonMap.put((Long) item[0], (Person) item[1]);
        }

        List<FefuNsiPersonContact> contactDataList = new DQLSelectBuilder().fromEntity(FefuNsiPersonContact.class, "e").column(property("e"))
                .where(in(property(FefuNsiPersonContact.person().contactData().id().fromAlias("e")), preProcessedContactIdList))
                .where(isNull(property(FefuNsiPersonContact.address().fromAlias("e")))).createStatement(session).list();

        Map<Long, Map<String, FefuNsiPersonContact>> contactsMap = new HashMap<>();
        for (FefuNsiPersonContact contact : contactDataList)
        {
            Map<String, FefuNsiPersonContact> perContactsMap = contactsMap.get(contact.getPerson().getId());
            if (null == perContactsMap) perContactsMap = new HashMap<>();
            perContactsMap.put(String.valueOf(contact.getType().getId()), contact);
            if (null != contact.getField()) perContactsMap.put(contact.getField(), contact);
            contactsMap.put(contact.getPerson().getId(), perContactsMap);
        }

        List<Person> perList = new DQLSelectBuilder().fromEntity(Person.class, "e").column(property("e"))
                .where(in(property(Person.contactData().id().fromAlias("e")), preProcessedContactIdList)).createStatement(session).list();

        Set<Long> contactsToDelete = new HashSet<>();
        for (Person person : perList)
        {
            PersonContactData perContData = person.getContactData();
            Map<String, FefuNsiPersonContact> contactMap = contactsMap.get(person.getId());
            if (null == contactMap) contactMap = new HashMap<>();
            contactsMap.put(person.getId(), contactMap);

            // Формируем список на удаление утративших актуальность контактных данных НСИ
            if (null == perContData.getPhoneReg() && null != contactMap.get(PersonContactData.P_PHONE_REG))
            {
                contactsToDelete.add(contactMap.get(PersonContactData.P_PHONE_REG).getId());
                contactMap.remove(PersonContactData.P_PHONE_REG);
            }
            if (null == perContData.getPhoneRegTemp() && null != contactMap.get(PersonContactData.P_PHONE_REG_TEMP))
            {
                contactsToDelete.add(contactMap.get(PersonContactData.P_PHONE_REG_TEMP).getId());
                contactMap.remove(PersonContactData.P_PHONE_REG_TEMP);
            }
            if (null == perContData.getPhoneFact() && null != contactMap.get(PersonContactData.P_PHONE_FACT))
            {
                contactsToDelete.add(contactMap.get(PersonContactData.P_PHONE_FACT).getId());
                contactMap.remove(PersonContactData.P_PHONE_FACT);
            }
            if (null == perContData.getPhoneDefault() && null != contactMap.get(PersonContactData.P_PHONE_DEFAULT))
            {
                contactsToDelete.add(contactMap.get(PersonContactData.P_PHONE_DEFAULT).getId());
                contactMap.remove(PersonContactData.P_PHONE_DEFAULT);
            }
            if (null == perContData.getPhoneWork() && null != contactMap.get(PersonContactData.P_PHONE_WORK))
            {
                contactsToDelete.add(contactMap.get(PersonContactData.P_PHONE_WORK).getId());
                contactMap.remove(PersonContactData.P_PHONE_WORK);
            }
            if (null == perContData.getPhoneMobile() && null != contactMap.get(PersonContactData.P_PHONE_MOBILE))
            {
                contactsToDelete.add(contactMap.get(PersonContactData.P_PHONE_MOBILE).getId());
                contactMap.remove(PersonContactData.P_PHONE_MOBILE);
            }
            if (null == perContData.getPhoneRelatives() && null != contactMap.get(PersonContactData.P_PHONE_RELATIVES))
            {
                contactsToDelete.add(contactMap.get(PersonContactData.P_PHONE_RELATIVES).getId());
                contactMap.remove(PersonContactData.P_PHONE_RELATIVES);
            }

            // Обновляем существующие контакты НСИ
            if (null != perContData.getPhoneReg() && null != contactMap.get(PersonContactData.P_PHONE_REG))
                contactMap.get(PersonContactData.P_PHONE_REG).update(perContData.getPhoneReg());
            if (null != perContData.getPhoneRegTemp() && null != contactMap.get(PersonContactData.P_PHONE_REG_TEMP))
                contactMap.get(PersonContactData.P_PHONE_REG_TEMP).update(perContData.getPhoneRegTemp());
            if (null != perContData.getPhoneFact() && null != contactMap.get(PersonContactData.P_PHONE_FACT))
                contactMap.get(PersonContactData.P_PHONE_FACT).update(perContData.getPhoneFact());
            if (null != perContData.getPhoneDefault() && null != contactMap.get(PersonContactData.P_PHONE_DEFAULT))
                contactMap.get(PersonContactData.P_PHONE_DEFAULT).update(perContData.getPhoneDefault());
            if (null != perContData.getPhoneWork() && null != contactMap.get(PersonContactData.P_PHONE_WORK))
                contactMap.get(PersonContactData.P_PHONE_WORK).update(perContData.getPhoneWork());
            if (null != perContData.getPhoneMobile() && null != contactMap.get(PersonContactData.P_PHONE_MOBILE))
                contactMap.get(PersonContactData.P_PHONE_MOBILE).update(perContData.getPhoneMobile());
            if (null != perContData.getPhoneRelatives() && null != contactMap.get(PersonContactData.P_PHONE_RELATIVES))
                contactMap.get(PersonContactData.P_PHONE_RELATIVES).update(perContData.getPhoneRelatives());

            // Создаём недостающие контакты НСИ
            if (null != perContData.getPhoneReg() && null == contactMap.get(PersonContactData.P_PHONE_REG))
                contactMap.put(PersonContactData.P_PHONE_REG, new FefuNsiPersonContact(person, phoneType, PersonContactData.P_PHONE_REG, perContData.getPhoneReg(), "Телефон по месту постоянной регистрации"));
            if (null != perContData.getPhoneRegTemp() && null == contactMap.get(PersonContactData.P_PHONE_REG_TEMP))
                contactMap.put(PersonContactData.P_PHONE_REG_TEMP, new FefuNsiPersonContact(person, phoneType, PersonContactData.P_PHONE_REG_TEMP, perContData.getPhoneRegTemp(), "Телефон по месту временной регистрации"));
            if (null != perContData.getPhoneFact() && null == contactMap.get(PersonContactData.P_PHONE_FACT))
                contactMap.put(PersonContactData.P_PHONE_FACT, new FefuNsiPersonContact(person, phoneType, PersonContactData.P_PHONE_FACT, perContData.getPhoneFact(), "Телефон по месту фактического проживания"));
            if (null != perContData.getPhoneDefault() && null == contactMap.get(PersonContactData.P_PHONE_DEFAULT))
                contactMap.put(PersonContactData.P_PHONE_DEFAULT, new FefuNsiPersonContact(person, phoneType, PersonContactData.P_PHONE_DEFAULT, perContData.getPhoneDefault(), "Контактный телефон"));
            if (null != perContData.getPhoneWork() && null == contactMap.get(PersonContactData.P_PHONE_WORK))
                contactMap.put(PersonContactData.P_PHONE_WORK, new FefuNsiPersonContact(person, workType, PersonContactData.P_PHONE_WORK, perContData.getPhoneWork(), "Рабочий телефон"));
            if (null != perContData.getPhoneMobile() && null == contactMap.get(PersonContactData.P_PHONE_MOBILE))
                contactMap.put(PersonContactData.P_PHONE_MOBILE, new FefuNsiPersonContact(person, mobileType, PersonContactData.P_PHONE_MOBILE, perContData.getPhoneMobile(), "Мобильный телефон"));
            if (null != perContData.getPhoneRelatives() && null == contactMap.get(PersonContactData.P_PHONE_RELATIVES))
                contactMap.put(PersonContactData.P_PHONE_RELATIVES, new FefuNsiPersonContact(person, phoneType, PersonContactData.P_PHONE_RELATIVES, perContData.getPhoneRelatives(), "Телефон родственников"));
        }

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuNsiPersonContact.class);
        deleteBuilder.where(in(property(FefuNsiPersonContact.id()), contactsToDelete));
        deleteBuilder.createStatement(session).execute();

        for (Map<String, FefuNsiPersonContact> item : contactsMap.values())
        {
            for (FefuNsiPersonContact contact : item.values())
            {
                session.saveOrUpdate(contact);

                // Вносим идентификатор сущности в сет для листенера, обрабатывающего изменения контактов НСИ, дабы избежать зацикливания листенеров
                NsiPersonContactListener.JUST_CHANGED_CONTACT_ID_SET.add(contact.getId());
            }
        }

        session.flush();

        return true;
    }
}