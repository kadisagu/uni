package ru.tandemservice.unifefu.events.dset;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.unifefu.dao.daemon.FEFUAllStudExportDaemonDAO;
import ru.tandemservice.unifefu.dao.daemon.IFEFUAllStudExportDaemonDAO;
import ru.tandemservice.unifefu.entity.ws.MdbViewPersonEduinstitution;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vnekrasov
 */
public class FefuPersonEduInstitutionListener extends ParamTransactionCompleteListener<Boolean>
{

    @SuppressWarnings("unchecked")
    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, PersonEduInstitution.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, PersonEduInstitution.class, this);

    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(MdbViewPersonEduinstitution.class);
        deleteBuilder.where(in(property(MdbViewPersonEduinstitution.personEduInstitution().id()), params));
        deleteBuilder.createStatement(session).execute();

        IFEFUAllStudExportDaemonDAO.instance.get().doRegisterEntity(MdbViewPersonEduinstitution.ENTITY_NAME, params);
        FEFUAllStudExportDaemonDAO.DAEMON.registerAfterCompleteWakeUp(session);
        return true;
    }

    @Override
    @Transactional(readOnly = false)
    public void afterCompletion(Session session, int status, Collection<Long> params, Boolean beforeCompletionResult)
    {


    }

}
