package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параметры дополнительных проверок ГОС (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEppStateEduStandardAdditionalChecksGen extends EntityBase
 implements INaturalIdentifiable<FefuEppStateEduStandardAdditionalChecksGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks";
    public static final String ENTITY_NAME = "fefuEppStateEduStandardAdditionalChecks";
    public static final int VERSION_HASH = -816260528;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_STANDARD = "eduStandard";
    public static final String P_ALT_IN_CHOICE_DISCIPLINES = "altInChoiceDisciplines";
    public static final String P_CONTROL_ACTION_ON_TOTAL_REG_ELEMENT = "controlActionOnTotalRegElement";
    public static final String P_FORMING_COMPETENCE_REG_ELEMENT = "formingCompetenceRegElement";
    public static final String P_INTEGRALITY_LABOR_PART_YEAR = "integralityLaborPartYear";
    public static final String P_EVEN_AUDIT_HOURS = "evenAuditHours";
    public static final String P_MULTIPLE_AUDIT_HOURS_PART_YEAR = "multipleAuditHoursPartYear";
    public static final String P_BIND_EDU_PLAN_AND_FIX_OWNER = "bindEduPlanAndFixOwner";
    public static final String P_EDU_PLAN_CONFORMITY_SCHEDULE_IN_PRACTICES = "eduPlanConformityScheduleInPractices";
    public static final String P_SELF_WORK_FOR_ANY_DISCIPLINE = "selfWorkForAnyDiscipline";
    public static final String P_DUPLICATE_DISCIPLINES_TITLES = "duplicateDisciplinesTitles";
    public static final String P_REG_ELEMENT_CONFORMITY_EPV = "regElementConformityEpv";

    private EppStateEduStandard _eduStandard;     // Государственный образовательный стандарт
    private boolean _altInChoiceDisciplines;     // Наличие альтернативных дисциплин в блоках дисциплин по выбору
    private boolean _controlActionOnTotalRegElement;     // Наличие контрольных мероприятий по итогам прохождения мероприятия (дисциплины, практики)
    private boolean _formingCompetenceRegElement;     // Формирование компетенций мероприятиями (дисциплинами, практиками)
    private boolean _integralityLaborPartYear;     // Целочисленность трудоемкости (ЗЕТ) мероприятий в части учебного года
    private boolean _evenAuditHours;     // Четность аудиторных часов
    private boolean _multipleAuditHoursPartYear;     // Кратность аудиторных часов в части учебного года заданному количеству недель
    private boolean _bindEduPlanAndFixOwner;     // Привязка к учебному плану мероприятий из реестров и закрепление за ответственными подразделениями
    private boolean _eduPlanConformityScheduleInPractices;     // Соответствие учебного плана учебному графику в части практик
    private boolean _selfWorkForAnyDiscipline;     // Наличие самостоятельной работы для любой дисциплины
    private boolean _duplicateDisciplinesTitles;     // Дублирование наименований дисциплин
    private boolean _regElementConformityEpv;     // Соответствие нагрузки реестрового элемента и строки УПв

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppStateEduStandard getEduStandard()
    {
        return _eduStandard;
    }

    /**
     * @param eduStandard Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     */
    public void setEduStandard(EppStateEduStandard eduStandard)
    {
        dirty(_eduStandard, eduStandard);
        _eduStandard = eduStandard;
    }

    /**
     * @return Наличие альтернативных дисциплин в блоках дисциплин по выбору. Свойство не может быть null.
     */
    @NotNull
    public boolean isAltInChoiceDisciplines()
    {
        return _altInChoiceDisciplines;
    }

    /**
     * @param altInChoiceDisciplines Наличие альтернативных дисциплин в блоках дисциплин по выбору. Свойство не может быть null.
     */
    public void setAltInChoiceDisciplines(boolean altInChoiceDisciplines)
    {
        dirty(_altInChoiceDisciplines, altInChoiceDisciplines);
        _altInChoiceDisciplines = altInChoiceDisciplines;
    }

    /**
     * @return Наличие контрольных мероприятий по итогам прохождения мероприятия (дисциплины, практики). Свойство не может быть null.
     */
    @NotNull
    public boolean isControlActionOnTotalRegElement()
    {
        return _controlActionOnTotalRegElement;
    }

    /**
     * @param controlActionOnTotalRegElement Наличие контрольных мероприятий по итогам прохождения мероприятия (дисциплины, практики). Свойство не может быть null.
     */
    public void setControlActionOnTotalRegElement(boolean controlActionOnTotalRegElement)
    {
        dirty(_controlActionOnTotalRegElement, controlActionOnTotalRegElement);
        _controlActionOnTotalRegElement = controlActionOnTotalRegElement;
    }

    /**
     * @return Формирование компетенций мероприятиями (дисциплинами, практиками). Свойство не может быть null.
     */
    @NotNull
    public boolean isFormingCompetenceRegElement()
    {
        return _formingCompetenceRegElement;
    }

    /**
     * @param formingCompetenceRegElement Формирование компетенций мероприятиями (дисциплинами, практиками). Свойство не может быть null.
     */
    public void setFormingCompetenceRegElement(boolean formingCompetenceRegElement)
    {
        dirty(_formingCompetenceRegElement, formingCompetenceRegElement);
        _formingCompetenceRegElement = formingCompetenceRegElement;
    }

    /**
     * @return Целочисленность трудоемкости (ЗЕТ) мероприятий в части учебного года. Свойство не может быть null.
     */
    @NotNull
    public boolean isIntegralityLaborPartYear()
    {
        return _integralityLaborPartYear;
    }

    /**
     * @param integralityLaborPartYear Целочисленность трудоемкости (ЗЕТ) мероприятий в части учебного года. Свойство не может быть null.
     */
    public void setIntegralityLaborPartYear(boolean integralityLaborPartYear)
    {
        dirty(_integralityLaborPartYear, integralityLaborPartYear);
        _integralityLaborPartYear = integralityLaborPartYear;
    }

    /**
     * @return Четность аудиторных часов. Свойство не может быть null.
     */
    @NotNull
    public boolean isEvenAuditHours()
    {
        return _evenAuditHours;
    }

    /**
     * @param evenAuditHours Четность аудиторных часов. Свойство не может быть null.
     */
    public void setEvenAuditHours(boolean evenAuditHours)
    {
        dirty(_evenAuditHours, evenAuditHours);
        _evenAuditHours = evenAuditHours;
    }

    /**
     * @return Кратность аудиторных часов в части учебного года заданному количеству недель. Свойство не может быть null.
     */
    @NotNull
    public boolean isMultipleAuditHoursPartYear()
    {
        return _multipleAuditHoursPartYear;
    }

    /**
     * @param multipleAuditHoursPartYear Кратность аудиторных часов в части учебного года заданному количеству недель. Свойство не может быть null.
     */
    public void setMultipleAuditHoursPartYear(boolean multipleAuditHoursPartYear)
    {
        dirty(_multipleAuditHoursPartYear, multipleAuditHoursPartYear);
        _multipleAuditHoursPartYear = multipleAuditHoursPartYear;
    }

    /**
     * @return Привязка к учебному плану мероприятий из реестров и закрепление за ответственными подразделениями. Свойство не может быть null.
     */
    @NotNull
    public boolean isBindEduPlanAndFixOwner()
    {
        return _bindEduPlanAndFixOwner;
    }

    /**
     * @param bindEduPlanAndFixOwner Привязка к учебному плану мероприятий из реестров и закрепление за ответственными подразделениями. Свойство не может быть null.
     */
    public void setBindEduPlanAndFixOwner(boolean bindEduPlanAndFixOwner)
    {
        dirty(_bindEduPlanAndFixOwner, bindEduPlanAndFixOwner);
        _bindEduPlanAndFixOwner = bindEduPlanAndFixOwner;
    }

    /**
     * @return Соответствие учебного плана учебному графику в части практик. Свойство не может быть null.
     */
    @NotNull
    public boolean isEduPlanConformityScheduleInPractices()
    {
        return _eduPlanConformityScheduleInPractices;
    }

    /**
     * @param eduPlanConformityScheduleInPractices Соответствие учебного плана учебному графику в части практик. Свойство не может быть null.
     */
    public void setEduPlanConformityScheduleInPractices(boolean eduPlanConformityScheduleInPractices)
    {
        dirty(_eduPlanConformityScheduleInPractices, eduPlanConformityScheduleInPractices);
        _eduPlanConformityScheduleInPractices = eduPlanConformityScheduleInPractices;
    }

    /**
     * @return Наличие самостоятельной работы для любой дисциплины. Свойство не может быть null.
     */
    @NotNull
    public boolean isSelfWorkForAnyDiscipline()
    {
        return _selfWorkForAnyDiscipline;
    }

    /**
     * @param selfWorkForAnyDiscipline Наличие самостоятельной работы для любой дисциплины. Свойство не может быть null.
     */
    public void setSelfWorkForAnyDiscipline(boolean selfWorkForAnyDiscipline)
    {
        dirty(_selfWorkForAnyDiscipline, selfWorkForAnyDiscipline);
        _selfWorkForAnyDiscipline = selfWorkForAnyDiscipline;
    }

    /**
     * @return Дублирование наименований дисциплин. Свойство не может быть null.
     */
    @NotNull
    public boolean isDuplicateDisciplinesTitles()
    {
        return _duplicateDisciplinesTitles;
    }

    /**
     * @param duplicateDisciplinesTitles Дублирование наименований дисциплин. Свойство не может быть null.
     */
    public void setDuplicateDisciplinesTitles(boolean duplicateDisciplinesTitles)
    {
        dirty(_duplicateDisciplinesTitles, duplicateDisciplinesTitles);
        _duplicateDisciplinesTitles = duplicateDisciplinesTitles;
    }

    /**
     * @return Соответствие нагрузки реестрового элемента и строки УПв. Свойство не может быть null.
     */
    @NotNull
    public boolean isRegElementConformityEpv()
    {
        return _regElementConformityEpv;
    }

    /**
     * @param regElementConformityEpv Соответствие нагрузки реестрового элемента и строки УПв. Свойство не может быть null.
     */
    public void setRegElementConformityEpv(boolean regElementConformityEpv)
    {
        dirty(_regElementConformityEpv, regElementConformityEpv);
        _regElementConformityEpv = regElementConformityEpv;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEppStateEduStandardAdditionalChecksGen)
        {
            if (withNaturalIdProperties)
            {
                setEduStandard(((FefuEppStateEduStandardAdditionalChecks)another).getEduStandard());
            }
            setAltInChoiceDisciplines(((FefuEppStateEduStandardAdditionalChecks)another).isAltInChoiceDisciplines());
            setControlActionOnTotalRegElement(((FefuEppStateEduStandardAdditionalChecks)another).isControlActionOnTotalRegElement());
            setFormingCompetenceRegElement(((FefuEppStateEduStandardAdditionalChecks)another).isFormingCompetenceRegElement());
            setIntegralityLaborPartYear(((FefuEppStateEduStandardAdditionalChecks)another).isIntegralityLaborPartYear());
            setEvenAuditHours(((FefuEppStateEduStandardAdditionalChecks)another).isEvenAuditHours());
            setMultipleAuditHoursPartYear(((FefuEppStateEduStandardAdditionalChecks)another).isMultipleAuditHoursPartYear());
            setBindEduPlanAndFixOwner(((FefuEppStateEduStandardAdditionalChecks)another).isBindEduPlanAndFixOwner());
            setEduPlanConformityScheduleInPractices(((FefuEppStateEduStandardAdditionalChecks)another).isEduPlanConformityScheduleInPractices());
            setSelfWorkForAnyDiscipline(((FefuEppStateEduStandardAdditionalChecks)another).isSelfWorkForAnyDiscipline());
            setDuplicateDisciplinesTitles(((FefuEppStateEduStandardAdditionalChecks)another).isDuplicateDisciplinesTitles());
            setRegElementConformityEpv(((FefuEppStateEduStandardAdditionalChecks)another).isRegElementConformityEpv());
        }
    }

    public INaturalId<FefuEppStateEduStandardAdditionalChecksGen> getNaturalId()
    {
        return new NaturalId(getEduStandard());
    }

    public static class NaturalId extends NaturalIdBase<FefuEppStateEduStandardAdditionalChecksGen>
    {
        private static final String PROXY_NAME = "FefuEppStateEduStandardAdditionalChecksNaturalProxy";

        private Long _eduStandard;

        public NaturalId()
        {}

        public NaturalId(EppStateEduStandard eduStandard)
        {
            _eduStandard = ((IEntity) eduStandard).getId();
        }

        public Long getEduStandard()
        {
            return _eduStandard;
        }

        public void setEduStandard(Long eduStandard)
        {
            _eduStandard = eduStandard;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuEppStateEduStandardAdditionalChecksGen.NaturalId) ) return false;

            FefuEppStateEduStandardAdditionalChecksGen.NaturalId that = (NaturalId) o;

            if( !equals(getEduStandard(), that.getEduStandard()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEduStandard());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEduStandard());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEppStateEduStandardAdditionalChecksGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEppStateEduStandardAdditionalChecks.class;
        }

        public T newInstance()
        {
            return (T) new FefuEppStateEduStandardAdditionalChecks();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduStandard":
                    return obj.getEduStandard();
                case "altInChoiceDisciplines":
                    return obj.isAltInChoiceDisciplines();
                case "controlActionOnTotalRegElement":
                    return obj.isControlActionOnTotalRegElement();
                case "formingCompetenceRegElement":
                    return obj.isFormingCompetenceRegElement();
                case "integralityLaborPartYear":
                    return obj.isIntegralityLaborPartYear();
                case "evenAuditHours":
                    return obj.isEvenAuditHours();
                case "multipleAuditHoursPartYear":
                    return obj.isMultipleAuditHoursPartYear();
                case "bindEduPlanAndFixOwner":
                    return obj.isBindEduPlanAndFixOwner();
                case "eduPlanConformityScheduleInPractices":
                    return obj.isEduPlanConformityScheduleInPractices();
                case "selfWorkForAnyDiscipline":
                    return obj.isSelfWorkForAnyDiscipline();
                case "duplicateDisciplinesTitles":
                    return obj.isDuplicateDisciplinesTitles();
                case "regElementConformityEpv":
                    return obj.isRegElementConformityEpv();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduStandard":
                    obj.setEduStandard((EppStateEduStandard) value);
                    return;
                case "altInChoiceDisciplines":
                    obj.setAltInChoiceDisciplines((Boolean) value);
                    return;
                case "controlActionOnTotalRegElement":
                    obj.setControlActionOnTotalRegElement((Boolean) value);
                    return;
                case "formingCompetenceRegElement":
                    obj.setFormingCompetenceRegElement((Boolean) value);
                    return;
                case "integralityLaborPartYear":
                    obj.setIntegralityLaborPartYear((Boolean) value);
                    return;
                case "evenAuditHours":
                    obj.setEvenAuditHours((Boolean) value);
                    return;
                case "multipleAuditHoursPartYear":
                    obj.setMultipleAuditHoursPartYear((Boolean) value);
                    return;
                case "bindEduPlanAndFixOwner":
                    obj.setBindEduPlanAndFixOwner((Boolean) value);
                    return;
                case "eduPlanConformityScheduleInPractices":
                    obj.setEduPlanConformityScheduleInPractices((Boolean) value);
                    return;
                case "selfWorkForAnyDiscipline":
                    obj.setSelfWorkForAnyDiscipline((Boolean) value);
                    return;
                case "duplicateDisciplinesTitles":
                    obj.setDuplicateDisciplinesTitles((Boolean) value);
                    return;
                case "regElementConformityEpv":
                    obj.setRegElementConformityEpv((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduStandard":
                        return true;
                case "altInChoiceDisciplines":
                        return true;
                case "controlActionOnTotalRegElement":
                        return true;
                case "formingCompetenceRegElement":
                        return true;
                case "integralityLaborPartYear":
                        return true;
                case "evenAuditHours":
                        return true;
                case "multipleAuditHoursPartYear":
                        return true;
                case "bindEduPlanAndFixOwner":
                        return true;
                case "eduPlanConformityScheduleInPractices":
                        return true;
                case "selfWorkForAnyDiscipline":
                        return true;
                case "duplicateDisciplinesTitles":
                        return true;
                case "regElementConformityEpv":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduStandard":
                    return true;
                case "altInChoiceDisciplines":
                    return true;
                case "controlActionOnTotalRegElement":
                    return true;
                case "formingCompetenceRegElement":
                    return true;
                case "integralityLaborPartYear":
                    return true;
                case "evenAuditHours":
                    return true;
                case "multipleAuditHoursPartYear":
                    return true;
                case "bindEduPlanAndFixOwner":
                    return true;
                case "eduPlanConformityScheduleInPractices":
                    return true;
                case "selfWorkForAnyDiscipline":
                    return true;
                case "duplicateDisciplinesTitles":
                    return true;
                case "regElementConformityEpv":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduStandard":
                    return EppStateEduStandard.class;
                case "altInChoiceDisciplines":
                    return Boolean.class;
                case "controlActionOnTotalRegElement":
                    return Boolean.class;
                case "formingCompetenceRegElement":
                    return Boolean.class;
                case "integralityLaborPartYear":
                    return Boolean.class;
                case "evenAuditHours":
                    return Boolean.class;
                case "multipleAuditHoursPartYear":
                    return Boolean.class;
                case "bindEduPlanAndFixOwner":
                    return Boolean.class;
                case "eduPlanConformityScheduleInPractices":
                    return Boolean.class;
                case "selfWorkForAnyDiscipline":
                    return Boolean.class;
                case "duplicateDisciplinesTitles":
                    return Boolean.class;
                case "regElementConformityEpv":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEppStateEduStandardAdditionalChecks> _dslPath = new Path<FefuEppStateEduStandardAdditionalChecks>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEppStateEduStandardAdditionalChecks");
    }
            

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#getEduStandard()
     */
    public static EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
    {
        return _dslPath.eduStandard();
    }

    /**
     * @return Наличие альтернативных дисциплин в блоках дисциплин по выбору. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isAltInChoiceDisciplines()
     */
    public static PropertyPath<Boolean> altInChoiceDisciplines()
    {
        return _dslPath.altInChoiceDisciplines();
    }

    /**
     * @return Наличие контрольных мероприятий по итогам прохождения мероприятия (дисциплины, практики). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isControlActionOnTotalRegElement()
     */
    public static PropertyPath<Boolean> controlActionOnTotalRegElement()
    {
        return _dslPath.controlActionOnTotalRegElement();
    }

    /**
     * @return Формирование компетенций мероприятиями (дисциплинами, практиками). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isFormingCompetenceRegElement()
     */
    public static PropertyPath<Boolean> formingCompetenceRegElement()
    {
        return _dslPath.formingCompetenceRegElement();
    }

    /**
     * @return Целочисленность трудоемкости (ЗЕТ) мероприятий в части учебного года. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isIntegralityLaborPartYear()
     */
    public static PropertyPath<Boolean> integralityLaborPartYear()
    {
        return _dslPath.integralityLaborPartYear();
    }

    /**
     * @return Четность аудиторных часов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isEvenAuditHours()
     */
    public static PropertyPath<Boolean> evenAuditHours()
    {
        return _dslPath.evenAuditHours();
    }

    /**
     * @return Кратность аудиторных часов в части учебного года заданному количеству недель. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isMultipleAuditHoursPartYear()
     */
    public static PropertyPath<Boolean> multipleAuditHoursPartYear()
    {
        return _dslPath.multipleAuditHoursPartYear();
    }

    /**
     * @return Привязка к учебному плану мероприятий из реестров и закрепление за ответственными подразделениями. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isBindEduPlanAndFixOwner()
     */
    public static PropertyPath<Boolean> bindEduPlanAndFixOwner()
    {
        return _dslPath.bindEduPlanAndFixOwner();
    }

    /**
     * @return Соответствие учебного плана учебному графику в части практик. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isEduPlanConformityScheduleInPractices()
     */
    public static PropertyPath<Boolean> eduPlanConformityScheduleInPractices()
    {
        return _dslPath.eduPlanConformityScheduleInPractices();
    }

    /**
     * @return Наличие самостоятельной работы для любой дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isSelfWorkForAnyDiscipline()
     */
    public static PropertyPath<Boolean> selfWorkForAnyDiscipline()
    {
        return _dslPath.selfWorkForAnyDiscipline();
    }

    /**
     * @return Дублирование наименований дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isDuplicateDisciplinesTitles()
     */
    public static PropertyPath<Boolean> duplicateDisciplinesTitles()
    {
        return _dslPath.duplicateDisciplinesTitles();
    }

    /**
     * @return Соответствие нагрузки реестрового элемента и строки УПв. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isRegElementConformityEpv()
     */
    public static PropertyPath<Boolean> regElementConformityEpv()
    {
        return _dslPath.regElementConformityEpv();
    }

    public static class Path<E extends FefuEppStateEduStandardAdditionalChecks> extends EntityPath<E>
    {
        private EppStateEduStandard.Path<EppStateEduStandard> _eduStandard;
        private PropertyPath<Boolean> _altInChoiceDisciplines;
        private PropertyPath<Boolean> _controlActionOnTotalRegElement;
        private PropertyPath<Boolean> _formingCompetenceRegElement;
        private PropertyPath<Boolean> _integralityLaborPartYear;
        private PropertyPath<Boolean> _evenAuditHours;
        private PropertyPath<Boolean> _multipleAuditHoursPartYear;
        private PropertyPath<Boolean> _bindEduPlanAndFixOwner;
        private PropertyPath<Boolean> _eduPlanConformityScheduleInPractices;
        private PropertyPath<Boolean> _selfWorkForAnyDiscipline;
        private PropertyPath<Boolean> _duplicateDisciplinesTitles;
        private PropertyPath<Boolean> _regElementConformityEpv;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#getEduStandard()
     */
        public EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
        {
            if(_eduStandard == null )
                _eduStandard = new EppStateEduStandard.Path<EppStateEduStandard>(L_EDU_STANDARD, this);
            return _eduStandard;
        }

    /**
     * @return Наличие альтернативных дисциплин в блоках дисциплин по выбору. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isAltInChoiceDisciplines()
     */
        public PropertyPath<Boolean> altInChoiceDisciplines()
        {
            if(_altInChoiceDisciplines == null )
                _altInChoiceDisciplines = new PropertyPath<Boolean>(FefuEppStateEduStandardAdditionalChecksGen.P_ALT_IN_CHOICE_DISCIPLINES, this);
            return _altInChoiceDisciplines;
        }

    /**
     * @return Наличие контрольных мероприятий по итогам прохождения мероприятия (дисциплины, практики). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isControlActionOnTotalRegElement()
     */
        public PropertyPath<Boolean> controlActionOnTotalRegElement()
        {
            if(_controlActionOnTotalRegElement == null )
                _controlActionOnTotalRegElement = new PropertyPath<Boolean>(FefuEppStateEduStandardAdditionalChecksGen.P_CONTROL_ACTION_ON_TOTAL_REG_ELEMENT, this);
            return _controlActionOnTotalRegElement;
        }

    /**
     * @return Формирование компетенций мероприятиями (дисциплинами, практиками). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isFormingCompetenceRegElement()
     */
        public PropertyPath<Boolean> formingCompetenceRegElement()
        {
            if(_formingCompetenceRegElement == null )
                _formingCompetenceRegElement = new PropertyPath<Boolean>(FefuEppStateEduStandardAdditionalChecksGen.P_FORMING_COMPETENCE_REG_ELEMENT, this);
            return _formingCompetenceRegElement;
        }

    /**
     * @return Целочисленность трудоемкости (ЗЕТ) мероприятий в части учебного года. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isIntegralityLaborPartYear()
     */
        public PropertyPath<Boolean> integralityLaborPartYear()
        {
            if(_integralityLaborPartYear == null )
                _integralityLaborPartYear = new PropertyPath<Boolean>(FefuEppStateEduStandardAdditionalChecksGen.P_INTEGRALITY_LABOR_PART_YEAR, this);
            return _integralityLaborPartYear;
        }

    /**
     * @return Четность аудиторных часов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isEvenAuditHours()
     */
        public PropertyPath<Boolean> evenAuditHours()
        {
            if(_evenAuditHours == null )
                _evenAuditHours = new PropertyPath<Boolean>(FefuEppStateEduStandardAdditionalChecksGen.P_EVEN_AUDIT_HOURS, this);
            return _evenAuditHours;
        }

    /**
     * @return Кратность аудиторных часов в части учебного года заданному количеству недель. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isMultipleAuditHoursPartYear()
     */
        public PropertyPath<Boolean> multipleAuditHoursPartYear()
        {
            if(_multipleAuditHoursPartYear == null )
                _multipleAuditHoursPartYear = new PropertyPath<Boolean>(FefuEppStateEduStandardAdditionalChecksGen.P_MULTIPLE_AUDIT_HOURS_PART_YEAR, this);
            return _multipleAuditHoursPartYear;
        }

    /**
     * @return Привязка к учебному плану мероприятий из реестров и закрепление за ответственными подразделениями. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isBindEduPlanAndFixOwner()
     */
        public PropertyPath<Boolean> bindEduPlanAndFixOwner()
        {
            if(_bindEduPlanAndFixOwner == null )
                _bindEduPlanAndFixOwner = new PropertyPath<Boolean>(FefuEppStateEduStandardAdditionalChecksGen.P_BIND_EDU_PLAN_AND_FIX_OWNER, this);
            return _bindEduPlanAndFixOwner;
        }

    /**
     * @return Соответствие учебного плана учебному графику в части практик. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isEduPlanConformityScheduleInPractices()
     */
        public PropertyPath<Boolean> eduPlanConformityScheduleInPractices()
        {
            if(_eduPlanConformityScheduleInPractices == null )
                _eduPlanConformityScheduleInPractices = new PropertyPath<Boolean>(FefuEppStateEduStandardAdditionalChecksGen.P_EDU_PLAN_CONFORMITY_SCHEDULE_IN_PRACTICES, this);
            return _eduPlanConformityScheduleInPractices;
        }

    /**
     * @return Наличие самостоятельной работы для любой дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isSelfWorkForAnyDiscipline()
     */
        public PropertyPath<Boolean> selfWorkForAnyDiscipline()
        {
            if(_selfWorkForAnyDiscipline == null )
                _selfWorkForAnyDiscipline = new PropertyPath<Boolean>(FefuEppStateEduStandardAdditionalChecksGen.P_SELF_WORK_FOR_ANY_DISCIPLINE, this);
            return _selfWorkForAnyDiscipline;
        }

    /**
     * @return Дублирование наименований дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isDuplicateDisciplinesTitles()
     */
        public PropertyPath<Boolean> duplicateDisciplinesTitles()
        {
            if(_duplicateDisciplinesTitles == null )
                _duplicateDisciplinesTitles = new PropertyPath<Boolean>(FefuEppStateEduStandardAdditionalChecksGen.P_DUPLICATE_DISCIPLINES_TITLES, this);
            return _duplicateDisciplinesTitles;
        }

    /**
     * @return Соответствие нагрузки реестрового элемента и строки УПв. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks#isRegElementConformityEpv()
     */
        public PropertyPath<Boolean> regElementConformityEpv()
        {
            if(_regElementConformityEpv == null )
                _regElementConformityEpv = new PropertyPath<Boolean>(FefuEppStateEduStandardAdditionalChecksGen.P_REG_ELEMENT_CONFORMITY_EPV, this);
            return _regElementConformityEpv;
        }

        public Class getEntityClass()
        {
            return FefuEppStateEduStandardAdditionalChecks.class;
        }

        public String getEntityName()
        {
            return "fefuEppStateEduStandardAdditionalChecks";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
