/* $Id$ */
package ru.tandemservice.unifefu.component.catalog.fefuOrphanPaymentType.FefuOrphanPaymentTypePub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.unibase.UniBaseUtils;

import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;

import java.util.List;

/**
 * @author nvankov
 * @since 11/18/13
 */
public class DAO extends DefaultCatalogPubDAO<FefuOrphanPaymentType, Model> implements IDAO
{
    @Override
    public void prepareListItemDataSource(final Model model) {
        final MQBuilder builder = new MQBuilder(FefuOrphanPaymentType.ENTITY_CLASS, "ci");
        applyFilters(model, builder);
        builder.addOrder("ci", FefuOrphanPaymentType.P_PRIORITY);
        final List<FefuOrphanPaymentType> result = builder.getResultList(this.getSession());
        int curPrior = 0;
        for (FefuOrphanPaymentType item : result){
            item.setPriority(curPrior++);
        }
        final DynamicListDataSource<FefuOrphanPaymentType> dataSource = model.getDataSource();
        dataSource.setTotalSize(result.size());
        dataSource.setCountRow(Math.max(4, 1+result.size()));
        dataSource.createPage(result);

    }

    @Override
    public void updatePriorityUp(Long studentStatusId)
    {
        changePriority(studentStatusId, true);
    }

    @Override
    public void updatePriorityDown(Long studentStatusId)
    {
        changePriority(studentStatusId, false);
    }

    private void changePriority(Long paymentTypeId, boolean up)
    {
        UniBaseUtils.changePriority(FefuOrphanPaymentType.class, paymentTypeId, FefuOrphanPaymentType.P_PRIORITY, up, getSession());
    }
}
