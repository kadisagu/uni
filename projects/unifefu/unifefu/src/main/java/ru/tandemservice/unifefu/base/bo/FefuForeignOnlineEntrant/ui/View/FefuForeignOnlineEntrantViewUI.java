/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.ui.View;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.FefuForeignOnlineEntrantManager;
import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.ui.Edit.FefuForeignOnlineEntrantEdit;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;

import java.util.Date;
import java.util.Map;

/**
 * @author nvankov
 * @since 4/2/13
 */
@Input
        ({
                @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entrantId"),
        })
public class FefuForeignOnlineEntrantViewUI extends UIPresenter
{
    private Long _entrantId;
    private FefuForeignOnlineEntrant _entrant;

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }

    public FefuForeignOnlineEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(FefuForeignOnlineEntrant entrant)
    {
        _entrant = entrant;
    }

    @Override
    public void onComponentRefresh()
    {
        _entrant = DataAccessServices.dao().get(_entrantId);
    }

    public Boolean getDownloadable()
    {
        return null != _entrant.getDocumentCopies();
    }

    public void onClickEdit()
    {
        _uiActivation.asDesktopRoot(FefuForeignOnlineEntrantEdit.class).parameter(UIPresenter.PUBLISHER_ID, _entrantId).activate();
    }

    public void onClickAcceptRequest()
    {
        _entrant.setAccepted(true);
        _entrant.setProcessDate(new Date());
        FefuForeignOnlineEntrantManager.instance().dao().createOrUpdate(_entrant);
    }

    public void onClickReturnRequest()
    {
        _entrant.setAccepted(false);
        _entrant.setProcessDate(null);
        FefuForeignOnlineEntrantManager.instance().dao().createOrUpdate(_entrant);
    }

    public void onClickDownloadDocumentCopies()
    {
        final DatabaseFile documentCopies = _entrant.getDocumentCopies();
        if (null == documentCopies) { return; }

        final String filename = StringUtils.trimToNull(_entrant.getDocumentCopiesName());
        if (null == filename) { return; }

        final byte[] content = documentCopies.getContent();
        if (null == content) { return; }

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(null).fileName(filename).document(content), true);

    }

    public void onClickPrintRequestRu()
    {
        UniecScriptItem template = DataAccessServices.dao().get(UniecScriptItem.class, UniecScriptItem.code(), FefuForeignOnlineEntrantManager.TEMPLATE_PRINT_REQUEST_RU);
        if (template == null) return;

        IPrintFormCreator<Map> formCreator = (IPrintFormCreator<Map>) ApplicationRuntime.getBean("fefuForeignOnlineEntrantRequestPrint");
        byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(template.getCurrentTemplate(), new ParametersMap().add("id", _entrant.getId()).add("lang", "ru")));
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Заявление онлайн-абитуриента " + _entrant.getFullFio() + ".rtf");
        _uiActivation.asCurrent(IUniComponents.PRINT_REPORT)
                .parameter("id", id).parameter("zip", Boolean.FALSE).parameter("extension", "rtf")
                .activate();

    }

    public void onClickPrintRequestEn()
    {
        UniecScriptItem template = DataAccessServices.dao().get(UniecScriptItem.class, UniecScriptItem.code(), FefuForeignOnlineEntrantManager.TEMPLATE_PRINT_REQUEST_EN);
        if (template == null) return;

        IPrintFormCreator<Map> formCreator = (IPrintFormCreator<Map>) ApplicationRuntime.getBean("fefuForeignOnlineEntrantRequestPrint");
        byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(template.getCurrentTemplate(), new ParametersMap().add("id", _entrant.getId()).add("lang", "en")));
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Заявление онлайн-абитуриента " + _entrant.getFullFio() + ".rtf");
        _uiActivation.asCurrent(IUniComponents.PRINT_REPORT)
                .parameter("id", id).parameter("zip", Boolean.FALSE).parameter("extension", "rtf")
                .activate();
    }

}
