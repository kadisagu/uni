/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 16.07.2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Attachments", propOrder = {})
public class AttachmentsType
{
    @XmlElements({
            @XmlElement(name = "Attachment", type = AttachmentType.class)
    })

    protected List<AttachmentType> attachments = new ArrayList<>();

    public AttachmentsType()
    {
    }

    public AttachmentsType(AttachmentType attachment)
    {
        this.attachments.add(attachment);
    }

    public AttachmentsType(List<AttachmentType> attachments)
    {
        this.attachments = attachments;
    }

    public List<AttachmentType> getAttachments()
    {
        return attachments;
    }

    public void setAttachments(List<AttachmentType> attachments)
    {
        this.attachments = attachments;
    }
}