/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.ScienceDiplomaWrapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 26.06.2013
 */
public class NsiDatagramUtil
{
    private static final GregorianCalendar CALENDAR = new GregorianCalendar();
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private static String MONTH_CHAIN_STR = "января февраля марта апреля мая июня июля августа сентября октября ноября декабря";
    private static Map<String, Integer> MONTH_STR_TO_NUMBER_MAP = new HashMap<>();

    static
    {
        MONTH_STR_TO_NUMBER_MAP.put("января", 0);
        MONTH_STR_TO_NUMBER_MAP.put("февраля", 1);
        MONTH_STR_TO_NUMBER_MAP.put("марта", 2);
        MONTH_STR_TO_NUMBER_MAP.put("апреля", 3);
        MONTH_STR_TO_NUMBER_MAP.put("мая", 4);
        MONTH_STR_TO_NUMBER_MAP.put("июня", 5);
        MONTH_STR_TO_NUMBER_MAP.put("июля", 6);
        MONTH_STR_TO_NUMBER_MAP.put("августа", 7);
        MONTH_STR_TO_NUMBER_MAP.put("сентября", 8);
        MONTH_STR_TO_NUMBER_MAP.put("октября", 9);
        MONTH_STR_TO_NUMBER_MAP.put("ноября", 10);
        MONTH_STR_TO_NUMBER_MAP.put("декабря", 11);
    }

    public static final Date parseDate(String dateStr)
    {
        if (null == dateStr) return null;
        try
        {
            return DATE_FORMAT.parse(dateStr);
        } catch (ParseException e)
        {
        }
        return null;
    }

    public static final String formatDate(Date date)
    {
        if (null == date) return null;
        return DATE_FORMAT.format(date);
    }

    public static final Boolean parseBoolean(String boolStr)
    {
        if (null == boolStr) return null;
        return Boolean.valueOf(boolStr);
    }

    public static final String formatBoolean(Boolean bool)
    {
        if (null == bool) return null;
        return bool.toString();
    }

    public static String formatScienceDiploma(PersonAcademicDegree degree)
    {
        StringBuilder dipNumBuilder = new StringBuilder();
        if (null != degree.getSeria()) dipNumBuilder.append(degree.getSeria());
        if (null != degree.getNumber())
            dipNumBuilder.append(dipNumBuilder.length() > 0 ? " № " : "№ ").append(degree.getNumber());
        if (null != degree.getIssuanceDate())
            dipNumBuilder.append(dipNumBuilder.length() > 0 ? " от " : "от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(degree.getIssuanceDate()));
        if (null == StringUtils.trimToNull(dipNumBuilder.toString())) return null;
        return dipNumBuilder.toString();
    }

    public static String formatScienceDiploma(PersonAcademicStatus status)
    {
        StringBuilder dipNumBuilder = new StringBuilder();
        if (null != status.getSeria()) dipNumBuilder.append(status.getSeria());
        if (null != status.getNumber())
            dipNumBuilder.append(dipNumBuilder.length() > 0 ? " № " : "№ ").append(status.getNumber());
        if (null != status.getIssuanceDate())
            dipNumBuilder.append(dipNumBuilder.length() > 0 ? " от " : "от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(status.getIssuanceDate()));
        if (null == StringUtils.trimToNull(dipNumBuilder.toString())) return null;
        return dipNumBuilder.toString();
    }

    public static ScienceDiplomaWrapper parseScienceDiplomaWrapper(String diplomaString)
    {
        ScienceDiplomaWrapper wrapper = new ScienceDiplomaWrapper();

        String resultItem = diplomaString.replaceAll("№", " ").replaceAll("Диплом", "").replaceAll("от", " ").replaceAll(",", " ").replaceAll("\\. ", ".").replaceAll("г\\.", " ");
        String[] parts = resultItem.replaceAll("  ", " ").replaceAll("  ", " ").replaceAll("  ", " ").replaceAll("  ", " ").trim().split(" ");
        if (parts.length > 3) System.out.print("* ");

        for (int partIdx = 0; partIdx < parts.length; partIdx++)
        {
            int pointCount = 0;
            int digitsCount = 0;

            String part = parts[partIdx];

            for (int chIdx = 0; chIdx < part.length(); chIdx++)
            {
                if (".".equals(part.substring(chIdx, chIdx + 1))) pointCount++;
                else if (StringUtils.isNumeric(part.substring(chIdx, chIdx + 1))) digitsCount++;
            }

            if (null == wrapper.getSeria() && pointCount == 0 && digitsCount == 0) wrapper.setSeria(part);
            else if (null == wrapper.getNumber() && pointCount == 0) wrapper.setNumber(part);
            else if (pointCount > 1 && part.length() > 7)
            {
                String[] dateParts = part.split("\\.");
                try
                {
                    int day = Integer.valueOf(dateParts[0]);
                    int month = Integer.valueOf(dateParts[1]);

                    int i = 0;
                    while (i < dateParts[2].length() && StringUtils.isNumeric(dateParts[2].substring(i, i + 1))) i++;

                    if (i > 1)
                    {
                        int year = Integer.valueOf(dateParts[2].substring(0, i));
                        if (year < 30) year = 2000 + year;
                        else if (year < 100) year = 1900 + year;

                        CALENDAR.set(Calendar.YEAR, year);
                        CALENDAR.set(Calendar.MONTH, month - 1);
                        CALENDAR.set(Calendar.DAY_OF_MONTH, day);
                        wrapper.setDate(CALENDAR.getTime());
                    }
                } catch (NumberFormatException ex)
                {
                    System.out.println("!!!!!!! Could not parse date '" + part + "'");
                }
            } else if (null == wrapper.getDate() && digitsCount == 0 && pointCount == 0 && MONTH_CHAIN_STR.contains(part.toLowerCase()) && partIdx > 0 && partIdx < parts.length - 1)
            {
                try
                {
                    int day = Integer.valueOf(parts[partIdx - 1]);

                    Integer month = MONTH_STR_TO_NUMBER_MAP.get(part.toLowerCase());
                    if (null == month)
                    {
                        for (Map.Entry<String, Integer> entry : MONTH_STR_TO_NUMBER_MAP.entrySet())
                        {
                            if (entry.getKey().contains(part.toLowerCase()))
                            {
                                month = entry.getValue();
                                break;
                            }
                        }
                    }

                    int i = 0;
                    while (i < parts[partIdx + 1].length() && StringUtils.isNumeric(parts[partIdx + 1].substring(i, i + 1)))
                        i++;

                    if (i > 1)
                    {
                        int year = Integer.valueOf(parts[partIdx + 1].substring(0, i));
                        if (year < 30) year = 2000 + year;
                        else if (year < 100) year = 1900 + year;

                        CALENDAR.set(Calendar.YEAR, year);
                        CALENDAR.set(Calendar.MONTH, month);
                        CALENDAR.set(Calendar.DAY_OF_MONTH, day);
                        wrapper.setDate(CALENDAR.getTime());
                    }
                } catch (NumberFormatException ex)
                {
                    System.out.println("!!!!!!! Could not parse date '" + part + "'");
                }
            }
        }


        if (wrapper.isEmpty()) return null;

        return wrapper;
    }
}