package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.unifefu.entity.ws.MdbViewAddress;
import ru.tandemservice.unifefu.entity.ws.MdbViewPerson;
import ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ближайший родственник
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewPersonNextofkinGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin";
    public static final String ENTITY_NAME = "mdbViewPersonNextofkin";
    public static final int VERSION_HASH = -1467420194;
    private static IEntityMeta ENTITY_META;

    public static final String L_MDB_VIEW_PERSON = "mdbViewPerson";
    public static final String L_PERSON_NEXT_OF_KIN = "personNextOfKin";
    public static final String P_RELATION_DEGREE = "relationDegree";
    public static final String P_IDENTITY_SERIA = "identitySeria";
    public static final String P_IDENTITY_NUMBER = "identityNumber";
    public static final String P_IDENTITY_FIRST_NAME = "identityFirstName";
    public static final String P_IDENTITY_LAST_NAME = "identityLastName";
    public static final String P_IDENTITY_MIDDLE_NAME = "identityMiddleName";
    public static final String P_IDENTITY_BIRTH_DATE = "identityBirthDate";
    public static final String P_IDENTITY_DATE = "identityDate";
    public static final String P_IDENTITY_CODE = "identityCode";
    public static final String P_IDENTITY_PLACE = "identityPlace";
    public static final String P_EMPLOYEE_PLACE = "employeePlace";
    public static final String P_PHONES = "phones";
    public static final String L_ADDRESS_ID = "addressId";

    private MdbViewPerson _mdbViewPerson;     // Персона
    private PersonNextOfKin _personNextOfKin;     // Ближайший родственник
    private String _relationDegree; 
    private String _identitySeria;     // Серия
    private String _identityNumber;     // Номер
    private String _identityFirstName;     // Имя
    private String _identityLastName;     // Фамилия
    private String _identityMiddleName;     // Отчество
    private Date _identityBirthDate;     // Дата рождения
    private Date _identityDate;     // Дата выдачи удостоверения
    private String _identityCode;     // Код подразделения
    private String _identityPlace;     // Кем выдано удостоверение
    private String _employeePlace;     // Место работы
    private String _phones;     // Контактные телефоны
    private MdbViewAddress _addressId;     // Адрес

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона. Свойство не может быть null.
     */
    @NotNull
    public MdbViewPerson getMdbViewPerson()
    {
        return _mdbViewPerson;
    }

    /**
     * @param mdbViewPerson Персона. Свойство не может быть null.
     */
    public void setMdbViewPerson(MdbViewPerson mdbViewPerson)
    {
        dirty(_mdbViewPerson, mdbViewPerson);
        _mdbViewPerson = mdbViewPerson;
    }

    /**
     * @return Ближайший родственник. Свойство не может быть null.
     */
    @NotNull
    public PersonNextOfKin getPersonNextOfKin()
    {
        return _personNextOfKin;
    }

    /**
     * @param personNextOfKin Ближайший родственник. Свойство не может быть null.
     */
    public void setPersonNextOfKin(PersonNextOfKin personNextOfKin)
    {
        dirty(_personNextOfKin, personNextOfKin);
        _personNextOfKin = personNextOfKin;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRelationDegree()
    {
        return _relationDegree;
    }

    /**
     * @param relationDegree  Свойство не может быть null.
     */
    public void setRelationDegree(String relationDegree)
    {
        dirty(_relationDegree, relationDegree);
        _relationDegree = relationDegree;
    }

    /**
     * @return Серия.
     */
    @Length(max=255)
    public String getIdentitySeria()
    {
        return _identitySeria;
    }

    /**
     * @param identitySeria Серия.
     */
    public void setIdentitySeria(String identitySeria)
    {
        dirty(_identitySeria, identitySeria);
        _identitySeria = identitySeria;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getIdentityNumber()
    {
        return _identityNumber;
    }

    /**
     * @param identityNumber Номер.
     */
    public void setIdentityNumber(String identityNumber)
    {
        dirty(_identityNumber, identityNumber);
        _identityNumber = identityNumber;
    }

    /**
     * @return Имя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getIdentityFirstName()
    {
        return _identityFirstName;
    }

    /**
     * @param identityFirstName Имя. Свойство не может быть null.
     */
    public void setIdentityFirstName(String identityFirstName)
    {
        dirty(_identityFirstName, identityFirstName);
        _identityFirstName = identityFirstName;
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getIdentityLastName()
    {
        return _identityLastName;
    }

    /**
     * @param identityLastName Фамилия. Свойство не может быть null.
     */
    public void setIdentityLastName(String identityLastName)
    {
        dirty(_identityLastName, identityLastName);
        _identityLastName = identityLastName;
    }

    /**
     * @return Отчество.
     */
    @Length(max=255)
    public String getIdentityMiddleName()
    {
        return _identityMiddleName;
    }

    /**
     * @param identityMiddleName Отчество.
     */
    public void setIdentityMiddleName(String identityMiddleName)
    {
        dirty(_identityMiddleName, identityMiddleName);
        _identityMiddleName = identityMiddleName;
    }

    /**
     * @return Дата рождения.
     */
    public Date getIdentityBirthDate()
    {
        return _identityBirthDate;
    }

    /**
     * @param identityBirthDate Дата рождения.
     */
    public void setIdentityBirthDate(Date identityBirthDate)
    {
        dirty(_identityBirthDate, identityBirthDate);
        _identityBirthDate = identityBirthDate;
    }

    /**
     * @return Дата выдачи удостоверения.
     */
    public Date getIdentityDate()
    {
        return _identityDate;
    }

    /**
     * @param identityDate Дата выдачи удостоверения.
     */
    public void setIdentityDate(Date identityDate)
    {
        dirty(_identityDate, identityDate);
        _identityDate = identityDate;
    }

    /**
     * @return Код подразделения.
     */
    @Length(max=255)
    public String getIdentityCode()
    {
        return _identityCode;
    }

    /**
     * @param identityCode Код подразделения.
     */
    public void setIdentityCode(String identityCode)
    {
        dirty(_identityCode, identityCode);
        _identityCode = identityCode;
    }

    /**
     * @return Кем выдано удостоверение.
     */
    @Length(max=255)
    public String getIdentityPlace()
    {
        return _identityPlace;
    }

    /**
     * @param identityPlace Кем выдано удостоверение.
     */
    public void setIdentityPlace(String identityPlace)
    {
        dirty(_identityPlace, identityPlace);
        _identityPlace = identityPlace;
    }

    /**
     * @return Место работы.
     */
    @Length(max=255)
    public String getEmployeePlace()
    {
        return _employeePlace;
    }

    /**
     * @param employeePlace Место работы.
     */
    public void setEmployeePlace(String employeePlace)
    {
        dirty(_employeePlace, employeePlace);
        _employeePlace = employeePlace;
    }

    /**
     * @return Контактные телефоны.
     */
    @Length(max=255)
    public String getPhones()
    {
        return _phones;
    }

    /**
     * @param phones Контактные телефоны.
     */
    public void setPhones(String phones)
    {
        dirty(_phones, phones);
        _phones = phones;
    }

    /**
     * @return Адрес.
     */
    public MdbViewAddress getAddressId()
    {
        return _addressId;
    }

    /**
     * @param addressId Адрес.
     */
    public void setAddressId(MdbViewAddress addressId)
    {
        dirty(_addressId, addressId);
        _addressId = addressId;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewPersonNextofkinGen)
        {
            setMdbViewPerson(((MdbViewPersonNextofkin)another).getMdbViewPerson());
            setPersonNextOfKin(((MdbViewPersonNextofkin)another).getPersonNextOfKin());
            setRelationDegree(((MdbViewPersonNextofkin)another).getRelationDegree());
            setIdentitySeria(((MdbViewPersonNextofkin)another).getIdentitySeria());
            setIdentityNumber(((MdbViewPersonNextofkin)another).getIdentityNumber());
            setIdentityFirstName(((MdbViewPersonNextofkin)another).getIdentityFirstName());
            setIdentityLastName(((MdbViewPersonNextofkin)another).getIdentityLastName());
            setIdentityMiddleName(((MdbViewPersonNextofkin)another).getIdentityMiddleName());
            setIdentityBirthDate(((MdbViewPersonNextofkin)another).getIdentityBirthDate());
            setIdentityDate(((MdbViewPersonNextofkin)another).getIdentityDate());
            setIdentityCode(((MdbViewPersonNextofkin)another).getIdentityCode());
            setIdentityPlace(((MdbViewPersonNextofkin)another).getIdentityPlace());
            setEmployeePlace(((MdbViewPersonNextofkin)another).getEmployeePlace());
            setPhones(((MdbViewPersonNextofkin)another).getPhones());
            setAddressId(((MdbViewPersonNextofkin)another).getAddressId());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewPersonNextofkinGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewPersonNextofkin.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewPersonNextofkin();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mdbViewPerson":
                    return obj.getMdbViewPerson();
                case "personNextOfKin":
                    return obj.getPersonNextOfKin();
                case "relationDegree":
                    return obj.getRelationDegree();
                case "identitySeria":
                    return obj.getIdentitySeria();
                case "identityNumber":
                    return obj.getIdentityNumber();
                case "identityFirstName":
                    return obj.getIdentityFirstName();
                case "identityLastName":
                    return obj.getIdentityLastName();
                case "identityMiddleName":
                    return obj.getIdentityMiddleName();
                case "identityBirthDate":
                    return obj.getIdentityBirthDate();
                case "identityDate":
                    return obj.getIdentityDate();
                case "identityCode":
                    return obj.getIdentityCode();
                case "identityPlace":
                    return obj.getIdentityPlace();
                case "employeePlace":
                    return obj.getEmployeePlace();
                case "phones":
                    return obj.getPhones();
                case "addressId":
                    return obj.getAddressId();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mdbViewPerson":
                    obj.setMdbViewPerson((MdbViewPerson) value);
                    return;
                case "personNextOfKin":
                    obj.setPersonNextOfKin((PersonNextOfKin) value);
                    return;
                case "relationDegree":
                    obj.setRelationDegree((String) value);
                    return;
                case "identitySeria":
                    obj.setIdentitySeria((String) value);
                    return;
                case "identityNumber":
                    obj.setIdentityNumber((String) value);
                    return;
                case "identityFirstName":
                    obj.setIdentityFirstName((String) value);
                    return;
                case "identityLastName":
                    obj.setIdentityLastName((String) value);
                    return;
                case "identityMiddleName":
                    obj.setIdentityMiddleName((String) value);
                    return;
                case "identityBirthDate":
                    obj.setIdentityBirthDate((Date) value);
                    return;
                case "identityDate":
                    obj.setIdentityDate((Date) value);
                    return;
                case "identityCode":
                    obj.setIdentityCode((String) value);
                    return;
                case "identityPlace":
                    obj.setIdentityPlace((String) value);
                    return;
                case "employeePlace":
                    obj.setEmployeePlace((String) value);
                    return;
                case "phones":
                    obj.setPhones((String) value);
                    return;
                case "addressId":
                    obj.setAddressId((MdbViewAddress) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mdbViewPerson":
                        return true;
                case "personNextOfKin":
                        return true;
                case "relationDegree":
                        return true;
                case "identitySeria":
                        return true;
                case "identityNumber":
                        return true;
                case "identityFirstName":
                        return true;
                case "identityLastName":
                        return true;
                case "identityMiddleName":
                        return true;
                case "identityBirthDate":
                        return true;
                case "identityDate":
                        return true;
                case "identityCode":
                        return true;
                case "identityPlace":
                        return true;
                case "employeePlace":
                        return true;
                case "phones":
                        return true;
                case "addressId":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mdbViewPerson":
                    return true;
                case "personNextOfKin":
                    return true;
                case "relationDegree":
                    return true;
                case "identitySeria":
                    return true;
                case "identityNumber":
                    return true;
                case "identityFirstName":
                    return true;
                case "identityLastName":
                    return true;
                case "identityMiddleName":
                    return true;
                case "identityBirthDate":
                    return true;
                case "identityDate":
                    return true;
                case "identityCode":
                    return true;
                case "identityPlace":
                    return true;
                case "employeePlace":
                    return true;
                case "phones":
                    return true;
                case "addressId":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mdbViewPerson":
                    return MdbViewPerson.class;
                case "personNextOfKin":
                    return PersonNextOfKin.class;
                case "relationDegree":
                    return String.class;
                case "identitySeria":
                    return String.class;
                case "identityNumber":
                    return String.class;
                case "identityFirstName":
                    return String.class;
                case "identityLastName":
                    return String.class;
                case "identityMiddleName":
                    return String.class;
                case "identityBirthDate":
                    return Date.class;
                case "identityDate":
                    return Date.class;
                case "identityCode":
                    return String.class;
                case "identityPlace":
                    return String.class;
                case "employeePlace":
                    return String.class;
                case "phones":
                    return String.class;
                case "addressId":
                    return MdbViewAddress.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewPersonNextofkin> _dslPath = new Path<MdbViewPersonNextofkin>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewPersonNextofkin");
    }
            

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getMdbViewPerson()
     */
    public static MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
    {
        return _dslPath.mdbViewPerson();
    }

    /**
     * @return Ближайший родственник. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getPersonNextOfKin()
     */
    public static PersonNextOfKin.Path<PersonNextOfKin> personNextOfKin()
    {
        return _dslPath.personNextOfKin();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getRelationDegree()
     */
    public static PropertyPath<String> relationDegree()
    {
        return _dslPath.relationDegree();
    }

    /**
     * @return Серия.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentitySeria()
     */
    public static PropertyPath<String> identitySeria()
    {
        return _dslPath.identitySeria();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityNumber()
     */
    public static PropertyPath<String> identityNumber()
    {
        return _dslPath.identityNumber();
    }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityFirstName()
     */
    public static PropertyPath<String> identityFirstName()
    {
        return _dslPath.identityFirstName();
    }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityLastName()
     */
    public static PropertyPath<String> identityLastName()
    {
        return _dslPath.identityLastName();
    }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityMiddleName()
     */
    public static PropertyPath<String> identityMiddleName()
    {
        return _dslPath.identityMiddleName();
    }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityBirthDate()
     */
    public static PropertyPath<Date> identityBirthDate()
    {
        return _dslPath.identityBirthDate();
    }

    /**
     * @return Дата выдачи удостоверения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityDate()
     */
    public static PropertyPath<Date> identityDate()
    {
        return _dslPath.identityDate();
    }

    /**
     * @return Код подразделения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityCode()
     */
    public static PropertyPath<String> identityCode()
    {
        return _dslPath.identityCode();
    }

    /**
     * @return Кем выдано удостоверение.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityPlace()
     */
    public static PropertyPath<String> identityPlace()
    {
        return _dslPath.identityPlace();
    }

    /**
     * @return Место работы.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getEmployeePlace()
     */
    public static PropertyPath<String> employeePlace()
    {
        return _dslPath.employeePlace();
    }

    /**
     * @return Контактные телефоны.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getPhones()
     */
    public static PropertyPath<String> phones()
    {
        return _dslPath.phones();
    }

    /**
     * @return Адрес.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getAddressId()
     */
    public static MdbViewAddress.Path<MdbViewAddress> addressId()
    {
        return _dslPath.addressId();
    }

    public static class Path<E extends MdbViewPersonNextofkin> extends EntityPath<E>
    {
        private MdbViewPerson.Path<MdbViewPerson> _mdbViewPerson;
        private PersonNextOfKin.Path<PersonNextOfKin> _personNextOfKin;
        private PropertyPath<String> _relationDegree;
        private PropertyPath<String> _identitySeria;
        private PropertyPath<String> _identityNumber;
        private PropertyPath<String> _identityFirstName;
        private PropertyPath<String> _identityLastName;
        private PropertyPath<String> _identityMiddleName;
        private PropertyPath<Date> _identityBirthDate;
        private PropertyPath<Date> _identityDate;
        private PropertyPath<String> _identityCode;
        private PropertyPath<String> _identityPlace;
        private PropertyPath<String> _employeePlace;
        private PropertyPath<String> _phones;
        private MdbViewAddress.Path<MdbViewAddress> _addressId;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getMdbViewPerson()
     */
        public MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
        {
            if(_mdbViewPerson == null )
                _mdbViewPerson = new MdbViewPerson.Path<MdbViewPerson>(L_MDB_VIEW_PERSON, this);
            return _mdbViewPerson;
        }

    /**
     * @return Ближайший родственник. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getPersonNextOfKin()
     */
        public PersonNextOfKin.Path<PersonNextOfKin> personNextOfKin()
        {
            if(_personNextOfKin == null )
                _personNextOfKin = new PersonNextOfKin.Path<PersonNextOfKin>(L_PERSON_NEXT_OF_KIN, this);
            return _personNextOfKin;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getRelationDegree()
     */
        public PropertyPath<String> relationDegree()
        {
            if(_relationDegree == null )
                _relationDegree = new PropertyPath<String>(MdbViewPersonNextofkinGen.P_RELATION_DEGREE, this);
            return _relationDegree;
        }

    /**
     * @return Серия.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentitySeria()
     */
        public PropertyPath<String> identitySeria()
        {
            if(_identitySeria == null )
                _identitySeria = new PropertyPath<String>(MdbViewPersonNextofkinGen.P_IDENTITY_SERIA, this);
            return _identitySeria;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityNumber()
     */
        public PropertyPath<String> identityNumber()
        {
            if(_identityNumber == null )
                _identityNumber = new PropertyPath<String>(MdbViewPersonNextofkinGen.P_IDENTITY_NUMBER, this);
            return _identityNumber;
        }

    /**
     * @return Имя. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityFirstName()
     */
        public PropertyPath<String> identityFirstName()
        {
            if(_identityFirstName == null )
                _identityFirstName = new PropertyPath<String>(MdbViewPersonNextofkinGen.P_IDENTITY_FIRST_NAME, this);
            return _identityFirstName;
        }

    /**
     * @return Фамилия. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityLastName()
     */
        public PropertyPath<String> identityLastName()
        {
            if(_identityLastName == null )
                _identityLastName = new PropertyPath<String>(MdbViewPersonNextofkinGen.P_IDENTITY_LAST_NAME, this);
            return _identityLastName;
        }

    /**
     * @return Отчество.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityMiddleName()
     */
        public PropertyPath<String> identityMiddleName()
        {
            if(_identityMiddleName == null )
                _identityMiddleName = new PropertyPath<String>(MdbViewPersonNextofkinGen.P_IDENTITY_MIDDLE_NAME, this);
            return _identityMiddleName;
        }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityBirthDate()
     */
        public PropertyPath<Date> identityBirthDate()
        {
            if(_identityBirthDate == null )
                _identityBirthDate = new PropertyPath<Date>(MdbViewPersonNextofkinGen.P_IDENTITY_BIRTH_DATE, this);
            return _identityBirthDate;
        }

    /**
     * @return Дата выдачи удостоверения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityDate()
     */
        public PropertyPath<Date> identityDate()
        {
            if(_identityDate == null )
                _identityDate = new PropertyPath<Date>(MdbViewPersonNextofkinGen.P_IDENTITY_DATE, this);
            return _identityDate;
        }

    /**
     * @return Код подразделения.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityCode()
     */
        public PropertyPath<String> identityCode()
        {
            if(_identityCode == null )
                _identityCode = new PropertyPath<String>(MdbViewPersonNextofkinGen.P_IDENTITY_CODE, this);
            return _identityCode;
        }

    /**
     * @return Кем выдано удостоверение.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getIdentityPlace()
     */
        public PropertyPath<String> identityPlace()
        {
            if(_identityPlace == null )
                _identityPlace = new PropertyPath<String>(MdbViewPersonNextofkinGen.P_IDENTITY_PLACE, this);
            return _identityPlace;
        }

    /**
     * @return Место работы.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getEmployeePlace()
     */
        public PropertyPath<String> employeePlace()
        {
            if(_employeePlace == null )
                _employeePlace = new PropertyPath<String>(MdbViewPersonNextofkinGen.P_EMPLOYEE_PLACE, this);
            return _employeePlace;
        }

    /**
     * @return Контактные телефоны.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getPhones()
     */
        public PropertyPath<String> phones()
        {
            if(_phones == null )
                _phones = new PropertyPath<String>(MdbViewPersonNextofkinGen.P_PHONES, this);
            return _phones;
        }

    /**
     * @return Адрес.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonNextofkin#getAddressId()
     */
        public MdbViewAddress.Path<MdbViewAddress> addressId()
        {
            if(_addressId == null )
                _addressId = new MdbViewAddress.Path<MdbViewAddress>(L_ADDRESS_ID, this);
            return _addressId;
        }

        public Class getEntityClass()
        {
            return MdbViewPersonNextofkin.class;
        }

        public String getEntityName()
        {
            return "mdbViewPersonNextofkin";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
