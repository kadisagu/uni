/* $Id: DAO.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.workplan.WorkPlanAddEdit;

import ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit.Model;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 08.02.14
 * Time: 21:13
 */
public class DAO extends ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit.DAO
{
    @Override
    public void onChangeEppYear(Model model)
    {
        super.onChangeEppYear(model);
        if(null == model.getElement().getRegistrationNumber())
            model.getElement().setRegistrationNumber(model.getElement().getNumber());
    }
}
