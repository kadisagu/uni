/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu11;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuCompensationTypeTransferStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class FefuCompensationTypeTransferStuListExtractDao extends UniBaseDao implements IExtractComponentDao<FefuCompensationTypeTransferStuListExtract>
{
    @Override
    public void doCommit(FefuCompensationTypeTransferStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setCompensationTypeOld(extract.getEntity().getCompensationType());
        extract.getEntity().setCompensationType(extract.getCompensationTypeNew());
    }

    @Override
    public void doRollback(FefuCompensationTypeTransferStuListExtract extract, Map parameters)
    {
        extract.getEntity().setCompensationType(extract.getCompensationTypeOld());
    }
}