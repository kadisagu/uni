/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.logic;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unifefu.entity.report.FefuBrsStudentDisciplinesReport;

import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.Map;

/**
 * @author nvankov
 * @since 12/19/13
 */
public interface IFefuBrsStudentDisciplinesReportDAO
{
    FefuBrsStudentDisciplinesReport createReport(FefuBrsStudentDisciplinesReportParams reportSettings);

    Map<Long, FefuBrsStudentDisciplinesReportDAO.StudentData> calcStudentsRating(EppYearPart eppYearPart, Collection<Group> groups, OrgUnit formativeOrgUnit, OrgUnit responsibilityOrgUnit, boolean onlyFilledJournals, boolean withGroup);
}
