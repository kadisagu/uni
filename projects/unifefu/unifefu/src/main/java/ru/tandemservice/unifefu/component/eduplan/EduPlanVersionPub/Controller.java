/* $Id$ */
package ru.tandemservice.unifefu.component.eduplan.EduPlanVersionPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.VersionBlockPrint.FefuEduPlanVersionBlockPrint;

/**
 * @author Alexey Lopatin
 * @since 20.12.2014
 */
public class Controller extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub.Controller
{
    @Override
    public void onClickPrint(final IBusinessComponent component)
    {
        EppEduPlanVersion version = getModel(component).getEduplanVersion();
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(FefuEduPlanVersionBlockPrint.class.getSimpleName(), new ParametersMap().add(UIPresenter.PUBLISHER_ID, version.getId())));
    }
}
