/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

/**
 * @author Dmitry Seleznev
 * @since 20.01.2015
 */
public interface IFefuNsiOrphanGuidDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IFefuNsiOrphanGuidDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IFefuNsiOrphanGuidDaemonDao> instance = new SpringBeanCache<>(IFefuNsiOrphanGuidDaemonDao.class.getName());

    /**
     * Производит чистку таблицы с guid'ами объекты которых были удалены.
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doCleanOrphanGuids();
}