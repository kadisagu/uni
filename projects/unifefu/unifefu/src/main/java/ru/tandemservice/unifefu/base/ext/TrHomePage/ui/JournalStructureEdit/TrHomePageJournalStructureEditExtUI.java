/* $Id$ */
package ru.tandemservice.unifefu.base.ext.TrHomePage.ui.JournalStructureEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unifefu.base.bo.FefuTrHomePage.FefuTrHomePageManager;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalStructureEdit.TrHomePageJournalStructureEditUI;

/**
 * @author Nikolay Fedorovskih
 * @since 13.02.2014
 */
public class TrHomePageJournalStructureEditExtUI extends UIAddon
{
    public TrHomePageJournalStructureEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public boolean isFormativeState()
    {
        TrHomePageJournalStructureEditUI presenter = getPresenter();
        return presenter.getJournal() != null && presenter.getJournal().getState().isFormative();
    }

    public void onClickSendToCoordination()
    {
        if (isFormativeState())
        {
            TrHomePageJournalStructureEditUI presenter = getPresenter();
            FefuTrHomePageManager.instance().homePageDao().doSendToCoordinateRegistryElement(presenter.getJournal());
        }
    }
}