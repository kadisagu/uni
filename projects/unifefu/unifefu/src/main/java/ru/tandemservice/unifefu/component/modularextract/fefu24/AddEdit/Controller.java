/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu24.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract;

import java.util.Calendar;

/**
 * @author Andrey Andreev
 * @since 12.01.2016
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuAdmittedToGIAExtract, IDAO, Model>
{

    public void onSubmitYear(IBusinessComponent component)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        if (((Model) component.getModel()).getYear() < Calendar.getInstance().get(Calendar.YEAR))
            errCollector.add("Год должен быть больше или равен текущему.", "year");
    }
}