package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.AdditionalProfessionalEducationProgramForStudentManager;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic.Constants.APE_PROGRAM_DS;
import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic.Constants.STUDENT_ID;
import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic.Constants.EDUCATION_ORG_UNIT;

@Input({
               @Bind(key = STUDENT_ID, binding = STUDENT_ID)
       })
public class AdditionalProfessionalEducationProgramForStudentAddEditUI extends UIPresenter
{
    private FefuAdditionalProfessionalEducationProgramForStudent programForStudent;
    private Long studentId;
    private Student student;

    public Student getStudent()
    {
        return student;
    }

    public void setStudent(Student student)
    {
        this.student = student;
    }

    public void setStudentId(Long studentId)
    {
        this.studentId = studentId;
    }


    @Override
    public void onComponentRefresh()
    {
        if (studentId != null)
        {
            student = DataAccessServices.dao().get(Student.class, studentId);
            programForStudent = AdditionalProfessionalEducationProgramForStudentManager.instance().apeProgramForStudentDao().getApeProgramForStudentByStudentId(studentId);

            if (programForStudent == null)
            {
                programForStudent = new FefuAdditionalProfessionalEducationProgramForStudent();
                programForStudent.setStudent(student);
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (student != null)
        {
            if (APE_PROGRAM_DS.equals(dataSource.getName()))
            {
                dataSource.put(EDUCATION_ORG_UNIT, student.getEducationOrgUnit());
            }
        }
    }

    public void onClickApply()
    {
        if (programForStudent != null)
        {
            if (validate().hasErrors())
            {
                return;
            }
            DataAccessServices.dao().saveOrUpdate(programForStudent);
        }
        deactivate();
    }

    protected ErrorCollector validate()
    {
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        Date startDate = programForStudent.getEducationStartDate();
        Date endDate = programForStudent.getEducationEndDate();
        if (startDate != null && endDate != null && endDate.before(startDate))
        {
            errorCollector.add("Дата начала обучения не может быть больше даты окончания обучения.", "educationStartDate", "educationEndDate");
        }

        return errorCollector;
    }


    public FefuAdditionalProfessionalEducationProgramForStudent getProgramForStudent()
    {
        return programForStudent;
    }

    public void setProgramForStudent(FefuAdditionalProfessionalEducationProgramForStudent programForStudent)
    {
        this.programForStudent = programForStudent;
    }


}
