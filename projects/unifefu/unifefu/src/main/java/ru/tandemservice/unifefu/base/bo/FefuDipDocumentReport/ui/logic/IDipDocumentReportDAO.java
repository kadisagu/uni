/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.ui.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.ui.ResultsList.FefuDipDocumentExcelBuilder;

import java.io.ByteArrayOutputStream;

/**
 * @author Andrey Avetisov
 * @since 30.09.2014
 */
public interface IDipDocumentReportDAO extends ICommonDAO, INeedPersistenceSupport
{
    ByteArrayOutputStream buildReport(FefuDipDocumentExcelBuilder builder) throws Exception;
}
