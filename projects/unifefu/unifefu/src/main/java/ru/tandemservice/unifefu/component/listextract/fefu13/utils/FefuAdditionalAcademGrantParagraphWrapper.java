package ru.tandemservice.unifefu.component.listextract.fefu13.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.unifefu.entity.catalog.FefuPromotedActivityType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public class FefuAdditionalAcademGrantParagraphWrapper implements Comparable<FefuAdditionalAcademGrantParagraphWrapper>
{
    private final FefuPromotedActivityType _promotedActivityType;
    private final Date _dateStart;
    private final Date _dateEnd;
    private final ListStudentExtract _firstExtract;

    public FefuAdditionalAcademGrantParagraphWrapper(FefuPromotedActivityType promotedActivityType, Date dateStart, Date dateEnd, ListStudentExtract firstExtract)
    {
        _promotedActivityType = promotedActivityType;
        _dateStart = dateStart;
        _dateEnd = dateEnd;
        _firstExtract = firstExtract;
    }

    private final List<FefuAdditionalAcademGrantExtractWrapper> _extractWrapperList = new ArrayList<>();

    public FefuPromotedActivityType getPromotedActivityType()
    {
        return _promotedActivityType;
    }

    public Date getDateStart()
    {
        return _dateStart;
    }

    public Date getDateEnd()
    {
        return _dateEnd;
    }

    public List<FefuAdditionalAcademGrantExtractWrapper> getExtractWrapperList()
    {
        return _extractWrapperList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuAdditionalAcademGrantParagraphWrapper))
            return false;

        FefuAdditionalAcademGrantParagraphWrapper that = (FefuAdditionalAcademGrantParagraphWrapper) o;

        return _promotedActivityType.equals(that.getPromotedActivityType())
        && _dateStart.equals(that.getDateStart())
        && _dateEnd.equals(that.getDateEnd());
    }

    @Override
    public int hashCode()
    {
        return _promotedActivityType.hashCode() &  _dateStart.hashCode() & _dateEnd.hashCode();
    }

    @Override
    public int compareTo(FefuAdditionalAcademGrantParagraphWrapper o)
    {
       int result = _promotedActivityType.getCode().compareTo(o.getPromotedActivityType().getCode());

        if (result == 0)
        result = _dateStart.compareTo(o.getDateStart());

        if (result == 0)
            result = _dateEnd.compareTo(o.getDateEnd());

        return result;
    }
}