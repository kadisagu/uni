/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.DirectumList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.unifefu.base.bo.FefuSettings.ui.DirectumEdit.FefuSettingsDirectumEdit;
import ru.tandemservice.unifefu.base.bo.FefuSettings.ui.DirectumEdit.FefuSettingsDirectumEditUI;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuDirectumOrderTypeCodes;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 10.07.2013
 */
public class FefuSettingsDirectumListUI extends UIPresenter
{
    public static final Map<Long, TreeableDirectumSettingDataWrapper> wrappersMap = new LinkedHashMap<>();

    @Override
    public void onComponentRefresh()
    {
        wrappersMap.clear();

        List<StudentExtractType> exTypes = new ArrayList<>();
        for (StudentExtractType extType : DataAccessServices.dao().getList(StudentExtractType.class))
        {
            if (null != extType.getParent() && null != extType.getParent().getParent() && StudentExtractTypeCodes.LIST_ORDER.equals(extType.getParent().getParent().getCode()))
                continue;
            if (extType.isActive()) exTypes.add(extType);
        }

        Map<CoreCollectionUtils.Pair<Long, Long>, FefuDirectumSettings> directumSettingsMap = new HashMap<>();
        for (FefuDirectumSettings settings : DataAccessServices.dao().getList(FefuDirectumSettings.class))
        {
            CoreCollectionUtils.Pair<Long, Long> id = new CoreCollectionUtils.Pair<>(settings.getStudentExtractType().getId(), settings.getFefuDirectumOrderType().getId());
            directumSettingsMap.put(id, settings);
        }

        Collections.sort(exTypes, new Comparator<StudentExtractType>()
        {
            @Override
            public int compare(StudentExtractType o1, StudentExtractType o2)
            {
                Stack<StudentExtractType> parents1 = new Stack<>();
                Stack<StudentExtractType> parents2 = new Stack<>();

                while (o1 != null)
                {
                    parents1.push(o1);
                    o1 = o1.getParent();
                }

                while (o2 != null)
                {
                    parents2.push(o2);
                    o2 = o2.getParent();
                }

                int result = parents1.pop().getCode().compareTo(parents2.pop().getCode());

                if (result != 0)
                {
                    return result;
                }

                while (!parents1.empty() && !parents2.empty())
                {
                    result = parents1.pop().getTitle().compareTo(parents2.pop().getTitle());
                    if (result != 0)
                    {
                        return result;
                    }
                }

                if (!parents1.empty() || !parents2.empty())
                {
                    result = parents1.empty() ? -1 : 1;
                }

                return result;
            }
        });

        Map<Long, TreeableDirectumSettingDataWrapper> realTypeParentIdToWrapperMap = new HashMap<>();
        List<FefuDirectumOrderType> orderTypes = DataAccessServices.dao().getList(FefuDirectumOrderType.class, FefuDirectumOrderType.shortTitle().s());
        orderTypes.remove(DataAccessServices.dao().getByNaturalId(new FefuDirectumOrderType.NaturalId(FefuDirectumOrderTypeCodes.ENR)));

        for (StudentExtractType extractType : exTypes)
        {
            TreeableDirectumSettingDataWrapper record;

            if (null == extractType.getParent())
            {
                Long id = extractType.getId();
                record = new TreeableDirectumSettingDataWrapper(id, extractType.getTitle(), extractType, null, null);
                record.setProperty(FefuSettingsDirectumList.EDIT_DISABLED, Boolean.TRUE);
                record.setProperty(FefuSettingsDirectumList.CLEAR_DISABLED, Boolean.TRUE);
                record.setProperty(FefuSettingsDirectumList.SETTIGS_PARAM, null);
                realTypeParentIdToWrapperMap.put(extractType.getId(), record);
                wrappersMap.put(id, record);
            } else
            {
                for (FefuDirectumOrderType orderType : orderTypes)
                {
                    Long id = System.currentTimeMillis();
                    while (wrappersMap.containsKey(id)) id = System.currentTimeMillis();
                    CoreCollectionUtils.Pair<Long, Long> pair = new CoreCollectionUtils.Pair<>(extractType.getId(), orderType.getId());
                    record = new TreeableDirectumSettingDataWrapper(id, extractType.getTitle(), extractType, orderType, realTypeParentIdToWrapperMap.get(extractType.getParent().getId()));
                    record.setProperty(FefuSettingsDirectumList.EDIT_DISABLED, Boolean.FALSE);
                    FefuDirectumSettings settings = directumSettingsMap.get(pair);
                    if (null == settings) record.setProperty(FefuSettingsDirectumList.CLEAR_DISABLED, Boolean.TRUE);
                    else record.setProperty(FefuSettingsDirectumList.CLEAR_DISABLED, Boolean.FALSE);
                    record.setProperty(FefuSettingsDirectumList.SETTIGS_PARAM, settings);
                    wrappersMap.put(id, record);
                }
            }
        }

        Map<CoreCollectionUtils.Pair<Long, Long>, FefuDirectumEnrSettings> enrDirectumSettingsMap = new HashMap<>();
        for (FefuDirectumEnrSettings settings : DataAccessServices.dao().getList(FefuDirectumEnrSettings.class))
        {
            CoreCollectionUtils.Pair<Long, Long> id = new CoreCollectionUtils.Pair<>(settings.getEntrantEnrollmentOrderType().getId(), settings.getFefuDirectumOrderType().getId());
            enrDirectumSettingsMap.put(id, settings);
        }

        EnrollmentCampaign campaign = UniecDAOFacade.getEntrantDAO().getLastEnrollmentCampaign();
        if (null != campaign)
        {
            Long rootId = System.currentTimeMillis();
            FefuDirectumOrderType enrType = DataAccessServices.dao().getByNaturalId(new FefuDirectumOrderType.NaturalId(FefuDirectumOrderTypeCodes.ENR));
            TreeableDirectumSettingDataWrapper rootRecord = new TreeableDirectumSettingDataWrapper(rootId, "Приказы о зачислении абитуриентов", new EntrantEnrollmentOrderType(), null, null);
            rootRecord.setProperty(FefuSettingsDirectumList.EDIT_DISABLED, Boolean.TRUE);
            rootRecord.setProperty(FefuSettingsDirectumList.CLEAR_DISABLED, Boolean.TRUE);
            rootRecord.setProperty(FefuSettingsDirectumList.SETTIGS_PARAM, null);
            realTypeParentIdToWrapperMap.put(rootId, rootRecord);
            wrappersMap.put(rootId, rootRecord);

            TreeableDirectumSettingDataWrapper record;
            for (EnrollmentOrderType extType : DataAccessServices.dao().getList(EnrollmentOrderType.class, EnrollmentOrderType.enrollmentCampaign(), campaign, EnrollmentOrderType.entrantEnrollmentOrderType().title().s()))
            {
                if (extType.isUsed())
                {
                    Long id = System.currentTimeMillis();
                    while (wrappersMap.containsKey(id)) id = System.currentTimeMillis();
                    CoreCollectionUtils.Pair<Long, Long> pair = new CoreCollectionUtils.Pair<>(extType.getEntrantEnrollmentOrderType().getId(), enrType.getId());
                    record = new TreeableDirectumSettingDataWrapper(id, extType.getTitle(), extType.getEntrantEnrollmentOrderType(), enrType, realTypeParentIdToWrapperMap.get(rootId));
                    record.setProperty(FefuSettingsDirectumList.EDIT_DISABLED, Boolean.FALSE);
                    FefuDirectumEnrSettings settings = enrDirectumSettingsMap.get(pair);
                    if (null == settings) record.setProperty(FefuSettingsDirectumList.CLEAR_DISABLED, Boolean.TRUE);
                    else record.setProperty(FefuSettingsDirectumList.CLEAR_DISABLED, Boolean.FALSE);
                    record.setProperty(FefuSettingsDirectumList.SETTIGS_PARAM, settings);
                    wrappersMap.put(id, record);
                }
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuSettingsDirectumList.SETTIGS_DIRECTUM_DS.equals(dataSource.getName()))
            dataSource.put(FefuSettingsDirectumList.DATA_WRAPPERT_LIST, new ArrayList<>(wrappersMap.values()));
    }

    public void onEditEntityFromList()
    {
        TreeableDirectumSettingDataWrapper wrapper = wrappersMap.get(getListenerParameter());

        _uiActivation.asRegionDialog(FefuSettingsDirectumEdit.class)
                .parameter(FefuSettingsDirectumEditUI.EXTRACT_TYPE_ID, null != wrapper.getExtractType() ? wrapper.getExtractType().getId() : null)
                .parameter(FefuSettingsDirectumEditUI.ENR_EXTRACT_TYPE_ID, null != wrapper.getEnrExtractType() ? wrapper.getEnrExtractType().getId() : null)
                .parameter(FefuSettingsDirectumEditUI.FEFU_DIRECTUM_ORDER_TYPE_ID, wrapper.getOrderType().getId())
                .activate();
    }

    public void onClickResetSettings()
    {
        TreeableDirectumSettingDataWrapper wrapper = wrappersMap.get(getListenerParameter());
        if (null != wrapper.getExtractType())
        {
            FefuDirectumSettings settings = DataAccessServices.dao().getByNaturalId(new FefuDirectumSettings.NaturalId(wrapper.getExtractType(), wrapper.getOrderType()));
            if (null != settings) DataAccessServices.dao().delete(settings);
        } else if (null != wrapper.getEnrExtractType())
        {
            FefuDirectumEnrSettings settings = DataAccessServices.dao().getByNaturalId(new FefuDirectumEnrSettings.NaturalId(wrapper.getEnrExtractType(), wrapper.getOrderType()));
            if (null != settings) DataAccessServices.dao().delete(settings);
        }
    }
}