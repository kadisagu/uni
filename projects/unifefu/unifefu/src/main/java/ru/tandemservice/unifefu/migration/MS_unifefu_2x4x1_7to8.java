package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x1_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuSignerStuExtractRel

        short entityCodeSignerRel;
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefusignerstuextractrel_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("setting_id", DBType.LONG).setNullable(false), 
				new DBColumn("edulevelstage_id", DBType.LONG).setNullable(false), 
				new DBColumn("signer_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			entityCodeSignerRel = tool.entityCodes().ensure("fefuSignerStuExtractRel");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuSignerStuExtractSetting

        short entityCodeSigner;
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefusignerstuextractsetting_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("studentextracttype_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			entityCodeSigner = tool.entityCodes().ensure("fefuSignerStuExtractSetting");
		}

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuSignerStuDocTypeRel

        short entityCodeSignerStuDocTypeRel;
        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fefusignerstudoctyperel_t",
	            new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
	            new DBColumn("discriminator", DBType.SHORT).setNullable(false),
	            new DBColumn("setting_id", DBType.LONG).setNullable(false),
	            new DBColumn("edulevelstage_id", DBType.LONG).setNullable(false),
	            new DBColumn("signer_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            entityCodeSignerStuDocTypeRel = tool.entityCodes().ensure("fefuSignerStuDocTypeRel");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuSignerStuDocTypeSetting

        short entityCodeSignerStuDocType;
        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fefusignerstudoctypesetting_t",
	            new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
	            new DBColumn("discriminator", DBType.SHORT).setNullable(false),
	            new DBColumn("studentdocumenttype_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            entityCodeSignerStuDocType = tool.entityCodes().ensure("fefuSignerStuDocTypeSetting");
        }

		// Формируем запрос для поднятия должностей
        Statement employeePost = tool.getConnection().createStatement();
        employeePost.execute("select ep.id, icard.lastname_p from employeepost_t ep " +
                                     "left join (personrole_t pr inner join employee_t e on e.id=pr.id) on e.id = ep.employee_id " +
                                     "left join person_t p on p.id = pr.person_id " +
                                     "left join identitycard_t icard on icard.id = p.identitycard_id " +
                                     "left join orgunittypepostrelation_t oupr on ep.postrelation_id = oupr.id " +
                                     "left join postboundedwithqgandql_t b on oupr.postboundedwithqgandql_id = b.id " +
                                     "left join post_t post on b.post_id = post.id " +
                                     "where post.code_p = '36' " +
                                     "and ((icard.lastname_p = 'Соппа' and icard.firstname_p = 'Игорь' and icard.middlename_p = 'Владимирович') " +
                                     "or (icard.lastname_p = 'Шушин' and icard.firstname_p = 'Андрей' and icard.middlename_p = 'Николаевич')) " +
                                     "order by icard.lastname_p desc");

        ResultSet srcEmployeePost = employeePost.getResultSet();

        PreparedStatement insertVisa = tool.prepareStatement("insert into employeepostpossiblevisa_t (id, discriminator, title_p, entity_id) values (?, ?, ?, ?)");

        PreparedStatement insertSigner = tool.prepareStatement("insert into fefusignerstuextractsetting_t (id, discriminator, studentextracttype_id) values (?, ?, ?)");
        PreparedStatement insertSignerRelation = tool.prepareStatement("insert into fefusignerstuextractrel_t (id, discriminator, setting_id, edulevelstage_id, signer_id) values (?, ?, ?, ?, ?)");

        PreparedStatement insertSignerStuDocType = tool.prepareStatement("insert into fefusignerstudoctypesetting_t (id, discriminator, studentdocumenttype_id) values (?, ?, ?)");
        PreparedStatement insertSignerStuDocTypeRelation = tool.prepareStatement("insert into fefusignerstudoctyperel_t (id, discriminator, setting_id, edulevelstage_id, signer_id) values (?, ?, ?, ?, ?)");

        ////////////////////////////////////////////////////////////////////////////////
        // СПО (ГОС2) и СПО (ФГОС)
        Statement stSPO = tool.getConnection().createStatement();
        stSPO.execute("select id, code_p from structureEducationLevels_t where shorttitle_p is not null and (code_p = 11 or code_p = 34) order by code_p");
        ResultSet srcSPO = stSPO.getResultSet();

        // ВПО (ГОС2) и ВПО (ФГОС)
        Statement stVPO = tool.getConnection().createStatement();
        stVPO.execute("select id, code_p from structureEducationLevels_t where shorttitle_p is not null and (code_p = 1 or code_p = 26) order by code_p");
        ResultSet srcVPO = stVPO.getResultSet();

        // НПО и ДПО
        Statement stNPO_DPO = tool.getConnection().createStatement();
        stNPO_DPO.execute("select id, code_p from structureEducationLevels_t where shorttitle_p is not null and (code_p = 15 or code_p = 18) order by code_p");
        ResultSet srcNPO_DPO = stNPO_DPO.getResultSet();

        ////////////////////////////////////////////////////////////////////////////////
        // заносим приказы для СПО
        long eduLevelStage_GOS2_Id = 0;
        long eduLevelStage_FGOS_Id = 0;

        long employeePostShushinId = 0;
        long employeePostSoppaId = 0;

        long eduLevelStage_NPO_Id = 0;
        long eduLevelStage_DPO_Id = 0;

        // СПО - (ГОС2, ФГОС)
        while (srcSPO.next())
        {
            String code = srcSPO.getString(2);
            if (code.equals("11"))
                eduLevelStage_GOS2_Id = srcSPO.getLong(1);
            else
                eduLevelStage_FGOS_Id = srcSPO.getLong(1);
        }

        // гарантировать наличие кода сущности
        short entityCodeVisa = tool.entityCodes().ensure("employeePostPossibleVisa");

        // создаем две визы: Шушин и Соппа
        while (srcEmployeePost.next())
        {
            long employeePostId = srcEmployeePost.getLong(1);
            String lastName = srcEmployeePost.getString(2);

            if (lastName.equals("Шушин"))
            {
                Long id = EntityIDGenerator.generateNewId(entityCodeVisa);
                insertVisa.setLong(1, id);
                insertVisa.setShort(2, entityCodeVisa);
                insertVisa.setString(3, "Зам. проректора по учебной и воспитательной работе");
                insertVisa.setLong(4, employeePostId);
                insertVisa.executeUpdate();

                employeePostShushinId = id;
            }
            else
            {
                Long id = EntityIDGenerator.generateNewId(entityCodeVisa);
                insertVisa.setLong(1, id);
                insertVisa.setShort(2, entityCodeVisa);
                insertVisa.setString(3, "И.о. проректора по учебной и воспитательной работе");
                insertVisa.setLong(4, employeePostId);
                insertVisa.executeUpdate();

                employeePostSoppaId = id;
            }
        }

        for (String code : DOCUMENT_TYPE_SIGNER_SHUSHIN)
        {
            Long signerId = EntityIDGenerator.generateNewId(entityCodeSignerStuDocType);
            insertSignerStuDocType(tool, entityCodeSignerStuDocType, insertSignerStuDocType, signerId, code);

            insertSignerStuDocTypeRelation(entityCodeSignerStuDocTypeRel, insertSignerStuDocTypeRelation, signerId, eduLevelStage_GOS2_Id, employeePostShushinId);
            insertSignerStuDocTypeRelation(entityCodeSignerStuDocTypeRel, insertSignerStuDocTypeRelation, signerId, eduLevelStage_FGOS_Id, employeePostShushinId);
        }

        for (String code : DOCUMENT_TYPE_SIGNER_SOPPA_SPO)
        {
            Long signerId = EntityIDGenerator.generateNewId(entityCodeSignerStuDocType);
            insertSignerStuDocType(tool, entityCodeSignerStuDocType, insertSignerStuDocType, signerId, code);

            insertSignerStuDocTypeRelation(entityCodeSignerStuDocTypeRel, insertSignerStuDocTypeRelation, signerId, eduLevelStage_GOS2_Id, employeePostSoppaId);
            insertSignerStuDocTypeRelation(entityCodeSignerStuDocTypeRel, insertSignerStuDocTypeRelation, signerId, eduLevelStage_FGOS_Id, employeePostSoppaId);
        }

        for (String code : ORDER_TYPE_TO_SPO_SIGNER_SHUSHIN)
        {
            Long signerId = EntityIDGenerator.generateNewId(entityCodeSigner);
            insertSigner(tool, entityCodeSigner, insertSigner, signerId, code);

            insertSignerRelation(entityCodeSignerRel, insertSignerRelation, signerId, eduLevelStage_GOS2_Id, employeePostShushinId);
            insertSignerRelation(entityCodeSignerRel, insertSignerRelation, signerId, eduLevelStage_FGOS_Id, employeePostShushinId);
        }

        for (String code : ORDER_TYPE_TO_SPO_SIGNER_SOPPA)
        {
            Long signerId = EntityIDGenerator.generateNewId(entityCodeSigner);
            insertSigner(tool, entityCodeSigner, insertSigner, signerId, code);

            insertSignerRelation(entityCodeSignerRel, insertSignerRelation, signerId, eduLevelStage_GOS2_Id, employeePostSoppaId);
            insertSignerRelation(entityCodeSignerRel, insertSignerRelation, signerId, eduLevelStage_FGOS_Id, employeePostSoppaId);
        }

        // ВПО - (ГОС2, ФГОС)
        while (srcVPO.next())
        {
            String code = srcVPO.getString(2);
            if (code.equals("1"))
                eduLevelStage_GOS2_Id = srcVPO.getLong(1);
            else
                eduLevelStage_FGOS_Id = srcVPO.getLong(1);
        }

        // НПО, ДПО
        while (srcNPO_DPO.next())
        {
            String code = srcNPO_DPO.getString(2);
            if (code.equals("15"))
                eduLevelStage_NPO_Id = srcNPO_DPO.getLong(1);
            else
                eduLevelStage_DPO_Id = srcNPO_DPO.getLong(1);
        }

        for (String code : DOCUMENT_TYPE_SIGNER_SOPPA)
        {
            Long signerId = null;
            if (DOCUMENT_TYPE_SIGNER_SHUSHIN.contains(code) || DOCUMENT_TYPE_SIGNER_SOPPA_SPO.contains(code))
            {
                PreparedStatement selectDocumentType = tool.prepareStatement("select id from fefusignerstudoctypesetting_t where studentdocumenttype_id = (select id from studentdocumenttype_t where code_p = ?)");
                selectDocumentType.setString(1, code);
                selectDocumentType.execute();

                ResultSet srcDocumentType = selectDocumentType.getResultSet();

                while (srcDocumentType.next())
                {
                    signerId = srcDocumentType.getLong(1);
                }
            }
            else
            {
                signerId = EntityIDGenerator.generateNewId(entityCodeSignerStuDocType);
                insertSignerStuDocType(tool, entityCodeSignerStuDocType, insertSignerStuDocType, signerId, code);
            }

            insertSignerStuDocTypeRelation(entityCodeSignerStuDocTypeRel, insertSignerStuDocTypeRelation, signerId, eduLevelStage_GOS2_Id, employeePostSoppaId);
            insertSignerStuDocTypeRelation(entityCodeSignerStuDocTypeRel, insertSignerStuDocTypeRelation, signerId, eduLevelStage_FGOS_Id, employeePostSoppaId);
            insertSignerStuDocTypeRelation(entityCodeSignerStuDocTypeRel, insertSignerStuDocTypeRelation, signerId, eduLevelStage_NPO_Id, employeePostSoppaId);
            insertSignerStuDocTypeRelation(entityCodeSignerStuDocTypeRel, insertSignerStuDocTypeRelation, signerId, eduLevelStage_DPO_Id, employeePostSoppaId);
        }

        for (String code : ORDER_TYPE_TO_VPO_SIGNER_SOPPA)
        {
            Long signerId = null;

            if (ORDER_TYPE_TO_SPO_SIGNER_SHUSHIN.contains(code) || ORDER_TYPE_TO_SPO_SIGNER_SOPPA.contains(code))
            {
                PreparedStatement selectExtractType = tool.prepareStatement("select id from fefusignerstuextractsetting_t where studentextracttype_id = (select id from studentExtractType_t where code_p = ?)");
                selectExtractType.setString(1, code);
                selectExtractType.execute();

                ResultSet srcExtractType = selectExtractType.getResultSet();

                while (srcExtractType.next())
                {
                    signerId = srcExtractType.getLong(1);
                }
            }
            else
            {
                signerId = EntityIDGenerator.generateNewId(entityCodeSigner);
                insertSigner(tool, entityCodeSigner, insertSigner, signerId, code);
            }

            insertSignerRelation(entityCodeSignerRel, insertSignerRelation, signerId, eduLevelStage_GOS2_Id, employeePostSoppaId);
            insertSignerRelation(entityCodeSignerRel, insertSignerRelation, signerId, eduLevelStage_FGOS_Id, employeePostSoppaId);
        }
    }

    private void insertSigner(DBTool tool, short entityCode, PreparedStatement insert, long id, String code) throws Exception
    {
        Statement stmn = tool.getConnection().createStatement();
        stmn.execute("select id from studentExtractType_t where code_p = '" + code + "'");
        ResultSet src = stmn.getResultSet();
        while (src.next())
        {
            long studentExtractTypeId = src.getLong(1);
            insert.setLong(1, id);
            insert.setShort(2, entityCode);
            insert.setLong(3, studentExtractTypeId);
            insert.executeUpdate();
        }
    }

    private void insertSignerRelation(short entityCode, PreparedStatement insert, long signerId, long eduLevelStageId, long employeePostId) throws Exception
    {
        Long id = EntityIDGenerator.generateNewId(entityCode);
        insert.setLong(1, id);
        insert.setShort(2, entityCode);
        insert.setLong(3, signerId);
        insert.setLong(4, eduLevelStageId);
        insert.setLong(5, employeePostId);
        insert.executeUpdate();
    }

    private void insertSignerStuDocType(DBTool tool, short entityCode, PreparedStatement insert, long id, String code) throws Exception
    {
        Statement stmn = tool.getConnection().createStatement();
        stmn.execute("select id from studentdocumenttype_t where code_p = '" + code + "'");;
        ResultSet src = stmn.getResultSet();
        while (src.next())
        {
            long studentDocumentTypeId = src.getLong(1);
            insert.setLong(1, id);
            insert.setShort(2, entityCode);
            insert.setLong(3, studentDocumentTypeId);
            insert.executeUpdate();
        }
    }

    private void insertSignerStuDocTypeRelation(short entityCode, PreparedStatement insert, long signerId, long eduLevelStageId, long employeePostId) throws Exception
    {
        Long id = EntityIDGenerator.generateNewId(entityCode);
        insert.setLong(1, id);
        insert.setShort(2, entityCode);
        insert.setLong(3, signerId);
        insert.setLong(4, eduLevelStageId);
        insert.setLong(5, employeePostId);
        insert.executeUpdate();
    }

    private static final Set<String> ORDER_TYPE_TO_SPO_SIGNER_SOPPA;
    private static final Set<String> ORDER_TYPE_TO_SPO_SIGNER_SHUSHIN;
    private static final Set<String> ORDER_TYPE_TO_VPO_SIGNER_SOPPA;
    private static final Set<String> DOCUMENT_TYPE_SIGNER_SHUSHIN;
    private static final Set<String> DOCUMENT_TYPE_SIGNER_SOPPA_SPO;
    private static final Set<String> DOCUMENT_TYPE_SIGNER_SOPPA;

    static
    {
        // FEFU_SPO_SIGNER (Шушин) для СПО
        ORDER_TYPE_TO_SPO_SIGNER_SHUSHIN = new HashSet<>(Arrays.asList(
                // Сборные
                "1.61",     // О восстановлении
                "1.81",     // О восстановлении и допуске к защите ВКР (неявка на защиту)
                "1.83",     // О восстановлении и допуске к защите ВКР (повторная защита)
                "1.77",     // О восстановлении и допуске к подготовке ВКР
                "1.75",     // О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ)
                "1.73",     // О восстановлении и повторном допуске к сдаче государственного экзамена (пересдача ГЭ)
                "1.9",      // О выходе из академического отпуска
                "1.89",     // О выходе из отпуска по беременности и родам
                "1.90",     // О выходе из отпуска по уходу за ребенком
                "1.96",     // О допуске к защите выпускной квалификационной работы
                "1.57",     // О допуске к защите выпускной квалификационной работы (после ГЭ)
                "1.52",     // О допуске к сдаче государственного экзамена
                "1.fefu12", // О зачислении в порядке перевода
                "1.fefu10", // О зачислении на второй и последующий курс
                "1.47",     // О назначении старостой учебной группы
                "1.78",     // О переводе на вакантное бюджетное место
                "1.72",     // О переводе на другие условия освоения
                "1.71",     // О переводе на другие условия освоения и срок освоения
                "1.fefu5",  // О переводе на другую форму освоения
                "1.58",     // О переводе на индивидуальный график обучения
                "1.56",     // О переводе на индивидуальный план обучения
                "1.66",     // О переводе с курса на следующий курс
                "1.67",     // О переводе со специальности на специальность
                "1.68",     // О переводе со специальности на специальность, из одной школы в другую
                "1.70",     // О переводе со специальности на специальность, из филиала в филиал
                "1.69",     // О переводе со специальности на специальность, из школы в филиал
                "1.fefu2",  // О переносе защиты выпускной квалификационной работы
                "1.54",     // О переносе итоговой государственной аттестации
                "1.fefu1",  // О переносе сдачи государственного экзамена
                "1.16",     // О предоставлении академического отпуска
                "1.53",     // О предоставлении каникул
                "1.59",     // О предоставлении отпуска по уходу за ребенком до 1,5 лет
                "1.94",     // О предоставлении отпуска по уходу за ребенком до 3 лет
                "1.fefu3",  // О предоставлении повторного года обучения
                "1.60",     // О предоставлении повторного года обучения с переходом на ФГОС
                "1.85",     // О присвоении степени, выдаче диплома и отчислении в связи с окончанием университета
                "1.93",     // О распределении в группу
                "1.80",     // Об изменении категории обучаемого
                "1.fefu6",  // Об изменении фамилии, имени, отчества
                "1.48",     // Об освобождении от обязанностей старосты учебной группы
                "1.fefu4",  // Об отчислении
                "1.64",     // Об отчислении (ВКР)
                "1.62",     // Об отчислении (государственный экзамен)
                "1.63",     // Об отчислении (недопуск к ВКР)
                "1.95",     // Об установлении индивидуального срока сдачи сессии (досрочная сдача сессии)
                "1.55",     // Об установлении индивидуального срока сдачи сессии (продление сессии)
                // Списочные
                "2.fefu3",  // О допуске к защите выпускной квалификационной работы (списочный)
                "2.27",     // О допуске к защите выпускной квалификационной работы (списочный)
                "2.fefu2",  // О допуске к сдаче государственного экзамена  (списочный)
                "2.26",     // О допуске к сдаче государственного экзамена (списочный)
                "2.28",     // О назначении старостами учебных групп (списочный)
                "2.29",     // О присвоении степени, выдаче диплома и отчислении в связи с окончанием университета (списочный)
                "2.39",     // О переводе из группы в группу (списочный)
                "2.fefu5",  // О переводе с курса на курс (списочный)
                "2.30",     // О переводе с курса на следующий курс (по направлениям подготовки) (списочный)
                "2.fefu6",  // О присвоении квалификации, выдаче диплома и отчислении в связи с окончанием университета (списочный)
                "2.34"      // О распределении студентов 1 курса в группы (списочный)
        ));

        // FEFU_SPO_SIGNER (Соппа) для СПО
        ORDER_TYPE_TO_SPO_SIGNER_SOPPA = new HashSet<>(Arrays.asList(
                // Сборные
                "1.fefu9",  // О возобновлении выплаты социальной стипендии
                "1.fefu7",  // О назначении академической стипендии
                "1.fefu11", // О назначении академической стипендии (вступительные испытания)
                "1.91",     // О назначении надбавки к государственной академической стипендии
                "1.23",     // О назначении социальной стипендии
                "1.12",     // О предоставлении отпуска по беременности и родам
                "1.99",     // О прекращении выплаты академической стипендии
                "1.101",    // О прекращении выплаты надбавки к государственной академической стипендии
                "1.100",    // О прекращении выплаты социальной стипендии
                "1.fefu8",  // О приостановлении выплаты социальной стипендии
                "1.87",     // Об однократном повышении государственной академической стипендии
                "1.86",     // Об оказании материальной помощи
                "1.fefu15", // О зачислении на полное государственное обеспечение
                "1.fefu16", // О полном государственном обеспечении
                // Списочные
                "2.fefu4",  // О назначении академической стипендии (вступительные испытания)
                "2.31",     // О назначении академической стипендии (списочный)
                "2.32",     // О назначении социальной стипендии (списочный)
                "2.fefu11", // О переводе на другую основу оплаты обучения (вакантные бюджетные места)
                "2.41",     // О переводе студентов территориального подразделения (списочный)
                "2.fefu9",  // Об изменении формирующего подразделения (списочный)
                "2.fefu10", // О зачислении на полное государственное обеспечение (списочный)
                "2.35",     // Об однократном повышении государственной академической стипендии (списочный)
                "2.33"      // Об оказании материальной помощи (списочный)
        ));

        // FEFU_VPO_SIGNER (Соппа) для ВПО
        ORDER_TYPE_TO_VPO_SIGNER_SOPPA = new HashSet<>(Arrays.asList(
                // Сборные
                "1.61",     // О восстановлении
                "1.81",     // О восстановлении и допуске к защите ВКР (неявка на защиту)
                "1.83",     // О восстановлении и допуске к защите ВКР (повторная защита)
                "1.77",     // О восстановлении и допуске к подготовке ВКР
                "1.75",     // О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ)
                "1.73",     // О восстановлении и повторном допуске к сдаче государственного экзамена (пересдача ГЭ)
                "1.fefu12", // О зачислении в порядке перевода
                "1.fefu10", // О зачислении на второй и последующий курс
                "1.fefu15", // О зачислении на полное государственное обеспечение
                "1.96",     // О допуске к защите выпускной квалификационной работы
                "1.57",     // О допуске к защите выпускной квалификационной работы(после ГЭ)
                "1.52",     // О допуске к сдаче государственного экзамена
                "1.78",     // О переводе на вакантное бюджетное место
                "1.85",     // О присвоении степени, выдаче диплома и отчислении в связи с окончанием университета
                "1.fefu16", // О полном государственном обеспечении
                // Списочные
                "2.27",     // О допуске к защите выпускной квалификационной работы  (списочный)
                "2.fefu3",  // О допуске к защите выпускной квалификационной работы (списочный)
                "2.26",     // О допуске к сдаче государственного экзамена  (списочный)
                "2.fefu2",  // О допуске к сдаче государственного экзамена (списочный)
                "2.fefu10", // О зачислении на полное государственное обеспечение (списочный)
                "2.fefu11", // О переводе на другую основу оплаты обучения (вакантные бюджетные места)
                "2.fefu6",  // О присвоении квалификации, выдаче диплома и отчислении в связи с окончанием университета
                "2.29"      // О присвоении степени, выдаче диплома и отчислении в связи с окончанием университет
        ));

        // Справки (Шушин) для СПО
        DOCUMENT_TYPE_SIGNER_SHUSHIN = new HashSet<>(Arrays.asList(
                "unifefu.0",     // Справка «Справка по месту предъявления с выводом всех приказов»
                "unifefu.1",     // Справка «Является студентом (с подписью руководителя формирующего подразделения)»
                "unifefu.2",     // Справка «Является студентом (с подписью руководителя формирующего подразделения) (вариант 2)»
                "unifefu.3"      // Справка «Об аттестации (с подписью проректора по учебной работе)»
        ));

        // Справки (Соппа) для СПО
        DOCUMENT_TYPE_SIGNER_SOPPA_SPO = new HashSet<>(Arrays.asList(
                "uni.3"          // Справка «В военкомат»
        ));

        // Справки (Соппа) для ВПО, ДПО, НПО
        DOCUMENT_TYPE_SIGNER_SOPPA = new HashSet<>(Arrays.asList(
                "uni.3",         // Справка «В военкомат»
                "unifefu.0",     // Справка «Справка по месту предъявления с выводом всех приказов»
                "unifefu.3"      // Справка «Об аттестации (с подписью проректора по учебной работе)»
        ));
    }
}