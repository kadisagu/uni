/**
 * GetRegCard.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class GetRegCard  implements java.io.Serializable {
    private java.lang.Integer EDocumentID;

    private java.lang.String ISCode;

    public GetRegCard() {
    }

    public GetRegCard(
           java.lang.Integer EDocumentID,
           java.lang.String ISCode) {
           this.EDocumentID = EDocumentID;
           this.ISCode = ISCode;
    }


    /**
     * Gets the EDocumentID value for this GetRegCard.
     * 
     * @return EDocumentID
     */
    public java.lang.Integer getEDocumentID() {
        return EDocumentID;
    }


    /**
     * Sets the EDocumentID value for this GetRegCard.
     * 
     * @param EDocumentID
     */
    public void setEDocumentID(java.lang.Integer EDocumentID) {
        this.EDocumentID = EDocumentID;
    }


    /**
     * Gets the ISCode value for this GetRegCard.
     * 
     * @return ISCode
     */
    public java.lang.String getISCode() {
        return ISCode;
    }


    /**
     * Sets the ISCode value for this GetRegCard.
     * 
     * @param ISCode
     */
    public void setISCode(java.lang.String ISCode) {
        this.ISCode = ISCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetRegCard)) return false;
        GetRegCard other = (GetRegCard) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.EDocumentID==null && other.getEDocumentID()==null) || 
             (this.EDocumentID!=null &&
              this.EDocumentID.equals(other.getEDocumentID()))) &&
            ((this.ISCode==null && other.getISCode()==null) || 
             (this.ISCode!=null &&
              this.ISCode.equals(other.getISCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEDocumentID() != null) {
            _hashCode += getEDocumentID().hashCode();
        }
        if (getISCode() != null) {
            _hashCode += getISCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetRegCard.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">GetRegCard"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EDocumentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "EDocumentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "ISCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
