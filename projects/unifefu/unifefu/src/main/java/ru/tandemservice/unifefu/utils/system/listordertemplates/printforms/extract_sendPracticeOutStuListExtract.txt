\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
{reason}\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_A}\cell\b0
{studentCategory_A} {course} курса{groupInternal_G} {formativeOrgUnitStrWithTerritorial_G}, {learned_A} {fefuCompensationTypeStr}{fefuShortFastExtendedOptionalText} по {fefuEducationStr_D} по {developForm_DF} форме обучения,{doneEduPlan} {donePractice} {practiceKind} практику за {practiceCourse} курс ({outClassTime}в объеме {practiceDuration}) с {practiceBeginDate} по {practiceEndDate}.\par
Место прохождения практики: {practiceExtOrgUnitStr}.\par
Руководитель практики от {topOu}: {practiceHeaderInner_N}.\par
Руководитель практики от {practiceExtOrgUnit}: {practiceHeaderOut_N}.\par
Установить срок аттестации по итогам практики {attestationDate}.\par
\par
{basicsWord}{listBasics}{basicsPoint}\fi0\cell\row\pard