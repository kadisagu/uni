/*$Id$*/
package ru.tandemservice.unifefu.base.ext.EmployeePost.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.bo.EmployeePost.logic.DefaultEmployeeAdditionalInfo;
import ru.tandemservice.unifefu.entity.ws.FefuEmployeePostExt;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author DMITRY KNYAZEV
 * @since 31.07.2014
 */
public class FefuEmployeeAdditionalInfo extends DefaultEmployeeAdditionalInfo
{
	@Override
	public Integer getRate(Long employeePostId)
	{
		FefuEmployeePostExt employeePostExt = DataAccessServices.dao().getByNaturalId(new FefuEmployeePostExt.NaturalId(getEmployeePost(employeePostId)));

		if (employeePostExt == null ||
				(employeePostExt.getRate() == null) ||
//				(employeePostExt.getRate() > MAX_RATE * MULTIPLIER) ||
				(employeePostExt.getRate() > 100 * 10000) ||
				(employeePostExt.getRate() < 0))
			return 0;

		return employeePostExt.getRate();
	}

    @Override
    public Map<Long, Long> getRateMap(Collection<Long> employeePostIds)
    {
        List<Object[]> list = DataAccessServices.dao().getList(
                new DQLSelectBuilder()
                        .fromEntity(FefuEmployeePostExt.class, "ext")
                        .column(property("ext", FefuEmployeePostExt.employeePost().id()))
                        .column(property("ext", FefuEmployeePostExt.rate()))
                        .where(in(property("ext", FefuEmployeePostExt.employeePost().id()), employeePostIds))
        );

        return list.stream().collect(Collectors.toMap(o -> (Long) o[0], o -> ((Integer) o[1]).longValue()));
    }
}
