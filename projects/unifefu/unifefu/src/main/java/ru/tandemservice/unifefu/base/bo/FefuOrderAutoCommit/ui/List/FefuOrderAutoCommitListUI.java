/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic.FefuOrderAutoCommitDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.ui.Add.FefuOrderAutoCommitAdd;

import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 12.11.2012
 */
public class FefuOrderAutoCommitListUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuOrderAutoCommitList.FEFU_ORDER_AUTO_COMMIT_DS.equals(dataSource.getName()))
        {
            // добавляем значения фильтров с текущей страницы
            Map<String, Object> settingMap = _uiSettings.getAsMap(
                    FefuOrderAutoCommitDSHandler.ORDER_ID,
                    FefuOrderAutoCommitDSHandler.TRANSACTION_ID,
                    FefuOrderAutoCommitDSHandler.SYNCHRONIZE_DATE_FROM,
                    FefuOrderAutoCommitDSHandler.SYNCHRONIZE_DATE_TO,
                    FefuOrderAutoCommitDSHandler.ORDER_NUMBER,
                    FefuOrderAutoCommitDSHandler.ORDER_DATE_FROM,
                    FefuOrderAutoCommitDSHandler.ORDER_DATE_TO,
                    FefuOrderAutoCommitDSHandler.TRANSACTION_STATE,
                    FefuOrderAutoCommitDSHandler.COMMENT_SHORT
            );
            dataSource.putAll(settingMap);
        }
    }

    public void onClickAddCommitOrderList()
    {
        _uiActivation.asRegionDialog(FefuOrderAutoCommitAdd.class).activate();
    }
}