/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrgUnitNumberList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.FefuOrgUnitNumber;

/**
 * @author Nikolay Fedorovskih
 * @since 12.08.2013
 */
@Configuration
public class FefuSettingsOrgUnitNumberList extends BusinessComponentManager
{
    public static final String ORGUNIT_NUMBER_DS = "orgUnitNumberDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ORGUNIT_NUMBER_DS, getOrgUnitNumberDS(), orgUnitNumberDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getOrgUnitNumberDS()
    {
        return columnListExtPointBuilder(ORGUNIT_NUMBER_DS)
                .addColumn(textColumn(FefuOrgUnitNumber.formativeOrgUnit().title().s(), FefuOrgUnitNumber.formativeOrgUnit().title()).order())
                .addColumn(textColumn(FefuOrgUnitNumber.territorialOrgUnit().title().s(), FefuOrgUnitNumber.territorialOrgUnit().title()).order())
                .addColumn(textColumn(FefuOrgUnitNumber.number().s(), FefuOrgUnitNumber.number()).order().formatter(FefuOrgUnitNumber.NUMBER_FORMATTER))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                                        alert(ORGUNIT_NUMBER_DS + ".delete.alert", FefuOrgUnitNumber.formativeOrgUnit().title())))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> orgUnitNumberDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuOrgUnitNumber.class, "e")
                        .order(DQLExpressions.property("e", input.getEntityOrder().getColumnName()), input.getEntityOrder().getDirection());
                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(false).build();
            }
        };
    }
}