/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuImtsaImport;

import org.apache.commons.io.FileUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel;
import ru.tandemservice.unifefu.entity.FefuImtsaImportLog;
import ru.tandemservice.unifefu.entity.FefuImtsaXml;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        EppEduPlanVersionBlock block = get(EppEduPlanVersionBlock.class, model.getId());
        model.setBlock(block);

        boolean secondGosGeneration = block.getEduPlanVersion().getEduPlan().getGeneration().getNumber() == 2;
        model.setSecondGosGeneration(secondGosGeneration);

        model.setCycleSelectModel(new SimplePlanStructureSelectModel(secondGosGeneration ? EppPlanStructureCodes.GOS_CYCLES : EppPlanStructureCodes.FGOS_CYCLES));
        model.setPartSelectModel(new SimplePlanStructureSelectModel(secondGosGeneration ? EppPlanStructureCodes.GOS_COMPONENTS : EppPlanStructureCodes.FGOS_PARTS));
        model.setSaveConfig(true);
    }

    @Override
    public List<FefuImtsaCyclePlanStructureRel> getImtsaCycles(Model model)
    {
        return getList(FefuImtsaCyclePlanStructureRel.class, FefuImtsaCyclePlanStructureRel.gos2(), model.isSecondGosGeneration());
    }

    @Override
    public List<EppPlanStructure> getParts(Model model)
    {
        return getList(EppPlanStructure.class, EppPlanStructure.parent().code(), model.isSecondGosGeneration() ? EppPlanStructureCodes.GOS_COMPONENTS : EppPlanStructureCodes.FGOS_PARTS);
    }

    @Override
    public void updateCycleConfiguration(Model model)
    {
        Map<String, FefuImtsaCyclePlanStructureRel> cyclesMap = new HashMap<>();
        for (FefuImtsaCyclePlanStructureRel rel: getList(FefuImtsaCyclePlanStructureRel.class, FefuImtsaCyclePlanStructureRel.gos2(), model.isSecondGosGeneration()))
        {
            cyclesMap.put(rel.getImtsaCycle(), rel);
        }

        for (Map.Entry<String, EppPlanStructure> cycleEntry: model.getCyclesMap().entrySet())
        {
            String cycle = model.getCycleTitlesMap().containsKey(cycleEntry.getKey()) ? model.getCycleTitlesMap().get(cycleEntry.getKey()) : cycleEntry.getKey();
            EppPlanStructure newPlanStructure = cycleEntry.getValue();
            FefuImtsaCyclePlanStructureRel rel = cyclesMap.get(cycle);
            if (rel == null)
            {
                rel = new FefuImtsaCyclePlanStructureRel();
                rel.setImtsaCycle(cycle);
                rel.setGos2(model.isSecondGosGeneration());
            }

            if (rel.getPlanStructure() == null || !newPlanStructure.getCode().equals(rel.getPlanStructure().getCode()))
            {
                rel.setPlanStructure(newPlanStructure);
            }

            saveOrUpdate(rel);
        }
    }

    public void deleteImtsaImportLog(Long blockId)
    {
        new DQLDeleteBuilder(FefuImtsaImportLog.class).where(eq(property(FefuImtsaImportLog.block().id()), value(blockId))).createStatement(getSession()).execute();
    }

    private class SimplePlanStructureSelectModel extends UniSimpleAutocompleteModel
    {
        private String _parentCode;

        private SimplePlanStructureSelectModel(String parentCode)
        {
            _parentCode = parentCode;
        }

        @Override
        public ListResult findValues(String filter)
        {
            List<EppPlanStructure> planStructureList = new DQLSelectBuilder()
                    .fromEntity(EppPlanStructure.class, "ps")
                    .where(eq(property("ps", EppPlanStructure.parent().code()), value(_parentCode)))
                    .where(likeUpper(property("ps", EppPlanStructure.title()), value(CoreStringUtils.escapeLike(filter))))
                    .createStatement(getSession())
                    .list();

            return new ListResult<>(planStructureList);
        }
    }

    @Override
    public void saveImtsaXml(Model model)
    {
        FefuImtsaXml xml = get(FefuImtsaXml.class, FefuImtsaXml.block(), model.getBlock());
        Integer oldNumber = null;
        if (xml != null)
        {
            oldNumber = xml.getNumber();
            delete(xml);
            getSession().flush();
            getSession().clear();
        }

        File file;
        byte[] xmlTemplate;
        String encoding = model.getEncoding();
        try
        {
            file = File.createTempFile("fefuTemp-", "xml");
            try
            {
                model.getSource().write(file);
                xmlTemplate = FileUtils.readFileToString(file, encoding).getBytes();

            } finally
            {
                file.delete();
            }

        } catch (IOException e)
        {
            throw new ApplicationException("Невозможно сохранить файл XML.");
        }

        xml = new FefuImtsaXml(model.getBlock(), model.getSource().getFileName(), encoding, xmlTemplate, oldNumber == null ? 1 : oldNumber + 1);
        save(xml);
    }
}