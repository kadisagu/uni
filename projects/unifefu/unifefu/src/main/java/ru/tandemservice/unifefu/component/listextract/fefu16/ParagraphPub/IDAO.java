/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu16.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuExcludeStuDPOListExtract, Model>
{
}