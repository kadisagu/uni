/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d102.Add;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseController;

/**
 * @author Alexey Lopatin
 * @since 29.09.2013
 */
public class Controller extends DocumentAddBaseController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        onClickApply(component);
    }
}
