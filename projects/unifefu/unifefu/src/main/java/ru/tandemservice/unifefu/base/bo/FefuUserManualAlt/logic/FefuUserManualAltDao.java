/* $Id: FefuUserManualAltDao.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuUserManualAlt.logic;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.unifefu.entity.FefuUserManual;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Victor Nekrasov
  * @since 28.04.2014
 */
public class FefuUserManualAltDao extends CommonDAO implements IFefuUserManualAltDao
{
    @Override
    public FefuUserManual doSaveOrUpdate(final FefuUserManual entity, final IUploadFile document)
    {
        // если был указан файл на форме, та сохраняем его в объект
        if (document != null)
        {
            try
            {
                final byte[] content = IOUtils.toByteArray(document.getStream());
                entity.setDocument(CommonBaseUtil.buildTemplateDocument(content));
                entity.setFileName(document.getFileName());
            }
            catch (final IOException e)
            {
                throw new ApplicationException("Ошибка обработки файла.", e);
            }
        }

        // если создаем новый объект, то генерим для него код
        if (entity.getId() == null)
        {
            final List<String> useCodeList = new DQLSelectBuilder().fromEntity(FefuUserManual.class, "b").column(property(FefuUserManual.code().fromAlias("b")))
            .createStatement(getSession()).list();

            final String codeStr = "user_";
            int codeInt = 1;
            String code = codeStr + codeInt;
            while (useCodeList.contains(code))
            {
                codeInt++;
                code = codeStr + codeInt;
            }

            entity.setCode(code);
        }

        saveOrUpdate(entity);

        return entity;
    }

    @Override
    public FefuUserManual prepare(final Long id)
    {
        FefuUserManual entity;
        if (id != null) {
            entity = DataAccessServices.dao().getNotNull(FefuUserManual.class, id);
        } else {
            entity = new FefuUserManual();
        }

        entity.setEditDate(new Date());

        return entity;
    }
}
