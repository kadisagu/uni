/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.logic;

import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuOrgUnitNumber;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Демон заполняет номера пустые зачетных книжет и личных дел студентов
 *
 * @author Nikolay Fedorovskih
 * @since 13.08.2013
 */
public class FefuStudentBookNumberDaemonBean extends UniBaseDao implements IFefuStudentBookNumberDaemonBean
{
    public static final String GLOBAL_DAEMON_LOCK = FefuStudentBookNumberDaemonBean.class.getSimpleName() + ".lock";

    public static final SyncDaemon DAEMON = new SyncDaemon(FefuStudentBookNumberDaemonBean.class.getName(), 24 * 60, GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            try
            {
                IFefuStudentBookNumberDaemonBean.instance.get().doFillStudentBookNumbers();
            }
            catch (final Exception t)
            {
                logger.error(t.getMessage(), t);
                Debug.exception(t.getMessage(), t);
            }
        }
    };

    /**
     * Получение максимальных номеров зачеток для набора префиксов
     */
    private void findMaxNumbers(Set<String> prefixSet, Session session, Map<String, MutableInt> resultMap)
    {
        if (!prefixSet.isEmpty())
        {
            int prefixLen = prefixSet.iterator().next().length();

            List<Object[]> maxNumbersListResult = new DQLSelectBuilder().fromEntity(Student.class, "ps")
                    .column(DQLFunctions.substring(property("ps", Student.bookNumber()), 1, prefixLen)) // [0]
                    .column(DQLFunctions.cast(DQLFunctions.max(DQLFunctions.substring(property("ps", Student.bookNumber()), prefixLen + 2, 4)), PropertyType.INTEGER)) // [1] Вычисляем максимальный номер в рамках префикса
                    .where(and(
                            eq(DQLFunctions.length(property("ps", Student.bookNumber())), value(prefixLen + 5)), // Длина должна быть 9 (10 для СПО) символов
                            eq(DQLFunctions.substring(property("ps", Student.bookNumber()), prefixLen + 1, 1), value("-")), // Пятым (шестым для СПО) символом должен быть дефис
                            in(DQLFunctions.substring(property("ps", Student.bookNumber()), 1, prefixLen), prefixSet), // Префикс должен быть из искомого набора
                            gt(DQLFunctions.substring(property("ps", Student.bookNumber()), prefixLen + 2, 4), value("0000")),
                            le(DQLFunctions.substring(property("ps", Student.bookNumber()), prefixLen + 2, 4), value("9999")) // Максимальное значение нужно только среди чисел
                    ))
                    .group(DQLFunctions.substring(property("ps", Student.bookNumber()), 1, prefixLen)) // Группируем по префиксу
                    .createStatement(session).list();

            for (Object[] item : maxNumbersListResult)
            {
                resultMap.put((String) item[0], new MutableInt((Integer) item[1]));
            }
        }
    }

    @Override
    public void doFillStudentBookNumbers()
    {
        final IFormatter<Integer> TWO_DIGIT_FORMATTER = source -> source < 10 ? ("0" + source) : String.valueOf(source);
        final Session session = getSession();

        // Номер должен быть формата «XX[С]YY-NNNN».
        // XX - код подразделения,
        // [С] - русская буква С, вставляется в префикс в случае, если студент учится на СПО. (Квадратрые скобки не ставятся.)
        // YY - две последние цифры года поступления,
        // NNNN - уникальный четырехзначный номер в рамках префикса.

        List<Object[]> listResult = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(property("s", Student.id())) // [0]
                .column(property("s", Student.entranceYear())) // [1] год поступления
                .column(property("n", FefuOrgUnitNumber.number())) // [2] номер подразделения
                .column(property("level", StructureEducationLevels.middle())) // [3] признак СПО
                .joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("s"), "ou")
                .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().fromAlias("ou"), "level")
                        // Джойним номера подразделений
                .joinEntity("s", DQLJoinType.left, FefuOrgUnitNumber.class, "n", and(
                        eq(property("ou", EducationOrgUnit.formativeOrgUnit()), property("n", FefuOrgUnitNumber.formativeOrgUnit())),
                        eq(property("ou", EducationOrgUnit.territorialOrgUnit()), property("n", FefuOrgUnitNumber.territorialOrgUnit()))
                ))
                .where(and(
                        // Нужны только те, у которых не заполнены оба поля
                        isNull(property("s", Student.bookNumber())),
                        isNull(property("s", Student.personalFileNumber())),
                        // Исключаем архивных
                        eq(property("s", Student.archival()), value(Boolean.FALSE)),
                        // Исключаем тех, для кого настройка номера подразделений не настроена
                        isNotNull(property("n", FefuOrgUnitNumber.number()))
                ))
                .order(property("s", Student.personalNumber()))
                .createStatement(session).list();

        if (listResult.isEmpty())
            return;

        Set<String> vpoPrefixes = new HashSet<>();
        Set<String> spoPrefixes = new HashSet<>();
        for (Object[] itemResult : listResult)
        {
            boolean isSPO = (Boolean) itemResult[3];
            // Генерируем префикс номера для конкретного студента
            String prefix = TWO_DIGIT_FORMATTER.format(((Short) itemResult[2]).intValue()) + (isSPO ? "С" : "") + TWO_DIGIT_FORMATTER.format(((Integer) itemResult[1]) % 100);
            if (isSPO)
                spoPrefixes.add(prefix);
            else
                vpoPrefixes.add(prefix);
            itemResult[1] = prefix; // сохраняем префикс вместо года постапления (для экономии памяти)
        }

        // Получаем максимальные номера, имеющиеся в базе, для каждого префикса
        Map<String, MutableInt> prefixMap = new HashMap<>();
        findMaxNumbers(vpoPrefixes, session, prefixMap); // ВПО
        findMaxNumbers(spoPrefixes, session, prefixMap); // СПО

        // Начинаем действо - присваиваем новые номера
        IDQLStatement stmt = new DQLUpdateBuilder(Student.class)
                .set(Student.P_BOOK_NUMBER, parameter(Student.P_BOOK_NUMBER, PropertyType.STRING))
                .set(Student.P_PERSONAL_FILE_NUMBER, parameter(Student.P_PERSONAL_FILE_NUMBER, PropertyType.STRING))
                .where(eq(property(Student.id()), parameter(Student.P_ID, PropertyType.LONG)))
                .createStatement(session);

        for (Object[] itemResult : listResult)
        {
            String prefix = (String) itemResult[1];
            MutableInt maxNumber = prefixMap.get(prefix);
            if (maxNumber == null)
                prefixMap.put(prefix, maxNumber = new MutableInt(1));
            else
                maxNumber.increment();

            if (maxNumber.intValue() > 9999)
                throw new ApplicationException("Для префикса «" + prefix + "» не осталось свободных номеров зачетных книжек.");

            String newNumber = prefix + String.format("-%04d", maxNumber.intValue());

            stmt.setLong(Student.P_ID, (Long) itemResult[0]);
            stmt.setString(Student.P_BOOK_NUMBER, newNumber);
            stmt.setString(Student.P_PERSONAL_FILE_NUMBER, newNumber);
            stmt.execute();
        }
    }
}