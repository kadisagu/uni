/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;

/**
 * @author nvankov
 * @since 12/16/13
 */
@Configuration
public class FefuBrsPpsGradingSumDataReportAdd extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(FefuBrsReportManager.instance().orgUnitDSConfig())
                .addDataSource(FefuBrsReportManager.instance().yearPartDSConfig())
                .addDataSource(FefuBrsReportManager.instance().tutorOUDSConfig())
                .addDataSource(FefuBrsReportManager.instance().cutReportDSConfig())
                .create();
    }
}



    