/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSchedule.ui.OrgUnitPrintFormList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.logic.FefuSchedulePrintFormDSHandler;
import ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;

/**
 * @author nvankov
 * @since 9/25/13
 */
@Configuration
public class FefuScheduleOrgUnitPrintFormList extends BusinessComponentManager
{
    public final static String PRINT_FORM_DS = "schedulePrintFormDS";
    public static final String ADMIN_DS = "adminDS";
    public static final String GROUP_DS = "groupDS";

    @Bean
    public ColumnListExtPoint printFormCL()
    {
        return columnListExtPointBuilder(PRINT_FORM_DS)
                .addColumn(dateColumn("createDate", FefuSchedulePrintFormDSHandler.PROPERTY_CREATE_DATE).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().required(true))
                .addColumn(textColumn("season", FefuSchedulePrintFormDSHandler.PROPERTY_SEASON))
                .addColumn(textColumn("ou", SchedulePrintFormFefuExt.formWithTerrTitle()))
                .addColumn(textColumn("groups", FefuSchedulePrintFormDSHandler.PROPERTY_GROUPS).order().required(true))
                .addColumn(textColumn("admin", SchedulePrintFormFefuExt.admin().titleWithOrgUnitShort()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickDownload").permissionKey("ui:sec.orgUnit_getContentSppSchedulePrintFormTab"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("message:ui.deleteAlert").permissionKey("ui:sec.orgUnit_deleteSppSchedulePrintFormTab"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRINT_FORM_DS, printFormCL(), printFormDSHandler()))
                .addDataSource(selectDS(ADMIN_DS, SppScheduleManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s()))
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler printFormDSHandler()
    {
        return new FefuSchedulePrintFormDSHandler(getName());
    }
}
