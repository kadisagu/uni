/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu12.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.listextract.e34.utils.FinAidAssignExtractWrapper;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public class FefuStuffCompensationExtractWrapper extends FinAidAssignExtractWrapper
{
	private final Double _immediateSum;

    public FefuStuffCompensationExtractWrapper(Person person, Group group, Double compensationSum, Double immediateSum)
    {
        super(person, compensationSum, group);
	    _immediateSum = immediateSum;
    }

	public Double getImmediateSum()
	{
		return _immediateSum;
	}
}