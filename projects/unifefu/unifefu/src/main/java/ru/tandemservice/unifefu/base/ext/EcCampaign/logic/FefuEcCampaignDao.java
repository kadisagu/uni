/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EcCampaign.logic;

import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import ru.tandemservice.uniec.base.bo.EcCampaign.logic.EcCampaignDao;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 21.06.2013
 */
public class FefuEcCampaignDao extends EcCampaignDao
{
    @Override
    public String getUniqueEntrantNumber(EnrollmentCampaign enrollmentCampaign)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), "ecEntrantNumberSync" + enrollmentCampaign.getId(), 500);

        int year = enrollmentCampaign.getEducationYear().getIntValue();
        String xx = String.format("%04d", year % 100);

        String max = new DQLSelectBuilder().fromEntity(Entrant.class, "e")
                .column(DQLFunctions.max(property(Entrant.personalNumber().fromAlias("e"))))
                .where(and(
                        eq(
                                property(Entrant.enrollmentCampaign().fromAlias("e")),
                                value(enrollmentCampaign)
                        ),
                        eq(
                                DQLFunctions.length(property(Entrant.personalNumber().fromAlias("e"))),
                                value(9)
                        ),
                        like(
                                property(Entrant.personalNumber().fromAlias("e")),
                                value(xx + "%")
                        )
                )).createStatement(getSession()).uniqueResult();

        return (max != null) ? String.format("%09d", Integer.valueOf(max) + 1) : xx + "00001";
    }
}