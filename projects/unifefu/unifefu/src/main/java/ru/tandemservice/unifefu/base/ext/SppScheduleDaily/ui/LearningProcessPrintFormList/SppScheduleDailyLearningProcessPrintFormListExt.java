/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleDaily.ui.LearningProcessPrintFormList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.LearningProcessPrintFormList.SppScheduleDailyLearningProcessPrintFormList;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm;

/**
 * @author Igor Belanov
 * @since 05.09.2016
 */
@Configuration
public class SppScheduleDailyLearningProcessPrintFormListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "fefu" + SppScheduleDailyLearningProcessPrintFormListExtUI.class.getSimpleName();

    public static final String ADMIN_DS = "adminDS";

    @Autowired
    private SppScheduleDailyLearningProcessPrintFormList _sppScheduleDailyLearningProcessPrintFormList;

    @Bean
    public ColumnListExtension columnListExtension() {
        return columnListExtensionBuilder(_sppScheduleDailyLearningProcessPrintFormList.printFormCL())
                .addAllAfter("groups")
                .addColumn(textColumn("admin", SppScheduleDailyPrintForm.adminOOP().titleWithOrgUnitShort()))
                .create();
    }

    @Bean
    public PresenterExtension presenterExtension() {
        return presenterExtensionBuilder(_sppScheduleDailyLearningProcessPrintFormList.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, SppScheduleDailyLearningProcessPrintFormListExtUI.class))
                .addDataSource(selectDS(ADMIN_DS, SppScheduleDailyManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s()))
                .create();
    }
}
