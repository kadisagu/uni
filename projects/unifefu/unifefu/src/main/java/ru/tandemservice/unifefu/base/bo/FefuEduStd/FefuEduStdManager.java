/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.logic.FefuEduStandardHandler;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.logic.FefuEduStdDAO;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.logic.IFefuEduStdDAO;

/**
 * @author Alexey Lopatin
 * @since 01.12.2014
 */
@Configuration
public class FefuEduStdManager extends BusinessObjectManager
{
    public static FefuEduStdManager instance()
    {
        return instance(FefuEduStdManager.class);
    }

    @Bean
    public IFefuEduStdDAO dao()
    {
        return new FefuEduStdDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler fefuEppEduStandardHandler()
    {
        return new FefuEduStandardHandler(getName());
    }
}
