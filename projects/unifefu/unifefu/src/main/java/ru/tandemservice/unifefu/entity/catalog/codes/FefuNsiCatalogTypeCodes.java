package ru.tandemservice.unifefu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Справочники НСИ"
 * Имя сущности : fefuNsiCatalogType
 * Файл data.xml : unifefu-catalog.data.xml
 */
public interface FefuNsiCatalogTypeCodes
{
    /** Константа кода (code) элемента : Ученые звания (title) */
    String ACADEMIC_RANK_TYPE = "academicRankType";
    /** Константа кода (code) элемента : Ученые степени (title) */
    String DEGREE_TYPE = "degreeType";
    /** Константа кода (code) элемента : Курс (title) */
    String COURSE_TYPE = "courseType";
    /** Константа кода (code) элемента : Состояние студента (title) */
    String STUDENT_STATUS_TYPE = "studentStatusType";
    /** Константа кода (code) элемента : Категория обучаемого (title) */
    String STUDENT_CATEGORY_TYPE = "studentCategoryType";
    /** Константа кода (code) элемента : Форма обучения (title) */
    String EDUCATION_FORM_TYPE = "educationFormType";
    /** Константа кода (code) элемента : Условие освоения (title) */
    String DEVELOP_CONDITION_TYPE = "developConditionType";
    /** Константа кода (code) элемента : Технология освоения (title) */
    String DEVELOP_TECH_TYPE = "developTechType";
    /** Константа кода (code) элемента : Срок освоения (title) */
    String DEVELOP_PERIOD_TYPE = "developPeriodType";
    /** Константа кода (code) элемента : Вид возмещения затрат (title) */
    String COMPENSATION_TYPE_TYPE = "compensationTypeType";
    /** Константа кода (code) элемента : Степень родства (title) */
    String REL_DEGREE_TYPE = "relDegreeType";
    /** Константа кода (code) элемента : Языки (title) */
    String LANGUAGES_TYPE = "languagesType";
    /** Константа кода (code) элемента : Степень знания языков (title) */
    String LANGUAGE_LEVEL_TYPE = "languageLevelType";
    /** Константа кода (code) элемента : Виды образования (title) */
    String EDUCATION_KIND_TYPE = "educationKindType";
    /** Константа кода (code) элемента : Классификатор специальностей по образованию (title) */
    String OKSO_TYPE = "oksoType";
    /** Константа кода (code) элемента : Виды контактной информации (title) */
    String CONTACT_KIND_TYPE = "contactKindType";
    /** Константа кода (code) элемента : Классификатор стран мира (title) */
    String OKSM_TYPE = "oksmType";
    /** Константа кода (code) элемента : Вид удостоверения (title) */
    String IDENTITY_CARD_KIND_TYPE = "identityCardKindType";
    /** Константа кода (code) элемента : Учреждения, филиалы (title) */
    String ORGANIZATION_TYPE = "organizationType";
    /** Константа кода (code) элемента : Подразделения (title) */
    String DEPARTMENT_TYPE = "departmentType";
    /** Константа кода (code) элемента : Направления подготовки (Образовательные программы) (title) */
    String EDUCATIONAL_PROGRAM_TYPE = "educationalProgramType";
    /** Константа кода (code) элемента : Академические группы (title) */
    String ACADEMIC_GROUP_TYPE = "academicGroupType";
    /** Константа кода (code) элемента : Физическое лицо, Удостоверение личности (title) */
    String HUMAN_TYPE = "humanType";
    /** Константа кода (code) элемента : Обучающиеся (title) */
    String STUDENT_TYPE = "studentType";
    /** Константа кода (code) элемента : Должности (title) */
    String POST_TYPE = "postType";
    /** Константа кода (code) элемента : Контрагенты (title) */
    String CONTRACTOR_TYPE = "contractorType";
    /** Константа кода (code) элемента : Договоры (title) */
    String AGREEMENT_TYPE = "agreementType";
    /** Константа кода (code) элемента : Сотрудники (title) */
    String EMPLOYEE_TYPE = "employeeType";
    /** Константа кода (code) элемента : Контакты (title) */
    String CONTACT_TYPE = "contactType";
    /** Константа кода (code) элемента : Ученые степени физ. лица (title) */
    String HUMAN_DEGREE_TYPE = "humanDegreeType";
    /** Константа кода (code) элемента : Ученые звания физ. лица (title) */
    String HUMAN_ACADEMIC_RANK_TYPE = "humanAcademicRankType";
    /** Константа кода (code) элемента : Роли пользователей (title) */
    String ROLE_TYPE = "roleType";
    /** Константа кода (code) элемента : Роли Физических лиц (title) */
    String HUMAN_ROLE_TYPE = "humanRoleType";

    Set<String> CODES = ImmutableSet.of(ACADEMIC_RANK_TYPE, DEGREE_TYPE, COURSE_TYPE, STUDENT_STATUS_TYPE, STUDENT_CATEGORY_TYPE, EDUCATION_FORM_TYPE, DEVELOP_CONDITION_TYPE, DEVELOP_TECH_TYPE, DEVELOP_PERIOD_TYPE, COMPENSATION_TYPE_TYPE, REL_DEGREE_TYPE, LANGUAGES_TYPE, LANGUAGE_LEVEL_TYPE, EDUCATION_KIND_TYPE, OKSO_TYPE, CONTACT_KIND_TYPE, OKSM_TYPE, IDENTITY_CARD_KIND_TYPE, ORGANIZATION_TYPE, DEPARTMENT_TYPE, EDUCATIONAL_PROGRAM_TYPE, ACADEMIC_GROUP_TYPE, HUMAN_TYPE, STUDENT_TYPE, POST_TYPE, CONTRACTOR_TYPE, AGREEMENT_TYPE, EMPLOYEE_TYPE, CONTACT_TYPE, HUMAN_DEGREE_TYPE, HUMAN_ACADEMIC_RANK_TYPE, ROLE_TYPE, HUMAN_ROLE_TYPE);
}
