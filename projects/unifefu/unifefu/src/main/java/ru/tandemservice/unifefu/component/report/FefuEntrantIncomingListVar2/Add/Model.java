/* $Id$ */
package ru.tandemservice.unifefu.component.report.FefuEntrantIncomingListVar2.Add;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unifefu.component.report.FefuEntrantSubmittedDocumentsReportModel;
import ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report;

import java.util.Date;

/**
 * @author Igor Belanov
 * @since 25.07.2016
 * так как отчёт является копией отчёта "Списки подавших документов",
 * то и наследование идёт от FefuEntrantSubmittedDocumentsReportModel
 */
public class Model extends FefuEntrantSubmittedDocumentsReportModel implements IEnrollmentCampaignModel, IEnrollmentCampaignSelectModel
{
    private FefuEntrantIncomingListVar2Report _report = new FefuEntrantIncomingListVar2Report();

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    public FefuEntrantIncomingListVar2Report getReport()
    {
        return _report;
    }

    public void setReport(FefuEntrantIncomingListVar2Report report)
    {
        _report = report;
    }

    @Override
    public Date getDateFrom()
    {
        return getReport().getDateFrom();
    }

    @Override
    public Date getDateTo()
    {
        return getReport().getDateTo();
    }

    @Override
    public EnrollmentDirection getEnrollmentDirection()
    {
        return getReport().getEnrollmentDirection();
    }

    @Override
    public CompensationType getCompensationType()
    {
        return getReport().getCompensationType();
    }

    @Override
    public boolean isOnlyWithOriginals()
    {
        return false;
    }
}
