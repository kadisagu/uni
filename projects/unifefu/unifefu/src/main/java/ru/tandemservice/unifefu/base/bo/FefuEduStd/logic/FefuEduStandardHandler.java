/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.EppEduStandardManager;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.edustd.ext.EppEduStandard.ui.List.EppEduStandardListExtUI;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt;
import ru.tandemservice.unifefu.entity.catalog.FefuEduStandartGeneration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;

/**
 * @author Ekaterina Zvereva
 * @since 28.05.2015
 */
public class FefuEduStandardHandler extends DefaultSearchDataSourceHandler
{
    public static final String GENERATION_EXT_COLUMN = "generationColumn";
    public static final String CUSTOM_STANDARD_COLUMN = "customStandardColumn";


    public FefuEduStandardHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(EppStateEduStandard.class, "p");
        DQLSelectBuilder dql = orderDescription.buildDQLSelectBuilder().column(property("p"));

        IDataSettings settings = context.get("settings");
        EduProgramKind programKind = settings.get(EppEduStandardManager.BIND_PROGRAM_KIND);
        FefuEduStandartGeneration generation = context.get("generation");

        FilterUtils.applySimpleLikeFilter(dql, "p", EppStateEduStandard.number(), settings.<String>get("number"));
        FilterUtils.applySelectFilter(dql, "p", EppStateEduStandard.programSubject().subjectIndex().programKind(), programKind);
        FilterUtils.applySelectFilter(dql, "p", EppStateEduStandard.programSubject(), settings.get("programSubject"));
        FilterUtils.applySelectFilter(dql, "p", EppStateEduStandard.state(), settings.get("state"));

        IdentifiableWrapper identifiableWrapper = context.get(EppEduStandardListExtUI.CUSTOM_EDU_STANDARD);

        dql.joinEntity("p", DQLJoinType.inner, FefuEppStateEduStandardExt.class, "ext", eq(property("p.id"), property("ext", FefuEppStateEduStandardExt.eduStandard().id())));

        if (identifiableWrapper != null && identifiableWrapper.getId().equals(EppEduStandardListExtUI.YES))
        {
            dql.where(eq(property("ext", FefuEppStateEduStandardExt.customEduStandart()), value(true)));
        }
        if (null != generation)
        {
            dql.where(eqValue(property("ext", FefuEppStateEduStandardExt.eduStandardGen()), generation));
        }
        orderDescription.applyOrder(dql, input.getEntityOrder());

        DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();


        Map<Long, String> generationMap = new HashMap<>();
        Map<Long, Boolean> customStandardMap = new HashMap<>();
        List<FefuEppStateEduStandardExt> stateEduStandardExtList = new DQLSelectBuilder()
                .fromEntity(FefuEppStateEduStandardExt.class, "ext")
                .column(property("ext"))
                .where(in(property("ext", FefuEppStateEduStandardExt.eduStandard().id()), output.getRecordIds()))
                .createStatement(context.getSession()).list();
        for (FefuEppStateEduStandardExt stateEduStandardExt : stateEduStandardExtList)
        {
            generationMap.put(stateEduStandardExt.getEduStandard().getId(), stateEduStandardExt.getEduStandardGen().getTitle());
            customStandardMap.put(stateEduStandardExt.getEduStandard().getId(), stateEduStandardExt.isCustomEduStandart());
        }

        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            wrapper.setProperty(GENERATION_EXT_COLUMN, generationMap.get(wrapper.getId()));
            wrapper.setProperty(CUSTOM_STANDARD_COLUMN, customStandardMap.get(wrapper.getId()));
        }

        return output;
    }
}