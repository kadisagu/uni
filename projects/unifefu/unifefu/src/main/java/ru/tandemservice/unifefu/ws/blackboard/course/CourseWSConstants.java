/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.course;

/**
 * @author Nikolay Fedorovskih
 * @since 28.01.2014
 */
public class CourseWSConstants
{
    public static final int GET_ALL_COURSE_CATEGORY = 0; // Use filter.templateCategories.available
    public static final int GET_ALL_COURSES = 0; // Supported filter type values which must be specified for loading course related objects
    public static final int GET_ALL_ORG_CATEGORY = 1;
    public static final int GET_CATEGORY_BY_ID = 2; // Use filter.templateCategories.id, filter.templateCategories.available and filter.templateCategories.organization
    public static final int GET_CATEGORY_BY_PARENT_ID = 3; // Use filter.templateCategories.parentId, filter.templateCategories.available and filter.templateCategories.organization
    public static final int GET_CATEGORY_MEMBERSHIP_BY_COURSE_ID = 2; // Use filter.templateCategoryMembership.courseId, filter.templateCategoryMembership.available and filter.templateCategoryMembership.organization
    public static final int GET_CATEGORY_MEMBERSHIP_BY_ID = 1; // Use filter.templateCategoryMembership.id, filter.templateCategoryMembership.available and filter.templateCategoryMembership.organization
    public static final int GET_COURSE_BY_BATCHUID = 2;
    public static final int GET_COURSE_BY_CATEGORY_ID = 4;
    public static final int GET_COURSE_BY_COURSEID = 1;
    public static final int GET_COURSE_BY_ID = 3;
    public static final int GET_COURSE_BY_SEARCH_KEYVALUE = 5;
    public static final int GET_ENROLLED_GROUP_BY_USER_ID = 3; // Use filter.userIds and courseIdVO
    public static final int GET_GROUP_BY_COURSE_ID = 2; // Use courseIdVO
    public static final int GET_GROUP_BY_ID = 1; // Use filter.groupIds and courseIdVO


    public static final String COURSEWS001 = "Course.WS001"; // Failed to fetch the user in session.
    public static final String COURSEWS002 = "Course.WS002"; // Failed to fetch the course in session.
    public static final String COURSEWS003 = "Course.WS003"; // Failed to save a course or org group ,either invalid data or appropriate permission not available for user
    public static final String COURSEWS004 = "Course.WS004"; // Failed to delete a course or org group, either invalid data or appropriate permission not available for user
    public static final String COURSEWS005 = "Course.WS005"; // Failed to load requested course or org groups
    public static final String COURSEWS006 = "Course.WS006"; // Failed to save staff information
    public static final String COURSEWS007 = "Course.WS007"; // Failed to save a course or org category ,either invalid data or appropriate permission not available for user
    public static final String COURSEWS008 = "Course.WS008"; // Failed to delete a course or org category, either invalid data or appropriate permission not available for user
    public static final String COURSEWS009 = "Course.WS009"; // Failed to load requested course or org categories
    public static final String COURSEWS010 = "Course.WS010"; // Failed to save a course or org category membership ,either invalid data or appropriate permission not available for user
    public static final String COURSEWS011 = "Course.WS011"; // Failed to delete a course or org category membership, either invalid data or appropriate permission not available for user
    public static final String COURSEWS012 = "Course.WS012"; // Failed to load requested course or org category memberships
    public static final String COURSEWS013 = "Course.WS013"; // Failed to load classifications
    public static final String COURSEWS014 = "Course.WS014"; // Failed to save a course or org, either invalid data or appropriate permission not available for user
    public static final String COURSEWS015 = "Course.WS015"; // Failed to delete a course or org, either invalid data or appropriate permission not available for user
    public static final String COURSEWS016 = "Course.WS016"; // Failed to save changes to the banner image file
}