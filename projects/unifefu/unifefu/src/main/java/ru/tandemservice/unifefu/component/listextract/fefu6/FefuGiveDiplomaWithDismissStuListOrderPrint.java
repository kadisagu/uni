/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e85.GiveDiplomaStuExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.unifefu.component.listextract.fefu6.utils.FefuGiveDiplomaWithDismissParagraphPartPartWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu6.utils.FefuGiveDiplomaWithDismissParagraphPartWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu6.utils.FefuGiveDiplomaWithDismissParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaSuccessWithDismissStuListExtract;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author nvankov
 * @since 7/2/13
 */
public class FefuGiveDiplomaWithDismissStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    public static final String SUB_PARAGRAPH_CONTENT = "SUB_PARAGRAPH_CONTENT";

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);

        AbstractStudentExtract firstExtract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(order.getId());
        EducationLevels educationLevels;
        if (firstExtract instanceof FefuGiveDiplomaWithDismissStuListExtract)
        {
            FefuGiveDiplomaWithDismissStuListExtract extract = (FefuGiveDiplomaWithDismissStuListExtract) firstExtract;
            educationLevels = extract.getEducationLevelsHighSchool().getEducationLevel();
        }
        else
        {
            FefuGiveDiplomaSuccessWithDismissStuListExtract extract = (FefuGiveDiplomaSuccessWithDismissStuListExtract) firstExtract;
            educationLevels = extract.getEducationLevelsHighSchool().getEducationLevel();
        }

        injectModifier.put("eduLevelTypeStr_P", CommonExtractPrint.getEduLevelTypeStr_P(educationLevels));

        switch (educationLevels.getSafeQCode())
        {
            case QualificationsCodes.BAKALAVR:
            case QualificationsCodes.MAGISTR:
                injectModifier.put("graduate_G", "степени");
                break;
            case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
            case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
            case QualificationsCodes.SPETSIALIST:
                injectModifier.put("graduate_G", "квалификации");
                break;
            default:
                injectModifier.put("graduate_G", "__________");
                break;
        }

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, prepareParagraphsStructure(order));

        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    protected List<FefuGiveDiplomaWithDismissParagraphWrapper> prepareParagraphsStructure(StudentListOrder order)
    {
        int ind;
        final List<FefuGiveDiplomaWithDismissParagraphWrapper> paragraphWrapperList = new ArrayList<>();

        final List<FefuGiveDiplomaWithDismissStuListExtract> woSuccessExtracts = new ArrayList<>();
        final List<FefuGiveDiplomaSuccessWithDismissStuListExtract> withSuccessExtracts = new ArrayList<>();
        MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, false).forEach(e -> {
            if (e instanceof FefuGiveDiplomaWithDismissStuListExtract) {
                woSuccessExtracts.add((FefuGiveDiplomaWithDismissStuListExtract)  e);
            } else if (e instanceof FefuGiveDiplomaSuccessWithDismissStuListExtract) {
                withSuccessExtracts.add((FefuGiveDiplomaSuccessWithDismissStuListExtract) e);
            } else {
                throw new IllegalStateException();
            }
        });

        for (FefuGiveDiplomaWithDismissStuListExtract extract : woSuccessExtracts)
        {
            Student student = extract.getEntity();
            // обычный диплом
            EducationLevels educationLevels = extract.getEducationLevelsHighSchool().getEducationLevel();
            StructureEducationLevels levelType = educationLevels.getLevelType();
            if (levelType != null && (levelType.isSpecialization() || levelType.isProfile()))
                educationLevels = educationLevels.getParentLevel();

            FefuGiveDiplomaWithDismissParagraphWrapper paragraphWrapper = new FefuGiveDiplomaWithDismissParagraphWrapper(
                    student.getStudentCategory(),
                    student.getCompensationType(),
                    extract.getCourse(),
                    extract.getDevelopForm(),
                    extract.getDevelopCondition(),
                    extract.getDevelopTech(),
                    extract.getDevelopPeriod(),
                    educationLevels,
                    student.getEducationOrgUnit().getFormativeOrgUnit(),
                    student.getEducationOrgUnit().getTerritorialOrgUnit(),
                    CommonListOrderPrint.getEducationBaseText(student.getGroup()),
                    extract.isPrintDiplomaQualification(),
                    extract.getEducationLevelsHighSchool().getAssignedQualification(),
                    extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            FefuGiveDiplomaWithDismissParagraphPartWrapper paragraphPartWrapper = new FefuGiveDiplomaWithDismissParagraphPartWrapper(Boolean.FALSE, extract);
            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
            {
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
                paragraphPartWrapper.setParagraphWrapper(paragraphWrapper);
            }
            else
            {
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);
            }

            FefuGiveDiplomaWithDismissParagraphPartPartWrapper paragraphPartPartDataWrapper = new FefuGiveDiplomaWithDismissParagraphPartPartWrapper(student.getEducationOrgUnit().getEducationLevelHighSchool(), extract);
            ind = paragraphPartWrapper.getParagraphPartPartWrapperList().indexOf(paragraphPartPartDataWrapper);
            if (ind == -1)
                paragraphPartWrapper.getParagraphPartPartWrapperList().add(paragraphPartPartDataWrapper);
            else
                paragraphPartPartDataWrapper = paragraphPartWrapper.getParagraphPartPartWrapperList().get(ind);

            Person person = student.getPerson();

            if (!paragraphPartPartDataWrapper.getPersonList().contains(person))
                paragraphPartPartDataWrapper.getPersonList().add(person);

        }

        for (FefuGiveDiplomaSuccessWithDismissStuListExtract extract : withSuccessExtracts)
        {
            Student student = extract.getEntity();
            EducationLevels educationLevels = extract.getEducationLevelsHighSchool().getEducationLevel();
            StructureEducationLevels levelType = educationLevels.getLevelType();
            if (levelType != null && (levelType.isSpecialization() || levelType.isProfile()))
                educationLevels = educationLevels.getParentLevel();

            FefuGiveDiplomaWithDismissParagraphWrapper paragraphDataWrapper = new FefuGiveDiplomaWithDismissParagraphWrapper(
                    student.getStudentCategory(),
                    student.getCompensationType(),
                    extract.getCourse(),
                    extract.getDevelopForm(),
                    extract.getDevelopCondition(),
                    extract.getDevelopTech(),
                    extract.getDevelopPeriod(),
                    educationLevels,
                    student.getEducationOrgUnit().getFormativeOrgUnit(),
                    student.getEducationOrgUnit().getTerritorialOrgUnit(),
                    CommonListOrderPrint.getEducationBaseText(student.getGroup()),
                    extract.isPrintDiplomaQualification(),
                    extract.getEducationLevelsHighSchool().getAssignedQualification(),
                    extract);
            ind = paragraphWrapperList.indexOf(paragraphDataWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphDataWrapper);
            else
                paragraphDataWrapper = paragraphWrapperList.get(ind);

            FefuGiveDiplomaWithDismissParagraphPartWrapper paragraphPartDataWrapper = new FefuGiveDiplomaWithDismissParagraphPartWrapper(Boolean.TRUE, extract);
            ind = paragraphDataWrapper.getParagraphPartWrapperList().indexOf(paragraphPartDataWrapper);
            if (ind == -1)
                paragraphDataWrapper.getParagraphPartWrapperList().add(paragraphPartDataWrapper);
            else
                paragraphPartDataWrapper = paragraphDataWrapper.getParagraphPartWrapperList().get(ind);

            FefuGiveDiplomaWithDismissParagraphPartPartWrapper paragraphPartPartDataWrapper = new FefuGiveDiplomaWithDismissParagraphPartPartWrapper(student.getEducationOrgUnit().getEducationLevelHighSchool(), extract);
            ind = paragraphPartDataWrapper.getParagraphPartPartWrapperList().indexOf(paragraphPartPartDataWrapper);
            if (ind == -1)
                paragraphPartDataWrapper.getParagraphPartPartWrapperList().add(paragraphPartPartDataWrapper);
            else
                paragraphPartPartDataWrapper = paragraphPartDataWrapper.getParagraphPartPartWrapperList().get(ind);

            Person person = student.getPerson();

            if (!paragraphPartPartDataWrapper.getPersonList().contains(person))
                paragraphPartPartDataWrapper.getPersonList().add(person);
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<FefuGiveDiplomaWithDismissParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            int parNumber = 0;
            for (FefuGiveDiplomaWithDismissParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_GIVE_GENERAL_DIPLOMA_WITH_DISMISS_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());

                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, paragraphWrapper.getDevelopForm(), "");

                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, paragraphWrapper.getCompensationType(), "", false);

                paragraphInjectModifier.put("highSchoolShortTitle",
                                            paragraphWrapper.getTerritorialOrgUnit().getOrgUnitType().isRootType() ? " " + TopOrgUnit.getInstance().getShortTitle() : "");

                CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());

                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), paragraphWrapper.getFormativeOrgUnit(), paragraphWrapper.getTerritorialOrgUnit(), "formativeOrgUnitStr", "");

                paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());

                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), new String[]{"fefuEducationStrDirection", "educationStrDirection"}, false);

                EducationLevels educationLevels = paragraphWrapper.getEducationLevels();
                String qCode = educationLevels.getSafeQCode();
                final EduProgramQualification qualification = paragraphWrapper.getQualification();
                final String assignedQualificationTitle = qualification != null ? qualification.getTitle().toUpperCase() : "______________________________";
                StringBuilder diplomaQualificationAward = new StringBuilder();
                boolean isGos2 = educationLevels.getLevelType().isGos2();

                switch (qCode)
                {
                    case QualificationsCodes.BAKALAVR:
                    case QualificationsCodes.MAGISTR:
                        paragraphInjectModifier.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);

                        if (paragraphWrapper.isPrintDiplomaQualification() && !isGos2)
                        {
                            diplomaQualificationAward.append(" с присвоением специального звания ");
                            diplomaQualificationAward.append(assignedQualificationTitle);
                        }

                        break;
                    case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
                    case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
                    case QualificationsCodes.SPETSIALIST:
                        paragraphInjectModifier.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);
                        break;
                    default:
                        paragraphInjectModifier.put("graduateWithLevel_A", "______________________________");
                        break;
                }

                paragraphInjectModifier.put("parNumber", paragraphWrappers.size() > 1 ? (++parNumber + ". ") : "");
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), paragraphWrapper.getEduBaseText(), "fefuShortFastExtendedOptionalText");
                paragraphInjectModifier.put("diplomaQualificationAward", diplomaQualificationAward.toString());

                paragraphInjectModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<FefuGiveDiplomaWithDismissParagraphPartWrapper> paragraphPartDataWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartDataWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (FefuGiveDiplomaWithDismissParagraphPartWrapper paragraphPartWrapper : paragraphPartDataWrappers)
            {
                if (paragraphPartWrapper.getDiplomaWithSuccess())
                {
                    byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_GIVE_DIPLOMA_WITH_HONOURS_WITH_DISMISS_LIST_EXTRACT), 3);
                    RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                    // Вносим необходимые метки
                    RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, paragraphPartWrapper.getFirstExtract());

                    EducationLevelsHighSchool educationLevelsHighSchool = paragraphPartWrapper.getParagraphPartPartWrapperList().get(0).getEducationLevelsHighSchool();
                    modifyEducationStrProfile(paragraphPartInjectModifier, educationLevelsHighSchool);

                    paragraphPartInjectModifier.put("diplomaName_G", GiveDiplomaStuExtractPrint.getDiplomaName_G(educationLevelsHighSchool.getEducationLevel().getSafeQCode()));

                    EducationLevels educationLevels = educationLevelsHighSchool.getEducationLevel();

                    paragraphPartInjectModifier.put("eduLevelTypeStr_P", CommonExtractPrint.getEduLevelTypeStr_P(educationLevels));
                    paragraphPartInjectModifier.modify(paragraphPart);

                    // Вставляем список подподпараграфов
                    injectSubSubParagraphs(paragraphPart, paragraphPartWrapper.getParagraphPartPartWrapperList());

                    IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                    rtfGroup.setElementList(paragraphPart.getElementList());
                    parList.add(rtfGroup);

                }
                else
                {
                    byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_GIVE_GENERAL_DIPLOMA_WITH_DISMISS_LIST_EXTRACT), 3);
                    RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                    // Вносим необходимые метки
                    RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, paragraphPartWrapper.getFirstExtract());

                    EducationLevelsHighSchool educationLevelsHighSchool = paragraphPartWrapper.getParagraphPartPartWrapperList().get(0).getEducationLevelsHighSchool();
                    modifyEducationStrProfile(paragraphPartInjectModifier, educationLevelsHighSchool);

                    paragraphPartInjectModifier.put("diplomaName_G", GiveDiplomaStuExtractPrint.getDiplomaName_G(educationLevelsHighSchool.getEducationLevel().getSafeQCode()));

                    EducationLevels educationLevels = educationLevelsHighSchool.getEducationLevel();
                    paragraphPartInjectModifier.put("eduLevelTypeStr_P", CommonExtractPrint.getEduLevelTypeStr_P(educationLevels));
                    paragraphPartInjectModifier.modify(paragraphPart);

                    // Вставляем список подподпараграфов
                    injectSubSubParagraphs(paragraphPart, paragraphPartWrapper.getParagraphPartPartWrapperList());

                    IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                    rtfGroup.setElementList(paragraphPart.getElementList());
                    parList.add(rtfGroup);
                }
            }

            // полученный документ (набор подпараграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubSubParagraphs(RtfDocument paragraphPart, List<FefuGiveDiplomaWithDismissParagraphPartPartWrapper> paragraphPartPartWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraphPart, SUB_PARAGRAPH_CONTENT);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (FefuGiveDiplomaWithDismissParagraphPartPartWrapper paragraphPartPartWrapper : paragraphPartPartWrappers)
            {
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_GIVE_GENERAL_DIPLOMA_WITH_DISMISS_LIST_EXTRACT), 4);
                RtfDocument paragraphPartPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, paragraphPartPartWrapper.getFirstExtract());
                modifyFefuEducationStrProfile(paragraphPartInjectModifier, paragraphPartPartWrapper.getEducationLevelsHighSchool());

                List<Person> personList = paragraphPartPartWrapper.getPersonList();
                RtfString rtfString = new RtfString();

                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);

                int i = 0;

                for (; i < personList.size() - 1; i++)
                {
                    rtfString.append(String.valueOf(i + 1)).append(". ").append(personList.get(i).getFullFio()).par();
                }

                rtfString.append(String.valueOf(i + 1)).append(". ").append(personList.get(i).getFullFio());

                paragraphPartInjectModifier.put("STUDENT_LIST", rtfString);
                paragraphPartInjectModifier.modify(paragraphPartPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPartPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор подподпараграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected static void modifyFefuEducationStrProfile(RtfInjectModifier modifier, EducationLevelsHighSchool educationLevelsHighSchool)
    {
        StructureEducationLevels levels = educationLevelsHighSchool.getEducationLevel().getLevelType();
        RtfString fefuEducationStrProfile = new RtfString();
        if (levels.isSpecialization() || levels.isProfile())
        {
            fefuEducationStrProfile.append(levels.isProfile() ? levels.isBachelor() ? "Профиль «" : "Магистерская программа «" : "Специализация «");
            fefuEducationStrProfile.append(educationLevelsHighSchool.getTitle() + "»").par();
        }
        modifier.put("fefuEducationStrProfile", fefuEducationStrProfile);
    }

    protected void modifyEducationStrProfile(RtfInjectModifier modifier, EducationLevelsHighSchool educationLevelsHighSchool)
    {
        modifier.put("fefuEducationStrProfile", "");
        modifier.put("educationStrProfile", "");
        if (educationLevelsHighSchool == null)
            return;

        StructureEducationLevels levelType = educationLevelsHighSchool.getEducationLevel().getLevelType();
        String[] eduTypeCases;

        if (levelType.isSpecialization()) // специализация
            eduTypeCases = CommonExtractPrint.SPECIALIZATION_CASES;
        else if (levelType.isBachelor() && levelType.isProfile()) // бакалаврский профиль
            eduTypeCases = CommonExtractPrint.PROFILE_CASES;
        else if (levelType.isMaster() && levelType.isProfile()) // магистерский профиль
            eduTypeCases = CommonExtractPrint.MASTER_PROGRAM_CASES;
        else
            return;

        String educationStrProfile,
                profile = " «" + educationLevelsHighSchool.getTitle() + "»",
                postfix;

        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            educationStrProfile = StringUtils.capitalize(eduTypeCases[i]) + profile;
            postfix = UniRtfUtil.CASE_POSTFIX.get(i);
            modifier.put("fefuEducationStrProfile" + postfix, educationStrProfile);
            modifier.put("educationStrProfile" + postfix, educationStrProfile);
        }
    }
}
