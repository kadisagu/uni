/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e63.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.e63.AddEdit.Model;

/**
 * @author nvankov
 * @since 6/18/13
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e63.AddEdit.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if(!model.isEditForm())
        {
            model.getExtract().setPassStateExam(true);
        }
    }
}
