/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.RegistryTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.component.registry.base.Pub.RegistryElementPubContext;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.base.bo.Blackboard.BlackboardManager;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.BBCourseDSHandler;
import ru.tandemservice.unifefu.base.bo.Blackboard.ui.AddCourseToRegElement.BlackboardAddCourseToRegElement;
import ru.tandemservice.unifefu.base.bo.Blackboard.ui.AddCourseToRegElement.BlackboardAddCourseToRegElementUI;

import java.util.Arrays;

/**
 * @author Nikolay Fedorovskih
 * @since 04.03.2014
 */
@Input({
               @Bind(key = RegistryElementPubContext.REGISTRY_ELEMENT_PUB_CONTEXT_PARAM, binding = "context", required = true)
       })
public class BlackboardRegistryTabUI extends UIPresenter
{
    private RegistryElementPubContext _context;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(BBCourseDSHandler.REG_ELEMENT_PARAM, getContext().getElement());
        PpsEntry ppsEntry = getSettings().get("ppsEntry");
        if (ppsEntry != null)
        {
            dataSource.put(BBCourseDSHandler.PPS_ENTRY_LIST_FILTER_PARAM, Arrays.asList(ppsEntry));
        }
        dataSource.putAll(getSettings().getAsMap(BBCourseDSHandler.COURSE_TITLE_FILTER_PARAM,
                                                 BBCourseDSHandler.COURSE_ID_FILTER_PARAM,
                                                 BBCourseDSHandler.SHOW_ARCHIVE_FILTER_PARAM));
    }

    public void onClickAddCourse()
    {
        _uiActivation.asRegionDialog(BlackboardAddCourseToRegElement.class)
                .parameter(BlackboardAddCourseToRegElementUI.REGISTRY_ELEMENT_CONTEXT_PARAM, _context.getElement())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        BlackboardManager.instance().dao().deleteCourse2EppRegElementRelation(getListenerParameterAsLong(), getContext().getElement().getId());
    }

    public RegistryElementPubContext getContext()
    {
        return _context;
    }

    public EppRegistryElement getElement()
    {
        return _context.getElement();
    }

    public void setContext(RegistryElementPubContext context)
    {
        _context = context;
    }
}