/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu11;

import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentExtract;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * Проект сборного приказа "О назначении академической стипендии (вступительные испытания)"
 *
 * @author Nikolay Fedorovskih
 * @since 25.04.2013
 */
public class FefuAcadGrantAssignStuEnrolmentExtractDao extends UniBaseDao implements IExtractComponentDao<FefuAcadGrantAssignStuEnrolmentExtract>
{
    public void doCommit(FefuAcadGrantAssignStuEnrolmentExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (orderData == null)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getAcadGrantAssignmentOrderDate());
            extract.setPrevOrderNumber(orderData.getAcadGrantAssignmentOrderNumber());
        }

        orderData.setAcadGrantAssignmentOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setAcadGrantAssignmentOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setAcadGrantPaymentDateFrom(extract.getBeginDate());
        orderData.setAcadGrantPaymentDateTo(extract.getEndDate());
        getSession().saveOrUpdate(orderData);

        //выплата
        FefuStudentPayment payment = new FefuStudentPayment();
        payment.setExtract(extract);
        payment.setStopOrPauseOrder(true);
        payment.setStartDate(extract.getBeginDate());
        payment.setStopDate(extract.getEndDate());
        payment.setPaymentSum(extract.getGrantSize());
        if(null != extract.getReason())
            payment.setReason(extract.getReason().getTitle());
        save(payment);

        if(extract.isGroupManagerBonus())
        {
            FefuStudentPayment groupManagerPayment = new FefuStudentPayment();
            groupManagerPayment.setExtract(extract);
            groupManagerPayment.setStopOrPauseOrder(true);
            groupManagerPayment.setStartDate(extract.getGroupManagerBonusBeginDate());
            groupManagerPayment.setStopDate(extract.getGroupManagerBonusEndDate());
            groupManagerPayment.setPaymentSum(extract.getGroupManagerBonusSize());
            if(null != extract.getReason())
                groupManagerPayment.setReason(extract.getReason().getTitle());
            groupManagerPayment.setComment("Надбавка старосте");
            save(groupManagerPayment);
        }
    }

    public void doRollback(FefuAcadGrantAssignStuEnrolmentExtract extract, Map parameters)
    {
        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }

        orderData.setAcadGrantAssignmentOrderDate(extract.getPrevOrderDate());
        orderData.setAcadGrantAssignmentOrderNumber(extract.getPrevOrderNumber());
        orderData.setAcadGrantPaymentDateFrom(null);
        orderData.setAcadGrantPaymentDateTo(null);
        getSession().saveOrUpdate(orderData);

        // Удаляем выплаты
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuStudentPayment.class);
        deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(FefuStudentPayment.extract()), DQLExpressions.value(extract)));
        deleteBuilder.createStatement(getSession()).execute();
    }
}