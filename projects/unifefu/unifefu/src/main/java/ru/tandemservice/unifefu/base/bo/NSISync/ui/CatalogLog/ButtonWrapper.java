/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.CatalogLog;

/**
 * @author Dmitry Seleznev
 * @since 08.04.2014
 */
public class ButtonWrapper
{
    private String _caption;
    private String _listener;
    private boolean _disabled;

    public ButtonWrapper(String caption, String listener)
    {
        _caption = caption;
        _listener = listener;
        _disabled = false;
    }

    public ButtonWrapper(String caption, String listener, boolean disabled)
    {
        _caption = caption;
        _listener = listener;
        _disabled = disabled;
    }

    public String getCaption()
    {
        return _caption;
    }

    public void setCaption(String caption)
    {
        _caption = caption;
    }

    public String getListener()
    {
        return _listener;
    }

    public void setListener(String listener)
    {
        _listener = listener;
    }

    public boolean isDisabled()
    {
        return _disabled;
    }

    public void setDisabled(boolean disabled)
    {
        _disabled = disabled;
    }
}