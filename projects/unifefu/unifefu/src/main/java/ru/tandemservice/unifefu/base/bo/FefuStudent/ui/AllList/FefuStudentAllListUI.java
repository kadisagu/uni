/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.ui.AllList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.logic.IEducationOrgUnitContextHandler;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.BaseStudentListContextHandler;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.List.UniStudentListUI;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.FefuEduProgramEducationOrgUnitAddon;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.FefuLevelTypeFilter;
import ru.tandemservice.unifefu.base.ext.UniStudent.UniStudentExtManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 03.11.2013
 */
public class FefuStudentAllListUI extends UniStudentListUI
{
    @Override
    public void onComponentRefresh()
    {

        super.onComponentRefresh();
        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (util != null)
        {
            UniStudentExtManager.configEduUtil(util, getSettingsKey());
            initDefaults();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS))
        {
            dataSource.put(UniStudentExtManager.PROP_DEVELOP_FORM, getSettings().get(UniStudentExtManager.PROP_DEVELOP_FORM));
            dataSource.put(UniStudentExtManager.PROP_BASE_EDU, getSettings().get(UniStudentExtManager.PROP_BASE_EDU));
            dataSource.put(FefuStudentManager.FEFU_STUDENT_FIO, getSettings().get(FefuStudentManager.FEFU_STUDENT_FIO));
            dataSource.put(FefuStudentManager.FEFU_ARCHIVAL, getSettings().get(FefuStudentManager.FEFU_ARCHIVAL));
            dataSource.put(FefuStudentManager.FEFU_TYPE_MATCH, getSettings().get(FefuStudentManager.FEFU_TYPE_MATCH));
            dataSource.put(FefuStudentManager.FEFU_LEARN_FVO, getSettings().get(FefuStudentManager.FEFU_LEARN_FVO));
            dataSource.put(FefuStudentManager.FEFU_LEARN_UVC, getSettings().get(FefuStudentManager.FEFU_LEARN_UVC));
        }
        else if (dataSource.getName().equals(FefuStudentManager.FEFU_STUDENT_FIO_DS))
        {
            dataSource.put(FefuStudentManager.FEFU_ARCHIVAL, getSettings().get(FefuStudentManager.FEFU_ARCHIVAL));
        }
        else if (dataSource.getName().equals(UniStudentManger.GROUP_DS))
             dataSource.put("archival", getSettings().get(FefuStudentManager.FEFU_ARCHIVAL) == null? null : ((DataWrapper)getSettings().get(FefuStudentManager.FEFU_ARCHIVAL)).getId().equals(FefuStudentManager.ARCHIVAL_YES));

    }

    @Override
    protected IEducationOrgUnitContextHandler getContextHandler()
    {
        if (getSettings().get(FefuStudentManager.FEFU_ARCHIVAL) != null) {
            Boolean archival = ((DataWrapper)getSettings().get(FefuStudentManager.FEFU_ARCHIVAL)).getId().equals(FefuStudentManager.ARCHIVAL_YES);
            return new BaseStudentListContextHandler(archival);
        } else {
            return null;
        }
    }

    @Override
    public String getSettingsKey()
    {
        return "FefuStudentAllList.filter";
    }

    @Override
    public void onClickClear()
    {
        super.onClickClear();
        initDefaults();
    }

    private void initDefaults()
    {
        FefuEduProgramEducationOrgUnitAddon util = (FefuEduProgramEducationOrgUnitAddon) _uiConfig.getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (util != null)
        {
            util.configRequiredFilters(FefuEduProgramEducationOrgUnitAddon.FEFU_LEVEL_TYPE);
        }

        if (getSettings().get(FefuLevelTypeFilter.LEVEL_TYPE_FILTER_NAME) == null)
        {
            List<StructureEducationLevels> levelsList = new ArrayList<>(2);
            levelsList.add(DataAccessServices.dao().get(StructureEducationLevels.class, StructureEducationLevels.P_CODE, StructureEducationLevelsCodes.HIGH_GOS3_GROUP));
            levelsList.add(DataAccessServices.dao().get(StructureEducationLevels.class, StructureEducationLevels.P_CODE, StructureEducationLevelsCodes.HIGH_GOS2_GROUP));

            if (util != null)
            {
                util.setCurrentFilter(FefuEduProgramEducationOrgUnitAddon.FEFU_LEVEL_TYPE);
                util.setCurrentValues(levelsList);
            }
            getSettings().set(FefuLevelTypeFilter.LEVEL_TYPE_FILTER_NAME, levelsList);
            getSettings().save();
        }

        if (getSettings().get(FefuStudentManager.FEFU_TYPE_MATCH) == null)
        {
            getSettings().set(FefuStudentManager.FEFU_TYPE_MATCH, FefuStudentManager.TYPE_MATCH_PARTIAL);
            getSettings().save();
        }
    }
}