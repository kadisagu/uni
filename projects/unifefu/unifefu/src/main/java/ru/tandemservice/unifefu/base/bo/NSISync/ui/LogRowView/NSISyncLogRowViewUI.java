/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.LogRowView;

import com.google.common.collect.Maps;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.component.catalog.CommonBaseCatalogDefine;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.render.RichTableListRenderUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import ru.tandemservice.unifefu.base.bo.NSISync.NSISyncManager;
import ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow;
import ru.tandemservice.unifefu.tapestry.formatter.FefuCrazyXmlFormatter;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Dmitry Seleznev
 * @since 04.12.2013
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "logRowId")
})
public class NSISyncLogRowViewUI extends UIPresenter
{
    private Long _logRowId;
    private FefuNsiLogRow _logRow;

    @Override
    public void onComponentRefresh()
    {
        _logRow = DataAccessServices.dao().getNotNull(FefuNsiLogRow.class, _logRowId);
    }

    public String getMessageBodyFormatted() throws Exception
    {
        return getXmlFormatted(_logRow.getMessageBody());
    }

    public String getRoutingHeaderFormatted() throws Exception
    {
        return getXmlFormatted(_logRow.getRoutingHeader()).replaceAll("x-datagram", "header");
    }

    public String getSoapFaultFormatted()
    {
        return null == _logRow.getSoapFault() ? null : FefuCrazyXmlFormatter.INSTANCE.format(_logRow.getSoapFault());
    }

    public String getXmlFormatted(String src) throws Exception
    {
        if (null == src || src.trim().length() == 0) return null;

        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(src));
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(is);

        DOMSource domSource = new DOMSource(doc);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "Utf8");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);
        String result = FefuCrazyXmlFormatter.INSTANCE.format(sw.toString());

        int idx = result.indexOf("&lt;ID&gt;");
        int endIdx = 0;

        Set<String> guidSet = new HashSet<>();

        while (idx > -1)
        {
            endIdx = result.indexOf("&lt;/ID&gt;", idx);
            if (endIdx > 0) guidSet.add(result.substring(idx + 10, endIdx));
            idx = result.indexOf("&lt;ID&gt;", idx + 1);
        }

        Map<String, Long> guidToRealObjectIdMap = NSISyncManager.instance().dao().getGuidToRealObjectIdMap(guidSet);
        for (Map.Entry<String, Long> entry : guidToRealObjectIdMap.entrySet())
        {
            Map<String, Object> parameters = Maps.newHashMap();
            parameters.put(IUIPresenter.PUBLISHER_ID, entry.getValue());
            String componentName = RichTableListRenderUtils.getComponentName(null, parameters);

            if (null == componentName)
            {
                Class clazz = EntityRuntime.getMeta(entry.getValue()).getEntityClass();
                if (clazz.newInstance() instanceof ICatalogItem)
                {
                    String publisherItem = CommonManager.catalogComponentProvider().getItemPubComponent(clazz);
                    if (null == publisherItem) publisherItem = CommonBaseCatalogDefine.DEFAULT_CATALOG_ITEM_PUB;
                    componentName = publisherItem;
                }
            }

            if (null != componentName)
            {
                String link = CoreServices.linkFactoryService().getExternalLink(null, componentName, parameters, null);
                if (null != link)
                    result = result.replaceAll("&lt;ID&gt;" + entry.getKey() + "&lt;/ID&gt;", "&lt;ID&gt;<a href=\"" + link + "\">" + entry.getKey() + "</a>&lt;/ID&gt;");
            }
        }

        return result;
    }

    public void onSendMessageAgain()
    {
        NsiReactorUtils.executeNSIAction(getLogRowId(), true);
    }

    public void onClickGetPack()
    {
        NSISyncManager.instance().dao().getNsiCatalogLogRowPackFile(getLogRowId());
    }

    public Long getLogRowId()
    {
        return _logRowId;
    }

    public void setLogRowId(Long logRowId)
    {
        _logRowId = logRowId;
    }

    public FefuNsiLogRow getLogRow()
    {
        return _logRow;
    }

    public void setLogRow(FefuNsiLogRow logRow)
    {
        _logRow = logRow;
    }
}