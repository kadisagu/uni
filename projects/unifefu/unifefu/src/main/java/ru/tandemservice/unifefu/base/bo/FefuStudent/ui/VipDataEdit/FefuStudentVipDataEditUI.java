/**
 *$Id: FefuStudentVipDataEditUI.java 38584 2014-10-08 11:05:36Z azhebko $
 */
package ru.tandemservice.unifefu.base.bo.FefuStudent.ui.VipDataEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.entity.PersonFefuExt;

/**
 * @author Alexander Shaburov
 * @since 30.08.12
 */
@Input({
    @Bind(key="studentId", binding="studentId", required=true)
})
public class FefuStudentVipDataEditUI extends UIPresenter
{
    // fields
    Long _studentId;
    Student _student;
    PersonFefuExt _personFefuExt;
    boolean _vip;

    @Override
    public void onComponentRefresh()
    {
        _student = DataAccessServices.dao().get(_studentId);
        _personFefuExt = FefuStudentManager.instance().fefuStudentVipDataDao().preparePersonFefuExt(_student.getPerson());
        _vip = _personFefuExt.getVipDate() != null;
    }

    public void onClickApply()
    {
        FefuStudentManager.instance().fefuStudentVipDataDao().update(_personFefuExt, _vip);

        deactivate();
    }

    // Getters & Setters

    public boolean isVip()
    {
        return _vip;
    }

    public void setVip(boolean vip)
    {
        _vip = vip;
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public PersonFefuExt getPersonFefuExt()
    {
        return _personFefuExt;
    }

    public void setPersonFefuExt(PersonFefuExt personFefuExt)
    {
        _personFefuExt = personFefuExt;
    }
}
