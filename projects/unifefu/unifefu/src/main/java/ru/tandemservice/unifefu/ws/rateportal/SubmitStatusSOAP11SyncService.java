/**
 * SubmitStatusSOAP11SyncService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportal;

public interface SubmitStatusSOAP11SyncService extends javax.xml.rpc.Service {
    public java.lang.String getSubmitStatusSOAP11SyncPortAddress();

    public ru.tandemservice.unifefu.ws.rateportal.SubmitStatus getSubmitStatusSOAP11SyncPort() throws javax.xml.rpc.ServiceException;

    public ru.tandemservice.unifefu.ws.rateportal.SubmitStatus getSubmitStatusSOAP11SyncPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
