/* $Id: FefuUserManualAltPermissionModifier.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.entity.CommonSecurityObject;
import org.tandemframework.sec.meta.ClassGroupMeta;
import org.tandemframework.sec.meta.ModuleGlobalGroupMeta;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.base.entity.UserManual;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;

import java.util.List;
import java.util.Map;

/**
 * @author Victor Nekrasov
 * @since 28.04.2014
 */

public class FefuUserManualAltPermissionModifier  implements ISecurityConfigMetaMapModifier
{
    @Override
        public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
        {
            final SecurityConfigMeta config = new SecurityConfigMeta();
            config.setModuleName("unifefu");
            config.setName("unifefu-fefuUserManualAlt-sec-config");
            config.setTitle("");

            final ModuleGlobalGroupMeta mgg = PermissionMetaUtil.createModuleGlobalGroup(config, "systemEntitiesMGG", "Служебные объекты");
            final ClassGroupMeta permissionClassGroup = PermissionMetaUtil.createClassGroup(mgg, "fefuUserManualAltCG", "Объект «Инструкция (ДВФУ)»", CommonSecurityObject.class.getName());

            final PermissionGroupMeta objectPermissionGroup = PermissionMetaUtil.createPermissionGroup(config, "fefuUserManualAltPermissionGroup", "Объект «Инструкция (ДВФУ)»");
            PermissionMetaUtil.createPermission(objectPermissionGroup, "menuFefuUserManualAltList", "Просмотр списка инструкций");
            PermissionMetaUtil.createPermission(objectPermissionGroup, "fefuUserManualAlt_admin", "Администрирование списка инструкций");
            PermissionMetaUtil.createPermission(objectPermissionGroup, "fefuUserManualAlt_add", "Добавление инструкции");
            PermissionMetaUtil.createPermission(objectPermissionGroup, "fefuUserManualAlt_edit", "Редактирование инструкции");
            PermissionMetaUtil.createPermission(objectPermissionGroup, "fefuUserManualAlt_delete", "Удаление инструкции");
            PermissionMetaUtil.createGroupRelation(config, objectPermissionGroup.getName(), permissionClassGroup.getName());

            final PermissionGroupMeta downloadPermissionGroup = PermissionMetaUtil.createPermissionGroup(config, "FefuUserManualAltDownloadPermissionGroup", "Скачивание инструкций");
            PermissionMetaUtil.createGroupRelation(config, downloadPermissionGroup.getName(), permissionClassGroup.getName());
            List<UserManual> list = DataAccessServices.dao().getList(UserManual.class, UserManual.title().s());
            for (UserManual userManual : list)
            {
                PermissionMetaUtil.createPermission(downloadPermissionGroup, "fefuUserManualAlt_documentDownload_" + userManual.getCode(), "Скачивание инструкции «" + userManual.getTitle() + "»");
            }

            securityConfigMetaMap.put(config.getName(), config);
        }
}

