/* $Id$ */
package ru.tandemservice.unifefu.component.student.LogView;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuStudentContract;
import ru.tandemservice.unifefu.entity.ws.FefuContractToPersonRel;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 18.10.2013
 */
public class DAO extends ru.tandemservice.uni.component.log.EntityLogViewBase.DAO
{
    private static final List<String> _entityClassNames = Arrays.asList(
            Student.ENTITY_CLASS,                     // Студент
            Person.ENTITY_CLASS,                      // Персона
            PersonContactData.ENTITY_CLASS,           // Контактные данные персоны
            IdentityCard.ENTITY_CLASS,                // Удостоверение личности
            PersonEduInstitution.ENTITY_CLASS,        // Документ о полученном образовании
            PersonBenefit.ENTITY_CLASS,               // Льгота для персоны
            PersonNextOfKin.ENTITY_CLASS,             // Ближайший родственник
            PersonSportAchievement.ENTITY_CLASS,      // Спортивное достижение
            PersonForeignLanguage.ENTITY_CLASS,       // Иностранный язык персоны
            Address.ENTITY_CLASS,                     // Адрес
            PersonMilitaryStatus.ENTITY_CLASS,        // Сведения о воинском учете персоны
            Principal.ENTITY_CLASS,                   // Принципал
            FefuStudentContract.ENTITY_CLASS          // Договоры ДВФУ
    );

    @Override
    protected List<String> getEntityClassNames()
    {
        return _entityClassNames;
    }

    @Override
    protected Collection<Long> getEntityIds(ru.tandemservice.uni.component.log.EntityLogViewBase.Model model)
    {
        Student student = getNotNull(Student.class, model.getEntityId());
        PersonMilitaryStatus militaryStatus = get(PersonMilitaryStatus.class, PersonMilitaryStatus.person(), student.getPerson());

        List<Long> studentFefuContractIds = new DQLSelectBuilder()
                .fromEntity(FefuStudentContract.class, "c")
                .column(property(FefuStudentContract.id().fromAlias("c")))
                .where(
                        exists(FefuContractToPersonRel.class,
                               FefuContractToPersonRel.L_CONTRACT, property(FefuStudentContract.id().fromAlias("c")),
                               FefuContractToPersonRel.L_PERSON, student.getPerson()))
                .order(property(FefuStudentContract.contractDate().fromAlias("c")), OrderDirection.desc)
                .createStatement(getSession())
                .<Long>list();
        Set<Long> entityIds = new HashSet<>();
        entityIds.add(student.getId());
        entityIds.add(student.getPerson().getId());
        entityIds.add(student.getPerson().getIdentityCard().getId());
        entityIds.add(student.getPerson().getContactData().getId());

        if (studentFefuContractIds.size() > 0)
            entityIds.addAll(studentFefuContractIds);
        if (militaryStatus != null)
            entityIds.add(militaryStatus.getId());

        return entityIds;
    }
}
