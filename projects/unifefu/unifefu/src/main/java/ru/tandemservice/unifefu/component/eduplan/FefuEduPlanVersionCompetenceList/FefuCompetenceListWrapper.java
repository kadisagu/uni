/**
 * $Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionCompetenceList;

import com.google.common.collect.ComparisonChain;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.IRawFormatter;
import ru.tandemservice.uni.entity.catalog.IColoredEntity;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

import java.util.*;


/**
 * @author Alexander Zhebko
 * @since 22.02.2013
 */
public class FefuCompetenceListWrapper extends IdentifiableWrapper<FefuCompetence>
{
    public static final Comparator<FefuCompetenceListWrapper> COMPETENCE_COMPARATOR =
            (o1, o2) -> ComparisonChain.start()
                    .compare(o1.getCompetenceGroupShortTitle(), o2.getCompetenceGroupShortTitle())
                    .compare(o1.getCompetenceNumber(), o2.getCompetenceNumber())
                    .compare(o1.getCompetenceTitle(), o2.getCompetenceTitle())
                    .compare(o1.getId(), o2.getId())
                    .result();
   public static final IRawFormatter<Collection<RowWrapper>> FEFU_EPV_ROW_FORMATTER = source -> {
        if (source == null || source.isEmpty())
        {
            return "";
        }

        StringBuilder tableBuilder = new StringBuilder("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
        for (RowWrapper rowWrapper : source)
        {
            tableBuilder.append("<tr><td style=\"white-space: nowrap;\"><div style=\"color:");
            tableBuilder.append(rowWrapper.getHtmlColor());
            tableBuilder.append("\">");
            tableBuilder.append(rowWrapper.getRowTitle());
            tableBuilder.append("</div></td></tr>");
        }
        tableBuilder.append("</table>");
        return tableBuilder.toString();
    };
    public static final String COMPETENCE_TITLE = "competenceTitle";
    public static final String COMPETENCE_CODE = "competenceCode";
    public static final String DISCIPLINE_TITLES = "disciplineTitles";

    private String _competenceGroupShortTitle;
    private Integer _competenceNumber;
    private Set<RowWrapper> _epvRows = new TreeSet<>(Comparator.comparing(RowWrapper::getRowTitle));

    public FefuCompetenceListWrapper(FefuCompetence competence, Integer competenceNumber)
    {
        super(competence.getId(), competence.getTitle());
        _competenceGroupShortTitle = competence.getEppSkillGroup().getShortTitle();
        _competenceNumber = competenceNumber;
    }

    public void addRow(EppEpvRegistryRow row, String htmlColor)
    {
        _epvRows.add(new RowWrapper(row, htmlColor));
    }

    public void addRows(FefuCompetenceListWrapper wrapper)
    {
        if (wrapper == null) return;
        _epvRows.addAll(wrapper._epvRows);
    }

    public String getCompetenceTitle()
    {
        return getTitle();
    }

    public String getCompetenceCode()
    {
        return _competenceGroupShortTitle + "-" + String.valueOf(_competenceNumber);
    }

    public Integer getCompetenceNumber()
    {
        return _competenceNumber;
    }

    public String getCompetenceGroupShortTitle()
    {
        return _competenceGroupShortTitle;
    }

    public Set<RowWrapper> getDisciplineTitles()
    {
        return _epvRows;
    }

    static private class RowWrapper implements IColoredEntity
    {
        private String _rowTitle;
        private String _htmlColor;

        private RowWrapper(EppEpvRegistryRow row, String htmlColor)
        {
            _rowTitle = row.getTitle();
            if (row.getRegistryElementType().getCode().equals(EppRegistryStructureCodes.REGISTRY_DISCIPLINE))
                _rowTitle += " (" + row.getRegistryElementType().getTitle() + ")";
            _htmlColor = htmlColor;
        }

        public String getRowTitle()
        {
            return _rowTitle;
        }

        @Override
        public String getHtmlColor()
        {
            return _htmlColor;
        }
    }
}