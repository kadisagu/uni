package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x8_9to10 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuAdditionalProfessionalEducationProgram

		// создана новая сущность
        if (!tool.tableExists("fefu_ape_program_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_ape_program_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("formativeorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("territorialorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("educationorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("specialtygroup_p", DBType.createVarchar(255)), 
				new DBColumn("programopeningdate_p", DBType.DATE), 
				new DBColumn("programclosingdate_p", DBType.DATE), 
				new DBColumn("ownerorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("diplomaqualification_id", DBType.LONG), 
				new DBColumn("diplomatype_id", DBType.LONG), 
				new DBColumn("testunits_p", DBType.createVarchar(255)), 
				new DBColumn("hours_p", DBType.createVarchar(255)), 
				new DBColumn("fgoscycle_p", DBType.createVarchar(255)), 
				new DBColumn("programstatus_id", DBType.LONG).setNullable(false), 
				new DBColumn("programpublicationstatus_id", DBType.LONG).setNullable(false), 
				new DBColumn("openingprogramorderdetails_p", DBType.createVarchar(255)), 
				new DBColumn("educationlevel_id", DBType.LONG), 
				new DBColumn("developform_id", DBType.LONG), 
				new DBColumn("developcondition_id", DBType.LONG), 
				new DBColumn("developtech_id", DBType.LONG), 
				new DBColumn("developperiod_id", DBType.LONG), 
				new DBColumn("issueddocument_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuAdditionalProfessionalEducationProgram");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDevelopConditionApe

		// создана новая сущность
        if (!tool.tableExists("fefu_c_develop_condition_ape_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_c_develop_condition_ape_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuDevelopConditionApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDevelopFormApe

		// создана новая сущность
        if (!tool.tableExists("fefu_c_develop_form_ape_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_c_develop_form_ape_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuDevelopFormApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDevelopPeriodApe

		// создана новая сущность
        if (!tool.tableExists("fefu_c_develop_period_ape_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_c_develop_period_ape_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuDevelopPeriodApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDevelopTechApe

		// создана новая сущность
        if (!tool.tableExists("fefu_c_develop_tech_ape_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_c_develop_tech_ape_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuDevelopTechApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDiplomaTypeApe

		// создана новая сущность
        if (!tool.tableExists("fefu_c_diploma_type_ape_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_c_diploma_type_ape_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuDiplomaTypeApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuIssuedDocumentApe

		// создана новая сущность
        if (!tool.tableExists("fefu_c_issued_document_ape_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_c_issued_document_ape_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuIssuedDocumentApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuProgramPublicationStatusApe

		// создана новая сущность
        if (!tool.tableExists("fefu_c_pr_public_status_ape_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_c_pr_public_status_ape_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuProgramPublicationStatusApe");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuProgramStatusApe

		// создана новая сущность
        if (!tool.tableExists("fefu_c_program_status_ape_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_c_program_status_ape_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuProgramStatusApe");

		}
    }
}