/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu22;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.unifefu.entity.FefuPerformConditionCourseTransferListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentCustomStateCICodes;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 29.06.2016
 */
public class FefuPerformConditionCourseTransferListExtractDao extends UniBaseDao implements IExtractComponentDao<FefuPerformConditionCourseTransferListExtract>
{
    @Override
    @SuppressWarnings("Duplicates")
    public void doCommit(FefuPerformConditionCourseTransferListExtract extract, Map parameters)
    {
        // save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        // возьмём доп.статус "условный перевод"
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "cs")
                .where(eq(property("cs", StudentCustomState.student().id()), value(extract.getEntity().getId())))
                .where(eq(property("cs", StudentCustomState.customState().code()), value(StudentCustomStateCICodes.USLOVNYY_PEREVOD)));
        List<StudentCustomState> studentCustomStateList = builder.createStatement(getSession()).list();
        // нам нужен тот статус "условный перевод", в котором ещё не проставлена дата окончания действия статуса:
        StudentCustomState studentCustomState = null;
        for (StudentCustomState cs : studentCustomStateList)
        {
            if (cs.getEndDate() == null)
            {
                studentCustomState = cs;
                break;
            }
        }
        if (studentCustomState != null)
        {
            Date stateBeginDate = studentCustomState.getBeginDate();
            Date extractBeginDate = extract.getBeginDate();
            // ставим дату окончания действия статуса
            // (= начало действия выписки или = начало действия статуса (если дата выписки оказалась меньше))
            studentCustomState.setEndDate(stateBeginDate.after(extractBeginDate) ? stateBeginDate : extractBeginDate);
            UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(studentCustomState);
            // так же нужно связать с выпиской
            // попытаемся найти уже имеющуюся связь выписки со статусом
            DQLSelectBuilder stateToExtractRelationBuilder = new DQLSelectBuilder().fromEntity(StudentCustomStateToExtractRelation.class, "se")
                    .where(eq(property("se", StudentCustomStateToExtractRelation.extract().id()), value(extract.getId())))
                    .where(eq(property("se", StudentCustomStateToExtractRelation.studentCustomState().id()), value(studentCustomState.getId())));
            List<StudentCustomStateToExtractRelation> stateToExtractRelationList = stateToExtractRelationBuilder.createStatement(getSession()).list();
            // создать новую связь если таких связей ещё не было
            if (stateToExtractRelationList.isEmpty())
            {
                StudentCustomStateToExtractRelation extractRelation = new StudentCustomStateToExtractRelation();
                extractRelation.setStudentCustomState(studentCustomState);
                extractRelation.setExtract(extract);
                save(extractRelation);
            }
        }
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void doRollback(FefuPerformConditionCourseTransferListExtract extract, Map parameters)
    {
        // вытащим нужную связь доп.статуса студента с выпиской
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateToExtractRelation.class, "se")
                .where(eq(property("se", StudentCustomStateToExtractRelation.extract().id()), value(extract.getId())));
        List<StudentCustomStateToExtractRelation> stateToExtractRelationList = builder.createStatement(getSession()).list();
        StudentCustomStateToExtractRelation stateToExtractRelation = !stateToExtractRelationList.isEmpty() ? stateToExtractRelationList.get(0) : null;
        if (stateToExtractRelation != null && stateToExtractRelation.getStudentCustomState() != null)
        {
            StudentCustomState studentCustomState = stateToExtractRelation.getStudentCustomState();
            studentCustomState.setEndDate(null);
            UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(studentCustomState);
        }
    }
}
