/* $Id$ */
package ru.tandemservice.unifefu.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEppRegistryModuleALoadExt;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEppRegistryModuleELoad;

/**
 * @author nvankov
 * @since 2/7/14
 */
public class FefuLoadVO extends DataWrapper
{
    EppRegistryModuleALoad _aLoad;
    FefuEppRegistryModuleELoad _eLoad;
    FefuEppRegistryModuleALoadExt _fefuLoad;

    public FefuLoadVO(Long loadTypeId, String loadTypeTitle, EppRegistryModuleALoad aLoad)
    {
        super(loadTypeId, loadTypeTitle);
        _aLoad = aLoad;
    }

    public FefuLoadVO(Long loadTypeId, String loadTypeTitle, FefuEppRegistryModuleELoad eLoad)
    {
        super(loadTypeId, loadTypeTitle);
        _eLoad = eLoad;
    }

    public FefuLoadVO(Long loadTypeId, String loadTypeTitle, FefuEppRegistryModuleALoadExt fefuLoad)
    {
        super(loadTypeId, loadTypeTitle);
        _fefuLoad = fefuLoad;
    }

    public EppRegistryModuleALoad getALoad()
    {
        return _aLoad;
    }

    public FefuEppRegistryModuleELoad getELoad()
    {
        return _eLoad;
    }

    public FefuEppRegistryModuleALoadExt getFefuLoad()
    {
        return _fefuLoad;
    }

    public String getValue()
    {
        if(null != _aLoad)
            return UniEppUtils.formatLoad(_aLoad.getLoadAsDouble(), true);
        else if(null != _eLoad)
            return UniEppUtils.formatLoad(_eLoad.getLoadAsDouble(), true);
        else if(null != _fefuLoad)
            return UniEppUtils.formatLoad(_fefuLoad.getLoadAsDouble(), true);
        else return null;
    }

    public Double getLoadValue()
    {
        if(null != _aLoad)
            return _aLoad.getLoadAsDouble();
        else if(null != _eLoad)
            return _eLoad.getLoadAsDouble();
        else if(null != _fefuLoad)
            return _fefuLoad.getLoadAsDouble();
        else return null;
    }

    public void setLoadValue(final Double value) {
        if(null != _aLoad)
            _aLoad.setLoadAsDouble(value);
        else if(null != _eLoad)
            _eLoad.setLoadAsDouble(value);
        else if(null != _fefuLoad)
            _fefuLoad.setLoadAsDouble(value);
    }

    public boolean isDisabled()
    {
        return !isCanBeModified();
    }

    public boolean isCanBeModified()
    {
        return null == _aLoad;
    }

}
