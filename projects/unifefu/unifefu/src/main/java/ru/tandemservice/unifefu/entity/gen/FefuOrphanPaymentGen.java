package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выплаты детям-сиротам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuOrphanPaymentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuOrphanPayment";
    public static final String ENTITY_NAME = "fefuOrphanPayment";
    public static final int VERSION_HASH = 269481442;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRINT = "print";
    public static final String P_PAYMENT_SUM = "paymentSum";
    public static final String P_PAYMENT_SUM_PRINT = "paymentSumPrint";
    public static final String P_PAYMENT_MONTH = "paymentMonth";
    public static final String P_PAYMENT_YEAR = "paymentYear";
    public static final String P_PERIOD_START_DATE = "periodStartDate";
    public static final String P_PERIOD_END_DATE = "periodEndDate";
    public static final String L_TYPE = "type";
    public static final String L_EXTRACT = "extract";

    private String _print;     // Вывод на печать
    private Long _paymentSum;     // Сумма выплаты (в сотых долях копейки)
    private String _paymentSumPrint;     // Сумма выплаты прописью
    private Integer _paymentMonth;     // Месяц
    private Integer _paymentYear;     // Год
    private Date _periodStartDate;     // За период с
    private Date _periodEndDate;     // За период по
    private FefuOrphanPaymentType _type;     // Тип выплаты
    private AbstractStudentExtract _extract;     // Выписка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вывод на печать.
     */
    @Length(max=255)
    public String getPrint()
    {
        return _print;
    }

    /**
     * @param print Вывод на печать.
     */
    public void setPrint(String print)
    {
        dirty(_print, print);
        _print = print;
    }

    /**
     * @return Сумма выплаты (в сотых долях копейки).
     */
    public Long getPaymentSum()
    {
        return _paymentSum;
    }

    /**
     * @param paymentSum Сумма выплаты (в сотых долях копейки).
     */
    public void setPaymentSum(Long paymentSum)
    {
        dirty(_paymentSum, paymentSum);
        _paymentSum = paymentSum;
    }

    /**
     * @return Сумма выплаты прописью.
     */
    @Length(max=255)
    public String getPaymentSumPrint()
    {
        return _paymentSumPrint;
    }

    /**
     * @param paymentSumPrint Сумма выплаты прописью.
     */
    public void setPaymentSumPrint(String paymentSumPrint)
    {
        dirty(_paymentSumPrint, paymentSumPrint);
        _paymentSumPrint = paymentSumPrint;
    }

    /**
     * @return Месяц.
     */
    public Integer getPaymentMonth()
    {
        return _paymentMonth;
    }

    /**
     * @param paymentMonth Месяц.
     */
    public void setPaymentMonth(Integer paymentMonth)
    {
        dirty(_paymentMonth, paymentMonth);
        _paymentMonth = paymentMonth;
    }

    /**
     * @return Год.
     */
    public Integer getPaymentYear()
    {
        return _paymentYear;
    }

    /**
     * @param paymentYear Год.
     */
    public void setPaymentYear(Integer paymentYear)
    {
        dirty(_paymentYear, paymentYear);
        _paymentYear = paymentYear;
    }

    /**
     * @return За период с.
     */
    public Date getPeriodStartDate()
    {
        return _periodStartDate;
    }

    /**
     * @param periodStartDate За период с.
     */
    public void setPeriodStartDate(Date periodStartDate)
    {
        dirty(_periodStartDate, periodStartDate);
        _periodStartDate = periodStartDate;
    }

    /**
     * @return За период по.
     */
    public Date getPeriodEndDate()
    {
        return _periodEndDate;
    }

    /**
     * @param periodEndDate За период по.
     */
    public void setPeriodEndDate(Date periodEndDate)
    {
        dirty(_periodEndDate, periodEndDate);
        _periodEndDate = periodEndDate;
    }

    /**
     * @return Тип выплаты. Свойство не может быть null.
     */
    @NotNull
    public FefuOrphanPaymentType getType()
    {
        return _type;
    }

    /**
     * @param type Тип выплаты. Свойство не может быть null.
     */
    public void setType(FefuOrphanPaymentType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Выписка. Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка. Свойство не может быть null.
     */
    public void setExtract(AbstractStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuOrphanPaymentGen)
        {
            setPrint(((FefuOrphanPayment)another).getPrint());
            setPaymentSum(((FefuOrphanPayment)another).getPaymentSum());
            setPaymentSumPrint(((FefuOrphanPayment)another).getPaymentSumPrint());
            setPaymentMonth(((FefuOrphanPayment)another).getPaymentMonth());
            setPaymentYear(((FefuOrphanPayment)another).getPaymentYear());
            setPeriodStartDate(((FefuOrphanPayment)another).getPeriodStartDate());
            setPeriodEndDate(((FefuOrphanPayment)another).getPeriodEndDate());
            setType(((FefuOrphanPayment)another).getType());
            setExtract(((FefuOrphanPayment)another).getExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuOrphanPaymentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuOrphanPayment.class;
        }

        public T newInstance()
        {
            return (T) new FefuOrphanPayment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "print":
                    return obj.getPrint();
                case "paymentSum":
                    return obj.getPaymentSum();
                case "paymentSumPrint":
                    return obj.getPaymentSumPrint();
                case "paymentMonth":
                    return obj.getPaymentMonth();
                case "paymentYear":
                    return obj.getPaymentYear();
                case "periodStartDate":
                    return obj.getPeriodStartDate();
                case "periodEndDate":
                    return obj.getPeriodEndDate();
                case "type":
                    return obj.getType();
                case "extract":
                    return obj.getExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "print":
                    obj.setPrint((String) value);
                    return;
                case "paymentSum":
                    obj.setPaymentSum((Long) value);
                    return;
                case "paymentSumPrint":
                    obj.setPaymentSumPrint((String) value);
                    return;
                case "paymentMonth":
                    obj.setPaymentMonth((Integer) value);
                    return;
                case "paymentYear":
                    obj.setPaymentYear((Integer) value);
                    return;
                case "periodStartDate":
                    obj.setPeriodStartDate((Date) value);
                    return;
                case "periodEndDate":
                    obj.setPeriodEndDate((Date) value);
                    return;
                case "type":
                    obj.setType((FefuOrphanPaymentType) value);
                    return;
                case "extract":
                    obj.setExtract((AbstractStudentExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "print":
                        return true;
                case "paymentSum":
                        return true;
                case "paymentSumPrint":
                        return true;
                case "paymentMonth":
                        return true;
                case "paymentYear":
                        return true;
                case "periodStartDate":
                        return true;
                case "periodEndDate":
                        return true;
                case "type":
                        return true;
                case "extract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "print":
                    return true;
                case "paymentSum":
                    return true;
                case "paymentSumPrint":
                    return true;
                case "paymentMonth":
                    return true;
                case "paymentYear":
                    return true;
                case "periodStartDate":
                    return true;
                case "periodEndDate":
                    return true;
                case "type":
                    return true;
                case "extract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "print":
                    return String.class;
                case "paymentSum":
                    return Long.class;
                case "paymentSumPrint":
                    return String.class;
                case "paymentMonth":
                    return Integer.class;
                case "paymentYear":
                    return Integer.class;
                case "periodStartDate":
                    return Date.class;
                case "periodEndDate":
                    return Date.class;
                case "type":
                    return FefuOrphanPaymentType.class;
                case "extract":
                    return AbstractStudentExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuOrphanPayment> _dslPath = new Path<FefuOrphanPayment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuOrphanPayment");
    }
            

    /**
     * @return Вывод на печать.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPrint()
     */
    public static PropertyPath<String> print()
    {
        return _dslPath.print();
    }

    /**
     * @return Сумма выплаты (в сотых долях копейки).
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPaymentSum()
     */
    public static PropertyPath<Long> paymentSum()
    {
        return _dslPath.paymentSum();
    }

    /**
     * @return Сумма выплаты прописью.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPaymentSumPrint()
     */
    public static PropertyPath<String> paymentSumPrint()
    {
        return _dslPath.paymentSumPrint();
    }

    /**
     * @return Месяц.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPaymentMonth()
     */
    public static PropertyPath<Integer> paymentMonth()
    {
        return _dslPath.paymentMonth();
    }

    /**
     * @return Год.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPaymentYear()
     */
    public static PropertyPath<Integer> paymentYear()
    {
        return _dslPath.paymentYear();
    }

    /**
     * @return За период с.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPeriodStartDate()
     */
    public static PropertyPath<Date> periodStartDate()
    {
        return _dslPath.periodStartDate();
    }

    /**
     * @return За период по.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPeriodEndDate()
     */
    public static PropertyPath<Date> periodEndDate()
    {
        return _dslPath.periodEndDate();
    }

    /**
     * @return Тип выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getType()
     */
    public static FefuOrphanPaymentType.Path<FefuOrphanPaymentType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Выписка. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    public static class Path<E extends FefuOrphanPayment> extends EntityPath<E>
    {
        private PropertyPath<String> _print;
        private PropertyPath<Long> _paymentSum;
        private PropertyPath<String> _paymentSumPrint;
        private PropertyPath<Integer> _paymentMonth;
        private PropertyPath<Integer> _paymentYear;
        private PropertyPath<Date> _periodStartDate;
        private PropertyPath<Date> _periodEndDate;
        private FefuOrphanPaymentType.Path<FefuOrphanPaymentType> _type;
        private AbstractStudentExtract.Path<AbstractStudentExtract> _extract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вывод на печать.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPrint()
     */
        public PropertyPath<String> print()
        {
            if(_print == null )
                _print = new PropertyPath<String>(FefuOrphanPaymentGen.P_PRINT, this);
            return _print;
        }

    /**
     * @return Сумма выплаты (в сотых долях копейки).
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPaymentSum()
     */
        public PropertyPath<Long> paymentSum()
        {
            if(_paymentSum == null )
                _paymentSum = new PropertyPath<Long>(FefuOrphanPaymentGen.P_PAYMENT_SUM, this);
            return _paymentSum;
        }

    /**
     * @return Сумма выплаты прописью.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPaymentSumPrint()
     */
        public PropertyPath<String> paymentSumPrint()
        {
            if(_paymentSumPrint == null )
                _paymentSumPrint = new PropertyPath<String>(FefuOrphanPaymentGen.P_PAYMENT_SUM_PRINT, this);
            return _paymentSumPrint;
        }

    /**
     * @return Месяц.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPaymentMonth()
     */
        public PropertyPath<Integer> paymentMonth()
        {
            if(_paymentMonth == null )
                _paymentMonth = new PropertyPath<Integer>(FefuOrphanPaymentGen.P_PAYMENT_MONTH, this);
            return _paymentMonth;
        }

    /**
     * @return Год.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPaymentYear()
     */
        public PropertyPath<Integer> paymentYear()
        {
            if(_paymentYear == null )
                _paymentYear = new PropertyPath<Integer>(FefuOrphanPaymentGen.P_PAYMENT_YEAR, this);
            return _paymentYear;
        }

    /**
     * @return За период с.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPeriodStartDate()
     */
        public PropertyPath<Date> periodStartDate()
        {
            if(_periodStartDate == null )
                _periodStartDate = new PropertyPath<Date>(FefuOrphanPaymentGen.P_PERIOD_START_DATE, this);
            return _periodStartDate;
        }

    /**
     * @return За период по.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getPeriodEndDate()
     */
        public PropertyPath<Date> periodEndDate()
        {
            if(_periodEndDate == null )
                _periodEndDate = new PropertyPath<Date>(FefuOrphanPaymentGen.P_PERIOD_END_DATE, this);
            return _periodEndDate;
        }

    /**
     * @return Тип выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getType()
     */
        public FefuOrphanPaymentType.Path<FefuOrphanPaymentType> type()
        {
            if(_type == null )
                _type = new FefuOrphanPaymentType.Path<FefuOrphanPaymentType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Выписка. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrphanPayment#getExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

        public Class getEntityClass()
        {
            return FefuOrphanPayment.class;
        }

        public String getEntityName()
        {
            return "fefuOrphanPayment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
