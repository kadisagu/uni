package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeekPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент разбиения учебного графика (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEduPlanVersionWeekPartGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeekPart";
    public static final String ENTITY_NAME = "fefuEduPlanVersionWeekPart";
    public static final int VERSION_HASH = -2012629296;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VERSION_WEEK = "eduPlanVersionWeek";
    public static final String P_PARTITION_ELEMENT_NUMBER = "partitionElementNumber";
    public static final String L_WEEK_TYPE = "weekType";

    private FefuEduPlanVersionWeek _eduPlanVersionWeek;     // Неделя строки курса в учебном графике (ДВФУ)
    private int _partitionElementNumber;     // Номер элемента в разбиении
    private EppWeekType _weekType;     // Тип недели

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Неделя строки курса в учебном графике (ДВФУ). Свойство не может быть null.
     */
    @NotNull
    public FefuEduPlanVersionWeek getEduPlanVersionWeek()
    {
        return _eduPlanVersionWeek;
    }

    /**
     * @param eduPlanVersionWeek Неделя строки курса в учебном графике (ДВФУ). Свойство не может быть null.
     */
    public void setEduPlanVersionWeek(FefuEduPlanVersionWeek eduPlanVersionWeek)
    {
        dirty(_eduPlanVersionWeek, eduPlanVersionWeek);
        _eduPlanVersionWeek = eduPlanVersionWeek;
    }

    /**
     * @return Номер элемента в разбиении. Свойство не может быть null.
     */
    @NotNull
    public int getPartitionElementNumber()
    {
        return _partitionElementNumber;
    }

    /**
     * @param partitionElementNumber Номер элемента в разбиении. Свойство не может быть null.
     */
    public void setPartitionElementNumber(int partitionElementNumber)
    {
        dirty(_partitionElementNumber, partitionElementNumber);
        _partitionElementNumber = partitionElementNumber;
    }

    /**
     * @return Тип недели. Свойство не может быть null.
     */
    @NotNull
    public EppWeekType getWeekType()
    {
        return _weekType;
    }

    /**
     * @param weekType Тип недели. Свойство не может быть null.
     */
    public void setWeekType(EppWeekType weekType)
    {
        dirty(_weekType, weekType);
        _weekType = weekType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEduPlanVersionWeekPartGen)
        {
            setEduPlanVersionWeek(((FefuEduPlanVersionWeekPart)another).getEduPlanVersionWeek());
            setPartitionElementNumber(((FefuEduPlanVersionWeekPart)another).getPartitionElementNumber());
            setWeekType(((FefuEduPlanVersionWeekPart)another).getWeekType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEduPlanVersionWeekPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEduPlanVersionWeekPart.class;
        }

        public T newInstance()
        {
            return (T) new FefuEduPlanVersionWeekPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVersionWeek":
                    return obj.getEduPlanVersionWeek();
                case "partitionElementNumber":
                    return obj.getPartitionElementNumber();
                case "weekType":
                    return obj.getWeekType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVersionWeek":
                    obj.setEduPlanVersionWeek((FefuEduPlanVersionWeek) value);
                    return;
                case "partitionElementNumber":
                    obj.setPartitionElementNumber((Integer) value);
                    return;
                case "weekType":
                    obj.setWeekType((EppWeekType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVersionWeek":
                        return true;
                case "partitionElementNumber":
                        return true;
                case "weekType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVersionWeek":
                    return true;
                case "partitionElementNumber":
                    return true;
                case "weekType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVersionWeek":
                    return FefuEduPlanVersionWeek.class;
                case "partitionElementNumber":
                    return Integer.class;
                case "weekType":
                    return EppWeekType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEduPlanVersionWeekPart> _dslPath = new Path<FefuEduPlanVersionWeekPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEduPlanVersionWeekPart");
    }
            

    /**
     * @return Неделя строки курса в учебном графике (ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeekPart#getEduPlanVersionWeek()
     */
    public static FefuEduPlanVersionWeek.Path<FefuEduPlanVersionWeek> eduPlanVersionWeek()
    {
        return _dslPath.eduPlanVersionWeek();
    }

    /**
     * @return Номер элемента в разбиении. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeekPart#getPartitionElementNumber()
     */
    public static PropertyPath<Integer> partitionElementNumber()
    {
        return _dslPath.partitionElementNumber();
    }

    /**
     * @return Тип недели. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeekPart#getWeekType()
     */
    public static EppWeekType.Path<EppWeekType> weekType()
    {
        return _dslPath.weekType();
    }

    public static class Path<E extends FefuEduPlanVersionWeekPart> extends EntityPath<E>
    {
        private FefuEduPlanVersionWeek.Path<FefuEduPlanVersionWeek> _eduPlanVersionWeek;
        private PropertyPath<Integer> _partitionElementNumber;
        private EppWeekType.Path<EppWeekType> _weekType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Неделя строки курса в учебном графике (ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeekPart#getEduPlanVersionWeek()
     */
        public FefuEduPlanVersionWeek.Path<FefuEduPlanVersionWeek> eduPlanVersionWeek()
        {
            if(_eduPlanVersionWeek == null )
                _eduPlanVersionWeek = new FefuEduPlanVersionWeek.Path<FefuEduPlanVersionWeek>(L_EDU_PLAN_VERSION_WEEK, this);
            return _eduPlanVersionWeek;
        }

    /**
     * @return Номер элемента в разбиении. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeekPart#getPartitionElementNumber()
     */
        public PropertyPath<Integer> partitionElementNumber()
        {
            if(_partitionElementNumber == null )
                _partitionElementNumber = new PropertyPath<Integer>(FefuEduPlanVersionWeekPartGen.P_PARTITION_ELEMENT_NUMBER, this);
            return _partitionElementNumber;
        }

    /**
     * @return Тип недели. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeekPart#getWeekType()
     */
        public EppWeekType.Path<EppWeekType> weekType()
        {
            if(_weekType == null )
                _weekType = new EppWeekType.Path<EppWeekType>(L_WEEK_TYPE, this);
            return _weekType;
        }

        public Class getEntityClass()
        {
            return FefuEduPlanVersionWeekPart.class;
        }

        public String getEntityName()
        {
            return "fefuEduPlanVersionWeekPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
