package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Связь федерального округа с административной единицей
 */
public class FefuFederalAreasRel extends FefuFederalAreasRelGen
{
    public String getAddressTitle()
    {
        return getAddressItem().getAddressTitle();
    }
}