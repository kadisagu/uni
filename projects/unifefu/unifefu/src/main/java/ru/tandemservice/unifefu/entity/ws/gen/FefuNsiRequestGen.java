package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRequest;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запрос на регистрацию пользователя в НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuNsiRequestGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuNsiRequest";
    public static final String ENTITY_NAME = "fefuNsiRequest";
    public static final int VERSION_HASH = -1716969338;
    private static IEntityMeta ENTITY_META;

    public static final String L_NSI_GUID = "nsiGuid";
    public static final String L_PERSON = "person";
    public static final String P_PASSWORD_ENCRYPTED = "passwordEncrypted";
    public static final String P_LOGIN_FROM_N_S_I = "loginFromNSI";
    public static final String P_SENT = "sent";
    public static final String P_RESPONSE = "response";

    private FefuNsiIds _nsiGuid;     // Идентификатор сущности в НСИ
    private Person _person;     // Персона (физическое лицо)
    private String _passwordEncrypted;     // Зашифрованный пароль
    private String _loginFromNSI;     // Логин, полученный из НСИ
    private boolean _sent;     // Запрос отправлен в НСИ (ожидается ответ от НСИ)
    private String _response;     // Код ошибки, получаемый в случае неудачной регистрации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор сущности в НСИ. Свойство не может быть null.
     */
    @NotNull
    public FefuNsiIds getNsiGuid()
    {
        return _nsiGuid;
    }

    /**
     * @param nsiGuid Идентификатор сущности в НСИ. Свойство не может быть null.
     */
    public void setNsiGuid(FefuNsiIds nsiGuid)
    {
        dirty(_nsiGuid, nsiGuid);
        _nsiGuid = nsiGuid;
    }

    /**
     * @return Персона (физическое лицо). Свойство не может быть null.
     */
    @NotNull
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Персона (физическое лицо). Свойство не может быть null.
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Зашифрованный пароль. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPasswordEncrypted()
    {
        return _passwordEncrypted;
    }

    /**
     * @param passwordEncrypted Зашифрованный пароль. Свойство не может быть null.
     */
    public void setPasswordEncrypted(String passwordEncrypted)
    {
        dirty(_passwordEncrypted, passwordEncrypted);
        _passwordEncrypted = passwordEncrypted;
    }

    /**
     * @return Логин, полученный из НСИ.
     */
    @Length(max=255)
    public String getLoginFromNSI()
    {
        return _loginFromNSI;
    }

    /**
     * @param loginFromNSI Логин, полученный из НСИ.
     */
    public void setLoginFromNSI(String loginFromNSI)
    {
        dirty(_loginFromNSI, loginFromNSI);
        _loginFromNSI = loginFromNSI;
    }

    /**
     * @return Запрос отправлен в НСИ (ожидается ответ от НСИ). Свойство не может быть null.
     */
    @NotNull
    public boolean isSent()
    {
        return _sent;
    }

    /**
     * @param sent Запрос отправлен в НСИ (ожидается ответ от НСИ). Свойство не может быть null.
     */
    public void setSent(boolean sent)
    {
        dirty(_sent, sent);
        _sent = sent;
    }

    /**
     * @return Код ошибки, получаемый в случае неудачной регистрации.
     */
    @Length(max=255)
    public String getResponse()
    {
        return _response;
    }

    /**
     * @param response Код ошибки, получаемый в случае неудачной регистрации.
     */
    public void setResponse(String response)
    {
        dirty(_response, response);
        _response = response;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuNsiRequestGen)
        {
            setNsiGuid(((FefuNsiRequest)another).getNsiGuid());
            setPerson(((FefuNsiRequest)another).getPerson());
            setPasswordEncrypted(((FefuNsiRequest)another).getPasswordEncrypted());
            setLoginFromNSI(((FefuNsiRequest)another).getLoginFromNSI());
            setSent(((FefuNsiRequest)another).isSent());
            setResponse(((FefuNsiRequest)another).getResponse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuNsiRequestGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuNsiRequest.class;
        }

        public T newInstance()
        {
            return (T) new FefuNsiRequest();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "nsiGuid":
                    return obj.getNsiGuid();
                case "person":
                    return obj.getPerson();
                case "passwordEncrypted":
                    return obj.getPasswordEncrypted();
                case "loginFromNSI":
                    return obj.getLoginFromNSI();
                case "sent":
                    return obj.isSent();
                case "response":
                    return obj.getResponse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "nsiGuid":
                    obj.setNsiGuid((FefuNsiIds) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "passwordEncrypted":
                    obj.setPasswordEncrypted((String) value);
                    return;
                case "loginFromNSI":
                    obj.setLoginFromNSI((String) value);
                    return;
                case "sent":
                    obj.setSent((Boolean) value);
                    return;
                case "response":
                    obj.setResponse((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "nsiGuid":
                        return true;
                case "person":
                        return true;
                case "passwordEncrypted":
                        return true;
                case "loginFromNSI":
                        return true;
                case "sent":
                        return true;
                case "response":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "nsiGuid":
                    return true;
                case "person":
                    return true;
                case "passwordEncrypted":
                    return true;
                case "loginFromNSI":
                    return true;
                case "sent":
                    return true;
                case "response":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "nsiGuid":
                    return FefuNsiIds.class;
                case "person":
                    return Person.class;
                case "passwordEncrypted":
                    return String.class;
                case "loginFromNSI":
                    return String.class;
                case "sent":
                    return Boolean.class;
                case "response":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuNsiRequest> _dslPath = new Path<FefuNsiRequest>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuNsiRequest");
    }
            

    /**
     * @return Идентификатор сущности в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#getNsiGuid()
     */
    public static FefuNsiIds.Path<FefuNsiIds> nsiGuid()
    {
        return _dslPath.nsiGuid();
    }

    /**
     * @return Персона (физическое лицо). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Зашифрованный пароль. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#getPasswordEncrypted()
     */
    public static PropertyPath<String> passwordEncrypted()
    {
        return _dslPath.passwordEncrypted();
    }

    /**
     * @return Логин, полученный из НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#getLoginFromNSI()
     */
    public static PropertyPath<String> loginFromNSI()
    {
        return _dslPath.loginFromNSI();
    }

    /**
     * @return Запрос отправлен в НСИ (ожидается ответ от НСИ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#isSent()
     */
    public static PropertyPath<Boolean> sent()
    {
        return _dslPath.sent();
    }

    /**
     * @return Код ошибки, получаемый в случае неудачной регистрации.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#getResponse()
     */
    public static PropertyPath<String> response()
    {
        return _dslPath.response();
    }

    public static class Path<E extends FefuNsiRequest> extends EntityPath<E>
    {
        private FefuNsiIds.Path<FefuNsiIds> _nsiGuid;
        private Person.Path<Person> _person;
        private PropertyPath<String> _passwordEncrypted;
        private PropertyPath<String> _loginFromNSI;
        private PropertyPath<Boolean> _sent;
        private PropertyPath<String> _response;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор сущности в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#getNsiGuid()
     */
        public FefuNsiIds.Path<FefuNsiIds> nsiGuid()
        {
            if(_nsiGuid == null )
                _nsiGuid = new FefuNsiIds.Path<FefuNsiIds>(L_NSI_GUID, this);
            return _nsiGuid;
        }

    /**
     * @return Персона (физическое лицо). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Зашифрованный пароль. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#getPasswordEncrypted()
     */
        public PropertyPath<String> passwordEncrypted()
        {
            if(_passwordEncrypted == null )
                _passwordEncrypted = new PropertyPath<String>(FefuNsiRequestGen.P_PASSWORD_ENCRYPTED, this);
            return _passwordEncrypted;
        }

    /**
     * @return Логин, полученный из НСИ.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#getLoginFromNSI()
     */
        public PropertyPath<String> loginFromNSI()
        {
            if(_loginFromNSI == null )
                _loginFromNSI = new PropertyPath<String>(FefuNsiRequestGen.P_LOGIN_FROM_N_S_I, this);
            return _loginFromNSI;
        }

    /**
     * @return Запрос отправлен в НСИ (ожидается ответ от НСИ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#isSent()
     */
        public PropertyPath<Boolean> sent()
        {
            if(_sent == null )
                _sent = new PropertyPath<Boolean>(FefuNsiRequestGen.P_SENT, this);
            return _sent;
        }

    /**
     * @return Код ошибки, получаемый в случае неудачной регистрации.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRequest#getResponse()
     */
        public PropertyPath<String> response()
        {
            if(_response == null )
                _response = new PropertyPath<String>(FefuNsiRequestGen.P_RESPONSE, this);
            return _response;
        }

        public Class getEntityClass()
        {
            return FefuNsiRequest.class;
        }

        public String getEntityName()
        {
            return "fefuNsiRequest";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
