/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.logic.FefuSessionDocumentSlotWrapper;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 26.08.2014
 */
@Configuration
public class FefuPpsSessionListDocumentPub extends BusinessComponentManager
{
	//data source
	public final static String DOCUMENT_SLOT_DS = "documentSlotDS";
	public static final String DOCUMENT_SLOT_LIST = "documentSlot";

	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(searchListDS(DOCUMENT_SLOT_DS, getDocumentSlotDS(), documentSlotDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint getDocumentSlotDS()
	{
		return columnListExtPointBuilder(DOCUMENT_SLOT_DS)
				.addColumn(textColumn("event", SessionDocumentSlot.studentWpeCAction().registryElementTitle()))
				.addColumn(textColumn("term", SessionDocumentSlot.termTitle()))
				.addColumn(textColumn("rating", FefuSessionDocumentSlotWrapper.RATING).visible("ui:useRating"))
				.addColumn(textColumn("mark", FefuSessionDocumentSlotWrapper.MARK))
				.addColumn(textColumn("pps", FefuSessionDocumentSlotWrapper.PPS))
				.addColumn(textColumn("comment", FefuSessionDocumentSlotWrapper.MARK_COMMENT))
				.create();
	}

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> documentSlotDSHandler()
	{
		return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
		{
			@Override
			protected DSOutput execute(DSInput input, ExecutionContext context)
			{
				return ListOutputBuilder.get(input, context.<List>get(FefuPpsSessionListDocumentPub.DOCUMENT_SLOT_LIST)).pageable(true).build();
			}
		};
	}
}
