/* $Id$ */
package ru.tandemservice.unifefu.events.nsi;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import org.tandemframework.sec.entity.Role;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.organization.sec.entity.RoleConfigLocalOrgUnit;
import org.tandemframework.shared.organization.sec.entity.RoleConfigTemplateOrgUnit;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRole;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry V. Seleznev
 * @since 08.11.2015
 */
public class OrgUnitListener extends ParamTransactionCompleteListener<Boolean>
{
    @SuppressWarnings("unchecked")
    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, OrgUnit.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, OrgUnit.class, this);
    }

    @Override
    public Boolean beforeCompletion(final Session session, final Collection<Long> params)
    {
        List<Object[]> localRoleList = new DQLSelectBuilder().fromEntity(RoleConfigLocalOrgUnit.class, "r").column(property("r")).column("nsr")
                .joinEntity("r", DQLJoinType.left, FefuNsiRole.class, "nsr", eq(property(RoleConfigLocalOrgUnit.role().id().fromAlias("r")), property(FefuNsiRole.role().id().fromAlias("nsr"))))
                .where(in(property(RoleConfigLocalOrgUnit.orgUnit().id().fromAlias("r")), params)).createStatement(session).list();

        for (Object[] roleItem : localRoleList)
        {
            RoleConfigLocalOrgUnit roleConfig = (RoleConfigLocalOrgUnit) roleItem[0];
            FefuNsiRole nsiRole = (FefuNsiRole) roleItem[1];

            if (null == nsiRole) nsiRole = new FefuNsiRole();
            nsiRole.setRole(roleConfig.getRole());
            nsiRole.setName(roleConfig.getRole().getTitle() + " " + roleConfig.getOrgUnit().getTitle());
            nsiRole.setOrgUnit(roleConfig.getOrgUnit());
            session.saveOrUpdate(nsiRole);
        }

        List<OrgUnit> orgUnitList = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou").column(property("ou"))
                .where(eq(property(OrgUnit.archival().fromAlias("ou")), value(Boolean.FALSE)))
                .where(in(property(OrgUnit.id().fromAlias("ou")), params))
                .createStatement(session).list();

        Map<OrgUnitType, List<OrgUnit>> orgUnitsMap = new HashMap<>();
        Set<Long> ouTypesSet = new HashSet<>();

        for (OrgUnit orgUnit : orgUnitList)
        {
            List<OrgUnit> typeGroupedOrgUnitsList = orgUnitsMap.get(orgUnit.getOrgUnitType());
            if (null == typeGroupedOrgUnitsList) typeGroupedOrgUnitsList = new ArrayList<>();
            typeGroupedOrgUnitsList.add(orgUnit);
            orgUnitsMap.put(orgUnit.getOrgUnitType(), typeGroupedOrgUnitsList);
            ouTypesSet.add(orgUnit.getOrgUnitType().getId());
        }

        List<Object[]> templateRoleList = new DQLSelectBuilder().fromEntity(RoleConfigTemplateOrgUnit.class, "r").column(property("r")).column("nsr")
                .joinEntity("r", DQLJoinType.left, FefuNsiRole.class, "nsr", eq(property(RoleConfigTemplateOrgUnit.role().id().fromAlias("r")), property(FefuNsiRole.role().id().fromAlias("nsr"))))
                .where(in(property(RoleConfigTemplateOrgUnit.orgUnitType().id().fromAlias("r")), ouTypesSet)).createStatement(session).list();

        Map<CoreCollectionUtils.Pair<Role, OrgUnit>, FefuNsiRole> nsiRoleMap = new HashMap<>();
        Set<RoleConfigTemplateOrgUnit> templateRoleSet = new HashSet<>();
        for (Object[] roleItem : templateRoleList)
        {
            RoleConfigTemplateOrgUnit roleConfig = (RoleConfigTemplateOrgUnit) roleItem[0];
            FefuNsiRole nsiRole = (FefuNsiRole) roleItem[1];
            templateRoleSet.add(roleConfig);

            if (null != nsiRole)
            {
                CoreCollectionUtils.Pair<Role, OrgUnit> pairKey = new CoreCollectionUtils.Pair(roleConfig.getRole(), nsiRole.getOrgUnit());
                nsiRoleMap.put(pairKey, nsiRole);
            }
        }

        for (RoleConfigTemplateOrgUnit templateRole : templateRoleSet)
        {
            for (OrgUnit orgUnit : orgUnitsMap.get(templateRole.getOrgUnitType()))
            {
                CoreCollectionUtils.Pair<Role, OrgUnit> pairKey = new CoreCollectionUtils.Pair(templateRole.getRole(), orgUnit);
                FefuNsiRole nsiRole = nsiRoleMap.get(pairKey);

                if (null == nsiRole) nsiRole = new FefuNsiRole();
                nsiRole.setRole(templateRole.getRole());
                nsiRole.setName(templateRole.getRole().getTitle() + " " + orgUnit.getTitle());
                nsiRole.setOrgUnit(orgUnit);
                session.saveOrUpdate(nsiRole);
            }
        }

        session.flush();

        return true;
    }
}