/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.ui.AllList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.datasource.column.IPublisherColumnBuilder;
import org.tandemframework.caf.ui.config.datasource.column.ITextColumnBuilder;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.base.bo.FefuStudent.logic.FefuStudentSearchAllListDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuStudent.logic.FefuStudentSearchListOrderNonStateFinishedDSHandler;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.FefuEduProgramEducationOrgUnitAddon;
import ru.tandemservice.unifefu.base.ext.UniStudent.UniStudentExtManager;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 03.11.2013
 */
@Configuration
public class FefuStudentAllList extends AbstractUniStudentList
{
    @Override
    protected IPresenterExtPointBuilder addUniUtilEduOrgUnitAddons(IPresenterExtPointBuilder builder)
    {
        return builder
                .addAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, FefuEduProgramEducationOrgUnitAddon.class));
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, fefuStudentSearchAllListDSColumnExtPoint(), fefuStudentSearchAllListDSHandler()))
                        .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                        .addDataSource(selectDS(UniStudentExtManager.BASE_EDU_DS, FefuStudentManager.instance().baseEduDSHandler()))
                        .addDataSource(selectDS(FefuStudentManager.FEFU_ARCHIVAL_DS, FefuStudentManager.instance().archivalDSHandler()))
                        .addDataSource(selectDS(FefuStudentManager.FEFU_TYPE_MATCH_DS, FefuStudentManager.instance().typeMatchDSHandler()))
                        .addDataSource(selectDS(FefuStudentManager.FEFU_STUDENT_FIO_DS, FefuStudentManager.instance().fefuStudentListDSHandler()).addColumn("title", null, new IFormatter<Student>()
                        {
                            @Override
                            public String format(Student student)
                            {
                                String group = student.getGroup() == null ? "" : " (" + student.getGroup().getTitle() + ")";
                                return FefuStudentManager.instance().getIdentityCards(student.getPerson()) + group;
                            }
                        }))
        .addDataSource(selectDS(FefuStudentManager.FEFU_IS_LEARNING_DS, FefuStudentManager.instance().yesDSHandler())));
    }

    @Override
    protected IPublisherColumnBuilder newFioColumnBuilder(IMergeRowIdResolver merger)
    {
        final IPublisherLinkResolver resolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Student student = ((DataWrapper) entity).getWrapped();
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, student != null ? student.getId() : null).add("selectedStudentTab", "studentTab");
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return null;
            }
        };
        return publisherColumn(FIO_COLUMN, Student.person().s())
                .formatter(object -> FefuStudentManager.instance().getIdentityCards((Person) object))
                .publisherLinkResolver(resolver).order().required(true);
    }

    @Bean
    public ColumnListExtPoint fefuStudentSearchAllListDSColumnExtPoint()
    {
        return createStudentSearchListColumnsBuilder()
                .addColumn(newOrderNonStateFinishedColumnBuilder().after(FIO_COLUMN).create())
                .addColumn(actionColumn("printStudentPersonalCard", new Icon("printer"), "onClickPrintStudentPersonalCard").permissionKey("printStudentPersonalCard_List"))
                .addColumn(actionColumn("editStudent", new Icon("edit"), "onClickEditStudent").permissionKey("editStudentList"))
                .addColumn(actionColumn("deleteStudent", new Icon("delete"), "onClickDeleteStudent").permissionKey("deleteStudentList"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> fefuStudentSearchAllListDSHandler()
    {
        return new FefuStudentSearchAllListDSHandler(getName());
    }

    @Override
    protected List<ColumnBase> customizeStudentSearchListColumns(List<ColumnBase> columnList)
    {
        removeColumns(columnList, GROUP_COLUMN);
        insertColumns(columnList, newGroupPublisherColumnBuilder().after(COMPENSATION_TYPE_COLUMN).create());
        return columnList;
    }

    private ITextColumnBuilder newOrderNonStateFinishedColumnBuilder()
    {
        return textColumn("orderNonStateFinished", FefuStudentSearchListOrderNonStateFinishedDSHandler.ORDER_NON_STATE_FINISHED).formatter(source ->
            (Boolean) source ? "Есть непроведенные приказы" : ""
        );
    }
}
