/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.AcademicRankType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class AcademicRankTypeReactor extends SimpleCatalogEntityNewReactor<AcademicRankType, ScienceStatus>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<ScienceStatus> getEntityClass()
    {
        return ScienceStatus.class;
    }

    @Override
    public Class<AcademicRankType> getNSIEntityClass()
    {
        return AcademicRankType.class;
    }

    @Override
    public AcademicRankType getCatalogElementRetrieveRequestObject(String guid)
    {
        AcademicRankType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createAcademicRankType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public AcademicRankType createEntity(CoreCollectionUtils.Pair<ScienceStatus, FefuNsiIds> entityPair)
    {
        AcademicRankType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createAcademicRankType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setAcademicRankID(getUserCodeForNSIChecked(entityPair.getX().getUserCode()));
        nsiEntity.setAcademicRankName(entityPair.getX().getTitle());
        return nsiEntity;
    }

    @Override
    public ScienceStatus createEntity(AcademicRankType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getAcademicRankName()) return null;

        ScienceStatus entity = new ScienceStatus();
        entity.setTitle(nsiEntity.getAcademicRankName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(ScienceStatus.class));
        entity.setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getAcademicRankID()));

        return entity;
    }

    @Override
    public ScienceStatus updatePossibleDuplicateFields(AcademicRankType nsiEntity, CoreCollectionUtils.Pair<ScienceStatus, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), ScienceStatus.title().s()))
                entityPair.getX().setTitle(nsiEntity.getAcademicRankName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), ScienceStatus.userCode().s()))
                entityPair.getX().setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getAcademicRankID()));
        }
        return entityPair.getX();
    }

    @Override
    public AcademicRankType updateNsiEntityFields(AcademicRankType nsiEntity, ScienceStatus entity)
    {
        nsiEntity.setAcademicRankName(entity.getTitle());
        nsiEntity.setAcademicRankID(getUserCodeForNSIChecked(entity.getUserCode()));
        return nsiEntity;
    }
}