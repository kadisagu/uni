/**
 * Tandem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.c1;

public interface Tandem extends javax.xml.rpc.Service {
    public java.lang.String getTandemSoapAddress();

    public ru.tandemservice.unifefu.ws.c1.TandemPortType getTandemSoap() throws javax.xml.rpc.ServiceException;

    public ru.tandemservice.unifefu.ws.c1.TandemPortType getTandemSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getTandemSoap12Address();

    public ru.tandemservice.unifefu.ws.c1.TandemPortType getTandemSoap12() throws javax.xml.rpc.ServiceException;

    public ru.tandemservice.unifefu.ws.c1.TandemPortType getTandemSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
