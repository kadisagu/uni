/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.orgunit.EppEpvAcceptionTab;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;

import java.util.Arrays;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 09.12.2013
 */
public class Model extends ru.tandemservice.uniepp.component.orgunit.EppEpvAcceptionTab.Model
{
    public static final IdentifiableWrapper<IEntity> YES = new IdentifiableWrapper<>(1L, "Да");
    public static final IdentifiableWrapper<IEntity> NO = new IdentifiableWrapper<>(2L, "Нет");

    private static final List<IdentifiableWrapper<IEntity>> YES_OPTIONS = Arrays.asList(YES);
    private static final List<IdentifiableWrapper<IEntity>> YES_NO_OPTIONS = Arrays.asList(YES, NO);

    public List<IdentifiableWrapper<IEntity>> getOptions(){ return YES_OPTIONS; }

    public final static String UMU_ID = "checkedByUMU";
    public final static String CRK_ID = "checkedByCRK";
    public final static String OOP_ID = "checkedByOOP";

    public String getUMUId(){ return UMU_ID; }
    public String getCRKId(){ return CRK_ID; }
    public String getOOPId(){ return OOP_ID; }
}
