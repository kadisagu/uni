package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь цикла ИМЦА с элементом ГОС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuImtsaCyclePlanStructureRelGen extends EntityBase
 implements INaturalIdentifiable<FefuImtsaCyclePlanStructureRelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel";
    public static final String ENTITY_NAME = "fefuImtsaCyclePlanStructureRel";
    public static final int VERSION_HASH = -97661779;
    private static IEntityMeta ENTITY_META;

    public static final String P_IMTSA_CYCLE = "imtsaCycle";
    public static final String P_GOS2 = "gos2";
    public static final String L_PLAN_STRUCTURE = "planStructure";

    private String _imtsaCycle;     // Цикл ИМЦА
    private boolean _gos2;     // ГОС2
    private EppPlanStructure _planStructure;     // Элемент ГОС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Цикл ИМЦА. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getImtsaCycle()
    {
        return _imtsaCycle;
    }

    /**
     * @param imtsaCycle Цикл ИМЦА. Свойство не может быть null.
     */
    public void setImtsaCycle(String imtsaCycle)
    {
        dirty(_imtsaCycle, imtsaCycle);
        _imtsaCycle = imtsaCycle;
    }

    /**
     * @return ГОС2. Свойство не может быть null.
     */
    @NotNull
    public boolean isGos2()
    {
        return _gos2;
    }

    /**
     * @param gos2 ГОС2. Свойство не может быть null.
     */
    public void setGos2(boolean gos2)
    {
        dirty(_gos2, gos2);
        _gos2 = gos2;
    }

    /**
     * @return Элемент ГОС. Свойство не может быть null.
     */
    @NotNull
    public EppPlanStructure getPlanStructure()
    {
        return _planStructure;
    }

    /**
     * @param planStructure Элемент ГОС. Свойство не может быть null.
     */
    public void setPlanStructure(EppPlanStructure planStructure)
    {
        dirty(_planStructure, planStructure);
        _planStructure = planStructure;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuImtsaCyclePlanStructureRelGen)
        {
            if (withNaturalIdProperties)
            {
                setImtsaCycle(((FefuImtsaCyclePlanStructureRel)another).getImtsaCycle());
                setGos2(((FefuImtsaCyclePlanStructureRel)another).isGos2());
            }
            setPlanStructure(((FefuImtsaCyclePlanStructureRel)another).getPlanStructure());
        }
    }

    public INaturalId<FefuImtsaCyclePlanStructureRelGen> getNaturalId()
    {
        return new NaturalId(getImtsaCycle(), isGos2());
    }

    public static class NaturalId extends NaturalIdBase<FefuImtsaCyclePlanStructureRelGen>
    {
        private static final String PROXY_NAME = "FefuImtsaCyclePlanStructureRelNaturalProxy";

        private String _imtsaCycle;
        private boolean _gos2;

        public NaturalId()
        {}

        public NaturalId(String imtsaCycle, boolean gos2)
        {
            _imtsaCycle = imtsaCycle;
            _gos2 = gos2;
        }

        public String getImtsaCycle()
        {
            return _imtsaCycle;
        }

        public void setImtsaCycle(String imtsaCycle)
        {
            _imtsaCycle = imtsaCycle;
        }

        public boolean isGos2()
        {
            return _gos2;
        }

        public void setGos2(boolean gos2)
        {
            _gos2 = gos2;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuImtsaCyclePlanStructureRelGen.NaturalId) ) return false;

            FefuImtsaCyclePlanStructureRelGen.NaturalId that = (NaturalId) o;

            if( !equals(getImtsaCycle(), that.getImtsaCycle()) ) return false;
            if( !equals(isGos2(), that.isGos2()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getImtsaCycle());
            result = hashCode(result, isGos2());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getImtsaCycle());
            sb.append("/");
            sb.append(isGos2());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuImtsaCyclePlanStructureRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuImtsaCyclePlanStructureRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuImtsaCyclePlanStructureRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "imtsaCycle":
                    return obj.getImtsaCycle();
                case "gos2":
                    return obj.isGos2();
                case "planStructure":
                    return obj.getPlanStructure();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "imtsaCycle":
                    obj.setImtsaCycle((String) value);
                    return;
                case "gos2":
                    obj.setGos2((Boolean) value);
                    return;
                case "planStructure":
                    obj.setPlanStructure((EppPlanStructure) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "imtsaCycle":
                        return true;
                case "gos2":
                        return true;
                case "planStructure":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "imtsaCycle":
                    return true;
                case "gos2":
                    return true;
                case "planStructure":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "imtsaCycle":
                    return String.class;
                case "gos2":
                    return Boolean.class;
                case "planStructure":
                    return EppPlanStructure.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuImtsaCyclePlanStructureRel> _dslPath = new Path<FefuImtsaCyclePlanStructureRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuImtsaCyclePlanStructureRel");
    }
            

    /**
     * @return Цикл ИМЦА. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel#getImtsaCycle()
     */
    public static PropertyPath<String> imtsaCycle()
    {
        return _dslPath.imtsaCycle();
    }

    /**
     * @return ГОС2. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel#isGos2()
     */
    public static PropertyPath<Boolean> gos2()
    {
        return _dslPath.gos2();
    }

    /**
     * @return Элемент ГОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel#getPlanStructure()
     */
    public static EppPlanStructure.Path<EppPlanStructure> planStructure()
    {
        return _dslPath.planStructure();
    }

    public static class Path<E extends FefuImtsaCyclePlanStructureRel> extends EntityPath<E>
    {
        private PropertyPath<String> _imtsaCycle;
        private PropertyPath<Boolean> _gos2;
        private EppPlanStructure.Path<EppPlanStructure> _planStructure;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Цикл ИМЦА. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel#getImtsaCycle()
     */
        public PropertyPath<String> imtsaCycle()
        {
            if(_imtsaCycle == null )
                _imtsaCycle = new PropertyPath<String>(FefuImtsaCyclePlanStructureRelGen.P_IMTSA_CYCLE, this);
            return _imtsaCycle;
        }

    /**
     * @return ГОС2. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel#isGos2()
     */
        public PropertyPath<Boolean> gos2()
        {
            if(_gos2 == null )
                _gos2 = new PropertyPath<Boolean>(FefuImtsaCyclePlanStructureRelGen.P_GOS2, this);
            return _gos2;
        }

    /**
     * @return Элемент ГОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel#getPlanStructure()
     */
        public EppPlanStructure.Path<EppPlanStructure> planStructure()
        {
            if(_planStructure == null )
                _planStructure = new EppPlanStructure.Path<EppPlanStructure>(L_PLAN_STRUCTURE, this);
            return _planStructure;
        }

        public Class getEntityClass()
        {
            return FefuImtsaCyclePlanStructureRel.class;
        }

        public String getEntityName()
        {
            return "fefuImtsaCyclePlanStructureRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
