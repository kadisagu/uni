/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.ExtractCreationRule;
import ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow;
import ru.tandemservice.unifefu.entity.catalog.FefuOrderCommitResult;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 13.11.2012
 */
public class FefuOrderAutoCommitDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String TRANSACTION_ID = "transactionId";
    public static final String SYNCHRONIZE_DATE_FROM = "synchronizeDateFrom";
    public static final String SYNCHRONIZE_DATE_TO = "synchronizeDateTo";
    public static final String ORDER_ID = "orderId";
    public static final String ORDER_NUMBER = "orderNumber";
    public static final String ORDER_DATE_FROM = "orderDateFrom";
    public static final String ORDER_DATE_TO = "orderDateTo";
    public static final String TRANSACTION_STATE = "transactionState";
    public static final String COMMENT_SHORT = "commentShort";

    public static final String CLICKABLE = "clickable";
    public static final String EXTRACT_ID = "extractId";

    public FefuOrderAutoCommitDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long orderIdFilter = (Long) context.get(ORDER_ID);
        Long transactionIdFilter = (Long) context.get(TRANSACTION_ID);
        Date synchronizeDateFromFilter = (Date) context.get(SYNCHRONIZE_DATE_FROM);
        Date synchronizeDateToFilter = (Date) context.get(SYNCHRONIZE_DATE_TO);
        String orderNumberFilter = (String) context.get(ORDER_NUMBER);
        Date orderDateFromFilter = (Date) context.get(ORDER_DATE_FROM);
        Date orderDateToFilter = (Date) context.get(ORDER_DATE_TO);
        FefuOrderCommitResult transactionStateFilter = (FefuOrderCommitResult) context.get(TRANSACTION_STATE);
        String commentShortFilter = (String) context.get(COMMENT_SHORT);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuOrderAutoCommitLogRow.class, "e").column("e");

        // применяем фильтры

        if (null != orderIdFilter)
            builder.where(eq(property(FefuOrderAutoCommitLogRow.orderId().fromAlias("e")), value(orderIdFilter)));

        if (null != transactionIdFilter)
            builder.where(eq(property(FefuOrderAutoCommitLogRow.transactionId().fromAlias("e")), value(transactionIdFilter)));

        if (null != synchronizeDateFromFilter)
        {
            synchronizeDateFromFilter = CoreDateUtils.getDayFirstTimeMoment(synchronizeDateFromFilter);
            builder.where(ge(property(FefuOrderAutoCommitLogRow.executeDate().fromAlias("e")), valueTimestamp(synchronizeDateFromFilter)));
        }

        if (null != synchronizeDateToFilter)
        {
            synchronizeDateToFilter = CoreDateUtils.getDayFirstTimeMoment(synchronizeDateToFilter);
            builder.where(le(property(FefuOrderAutoCommitLogRow.executeDate().fromAlias("e")), valueTimestamp(synchronizeDateToFilter)));
        }

        if (null != orderNumberFilter)
            builder.where(like(DQLFunctions.upper(property(FefuOrderAutoCommitLogRow.orderNumber().fromAlias("e"))), value(CoreStringUtils.escapeLike(orderNumberFilter))));

        if (null != orderDateFromFilter)
        {
            orderDateFromFilter = CoreDateUtils.getDayFirstTimeMoment(orderDateFromFilter);
            builder.where(ge(property(FefuOrderAutoCommitLogRow.orderDate().fromAlias("e")), valueDate(orderDateFromFilter)));
        }

        if (null != orderDateToFilter)
        {
            orderDateToFilter = CoreDateUtils.getDayFirstTimeMoment(orderDateToFilter);
            builder.where(le(property(FefuOrderAutoCommitLogRow.orderDate().fromAlias("e")), valueDate(orderDateToFilter)));
        }

        if (null != transactionStateFilter)
            builder.where(eq(property(FefuOrderAutoCommitLogRow.commitResult().fromAlias("e")), value(transactionStateFilter)));

        if (null != commentShortFilter)
            builder.where(like(DQLFunctions.upper(property(FefuOrderAutoCommitLogRow.commentShort().fromAlias("e"))), value(CoreStringUtils.escapeLike(commentShortFilter))));

        builder.order(property("e", input.getEntityOrder().getKeyString()), OrderDirection.valueOf(input.getEntityOrder().getDirection().name()));

        List<FefuOrderAutoCommitLogRow> rowsList = builder.createStatement(new DQLExecutionContext(context.getSession())).list();

        DQLSelectBuilder orderBuilder = new DQLSelectBuilder()
                .fromEntity(FefuOrderAutoCommitLogRow.class, "ac")
                .joinEntity("ac", DQLJoinType.left, AbstractStudentOrder.class, "so", eq(property(FefuOrderAutoCommitLogRow.orderId().fromAlias("ac")), property(AbstractStudentOrder.id().fromAlias("so"))))
                .joinEntity("so", DQLJoinType.left, AbstractStudentParagraph.class, "sp", eq(property(FefuOrderAutoCommitLogRow.orderId().fromAlias("ac")), property(AbstractStudentParagraph.order().id().fromAlias("sp"))))
                .joinEntity("sp", DQLJoinType.left, AbstractStudentExtract.class, "se", eq(property(AbstractStudentParagraph.id().fromAlias("sp")), property(AbstractStudentExtract.paragraph().id().fromAlias("se"))))
                .joinEntity("se", DQLJoinType.left, ExtractCreationRule.class, "cr", eq(property(AbstractStudentExtract.type().id().fromAlias("se")), property(ExtractCreationRule.studentExtractType().id().fromAlias("cr"))))
                .column(property(FefuOrderAutoCommitLogRow.transactionId().fromAlias("ac")))
                .column(property(FefuOrderAutoCommitLogRow.orderId().fromAlias("ac")))
                .column(property(AbstractStudentExtract.id().fromAlias("se")))
                .column(property(ExtractCreationRule.individualOrder().fromAlias("cr")))
                .where(isNotNull(property(AbstractStudentOrder.id().fromAlias("so"))));
        List<Object[]> orderData = orderBuilder.createStatement(context.getSession()).list();

        Map<Long, Long> orderToExtractMap = new HashMap<>();
        Set<CoreCollectionUtils.Pair<Long, Long>> transactionIdToOrderIdSet = new HashSet<>();

        for (Object[] singleRecord : orderData)
        {
            Long orderId = (Long) singleRecord[1];
            Long transactionId = (Long) singleRecord[0];
            Long extractId = singleRecord.length > 2 ? (Long) singleRecord[2] : null;
            Boolean individual = singleRecord.length > 3 ? (Boolean) singleRecord[3] : null;

            CoreCollectionUtils.Pair<Long, Long> pair = new CoreCollectionUtils.Pair<>(transactionId, orderId);
            if (!transactionIdToOrderIdSet.contains(pair) && null != extractId && null != individual && individual)
            {
                orderToExtractMap.put(orderId, extractId);
            } else
            {
                orderToExtractMap.put(orderId, null);
            }

            transactionIdToOrderIdSet.add(pair);
        }


        List<DataWrapper> recordList = new ArrayList<>();
        for (FefuOrderAutoCommitLogRow row : rowsList)
        {
            Long extractId = orderToExtractMap.get(row.getOrderId());
            Boolean clickable = orderToExtractMap.containsKey(row.getOrderId());

            DataWrapper record = new DataWrapper(row.getId(), String.valueOf(row.getTransactionId()), row);
            record.setProperty(EXTRACT_ID, extractId);
            record.setProperty(CLICKABLE, clickable);
            recordList.add(record);
        }

        return ListOutputBuilder.get(input, recordList).pageable(true).build();
    }
}