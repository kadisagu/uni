/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;

/**
 * @author Andrey Andreev
 * @since 13.01.2016
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuAdmittedToGIAListExtract, Model>
{
}
