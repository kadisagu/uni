/*$Id$*/
package ru.tandemservice.unifefu.events.eduplan;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.unifefu.entity.eduPlan.FefuChangedWorkPlan;

import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 16.10.2014
 */
public class FefuWorkPlanUpdateListenerBean implements IDSetEventListener
{
	public static final EventListenerLocker<IDSetEventListener> LOCKER = new EventListenerLocker<>();

	public void init()
	{
		DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppWorkPlan.class, this);
	}

	@Override
	public void onEvent(DSetEvent event)
	{
        if (!event.getMultitude().isSingular())
            return;

		ICommonDAO dao = DataAccessServices.dao();
		EppWorkPlan workPlan = (EppWorkPlan) event.getMultitude().getSingularEntity();
		FefuChangedWorkPlan changedWorkPlan = dao.get(FefuChangedWorkPlan.class, FefuChangedWorkPlan.L_EPP_WORK_PLAN, workPlan.getId());
		if (changedWorkPlan == null || changedWorkPlan.isChanged()) return;
		Set<String> properties = event.getMultitude().getAffectedProperties();
		if (properties.size() > 1 || !properties.contains(EppWorkPlan.L_STATE))
		{
			new DQLUpdateBuilder(FefuChangedWorkPlan.class)
					.set(FefuChangedWorkPlan.P_CHANGED, value(true))
					.where(eq(property(FefuChangedWorkPlan.L_EPP_WORK_PLAN), value(workPlan.getId())))
					.createStatement(event.getContext())
					.execute();
		}
	}
}
