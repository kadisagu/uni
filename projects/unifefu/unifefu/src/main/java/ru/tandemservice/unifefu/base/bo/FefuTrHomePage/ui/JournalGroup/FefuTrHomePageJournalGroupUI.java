package ru.tandemservice.unifefu.base.bo.FefuTrHomePage.ui.JournalGroup;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.unifefu.base.bo.FefuTrHomePage.FefuTrHomePageManager;
import ru.tandemservice.unifefu.base.bo.FefuTrHomePage.logic.FefuGroupsDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuTrHomePage.ui.JournalGroupEdit.FefuTrHomePageJournalGroupEdit;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalMark.TrHomePageJournalMark;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditUI;

/**
 * @author amakarova
 */

public class FefuTrHomePageJournalGroupUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        if (!_uiSettings.getSettings().containsKey(FefuGroupsDSHandler.PARAM_YEAR_PART))
        {
            _uiSettings.set(FefuGroupsDSHandler.PARAM_YEAR_PART, FefuTrHomePageManager.instance().homePageDao().getLastYearPart());
        }

    }

    @Override
    public void onBeforeDataSourceFetch(final IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(FefuTrHomePageManager.PERSON_KEY, PersonManager.instance().dao().getPerson(_uiConfig.getUserContext().getPrincipalContext()));
        // часть учебного года
        dataSource.put(FefuGroupsDSHandler.PARAM_YEAR_PART, _uiSettings.get(FefuGroupsDSHandler.PARAM_YEAR_PART));
    }

    public void onClickShowJournal()
    {
        this._uiActivation.asDesktopRoot(TrHomePageJournalMark.class)
                .parameter(TrJournalGroupMarkEditUI.GROUP_BINDING, getListenerParameterAsLong())
                .activate();
    }

    public void onEditEntityFromList()
    {
        this._uiActivation.asDesktopRoot(FefuTrHomePageJournalGroupEdit.class)
                .parameter(TrJournalGroupMarkEditUI.GROUP_BINDING, getListenerParameterAsLong())
                .activate();
    }

    @Override
    public void clearSettings()
    {
        super.clearSettings();
        _uiSettings.set(FefuGroupsDSHandler.PARAM_YEAR_PART, FefuTrHomePageManager.instance().homePageDao().getLastYearPart());
    }
}
