/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu9.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuSocGrantResumptionStuExtract, Model>
{
}