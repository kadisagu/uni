/* $Id: FefuTrJournalStudentMarkEditUI.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuTrJournal.ui.StudentMarkEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unifefu.base.ext.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditExtUI;
import ru.tandemservice.unifefu.utils.FefuTrGroupEventDateComparator;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkCell;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkColumn;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkColumnGroup;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Input({
        @Bind(key = TrJournalGroupMarkEditExtUI.GROUP_BINDING, binding = "group.id", required = true),
        @Bind(key = TrJournalGroupMarkEditExtUI.SLOT_BINDING, binding = "studentSlot.id", required = true)
})

public class FefuTrJournalStudentMarkEditUI extends UIPresenter
{
    private TrJournalGroup group = new TrJournalGroup();
    private EppStudentWorkPlanElement studentSlot = new EppStudentWorkPlanElement();
    private List<TrJournalMarkColumnGroup> columnGroupList = new ArrayList<>();
    private TrJournalMarkColumnGroup currentColumnGroup;
    private TrJournalMarkColumn currentColumn;
    private Map<TrEduGroupEvent, TrJournalMarkCell> cellMap = new HashMap<>();

    @Override
    public void onComponentRefresh()
    {
        setGroup(IUniBaseDao.instance.get().get(TrJournalGroup.class, getGroup().getId()));
        setStudentSlot(IUniBaseDao.instance.get().get(EppStudentWorkPlanElement.class, getStudentSlot().getId()));

        DQLSelectBuilder eventDQL = new DQLSelectBuilder()
                .fromEntity(TrEduGroupEvent.class, "rel").column("rel").order(property(TrEduGroupEvent.journalEvent().journalModule().module().number().fromAlias("rel"))).order(property(TrEduGroupEvent.journalEvent().number().fromAlias("rel")))
                .where(eq(property(TrEduGroupEvent.group().fromAlias("rel")), value(getGroup().getGroup())))
                .where(eq(property(TrEduGroupEvent.journalEvent().journalModule().journal().fromAlias("rel")), value(getGroup().getJournal())))
                 ;
        final List<TrEduGroupEvent> events = IUniBaseDao.instance.get().<TrEduGroupEvent>getList(eventDQL);
        Collections.sort(events, new FefuTrGroupEventDateComparator());
        for (TrJournalModule module : UniBaseUtils.<TrJournalModule>getPropertiesSet(events, TrEduGroupEvent.journalEvent().journalModule().s()))
        {
            TrJournalMarkColumnGroup columnGroup = new TrJournalMarkColumnGroup(module);
            for (TrEduGroupEvent event : events)
            {
                if (event.getJournalEvent().getJournalModule().equals(module))
                {
                    TrJournalMarkColumn column = new TrJournalMarkColumn(event);
                    columnGroup.getColumnList().add(column);
                }
            }
            getColumnGroupList().add(columnGroup);
        }

        fillCellMap();
        fillRowByDefault();
    }

    private void fillCellMap() {

        for (TrJournalMarkColumnGroup group : getColumnGroupList())
        {
            for (TrJournalMarkColumn column : group.getColumnList()) {
                cellMap.put(column.getEvent(), new TrJournalMarkCell(column.getEvent().getJournalEvent()));
            }
        }
        // создаются ячейки по существующим событиям
        DQLSelectBuilder eventDQL = new DQLSelectBuilder()
                .fromEntity(TrEduGroupEventStudent.class, "event", true).column("event")
                .where(eq(property(TrEduGroupEventStudent.event().group().fromAlias("event")), value(getGroup().getGroup())))
                .where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("event")), value(getGroup().getJournal())))
                .where(eq(property(TrEduGroupEventStudent.studentWpe().fromAlias("event")), value(getStudentSlot())))
                ;
        final List<TrEduGroupEventStudent> events = IUniBaseDao.instance.get().getList(eventDQL);

        for (TrEduGroupEventStudent event : events) {
            cellMap.put(event.getEvent(), new TrJournalMarkCell(event));
        }
    }

    private void fillRowByDefault()
    {
        if (null == getStudentSlot())
            return;

        final List<TrJournalMarkCell> cells = new ArrayList<>();
        for (TrJournalMarkColumnGroup markStudent : getColumnGroupList())
        {
            for (TrJournalMarkColumn column : markStudent.getColumnList())
            {
                TrJournalMarkCell cell = getCellMap().get(column.getEvent());
                if (cell.getAbsenceNote() == null && cell.getGrade() == null && column.getEvent().getScheduleEvent() != null) cells.add(cell);
            }
        }
        for (TrJournalMarkCell cell : cells)
        {
            cell.setAbsenceNote(TrJournalMarkCell.IDW_PRESENT);
        }
    }

    public void onClickApply()
    {
        if (getStudentSlot() != null) {
            DataAccessServices.dao().doInTransaction(session -> {
                for (TrJournalMarkColumnGroup groupList : getColumnGroupList()) {
                    for (TrJournalMarkColumn column : groupList.getColumnList()) {
                        TrJournalMarkCell cell = getCellMap().get(column.getEvent());
                        TrJournalManager.instance().eventDao().saveJournalMark(getStudentSlot(), column.getEvent(), cell);
                    }
                }
                return Boolean.TRUE;
            });
        }
        deactivate();
    }

    // methods
    public TrJournalGroup getGroup() {
        return group;
    }

    public void setGroup(TrJournalGroup group) {
        this.group = group;
    }

    public EppStudentWorkPlanElement getStudentSlot() {
        return studentSlot;
    }

    public void setStudentSlot(EppStudentWorkPlanElement studentSlot) {
        this.studentSlot = studentSlot;
    }

    public List<TrJournalMarkColumnGroup> getColumnGroupList() {
        return columnGroupList;
    }

    public void setColumnGroupList(List<TrJournalMarkColumnGroup> columnGroupList) {
        this.columnGroupList = columnGroupList;
    }

    public TrJournalMarkColumnGroup getCurrentColumnGroup() {
        return currentColumnGroup;
    }

    public void setCurrentColumnGroup(TrJournalMarkColumnGroup currentColumnGroup) {
        this.currentColumnGroup = currentColumnGroup;
    }

    public TrJournalMarkColumn getCurrentColumn() {
        return currentColumn;
    }

    public void setCurrentColumn(TrJournalMarkColumn currentColumn) {
        this.currentColumn = currentColumn;
    }

    public ISelectModel getAbsenceModel()
    {
        return TrJournalMarkCell.getAbsenceModel();
    }

    public Map<TrEduGroupEvent, TrJournalMarkCell> getCellMap() {
        return cellMap;
    }

    public void setCellMap(Map<TrEduGroupEvent, TrJournalMarkCell> cellMap) {
        this.cellMap = cellMap;
    }

    public TrJournalMarkCell getCurrentCell()
    {
        return getCellMap().get(getCurrentColumn().getEvent());
    }

    public List<TrJournalMarkColumn> getColumnList()
    {
        return getCurrentColumnGroup().getColumnList();
    }

    public boolean isCanEditCell() {
        return getCurrentColumn().getEvent().getScheduleEvent() != null;
    }
}


