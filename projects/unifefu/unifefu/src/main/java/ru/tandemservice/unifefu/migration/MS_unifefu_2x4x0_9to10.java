package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x0_9to10 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuBrsSumDataReport

        // создана новая сущность
        {
            if (!tool.tableExists("fefubrssumdatareport_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefubrssumdatareport_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("content_id", DBType.LONG).setNullable(false),
                        new DBColumn("formingdate_p", DBType.TIMESTAMP).setNullable(false),
                        new DBColumn("formativeorgunit_p", DBType.createVarchar(255)),
                        new DBColumn("yearpart_p", DBType.createVarchar(255))
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuBrsSumDataReport");

        }


    }
}