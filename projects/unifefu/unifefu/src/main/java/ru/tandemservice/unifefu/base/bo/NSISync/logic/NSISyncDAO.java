/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.logic;

import org.apache.axis.AxisFault;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.commonbase.utils.SharedDQLUtils;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnitTypeRestriction;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.NSISync.NSISyncManager;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow;
import ru.tandemservice.unifefu.entity.ws.FefuProcessingStack;
import ru.tandemservice.unifefu.entity.ws.FefuTopOrgUnit;
import ru.tandemservice.unifefu.ws.nsi.*;
import ru.tandemservice.unifefu.ws.nsi.datagram.*;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import javax.xml.rpc.ServiceException;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 23.09.2013
 */
public class NSISyncDAO extends SharedBaseDao implements INSISyncDAO
{
    private boolean _orgstructureInProcess = false;
    private byte[] _orgstructureFile = null;
    private MessageElement _fileContent = null;

    private static final Set<String> DEPARTMENTS_TO_IGNORE = new HashSet<>();
    private static Comparator<OrganizationWrapper> WRAPPER_COMPARATOR = new Comparator<OrganizationWrapper>()
    {
        @Override
        public int compare(OrganizationWrapper o1, OrganizationWrapper o2)
        {
            return o1.getTitle().compareTo(o2.getTitle());
        }
    };

    static
    {
        DEPARTMENTS_TO_IGNORE.add("УЧЕБНЫЕ ПОДРАЗДЕЛЕНИЯ");
        DEPARTMENTS_TO_IGNORE.add("НЕУЧЕБНЫЕ ПОДРАЗДЕЛЕНИЯ");
        DEPARTMENTS_TO_IGNORE.add("Школы");
    }

    @Override
    public void updateNsiCatalogAutosync(Long id)
    {
        FefuNsiCatalogType catalog = getNotNull(FefuNsiCatalogType.class, id);
        catalog.setAutoSync(!catalog.isAutoSync());
    }

    @Override
    public void updateNsiCatalogRealTimeSync(Long id)
    {
        FefuNsiCatalogType catalog = getNotNull(FefuNsiCatalogType.class, id);
        catalog.setRealTimeSync(!catalog.isRealTimeSync());
    }

    @Override
    public boolean isAnyIdInLoginSyncStack()
    {
        return ((Number) new DQLSelectBuilder().fromEntity(FefuProcessingStack.class, "s").createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue() > 0;
    }

    @Override
    public void doFullFillHumanLoginSyncStack()
    {
        List<Long> personIds = new DQLSelectBuilder().fromEntity(Student.class, "e").distinct()
                .column(property(Student.person().id().fromAlias("e")))
                .joinEntity("e", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(Student.person().id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .joinEntity("e", DQLJoinType.left, FefuProcessingStack.class, "st", eq(property(Student.person().id().fromAlias("e")), property(FefuProcessingStack.entityId().fromAlias("st"))))
                .where(eq(property(Student.status().active().fromAlias("e")), value(Boolean.TRUE)))
                .where(isNull(property(FefuProcessingStack.id().fromAlias("st"))))
                .createStatement(getSession()).list();

        int processedCnt = 0;
        DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(FefuProcessingStack.class);

        for (Long id : personIds)
        {
            processedCnt++;
            insertBuilder.value(FefuProcessingStack.entityId().s(), id);
            insertBuilder.value(FefuProcessingStack.stackId().s(), Person.ENTITY_NAME);
            insertBuilder.value(FefuProcessingStack.addTime().s(), new Date());
            insertBuilder.addBatch();

            if (processedCnt == 300)
            {
                createStatement(insertBuilder).execute();
                insertBuilder = new DQLInsertValuesBuilder(FefuProcessingStack.class);
                processedCnt = 0;
            }
        }

        if (processedCnt > 0) createStatement(insertBuilder).execute();
    }

    // TODO: Do not forget to kill this method (after the first sync at FEFU)
    public void prepareOrgstructDifferences(final OutputStream stream) throws Exception
    {
        List<OrgUnit> orgUnitsList = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou").column("ou")
                .where(eq(property(OrgUnit.archival().fromAlias("ou")), value(Boolean.FALSE)))
                .createStatement(getSession()).list();

        StringBuilder outputBuilder = new StringBuilder("Orgunits from OB ").append(orgUnitsList.size()).append("\n");

        int maxTypeLength = 0;
        //int maxStringLength = 0;

        Map<Long, List<OrgUnit>> orgUnitChildsMap = new HashMap<>();
        Map<String, OrgUnit> orgUnitTitlePathToOrgUnitMap = new HashMap<>();
        Map<String, List<OrgUnit>> orgUnitTitleToOrgUnitListMap = new HashMap<>();

        for (OrgUnit orgUnit : orgUnitsList)
        {
            //if (orgUnit.getTitle().length() > maxStringLength) maxStringLength = orgUnit.getTitle().length();
            if (orgUnit.getOrgUnitType().getTitle().length() > maxTypeLength)
                maxTypeLength = orgUnit.getOrgUnitType().getTitle().length();

            String orgUnitTitle = orgUnit instanceof TopOrgUnit ? orgUnit.getShortTitle().toUpperCase() : orgUnit.getTitle().toUpperCase();
            List<OrgUnit> orgUnitsByTitleList = orgUnitTitleToOrgUnitListMap.get(orgUnitTitle);
            if (null == orgUnitsByTitleList) orgUnitsByTitleList = new ArrayList<>();
            orgUnitsByTitleList.add(orgUnit);
            orgUnitTitleToOrgUnitListMap.put(orgUnitTitle, orgUnitsByTitleList);

            OrgUnit parentOrgUnit = orgUnit.getParent();
            StringBuilder titlePathBuilder = new StringBuilder(orgUnitTitle);
            while (null != parentOrgUnit)
            {
                String parentOrgUnitTitle = parentOrgUnit instanceof TopOrgUnit ? parentOrgUnit.getShortTitle() : parentOrgUnit.getTitle();
                titlePathBuilder.append("|").append(parentOrgUnitTitle.toUpperCase());
                parentOrgUnit = parentOrgUnit.getParent();
            }
            orgUnitTitlePathToOrgUnitMap.put(titlePathBuilder.toString(), orgUnit);

            parentOrgUnit = orgUnit.getParent();
            if (null != parentOrgUnit)
            {
                List<OrgUnit> childOrgUnitsList = orgUnitChildsMap.get(parentOrgUnit.getId());
                if (null == childOrgUnitsList) childOrgUnitsList = new ArrayList<>();
                childOrgUnitsList.add(orgUnit);
                orgUnitChildsMap.put(parentOrgUnit.getId(), childOrgUnitsList);
            } else
            {
                List<OrgUnit> childOrgUnitsList = orgUnitChildsMap.get(0L);
                if (null == childOrgUnitsList) childOrgUnitsList = new ArrayList<>();
                childOrgUnitsList.add(orgUnit);
                orgUnitChildsMap.put(0L, childOrgUnitsList);
            }
        }
/*
        for (Map.Entry<Long, List<OrgUnit>> entry : orgUnitChildsMap.entrySet())
        {
            Collections.sort(entry.getValue(), new Comparator<OrgUnit>()
            {
                @Override
                public int compare(OrgUnit o1, OrgUnit o2)
                {
                    int typeComparison = o1.getOrgUnitType().getTitle().compareTo(o2.getOrgUnitType().getTitle());

                    return 0 != typeComparison ? typeComparison : (o1.getTitle().compareTo(o2.getTitle()));
                }
            });
        }

        printOrgUnitNode(orgUnitChildsMap, TopOrgUnit.getInstance(), 0, maxTypeLength, maxStringLength);*/

        List<DepartmentType> departmentsList = new ArrayList<>();

        try
        {
            List<Object> objects = new ArrayList<>();
            ObjectFactory factory = new ObjectFactory();
            objects.add(factory.createDepartmentType());

            ServiceSoap_Service service = NsiReactorUtils.getServiceProvider();
            ServiceSoap_PortType port = service.getServiceSoap12();

            ServiceRequestType request = new ServiceRequestType();
            RoutingHeaderType header = new RoutingHeaderType();
            ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
            XDatagram xDatagram = factory.createXDatagram();
            xDatagram.getEntityList().addAll(objects);

            ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram));
            MessageElement datagramOut = new SOAPBodyElement(inStream);

            System.out.println(datagramOut.toString());

            header.setSourceId("OB");
            header.setOperationType(RoutingHeaderTypeOperationType.fromValue("retrieve"));
            header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
            request.setRoutingHeader(header);

            datagram.set_any(new MessageElement[]{datagramOut});
            request.setDatagram(datagram);

            ServiceResponseType response = port.retrieve(request);

            MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

            if (null != respMsg)
            {
                XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());
                for (Object retrievedObject : respDatagram.getEntityList())
                {
                    if (retrievedObject instanceof DepartmentType)
                    {
                        departmentsList.add((DepartmentType) retrievedObject);
                    }
                }
            }
        } catch (Exception e)
        {
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            throw e;
        }
        System.out.println("------- Retrieved from NSI " + departmentsList.size());

/*//TODO
        ByteArrayOutputStream out = null;
        InputStream input = null;
        try
        {
            out = new ByteArrayOutputStream();
            input = new BufferedInputStream(new FileInputStream("d:/retrieved24.10.2013 13_02.xml"));
            int data = 0;
            while ((data = input.read()) != -1)
            {
                out.write(data);
            }
        } finally
        {
            if (null != input)
            {
                input.close();
            }
            if (null != out)
            {
                out.close();
            }
        }
        XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, out.toByteArray());
        for (Object retrievedObject : respDatagram.getEntityList())
        {
            if (retrievedObject instanceof DepartmentType)
            {
                departmentsList.add((DepartmentType) retrievedObject);
            }
        }               /**/

        int maxStringLength = 0;
        DepartmentType rootDepartment = null;
        Map<String, DepartmentType> departmentTypeMap = new HashMap<>();
        Map<String, List<DepartmentType>> departmentChildsMap = new HashMap<>();

        for (DepartmentType department : departmentsList)
        {
            if (null == department.getID() || null == department.getDepartmentName()) continue;

            if (null == rootDepartment && null == department.getDepartmentUpper() && "ДВФУ".equals(department.getDepartmentName()))
                rootDepartment = department;

            if (department.getDepartmentName().length() > maxStringLength)
                maxStringLength = department.getDepartmentName().length();
            departmentTypeMap.put(department.getID(), department);

            String parentDepartmentGuid = null != department.getDepartmentUpper() ? department.getDepartmentUpper().getDepartment().getID() : null;
            if (null != parentDepartmentGuid)
            {
                if ("НЕУЧЕБНЫЕ ПОДРАЗДЕЛЕНИЯ".equals(department.getDepartmentName())) continue;
                List<DepartmentType> childDepartmentsList = departmentChildsMap.get(parentDepartmentGuid);
                if (null == childDepartmentsList) childDepartmentsList = new ArrayList<>();
                childDepartmentsList.add(department);
                departmentChildsMap.put(parentDepartmentGuid, childDepartmentsList);
            } else
            {
                List<DepartmentType> childDepartmentsList = departmentChildsMap.get("---");
                if (null == childDepartmentsList) childDepartmentsList = new ArrayList<>();
                childDepartmentsList.add(department);
                departmentChildsMap.put("---", childDepartmentsList);
            }
        }

        Map<String, String> departmentGuidToParentGuidMap = new HashMap<>();
        Map<String, List<DepartmentType>> actualDepartmentChildsMap = new HashMap<>();
        addAllChilds(departmentChildsMap, actualDepartmentChildsMap, rootDepartment);

        for (Map.Entry<String, List<DepartmentType>> entry : actualDepartmentChildsMap.entrySet())
        {
            Collections.sort(entry.getValue(), new Comparator<DepartmentType>()
            {
                @Override
                public int compare(DepartmentType o1, DepartmentType o2)
                {
                    return o1.getDepartmentName().compareTo(o2.getDepartmentName());
                }
            });
            for (DepartmentType department : entry.getValue())
                departmentGuidToParentGuidMap.put(department.getID(), entry.getKey());
        }

        Map<String, OrgUnit> guidToOrgUnitMap = new HashMap<>();
        for (Map.Entry<String, List<DepartmentType>> entry : actualDepartmentChildsMap.entrySet())
        {
            for (DepartmentType departmentType : entry.getValue())
            {
                /*List<OrgUnit> orgUnitList = orgUnitTitleToOrgUnitListMap.get(departmentType.getDepartmentName().toUpperCase());
                if (null != orgUnitList && orgUnitList.size() == 1)
                {
                    guidToOrgUnitMap.put(departmentType.getID(), orgUnitList.get(0));
                } else*/
                {
                    StringBuilder depTitlePath = new StringBuilder(departmentType.getDepartmentName().toUpperCase());
                    DepartmentType parentDep = departmentTypeMap.get(entry.getKey());
                    if (null != parentDep)
                    {
                        if (!DEPARTMENTS_TO_IGNORE.contains(parentDep.getDepartmentName()))
                            depTitlePath.append("|").append(parentDep.getDepartmentName().toUpperCase());
                        String parentDepGuid = null != parentDep.getDepartmentUpper() ? parentDep.getDepartmentUpper().getDepartment().getID() : null;
                        while (null != parentDepGuid)
                        {
                            parentDep = departmentTypeMap.get(parentDepGuid);
                            if (null != parentDep)
                            {
                                if (!DEPARTMENTS_TO_IGNORE.contains(parentDep.getDepartmentName()))
                                    depTitlePath.append("|").append(parentDep.getDepartmentName().toUpperCase());
                                parentDepGuid = null != parentDep.getDepartmentUpper() ? parentDep.getDepartmentUpper().getDepartment().getID() : null;
                            } else parentDepGuid = null;
                        }
                    }

                    OrgUnit orgUnit = orgUnitTitlePathToOrgUnitMap.get(depTitlePath.toString());
                    if (null != orgUnit) guidToOrgUnitMap.put(departmentType.getID(), orgUnit);
                }
            }
        }

        Set<OrgUnit> alreadyMappedOrgUnitsSet = new HashSet<>();
        printDepartmentNodeWithOrgUnitsMapping(rootDepartment, 0, actualDepartmentChildsMap, guidToOrgUnitMap, orgUnitChildsMap, alreadyMappedOrgUnitsSet, maxStringLength + 10, outputBuilder);
        outputBuilder.append("!!!! Mapped ").append(alreadyMappedOrgUnitsSet.size()).append(" of ").append(orgUnitsList.size()).append(" org units from OB. Total NSI departments list size is ").append(departmentsList.size());
        System.out.println(outputBuilder.toString());
        stream.write(outputBuilder.toString().getBytes("Cp1251")); //UTF-8
    }

    private void addAllChilds(Map<String, List<DepartmentType>> srcDepartmentsMap, Map<String, List<DepartmentType>> resultDepartmentsMap, DepartmentType department)
    {
        List<DepartmentType> childs = srcDepartmentsMap.get(department.getID());
        if (null != childs)
        {
            resultDepartmentsMap.put(department.getID(), childs);
            for (DepartmentType childDepartment : childs)
            {
                addAllChilds(srcDepartmentsMap, resultDepartmentsMap, childDepartment);
            }
        }
    }

    private void printDepartmentNodeWithOrgUnitsMapping(DepartmentType department, int level, Map<String, List<DepartmentType>> departmenChildsMap, Map<String, OrgUnit> depGuidToOrgUnitMap, Map<Long, List<OrgUnit>> orgUnitChildsMap, Set<OrgUnit> alreadyMappedOrgUnitsSet, int maxStringLength, StringBuilder outputBuilder)
    {
        //maxStringLength += 10;
        OrgUnit orgUnit = null;

        for (int i = 0; i < level; i++) outputBuilder.append("   ");
        if (null == department) outputBuilder.append("--------\n");
        else
        {
            outputBuilder.append(department.getDepartmentName());
            /*for (int k = 0; k <= maxStringLength - department.getDepartmentName().length() - (level * 3); k++)
                outputBuilder.append(" ");*/
            outputBuilder.append("|");
            orgUnit = depGuidToOrgUnitMap.get(department.getID());
            if (null != orgUnit)
            {
                outputBuilder.append(orgUnit.getTitle());
                alreadyMappedOrgUnitsSet.add(orgUnit);
            }
            outputBuilder.append("\n");
        }

        List<DepartmentType> childs = departmenChildsMap.get(null != department ? department.getID() : "---");
        if (null != childs)
        {
            for (DepartmentType child : childs)
                printDepartmentNodeWithOrgUnitsMapping(child, level + 1, departmenChildsMap, depGuidToOrgUnitMap, orgUnitChildsMap, alreadyMappedOrgUnitsSet, maxStringLength, outputBuilder);
        }

        if (null != orgUnit)
        {
            List<OrgUnit> childsList = orgUnitChildsMap.get(orgUnit.getId());
            if (null != childsList)
            {
                for (OrgUnit child : childsList)
                {
                    if (!alreadyMappedOrgUnitsSet.contains(child))
                    {
                        /*for (int k = 0; k <= maxStringLength; k++) outputBuilder.append(" ");*/
                        outputBuilder.append("|").append(child.getTitle()).append("\n");
                    }
                }
            }
        }
    }

    @Override
    public void doGetFullOrgStructureFromNsi() throws Exception
    {
        //TODO раскомментарить в случае необходимости локальной проверки на готовом xml файле
        /*_orgstructureFile = NsiReactorUtils.getEmulatedResponseDatagramElementsFromFile("d:/NsiDepartments17.12.2014 21_30.xml");
        if(null != _orgstructureFile) return;*/

        //TODO закомментарить в случае необходимости локальной проверки на готовом xml файле
        try
        {
            List<Object> objects = new ArrayList<>();
            ObjectFactory factory = new ObjectFactory();
            objects.add(factory.createDepartmentType());

            ServiceSoap_Service service = NsiReactorUtils.getServiceProvider();
            ServiceSoap_PortType port = service.getServiceSoap12();

            ServiceRequestType request = new ServiceRequestType();
            RoutingHeaderType header = new RoutingHeaderType();
            ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
            XDatagram xDatagram = factory.createXDatagram();
            xDatagram.getEntityList().addAll(objects);

            ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram));
            MessageElement datagramOut = new SOAPBodyElement(inStream);

            System.out.println(datagramOut.toString());

            header.setSourceId("OB");
            header.setOperationType(RoutingHeaderTypeOperationType.fromValue("retrieve"));
            header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
            request.setRoutingHeader(header);

            datagram.set_any(new MessageElement[]{datagramOut});
            request.setDatagram(datagram);

            System.out.println("Trying to get full orgstructure from nsi. Start");
            ServiceResponseType response = port.retrieve(request);

            MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

            if (null != respMsg)
            {
                _fileContent = response.getDatagram().get_any()[0];
                String orgStructStr = NsiReactorUtils.getXmlFormatted(response.getDatagram().get_any()[0].toString());
                _orgstructureFile = orgStructStr.getBytes();
                _orgstructureInProcess = false;
                System.out.println("Trying to get full orgstructure from nsi. SUCCESS");
            }
        } catch (Exception e)
        {
            _orgstructureFile = null;
            _orgstructureInProcess = false;
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            System.out.println("Trying to get full orgstructure from nsi. FAILED");
            throw e;
        }
    }

    @Override
    public void doCreateOrganizationsMappingReport() throws Exception
    {
        List<OrganizationType> organizationsList = new ArrayList<>();

        try
        {
            List<Object> objects = new ArrayList<>();
            ObjectFactory factory = new ObjectFactory();
            objects.add(factory.createOrganizationType());

            ServiceSoap_Service service = NsiReactorUtils.getServiceProvider();
            ServiceSoap_PortType port = service.getServiceSoap12();

            ServiceRequestType request = new ServiceRequestType();
            RoutingHeaderType header = new RoutingHeaderType();
            ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
            XDatagram xDatagram = factory.createXDatagram();
            xDatagram.getEntityList().addAll(objects);

            ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram));
            MessageElement datagramOut = new SOAPBodyElement(inStream);

            System.out.println(datagramOut.toString());

            header.setSourceId("OB");
            header.setOperationType(RoutingHeaderTypeOperationType.fromValue("retrieve"));
            header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
            request.setRoutingHeader(header);

            datagram.set_any(new MessageElement[]{datagramOut});
            request.setDatagram(datagram);

            ServiceResponseType response = port.retrieve(request);

            MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

            if (null != respMsg)
            {
                XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());
                for (Object retrievedObject : respDatagram.getEntityList())
                {
                    if (retrievedObject instanceof OrganizationType)
                    {
                        organizationsList.add((OrganizationType) retrievedObject);
                    }
                }
            }
        } catch (Exception e)
        {
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            throw e;
        }
        System.out.println("------- Retrieved from NSI " + organizationsList.size());

        System.out.println("\n\n\n");

        List<OrganizationWrapper> nsiList = new ArrayList<>();
        Map<String, List<OrganizationWrapper>> nsiChildsMap = new HashMap<>();
        Map<Integer, OrganizationWrapper> hashedOrganizationsMap = new HashMap<>();

        for (OrganizationType organization : organizationsList)
        {
            OrganizationWrapper wrapper = new OrganizationWrapper(organization);
            nsiList.add(wrapper);

            List<OrganizationWrapper> parentChildsList = nsiChildsMap.get(wrapper.getParentId());
            if (null == parentChildsList) parentChildsList = new ArrayList<>();
            parentChildsList.add(wrapper);
            nsiChildsMap.put(wrapper.getParentId(), parentChildsList);

            if (hashedOrganizationsMap.containsKey(wrapper.hashCode()))
                throw new UnsupportedOperationException("There are more than one wrappers with the same hashCode.");

            hashedOrganizationsMap.put(wrapper.hashCode(), wrapper);
        }

        /*for (Map.Entry<String, List<OrganizationWrapper>> entry : nsiChildsMap.entrySet())
        {
            Collections.sort(entry.getValue(), WRAPPER_COMPARATOR);
        }

        printRecursive(OrganizationWrapper.ROOT_ELEMENT, nsiChildsMap, 0);

        System.out.println("\n\n\n");*/

        List<Object[]> topOrgUnitsList = new DQLSelectBuilder().fromEntity(FefuTopOrgUnit.class, "e").column("e").column("ids")
                .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "ids", eq(property(FefuTopOrgUnit.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .createStatement(getSession()).list();

        int maxTitleLength = 0;
        boolean anyGuidExists = false;

        List<OrganizationWrapper> localList = new ArrayList<>();
        Map<String, List<OrganizationWrapper>> childsMap = new HashMap<>();
        for (Object[] item : topOrgUnitsList)
        {
            FefuTopOrgUnit topOrgUnit = (FefuTopOrgUnit) item[0];
            FefuNsiIds ids = (FefuNsiIds) item[1];

            OrganizationWrapper wrapper = new OrganizationWrapper(topOrgUnit, ids);
            localList.add(wrapper);

            anyGuidExists = anyGuidExists || null != ids;
            if (wrapper.getTitle().length() > maxTitleLength)
                maxTitleLength = wrapper.getTitle().length();

            List<OrganizationWrapper> parentChildsList = childsMap.get(wrapper.getParentId());
            if (null == parentChildsList) parentChildsList = new ArrayList<>();
            parentChildsList.add(wrapper);
            childsMap.put(wrapper.getParentId(), parentChildsList);


        }

        for (Map.Entry<String, List<OrganizationWrapper>> entry : childsMap.entrySet())
        {
            Collections.sort(entry.getValue(), WRAPPER_COMPARATOR);
        }

        maxTitleLength += anyGuidExists ? 60 : 20;
        Set<Integer> usedHashSet = new HashSet<>();
        StringBuilder result = new StringBuilder();
        printRecursive(OrganizationWrapper.ROOT_ELEMENT, result, childsMap, nsiChildsMap, hashedOrganizationsMap, usedHashSet, 0, maxTitleLength);
        System.out.println(result.toString());
    }

    private void printRecursive(String parentId, StringBuilder result, Map<String, List<OrganizationWrapper>> childsMap, Map<String, List<OrganizationWrapper>> nsiChildsMap, Map<Integer, OrganizationWrapper> hashedOrganizationsMap, Set<Integer> usedHashSet, int level, int maxLength)
    {
        List<OrganizationWrapper> wrappersList = new ArrayList();
        if (null != childsMap.get(parentId)) wrappersList.addAll(childsMap.get(parentId));
        if (null != nsiChildsMap.get(parentId)) wrappersList.addAll(nsiChildsMap.get(parentId));

        for (OrganizationWrapper wrapper : wrappersList)
        {
            if (wrapper.isNsiWrapper())
            {
                if (!usedHashSet.contains(wrapper.hashCode()))
                {
                    for (int i = 0; i < maxLength; i++) result.append(" ");
                    for (int i = 0; i < level; i++) result.append("    ");
                    result.append(wrapper.getTitle()).append(null != wrapper.getGuid() ? (" (" + wrapper.getGuid() + ")") : "").append("\n");
                }
            } else
            {
                StringBuilder halfLineResult = new StringBuilder();
                for (int i = 0; i < level; i++) halfLineResult.append("    ");
                halfLineResult.append(wrapper.getTitle()).append(null != wrapper.getGuid() ? (" (" + wrapper.getGuid() + ")") : "");
                result.append(halfLineResult);

                if (hashedOrganizationsMap.containsKey(wrapper.hashCode()))
                {
                    int dif = maxLength - halfLineResult.length();
                    OrganizationWrapper nsiWrapper = hashedOrganizationsMap.get(wrapper.hashCode());
                    for (int i = 0; i < dif; i++) result.append(" ");
                    for (int i = 0; i < level; i++) result.append("    ");
                    result.append(nsiWrapper.getTitle()).append(null != nsiWrapper.getGuid() ? (" (" + nsiWrapper.getGuid() + ")") : "");
                    usedHashSet.add(nsiWrapper.hashCode());
                }
                result.append("\n");
            }

            //if(result.length() > 0) System.out.println(result.toString());
            printRecursive(wrapper.getId(), result, childsMap, nsiChildsMap, hashedOrganizationsMap, usedHashSet, level + 1, maxLength);
        }
    }

    public void getNsiCatalogLogRowPackFile(Long logRowId)
    {
        FefuNsiLogRow logRow = getNotNull(FefuNsiLogRow.class, logRowId);
        DateFormatter dateFormatterWithTimeMillisec = new DateFormatter("dd.MM.yyyy HH:mm:ss.SSS");

        try
        {
            StringBuilder resultBody = new StringBuilder();
            String statusString = "Не известен";
            if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_INITIAL.intValue() == logRow.getMessageStatus())
                statusString = NSISyncManager.MESSAGE_STATUS_UNKNOWN_TITLE;
            else if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_OK.intValue() == logRow.getMessageStatus())
                statusString = NSISyncManager.MESSAGE_STATUS_SUCCESS_TITLE;
            else if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_WARN.intValue() == logRow.getMessageStatus())
                statusString = NSISyncManager.MESSAGE_STATUS_WARNING_TITLE;
            else if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_ERROR.intValue() == logRow.getMessageStatus())
                statusString = NSISyncManager.MESSAGE_STATUS_ERROR_TITLE;

            resultBody.append("Идентификатор справочника:                                             ").append(logRow.getDirectoryId()).append("\r\n");
            resultBody.append("Направление:                                                           ").append(logRow.getEventType()).append("\r\n"); //TODO
            resultBody.append("Тип операции:                                                          ").append(logRow.getOperationType()).append("\r\n");
            resultBody.append("Идентификатор сообщения:                                               ").append(logRow.getMessageId()).append("\r\n");
            resultBody.append("Идентификатор сообщения, ответом на которое является это сообщение:    ").append(getNotNullString(logRow.getCorrelationId())).append("\r\n");
            resultBody.append("Идентификатор родительского сообщения по отношению к этому сообщению:  ").append(getNotNullString(logRow.getParentId())).append("\r\n");
            resultBody.append("Код системы-отправителя сообщения:                                     ").append(logRow.getSourceId()).append("\r\n");
            resultBody.append("Код системы-получателя сообщения:                                      ").append(getNotNullString(logRow.getDestinationId())).append("\r\n");
            resultBody.append("Код системы-получателя ответа на это сообщение:                        ").append(getNotNullString(logRow.getReplyDestinationId())).append("\r\n");
            resultBody.append("Код системы-получателя квитовки на это сообщение:                      ").append(getNotNullString(logRow.getTicketDestinationId())).append("\r\n");
            resultBody.append("Время и дата сообщения:                                                ").append(dateFormatterWithTimeMillisec.format(logRow.getMessageTime())).append("\r\n");
            resultBody.append("Статус сообщения:                                                      ").append(logRow.getMessageStatus()).append(" (").append(statusString).append(")").append("\r\n\r\n");
            resultBody.append("Комментарий к сообщению:                                               ").append(getNotNullString(logRow.getMessageComment())).append("\r\n\r\n");
            resultBody.append("Технический заголовок сообщения:                                       \r\n").append(getNotNullString(NsiReactorUtils.getXmlFormatted(logRow.getRoutingHeader())).replaceAll("x-datagram", "header")).append("\r\n\r\n");
            resultBody.append("Тело сообщения:                                                        \r\n").append(getNotNullString(NsiReactorUtils.getXmlFormatted(logRow.getMessageBody()))).append("\r\n\r\n");
            resultBody.append("SOAP Header сообщения:                                                 ").append(getNotNullString(logRow.getSoapHeader())).append("\r\n\r\n");
            resultBody.append("SOAP Body сообщения:                                                   ").append(getNotNullString(logRow.getSoapBody())).append("\r\n\r\n");
            resultBody.append("SOAP Fault сообщения:                                                  ").append(getNotNullString(logRow.getSoapFault())).append("\r\n\r\n");

            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("application/txt").fileName(logRow.getMessageId() + ".txt").document(resultBody.toString().getBytes()), true);

        } catch (Exception e)
        {
            throw new ApplicationException("Не судьба");
        }
    }

    private String getNotNullString(String src)
    {
        return null != src ? src : "";
    }

    public Map<String, Long> getGuidToRealObjectIdMap(Set<String> guidsSet)
    {
        List<Object[]> guidsList = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e")
                .column(property(FefuNsiIds.guid().fromAlias("e")))
                .column(property(FefuNsiIds.entityId().fromAlias("e")))
                .where(in(property(FefuNsiIds.guid().fromAlias("e")), guidsSet))
                .createStatement(getSession()).list();

        Map<String, Long> guidToRealObjectIdMap = new HashMap<>();
        for (Object[] item : guidsList)
        {
            guidToRealObjectIdMap.put((String) item[0], (Long) item[1]);
        }

        return guidToRealObjectIdMap;
    }

    public boolean isOrgStructureInProcess()
    {
        return _orgstructureInProcess;
    }

    public byte[] getOrgStructureFile()
    {
        return _orgstructureFile;
    }

    public byte[] getOrgstructureDifferencesFile()
    {
        //TODO закомментарить в случае необходимости локальной проверки на готовом xml файле
        if (null == _fileContent)
            throw new ApplicationException("Данные об оргструктуре НСИ не получены. Необходимо сначала получить оргструктуру из НСИ. Нажмите кнопку \"Выгрузить подразделения из НСИ\".");

        try
        {
            //TODO закомментарить в случае необходимости локальной проверки на готовом xml файле
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, _fileContent.getAsString().getBytes());
            // TODO использовать в дебаге в случае необходимости локальной проверки на готовом xml файле
            // TODO XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, _orgstructureFile);

            List<INsiEntity> departmentsList = new ArrayList<>();
            for (Object retrievedObject : respDatagram.getEntityList())
            {
                if (retrievedObject instanceof DepartmentType)
                {
                    departmentsList.add((DepartmentType) retrievedObject);
                }
            }

            NsiReactorUtils.OrgStructureWrapper orgStructureWrapper = new NsiReactorUtils.OrgStructureWrapper();
            orgStructureWrapper.useSeparators = true;
            orgStructureWrapper.printGuids = true;

            NsiReactorUtils.prepareActualDepartmentsStructure(departmentsList, orgStructureWrapper);
            NsiReactorUtils.prepareOBOrgStructure(orgStructureWrapper);
            NsiReactorUtils.printMappedOrgStructureRecursive(null, null, orgStructureWrapper, 0);

            orgStructureWrapper.outputBuilder.append("\n");
            orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.guidToDepartmentMap.values().size() + " departments received.\n");
            orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.idToOrgUnitMap.values().size() + " orgUnits loaded from database.\n");
            orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.mappedByGuidCnt + " mapped by guid\n");
            orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.mappedByTitleCnt + " mapped by title\n");
            orgStructureWrapper.outputBuilder.append("---------------------- Total " + orgStructureWrapper.alreadyMappedOrgUnitsSet.size() + " mapped\n");

            orgStructureWrapper.outputBuilder.append("\n---------------------- Unmapped OB org units: \n");
            List<OrgUnit> unmappedOrgUnitList = new ArrayList<>();
            for (Long unusedId : orgStructureWrapper.unmappedOrgUnitIds)
            {
                OrgUnit orgUnit = orgStructureWrapper.idToOrgUnitMap.get(unusedId);
                if (null != orgUnit) unmappedOrgUnitList.add(orgUnit);
            }

            Collections.sort(unmappedOrgUnitList, new Comparator<OrgUnit>()
            {
                @Override
                public int compare(OrgUnit o1, OrgUnit o2)
                {
                    return o1.getTitleWithParent().compareTo(o2.getTitleWithParent());
                }
            });

            for (OrgUnit orgUnit : unmappedOrgUnitList)
            {
                orgStructureWrapper.outputBuilder.append(orgUnit.getId());
                orgStructureWrapper.outputBuilder.append(". ").append(orgStructureWrapper.useSeparators ? "|" : "");
                orgStructureWrapper.outputBuilder.append(orgUnit.getTitleWithParent());
                orgStructureWrapper.outputBuilder.append("\n");
            }

            return orgStructureWrapper.outputBuilder.toString().getBytes();

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void doDeleteOrgUnitsTree(Long orgUnitId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou").column(property("ou"));
        DQLCTEBuilder2 cte = SharedDQLUtils.createChildTreeRecursiveCTE("tree", OrgUnit.class, orgUnitId);
        builder.with(cte.buildCTE());
        builder.where(eqSubquery(property(OrgUnit.id().fromAlias("ou")), DQLSubselectType.any, new DQLSelectBuilder().fromEntity("tree", "t").buildQuery()));
        List<OrgUnit> ouList = builder.createStatement(getSession()).list();

        Map<Long, Integer> childCountsMap = new HashMap<>();
        childCountsMap.put(0L, 0);

        Set<Long> alreadyDeletedOrgUnits = new HashSet<>();

        while (!childCountsMap.isEmpty())
        {
            childCountsMap.clear();

            for (OrgUnit ou : ouList)
            {
                if (alreadyDeletedOrgUnits.contains(ou.getId())) continue;

                Integer count = childCountsMap.get(ou.getId());
                if (null == count) childCountsMap.put(ou.getId(), 0);

                Long parentId = ou.getParent().getId();
                Integer parentCount = childCountsMap.get(parentId);
                if (null == parentCount) parentCount = 1;
                else parentCount++;
                childCountsMap.put(parentId, parentCount);
            }

            for (Map.Entry<Long, Integer> entry : childCountsMap.entrySet())
            {
                if (0 == entry.getValue())
                {
                    //System.out.println(entry.getKey() + " - " + entry.getValue());
                    alreadyDeletedOrgUnits.add(entry.getKey());
                    delete(entry.getKey());
                }
            }
        }
    }

    @Override
    public void doCorrectOrgstructSettings()
    {
        Map<Long, Set<Long>> settingsMap = new HashMap<>();
        List<Object[]> settings = new DQLSelectBuilder().fromEntity(OrgUnitTypeRestriction.class, "e")
                .column(property(OrgUnitTypeRestriction.parent().fromAlias("e")))
                .column(property(OrgUnitTypeRestriction.child().fromAlias("e")))
                .order(property(OrgUnitTypeRestriction.parent().fromAlias("e")))
                .createStatement(getSession()).list();

        for (Object[] item : settings)
        {
            OrgUnitType out = (OrgUnitType) item[0];
            Set<Long> typeTypes = settingsMap.get(out.getId());
            if (null == typeTypes) typeTypes = new HashSet<>();
            typeTypes.add(((OrgUnitType) item[1]).getId());
            settingsMap.put(out.getId(), typeTypes);
        }


        List<Object[]> types = new DQLSelectBuilder().fromEntity(OrgUnit.class, "e").distinct()
                .column(property(OrgUnit.parent().orgUnitType().fromAlias("e")))
                .column(property(OrgUnit.orgUnitType().fromAlias("e")))
                .order(property(OrgUnit.parent().orgUnitType().priority().fromAlias("e")))
                .createStatement(getSession()).list();

        int processedCnt = 0;
        DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(OrgUnitTypeRestriction.class);

        for (Object[] item : types)
        {
            OrgUnitType parent = (OrgUnitType) item[0];
            OrgUnitType child = (OrgUnitType) item[1];
            Set<Long> childsSet = settingsMap.get(parent.getId());

            if (null == childsSet || !childsSet.contains(child.getId()))
            {
                processedCnt++;
                insertBuilder.value(OrgUnitTypeRestriction.parent().s(), parent.getId());
                insertBuilder.value(OrgUnitTypeRestriction.child().s(), child.getId());
                insertBuilder.addBatch();

                if (processedCnt == 300)
                {
                    createStatement(insertBuilder).execute();
                    insertBuilder = new DQLInsertValuesBuilder(OrgUnitTypeRestriction.class);
                    processedCnt = 0;
                }
            }
        }

        if (processedCnt > 0) createStatement(insertBuilder).execute();
    }

    @Override
    public Map<String, OrgUnit> getGuidToOrgUnitMap()
    {
        Map<String, OrgUnit> resultMap = new HashMap<>();
        List<Object[]> ouList = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou").column(property(FefuNsiIds.guid().fromAlias("ids"))).column(property("ou"))
                .joinEntity("ou", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(OrgUnit.id().fromAlias("ou")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .createStatement(getSession()).list();

        for (Object[] item : ouList) resultMap.put((String) item[0], (OrgUnit) item[1]);
        return resultMap;
    }

    @Override
    public void doChangeGuid(Long entityId, Long nsiId, String guid, boolean newGuid)
    {
        FefuNsiIds ids = null != nsiId ? get(FefuNsiIds.class, nsiId) : null;
        List<FefuNsiIds> idsList = getList(FefuNsiIds.class, FefuNsiIds.entityId(), entityId);

        if (idsList.size() == 1 && null != ids && null != ids.getGuid() && ids.getGuid().equals(guid))
            throw new ApplicationException("Новый GUID должен отличаться от старого.");

        FefuNsiIds NsiIdFromDB = getByNaturalId(new FefuNsiIds.NaturalId(guid));
        if (null != NsiIdFromDB && !entityId.equals(NsiIdFromDB.getEntityId()))
            throw new ApplicationException("Данный GUID занят другим объектом с ID = " + NsiIdFromDB.getEntityId() + ".");

        FefuNsiIds newNsiId = new FefuNsiIds();
        newNsiId.update(ids);

        if (!newGuid)
        {
            for (FefuNsiIds id : idsList) delete(id);
            getSession().flush();
        } else
        {

            IEntityMeta meta = EntityRuntime.getMeta(entityId);
            if (null != meta) newNsiId.setEntityType(meta.getName());
            newNsiId.setEntityId(entityId);
        }

        newNsiId.setGuid(guid);
        saveOrUpdate(newNsiId);
    }
}