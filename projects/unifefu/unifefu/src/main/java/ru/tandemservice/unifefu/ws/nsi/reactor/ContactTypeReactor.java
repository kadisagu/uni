/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiContactType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact;
import ru.tandemservice.unifefu.ws.nsi.datagram.ContactKindType;
import ru.tandemservice.unifefu.ws.nsi.datagram.ContactType;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 08.01.2015
 */
public class ContactTypeReactor extends SimpleCatalogEntityNewReactor<ContactType, FefuNsiPersonContact>
{
    public static final String REACTOR_BEAN_NAME = "ContactTypeReactor1";

    public static final String CONTACT_KIND_CODE_INFO_ADDR = "00001";
    public static final String CONTACT_KIND_CODE_REG_ADDR = "00002";
    public static final String CONTACT_KIND_CODE_FACT_ADDR = "00003";
    public static final String CONTACT_KIND_CODE_INTER_ADDR = "00004";
    public static final String CONTACT_KIND_CODE_TEMP_REG_ADDR = "00023";
    public static final String CONTACT_KIND_CODE_PHONE = "00007";
    public static final String CONTACT_KIND_CODE_CONT_PHONE = "00008";
    public static final String CONTACT_KIND_CODE_WORK_PHONE = "00020";
    public static final String CONTACT_KIND_CODE_HOME_PHONE = "00021";
    public static final String CONTACT_KIND_CODE_MOBILE_PHONE = "00022";
    public static final String CONTACT_KIND_CODE_EMAIL = "00024";
    public static final String CONTACT_KIND_CODE_PHONE_ALT = "00026";
    public static final String CONTACT_KIND_CODE_EMAIL_ALT = "00027";

    private static final Set<String> PREFIX_REGION_TYPE_CODES = new HashSet<>();
    private static final Set<String> CONTACT_KIND_ADDRESS_CODES = new HashSet<>();

    static
    {
        //TODO жуткий грабельный склад
        CONTACT_KIND_ADDRESS_CODES.add(CONTACT_KIND_CODE_INFO_ADDR);
        CONTACT_KIND_ADDRESS_CODES.add(CONTACT_KIND_CODE_REG_ADDR);
        CONTACT_KIND_ADDRESS_CODES.add(CONTACT_KIND_CODE_FACT_ADDR);
        CONTACT_KIND_ADDRESS_CODES.add(CONTACT_KIND_CODE_INTER_ADDR);
        CONTACT_KIND_ADDRESS_CODES.add(CONTACT_KIND_CODE_TEMP_REG_ADDR);
        PREFIX_REGION_TYPE_CODES.add("106");
    }

    private Map<String, FefuNsiContactType> _contactKindsMap = new HashMap<>();
    private Map<String, Person> _personsMap = new HashMap<>();

    private Map<String, ContactKindType> _contactKindGuidsMap = new HashMap<>();
    private Map<String, HumanType> _humanGuidsMap = new HashMap<>();

    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<FefuNsiPersonContact> getEntityClass()
    {
        return FefuNsiPersonContact.class;
    }

    @Override
    public Class<ContactType> getNSIEntityClass()
    {
        return ContactType.class;
    }

    @Override
    public ContactType getCatalogElementRetrieveRequestObject(String guid)
    {
        // TODO: Имеет смысл переопределить метод получения полного списка контрагентов из НСИ, поскольку их может быть очень много.
        ContactType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createContactType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    protected void preProcessNsiEntities(List<INsiEntity> itemsToProcess)
    {
        // Подготавливаем мап видов контактной информации
        DQLSelectBuilder contactKindsBuilder = new DQLSelectBuilder().fromEntity(FefuNsiContactType.class, "e")
                .joinEntity("e", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(FefuNsiContactType.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .column(property("e")).column(property(FefuNsiIds.guid().fromAlias("ids")));

        List<Object[]> contactKindList = DataAccessServices.dao().getList(contactKindsBuilder);

        for (Object[] item : contactKindList)
        {
            FefuNsiContactType kind = (FefuNsiContactType) item[0];
            _contactKindsMap.put(kind.getUserCode(), kind);
            _contactKindsMap.put((String) item[1], kind);
        }


        // Подготовливаем мап физ лиц для обрбаотки адресов
        Set<String> personGuids = new HashSet<>();
        for (INsiEntity nsiEntity : itemsToProcess)
        {
            if (nsiEntity instanceof ContactType)
            {
                ContactType contact = (ContactType) nsiEntity;
                if (null != contact.getHumanID() && null != contact.getHumanID().getHuman())
                    personGuids.add(contact.getHumanID().getHuman().getID());
            }
        }

        DQLSelectBuilder personBuilder = new DQLSelectBuilder().fromEntity(Person.class, "e").column(property("e")).column(property(FefuNsiIds.guid().fromAlias("ids")))
                .joinEntity("e", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(Person.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .where(in(property(FefuNsiIds.guid().fromAlias("ids")), personGuids));

        List<Object[]> personsList = DataAccessServices.dao().getList(personBuilder);

        for (Object[] item : personsList)
        {
            Person person = (Person) item[0];
            _personsMap.put((String) item[1], person);
            _personsMap.put(String.valueOf(person.getId()), person);
        }
    }

    /*@Override
    protected void initEntityMap(Set<Long> entityIds)
    {
        // ничего не инициализируем, всё сделано на предобработке (preProcessNsiEntities)
    } */

    @Override
    protected void prepareRelatedObjectsMap(Set<Long> entityIds)
    {
        // Подготавливаем мап видов контактной информации
        DQLSelectBuilder contactKindsBuilder = new DQLSelectBuilder().fromEntity(FefuNsiContactType.class, "e")
                .joinEntity("e", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(FefuNsiContactType.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .column(property(FefuNsiContactType.id().fromAlias("e"))).column(property(FefuNsiContactType.userCode().fromAlias("e"))).column(property(FefuNsiIds.guid().fromAlias("ids")));

        List<Object[]> contactKindList = DataAccessServices.dao().getList(contactKindsBuilder);

        for (Object[] item : contactKindList)
        {
            ContactKindType kind = new ContactKindType();
            kind.setID((String) item[2]);
            _contactKindGuidsMap.put(String.valueOf(item[0]), kind);
            _contactKindGuidsMap.put((String) item[1], kind);
        }


        // Подготавливаем ацкий мап соответствия ид персоны рыбе с гуидом
        DQLSelectBuilder personBuilder = new DQLSelectBuilder().fromEntity(Person.class, "e").column(property(Person.id().fromAlias("e"))).column(property(FefuNsiIds.guid().fromAlias("ids")))
                .joinEntity("e", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(Person.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))));

        List<Object[]> personsList = DataAccessServices.dao().getList(personBuilder);

        for (Object[] item : personsList)
        {
            HumanType human = new HumanType();
            human.setID((String) item[1]);
            _humanGuidsMap.put(String.valueOf(item[0]), human);
        }
    }

    @Override
    public ContactType createEntity(CoreCollectionUtils.Pair<FefuNsiPersonContact, FefuNsiIds> entityPair)
    {
        //TODO надо предварительно создавать связанный объект по адресам персоны
        ContactType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createContactType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setContactType(entityPair.getX().getTypeStr());
        nsiEntity.setContactDefault(entityPair.getX().isDefaultAddr() ? "1" : "0");
        nsiEntity.setContactComment(entityPair.getX().getComment());
        nsiEntity.setContactDescription(entityPair.getX().getDescription());
        nsiEntity.setContactField1(entityPair.getX().getField1());
        nsiEntity.setContactField2(entityPair.getX().getField2());
        nsiEntity.setContactField3(entityPair.getX().getField3());
        nsiEntity.setContactField4(entityPair.getX().getField4());
        nsiEntity.setContactField5(entityPair.getX().getField5());
        nsiEntity.setContactField6(entityPair.getX().getField6());
        nsiEntity.setContactField7(entityPair.getX().getField7());
        nsiEntity.setContactField8(entityPair.getX().getField8());
        nsiEntity.setContactField9(entityPair.getX().getField9());
        nsiEntity.setContactField10(entityPair.getX().getField10());

        ContactKindType kind = _contactKindGuidsMap.get(String.valueOf(entityPair.getX().getType().getId()));
        if (null != kind)
        {
            ContactType.ContactKindID kindID = new ContactType.ContactKindID();
            kindID.setContactKind(kind);
            nsiEntity.setContactKindID(kindID);
        }

        HumanType human = _humanGuidsMap.get(String.valueOf(entityPair.getX().getPerson().getId()));
        if (null != human)
        {
            ContactType.HumanID humanID = new ContactType.HumanID();
            humanID.setHuman(human);
            nsiEntity.setHumanID(humanID);
        }

        return nsiEntity;
    }

    @Override
    public FefuNsiPersonContact createEntity(ContactType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getContactKindID() || null == nsiEntity.getContactKindID().getContactKind()
                || null == nsiEntity.getHumanID() || null == nsiEntity.getHumanID().getHuman())
            return null;

        FefuNsiContactType kind = _contactKindsMap.get(nsiEntity.getContactKindID().getContactKind().getID());
        Person person = _personsMap.get(nsiEntity.getHumanID().getHuman().getID());
        if (null == kind || null == person) return null;

        FefuNsiPersonContact entity = new FefuNsiPersonContact();
        entity.setTypeStr(nsiEntity.getContactType());
        entity.setPerson(person);
        entity.setType(kind);

        Boolean def = null != nsiEntity.getContactDefault() && "1".equals(nsiEntity.getContactDefault()) ? Boolean.TRUE : Boolean.FALSE;
        entity.setDefaultAddr(def);

        entity.setComment(nsiEntity.getContactComment());
        entity.setDescription(nsiEntity.getContactDescription());
        entity.setField1(nsiEntity.getContactField1());
        entity.setField2(nsiEntity.getContactField2());
        entity.setField3(nsiEntity.getContactField3());
        entity.setField4(nsiEntity.getContactField4());
        entity.setField5(nsiEntity.getContactField5());
        entity.setField6(nsiEntity.getContactField6());
        entity.setField7(nsiEntity.getContactField7());
        entity.setField8(nsiEntity.getContactField8());
        entity.setField9(nsiEntity.getContactField9());
        entity.setField10(nsiEntity.getContactField10());

        if (CONTACT_KIND_CODE_REG_ADDR.equals(kind.getUserCode()))
            entity.setAddress(person.getIdentityCard().getAddress());
        else if (CONTACT_KIND_CODE_FACT_ADDR.equals(kind.getUserCode()))
            entity.setAddress(person.getAddress());

        return entity;
    }

    @Override
    public FefuNsiPersonContact updatePossibleDuplicateFields(ContactType nsiEntity, CoreCollectionUtils.Pair<FefuNsiPersonContact, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (null != nsiEntity.getContactKindID() && null != nsiEntity.getContactKindID().getContactKind())
            {
                FefuNsiContactType kind = _contactKindsMap.get(nsiEntity.getContactKindID().getContactKind().getID());
                entityPair.getX().setType(kind);
            }

            if (null != nsiEntity.getHumanID().getHuman())
            {
                Person person = _personsMap.get(nsiEntity.getHumanID().getHuman().getID());
                entityPair.getX().setPerson(person);

                FefuNsiContactType kind = entityPair.getX().getType();
                if (null != kind)
                {
                    if (CONTACT_KIND_CODE_REG_ADDR.equals(kind.getUserCode()))
                        entityPair.getX().setAddress(person.getIdentityCard().getAddress());
                    else if (CONTACT_KIND_CODE_FACT_ADDR.equals(kind.getUserCode()))
                        entityPair.getX().setAddress(person.getAddress());
                }
            }

            entityPair.getX().setTypeStr(nsiEntity.getContactType());

            Boolean def = null != nsiEntity.getContactDefault() && "1".equals(nsiEntity.getContactDefault()) ? Boolean.TRUE : Boolean.FALSE;
            entityPair.getX().setDefaultAddr(def);

            entityPair.getX().setComment(nsiEntity.getContactComment());
            entityPair.getX().setDescription(nsiEntity.getContactDescription());
            entityPair.getX().setField1(nsiEntity.getContactField1());
            entityPair.getX().setField2(nsiEntity.getContactField2());
            entityPair.getX().setField3(nsiEntity.getContactField3());
            entityPair.getX().setField4(nsiEntity.getContactField4());
            entityPair.getX().setField5(nsiEntity.getContactField5());
            entityPair.getX().setField6(nsiEntity.getContactField6());
            entityPair.getX().setField7(nsiEntity.getContactField7());
            entityPair.getX().setField8(nsiEntity.getContactField8());
            entityPair.getX().setField9(nsiEntity.getContactField9());
            entityPair.getX().setField10(nsiEntity.getContactField10());

            return entityPair.getX();
        }
        return null;
    }

    @Override
    public ContactType updateNsiEntityFields(ContactType nsiEntity, FefuNsiPersonContact entity)
    {
        //TODO надо предварительно создавать связанный объект по адресам персоны
        nsiEntity.setContactType(entity.getTypeStr());
        nsiEntity.setContactDefault(entity.isDefaultAddr() ? "1" : "0");
        nsiEntity.setContactComment(entity.getComment());
        nsiEntity.setContactDescription(entity.getDescription());
        nsiEntity.setContactField1(entity.getField1());
        nsiEntity.setContactField2(entity.getField2());
        nsiEntity.setContactField3(entity.getField3());
        nsiEntity.setContactField4(entity.getField4());
        nsiEntity.setContactField5(entity.getField5());
        nsiEntity.setContactField6(entity.getField6());
        nsiEntity.setContactField7(entity.getField7());
        nsiEntity.setContactField8(entity.getField8());
        nsiEntity.setContactField9(entity.getField9());
        nsiEntity.setContactField10(entity.getField10());

        ContactKindType kind = _contactKindGuidsMap.get(String.valueOf(entity.getType().getId()));
        if (null != kind)
        {
            ContactType.ContactKindID kindID = new ContactType().getContactKindID();
            kindID.setContactKind(kind);
            nsiEntity.setContactKindID(kindID);
        }

        HumanType human = _humanGuidsMap.get(String.valueOf(entity.getPerson().getId()));
        if (null != human)
        {
            ContactType.HumanID humanID = new ContactType.HumanID();
            humanID.setHuman(human);
            nsiEntity.setHumanID(humanID);
        }

        return nsiEntity;
    }
}