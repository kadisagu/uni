/* $Id$ */
package ru.tandemservice.unifefu.base.ext.UniStudent.ui.OrgUnitList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.OrgUnitList.UniStudentOrgUnitList;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.FefuEduProgramEducationOrgUnitAddon;
import ru.tandemservice.unifefu.base.ext.UniStudent.UniStudentExtManager;

/**
 * @author Nikolay Fedorovskih
 * @since 21.10.2013
 */
@Configuration
public class UniStudentOrgUnitListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + UniStudentOrgUnitListExtUI.class.getSimpleName();

    @Autowired
    private UniStudentOrgUnitList _uniStudentOrgUnitList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_uniStudentOrgUnitList.presenterExtPoint())
                .replaceDataSource(searchListDS(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS, _uniStudentOrgUnitList.studentSearchListDSColumnExtPoint(), FefuStudentManager.instance().studentOrgUnitListExtSearchListDSHandler()))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(selectDS(UniStudentExtManager.BASE_EDU_DS, FefuStudentManager.instance().baseEduDSHandler()))
                .replaceAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, FefuEduProgramEducationOrgUnitAddon.class))
                .addAddon(uiAddon(ADDON_NAME, UniStudentOrgUnitListExtUI.class))
                .create();
    }

}