package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.RejectCommentDialog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author ilunin
 * @since 13.10.2014
 */
@Configuration
public class FefuEduPlanRejectCommentDialog extends BusinessComponentManager
{
    public static final String CHANGED_STATE = " changedState";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }
}