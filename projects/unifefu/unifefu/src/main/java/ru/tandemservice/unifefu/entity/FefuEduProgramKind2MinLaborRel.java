package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.unifefu.entity.gen.FefuEduProgramKind2MinLaborRelGen;

/**
 * Связь вида образовательной программы c минимальной трудоемкостью дисциплины (ДВФУ)
 */
public class FefuEduProgramKind2MinLaborRel extends FefuEduProgramKind2MinLaborRelGen
{
    public Double getLaborAsDouble()
    {
        long labor = getLabor();
        return UniEppUtils.wrap(labor < 0 ? null : labor);
    }

    public void setLaborAsDouble(Double value)
    {
        setLabor(UniEppUtils.unwrap(value));
    }
}