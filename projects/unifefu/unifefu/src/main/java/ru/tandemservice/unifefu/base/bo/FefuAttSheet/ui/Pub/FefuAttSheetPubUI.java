/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.FefuAttSheetManager;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.AddEdit.FefuAttSheetAddEdit;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.AddEdit.FefuAttSheetAddEditUI;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.AddEdit.FefuAttSheetWrapper;
import ru.tandemservice.unifefu.entity.SessionAttSheetDocument;
import ru.tandemservice.unifefu.entity.SessionAttSheetOperation;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 09.07.2014
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "documentId", required = true),
		@Bind(key = FefuAttSheetPubUI.ORGUNIT_ID_PARAM, binding = "orgUnitId")})
public class FefuAttSheetPubUI extends UIPresenter
{
	public static final String ORGUNIT_ID_PARAM = "orgUnitId";

	private Long documentId;
	private Long orgUnitId;

	private SessionAttSheetDocument _document;

	private List<FefuAttSheetWrapper> _rowList;
	private String _ppsList;
	private FefuAttSheetWrapper _currentRow;

	private boolean useCurrentRating;

	@Override
	public void onComponentRefresh()
	{

		_document = DataAccessServices.dao().getNotNull(getDocumentId());
		setRowList(new ArrayList<FefuAttSheetWrapper>());
		List<SessionAttSheetOperation> operations = DataAccessServices.dao()
				.getList(SessionAttSheetOperation.class,
				         SessionAttSheetOperation.slot().document().s(), getDocument()
				);

		useCurrentRating = ISessionBrsDao.instance.get().getSettings().isUseCurrentRatingForTransferDocument();

		for (SessionAttSheetOperation operation : operations)
		{
			operation.getSlot().getCommission();
			getRowList().add(new FefuAttSheetWrapper(operation));
		}

		Collection<SessionComissionPps> commissionPps = FefuAttSheetManager.instance().dao().getCommissionPps(getDocument());
		setPpsList(CommonBaseStringUtil.join(commissionPps, SessionComissionPps.pps().shortTitle().s(), ";"));
	}

	//Handlers

	public void onClickEdit()
	{
		_uiActivation.asRegion(FefuAttSheetAddEdit.class)
				.parameter(FefuAttSheetAddEditUI.DOCUMENT_ID, getDocumentId())
				.parameter(FefuAttSheetAddEditUI.ORG_UNIT_ID, getOrgUnitId())
				.activate();
	}

	public void onClickDelete()
	{
		FefuAttSheetManager.instance().dao().deleteAttSheetDocument(getDocument());
		deactivate();
	}

	public void onClickPrint()
	{
		String COMPONENT_NAME = "ru.tandemservice.unifefu.component.orgunit.AttSheetPrintRtf";
		getActivationBuilder().asRegion(COMPONENT_NAME)
				.parameter("sessionTransferDocumentId", getDocumentId())
				.activate();
	}

	public Long getDocumentId()
	{
		return documentId;
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setDocumentId(Long documentId)
	{
		this.documentId = documentId;
	}

	public Long getOrgUnitId()
	{
		return orgUnitId;
	}

	public void setOrgUnitId(Long orgUnitId)
	{
		this.orgUnitId = orgUnitId;
	}

	public SessionAttSheetDocument getDocument()
	{
		return _document;
	}

	@SuppressWarnings("UnusedDeclaration")
	public boolean isUseCurrentRating()
	{
		return useCurrentRating;
	}

	public Integer getColumnCount()
	{
		return isUseCurrentRating() ? 6 : 5;
	}

	@SuppressWarnings("UnusedDeclaration")
	public FefuAttSheetWrapper getCurrentRow()
	{
		return _currentRow;
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setCurrentRow(FefuAttSheetWrapper currentRow)
	{
		_currentRow = currentRow;
	}

	public List<FefuAttSheetWrapper> getRowList()
	{
		return _rowList;
	}

	public void setRowList(List<FefuAttSheetWrapper> rowList)
	{
		_rowList = rowList;
	}

	public String getPpsList()
	{
		return _ppsList;
	}

	private void setPpsList(String ppsList)
	{
		_ppsList = ppsList;
	}
}
