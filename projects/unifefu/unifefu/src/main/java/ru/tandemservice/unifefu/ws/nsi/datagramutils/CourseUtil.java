/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.CourseType;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 07.05.2014
 */
public class CourseUtil extends SimpleNsiCatalogUtil<CourseType, Course>
{
    public static final String COURSE_NUMBER_NSI_FIELD = "CourseNumber";
    public static final String COURSE_NUMBER_FIELD = "intValue";

    @Override
    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            super.getNsiFields();
            NSI_FIELDS.add(COURSE_NUMBER_NSI_FIELD);
        }
        return NSI_FIELDS;
    }

    @Override
    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            super.getEntityFields();
            ENTITY_FIELDS.add(Course.intValue().s());
        }
        return ENTITY_FIELDS;
    }

    @Override
    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            super.getNsiToObFieldsMap();
            NSI_TO_OB_FIELDS_MAP.put(COURSE_NUMBER_NSI_FIELD, Course.intValue().s());
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    @Override
    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            super.getObToNsiFieldsMap();
            OB_TO_NSI_FIELDS_MAP.put(Course.intValue().s(), COURSE_NUMBER_NSI_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    public long getNsiCourseNumberHash(CourseType nsiEntity)
    {
        return 0; //null == nsiEntity.getCourseNumber() ? 0 : (("int" + nsiEntity.getCourseNumber()).hashCode());
    }

    @Override
    public boolean isFieldEmpty(CourseType nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case COURSE_NUMBER_NSI_FIELD:
                return null == nsiEntity.getCourseNumber();
            default:
                return super.isFieldEmpty(nsiEntity, fieldName);
        }
    }

    @Override
    public long getNsiFieldHash(CourseType nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case COURSE_NUMBER_NSI_FIELD:
                return getNsiCourseNumberHash(nsiEntity);
            default:
                return super.getNsiFieldHash(nsiEntity, fieldName, caseInsensitive);
        }
    }

    public long getCourseNumberHash(Course entity)
    {
        return 0; //("int" + String.valueOf(entity.getIntValue())).hashCode();
    }

    @Override
    public long getEntityFieldHash(Course entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case COURSE_NUMBER_FIELD:
                return getCourseNumberHash(entity);
            default:
                return super.getEntityFieldHash(entity, nsiId, fieldName, caseInsensitive);
        }
    }
}