package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unifefu.entity.FefuSignerStuExtractSetting;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка подписантов для приказов по движению студентов (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuSignerStuExtractSettingGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuSignerStuExtractSetting";
    public static final String ENTITY_NAME = "fefuSignerStuExtractSetting";
    public static final int VERSION_HASH = -1902494045;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_EXTRACT_TYPE = "studentExtractType";

    private StudentExtractType _studentExtractType;     // Тип приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     */
    @NotNull
    public StudentExtractType getStudentExtractType()
    {
        return _studentExtractType;
    }

    /**
     * @param studentExtractType Тип приказа. Свойство не может быть null.
     */
    public void setStudentExtractType(StudentExtractType studentExtractType)
    {
        dirty(_studentExtractType, studentExtractType);
        _studentExtractType = studentExtractType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuSignerStuExtractSettingGen)
        {
            setStudentExtractType(((FefuSignerStuExtractSetting)another).getStudentExtractType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuSignerStuExtractSettingGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuSignerStuExtractSetting.class;
        }

        public T newInstance()
        {
            return (T) new FefuSignerStuExtractSetting();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentExtractType":
                    return obj.getStudentExtractType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentExtractType":
                    obj.setStudentExtractType((StudentExtractType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentExtractType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentExtractType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentExtractType":
                    return StudentExtractType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuSignerStuExtractSetting> _dslPath = new Path<FefuSignerStuExtractSetting>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuSignerStuExtractSetting");
    }
            

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSignerStuExtractSetting#getStudentExtractType()
     */
    public static StudentExtractType.Path<StudentExtractType> studentExtractType()
    {
        return _dslPath.studentExtractType();
    }

    public static class Path<E extends FefuSignerStuExtractSetting> extends EntityPath<E>
    {
        private StudentExtractType.Path<StudentExtractType> _studentExtractType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSignerStuExtractSetting#getStudentExtractType()
     */
        public StudentExtractType.Path<StudentExtractType> studentExtractType()
        {
            if(_studentExtractType == null )
                _studentExtractType = new StudentExtractType.Path<StudentExtractType>(L_STUDENT_EXTRACT_TYPE, this);
            return _studentExtractType;
        }

        public Class getEntityClass()
        {
            return FefuSignerStuExtractSetting.class;
        }

        public String getEntityName()
        {
            return "fefuSignerStuExtractSetting";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
