/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.component.listextract.fefu4.utils.FefuAcadGrantAssignExtractWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu4.utils.FefuAcadGrantAssignParagraphPartWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu4.utils.FefuAcadGrantAssignParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class FefuAcadGrantAssignStuEnrolmentListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<FefuAcadGrantAssignStuEnrolmentListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, extracts.get(0), prepareParagraphsStructure(extracts));

        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    protected List<FefuAcadGrantAssignParagraphWrapper> prepareParagraphsStructure(List<FefuAcadGrantAssignStuEnrolmentListExtract> extracts)
    {
        List<FefuAcadGrantAssignParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        int ind;
        for (FefuAcadGrantAssignStuEnrolmentListExtract extract : extracts)
        {
            Person person = extract.getEntity().getPerson();

            Group group = extract.getGroup();
            EducationOrgUnit educationOrgUnit = group.getEducationOrgUnit();

            FefuAcadGrantAssignParagraphWrapper paragraphWrapper = new FefuAcadGrantAssignParagraphWrapper(educationOrgUnit.getEducationLevelHighSchool().getEducationLevel(), educationOrgUnit.getDevelopForm(), educationOrgUnit.getDevelopCondition(), educationOrgUnit.getDevelopTech(), educationOrgUnit.getDevelopPeriod(), extract.getBeginDate(), extract.getEndDate(), educationOrgUnit.getFormativeOrgUnit(), educationOrgUnit.getTerritorialOrgUnit(), CommonListOrderPrint.getEducationBaseText(group), extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            FefuAcadGrantAssignParagraphPartWrapper paragraphPartWrapper = new FefuAcadGrantAssignParagraphPartWrapper(extract.getCourse(), extract.getGroup(), extract);

            ind = paragraphWrapper.getParagraphPartWrapperList().indexOf(paragraphPartWrapper);
            if (ind == -1)
                paragraphWrapper.getParagraphPartWrapperList().add(paragraphPartWrapper);
            else
                paragraphPartWrapper = paragraphWrapper.getParagraphPartWrapperList().get(ind);

            paragraphPartWrapper.getExtractWrapperList().add(
                    new FefuAcadGrantAssignExtractWrapper(person, FefuAcadGrantAssignStuEnrolmentListExtractPrint.getPrintedGrantSize(extract)));
        }

        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, FefuAcadGrantAssignStuEnrolmentListExtract firstExtract, List<FefuAcadGrantAssignParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            int parNumber = 0;
            int parCount = paragraphWrappers.size();
            for (FefuAcadGrantAssignParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_ACAD_GRANT_ASSIGN_ENROLLMENT_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());

                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, paragraphWrapper.getDevelopForm(), "");

                CommonExtractPrint.initCompensationType(paragraphInjectModifier, "", firstExtract.getCompensationType(), true, firstExtract.getEntity().isTargetAdmission());

                OrgUnit formativeOrgUnit = paragraphWrapper.getFormativeOrgUnit();
                OrgUnit territorialOrgUnit = paragraphWrapper.getTerritorialOrgUnit();
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, formativeOrgUnit, "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), formativeOrgUnit, territorialOrgUnit, "formativeOrgUnitStr", "");

                paragraphInjectModifier.put("dateBegin", DateFormatter.DEFAULT_DATE_FORMATTER.format(paragraphWrapper.getBeginDate()));
                paragraphInjectModifier.put("dateEnd", DateFormatter.DEFAULT_DATE_FORMATTER.format(paragraphWrapper.getEndDate()));

                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, paragraphWrapper.getEducationLevels());
                paragraphInjectModifier.put("parNumber", parCount > 1 ? String.valueOf(++parNumber) + ". " : "");
                paragraphInjectModifier.put("parNumberConditional", parCount > 1 ? String.valueOf(parNumber) + ". " : "");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), paragraphWrapper.getEduBaseText(), "fefuShortFastExtendedOptionalText");

                paragraphInjectModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList(), paragraphWrapper.getDevelopForm());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    protected void injectSubParagraphs(RtfDocument paragraph, List<FefuAcadGrantAssignParagraphPartWrapper> paragraphPartWrappers, DevelopForm developForm)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            Collections.sort(paragraphPartWrappers);
            List<IRtfElement> parList = new ArrayList<>();

            for (FefuAcadGrantAssignParagraphPartWrapper paragraphPartWrapper : paragraphPartWrappers)
            {
                // Получаем шаблон подпараграфа
                byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_ACAD_GRANT_ASSIGN_ENROLLMENT_LIST_EXTRACT), 3);
                RtfDocument paragraphPart = new RtfReader().read(paragraphPartTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphPartInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, paragraphPartWrapper.getFirstExtract());

                paragraphPartInjectModifier.put("course", paragraphPartWrapper.getCourse().getTitle());
                paragraphPartInjectModifier.put("group", paragraphPartWrapper.getGroup().getTitle());
                CommonListOrderPrint.injectFefuGroupPar(paragraphPartInjectModifier, paragraphPartWrapper.getGroup(), developForm);
                paragraphPartInjectModifier.modify(paragraphPart);

                RtfTableModifier paragraphPartTableModifier = new RtfTableModifier();
                List<FefuAcadGrantAssignExtractWrapper> extractWrapperList = paragraphPartWrapper.getExtractWrapperList();

                Collections.sort(extractWrapperList);

                int j = 0;
                String[][] tableData = new String[extractWrapperList.size()][];
                for (FefuAcadGrantAssignExtractWrapper extractWrapper : extractWrapperList)
                {
                    tableData[j++] = new String[]{String.valueOf(j) + ".", extractWrapper.getPerson().getFullFio(), extractWrapper.getGrantSize()};
                }

                paragraphPartTableModifier.put("STUDENTS_TABLE", tableData);
                paragraphPartTableModifier.modify(paragraphPart);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}