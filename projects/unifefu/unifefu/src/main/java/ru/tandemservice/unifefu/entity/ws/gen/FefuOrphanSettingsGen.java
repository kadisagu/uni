package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.unifefu.entity.ws.FefuOrphanSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка Directum для указания сиротских льгот
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuOrphanSettingsGen extends EntityBase
 implements INaturalIdentifiable<FefuOrphanSettingsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuOrphanSettings";
    public static final String ENTITY_NAME = "fefuOrphanSettings";
    public static final int VERSION_HASH = 1632839793;
    private static IEntityMeta ENTITY_META;

    public static final String L_BENEFIT = "benefit";
    public static final String P_ORPHAN_BENEFIT = "orphanBenefit";

    private Benefit _benefit;     // Льгота
    private boolean _orphanBenefit;     // Сиротская льгота

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Льгота. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Benefit getBenefit()
    {
        return _benefit;
    }

    /**
     * @param benefit Льгота. Свойство не может быть null и должно быть уникальным.
     */
    public void setBenefit(Benefit benefit)
    {
        dirty(_benefit, benefit);
        _benefit = benefit;
    }

    /**
     * @return Сиротская льгота. Свойство не может быть null.
     */
    @NotNull
    public boolean isOrphanBenefit()
    {
        return _orphanBenefit;
    }

    /**
     * @param orphanBenefit Сиротская льгота. Свойство не может быть null.
     */
    public void setOrphanBenefit(boolean orphanBenefit)
    {
        dirty(_orphanBenefit, orphanBenefit);
        _orphanBenefit = orphanBenefit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuOrphanSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setBenefit(((FefuOrphanSettings)another).getBenefit());
            }
            setOrphanBenefit(((FefuOrphanSettings)another).isOrphanBenefit());
        }
    }

    public INaturalId<FefuOrphanSettingsGen> getNaturalId()
    {
        return new NaturalId(getBenefit());
    }

    public static class NaturalId extends NaturalIdBase<FefuOrphanSettingsGen>
    {
        private static final String PROXY_NAME = "FefuOrphanSettingsNaturalProxy";

        private Long _benefit;

        public NaturalId()
        {}

        public NaturalId(Benefit benefit)
        {
            _benefit = ((IEntity) benefit).getId();
        }

        public Long getBenefit()
        {
            return _benefit;
        }

        public void setBenefit(Long benefit)
        {
            _benefit = benefit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuOrphanSettingsGen.NaturalId) ) return false;

            FefuOrphanSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getBenefit(), that.getBenefit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBenefit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBenefit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuOrphanSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuOrphanSettings.class;
        }

        public T newInstance()
        {
            return (T) new FefuOrphanSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "benefit":
                    return obj.getBenefit();
                case "orphanBenefit":
                    return obj.isOrphanBenefit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "benefit":
                    obj.setBenefit((Benefit) value);
                    return;
                case "orphanBenefit":
                    obj.setOrphanBenefit((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "benefit":
                        return true;
                case "orphanBenefit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "benefit":
                    return true;
                case "orphanBenefit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "benefit":
                    return Benefit.class;
                case "orphanBenefit":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuOrphanSettings> _dslPath = new Path<FefuOrphanSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuOrphanSettings");
    }
            

    /**
     * @return Льгота. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrphanSettings#getBenefit()
     */
    public static Benefit.Path<Benefit> benefit()
    {
        return _dslPath.benefit();
    }

    /**
     * @return Сиротская льгота. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrphanSettings#isOrphanBenefit()
     */
    public static PropertyPath<Boolean> orphanBenefit()
    {
        return _dslPath.orphanBenefit();
    }

    public static class Path<E extends FefuOrphanSettings> extends EntityPath<E>
    {
        private Benefit.Path<Benefit> _benefit;
        private PropertyPath<Boolean> _orphanBenefit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Льгота. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrphanSettings#getBenefit()
     */
        public Benefit.Path<Benefit> benefit()
        {
            if(_benefit == null )
                _benefit = new Benefit.Path<Benefit>(L_BENEFIT, this);
            return _benefit;
        }

    /**
     * @return Сиротская льгота. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuOrphanSettings#isOrphanBenefit()
     */
        public PropertyPath<Boolean> orphanBenefit()
        {
            if(_orphanBenefit == null )
                _orphanBenefit = new PropertyPath<Boolean>(FefuOrphanSettingsGen.P_ORPHAN_BENEFIT, this);
            return _orphanBenefit;
        }

        public Class getEntityClass()
        {
            return FefuOrphanSettings.class;
        }

        public String getEntityName()
        {
            return "fefuOrphanSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
