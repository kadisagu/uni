package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x2_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEppStateEduStandardParameters

		// создано свойство minChoiceDisciplinesPercent
		{
			// создать колонку
			tool.createColumn("ffeppsttedstndrdprmtrs_t", new DBColumn("minchoicedisciplinespercent_p", DBType.INTEGER));
		}

        tool.getStatement().executeUpdate("update ffeppsttedstndrdprmtrs_t set minchoicedisciplinespercent_p = maxchoicedisciplinespercent_p");

        // удалено свойство maxChoiceDisciplinesPercent
        {
            // удалить колонку
            tool.dropColumn("ffeppsttedstndrdprmtrs_t", "maxchoicedisciplinespercent_p");
        }
    }
}