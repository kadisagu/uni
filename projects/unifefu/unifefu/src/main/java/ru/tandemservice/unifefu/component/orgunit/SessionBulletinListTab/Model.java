/*$Id$*/
package ru.tandemservice.unifefu.component.orgunit.SessionBulletinListTab;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 17.09.2015
 */
public class Model extends ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab.Model
{
    private ISelectModel educationLevelsModel;

    public ISelectModel getEducationLevelsModel()
    {
        return educationLevelsModel;
    }

    public void setEducationLevelsModel(ISelectModel educationLevelsModel)
    {
        this.educationLevelsModel = educationLevelsModel;
    }

    protected List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList()
    {
        return this.getSettings().get("educationLevelsList");
    }

    protected boolean getConsiderChild()
    {
        Boolean result = this.getSettings().get("considerChild");
        return result == null ? false : result;
    }
}
