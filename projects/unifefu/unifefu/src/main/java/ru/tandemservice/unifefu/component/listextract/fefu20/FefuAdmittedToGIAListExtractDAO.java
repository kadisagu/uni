/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentCustomStateCICodes;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Andrey Andreev
 * @since 13.01.2016
 */
public class FefuAdmittedToGIAListExtractDAO extends UniBaseDao implements IExtractComponentDao<FefuAdmittedToGIAListExtract>
{
    /**
     * Внесение выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doCommit(FefuAdmittedToGIAListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getTransferOrderDate());
            extract.setPrevOrderNumber(orderData.getTransferOrderNumber());
        }
        orderData.setTransferOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setTransferOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);

        //создать статус
        StudentCustomStateCI customStateCI = DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), StudentCustomStateCICodes.DOPUTSHEN_K_G_I_A);
        StudentCustomState studentCustomState = new StudentCustomState();
        studentCustomState.setCustomState(customStateCI);
        studentCustomState.setStudent(extract.getEntity());
        studentCustomState.setBeginDate(extract.getBeginDate());
        UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(studentCustomState);
        StudentCustomStateToExtractRelation extractRelation = new StudentCustomStateToExtractRelation();
        extractRelation.setStudentCustomState(studentCustomState);
        extractRelation.setExtract(extract);
        save(extractRelation);
    }

    /**
     * Отмена вносимых выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doRollback(FefuAdmittedToGIAListExtract extract, Map parameters)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateToExtractRelation.class, "r").
                column(DQLExpressions.property("r", StudentCustomStateToExtractRelation.studentCustomState().id())).
                where(DQLExpressions.eq(DQLExpressions.property("r", StudentCustomStateToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(StudentCustomState.class).
                where(DQLExpressions.in(DQLExpressions.property(StudentCustomState.id()), builder.buildQuery()));
        deleteBuilder.createStatement(getSession()).execute();
    }
}
