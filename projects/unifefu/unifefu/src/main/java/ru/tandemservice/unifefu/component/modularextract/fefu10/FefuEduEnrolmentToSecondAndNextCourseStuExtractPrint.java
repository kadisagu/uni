/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu10;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.unifefu.entity.FefuEduEnrolmentToSecondAndNextCourseStuExtract;

import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 17.04.2013
 */
public class FefuEduEnrolmentToSecondAndNextCourseStuExtractPrint implements IPrintFormCreator<FefuEduEnrolmentToSecondAndNextCourseStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuEduEnrolmentToSecondAndNextCourseStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("liquidateEduPlanDifference", getLiquidationPart(extract.isLiquidateEduPlanDifference(), extract.getLiquidationDeadlineDate(), extract));
        modifier.modify(document);

        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    public static RtfString getLiquidationPart(boolean isNeedLiquidate, Date liquidationDeadlineDate, ModularStudentExtract extract)
    {
        RtfString debts = new RtfString();
        if (isNeedLiquidate)
        {
            debts.par();
            debts.append("Ликвидировать разницу в учебных планах до ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(liquidationDeadlineDate));

            List<StuExtractToDebtRelation> rels = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
            if (!rels.isEmpty())
            {
                debts.append(":");
                StuExtractToDebtRelation rel;
                for (int i = 0; i < rels.size() && (rel = rels.get(i)) != null; i++)
                {
                    debts.par();
                    debts.append(String.valueOf(i + 1)).
                            append(". «").
                            append(rel.getDiscipline()).
                            append("» - ").
                            append(String.valueOf(rel.getHours())).
                            append(" ч. - ").
                            append(rel.getControlAction());
                }
            }
            debts.append(".");
        }
        return debts;
    }
}