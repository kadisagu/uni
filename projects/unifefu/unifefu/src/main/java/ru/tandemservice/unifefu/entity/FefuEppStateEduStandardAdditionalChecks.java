package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.*;

/** @see ru.tandemservice.unifefu.entity.gen.FefuEppStateEduStandardAdditionalChecksGen */
public class FefuEppStateEduStandardAdditionalChecks extends FefuEppStateEduStandardAdditionalChecksGen
{
    public FefuEppStateEduStandardAdditionalChecks()
    {
    }

    public FefuEppStateEduStandardAdditionalChecks(boolean check)
    {
        setAltInChoiceDisciplines(check);
        setControlActionOnTotalRegElement(check);
        setFormingCompetenceRegElement(check);
        setIntegralityLaborPartYear(check);
        setEvenAuditHours(check);
        setMultipleAuditHoursPartYear(check);
        setBindEduPlanAndFixOwner(check);
        setEduPlanConformityScheduleInPractices(check);
        setSelfWorkForAnyDiscipline(check);
        setDuplicateDisciplinesTitles(check);
        setRegElementConformityEpv(check);
    }
}