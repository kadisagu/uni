/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EppWorkPlan.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.List.EppWorkPlanList;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.unifefu.base.ext.EppWorkPlan.logic.List.FefuWorkPlanListDSHandler;

/**
 * @author Denis Katkov
 * @since 10.03.2016
 */
@Configuration
public class EppWorkPlanListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + EppWorkPlanListExtUI.class.getSimpleName();

    @Autowired
    private EppWorkPlanList _eppWorkPlanList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eppWorkPlanList.presenterExtPoint())
                .replaceDataSource(searchListDS(EppWorkPlanList.EPP_WORK_PLAN_LIST_DS, _eppWorkPlanList.workPlanListDSColumns(), fefuWorkPlanListDSHandler()))
                .addAddon(uiAddon(ADDON_NAME, EppWorkPlanListExtUI.class))
                .create();
    }

    @Bean
    public ColumnListExtension workPlanListDSColumnsExtension()
    {
        return columnListExtensionBuilder(_eppWorkPlanList.workPlanListDSColumns())
                .addColumn(textColumn("okso", EppWorkPlan.parent().eduPlanVersion().eduPlan().parent().programSubject().subjectCode()).after("title"))
                .addColumn(textColumn("specializationFgos", EppWorkPlan.parent().eduPlanVersion().eduPlan().parent().programSubject().title()).after("okso"))
                .addColumn(textColumn("specialization", EppWorkPlan.parent().title()).after("specializationFgos"))
                .addColumn(textColumn("standardGeneration", EppWorkPlan.parent().eduPlanVersion().eduPlan().parent().programSubject().subjectIndex().generation().title()).after("specialization"))
                .addColumn(textColumn("existActiveStudent", FefuWorkPlanListDSHandler.P_EXIST_ACTIVE_STUDENT).formatter(YesNoFormatter.INSTANCE).after("developTech"))
                .addColumn(textColumn("changed", FefuWorkPlanListDSHandler.P_CHANGED).formatter(YesNoFormatter.INSTANCE).after("existActiveStudent"))
                .addColumn(textColumn("existUnallocatedDiscipline", FefuWorkPlanListDSHandler.P_EXIST_UNALLOCATED_DISCIPLINE).formatter(YesNoFormatter.INSTANCE).after("changed"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> fefuWorkPlanListDSHandler()
    {
        return new FefuWorkPlanListDSHandler(getName());
    }
}