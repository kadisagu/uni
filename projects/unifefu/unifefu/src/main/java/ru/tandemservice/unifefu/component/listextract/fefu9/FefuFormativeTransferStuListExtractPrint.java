/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu9;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class FefuFormativeTransferStuListExtractPrint implements IPrintFormCreator<FefuFormativeTransferStuListExtract>, IListParagraphPrintFormCreator<FefuFormativeTransferStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuFormativeTransferStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuFormativeTransferStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);

        CompensationType compensationTypeOld = firstExtract.getCompensationTypeOld();
        CompensationType compensationTypeNew = firstExtract.getCompensationTypeNew();

        EducationOrgUnit educationOrgUnitOld = firstExtract.getEducationOrgUnitOld();
        EducationOrgUnit educationOrgUnitNew = firstExtract.getEducationOrgUnitNew();

        EducationLevels educationLevelOld = educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel();
        EducationLevels educationLevelNew = educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel();

        CommonListOrderPrint.injectFefuDevelopConditionAndTech(injectModifier, educationOrgUnitOld, CommonListOrderPrint.getEducationBaseText(firstExtract.getGroupOld()), "fefuShortFastExtendedOptionalTextOld");
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(injectModifier, educationOrgUnitNew, CommonListOrderPrint.getEducationBaseText(firstExtract.getGroupNew()), "fefuShortFastExtendedOptionalTextNew");
        CommonExtractPrint.initFefuGroup(injectModifier, "intoFefuGroupOld", firstExtract.getGroupOld(), educationOrgUnitOld.getDevelopForm(), " группы ");
        CommonExtractPrint.initFefuGroup(injectModifier, "intoFefuGroupNew", firstExtract.getGroupNew(), educationOrgUnitNew.getDevelopForm(), " в группу ");

        injectModifier.put("fefuCompensationTypeStrOld", compensationTypeOld != null ? compensationTypeOld.isBudget() ? "за счет средств федерального бюджета" : "на договорной основе" : "");
        injectModifier.put("fefuCompensationTypeStrNew", compensationTypeNew != null ? compensationTypeNew.isBudget() ? "за счет средств федерального бюджета" : "на договорной основе" : "");

        injectModifier.put("formativeOrgUnitOld_P", educationOrgUnitOld.getFormativeOrgUnit().getPrepositionalCaseTitle() != null ? educationOrgUnitOld.getFormativeOrgUnit().getPrepositionalCaseTitle() : educationOrgUnitOld.getFormativeOrgUnit().getPrintTitle());
        injectModifier.put("formativeOrgUnitNew_P", educationOrgUnitNew.getFormativeOrgUnit().getPrepositionalCaseTitle() != null ? educationOrgUnitNew.getFormativeOrgUnit().getPrepositionalCaseTitle() : educationOrgUnitNew.getFormativeOrgUnit().getPrintTitle());

        injectModifier.put("territorialOrgUnitOld_P", educationOrgUnitOld.getTerritorialOrgUnit().getTerritorialTitle().equals(TopOrgUnit.getInstance().getTerritorialTitle()) ? "" :" (" + educationOrgUnitOld.getTerritorialOrgUnit().getPrintTitle() + ")");
        injectModifier.put("territorialOrgUnitNew_P", educationOrgUnitNew.getTerritorialOrgUnit().getTerritorialTitle().equals(TopOrgUnit.getInstance().getTerritorialTitle()) ? "" :" (" + educationOrgUnitNew.getTerritorialOrgUnit().getPrintTitle() + ")");

        CommonExtractPrint.modifyEducationStr(injectModifier, educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel(), new String[]{"educationStrOld"});
        StructureEducationLevels levelOld = educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel().getLevelType();

        String fefuEducationStrNew = levelOld.isSpecialty() ? "указанной специальности" : levelOld.isMaster() || levelOld.isBachelor() ? "указанному направлению" : "";

        // если нет профиля или специализации у старого НП
        if (!educationLevelOld.isProfileOrSpecialization())
        {
            // если есть профиль или специализации у нового НП
            if (educationLevelNew.isProfileOrSpecialization())
                fefuEducationStrNew += (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
        }
        else
        {
            EducationLevels parentLevelOld = educationLevelOld;

            // если новое НП является направлением или специализацией (когда новое НП не выбрано)
            if (!educationLevelNew.isProfileOrSpecialization())
                parentLevelOld = educationLevelOld.getParentLevel();

            if (!parentLevelOld.equals(educationLevelNew))
                fefuEducationStrNew += (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
            else
                fefuEducationStrNew += levelOld.isSpecialty() ? " и специализации" : levelOld.isMaster() ? " и магистерской программе" : levelOld.isBachelor() ? " и профилю" : "";
        }

        injectModifier.put("fefuEducationStrNew_D", fefuEducationStrNew);
        CommonExtractPrint.initEducationType(injectModifier, educationOrgUnitOld, "Old");

        return injectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuFormativeTransferStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuFormativeTransferStuListExtract firstExtract)
    {
    }
}