/* $Id: MS_unifefu_2x5x1_3to4.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Denis Perminov
 * @since 11.04.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x1_3to4  extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.getStatement().executeUpdate("update dfl set dfl.DIRECTUMID_P=soe.DIRECTUMID_P" +
                " from FEFUDIRECTUMLOG_T as dfl" +
                " left join FEFUSTUDENTORDEREXTENSION_T as soe" +
                " on soe.ORDER_ID=dfl.ORDER_ID" +
                " where dfl.DIRECTUMID_P is null and soe.DIRECTUMID_P is not null");
        tool.getStatement().executeUpdate("update dfl set dfl.DIRECTUMID_P=soe.DIRECTUMID_P" +
                " from FEFUDIRECTUMLOG_T as dfl" +
                " left join FEFUSTUDENTORDEREXTENSION_T as soe" +
                " on soe.ORDER_ID=dfl.ORDER_ID" +
                " where dfl.DIRECTUMID_P=soe.DIRECTUMTASK_P and soe.DIRECTUMID_P is not null");


        tool.getStatement().executeUpdate("update dfl set dfl.DIRECTUMID_P=eoe.DIRECTUMID_P" +
                " from FEFUDIRECTUMLOG_T as dfl" +
                " left join FEFUENTRANTORDEREXTENSION_T as eoe" +
                " on eoe.ORDER_ID=dfl.ENRORDER_ID" +
                " where dfl.DIRECTUMID_P is null and eoe.DIRECTUMID_P is not null");
        tool.getStatement().executeUpdate("update dfl set dfl.DIRECTUMID_P=eoe.DIRECTUMID_P" +
                " from FEFUDIRECTUMLOG_T as dfl" +
                " left join FEFUENTRANTORDEREXTENSION_T as eoe" +
                " on eoe.ORDER_ID=dfl.ENRORDER_ID" +
                " where dfl.DIRECTUMID_P=eoe.DIRECTUMTASK_P and eoe.DIRECTUMID_P is not null");
    }
}
