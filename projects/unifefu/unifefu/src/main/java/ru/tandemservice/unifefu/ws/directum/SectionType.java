/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 16.07.2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Section", propOrder = {})
public class SectionType
{
    @XmlElements({
            @XmlElement(name = "Requisite", type = RequisiteType.class)
    })

    protected List<RequisiteType> requisites = new ArrayList<>();

    @XmlAttribute(name = "Index")
    protected int index = 0;

    public SectionType()
    {
    }

    public SectionType(List<RequisiteType> requisites)
    {
        this.requisites = requisites;
    }

    public List<RequisiteType> getRequisites()
    {
        return requisites;
    }

    public void setRequisites(List<RequisiteType> requisites)
    {
        this.requisites = requisites;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }
}