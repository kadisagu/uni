package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifefu.entity.TrOrgUnitSettingsFefuExt;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройки БРС на подразделении на год (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrOrgUnitSettingsFefuExtGen extends EntityBase
 implements INaturalIdentifiable<TrOrgUnitSettingsFefuExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.TrOrgUnitSettingsFefuExt";
    public static final String ENTITY_NAME = "trOrgUnitSettingsFefuExt";
    public static final int VERSION_HASH = 266395216;
    private static IEntityMeta ENTITY_META;

    public static final String L_TR_ORG_UNIT_SETTINGS = "trOrgUnitSettings";
    public static final String P_BLOCK_TUTOR_EDIT = "blockTutorEdit";

    private TrOrgUnitSettings _trOrgUnitSettings;     // Настройки оценивания на подразделении на часть года
    private boolean _blockTutorEdit = false;     // Заблокировать преподавателям выставление оценок в журнал

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Настройки оценивания на подразделении на часть года. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public TrOrgUnitSettings getTrOrgUnitSettings()
    {
        return _trOrgUnitSettings;
    }

    /**
     * @param trOrgUnitSettings Настройки оценивания на подразделении на часть года. Свойство не может быть null и должно быть уникальным.
     */
    public void setTrOrgUnitSettings(TrOrgUnitSettings trOrgUnitSettings)
    {
        dirty(_trOrgUnitSettings, trOrgUnitSettings);
        _trOrgUnitSettings = trOrgUnitSettings;
    }

    /**
     * @return Заблокировать преподавателям выставление оценок в журнал. Свойство не может быть null.
     */
    @NotNull
    public boolean isBlockTutorEdit()
    {
        return _blockTutorEdit;
    }

    /**
     * @param blockTutorEdit Заблокировать преподавателям выставление оценок в журнал. Свойство не может быть null.
     */
    public void setBlockTutorEdit(boolean blockTutorEdit)
    {
        dirty(_blockTutorEdit, blockTutorEdit);
        _blockTutorEdit = blockTutorEdit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrOrgUnitSettingsFefuExtGen)
        {
            if (withNaturalIdProperties)
            {
                setTrOrgUnitSettings(((TrOrgUnitSettingsFefuExt)another).getTrOrgUnitSettings());
            }
            setBlockTutorEdit(((TrOrgUnitSettingsFefuExt)another).isBlockTutorEdit());
        }
    }

    public INaturalId<TrOrgUnitSettingsFefuExtGen> getNaturalId()
    {
        return new NaturalId(getTrOrgUnitSettings());
    }

    public static class NaturalId extends NaturalIdBase<TrOrgUnitSettingsFefuExtGen>
    {
        private static final String PROXY_NAME = "TrOrgUnitSettingsFefuExtNaturalProxy";

        private Long _trOrgUnitSettings;

        public NaturalId()
        {}

        public NaturalId(TrOrgUnitSettings trOrgUnitSettings)
        {
            _trOrgUnitSettings = ((IEntity) trOrgUnitSettings).getId();
        }

        public Long getTrOrgUnitSettings()
        {
            return _trOrgUnitSettings;
        }

        public void setTrOrgUnitSettings(Long trOrgUnitSettings)
        {
            _trOrgUnitSettings = trOrgUnitSettings;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TrOrgUnitSettingsFefuExtGen.NaturalId) ) return false;

            TrOrgUnitSettingsFefuExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getTrOrgUnitSettings(), that.getTrOrgUnitSettings()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getTrOrgUnitSettings());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getTrOrgUnitSettings());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrOrgUnitSettingsFefuExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrOrgUnitSettingsFefuExt.class;
        }

        public T newInstance()
        {
            return (T) new TrOrgUnitSettingsFefuExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "trOrgUnitSettings":
                    return obj.getTrOrgUnitSettings();
                case "blockTutorEdit":
                    return obj.isBlockTutorEdit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "trOrgUnitSettings":
                    obj.setTrOrgUnitSettings((TrOrgUnitSettings) value);
                    return;
                case "blockTutorEdit":
                    obj.setBlockTutorEdit((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "trOrgUnitSettings":
                        return true;
                case "blockTutorEdit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "trOrgUnitSettings":
                    return true;
                case "blockTutorEdit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "trOrgUnitSettings":
                    return TrOrgUnitSettings.class;
                case "blockTutorEdit":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrOrgUnitSettingsFefuExt> _dslPath = new Path<TrOrgUnitSettingsFefuExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrOrgUnitSettingsFefuExt");
    }
            

    /**
     * @return Настройки оценивания на подразделении на часть года. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.TrOrgUnitSettingsFefuExt#getTrOrgUnitSettings()
     */
    public static TrOrgUnitSettings.Path<TrOrgUnitSettings> trOrgUnitSettings()
    {
        return _dslPath.trOrgUnitSettings();
    }

    /**
     * @return Заблокировать преподавателям выставление оценок в журнал. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.TrOrgUnitSettingsFefuExt#isBlockTutorEdit()
     */
    public static PropertyPath<Boolean> blockTutorEdit()
    {
        return _dslPath.blockTutorEdit();
    }

    public static class Path<E extends TrOrgUnitSettingsFefuExt> extends EntityPath<E>
    {
        private TrOrgUnitSettings.Path<TrOrgUnitSettings> _trOrgUnitSettings;
        private PropertyPath<Boolean> _blockTutorEdit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Настройки оценивания на подразделении на часть года. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.TrOrgUnitSettingsFefuExt#getTrOrgUnitSettings()
     */
        public TrOrgUnitSettings.Path<TrOrgUnitSettings> trOrgUnitSettings()
        {
            if(_trOrgUnitSettings == null )
                _trOrgUnitSettings = new TrOrgUnitSettings.Path<TrOrgUnitSettings>(L_TR_ORG_UNIT_SETTINGS, this);
            return _trOrgUnitSettings;
        }

    /**
     * @return Заблокировать преподавателям выставление оценок в журнал. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.TrOrgUnitSettingsFefuExt#isBlockTutorEdit()
     */
        public PropertyPath<Boolean> blockTutorEdit()
        {
            if(_blockTutorEdit == null )
                _blockTutorEdit = new PropertyPath<Boolean>(TrOrgUnitSettingsFefuExtGen.P_BLOCK_TUTOR_EDIT, this);
            return _blockTutorEdit;
        }

        public Class getEntityClass()
        {
            return TrOrgUnitSettingsFefuExt.class;
        }

        public String getEntityName()
        {
            return "trOrgUnitSettingsFefuExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
