package ru.tandemservice.unifefu.base.ext.TrJournal.ui.EventThemeEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unifefu.base.bo.FefuTrJournal.FefuTrJournalManager;
import ru.tandemservice.unifefu.base.ext.TrJournal.ui.AddEvent.TrJournalAddEventClickApplyAction;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.EventThemeEdit.TrJournalEventThemeEditUI;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.IBrsCoefficientRowWrapper;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBaseUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * User: amakarova
 * Date: 18.12.13
 */

public class TrJournalEventThemeEditClickApplyAction extends NamedUIAction
{

    protected TrJournalEventThemeEditClickApplyAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof TrJournalEventThemeEditUI)
        {
            Map<String, Object> values = new HashMap<>();
            TrJournalEventThemeEditUI ui = (TrJournalEventThemeEditUI) presenter;
            TrBrsCoefficientEditBaseUI brsCoefEditComponent = (TrBrsCoefficientEditBaseUI) ui.getSupport().getChildrenUI().entrySet().iterator().next().getValue();
            for (IBrsCoefficientRowWrapper row : brsCoefEditComponent.getRowList())
            {
                if (row.getDef().getUserCode().equals(FefuBrs.MAX_POINTS_CODE) || row.getDef().getUserCode().equals(FefuBrs.MIN_POINTS_CODE))
                    values.put(row.getDef().getUserCode(), row.getValue());
            }
            if (values.size() > 0)
            {
                Double max = 0.0d;
                Double min = 0.0d;
                if (values.get(FefuBrs.MAX_POINTS_CODE) instanceof Double)
                    max = (Double) values.get(FefuBrs.MAX_POINTS_CODE);
                else if (values.get(FefuBrs.MAX_POINTS_CODE) instanceof Long)
                    max = ((Long) values.get(FefuBrs.MAX_POINTS_CODE)).doubleValue();
                if (values.get(FefuBrs.MIN_POINTS_CODE) instanceof Double)
                    min = (Double) values.get(FefuBrs.MIN_POINTS_CODE);
                else if (values.get(FefuBrs.MIN_POINTS_CODE) instanceof Long)
                    min = ((Long) values.get(FefuBrs.MIN_POINTS_CODE)).doubleValue();
                if (max < min)
                    throw new ApplicationException(TrJournalAddEventClickApplyAction.MAX_MIN_MESSAGE_ERROR);
            }
            doSaveScheduleDates(ui);
            ui.onClickApply();
        }
    }

    /**
     * Сохранение дат
     */
    private void doSaveScheduleDates(IUIPresenter presenter)
    {
        IUIAddon uiAddon = presenter.getConfig().getAddon(TrJournalEventThemeEditExt.ADDON_NAME);
        if (null != uiAddon)
        {
            if (uiAddon instanceof TrJournalEventThemeEditExtUI)
            {
                TrJournalEventThemeEditExtUI addon = (TrJournalEventThemeEditExtUI) uiAddon;
                addon.getSchEvent().setDurationBegin(addon.getDate());
                if (addon.getSchEvent().getDurationEnd() == null)
                {
                    addon.getSchEvent().setDurationEnd(addon.getDate());
                }

                if (addon.getGroupEvent() == null)
                {
                    FefuTrJournalManager.instance().fefuEventDao().createEduGroupEventsForJournalEvent(addon.getJournalEvent().getId());
                    addon.setGroupEvent(addon.getTrEduGroupEvent(addon.getJournalEvent()));
                }

                if (addon.getGroupEvent() != null)
                {
                    TrJournalManager.instance().eventDao().saveOrUpdateScheduleEvent(addon.getGroupEvent(), addon.getSchEvent(), new ArrayList<UniplacesPlace>());
                }
            }
        }
    }
}

