/* $Id: EmployeePostRateUpdateExtension.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.ext.Employee.ui.PostAddEdit;

import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEditUI;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.IOnUpdateEmployeePostExt;
import ru.tandemservice.unifefu.base.ext.Employee.ui.logic.IFefuEmployeePostDao;


/**
 * @author Dmitry Seleznev
 * @since 09.07.2014
 */
public class EmployeePostRateUpdateExtension implements IOnUpdateEmployeePostExt
{
    @Override
    public void onUpdateExt(IUIAddon addon)
    {
        EmployeePostAddEditUI prsnt = ((EmployeePostAddEditExtUI) addon).getPresenter();

        if (null != addon && addon instanceof EmployeePostAddEditExtUI)
        {
            EmployeePostAddEditExtUI fefuAddon = (EmployeePostAddEditExtUI) addon;
            Integer rate = null != fefuAddon.getRate() ? Double.valueOf(fefuAddon.getRate() * 10000).intValue() : null;

            if (prsnt.isAddForm())
                ((IFefuEmployeePostDao) EmployeeManager.instance().dao()).saveEmployeePostExtension(prsnt.getEmployeeId(), prsnt.getEmployeePost(), rate);
            else
                ((IFefuEmployeePostDao) EmployeeManager.instance().dao()).updateEmployeePostExtension(prsnt.getEmployeePostId(), prsnt.getEmployeePost(), rate);
        }
    }
}
