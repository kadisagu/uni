/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d104.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;

import java.util.Date;

/**
 * @author Alexey Lopatin
 * @since 29.09.2013
 */
public class Model extends DocumentAddBaseModel
{
    private String _oldBookNumber;
    private Date _oldBookDate;

    public String getOldBookNumber()
    {
        return _oldBookNumber;
    }

    public void setOldBookNumber(String oldBookNumber)
    {
        _oldBookNumber = oldBookNumber;
    }

    public Date getOldBookDate()
    {
        return _oldBookDate;
    }

    public void setOldBookDate(Date oldBookDate)
    {
        _oldBookDate = oldBookDate;
    }
}
