package ru.tandemservice.unifefu.ws.blackboard.membership.gen;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.2.12
 * Thu Jan 30 16:55:56 YEKT 2014
 * Generated source version: 2.2.12
 * 
 */
 
@WebService(targetNamespace = "http://coursemembership.ws.blackboard/", name = "CourseMembership.WSPortType")
@XmlSeeAlso({ObjectFactory.class})
public interface CourseMembershipWSPortType {

    @WebResult(name = "return", targetNamespace = "http://coursemembership.ws.blackboard")
    @Action(input = "getCourseRoles", output = "//coursemembership.ws.blackboard/CourseMembership/getCourseRolesResponse")
    @RequestWrapper(localName = "getCourseRoles", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.GetCourseRoles")
    @WebMethod(action = "getCourseRoles")
    @ResponseWrapper(localName = "getCourseRolesResponse", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.GetCourseRolesResponse")
    public java.util.List<ru.tandemservice.unifefu.ws.blackboard.membership.gen.CourseMembershipRoleVO> getCourseRoles(
        @WebParam(name = "f", targetNamespace = "http://coursemembership.ws.blackboard")
        ru.tandemservice.unifefu.ws.blackboard.membership.gen.CourseRoleFilter f
    );

    @WebResult(name = "return", targetNamespace = "http://coursemembership.ws.blackboard")
    @Action(input = "saveCourseMembership", output = "//coursemembership.ws.blackboard/CourseMembership/saveCourseMembershipResponse")
    @RequestWrapper(localName = "saveCourseMembership", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.SaveCourseMembership")
    @WebMethod(action = "saveCourseMembership")
    @ResponseWrapper(localName = "saveCourseMembershipResponse", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.SaveCourseMembershipResponse")
    public java.util.List<java.lang.String> saveCourseMembership(
        @WebParam(name = "courseId", targetNamespace = "http://coursemembership.ws.blackboard")
        java.lang.String courseId,
        @WebParam(name = "cmArray", targetNamespace = "http://coursemembership.ws.blackboard")
        java.util.List<ru.tandemservice.unifefu.ws.blackboard.membership.gen.CourseMembershipVO> cmArray
    );

    @WebResult(name = "return", targetNamespace = "http://coursemembership.ws.blackboard")
    @Action(input = "saveGroupMembership", output = "//coursemembership.ws.blackboard/CourseMembership/saveGroupMembershipResponse")
    @RequestWrapper(localName = "saveGroupMembership", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.SaveGroupMembership")
    @WebMethod(action = "saveGroupMembership")
    @ResponseWrapper(localName = "saveGroupMembershipResponse", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.SaveGroupMembershipResponse")
    public java.util.List<java.lang.String> saveGroupMembership(
        @WebParam(name = "courseId", targetNamespace = "http://coursemembership.ws.blackboard")
        java.lang.String courseId,
        @WebParam(name = "g", targetNamespace = "http://coursemembership.ws.blackboard")
        java.util.List<ru.tandemservice.unifefu.ws.blackboard.membership.gen.GroupMembershipVO> g
    );

    @WebResult(name = "return", targetNamespace = "http://coursemembership.ws.blackboard")
    @Action(input = "initializeCourseMembershipWS", output = "//coursemembership.ws.blackboard/CourseMembership/initializeCourseMembershipWSResponse")
    @RequestWrapper(localName = "initializeCourseMembershipWS", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.InitializeCourseMembershipWS")
    @WebMethod(action = "initializeCourseMembershipWS")
    @ResponseWrapper(localName = "initializeCourseMembershipWSResponse", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.InitializeCourseMembershipWSResponse")
    public java.lang.Boolean initializeCourseMembershipWS(
        @WebParam(name = "ignore", targetNamespace = "http://coursemembership.ws.blackboard")
        java.lang.Boolean ignore
    );

    @WebResult(name = "return", targetNamespace = "http://coursemembership.ws.blackboard")
    @Action(input = "deleteCourseMembership", output = "//coursemembership.ws.blackboard/CourseMembership/deleteCourseMembershipResponse")
    @RequestWrapper(localName = "deleteCourseMembership", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.DeleteCourseMembership")
    @WebMethod(action = "deleteCourseMembership")
    @ResponseWrapper(localName = "deleteCourseMembershipResponse", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.DeleteCourseMembershipResponse")
    public java.util.List<java.lang.String> deleteCourseMembership(
        @WebParam(name = "courseId", targetNamespace = "http://coursemembership.ws.blackboard")
        java.lang.String courseId,
        @WebParam(name = "ids", targetNamespace = "http://coursemembership.ws.blackboard")
        java.util.List<java.lang.String> ids
    );

    @WebResult(name = "return", targetNamespace = "http://coursemembership.ws.blackboard")
    @Action(input = "deleteGroupMembership", output = "//coursemembership.ws.blackboard/CourseMembership/deleteGroupMembershipResponse")
    @RequestWrapper(localName = "deleteGroupMembership", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.DeleteGroupMembership")
    @WebMethod(action = "deleteGroupMembership")
    @ResponseWrapper(localName = "deleteGroupMembershipResponse", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.DeleteGroupMembershipResponse")
    public java.util.List<java.lang.String> deleteGroupMembership(
        @WebParam(name = "courseId", targetNamespace = "http://coursemembership.ws.blackboard")
        java.lang.String courseId,
        @WebParam(name = "ids", targetNamespace = "http://coursemembership.ws.blackboard")
        java.util.List<java.lang.String> ids
    );

    @WebResult(name = "return", targetNamespace = "http://coursemembership.ws.blackboard")
    @Action(input = "getServerVersion", output = "//coursemembership.ws.blackboard/CourseMembership/getServerVersionResponse")
    @RequestWrapper(localName = "getServerVersion", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.GetServerVersion")
    @WebMethod(action = "getServerVersion")
    @ResponseWrapper(localName = "getServerVersionResponse", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.GetServerVersionResponse")
    public ru.tandemservice.unifefu.ws.blackboard.membership.gen.VersionVO getServerVersion(
        @WebParam(name = "unused", targetNamespace = "http://coursemembership.ws.blackboard")
        ru.tandemservice.unifefu.ws.blackboard.membership.gen.VersionVO unused
    );

    @WebResult(name = "return", targetNamespace = "http://coursemembership.ws.blackboard")
    @Action(input = "getCourseMembership", output = "//coursemembership.ws.blackboard/CourseMembership/getCourseMembershipResponse")
    @RequestWrapper(localName = "getCourseMembership", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.GetCourseMembership")
    @WebMethod(action = "getCourseMembership")
    @ResponseWrapper(localName = "getCourseMembershipResponse", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.GetCourseMembershipResponse")
    public java.util.List<ru.tandemservice.unifefu.ws.blackboard.membership.gen.CourseMembershipVO> getCourseMembership(
        @WebParam(name = "courseId", targetNamespace = "http://coursemembership.ws.blackboard")
        java.lang.String courseId,
        @WebParam(name = "f", targetNamespace = "http://coursemembership.ws.blackboard")
        ru.tandemservice.unifefu.ws.blackboard.membership.gen.MembershipFilter f
    );

    @WebResult(name = "return", targetNamespace = "http://coursemembership.ws.blackboard")
    @Action(input = "getGroupMembership", output = "//coursemembership.ws.blackboard/CourseMembership/getGroupMembershipResponse")
    @RequestWrapper(localName = "getGroupMembership", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.GetGroupMembership")
    @WebMethod(action = "getGroupMembership")
    @ResponseWrapper(localName = "getGroupMembershipResponse", targetNamespace = "http://coursemembership.ws.blackboard", className = "ru.tandemservice.unifefu.ws.blackboard.membership.gen.GetGroupMembershipResponse")
    public java.util.List<ru.tandemservice.unifefu.ws.blackboard.membership.gen.GroupMembershipVO> getGroupMembership(
        @WebParam(name = "courseId", targetNamespace = "http://coursemembership.ws.blackboard")
        java.lang.String courseId,
        @WebParam(name = "f", targetNamespace = "http://coursemembership.ws.blackboard")
        ru.tandemservice.unifefu.ws.blackboard.membership.gen.MembershipFilter f
    );
}
