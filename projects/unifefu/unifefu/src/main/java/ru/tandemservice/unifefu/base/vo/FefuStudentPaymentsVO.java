/* $Id$ */
package ru.tandemservice.unifefu.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;

/**
 * @author nvankov
 * @since 8/27/13
 */
public class FefuStudentPaymentsVO extends DataWrapper
{
    private FefuStudentPayment _payment;
    private String _pauseDate;
    private String _continueDate;

    public FefuStudentPaymentsVO(FefuStudentPayment payment)
    {
        super(payment.getId(), payment.getExtract().getType().getTitle());
        _payment = payment;
    }

    public FefuStudentPayment getPayment()
    {
        return _payment;
    }

    public void setPayment(FefuStudentPayment payment)
    {
        _payment = payment;
    }

    public String getPauseDate()
    {
        return _pauseDate;
    }

    public void setPauseDate(String pauseDate)
    {
        _pauseDate = pauseDate;
    }

    public String getContinueDate()
    {
        return _continueDate;
    }

    public void setContinueDate(String continueDate)
    {
        _continueDate = continueDate;
    }
}
