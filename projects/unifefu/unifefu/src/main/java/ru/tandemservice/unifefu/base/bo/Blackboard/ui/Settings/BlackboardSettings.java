/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.Settings;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Nikolay Fedorovskih
 * @since 05.02.2014
 */
@Configuration
public class BlackboardSettings extends BusinessComponentManager
{
}