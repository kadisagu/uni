/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.ThematicGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifefu.base.bo.FefuSettings.logic.FefuThematicGroupsDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuSettings.ui.ThematicGroupList.FefuSettingsThematicGroupList;
import ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup;
import ru.tandemservice.unifefu.entity.catalog.FefuThematicGroupStuExtractTypes;

/**
 * @author nvankov
 * @since 11/7/13
 */
@Configuration
public class FefuSettingsThematicGroup extends BusinessComponentManager
{
    public final static String THEMATIC_GROUPS_DS = "thematicGroupsDS";

    @Bean
    public ColumnListExtPoint thematicGroupsCL()
    {
        return columnListExtPointBuilder(THEMATIC_GROUPS_DS)
                .addColumn(publisherColumn("title", FefuThematicGroupStuExtractTypes.title()).businessComponent(FefuSettingsThematicGroupList.class).required(true))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(THEMATIC_GROUPS_DS, thematicGroupsCL(), thematicGroupsDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler thematicGroupsDSHandler()
    {
        return new FefuThematicGroupsDSHandler(getName());
    }
}
