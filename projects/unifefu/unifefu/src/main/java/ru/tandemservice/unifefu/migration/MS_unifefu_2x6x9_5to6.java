package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x9_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.9"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.9")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuDirectumSendingOrder

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fefudirectumsendingorder_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("order_id", DBType.LONG).setNullable(false),
				new DBColumn("firstextract_id", DBType.LONG).setNullable(false),
				new DBColumn("ordertype_id", DBType.LONG).setNullable(false),
				new DBColumn("donesend_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("senddatetime_p", DBType.TIMESTAMP),
				new DBColumn("signer_p", DBType.createVarchar(255)),
				new DBColumn("routecode_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("orgunitcode_p", DBType.createVarchar(255)),
				new DBColumn("orgunittitle_p", DBType.createVarchar(255)),
				new DBColumn("executor_id", DBType.LONG).setNullable(false),
				new DBColumn("dismiss_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("restore_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("offbudgetpayments_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("foreignstudent_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("grantmataid_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("social_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("practice_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("groupmanager_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("uvcstudents_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("fvostudents_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("penalty_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("magister_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("payments_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("stateformular_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("international_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("checkmedicaldocs_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuDirectumSendingOrder");
        }
    }
}