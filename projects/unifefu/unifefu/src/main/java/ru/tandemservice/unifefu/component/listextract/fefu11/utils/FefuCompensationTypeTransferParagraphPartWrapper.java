/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu11.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 22.11.2013
 */
public class FefuCompensationTypeTransferParagraphPartWrapper implements Comparable<FefuCompensationTypeTransferParagraphPartWrapper>
{
    private OrgUnit _territorialOrgUnit;

    private List<FefuCompensationTypeTransferExtractWrapper> _extractWrapperList = new ArrayList<>();

    public FefuCompensationTypeTransferParagraphPartWrapper(OrgUnit orgUnit)
    {
        _territorialOrgUnit = orgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public List<FefuCompensationTypeTransferExtractWrapper> getExtractWrapperList()
    {
        return _extractWrapperList;
    }

    public void setExtractWrapperList(List<FefuCompensationTypeTransferExtractWrapper> extractWrapperList)
    {
        _extractWrapperList = extractWrapperList;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuCompensationTypeTransferParagraphPartWrapper))
            return false;

        FefuCompensationTypeTransferParagraphPartWrapper that = (FefuCompensationTypeTransferParagraphPartWrapper) o;

        return _territorialOrgUnit.equals(that.getTerritorialOrgUnit());
    }

    @Override
    public int compareTo(FefuCompensationTypeTransferParagraphPartWrapper o)
    {
        return _territorialOrgUnit.getPrintTitle().compareTo(o.getTerritorialOrgUnit().getPrintTitle());
    }
}