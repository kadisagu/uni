/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu18;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;

/**
 * @author Ekaterina Zvereva
 * @since 29.01.2015
 */
public class FefuOrderContingentStuDPOListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        byte[] rel = UniDaoFacade.getCoreDao().getProperty(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.P_CONTENT, FefuOrderToPrintFormRelation.L_ORDER, order);
        if (rel != null)
            template = rel;
        return new RtfReader().read(template);
    }
}