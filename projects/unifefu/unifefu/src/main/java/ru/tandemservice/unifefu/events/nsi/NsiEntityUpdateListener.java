/* $Id$ */
package ru.tandemservice.unifefu.events.nsi;

import org.hibernate.Session;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.unifefu.dao.daemon.IFefuNsiDaemonDao;

import java.util.Collection;

/**
 * @author Dmitry Seleznev
 * @since 12.09.2013
 */
public class NsiEntityUpdateListener extends ParamTransactionCompleteListener<Boolean>
{
    public void init()
    {
        for (Class clazz : INsiEntityToBeTracked.ENTITY_TYPES_TO_BE_TRACKED)
            DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, clazz, this);
    }

    @Override
    public void afterCompletion(Session session, int status, Collection<Long> params, Boolean beforeCompletionResult)
    {
        IFefuNsiDaemonDao.instance.get().doRegisterEntityEdit(params.toArray(new Long[]{}));
    }
}