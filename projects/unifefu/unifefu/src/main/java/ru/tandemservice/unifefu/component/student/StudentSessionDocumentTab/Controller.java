/*$Id$*/
package ru.tandemservice.unifefu.component.student.StudentSessionDocumentTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.FormatterColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisession.entity.document.SessionDocument;

/**
 * @author DMITRY KNYAZEV
 * @since 04.09.2014
 */
public class Controller extends ru.tandemservice.unisession.component.student.StudentSessionDocumentTab.Controller
{
	@SuppressWarnings("unchecked")
	@Override
	public void onRefreshComponent(IBusinessComponent component)
	{
		super.onRefreshComponent(component);
		DynamicListDataSource<SessionDocument> dataSource = getModel(component).getDataSource();
		((FormatterColumn) dataSource.getColumn(SessionDocument.typeTitle().s())).setFormatter(new FefuSpecialTitleFormatter());
	}
}
