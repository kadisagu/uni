/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e64;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ExcludeGPDefenceFailStuExtract;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.StudentCustomStateToExtractRollbackData;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 21.11.2014
 */
public class ExcludeGPDefenceFailStuExtractDao extends ru.tandemservice.movestudent.component.modularextract.e64.ExcludeGPDefenceFailStuExtractDao
{

    @Override
    public void doCommit(ExcludeGPDefenceFailStuExtract extract, Map parameters)
    {
        super.doCommit(extract, parameters);

        UnifefuDaoFacade.getFefuMoveStudentDAO().setStatusEndDateByCode(extract, UniFefuDefines.ADMITTED_TO_DIPLOMA, extract.getExcludeDate());
    }

    @Override
    public void doRollback(ExcludeGPDefenceFailStuExtract extract, Map parameters)
    {
        super.doRollback(extract, parameters);

        UnifefuDaoFacade.getFefuMoveStudentDAO().revertCustomStateDates(extract);
    }
}