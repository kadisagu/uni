/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu20.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOExtract;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;

/**
 * @author Ekaterina Zvereva
 * @since 20.01.2015
 */
public class DAO extends ModularStudentExtractPubDAO<FefuEnrollStuDPOExtract, Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
        if (model.getExtract().isIndividual())
            model.setPrintFormFileName((String)getProperty(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.P_FILE_NAME, FefuOrderToPrintFormRelation.L_ORDER, model.getExtract().getParagraph().getOrder()));
        model.setDpoProgramOld(model.getExtract().getDpoProgramOld());
    }


}