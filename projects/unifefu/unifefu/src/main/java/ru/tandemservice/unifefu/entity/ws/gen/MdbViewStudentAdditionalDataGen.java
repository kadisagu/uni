package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.entity.ws.MdbViewStudent;
import ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительные данные по студенту
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewStudentAdditionalDataGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData";
    public static final String ENTITY_NAME = "mdbViewStudentAdditionalData";
    public static final int VERSION_HASH = -922102554;
    private static IEntityMeta ENTITY_META;

    public static final String L_MDB_VIEW_STUDENT = "mdbViewStudent";
    public static final String L_STUDENT_FEFU_EXT = "studentFefuExt";
    public static final String P_INTEGRATION_ID = "integrationId";
    public static final String P_EMAIL = "email";
    public static final String P_PHONES = "phones";

    private MdbViewStudent _mdbViewStudent;     // Студент
    private StudentFefuExt _studentFefuExt;     // Студент (расширение ДВФУ)
    private String _integrationId;     // Идентификатор для интеграции
    private String _email;     // E-mail
    private String _phones;     // Контактные телефоны

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public MdbViewStudent getMdbViewStudent()
    {
        return _mdbViewStudent;
    }

    /**
     * @param mdbViewStudent Студент. Свойство не может быть null.
     */
    public void setMdbViewStudent(MdbViewStudent mdbViewStudent)
    {
        dirty(_mdbViewStudent, mdbViewStudent);
        _mdbViewStudent = mdbViewStudent;
    }

    /**
     * @return Студент (расширение ДВФУ).
     */
    public StudentFefuExt getStudentFefuExt()
    {
        return _studentFefuExt;
    }

    /**
     * @param studentFefuExt Студент (расширение ДВФУ).
     */
    public void setStudentFefuExt(StudentFefuExt studentFefuExt)
    {
        dirty(_studentFefuExt, studentFefuExt);
        _studentFefuExt = studentFefuExt;
    }

    /**
     * @return Идентификатор для интеграции.
     */
    @Length(max=255)
    public String getIntegrationId()
    {
        return _integrationId;
    }

    /**
     * @param integrationId Идентификатор для интеграции.
     */
    public void setIntegrationId(String integrationId)
    {
        dirty(_integrationId, integrationId);
        _integrationId = integrationId;
    }

    /**
     * @return E-mail.
     */
    @Length(max=255)
    public String getEmail()
    {
        return _email;
    }

    /**
     * @param email E-mail.
     */
    public void setEmail(String email)
    {
        dirty(_email, email);
        _email = email;
    }

    /**
     * @return Контактные телефоны.
     */
    @Length(max=255)
    public String getPhones()
    {
        return _phones;
    }

    /**
     * @param phones Контактные телефоны.
     */
    public void setPhones(String phones)
    {
        dirty(_phones, phones);
        _phones = phones;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewStudentAdditionalDataGen)
        {
            setMdbViewStudent(((MdbViewStudentAdditionalData)another).getMdbViewStudent());
            setStudentFefuExt(((MdbViewStudentAdditionalData)another).getStudentFefuExt());
            setIntegrationId(((MdbViewStudentAdditionalData)another).getIntegrationId());
            setEmail(((MdbViewStudentAdditionalData)another).getEmail());
            setPhones(((MdbViewStudentAdditionalData)another).getPhones());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewStudentAdditionalDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewStudentAdditionalData.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewStudentAdditionalData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mdbViewStudent":
                    return obj.getMdbViewStudent();
                case "studentFefuExt":
                    return obj.getStudentFefuExt();
                case "integrationId":
                    return obj.getIntegrationId();
                case "email":
                    return obj.getEmail();
                case "phones":
                    return obj.getPhones();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mdbViewStudent":
                    obj.setMdbViewStudent((MdbViewStudent) value);
                    return;
                case "studentFefuExt":
                    obj.setStudentFefuExt((StudentFefuExt) value);
                    return;
                case "integrationId":
                    obj.setIntegrationId((String) value);
                    return;
                case "email":
                    obj.setEmail((String) value);
                    return;
                case "phones":
                    obj.setPhones((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mdbViewStudent":
                        return true;
                case "studentFefuExt":
                        return true;
                case "integrationId":
                        return true;
                case "email":
                        return true;
                case "phones":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mdbViewStudent":
                    return true;
                case "studentFefuExt":
                    return true;
                case "integrationId":
                    return true;
                case "email":
                    return true;
                case "phones":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mdbViewStudent":
                    return MdbViewStudent.class;
                case "studentFefuExt":
                    return StudentFefuExt.class;
                case "integrationId":
                    return String.class;
                case "email":
                    return String.class;
                case "phones":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewStudentAdditionalData> _dslPath = new Path<MdbViewStudentAdditionalData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewStudentAdditionalData");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData#getMdbViewStudent()
     */
    public static MdbViewStudent.Path<MdbViewStudent> mdbViewStudent()
    {
        return _dslPath.mdbViewStudent();
    }

    /**
     * @return Студент (расширение ДВФУ).
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData#getStudentFefuExt()
     */
    public static StudentFefuExt.Path<StudentFefuExt> studentFefuExt()
    {
        return _dslPath.studentFefuExt();
    }

    /**
     * @return Идентификатор для интеграции.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData#getIntegrationId()
     */
    public static PropertyPath<String> integrationId()
    {
        return _dslPath.integrationId();
    }

    /**
     * @return E-mail.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData#getEmail()
     */
    public static PropertyPath<String> email()
    {
        return _dslPath.email();
    }

    /**
     * @return Контактные телефоны.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData#getPhones()
     */
    public static PropertyPath<String> phones()
    {
        return _dslPath.phones();
    }

    public static class Path<E extends MdbViewStudentAdditionalData> extends EntityPath<E>
    {
        private MdbViewStudent.Path<MdbViewStudent> _mdbViewStudent;
        private StudentFefuExt.Path<StudentFefuExt> _studentFefuExt;
        private PropertyPath<String> _integrationId;
        private PropertyPath<String> _email;
        private PropertyPath<String> _phones;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData#getMdbViewStudent()
     */
        public MdbViewStudent.Path<MdbViewStudent> mdbViewStudent()
        {
            if(_mdbViewStudent == null )
                _mdbViewStudent = new MdbViewStudent.Path<MdbViewStudent>(L_MDB_VIEW_STUDENT, this);
            return _mdbViewStudent;
        }

    /**
     * @return Студент (расширение ДВФУ).
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData#getStudentFefuExt()
     */
        public StudentFefuExt.Path<StudentFefuExt> studentFefuExt()
        {
            if(_studentFefuExt == null )
                _studentFefuExt = new StudentFefuExt.Path<StudentFefuExt>(L_STUDENT_FEFU_EXT, this);
            return _studentFefuExt;
        }

    /**
     * @return Идентификатор для интеграции.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData#getIntegrationId()
     */
        public PropertyPath<String> integrationId()
        {
            if(_integrationId == null )
                _integrationId = new PropertyPath<String>(MdbViewStudentAdditionalDataGen.P_INTEGRATION_ID, this);
            return _integrationId;
        }

    /**
     * @return E-mail.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData#getEmail()
     */
        public PropertyPath<String> email()
        {
            if(_email == null )
                _email = new PropertyPath<String>(MdbViewStudentAdditionalDataGen.P_EMAIL, this);
            return _email;
        }

    /**
     * @return Контактные телефоны.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentAdditionalData#getPhones()
     */
        public PropertyPath<String> phones()
        {
            if(_phones == null )
                _phones = new PropertyPath<String>(MdbViewStudentAdditionalDataGen.P_PHONES, this);
            return _phones;
        }

        public Class getEntityClass()
        {
            return MdbViewStudentAdditionalData.class;
        }

        public String getEntityName()
        {
            return "mdbViewStudentAdditionalData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
