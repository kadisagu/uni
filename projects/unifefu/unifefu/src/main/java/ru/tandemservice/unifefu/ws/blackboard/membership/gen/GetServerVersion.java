
package ru.tandemservice.unifefu.ws.blackboard.membership.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="unused" type="{http://ws.platform.blackboard/xsd}VersionVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "unused"
})
@XmlRootElement(name = "getServerVersion")
public class GetServerVersion {

    @XmlElementRef(name = "unused", namespace = "http://coursemembership.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<VersionVO> unused;

    /**
     * Gets the value of the unused property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link VersionVO }{@code >}
     *     
     */
    public JAXBElement<VersionVO> getUnused() {
        return unused;
    }

    /**
     * Sets the value of the unused property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link VersionVO }{@code >}
     *     
     */
    public void setUnused(JAXBElement<VersionVO> value) {
        this.unused = ((JAXBElement<VersionVO> ) value);
    }

}
