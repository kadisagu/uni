/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.settings.FefuStudentPaymentsAndBonusesSizePub;

/**
 * @author Alexander Zhebko
 * @since 12.03.2013
 */
public class Model
{
    private Integer _defaultGrantSize;
    private Integer _defaultGroupManagerBonusSize;
    private Integer _defaultSocialGrantSize;

    public Integer getDefaultGrantSize()
    {
        return _defaultGrantSize;
    }

    public void setDefaultGrantSize(Integer defaultGrantSize)
    {
        _defaultGrantSize = defaultGrantSize;
    }

    public Integer getDefaultGroupManagerBonusSize()
    {
        return _defaultGroupManagerBonusSize;
    }

    public void setDefaultGroupManagerBonusSize(Integer defaultGroupManagerBonusSize)
    {
        _defaultGroupManagerBonusSize = defaultGroupManagerBonusSize;
    }

    public Integer getDefaultSocialGrantSize()
    {
        return _defaultSocialGrantSize;
    }

    public void setDefaultSocialGrantSize(Integer defaultSocialGrantSize)
    {
        _defaultSocialGrantSize = defaultSocialGrantSize;
    }

}