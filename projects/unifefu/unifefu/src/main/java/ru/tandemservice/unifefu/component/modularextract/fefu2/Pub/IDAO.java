package ru.tandemservice.unifefu.component.modularextract.fefu2.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/20/12
 * Time: 1:58 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuChangePassDiplomaWorkPeriodStuExtract, Model>
{
}
