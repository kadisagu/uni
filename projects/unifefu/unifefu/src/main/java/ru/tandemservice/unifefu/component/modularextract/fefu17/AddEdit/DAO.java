/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu17.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unifefu.entity.TransitCompensationStuExtract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2014
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<TransitCompensationStuExtract, Model> implements IDAO
{
	@Override
	protected GrammaCase getStudentTitleCase()
	{
		return GrammaCase.ACCUSATIVE;
	}

	@Override
	protected TransitCompensationStuExtract createNewInstance()
	{
		return new TransitCompensationStuExtract();
	}

	@Override
	public void prepare(final Model model)
	{
		super.prepare(model);
		model.setCompensationSum(model.getExtract().getCompensationSum() / 100D);
		model.setTransportKindList(model.getExtract().getTransportKind());
		model.setTransportKindModel( UniBaseUtils.getTitleSelectModel(
				new SingleValueCache<List<String>>()
				{
					@Override
					protected List<String> resolve()
					{
						String transportKindProp = ApplicationRuntime.getProperty("transportKindList");
						if (transportKindProp != null)
							return Arrays.asList(transportKindProp.split(";"));
						else
							return new ArrayList<>();
					}
				}
		));
	}

	@Override
	public void update(Model model)
	{

		TransitCompensationStuExtract extract = model.getExtract();
		extract.setCompensationSum(new Double((model.getCompensationSum()*100D)).longValue());
		extract.setTransportKind(StringUtils.join(model.getTransportKindList(), ';'));
		extract.setResponsiblePersonFio(extract.getResponsiblePerson().getPerson().getFio());
		super.update(model);
	}
}
