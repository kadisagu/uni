package ru.tandemservice.unifefu.component.report;

import com.google.common.collect.Iterables;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.catalog.codes.SubjectPassFormCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.entrant.gen.RequestedEnrollmentDirectionGen;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.examset.ExamSetStructureItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.DisciplinesGroup;
import ru.tandemservice.uniec.entity.settings.Group2DisciplineRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: amakarova
 * Date: 28.06.13
 */
public class FefuReportUtil {

    public static Double MAX_MARK = 200.0d;
    public static Double PROFILE_MAX_MARK = 70.0d;

    public static int getBudgetPlan(EnrollmentDirection direction) {
        return direction.getMinisterialPlan() != null ? direction.getMinisterialPlan() : 0;
    }

    public static int getContractPlan(EnrollmentDirection direction) {
        return direction.getContractPlan() != null ? direction.getContractPlan() : 0;
    }

    public static int getBudgetPlanWithoutTarget(EnrollmentDirection direction) {
        int target = direction.getTargetAdmissionPlanBudget() != null ? direction.getTargetAdmissionPlanBudget() : 0;
        return getBudgetPlan(direction) - target;
    }

    public static int getContractPlanWithoutTarget(EnrollmentDirection direction) {
        int target = direction.getTargetAdmissionPlanContract() != null ? direction.getTargetAdmissionPlanContract() : 0;
        return getContractPlan(direction) - target;
    }

    /**
     * Вставляет разрыв страницы в документ.
     * @param elementList список элементов в документе
     */
    public static void insertPageBreak(List<IRtfElement> elementList)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));

        elementList.add(elementFactory.createRtfControl(IRtfData.PAGE));

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));
    }

    public static DQLSelectBuilder getEntrantDirections(Entrant entrant, Session session) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
        .fromEntity(RequestedEnrollmentDirection.class, "d")
        .where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("d")), DQLExpressions.value(entrant)));
        return builder;
    }

    public static List<Object[]> getDirectionsWithOriginal(Session session, EnrollmentCampaign campaign) {
        DQLSelectBuilder directionBuilder = new DQLSelectBuilder();
        directionBuilder.fromEntity(EntrantOriginalDocumentRelation.class, "orig");
        directionBuilder.where(eq(property(EntrantOriginalDocumentRelation.requestedEnrollmentDirection().enrollmentDirection().enrollmentCampaign().fromAlias("orig")), value(campaign)));
        // не забравший документы
        directionBuilder.where(ne(property(EntrantOriginalDocumentRelation.requestedEnrollmentDirection().entrantRequest().entrant().state().code().fromAlias("orig")), value(UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY)));
        directionBuilder.column("orig." + EntrantOriginalDocumentRelation.L_ENTRANT);
        directionBuilder.column("orig." + EntrantOriginalDocumentRelation.L_REQUESTED_ENROLLMENT_DIRECTION);

        return directionBuilder.createStatement(session).list();
    }

    /**
     * @return Список направлений приема с учетом фильтров
     */
    public static List<EnrollmentDirection> getEnrollmentDirectionList(Session session, FefuEntrantSubmittedDocumentsReportModel model)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirection.class, "ed")
                .column("ed")
                .where(eq(property(EnrollmentDirection.enrollmentCampaign().fromAlias("ed")), value(model.getEnrollmentCampaign())));
        if (model.isByAllEnrollmentDirections())
        {
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developForm().s(), model.getDevelopFormList());
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developCondition().s(), model.getDevelopConditionList());
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), model.getQualificationList());
        }
        else
        {
            builder.where(eq(property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().fromAlias("ed")), value(model.getFormativeOrgUnit())));
            builder.where(eq(property(EnrollmentDirection.educationOrgUnit().territorialOrgUnit().fromAlias("ed")), value(model.getTerritorialOrgUnit())));
            if (model.getEducationLevelsHighSchool() != null)
                builder.where(eq(property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().fromAlias("ed")), value(model.getEducationLevelsHighSchool())));
            if (model.getDevelopForm() != null)
                builder.where(eq(property(EnrollmentDirection.educationOrgUnit().developForm().fromAlias("ed")), value(model.getDevelopForm())));
            if (model.getDevelopCondition() != null)
                builder.where(eq(property(EnrollmentDirection.educationOrgUnit().developCondition().fromAlias("ed")), value(model.getDevelopCondition())));
            if (model.getDevelopTech() != null)
                builder.where(eq(property(EnrollmentDirection.educationOrgUnit().developTech().fromAlias("ed")), value(model.getDevelopTech())));
            if (model.getDevelopPeriod() != null)
                builder.where(eq(property(EnrollmentDirection.educationOrgUnit().developPeriod().fromAlias("ed")), value(model.getDevelopPeriod())));
        }

        return builder.createStatement(session).list();
    }

    public static DQLSelectBuilder getRequestedEnrollmentDirections(FefuEntrantSubmittedDocumentsReportModel model, List<EnrollmentDirection> enrollmentDirectionList)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.ENTITY_CLASS, "r")
                .column(property("r"))
                .where(in(property("r", RequestedEnrollmentDirection.enrollmentDirection()), enrollmentDirectionList))

                .where(eq(property("r", RequestedEnrollmentDirection.compensationType()), value(model.getCompensationType())))
                .where(betweenDays(RequestedEnrollmentDirection.regDate().fromAlias("r"), model.getDateFrom(), model.getDateTo()))

                .where(eq(property("r", RequestedEnrollmentDirection.entrantRequest().entrant().archival()), value(false)))
                .where(not(eq(property("r", RequestedEnrollmentDirection.state().code()), value(EntrantStateCodes.OUT_OF_COMPETITION))))
                .where(not(eq(property("r", RequestedEnrollmentDirection.state().code()), value(EntrantStateCodes.TAKE_DOCUMENTS_AWAY))));

        if (model.isOnlyWithOriginals())
            builder.where(eq(property("r", RequestedEnrollmentDirection.originalDocumentHandedIn()), value(true)));

        FilterUtils.applySelectFilter(builder, "r", RequestedEnrollmentDirection.studentCategory().s(), model.getStudentCategoryList());

        if (!model.getEntrantCustomStateList().isEmpty())
        {
            Date now = new Date();
            IDQLExpression customStateExp = and(
                    in(property("ecs", EntrantCustomState.customState()), model.getEntrantCustomStateList()),
                    eq(property("ecs", EntrantCustomState.entrant()), property("r", RequestedEnrollmentDirection.entrantRequest().entrant())),
                    and(
                            or(isNull(property("ecs", EntrantCustomState.beginDate())), ge(valueDate(now), property("ecs", EntrantCustomState.beginDate()))),
                            or(isNull(property("ecs", EntrantCustomState.endDate())), le(valueDate(now), property("ecs", EntrantCustomState.endDate())))
                    )
            );
            if (model.getCustomStateRule().getId().equals(FefuEntrantSubmittedDocumentsReportModel.CUSTOM_STATE_CONTAINS))
                builder.where(existsByExpr(EntrantCustomState.class, "ecs", customStateExp));
            else builder.where(notExistsByExpr(EntrantCustomState.class, "ecs", customStateExp));
        }

        if (!model.isIncludeForeignPerson())
            builder.where(eq(property("r", RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().citizenship().code()),
                             value(IKladrDefines.RUSSIA_COUNTRY_CODE)));

        if (model.isIncludeEntrantTargetAdmission())
            builder.where(eq(property("r", RequestedEnrollmentDirection.targetAdmission()), value(Boolean.TRUE)));

        if (model.isIncludeEntrantWithBenefit())
            builder.where(exists(PersonBenefit.class, PersonBenefit.person().s(), property("r", RequestedEnrollmentDirection.entrantRequest().entrant().person())));

        if (model.isIncludeEntrantNoMark())
        {
            builder.where(existsByExpr(ChosenEntranceDiscipline.class, "ced", and(
                    eq(property("r"), property("ced", ChosenEntranceDiscipline.chosenEnrollmentDirection())),
                    isNull(property("ced", ChosenEntranceDiscipline.finalMark()))
            )))
                    .where(not(eq(property("r", RequestedEnrollmentDirection.competitionKind().code()), value(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))));
        }

        return builder;
    }

    /**
     * @return map(RequestedEnrollmentDirection Id, list[ChosenEntranceDiscipline) )
     */
    public static Map<Long, Set<ChosenEntranceDiscipline>> getChosenDisciplineByRequestedDirection(Session session, DQLSelectBuilder requestedDirectionsBuilder)
    {
        return new DQLSelectBuilder()
                .fromEntity(ChosenEntranceDiscipline.class, "cho")
                .column(property("cho"))
                .where(in(property("cho", ChosenEntranceDiscipline.chosenEnrollmentDirection()), requestedDirectionsBuilder.buildQuery()))
                .createStatement(session).<ChosenEntranceDiscipline>list()

                .stream()
                .collect(Collectors.groupingBy(cho -> cho.getChosenEnrollmentDirection().getId(),
                                               Collectors.mapping(cho -> cho, Collectors.toSet())));
    }

    /**
     * @param requestedDirectionsBuilder  список выбранных направлений
     * @return мапа id Заявления абитуриента - список индивидуальных достижений
     */
    public static Map<Long, Set<EntrantIndividualProgress>> getIndividualAchievementsMap(Session session, DQLSelectBuilder requestedDirectionsBuilder)
    {
        String eipAlias = "eip";
        String reqAlias = "req";
        List<Object[]> raws = new DQLSelectBuilder()
                .fromEntity(EntrantIndividualProgress.class, eipAlias)
                .column(property(eipAlias))

                .joinEntity(eipAlias, DQLJoinType.inner, EntrantRequest.class, reqAlias,
                            and(eq(property(eipAlias, EntrantIndividualProgress.entrant()),
                                   property(reqAlias, EntrantRequest.entrant())),
                                IndProg2Request4Exclude.getExpression4ExcludeEntrantIndividualProgress(property(reqAlias), property(eipAlias)),
                                existsByExpr(RequestedEnrollmentDirection.class, "red", and(
                                        eq(property(reqAlias), property("red", RequestedEnrollmentDirection.entrantRequest())),
                                        in(property("red"), requestedDirectionsBuilder.buildQuery())
                                ))
                            ))
                .column(property(reqAlias, EntrantRequest.id()))

                .createStatement(session).list();

        return raws.stream()
                .collect(Collectors.groupingBy(
                        raw -> (Long) raw[1],
                        Collectors.mapping(raw -> (EntrantIndividualProgress) raw[0], Collectors.toSet())));
    }

    /**
     * Другие выбранные направление по заявлениям из списка выбранных направлений
     * @return map(EntrantRequest Id, RequestedEnrollmentDirection)
     */
    public static Map<Long, List<RequestedEnrollmentDirection>> getOtherRequestedDirection(Session session, List<RequestedEnrollmentDirection> requestedDirections)
    {
        List<Long> requestIds = requestedDirections.stream()
                .map(RequestedEnrollmentDirectionGen::getEntrantRequest)
                .map(EntityBase::getId)
                .distinct()
                .collect(Collectors.toList());

        List<RequestedEnrollmentDirection> other = new ArrayList<>();
        for (final List requests : Iterables.partition(requestIds, DQL.MAX_VALUES_ROW_NUMBER))
        {
            other.addAll(new DQLSelectBuilder()
                                 .fromEntity(RequestedEnrollmentDirection.class, "red")
                                 .column(property("red"))
                                 .where(in(property("red", RequestedEnrollmentDirection.entrantRequest().id()), requests))
                                 .createStatement(session).list());
        }

        return other.stream().collect(Collectors.groupingBy(red -> red.getEntrantRequest().getId(), Collectors.mapping(red -> red, Collectors.toList())));
    }

    /**
     * Персоны с льготами из списка выбранных направлений
     *
     * @return map(Person Id, list[PersonBenefit) )
     */
    public static Map<Long, List<PersonBenefit>> getPersonBenefitsMap(Session session, DQLSelectBuilder requestedDirectionsBuilder)
    {
        return new DQLSelectBuilder()
                .fromEntity(PersonBenefit.class, "pb")
                .column(property("pb", PersonBenefit.person().id()))
                .column(property("pb"))
                .where(existsByExpr(RequestedEnrollmentDirection.class, "red",
                                    and(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().person()),
                                           property("pb", PersonBenefit.person())),
                                        in(property("red"), requestedDirectionsBuilder.buildQuery()))
                ))
                .createStatement(session).<Object[]>list().stream()
                .collect(Collectors.groupingBy(row -> (Long) row[0], Collectors.mapping(row -> (PersonBenefit) row[1], Collectors.toList())));
    }

    public static String getCategory(RequestedEnrollmentDirection requestedDirection,
                                     boolean isIncludeEntrantTargetAdmission, boolean hasBenefits, boolean hasRecommendations)
    {
        StringBuilder out = new StringBuilder();
        if (isIncludeEntrantTargetAdmission)
        {
            ExternalOrgUnit orgUnit = requestedDirection.getExternalOrgUnit();
            return orgUnit != null ? orgUnit.getTitle() : "";
        }
        else if (requestedDirection.isTargetAdmission())
            out.append("целевая квота ");

        CompetitionKind competitionKind = requestedDirection.getCompetitionKind();
        if (competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
            out.append(competitionKind.getShortTitle());
        else if (competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION))
            out.append("особая квота ");
        if (hasBenefits || hasRecommendations)
            out.append("преим. право");

        return out.toString();
    }

    public static String getRegNumber(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return requestedEnrollmentDirection.getEntrantRequest().getEntrant().getPersonalNumber();
    }

    public static void addPassFormNSummMark(ReportItem reportItem, List<SubjectPassForm> passForms, ChosenEntranceDiscipline discipline)
    {
        Double mark = discipline.getFinalMark();
        if (passForms == null)
        {
            if (mark != null && mark > 0)
                reportItem.setPassFormEge(true);
        }
        else
        {
            passForms.forEach(passForm -> {
                switch (passForm.getCode())
                {
                    case SubjectPassFormCodes.STATE_EXAM:
                    case SubjectPassFormCodes.STATE_EXAM_INTERNAL:
                        reportItem.setPassFormEge(true);
                        break;
                    default:
                        reportItem.setPassFormInner(true);
                }
            });
        }

        if (mark != null) reportItem.setSumMark(reportItem.getSumMark() + mark);
    }

    public static void splitMarksColumns(RtfRow headRow, RtfRow firstRow, int colIndex,
                                         EnrollmentDirectionExamSetData examSetData,
                                         List<StudentCategory> listCategory, boolean isShowDisciplineTitles)
    {
        List<ExamSetStructureItem> examSetStructureItems = examSetData.getExamSetStructure().getItemList();
        if(CollectionUtils.isEmpty(examSetStructureItems)) return;

        int[] parts = new int[examSetStructureItems.size()];
        Arrays.fill(parts, 1);

        RtfUtil.splitRow(headRow, colIndex, (newCell, index) -> {
                             RtfString string = new RtfString();
                             if (isShowDisciplineTitles)
                             {
                                 // сейчас хитро, надо выбрать по всем категориям самое короткое название
                                 int length = Integer.MAX_VALUE;
                                 List<String> columnTitleList = new ArrayList<>();
                                 for (StudentCategory studentCategory : listCategory)
                                 {
                                     ExamSetItem item = examSetData.getCategoryExamSet(studentCategory).get(index);

                                     List<String> titleList = new ArrayList<>();
                                     if (item != null)
                                     {
                                         titleList.add(item.getSubject().getShortTitle());
                                         titleList.addAll(item.getChoice().stream().map(SetDiscipline::getShortTitle).collect(Collectors.toList()));
                                     }

                                     String title = StringUtils.join(titleList, " ");
                                     if (title.length() < length)
                                     {
                                         length = title.length();
                                         columnTitleList = titleList;
                                     }
                                 }
                                 Collections.sort(columnTitleList);
                                 for (Iterator<String> iter = columnTitleList.iterator(); iter.hasNext(); )
                                 {
                                     String title = iter.next();
                                     string.append(title);
                                     if (iter.hasNext()) string.append("/").append(IRtfData.LINE);
                                 }
                             }
                             else
                             {
                                 string.append(String.valueOf(index + 1) + " экз.");
                             }

                             newCell.setElementList(string.toList());
                         },
                         parts);
        RtfUtil.splitRow(firstRow, colIndex, null, parts);
    }

    /**
     * @return map(EnrollmentDirection Id, map[StudentCategoryId, map[Priority, list(Discipline2RealizationWayRelation Id) ) )
     */
    public static Map<Long, Map<Long, Map<Integer, List<Long>>>>  getDisciplinesByStudentCategoryByDirection(Session session, List<EnrollmentDirection> enrollmentDirectionList)
    {
        List<EntranceDiscipline> entranceDisciplines = new DQLSelectBuilder()
                .fromEntity(EntranceDiscipline.class, "ed")
                .column(property("ed"))
                .where(in(property("ed", EntranceDiscipline.enrollmentDirection()), enrollmentDirectionList))

                .createStatement(session).<EntranceDiscipline>list();

        List<DisciplinesGroup> disciplinesGroupList = entranceDisciplines.stream()
                .filter(dis -> dis.getDiscipline() instanceof DisciplinesGroup)
                .map(dis -> (DisciplinesGroup) dis.getDiscipline())
                .collect(Collectors.toList());

        Map<Long, List<Long>> disciplineByGroup = new DQLSelectBuilder()
                .fromEntity(Group2DisciplineRelation.class, "g2d")
                .column(property("g2d"))
                .where(in(property("g2d", Group2DisciplineRelation.group()), disciplinesGroupList))

                .createStatement(session).<Group2DisciplineRelation>list().stream()

                .collect(Collectors.groupingBy(g2d -> g2d.getGroup().getId(),
                                               Collectors.mapping(g2d -> g2d.getDiscipline().getId(), Collectors.toList())));

        return entranceDisciplines.stream()
                .filter(dis -> dis.getPriority() != null)
                .flatMap(dis -> {
                    SetDiscipline setDiscipline = dis.getDiscipline();
                    ArrayList<Long> list = new ArrayList<>();
                    Long setDisciplineId = setDiscipline.getId();
                    if (setDiscipline instanceof Discipline2RealizationWayRelation) list.add(setDisciplineId);

                    List<Long> disciplinesFromGroup = disciplineByGroup.get(setDisciplineId);
                    if (disciplinesFromGroup != null) list.addAll(disciplinesFromGroup); // Group2DisciplineRelation

                    return list.stream().map(setDis -> new CoreCollectionUtils.Pair<>(dis, setDis));
                })
                .collect(Collectors.groupingBy(
                        pair -> pair.getX().getEnrollmentDirection().getId(),//Направление
                        Collectors.mapping(
                                pair -> pair,
                                Collectors.groupingBy(
                                        pair -> pair.getX().getStudentCategory().getId(),//Категория обучающегося
                                        Collectors.mapping(
                                                pair -> pair,
                                                Collectors.groupingBy(
                                                        pair -> pair.getX().getPriority(),//Приоритет
                                                        TreeMap::new,//Сортировка по приоритету
                                                        Collectors.mapping(CoreCollectionUtils.Pair::getY, Collectors.toList())))))));//список дисциплин с этим приоритетом
    }
}
