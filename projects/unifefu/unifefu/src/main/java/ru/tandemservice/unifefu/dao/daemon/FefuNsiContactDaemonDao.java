/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiContactType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact;
import ru.tandemservice.unifefu.events.nsi.AddressListener;
import ru.tandemservice.unifefu.events.nsi.ContactDataListener;
import ru.tandemservice.unifefu.events.nsi.NsiPersonContactListener;
import ru.tandemservice.unifefu.ws.nsi.reactor.ContactTypeReactor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 12.01.2015
 */
public class FefuNsiContactDaemonDao extends UniBaseDao implements IFefuNsiContactDaemonDao
{
    public static final String NSI_CONTACT_SERVICE_AUTOSYNC = "fefu.nsi.daemon.addrautosync";
    public static final int DAEMON_ITERATION_TIME = 60;  //TODO

    // Используется для накопления идентификаторов персон, адреса которых нельзя синхронизировать.
    // Сейчас адреса в России, у которых кроме страны не заполнено ни одно поле, очень сложно отловить на уровне запроса, а
    // демон пытается их обработать снова и снова, считая их необработанными
    private static Set<Long> PERSON_ID_TO_IGNORE = new HashSet<>();

    private static boolean sync_locked = false;
    private static boolean manual_locked = false;

    public static void lockDaemon()
    {
        manual_locked = true;
    }

    public static void unlockDaemon()
    {
        manual_locked = false;
    }

    public static boolean isLocked()
    {
        return manual_locked;
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(FefuNsiContactDaemonDao.class.getName(), DAEMON_ITERATION_TIME, IFefuNsiContactDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            // Проверяем, включена ли автосинхронизация. Если порпертя отсутствует в конфигах, то прерываем выполнение демона
            if (null == ApplicationRuntime.getProperty(NSI_CONTACT_SERVICE_AUTOSYNC)) return;
            if (!Boolean.parseBoolean(ApplicationRuntime.getProperty(NSI_CONTACT_SERVICE_AUTOSYNC))) return;


            // Если в данный момент идёт синхронизация, то пропускаем очередной шаг демона
            if (sync_locked || manual_locked) return;

            final IFefuNsiContactDaemonDao dao = IFefuNsiContactDaemonDao.instance.get();
            sync_locked = true;

            try
            {
                dao.doSyncContactsWithNSI();
            } catch (final Throwable t)
            {
                sync_locked = false;
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            }
            sync_locked = false;
        }
    };

    protected Session lock4update()
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, IFefuNsiContactDaemonDao.GLOBAL_DAEMON_LOCK);
        return session;
    }

    @Override
    public void doSyncContactsWithNSI()
    {
        FefuNsiContactType regConType = DataAccessServices.dao().get(FefuNsiContactType.class, FefuNsiContactType.userCode(), ContactTypeReactor.CONTACT_KIND_CODE_REG_ADDR);
        FefuNsiContactType factConType = DataAccessServices.dao().get(FefuNsiContactType.class, FefuNsiContactType.userCode(), ContactTypeReactor.CONTACT_KIND_CODE_FACT_ADDR);
        FefuNsiContactType mobileType = DataAccessServices.dao().get(FefuNsiContactType.class, FefuNsiContactType.userCode(), ContactTypeReactor.CONTACT_KIND_CODE_MOBILE_PHONE);
        FefuNsiContactType phoneType = DataAccessServices.dao().get(FefuNsiContactType.class, FefuNsiContactType.userCode(), ContactTypeReactor.CONTACT_KIND_CODE_PHONE);
        FefuNsiContactType workType = DataAccessServices.dao().get(FefuNsiContactType.class, FefuNsiContactType.userCode(), ContactTypeReactor.CONTACT_KIND_CODE_WORK_PHONE);

        if (null == regConType || null == factConType || null == mobileType || null == phoneType || null == workType)
        {
            System.out.println("+++++++++++ FefuNsiContactDaemonDao could not be started, because there is no elements in ContactType catalog. Probably, it was not synchronized with NSI.");
            return;
        }


        DQLSelectBuilder suBuilder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").column(property(EmployeePost.person().id().fromAlias("ep")));
        DQLSelectBuilder emplSuBuilder = new DQLSelectBuilder().fromEntity(Employee.class, "em").column(property(Employee.person().id().fromAlias("em")));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Person.class, "e").column(property("e"))
                .joinEntity("e", DQLJoinType.left, FefuNsiPersonContact.class, "c", eq(property(Person.id().fromAlias("e")), property(FefuNsiPersonContact.person().id().fromAlias("c"))))
                .where(or(
                        isNotNull(property(Person.address().fromAlias("e"))),
                        isNotNull(property(Person.identityCard().address().fromAlias("e"))),
                        isNotNull(property(Person.contactData().phoneReg().fromAlias("e"))),
                        isNotNull(property(Person.contactData().phoneRegTemp().fromAlias("e"))),
                        isNotNull(property(Person.contactData().phoneFact().fromAlias("e"))),
                        isNotNull(property(Person.contactData().phoneDefault().fromAlias("e"))),
                        isNotNull(property(Person.contactData().phoneWork().fromAlias("e"))),
                        isNotNull(property(Person.contactData().phoneMobile().fromAlias("e"))),
                        isNotNull(property(Person.contactData().phoneRelatives().fromAlias("e")))
                ))
                .where(notIn(property(Person.id().fromAlias("e")), suBuilder.buildQuery()))
                .where(notIn(property(Person.id().fromAlias("e")), emplSuBuilder.buildQuery()))
                .where(isNull(property("c"))).top(100);

        if (!PERSON_ID_TO_IGNORE.isEmpty()) builder.where(notIn(property(Person.id().fromAlias("e")), PERSON_ID_TO_IGNORE));
        List<Person> nonSynchronizedPersonContacts = builder.createStatement(getSession()).list();

        if (nonSynchronizedPersonContacts.isEmpty()) return;

        System.out.println("+++++++++++ FefuNsiContactDaemonDao has started with " + nonSynchronizedPersonContacts.size() + " persons.");

        int contactCnt = 0;
        for (Person person : nonSynchronizedPersonContacts)
        {
            if (null != person.getAddress() && !AddressListener.JUST_CHANGED_CONTACT_ID_SET.contains(person.getAddress().getId()))
            {
                FefuNsiPersonContact contact = createNsiPersonContactFromAddress(person, person.getAddress(), factConType);
                if (null != contact)
                {
                    save(contact);
                    NsiPersonContactListener.JUST_CHANGED_CONTACT_ID_SET.add(contact.getId());
                    contactCnt++;
                } else PERSON_ID_TO_IGNORE.add(person.getId());
            }

            if (null != person.getIdentityCard().getAddress() && !AddressListener.JUST_CHANGED_CONTACT_ID_SET.contains(person.getIdentityCard().getAddress().getId()))
            {
                FefuNsiPersonContact contact = createNsiPersonContactFromAddress(person, person.getIdentityCard().getAddress(), regConType);
                if (null != contact)
                {
                    save(contact);
                    NsiPersonContactListener.JUST_CHANGED_CONTACT_ID_SET.add(contact.getId());
                    contactCnt++;
                } else PERSON_ID_TO_IGNORE.add(person.getId());
            }

            if (!ContactDataListener.JUST_CHANGED_CONTACT_ID_SET.contains(person.getContactData().getId()))
            {
                for (FefuNsiPersonContact contact : createNsiPersonContactFromContactData(person, phoneType, workType, mobileType))
                {
                    save(contact);
                    contactCnt++;
                    NsiPersonContactListener.JUST_CHANGED_CONTACT_ID_SET.add(contact.getId());
                }
            }
        }

        System.out.println("+++++++++++ FefuNsiContactDaemonDao was finished with 100 persons. " + contactCnt + " contacts were added.");
    }

    private FefuNsiPersonContact createNsiPersonContactFromAddress(Person person, AddressBase addr, FefuNsiContactType type)
    {
        FefuNsiPersonContact contact = new FefuNsiPersonContact();
        contact.setPerson(person);
        contact.setAddress(addr);
        contact.setType(type);
        contact.setTypeStr("Адрес");

        if (addr instanceof AddressRu)
        {
            AddressRu addrRu = (AddressRu) addr;
            contact.setField1(addrRu.getInheritedPostCode());

            if (null != addrRu.getSettlement())
            {
                List<AddressItem> addressItemList = new ArrayList<>();
                AddressItem parentAddrItem = addrRu.getSettlement();
                while (null != parentAddrItem)
                {
                    addressItemList.add(parentAddrItem);
                    parentAddrItem = parentAddrItem.getParent();
                }

                if (addressItemList.get(0).getAddressType().isCountryside())
                {
                    contact.setField5(addressItemList.get(0).getTitle() + " " + addressItemList.get(0).getAddressType().getShortTitle());
                    if (addressItemList.size() == 2)
                    {
                        contact.setField2(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                    } else if (addressItemList.size() == 3)
                    {
                        contact.setField4(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                        contact.setField2(addressItemList.get(2).getTitle() + " " + addressItemList.get(2).getAddressType().getShortTitle());
                    } else if (addressItemList.size() == 4)
                    {
                        contact.setField4(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                        contact.setField2(addressItemList.get(3).getTitle() + " " + addressItemList.get(3).getAddressType().getShortTitle());
                    }
                } else
                {
                    contact.setField4(addressItemList.get(0).getTitle() + " " + addressItemList.get(0).getAddressType().getShortTitle());
                    if (addressItemList.size() == 2)
                    {
                        contact.setField2(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                    } else if (addressItemList.size() == 3)
                    {
                        contact.setField2(addressItemList.get(2).getTitle() + " " + addressItemList.get(2).getAddressType().getShortTitle());
                        contact.setField3(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                    } else if (addressItemList.size() == 4)
                    {
                        contact.setField2(addressItemList.get(3).getTitle() + " " + addressItemList.get(3).getAddressType().getShortTitle());
                        contact.setField3(addressItemList.get(1).getTitle() + " " + addressItemList.get(1).getAddressType().getShortTitle());
                    }
                }
            }

            if (null != addrRu.getStreet())
            {
                contact.setField6(addrRu.getStreet().getTitle() + " " + addrRu.getStreet().getAddressType().getShortTitle());
            }

            contact.setField7(addrRu.getHouseNumber());
            contact.setField8(addrRu.getHouseUnitNumber());
            contact.setField9(addrRu.getFlatNumber());
        } else if (addr instanceof AddressInter)
        {
            AddressInter addrInt = (AddressInter) addr;
            contact.setField1(null != addrInt.getCountry().getFullTitle() ? addrInt.getCountry().getFullTitle() : addrInt.getCountry().getTitle());
            contact.setField2(addrInt.getRegion());
            contact.setField3(addrInt.getDistrict());

            if (null != addrInt.getSettlement())
            {
                contact.setField4(addrInt.getSettlement().getTitle() + " " + addrInt.getSettlement().getAddressType().getShortTitle());
            }

            String[] addrArr = StringUtils.split(addrInt.getAddressLocation(), ",");
            if (null != addrArr)
            {
                if (addrArr.length > 0) contact.setField6(addrArr[0]);
                if (addrArr.length > 1) contact.setField7(addrArr[1]);
                if (addrArr.length > 2) contact.setField8(addrArr[2]);
                if (addrArr.length > 3) contact.setField9(addrArr[3]);
                if (addrArr.length > 4) contact.setField10(addrArr[4]);
            }
        } else if (addr instanceof AddressString)
        {
            AddressString addrStr = (AddressString) addr;
            contact.setDescription(addrStr.getAddress());
            String[] addrArr = StringUtils.split(addrStr.getAddress(), ",");
            if (null != addrArr)
            {
                if (addrArr.length > 0) contact.setField1(addrArr[0]);
                if (addrArr.length > 1) contact.setField2(addrArr[1]);
                if (addrArr.length > 2) contact.setField3(addrArr[2]);
                if (addrArr.length > 3) contact.setField4(addrArr[3]);
                if (addrArr.length > 4) contact.setField5(addrArr[4]);
                if (addrArr.length > 5) contact.setField6(addrArr[5]);
                if (addrArr.length > 6) contact.setField7(addrArr[6]);
                if (addrArr.length > 7) contact.setField8(addrArr[7]);
                if (addrArr.length > 8) contact.setField9(addrArr[8]);
                if (addrArr.length > 9) contact.setField10(addrArr[9]);
            }
        }

        if (addr instanceof AddressRu || addr instanceof AddressInter)
        {
            StringBuilder descBuilder = new StringBuilder();
            if (null != contact.getField1()) descBuilder.append(contact.getField1());
            if (null != contact.getField2())
                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField2());
            if (null != contact.getField3())
                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField3());
            if (null != contact.getField4())
                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField4());
            if (null != contact.getField5())
                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField5());
            if (null != contact.getField6())
                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField6());
            if (null != contact.getField7())
                descBuilder.append(descBuilder.length() > 0 ? ", дом № " : "дом № ").append(contact.getField7());
            if (null != contact.getField8())
                descBuilder.append(descBuilder.length() > 0 ? ", корп. " : "корп. ").append(contact.getField8());
            if (null != contact.getField9())
                descBuilder.append(descBuilder.length() > 0 ? ", кв. " : "кв. ").append(contact.getField9());
            if (null != contact.getField10())
                descBuilder.append(descBuilder.length() > 0 ? ", " : "").append(contact.getField10());
            contact.setDescription(descBuilder.toString());
        }

        if (null == StringUtils.trimToNull(contact.getDescription())) return null;
        return contact;
    }

    private List<FefuNsiPersonContact> createNsiPersonContactFromContactData(Person person, FefuNsiContactType phoneType, FefuNsiContactType workType, FefuNsiContactType mobileType)
    {
        List<FefuNsiPersonContact> result = new ArrayList<>();
        PersonContactData perContData = person.getContactData();

        if (null != perContData.getPhoneReg())
            result.add(new FefuNsiPersonContact(person, phoneType, PersonContactData.P_PHONE_REG, perContData.getPhoneReg(), "Телефон по месту постоянной регистрации"));
        if (null != perContData.getPhoneRegTemp())
            result.add(new FefuNsiPersonContact(person, phoneType, PersonContactData.P_PHONE_REG_TEMP, perContData.getPhoneRegTemp(), "Телефон по месту временной регистрации"));
        if (null != perContData.getPhoneFact())
            result.add(new FefuNsiPersonContact(person, phoneType, PersonContactData.P_PHONE_FACT, perContData.getPhoneFact(), "Телефон по месту фактического проживания"));
        if (null != perContData.getPhoneDefault())
            result.add(new FefuNsiPersonContact(person, phoneType, PersonContactData.P_PHONE_DEFAULT, perContData.getPhoneDefault(), "Контактный телефон"));
        if (null != perContData.getPhoneWork())
            result.add(new FefuNsiPersonContact(person, workType, PersonContactData.P_PHONE_WORK, perContData.getPhoneWork(), "Рабочий телефон"));
        if (null != perContData.getPhoneMobile())
            result.add(new FefuNsiPersonContact(person, mobileType, PersonContactData.P_PHONE_MOBILE, perContData.getPhoneMobile(), "Мобильный телефон"));
        if (null != perContData.getPhoneRelatives())
            result.add(new FefuNsiPersonContact(person, phoneType, PersonContactData.P_PHONE_RELATIVES, perContData.getPhoneRelatives(), "Телефон родственников"));

        return result;
    }
}