package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О направлении на практику в пределах ОУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SendPracticOutStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.SendPracticOutStuExtract";
    public static final String ENTITY_NAME = "sendPracticOutStuExtract";
    public static final int VERSION_HASH = -454047687;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRACTICE_KIND = "practiceKind";
    public static final String P_PRACTICE_DURATION_STR = "practiceDurationStr";
    public static final String P_PRACTICE_BEGIN_DATE = "practiceBeginDate";
    public static final String P_PRACTICE_END_DATE = "practiceEndDate";
    public static final String P_ATTESTATION_DATE = "attestationDate";
    public static final String P_OUT_CLASS_TIME = "outClassTime";
    public static final String P_DONE_EDU_PLAN = "doneEduPlan";
    public static final String P_PROVIDE_FUNDS_ACCORDING_TO_ESTIMATES = "provideFundsAccordingToEstimates";
    public static final String P_ESTIMATE_NUM = "estimateNum";
    public static final String P_DONE_PRACTICE = "donePractice";
    public static final String P_PRACTICE_FACT_ADDRESS_STR = "practiceFactAddressStr";
    public static final String P_PRACTICE_HEADER_OUT_STR = "practiceHeaderOutStr";
    public static final String P_PRACTICE_HEADER_INNER_STR = "practiceHeaderInnerStr";
    public static final String P_PREVENT_ACCIDENTS_I_C_STR = "preventAccidentsICStr";
    public static final String P_RESPONS_FOR_RECIEVE_CASH_STR = "responsForRecieveCashStr";
    public static final String P_TEXT_PARAGRAPH = "textParagraph";
    public static final String L_PRACTICE_COURSE = "practiceCourse";
    public static final String L_PREVENT_ACCIDENTS_I_C = "preventAccidentsIC";
    public static final String L_PREVENT_ACCIDENTS_I_C_DEGREE = "preventAccidentsICDegree";
    public static final String L_RESPONS_FOR_RECIEVE_CASH = "responsForRecieveCash";
    public static final String L_RESPONS_FOR_RECIEVE_CASH_DEGREE = "responsForRecieveCashDegree";
    public static final String L_PRACTICE_EXT_ORG_UNIT = "practiceExtOrgUnit";
    public static final String L_PRACTICE_HEADER_INNER = "practiceHeaderInner";
    public static final String L_PRACTICE_HEADER_INNER_DEGREE = "practiceHeaderInnerDegree";
    public static final String L_PRACTICE_CONTRACT = "practiceContract";

    private String _practiceKind;     // Вид практики
    private String _practiceDurationStr;     // Объем(недель)
    private Date _practiceBeginDate;     // Дата начала практики
    private Date _practiceEndDate;     // Дата окончания практики
    private Date _attestationDate;     // Дата аттестации по итогам практики
    private boolean _outClassTime;     // В свободное от аудиторных занятий время
    private boolean _doneEduPlan;     // Полное выполнение учебного плана
    private boolean _provideFundsAccordingToEstimates;     // Выделить средства согласно сметам
    private String _estimateNum;     // Номер приложения(сметы расходов)
    private boolean _donePractice;     // Cчитать прошедшими практику
    private String _practiceFactAddressStr;     // Фактический адрес прохождения практики
    private String _practiceHeaderOutStr;     // Руководитель практики от организации(строка)
    private String _practiceHeaderInnerStr;     // Руководитель практики от ОУ(строка)
    private String _preventAccidentsICStr;     // Ответственный за соблюдение техники безопасности(печать)
    private String _responsForRecieveCashStr;     // Ответственный за получение денежных средств(печать)
    private String _textParagraph;     // Текстовый параграф
    private Course _practiceCourse;     // Курс прохождения практики
    private EmployeePost _preventAccidentsIC;     // Ответственный за соблюдение техники безопасности
    private PersonAcademicDegree _preventAccidentsICDegree;     // Ответственный за соблюдение техники безопасности(уч. степень)
    private EmployeePost _responsForRecieveCash;     // Ответственный за получение денежных средств
    private PersonAcademicDegree _responsForRecieveCashDegree;     // Ответственный за получение денежных средств(уч. степень)
    private ExternalOrgUnit _practiceExtOrgUnit;     // Организация
    private EmployeePost _practiceHeaderInner;     // Руководитель практики от ОУ
    private PersonAcademicDegree _practiceHeaderInnerDegree;     // Руководитель практики от ОУ(уч. степень)
    private FefuPracticeContractWithExtOu _practiceContract;     // Договор на прохождение практики

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид практики. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeKind()
    {
        return _practiceKind;
    }

    /**
     * @param practiceKind Вид практики. Свойство не может быть null.
     */
    public void setPracticeKind(String practiceKind)
    {
        dirty(_practiceKind, practiceKind);
        _practiceKind = practiceKind;
    }

    /**
     * @return Объем(недель). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeDurationStr()
    {
        return _practiceDurationStr;
    }

    /**
     * @param practiceDurationStr Объем(недель). Свойство не может быть null.
     */
    public void setPracticeDurationStr(String practiceDurationStr)
    {
        dirty(_practiceDurationStr, practiceDurationStr);
        _practiceDurationStr = practiceDurationStr;
    }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     */
    @NotNull
    public Date getPracticeBeginDate()
    {
        return _practiceBeginDate;
    }

    /**
     * @param practiceBeginDate Дата начала практики. Свойство не может быть null.
     */
    public void setPracticeBeginDate(Date practiceBeginDate)
    {
        dirty(_practiceBeginDate, practiceBeginDate);
        _practiceBeginDate = practiceBeginDate;
    }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     */
    @NotNull
    public Date getPracticeEndDate()
    {
        return _practiceEndDate;
    }

    /**
     * @param practiceEndDate Дата окончания практики. Свойство не может быть null.
     */
    public void setPracticeEndDate(Date practiceEndDate)
    {
        dirty(_practiceEndDate, practiceEndDate);
        _practiceEndDate = practiceEndDate;
    }

    /**
     * @return Дата аттестации по итогам практики. Свойство не может быть null.
     */
    @NotNull
    public Date getAttestationDate()
    {
        return _attestationDate;
    }

    /**
     * @param attestationDate Дата аттестации по итогам практики. Свойство не может быть null.
     */
    public void setAttestationDate(Date attestationDate)
    {
        dirty(_attestationDate, attestationDate);
        _attestationDate = attestationDate;
    }

    /**
     * @return В свободное от аудиторных занятий время. Свойство не может быть null.
     */
    @NotNull
    public boolean isOutClassTime()
    {
        return _outClassTime;
    }

    /**
     * @param outClassTime В свободное от аудиторных занятий время. Свойство не может быть null.
     */
    public void setOutClassTime(boolean outClassTime)
    {
        dirty(_outClassTime, outClassTime);
        _outClassTime = outClassTime;
    }

    /**
     * @return Полное выполнение учебного плана. Свойство не может быть null.
     */
    @NotNull
    public boolean isDoneEduPlan()
    {
        return _doneEduPlan;
    }

    /**
     * @param doneEduPlan Полное выполнение учебного плана. Свойство не может быть null.
     */
    public void setDoneEduPlan(boolean doneEduPlan)
    {
        dirty(_doneEduPlan, doneEduPlan);
        _doneEduPlan = doneEduPlan;
    }

    /**
     * @return Выделить средства согласно сметам. Свойство не может быть null.
     */
    @NotNull
    public boolean isProvideFundsAccordingToEstimates()
    {
        return _provideFundsAccordingToEstimates;
    }

    /**
     * @param provideFundsAccordingToEstimates Выделить средства согласно сметам. Свойство не может быть null.
     */
    public void setProvideFundsAccordingToEstimates(boolean provideFundsAccordingToEstimates)
    {
        dirty(_provideFundsAccordingToEstimates, provideFundsAccordingToEstimates);
        _provideFundsAccordingToEstimates = provideFundsAccordingToEstimates;
    }

    /**
     * @return Номер приложения(сметы расходов).
     */
    @Length(max=255)
    public String getEstimateNum()
    {
        return _estimateNum;
    }

    /**
     * @param estimateNum Номер приложения(сметы расходов).
     */
    public void setEstimateNum(String estimateNum)
    {
        dirty(_estimateNum, estimateNum);
        _estimateNum = estimateNum;
    }

    /**
     * @return Cчитать прошедшими практику. Свойство не может быть null.
     */
    @NotNull
    public boolean isDonePractice()
    {
        return _donePractice;
    }

    /**
     * @param donePractice Cчитать прошедшими практику. Свойство не может быть null.
     */
    public void setDonePractice(boolean donePractice)
    {
        dirty(_donePractice, donePractice);
        _donePractice = donePractice;
    }

    /**
     * @return Фактический адрес прохождения практики.
     */
    @Length(max=255)
    public String getPracticeFactAddressStr()
    {
        return _practiceFactAddressStr;
    }

    /**
     * @param practiceFactAddressStr Фактический адрес прохождения практики.
     */
    public void setPracticeFactAddressStr(String practiceFactAddressStr)
    {
        dirty(_practiceFactAddressStr, practiceFactAddressStr);
        _practiceFactAddressStr = practiceFactAddressStr;
    }

    /**
     * @return Руководитель практики от организации(строка).
     */
    @Length(max=255)
    public String getPracticeHeaderOutStr()
    {
        return _practiceHeaderOutStr;
    }

    /**
     * @param practiceHeaderOutStr Руководитель практики от организации(строка).
     */
    public void setPracticeHeaderOutStr(String practiceHeaderOutStr)
    {
        dirty(_practiceHeaderOutStr, practiceHeaderOutStr);
        _practiceHeaderOutStr = practiceHeaderOutStr;
    }

    /**
     * @return Руководитель практики от ОУ(строка).
     */
    @Length(max=255)
    public String getPracticeHeaderInnerStr()
    {
        return _practiceHeaderInnerStr;
    }

    /**
     * @param practiceHeaderInnerStr Руководитель практики от ОУ(строка).
     */
    public void setPracticeHeaderInnerStr(String practiceHeaderInnerStr)
    {
        dirty(_practiceHeaderInnerStr, practiceHeaderInnerStr);
        _practiceHeaderInnerStr = practiceHeaderInnerStr;
    }

    /**
     * @return Ответственный за соблюдение техники безопасности(печать).
     */
    @Length(max=255)
    public String getPreventAccidentsICStr()
    {
        return _preventAccidentsICStr;
    }

    /**
     * @param preventAccidentsICStr Ответственный за соблюдение техники безопасности(печать).
     */
    public void setPreventAccidentsICStr(String preventAccidentsICStr)
    {
        dirty(_preventAccidentsICStr, preventAccidentsICStr);
        _preventAccidentsICStr = preventAccidentsICStr;
    }

    /**
     * @return Ответственный за получение денежных средств(печать).
     */
    @Length(max=255)
    public String getResponsForRecieveCashStr()
    {
        return _responsForRecieveCashStr;
    }

    /**
     * @param responsForRecieveCashStr Ответственный за получение денежных средств(печать).
     */
    public void setResponsForRecieveCashStr(String responsForRecieveCashStr)
    {
        dirty(_responsForRecieveCashStr, responsForRecieveCashStr);
        _responsForRecieveCashStr = responsForRecieveCashStr;
    }

    /**
     * @return Текстовый параграф.
     */
    public String getTextParagraph()
    {
        return _textParagraph;
    }

    /**
     * @param textParagraph Текстовый параграф.
     */
    public void setTextParagraph(String textParagraph)
    {
        dirty(_textParagraph, textParagraph);
        _textParagraph = textParagraph;
    }

    /**
     * @return Курс прохождения практики. Свойство не может быть null.
     */
    @NotNull
    public Course getPracticeCourse()
    {
        return _practiceCourse;
    }

    /**
     * @param practiceCourse Курс прохождения практики. Свойство не может быть null.
     */
    public void setPracticeCourse(Course practiceCourse)
    {
        dirty(_practiceCourse, practiceCourse);
        _practiceCourse = practiceCourse;
    }

    /**
     * @return Ответственный за соблюдение техники безопасности.
     */
    public EmployeePost getPreventAccidentsIC()
    {
        return _preventAccidentsIC;
    }

    /**
     * @param preventAccidentsIC Ответственный за соблюдение техники безопасности.
     */
    public void setPreventAccidentsIC(EmployeePost preventAccidentsIC)
    {
        dirty(_preventAccidentsIC, preventAccidentsIC);
        _preventAccidentsIC = preventAccidentsIC;
    }

    /**
     * @return Ответственный за соблюдение техники безопасности(уч. степень).
     */
    public PersonAcademicDegree getPreventAccidentsICDegree()
    {
        return _preventAccidentsICDegree;
    }

    /**
     * @param preventAccidentsICDegree Ответственный за соблюдение техники безопасности(уч. степень).
     */
    public void setPreventAccidentsICDegree(PersonAcademicDegree preventAccidentsICDegree)
    {
        dirty(_preventAccidentsICDegree, preventAccidentsICDegree);
        _preventAccidentsICDegree = preventAccidentsICDegree;
    }

    /**
     * @return Ответственный за получение денежных средств.
     */
    public EmployeePost getResponsForRecieveCash()
    {
        return _responsForRecieveCash;
    }

    /**
     * @param responsForRecieveCash Ответственный за получение денежных средств.
     */
    public void setResponsForRecieveCash(EmployeePost responsForRecieveCash)
    {
        dirty(_responsForRecieveCash, responsForRecieveCash);
        _responsForRecieveCash = responsForRecieveCash;
    }

    /**
     * @return Ответственный за получение денежных средств(уч. степень).
     */
    public PersonAcademicDegree getResponsForRecieveCashDegree()
    {
        return _responsForRecieveCashDegree;
    }

    /**
     * @param responsForRecieveCashDegree Ответственный за получение денежных средств(уч. степень).
     */
    public void setResponsForRecieveCashDegree(PersonAcademicDegree responsForRecieveCashDegree)
    {
        dirty(_responsForRecieveCashDegree, responsForRecieveCashDegree);
        _responsForRecieveCashDegree = responsForRecieveCashDegree;
    }

    /**
     * @return Организация. Свойство не может быть null.
     */
    @NotNull
    public ExternalOrgUnit getPracticeExtOrgUnit()
    {
        return _practiceExtOrgUnit;
    }

    /**
     * @param practiceExtOrgUnit Организация. Свойство не может быть null.
     */
    public void setPracticeExtOrgUnit(ExternalOrgUnit practiceExtOrgUnit)
    {
        dirty(_practiceExtOrgUnit, practiceExtOrgUnit);
        _practiceExtOrgUnit = practiceExtOrgUnit;
    }

    /**
     * @return Руководитель практики от ОУ.
     */
    public EmployeePost getPracticeHeaderInner()
    {
        return _practiceHeaderInner;
    }

    /**
     * @param practiceHeaderInner Руководитель практики от ОУ.
     */
    public void setPracticeHeaderInner(EmployeePost practiceHeaderInner)
    {
        dirty(_practiceHeaderInner, practiceHeaderInner);
        _practiceHeaderInner = practiceHeaderInner;
    }

    /**
     * @return Руководитель практики от ОУ(уч. степень).
     */
    public PersonAcademicDegree getPracticeHeaderInnerDegree()
    {
        return _practiceHeaderInnerDegree;
    }

    /**
     * @param practiceHeaderInnerDegree Руководитель практики от ОУ(уч. степень).
     */
    public void setPracticeHeaderInnerDegree(PersonAcademicDegree practiceHeaderInnerDegree)
    {
        dirty(_practiceHeaderInnerDegree, practiceHeaderInnerDegree);
        _practiceHeaderInnerDegree = practiceHeaderInnerDegree;
    }

    /**
     * @return Договор на прохождение практики. Свойство не может быть null.
     */
    @NotNull
    public FefuPracticeContractWithExtOu getPracticeContract()
    {
        return _practiceContract;
    }

    /**
     * @param practiceContract Договор на прохождение практики. Свойство не может быть null.
     */
    public void setPracticeContract(FefuPracticeContractWithExtOu practiceContract)
    {
        dirty(_practiceContract, practiceContract);
        _practiceContract = practiceContract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SendPracticOutStuExtractGen)
        {
            setPracticeKind(((SendPracticOutStuExtract)another).getPracticeKind());
            setPracticeDurationStr(((SendPracticOutStuExtract)another).getPracticeDurationStr());
            setPracticeBeginDate(((SendPracticOutStuExtract)another).getPracticeBeginDate());
            setPracticeEndDate(((SendPracticOutStuExtract)another).getPracticeEndDate());
            setAttestationDate(((SendPracticOutStuExtract)another).getAttestationDate());
            setOutClassTime(((SendPracticOutStuExtract)another).isOutClassTime());
            setDoneEduPlan(((SendPracticOutStuExtract)another).isDoneEduPlan());
            setProvideFundsAccordingToEstimates(((SendPracticOutStuExtract)another).isProvideFundsAccordingToEstimates());
            setEstimateNum(((SendPracticOutStuExtract)another).getEstimateNum());
            setDonePractice(((SendPracticOutStuExtract)another).isDonePractice());
            setPracticeFactAddressStr(((SendPracticOutStuExtract)another).getPracticeFactAddressStr());
            setPracticeHeaderOutStr(((SendPracticOutStuExtract)another).getPracticeHeaderOutStr());
            setPracticeHeaderInnerStr(((SendPracticOutStuExtract)another).getPracticeHeaderInnerStr());
            setPreventAccidentsICStr(((SendPracticOutStuExtract)another).getPreventAccidentsICStr());
            setResponsForRecieveCashStr(((SendPracticOutStuExtract)another).getResponsForRecieveCashStr());
            setTextParagraph(((SendPracticOutStuExtract)another).getTextParagraph());
            setPracticeCourse(((SendPracticOutStuExtract)another).getPracticeCourse());
            setPreventAccidentsIC(((SendPracticOutStuExtract)another).getPreventAccidentsIC());
            setPreventAccidentsICDegree(((SendPracticOutStuExtract)another).getPreventAccidentsICDegree());
            setResponsForRecieveCash(((SendPracticOutStuExtract)another).getResponsForRecieveCash());
            setResponsForRecieveCashDegree(((SendPracticOutStuExtract)another).getResponsForRecieveCashDegree());
            setPracticeExtOrgUnit(((SendPracticOutStuExtract)another).getPracticeExtOrgUnit());
            setPracticeHeaderInner(((SendPracticOutStuExtract)another).getPracticeHeaderInner());
            setPracticeHeaderInnerDegree(((SendPracticOutStuExtract)another).getPracticeHeaderInnerDegree());
            setPracticeContract(((SendPracticOutStuExtract)another).getPracticeContract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SendPracticOutStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SendPracticOutStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new SendPracticOutStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "practiceKind":
                    return obj.getPracticeKind();
                case "practiceDurationStr":
                    return obj.getPracticeDurationStr();
                case "practiceBeginDate":
                    return obj.getPracticeBeginDate();
                case "practiceEndDate":
                    return obj.getPracticeEndDate();
                case "attestationDate":
                    return obj.getAttestationDate();
                case "outClassTime":
                    return obj.isOutClassTime();
                case "doneEduPlan":
                    return obj.isDoneEduPlan();
                case "provideFundsAccordingToEstimates":
                    return obj.isProvideFundsAccordingToEstimates();
                case "estimateNum":
                    return obj.getEstimateNum();
                case "donePractice":
                    return obj.isDonePractice();
                case "practiceFactAddressStr":
                    return obj.getPracticeFactAddressStr();
                case "practiceHeaderOutStr":
                    return obj.getPracticeHeaderOutStr();
                case "practiceHeaderInnerStr":
                    return obj.getPracticeHeaderInnerStr();
                case "preventAccidentsICStr":
                    return obj.getPreventAccidentsICStr();
                case "responsForRecieveCashStr":
                    return obj.getResponsForRecieveCashStr();
                case "textParagraph":
                    return obj.getTextParagraph();
                case "practiceCourse":
                    return obj.getPracticeCourse();
                case "preventAccidentsIC":
                    return obj.getPreventAccidentsIC();
                case "preventAccidentsICDegree":
                    return obj.getPreventAccidentsICDegree();
                case "responsForRecieveCash":
                    return obj.getResponsForRecieveCash();
                case "responsForRecieveCashDegree":
                    return obj.getResponsForRecieveCashDegree();
                case "practiceExtOrgUnit":
                    return obj.getPracticeExtOrgUnit();
                case "practiceHeaderInner":
                    return obj.getPracticeHeaderInner();
                case "practiceHeaderInnerDegree":
                    return obj.getPracticeHeaderInnerDegree();
                case "practiceContract":
                    return obj.getPracticeContract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "practiceKind":
                    obj.setPracticeKind((String) value);
                    return;
                case "practiceDurationStr":
                    obj.setPracticeDurationStr((String) value);
                    return;
                case "practiceBeginDate":
                    obj.setPracticeBeginDate((Date) value);
                    return;
                case "practiceEndDate":
                    obj.setPracticeEndDate((Date) value);
                    return;
                case "attestationDate":
                    obj.setAttestationDate((Date) value);
                    return;
                case "outClassTime":
                    obj.setOutClassTime((Boolean) value);
                    return;
                case "doneEduPlan":
                    obj.setDoneEduPlan((Boolean) value);
                    return;
                case "provideFundsAccordingToEstimates":
                    obj.setProvideFundsAccordingToEstimates((Boolean) value);
                    return;
                case "estimateNum":
                    obj.setEstimateNum((String) value);
                    return;
                case "donePractice":
                    obj.setDonePractice((Boolean) value);
                    return;
                case "practiceFactAddressStr":
                    obj.setPracticeFactAddressStr((String) value);
                    return;
                case "practiceHeaderOutStr":
                    obj.setPracticeHeaderOutStr((String) value);
                    return;
                case "practiceHeaderInnerStr":
                    obj.setPracticeHeaderInnerStr((String) value);
                    return;
                case "preventAccidentsICStr":
                    obj.setPreventAccidentsICStr((String) value);
                    return;
                case "responsForRecieveCashStr":
                    obj.setResponsForRecieveCashStr((String) value);
                    return;
                case "textParagraph":
                    obj.setTextParagraph((String) value);
                    return;
                case "practiceCourse":
                    obj.setPracticeCourse((Course) value);
                    return;
                case "preventAccidentsIC":
                    obj.setPreventAccidentsIC((EmployeePost) value);
                    return;
                case "preventAccidentsICDegree":
                    obj.setPreventAccidentsICDegree((PersonAcademicDegree) value);
                    return;
                case "responsForRecieveCash":
                    obj.setResponsForRecieveCash((EmployeePost) value);
                    return;
                case "responsForRecieveCashDegree":
                    obj.setResponsForRecieveCashDegree((PersonAcademicDegree) value);
                    return;
                case "practiceExtOrgUnit":
                    obj.setPracticeExtOrgUnit((ExternalOrgUnit) value);
                    return;
                case "practiceHeaderInner":
                    obj.setPracticeHeaderInner((EmployeePost) value);
                    return;
                case "practiceHeaderInnerDegree":
                    obj.setPracticeHeaderInnerDegree((PersonAcademicDegree) value);
                    return;
                case "practiceContract":
                    obj.setPracticeContract((FefuPracticeContractWithExtOu) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceKind":
                        return true;
                case "practiceDurationStr":
                        return true;
                case "practiceBeginDate":
                        return true;
                case "practiceEndDate":
                        return true;
                case "attestationDate":
                        return true;
                case "outClassTime":
                        return true;
                case "doneEduPlan":
                        return true;
                case "provideFundsAccordingToEstimates":
                        return true;
                case "estimateNum":
                        return true;
                case "donePractice":
                        return true;
                case "practiceFactAddressStr":
                        return true;
                case "practiceHeaderOutStr":
                        return true;
                case "practiceHeaderInnerStr":
                        return true;
                case "preventAccidentsICStr":
                        return true;
                case "responsForRecieveCashStr":
                        return true;
                case "textParagraph":
                        return true;
                case "practiceCourse":
                        return true;
                case "preventAccidentsIC":
                        return true;
                case "preventAccidentsICDegree":
                        return true;
                case "responsForRecieveCash":
                        return true;
                case "responsForRecieveCashDegree":
                        return true;
                case "practiceExtOrgUnit":
                        return true;
                case "practiceHeaderInner":
                        return true;
                case "practiceHeaderInnerDegree":
                        return true;
                case "practiceContract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceKind":
                    return true;
                case "practiceDurationStr":
                    return true;
                case "practiceBeginDate":
                    return true;
                case "practiceEndDate":
                    return true;
                case "attestationDate":
                    return true;
                case "outClassTime":
                    return true;
                case "doneEduPlan":
                    return true;
                case "provideFundsAccordingToEstimates":
                    return true;
                case "estimateNum":
                    return true;
                case "donePractice":
                    return true;
                case "practiceFactAddressStr":
                    return true;
                case "practiceHeaderOutStr":
                    return true;
                case "practiceHeaderInnerStr":
                    return true;
                case "preventAccidentsICStr":
                    return true;
                case "responsForRecieveCashStr":
                    return true;
                case "textParagraph":
                    return true;
                case "practiceCourse":
                    return true;
                case "preventAccidentsIC":
                    return true;
                case "preventAccidentsICDegree":
                    return true;
                case "responsForRecieveCash":
                    return true;
                case "responsForRecieveCashDegree":
                    return true;
                case "practiceExtOrgUnit":
                    return true;
                case "practiceHeaderInner":
                    return true;
                case "practiceHeaderInnerDegree":
                    return true;
                case "practiceContract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "practiceKind":
                    return String.class;
                case "practiceDurationStr":
                    return String.class;
                case "practiceBeginDate":
                    return Date.class;
                case "practiceEndDate":
                    return Date.class;
                case "attestationDate":
                    return Date.class;
                case "outClassTime":
                    return Boolean.class;
                case "doneEduPlan":
                    return Boolean.class;
                case "provideFundsAccordingToEstimates":
                    return Boolean.class;
                case "estimateNum":
                    return String.class;
                case "donePractice":
                    return Boolean.class;
                case "practiceFactAddressStr":
                    return String.class;
                case "practiceHeaderOutStr":
                    return String.class;
                case "practiceHeaderInnerStr":
                    return String.class;
                case "preventAccidentsICStr":
                    return String.class;
                case "responsForRecieveCashStr":
                    return String.class;
                case "textParagraph":
                    return String.class;
                case "practiceCourse":
                    return Course.class;
                case "preventAccidentsIC":
                    return EmployeePost.class;
                case "preventAccidentsICDegree":
                    return PersonAcademicDegree.class;
                case "responsForRecieveCash":
                    return EmployeePost.class;
                case "responsForRecieveCashDegree":
                    return PersonAcademicDegree.class;
                case "practiceExtOrgUnit":
                    return ExternalOrgUnit.class;
                case "practiceHeaderInner":
                    return EmployeePost.class;
                case "practiceHeaderInnerDegree":
                    return PersonAcademicDegree.class;
                case "practiceContract":
                    return FefuPracticeContractWithExtOu.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SendPracticOutStuExtract> _dslPath = new Path<SendPracticOutStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SendPracticOutStuExtract");
    }
            

    /**
     * @return Вид практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeKind()
     */
    public static PropertyPath<String> practiceKind()
    {
        return _dslPath.practiceKind();
    }

    /**
     * @return Объем(недель). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeDurationStr()
     */
    public static PropertyPath<String> practiceDurationStr()
    {
        return _dslPath.practiceDurationStr();
    }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeBeginDate()
     */
    public static PropertyPath<Date> practiceBeginDate()
    {
        return _dslPath.practiceBeginDate();
    }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeEndDate()
     */
    public static PropertyPath<Date> practiceEndDate()
    {
        return _dslPath.practiceEndDate();
    }

    /**
     * @return Дата аттестации по итогам практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getAttestationDate()
     */
    public static PropertyPath<Date> attestationDate()
    {
        return _dslPath.attestationDate();
    }

    /**
     * @return В свободное от аудиторных занятий время. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#isOutClassTime()
     */
    public static PropertyPath<Boolean> outClassTime()
    {
        return _dslPath.outClassTime();
    }

    /**
     * @return Полное выполнение учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#isDoneEduPlan()
     */
    public static PropertyPath<Boolean> doneEduPlan()
    {
        return _dslPath.doneEduPlan();
    }

    /**
     * @return Выделить средства согласно сметам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#isProvideFundsAccordingToEstimates()
     */
    public static PropertyPath<Boolean> provideFundsAccordingToEstimates()
    {
        return _dslPath.provideFundsAccordingToEstimates();
    }

    /**
     * @return Номер приложения(сметы расходов).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getEstimateNum()
     */
    public static PropertyPath<String> estimateNum()
    {
        return _dslPath.estimateNum();
    }

    /**
     * @return Cчитать прошедшими практику. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#isDonePractice()
     */
    public static PropertyPath<Boolean> donePractice()
    {
        return _dslPath.donePractice();
    }

    /**
     * @return Фактический адрес прохождения практики.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeFactAddressStr()
     */
    public static PropertyPath<String> practiceFactAddressStr()
    {
        return _dslPath.practiceFactAddressStr();
    }

    /**
     * @return Руководитель практики от организации(строка).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeHeaderOutStr()
     */
    public static PropertyPath<String> practiceHeaderOutStr()
    {
        return _dslPath.practiceHeaderOutStr();
    }

    /**
     * @return Руководитель практики от ОУ(строка).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeHeaderInnerStr()
     */
    public static PropertyPath<String> practiceHeaderInnerStr()
    {
        return _dslPath.practiceHeaderInnerStr();
    }

    /**
     * @return Ответственный за соблюдение техники безопасности(печать).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPreventAccidentsICStr()
     */
    public static PropertyPath<String> preventAccidentsICStr()
    {
        return _dslPath.preventAccidentsICStr();
    }

    /**
     * @return Ответственный за получение денежных средств(печать).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getResponsForRecieveCashStr()
     */
    public static PropertyPath<String> responsForRecieveCashStr()
    {
        return _dslPath.responsForRecieveCashStr();
    }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getTextParagraph()
     */
    public static PropertyPath<String> textParagraph()
    {
        return _dslPath.textParagraph();
    }

    /**
     * @return Курс прохождения практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeCourse()
     */
    public static Course.Path<Course> practiceCourse()
    {
        return _dslPath.practiceCourse();
    }

    /**
     * @return Ответственный за соблюдение техники безопасности.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPreventAccidentsIC()
     */
    public static EmployeePost.Path<EmployeePost> preventAccidentsIC()
    {
        return _dslPath.preventAccidentsIC();
    }

    /**
     * @return Ответственный за соблюдение техники безопасности(уч. степень).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPreventAccidentsICDegree()
     */
    public static PersonAcademicDegree.Path<PersonAcademicDegree> preventAccidentsICDegree()
    {
        return _dslPath.preventAccidentsICDegree();
    }

    /**
     * @return Ответственный за получение денежных средств.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getResponsForRecieveCash()
     */
    public static EmployeePost.Path<EmployeePost> responsForRecieveCash()
    {
        return _dslPath.responsForRecieveCash();
    }

    /**
     * @return Ответственный за получение денежных средств(уч. степень).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getResponsForRecieveCashDegree()
     */
    public static PersonAcademicDegree.Path<PersonAcademicDegree> responsForRecieveCashDegree()
    {
        return _dslPath.responsForRecieveCashDegree();
    }

    /**
     * @return Организация. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeExtOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> practiceExtOrgUnit()
    {
        return _dslPath.practiceExtOrgUnit();
    }

    /**
     * @return Руководитель практики от ОУ.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeHeaderInner()
     */
    public static EmployeePost.Path<EmployeePost> practiceHeaderInner()
    {
        return _dslPath.practiceHeaderInner();
    }

    /**
     * @return Руководитель практики от ОУ(уч. степень).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeHeaderInnerDegree()
     */
    public static PersonAcademicDegree.Path<PersonAcademicDegree> practiceHeaderInnerDegree()
    {
        return _dslPath.practiceHeaderInnerDegree();
    }

    /**
     * @return Договор на прохождение практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeContract()
     */
    public static FefuPracticeContractWithExtOu.Path<FefuPracticeContractWithExtOu> practiceContract()
    {
        return _dslPath.practiceContract();
    }

    public static class Path<E extends SendPracticOutStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<String> _practiceKind;
        private PropertyPath<String> _practiceDurationStr;
        private PropertyPath<Date> _practiceBeginDate;
        private PropertyPath<Date> _practiceEndDate;
        private PropertyPath<Date> _attestationDate;
        private PropertyPath<Boolean> _outClassTime;
        private PropertyPath<Boolean> _doneEduPlan;
        private PropertyPath<Boolean> _provideFundsAccordingToEstimates;
        private PropertyPath<String> _estimateNum;
        private PropertyPath<Boolean> _donePractice;
        private PropertyPath<String> _practiceFactAddressStr;
        private PropertyPath<String> _practiceHeaderOutStr;
        private PropertyPath<String> _practiceHeaderInnerStr;
        private PropertyPath<String> _preventAccidentsICStr;
        private PropertyPath<String> _responsForRecieveCashStr;
        private PropertyPath<String> _textParagraph;
        private Course.Path<Course> _practiceCourse;
        private EmployeePost.Path<EmployeePost> _preventAccidentsIC;
        private PersonAcademicDegree.Path<PersonAcademicDegree> _preventAccidentsICDegree;
        private EmployeePost.Path<EmployeePost> _responsForRecieveCash;
        private PersonAcademicDegree.Path<PersonAcademicDegree> _responsForRecieveCashDegree;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _practiceExtOrgUnit;
        private EmployeePost.Path<EmployeePost> _practiceHeaderInner;
        private PersonAcademicDegree.Path<PersonAcademicDegree> _practiceHeaderInnerDegree;
        private FefuPracticeContractWithExtOu.Path<FefuPracticeContractWithExtOu> _practiceContract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeKind()
     */
        public PropertyPath<String> practiceKind()
        {
            if(_practiceKind == null )
                _practiceKind = new PropertyPath<String>(SendPracticOutStuExtractGen.P_PRACTICE_KIND, this);
            return _practiceKind;
        }

    /**
     * @return Объем(недель). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeDurationStr()
     */
        public PropertyPath<String> practiceDurationStr()
        {
            if(_practiceDurationStr == null )
                _practiceDurationStr = new PropertyPath<String>(SendPracticOutStuExtractGen.P_PRACTICE_DURATION_STR, this);
            return _practiceDurationStr;
        }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeBeginDate()
     */
        public PropertyPath<Date> practiceBeginDate()
        {
            if(_practiceBeginDate == null )
                _practiceBeginDate = new PropertyPath<Date>(SendPracticOutStuExtractGen.P_PRACTICE_BEGIN_DATE, this);
            return _practiceBeginDate;
        }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeEndDate()
     */
        public PropertyPath<Date> practiceEndDate()
        {
            if(_practiceEndDate == null )
                _practiceEndDate = new PropertyPath<Date>(SendPracticOutStuExtractGen.P_PRACTICE_END_DATE, this);
            return _practiceEndDate;
        }

    /**
     * @return Дата аттестации по итогам практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getAttestationDate()
     */
        public PropertyPath<Date> attestationDate()
        {
            if(_attestationDate == null )
                _attestationDate = new PropertyPath<Date>(SendPracticOutStuExtractGen.P_ATTESTATION_DATE, this);
            return _attestationDate;
        }

    /**
     * @return В свободное от аудиторных занятий время. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#isOutClassTime()
     */
        public PropertyPath<Boolean> outClassTime()
        {
            if(_outClassTime == null )
                _outClassTime = new PropertyPath<Boolean>(SendPracticOutStuExtractGen.P_OUT_CLASS_TIME, this);
            return _outClassTime;
        }

    /**
     * @return Полное выполнение учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#isDoneEduPlan()
     */
        public PropertyPath<Boolean> doneEduPlan()
        {
            if(_doneEduPlan == null )
                _doneEduPlan = new PropertyPath<Boolean>(SendPracticOutStuExtractGen.P_DONE_EDU_PLAN, this);
            return _doneEduPlan;
        }

    /**
     * @return Выделить средства согласно сметам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#isProvideFundsAccordingToEstimates()
     */
        public PropertyPath<Boolean> provideFundsAccordingToEstimates()
        {
            if(_provideFundsAccordingToEstimates == null )
                _provideFundsAccordingToEstimates = new PropertyPath<Boolean>(SendPracticOutStuExtractGen.P_PROVIDE_FUNDS_ACCORDING_TO_ESTIMATES, this);
            return _provideFundsAccordingToEstimates;
        }

    /**
     * @return Номер приложения(сметы расходов).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getEstimateNum()
     */
        public PropertyPath<String> estimateNum()
        {
            if(_estimateNum == null )
                _estimateNum = new PropertyPath<String>(SendPracticOutStuExtractGen.P_ESTIMATE_NUM, this);
            return _estimateNum;
        }

    /**
     * @return Cчитать прошедшими практику. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#isDonePractice()
     */
        public PropertyPath<Boolean> donePractice()
        {
            if(_donePractice == null )
                _donePractice = new PropertyPath<Boolean>(SendPracticOutStuExtractGen.P_DONE_PRACTICE, this);
            return _donePractice;
        }

    /**
     * @return Фактический адрес прохождения практики.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeFactAddressStr()
     */
        public PropertyPath<String> practiceFactAddressStr()
        {
            if(_practiceFactAddressStr == null )
                _practiceFactAddressStr = new PropertyPath<String>(SendPracticOutStuExtractGen.P_PRACTICE_FACT_ADDRESS_STR, this);
            return _practiceFactAddressStr;
        }

    /**
     * @return Руководитель практики от организации(строка).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeHeaderOutStr()
     */
        public PropertyPath<String> practiceHeaderOutStr()
        {
            if(_practiceHeaderOutStr == null )
                _practiceHeaderOutStr = new PropertyPath<String>(SendPracticOutStuExtractGen.P_PRACTICE_HEADER_OUT_STR, this);
            return _practiceHeaderOutStr;
        }

    /**
     * @return Руководитель практики от ОУ(строка).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeHeaderInnerStr()
     */
        public PropertyPath<String> practiceHeaderInnerStr()
        {
            if(_practiceHeaderInnerStr == null )
                _practiceHeaderInnerStr = new PropertyPath<String>(SendPracticOutStuExtractGen.P_PRACTICE_HEADER_INNER_STR, this);
            return _practiceHeaderInnerStr;
        }

    /**
     * @return Ответственный за соблюдение техники безопасности(печать).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPreventAccidentsICStr()
     */
        public PropertyPath<String> preventAccidentsICStr()
        {
            if(_preventAccidentsICStr == null )
                _preventAccidentsICStr = new PropertyPath<String>(SendPracticOutStuExtractGen.P_PREVENT_ACCIDENTS_I_C_STR, this);
            return _preventAccidentsICStr;
        }

    /**
     * @return Ответственный за получение денежных средств(печать).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getResponsForRecieveCashStr()
     */
        public PropertyPath<String> responsForRecieveCashStr()
        {
            if(_responsForRecieveCashStr == null )
                _responsForRecieveCashStr = new PropertyPath<String>(SendPracticOutStuExtractGen.P_RESPONS_FOR_RECIEVE_CASH_STR, this);
            return _responsForRecieveCashStr;
        }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getTextParagraph()
     */
        public PropertyPath<String> textParagraph()
        {
            if(_textParagraph == null )
                _textParagraph = new PropertyPath<String>(SendPracticOutStuExtractGen.P_TEXT_PARAGRAPH, this);
            return _textParagraph;
        }

    /**
     * @return Курс прохождения практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeCourse()
     */
        public Course.Path<Course> practiceCourse()
        {
            if(_practiceCourse == null )
                _practiceCourse = new Course.Path<Course>(L_PRACTICE_COURSE, this);
            return _practiceCourse;
        }

    /**
     * @return Ответственный за соблюдение техники безопасности.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPreventAccidentsIC()
     */
        public EmployeePost.Path<EmployeePost> preventAccidentsIC()
        {
            if(_preventAccidentsIC == null )
                _preventAccidentsIC = new EmployeePost.Path<EmployeePost>(L_PREVENT_ACCIDENTS_I_C, this);
            return _preventAccidentsIC;
        }

    /**
     * @return Ответственный за соблюдение техники безопасности(уч. степень).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPreventAccidentsICDegree()
     */
        public PersonAcademicDegree.Path<PersonAcademicDegree> preventAccidentsICDegree()
        {
            if(_preventAccidentsICDegree == null )
                _preventAccidentsICDegree = new PersonAcademicDegree.Path<PersonAcademicDegree>(L_PREVENT_ACCIDENTS_I_C_DEGREE, this);
            return _preventAccidentsICDegree;
        }

    /**
     * @return Ответственный за получение денежных средств.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getResponsForRecieveCash()
     */
        public EmployeePost.Path<EmployeePost> responsForRecieveCash()
        {
            if(_responsForRecieveCash == null )
                _responsForRecieveCash = new EmployeePost.Path<EmployeePost>(L_RESPONS_FOR_RECIEVE_CASH, this);
            return _responsForRecieveCash;
        }

    /**
     * @return Ответственный за получение денежных средств(уч. степень).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getResponsForRecieveCashDegree()
     */
        public PersonAcademicDegree.Path<PersonAcademicDegree> responsForRecieveCashDegree()
        {
            if(_responsForRecieveCashDegree == null )
                _responsForRecieveCashDegree = new PersonAcademicDegree.Path<PersonAcademicDegree>(L_RESPONS_FOR_RECIEVE_CASH_DEGREE, this);
            return _responsForRecieveCashDegree;
        }

    /**
     * @return Организация. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeExtOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> practiceExtOrgUnit()
        {
            if(_practiceExtOrgUnit == null )
                _practiceExtOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_PRACTICE_EXT_ORG_UNIT, this);
            return _practiceExtOrgUnit;
        }

    /**
     * @return Руководитель практики от ОУ.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeHeaderInner()
     */
        public EmployeePost.Path<EmployeePost> practiceHeaderInner()
        {
            if(_practiceHeaderInner == null )
                _practiceHeaderInner = new EmployeePost.Path<EmployeePost>(L_PRACTICE_HEADER_INNER, this);
            return _practiceHeaderInner;
        }

    /**
     * @return Руководитель практики от ОУ(уч. степень).
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeHeaderInnerDegree()
     */
        public PersonAcademicDegree.Path<PersonAcademicDegree> practiceHeaderInnerDegree()
        {
            if(_practiceHeaderInnerDegree == null )
                _practiceHeaderInnerDegree = new PersonAcademicDegree.Path<PersonAcademicDegree>(L_PRACTICE_HEADER_INNER_DEGREE, this);
            return _practiceHeaderInnerDegree;
        }

    /**
     * @return Договор на прохождение практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SendPracticOutStuExtract#getPracticeContract()
     */
        public FefuPracticeContractWithExtOu.Path<FefuPracticeContractWithExtOu> practiceContract()
        {
            if(_practiceContract == null )
                _practiceContract = new FefuPracticeContractWithExtOu.Path<FefuPracticeContractWithExtOu>(L_PRACTICE_CONTRACT, this);
            return _practiceContract;
        }

        public Class getEntityClass()
        {
            return SendPracticOutStuExtract.class;
        }

        public String getEntityName()
        {
            return "sendPracticOutStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
