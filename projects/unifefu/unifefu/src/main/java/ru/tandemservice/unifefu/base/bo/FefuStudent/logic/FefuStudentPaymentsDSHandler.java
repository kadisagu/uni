/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.base.vo.FefuStudentPaymentsVO;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 8/27/13
 */
public class FefuStudentPaymentsDSHandler extends DefaultSearchDataSourceHandler
{
    public FefuStudentPaymentsDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long studentId = context.get("studentId");
        Date paymentsDateFrom = null != context.get("paymentsDateFrom") ? getDayStartDate((Date) context.get("paymentsDateFrom")): null;
        Date paymentsDateTo = null != context.get("paymentsDateTo") ? getDayEndDate((Date) context.get("paymentsDateTo")): null;
        List<Long> extractTypesId = context.get("extractTypes");
        String orderNum = context.get("orderNum");
        boolean allPayments = context.getBoolean("allPayments", false);


        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuStudentPayment.class, "p");
        if(null != studentId)
            builder.where(eq(property("p", FefuStudentPayment.extract().entity().id()), value(studentId)));

        if(null != extractTypesId && !extractTypesId.isEmpty())
            builder.where(in(property("p", FefuStudentPayment.extract().type().id()), extractTypesId));

        if(!StringUtils.isEmpty(orderNum))
            builder.where(like(DQLFunctions.upper(property("p", FefuStudentPayment.extract().paragraph().order().number())), value(CoreStringUtils.escapeLike(orderNum, true))));

        if(null != paymentsDateFrom && null != paymentsDateTo)
        {
            builder.where(or(
                    and(
                            isNotNull(property("p", FefuStudentPayment.startDate())),
                            isNotNull(property("p", FefuStudentPayment.stopDate())),
                            le(DQLFunctions.cast(property("p", FefuStudentPayment.startDate()), PropertyType.DATE), valueDate(paymentsDateTo)),
                            ge(DQLFunctions.cast(property("p", FefuStudentPayment.stopDate()), PropertyType.DATE), valueDate(paymentsDateFrom))
                    ),
                    and(
                            isNotNull(property("p", FefuStudentPayment.startDate())),
                            isNull(property("p", FefuStudentPayment.stopDate())),
                            le(DQLFunctions.cast(property("p", FefuStudentPayment.startDate()), PropertyType.DATE), valueDate(paymentsDateTo))
                    ),
                    and(
                            isNull(property("p", FefuStudentPayment.startDate())),
                            isNotNull(property("p", FefuStudentPayment.stopDate())),
                            ge(DQLFunctions.cast(property("p", FefuStudentPayment.stopDate()), PropertyType.DATE), valueDate(paymentsDateFrom))
                    ),
                    and(
                            isNull(property("p", FefuStudentPayment.startDate())),
                            isNull(property("p", FefuStudentPayment.stopDate()))
                    )
            ));
        }
        else if(null == paymentsDateFrom && null != paymentsDateTo)
        {
            builder.where(or(
                    and(
                            isNotNull(property("p", FefuStudentPayment.startDate())),
                            le(DQLFunctions.cast(property("p", FefuStudentPayment.startDate()), PropertyType.DATE), valueDate(paymentsDateTo))
                    ),
                    isNull(property("p", FefuStudentPayment.startDate()))
            ));
        }
        else if(null != paymentsDateFrom && null == paymentsDateTo)
        {
            builder.where(or(
                    and(
                            isNotNull(property("p", FefuStudentPayment.stopDate())),
                            ge(DQLFunctions.cast(property("p", FefuStudentPayment.stopDate()), PropertyType.DATE), valueDate(paymentsDateFrom))
                    ),
                    isNull(property("p", FefuStudentPayment.stopDate()))
            ));
        }
        builder.order(property("p", FefuStudentPayment.extract().paragraph().order().commitDate()));

        List<FefuStudentPayment> payments = createStatement(builder).list();

        List<FefuStudentPaymentsVO> paymentsVOs = Lists.newArrayList();
        Map<Long, FefuStudentPaymentsVO> onlyPaymentsVOMap = Maps.newHashMap();
        Map<String, FefuStudentPaymentsVO> onlyPaymentsVONumMap = Maps.newHashMap();
        for(FefuStudentPayment payment : payments)
        {
            FefuStudentPaymentsVO paymentsVO = new FefuStudentPaymentsVO(payment);
            paymentsVOs.add(paymentsVO);
            if(!payment.isContinueOrder() && !payment.isStopOrPauseOrder())
            {
                onlyPaymentsVOMap.put(paymentsVO.getPayment().getExtract().getId(), paymentsVO);
                onlyPaymentsVONumMap.put(paymentsVO.getPayment().getExtract().getParagraph().getOrder().getNumber(), paymentsVO);
            }
        }


        DQLSelectBuilder stopPauseOrContinuePaymentsBuilder = new DQLSelectBuilder().fromEntity(FefuStudentPayment.class, "spoc");
        stopPauseOrContinuePaymentsBuilder.where(eq(property("spoc", FefuStudentPayment.stopOrPauseOrder()), value(Boolean.TRUE)));
        if(null != studentId)
            stopPauseOrContinuePaymentsBuilder.where(eq(property("spoc", FefuStudentPayment.extract().entity().id()), value(studentId)));
        List<FefuStudentPayment> stopPauseOrContinuePayments = createStatement(stopPauseOrContinuePaymentsBuilder).list();

        for(FefuStudentPayment payment : stopPauseOrContinuePayments)
        {
            if(null != payment.getInfluencedExtract())
            {
                FefuStudentPaymentsVO onlyPaymentsVO = onlyPaymentsVOMap.get(payment.getInfluencedExtract().getId());
                if(null != onlyPaymentsVO)
                {
                    if(payment.isContinueOrder())
                    {

                        onlyPaymentsVO.setContinueDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(payment.getStartDate()) +
                                " (приказ №" + payment.getExtract().getParagraph().getOrder().getNumber() + " от " +
                                DateFormatter.DEFAULT_DATE_FORMATTER.format(payment.getExtract().getParagraph().getOrder().getCommitDate()) + ")");
                    }
                    else if(payment.isStopOrPauseOrder())
                    {
                        StringBuilder dateStr = new StringBuilder();

                        onlyPaymentsVO.setPauseDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(payment.getStartDate()) +
                                " (приказ №" + payment.getExtract().getParagraph().getOrder().getNumber() + " от " +
                                DateFormatter.DEFAULT_DATE_FORMATTER.format(payment.getExtract().getParagraph().getOrder().getCommitDate()) + ")");
                    }
                }
            }
            else if(!StringUtils.isEmpty(payment.getInfluencedOrderNum()))
            {
                FefuStudentPaymentsVO onlyPaymentsVO = onlyPaymentsVONumMap.get(payment.getInfluencedOrderNum());
                if(null != onlyPaymentsVO)
                {
                    if(payment.isContinueOrder())
                    {

                        onlyPaymentsVO.setContinueDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(payment.getStartDate()) +
                                " (приказ №" + payment.getExtract().getParagraph().getOrder().getNumber() + " от " +
                                payment.getExtract().getParagraph().getOrder().getCommitDate() + ")");
                    }
                    else if(payment.isStopOrPauseOrder())
                    {
                        StringBuilder dateStr = new StringBuilder();

                        onlyPaymentsVO.setPauseDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(payment.getStartDate()) +
                                " (приказ №" + payment.getExtract().getParagraph().getOrder().getNumber() + " от " +
                                payment.getExtract().getParagraph().getOrder().getCommitDate() + ")");
                    }
                }
            }
        }

        return ListOutputBuilder.get(input, paymentsVOs).pageable(true).build();
    }

    private Date getDayStartDate(Date paymentsDateFrom)
    {
        return new DateTime(paymentsDateFrom).withTime(0, 0, 0, 0).toDate();
    }

    private Date getDayEndDate(Date paymentsDateTo)
    {
        return new DateTime(paymentsDateTo).withTime(0, 0, 0, 0).toDate();
    }

}
