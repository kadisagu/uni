package ru.tandemservice.unifefu.component.listextract.fefu13.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public class FefuAdditionalAcademGrantExtractWrapper implements Comparable<FefuAdditionalAcademGrantExtractWrapper>
{
    private final Person _person;
    private final Double _grantSize;
	private final Group _group;

    public FefuAdditionalAcademGrantExtractWrapper(Person person, Double grantSize, Group group)
    {
        _person = person;
        _grantSize = grantSize;
	    _group = group;
    }

    public Person getPerson()
    {
        return _person;
    }

    public Double getGrantSize()
    {
        return _grantSize;
    }

	public Group getGroup()
	{
		return _group;
	}

	@Override
    public int compareTo(FefuAdditionalAcademGrantExtractWrapper o)
    {
        return Person.FULL_FIO_AND_ID_COMPARATOR.compare(_person, o.getPerson());
    }
}