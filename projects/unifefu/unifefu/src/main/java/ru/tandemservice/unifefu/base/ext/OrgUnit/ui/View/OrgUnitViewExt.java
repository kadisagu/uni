/* $Id$ */
package ru.tandemservice.unifefu.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;

/**
 * @author Alexey Lopatin
 * @since 24.09.2013
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + OrgUnitViewExtUI.class.getSimpleName();
    public static final String ORG_UNIT_EDIT_SIGNATORY = "orgUnitEditSignatory";

    @Autowired
    private OrgUnitView _orgUnitView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_orgUnitView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, OrgUnitViewExtUI.class))
                .create();
    }

    @Bean
    public ButtonListExtension blockListExtension()
    {
        return buttonListExtensionBuilder(_orgUnitView.orgUnitTabButtonListExtPoint())
                .addButton(submitButton(ORG_UNIT_EDIT_SIGNATORY, ADDON_NAME + ":onClickOrgUnitSignatoryEdit")
                                   .parameters("ui:orgUnit.id")
                                   .permissionKey("ui:secModel.unifefuOrgUnitSignatoryEdit")
                                   .visible("ui:notArchival"))
                .create();
    }
}
