/* $Id$ */
package ru.tandemservice.unifefu.dao;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.dao.OnlineEntrantRegistrationDAO;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;
import ru.tandemservice.uniec.ws.UserDirectionData;
import ru.tandemservice.unifefu.entity.OnlineEnrDirectionFefuExt;
import ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt;
import ru.tandemservice.unifefu.entity.catalog.FefuEntrantOlympiads;
import ru.tandemservice.unifefu.ws.FefuOnlineEntrantData;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 28.05.2013
 */
public class FefuOnlineEntrantRegistrationDAO extends OnlineEntrantRegistrationDAO implements IFefuOnlineEntrantRegistrationDAO
{
    @Override
    public OnlineEntrantFefuExt createOrUpdateFefuOnlineEntrant(long userId, FefuOnlineEntrantData entrantData)
    {
        OnlineEntrant onlineEntrant = createOrUpdateOnlineEntrant(userId, entrantData);

        OnlineEntrantFefuExt onlineEntrantFefuExt = getByNaturalId(new OnlineEntrantFefuExt.NaturalId(onlineEntrant));
        if (onlineEntrantFefuExt == null)
        {
            onlineEntrantFefuExt = new OnlineEntrantFefuExt();
            onlineEntrantFefuExt.setOnlineEntrant(onlineEntrant);
        }

        onlineEntrantFefuExt.setEntrantOlympiad(safeGet(FefuEntrantOlympiads.class, FefuEntrantOlympiads.id().s(), entrantData.fefuEntrantOlympiadId));
        onlineEntrantFefuExt.setAgreeWithPrivacyNotice(entrantData.agreeWithPrivacyNotice);

        saveOrUpdate(onlineEntrantFefuExt);

        return onlineEntrantFefuExt;
    }

    private void fillFefuEntrantData(FefuOnlineEntrantData data, OnlineEntrantFefuExt onlineEntrantFefuExt)
    {
        data.fefuEntrantOlympiadId = (Long) onlineEntrantFefuExt.getProperty(OnlineEntrantFefuExt.entrantOlympiad().id().s());
        data.agreeWithPrivacyNotice = onlineEntrantFefuExt.getAgreeWithPrivacyNotice();
    }

    @Override
    public FefuOnlineEntrantData getFefuOnlineEntrantData(long userId)
    {
        OnlineEntrantFefuExt onlineEntrantFefuExt = getOnlineEntrantFefuExt(userId);
        if (onlineEntrantFefuExt == null) return null;

        FefuOnlineEntrantData result = new FefuOnlineEntrantData();

        // from uniec
        fillEntrantData(result, onlineEntrantFefuExt.getOnlineEntrant());

        // from unifefu
        fillFefuEntrantData(result, onlineEntrantFefuExt);

        return result;
    }

    @Override
    public OnlineEntrantFefuExt getOnlineEntrantFefuExt(long userId)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);
        if (onlineEntrant != null)
        {
            OnlineEntrantFefuExt onlineEntrantFefuExt = getByNaturalId(new OnlineEntrantFefuExt.NaturalId(onlineEntrant));
            if (onlineEntrantFefuExt == null)
            {
                onlineEntrantFefuExt = new OnlineEntrantFefuExt();
                onlineEntrantFefuExt.setOnlineEntrant(onlineEntrant);
            }
            return onlineEntrantFefuExt;
        }
        return null;
    }

    @Override
    public void updateUserDirections(long userId, UserDirectionData[] userDirections)
    {
        OnlineEntrant onlineEntrant = getOnlineEntrant(userId);

        // удаляем все старые
        getList(OnlineRequestedEnrollmentDirection.class, OnlineRequestedEnrollmentDirection.entrant().s(), onlineEntrant)
                .forEach(this::delete);

        // синхронизируем сессию с базой
        getSession().flush();

        updateEnrollmentCampaign(onlineEntrant, userDirections);

        int priority = 0;
        // сохраняем новые
        for (UserDirectionData userDirectionData : userDirections)
        {
            OnlineRequestedEnrollmentDirection direction = new OnlineRequestedEnrollmentDirection();
            direction.setEntrant(onlineEntrant);
            direction.setEnrollmentDirection(get(EnrollmentDirection.class, userDirectionData.enrollmentDirectionId));
            direction.setCompensationType(get(CompensationType.class, userDirectionData.compensationTypeId));
            direction.setStudentCategory(get(StudentCategory.class, userDirectionData.studentCategoryId));
            direction.setCompetitionKind(get(CompetitionKind.class, userDirectionData.competitionKindId));
            direction.setTargetAdmission(userDirectionData.targetAdmission);
            direction.setProfileWorkExperienceYears(userDirectionData.profileWorkExperienceYears);
            direction.setProfileWorkExperienceMonths(userDirectionData.profileWorkExperienceMonths);
            direction.setProfileWorkExperienceDays(userDirectionData.profileWorkExperienceDays);
            save(direction);

            // Сохраняем приоритет
            OnlineEnrDirectionFefuExt fefuDirectionExt = new OnlineEnrDirectionFefuExt();
            fefuDirectionExt.setOnlineEnrDirection(direction);
            fefuDirectionExt.setPriority(priority++);
            save(fefuDirectionExt);
        }
    }

    @Override
    public List<OnlineRequestedEnrollmentDirection> getRequestedEnrollmentDirections(OnlineEntrant onlineEntrant)
    {
        // Достаем выбранные напрвления подготовки, отсортированные по приоритетам, которые проставил онлайн-абитуриент
        return new DQLSelectBuilder().fromEntity(OnlineRequestedEnrollmentDirection.class, "e")
                .joinEntity("e", DQLJoinType.left, OnlineEnrDirectionFefuExt.class, "f",
                            DQLExpressions.eq(OnlineRequestedEnrollmentDirection.id().fromAlias("e"),
                                              OnlineEnrDirectionFefuExt.onlineEnrDirection().id().fromAlias("f")))
                .column("e")
                .where(DQLExpressions.eq(DQLExpressions.property(OnlineRequestedEnrollmentDirection.entrant().fromAlias("e")), DQLExpressions.value(onlineEntrant)))
                .where(DQLExpressions.or(
                        DQLExpressions.eq(DQLExpressions.property(OnlineRequestedEnrollmentDirection.enrollmentDirection().budget().fromAlias("e")), DQLExpressions.value(Boolean.TRUE)),
                        DQLExpressions.eq(DQLExpressions.property(OnlineRequestedEnrollmentDirection.enrollmentDirection().contract().fromAlias("e")), DQLExpressions.value(Boolean.TRUE))
                ))
                .order(DQLExpressions.property(OnlineEnrDirectionFefuExt.priority().fromAlias("f")), OrderDirection.asc)
                .createStatement(getSession()).list();
    }
}