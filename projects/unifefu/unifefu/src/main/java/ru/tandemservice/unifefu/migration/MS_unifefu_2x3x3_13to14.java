package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x3x3_13to14 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEpvCheckState
        if (!tool.tableExists("fefuepvcheckstate_t"))
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefuepvcheckstate_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("version_id", DBType.LONG).setNullable(false),
				new DBColumn("checkedbyumu_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("checkedbyumudate_p", DBType.TIMESTAMP),
				new DBColumn("checkedbycrk_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("checkedbycrkdate_p", DBType.TIMESTAMP),
				new DBColumn("checkedbyoop_p", DBType.BOOLEAN).setNullable(false),
				new DBColumn("checkedbyoopdate_p", DBType.TIMESTAMP)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuEpvCheckState");
		}
    }
}