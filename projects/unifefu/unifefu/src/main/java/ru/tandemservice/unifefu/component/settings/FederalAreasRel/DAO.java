package ru.tandemservice.unifefu.component.settings.FederalAreasRel;


import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLInsertValuesBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unifefu.entity.FefuFederalAreasRel;
import ru.tandemservice.unifefu.entity.catalog.FefuFederalDistrict;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author amakarova
 * @since 25.10.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setFederalDistrictList(getCatalogItemList(FefuFederalDistrict.class));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuFederalAreasRel.class, "e")
                .fetchPath(DQLJoinType.inner, FefuFederalAreasRel.addressItem().fromAlias("e"), "a")
                .fetchPath(DQLJoinType.inner, FefuFederalAreasRel.fefuFederalDistrict().fromAlias("e"), "d")
                .order(property("a", AddressItem.P_INHERITED_REGION_CODE));

        List<FefuFederalAreasRel> list = createStatement(builder).list();
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void fillData(Document doc)
    {
        // Парсим xml
        Multimap<String, String> srcMap = HashMultimap.create();
        NodeList nodeList = doc.getElementsByTagName("federal");
        for (int i = 0; i < nodeList.getLength(); i++)
        {
            Element node = (Element) nodeList.item(i);
            String districtCode = node.getAttribute("code");
            NodeList childList = node.getElementsByTagName("area");
            for (int j = 0; j < childList.getLength(); j++)
            {
                srcMap.put(districtCode, ((Element) childList.item(j)).getAttribute("code"));
            }
        }

        // Получаем федеральные округа из базы
        List<FefuFederalDistrict> fefuFederalDistrictList = getList(FefuFederalDistrict.class);
        Map<String, FefuFederalDistrict> districtMap = new HashMap<>(fefuFederalDistrictList.size());
        for (FefuFederalDistrict district : fefuFederalDistrictList)
        {
            districtMap.put(district.getCode(), district);
        }

        // Получаем административно-территориальные единицы
        List<AddressItem> addressItemList = new DQLSelectBuilder().fromEntity(AddressItem.class, "a")
                .where(isNull(property("a", AddressItem.L_PARENT)))
                .where(isNotNull(property("a", AddressItem.P_INHERITED_REGION_CODE)))
                .createStatement(getSession()).list();

        // Поскольку с одним кодом региона может оказаться несколько адм-терр. единиц (почему-то в базе есть дубли типа Ленинград/Санк-Петербург), складывам их в мультимапу
        ListMultimap<String, AddressItem> addressItemMap = ArrayListMultimap.create();
        for (AddressItem item : addressItemList)
        {
            addressItemMap.put(item.getInheritedRegionCode(), item);
        }

        // Создаем связи между федеральными округами и административно-территориальными единицами
        DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(FefuFederalAreasRel.class);
        for (Map.Entry<String, String> itemEntry : srcMap.entries())
        {
            String districtCode = itemEntry.getKey();
            String areaCode = itemEntry.getValue();

            FefuFederalDistrict district = districtMap.get(districtCode);
            if (district == null)
                throw new ApplicationException("Федеральный округ с кодом " + districtCode + " не найден в базе данных.");

            List<AddressItem> itemList = addressItemMap.get(areaCode);
            if (itemList.isEmpty())
                throw new ApplicationException("Административно-территориальная единица с кодом " + areaCode + " не найдена в базе данных. Возможно, необходимо импортировать свежую версию КЛАДРа.");

            for (AddressItem item : itemList)
            {
                insertBuilder.value(FefuFederalAreasRel.L_ADDRESS_ITEM, item);
                insertBuilder.value(FefuFederalAreasRel.L_FEFU_FEDERAL_DISTRICT, district);
                insertBuilder.addBatch();
            }
        }
        createStatement(insertBuilder).execute();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void clearData()
    {
        createStatement(new DQLDeleteBuilder(FefuFederalAreasRel.class)).execute();
    }

    @Override
    public void updateData(Document doc)
    {
        clearData();
        fillData(doc);
    }

}