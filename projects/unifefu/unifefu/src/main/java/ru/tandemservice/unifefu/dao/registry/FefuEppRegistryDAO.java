/* $Id$ */
package ru.tandemservice.unifefu.dao.registry;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.dao.registry.EppRegistryDAO;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexey Lopatin
 * @since 17.11.2014
 */
public class FefuEppRegistryDAO extends EppRegistryDAO implements IFefuEppRegistryDAO
{

    @Override
    public Map<Long, List<Long>> getRegistryElement2WorkPlanRelMap(Collection<Long> disciplineIds)
    {
        Map<Long, List<Long>> resultMap = Maps.newHashMap();
        if (disciplineIds.isEmpty()) return resultMap;

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, "e").column(property("e"))
                .where(in(property("e", EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id()), disciplineIds))
                .order(property("e", EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id()))
                .order(property("e", EppWorkPlanRegistryElementRow.workPlan().id()));

        List<EppWorkPlanRegistryElementRow> elementRowList = dql.createStatement(getSession()).list();

        for (EppWorkPlanRegistryElementRow elementRow : elementRowList)
        {
            EppRegistryElement element = elementRow.getRegistryElement();
            if (null != element)
            {
                Long id = element.getId();
                List<Long> rowList = resultMap.get(id);
                if (null == rowList)
                {
                    rowList = Lists.newArrayList();
                    resultMap.put(id, rowList);
                }
                resultMap.get(id).add(elementRow.getId());
            }
        }
        return resultMap;
    }

    @Override
    public Map<Long, List<Long>> getRegistryElement2EduPlanVersionRelMap(Collection<Long> disciplineIds)
    {
        Map<Long, List<Long>> resultMap = Maps.newHashMap();
        if (disciplineIds.isEmpty()) return resultMap;

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "e").column(property("e"))
                .where(in(property("e", EppEpvRegistryRow.registryElement().id()), disciplineIds))
                .order(property("e", EppEpvRegistryRow.registryElement().id()))
                .order(property("e", EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().number()))
                .order(property("e", EppEpvRegistryRow.owner().eduPlanVersion().number()));

        List<EppEpvRegistryRow> elementRowList = dql.createStatement(getSession()).list();

        for (EppEpvRegistryRow registryRow : elementRowList)
        {
            EppRegistryElement element = registryRow.getRegistryElement();
            if (null != element)
            {
                Long id = element.getId();
                List<Long> rowList = resultMap.get(id);
                if (null == rowList)
                {
                    rowList = Lists.newArrayList();
                    resultMap.put(id, rowList);
                }
                resultMap.get(id).add(registryRow.getId());
            }
        }
        return resultMap;
    }
}
