/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.row.FefuCompetenceAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 13.05.2013
 */
public interface IDAO extends IUniDao<Model>
{
    /**
     * Привязывает к строке компетенции, которые были привязаны к ее дисциплине, но не было у самой строки
     * @param model модель
     */
    void updateCompetence(Model model);

    void addRowCompetence2EduStandartRel(Model model);

    void deleteRowCompetence2EduStandartRel(Model model, Long relId);

    void changeDispersion(Model model);
}