/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu14;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.component.listextract.fefu14.utils.FefuTransfAcceleratedTimeParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unifefu.entity.gen.StudentFefuExtGen;

import java.util.*;

/**
 * @author Ekaterina Zvereva
 * @since 13.11.2014
 */
public class FefuTransfAcceleratedTimeStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
    {
        @Override
        public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
        {
            RtfDocument document = new RtfReader().read(template);

            List<FefuTransfAcceleratedTimeStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

            RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
            List<FefuTransfAcceleratedTimeParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts);
            injectModifier.modify(document);

            RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
            tableModifier.modify(document);

            // Вставляем список параграфов
            injectParagraphs(document, preparedParagraphsStructure);

            RtfUtil.optimizeFontAndColorTable(document);

            return document;
        }

        protected List<FefuTransfAcceleratedTimeParagraphWrapper> prepareParagraphsStructure(List<FefuTransfAcceleratedTimeStuListExtract> extracts)
        {
            int ind;
            List<FefuTransfAcceleratedTimeParagraphWrapper> paragraphWrapperList = new ArrayList<>();
            for (FefuTransfAcceleratedTimeStuListExtract extract : extracts)
            {
                Student student = extract.getEntity();

                FefuTransfAcceleratedTimeParagraphWrapper paragraphWrapper = new FefuTransfAcceleratedTimeParagraphWrapper(
                        student.getCourse(),
                        student.getStudentCategory(),
                        extract.getGroupOld(),
                        extract.getGroupNew(),
                        student.getCompensationType(),
                        student.getEducationOrgUnit(),
                        extract.getEducationOrgUnitNew(),
                        student.getEducationOrgUnit().getDevelopForm(),
                        extract);

                ind = paragraphWrapperList.indexOf(paragraphWrapper);
                if (ind == -1)
                    paragraphWrapperList.add(paragraphWrapper);
                else
                    paragraphWrapper = paragraphWrapperList.get(ind);

                paragraphWrapper.getPersonList().add(student);
                paragraphWrapper.setPlannedDate(extract.getPlannedDate());
                paragraphWrapper.setSeason(extract.getSeason());
            }
            return paragraphWrapperList;
        }

        protected void injectParagraphs(RtfDocument document, List<FefuTransfAcceleratedTimeParagraphWrapper> paragraphWrappers)
        {
            // 1. ищем ключевое слово
            final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

            // 2. Если нашли, то вместо него вставляем все параграфы
            if (rtfSearchResult.isFound())
            {
                int parNumber = 0;
                Collections.sort(paragraphWrappers);
                List<IRtfElement> parList = new ArrayList<>();
                for (FefuTransfAcceleratedTimeParagraphWrapper paragraphWrapper : paragraphWrappers)
                {
                    // Получаем шаблон параграфа
                    byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_TRANSFER_ACCELERATED_TIME_LIST_EXTRACT), 1);
                    RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                    // Вносим необходимые метки
                    RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());

                    CompensationType compensationTypeOld = paragraphWrapper.getCompensationTypeOld();

                    EducationOrgUnit educationOrgUnitOld = paragraphWrapper.getEducationOrgUnitOld();

                    //EducationLevels educationLevelOld = educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel();

                    paragraphInjectModifier.put("parNumber", paragraphWrappers.size() == 1 ? "" : String.valueOf(++parNumber) + ". ");
                    paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());
                    CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, compensationTypeOld, "", false);

                    CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());


                    CommonExtractPrint.initOrgUnit(paragraphInjectModifier, educationOrgUnitOld, "formativeOrgUnit", "");
                    CommonExtractPrint.initDevelopForm(paragraphInjectModifier, paragraphWrapper.getDevelopForm(), "");


                    // developTech NOT USED
                    //if (UniDefines.DEVELOP_TECH_REMOTE.equals(paragraphWrapper.getEducationOrgUnitOld().getDevelopTech().getCode()))
                    {
//                        fefuShortFastExtendedOptionalText.append(CommonExtractPrint.WITH_REMOTE_EDU_TECH);
                    }

                    CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "intoFefuGroupOld", paragraphWrapper.getGroupOld(), educationOrgUnitOld.getDevelopForm(), " группы ");
                    CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "intoFefuGroupNew", paragraphWrapper.getGroupNew(), paragraphWrapper.getEducationOrgUnitNew().getDevelopForm(), " в группу ");


                    // developTech NOT USED
                    if (UniDefines.DEVELOP_TECH_REMOTE.equals(paragraphWrapper.getEducationOrgUnitOld().getDevelopTech().getCode()))
                        paragraphInjectModifier.put("developTech",CommonExtractPrint.WITH_REMOTE_EDU_TECH);
                    else
                        paragraphInjectModifier.put("developTech","");

                    //Сезон выпуска и дата выпуска, период обучения
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(paragraphWrapper.getPlannedDate());

                    List<String> seasons_a = Arrays.asList((ApplicationRuntime.getProperty("seasons_A")).split(";"));
                    List<String> seasons_i = Arrays.asList((ApplicationRuntime.getProperty("seasons_I")).split(";"));
                    String season = "";
                    if (seasons_a.contains(paragraphWrapper.getSeason()))
                        season = seasons_i.get(seasons_a.indexOf(paragraphWrapper.getSeason()));
                    paragraphInjectModifier.put("season", season + " " + calendar.get(Calendar.YEAR));
                    paragraphInjectModifier.put("plannedDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(paragraphWrapper.getPlannedDate()));
                    paragraphInjectModifier.put("developPeriod", paragraphWrapper.getEducationOrgUnitNew().getDevelopPeriod().getTitle());

                    CommonExtractPrint.initEducationType(paragraphInjectModifier, educationOrgUnitOld, "Old");

                    // Получаем список студентов
                    List<Student> studentList = paragraphWrapper.getPersonList();
                    studentList.sort(Student.FULL_FIO_AND_ID_COMPARATOR);

                    //список уровней образования
                    Set<String> baseEduLevels = new HashSet<>();
                    for (Student student : studentList)
                    {
                        StudentFefuExt studentExt = DataAccessServices.dao().getByNaturalId(new StudentFefuExtGen.NaturalId(student));
                        String eduLevel = (studentExt != null && studentExt.getBaseEdu() != null) ? (studentExt.getBaseEdu().equals(FefuStudentManager.BASE_VPO) ? "высшее образование" : "среднее профессиональное образование") : "";

                        if (!eduLevel.isEmpty())
                            baseEduLevels.add(eduLevel);
                    }


                    paragraphInjectModifier.put("baseEducationLevel", baseEduLevels.isEmpty()? "": " имеющих " + CoreStringUtils.join(baseEduLevels, ",") + ",");

                    RtfString rtfString = new RtfString();

                    int j = 0;
                    for (; j < studentList.size() - 1; j++)
                    {
                        rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(studentList.get(j).getFullFio()).par();
                    }

                    rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(studentList.get(j).getFullFio());
                    paragraphInjectModifier.put("STUDENT_LIST", rtfString);
                    paragraphInjectModifier.modify(paragraph);

                    IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                    rtfGroup.setElementList(paragraph.getElementList());
                    parList.add(rtfGroup);
                }
                // полученный документ (набор параграфов вставляем вместо ключевого слова)
                rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
                rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
            }
        }
}