/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.SendPortalNotifications;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic.FefuEntrantsDSHandler;

/**
 * @author Dmitry Seleznev
 * @since 13.06.2013
 */
@Configuration
public class FefuSystemActionSendPortalNotifications extends BusinessComponentManager
{
    public final static String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public final static String NOTIFICATION_TYPE_DS = "notificationTypeDS";
    public final static String ENTRANTS_DS = "entrantsDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, enrollmentCampaignDSHandler()))
                .addDataSource(selectDS(NOTIFICATION_TYPE_DS, notificationTypeComboDSHandler()))
                .addDataSource(selectDS(ENTRANTS_DS, entrantsDSHandler()).addColumn(Entrant.person().identityCard().fullFio().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrollmentCampaignDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EnrollmentCampaign.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", EnrollmentCampaign.useCompetitionGroup()), DQLExpressions.value(Boolean.TRUE)));
                ep.dqlBuilder.order(DQLExpressions.property("e", EnrollmentCampaign.educationYear().intValue()), OrderDirection.desc);
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler notificationTypeComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).filtered(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantsDSHandler()
    {
        return new FefuEntrantsDSHandler(getName());
    }
}