/* $Id$ */
package ru.tandemservice.unifefu.ws.c1.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.Collection;

/**
 * @author Nikolay Fedorovskih
 * @since 13.12.2013
 */
public interface IFefu_1C_Daemon
{
    SpringBeanCache<IFefu_1C_Daemon> instance = new SpringBeanCache<>(IFefu_1C_Daemon.class.getName());

    /**
     * Запуск синхронизации с 1С.
     *
     * @param extractIds список идентификаторов выписок, которые нужно засинхронизировать. Если список не указан, синхронизироваться будут все выписки из лога, по которым еще нет успешного ответа.
     *                   Параметр нужен для только тестовых целей.
     */
    void doSynchronization(Collection<Long> extractIds, boolean systemAction);

    /**
     * После проведения какого-то приказа добавляем все его выписки в очередь на синхронизацию с помощью этого метода.
     */
    void addNewItems(Collection<IAbstractExtract> items);

    /**
     * Тест. Всегда генерируется исключение TestResultException, в котором содержится лог. Исключение вызывается, чтобы откатить транзакцию и тестовые данные в базу не закоммитились.
     */
    void doTest(Collection<Long> extractIds) throws Fefu_1C_Daemon.TestResultException;
}