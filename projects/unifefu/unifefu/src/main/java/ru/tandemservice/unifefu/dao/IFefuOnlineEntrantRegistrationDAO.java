/* $Id$ */
package ru.tandemservice.unifefu.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniec.dao.IOnlineEntrantRegistrationDAO;
import ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt;
import ru.tandemservice.unifefu.ws.FefuOnlineEntrantData;

/**
 * @author Nikolay Fedorovskih
 * @since 31.05.2013
 */
public interface IFefuOnlineEntrantRegistrationDAO extends IOnlineEntrantRegistrationDAO
{
    final SpringBeanCache<IFefuOnlineEntrantRegistrationDAO> instance = new SpringBeanCache<>(IFefuOnlineEntrantRegistrationDAO.class.getName());

    OnlineEntrantFefuExt createOrUpdateFefuOnlineEntrant(long userId, FefuOnlineEntrantData entrantData);

    OnlineEntrantFefuExt getOnlineEntrantFefuExt(long userId);

    FefuOnlineEntrantData getFefuOnlineEntrantData(long userId);
}