/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu19.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract;

/**
 * @author Ekaterina Zvereva
 * @since 18.11.2014
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuTransfAcceleratedTimeStuExtract, Model>
{
}