/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu7;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaSuccessWithDismissStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 7/3/13
 */
public class FefuGiveDiplomaSuccessWithDismissStuListExtractDao extends UniBaseDao implements IExtractComponentDao<FefuGiveDiplomaSuccessWithDismissStuListExtract>
{
    @Override
    public void doCommit(FefuGiveDiplomaSuccessWithDismissStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setStudentStatusOld(extract.getEntity().getStatus());
        extract.getEntity().setStatus(extract.getStudentStatusNew());

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getGraduateDiplomaOrderDate());
            extract.setPrevOrderNumber(orderData.getGraduateDiplomaOrderNumber());
        }

        extract.setFinishedYear(student.getFinishYear());
        student.setFinishYear(CoreDateUtils.getYear(extract.getParagraph().getOrder().getCommitDate()));

        orderData.setGraduateDiplomaOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setGraduateDiplomaOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);

        // создание доп. статуса: «Диплом с отличием»
        StudentCustomStateCI customStateCI = DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), UniFefuDefines.DIPLOMA_SUCCESS);
        StudentCustomState studentCustomState = new StudentCustomState();
        studentCustomState.setCustomState(customStateCI);
        studentCustomState.setStudent(extract.getEntity());
        UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(studentCustomState);
        StudentCustomStateToExtractRelation extractRelation = new StudentCustomStateToExtractRelation();
        extractRelation.setStudentCustomState(studentCustomState);
        extractRelation.setExtract(extract);
        save(extractRelation);

        //проверяем являлся ли студент старостой, если да - то разрываем связь
        UnifefuDaoFacade.getFefuMoveStudentDAO().breakLinkCaptainWithGroups(extract);
    }

    @Override
    public void doRollback(FefuGiveDiplomaSuccessWithDismissStuListExtract extract, Map parameters)
    {
        extract.getEntity().setStatus(extract.getStudentStatusOld());

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setGraduateDiplomaOrderDate(extract.getPrevOrderDate());
        orderData.setGraduateDiplomaOrderNumber(extract.getPrevOrderNumber());


        student.setFinishYear(extract.getFinishedYear());

        getSession().saveOrUpdate(orderData);

        UnifefuDaoFacade.getFefuMoveStudentDAO().linkStudCaptainWithGroups(extract);

        // удаление доп. статуса: «Диплом с отличием»
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateToExtractRelation.class, "r")
                .column(property("r", StudentCustomStateToExtractRelation.studentCustomState().id()))
                .where(eq(property("r", StudentCustomStateToExtractRelation.extract().id()), value(extract.getId())));

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(StudentCustomState.class).where(in(property(StudentCustomState.id()), builder.buildQuery()));
        deleteBuilder.createStatement(getSession()).execute();
    }
}
