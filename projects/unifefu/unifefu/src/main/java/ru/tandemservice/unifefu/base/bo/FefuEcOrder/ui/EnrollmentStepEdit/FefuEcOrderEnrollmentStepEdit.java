/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.EnrollmentStepEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unifefu.entity.catalog.FefuEnrollmentStep;

/**
 * @author Nikolay Fedorovskih
 * @since 10.09.2013
 */
@Configuration
public class FefuEcOrderEnrollmentStepEdit extends BusinessComponentManager
{
    public static final String ENROLLMENT_STEP_DS = "enrollmentStepDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_STEP_DS, enrollmentStepDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> enrollmentStepDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FefuEnrollmentStep.class)
                .order(FefuEnrollmentStep.code());
    }
}