/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ParametersAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;

/**
 * @author Alexey Lopatin
 * @since 01.12.2014
 */
@Configuration
public class FefuEduStdParametersAddEdit extends BusinessComponentManager
{
    public static final String WEEK_TYPE_DS = "weekTypeDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EducationCatalogsManager.instance().developGridDSConfig())
                .addDataSource(selectDS(WEEK_TYPE_DS, weekTypeDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> weekTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppWeekType.class)
                .filter(EppWeekType.title())
                .order(EppWeekType.title());
    }
}