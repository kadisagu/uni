/**
 * GetEDocumentRequisiteResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class GetEDocumentRequisiteResponse  implements java.io.Serializable {
    private java.lang.String getEDocumentRequisiteResult;

    public GetEDocumentRequisiteResponse() {
    }

    public GetEDocumentRequisiteResponse(
           java.lang.String getEDocumentRequisiteResult) {
           this.getEDocumentRequisiteResult = getEDocumentRequisiteResult;
    }


    /**
     * Gets the getEDocumentRequisiteResult value for this GetEDocumentRequisiteResponse.
     * 
     * @return getEDocumentRequisiteResult
     */
    public java.lang.String getGetEDocumentRequisiteResult() {
        return getEDocumentRequisiteResult;
    }


    /**
     * Sets the getEDocumentRequisiteResult value for this GetEDocumentRequisiteResponse.
     * 
     * @param getEDocumentRequisiteResult
     */
    public void setGetEDocumentRequisiteResult(java.lang.String getEDocumentRequisiteResult) {
        this.getEDocumentRequisiteResult = getEDocumentRequisiteResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetEDocumentRequisiteResponse)) return false;
        GetEDocumentRequisiteResponse other = (GetEDocumentRequisiteResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getEDocumentRequisiteResult==null && other.getGetEDocumentRequisiteResult()==null) || 
             (this.getEDocumentRequisiteResult!=null &&
              this.getEDocumentRequisiteResult.equals(other.getGetEDocumentRequisiteResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetEDocumentRequisiteResult() != null) {
            _hashCode += getGetEDocumentRequisiteResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetEDocumentRequisiteResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">GetEDocumentRequisiteResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getEDocumentRequisiteResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "GetEDocumentRequisiteResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
