/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu9.ParagraphAddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuFormativeTransferStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        FefuFormativeTransferStuListExtract firstExtract = model.getFirstExtract();

        if (firstExtract != null)
        {
            model.setCourse(firstExtract.getCourse());
            model.setGroupOld(firstExtract.getGroupOld());
            model.setGroupNew(firstExtract.getGroupNew());
            model.setFormativeOrgUnitNew(firstExtract.getEducationOrgUnitNew().getFormativeOrgUnit());
            model.setCompensationTypeOld(firstExtract.getCompensationTypeOld());
            model.setCompensationTypeNew(firstExtract.getCompensationTypeNew());

        }

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupOldListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));

        model.setFormativeOrgUnitsListModel(new UniSimpleAutocompleteModel()
        {
            private DQLSelectBuilder createBuilder()
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, "g")
                        .joinPath(DQLJoinType.inner, Group.educationOrgUnit().fromAlias("g"), "eou")
                        .column(property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")));

                if (model.getCourse() == null || model.getGroupOld() == null)
                {
                    return builder.where(nothing());
                }
                UniDaoFacade.getEducationLevelDao().applyUsedFilterForEducationOrgUnit(builder, "eou");

                return builder
                        .where(eq(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().fromAlias("eou")), value(model.getGroupOld().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel())))
                        .where(eq(property(EducationOrgUnit.territorialOrgUnit().fromAlias("eou")), value(model.getTerritorialOrgUnit())))
                        .where(eq(property(EducationOrgUnit.developForm().fromAlias("eou")), value(model.getDevelopForm())))
                        .where(eq(property(EducationOrgUnit.developCondition().fromAlias("eou")), value(model.getDevelopCondition())))
                        .where(eq(property(EducationOrgUnit.developTech().fromAlias("eou")), value(model.getDevelopTech())))
                        .where(eq(property(EducationOrgUnit.developPeriod().fromAlias("eou")), value(model.getDevelopPeriod())))
                        .where(ne(property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), value(model.getGroupOld().getEducationOrgUnit().getFormativeOrgUnit())));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                DQLSelectBuilder builder = createBuilder();
                builder.where(eq(property(EducationOrgUnit.formativeOrgUnit().id().fromAlias("eou")), commonValue(primaryKey)));
                return builder.createStatement(getSession()).uniqueResult();
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = createBuilder()
                        .where(like(DQLFunctions.upper(property(EducationOrgUnit.formativeOrgUnit().title().fromAlias("eou"))), value(CoreStringUtils.escapeLike(filter))))
                        .order(property(EducationOrgUnit.formativeOrgUnit().title().fromAlias("eou")));

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > MAX_ROWS)
                {
                    builder.top(MAX_ROWS);
                }

                return new ListResult<>(builder.createStatement(getSession()).list(), count);
            }
        });

        model.setGroupNewListModel(new UniSimpleAutocompleteModel()
        {
            private DQLSelectBuilder createBuilder()
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, "g")
                        .where(eq(property(Group.course().fromAlias("g")), value(model.getCourse())))
                        .where(eq(property(Group.archival().fromAlias("g")), value(Boolean.FALSE)));

                if (model.getCourse() == null || model.getGroupOld() == null || model.getFormativeOrgUnitNew() == null)
                {
                    return builder.where(nothing());
                }

                return builder
                        .where(eq(property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("g")), value(model.getFormativeOrgUnit())))
                        .where(eq(property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("g")), value(model.getTerritorialOrgUnit())))
                        .where(eq(property(Group.educationOrgUnit().educationLevelHighSchool().educationLevel().fromAlias("g")), value(model.getGroupOld().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel())))
                        .where(eq(property(Group.educationOrgUnit().developForm().fromAlias("g")), value(model.getDevelopForm())))
                        .where(eq(property(Group.educationOrgUnit().developCondition().fromAlias("g")), value(model.getDevelopCondition())))
                        .where(eq(property(Group.educationOrgUnit().developTech().fromAlias("g")), value(model.getDevelopTech())))
                        .where(eq(property(Group.educationOrgUnit().developPeriod().fromAlias("g")), value(model.getDevelopPeriod())))
                        .where(eq(property(Group.educationOrgUnit().used().fromAlias("g")), value(Boolean.TRUE)))
                        .where(eq(property(Group.educationOrgUnit().educationLevelHighSchool().allowStudents().fromAlias("g")), value(Boolean.TRUE)));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                DQLSelectBuilder builder = createBuilder();
                builder.where(eq(property(Group.id().fromAlias("g")), commonValue(primaryKey)));
                return builder.createStatement(getSession()).uniqueResult();
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = createBuilder()
                        .where(like(DQLFunctions.upper(property(Group.title().fromAlias("g"))), value(CoreStringUtils.escapeLike(filter))))
                        .order(property(Group.title().fromAlias("g")));

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > MAX_ROWS)
                {
                    builder.top(MAX_ROWS);
                }

                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });

        model.setFormativeOrgUnitNewHint("Список формирующих подразделений, направление подготовки которых и комбинация формы, условия, срока и технологии освоения совпадают с текущими.");
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroupOld()));
    }

    @Override
    protected FefuFormativeTransferStuListExtract createNewInstance(Model model)
    {
        return new FefuFormativeTransferStuListExtract();
    }

    private void validateEducationLevels(Student student, Model model)
    {
        EducationLevels educationLevelStudent = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
        EducationLevels educationLevelGroup = model.getGroupNew().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

        if (!educationLevelStudent.equals(educationLevelGroup))
            UserContext.getInstance().getErrorCollector().add(student.getStudentCategory().getTitle() + " «" + student.getFullFio() + "»: Направление подготовки (специальность) отличается от указанного в новой группе.");
    }

    @Override
    protected void fillExtract(FefuFormativeTransferStuListExtract extract, Student student, Model model)
    {
        validateEducationLevels(student, model);

        extract.setCourse(student.getCourse());
        extract.setGroupOld(student.getGroup());
        extract.setGroupNew(model.getGroupNew());
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
        extract.setEducationOrgUnitNew(model.getGroupNew().getEducationOrgUnit());
        extract.setCompensationTypeOld(student.getCompensationType());
        extract.setCompensationTypeNew(model.getCompensationTypeNew());
    }
}