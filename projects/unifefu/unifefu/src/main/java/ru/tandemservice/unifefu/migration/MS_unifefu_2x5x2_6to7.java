package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x2_6to7 extends IndependentMigrationScript
{
	@Override
	public ScriptDependency[] getBoundaryDependencies()
	{
		return new ScriptDependency[]
				{
						new ScriptDependency("org.tandemframework", "1.6.14"),
						new ScriptDependency("org.tandemframework.shared", "1.5.2"),
						new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
				};
	}

	@Override
	public void run(DBTool tool) throws Exception
	{
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuRatingPackageStudentRow

		// создана новая сущность
		if (!tool.tableExists("fefu_rating_pkg_student_row"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_rating_pkg_student_row",
			                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
			                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
			                          new DBColumn("ratingpackage_id", DBType.LONG).setNullable(false),
			                          new DBColumn("student_id", DBType.LONG).setNullable(false),
			                          new DBColumn("nsiid_id", DBType.LONG).setNullable(false),
			                          new DBColumn("fio_p", DBType.createVarchar(255)).setNullable(false),
			                          new DBColumn("groupnumber_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuRatingPackageStudentRow");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuSendingRatingPackage

		// создана новая сущность
		if (!tool.tableExists("fefu_sending_rating_pkg"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefu_sending_rating_pkg",
			                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
			                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
			                          new DBColumn("sendingtime_p", DBType.TIMESTAMP).setNullable(false),
			                          new DBColumn("sendingdata_p", DBType.TEXT).setNullable(false),
			                          new DBColumn("externalid_p", DBType.createVarchar(255)),
			                          new DBColumn("sendingresult_p", DBType.TEXT).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuSendingRatingPackage");

		}


	}
}
