package ru.tandemservice.unifefu.component.settings.FederalAreasRel;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.fias.base.entity.AddressItem;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.unifefu.entity.FefuFederalAreasRel;
import ru.tandemservice.unifefu.entity.catalog.FefuFederalDistrict;

/**
 * @author amakarova
 * @since 25.10.2013
 */
public class Model
{
    private String _selectedPage;
    private DynamicListDataSource<FefuFederalAreasRel> _dataSource;
    private IDataSettings _settings;
    private List<AddressItem> _addressItems;
    private List<FefuFederalDistrict> _federalDistrictList;

    public IDataSettings getSettings() {
        return _settings;
    }

    public void setSettings(IDataSettings settings) {
        _settings = settings;
    }

    public String getSelectedPage()
    {
        return _selectedPage;
    }

    public void setSelectedPage(String selectedPage)
    {
        _selectedPage = selectedPage;
    }

    public DynamicListDataSource<FefuFederalAreasRel> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<FefuFederalAreasRel> dataSource)
    {
        _dataSource = dataSource;
    }
    public List<AddressItem> getAddressItems() {
        return _addressItems;
    }

    public void setAddressItems(List<AddressItem> addressItems) {
        _addressItems = addressItems;
    }

    public List<FefuFederalDistrict> getFederalDistrictList() {
        return _federalDistrictList;
    }

    public void setFederalDistrictList(List<FefuFederalDistrict> federalDistrictList) {
        _federalDistrictList = federalDistrictList;
    }

}
