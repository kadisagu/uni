/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu18.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.StuffCompensationStuExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2014
 */
public class Model extends ModularStudentExtractPubModel<StuffCompensationStuExtract>
{
	private Double _compensationSum;
	private Double _immediateSum;

	public Double getImmediateSum()
	{
		return _immediateSum;
	}

	public void setImmediateSum(Double immediateSum)
	{
		_immediateSum = immediateSum;
	}

	public Double getCompensationSum()
	{
		return _compensationSum;
	}

	public void setCompensationSum(Double compensationSum)
	{
		_compensationSum = compensationSum;
	}
}
