/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.dao;

import org.apache.axis.AxisFault;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.*;
import org.tandemframework.common.catalog.entity.IActiveCatalogItem;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.IOrgUnitDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiDaemonDao;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiDeliveryDaemonDao;
import ru.tandemservice.unifefu.dao.daemon.IFefuNsiDaemonDao;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;
import ru.tandemservice.unifefu.entity.ws.*;
import ru.tandemservice.unifefu.ws.nsi.*;
import ru.tandemservice.unifefu.ws.nsi.datagram.*;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.FefuNsiIdWrapper;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 03.07.2013
 */
public class FefuNsiSyncDAO extends UniBaseDao implements IFefuNsiSyncDAO
{
    public static final String NSI_SERVICE_URL = "fefu.nsi.service.url";

    public static final String USER_CODE_PREFIX = "userCode_";
    public static final int PACKAGE_LENGTH = 200;

    protected static final Logger log4j_logger = Logger.getLogger(FefuNsiSyncDAO.class);

    public static Logger getLog4jLogger()
    {
        return log4j_logger;
    }

    public static void logEvent(Level loggingLevel, String message)
    {
        // проверяем, есть ли уже appender
        Appender appender = log4j_logger.getAppender("NSISyncAppender");

        if (null != appender)
        {
            log4j_logger.log(loggingLevel, message);
        }

        if (null == appender)
        {
            try
            {
                // добавляем, если нет
                final String path = ApplicationRuntime.getAppInstallPath();
                Calendar cal = CoreDateUtils.createCalendar(new Date());
                String logFileName = "NSISync_" + cal.get(Calendar.YEAR) + "_" + (cal.get(Calendar.MONTH) + 1) + "_" + cal.get(Calendar.DAY_OF_MONTH) + ".log";
                try
                {
                    appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} %p - %m%n"), FilenameUtils.concat(path, "tomcat/logs/" + logFileName));
                } catch (IOException ex)
                {

                }

                appender.setName("NSISyncAppender");
                ((FileAppender) appender).setThreshold(Level.INFO);
                log4j_logger.addAppender(appender);
                log4j_logger.setLevel(Level.INFO);

                if (null != appender)
                {
                    log4j_logger.log(loggingLevel, message);
                }

            } finally
            {
                // и отцепляем, если добавляли
                if (null != appender)
                {
                    log4j_logger.removeAppender(appender);
                }
            }
        }
    }

    @Override
    public Map<String, FefuNsiIds> getFefuNsiIdsMap(final String entityType)
    {
        return getFefuNsiIdsMap(entityType, null);
    }

    @Override
    public Map<String, FefuNsiIds> getFefuNsiIdsMap(final String entityType, Set<Long> entityIdSet)
    {
        Map<String, FefuNsiIds> result = new HashMap<>();
        final List<FefuNsiIds> idsList = new ArrayList<>();

        if (null == entityIdSet)
        {
            idsList.addAll(new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                    .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(entityType)))
                    .createStatement(getSession()).<FefuNsiIds>list());
        } else
        {
            BatchUtils.execute(entityIdSet, 100, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> entityIdsPortion)
                {
                    idsList.addAll(new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                            .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(entityType)))
                            .where(in(property(FefuNsiIds.entityId().fromAlias("e")), entityIdsPortion))
                            .createStatement(getSession()).<FefuNsiIds>list());
                }
            });
        }

        for (FefuNsiIds item : idsList)
        {
            result.put(item.getGuid(), item);
            result.put(String.valueOf(item.getEntityId()), item);
        }

        return result;
    }

    @Override
    public FefuNsiIds getNsiId(Long entityId)
    {
        // entityId не уникален, поэтому берем добавленный последним nsiIds
        return new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "nsi")
                .top(1).column("nsi")
                .where(eq(property("nsi", FefuNsiIds.P_ENTITY_ID), value(entityId)))
                .order(property("nsi", FefuNsiIds.P_SYNC_TIME), OrderDirection.desc)
                .createStatement(getSession()).uniqueResult();
    }

    @Override
    public String getNsiIdGuid(Long entityId)
    {
        // entityId не уникален, поэтому берем добавленный последним nsiIds
        return new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "nsi")
                .top(1).column(property("nsi", FefuNsiIds.P_GUID))
                .where(eq(property("nsi", FefuNsiIds.P_ENTITY_ID), value(entityId)))
                .order(property("nsi", FefuNsiIds.P_SYNC_TIME), OrderDirection.desc)
                .createStatement(getSession()).uniqueResult();
    }

    @Override
    public Map<String, FefuNsiIds> getFefuNsiIdsMapByGuids(final String entityType, Set<String> guids)
    {
        Map<String, FefuNsiIds> result = new HashMap<>();
        final List<FefuNsiIds> idsList = new ArrayList<>();

        if (null == guids)
        {
            idsList.addAll(new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                    .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(entityType)))
                    .createStatement(getSession()).<FefuNsiIds>list());
        } else
        {
            BatchUtils.execute(guids, 100, new BatchUtils.Action<String>()
            {
                @Override
                public void execute(final Collection<String> entityGuidsPortion)
                {
                    idsList.addAll(new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                            .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(entityType)))
                            .where(in(property(FefuNsiIds.guid().fromAlias("e")), entityGuidsPortion))
                            .createStatement(getSession()).<FefuNsiIds>list());
                }
            });
        }

        for (FefuNsiIds item : idsList)
        {
            result.put(item.getGuid(), item);
            result.put(String.valueOf(item.getEntityId()), item);
        }

        return result;
    }

    @Override
    public <T> Map<String, T> getEntityMap(final Class<T> entityClass)
    {
        return getEntityMap(entityClass, null);
    }

    @Override
    public <T> Map<String, T> getEntityMap(final Class<T> entityClass, Set<Long> entityIdSet)
    {
        Map<String, T> result = new HashMap<>();
        final List<T> entityList = new ArrayList<>();

        if (null == entityIdSet)
        {
            entityList.addAll(new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
                    .createStatement(getSession()).<T>list());
        } else
        {
            BatchUtils.execute(entityIdSet, 100, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> entityIdsPortion)
                {
                    entityList.addAll(new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
                            .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                            .createStatement(getSession()).<T>list());
                }
            });
        }


        for (T item : entityList)
        {
            result.put(((IEntity) item).getId().toString(), item);
            if (item instanceof ITitled) result.put(((ITitled) item).getTitle().trim().toUpperCase(), item);
            if (item instanceof IActiveCatalogItem && null != ((IActiveCatalogItem) item).getUserCode())
                result.put(USER_CODE_PREFIX + ((IActiveCatalogItem) item).getUserCode().trim().toUpperCase(), item);
        }

        return result;
    }

    @Override
    public <T> Map<String, CoreCollectionUtils.Pair<T, FefuNsiIds>> getEntityWithNsiIdsMap(final Class<T> entityClass)
    {
        return getEntityWithNsiIdsMap(entityClass, null);
    }

    @Override
    public <T> Map<String, CoreCollectionUtils.Pair<T, FefuNsiIds>> getEntityWithNsiIdsMap(final Class<T> entityClass, Set<Long> entityIdSet)
    {
        return getEntityWithNsiIdsMap(entityClass, entityIdSet, false);
    }

    @Override
    public <T> Map<String, CoreCollectionUtils.Pair<T, FefuNsiIds>> getEntityWithNsiIdsMap(final Class<T> entityClass, Set<Long> entityIdSet, boolean putTitle)
    {
        getSession().clear();

        Map<String, CoreCollectionUtils.Pair<T, FefuNsiIds>> result = new HashMap<>();
        final List<Object[]> entityList = new ArrayList<>();

        if (null == entityIdSet || entityIdSet.isEmpty())
        {
            entityList.addAll(new DQLSelectBuilder().fromEntity(entityClass, "e").column("e").column("nsi")
                    .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "nsi", eq(property("e", IEntity.P_ID), property(FefuNsiIds.entityId().fromAlias("nsi"))))
                    .createStatement(getSession()).<Object[]>list());
        } else
        {
            BatchUtils.execute(entityIdSet, 100, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> entityIdsPortion)
                {
                    entityList.addAll(new DQLSelectBuilder().fromEntity(entityClass, "e").column("e").column("nsi")
                            .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "nsi", eq(property("e", IEntity.P_ID), property(FefuNsiIds.entityId().fromAlias("nsi"))))
                            .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                            .createStatement(getSession()).<Object[]>list());
                }
            });
        }

        // На случай, если объект удалён, но остался его GUID, нужно поднять в мапу эти guid'ы
        if (entityList.isEmpty() && null != entityIdSet)
        {
            final IEntityMeta meta = EntityRuntime.getMeta(entityClass);
            if (null != meta)
            {
                BatchUtils.execute(entityIdSet, 100, new BatchUtils.Action<Long>()
                {
                    @Override
                    public void execute(final Collection<Long> entityIdsPortion)
                    {
                        entityList.addAll(new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "nsi").column("e").column("nsi")
                                .joinEntity("nsi", DQLJoinType.left, entityClass, "e", eq(property(FefuNsiIds.entityId().fromAlias("nsi")), property("e", IEntity.P_ID)))
                                .where(eq(property(FefuNsiIds.entityType().fromAlias("nsi")), value(meta.getName()))).where(isNull(property("e", IEntity.P_ID)))
                                .where(in(property(FefuNsiIds.entityId().fromAlias("nsi")), entityIdsPortion))
                                .createStatement(getSession()).<Object[]>list());
                    }
                });
            }
        }

        for (Object[] item : entityList)
        {
            T entity = (T) item[0];
            FefuNsiIds nsiIds = (FefuNsiIds) item[1];
            Long entityId = null != entity ? ((IEntity) entity).getId() : (null != nsiIds ? nsiIds.getEntityId() : null);

            CoreCollectionUtils.Pair<T, FefuNsiIds> pair = new CoreCollectionUtils.Pair<>(entity, nsiIds);
            result.put(entityId.toString(), pair);
            if (null != nsiIds) result.put(nsiIds.getGuid(), pair);
            if (putTitle && entity instanceof ITitled) result.put(((ITitled) entity).getTitle(), pair);
        }

        return result;
    }

    @Override
    public <T> Map<String, CoreCollectionUtils.Pair<T, FefuNsiIds>> getEntityWithNsiIdsMapByGuids(Class<T> entityClass, Set<String> entityIdSet, boolean getFullIfEntitySetIsEmpty)
    {
        getSession().clear();

        Map<String, CoreCollectionUtils.Pair<T, FefuNsiIds>> result = new HashMap<>();
        final List<Object[]> entityList = new ArrayList<>();

        if (!getFullIfEntitySetIsEmpty && (null == entityIdSet || entityIdSet.isEmpty()))
            return new HashMap<String, CoreCollectionUtils.Pair<T, FefuNsiIds>>();

        if (null == entityIdSet || entityIdSet.isEmpty())
        {
            entityList.addAll(new DQLSelectBuilder().fromEntity(entityClass, "e").column("e").column("nsi")
                    .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "nsi", eq(property("e", IEntity.P_ID), property(FefuNsiIds.entityId().fromAlias("nsi"))))
                    .createStatement(getSession()).<Object[]>list());
        } else
        {
            BatchUtils.execute(entityIdSet, 100, new BatchUtils.Action<String>()
            {
                @Override
                public void execute(final Collection<String> entityIdsPortion)
                {
                    entityList.addAll(new DQLSelectBuilder().fromEntity(entityClass, "e").column("e").column("nsi")
                            .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "nsi", eq(property("e", IEntity.P_ID), property(FefuNsiIds.entityId().fromAlias("nsi"))))
                            .where(in(property("nsi", FefuNsiIds.guid().s()), entityIdsPortion))
                            .createStatement(getSession()).<Object[]>list());
                }
            });
        }

        // На случай, если объект удалён, но остался его GUID, нужно поднять в мапу эти guid'ы
        if (entityList.isEmpty() && null != entityIdSet)
        {
            final IEntityMeta meta = EntityRuntime.getMeta(entityClass);
            if (null != meta)
            {
                BatchUtils.execute(entityIdSet, 100, new BatchUtils.Action<String>()
                {
                    @Override
                    public void execute(final Collection<String> entityIdsPortion)
                    {
                        entityList.addAll(new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "nsi").column("e").column("nsi")
                                .joinEntity("nsi", DQLJoinType.left, entityClass, "e", eq(property(FefuNsiIds.entityId().fromAlias("nsi")), property("e", IEntity.P_ID)))
                                .where(eq(property(FefuNsiIds.entityType().fromAlias("nsi")), value(meta.getName()))).where(isNull(property("e", IEntity.P_ID)))
                                .where(in(property(FefuNsiIds.guid().fromAlias("nsi")), entityIdsPortion))
                                .createStatement(getSession()).<Object[]>list());
                    }
                });
            }
        }

        for (Object[] item : entityList)
        {
            T entity = (T) item[0];
            FefuNsiIds nsiIds = (FefuNsiIds) item[1];
            Long entityId = null != entity ? ((IEntity) entity).getId() : (null != nsiIds ? nsiIds.getEntityId() : null);

            CoreCollectionUtils.Pair<T, FefuNsiIds> pair = new CoreCollectionUtils.Pair<>(entity, nsiIds);
            result.put(entityId.toString(), pair);
            if (null != nsiIds) result.put(nsiIds.getGuid(), pair);
        }

        return result;
    }

    @Override
    public <T> Map<String, T> getNsiSynchronizedEntityMap(final Class<T> entityClass, Set<String> guidsSet, boolean includeGuidGeneratedItems)
    {
        Map<String, T> result = new HashMap<>();
        final List<Object[]> preEntityList = new ArrayList();

        if (null == guidsSet)
        {
            preEntityList.addAll(new DQLSelectBuilder().fromEntity(entityClass, "e").column("e").column(property(FefuNsiIds.guid().fromAlias("guid")))
                    .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "guid", eq(property("e", IEntity.P_ID), property(FefuNsiIds.entityId().fromAlias("guid"))))
                    .createStatement(getSession()).<Object[]>list());
        } else
        {
            BatchUtils.execute(guidsSet, 100, new BatchUtils.Action<String>()
            {
                @Override
                public void execute(final Collection<String> entityGuidsPortion)
                {
                    preEntityList.addAll(new DQLSelectBuilder().fromEntity(entityClass, "e").column("e").column(property(FefuNsiIds.guid().fromAlias("guid")))
                            .joinEntity("e", DQLJoinType.left, FefuNsiIds.class, "guid", eq(property("e", IEntity.P_ID), property(FefuNsiIds.entityId().fromAlias("guid"))))
                            .where(in(property(FefuNsiIds.guid().fromAlias("guid")), entityGuidsPortion))
                            .createStatement(getSession()).<Object[]>list());
                }
            });
        }

        for (Object[] item : preEntityList)
        {
            T entity = (T) item[0];
            String guid = (String) item[1];
            if (null == guid && includeGuidGeneratedItems)
                guid = UUID.nameUUIDFromBytes(String.valueOf(((IEntity) entity).getId()).getBytes()).toString();
            if (null != guid) result.put(guid, entity);
        }

        return result;
    }

    @Override
    public Set<Long> getEntityIdSetByNsiEntityList(List<INsiEntity> nsiEntitySet)
    {
        if (null == nsiEntitySet || nsiEntitySet.isEmpty()) return null;

        Set<String> guidsSet = new HashSet<>();
        for (INsiEntity nsiEntity : nsiEntitySet)
        {
            if (null == nsiEntity.getID()) return null; //Если GUID у сущности НСИ не задан, значит нужны все ИД
            guidsSet.add(nsiEntity.getID());
        }

        return getEntityIdSetByNsiGuidsList(guidsSet);
    }

    @Override
    public Set<Long> getEntityIdSetByNsiGuidsList(Collection<String> nsiGuids)
    {
        final Set<Long> result = new HashSet<>();
        BatchUtils.execute(nsiGuids, 100, new BatchUtils.Action<String>()
        {
            @Override
            public void execute(final Collection<String> entityGuidsPortion)
            {
                result.addAll(new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column(property(FefuNsiIds.entityId().fromAlias("e")))
                        .where(in(property(FefuNsiIds.guid().fromAlias("e")), entityGuidsPortion))
                        .createStatement(getSession()).<Long>list());
            }
        });

        return result;
    }

    @Override
    public FefuNsiIds saveOrUpdateNsiEntities(IEntity entity, FefuNsiIds nsiId, FefuNsiIdWrapper nsiIdWrapper, Set<CoreCollectionUtils.Pair<IEntity, FefuNsiIds>> otherEntityToSaveOrUpdate, Set<CoreCollectionUtils.Pair<IEntity, FefuNsiIds>> otherEntityToPostSaveOrUpdateSet)
    {
        if (entity instanceof OrgUnit)
        {
            NamedSyncInTransactionCheckLocker.register(getSession(), IOrgUnitDao.ORG_UNIT_HIERARCHY_LOCK);
        }

        if (null != otherEntityToSaveOrUpdate && !otherEntityToSaveOrUpdate.isEmpty())
        {
            for (CoreCollectionUtils.Pair<IEntity, FefuNsiIds> otherEntityPair : otherEntityToSaveOrUpdate)
            {
                if (null != otherEntityPair.getX())
                {
                    getSession().saveOrUpdate(otherEntityPair.getX());
                    IEntityMeta meta = EntityRuntime.getMeta(otherEntityPair.getX());
                    FefuNsiSyncDAO.logEvent(Level.INFO, "New item with GUID=" + (null != otherEntityPair.getY() ? otherEntityPair.getY().getGuid() : "UNKNOWN") + " was successfully added to/updated at " + (null != meta ? meta.getName() : "UNKNOWN") + " catalog at OB");
                }
                if (null != otherEntityPair.getY())
                {
                    getSession().saveOrUpdate(otherEntityPair.getY());
                    FefuNsiSyncDAO.logEvent(Level.INFO, "NSI Id object with GUID=" + otherEntityPair.getY().getGuid() + " was successfully added to/updated at OB");
                }
            }

            otherEntityToSaveOrUpdate.clear();
            getSession().flush();
        }

        boolean newItem = null == entity.getId();
        if (nsiIdWrapper.isAddItemsToOBAllowed() && newItem)
        {
            if (entity instanceof OrgUnit) OrgUnitManager.instance().dao().saveOrUpdateOrgUnit((OrgUnit) entity);
            else getSession().save(entity);
            FefuNsiSyncDAO.logEvent(Level.INFO, "New item with GUID=" + nsiIdWrapper.getGuid() + " was successfully added to " + entity.getClass().getSimpleName() + " catalog at OB");
        } else if (nsiIdWrapper.isNsiIsMasterSystem() && !newItem)
        {
            if (entity instanceof OrgUnit) OrgUnitManager.instance().dao().saveOrUpdateOrgUnit((OrgUnit) entity);
            else getSession().update(entity);
            FefuNsiSyncDAO.logEvent(Level.INFO, "Item with GUID=" + nsiIdWrapper.getGuid() + " was successfully updated in " + entity.getClass().getSimpleName() + " catalog at OB");
        }

        IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(entity.getId());


        if (null != otherEntityToPostSaveOrUpdateSet && !otherEntityToPostSaveOrUpdateSet.isEmpty())
        {
            for (CoreCollectionUtils.Pair<IEntity, FefuNsiIds> otherEntityPair : otherEntityToPostSaveOrUpdateSet)
            {
                if (null != otherEntityPair.getX())
                {
                    getSession().saveOrUpdate(otherEntityPair.getX());
                    IEntityMeta meta = EntityRuntime.getMeta(otherEntityPair.getX());
                    FefuNsiSyncDAO.logEvent(Level.INFO, "New item with GUID=" + (null != otherEntityPair.getY() ? otherEntityPair.getY().getGuid() : "UNKNOWN") + " was successfully added to/updated at " + (null != meta ? meta.getName() : "UNKNOWN") + " catalog at OB");
                }
                if (null != otherEntityPair.getY())
                {
                    getSession().saveOrUpdate(otherEntityPair.getY());
                    FefuNsiSyncDAO.logEvent(Level.INFO, "NSI Id object with GUID=" + otherEntityPair.getY().getGuid() + " was successfully added to/updated at OB");
                }
            }

            otherEntityToPostSaveOrUpdateSet.clear();
            getSession().flush();
        }

        if (null == entity.getId()) return null;

        boolean newNsiId = false;
        if (null != nsiId && null != nsiId.getGuid() && !nsiId.getGuid().equals(nsiIdWrapper.getGuid()))
        {
            delete(nsiId);
            nsiId = null;
        }

        // Проверяем занятость guid'а и вылавливаем, а затем удаляем мусорные guid'ы, не удаленные при удалении объекта в ОБ
        if (null != nsiIdWrapper.getGuid())
        {
            // Если у нас в базе уже есть GUID, но он не связан с текущим объектом ОБ, то нужно его поднять и перевесить на новую сущность.
            // Увы, две сущности ОБ с одним GUID мы себе позволить не можем.
            FefuNsiIds junkGuid = getByNaturalId(new FefuNsiIds.NaturalId(nsiIdWrapper.getGuid()));
            if (null != junkGuid)
            {
                IEntity junkRelatedEntity = get(junkGuid.getEntityId());
                if (null == junkRelatedEntity || junkGuid.getEntityType().equals(nsiIdWrapper.getEntityType()))
                {
                    nsiId = junkGuid;
                }
            }
        }

        if (null == nsiId)
        {
            nsiId = new FefuNsiIds();
            nsiId.setHasFormFilled(false);
            newNsiId = true;
        }

        nsiId.setGuid(nsiIdWrapper.getGuid());
        nsiId.setEntityId(entity.getId());
        nsiId.setEntityType(nsiIdWrapper.getEntityType());
        nsiId.setSyncTime(new Date());
        nsiId.setEntityHash(nsiIdWrapper.getObEntityHash());
        getSession().saveOrUpdate(nsiId);

        if (null != nsiIdWrapper.getNsiEntity())
        {
            FefuNsiEntity nsiEntityDatagram = getByNaturalId(new FefuNsiEntity.NaturalId(nsiId));
            if (null == nsiEntityDatagram)
            {
                new DQLUpdateBuilder(FefuNsiEntity.class)
                        .set(FefuNsiEntity.P_ENTITY_XML, DQLExpressions.value(nsiIdWrapper.getNsiEntity()))
                        .where(eq(property(FefuNsiEntity.guid().id()), value(nsiId.getId()))).createStatement(getSession())
                        .execute();
            } else
            {
                nsiEntityDatagram = new FefuNsiEntity();
                nsiEntityDatagram.setGuid(nsiId);
                nsiEntityDatagram.setEntityXml(nsiIdWrapper.getNsiEntity());
                getSession().save(nsiEntityDatagram);
            }
        }

        if (newNsiId)
            FefuNsiSyncDAO.logEvent(Level.INFO, "NSI Id object with GUID=" + nsiIdWrapper.getGuid() + " was successfully added to OB");
        else
            FefuNsiSyncDAO.logEvent(Level.INFO, "NSI Id object with GUID=" + nsiIdWrapper.getGuid() + " was successfully updated at OB");

        getSession().flush();

        return nsiId;
    }

    @Override
    public FefuNsiIds saveOrUpdateNsiId(IEntity entity, FefuNsiIds nsiId, FefuNsiIdWrapper nsiIdWrapper)
    {
        if (null == entity.getId()) return null;
        FefuNsiIds oldNsiId = nsiId;

        FefuNsiIds duplicateGuid = getByNaturalId(new FefuNsiIds.NaturalId(nsiIdWrapper.getGuid()));
        if (null == duplicateGuid && null != oldNsiId)
            duplicateGuid = getByNaturalId(new FefuNsiIds.NaturalId(oldNsiId.getGuid()));
        if (null != duplicateGuid /*&& entityType.equals(duplicateGuid.getEntityType())*/) nsiId = duplicateGuid;

        boolean newNsiId = false;
        if (null == nsiId || (null != nsiId.getGuid() && null != nsiIdWrapper.getGuid() && !nsiId.getGuid().equals(nsiIdWrapper.getGuid())))
        {
            nsiId = new FefuNsiIds();
            nsiId.setHasFormFilled(false);
            if (null == nsiIdWrapper.getGuid())
                nsiIdWrapper.setGuid(UUID.nameUUIDFromBytes(String.valueOf(entity.getId()).getBytes()).toString());
            newNsiId = true;
            if (null != oldNsiId) delete(oldNsiId);
        }
        if (null != nsiIdWrapper.getGuid()) nsiId.setGuid(nsiIdWrapper.getGuid());
        nsiId.setEntityId(entity.getId());
        nsiId.setEntityType(nsiIdWrapper.getEntityType());
        nsiId.setSyncTime(new Date());
        nsiId.setEntityHash(nsiIdWrapper.getObEntityHash());
        getSession().saveOrUpdate(nsiId);

        if (null != nsiIdWrapper.getNsiEntity())
        {
            FefuNsiEntity nsiEntityDatagram = getByNaturalId(new FefuNsiEntity.NaturalId(nsiId));
            if (null == nsiEntityDatagram)
            {
                new DQLUpdateBuilder(FefuNsiEntity.class)
                        .set(FefuNsiEntity.P_ENTITY_XML, DQLExpressions.value(nsiIdWrapper.getNsiEntity()))
                        .where(eq(property(FefuNsiEntity.guid().id()), value(nsiId.getId()))).createStatement(getSession())
                        .execute();
            } else
            {
                nsiEntityDatagram = new FefuNsiEntity();
                nsiEntityDatagram.setGuid(nsiId);
                nsiEntityDatagram.setEntityXml(nsiIdWrapper.getNsiEntity());
                getSession().save(nsiEntityDatagram);
            }
        }

        if (newNsiId)
            FefuNsiSyncDAO.logEvent(Level.INFO, "NSI Id object with GUID=" + nsiId.getGuid() + " was successfully added to OB");
        else
            FefuNsiSyncDAO.logEvent(Level.INFO, "NSI Id object with GUID=" + nsiId.getGuid() + " was successfully updated at OB");

        return nsiId;
    }

    @Override
    public FefuNsiIds saveOrUpdateCrazyPersonWithNsiId(Person person, FefuNsiIds nsiId, String guid, String entityType, FefuNsiIds icNsiId, String icGuid, List<Person> dubPersonList, Map<FefuNsiIds, IdentityCard> prefICMap, String login, Person studentPerson)
    {
        boolean newPerson = null == person.getId();
        List<PersonRole> currentPersonRoleList = new ArrayList<>();

        //TODO гадкий хак для поддержки тупологики сохранения фио при объединении персон, где "страдает" студенческая персона.
        String studentLastName = null != studentPerson ? studentPerson.getIdentityCard().getLastName() : null;
        String studentFirstName = null != studentPerson ? studentPerson.getIdentityCard().getFirstName() : null;
        String studentMiddleName = null != studentPerson ? studentPerson.getIdentityCard().getMiddleName() : null;

        IdentityCard originalIC = person.getIdentityCard();

        Map<FefuNsiIds, IdentityCard> resultPrefICMap = new HashMap<>();
        if (null != prefICMap && !prefICMap.values().isEmpty())
        {
            for (Map.Entry<FefuNsiIds, IdentityCard> prefIC : prefICMap.entrySet())
            {
                if (null == person.getIdentityCard().getId() || !prefIC.getValue().getId().equals(person.getIdentityCard().getId()))
                {
                    DatabaseFile photo = new DatabaseFile();


                    if (null != prefIC.getValue().getPhoto()) photo.update(prefIC.getValue().getPhoto());

                    IdentityCard icToUpdate = new IdentityCard();
                    icToUpdate.update(prefIC.getValue());
                    icToUpdate.setPhoto(photo);
                    icToUpdate.setPerson(null != person.getId() ? person : null);
                    icToUpdate.setLastName(originalIC.getLastName());
                    icToUpdate.setFirstName(originalIC.getFirstName());
                    icToUpdate.setMiddleName(originalIC.getMiddleName());
                    icToUpdate.setBirthDate(originalIC.getBirthDate());
                    icToUpdate.setBirthPlace(originalIC.getBirthPlace());
                    icToUpdate.setSex(originalIC.getSex());

                    if (null != prefIC.getValue().getAddress())
                    {
                        try
                        {
                            AddressBase addr = prefIC.getValue().getAddress().getClass().newInstance();
                            addr.update(prefIC.getValue().getAddress());
                            icToUpdate.setAddress(addr);
                        } catch (Exception e)
                        {
                        }
                    }

                    save(icToUpdate);
                    FefuNsiIds icIds = prefIC.getKey();
                    icIds.setEntityId(icToUpdate.getId());
                    icIds.setSyncTime(new Date());

                    resultPrefICMap.put(icIds, icToUpdate);
                    person.setIdentityCard(icToUpdate);

                    IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(icToUpdate.getId());
                }

                icGuid = prefIC.getKey().getGuid();
                icNsiId = prefIC.getKey();
            }
        }

        getSession().saveOrUpdate(person.getContactData());

        if (null != person.getAddress()) saveOrUpdate(person.getAddress());
        if (null != person.getIdentityCard().getAddress()) saveOrUpdate(person.getIdentityCard().getAddress());
        if (null == person.getIdentityCard().getPhoto()) person.getIdentityCard().setPhoto(new DatabaseFile());
        saveOrUpdate(person.getIdentityCard().getPhoto());
        saveOrUpdate(person.getIdentityCard());
        saveOrUpdate(person);
        person.getIdentityCard().setPerson(person);
        update(person.getIdentityCard());

        IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(person.getId());
        IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(person.getIdentityCard().getId());

        if (null != prefICMap && !prefICMap.values().isEmpty())
        {
            for (Map.Entry<FefuNsiIds, IdentityCard> prefIC : resultPrefICMap.entrySet())
            {
                IdentityCard icToUpdate = prefIC.getValue();
                icToUpdate.setPerson(person);
                update(icToUpdate);
                update(prefIC.getKey());
                IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(icToUpdate.getId());
                FefuNsiDaemonDao.registerNsiEntityInfo(icToUpdate);
            }
        }


        if (null != dubPersonList && dubPersonList.size() > 0)
        {
            for (Person dbl : dubPersonList)
            {
                IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(dbl.getId());
                IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(dbl.getIdentityCard().getId());
                FefuNsiDaemonDao.registerNsiEntityInfo(dbl);
                FefuNsiDaemonDao.registerNsiEntityInfo(dbl.getIdentityCard());
            }
            PersonManager.instance().dao().doMergePersonDublicates(person, dubPersonList);

            if (null != studentLastName) //TODO гадкий хак для поддержки тупологики сохранения фио при объединении персон, где "страдает" студенческая персона.
            {
                person.getIdentityCard().setLastName(studentLastName);
                person.getIdentityCard().setFirstName(studentFirstName);
                person.getIdentityCard().setMiddleName(studentMiddleName);
                update(person.getIdentityCard());
            }
        }


        if (!newPerson)
        {
            currentPersonRoleList.addAll(new DQLSelectBuilder().fromEntity(PersonRole.class, "e").column("e")
                    .where(eq(property(PersonRole.person().id().fromAlias("e")), value(person.getId())))
                    .createStatement(getSession()).<PersonRole>list());
        }

        if (currentPersonRoleList.isEmpty())
        {
            NsiPerson personRole = new NsiPerson();
            personRole.setCreateDate(new Date());
            personRole.setPerson(person);
            save(personRole);
        }

        FefuNsiDaemonDao.registerNsiEntityInfo(person);
        FefuNsiDaemonDao.registerNsiEntityInfo(person.getIdentityCard());

        login = StringUtils.trimToNull(login); // В случае, если пришла пустая строка, то нельзя брать логин в работу.

        if (null != login)
        {
            List<Object[]> principalList = new DQLSelectBuilder().fromEntity(Principal.class, "p").column(property("p"))
                    .column(property(Person2PrincipalRelation.person().id().fromAlias("r")))
                    .column(property(Person2PrincipalRelation.id().fromAlias("r")))
                    .joinEntity("p", DQLJoinType.left, Person2PrincipalRelation.class, "r", eq(property(Principal.id().fromAlias("p")), property(Person2PrincipalRelation.principal().id().fromAlias("r"))))
                    .where(or(
                            eq(property(Person2PrincipalRelation.person().id().fromAlias("r")), value(person.getId())),
                            eq(property(Principal.login().fromAlias("p")), value(login))
                    ))
                    .createStatement(getSession()).list();

            Principal principal = null;
            Principal loginPrincipal = null;
            Long person2PrincipalId = null;
            boolean loginOccupied = false;

            for (Object[] item : principalList)
            {
                Long personId = (Long) item[1];
                if (null != personId && person.getId().equals(personId))
                {
                    principal = (Principal) item[0];
                    person2PrincipalId = (Long) item[2];
                }

                if (null == personId) loginPrincipal = (Principal) item[0];
                if (null != personId && !person.getId().equals(personId))
                    loginOccupied = true;
            }

            if (null == loginPrincipal && !loginOccupied)
            {
                principal.setLogin(login);
            } else if (!loginOccupied)
            {
                if (null != person2PrincipalId)
                {
                    delete(person2PrincipalId);
                    getSession().flush();
                }

                Person2PrincipalRelation rel = new Person2PrincipalRelation();
                rel.setPrincipal(loginPrincipal);
                rel.setPerson(person);
                getSession().saveOrUpdate(rel);
            }

            FefuNsiPersonExt perExt = getByNaturalId(new FefuNsiPersonExt.NaturalId(person));
            if (null == perExt)
            {
                perExt = new FefuNsiPersonExt();
                perExt.setPerson(person);
            }
            perExt.setLogin(login);

            getSession().saveOrUpdate(perExt);
        }

        saveOrUpdateNsiId(person.getIdentityCard(), icNsiId, new FefuNsiIdWrapper(icGuid, EntityRuntime.getMeta(IdentityCard.class).getName()));
        return saveOrUpdateNsiId(person, nsiId, new FefuNsiIdWrapper(guid, entityType));
    }

    @Override
    public void deletePersonWithNsiId(Person person, FefuNsiIds nsiId)
    {
        List<PersonRole> prList = new DQLSelectBuilder().fromEntity(PersonRole.class, "pr").column("pr")
                .where(eq(property(PersonRole.person().id().fromAlias("pr")), value(person.getId())))
                .createStatement(getSession()).list();

        IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(person.getId());
        IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(person.getIdentityCard().getId());

        for (PersonRole pr : prList)
        {
            if (pr instanceof Employee || pr instanceof NsiPerson)
                PersonManager.instance().dao().deletePersonRole(pr.getId());
        }

        if (null != nsiId && null != nsiId.getId()) getSession().delete(nsiId);

        FefuNsiDaemonDao.registerNsiEntityInfo(person);
        FefuNsiDaemonDao.registerNsiEntityInfo(person.getIdentityCard());
    }

    @Override
    public void deleteOrgUnitWithNsiId(OrgUnit orgUnit, FefuNsiIds nsiId)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), IOrgUnitDao.ORG_UNIT_HIERARCHY_LOCK);
        IFefuNsiDaemonDao.instance.get().doRegisterEntityToIgnore(orgUnit.getId());
        if (null != nsiId && null != nsiId.getId()) getSession().delete(nsiId);
        if (null != orgUnit) getSession().delete(orgUnit);
        FefuNsiDaemonDao.registerNsiEntityInfo(orgUnit);
    }

    @Override
    public void updateNsiCatalogSyncTime(String nsiCatalogCode)
    {
        List<FefuNsiCatalogType> catalogsList = new DQLSelectBuilder().fromEntity(FefuNsiCatalogType.class, "e").column("e")
                .where(eq(property(FefuNsiCatalogType.nsiCode().fromAlias("e")), value(nsiCatalogCode)))
                .createStatement(getSession()).list();

        if (!catalogsList.isEmpty())
        {
            FefuNsiCatalogType catalogType = catalogsList.get(0);
            catalogType.setSyncDateTime(new Date());
            update(catalogType);
        }
    }

    @Override
    public void updateDeletedFromOBProperty(Collection<FefuNsiIds> nsiIds)
    {
        for (FefuNsiIds nsiId : nsiIds)
        {
            nsiId.setDeletedFromOB(true);
            nsiId.setSyncTime(new Date());
            update(nsiId);
        }
    }

    @Override
    public void deleteRelatedNsiId(Set<Long> entityIdSet)
    {
        if (null == entityIdSet || entityIdSet.isEmpty()) return;

        DQLDeleteBuilder delBuilder = new DQLDeleteBuilder(FefuNsiIds.class)
                .where(in(property(FefuNsiIds.entityId()), entityIdSet));

        final int deleted = delBuilder.createStatement(getSession()).execute();
    }

    @Override
    public int deleteObjectsSetCheckedForDeletability(Class entityClass, Set<Long> entityIdSet, boolean checkDeletability)
    {
        if (null == entityIdSet || entityIdSet.isEmpty()) return 0;

        final DQLDeleteBuilder del = new DQLDeleteBuilder(entityClass);
        del.where(in(property("id"), entityIdSet));

        if (checkDeletability) del.where(new DQLCanDeleteExpressionBuilder(entityClass, "id").getExpression());

        return del.createStatement(getSession()).execute();
    }

    @Override
    public List<String> getNonDeletableGuidsList(Class entityClass, Set<Long> entityIdSet)
    {
        if (null == entityIdSet || entityIdSet.isEmpty()) return new ArrayList<>();

        DQLSelectBuilder sub = new DQLSelectBuilder().fromEntity(entityClass, "e").column(property("e", "id"))
                .where(new DQLCanDeleteExpressionBuilder(entityClass, "e").getExpression())
                .where(in(property("e", "id"), entityIdSet));

        DQLSelectBuilder sel = new DQLSelectBuilder().fromEntity(entityClass, "e").column(property(FefuNsiIds.guid().fromAlias("ids")))
                .joinEntity("e", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property("e", "id"), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .where(notIn(property("e", "id"), sub.buildQuery()))
                .where(in(property("e", "id"), entityIdSet));

        return sel.createStatement(getSession()).list();
    }

    public void doInsertSinglePerson(List<Long> selectedStudIds)
    {
        if (null == ApplicationRuntime.getProperty(NSI_SERVICE_URL))
            throw new ApplicationException("Адрес сервиса НСИ не задан в конфигурационных файлах.");

        long startTime = System.currentTimeMillis();
        long operationTime = System.currentTimeMillis();
        System.out.println("-------- SENDING PERSONS TO NSI HAS STARTED --------");

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Student.class, "s").column("s").column(property(FefuNsiIds.guid().fromAlias("pg")))
                .joinEntity("s", DQLJoinType.inner, FefuNsiIds.class, "pg", eq(property(Student.person().id().fromAlias("s")), property(FefuNsiIds.entityId().fromAlias("pg"))));

        if (null != selectedStudIds) builder.where(in(property(Student.id().fromAlias("s")), selectedStudIds));
        else builder.where(eq(property(Student.archival().fromAlias("s")), value(Boolean.FALSE)));
        List<Object[]> studentsList = builder.createStatement(getSession()).list();

        final ObjectFactory factory = new ObjectFactory();
        final ServiceSoap_Service service = NsiReactorUtils.getServiceProvider();

        Set<Long> selectedPersonIds = new HashSet();

        List<HumanType> humansToCheckout = new ArrayList<>();


        //TODO нужно оптимизировать. после проверки делать ещё один запрос на тех персон, которых нет
        for (Object[] item : studentsList)
        {
            String personGuid = (String) item[1];
            if (!selectedPersonIds.contains(personGuid))
            {
                HumanType human = factory.createHumanType();
                human.setID(personGuid);
                humansToCheckout.add(human);
            }
        }

        System.out.println("-------- Preparing persons data for checking at NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        operationTime = System.currentTimeMillis();

        final Set<String> alreadySentPersons = new HashSet<>();
        final Long cnt = 0L;

        BatchUtils.execute(humansToCheckout, PACKAGE_LENGTH, new BatchUtils.Action<HumanType>()
        {
            private long localStartTime = System.currentTimeMillis();

            @Override
            public void execute(final Collection<HumanType> humans)
            {
                try
                {
                    ServiceSoap_PortType port = service.getServiceSoap12();
                    ServiceRequestType request = new ServiceRequestType();
                    RoutingHeaderType header = new RoutingHeaderType();
                    ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
                    XDatagram xDatagram = factory.createXDatagram();

                    for (HumanType human : humans)
                        xDatagram.getEntityList().add(human);

                    ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
                    MessageElement datagramOut = new SOAPBodyElement(inStream);

                    header.setOperationType(RoutingHeaderTypeOperationType.fromValue("retrieve"));
                    header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                    header.setSourceId("OB");

                    request.setRoutingHeader(header);
                    datagram.set_any(new MessageElement[]{datagramOut});
                    request.setDatagram(datagram);

                    ServiceResponseType response = port.retrieve(request);

                    if (null != response.getDatagram())
                    {
                        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

                        if (null != respMsg && response.getCallCC().getValue().intValue() == 1)
                        {
                            XDatagram respDatagram = fromXml(XDatagram.class, respMsg.getAsString().getBytes());

                            for (Object item : respDatagram.getEntityList())
                            {
                                if (item instanceof HumanType)
                                {
                                    alreadySentPersons.add(((HumanType) item).getID());
                                }
                            }
                        }
                    } else
                    {
                        System.out.println("CallCC = " + response.getCallCC().getValue().intValue());
                        System.out.println("CallRC = " + response.getCallRC());
                    }

                } catch (final Throwable t)
                {
                    if (t instanceof AxisFault)
                    {
                        System.out.println("FaultString = " + ((AxisFault) t).getFaultString());
                        System.out.println("FaultReason =  " + ((AxisFault) t).getFaultReason());
                        System.out.println("FaultMessage = " + ((AxisFault) t).getMessage());
                        System.out.println("FaultDump = " + ((AxisFault) t).dumpToString());
                    }
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
                System.out.println("Retrieve " + PACKAGE_LENGTH + " more persons. " + ((System.currentTimeMillis() - localStartTime) / 1000d) + " seconds spent since retrieve persons start.");
            }
        });

        System.out.println("-------- Retrieve persons data from NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        operationTime = System.currentTimeMillis();

        final List<HumanType> humansToSend = new ArrayList<>();
        final List<IdentityCardType> icardsToSend = new ArrayList<>();
        selectedPersonIds.clear();

        for (Object[] item : studentsList)
        {
            String personGuid = (String) item[1];
            if (!selectedPersonIds.contains(personGuid) && !alreadySentPersons.contains(personGuid))
            {
                Student student = (Student) item[0];
                Person person = student.getPerson();

                HumanType human = factory.createHumanType();
                human.setID(personGuid);
                human.setHumanLastNameShort(person.getIdentityCard().getLastName().substring(0, 1).toUpperCase());
                human.setHumanLastName(person.getIdentityCard().getLastName());
                human.setHumanFirstName(person.getIdentityCard().getFirstName());
                human.setHumanMiddleName(person.getIdentityCard().getMiddleName());
                human.setHumanBirthdate(NsiDatagramUtil.formatDate(person.getIdentityCard().getBirthDate()));
                human.setHumanSex(SexCodes.MALE.equals(person.getIdentityCard().getSex().getCode()) ? "Мужской" : "Женский");
                human.setHumanBirthPlace(person.getIdentityCard().getBirthPlace());
                human.setHumanINN(person.getInnNumber());
                //human.setHumanLogin(student.getPrincipal().getLogin());
                human.setHumanLearnedWork("0");
                human.setHumanInvention("0");

                IdentityCardType.HumanID humanId = factory.createIdentityCardTypeHumanID();
                humanId.setHuman(human);

                IdentityCardType ct = factory.createIdentityCardType();
                ct.setHumanID(humanId);
                ct.setID(UUID.nameUUIDFromBytes(String.valueOf(person.getIdentityCard().getId()).getBytes()).toString());
                ct.setIdentityCardSeries(person.getIdentityCard().getSeria());
                ct.setIdentityCardNumber(person.getIdentityCard().getNumber());
                ct.setIdentityCardDateGive(NsiDatagramUtil.formatDate(person.getIdentityCard().getIssuanceDate()));
                ct.setIdentityCardWhoGive(person.getIdentityCard().getIssuancePlace());
                ct.setIdentityCardDepartmentCode(person.getIdentityCard().getIssuanceCode());

                humansToSend.add(human);
                icardsToSend.add(ct);
            }
        }

        System.out.println("-------- Preparing persons and identity cards data for transfer to NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        operationTime = System.currentTimeMillis();


        BatchUtils.execute(humansToSend, PACKAGE_LENGTH, new BatchUtils.Action<HumanType>()
        {
            private int cnt = 0;
            private long localStartTime = System.currentTimeMillis();

            @Override
            public void execute(final Collection<HumanType> humans)
            {
                try
                {
                    ServiceSoap_PortType port = service.getServiceSoap12();
                    ServiceRequestType request = new ServiceRequestType();
                    RoutingHeaderType header = new RoutingHeaderType();
                    ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
                    XDatagram xDatagram = factory.createXDatagram();

                    for (HumanType human : humans)
                        xDatagram.getEntityList().add(human);

                    ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
                    MessageElement datagramOut = new SOAPBodyElement(inStream);

                    header.setOperationType(RoutingHeaderTypeOperationType.fromValue("initialize"));
                    header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                    header.setSourceId("OB");

                    request.setRoutingHeader(header);
                    datagram.set_any(new MessageElement[]{datagramOut});
                    request.setDatagram(datagram);

                    ServiceResponseType response = port.initialize(request);
                    MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

                    /*if (null != respMsg)
                    {
                        XDatagram respDatagram = fromXml(XDatagram.class, respMsg.getAsString().getBytes());

                        for(Object item : respDatagram.getEntityList())
                        {
                            if(item instanceof HumanType)
                            {
                                alreadySentPersons.add(((HumanType) item).getID());
                            }
                        }
                    } */

                } catch (final Throwable t)
                {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }

                cnt += humans.size();
                System.out.println("Persons " + cnt + " / " + humansToSend.size() + " were successfully sent to NSI. Single package sending took " + ((System.currentTimeMillis() - localStartTime) / 1000d) + " seconds");
                localStartTime = System.currentTimeMillis();
            }
        });

        System.out.println("-------- Sending persons data to NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        operationTime = System.currentTimeMillis();

        BatchUtils.execute(icardsToSend, PACKAGE_LENGTH, new BatchUtils.Action<IdentityCardType>()
        {
            private int cnt = 0;
            private long localStartTime = System.currentTimeMillis();

            @Override
            public void execute(final Collection<IdentityCardType> icards)
            {
                try
                {
                    ServiceSoap_PortType port = service.getServiceSoap12();
                    ServiceRequestType request = new ServiceRequestType();
                    RoutingHeaderType header = new RoutingHeaderType();
                    ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
                    XDatagram xDatagram = factory.createXDatagram();

                    for (IdentityCardType icard : icards)
                        xDatagram.getEntityList().add(icard);

                    ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
                    MessageElement datagramOut = new SOAPBodyElement(inStream);

                    header.setOperationType(RoutingHeaderTypeOperationType.fromValue("initialize"));
                    header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                    header.setSourceId("OB");

                    request.setRoutingHeader(header);
                    datagram.set_any(new MessageElement[]{datagramOut});
                    request.setDatagram(datagram);

                    ServiceResponseType response = port.initialize(request);
                } catch (final Throwable t)
                {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }

                cnt += icards.size();
                System.out.println("Identity cards " + cnt + " / " + icardsToSend.size() + " were successfully sent to NSI. Single package sending took " + ((System.currentTimeMillis() - localStartTime) / 1000d) + " seconds");
                localStartTime = System.currentTimeMillis();
            }
        });

        System.out.println("-------- Sending persons data to NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        System.out.println("-------- SENDING PERSONS TO NSI WAS FINISHED. Total transfer took " + ((System.currentTimeMillis() - startTime) / 1000d) + " seconds");
    }


    public void doUpdateSinglePersonCitizenship(List<Long> selectedStudIds)
    {
        if (null == ApplicationRuntime.getProperty(NSI_SERVICE_URL))
            throw new ApplicationException("Адрес сервиса НСИ не задан в конфигурационных файлах.");

        long startTime = System.currentTimeMillis();
        long operationTime = System.currentTimeMillis();
        System.out.println("-------- SENDING PERSONS TO NSI HAS STARTED --------");

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Student.class, "s").column("s").column(property(FefuNsiIds.guid().fromAlias("pg")))
                .joinEntity("s", DQLJoinType.inner, FefuNsiIds.class, "pg", eq(property(Student.person().id().fromAlias("s")), property(FefuNsiIds.entityId().fromAlias("pg"))));

        if (null != selectedStudIds) builder.where(in(property(Student.id().fromAlias("s")), selectedStudIds));
        else builder.where(eq(property(Student.archival().fromAlias("s")), value(Boolean.FALSE)));
        List<Object[]> studentsList = builder.createStatement(getSession()).list();

        final ObjectFactory factory = new ObjectFactory();
        final ServiceSoap_Service service = NsiReactorUtils.getServiceProvider();

        Set<Long> selectedPersonIds = new HashSet();

        List<HumanType> humansToCheckout = new ArrayList<>();


        //TODO нужно оптимизировать. после проверки делать ещё один запрос на тех персон, которых нет
        for (Object[] item : studentsList)
        {
            String personGuid = (String) item[1];
            if (!selectedPersonIds.contains(personGuid))
            {
                HumanType human = factory.createHumanType();
                human.setID(personGuid);
                humansToCheckout.add(human);
            }
        }

        System.out.println("-------- Preparing persons data for checking at NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        operationTime = System.currentTimeMillis();

        final Map<String, HumanType> alreadySentPersons = new HashMap<>();
        final Long cnt = 0L;

        BatchUtils.execute(humansToCheckout, PACKAGE_LENGTH, new BatchUtils.Action<HumanType>()
        {
            private long localStartTime = System.currentTimeMillis();

            @Override
            public void execute(final Collection<HumanType> humans)
            {
                try
                {
                    ServiceSoap_PortType port = service.getServiceSoap12();
                    ServiceRequestType request = new ServiceRequestType();
                    RoutingHeaderType header = new RoutingHeaderType();
                    ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
                    XDatagram xDatagram = factory.createXDatagram();

                    for (HumanType human : humans)
                        xDatagram.getEntityList().add(human);

                    ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
                    MessageElement datagramOut = new SOAPBodyElement(inStream);

                    header.setOperationType(RoutingHeaderTypeOperationType.fromValue("retrieve"));
                    header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                    header.setSourceId("OB");

                    request.setRoutingHeader(header);
                    datagram.set_any(new MessageElement[]{datagramOut});
                    request.setDatagram(datagram);

                    ServiceResponseType response = port.retrieve(request);

                    if (null != response.getDatagram())
                    {
                        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

                        if (null != respMsg && response.getCallCC().getValue().intValue() == 0)
                        {
                            XDatagram respDatagram = fromXml(XDatagram.class, respMsg.getAsString().getBytes());

                            for (Object item : respDatagram.getEntityList())
                            {
                                if (item instanceof HumanType)
                                {
                                    alreadySentPersons.put(((HumanType) item).getID(), (HumanType) item);
                                }
                            }
                        }
                    } else
                    {
                        System.out.println("CallCC = " + response.getCallCC().getValue().intValue());
                        System.out.println("CallRC = " + response.getCallRC());
                    }

                } catch (final Throwable t)
                {
                    if (t instanceof AxisFault)
                    {
                        System.out.println("FaultString = " + ((AxisFault) t).getFaultString());
                        System.out.println("FaultReason =  " + ((AxisFault) t).getFaultReason());
                        System.out.println("FaultMessage = " + ((AxisFault) t).getMessage());
                        System.out.println("FaultDump = " + ((AxisFault) t).dumpToString());
                    }
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
                System.out.println("Retrieve " + PACKAGE_LENGTH + " more persons. " + ((System.currentTimeMillis() - localStartTime) / 1000d) + " seconds spent since retrieve persons start.");
            }
        });

        System.out.println("-------- Retrieve persons data from NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        operationTime = System.currentTimeMillis();


        Map<String, OksmType> oksmTypesMap = new HashMap<>();
        {
            long localStartTime = System.currentTimeMillis();
            try
            {
                ServiceSoap_PortType port = service.getServiceSoap12();
                ServiceRequestType request = new ServiceRequestType();
                RoutingHeaderType header = new RoutingHeaderType();
                ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
                XDatagram xDatagram = factory.createXDatagram();
                xDatagram.getEntityList().add(factory.createOksmType());

                ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
                MessageElement datagramOut = new SOAPBodyElement(inStream);

                header.setOperationType(RoutingHeaderTypeOperationType.fromValue("retrieve"));
                header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                header.setSourceId("OB");

                request.setRoutingHeader(header);
                datagram.set_any(new MessageElement[]{datagramOut});
                request.setDatagram(datagram);

                ServiceResponseType response = port.retrieve(request);

                if (null != response.getDatagram())
                {
                    MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

                    if (null != respMsg && response.getCallCC().getValue().intValue() == 0)
                    {
                        XDatagram respDatagram = fromXml(XDatagram.class, respMsg.getAsString().getBytes());

                        for (Object item : respDatagram.getEntityList())
                        {
                            if (item instanceof OksmType)
                            {
                                OksmType oksmItem = (OksmType) item;
                                String altId = oksmItem.getOksmID();
                                if (altId.length() < 3)
                                    altId = altId.length() == 1 ? ("00" + altId) : (altId.length() == 2 ? ("0" + altId) : altId);
                                oksmTypesMap.put(oksmItem.getOksmID(), oksmItem);
                                oksmTypesMap.put(altId, oksmItem);
                            }
                        }
                    }
                } else
                {
                    System.out.println("CallCC = " + response.getCallCC().getValue().intValue());
                    System.out.println("CallRC = " + response.getCallRC());
                }

            } catch (final Throwable t)
            {
                if (t instanceof AxisFault)
                {
                    System.out.println("FaultString = " + ((AxisFault) t).getFaultString());
                    System.out.println("FaultReason =  " + ((AxisFault) t).getFaultReason());
                    System.out.println("FaultMessage = " + ((AxisFault) t).getMessage());
                    System.out.println("FaultDump = " + ((AxisFault) t).dumpToString());
                }
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            System.out.println("Retrieve oksm took " + ((System.currentTimeMillis() - localStartTime) / 1000d) + " seconds.");
        }


        final List<HumanType> humansToSend = new ArrayList<>();
        final List<HumanType> humansToUpdate = new ArrayList<>();
        final List<IdentityCardType> icardsToSend = new ArrayList<>();
        selectedPersonIds.clear();

        for (Object[] item : studentsList)
        {
            String personGuid = (String) item[1];
            if (!selectedPersonIds.contains(personGuid) && !alreadySentPersons.containsKey(personGuid))
            {
                Student student = (Student) item[0];
                Person person = student.getPerson();

                ICitizenship citizenship = person.getIdentityCard().getCitizenship();
                String citshpCode = (null != citizenship && citizenship instanceof AddressCountry && null != ((AddressCountry) citizenship).getDigitalCode()) ? String.valueOf(((AddressCountry) citizenship).getDigitalCode()) : null;
                OksmType oksm = null != citshpCode ? oksmTypesMap.get(citshpCode) : null;
                if (null == oksm && null != citshpCode)
                {
                    citshpCode = citshpCode.length() == 1 ? ("00" + citshpCode) : (citshpCode.length() == 2 ? ("0" + citshpCode) : citshpCode);
                    oksm = null != citshpCode ? oksmTypesMap.get(citshpCode) : null;
                }

                HumanType human = factory.createHumanType();
                human.setID(personGuid);
                human.setHumanLastNameShort(person.getIdentityCard().getLastName().substring(0, 1).toUpperCase());
                human.setHumanLastName(person.getIdentityCard().getLastName());
                human.setHumanFirstName(person.getIdentityCard().getFirstName());
                human.setHumanMiddleName(person.getIdentityCard().getMiddleName());
                human.setHumanBirthdate(NsiDatagramUtil.formatDate(person.getIdentityCard().getBirthDate()));
                human.setHumanSex(SexCodes.MALE.equals(person.getIdentityCard().getSex().getCode()) ? "Мужской" : "Женский");
                human.setHumanBirthPlace(person.getIdentityCard().getBirthPlace());
                human.setHumanINN(person.getInnNumber());
                if (null != oksm)
                {
                    HumanType.HumanCitizenship humCitshp = factory.createHumanTypeHumanCitizenship();
                    humCitshp.setOksm(oksm);
                    human.setHumanCitizenship(humCitshp);
                }
                //human.setHumanLogin(student.getPrincipal().getLogin());
                human.setHumanLearnedWork("0");
                human.setHumanInvention("0");

                IdentityCardType.HumanID humanId = factory.createIdentityCardTypeHumanID();
                humanId.setHuman(human);

                IdentityCardType ct = factory.createIdentityCardType();
                ct.setHumanID(humanId);
                ct.setID(UUID.nameUUIDFromBytes(String.valueOf(person.getIdentityCard().getId()).getBytes()).toString());
                ct.setIdentityCardSeries(person.getIdentityCard().getSeria());
                ct.setIdentityCardNumber(person.getIdentityCard().getNumber());
                ct.setIdentityCardDateGive(NsiDatagramUtil.formatDate(person.getIdentityCard().getIssuanceDate()));
                ct.setIdentityCardWhoGive(person.getIdentityCard().getIssuancePlace());
                ct.setIdentityCardDepartmentCode(person.getIdentityCard().getIssuanceCode());

                humansToSend.add(human);
                icardsToSend.add(ct);
            } else if (!selectedPersonIds.contains(personGuid) && alreadySentPersons.containsKey(personGuid))
            {
                HumanType nsiHuman = alreadySentPersons.get(personGuid);

                if (null != nsiHuman && null == nsiHuman.getHumanCitizenship())
                {
                    Student student = (Student) item[0];
                    Person person = student.getPerson();

                    ICitizenship citizenship = person.getIdentityCard().getCitizenship();
                    String citshpCode = (null != citizenship && citizenship instanceof AddressCountry && null != ((AddressCountry) citizenship).getDigitalCode()) ? String.valueOf(((AddressCountry) citizenship).getDigitalCode()) : null;
                    OksmType oksm = null != citshpCode ? oksmTypesMap.get(citshpCode) : null;
                    if (null == oksm)
                    {
                        citshpCode = citshpCode.length() == 1 ? ("00" + citshpCode) : (citshpCode.length() == 2 ? ("0" + citshpCode) : citshpCode);
                        oksm = null != citshpCode ? oksmTypesMap.get(citshpCode) : null;
                    }

                    if (null != oksm)
                    {
                        HumanType.HumanCitizenship humCitshp = factory.createHumanTypeHumanCitizenship();
                        humCitshp.setOksm(oksm);
                        nsiHuman.setHumanCitizenship(humCitshp);
                        humansToUpdate.add(nsiHuman);
                    }
                }
            }
        }

        System.out.println("-------- Preparing persons and identity cards data for transfer to NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        operationTime = System.currentTimeMillis();


        BatchUtils.execute(humansToSend, PACKAGE_LENGTH, new BatchUtils.Action<HumanType>()
        {
            private int cnt = 0;
            private long localStartTime = System.currentTimeMillis();

            @Override
            public void execute(final Collection<HumanType> humans)
            {
                try
                {
                    ServiceSoap_PortType port = service.getServiceSoap12();
                    ServiceRequestType request = new ServiceRequestType();
                    RoutingHeaderType header = new RoutingHeaderType();
                    ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
                    XDatagram xDatagram = factory.createXDatagram();

                    for (HumanType human : humans)
                        xDatagram.getEntityList().add(human);

                    ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
                    MessageElement datagramOut = new SOAPBodyElement(inStream);

                    header.setOperationType(RoutingHeaderTypeOperationType.fromValue("initialize"));
                    header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                    header.setSourceId("OB");

                    request.setRoutingHeader(header);
                    datagram.set_any(new MessageElement[]{datagramOut});
                    request.setDatagram(datagram);

                    ServiceResponseType response = port.initialize(request);
                    MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

                    /*if (null != respMsg)
                    {
                        XDatagram respDatagram = fromXml(XDatagram.class, respMsg.getAsString().getBytes());

                        for(Object item : respDatagram.getEntityList())
                        {
                            if(item instanceof HumanType)
                            {
                                alreadySentPersons.add(((HumanType) item).getID());
                            }
                        }
                    } */

                } catch (final Throwable t)
                {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }

                cnt += humans.size();
                System.out.println("Persons " + cnt + " / " + humansToSend.size() + " were successfully sent to NSI. Single package sending took " + ((System.currentTimeMillis() - localStartTime) / 1000d) + " seconds");
                localStartTime = System.currentTimeMillis();
            }
        });

        System.out.println("-------- Sending persons data to NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        operationTime = System.currentTimeMillis();

        BatchUtils.execute(icardsToSend, PACKAGE_LENGTH, new BatchUtils.Action<IdentityCardType>()
        {
            private int cnt = 0;
            private long localStartTime = System.currentTimeMillis();

            @Override
            public void execute(final Collection<IdentityCardType> icards)
            {
                try
                {
                    ServiceSoap_PortType port = service.getServiceSoap12();
                    ServiceRequestType request = new ServiceRequestType();
                    RoutingHeaderType header = new RoutingHeaderType();
                    ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
                    XDatagram xDatagram = factory.createXDatagram();

                    for (IdentityCardType icard : icards)
                        xDatagram.getEntityList().add(icard);

                    ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
                    MessageElement datagramOut = new SOAPBodyElement(inStream);

                    header.setOperationType(RoutingHeaderTypeOperationType.fromValue("initialize"));
                    header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                    header.setSourceId("OB");

                    request.setRoutingHeader(header);
                    datagram.set_any(new MessageElement[]{datagramOut});
                    request.setDatagram(datagram);

                    ServiceResponseType response = port.initialize(request);
                } catch (final Throwable t)
                {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }

                cnt += icards.size();
                System.out.println("Identity cards " + cnt + " / " + icardsToSend.size() + " were successfully sent to NSI. Single package sending took " + ((System.currentTimeMillis() - localStartTime) / 1000d) + " seconds");
                localStartTime = System.currentTimeMillis();
            }
        });

        System.out.println("-------- Preparing persons and identity cards data for transfer to NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        operationTime = System.currentTimeMillis();


        BatchUtils.execute(humansToUpdate, PACKAGE_LENGTH, new BatchUtils.Action<HumanType>()
        {
            private int cnt = 0;
            private long localStartTime = System.currentTimeMillis();

            @Override
            public void execute(final Collection<HumanType> humans)
            {
                try
                {
                    ServiceSoap_PortType port = service.getServiceSoap12();
                    ServiceRequestType request = new ServiceRequestType();
                    RoutingHeaderType header = new RoutingHeaderType();
                    ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
                    XDatagram xDatagram = factory.createXDatagram();

                    for (HumanType human : humans)
                        xDatagram.getEntityList().add(human);

                    ByteArrayInputStream inStream = new ByteArrayInputStream(toXml(xDatagram));
                    MessageElement datagramOut = new SOAPBodyElement(inStream);

                    header.setOperationType(RoutingHeaderTypeOperationType.fromValue("update"));
                    header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
                    header.setSourceId("OB");

                    request.setRoutingHeader(header);
                    datagram.set_any(new MessageElement[]{datagramOut});
                    request.setDatagram(datagram);

                    ServiceResponseType response = port.update(request);
                    MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;
                } catch (final Throwable t)
                {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }

                cnt += humans.size();
                System.out.println("Persons " + cnt + " / " + humansToSend.size() + " were successfully updated in the NSI. Single package sending took " + ((System.currentTimeMillis() - localStartTime) / 1000d) + " seconds");
                localStartTime = System.currentTimeMillis();
            }
        });

        System.out.println("-------- Sending persons data to NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        operationTime = System.currentTimeMillis();

        System.out.println("-------- Sending persons data to NSI took " + ((System.currentTimeMillis() - operationTime) / 1000d) + " seconds");
        System.out.println("-------- SENDING PERSONS TO NSI WAS FINISHED. Total transfer took " + ((System.currentTimeMillis() - startTime) / 1000d) + " seconds");
    }


    public static byte[] toXml(final Object root)
    {
        try
        {
            final JAXBContext jc = JAXBContext.newInstance(root.getClass());
            final Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            final ByteArrayOutputStream ba = new ByteArrayOutputStream(8192);
            m.marshal(root, ba);
            return ba.toByteArray();
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public static <T> T fromXml(final Class<T> clazz, final byte[] xml)
    {
        try
        {
            final JAXBContext jc = JAXBContext.newInstance(clazz.getPackage().getName());
            final Unmarshaller u = jc.createUnmarshaller();
            final Object object = u.unmarshal(new ByteArrayInputStream(xml));

            /*if (object instanceof JAXBElement)
            {
                JAXBElement element = ((JAXBElement) object);
                if ("error".equalsIgnoreCase(element.getName().getLocalPart()))
                {
                    throw new IFisService.FisError(element, element.getValue().toString()); // корневой тег с ошибкой
                }
            } */

            return clazz.cast(object);
        } catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    @Override
    public Long doInitLogNSIOutcomingAction(RoutingHeaderType header, XDatagram datagram, String comment, Long prevLogRowId)
    {
        if (null == header && null == prevLogRowId) return null;
        FefuNsiLogRow prevLogRow = null != prevLogRowId ? get(FefuNsiLogRow.class, prevLogRowId) : null;
        if (null == header && null == prevLogRow) return null;

        if (null != prevLogRow && 0 == prevLogRow.getAttemptsCount() && !prevLogRow.isExecuted())
        {
            prevLogRow.setAttemptsCount(1);
            update(prevLogRow);
            return prevLogRowId;
        }

        FefuNsiLogRow row = new FefuNsiLogRow();
        row.setExecuted(false);
        row.setMessageTime(new Date());
        row.setMessageStatus(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_INITIAL.intValue());

        if (null != prevLogRow)
        {
            row.setMessageId(null != header.getMessageId() ? header.getMessageId() : UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
            row.setOperationType(prevLogRow.getOperationType());
            row.setEventType(prevLogRow.getEventType());
            row.setCorrelationId(prevLogRow.getCorrelationId());
            row.setParentId(prevLogRow.getMessageId());
            row.setSourceId(prevLogRow.getSourceId());
            row.setDestinationId(prevLogRow.getDestinationId());
            row.setReplyDestinationId(prevLogRow.getReplyDestinationId());
            row.setTicketDestinationId(prevLogRow.getTicketDestinationId());
            if (null != header) row.setRoutingHeader(new String(NsiReactorUtils.toXml(header)));
            if (null != comment) row.setMessageComment(comment);
            row.setAttemptsCount(prevLogRow.getAttemptsCount() + 1);

            prevLogRow.setExecuted(true);
            update(prevLogRow);
        } else
        {
            row.setAttemptsCount(0);
            row.setOperationType(header.getOperationType().getValue());
            row.setEventType(FefuNsiLogRow.OUT_EVENT_TYPE);
            row.setMessageId(header.getMessageId());
            row.setCorrelationId(header.getCorrelationId());
            row.setParentId(header.getParentId());
            row.setSourceId(header.getSourceId());
            row.setDestinationId(header.getDestinationId());
            row.setReplyDestinationId(header.getReplyDestinationId());
            row.setTicketDestinationId(header.getTicketDestinationId());
            row.setMessageStatus(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_INITIAL.intValue());
            if (null != header) row.setRoutingHeader(new String(NsiReactorUtils.toXml(header)));
            if (null != comment) row.setMessageComment(comment);
        }

        if (null != datagram)
        {
            Object obj = datagram.getEntityList().get(0);
            row.setMessageBody(new String(NsiReactorUtils.toXml(datagram)));
            if (null != obj)
            {
                row.setDirectoryId(obj.getClass().getSimpleName());
                if (datagram.getEntityList().size() == 1)
                {
                    try
                    {
                        row.setObjectId((String) obj.getClass().getMethod("getID").invoke(obj));
                    } catch (Exception ex)
                    {
                        // Не судьба
                    }
                }
            }
        }

        if (null == row.getDirectoryId()) row.setDirectoryId("-");
        if (null == row.getObjectId()) row.setObjectId("-");

        save(row);
        getSession().flush();
        return row.getId();
    }

    @Override
    public Long doUpdateLogNSIOutcomingAction(Long prevLogRowId, RoutingHeaderType header, XDatagram datagram, ServiceResponseType response, Throwable error, String comment)
    {
        if (null == prevLogRowId)
        {
            FefuNsiSyncDAO.logEvent(Level.ERROR, "!!!!!!! Some error has occured, but was not logged in GUI !!!!!!\n" + error.getMessage());
            return null;
        }

        FefuNsiLogRow prevRow = get(FefuNsiLogRow.class, prevLogRowId);
        if (null == prevLogRowId) return null;

        if (0 == prevRow.getAttemptsCount())
        {
            prevRow.setAttemptsCount(1);
            prevRow.setMessageTime(new Date());
        } else
        {
            if (null != response) prevRow.setMessageStatus(response.getCallCC().getValue().intValue());

            if (null == error)
            {
                prevRow.setSoapFault(null);
                prevRow.setMessageComment((null != comment ? (comment + " ") : "") + (null != response && null != response.getCallRC() ? response.getCallRC() : ""));
            } else
            {
                if (null == response)
                    prevRow.setMessageStatus(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_ERROR.intValue());

                if (error instanceof AxisFault)
                {
                    prevRow.setMessageComment(((AxisFault) error).getFaultString());
                    if (null != ((AxisFault) error).getFaultString()
                            && (((AxisFault) error).getFaultString().contains("java.net.UnknownHostException")
                            || ((AxisFault) error).getFaultString().contains("java.net.SocketTimeoutException")
                            || ((AxisFault) error).getFaultString().contains("java.net.ConnectException")
                            || ((AxisFault) error).getFaultString().contains("(404)Not Found")))
                    {
                        prevRow.setMessageStatus(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_TRANSPORT_ERROR.intValue());
                        FefuNsiDeliveryDaemonDao.lockByTransportError(prevLogRowId);
                    } else
                    {
                        prevRow.setSoapFault(((AxisFault) error).dumpToString());
                        prevRow.setMessageStatus(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_ERROR.intValue());
                    }
                } else prevRow.setSoapFault(error.getMessage());

                if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_OK.intValue() == prevRow.getMessageStatus()
                        || FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_WARN.intValue() == prevRow.getMessageStatus())
                    prevRow.setExecuted(true);
            }
        }

        if (FefuNsiRequestsProcessor.OPERATION_TYPE_RETRIEVE.equals(prevRow.getOperationType()))
            prevRow.setExecuted(true);

        update(prevRow);
        getSession().flush();
        return prevLogRowId;
    }

    @Override
    public Long doLogNSIRequest(ru.tandemservice.unifefu.ws.nsi.server.ServiceRequestType request, List<Object> datagramElements)
    {
        FefuNsiLogRow logRow = new FefuNsiLogRow();
        logRow.setMessageTime(new Date());
        logRow.setMessageStatus(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_INITIAL.intValue());
        logRow.setEventType("in");

        ru.tandemservice.unifefu.ws.nsi.server.RoutingHeaderType header = request.getRoutingHeader();

        if (null != header)
        {
            logRow.setMessageId(header.getMessageId());
            logRow.setSourceId(header.getSourceId());
            logRow.setDestinationId(header.getDestinationId());
            logRow.setParentId(header.getParentId());
            logRow.setCorrelationId(header.getCorrelationId());
            logRow.setReplyDestinationId(header.getReplyDestinationId());
            logRow.setTicketDestinationId(header.getTicketDestinationId());
            logRow.setOperationType(header.getOperationType());
            logRow.setRoutingHeader(new String(NsiReactorUtils.toXml(header)));
        }

        if (null != datagramElements && datagramElements.size() > 0)
        {
            logRow.setDirectoryId(datagramElements.get(0).getClass().getSimpleName());
            if (datagramElements.size() == 1)
            {
                try
                {
                    logRow.setObjectId((String) datagramElements.get(0).getClass().getMethod("getID").invoke(datagramElements.get(0)));
                } catch (Exception e)
                {
                }
            }
        }

        if (null == logRow.getObjectId()) logRow.setObjectId("-");
        if (null == logRow.getDirectoryId()) logRow.setDirectoryId("-");

        if (null != request.getDatagram())
            logRow.setMessageBody(new String(NsiReactorUtils.toXml(request.getDatagram())));

        getSession().save(logRow);

        return logRow.getId();
    }

    @Override
    public void doUpdateLogNSIRequest(ru.tandemservice.unifefu.ws.nsi.server.ServiceResponseType response, Long logRowId)
    {
        FefuNsiLogRow logRowToUpdate = get(logRowId);

        if (null != logRowToUpdate)
        {
            logRowToUpdate.setMessageTime(new Date());
            logRowToUpdate.setMessageComment(response.getCallRC());

            if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_OK.equals(response.getCallCC()))
                logRowToUpdate.setMessageStatus(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_OK.intValue());
            else if (FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_WARN.equals(response.getCallCC()))
                logRowToUpdate.setMessageStatus(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_WARN.intValue());
            else
                logRowToUpdate.setMessageStatus(FefuNsiRequestsProcessor.RESPONSE_CODE_NUMBER_ERROR.intValue());

            getSession().update(logRowToUpdate);
        }
    }
}