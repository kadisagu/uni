/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e61.AddEdit;

import java.util.stream.Collectors;

/**
 * @author Alexey Lopatin
 * @since 08.12.2013
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e61.AddEdit.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e61.AddEdit.Model model)
    {
        super.prepare(model);
        model.setTermsList(model.getTermsList().stream().limit(14).collect(Collectors.toList()));
    }
}
