package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.MdbViewPrikazyOther;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Прочие приказы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewPrikazyOtherGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.MdbViewPrikazyOther";
    public static final String ENTITY_NAME = "mdbViewPrikazyOther";
    public static final int VERSION_HASH = -1323397500;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_ORDER = "order";
    public static final String L_STUDENT = "student";
    public static final String P_ORDER_NUMBER = "orderNumber";
    public static final String P_ORDER_TYPE = "orderType";
    public static final String P_MODULAR_REASON = "modularReason";
    public static final String P_LIST_REASON = "listReason";
    public static final String P_LIST_BASICS = "listBasics";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_ORDER_DATE = "orderDate";
    public static final String P_LAST_NAME = "lastName";
    public static final String P_FIRST_NAME = "firstName";
    public static final String P_MIDDLE_NAME = "middleName";
    public static final String P_PERSON_BENEFITS = "personBenefits";
    public static final String P_DESCRIPTION = "description";
    public static final String L_REASON = "reason";
    public static final String P_REASON_COMMENT = "reasonComment";
    public static final String L_COURSE = "course";
    public static final String L_TERM = "term";
    public static final String P_INSTITUTE = "institute";
    public static final String P_FACULTY = "faculty";
    public static final String P_SPECIALITY = "speciality";
    public static final String P_PROFILE = "profile";
    public static final String P_DOP_EDU_LEVEL = "dopEduLevel";
    public static final String P_ENCOURAGELTY = "encouragelty";
    public static final String P_ENCOURAGELTY_END_DATE = "encourageltyEndDate";
    public static final String P_GRANT_SIZE = "grantSize";
    public static final String P_GRANT_CATEGORY = "grantCategory";
    public static final String P_PRACTICE_TYPE = "practiceType";
    public static final String P_PRACTICE_PLACE = "practicePlace";
    public static final String P_PRACTICE_OWNERSHIP_TYPE = "practiceOwnershipType";
    public static final String P_PRACTICE_SETTLEMENT = "practiceSettlement";
    public static final String P_PRACTICE_PERIOD = "practicePeriod";
    public static final String P_PRACTICE_SUPERVISOR = "practiceSupervisor";
    public static final String P_COMMENT = "comment";

    private AbstractStudentExtract _extract;     // Id выписки
    private AbstractStudentOrder _order;     // Id приказа
    private Student _student;     // Студент
    private String _orderNumber;     // Номер приказа
    private String _orderType;     // Тип приказа
    private String _modularReason;     // Причина индивидуального приказа
    private String _listReason;     // Причина списочного приказа
    private String _listBasics;     // Основания
    private Date _createDate;     // Дата формирования
    private Date _orderDate;     // Дата приказа
    private String _lastName;     // Фамилия студента
    private String _firstName;     // Имя студента
    private String _middleName;     // Отчество студента
    private String _personBenefits;     // Наименование льготы
    private String _description;     // Примечание к приказу
    private StudentOrderReasons _reason;     // Причина выписки
    private String _reasonComment;     // Примечание к причине
    private Course _course;     // Курс
    private Term _term;     // Семестр
    private String _institute;     // Институт
    private String _faculty;     // Факультет/Школа
    private String _speciality;     // Направление (специальность)
    private String _profile;     // Профиль (специализация)
    private String _dopEduLevel;     // Дополнительная образовательная программа / Дополнительная квалификация
    private String _encouragelty;     // Поощрение / взыскание
    private Date _encourageltyEndDate;     // Дата снятия поощрения / взыскания
    private String _grantSize;     // Размер стипендии/выплаты
    private String _grantCategory;     // Категория стипендии/выплаты
    private String _practiceType;     // Вид практики
    private String _practicePlace;     // Место прохождения практики
    private String _practiceOwnershipType;     // Форма собственности предприятия прохождения практики
    private String _practiceSettlement;     // Город прохождения практики
    private String _practicePeriod;     // Сроки практики
    private String _practiceSupervisor;     // Руководитель от вуза
    private String _comment;     // Примечание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Id выписки. Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Id выписки. Свойство не может быть null.
     */
    public void setExtract(AbstractStudentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Id приказа. Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Id приказа. Свойство не может быть null.
     */
    public void setOrder(AbstractStudentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Номер приказа.
     */
    @Length(max=255)
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    /**
     * @param orderNumber Номер приказа.
     */
    public void setOrderNumber(String orderNumber)
    {
        dirty(_orderNumber, orderNumber);
        _orderNumber = orderNumber;
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOrderType()
    {
        return _orderType;
    }

    /**
     * @param orderType Тип приказа. Свойство не может быть null.
     */
    public void setOrderType(String orderType)
    {
        dirty(_orderType, orderType);
        _orderType = orderType;
    }

    /**
     * @return Причина индивидуального приказа.
     */
    @Length(max=255)
    public String getModularReason()
    {
        return _modularReason;
    }

    /**
     * @param modularReason Причина индивидуального приказа.
     */
    public void setModularReason(String modularReason)
    {
        dirty(_modularReason, modularReason);
        _modularReason = modularReason;
    }

    /**
     * @return Причина списочного приказа.
     */
    @Length(max=255)
    public String getListReason()
    {
        return _listReason;
    }

    /**
     * @param listReason Причина списочного приказа.
     */
    public void setListReason(String listReason)
    {
        dirty(_listReason, listReason);
        _listReason = listReason;
    }

    /**
     * @return Основания.
     */
    @Length(max=2048)
    public String getListBasics()
    {
        return _listBasics;
    }

    /**
     * @param listBasics Основания.
     */
    public void setListBasics(String listBasics)
    {
        dirty(_listBasics, listBasics);
        _listBasics = listBasics;
    }

    /**
     * @return Дата формирования.
     */
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Дата приказа.
     */
    public Date getOrderDate()
    {
        return _orderDate;
    }

    /**
     * @param orderDate Дата приказа.
     */
    public void setOrderDate(Date orderDate)
    {
        dirty(_orderDate, orderDate);
        _orderDate = orderDate;
    }

    /**
     * @return Фамилия студента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLastName()
    {
        return _lastName;
    }

    /**
     * @param lastName Фамилия студента. Свойство не может быть null.
     */
    public void setLastName(String lastName)
    {
        dirty(_lastName, lastName);
        _lastName = lastName;
    }

    /**
     * @return Имя студента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFirstName()
    {
        return _firstName;
    }

    /**
     * @param firstName Имя студента. Свойство не может быть null.
     */
    public void setFirstName(String firstName)
    {
        dirty(_firstName, firstName);
        _firstName = firstName;
    }

    /**
     * @return Отчество студента.
     */
    @Length(max=255)
    public String getMiddleName()
    {
        return _middleName;
    }

    /**
     * @param middleName Отчество студента.
     */
    public void setMiddleName(String middleName)
    {
        dirty(_middleName, middleName);
        _middleName = middleName;
    }

    /**
     * @return Наименование льготы.
     */
    @Length(max=255)
    public String getPersonBenefits()
    {
        return _personBenefits;
    }

    /**
     * @param personBenefits Наименование льготы.
     */
    public void setPersonBenefits(String personBenefits)
    {
        dirty(_personBenefits, personBenefits);
        _personBenefits = personBenefits;
    }

    /**
     * @return Примечание к приказу.
     */
    @Length(max=2048)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Примечание к приказу.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Причина выписки.
     */
    public StudentOrderReasons getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина выписки.
     */
    public void setReason(StudentOrderReasons reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Примечание к причине.
     */
    @Length(max=255)
    public String getReasonComment()
    {
        return _reasonComment;
    }

    /**
     * @param reasonComment Примечание к причине.
     */
    public void setReasonComment(String reasonComment)
    {
        dirty(_reasonComment, reasonComment);
        _reasonComment = reasonComment;
    }

    /**
     * @return Курс.
     */
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Семестр.
     */
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Институт.
     */
    @Length(max=255)
    public String getInstitute()
    {
        return _institute;
    }

    /**
     * @param institute Институт.
     */
    public void setInstitute(String institute)
    {
        dirty(_institute, institute);
        _institute = institute;
    }

    /**
     * @return Факультет/Школа.
     */
    @Length(max=255)
    public String getFaculty()
    {
        return _faculty;
    }

    /**
     * @param faculty Факультет/Школа.
     */
    public void setFaculty(String faculty)
    {
        dirty(_faculty, faculty);
        _faculty = faculty;
    }

    /**
     * @return Направление (специальность).
     */
    @Length(max=255)
    public String getSpeciality()
    {
        return _speciality;
    }

    /**
     * @param speciality Направление (специальность).
     */
    public void setSpeciality(String speciality)
    {
        dirty(_speciality, speciality);
        _speciality = speciality;
    }

    /**
     * @return Профиль (специализация).
     */
    @Length(max=255)
    public String getProfile()
    {
        return _profile;
    }

    /**
     * @param profile Профиль (специализация).
     */
    public void setProfile(String profile)
    {
        dirty(_profile, profile);
        _profile = profile;
    }

    /**
     * @return Дополнительная образовательная программа / Дополнительная квалификация.
     */
    @Length(max=255)
    public String getDopEduLevel()
    {
        return _dopEduLevel;
    }

    /**
     * @param dopEduLevel Дополнительная образовательная программа / Дополнительная квалификация.
     */
    public void setDopEduLevel(String dopEduLevel)
    {
        dirty(_dopEduLevel, dopEduLevel);
        _dopEduLevel = dopEduLevel;
    }

    /**
     * @return Поощрение / взыскание.
     */
    @Length(max=255)
    public String getEncouragelty()
    {
        return _encouragelty;
    }

    /**
     * @param encouragelty Поощрение / взыскание.
     */
    public void setEncouragelty(String encouragelty)
    {
        dirty(_encouragelty, encouragelty);
        _encouragelty = encouragelty;
    }

    /**
     * @return Дата снятия поощрения / взыскания.
     */
    public Date getEncourageltyEndDate()
    {
        return _encourageltyEndDate;
    }

    /**
     * @param encourageltyEndDate Дата снятия поощрения / взыскания.
     */
    public void setEncourageltyEndDate(Date encourageltyEndDate)
    {
        dirty(_encourageltyEndDate, encourageltyEndDate);
        _encourageltyEndDate = encourageltyEndDate;
    }

    /**
     * @return Размер стипендии/выплаты.
     */
    @Length(max=255)
    public String getGrantSize()
    {
        return _grantSize;
    }

    /**
     * @param grantSize Размер стипендии/выплаты.
     */
    public void setGrantSize(String grantSize)
    {
        dirty(_grantSize, grantSize);
        _grantSize = grantSize;
    }

    /**
     * @return Категория стипендии/выплаты.
     */
    @Length(max=255)
    public String getGrantCategory()
    {
        return _grantCategory;
    }

    /**
     * @param grantCategory Категория стипендии/выплаты.
     */
    public void setGrantCategory(String grantCategory)
    {
        dirty(_grantCategory, grantCategory);
        _grantCategory = grantCategory;
    }

    /**
     * @return Вид практики.
     */
    @Length(max=255)
    public String getPracticeType()
    {
        return _practiceType;
    }

    /**
     * @param practiceType Вид практики.
     */
    public void setPracticeType(String practiceType)
    {
        dirty(_practiceType, practiceType);
        _practiceType = practiceType;
    }

    /**
     * @return Место прохождения практики.
     */
    @Length(max=255)
    public String getPracticePlace()
    {
        return _practicePlace;
    }

    /**
     * @param practicePlace Место прохождения практики.
     */
    public void setPracticePlace(String practicePlace)
    {
        dirty(_practicePlace, practicePlace);
        _practicePlace = practicePlace;
    }

    /**
     * @return Форма собственности предприятия прохождения практики.
     */
    @Length(max=255)
    public String getPracticeOwnershipType()
    {
        return _practiceOwnershipType;
    }

    /**
     * @param practiceOwnershipType Форма собственности предприятия прохождения практики.
     */
    public void setPracticeOwnershipType(String practiceOwnershipType)
    {
        dirty(_practiceOwnershipType, practiceOwnershipType);
        _practiceOwnershipType = practiceOwnershipType;
    }

    /**
     * @return Город прохождения практики.
     */
    @Length(max=255)
    public String getPracticeSettlement()
    {
        return _practiceSettlement;
    }

    /**
     * @param practiceSettlement Город прохождения практики.
     */
    public void setPracticeSettlement(String practiceSettlement)
    {
        dirty(_practiceSettlement, practiceSettlement);
        _practiceSettlement = practiceSettlement;
    }

    /**
     * @return Сроки практики.
     */
    @Length(max=255)
    public String getPracticePeriod()
    {
        return _practicePeriod;
    }

    /**
     * @param practicePeriod Сроки практики.
     */
    public void setPracticePeriod(String practicePeriod)
    {
        dirty(_practicePeriod, practicePeriod);
        _practicePeriod = practicePeriod;
    }

    /**
     * @return Руководитель от вуза.
     */
    @Length(max=255)
    public String getPracticeSupervisor()
    {
        return _practiceSupervisor;
    }

    /**
     * @param practiceSupervisor Руководитель от вуза.
     */
    public void setPracticeSupervisor(String practiceSupervisor)
    {
        dirty(_practiceSupervisor, practiceSupervisor);
        _practiceSupervisor = practiceSupervisor;
    }

    /**
     * @return Примечание.
     */
    @Length(max=1024)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Примечание.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewPrikazyOtherGen)
        {
            setExtract(((MdbViewPrikazyOther)another).getExtract());
            setOrder(((MdbViewPrikazyOther)another).getOrder());
            setStudent(((MdbViewPrikazyOther)another).getStudent());
            setOrderNumber(((MdbViewPrikazyOther)another).getOrderNumber());
            setOrderType(((MdbViewPrikazyOther)another).getOrderType());
            setModularReason(((MdbViewPrikazyOther)another).getModularReason());
            setListReason(((MdbViewPrikazyOther)another).getListReason());
            setListBasics(((MdbViewPrikazyOther)another).getListBasics());
            setCreateDate(((MdbViewPrikazyOther)another).getCreateDate());
            setOrderDate(((MdbViewPrikazyOther)another).getOrderDate());
            setLastName(((MdbViewPrikazyOther)another).getLastName());
            setFirstName(((MdbViewPrikazyOther)another).getFirstName());
            setMiddleName(((MdbViewPrikazyOther)another).getMiddleName());
            setPersonBenefits(((MdbViewPrikazyOther)another).getPersonBenefits());
            setDescription(((MdbViewPrikazyOther)another).getDescription());
            setReason(((MdbViewPrikazyOther)another).getReason());
            setReasonComment(((MdbViewPrikazyOther)another).getReasonComment());
            setCourse(((MdbViewPrikazyOther)another).getCourse());
            setTerm(((MdbViewPrikazyOther)another).getTerm());
            setInstitute(((MdbViewPrikazyOther)another).getInstitute());
            setFaculty(((MdbViewPrikazyOther)another).getFaculty());
            setSpeciality(((MdbViewPrikazyOther)another).getSpeciality());
            setProfile(((MdbViewPrikazyOther)another).getProfile());
            setDopEduLevel(((MdbViewPrikazyOther)another).getDopEduLevel());
            setEncouragelty(((MdbViewPrikazyOther)another).getEncouragelty());
            setEncourageltyEndDate(((MdbViewPrikazyOther)another).getEncourageltyEndDate());
            setGrantSize(((MdbViewPrikazyOther)another).getGrantSize());
            setGrantCategory(((MdbViewPrikazyOther)another).getGrantCategory());
            setPracticeType(((MdbViewPrikazyOther)another).getPracticeType());
            setPracticePlace(((MdbViewPrikazyOther)another).getPracticePlace());
            setPracticeOwnershipType(((MdbViewPrikazyOther)another).getPracticeOwnershipType());
            setPracticeSettlement(((MdbViewPrikazyOther)another).getPracticeSettlement());
            setPracticePeriod(((MdbViewPrikazyOther)another).getPracticePeriod());
            setPracticeSupervisor(((MdbViewPrikazyOther)another).getPracticeSupervisor());
            setComment(((MdbViewPrikazyOther)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewPrikazyOtherGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewPrikazyOther.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewPrikazyOther();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "order":
                    return obj.getOrder();
                case "student":
                    return obj.getStudent();
                case "orderNumber":
                    return obj.getOrderNumber();
                case "orderType":
                    return obj.getOrderType();
                case "modularReason":
                    return obj.getModularReason();
                case "listReason":
                    return obj.getListReason();
                case "listBasics":
                    return obj.getListBasics();
                case "createDate":
                    return obj.getCreateDate();
                case "orderDate":
                    return obj.getOrderDate();
                case "lastName":
                    return obj.getLastName();
                case "firstName":
                    return obj.getFirstName();
                case "middleName":
                    return obj.getMiddleName();
                case "personBenefits":
                    return obj.getPersonBenefits();
                case "description":
                    return obj.getDescription();
                case "reason":
                    return obj.getReason();
                case "reasonComment":
                    return obj.getReasonComment();
                case "course":
                    return obj.getCourse();
                case "term":
                    return obj.getTerm();
                case "institute":
                    return obj.getInstitute();
                case "faculty":
                    return obj.getFaculty();
                case "speciality":
                    return obj.getSpeciality();
                case "profile":
                    return obj.getProfile();
                case "dopEduLevel":
                    return obj.getDopEduLevel();
                case "encouragelty":
                    return obj.getEncouragelty();
                case "encourageltyEndDate":
                    return obj.getEncourageltyEndDate();
                case "grantSize":
                    return obj.getGrantSize();
                case "grantCategory":
                    return obj.getGrantCategory();
                case "practiceType":
                    return obj.getPracticeType();
                case "practicePlace":
                    return obj.getPracticePlace();
                case "practiceOwnershipType":
                    return obj.getPracticeOwnershipType();
                case "practiceSettlement":
                    return obj.getPracticeSettlement();
                case "practicePeriod":
                    return obj.getPracticePeriod();
                case "practiceSupervisor":
                    return obj.getPracticeSupervisor();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((AbstractStudentExtract) value);
                    return;
                case "order":
                    obj.setOrder((AbstractStudentOrder) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "orderNumber":
                    obj.setOrderNumber((String) value);
                    return;
                case "orderType":
                    obj.setOrderType((String) value);
                    return;
                case "modularReason":
                    obj.setModularReason((String) value);
                    return;
                case "listReason":
                    obj.setListReason((String) value);
                    return;
                case "listBasics":
                    obj.setListBasics((String) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "orderDate":
                    obj.setOrderDate((Date) value);
                    return;
                case "lastName":
                    obj.setLastName((String) value);
                    return;
                case "firstName":
                    obj.setFirstName((String) value);
                    return;
                case "middleName":
                    obj.setMiddleName((String) value);
                    return;
                case "personBenefits":
                    obj.setPersonBenefits((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "reason":
                    obj.setReason((StudentOrderReasons) value);
                    return;
                case "reasonComment":
                    obj.setReasonComment((String) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "institute":
                    obj.setInstitute((String) value);
                    return;
                case "faculty":
                    obj.setFaculty((String) value);
                    return;
                case "speciality":
                    obj.setSpeciality((String) value);
                    return;
                case "profile":
                    obj.setProfile((String) value);
                    return;
                case "dopEduLevel":
                    obj.setDopEduLevel((String) value);
                    return;
                case "encouragelty":
                    obj.setEncouragelty((String) value);
                    return;
                case "encourageltyEndDate":
                    obj.setEncourageltyEndDate((Date) value);
                    return;
                case "grantSize":
                    obj.setGrantSize((String) value);
                    return;
                case "grantCategory":
                    obj.setGrantCategory((String) value);
                    return;
                case "practiceType":
                    obj.setPracticeType((String) value);
                    return;
                case "practicePlace":
                    obj.setPracticePlace((String) value);
                    return;
                case "practiceOwnershipType":
                    obj.setPracticeOwnershipType((String) value);
                    return;
                case "practiceSettlement":
                    obj.setPracticeSettlement((String) value);
                    return;
                case "practicePeriod":
                    obj.setPracticePeriod((String) value);
                    return;
                case "practiceSupervisor":
                    obj.setPracticeSupervisor((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "order":
                        return true;
                case "student":
                        return true;
                case "orderNumber":
                        return true;
                case "orderType":
                        return true;
                case "modularReason":
                        return true;
                case "listReason":
                        return true;
                case "listBasics":
                        return true;
                case "createDate":
                        return true;
                case "orderDate":
                        return true;
                case "lastName":
                        return true;
                case "firstName":
                        return true;
                case "middleName":
                        return true;
                case "personBenefits":
                        return true;
                case "description":
                        return true;
                case "reason":
                        return true;
                case "reasonComment":
                        return true;
                case "course":
                        return true;
                case "term":
                        return true;
                case "institute":
                        return true;
                case "faculty":
                        return true;
                case "speciality":
                        return true;
                case "profile":
                        return true;
                case "dopEduLevel":
                        return true;
                case "encouragelty":
                        return true;
                case "encourageltyEndDate":
                        return true;
                case "grantSize":
                        return true;
                case "grantCategory":
                        return true;
                case "practiceType":
                        return true;
                case "practicePlace":
                        return true;
                case "practiceOwnershipType":
                        return true;
                case "practiceSettlement":
                        return true;
                case "practicePeriod":
                        return true;
                case "practiceSupervisor":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "order":
                    return true;
                case "student":
                    return true;
                case "orderNumber":
                    return true;
                case "orderType":
                    return true;
                case "modularReason":
                    return true;
                case "listReason":
                    return true;
                case "listBasics":
                    return true;
                case "createDate":
                    return true;
                case "orderDate":
                    return true;
                case "lastName":
                    return true;
                case "firstName":
                    return true;
                case "middleName":
                    return true;
                case "personBenefits":
                    return true;
                case "description":
                    return true;
                case "reason":
                    return true;
                case "reasonComment":
                    return true;
                case "course":
                    return true;
                case "term":
                    return true;
                case "institute":
                    return true;
                case "faculty":
                    return true;
                case "speciality":
                    return true;
                case "profile":
                    return true;
                case "dopEduLevel":
                    return true;
                case "encouragelty":
                    return true;
                case "encourageltyEndDate":
                    return true;
                case "grantSize":
                    return true;
                case "grantCategory":
                    return true;
                case "practiceType":
                    return true;
                case "practicePlace":
                    return true;
                case "practiceOwnershipType":
                    return true;
                case "practiceSettlement":
                    return true;
                case "practicePeriod":
                    return true;
                case "practiceSupervisor":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return AbstractStudentExtract.class;
                case "order":
                    return AbstractStudentOrder.class;
                case "student":
                    return Student.class;
                case "orderNumber":
                    return String.class;
                case "orderType":
                    return String.class;
                case "modularReason":
                    return String.class;
                case "listReason":
                    return String.class;
                case "listBasics":
                    return String.class;
                case "createDate":
                    return Date.class;
                case "orderDate":
                    return Date.class;
                case "lastName":
                    return String.class;
                case "firstName":
                    return String.class;
                case "middleName":
                    return String.class;
                case "personBenefits":
                    return String.class;
                case "description":
                    return String.class;
                case "reason":
                    return StudentOrderReasons.class;
                case "reasonComment":
                    return String.class;
                case "course":
                    return Course.class;
                case "term":
                    return Term.class;
                case "institute":
                    return String.class;
                case "faculty":
                    return String.class;
                case "speciality":
                    return String.class;
                case "profile":
                    return String.class;
                case "dopEduLevel":
                    return String.class;
                case "encouragelty":
                    return String.class;
                case "encourageltyEndDate":
                    return Date.class;
                case "grantSize":
                    return String.class;
                case "grantCategory":
                    return String.class;
                case "practiceType":
                    return String.class;
                case "practicePlace":
                    return String.class;
                case "practiceOwnershipType":
                    return String.class;
                case "practiceSettlement":
                    return String.class;
                case "practicePeriod":
                    return String.class;
                case "practiceSupervisor":
                    return String.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewPrikazyOther> _dslPath = new Path<MdbViewPrikazyOther>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewPrikazyOther");
    }
            

    /**
     * @return Id выписки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Id приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getOrder()
     */
    public static AbstractStudentOrder.Path<AbstractStudentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getOrderNumber()
     */
    public static PropertyPath<String> orderNumber()
    {
        return _dslPath.orderNumber();
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getOrderType()
     */
    public static PropertyPath<String> orderType()
    {
        return _dslPath.orderType();
    }

    /**
     * @return Причина индивидуального приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getModularReason()
     */
    public static PropertyPath<String> modularReason()
    {
        return _dslPath.modularReason();
    }

    /**
     * @return Причина списочного приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getListReason()
     */
    public static PropertyPath<String> listReason()
    {
        return _dslPath.listReason();
    }

    /**
     * @return Основания.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getListBasics()
     */
    public static PropertyPath<String> listBasics()
    {
        return _dslPath.listBasics();
    }

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getOrderDate()
     */
    public static PropertyPath<Date> orderDate()
    {
        return _dslPath.orderDate();
    }

    /**
     * @return Фамилия студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getLastName()
     */
    public static PropertyPath<String> lastName()
    {
        return _dslPath.lastName();
    }

    /**
     * @return Имя студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getFirstName()
     */
    public static PropertyPath<String> firstName()
    {
        return _dslPath.firstName();
    }

    /**
     * @return Отчество студента.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getMiddleName()
     */
    public static PropertyPath<String> middleName()
    {
        return _dslPath.middleName();
    }

    /**
     * @return Наименование льготы.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPersonBenefits()
     */
    public static PropertyPath<String> personBenefits()
    {
        return _dslPath.personBenefits();
    }

    /**
     * @return Примечание к приказу.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Причина выписки.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getReason()
     */
    public static StudentOrderReasons.Path<StudentOrderReasons> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Примечание к причине.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getReasonComment()
     */
    public static PropertyPath<String> reasonComment()
    {
        return _dslPath.reasonComment();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Семестр.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Институт.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getInstitute()
     */
    public static PropertyPath<String> institute()
    {
        return _dslPath.institute();
    }

    /**
     * @return Факультет/Школа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getFaculty()
     */
    public static PropertyPath<String> faculty()
    {
        return _dslPath.faculty();
    }

    /**
     * @return Направление (специальность).
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getSpeciality()
     */
    public static PropertyPath<String> speciality()
    {
        return _dslPath.speciality();
    }

    /**
     * @return Профиль (специализация).
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getProfile()
     */
    public static PropertyPath<String> profile()
    {
        return _dslPath.profile();
    }

    /**
     * @return Дополнительная образовательная программа / Дополнительная квалификация.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getDopEduLevel()
     */
    public static PropertyPath<String> dopEduLevel()
    {
        return _dslPath.dopEduLevel();
    }

    /**
     * @return Поощрение / взыскание.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getEncouragelty()
     */
    public static PropertyPath<String> encouragelty()
    {
        return _dslPath.encouragelty();
    }

    /**
     * @return Дата снятия поощрения / взыскания.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getEncourageltyEndDate()
     */
    public static PropertyPath<Date> encourageltyEndDate()
    {
        return _dslPath.encourageltyEndDate();
    }

    /**
     * @return Размер стипендии/выплаты.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getGrantSize()
     */
    public static PropertyPath<String> grantSize()
    {
        return _dslPath.grantSize();
    }

    /**
     * @return Категория стипендии/выплаты.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getGrantCategory()
     */
    public static PropertyPath<String> grantCategory()
    {
        return _dslPath.grantCategory();
    }

    /**
     * @return Вид практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticeType()
     */
    public static PropertyPath<String> practiceType()
    {
        return _dslPath.practiceType();
    }

    /**
     * @return Место прохождения практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticePlace()
     */
    public static PropertyPath<String> practicePlace()
    {
        return _dslPath.practicePlace();
    }

    /**
     * @return Форма собственности предприятия прохождения практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticeOwnershipType()
     */
    public static PropertyPath<String> practiceOwnershipType()
    {
        return _dslPath.practiceOwnershipType();
    }

    /**
     * @return Город прохождения практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticeSettlement()
     */
    public static PropertyPath<String> practiceSettlement()
    {
        return _dslPath.practiceSettlement();
    }

    /**
     * @return Сроки практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticePeriod()
     */
    public static PropertyPath<String> practicePeriod()
    {
        return _dslPath.practicePeriod();
    }

    /**
     * @return Руководитель от вуза.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticeSupervisor()
     */
    public static PropertyPath<String> practiceSupervisor()
    {
        return _dslPath.practiceSupervisor();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends MdbViewPrikazyOther> extends EntityPath<E>
    {
        private AbstractStudentExtract.Path<AbstractStudentExtract> _extract;
        private AbstractStudentOrder.Path<AbstractStudentOrder> _order;
        private Student.Path<Student> _student;
        private PropertyPath<String> _orderNumber;
        private PropertyPath<String> _orderType;
        private PropertyPath<String> _modularReason;
        private PropertyPath<String> _listReason;
        private PropertyPath<String> _listBasics;
        private PropertyPath<Date> _createDate;
        private PropertyPath<Date> _orderDate;
        private PropertyPath<String> _lastName;
        private PropertyPath<String> _firstName;
        private PropertyPath<String> _middleName;
        private PropertyPath<String> _personBenefits;
        private PropertyPath<String> _description;
        private StudentOrderReasons.Path<StudentOrderReasons> _reason;
        private PropertyPath<String> _reasonComment;
        private Course.Path<Course> _course;
        private Term.Path<Term> _term;
        private PropertyPath<String> _institute;
        private PropertyPath<String> _faculty;
        private PropertyPath<String> _speciality;
        private PropertyPath<String> _profile;
        private PropertyPath<String> _dopEduLevel;
        private PropertyPath<String> _encouragelty;
        private PropertyPath<Date> _encourageltyEndDate;
        private PropertyPath<String> _grantSize;
        private PropertyPath<String> _grantCategory;
        private PropertyPath<String> _practiceType;
        private PropertyPath<String> _practicePlace;
        private PropertyPath<String> _practiceOwnershipType;
        private PropertyPath<String> _practiceSettlement;
        private PropertyPath<String> _practicePeriod;
        private PropertyPath<String> _practiceSupervisor;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Id выписки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> extract()
        {
            if(_extract == null )
                _extract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Id приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getOrder()
     */
        public AbstractStudentOrder.Path<AbstractStudentOrder> order()
        {
            if(_order == null )
                _order = new AbstractStudentOrder.Path<AbstractStudentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getOrderNumber()
     */
        public PropertyPath<String> orderNumber()
        {
            if(_orderNumber == null )
                _orderNumber = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_ORDER_NUMBER, this);
            return _orderNumber;
        }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getOrderType()
     */
        public PropertyPath<String> orderType()
        {
            if(_orderType == null )
                _orderType = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_ORDER_TYPE, this);
            return _orderType;
        }

    /**
     * @return Причина индивидуального приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getModularReason()
     */
        public PropertyPath<String> modularReason()
        {
            if(_modularReason == null )
                _modularReason = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_MODULAR_REASON, this);
            return _modularReason;
        }

    /**
     * @return Причина списочного приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getListReason()
     */
        public PropertyPath<String> listReason()
        {
            if(_listReason == null )
                _listReason = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_LIST_REASON, this);
            return _listReason;
        }

    /**
     * @return Основания.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getListBasics()
     */
        public PropertyPath<String> listBasics()
        {
            if(_listBasics == null )
                _listBasics = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_LIST_BASICS, this);
            return _listBasics;
        }

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(MdbViewPrikazyOtherGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getOrderDate()
     */
        public PropertyPath<Date> orderDate()
        {
            if(_orderDate == null )
                _orderDate = new PropertyPath<Date>(MdbViewPrikazyOtherGen.P_ORDER_DATE, this);
            return _orderDate;
        }

    /**
     * @return Фамилия студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getLastName()
     */
        public PropertyPath<String> lastName()
        {
            if(_lastName == null )
                _lastName = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_LAST_NAME, this);
            return _lastName;
        }

    /**
     * @return Имя студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getFirstName()
     */
        public PropertyPath<String> firstName()
        {
            if(_firstName == null )
                _firstName = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_FIRST_NAME, this);
            return _firstName;
        }

    /**
     * @return Отчество студента.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getMiddleName()
     */
        public PropertyPath<String> middleName()
        {
            if(_middleName == null )
                _middleName = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_MIDDLE_NAME, this);
            return _middleName;
        }

    /**
     * @return Наименование льготы.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPersonBenefits()
     */
        public PropertyPath<String> personBenefits()
        {
            if(_personBenefits == null )
                _personBenefits = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_PERSON_BENEFITS, this);
            return _personBenefits;
        }

    /**
     * @return Примечание к приказу.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Причина выписки.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getReason()
     */
        public StudentOrderReasons.Path<StudentOrderReasons> reason()
        {
            if(_reason == null )
                _reason = new StudentOrderReasons.Path<StudentOrderReasons>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Примечание к причине.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getReasonComment()
     */
        public PropertyPath<String> reasonComment()
        {
            if(_reasonComment == null )
                _reasonComment = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_REASON_COMMENT, this);
            return _reasonComment;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Семестр.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Институт.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getInstitute()
     */
        public PropertyPath<String> institute()
        {
            if(_institute == null )
                _institute = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_INSTITUTE, this);
            return _institute;
        }

    /**
     * @return Факультет/Школа.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getFaculty()
     */
        public PropertyPath<String> faculty()
        {
            if(_faculty == null )
                _faculty = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_FACULTY, this);
            return _faculty;
        }

    /**
     * @return Направление (специальность).
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getSpeciality()
     */
        public PropertyPath<String> speciality()
        {
            if(_speciality == null )
                _speciality = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_SPECIALITY, this);
            return _speciality;
        }

    /**
     * @return Профиль (специализация).
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getProfile()
     */
        public PropertyPath<String> profile()
        {
            if(_profile == null )
                _profile = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_PROFILE, this);
            return _profile;
        }

    /**
     * @return Дополнительная образовательная программа / Дополнительная квалификация.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getDopEduLevel()
     */
        public PropertyPath<String> dopEduLevel()
        {
            if(_dopEduLevel == null )
                _dopEduLevel = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_DOP_EDU_LEVEL, this);
            return _dopEduLevel;
        }

    /**
     * @return Поощрение / взыскание.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getEncouragelty()
     */
        public PropertyPath<String> encouragelty()
        {
            if(_encouragelty == null )
                _encouragelty = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_ENCOURAGELTY, this);
            return _encouragelty;
        }

    /**
     * @return Дата снятия поощрения / взыскания.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getEncourageltyEndDate()
     */
        public PropertyPath<Date> encourageltyEndDate()
        {
            if(_encourageltyEndDate == null )
                _encourageltyEndDate = new PropertyPath<Date>(MdbViewPrikazyOtherGen.P_ENCOURAGELTY_END_DATE, this);
            return _encourageltyEndDate;
        }

    /**
     * @return Размер стипендии/выплаты.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getGrantSize()
     */
        public PropertyPath<String> grantSize()
        {
            if(_grantSize == null )
                _grantSize = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_GRANT_SIZE, this);
            return _grantSize;
        }

    /**
     * @return Категория стипендии/выплаты.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getGrantCategory()
     */
        public PropertyPath<String> grantCategory()
        {
            if(_grantCategory == null )
                _grantCategory = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_GRANT_CATEGORY, this);
            return _grantCategory;
        }

    /**
     * @return Вид практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticeType()
     */
        public PropertyPath<String> practiceType()
        {
            if(_practiceType == null )
                _practiceType = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_PRACTICE_TYPE, this);
            return _practiceType;
        }

    /**
     * @return Место прохождения практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticePlace()
     */
        public PropertyPath<String> practicePlace()
        {
            if(_practicePlace == null )
                _practicePlace = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_PRACTICE_PLACE, this);
            return _practicePlace;
        }

    /**
     * @return Форма собственности предприятия прохождения практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticeOwnershipType()
     */
        public PropertyPath<String> practiceOwnershipType()
        {
            if(_practiceOwnershipType == null )
                _practiceOwnershipType = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_PRACTICE_OWNERSHIP_TYPE, this);
            return _practiceOwnershipType;
        }

    /**
     * @return Город прохождения практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticeSettlement()
     */
        public PropertyPath<String> practiceSettlement()
        {
            if(_practiceSettlement == null )
                _practiceSettlement = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_PRACTICE_SETTLEMENT, this);
            return _practiceSettlement;
        }

    /**
     * @return Сроки практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticePeriod()
     */
        public PropertyPath<String> practicePeriod()
        {
            if(_practicePeriod == null )
                _practicePeriod = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_PRACTICE_PERIOD, this);
            return _practicePeriod;
        }

    /**
     * @return Руководитель от вуза.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getPracticeSupervisor()
     */
        public PropertyPath<String> practiceSupervisor()
        {
            if(_practiceSupervisor == null )
                _practiceSupervisor = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_PRACTICE_SUPERVISOR, this);
            return _practiceSupervisor;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unifefu.entity.MdbViewPrikazyOther#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(MdbViewPrikazyOtherGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return MdbViewPrikazyOther.class;
        }

        public String getEntityName()
        {
            return "mdbViewPrikazyOther";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
