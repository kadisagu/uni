package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О смене условий освоения (перевод на обучение в ускоренные сроки)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuTransfAcceleratedTimeStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract";
    public static final String ENTITY_NAME = "fefuTransfAcceleratedTimeStuListExtract";
    public static final int VERSION_HASH = -2003178805;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP_OLD = "groupOld";
    public static final String L_GROUP_NEW = "groupNew";
    public static final String L_EDUCATION_ORG_UNIT_NEW = "educationOrgUnitNew";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";
    public static final String P_SEASON = "season";
    public static final String P_PLANNED_DATE = "plannedDate";

    private Course _course;     // Курс
    private Group _groupOld;     // Группа старая
    private Group _groupNew;     // Группа новая
    private EducationOrgUnit _educationOrgUnitNew;     // Параметры обучения студентов по направлению подготовки (НПП)
    private EducationOrgUnit _educationOrgUnitOld;     // Параметры обучения студентов по направлению подготовки (НПП)
    private String _season;     // Сезон планируемого выпуска
    private Date _plannedDate;     // Планируемая дата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа старая. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupOld()
    {
        return _groupOld;
    }

    /**
     * @param groupOld Группа старая. Свойство не может быть null.
     */
    public void setGroupOld(Group groupOld)
    {
        dirty(_groupOld, groupOld);
        _groupOld = groupOld;
    }

    /**
     * @return Группа новая. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupNew()
    {
        return _groupNew;
    }

    /**
     * @param groupNew Группа новая. Свойство не может быть null.
     */
    public void setGroupNew(Group groupNew)
    {
        dirty(_groupNew, groupNew);
        _groupNew = groupNew;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    /**
     * @param educationOrgUnitNew Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        dirty(_educationOrgUnitNew, educationOrgUnitNew);
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    /**
     * @return Сезон планируемого выпуска.
     */
    @Length(max=255)
    public String getSeason()
    {
        return _season;
    }

    /**
     * @param season Сезон планируемого выпуска.
     */
    public void setSeason(String season)
    {
        dirty(_season, season);
        _season = season;
    }

    /**
     * @return Планируемая дата.
     */
    public Date getPlannedDate()
    {
        return _plannedDate;
    }

    /**
     * @param plannedDate Планируемая дата.
     */
    public void setPlannedDate(Date plannedDate)
    {
        dirty(_plannedDate, plannedDate);
        _plannedDate = plannedDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuTransfAcceleratedTimeStuListExtractGen)
        {
            setCourse(((FefuTransfAcceleratedTimeStuListExtract)another).getCourse());
            setGroupOld(((FefuTransfAcceleratedTimeStuListExtract)another).getGroupOld());
            setGroupNew(((FefuTransfAcceleratedTimeStuListExtract)another).getGroupNew());
            setEducationOrgUnitNew(((FefuTransfAcceleratedTimeStuListExtract)another).getEducationOrgUnitNew());
            setEducationOrgUnitOld(((FefuTransfAcceleratedTimeStuListExtract)another).getEducationOrgUnitOld());
            setSeason(((FefuTransfAcceleratedTimeStuListExtract)another).getSeason());
            setPlannedDate(((FefuTransfAcceleratedTimeStuListExtract)another).getPlannedDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuTransfAcceleratedTimeStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuTransfAcceleratedTimeStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuTransfAcceleratedTimeStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "groupOld":
                    return obj.getGroupOld();
                case "groupNew":
                    return obj.getGroupNew();
                case "educationOrgUnitNew":
                    return obj.getEducationOrgUnitNew();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
                case "season":
                    return obj.getSeason();
                case "plannedDate":
                    return obj.getPlannedDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "groupOld":
                    obj.setGroupOld((Group) value);
                    return;
                case "groupNew":
                    obj.setGroupNew((Group) value);
                    return;
                case "educationOrgUnitNew":
                    obj.setEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
                case "season":
                    obj.setSeason((String) value);
                    return;
                case "plannedDate":
                    obj.setPlannedDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "groupOld":
                        return true;
                case "groupNew":
                        return true;
                case "educationOrgUnitNew":
                        return true;
                case "educationOrgUnitOld":
                        return true;
                case "season":
                        return true;
                case "plannedDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "groupOld":
                    return true;
                case "groupNew":
                    return true;
                case "educationOrgUnitNew":
                    return true;
                case "educationOrgUnitOld":
                    return true;
                case "season":
                    return true;
                case "plannedDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "groupOld":
                    return Group.class;
                case "groupNew":
                    return Group.class;
                case "educationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
                case "season":
                    return String.class;
                case "plannedDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuTransfAcceleratedTimeStuListExtract> _dslPath = new Path<FefuTransfAcceleratedTimeStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuTransfAcceleratedTimeStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа старая. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getGroupOld()
     */
    public static Group.Path<Group> groupOld()
    {
        return _dslPath.groupOld();
    }

    /**
     * @return Группа новая. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getGroupNew()
     */
    public static Group.Path<Group> groupNew()
    {
        return _dslPath.groupNew();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
    {
        return _dslPath.educationOrgUnitNew();
    }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    /**
     * @return Сезон планируемого выпуска.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getSeason()
     */
    public static PropertyPath<String> season()
    {
        return _dslPath.season();
    }

    /**
     * @return Планируемая дата.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getPlannedDate()
     */
    public static PropertyPath<Date> plannedDate()
    {
        return _dslPath.plannedDate();
    }

    public static class Path<E extends FefuTransfAcceleratedTimeStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _groupOld;
        private Group.Path<Group> _groupNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;
        private PropertyPath<String> _season;
        private PropertyPath<Date> _plannedDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа старая. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getGroupOld()
     */
        public Group.Path<Group> groupOld()
        {
            if(_groupOld == null )
                _groupOld = new Group.Path<Group>(L_GROUP_OLD, this);
            return _groupOld;
        }

    /**
     * @return Группа новая. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getGroupNew()
     */
        public Group.Path<Group> groupNew()
        {
            if(_groupNew == null )
                _groupNew = new Group.Path<Group>(L_GROUP_NEW, this);
            return _groupNew;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
        {
            if(_educationOrgUnitNew == null )
                _educationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_NEW, this);
            return _educationOrgUnitNew;
        }

    /**
     * @return Параметры обучения студентов по направлению подготовки (НПП). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

    /**
     * @return Сезон планируемого выпуска.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getSeason()
     */
        public PropertyPath<String> season()
        {
            if(_season == null )
                _season = new PropertyPath<String>(FefuTransfAcceleratedTimeStuListExtractGen.P_SEASON, this);
            return _season;
        }

    /**
     * @return Планируемая дата.
     * @see ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract#getPlannedDate()
     */
        public PropertyPath<Date> plannedDate()
        {
            if(_plannedDate == null )
                _plannedDate = new PropertyPath<Date>(FefuTransfAcceleratedTimeStuListExtractGen.P_PLANNED_DATE, this);
            return _plannedDate;
        }

        public Class getEntityClass()
        {
            return FefuTransfAcceleratedTimeStuListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuTransfAcceleratedTimeStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
