/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleSession.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.*;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.FefuScheduleManager;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.utils.fefuICal.FefuScheduleICalXST;
import ru.tandemservice.unifefu.utils.fefuICal.FefuScheduleVEventICalXST;
import ru.tandemservice.unifefu.utils.fefuICal.FefuTeacherScheduleICalXST;
import ru.tandemservice.unifefu.utils.fefuICal.FefuTeacherScheduleVEventICalXST;
import ru.tandemservice.unifefu.ws.eventsAxis2.FefuScheduleICalCommits;
import ru.tandemservice.unifefu.ws.eventsAxis2.FefuSubmitEventsClient;
import ru.tandemservice.unifefu.ws.eventsAxis2.FefuTeacherScheduleICalCommits;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionDAO;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionPrintFormExcelContentBuilder;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionPrintParams;
import ru.tandemservice.unispp.base.entity.SppScheduleICal;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionEvent;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionPrintForm;
import ru.tandemservice.unispp.base.entity.catalog.codes.SppScheduleStatusCodes;
import ru.tandemservice.unispp.base.vo.SppScheduleSessionGroupPrintVO;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Victor Nekrasov
 * @since 14.06.2014
 */
public class FefuScheduleSessionDAO extends SppScheduleSessionDAO
{
    @Override
    public void sendICalendars()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleSession.class, "s");
        builder.column(property("s", SppScheduleSession.id()));
        builder.where(eq(property("s", SppScheduleSession.approved()), value(true)));
        builder.where(eq(property("s", SppScheduleSession.group().archival()), value(false)));
        org.joda.time.DateTime date = new org.joda.time.DateTime().withTime(0, 0, 0, 1);
        builder.where(and(
                le(property("s", SppScheduleSession.season().startDate()), valueDate(date.minusMonths(1).toDate())),
                ge(property("s", SppScheduleSession.season().endDate()), valueDate(date.toDate()))
        ));

        List<Long> fefuSchedulesIds = createStatement(builder).list();
        for (Long scheduleId : fefuSchedulesIds)
        {
            sendICalendar(scheduleId);
        }
    }

    @Override
        public boolean getScheduleDataOrRecipientsChanged(Long scheduleId)
        {
            SppScheduleSession schedule = DataAccessServices.dao().get(scheduleId);
            if(null == schedule.getIcalData()) return true;
            if(null == schedule.getIcalTeacherData()) return true;
            if(schedule.getIcalData().isChanged()) return true;
            if(schedule.getIcalTeacherData().isChanged()) return true;

            boolean recipientsChanged = false;
            List<String> currentNsiIds =
            FefuScheduleManager.instance().eventsDao().
                    getCurrentNsiIds(schedule.getGroup().getId());
//                    getCurrentTestNsiIds();

            FefuScheduleICalXST scheduleICalXST = (FefuScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(schedule.getIcalData().getXml());
            List<String> oldNsiIds = scheduleICalXST.getStudentsNSIIds();

            for(String guid : currentNsiIds)
            {
                if(!oldNsiIds.contains(guid))
                {
                    recipientsChanged = true;
                    break;
                }
            }
            if(recipientsChanged) return true;

            for(String guid : oldNsiIds)
            {
                if(!currentNsiIds.contains(guid))
                {
                    recipientsChanged = true;
                    break;
                }
            }

            return recipientsChanged;
        }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
        @Override
        public void sendICalendar(Long scheduleId)
        {

            NamedSyncInTransactionCheckLocker.register(getSession().getTransaction(), "fefu.schedule.session.ws.export.lock_" + scheduleId, 300);
            if(null == ApplicationRuntime.getProperty(FefuSubmitEventsClient.PORTAL_SERVICE_EVENTS_URL))
            {
                throw new ApplicationException("Приложение не настроено для работы с веб-сервисом.");
            }
            SppScheduleSession schedule = DataAccessServices.dao().get(scheduleId);

            sendStudentsCalendar(schedule);
            sendTeacherCalendar(schedule);
        }

    private void sendTeacherCalendar(SppScheduleSession schedule)
    {
        if(null != schedule.getIcalTeacherData() && !schedule.getIcalTeacherData().isChanged()) return;

        DQLSelectBuilder periodBuilder = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "ev");
        periodBuilder.where(eq(property("ev", SppScheduleSessionEvent.schedule().id()), value(schedule.getId())));

        List<SppScheduleSessionEvent> events = createStatement(periodBuilder).list();

        // находим guid преподавателей
        List<Long> employeePostIds = Lists.newArrayList();

        for(SppScheduleSessionEvent event : events)
        {
            if(event.getTeacherVal() != null)
            {
                employeePostIds.add(event.getTeacherVal().getPerson().getId());
            }
        }

        // employeePostId -> guid
        final Map<Long, String> teacherGuids = Maps.newHashMap();

        // test
//        for(Long emplId : employeePostIds)
//        {
//            teacherGuids.put(emplId, getCurrentTestNsiIds().get(0));
//        }
        // test


        BatchUtils.execute(employeePostIds, 100, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                List<FefuNsiIds> nsiIdses = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "fn")
                        .where(in(property("fn", FefuNsiIds.entityId()), elements)).createStatement(getSession()).list();
                for (FefuNsiIds nsiIds : nsiIdses)
                {
                    teacherGuids.put(nsiIds.getEntityId(), nsiIds.getGuid());
                }
            }
        });

        List<FefuTeacherScheduleVEventICalXST> currentEventVEvents = getTeacherEventVEvents(events, teacherGuids);

        // находим события для отмены
        List<FefuTeacherScheduleVEventICalXST> cancelVEvents = Lists.newArrayList();

        SppScheduleICal iCalData = schedule.getIcalTeacherData();

        FefuTeacherScheduleICalXST scheduleICalXST = iCalData != null ? (FefuTeacherScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml()) : new FefuTeacherScheduleICalXST();

        if(iCalData != null)
        {
            scheduleICalXST = (FefuTeacherScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml());

            if(scheduleICalXST.getEvents() != null && !scheduleICalXST.getEvents().isEmpty())
            {
                for (FefuTeacherScheduleVEventICalXST event : scheduleICalXST.getEvents())
                {
                    if(!containsTeacherVEvents(event, currentEventVEvents))
                        cancelVEvents.add(event);
                }
            }
        }
        else
        {
            iCalData = new SppScheduleICal();
            iCalData.setSequence(0);
        }
        // nsiId -> list events
        Map<String, List<FefuTeacherScheduleVEventICalXST>> currentEventsTeacherMap = Maps.newHashMap();

        // nsiId -> list events
        Map<String, List<FefuTeacherScheduleVEventICalXST>> cancelEventsTeacherMap = Maps.newHashMap();

        for(FefuTeacherScheduleVEventICalXST event : currentEventVEvents)
        {
            SafeMap.safeGet(currentEventsTeacherMap, event.getTeacherNSIId(), ArrayList.class).add(event);
        }

        for(FefuTeacherScheduleVEventICalXST event : cancelVEvents)
        {
            SafeMap.safeGet(cancelEventsTeacherMap, event.getTeacherNSIId(), ArrayList.class).add(event);
        }

        int sequence = iCalData.getSequence() + 1;

        List<FefuTeacherScheduleVEventICalXST> commitedVEvents = Lists.newArrayList();

        boolean iCalDataChanged = false;

        for(Map.Entry<String, List<FefuTeacherScheduleVEventICalXST>> entry : cancelEventsTeacherMap.entrySet())
        {

            String iCalendar = getTeacherICalendar(entry.getValue(), schedule, sequence, true);
            FefuTeacherScheduleICalCommits publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), null, Lists.newArrayList(entry.getKey()), iCalendar, getTeacherVEventsMap(entry.getValue()));

            // если фейл, то события необходимо оставить.
            if(!publishRequest.getFailedVEvents().isEmpty())
            {
                commitedVEvents.addAll(publishRequest.getFailedVEvents());
                iCalDataChanged = true;
            }
        }

        for(Map.Entry<String, List<FefuTeacherScheduleVEventICalXST>> entry : currentEventsTeacherMap.entrySet())
        {

            String iCalendar = getTeacherICalendar(entry.getValue(), schedule, sequence, false);
            FefuTeacherScheduleICalCommits publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), "Расписание преподавателя", Lists.newArrayList(entry.getKey()), iCalendar, getTeacherVEventsMap(entry.getValue()));

            if(publishRequest.getFailedVEvents().isEmpty())
            {
                commitedVEvents.addAll(entry.getValue());
            }
            else
            {
                if(!publishRequest.getCommitedVEvents().isEmpty())
                    commitedVEvents.addAll(publishRequest.getCommitedVEvents());
                iCalDataChanged = true;
            }
        }

        if(!commitedVEvents.isEmpty())
        {
            iCalData.setChanged(iCalDataChanged);
            iCalData.setSequence(sequence);
            scheduleICalXST.setEvents(commitedVEvents);
            iCalData.setXml(FefuScheduleManager.instance().xStream().toXML(scheduleICalXST));
            save(iCalData);
            schedule.setIcalTeacherData(iCalData);
            update(schedule);
        }
    }

    private FefuTeacherScheduleICalCommits getTeacherVEventsMap(List<FefuTeacherScheduleVEventICalXST> events)
    {
        FefuTeacherScheduleICalCommits iCalCommits = new FefuTeacherScheduleICalCommits();
        Map<String, FefuTeacherScheduleVEventICalXST> vEventsMap = Maps.newHashMap();
        for(FefuTeacherScheduleVEventICalXST vEvent : events)
        {
            vEventsMap.put(vEvent.getUidPrefix() + "@" + vEvent.getPairId(), vEvent);
        }
        iCalCommits.setvEventsMap(vEventsMap);
        iCalCommits.setCommitedVEvents(Lists.<FefuTeacherScheduleVEventICalXST>newArrayList());
        iCalCommits.setFailedVEvents(Lists.<FefuTeacherScheduleVEventICalXST>newArrayList());
        return iCalCommits;
    }

    public boolean containsTeacherVEvents(FefuTeacherScheduleVEventICalXST o, List<FefuTeacherScheduleVEventICalXST> list)
    {
        boolean contains = false;
        for(FefuTeacherScheduleVEventICalXST event : list)
        {
            if(equalsTeacherVEvents(event, o))
            {
                contains = true;
                break;
            }
        }
        return contains;
    }


    public boolean equalsTeacherVEvents(FefuTeacherScheduleVEventICalXST o1, FefuTeacherScheduleVEventICalXST o2)
    {
        if(o1 == null && o2 == null) return true;
        if (o1 == null || o2 == null) return false;

        return com.google.common.base.Objects.equal(o1.getPairId(), o2.getPairId()) &&
                com.google.common.base.Objects.equal(o1.getUidPrefix(), o2.getUidPrefix()) &&
                com.google.common.base.Objects.equal(o1.getStartDate(), o2.getStartDate()) &&
                com.google.common.base.Objects.equal(o1.getEndDate(), o2.getEndDate()) &&
                com.google.common.base.Objects.equal(o1.getPeriodGroup(), o2.getPeriodGroup()) &&
                com.google.common.base.Objects.equal(o1.getSubject(), o2.getSubject()) &&
                com.google.common.base.Objects.equal(o1.getTeacherNSIId(), o2.getTeacherNSIId()) &&
                com.google.common.base.Objects.equal(o1.getLectureRoom(), o2.getLectureRoom());
    }

    private String getTeacherICalendar(List<FefuTeacherScheduleVEventICalXST> vEvents, SppScheduleSession schedule, int sequence, boolean cancel)
    {
        //        List<SppScheduleVEventICalXST> vEvents = scheduleICalXST.getEvents();

        // Create a calendar
        net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
        icsCalendar.getProperties().add(Version.VERSION_2_0);
        icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
        Method method = cancel ? Method.CANCEL : Method.PUBLISH;
        icsCalendar.getProperties().add(CalScale.GREGORIAN);
        icsCalendar.getProperties().add(method);

        TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
        net.fortuna.ical4j.model.TimeZone timezone = registry.getTimeZone(java.util.Calendar.getInstance().getTimeZone().getID());
        VTimeZone tz = timezone.getVTimeZone();

        for(FefuTeacherScheduleVEventICalXST vEvent : vEvents)
        {
            // Create the event
            String eventName = vEvent.getSubject();
            DateTime start = new DateTime(vEvent.getStartDate());
            Calendar c = GregorianCalendar.getInstance();
            c.setTime(vEvent.getStartDate());
            c.add(Calendar.HOUR_OF_DAY, 2);
            DateTime end = new DateTime(c.getTime());
            VEvent meeting = new VEvent(start, end, eventName);

            StringBuilder description = new StringBuilder();
            if(!StringUtils.isEmpty(vEvent.getPeriodGroup()))
                description.append("Подгруппа - ").append(vEvent.getPeriodGroup());
            description.append(StringUtils.isEmpty(description.toString()) ? "" : " ").append("Группа - " + schedule.getGroup().getTitle());

            if(!StringUtils.isEmpty(vEvent.getLectureRoom()))
                description.append(StringUtils.isEmpty(description.toString()) ? "" : " ").append("Аудитория - ").append(vEvent.getLectureRoom());
            if(!StringUtils.isEmpty(vEvent.getTeacher()))
                description.append(StringUtils.isEmpty(description.toString()) ? "" : " ").append("Преподаватель - ").append(vEvent.getTeacher());


            Description desc = new Description(description.toString());
            if(null != schedule)
            {
                Categories cat = new Categories("Расписание преподавателя");
                meeting.getProperties().add(cat);
            }
            else
            {
                Categories cat = new Categories("Отмена расписания сессии");
                meeting.getProperties().add(cat);
            }
            meeting.getProperties().add(tz.getTimeZoneId());

            meeting.getProperties().add(desc);
            Uid uid = new Uid(vEvent.getUidPrefix() + "@" + vEvent.getPairId());
            Sequence seq = new Sequence(sequence);
            meeting.getProperties().add(uid);
            meeting.getProperties().add(seq);
            icsCalendar.getComponents().add(meeting);
        }

        return icsCalendar.toString();


    }

    private void sendStudentsCalendar(SppScheduleSession schedule)
    {
        // Проверяем изменился ли список получателей для данного расписания
        List<String> guidsForAdd = Lists.newArrayList();
        List<String> guidsForCancel = Lists.newArrayList();
        List<String> currentNsiIds =
                FefuScheduleManager.instance().eventsDao().
                        getCurrentNsiIds(schedule.getGroup().getId());
//                    getCurrentTestNsiIds();

        if(null != schedule.getIcalData())
        {
            SppScheduleICal iCalData = schedule.getIcalData();
            FefuScheduleICalXST scheduleICalXST = (FefuScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml());

            List<String> previousNsiIds = scheduleICalXST.getStudentsNSIIds();

            for(String guid : previousNsiIds)
            {
                if(!currentNsiIds.contains(guid))
                    guidsForCancel.add(guid);
            }

            for(String guid : currentNsiIds)
            {
                if(!previousNsiIds.contains(guid))
                    guidsForAdd.add(guid);
            }
        }

        boolean recipientsNotChanged = guidsForAdd.isEmpty() && guidsForCancel.isEmpty();

        if(null != schedule.getIcalData() && !schedule.getIcalData().isChanged() && recipientsNotChanged) return;

        if(!recipientsNotChanged && (!schedule.getIcalData().isChanged() || currentNsiIds.isEmpty()))
        {
            sendICalendarWithChangesRecipientList(schedule, guidsForAdd, guidsForCancel, currentNsiIds);
            return;
        }
        if(currentNsiIds.isEmpty()) return;

        DQLSelectBuilder periodBuilder = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "ev");
        periodBuilder.where(eq(property("ev", SppScheduleSessionEvent.schedule().id()), value(schedule.getId())));

        List<SppScheduleSessionEvent> events = createStatement(periodBuilder).list();

        Date endDate = schedule.getSeason().getEndDate();
        Date startDate = schedule.getSeason().getStartDate();

        Map<String, LocalDate> datesMap = FefuScheduleManager.instance().eventsDao().getDatesMap(startDate, endDate);

        List<FefuScheduleVEventICalXST> currentEventVEvents = getEventVEvents(events);

        if(null == schedule.getIcalData())
        {
            if(currentEventVEvents.isEmpty()) return;
            sendICalendarWithNullICallData(schedule, currentEventVEvents, currentNsiIds);
        }
        else
        {
            sendICalendarWithChangedICallData(schedule, currentEventVEvents, guidsForAdd, guidsForCancel, currentNsiIds);
        }
    }


    private void sendICalendarWithChangesRecipientList(SppScheduleSession schedule, List<String> guidsForAdd, List<String> guidsForCancel, List<String> currentNsiIds)
    {

        SppScheduleICal iCalData = schedule.getIcalData();
        FefuScheduleICalXST scheduleICalXST = (FefuScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml());
        FefuScheduleICalCommits cancelRequest = getVEventsMap(scheduleICalXST.getEvents());

        if(!guidsForCancel.isEmpty())
        {
            int sequence = iCalData.getSequence();
            String iCalendar = getICalendar(scheduleICalXST.getEvents(), schedule, ++sequence, true);
            cancelRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), guidsForCancel, iCalendar, cancelRequest);
            if(!cancelRequest.isRequestFailed())
            {
                iCalData.setSequence(sequence);
                update(iCalData);
            }
        }
        else
        {
            cancelRequest.setRequestFailed(false);
        }

        if(cancelRequest.isRequestFailed()) return;


        FefuScheduleICalCommits publishRequest = getVEventsMap(scheduleICalXST.getEvents());
        if(!guidsForAdd.isEmpty())
        {
            int sequence = iCalData.getSequence();
            String iCalendar = getICalendar(scheduleICalXST.getEvents(), schedule, ++sequence, false);
            publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), currentNsiIds, iCalendar, publishRequest);
            if(!publishRequest.isRequestFailed())
            {
                iCalData.setSequence(sequence);
                update(iCalData);
            }
            else return;
        }
        scheduleICalXST.setStudentsNSIIds(currentNsiIds);
        scheduleICalXST.setEvents(publishRequest.getCommitedVEvents());
        iCalData.setXml(FefuScheduleManager.instance().xStream().toXML(scheduleICalXST));
        update(iCalData);

    }


    private FefuScheduleICalCommits getVEventsMap(List<FefuScheduleVEventICalXST> events)
    {
        FefuScheduleICalCommits iCalCommits = new FefuScheduleICalCommits();
        Map<String, FefuScheduleVEventICalXST> vEventsMap = Maps.newHashMap();
        for(FefuScheduleVEventICalXST vEvent : events)
        {
            vEventsMap.put(vEvent.getUidPrefix() + "@" + vEvent.getPairId(), vEvent);
        }
        iCalCommits.setvEventsMap(vEventsMap);
        iCalCommits.setCommitedVEvents(Lists.<FefuScheduleVEventICalXST>newArrayList());
        iCalCommits.setFailedVEvents(Lists.<FefuScheduleVEventICalXST>newArrayList());
        return iCalCommits;
    }


    private String getICalendar(List<FefuScheduleVEventICalXST> vEvents, SppScheduleSession schedule, int sequence, boolean cancel)
    {
//        List<SppScheduleVEventICalXST> vEvents = scheduleICalXST.getEvents();

        // Create a calendar
        net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
        icsCalendar.getProperties().add(Version.VERSION_2_0);
        icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
        Method method = cancel ? Method.CANCEL : Method.PUBLISH;
        icsCalendar.getProperties().add(CalScale.GREGORIAN);
        icsCalendar.getProperties().add(method);

        TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
        net.fortuna.ical4j.model.TimeZone timezone = registry.getTimeZone(java.util.Calendar.getInstance().getTimeZone().getID());
        VTimeZone tz = timezone.getVTimeZone();

        for(FefuScheduleVEventICalXST vEvent : vEvents)
        {
            // Create the event
            String eventName = vEvent.getSubject();
            DateTime start = new DateTime(vEvent.getStartDate());
            Calendar c = GregorianCalendar.getInstance();
            c.setTime(vEvent.getStartDate());
            c.add(Calendar.HOUR_OF_DAY, 2);
            DateTime end = new DateTime(c.getTime());
            VEvent meeting = new VEvent(start, end, eventName);
            Description desc = new Description((StringUtils.isEmpty(vEvent.getPeriodGroup()) ? "" : "Аудитория - " + vEvent.getLectureRoom() + " ") + "Преподаватель - " + vEvent.getTeacher());
            if(null != schedule)
            {
                Categories cat = new Categories("Расписание сессии для группы " + schedule.getGroup().getTitle());
                meeting.getProperties().add(cat);
            }
            else
            {
                Categories cat = new Categories("Отмена расписания сессии");
                meeting.getProperties().add(cat);
            }
            meeting.getProperties().add(tz.getTimeZoneId());

            meeting.getProperties().add(desc);
            Uid uid = new Uid(vEvent.getUidPrefix() + "@" + vEvent.getPairId());
            Sequence seq = new Sequence(sequence);
            meeting.getProperties().add(uid);
            meeting.getProperties().add(seq);
            icsCalendar.getComponents().add(meeting);
        }

        return icsCalendar.toString();
    }


    private List<FefuScheduleVEventICalXST> getEventVEvents(List<SppScheduleSessionEvent> events)
    {
        List<FefuScheduleVEventICalXST> eventVEvents = Lists.newArrayList();
        Calendar calendar = GregorianCalendar.getInstance();
        for(SppScheduleSessionEvent event : events)
        {
            calendar.setTime(event.getDate());
            FefuScheduleVEventICalXST vEvent = new FefuScheduleVEventICalXST();
            vEvent.setPairId(event.getId());
            vEvent.setUidPrefix(calendar.get(Calendar.DAY_OF_MONTH) + "_" + (calendar.get(Calendar.MONTH) + 1) + "_" + calendar.get(Calendar.YEAR));
            vEvent.setStartDate(event.getDate());
            vEvent.setSubject(event.getSubject());
            vEvent.setLectureRoom(event.getLectureRoom());
            vEvent.setTeacher(event.getTeacher());
            eventVEvents.add(vEvent);
        }
        return eventVEvents;
    }

    private List<FefuTeacherScheduleVEventICalXST> getTeacherEventVEvents(List<SppScheduleSessionEvent> events, Map<Long, String> teacherGuids)
    {
        List<FefuTeacherScheduleVEventICalXST> eventVEvents = Lists.newArrayList();
        Calendar calendar = GregorianCalendar.getInstance();
        for(SppScheduleSessionEvent event : events)
        {
            if(teacherGuids.get(event.getTeacherVal().getPerson().getId()) != null)
            {
                calendar.setTime(event.getDate());
                FefuTeacherScheduleVEventICalXST vEvent = new FefuTeacherScheduleVEventICalXST();
                vEvent.setPairId(event.getId());
                vEvent.setUidPrefix(calendar.get(Calendar.DAY_OF_MONTH) + "_" + (calendar.get(Calendar.MONTH) + 1) + "_" + calendar.get(Calendar.YEAR));
                vEvent.setStartDate(event.getDate());
                vEvent.setSubject(event.getSubject());
                vEvent.setLectureRoom(event.getLectureRoom());
                vEvent.setTeacher(event.getTeacher());
                vEvent.setTeacherNSIId(teacherGuids.get(event.getTeacherVal().getPerson().getId()));
                eventVEvents.add(vEvent);
            }
        }
        return eventVEvents;
    }


    private Date getStartDate(LocalDate date, ScheduleBellEntry bell)
    {
        org.joda.time.DateTime dateTime = date.toDateTimeAtCurrentTime();
        return dateTime.withHourOfDay(bell.getStartHour()).withMinuteOfHour(bell.getStartMin()).toDate();
    }

    private Date getEndDate(LocalDate date, ScheduleBellEntry bell)
    {
        org.joda.time.DateTime dateTime = date.toDateTimeAtCurrentTime();
        return dateTime.withHourOfDay(bell.getEndHour()).withMinuteOfHour(bell.getEndMin()).toDate();
    }

    private void sendICalendarWithNullICallData(SppScheduleSession schedule, List<FefuScheduleVEventICalXST> currentVEvents, List<String> currentNsiIds)
    {
        FefuScheduleICalXST scheduleICalXST = new FefuScheduleICalXST();
        scheduleICalXST.setStudentsNSIIds(currentNsiIds);
        int sequence = 1;
        String iCalendar = getICalendar(currentVEvents, schedule, sequence, false);
        FefuScheduleICalCommits publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), currentNsiIds, iCalendar, getVEventsMap(currentVEvents));
        if(!publishRequest.isRequestFailed())
        {
            SppScheduleICal iCalData = new SppScheduleICal();
            iCalData.setChanged(false);
            iCalData.setSequence(sequence);
            scheduleICalXST.setEvents(publishRequest.getCommitedVEvents());
            iCalData.setXml(FefuScheduleManager.instance().xStream().toXML(scheduleICalXST));
            save(iCalData);
            schedule.setIcalData(iCalData);
            update(schedule);
        }
    }

    private void sendICalendarWithChangedICallData(SppScheduleSession schedule, List<FefuScheduleVEventICalXST> currentVEvents, List<String> guidsForAdd, List<String> guidsForCancel, List<String> currentNsiIds)
    {

        SppScheduleICal iCalData = schedule.getIcalData();
        FefuScheduleICalXST scheduleICalXST = (FefuScheduleICalXST) FefuScheduleManager.instance().xStream().fromXML(iCalData.getXml());

        List<FefuScheduleVEventICalXST> oldVEvents = scheduleICalXST.getEvents();

        List<FefuScheduleVEventICalXST> vEventsForCancel = Lists.newArrayList();
        List<FefuScheduleVEventICalXST> vEventsForAdd = Lists.newArrayList();

        Map<String, FefuScheduleVEventICalXST> currentVEventsMap = Maps.newHashMap();
        Map<String, FefuScheduleVEventICalXST> oldVEventsMap = Maps.newHashMap();
        for(FefuScheduleVEventICalXST vEvent : currentVEvents)
        {
            currentVEventsMap.put(vEvent.getUidPrefix()+ "@" + vEvent.getPairId(), vEvent);
        }

        for(FefuScheduleVEventICalXST vEvent : oldVEvents)
        {
            oldVEventsMap.put(vEvent.getUidPrefix()+ "@" + vEvent.getPairId(), vEvent);
        }

        for(String vEventUid : oldVEventsMap.keySet())
        {
            if(!currentVEventsMap.containsKey(vEventUid))
                vEventsForCancel.add(oldVEventsMap.get(vEventUid));
        }
        for(String vEventUid : currentVEventsMap.keySet())
        {
            if(!oldVEventsMap.containsKey(vEventUid))
                vEventsForAdd.add(currentVEventsMap.get(vEventUid));
        }
        int sequence = iCalData.getSequence();

        FefuScheduleICalCommits cancelRecipRequest = getVEventsMap(oldVEvents);
        if(!guidsForCancel.isEmpty())
        {
            String iCalendar = getICalendar(oldVEvents, schedule, ++sequence, true);
            cancelRecipRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), guidsForCancel, iCalendar, cancelRecipRequest);

        }
        else
        {
            cancelRecipRequest.setRequestFailed(false);
        }

        FefuScheduleICalCommits cancelRequest = getVEventsMap(vEventsForCancel);
        if(!vEventsForCancel.isEmpty())
        {
            String iCalendar = getICalendar(vEventsForCancel, schedule, ++sequence, true);
            List<String> nsiIdsForCancel = Lists.newArrayList(currentNsiIds);
            nsiIdsForCancel.remove(guidsForCancel);
            cancelRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), nsiIdsForCancel, iCalendar, cancelRequest);
        }
        else cancelRequest.setRequestFailed(false);


        if(currentVEvents.isEmpty())
        {
            scheduleICalXST.setEvents(currentVEvents);
            scheduleICalXST.setStudentsNSIIds(currentNsiIds);
            iCalData.setSequence(sequence);
            iCalData.setXml(FefuScheduleManager.instance().xStream().toXML(scheduleICalXST));
            iCalData.setChanged(false);
            update(iCalData);
            return;
        }
        String iCalendar = getICalendar(currentVEvents, schedule, ++sequence, false);
        FefuScheduleICalCommits publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), currentNsiIds, iCalendar, getVEventsMap(currentVEvents));


        if(!cancelRecipRequest.isRequestFailed() && !cancelRequest.isRequestFailed() && !publishRequest.isRequestFailed())
        {
            scheduleICalXST.setEvents(publishRequest.getCommitedVEvents());
            scheduleICalXST.setStudentsNSIIds(currentNsiIds);
            iCalData.setSequence(sequence);
            iCalData.setXml(FefuScheduleManager.instance().xStream().toXML(scheduleICalXST));
            iCalData.setChanged(false);
            update(iCalData);
        }
    }

    private List<String> getCurrentTestNsiIds()
    {
        List<String> testIds = Lists.newArrayList();
        testIds.add("00000000-0000-0000-4444-000000000000");
        return testIds;
    }


    @Override
    public SppScheduleSessionPrintForm savePrintForm(SppScheduleSessionPrintParams printData)
    {
        SppScheduleSessionPrintForm printForm = new SppScheduleSessionPrintForm();
        printForm.setCreateDate(new Date());
        printForm.setFormativeOrgUnit(printData.getFormativeOrgUnit());
        printForm.setTerritorialOrgUnit(printData.getTerritorialOrgUnit());
        if (null != printData.getCourseList() && !printData.getCourseList().isEmpty())
            printForm.setCourses(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getCourseList(), Course.title()), ", "));
        printForm.setSeason(printData.getSeason());
        if (null != printData.getEduLevels() && !printData.getEduLevels().isEmpty())
            printForm.setEduLevels(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getEduLevels(), EducationLevelsHighSchool.printTitle()), ", "));
        printForm.setTerm(printForm.getTerm());
        printForm.setChief(printData.getChief());
        printForm.setChiefUMU(printData.getChiefUMU());
        printForm.setAdminOOP(printData.getAdmin());
        String headers = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getHeaders(), EmployeePost.person().identityCard().iof()), ", ");
        printForm.setHeadersOOP(headers.length() > 255 ? headers.substring(0, 255) : headers);
        printForm.setCreateOU(printData.getCurrentOrgUnit());
        if (null != printData.getSchedules() && !printData.getSchedules().isEmpty())
            printForm.setGroups(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getSchedules(), SppScheduleSession.group().title()), ", "));
        else
            printForm.setGroups("-");
        printForm.setEduYear(printData.getEducationYear().getTitle());
        List<SppScheduleSessionGroupPrintVO> schedules = Lists.newArrayList();
        Map<SppScheduleSession, List<SppScheduleSessionEvent>> schedulesMap = getSchedulesMap(printData);
        for (Map.Entry<SppScheduleSession, List<SppScheduleSessionEvent>> entry : schedulesMap.entrySet())
            schedules.add(prepareScheduleGroupPrintVO(entry));
        DatabaseFile content = new DatabaseFile();
        try
        {
            content.setContent(FefuScheduleSessionPrintFormExcelContentBuilder.buildExcelContent(schedules, printData));
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        save(content);
        printForm.setContent(content);
        save(printForm);
        return printForm;
    }

    protected Map<SppScheduleSession, List<SppScheduleSessionEvent>> getSchedulesMap(SppScheduleSessionPrintParams printData)
    {
        Map<SppScheduleSession, List<SppScheduleSessionEvent>> schedulesMap = Maps.newHashMap();
        DQLSelectBuilder eventsBuilder = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "ev");
        eventsBuilder.fetchPath(DQLJoinType.inner, SppScheduleSessionEvent.schedule().fromAlias("ev"), "schedule");
        eventsBuilder.where(eq(property("ev", SppScheduleSessionEvent.schedule().status().code()), value(SppScheduleStatusCodes.APPROVED)));
        if (null != printData.getSchedules() && !printData.getSchedules().isEmpty())
        {
            eventsBuilder.where(in(property("ev", SppScheduleSessionEvent.schedule()), printData.getSchedules()));
        } else
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
            subBuilder.distinct();

            if (null != printData.getCurrentOrgUnit())
            {
                Long orgUnitId = printData.getCurrentOrgUnit().getId();
                DQLSelectBuilder kindRelationsBuilder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                        .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                        .where(eq(property("rel", OrgUnitToKindRelation.orgUnit().id()), value(orgUnitId)));
                List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

                Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                        .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

                IDQLExpression condition = nothing(); // false
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                    condition = or(condition, eq(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().educationLevelHighSchool().orgUnit().id()), value(orgUnitId)));
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                    condition = or(condition, eq(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().formativeOrgUnit().id()), value(orgUnitId)));
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                    condition = or(condition, eq(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().territorialOrgUnit().id()), value(orgUnitId)));

                eventsBuilder.where(condition);
            }
            eventsBuilder.where(eq(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().formativeOrgUnit()), value(printData.getFormativeOrgUnit())));
            eventsBuilder.where(eq(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().territorialOrgUnit()), value(printData.getTerritorialOrgUnit())));
            if (null != printData.getEduLevels() && !printData.getEduLevels().isEmpty())
                eventsBuilder.where(in(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().educationLevelHighSchool()), printData.getEduLevels()));
            eventsBuilder.where(eq(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().developForm()), value(printData.getDevelopForm())));
        }

        for (SppScheduleSessionEvent event : createStatement(eventsBuilder).<SppScheduleSessionEvent>list())
        {
            if (!schedulesMap.containsKey(event.getSchedule()))
                schedulesMap.put(event.getSchedule(), Lists.<SppScheduleSessionEvent>newArrayList());
            if (!schedulesMap.get(event.getSchedule()).contains(event))
                schedulesMap.get(event.getSchedule()).add(event);
        }

        return schedulesMap;
    }
}
