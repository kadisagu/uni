/* $Id: FefuUserManualAltListUI.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuUserManualAlt.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.meta.entity.data.ItemPolicy;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.UserManual;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unifefu.base.bo.FefuUserManualAlt.ui.AddEdit.FefuUserManualAltAddEdit;
import ru.tandemservice.unifefu.entity.FefuUserManual;

/**
 * @author Victor Nekrasov
 * @since 28.04.2014
 */
public class FefuUserManualAltListUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuUserManualAltList.FEFU_USER_MANUAL_ALT_SEARCH_DS.equals(dataSource.getName()))
            dataSource.put("title", getSettings().get("title"));
    }

    public void onClickAdd()
    {
        getActivationBuilder().asRegionDialog(FefuUserManualAltAddEdit.class)
                .activate();
    }

    public void onClickDownload()
    {
        FefuUserManual userManual = DataAccessServices.dao().get(FefuUserManual.class, getListenerParameterAsLong());

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(userManual.getContent()).fileName(userManual.getFileName()), true);
    }

    public void onClickEdit()
    {
        getActivationBuilder().asRegionDialog(FefuUserManualAltAddEdit.class)
                .parameter("id", getListenerParameterAsLong())
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public boolean isDeleteDisabled()
    {
        FefuUserManual current = getConfig().getDataSource(FefuUserManualAltList.FEFU_USER_MANUAL_ALT_SEARCH_DS).getCurrent();

        ItemPolicy itemPolicy = EntityDataRuntime.getEntityPolicy(UserManual.ENTITY_NAME).getItemPolicyById(current.getCode());

        return itemPolicy != null && !SynchronizeMeta.temp.equals(itemPolicy.getSynchronize());
    }
}
