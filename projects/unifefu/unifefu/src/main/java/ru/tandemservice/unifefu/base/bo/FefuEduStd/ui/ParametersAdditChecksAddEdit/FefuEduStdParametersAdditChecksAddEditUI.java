/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ParametersAdditChecksAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardAdditionalChecks;

/**
 * @author Alexey Lopatin
 * @since 19.01.2015
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "additionalChecks.id")})
public class FefuEduStdParametersAdditChecksAddEditUI extends UIPresenter
{
    private FefuEppStateEduStandardAdditionalChecks _additionalChecks = new FefuEppStateEduStandardAdditionalChecks();

    @Override
    public void onComponentRefresh()
    {
        _additionalChecks = DataAccessServices.dao().getNotNull(_additionalChecks.getId());
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_additionalChecks);
        deactivate();
    }

    public FefuEppStateEduStandardAdditionalChecks getAdditionalChecks()
    {
        return _additionalChecks;
    }

    public void setAdditionalChecks(FefuEppStateEduStandardAdditionalChecks additionalChecks)
    {
        _additionalChecks = additionalChecks;
    }
}
