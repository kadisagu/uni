/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.catalog.fefuCompetence.FefuCompetencePub;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class DAO extends DefaultCatalogPubDAO<FefuCompetence, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setEppSkillGroupModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EppSkillGroup.class, "esg")
                        .where(or(like(DQLFunctions.upper(property("esg", EppSkillGroup.title())), value(CoreStringUtils.escapeLike(filter))),
                                  like(DQLFunctions.upper(property("esg", EppSkillGroup.shortTitle())), value(CoreStringUtils.escapeLike(filter)))));

                return new ListResult<>(builder.createStatement(getSession()).<EppSkillGroup>list());
            }
        });
    }

    @Override
    protected void applyFilters(Model model, MQBuilder builder)
    {
        super.applyFilters(model, builder);
        if(model.getEppSkillGroup()!=null)
            builder.add(MQExpression.eq("ci", FefuCompetence.eppSkillGroup(), model.getEppSkillGroup()));
    }

}