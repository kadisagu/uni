/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu5.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuTransferDevFormStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 29.08.2012
 */
public class DAO extends ModularStudentExtractPubDAO<FefuTransferDevFormStuExtract, Model> implements IDAO
{
}