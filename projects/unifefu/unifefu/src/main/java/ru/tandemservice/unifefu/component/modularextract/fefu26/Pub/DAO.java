/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu26.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuPerformConditionTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public class DAO extends ModularStudentExtractPubDAO<FefuPerformConditionTransferCourseStuExtract, Model> implements IDAO
{
}
