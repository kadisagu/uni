/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.settings.ImtsaCyclesConfiguration;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unifefu.entity.FefuImtsaCyclePlanStructureRel;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
public class Model
{
    public static final String CYCLE_FILTER_NAME = "imtsaCycle";
    private static final String FEFU_GOS_2_CYCLES_CONFIGURATION_PREFIX = "fefuGos2CyclesConfiguration.filter";
    private static final String FEFU_GOS_3_CYCLES_CONFIGURATION_PREFIX = "fefuGos3CyclesConfiguration.filter";

    private GosGenerationBlock _blockGos2;
    private GosGenerationBlock _blockGos3;

    public GosGenerationBlock getBlockGos2(){ return _blockGos2; }
    public void setBlockGos2(GosGenerationBlock blockGos2){ _blockGos2 = blockGos2; }

    public GosGenerationBlock getBlockGos3(){ return _blockGos3; }
    public void setBlockGos3(GosGenerationBlock blockGos3){ _blockGos3 = blockGos3; }


    public static class GosGenerationBlock
    {
        private final DynamicListDataSource<FefuImtsaCyclePlanStructureRel> _dataSource;
        private final IDataSettings _settings;
        private String _cycleFilter;

        public DynamicListDataSource<FefuImtsaCyclePlanStructureRel> getDataSource(){ return _dataSource; }
        public IDataSettings getSettings(){ return _settings; }

        public String getCycleFilter(){ return _cycleFilter; }
        public void setCycleFilter(String cycleFilter){ _cycleFilter = cycleFilter; }

        private GosGenerationBlock(boolean gos2, DynamicListDataSource<FefuImtsaCyclePlanStructureRel> dataSource)
        {
            _dataSource = dataSource;
            _settings = DataSettingsFacade.getSettings(UserContext.getInstance().getPrincipal().getId().toString(), gos2 ? FEFU_GOS_2_CYCLES_CONFIGURATION_PREFIX : FEFU_GOS_3_CYCLES_CONFIGURATION_PREFIX);
            _cycleFilter = _settings.get(CYCLE_FILTER_NAME);
        }

        public static GosGenerationBlock create(boolean gos2, DynamicListDataSource<FefuImtsaCyclePlanStructureRel> dataSource)
        {
            return new GosGenerationBlock(gos2, dataSource);
        }
    }
}