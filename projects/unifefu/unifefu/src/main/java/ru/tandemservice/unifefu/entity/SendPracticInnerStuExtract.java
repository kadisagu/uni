/* $Id$ */
package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IPracticeInExtract;
import ru.tandemservice.unifefu.entity.gen.SendPracticInnerStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О направлении на практику в пределах ОУ
 */
public class SendPracticInnerStuExtract extends SendPracticInnerStuExtractGen implements IPracticeInExtract
{
    @Override
    public Date getBeginDate()
    {
        return getPracticeBeginDate();
    }

    @Override
    public Date getEndDate()
    {
        return getPracticeEndDate();
    }

    @Override
    public String getPracticePlace()
    {
        return getPracticeOrgUnitStr();
    }

    @Override
    public String getPracticeHeaderInnerStr()
    {
        return getPracticeHeaderStr();
    }
}