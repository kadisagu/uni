package ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.Edit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.Edit.TrBrsCoefficientEdit;

/**
 * User: newdev
 */
@Configuration
public class TrBrsCoefficientEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrBrsCoefficientEditExtUI.class.getSimpleName();

    @Autowired
    private TrBrsCoefficientEdit _trBrsCoefficientEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trBrsCoefficientEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrBrsCoefficientEditExtUI.class))
                .addAction(new TrBrsCoefficientEditClickApply("onClickApply"))
                .create();
    }
}