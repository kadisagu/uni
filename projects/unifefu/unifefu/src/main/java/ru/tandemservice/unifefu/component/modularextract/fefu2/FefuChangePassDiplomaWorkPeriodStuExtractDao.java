package ru.tandemservice.unifefu.component.modularextract.fefu2;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/20/12
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class FefuChangePassDiplomaWorkPeriodStuExtractDao extends UniBaseDao implements IExtractComponentDao<FefuChangePassDiplomaWorkPeriodStuExtract>
{
    public void doCommit(FefuChangePassDiplomaWorkPeriodStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);
    }

    public void doRollback(FefuChangePassDiplomaWorkPeriodStuExtract extract, Map parameters)
    {
    }
}
