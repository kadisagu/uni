/*$Id$*/
package ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLSubselectType;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.bo.EduProgram.EduProgramManager;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.unifefu.entity.FefuBrsPermittedWorkPlan;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 22.01.2015
 */
@Configuration
public class FefuBrsPermittedEduPlanAdd extends BusinessComponentManager
{
    //data sources
    public static final String WORK_PLAN_DS = "workPlanDS";
    public static final String YEAR_EDU_PROCESS_DS = "yearEduProcessDS";

    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String PROGRAM_SPECIALIZATION_DS = "programSpecializationDS";

    public static final String TERM_DS = "termDS";

    //parameters
    public static final String CHECKBOX_COLUMN_NAME = "checked";

    public static final String PARAM_PROGRAM_KIND = "programKind";
    public static final String PARAM_EDU_YEAR_PROCESS = "eduYearProcess";
    public static final String PARAM_NUMBER = "number";

    public static final String PARAM_PROGRAM_SUBJECT = "programSubject";
    public static final String PARAM_PROGRAM_SPECIALIZATION_SUBJECT_LIST = "programSpecialization";

    public static final String PARAM_PROGRAM_FORM_LIST = "programForm";
    public static final String PARAM_DEVELOP_CONDITION_LIST = "developCondition";
    public static final String PARAM_PROGRAM_TRAIT_LIST = "programTrait";
    public static final String PARAM_TERM = "term";
    public static final String PARAM_STATE = "state";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EduProgramManager.instance().programKindDSConfig())
                .addDataSource(selectDS(YEAR_EDU_PROCESS_DS, yearEduProcessDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS, programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(selectDS(PROGRAM_SPECIALIZATION_DS, programSpecializationDSHandler()))
                .addDataSource(EducationCatalogsManager.instance().programFormDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
                .addDataSource(EducationCatalogsManager.instance().programTraitDSConfig())
                .addDataSource(selectDS(TERM_DS, termDSHandler()))
                .addDataSource(EppStateManager.instance().eppStateDSConfig())
                .addDataSource(searchListDS(WORK_PLAN_DS, workPlanDSColumns(), workPlanDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler termDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Term.class)
                .order(Term.intValue())
                .filter(Term.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler yearEduProcessDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppYearEducationProcess.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanProf.class, "p")
                        .fromEntity(EppWorkPlan.class, "w")
                        .where(eq(property("w", EppWorkPlan.parent().eduPlanVersion().eduPlan()), property("p")))
                        .where(eq(property("p", EppEduPlanProf.programSubject()), property(alias)));

                EppYearEducationProcess educationProcess = context.get(PARAM_EDU_YEAR_PROCESS);
                if (educationProcess != null)
                {
                    builder.where(eq(property("w", EppWorkPlan.L_YEAR), value(educationProcess)));
                }

                dql.where(exists(builder.buildQuery()));
            }
        }.where(EduProgramSubject.subjectIndex().programKind(), PARAM_PROGRAM_KIND)
                .filter(EduProgramSubject.code()).filter(EduProgramSubject.title())
                .order(EduProgramSubject.code()).order(EduProgramKind.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler programSpecializationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSpecialization.class)
                .where(EduProgramSpecialization.programSubject(), PARAM_PROGRAM_SUBJECT, false)
                .order(EduProgramSpecialization.title()).filter(EduProgramSpecialization.title());
    }

    @Bean
    public ColumnListExtPoint workPlanDSColumns()
    {
        return columnListExtPointBuilder(WORK_PLAN_DS)
                .addColumn(checkboxColumn(CHECKBOX_COLUMN_NAME).controlInHeader(true))
                .addColumn(textColumn("title", EppWorkPlan.title()))
                .addColumn(textColumn("eduElementTitle", EppWorkPlan.parent().eduPlanVersion().eduPlan().educationElementSimpleTitle()))
                .addColumn(textColumn("eduPlanVersionBlock", EppWorkPlan.parent().title()))
                .addColumn(textColumn("eduPlanVersion", EppWorkPlan.parent().eduPlanVersion().title()))
                .addColumn(textColumn("programForm", EppWorkPlan.parent().eduPlanVersion().eduPlan().programForm().title()))
                .addColumn(textColumn("developCondition", EppWorkPlan.parent().eduPlanVersion().eduPlan().developCondition().title()))
                .addColumn(textColumn("programTrait", EppWorkPlan.parent().eduPlanVersion().eduPlan().programTrait().title()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler workPlanDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppWorkPlan.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql.where(notExists(new DQLSelectBuilder().fromEntity(FefuBrsPermittedWorkPlan.class, "pwp")
                                            .column(property("pwp", FefuBrsPermittedWorkPlan.L_WORK_PLAN))
                                            .where(eq(property(alias, EppWorkPlan.P_ID), property("pwp", FefuBrsPermittedWorkPlan.workPlan().id())))
                                            .buildQuery()
                ));

                EduProgramSubject programSubject = context.get(PARAM_PROGRAM_SUBJECT);
                if (programSubject != null)
                {
                    dql.fromEntity(EppEduPlanProf.class, "prof");
                    dql.where(eq(property(alias, EppWorkPlan.parent().eduPlanVersion().eduPlan()), property("prof")));
                    dql.where(eq(property("prof", EppEduPlanProf.programSubject()), value(programSubject)));
                }

                Collection<EduProgramSpecialization> programSpecializations = context.get(PARAM_PROGRAM_SPECIALIZATION_SUBJECT_LIST);
                if (programSpecializations != null)
                {
                    dql.where(eqSubquery(property(alias, EppWorkPlan.parent().id()), DQLSubselectType.any,
                                         new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "bs")
                                                 .column("bs.id")
                                                 .where(in(property("bs", EppEduPlanVersionSpecializationBlock.programSpecialization()), programSpecializations))
                                                 .buildQuery()
                    ));
                }
            }
        }.pageable(true)
                .where(EppWorkPlan.number(), PARAM_NUMBER, true)
                .where(EppWorkPlan.year(), PARAM_EDU_YEAR_PROCESS, true)
                .where(EppWorkPlan.term(), PARAM_TERM, true)
                .where(EppWorkPlan.state(), PARAM_STATE, true)
                .where(EppWorkPlan.parent().eduPlanVersion().eduPlan().programKind(), PARAM_PROGRAM_KIND, true)
                .where(EppWorkPlan.parent().eduPlanVersion().eduPlan().programForm(), PARAM_PROGRAM_FORM_LIST, true)
                .where(EppWorkPlan.parent().eduPlanVersion().eduPlan().developCondition(), PARAM_DEVELOP_CONDITION_LIST, true)
                .where(EppWorkPlan.parent().eduPlanVersion().eduPlan().programTrait(), PARAM_PROGRAM_TRAIT_LIST, true);
    }
}
