package ru.tandemservice.unifefu.entity;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;
import ru.tandemservice.unifefu.entity.gen.FefuAdditionalProfessionalEducationProgramForStudentGen;

import java.util.Collection;
import java.util.Collections;

/**
 * Программа ДПО/ДО для студента
 */
public class FefuAdditionalProfessionalEducationProgramForStudent extends FefuAdditionalProfessionalEducationProgramForStudentGen implements ISecLocalEntityOwner
{
    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return getStudent() != null ? getStudent().getSecLocalEntities() : Collections.<IEntity>emptyList();
    }
}