/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unifefu.brs.base.BaseFefuReportAddUI;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport.FefuBrsPercentGradingReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport.logic.FefuBrsPercentGradingReportParams;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport.ui.View.FefuBrsPercentGradingReportView;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.report.FefuBrsPercentGradingReport;

import java.util.Collections;

/**
 * @author nvankov
 * @since 12/10/13
 */
public class FefuBrsPercentGradingReportAddUI extends BaseFefuReportAddUI<FefuBrsPercentGradingReportParams>
{
    @Override
    protected void fillDefaults(FefuBrsPercentGradingReportParams reportSettings)
    {
        super.fillDefaults(reportSettings);
        reportSettings.setFormativeOrgUnit(getCurrentFormativeOrgUnit());
        reportSettings.setOnlyFilledJournals(FefuBrsReportManager.instance().yesNoItemListExtPoint().getItem(FefuBrsReportManager.YES_ID.toString()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuBrsReportManager.GROUP_DS.equals(dataSource.getName()))
        {
            if (null != getReportSettings().getFormativeOrgUnit())
                dataSource.put(FefuBrsReportManager.PARAM_FORMATIVE_OU, Collections.singletonList(getReportSettings().getFormativeOrgUnit().getId()));
        }
    }

    public void onClickApply()
    {
        FefuBrsPercentGradingReport report = FefuBrsPercentGradingReportManager.instance().dao().createReport(getReportSettings());
        deactivate();
        _uiActivation.asDesktopRoot(FefuBrsPercentGradingReportView.class).parameter(UIPresenter.PUBLISHER_ID, report.getId()).activate();
    }
}
