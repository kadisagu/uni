/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.EntrantTAListsAdd;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDateTime;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.FefuEcReportManager;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.FefuEntrantTAListsReportBuilder;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.FefuEntrantTAListsReportEnrDirComboDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.EntrantTAListsPub.FefuEcReportEntrantTAListsPub;
import ru.tandemservice.unifefu.base.vo.FefuEntrantTAListsReportVO;
import ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 8/7/13
 */
public class FefuEcReportEntrantTAListsAddUI extends UIPresenter
{
    private FefuEntrantTAListsReportVO _reportVO;

    public FefuEntrantTAListsReportVO getReportVO()
    {
        return _reportVO;
    }

    public void setReportVO(FefuEntrantTAListsReportVO reportVO)
    {
        _reportVO = reportVO;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _reportVO)
        {
            _reportVO = new FefuEntrantTAListsReportVO();
            List<EnrollmentCampaign> enrollmentCampaignList = DataAccessServices.dao().getList(EnrollmentCampaign.class, EnrollmentCampaign.id().s());
            _reportVO.setEnrollmentCampaign(enrollmentCampaignList.isEmpty() ? null : enrollmentCampaignList.get(enrollmentCampaignList.size() - 1));
            _reportVO.setFrom(new LocalDateTime().withDayOfYear(1).toDateTime().toDate());
            _reportVO.setTo(new Date());
            _reportVO.setContractCanceled(FefuEcReportManager.instance().yesNoExtPoint().getItem(FefuEcReportManager.NO.toString()));
            _reportVO.setIncludeForeignPerson(FefuEcReportManager.instance().yesNoExtPoint().getItem(FefuEcReportManager.NO.toString()));
            _reportVO.setIncludeEnrolled(FefuEcReportManager.instance().yesNoExtPoint().getItem(FefuEcReportManager.YES.toString()));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("reportVO", _reportVO);
        if (FefuEcReportEntrantTAListsAdd.TERRITORIAL_ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", FefuEntrantTAListsReportEnrDirComboDSHandler.Columns.TERRITORIAL_ORG_UNIT);
        }
        if (FefuEcReportEntrantTAListsAdd.FORMATIVE_ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", FefuEntrantTAListsReportEnrDirComboDSHandler.Columns.FORMATIVE_ORG_UNIT);
        }
        if (FefuEcReportEntrantTAListsAdd.DEVELOP_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", FefuEntrantTAListsReportEnrDirComboDSHandler.Columns.DEVELOP_FORM);
        }
        if (FefuEcReportEntrantTAListsAdd.DEVELOP_CONDITION_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", FefuEntrantTAListsReportEnrDirComboDSHandler.Columns.DEVELOP_CONDITION);
        }
        if (FefuEcReportEntrantTAListsAdd.DEVELOP_TECH_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", FefuEntrantTAListsReportEnrDirComboDSHandler.Columns.DEVELOP_TECH);
        }
        if (FefuEcReportEntrantTAListsAdd.DEVELOP_PERIOD_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", FefuEntrantTAListsReportEnrDirComboDSHandler.Columns.DEVELOP_PERIOD);
        }
    }


    public void onClickApply()
    {
        if (_reportVO.getFrom().after(_reportVO.getTo()))
        {
            _uiSupport.error("Дата \"Заявления с\" не может быть позже даты \"Заявления по\"", "dateFrom", "dateTo");
            return;
        }

        Long reportId = FefuEcReportManager.instance().dao().createReport(new FefuEntrantTAListsReportBuilder(_reportVO), createReport());

        deactivate();
        getActivationBuilder().asDesktopRoot(FefuEcReportEntrantTAListsPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, reportId)
                .activate();
    }

    private FefuEntrantTAListsReport createReport()
    {
        FefuEntrantTAListsReport report = new FefuEntrantTAListsReport();
        report.setFormingDate(new Date());
        report.setDateFrom(_reportVO.getFrom());
        report.setDateTo(_reportVO.getTo());
        report.setEnrollmentCampaign(_reportVO.getEnrollmentCampaign());
        report.setEnrollmentCampaignStage(_reportVO.getEnrollmentCampaignStage().getTitle());

        if (_reportVO.isStudentCategoryActive())
            report.setStudentCategoryTitle(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(_reportVO.getStudentCategoryList(), StudentCategory.title()), "; "));
        if (_reportVO.isQualificationActive())
            report.setQualificationTitle(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(_reportVO.getQualifications(), Qualifications.title()), ", "));
        if (_reportVO.isFormativeOrgUnitActive())
            report.setFormativeOrgUnit(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(_reportVO.getFormativeOrgUnitList(), OrgUnit.title()), ", "));
        if (_reportVO.isTerritorialOrgUnitActive())
            report.setTerritorialOrgUnit(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(_reportVO.getTerritorialOrgUnitList(), OrgUnit.territorialTitle()), ", "));
        if (_reportVO.isDevelopFormActive())
            report.setDevelopForm(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(_reportVO.getDevelopFormList(), DevelopForm.title()), ", "));
        if (_reportVO.isDevelopConditionActive())
            report.setDevelopCondition(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(_reportVO.getDevelopConditionList(), DevelopCondition.title()), ", "));
        if (_reportVO.isDevelopTechActive())
            report.setDevelopTech(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(_reportVO.getDevelopTechList(), DevelopTech.title()), ", "));
        if (_reportVO.isDevelopPeriodActive())
            report.setDevelopPeriod(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(_reportVO.getDevelopPeriodList(), DevelopPeriod.title()), ", "));

        report.setIncludeForeignPerson(FefuEcReportManager.YES.equals(_reportVO.getIncludeForeignPerson().getId()));
        if (_reportVO.isExcludeParallelActive())
            report.setExcludeParallel(FefuEcReportManager.YES.equals(_reportVO.getExcludeParallel().getId()));
        report.setContractCanceled(FefuEcReportManager.YES.equals(_reportVO.getContractCanceled().getId()));
        report.setIncludeEnrolled(FefuEcReportManager.YES.equals(_reportVO.getIncludeEnrolled().getId()));

        return report;
    }
}
