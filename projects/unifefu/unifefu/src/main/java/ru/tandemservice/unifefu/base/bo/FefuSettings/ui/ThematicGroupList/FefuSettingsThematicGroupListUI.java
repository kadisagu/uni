/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.ThematicGroupList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuSettings.ui.ThematicGroupAdd.FefuSettingsThematicGroupAdd;
import ru.tandemservice.unifefu.entity.catalog.FefuThematicGroupStuExtractTypes;

/**
 * @author nvankov
 * @since 11/1/13
 */
@Input({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "thematicGroupId"),
})
public class FefuSettingsThematicGroupListUI extends UIPresenter
{
    private Long _thematicGroupId;
    private FefuThematicGroupStuExtractTypes _thematicGroup;

    public Long getThematicGroupId()
    {
        return _thematicGroupId;
    }

    public void setThematicGroupId(Long thematicGroupId)
    {
        _thematicGroupId = thematicGroupId;
    }

    public FefuThematicGroupStuExtractTypes getThematicGroup()
    {
        return _thematicGroup;
    }

    public void setThematicGroup(FefuThematicGroupStuExtractTypes thematicGroup)
    {
        _thematicGroup = thematicGroup;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _thematicGroup)
            _thematicGroup = DataAccessServices.dao().getNotNull(_thematicGroupId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuSettingsThematicGroupList.ORDER_TYPES_DS.equals(dataSource.getName()))
        {
            dataSource.put("thematicGroupId", _thematicGroupId);
        }
    }

    public void onClickAdd()
    {
        _uiActivation.asDesktopRoot(FefuSettingsThematicGroupAdd.class).parameter("thematicGroupId", _thematicGroupId).activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
