/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.ui.ContractsList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.FefuStudentContract;
import ru.tandemservice.unifefu.entity.ws.FefuContractToPersonRel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 20.12.2013
 */
@Configuration
public class FefuStudentContractsList extends BusinessComponentManager
{
    public static final String STUDENT_CONTRACTS_DS = "studentContractsDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(STUDENT_CONTRACTS_DS, studentContractCL(), fefuStudentContractsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint studentContractCL()
    {
        return columnListExtPointBuilder(STUDENT_CONTRACTS_DS)
                .addColumn(dateColumn("contractDate", FefuStudentContract.contractDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(textColumn("contractNumber", FefuStudentContract.contractNumber()))
                .addColumn(textColumn("contractBalance", FefuStudentContract.balance()))
                .addColumn(textColumn("contractName", FefuStudentContract.contractName()))
                .addColumn(dateColumn("contractBeginDate", FefuStudentContract.dateBegin()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(dateColumn("contractEndDate", FefuStudentContract.dateEnd()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(textColumn("contractEmplyeePost", FefuStudentContract.employeePost().fullTitle()))
                .addColumn(textColumn("contractResponsibilities", FefuStudentContract.responsibilities()))
                .addColumn(textColumn("contractShortContent", FefuStudentContract.shortContent()))
                .addColumn(textColumn("contractAddInfo", FefuStudentContract.additionalInfo()))
                .addColumn(toggleColumn("contractTerminated", FefuStudentContract.terminated())
                        .toggleOffListener("onChangeChecked").toggleOffLabel("Нет")
                        .toggleOnListener("onChangeChecked").toggleOnLabel("Да").permissionKey("changeFefuStudentContractState"))
                .addColumn(actionColumn("editContract", new Icon("edit"), "onClickEditContract").permissionKey("editFefuStudentContract"))
                .addColumn(actionColumn("deleteContract", new Icon("delete"), "onClickDeleteContract").permissionKey("deleteFefuStudentContract"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler fefuStudentContractsDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long personId = context.get("personId");
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuStudentContract.class, "c").column("c")
                        .where(/*or(
                                exists(
                                        new DQLSelectBuilder().fromEntity(FefuStudentContract.class, "c1").column("c1")
                                                .where(eq(property("c1", FefuStudentContract.contractor().person().id()), value(personId)))
                                                .buildQuery()
                                ),*/
                                exists(
                                        new DQLSelectBuilder().fromEntity(FefuContractToPersonRel.class, "rel")
                                                .where(eq(property(FefuStudentContract.id().fromAlias("c")), property(FefuContractToPersonRel.contract().id().fromAlias("rel"))))
                                                .where(eq(property("rel", FefuContractToPersonRel.person().id()), value(personId)))
                                                .buildQuery()
                                )
                        /*)*/)
                        .order(property(FefuStudentContract.contractDate().fromAlias("c")), OrderDirection.desc);
                return ListOutputBuilder.get(input, builder.createStatement(context.getSession()).list()).build();
            }
        };
    }
}
