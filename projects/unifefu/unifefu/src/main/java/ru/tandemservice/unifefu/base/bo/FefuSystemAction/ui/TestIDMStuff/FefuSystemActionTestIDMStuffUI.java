/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.TestIDMStuff;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifefu.utils.SHAUtils;

import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * @author Dmitry Seleznev
 * @since 22.05.2013
 */
public class FefuSystemActionTestIDMStuffUI extends UIPresenter
{
    private String PASSWORD_GENERATOR_SIGNS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    private String _destURL;
    private String _lastName;
    private String _firstName;
    private String _middleName;
    private String _guid;
    private String _email;
    private String _password;
    private String _shaKey;
    private String _hashStr;

    @Override
    public void onComponentRefresh()
    {
        _destURL = ApplicationRuntime.getProperty("fefu.idm.service.connectionstr");
        if (null == _destURL)
            throw new ApplicationException("Не задан URL сервиса IDM, принимающего запросы на регистрацию новых физ. лиц (см. свойство fefu.idm.service.connectionstr в файлах конфигурации).");

        _shaKey = ApplicationRuntime.getProperty("fefu.idm.service.shakey1");
        if (null == _shaKey)
            throw new ApplicationException("Не задан ключ для шифрования контрольной суммы (см. свойство fefu.idm.service.shakey1 в файлах конфигурации).");

        //TODO: Entrant randomEntrant
        //Person randomPerson;

        _lastName = "Петров";
        _firstName = "Иван";
        _middleName = "Иванович";
        _guid = UUID.randomUUID().toString();
        _email = "petrov" + System.currentTimeMillis() + "@nonamail.hz";
        _password = generateRandomPassword();

        try
        {
            _hashStr = SHAUtils.getSHAChecksum(_shaKey, _email, _password, _lastName, _firstName, _middleName, _guid);
        } catch (NoSuchAlgorithmException e)
        {
        }
    }

    public void onChangeHash()
    {
        try
        {
            _hashStr = SHAUtils.getSHAChecksum(_shaKey, _email, _password, _lastName, _firstName, _middleName, _guid);
        } catch (NoSuchAlgorithmException e)
        {
        }
    }

    public void onClickApply() throws Exception
    {
        System.out.println(DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date()) + " System action for IDM integration testing has started...");
        System.out.println("email\t= " + _email);
        System.out.println("password\t= " + _password);
        System.out.println("lastName\t= " + _lastName);
        System.out.println("firstName\t= " + _firstName);
        System.out.println("secondName\t= " + (null == _middleName ? "" : _middleName));
        System.out.println("guidPerson\t= " + _guid);
        System.out.println("shaKey\t= " + _shaKey);
        System.out.println("hashStr\t= " + new String(_hashStr));

        HttpClient httpClient = new HttpClient();
        PostMethod post = new PostMethod();
        post.addRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        post.setPath(_destURL);

        post.setParameter("email", _email);
        post.setParameter("password", _password);
        post.setParameter("firstName", _firstName);
        post.setParameter("secondName", _middleName);
        post.setParameter("lastName", _lastName);
        post.setParameter("guidPerson", _guid);
        post.setParameter("hashStr", new String(_hashStr));

        httpClient.executeMethod(post);
        System.out.println("System action for IDM integration testing was finished. The response from IDM is:");
        System.out.println(post.getResponseBodyAsString());
    }

    private String generateRandomPassword()
    {
        Random rnd = new Random();
        int passwordLength = rnd.nextInt(15);
        while(passwordLength < 8) passwordLength = rnd.nextInt(15);

        StringBuilder result = new StringBuilder();

        boolean hasNumbers = false;
        boolean hasCapitals = false;
        boolean hasSmalls = false;

        while (!hasCapitals || !hasSmalls || !hasNumbers)
        {
            result = new StringBuilder();
            hasNumbers = false;
            hasCapitals = false;
            hasSmalls = false;

            for(int i = 0; i < passwordLength; i++)
            {
                char ch = PASSWORD_GENERATOR_SIGNS.charAt(rnd.nextInt(PASSWORD_GENERATOR_SIGNS.length()));
                if(StringUtils.isNumeric(String.valueOf(ch))) hasNumbers = true;
                if(StringUtils.isAlpha(String.valueOf(ch)))
                {
                    if(StringUtils.isAllUpperCase(String.valueOf(ch))) hasCapitals = true;
                    if(StringUtils.isAllLowerCase(String.valueOf(ch))) hasSmalls = true;
                }
                result.append(ch);
            }
        }

        return result.toString();
    }

    public String getDestURL()
    {
        return _destURL;
    }

    public void setDestURL(String destURL)
    {
        _destURL = destURL;
    }

    public String getLastName()
    {
        return _lastName;
    }

    public void setLastName(String lastName)
    {
        _lastName = lastName;
    }

    public String getFirstName()
    {
        return _firstName;
    }

    public void setFirstName(String firstName)
    {
        _firstName = firstName;
    }

    public String getMiddleName()
    {
        return _middleName;
    }

    public void setMiddleName(String middleName)
    {
        _middleName = middleName;
    }

    public String getGuid()
    {
        return _guid;
    }

    public void setGuid(String guid)
    {
        _guid = guid;
    }

    public String getEmail()
    {
        return _email;
    }

    public void setEmail(String email)
    {
        _email = email;
    }

    public String getPassword()
    {
        return _password;
    }

    public void setPassword(String password)
    {
        _password = password;
    }

    public String getShaKey()
    {
        return _shaKey;
    }

    public void setShaKey(String shaKey)
    {
        _shaKey = shaKey;
    }

    public String getHashStr()
    {
        return _hashStr;
    }

    public void setHashStr(String hashStr)
    {
        _hashStr = hashStr;
    }
}