/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.unifefu.brs.base.BaseFefuReportAddUI;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.FefuBrsPpsGradingSumDataReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.logic.FefuBrsPpsGradingSumDataReportParams;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.ui.View.FefuBrsPpsGradingSumDataReportView;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport;

import java.util.Arrays;
import java.util.Date;

/**
 * @author nvankov
 * @since 12/16/13
 */
public class FefuBrsPpsGradingSumDataReportAddUI extends BaseFefuReportAddUI<FefuBrsPpsGradingSumDataReportParams>
{
    @Override
    protected void fillDefaults(FefuBrsPpsGradingSumDataReportParams reportSettings)
    {
        super.fillDefaults(reportSettings);
        reportSettings.setFormativeOrgUnit(getCurrentFormativeOrgUnitAsList());
        reportSettings.setCutReport(FefuBrsReportManager.instance().cutReportItemListExtPoint().getItem(FefuBrsReportManager.CUT_REPORT_CURRENT_ID.toString()));
        reportSettings.setCheckDate(new Date());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuBrsReportManager.GROUP_DS.equals(dataSource.getName()))
        {
            if(null != getReportSettings().getFormativeOrgUnit() && !getReportSettings().getFormativeOrgUnit().isEmpty())
                dataSource.put(FefuBrsReportManager.PARAM_FORMATIVE_OU, CommonBaseEntityUtil.getIdList(getReportSettings().getFormativeOrgUnit()));
        }
    }

    public Boolean getShowCheckDateParam()
    {
        return null != getReportSettings().getCutReport() && FefuBrsReportManager.CUT_REPORT_CURRENT_ID.equals(getReportSettings().getCutReport().getId());
    }

    public void onClickApply()
    {
        FefuBrsPpsGradingSumDataReport report = FefuBrsPpsGradingSumDataReportManager.instance().dao().createReport(getReportSettings());
        deactivate();
        _uiActivation.asDesktopRoot(FefuBrsPpsGradingSumDataReportView.class).parameter(UIPresenter.PUBLISHER_ID, report.getId()).activate();
    }
}
