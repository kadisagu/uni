
package ru.tandemservice.unifefu.ws.blackboard.context.gen;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clientVendorId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientProgramId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="registrationPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="initialSharedSecret" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requiredToolMethods" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="requiredTicketMethods" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "clientVendorId",
    "clientProgramId",
    "registrationPassword",
    "description",
    "initialSharedSecret",
    "requiredToolMethods",
    "requiredTicketMethods"
})
@XmlRootElement(name = "registerTool")
public class RegisterTool {

    @XmlElementRef(name = "clientVendorId", namespace = "http://context.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> clientVendorId;
    @XmlElementRef(name = "clientProgramId", namespace = "http://context.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> clientProgramId;
    @XmlElementRef(name = "registrationPassword", namespace = "http://context.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> registrationPassword;
    @XmlElementRef(name = "description", namespace = "http://context.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "initialSharedSecret", namespace = "http://context.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> initialSharedSecret;
    @XmlElement(nillable = true)
    protected List<String> requiredToolMethods;
    @XmlElement(nillable = true)
    protected List<String> requiredTicketMethods;

    /**
     * Gets the value of the clientVendorId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClientVendorId() {
        return clientVendorId;
    }

    /**
     * Sets the value of the clientVendorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClientVendorId(JAXBElement<String> value) {
        this.clientVendorId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the clientProgramId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClientProgramId() {
        return clientProgramId;
    }

    /**
     * Sets the value of the clientProgramId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClientProgramId(JAXBElement<String> value) {
        this.clientProgramId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the registrationPassword property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRegistrationPassword() {
        return registrationPassword;
    }

    /**
     * Sets the value of the registrationPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRegistrationPassword(JAXBElement<String> value) {
        this.registrationPassword = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the initialSharedSecret property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInitialSharedSecret() {
        return initialSharedSecret;
    }

    /**
     * Sets the value of the initialSharedSecret property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInitialSharedSecret(JAXBElement<String> value) {
        this.initialSharedSecret = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the requiredToolMethods property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requiredToolMethods property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequiredToolMethods().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRequiredToolMethods() {
        if (requiredToolMethods == null) {
            requiredToolMethods = new ArrayList<>();
        }
        return this.requiredToolMethods;
    }

    /**
     * Gets the value of the requiredTicketMethods property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requiredTicketMethods property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequiredTicketMethods().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRequiredTicketMethods() {
        if (requiredTicketMethods == null) {
            requiredTicketMethods = new ArrayList<>();
        }
        return this.requiredTicketMethods;
    }

}
