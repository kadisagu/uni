/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.logic.FefuTechnicalCommissionsDAO;
import ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.logic.IFefuTechnicalCommissionsDAO;

/**
 * @author Nikolay Fedorovskih
 * @since 11.06.2013
 */
@Configuration
public class FefuTechnicalCommissionsDataManager extends BusinessObjectManager
{
    public static FefuTechnicalCommissionsDataManager instance()
    {
        return instance(FefuTechnicalCommissionsDataManager.class);
    }

    @Bean
    public IFefuTechnicalCommissionsDAO dao()
    {
        return new FefuTechnicalCommissionsDAO();
    }
}