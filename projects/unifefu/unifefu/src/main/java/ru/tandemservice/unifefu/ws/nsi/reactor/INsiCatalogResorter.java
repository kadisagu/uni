/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 10.04.2014
 */
public interface INsiCatalogResorter<T>
{
    void resortNsiEntityList(List<T> entityList);
}