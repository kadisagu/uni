/* $Id$ */
package ru.tandemservice.unifefu.component.report.FefuEntrantIncomingListVar2.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Igor Belanov
 * @since 25.07.2016
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        model.setPrincipalContext(component.getUserContext().getPrincipalContext());
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().update(model);
        deactivate(component);
        activateInRoot(component, new PublisherActivator(model.getReport()));
    }

    /**
     * нужно сбросить чекбокс "Выделить абитуриентов с оригиналами документов"
     * если устновлено "Выводить данные по этапам зачисления"
     */
    public void onClickShowDataByEnrollmentStage(IBusinessComponent component)
    {
        getModel(component).getReport().setOrderByOriginals(false);
    }
}
