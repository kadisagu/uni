/* $Id: IFefuUserManualAltDao.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuUserManualAlt.logic;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unifefu.entity.FefuUserManual;

/**
 * @author Victor Nekrasov
  * @since 28.04.2014
 */
public interface IFefuUserManualAltDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет или обновляет инструкцию.
     * Если на форме указан файл для загрузки, то он зипуется и сохраняется в инструкцию.
     * @param userManual объект который сохраняем или обновляем
     * @param document загруженный с формы документ
     * @return сохраненный или обновленный объект
     */
    FefuUserManual doSaveOrUpdate(FefuUserManual userManual, IUploadFile document);

    /**
     * Если id null, то создается новый объект, если нет, то поднимается существующий из базы.
     * @param id id объекта
     * @return UserManual
     */
    FefuUserManual prepare(Long id);
}
