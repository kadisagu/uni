package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О переносе сдачи государственного экзамена
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuChangeDateStateExaminationStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract";
    public static final String ENTITY_NAME = "fefuChangeDateStateExaminationStuExtract";
    public static final int VERSION_HASH = 990263338;
    private static IEntityMeta ENTITY_META;

    public static final String P_NEW_DATE = "newDate";
    public static final String P_YEAR = "year";
    public static final String P_SEASON = "season";

    private Date _newDate;     // Новая дата
    private int _year;     // Год
    private String _season;     // Время года

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Новая дата. Свойство не может быть null.
     */
    @NotNull
    public Date getNewDate()
    {
        return _newDate;
    }

    /**
     * @param newDate Новая дата. Свойство не может быть null.
     */
    public void setNewDate(Date newDate)
    {
        dirty(_newDate, newDate);
        _newDate = newDate;
    }

    /**
     * @return Год. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Время года.
     */
    @Length(max=255)
    public String getSeason()
    {
        return _season;
    }

    /**
     * @param season Время года.
     */
    public void setSeason(String season)
    {
        dirty(_season, season);
        _season = season;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuChangeDateStateExaminationStuExtractGen)
        {
            setNewDate(((FefuChangeDateStateExaminationStuExtract)another).getNewDate());
            setYear(((FefuChangeDateStateExaminationStuExtract)another).getYear());
            setSeason(((FefuChangeDateStateExaminationStuExtract)another).getSeason());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuChangeDateStateExaminationStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuChangeDateStateExaminationStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuChangeDateStateExaminationStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                    return obj.getNewDate();
                case "year":
                    return obj.getYear();
                case "season":
                    return obj.getSeason();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "newDate":
                    obj.setNewDate((Date) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "season":
                    obj.setSeason((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                        return true;
                case "year":
                        return true;
                case "season":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                    return true;
                case "year":
                    return true;
                case "season":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "newDate":
                    return Date.class;
                case "year":
                    return Integer.class;
                case "season":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuChangeDateStateExaminationStuExtract> _dslPath = new Path<FefuChangeDateStateExaminationStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuChangeDateStateExaminationStuExtract");
    }
            

    /**
     * @return Новая дата. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract#getNewDate()
     */
    public static PropertyPath<Date> newDate()
    {
        return _dslPath.newDate();
    }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Время года.
     * @see ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract#getSeason()
     */
    public static PropertyPath<String> season()
    {
        return _dslPath.season();
    }

    public static class Path<E extends FefuChangeDateStateExaminationStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _newDate;
        private PropertyPath<Integer> _year;
        private PropertyPath<String> _season;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Новая дата. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract#getNewDate()
     */
        public PropertyPath<Date> newDate()
        {
            if(_newDate == null )
                _newDate = new PropertyPath<Date>(FefuChangeDateStateExaminationStuExtractGen.P_NEW_DATE, this);
            return _newDate;
        }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(FefuChangeDateStateExaminationStuExtractGen.P_YEAR, this);
            return _year;
        }

    /**
     * @return Время года.
     * @see ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract#getSeason()
     */
        public PropertyPath<String> season()
        {
            if(_season == null )
                _season = new PropertyPath<String>(FefuChangeDateStateExaminationStuExtractGen.P_SEASON, this);
            return _season;
        }

        public Class getEntityClass()
        {
            return FefuChangeDateStateExaminationStuExtract.class;
        }

        public String getEntityName()
        {
            return "fefuChangeDateStateExaminationStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
