/**
 * $Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionCompetenceList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.*;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 22.02.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        prepareDataSourceList(model);
    }

    private void prepareDataSourceList(Model model)
    {
        EppEduPlanVersion version = get(EppEduPlanVersion.class, model.getId());
        List<EppEduPlanVersionBlock> blocks = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.L_EDU_PLAN_VERSION, version);
        model.setBlocks(blocks);
        EppEduPlanVersionBlock rootBlock = blocks.stream().filter(EppEduPlanVersionBlock::isRootBlock).findFirst().get();
        EppStateEduStandard eduStandard = version.getEduPlan().getParent();

        List<FefuCompetenceListWrapper> wrappersFromRoot = getUsedCompetenceWrapers(rootBlock);
        for (EppEduPlanVersionBlock block : blocks)
        {
            StaticListDataSource<FefuCompetenceListWrapper> dataSource = new StaticListDataSource<>();
            dataSource.addColumn(new SimpleColumn("Код компетенции", FefuCompetenceListWrapper.COMPETENCE_CODE).setWidth(8).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Название", FefuCompetenceListWrapper.COMPETENCE_TITLE).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Строка УП", FefuCompetenceListWrapper.DISCIPLINE_TITLES).setFormatter(FefuCompetenceListWrapper.FEFU_EPV_ROW_FORMATTER).setClickable(false).setOrderable(false));

            List<FefuCompetenceListWrapper> wrappers;
            if (block.isRootBlock()) wrappers = wrappersFromRoot;
            else
            {
                wrappers = getUsedCompetenceWrapers(block);
                if (!wrappersFromRoot.isEmpty())
                {
                    Map<Long, FefuCompetenceListWrapper> map = wrappers.stream().collect(Collectors.toMap(IdentifiableWrapper::getId, w -> w));
                    wrappersFromRoot.forEach(fromRoot -> {
                        FefuCompetenceListWrapper fromBlock = map.get(fromRoot.getId());
                        if (fromBlock != null) fromBlock.addRows(fromRoot);
                        else wrappers.add(fromRoot);
                    });
                }
            }

            List<FefuCompetenceListWrapper> competencesFromStd = getUnusedCompetenceWrapers(wrappers, eduStandard);
            if (competencesFromStd != null)
                wrappers.addAll(competencesFromStd);

            wrappers.sort(FefuCompetenceListWrapper.COMPETENCE_COMPARATOR);
            dataSource.setRowList(wrappers);

            model.getDataSourceMap().put(block, dataSource);
        }
    }

    private static class CompetenceColor
    {
        private static final String DEFAULT_EPV_ROW_COMPETENCE_COLOR = "#DB0000";
        private static String epvRowCompetenceColor;

        private static String getEpvRowCompetenceColor()
        {
            if (epvRowCompetenceColor == null)
            {
                epvRowCompetenceColor = getColor("epvRowCompetenceColor", DEFAULT_EPV_ROW_COMPETENCE_COLOR);
            }

            return epvRowCompetenceColor;
        }

        private static final String DEFAULT_REG_ELEMENT_COMPETENCE_COLOR = "#006600";
        private static String registryElementCompetenceColor;

        private static String getRegistryElementCompetenceColor()
        {
            if (registryElementCompetenceColor == null)
            {
                registryElementCompetenceColor = getColor("registryElementCompetenceColor", DEFAULT_REG_ELEMENT_COMPETENCE_COLOR);
            }

            return registryElementCompetenceColor;
        }

        private static final String DEFAULT_COMMON_COMPETENCE_COLOR = "#000000";
        private static String commonCompetenceColor;

        private static String getCommonCompetenceColor()
        {
            if (commonCompetenceColor == null)
            {
                commonCompetenceColor = getColor("commonCompetenceColor", DEFAULT_COMMON_COMPETENCE_COLOR);
            }

            return commonCompetenceColor;
        }

        private static String getColor(String propertyName, String defaultValue)
        {
            String color = StringUtils.trimToNull(ApplicationRuntime.getProperty(propertyName));
            return color == null ? defaultValue : color;
        }
    }

    /**
     * Используемые в блоке компетенции
     */
    private List<FefuCompetenceListWrapper> getUsedCompetenceWrapers(EppEduPlanVersionBlock block)
    {
        //Используемые компетенции блока из строк
        String cm2row_alias = "cm2row";
        List<FefuAbstractCompetence2EpvRegistryRowRel> rowRelsFromBlock = getList(
                new DQLSelectBuilder()
                        .fromEntity(FefuAbstractCompetence2EpvRegistryRowRel.class, cm2row_alias)
                        .column(property(cm2row_alias))
                        .where(eq(property(cm2row_alias, FefuAbstractCompetence2EpvRegistryRowRel.registryRow().owner().id()), value(block.getId())))
        );

        //Используемые компетенции блока из элементов реестра
        List<Long> regIds = rowRelsFromBlock.stream()
                .filter(rel -> rel.getRegistryRow().getRegistryElement() != null)
                .map(rel -> rel.getRegistryRow().getRegistryElement().getId())
                .collect(Collectors.toList());
        String cm2reg_alias = "cm2reg";
        List<FefuCompetence2RegistryElementRel> regRelsFromBlock = getList(
                new DQLSelectBuilder()
                        .fromEntity(FefuCompetence2RegistryElementRel.class, cm2reg_alias)
                        .column(property(cm2reg_alias))
                        .where(in(property(cm2reg_alias, FefuCompetence2RegistryElementRel.registryElement().id()), regIds))
        );

        Map<FefuCompetence, List<FefuAbstractCompetence2EpvRegistryRowRel>> usedCompetenceWithRels = rowRelsFromBlock.stream()
                .filter(rel -> rel.getRegistryRow().getRegistryElement() != null)
                .collect(Collectors.groupingBy(
                        FefuAbstractCompetence2EpvRegistryRowRel::getFefuCompetence,
                        Collectors.mapping(rel -> rel, Collectors.toList())));
        Map<Long, Set<Long>> usedCompetenceByRegEl = regRelsFromBlock.stream()
                .collect(Collectors.groupingBy(
                                 rel -> rel.getRegistryElement().getId(),
                                 Collectors.mapping(rel -> rel.getFefuCompetence().getId(), Collectors.toSet()))
                );

        return usedCompetenceWithRels.entrySet().stream()
                .map(entry -> {
                    final FefuCompetence competence = entry.getKey();
                    List<FefuAbstractCompetence2EpvRegistryRowRel> rels = entry.getValue();
                    FefuAbstractCompetence2EpvRegistryRowRel firstRel = rels.iterator().next();
                    FefuCompetenceListWrapper wrapper = new FefuCompetenceListWrapper(competence, firstRel.getGroupNumber());

                    rels.forEach(rel -> {
                        Long regElId = rel.getRegistryRow().getRegistryElement().getId();
                        String color;
                        Set<Long> competences = usedCompetenceByRegEl.get(regElId);
                        if (competences !=null && competences.contains(competence.getId()))
                            color = CompetenceColor.getCommonCompetenceColor();
                        else color = CompetenceColor.getEpvRowCompetenceColor();

                        wrapper.addRow(rel.getRegistryRow(), color);
                    });
                    return wrapper;
                })
                .collect(Collectors.toList());
    }

    /**
     * Неиспользуемы компетенции из ГОСа
     */
    private List<FefuCompetenceListWrapper> getUnusedCompetenceWrapers(List<FefuCompetenceListWrapper> used, EppStateEduStandard eduStandard)
    {
        if (eduStandard == null) return null;

        String cm2std_alias = "cm2std";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(FefuCompetence2EppStateEduStandardRel.class, cm2std_alias)
                .column(property(cm2std_alias))
                .where(eq(property(cm2std_alias, FefuCompetence2EppStateEduStandardRel.eduStandard().id()), value(eduStandard.getId())));

        if (used != null && !used.isEmpty())
        {
            List<Long> cmpIds = used.stream().map(IdentifiableWrapper::getId).collect(Collectors.toList());
            builder.where(notIn(property(cm2std_alias, FefuCompetence2EppStateEduStandardRel.fefuCompetence().id()), cmpIds));
        }

        List<FefuCompetence2EppStateEduStandardRel> stdRels = builder.createStatement(getSession()).<FefuCompetence2EppStateEduStandardRel>list();

        return stdRels.stream().map(rel -> new FefuCompetenceListWrapper(rel.getFefuCompetence(), rel.getGroupNumber()))
                .collect(Collectors.toList());
    }
}