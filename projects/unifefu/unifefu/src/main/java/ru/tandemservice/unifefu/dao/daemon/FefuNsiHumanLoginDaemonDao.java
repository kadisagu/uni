/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuProcessingStack;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanType;
import ru.tandemservice.unifefu.ws.nsi.datagram.XDatagram;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;
import ru.tandemservice.unifefu.ws.nsi.servertest.ServiceRequestTypeDatagram;
import ru.tandemservice.unifefu.ws.nsi.servertest.ServiceSoapImplServiceLocator;

import java.io.ByteArrayInputStream;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 08.09.2013
 */
public class FefuNsiHumanLoginDaemonDao extends UniBaseDao implements IFefuNsiHumanLoginDaemonDao
{
    public static final int DAEMON_ITERATION_TIME = 1;  //TODO

    private static boolean sync_locked = false;
    private static boolean manual_locked = false;

    public static void lockDaemon()
    {
        manual_locked = true;
    }

    public static void unlockDaemon()
    {
        manual_locked = false;
    }

    public static boolean isLocked()
    {
        return manual_locked;
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(FefuNsiHumanLoginDaemonDao.class.getName(), DAEMON_ITERATION_TIME, IFefuNsiDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            // Если в данный момент идёт синхронизация, то пропускаем очередной шаг демона
            if (sync_locked || manual_locked) return;

            final IFefuNsiHumanLoginDaemonDao dao = IFefuNsiHumanLoginDaemonDao.instance.get();
            sync_locked = true;

            try
            {
                dao.doSyncLoginsWithNSI();
            } catch (final Throwable t)
            {
                sync_locked = false;
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            }
            sync_locked = false;
        }
    };

    protected Session lock4update()
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, IFefuNsiHumanLoginDaemonDao.GLOBAL_DAEMON_LOCK);
        return session;
    }

    @Override
    public void doSyncLoginsWithNSI()
    {
        int stackCount = ((Number) new DQLSelectBuilder().fromEntity(FefuProcessingStack.class, "s").createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
        if (0 == stackCount) return;

        System.out.println("+++++++++++ FefuNsiHumanLoginDaemonDao has started with 100 of" + stackCount + " items from stack.");

        List<Object[]> portion = new DQLSelectBuilder().fromEntity(FefuProcessingStack.class, "e").top(100)
                .column(property(FefuProcessingStack.entityId().fromAlias("e"))).column(property(FefuNsiIds.guid().fromAlias("ids")))
                .joinEntity("e", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(FefuProcessingStack.entityId().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .createStatement(getSession()).list();

        Set<Long> personIdSet = new HashSet<>();
        Set<String> fullGuidSet = new HashSet<>();
        List<HumanType> requestDatagram = new ArrayList<>();

        for (Object[] item : portion)
        {
            personIdSet.add((Long) item[0]);
            fullGuidSet.add((String) item[1]);
            HumanType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createHumanType();
            nsiEntity.setID((String) item[1]);
            requestDatagram.add(nsiEntity);
        }

        List<HumanType> humanTypesList = NsiReactorUtils.retrieveObjectsFromNSIByElementsList(HumanType.class, requestDatagram, 20);

        Set<String> guidSet = new HashSet<>();
        for (HumanType item : humanTypesList)
        {
            if (null != item.getHumanLogin()) guidSet.add(item.getID());
        }

        List<String> existPersons = new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "ids").column(property(FefuNsiIds.guid().fromAlias("ids")))
                .joinEntity("ids", DQLJoinType.inner, Person.class, "p", eq(property(FefuNsiIds.entityId().fromAlias("ids")), property(Person.id().fromAlias("p"))))
                .where(in(property(FefuNsiIds.guid().fromAlias("ids")), guidSet))
                .createStatement(getSession()).list();

        int sentToUpdate = 0;
        List<Object> loginFilledHumansList = new ArrayList<>();

        List<String> absentGuids = new ArrayList<>();
        List<String> loginFreeGuids = new ArrayList<>();
        absentGuids.addAll(fullGuidSet);

        for (HumanType item : humanTypesList)
        {
            if(null == item.getHumanLogin()) loginFreeGuids.add(item.getID());

            if (null != item.getHumanLogin() && existPersons.contains(item.getID()))
            {
                absentGuids.remove(item.getID());
                System.out.println("+++++++++++ FefuNsiHumanLoginDaemonDao --- Update login from NSI for " + item.getID() + " - " + item.getHumanLogin() + " - " + item.getHumanLastName() + " " + item.getHumanFirstName() + " " + item.getHumanMiddleName());
                loginFilledHumansList.add(item);
                sentToUpdate++;
            }
        }

        absentGuids.removeAll(loginFreeGuids);

        for(String guid : absentGuids) System.out.println("+++++++++++ FefuNsiHumanLoginDaemonDao --- Human with guid " + guid + " was not found at NSI.");
        for(String guid : loginFreeGuids) System.out.println("+++++++++++ FefuNsiHumanLoginDaemonDao --- Login for Human with guid " + guid + " is not specified at NSI.");

        ServiceSoapImplServiceLocator service = new ServiceSoapImplServiceLocator();

        ru.tandemservice.unifefu.ws.nsi.servertest.ServiceRequestType request = new ru.tandemservice.unifefu.ws.nsi.servertest.ServiceRequestType();
        ru.tandemservice.unifefu.ws.nsi.servertest.RoutingHeaderType header = new ru.tandemservice.unifefu.ws.nsi.servertest.RoutingHeaderType();
        ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
        XDatagram xDatagram = NsiReactorUtils.NSI_OBJECT_FACTORY.createXDatagram();
        xDatagram.getEntityList().addAll(loginFilledHumansList);

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram));
        MessageElement datagramOut = new SOAPBodyElement(inStream);

        //System.out.println(datagramOut.toString());

        header.setSourceId("NSI (by OB request)");
        header.setOperationType("update");
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        datagram.set_any(new MessageElement[]{datagramOut});
        request.setDatagram(datagram);

        try
        {
            service.getServiceSoapPort().update(request);
            new DQLDeleteBuilder(FefuProcessingStack.class).where(in(property(FefuProcessingStack.entityId()), personIdSet)).createStatement(getSession()).execute();
            System.out.println("+++++++++++ FefuNsiHumanLoginDaemonDao --- There were " + sentToUpdate + " logins updated from NSI.");
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}