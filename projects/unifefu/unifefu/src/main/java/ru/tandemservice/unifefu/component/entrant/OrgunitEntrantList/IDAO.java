/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.OrgunitEntrantList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Nikolay Fedorovskih
 * @since 08.05.2013
 */
public interface IDAO extends IUniDao<Model>
{
}