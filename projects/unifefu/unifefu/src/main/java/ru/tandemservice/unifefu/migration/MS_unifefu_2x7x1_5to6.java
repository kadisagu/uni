package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Dmitry Seleznev
 * @since 19.01.2015
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.16"),
                        new ScriptDependency("org.tandemframework.shared", "1.7.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("delete from fefunsiids_t where entityid_p in (" +
                "select pc.id from fefunsipersoncontact_t pc inner join addressbase_t ab on pc.address_id=ab.id where ab.id not in (" +
                "select id from addressdetailed_t union select id from addressru_t union select id from addressinter_t union select id from addressstring_t))");

        tool.executeUpdate("delete from fefunsipersoncontact_t where id in (" +
                "select pc.id from fefunsipersoncontact_t pc inner join addressbase_t ab on pc.address_id=ab.id where ab.id not in (" +
                "select id from addressdetailed_t union select id from addressru_t union select id from addressinter_t union select id from addressstring_t))");

        tool.executeUpdate("update person_t set address_id=null where address_id in (select id from addressbase_t ab where ab.id not in (" +
                "select id from addressdetailed_t union select id from addressru_t union select id from addressinter_t union select id from addressstring_t))");

        tool.executeUpdate("update identitycard_t set address_id=null where address_id in (select id from addressbase_t ab where ab.id not in (" +
                "select id from addressdetailed_t union select id from addressru_t union select id from addressinter_t union select id from addressstring_t))");
    }
}