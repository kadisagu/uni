/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.brs.base.IBaseFefuBrsObjectManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.logic.FefuBrsAttestationResultsReportDAO;
import ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.logic.IFefuBrsAttestationResultsReportDAO;

/**
 * @author nvankov
 * @since 12/4/13
 */
@Configuration
public class FefuBrsAttestationResultsReportManager extends BusinessObjectManager implements IBaseFefuBrsObjectManager
{
    @Override
    public String getPermissionKey()
    {
        return "fefuBrsDelayGradingReportPermissionKey";
    }

    public static FefuBrsAttestationResultsReportManager instance()
    {
        return instance(FefuBrsAttestationResultsReportManager.class);
    }

    @Bean
    public IFefuBrsAttestationResultsReportDAO dao()
    {
        return new FefuBrsAttestationResultsReportDAO();
    }
}



    