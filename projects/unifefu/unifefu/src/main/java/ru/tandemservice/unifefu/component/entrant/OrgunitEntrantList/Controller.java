/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.OrgunitEntrantList;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.component.wizard.IEntrantWizardComponents;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 08.05.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        OrgUnit orgUnit = ((ru.tandemservice.uniec.component.orgunit.OrgUnitEntrantList.Model)component.getModel(component.getName())).getOrgUnit();
        model.setOrgUnit(orgUnit);

        getDao().prepare(model);
    }

    public void onClickAddEntrant(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniecComponents.ENTRANT_ADD));
    }

    public void onClickAddEntrantWizard(IBusinessComponent component)
    {
        List<EnrollmentCampaign> openedCampaigns = getDao().getList(EnrollmentCampaign.class, EnrollmentCampaign.closed().s(), false);
        if (CollectionUtils.isEmpty(openedCampaigns))
            return;

        boolean searchBeforeEntrantRegistration = openedCampaigns.stream().anyMatch(EnrollmentCampaign::isSearchBeforeEntrantRegistration);
        component.createDefaultChildRegion(new ComponentActivator(searchBeforeEntrantRegistration ? IEntrantWizardComponents.SEARCH_STEP : IEntrantWizardComponents.IDENTITY_CARD_STEP));
    }

}