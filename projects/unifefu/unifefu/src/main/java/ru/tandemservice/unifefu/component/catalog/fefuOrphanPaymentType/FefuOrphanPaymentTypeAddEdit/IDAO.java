/* $Id$ */
package ru.tandemservice.unifefu.component.catalog.fefuOrphanPaymentType.FefuOrphanPaymentTypeAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;

/**
 * @author nvankov
 * @since 11/18/13
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<FefuOrphanPaymentType, Model>
{
}
