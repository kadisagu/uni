/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.logic;

import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;
import ru.tandemservice.unifefu.ws.foreignOnlineEntrants.FefuForeignOnlineEntrantsService;

/**
 * @author nvankov
 * @since 4/2/13
 */
public interface IFefuForeignOnlineEntrantDAO
{
    void createOrUpdate(FefuForeignOnlineEntrant foreignOnlineEntrant);

    void webServiceCreateOrUpdate(FefuForeignOnlineEntrantsService.ForeignOnlineEntrant foreignOnlineEntrantWS);

    FefuForeignOnlineEntrantsService.ForeignOnlineEntrant webServiceGetForeignOnlineEntrant(Long userId);

    FefuForeignOnlineEntrant getFefuForeignOnlineEntrant(long userId);

    boolean canDeleteEntrant(long userId);

    void deleteForeignOnlineEntrant(Long foreignOnlineEntrantId);

    boolean deleteForeignOnlineEntrantWS(long userId);

    boolean isForeignOnlineEntrantAccepted(long userId);

    String getForeignDocumentCopiesName(long userId);

    byte[] getForeignDocumentCopies(long userId);

    void deleteForeignDocumentCopies(long userId);

    void saveForeignDocumentCopies(long userId, byte[] zipArchive, String documentCopiesName);
}
