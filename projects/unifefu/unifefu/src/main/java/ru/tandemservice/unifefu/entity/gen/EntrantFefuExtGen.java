package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unifefu.entity.EntrantFefuExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абитуриент (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantFefuExtGen extends EntityBase
 implements INaturalIdentifiable<EntrantFefuExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.EntrantFefuExt";
    public static final String ENTITY_NAME = "entrantFefuExt";
    public static final int VERSION_HASH = 25929931;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String P_PARTICIPATE_IN_SECOND_WAVE_U_S_E = "participateInSecondWaveUSE";
    public static final String P_PARTICIPATE_IN_ENTRANCE_TESTS = "participateInEntranceTests";
    public static final String P_EDU_INSTITUTION_STR = "eduInstitutionStr";
    public static final String P_IS_ONLINE_ENTRANT = "isOnlineEntrant";
    public static final String P_AGREE_WITH_PRIVACY_NOTICE = "agreeWithPrivacyNotice";

    private Entrant _entrant;     // (Старый) Абитуриент
    private Boolean _participateInSecondWaveUSE;     // Участие во второй волне ЕГЭ
    private Boolean _participateInEntranceTests;     // Участие во вступительных испытаниях, проводимых вузом самостоятельно
    private String _eduInstitutionStr;     // Образовательное учреждение (текст)
    private Boolean _isOnlineEntrant;     // Является абитуриентом online-регистрации
    private Boolean _agreeWithPrivacyNotice;     // Согласен с правилами вуза и обработкой персональных данных

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Участие во второй волне ЕГЭ.
     */
    public Boolean getParticipateInSecondWaveUSE()
    {
        return _participateInSecondWaveUSE;
    }

    /**
     * @param participateInSecondWaveUSE Участие во второй волне ЕГЭ.
     */
    public void setParticipateInSecondWaveUSE(Boolean participateInSecondWaveUSE)
    {
        dirty(_participateInSecondWaveUSE, participateInSecondWaveUSE);
        _participateInSecondWaveUSE = participateInSecondWaveUSE;
    }

    /**
     * @return Участие во вступительных испытаниях, проводимых вузом самостоятельно.
     */
    public Boolean getParticipateInEntranceTests()
    {
        return _participateInEntranceTests;
    }

    /**
     * @param participateInEntranceTests Участие во вступительных испытаниях, проводимых вузом самостоятельно.
     */
    public void setParticipateInEntranceTests(Boolean participateInEntranceTests)
    {
        dirty(_participateInEntranceTests, participateInEntranceTests);
        _participateInEntranceTests = participateInEntranceTests;
    }

    /**
     * @return Образовательное учреждение (текст).
     */
    @Length(max=255)
    public String getEduInstitutionStr()
    {
        return _eduInstitutionStr;
    }

    /**
     * @param eduInstitutionStr Образовательное учреждение (текст).
     */
    public void setEduInstitutionStr(String eduInstitutionStr)
    {
        dirty(_eduInstitutionStr, eduInstitutionStr);
        _eduInstitutionStr = eduInstitutionStr;
    }

    /**
     * @return Является абитуриентом online-регистрации.
     */
    public Boolean getIsOnlineEntrant()
    {
        return _isOnlineEntrant;
    }

    /**
     * @param isOnlineEntrant Является абитуриентом online-регистрации.
     */
    public void setIsOnlineEntrant(Boolean isOnlineEntrant)
    {
        dirty(_isOnlineEntrant, isOnlineEntrant);
        _isOnlineEntrant = isOnlineEntrant;
    }

    /**
     * @return Согласен с правилами вуза и обработкой персональных данных.
     */
    public Boolean getAgreeWithPrivacyNotice()
    {
        return _agreeWithPrivacyNotice;
    }

    /**
     * @param agreeWithPrivacyNotice Согласен с правилами вуза и обработкой персональных данных.
     */
    public void setAgreeWithPrivacyNotice(Boolean agreeWithPrivacyNotice)
    {
        dirty(_agreeWithPrivacyNotice, agreeWithPrivacyNotice);
        _agreeWithPrivacyNotice = agreeWithPrivacyNotice;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantFefuExtGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrant(((EntrantFefuExt)another).getEntrant());
            }
            setParticipateInSecondWaveUSE(((EntrantFefuExt)another).getParticipateInSecondWaveUSE());
            setParticipateInEntranceTests(((EntrantFefuExt)another).getParticipateInEntranceTests());
            setEduInstitutionStr(((EntrantFefuExt)another).getEduInstitutionStr());
            setIsOnlineEntrant(((EntrantFefuExt)another).getIsOnlineEntrant());
            setAgreeWithPrivacyNotice(((EntrantFefuExt)another).getAgreeWithPrivacyNotice());
        }
    }

    public INaturalId<EntrantFefuExtGen> getNaturalId()
    {
        return new NaturalId(getEntrant());
    }

    public static class NaturalId extends NaturalIdBase<EntrantFefuExtGen>
    {
        private static final String PROXY_NAME = "EntrantFefuExtNaturalProxy";

        private Long _entrant;

        public NaturalId()
        {}

        public NaturalId(Entrant entrant)
        {
            _entrant = ((IEntity) entrant).getId();
        }

        public Long getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Long entrant)
        {
            _entrant = entrant;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EntrantFefuExtGen.NaturalId) ) return false;

            EntrantFefuExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrant(), that.getEntrant()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrant());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrant());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantFefuExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantFefuExt.class;
        }

        public T newInstance()
        {
            return (T) new EntrantFefuExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "participateInSecondWaveUSE":
                    return obj.getParticipateInSecondWaveUSE();
                case "participateInEntranceTests":
                    return obj.getParticipateInEntranceTests();
                case "eduInstitutionStr":
                    return obj.getEduInstitutionStr();
                case "isOnlineEntrant":
                    return obj.getIsOnlineEntrant();
                case "agreeWithPrivacyNotice":
                    return obj.getAgreeWithPrivacyNotice();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "participateInSecondWaveUSE":
                    obj.setParticipateInSecondWaveUSE((Boolean) value);
                    return;
                case "participateInEntranceTests":
                    obj.setParticipateInEntranceTests((Boolean) value);
                    return;
                case "eduInstitutionStr":
                    obj.setEduInstitutionStr((String) value);
                    return;
                case "isOnlineEntrant":
                    obj.setIsOnlineEntrant((Boolean) value);
                    return;
                case "agreeWithPrivacyNotice":
                    obj.setAgreeWithPrivacyNotice((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "participateInSecondWaveUSE":
                        return true;
                case "participateInEntranceTests":
                        return true;
                case "eduInstitutionStr":
                        return true;
                case "isOnlineEntrant":
                        return true;
                case "agreeWithPrivacyNotice":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "participateInSecondWaveUSE":
                    return true;
                case "participateInEntranceTests":
                    return true;
                case "eduInstitutionStr":
                    return true;
                case "isOnlineEntrant":
                    return true;
                case "agreeWithPrivacyNotice":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "participateInSecondWaveUSE":
                    return Boolean.class;
                case "participateInEntranceTests":
                    return Boolean.class;
                case "eduInstitutionStr":
                    return String.class;
                case "isOnlineEntrant":
                    return Boolean.class;
                case "agreeWithPrivacyNotice":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantFefuExt> _dslPath = new Path<EntrantFefuExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantFefuExt");
    }
            

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Участие во второй волне ЕГЭ.
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getParticipateInSecondWaveUSE()
     */
    public static PropertyPath<Boolean> participateInSecondWaveUSE()
    {
        return _dslPath.participateInSecondWaveUSE();
    }

    /**
     * @return Участие во вступительных испытаниях, проводимых вузом самостоятельно.
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getParticipateInEntranceTests()
     */
    public static PropertyPath<Boolean> participateInEntranceTests()
    {
        return _dslPath.participateInEntranceTests();
    }

    /**
     * @return Образовательное учреждение (текст).
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getEduInstitutionStr()
     */
    public static PropertyPath<String> eduInstitutionStr()
    {
        return _dslPath.eduInstitutionStr();
    }

    /**
     * @return Является абитуриентом online-регистрации.
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getIsOnlineEntrant()
     */
    public static PropertyPath<Boolean> isOnlineEntrant()
    {
        return _dslPath.isOnlineEntrant();
    }

    /**
     * @return Согласен с правилами вуза и обработкой персональных данных.
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getAgreeWithPrivacyNotice()
     */
    public static PropertyPath<Boolean> agreeWithPrivacyNotice()
    {
        return _dslPath.agreeWithPrivacyNotice();
    }

    public static class Path<E extends EntrantFefuExt> extends EntityPath<E>
    {
        private Entrant.Path<Entrant> _entrant;
        private PropertyPath<Boolean> _participateInSecondWaveUSE;
        private PropertyPath<Boolean> _participateInEntranceTests;
        private PropertyPath<String> _eduInstitutionStr;
        private PropertyPath<Boolean> _isOnlineEntrant;
        private PropertyPath<Boolean> _agreeWithPrivacyNotice;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Участие во второй волне ЕГЭ.
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getParticipateInSecondWaveUSE()
     */
        public PropertyPath<Boolean> participateInSecondWaveUSE()
        {
            if(_participateInSecondWaveUSE == null )
                _participateInSecondWaveUSE = new PropertyPath<Boolean>(EntrantFefuExtGen.P_PARTICIPATE_IN_SECOND_WAVE_U_S_E, this);
            return _participateInSecondWaveUSE;
        }

    /**
     * @return Участие во вступительных испытаниях, проводимых вузом самостоятельно.
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getParticipateInEntranceTests()
     */
        public PropertyPath<Boolean> participateInEntranceTests()
        {
            if(_participateInEntranceTests == null )
                _participateInEntranceTests = new PropertyPath<Boolean>(EntrantFefuExtGen.P_PARTICIPATE_IN_ENTRANCE_TESTS, this);
            return _participateInEntranceTests;
        }

    /**
     * @return Образовательное учреждение (текст).
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getEduInstitutionStr()
     */
        public PropertyPath<String> eduInstitutionStr()
        {
            if(_eduInstitutionStr == null )
                _eduInstitutionStr = new PropertyPath<String>(EntrantFefuExtGen.P_EDU_INSTITUTION_STR, this);
            return _eduInstitutionStr;
        }

    /**
     * @return Является абитуриентом online-регистрации.
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getIsOnlineEntrant()
     */
        public PropertyPath<Boolean> isOnlineEntrant()
        {
            if(_isOnlineEntrant == null )
                _isOnlineEntrant = new PropertyPath<Boolean>(EntrantFefuExtGen.P_IS_ONLINE_ENTRANT, this);
            return _isOnlineEntrant;
        }

    /**
     * @return Согласен с правилами вуза и обработкой персональных данных.
     * @see ru.tandemservice.unifefu.entity.EntrantFefuExt#getAgreeWithPrivacyNotice()
     */
        public PropertyPath<Boolean> agreeWithPrivacyNotice()
        {
            if(_agreeWithPrivacyNotice == null )
                _agreeWithPrivacyNotice = new PropertyPath<Boolean>(EntrantFefuExtGen.P_AGREE_WITH_PRIVACY_NOTICE, this);
            return _agreeWithPrivacyNotice;
        }

        public Class getEntityClass()
        {
            return EntrantFefuExt.class;
        }

        public String getEntityName()
        {
            return "entrantFefuExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
