package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;

import java.util.Date;

import static org.tandemframework.core.CoreStringUtils.escapeLike;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.logic.Constants.*;

public class ApeProgramDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public ApeProgramDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuAdditionalProfessionalEducationProgram.class, ALIAS).column(ALIAS);

        Object educationOrgUnit = context.get(EDUCATION_ORG_UNIT_F);
        if (educationOrgUnit != null)
        {
            builder.where(eq(
                    property(FefuAdditionalProfessionalEducationProgram.educationOrgUnit().fromAlias(ALIAS)),
                    commonValue(educationOrgUnit)));
        }

        String title = context.get(TITLE_F);
        if (title != null)
        {
            builder.where(likeUpper(
                    property(FefuAdditionalProfessionalEducationProgram.title().fromAlias(ALIAS)),
                    value(escapeLike(title))));
        }

        Object formativeOrgUnit = context.get(FORMATIVE_ORG_UNIT_F);
        if (formativeOrgUnit != null)
        {
            builder.where(eq(
                    property(FefuAdditionalProfessionalEducationProgram.formativeOrgUnit().fromAlias(ALIAS)),
                    commonValue(formativeOrgUnit)));
        }

        Object territorialOrgUnit = context.get(TERRITORIAL_ORG_UNIT_F);
        if (territorialOrgUnit != null)
        {
            builder.where(eq(
                    property(FefuAdditionalProfessionalEducationProgram.territorialOrgUnit().fromAlias(ALIAS)),
                    commonValue(territorialOrgUnit)));
        }

        Date openDateFrom = context.get(PROGRAM_OPENING_DATE_FROM_F);
        Date openDateTo = context.get(PROGRAM_OPENING_DATE_TO_F);
        builder.where(betweenDays(FefuAdditionalProfessionalEducationProgram.programOpeningDate().fromAlias(ALIAS),
                                  openDateFrom,
                                  openDateTo));

        Date closingDateFrom = context.get(PROGRAM_CLOSING_DATE_FROM_F);
        Date closingDateTo = context.get(PROGRAM_CLOSING_DATE_TO_F);
        builder.where(betweenDays(FefuAdditionalProfessionalEducationProgram.programClosingDate().fromAlias(ALIAS),
                                  closingDateFrom,
                                  closingDateTo));

        Object producingOrgUnit = context.get(PRODUCING_ORG_UNIT_F);
        if (producingOrgUnit != null)
        {
            builder.where(eq(
                    property(FefuAdditionalProfessionalEducationProgram.producingOrgUnit().fromAlias(ALIAS)),
                    commonValue(producingOrgUnit)));
        }

        Object programStatus = context.get(PROGRAM_STATUS_F);
        if (programStatus != null)
        {
            builder.where(eq(
                    property(FefuAdditionalProfessionalEducationProgram.programStatus().fromAlias(ALIAS)),
                    commonValue(programStatus)));
        }

        Object programPublicationStatus = context.get(PROGRAM_PUBLICATION_STATUS_F);
        if (programPublicationStatus != null)
        {
            builder.where(eq(
                    property(FefuAdditionalProfessionalEducationProgram.programPublicationStatus().fromAlias(ALIAS)),
                    commonValue(programPublicationStatus)));
        }

        Object educationLevel = context.get(EDUCATION_LEVEL_F);
        if (educationLevel != null)
        {
            builder.where(eq(
                    property(FefuAdditionalProfessionalEducationProgram.educationLevel().fromAlias(ALIAS)),
                    commonValue(educationLevel)));
        }

        builder.order(property(ALIAS, input.getEntityOrder().getKeyString()), input.getEntityOrder().getDirection());

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }

}
