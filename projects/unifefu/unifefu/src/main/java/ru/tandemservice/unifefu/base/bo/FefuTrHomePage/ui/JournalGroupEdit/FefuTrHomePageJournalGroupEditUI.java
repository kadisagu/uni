/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTrHomePage.ui.JournalGroupEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.unifefu.entity.TrOrgUnitSettingsFefuExt;
import ru.tandemservice.unitraining.base.bo.TrHomePage.TrHomePageManager;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalMark.TrHomePageJournalMark;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditUI;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.ScheduleEventEdit.TrJournalScheduleEventEdit;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureView.ITrJournalStructureOwner;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureView.TrJournalStructureViewUI;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;

import java.util.*;

/**
 * @author Nikolay Fedorovskih
 * @since 17.02.2014
 */
@Input({
               @Bind(key = TrJournalGroupMarkEditUI.GROUP_BINDING, binding = "groupId", required = true)
       })
@Output({
                @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journalId"),
                @Bind(key = TrJournalStructureViewUI.GROUP_TYPE_BIND, binding = "groupTypeId"),
                @Bind(key = TrJournalGroupMarkEditUI.GROUP_BINDING, binding = "groupId"),
                @Bind(key = TrBrsCoefficientManager.BIND_OWNER_ID, binding = "journalId"),
                @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType.id")
        })
public class FefuTrHomePageJournalGroupEditUI extends UIPresenter implements ITrJournalStructureOwner
{
    private TrJournalGroup _group;
    private Long _groupId;
    private Map<Long, TrEduGroupEvent> _groupEventMap;
    private String _fcaGroupTitle;
    private Set<PpsEntry> _tutors;
    private TrBrsCoefficientOwnerType ownerType = new TrBrsCoefficientOwnerType();
    private TrOrgUnitSettingsFefuExt _ouSettingsExt;

    @Override
    public void onComponentRefresh()
    {
        setGroup(DataAccessServices.dao().getNotNull(TrJournalGroup.class, _groupId));

        _groupEventMap = new HashMap<>();
        List<TrEduGroupEvent> list = IUniBaseDao.instance.get().getList(TrEduGroupEvent.class, TrEduGroupEvent.L_GROUP, getGroup().getGroup());
        for (TrEduGroupEvent event : list)
        {
            _groupEventMap.put(event.getJournalEvent().getId(), event);
        }

        ownerType = DataAccessServices.dao().get(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerType.code(), TrBrsCoefficientOwnerTypeCodes.JOURNAL);

        _tutors = new LinkedHashSet<>(TrJournalManager.instance().dao().getTrJournalEppPps(getJournalId()));

        try
        {
            _fcaGroupTitle = TrBrsCoefficientManager.instance().coefDao().getFcaGroup(getJournal()).getTitle();
        }
        catch (ApplicationException e)
        {
            // ничего не делаем, считаем, что мероприятие просто не заполнено/отсутствует в реестре
            _fcaGroupTitle = "";
        }

        TrOrgUnitSettings brsSettings = TrBrsCoefficientManager.instance().brsDao().getOrgUnitBrsSettings(getGroup().getJournal().getYearPart(), getGroup().getJournal().getOrgUnit(), null);
        if (brsSettings != null)
        {
            _ouSettingsExt = DataAccessServices.dao().getByNaturalId(new TrOrgUnitSettingsFefuExt.NaturalId(brsSettings));
        }
    }

    public void onClickShow()
    {
        _uiActivation.asDesktopRoot(TrHomePageJournalMark.class)
                .parameter(TrJournalGroupMarkEditUI.GROUP_BINDING, getGroupId())
                .activate();
    }

    public boolean isEmpty()
    {
        return !TrHomePageManager.instance().dao().hasJournalsForMarking(_uiConfig.getUserContext().getPrincipalContext());
    }

    public String getFcaGroupTitle()
    {
        return _fcaGroupTitle;
    }

    @Override
    public String getPermissionKey()
    {
        return "";
    }

    @Override
    public boolean isEditable()
    {
        return _ouSettingsExt == null || !_ouSettingsExt.isBlockTutorEdit();
    }

    @Override
    public List<IButtonInfo> getEventButtonList()
    {
        return Arrays.<IButtonInfo>asList(
                new ButtonInfo("dateEdit", "Редактировать даты", "img/general/edit_date.png")
                {
                    @Override
                    public void onClick(Long entityId)
                    {
                        final TrEduGroupEvent groupEvent = getGroupEvent(entityId);
                        if (groupEvent == null)
                            throw new ApplicationException("Состав событий в журнале изменился, обновите страницу.");
                        _uiActivation.asRegionDialog(TrJournalScheduleEventEdit.class)
                                .parameter(IUIPresenter.PUBLISHER_ID, groupEvent.getId())
                                .activate();
                    }
                },
                new ButtonInfo("mark", "Выставить отметки", "img/general/edit_mark.png")
                {
                    @Override
                    public void onClick(Long entityId)
                    {
                        _uiActivation.asDesktopRoot(TrHomePageJournalMark.class)
                                .parameter(TrJournalGroupMarkEditUI.GROUP_BINDING, getGroupId())
                                .parameter(TrJournalGroupMarkEditUI.EVENT_BINDING, entityId)
                                .activate();
                    }

                    @Override
                    public boolean isDisabled(Long rowEntityId)
                    {
                        final TrEduGroupEvent groupEvent = getGroupEvent(rowEntityId);
                        return groupEvent == null || groupEvent.getScheduleEvent() == null;
                    }
                }
        );
    }

    private TrEduGroupEvent getGroupEvent(Long journalEventId)
    {
        return _groupEventMap.get(journalEventId);
    }

    public boolean isMarkState()
    {
        return !getJournal().getRegistryElementPart().getRegistryElement().getState().getCode().equals(EppState.STATE_ACCEPTED);
    }

    @Override
    public List<ITrJournalStructureOwner.IButtonInfo> getModuleButtonList()
    {
        return Collections.emptyList();
    }

    public TrJournalGroup getGroup()
    {
        return _group;
    }

    public void setGroup(TrJournalGroup group)
    {
        _group = group;
    }

    public TrJournal getJournal()
    {
        return getGroup().getJournal();
    }

    public Long getJournalId()
    {
        return getGroup() == null ? null : getGroup().getJournal().getId();
    }

    public Long getGroupTypeId()
    {
        return getGroup() == null ? null : getGroup().getGroup().getType().getId();
    }

    public Long getGroupId()
    {
        return _groupId;
    }

    public void setGroupId(Long groupId)
    {
        _groupId = groupId;
    }

    public Set<PpsEntry> getTutors()
    {
        return _tutors;
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }
}