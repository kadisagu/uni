/* $Id$ */
package ru.tandemservice.unifefu.base.bo.AdditionalEntrantData.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unifefu.entity.EntrantFefuExt;

/**
 * @author Nikolay Fedorovskih
 * @since 06.06.2013
 */
@Transactional
public class AdditionalEntrantDataDAO extends CommonDAO implements IAdditionalEntrantDataDAO
{
    @Override
    public EntrantFefuExt getEntrantFefuExt(Long entrantId)
    {
        Entrant entrant = proxy(entrantId);
        EntrantFefuExt entrantFefuExt = getByNaturalId(new EntrantFefuExt.NaturalId(entrant));
        if (entrantFefuExt == null)
        {
            entrantFefuExt = new EntrantFefuExt();
            entrantFefuExt.setEntrant(entrant);
        }
        return entrantFefuExt;
    }

    public void updateData(EntrantFefuExt entrantFefuExt)
    {
        saveOrUpdate(entrantFefuExt);
    }
}