package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.sec.entity.gen.IPrincipalContextGen;
import ru.tandemservice.unifefu.entity.SessionSheetMarkBy;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * пользователь, фактически проставивший оценку в экзам. листе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionSheetMarkByGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.SessionSheetMarkBy";
    public static final String ENTITY_NAME = "sessionSheetMarkBy";
    public static final int VERSION_HASH = -1127100122;
    private static IEntityMeta ENTITY_META;

    public static final String L_SESSION_DOCUMENT = "sessionDocument";
    public static final String L_PRINCIPAL_CONTEXT = "principalContext";

    private SessionDocument _sessionDocument;     // экзам. лист
    private IPrincipalContext _principalContext;     // пользователь, фактически проставивший оценку

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return экзам. лист. Свойство должно быть уникальным.
     */
    public SessionDocument getSessionDocument()
    {
        return _sessionDocument;
    }

    /**
     * @param sessionDocument экзам. лист. Свойство должно быть уникальным.
     */
    public void setSessionDocument(SessionDocument sessionDocument)
    {
        dirty(_sessionDocument, sessionDocument);
        _sessionDocument = sessionDocument;
    }

    /**
     * @return пользователь, фактически проставивший оценку. Свойство не может быть null.
     */
    @NotNull
    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    /**
     * @param principalContext пользователь, фактически проставивший оценку. Свойство не может быть null.
     */
    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && principalContext!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPrincipalContext.class);
            IEntityMeta actual =  principalContext instanceof IEntity ? EntityRuntime.getMeta((IEntity) principalContext) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_principalContext, principalContext);
        _principalContext = principalContext;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionSheetMarkByGen)
        {
            setSessionDocument(((SessionSheetMarkBy)another).getSessionDocument());
            setPrincipalContext(((SessionSheetMarkBy)another).getPrincipalContext());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionSheetMarkByGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionSheetMarkBy.class;
        }

        public T newInstance()
        {
            return (T) new SessionSheetMarkBy();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sessionDocument":
                    return obj.getSessionDocument();
                case "principalContext":
                    return obj.getPrincipalContext();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sessionDocument":
                    obj.setSessionDocument((SessionDocument) value);
                    return;
                case "principalContext":
                    obj.setPrincipalContext((IPrincipalContext) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sessionDocument":
                        return true;
                case "principalContext":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sessionDocument":
                    return true;
                case "principalContext":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sessionDocument":
                    return SessionDocument.class;
                case "principalContext":
                    return IPrincipalContext.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionSheetMarkBy> _dslPath = new Path<SessionSheetMarkBy>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionSheetMarkBy");
    }
            

    /**
     * @return экзам. лист. Свойство должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.SessionSheetMarkBy#getSessionDocument()
     */
    public static SessionDocument.Path<SessionDocument> sessionDocument()
    {
        return _dslPath.sessionDocument();
    }

    /**
     * @return пользователь, фактически проставивший оценку. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionSheetMarkBy#getPrincipalContext()
     */
    public static IPrincipalContextGen.Path<IPrincipalContext> principalContext()
    {
        return _dslPath.principalContext();
    }

    public static class Path<E extends SessionSheetMarkBy> extends EntityPath<E>
    {
        private SessionDocument.Path<SessionDocument> _sessionDocument;
        private IPrincipalContextGen.Path<IPrincipalContext> _principalContext;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return экзам. лист. Свойство должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.SessionSheetMarkBy#getSessionDocument()
     */
        public SessionDocument.Path<SessionDocument> sessionDocument()
        {
            if(_sessionDocument == null )
                _sessionDocument = new SessionDocument.Path<SessionDocument>(L_SESSION_DOCUMENT, this);
            return _sessionDocument;
        }

    /**
     * @return пользователь, фактически проставивший оценку. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionSheetMarkBy#getPrincipalContext()
     */
        public IPrincipalContextGen.Path<IPrincipalContext> principalContext()
        {
            if(_principalContext == null )
                _principalContext = new IPrincipalContextGen.Path<IPrincipalContext>(L_PRINCIPAL_CONTEXT, this);
            return _principalContext;
        }

        public Class getEntityClass()
        {
            return SessionSheetMarkBy.class;
        }

        public String getEntityName()
        {
            return "sessionSheetMarkBy";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
