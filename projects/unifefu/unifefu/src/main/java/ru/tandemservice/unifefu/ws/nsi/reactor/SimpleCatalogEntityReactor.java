/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.log4j.Level;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.FefuNsiIdWrapper;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 08.08.2013
 */
public abstract class SimpleCatalogEntityReactor<T, E extends IEntity> implements INsiEntityReactor<T>
{
    /**
     * Добавление элементов в справочник в ОБ разрешено
     *
     * @return - да/нет
     */
    public abstract boolean isAddItemsToOBAllowed();

    /**
     * Добавление элементов в справочник в НСИ разрешено
     *
     * @return - да/нет
     */
    public abstract boolean isAddItemsToNSIAllowed();

    /**
     * НСИ является мастер-системой, т.е. все правки в уже существующие элементы должны вноситься в подсистеме ОБ
     *
     * @return - да/нет
     */
    public abstract boolean isNsiMasterSystem();

    /**
     * Возвращает класс сущности ОБ
     *
     * @return - класс сущности ОБ
     */
    public abstract Class<E> getEntityClass();

    /**
     * Возвращает класс сущности НСИ
     *
     * @return - класс сущности НСИ
     */
    public abstract Class<T> getNSIEntityClass();

    /**
     * Вычисляет и возвращает GUID сущности по сощности НСИ. Дело в том, что общего предка у сущностей НСИ нет,
     * а поля GUID потенциально могут называться по-разному. Классы сущностей НСИ автогенерируемые, поэтому вносить
     * в них модификации не хотелось бы.
     *
     * @param nsiEntity - сущность НСИ
     * @return - GUID сущности НСИ, взятый из самой сущности.
     */
    public abstract String getGUID(T nsiEntity);

    /**
     * Создаёт сощность НСИ по переданной сущности ОБ с учетом уже вероятно имеющегося GUID
     *
     * @param entity    - сущность ОБ
     * @param nsiIdsMap - мап guid'ов НСИ, где ключ - идентифиукатор сущности НСИ, или ОБ
     * @return - Сущность НСИ, готовая к включению в датаграмму для отправки в НСИ
     */
    public abstract T createEntity(E entity, Map<String, FefuNsiIds> nsiIdsMap);

    /**
     * Создаёт сущность ОБ на основе сщности НСИ.
     *
     * @param nsiEntity        - Сущность НСИ
     * @param similarEntityMap - мап мозможных кандидатов на дубли, где одна и та же сущность ОБ может фигурировать несколько раз,
     *                         причем ключ может быть идентификатором сущности ОБ, либо её title, либо пользовательским кодом.
     * @return - новая сущность ОБ, готовая к сохранению.
     */
    public abstract E createEntity(T nsiEntity, Map<String, E> similarEntityMap);

    /**
     * Возвращает возможный дубликат сущности НСИ в ОБ. Сравнение рекомендуется производить по ключевым полям элементов.
     * Например, классификатор стран ОКСМ лучше синхронизировать по пользовательским кодам, поскольку они универсальны во всех подсистемах.
     *
     * @param nsiEntity        - сущность НСИ, с которой будет производится поиск похожих элементов в ОБ
     * @param nsiIdsMap        - мап guid'ов НСИ, где ключ - идентифиукатор сущности НСИ, или ОБ
     * @param similarEntityMap - мап мозможных кандидатов на дубли, где одна и та же сущность ОБ может фигурировать несколько раз,
     *                         причем ключ может быть идентификатором сущности ОБ, либо её title, либо пользовательским кодом.
     * @return - Возможный кандидат на дубль в ОБ. Может быть пустым, если похожих элементов в ОБ не найдено.
     */
    public abstract E getPossibleDuplicate(T nsiEntity, Map<String, FefuNsiIds> nsiIdsMap, Map<String, E> similarEntityMap);

    /**
     * Возвращает true, если есть различия в ключевых полях сущностей ОБ и НСИ, например, если отличаются названия.
     * Сравнение полей производится после отдроса лишних пробелов по краям и приведения обеих строк к верхнему регистру.
     *
     * @param nsiEntity        - сущность НСИ для сравнения
     * @param entity           - сущность ОБ для сравнения
     * @param similarEntityMap - мап мозможных кандидатов на дубли, где одна и та же сущность ОБ может фигурировать несколько раз,
     *                         причем ключ может быть идентификатором сущности ОБ, либо её title, либо пользовательским кодом.
     * @return - true, если есть различия в ключевых полях и false, если их нет.
     */
    public abstract boolean isAnythingChanged(T nsiEntity, E entity, Map<String, E> similarEntityMap);

    /**
     * Обновляет ключевые поля сущности ОБ, в соответствии с сущностью НСИ.
     *
     * @param nsiEntity        - сущность НСИ
     * @param entity           - сущность ОБ
     * @param similarEntityMap - мап мозможных кандидатов на дубли, где одна и та же сущность ОБ может фигурировать несколько раз,
     *                         причем ключ может быть идентификатором сущности ОБ, либо её title, либо пользовательским кодом.
     * @return - Обновленная сущность ОБ, готовая к сохранению.
     */
    public abstract E updatePossibleDuplicateFields(T nsiEntity, E entity, Map<String, E> similarEntityMap);

    /**
     * Обновляет ключевые поля сущности НСИ, в соответствии с сущностью ОБ.
     *
     * @param nsiEntity - сущность НСИ
     * @param entity    - сущность ОБ
     * @return - Обновленная сущность НСИ, готовая к отправке в НСИ.
     */
    public abstract T updateNsiEntityFields(T nsiEntity, E entity);

    protected boolean useSmallPackage()
    {
        return false;
    }

    /**
     * Возвращает GUID сущности ОБ, либо доставая его из связанного объекта идентификаторов НСИ,
     * либо генерируя на основе идентификатора сущности в ОБ.
     *
     * @param entity    - сущность ОБ
     * @param nsiIdsMap - мап guid'ов НСИ, где ключ - идентифиукатор сущности НСИ, или ОБ
     * @return - GUID
     */
    public String getGUID(E entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        if (null == entity) return null;
        FefuNsiIds nsiIds = null != nsiIdsMap ? nsiIdsMap.get(entity.getId().toString()) : null;
        if (null != nsiIds) return nsiIds.getGuid();
        return NsiReactorUtils.generateGUID(entity);
    }

    public String getEntityName()
    {
        return EntityRuntime.getMeta(getEntityClass()).getName();
    }

    /**
     * Возвращает сущность НСИ, отправка которого в НСИ повлечет возврат полного перечня элементов этого справочника из НСИ.
     *
     * @return - сущность НСИ для запроса полного перечня элементов справочника из НСИ.
     */
    public T getFullCatalogRetrieveRequestObject()
    {
        return getCatalogElementRetrieveRequestObject(null);
    }

    /**
     * Возвращает набор идентификаторов в формате НСИ для формирования запроса в НСИ на получение
     * соответствующих элементов справочника из НСИ
     *
     * @param guidList - список GUID'ов НСИ для запроса.
     * @return - Список идентификаторов в формате НСИ для получения соответствующих элементов из НСИ.
     */
    public List<T> getCatalogElementsRetrieveRequestObject(List<String> guidList)
    {
        List<T> nsiEntityList = new ArrayList<>();
        for (String guid : guidList) nsiEntityList.add(getCatalogElementRetrieveRequestObject(guid));
        return nsiEntityList;
    }

    /**
     * Возвращает признак удалённости элемента из НСИ. Если некогда элемент был удалён в НСИ, то чтобы данный элемент
     * не создавался повторно в НСИ, как новый, то ставится признак его удалённости.
     *
     * @param entity    - сущность ОБ
     * @param nsiIdsMap - мап guid'ов НСИ, где ключ - идентифиукатор сущности НСИ, или ОБ
     * @return - true, если есть FefuNsiIds, связанный с переданным объектом, и у него установлен признак deletedFromNsi
     */
    public boolean isEntityDeletedFromNSI(E entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        if (null == entity || null == nsiIdsMap) return false;
        FefuNsiIds nsiId = nsiIdsMap.get(entity.getId());
        if (null == nsiId) return false;
        return nsiId.isDeletedFromNsi();
    }

    /**
     * Возвращает true, если первоисточником сущности является подсистема ОБ
     *
     * @param entity    - сущность ОБ
     * @param nsiIdsMap -
     * @return
     */
    public boolean isEntitySourceOB(E entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        if (null == entity || null == entity.getId()) return false;
        String generatedGuid = NsiReactorUtils.generateGUID(entity);
        FefuNsiIds nsiId = nsiIdsMap.get(entity.getId().toString());
        if (null == nsiId || null == generatedGuid) return false;
        return nsiId.getGuid().equals(generatedGuid);
    }

    public boolean isEntitySourceOB(Long entityId, String guid)
    {
        if (null == entityId || null == guid) return false;
        String generatedGuid = NsiReactorUtils.generateGUID(entityId);
        return guid.equals(generatedGuid);
    }

    protected Map<String, E> getEntityMap(Class<E> entityClass)
    {
        return IFefuNsiSyncDAO.instance.get().getEntityMap(entityClass);
    }

    protected Map<String, E> getEntityMap(Class<E> entityClass, Set<Long> entityIds)
    {
        return IFefuNsiSyncDAO.instance.get().getEntityMap(entityClass, entityIds);
    }

    protected Map<String, E> getNsiSynchrinizedEntityMap(Class<E> entityClass, Set<String> entityGuids, boolean includeGuidGeneratedItems)
    {
        return IFefuNsiSyncDAO.instance.get().getNsiSynchronizedEntityMap(entityClass, entityGuids, includeGuidGeneratedItems);
    }

    protected void prepareRelatedObjectsMap()
    {
    }

    protected void refreshNsiObjectHolder(CoreCollectionUtils.Pair<E, FefuNsiIds> entityPair)
    {
    }

    @Override
    public void synchronizeFullCatalogWithNsi()
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG HAVE STARTED ==========");

        // Забираем полный список всех элементов справочника из НСИ
        List<T> nsiEntityList = NsiReactorUtils.retrieveObjectsFromNsiBySingleElement(getNSIEntityClass(), getFullCatalogRetrieveRequestObject());
        if (this instanceof INsiCatalogResorter) ((INsiCatalogResorter) this).resortNsiEntityList(nsiEntityList);

        // Формируем мап сущностей ОБ, где ключ - идентификатор сущности ОБ, либо Название (ITitled), или пользовательский код (IActiveCatalogItem)
        Map<String, E> entityMap = getEntityMap(getEntityClass());
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");
        // Формируем мап идентификаторов НСИ из числа сохраненных в ОБ, где ключ - идентификатор ОБ, либо GUID
        Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMap(EntityRuntime.getMeta(getEntityClass()).getName());
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        // Если для синхронизации нужны дополнительные данные, то инициализируем их
        prepareRelatedObjectsMap();

        List<Object> nsiEntityToUpdate = new ArrayList<>(); // Сюда сваливаем все сущности НСИ, которые требуется обновить данными из ОБ
        Set<Long> alreadySynchronizedEntitySet = new HashSet<>(); // Контрольный сет, блокирующий возможность повторного добавления сущности в НСИ, если она обновляется.

        // Проводим синхронизацию всех полученных элементов из НСИ с имеющимися в ОБ
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start local (OB) synchronization of elements for " + getEntityClass().getSimpleName() + " catalog ----------");
        for (T nsiEntity : nsiEntityList)
        {
            E entity = getPossibleDuplicate(nsiEntity, nsiIdsMap, entityMap); // Вычисляем элемент ОБ, соответствующий элементу НСИ (по GUID, затем по названию и пользовательскому коду)
            FefuNsiIds nsiId = nsiIdsMap.get(getGUID(nsiEntity)); // Достаём GUID ОБ, если он есть

            if (isNsiMasterSystem()) // Если НСИ - мастер-система, то обновляем элементы в ОБ
            {
                E modifiedEntity;
                if (null != entity) // Если найдены схожие элементы
                {
                    if (isAnythingChanged(nsiEntity, entity, entityMap)) // Сверяем наличие изменений, чтобы не проводить обновление объекта без необходимости
                    {
                        modifiedEntity = updatePossibleDuplicateFields(nsiEntity, entity, entityMap); // Вносим изменения в сущность ОБ и далее обновляем сущности ОБ
                        FefuNsiIds newNsiId = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiEntities(modifiedEntity, nsiId, new FefuNsiIdWrapper(getGUID(nsiEntity), getEntityName(), isAddItemsToOBAllowed(), isNsiMasterSystem()), null, null);
                        nsiIdsMap.put(newNsiId.getGuid(), newNsiId);
                        entityMap.put(newNsiId.getGuid(), modifiedEntity);
                    } else
                    {
                        FefuNsiIds newNsiId = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(entity, nsiId, new FefuNsiIdWrapper(getGUID(nsiEntity), getEntityName())); // Иначе просто обновляем данные о GUID и времени синхронизации элемента
                        nsiIdsMap.put(newNsiId.getGuid(), newNsiId);
                        entityMap.put(newNsiId.getGuid(), entity);
                    }
                } else if (isAddItemsToOBAllowed()) // Если схожих не найдено, и позволено добавление новых элементов в ОБ, то создаём новый элемент и сохраняем его в ОБ
                {
                    modifiedEntity = createEntity(nsiEntity, entityMap);
                    FefuNsiIds newNsiId = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiEntities(modifiedEntity, nsiId, new FefuNsiIdWrapper(getGUID(nsiEntity), getEntityName(), isAddItemsToOBAllowed(), isNsiMasterSystem()), null, null);
                    entityMap.put(modifiedEntity.getId().toString(), modifiedEntity);
                    nsiIdsMap.put(newNsiId.getGuid(), newNsiId);
                    entityMap.put(newNsiId.getGuid(), modifiedEntity);
                }
            } else // Если НСИ - не мастер система, то обновляем элементы в НСИ значениями из ОБ (подготавливаем список для дальнейшего обновления)
            {
                // Чтобы повторно не заслать в НСИ элемент, который ранее оттуда был удалён, проверяем соответствующий признак, а так же смотрим, что что-то изменилось
                if ((null == nsiId || (nsiId != null && !nsiId.isDeletedFromNsi())) && isAnythingChanged(nsiEntity, entity, entityMap))
                    nsiEntityToUpdate.add(updateNsiEntityFields(nsiEntity, entity));
            }

            // Чтобы избежать повторной обработки далее, добавляем в соответствующий сет
            if (null != entity) alreadySynchronizedEntitySet.add(entity.getId());
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Local (OB) synchronization of elements for " + getEntityClass().getSimpleName() + " catalog was finished ----------");

        // Если разрешено добавление новых элементов в НСИ
        if (isAddItemsToNSIAllowed())
        {
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog ----------");

            List<Object> nsiInsertEntityList = new ArrayList<>();
            Set<Long> alreadyPreparedForSendingToNSIEntitySet = new HashSet<>();

            // Бежим по всем сущностям ОБ и выискиваем те, которых нет в НСИ
            for (E entity : entityMap.values())
            {
                FefuNsiIds nsiId = nsiIdsMap.get(entity.getId().toString()); // Вытаскиваем GUID ОБ, если есть такой
                if (null != nsiId && nsiId.isDeletedFromNsi())
                    continue; // Проверяем, чтобы соответствующий элемент не был удалён ранее из НСИ

                // Если ранее ещё не обрабатывали данную сущность и не отправляли на правки в НСИ
                if (!alreadySynchronizedEntitySet.contains(entity.getId())
                        && !alreadyPreparedForSendingToNSIEntitySet.contains(entity.getId()))
                {
                    nsiInsertEntityList.add(createEntity(entity, null));
                    alreadyPreparedForSendingToNSIEntitySet.add(entity.getId());
                    IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(entity, nsiId, new FefuNsiIdWrapper(null, getEntityName()));
                }
            }
            // Собственно, производим отправку свежедобавляемых элементов в НСИ
            NsiReactorUtils.executeNSIAction(nsiInsertEntityList, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT, useSmallPackage());
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
        }

        // Если НСИ не является мастер-системой по данному справочнику и есть некие отличия элементов с ОБ, то производим их обновление в НСИ
        if (!isNsiMasterSystem() && nsiEntityToUpdate.size() > 0)
        {
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start update elements at NSI for " + getEntityClass().getSimpleName() + " catalog ----------");
            NsiReactorUtils.executeNSIAction(nsiEntityToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE, useSmallPackage());
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Update elements at NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
        }

        // Обновляем содержимое мапа с сущностями в холдере
        refreshNsiObjectHolder(null);

        // Изменяем время последней синхронизации для справочника в целом
        IFefuNsiSyncDAO.instance.get().updateNsiCatalogSyncTime(getNSIEntityClass().getSimpleName());
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== SYNCHRONIZATION FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG WAS FINISHED ==========");
    }

    @Override
    public void insertOrUpdateNewItemsAtNSI(Set<Long> entityToAddIdsSet)
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== INSERT/UPDATE ITEMS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG HAVE STARTED ==========");

        Map<String, E> entityMap = getEntityMap(getEntityClass(), entityToAddIdsSet);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Some elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database by their ids ----------");
        Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMap(EntityRuntime.getMeta(getEntityClass()).getName(), entityToAddIdsSet);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Some nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database by their ids ----------");

        // Если для синхронизации нужны дополнительные данные, то инициализируем их
        prepareRelatedObjectsMap();

        List<String> guids = new ArrayList<>();
        for (FefuNsiIds nsiId : nsiIdsMap.values()) guids.add(nsiId.getGuid());
        List<T> nsiEntityList = NsiReactorUtils.retrieveObjectsFromNSIByElementsList(getNSIEntityClass(), getCatalogElementsRetrieveRequestObject(guids));

        List<Object> nsiEntityToUpdate = new ArrayList<>();
        Set<Long> alreadySynchronizedEntitySet = new HashSet<>();

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Calculating elements, have to update at NSI, for " + getEntityClass().getSimpleName() + " catalog ----------");
        for (T nsiEntity : nsiEntityList)
        {
            E entity = getPossibleDuplicate(nsiEntity, nsiIdsMap, entityMap);
            FefuNsiIds nsiId = nsiIdsMap.get(getGUID(nsiEntity));

            if (!isNsiMasterSystem() || isEntitySourceOB(entity, nsiIdsMap))
            {
                if ((null == nsiId || (nsiId != null && !nsiId.isDeletedFromNsi())) && isAnythingChanged(nsiEntity, entity, entityMap))
                    nsiEntityToUpdate.add(updateNsiEntityFields(nsiEntity, entity));
            }

            if (null != entity) alreadySynchronizedEntitySet.add(entity.getId());
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Calculating elements, have to update at NSI, for " + getEntityClass().getSimpleName() + " catalog was finished ----------");

        if (isAddItemsToNSIAllowed())
        {
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog ----------");

            List<Object> nsiInsertEntityList = new ArrayList<>();
            Set<Long> alreadyPreparedForSendingToNSIEntitySet = new HashSet<>();

            for (E entity : entityMap.values())
            {
                FefuNsiIds nsiId = nsiIdsMap.get(entity.getId().toString());
                if (null != nsiId && nsiId.isDeletedFromNsi()) continue;

                if (!alreadySynchronizedEntitySet.contains(entity.getId())
                        && !alreadyPreparedForSendingToNSIEntitySet.contains(entity.getId()))
                {
                    nsiInsertEntityList.add(createEntity(entity, null));
                    alreadyPreparedForSendingToNSIEntitySet.add(entity.getId());
                    FefuNsiIds newNsiId = IFefuNsiSyncDAO.instance.get().saveOrUpdateNsiId(entity, nsiId, new FefuNsiIdWrapper(null, getEntityName()));
                    refreshNsiObjectHolder(new CoreCollectionUtils.Pair(entity, newNsiId));
                }
            }
            NsiReactorUtils.prepareNSIAction(nsiInsertEntityList, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT, useSmallPackage());
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Adding new elements to NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
        }

        if (nsiEntityToUpdate.size() > 0)
        {
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Start update elements at NSI for " + getEntityClass().getSimpleName() + " catalog ----------");
            NsiReactorUtils.prepareNSIAction(nsiEntityToUpdate, FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE, useSmallPackage());
            FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Update elements at NSI for " + getEntityClass().getSimpleName() + " catalog was finished ----------");
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== INSERT/UPDATE ITEMS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG WAS FINISHED ==========");
    }

    @Override
    public void deleteItemsFromNSI(Set<Long> entityToAddIdsSet)
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM NSI HAVE STARTED ==========");

        Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMap(EntityRuntime.getMeta(getEntityClass()).getName(), entityToAddIdsSet);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Some nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database by their ids ----------");

        List<String> guids = new ArrayList<>();
        List<FefuNsiIds> nsiIdsListToUpdateDeleted = new ArrayList<>();

        for (FefuNsiIds nsiId : nsiIdsMap.values())
        {
            if (isEntitySourceOB(nsiId.getEntityId(), nsiId.getGuid())) guids.add(nsiId.getGuid());
            else nsiIdsListToUpdateDeleted.add(nsiId);
        }
        List<T> nsiEntityList = NsiReactorUtils.retrieveObjectsFromNSIByElementsList(getNSIEntityClass(), getCatalogElementsRetrieveRequestObject(guids));

        List<Object> nsiEntityToDelete = new ArrayList<>();
        for (T nsiRealEntity : nsiEntityList)
            nsiEntityToDelete.add(getCatalogElementRetrieveRequestObject(getGUID(nsiRealEntity)));
        NsiReactorUtils.prepareNSIAction(nsiEntityToDelete, FefuNsiRequestsProcessor.OPERATION_TYPE_DELETE, useSmallPackage());

        IFefuNsiSyncDAO.instance.get().updateDeletedFromOBProperty(nsiIdsListToUpdateDeleted);
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== DELETE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM NSI WAS FINISHED ==========");
    }

    @Override
    public List<Object> retrieve(List<INsiEntity> itemsToRetrieve)
    {
        FefuNsiSyncDAO.logEvent(Level.INFO, "========== RETRIEVE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM OB HAVE STARTED ==========");
        Set<String> guidsToRetrieve = new HashSet<>();
        if (null != itemsToRetrieve)
        {
            for (Object nsiEntity : itemsToRetrieve)
            {
                String guid = getGUID(getNSIEntityClass().cast(nsiEntity)); //TODO
                if (null != guid) guidsToRetrieve.add(guid);
            }
        }

        Map<String, E> entityMap = guidsToRetrieve.isEmpty() ? getNsiSynchrinizedEntityMap(getEntityClass(), null, true) : getNsiSynchrinizedEntityMap(getEntityClass(), guidsToRetrieve, true);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        Map<String, FefuNsiIds> nsiIdsMap = new HashMap<>();
        for (Map.Entry<String, E> entry : entityMap.entrySet())
        {
            FefuNsiIds dummyId = new FefuNsiIds();
            dummyId.setGuid(entry.getKey());
            dummyId.setEntityId(entry.getValue().getId());
            nsiIdsMap.put(entry.getKey(), dummyId);
            nsiIdsMap.put(entry.getValue().getId().toString(), dummyId);
        }
        //Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMap(EntityRuntime.getMeta(getEntityClass()).getName());
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        prepareRelatedObjectsMap();

        List<Object> result = new ArrayList<>();
        Set<Long> alreadyPreparedItemIdList = new HashSet<>();
        for (E entity : entityMap.values())
        {
            if (!alreadyPreparedItemIdList.contains(entity.getId()) && !isEntityDeletedFromNSI(entity, nsiIdsMap) && (guidsToRetrieve.isEmpty() || guidsToRetrieve.contains(getGUID(entity, nsiIdsMap))))
            {
                result.add(createEntity(entity, nsiIdsMap));
                alreadyPreparedItemIdList.add(entity.getId());
            }
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== RETRIEVE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG FROM OB WAS FINISHED ==========");

        return result;
    }

    @Override
    public List<Object> insert(List<INsiEntity> itemsToInsert) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        /*for(Object item : itemsToInsert)
        {
            //TODO if(getEntityClass().isInstance(item))
        }  */

        if (!isNsiMasterSystem() && !isAddItemsToOBAllowed())
            throw new NSIProcessingErrorException("There is no any entity guid to insert in datagram.");

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== INSERT/UPDATE ITEMS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB HAVE STARTED ==========");

        Map<String, E> entityMap = getEntityMap(getEntityClass()); // TODO: caching
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Some elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database by their ids ----------");
        //Map<String, FefuNsiIds> nsiIdsMap = IFefuNsiSyncDAO.instance.get().getFefuNsiIdsMap(EntityRuntime.getMeta(getEntityClass()).getName(), entityToAddIdsSet);
        //FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Some nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database by their ids ----------");

        List<Object> result = new ArrayList<>();
        for (Object item : itemsToInsert)
        {
            //getPossibleDuplicate()
        }

        return new ArrayList<>();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<Object> update(List<INsiEntity> itemsToUpdate) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        /*FefuNsiSyncDAO.logEvent(Level.INFO, "========== UPDATE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB HAVE STARTED ==========");

        Set<String> guidsToUpdate = new HashSet<>();
        if (null != itemsToUpdate)
        {
            for (T nsiEntity : itemsToUpdate)
            {
                String guid = getGUID(nsiEntity);
                if (null != guid) guidsToUpdate.add(guid);
            }
        }

        Map<String, E> entityMap = guidsToUpdate.isEmpty() ? getNsiSynchrinizedEntityMap(getEntityClass(), null, false) : getNsiSynchrinizedEntityMap(getEntityClass(), guidsToUpdate, false);
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole elements list (" + entityMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        Map<String, FefuNsiIds> nsiIdsMap = new HashMap<>();
        for (Map.Entry<String, E> entry : entityMap.entrySet())
        {
            FefuNsiIds dummyId = new FefuNsiIds();
            dummyId.setGuid(entry.getKey());
            dummyId.setEntityId(entry.getValue().getId());
            nsiIdsMap.put(entry.getKey(), dummyId);
            nsiIdsMap.put(entry.getValue().getId().toString(), dummyId);
        }
        FefuNsiSyncDAO.logEvent(Level.INFO, "---------- Whole nsiIds list (" + nsiIdsMap.values().size() + " items) for " + getEntityClass().getSimpleName() + " catalog was received from database ----------");

        List<Object> result = new ArrayList<>();
        for (T item : itemsToUpdate)
        {
            if(null == getGUID(item))
            {
                E entity = getPossibleDuplicate(item, nsiIdsMap, entityMap);
                if(null != entity)
                {
                    updatePossibleDuplicateFields()
                }
            }
            else
            {

            }
            //result.add(item);
        }

        FefuNsiSyncDAO.logEvent(Level.INFO, "========== UPDATE ELEMENTS FOR " + getEntityClass().getSimpleName().toUpperCase() + " CATALOG AT OB WAS FINISHED ==========");
        return result;*/
        return new ArrayList<>();
    }

    @Override
    public List<Object> delete(List<INsiEntity> itemsToDelete) throws NSIProcessingErrorException, NSIProcessingWarnException
    {
        return new ArrayList<>();
    }
}