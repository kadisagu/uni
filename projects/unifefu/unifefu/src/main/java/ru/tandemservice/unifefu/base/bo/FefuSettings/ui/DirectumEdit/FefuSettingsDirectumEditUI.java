/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.DirectumEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.unifefu.base.bo.FefuSettings.FefuSettingsManager;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuDirectumOrderTypeCodes;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 11.07.2013
 */
@Input({
        @Bind(key = FefuSettingsDirectumEditUI.EXTRACT_TYPE_ID, binding = "extractTypeId"),
        @Bind(key = FefuSettingsDirectumEditUI.ENR_EXTRACT_TYPE_ID, binding = "enrExtractTypeId"),
        @Bind(key = FefuSettingsDirectumEditUI.FEFU_DIRECTUM_ORDER_TYPE_ID, binding = "fefuDirectumOrderTypeId")
})
public class FefuSettingsDirectumEditUI extends UIPresenter
{
    public static final String EXTRACT_TYPE_ID = "extractTypeId";
    public static final String ENR_EXTRACT_TYPE_ID = "enrExtractTypeId";
    public static final String FEFU_DIRECTUM_ORDER_TYPE_ID = "fefuDirectumOrderTypeId";

    private DataWrapper _signer;
    private Long _extractTypeId;
    private Long _enrExtractTypeId;
    private Long _fefuDirectumOrderTypeId;
    private StudentExtractType _extractType;
    private EntrantEnrollmentOrderType _enrExtractType;
    private FefuDirectumOrderType _fefuDirectumOrderType;
    private FefuDirectumSettings _directumSettings;
    private FefuDirectumEnrSettings _directumEnrSettings;

    @Override
    public void onComponentRefresh()
    {
        _fefuDirectumOrderType = DataAccessServices.dao().getNotNull(FefuDirectumOrderType.class, _fefuDirectumOrderTypeId);

        if (null != _extractTypeId)
        {
            _extractType = DataAccessServices.dao().getNotNull(StudentExtractType.class, _extractTypeId);
            _directumSettings = DataAccessServices.dao().getByNaturalId(new FefuDirectumSettings.NaturalId(_extractType, _fefuDirectumOrderType));
        }

        if (null != _enrExtractTypeId)
        {
            _enrExtractType = DataAccessServices.dao().getNotNull(EntrantEnrollmentOrderType.class, _enrExtractTypeId);
            _directumEnrSettings = DataAccessServices.dao().getByNaturalId(new FefuDirectumEnrSettings.NaturalId(_enrExtractType, _fefuDirectumOrderType));
        }

        if (null == _directumSettings)
            _directumSettings = new FefuDirectumSettings(_extractType, _fefuDirectumOrderType);

        if (null == _directumEnrSettings)
            _directumEnrSettings = new FefuDirectumEnrSettings(_enrExtractType, _fefuDirectumOrderType);

        if (null != _directumSettings.getSigner())
        {
            String[] preItemList = StringUtils.split(_uiConfig.getProperty("ui.signerDS.itemslist"), ";");
            String[] codesList = StringUtils.split(_uiConfig.getProperty("ui.signerDS.itemcodes"), ";");
            for (int i = 0; i < preItemList.length; i++)
            {
                if (preItemList[i].equalsIgnoreCase(_directumSettings.getSigner()))
                    _signer = new DataWrapper(Long.parseLong(codesList[i]), preItemList[i], preItemList[i]);
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuSettingsDirectumEdit.SIGNER_DS.equals(dataSource.getName()))
        {
            String[] preItemList = StringUtils.split(_uiConfig.getProperty("ui.signerDS.itemslist"), ";");
            String[] codesList = StringUtils.split(_uiConfig.getProperty("ui.signerDS.itemcodes"), ";");
            List<DataWrapper> recordList = new ArrayList<>(preItemList.length);
            for (int i = 0; i < preItemList.length; i++)
                recordList.add(new DataWrapper(Long.parseLong(codesList[i]), preItemList[i], preItemList[i]));
            dataSource.put(UIDefines.COMBO_OBJECT_LIST, recordList);
        }
    }

    public void onClickApply()
    {
        if (null != _signer) _directumSettings.setSigner(_signer.getTitle());
        if (null != _extractTypeId)
            FefuSettingsManager.instance().dao().updateDirectumSettings(_directumSettings);
        if (null != _enrExtractTypeId)
            FefuSettingsManager.instance().dao().updateDirectumSettings(_directumEnrSettings);
        deactivate();
    }

    public boolean isVpo()
    {
        return null != _fefuDirectumOrderType && FefuDirectumOrderTypeCodes.VPO.equals(_fefuDirectumOrderType.getCode());
    }

    public boolean isSpo()
    {
        return null != _fefuDirectumOrderType && FefuDirectumOrderTypeCodes.SPO.equals(_fefuDirectumOrderType.getCode());
    }

    public boolean isDpo()
    {
        return null != _fefuDirectumOrderType && FefuDirectumOrderTypeCodes.DPO.equals(_fefuDirectumOrderType.getCode());
    }

    public boolean isEnr()
    {
        return null != _fefuDirectumOrderType && FefuDirectumOrderTypeCodes.ENR.equals(_fefuDirectumOrderType.getCode());
    }

    public String getExtractTypeTitle()
    {
        return null != _extractType ? _extractType.getTitle() : (null != _enrExtractType ? _enrExtractType.getTitle() : null);
    }

    public DataWrapper getSigner()
    {
        return _signer;
    }

    public void setSigner(DataWrapper signer)
    {
        _signer = signer;
    }

    public Long getExtractTypeId()
    {
        return _extractTypeId;
    }

    public void setExtractTypeId(Long extractTypeId)
    {
        _extractTypeId = extractTypeId;
    }

    public Long getEnrExtractTypeId()
    {
        return _enrExtractTypeId;
    }

    public void setEnrExtractTypeId(Long enrExtractTypeId)
    {
        _enrExtractTypeId = enrExtractTypeId;
    }

    public StudentExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(StudentExtractType extractType)
    {
        _extractType = extractType;
    }

    public EntrantEnrollmentOrderType getEnrExtractType()
    {
        return _enrExtractType;
    }

    public void setEnrExtractType(EntrantEnrollmentOrderType enrExtractType)
    {
        _enrExtractType = enrExtractType;
    }

    public FefuDirectumSettings getDirectumSettings()
    {
        return _directumSettings;
    }

    public void setDirectumSettings(FefuDirectumSettings directumSettings)
    {
        _directumSettings = directumSettings;
    }

    public FefuDirectumEnrSettings getDirectumEnrSettings()
    {
        return _directumEnrSettings;
    }

    public void setDirectumEnrSettings(FefuDirectumEnrSettings directumEnrSettings)
    {
        _directumEnrSettings = directumEnrSettings;
    }

    public Long getFefuDirectumOrderTypeId()
    {
        return _fefuDirectumOrderTypeId;
    }

    public void setFefuDirectumOrderTypeId(Long fefuDirectumOrderTypeId)
    {
        _fefuDirectumOrderTypeId = fefuDirectumOrderTypeId;
    }

    public FefuDirectumOrderType getFefuDirectumOrderType()
    {
        return _fefuDirectumOrderType;
    }

    public void setFefuDirectumOrderType(FefuDirectumOrderType fefuDirectumOrderType)
    {
        _fefuDirectumOrderType = fefuDirectumOrderType;
    }
}