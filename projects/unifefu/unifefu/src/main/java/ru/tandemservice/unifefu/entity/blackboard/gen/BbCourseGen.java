package ru.tandemservice.unifefu.entity.blackboard.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Электронный учебный курс (ЭУК) в системе Blackboard
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class BbCourseGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.blackboard.BbCourse";
    public static final String ENTITY_NAME = "bbCourse";
    public static final int VERSION_HASH = 1949832557;
    private static IEntityMeta ENTITY_META;

    public static final String P_BB_COURSE_ID = "bbCourseId";
    public static final String P_BB_PRIMARY_ID = "bbPrimaryId";
    public static final String P_TITLE = "title";
    public static final String P_ARCHIVE = "archive";
    public static final String P_INTERNAL = "internal";

    private String _bbCourseId;     // Идентификатор в системе Blackboard (не primary)
    private String _bbPrimaryId;     // Идентификатор Blackboard (primary)
    private String _title; 
    private boolean _archive = false;     // Архинвый
    private boolean _internal = true;     // Курс создан из Юни

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор в системе Blackboard (не primary). Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=48)
    public String getBbCourseId()
    {
        return _bbCourseId;
    }

    /**
     * @param bbCourseId Идентификатор в системе Blackboard (не primary). Свойство не может быть null и должно быть уникальным.
     */
    public void setBbCourseId(String bbCourseId)
    {
        dirty(_bbCourseId, bbCourseId);
        _bbCourseId = bbCourseId;
    }

    /**
     * @return Идентификатор Blackboard (primary). Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=16)
    public String getBbPrimaryId()
    {
        return _bbPrimaryId;
    }

    /**
     * @param bbPrimaryId Идентификатор Blackboard (primary). Свойство не может быть null и должно быть уникальным.
     */
    public void setBbPrimaryId(String bbPrimaryId)
    {
        dirty(_bbPrimaryId, bbPrimaryId);
        _bbPrimaryId = bbPrimaryId;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title  Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Архинвый. Свойство не может быть null.
     */
    @NotNull
    public boolean isArchive()
    {
        return _archive;
    }

    /**
     * @param archive Архинвый. Свойство не может быть null.
     */
    public void setArchive(boolean archive)
    {
        dirty(_archive, archive);
        _archive = archive;
    }

    /**
     * @return Курс создан из Юни. Свойство не может быть null.
     */
    @NotNull
    public boolean isInternal()
    {
        return _internal;
    }

    /**
     * @param internal Курс создан из Юни. Свойство не может быть null.
     */
    public void setInternal(boolean internal)
    {
        dirty(_internal, internal);
        _internal = internal;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof BbCourseGen)
        {
            setBbCourseId(((BbCourse)another).getBbCourseId());
            setBbPrimaryId(((BbCourse)another).getBbPrimaryId());
            setTitle(((BbCourse)another).getTitle());
            setArchive(((BbCourse)another).isArchive());
            setInternal(((BbCourse)another).isInternal());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends BbCourseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) BbCourse.class;
        }

        public T newInstance()
        {
            return (T) new BbCourse();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "bbCourseId":
                    return obj.getBbCourseId();
                case "bbPrimaryId":
                    return obj.getBbPrimaryId();
                case "title":
                    return obj.getTitle();
                case "archive":
                    return obj.isArchive();
                case "internal":
                    return obj.isInternal();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "bbCourseId":
                    obj.setBbCourseId((String) value);
                    return;
                case "bbPrimaryId":
                    obj.setBbPrimaryId((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "archive":
                    obj.setArchive((Boolean) value);
                    return;
                case "internal":
                    obj.setInternal((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "bbCourseId":
                        return true;
                case "bbPrimaryId":
                        return true;
                case "title":
                        return true;
                case "archive":
                        return true;
                case "internal":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "bbCourseId":
                    return true;
                case "bbPrimaryId":
                    return true;
                case "title":
                    return true;
                case "archive":
                    return true;
                case "internal":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "bbCourseId":
                    return String.class;
                case "bbPrimaryId":
                    return String.class;
                case "title":
                    return String.class;
                case "archive":
                    return Boolean.class;
                case "internal":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<BbCourse> _dslPath = new Path<BbCourse>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "BbCourse");
    }
            

    /**
     * @return Идентификатор в системе Blackboard (не primary). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse#getBbCourseId()
     */
    public static PropertyPath<String> bbCourseId()
    {
        return _dslPath.bbCourseId();
    }

    /**
     * @return Идентификатор Blackboard (primary). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse#getBbPrimaryId()
     */
    public static PropertyPath<String> bbPrimaryId()
    {
        return _dslPath.bbPrimaryId();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Архинвый. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse#isArchive()
     */
    public static PropertyPath<Boolean> archive()
    {
        return _dslPath.archive();
    }

    /**
     * @return Курс создан из Юни. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse#isInternal()
     */
    public static PropertyPath<Boolean> internal()
    {
        return _dslPath.internal();
    }

    public static class Path<E extends BbCourse> extends EntityPath<E>
    {
        private PropertyPath<String> _bbCourseId;
        private PropertyPath<String> _bbPrimaryId;
        private PropertyPath<String> _title;
        private PropertyPath<Boolean> _archive;
        private PropertyPath<Boolean> _internal;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор в системе Blackboard (не primary). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse#getBbCourseId()
     */
        public PropertyPath<String> bbCourseId()
        {
            if(_bbCourseId == null )
                _bbCourseId = new PropertyPath<String>(BbCourseGen.P_BB_COURSE_ID, this);
            return _bbCourseId;
        }

    /**
     * @return Идентификатор Blackboard (primary). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse#getBbPrimaryId()
     */
        public PropertyPath<String> bbPrimaryId()
        {
            if(_bbPrimaryId == null )
                _bbPrimaryId = new PropertyPath<String>(BbCourseGen.P_BB_PRIMARY_ID, this);
            return _bbPrimaryId;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(BbCourseGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Архинвый. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse#isArchive()
     */
        public PropertyPath<Boolean> archive()
        {
            if(_archive == null )
                _archive = new PropertyPath<Boolean>(BbCourseGen.P_ARCHIVE, this);
            return _archive;
        }

    /**
     * @return Курс создан из Юни. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse#isInternal()
     */
        public PropertyPath<Boolean> internal()
        {
            if(_internal == null )
                _internal = new PropertyPath<Boolean>(BbCourseGen.P_INTERNAL, this);
            return _internal;
        }

        public Class getEntityClass()
        {
            return BbCourse.class;
        }

        public String getEntityName()
        {
            return "bbCourse";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
