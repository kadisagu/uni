package ru.tandemservice.unifefu.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifefu.entity.report.gen.*;

/**
 * Отчет «Опоздание в простановке оценок»
 */
public class FefuBrsDelayGradingReport extends FefuBrsDelayGradingReportGen
{
    @Override
    @EntityDSLSupport(parts = FefuBrsAttestationResultsReport.P_FORMING_DATE)
    public String getFormingDateStr()
    {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(getFormingDate());
    }
}