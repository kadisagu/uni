
package ru.tandemservice.unifefu.ws.blackboard.membership.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tandemservice.unifefu.ws.blackboard.membership.gen package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CourseMembershipVOCourseId_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "courseId");
    private final static QName _CourseMembershipVOAvailable_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "available");
    private final static QName _CourseMembershipVOImageFile_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "imageFile");
    private final static QName _CourseMembershipVORoleId_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "roleId");
    private final static QName _CourseMembershipVODataSourceId_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "dataSourceId");
    private final static QName _CourseMembershipVOHasCartridgeAccess_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "hasCartridgeAccess");
    private final static QName _CourseMembershipVOId_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "id");
    private final static QName _CourseMembershipVOUserId_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "userId");
    private final static QName _SaveCourseMembershipCourseId_QNAME = new QName("http://coursemembership.ws.blackboard", "courseId");
    private final static QName _GetGroupMembershipF_QNAME = new QName("http://coursemembership.ws.blackboard", "f");
    private final static QName _GetServerVersionResponseReturn_QNAME = new QName("http://coursemembership.ws.blackboard", "return");
    private final static QName _GroupMembershipVOGroupMembershipId_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "groupMembershipId");
    private final static QName _GroupMembershipVOCourseMembershipId_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "courseMembershipId");
    private final static QName _GroupMembershipVOGroupId_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "groupId");
    private final static QName _CourseMembershipRoleVOCourseRoleDescription_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "courseRoleDescription");
    private final static QName _CourseMembershipRoleVOOrgRoleDescription_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "orgRoleDescription");
    private final static QName _CourseMembershipRoleVODefaultRole_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "defaultRole");
    private final static QName _CourseMembershipRoleVORoleIdentifier_QNAME = new QName("http://coursemembership.ws.blackboard/xsd", "roleIdentifier");
    private final static QName _GetServerVersionUnused_QNAME = new QName("http://coursemembership.ws.blackboard", "unused");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tandemservice.unifefu.ws.blackboard.membership.gen
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SaveCourseMembership }
     * 
     */
    public SaveCourseMembership createSaveCourseMembership() {
        return new SaveCourseMembership();
    }

    /**
     * Create an instance of {@link GetServerVersionResponse }
     * 
     */
    public GetServerVersionResponse createGetServerVersionResponse() {
        return new GetServerVersionResponse();
    }

    /**
     * Create an instance of {@link DeleteGroupMembershipResponse }
     * 
     */
    public DeleteGroupMembershipResponse createDeleteGroupMembershipResponse() {
        return new DeleteGroupMembershipResponse();
    }

    /**
     * Create an instance of {@link SaveGroupMembership }
     * 
     */
    public SaveGroupMembership createSaveGroupMembership() {
        return new SaveGroupMembership();
    }

    /**
     * Create an instance of {@link InitializeCourseMembershipWSResponse }
     * 
     */
    public InitializeCourseMembershipWSResponse createInitializeCourseMembershipWSResponse() {
        return new InitializeCourseMembershipWSResponse();
    }

    /**
     * Create an instance of {@link GetCourseMembership }
     * 
     */
    public GetCourseMembership createGetCourseMembership() {
        return new GetCourseMembership();
    }

    /**
     * Create an instance of {@link DeleteGroupMembership }
     * 
     */
    public DeleteGroupMembership createDeleteGroupMembership() {
        return new DeleteGroupMembership();
    }

    /**
     * Create an instance of {@link VersionVO }
     * 
     */
    public VersionVO createVersionVO() {
        return new VersionVO();
    }

    /**
     * Create an instance of {@link DeleteCourseMembership }
     * 
     */
    public DeleteCourseMembership createDeleteCourseMembership() {
        return new DeleteCourseMembership();
    }

    /**
     * Create an instance of {@link GetCourseRolesResponse }
     * 
     */
    public GetCourseRolesResponse createGetCourseRolesResponse() {
        return new GetCourseRolesResponse();
    }

    /**
     * Create an instance of {@link CourseMembershipVO }
     * 
     */
    public CourseMembershipVO createCourseMembershipVO() {
        return new CourseMembershipVO();
    }

    /**
     * Create an instance of {@link GetGroupMembership }
     * 
     */
    public GetGroupMembership createGetGroupMembership() {
        return new GetGroupMembership();
    }

    /**
     * Create an instance of {@link GetCourseMembershipResponse }
     * 
     */
    public GetCourseMembershipResponse createGetCourseMembershipResponse() {
        return new GetCourseMembershipResponse();
    }

    /**
     * Create an instance of {@link InitializeCourseMembershipWS }
     * 
     */
    public InitializeCourseMembershipWS createInitializeCourseMembershipWS() {
        return new InitializeCourseMembershipWS();
    }

    /**
     * Create an instance of {@link SaveGroupMembershipResponse }
     * 
     */
    public SaveGroupMembershipResponse createSaveGroupMembershipResponse() {
        return new SaveGroupMembershipResponse();
    }

    /**
     * Create an instance of {@link CourseRoleFilter }
     * 
     */
    public CourseRoleFilter createCourseRoleFilter() {
        return new CourseRoleFilter();
    }

    /**
     * Create an instance of {@link GroupMembershipVO }
     * 
     */
    public GroupMembershipVO createGroupMembershipVO() {
        return new GroupMembershipVO();
    }

    /**
     * Create an instance of {@link GetServerVersion }
     * 
     */
    public GetServerVersion createGetServerVersion() {
        return new GetServerVersion();
    }

    /**
     * Create an instance of {@link CourseMembershipRoleVO }
     * 
     */
    public CourseMembershipRoleVO createCourseMembershipRoleVO() {
        return new CourseMembershipRoleVO();
    }

    /**
     * Create an instance of {@link DeleteCourseMembershipResponse }
     * 
     */
    public DeleteCourseMembershipResponse createDeleteCourseMembershipResponse() {
        return new DeleteCourseMembershipResponse();
    }

    /**
     * Create an instance of {@link GetCourseRoles }
     * 
     */
    public GetCourseRoles createGetCourseRoles() {
        return new GetCourseRoles();
    }

    /**
     * Create an instance of {@link SaveCourseMembershipResponse }
     * 
     */
    public SaveCourseMembershipResponse createSaveCourseMembershipResponse() {
        return new SaveCourseMembershipResponse();
    }

    /**
     * Create an instance of {@link GetGroupMembershipResponse }
     * 
     */
    public GetGroupMembershipResponse createGetGroupMembershipResponse() {
        return new GetGroupMembershipResponse();
    }

    /**
     * Create an instance of {@link MembershipFilter }
     * 
     */
    public MembershipFilter createMembershipFilter() {
        return new MembershipFilter();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "courseId", scope = CourseMembershipVO.class)
    public JAXBElement<String> createCourseMembershipVOCourseId(String value) {
        return new JAXBElement<>(_CourseMembershipVOCourseId_QNAME, String.class, CourseMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "available", scope = CourseMembershipVO.class)
    public JAXBElement<Boolean> createCourseMembershipVOAvailable(Boolean value) {
        return new JAXBElement<>(_CourseMembershipVOAvailable_QNAME, Boolean.class, CourseMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "imageFile", scope = CourseMembershipVO.class)
    public JAXBElement<String> createCourseMembershipVOImageFile(String value) {
        return new JAXBElement<>(_CourseMembershipVOImageFile_QNAME, String.class, CourseMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "roleId", scope = CourseMembershipVO.class)
    public JAXBElement<String> createCourseMembershipVORoleId(String value) {
        return new JAXBElement<>(_CourseMembershipVORoleId_QNAME, String.class, CourseMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "dataSourceId", scope = CourseMembershipVO.class)
    public JAXBElement<String> createCourseMembershipVODataSourceId(String value) {
        return new JAXBElement<>(_CourseMembershipVODataSourceId_QNAME, String.class, CourseMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "hasCartridgeAccess", scope = CourseMembershipVO.class)
    public JAXBElement<Boolean> createCourseMembershipVOHasCartridgeAccess(Boolean value) {
        return new JAXBElement<>(_CourseMembershipVOHasCartridgeAccess_QNAME, Boolean.class, CourseMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "id", scope = CourseMembershipVO.class)
    public JAXBElement<String> createCourseMembershipVOId(String value) {
        return new JAXBElement<>(_CourseMembershipVOId_QNAME, String.class, CourseMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "userId", scope = CourseMembershipVO.class)
    public JAXBElement<String> createCourseMembershipVOUserId(String value) {
        return new JAXBElement<>(_CourseMembershipVOUserId_QNAME, String.class, CourseMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard", name = "courseId", scope = SaveCourseMembership.class)
    public JAXBElement<String> createSaveCourseMembershipCourseId(String value) {
        return new JAXBElement<>(_SaveCourseMembershipCourseId_QNAME, String.class, SaveCourseMembership.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard", name = "courseId", scope = GetGroupMembership.class)
    public JAXBElement<String> createGetGroupMembershipCourseId(String value) {
        return new JAXBElement<>(_SaveCourseMembershipCourseId_QNAME, String.class, GetGroupMembership.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MembershipFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard", name = "f", scope = GetGroupMembership.class)
    public JAXBElement<MembershipFilter> createGetGroupMembershipF(MembershipFilter value) {
        return new JAXBElement<>(_GetGroupMembershipF_QNAME, MembershipFilter.class, GetGroupMembership.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard", name = "return", scope = GetServerVersionResponse.class)
    public JAXBElement<VersionVO> createGetServerVersionResponseReturn(VersionVO value) {
        return new JAXBElement<>(_GetServerVersionResponseReturn_QNAME, VersionVO.class, GetServerVersionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "groupMembershipId", scope = GroupMembershipVO.class)
    public JAXBElement<String> createGroupMembershipVOGroupMembershipId(String value) {
        return new JAXBElement<>(_GroupMembershipVOGroupMembershipId_QNAME, String.class, GroupMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "courseMembershipId", scope = GroupMembershipVO.class)
    public JAXBElement<String> createGroupMembershipVOCourseMembershipId(String value) {
        return new JAXBElement<>(_GroupMembershipVOCourseMembershipId_QNAME, String.class, GroupMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "groupId", scope = GroupMembershipVO.class)
    public JAXBElement<String> createGroupMembershipVOGroupId(String value) {
        return new JAXBElement<>(_GroupMembershipVOGroupId_QNAME, String.class, GroupMembershipVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "courseRoleDescription", scope = CourseMembershipRoleVO.class)
    public JAXBElement<String> createCourseMembershipRoleVOCourseRoleDescription(String value) {
        return new JAXBElement<>(_CourseMembershipRoleVOCourseRoleDescription_QNAME, String.class, CourseMembershipRoleVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "orgRoleDescription", scope = CourseMembershipRoleVO.class)
    public JAXBElement<String> createCourseMembershipRoleVOOrgRoleDescription(String value) {
        return new JAXBElement<>(_CourseMembershipRoleVOOrgRoleDescription_QNAME, String.class, CourseMembershipRoleVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "defaultRole", scope = CourseMembershipRoleVO.class)
    public JAXBElement<Boolean> createCourseMembershipRoleVODefaultRole(Boolean value) {
        return new JAXBElement<>(_CourseMembershipRoleVODefaultRole_QNAME, Boolean.class, CourseMembershipRoleVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard/xsd", name = "roleIdentifier", scope = CourseMembershipRoleVO.class)
    public JAXBElement<String> createCourseMembershipRoleVORoleIdentifier(String value) {
        return new JAXBElement<>(_CourseMembershipRoleVORoleIdentifier_QNAME, String.class, CourseMembershipRoleVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard", name = "unused", scope = GetServerVersion.class)
    public JAXBElement<VersionVO> createGetServerVersionUnused(VersionVO value) {
        return new JAXBElement<>(_GetServerVersionUnused_QNAME, VersionVO.class, GetServerVersion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard", name = "courseId", scope = SaveGroupMembership.class)
    public JAXBElement<String> createSaveGroupMembershipCourseId(String value) {
        return new JAXBElement<>(_SaveCourseMembershipCourseId_QNAME, String.class, SaveGroupMembership.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CourseRoleFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard", name = "f", scope = GetCourseRoles.class)
    public JAXBElement<CourseRoleFilter> createGetCourseRolesF(CourseRoleFilter value) {
        return new JAXBElement<>(_GetGroupMembershipF_QNAME, CourseRoleFilter.class, GetCourseRoles.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard", name = "courseId", scope = DeleteGroupMembership.class)
    public JAXBElement<String> createDeleteGroupMembershipCourseId(String value) {
        return new JAXBElement<>(_SaveCourseMembershipCourseId_QNAME, String.class, DeleteGroupMembership.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard", name = "courseId", scope = GetCourseMembership.class)
    public JAXBElement<String> createGetCourseMembershipCourseId(String value) {
        return new JAXBElement<>(_SaveCourseMembershipCourseId_QNAME, String.class, GetCourseMembership.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MembershipFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard", name = "f", scope = GetCourseMembership.class)
    public JAXBElement<MembershipFilter> createGetCourseMembershipF(MembershipFilter value) {
        return new JAXBElement<>(_GetGroupMembershipF_QNAME, MembershipFilter.class, GetCourseMembership.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://coursemembership.ws.blackboard", name = "courseId", scope = DeleteCourseMembership.class)
    public JAXBElement<String> createDeleteCourseMembershipCourseId(String value) {
        return new JAXBElement<>(_SaveCourseMembershipCourseId_QNAME, String.class, DeleteCourseMembership.class, value);
    }

}
