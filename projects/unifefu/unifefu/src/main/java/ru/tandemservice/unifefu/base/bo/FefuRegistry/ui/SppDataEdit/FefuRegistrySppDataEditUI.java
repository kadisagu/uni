/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.SppDataEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.FefuRegistryManager;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.unispp.base.entity.SppRegElementExt;
import ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck;
import ru.tandemservice.unispp.base.entity.gen.SppRegElementExtGen;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 21.11.2014
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "elementId", required = true)})
public class FefuRegistrySppDataEditUI extends UIPresenter
{
    private Long _elementId;
    private EppRegistryElement _element;

    private ISelectModel _classroomTypes;
    private SppRegElementExt _sppRegElementExt;
    private Map<String, EppLoadType> _loadTypeMap;
    private Map<String, SppRegElementLoadCheck> _loadMap;
    private Map<String, Boolean> _defaultLoadCheckMap = Collections.emptyMap();
    private Map<String, UniplacesClassroomType> _defaultLoadPlaceMap = Collections.emptyMap();
    private EppLoadType _currentLoadType;

    @Override
    public void onComponentRefresh()
    {
        _element = DataAccessServices.dao().getNotNull(_elementId);

        setLoadTypeMap(EppLoadTypeUtils.getLoadTypeMap());
        List<SppRegElementExt> extList = DataAccessServices.dao().getList(SppRegElementExt.class, SppRegElementExtGen.registryElement().id(), _elementId);
        Map<Long, Map<String, SppRegElementLoadCheck>> disciplineLoadMap = FefuRegistryManager.instance().dao().getRegistryElementTotalLoadMap(Collections.singleton(_elementId));

        if (extList.size() == 1) setSppRegElementExt(extList.get(0));
        else
        {
            SppRegElementExt sppRegElementExt = new SppRegElementExt();
            sppRegElementExt.setRegistryElement(_element);
            setSppRegElementExt(sppRegElementExt);
        }
        setLoadMap(disciplineLoadMap.get(_elementId));
    }

    public void onClickApply()
    {
        List<EppALoadType> aLoadTypes = DataAccessServices.dao().getList(EppALoadType.class);
        for (EppALoadType loadType : aLoadTypes)
        {
            final SppRegElementLoadCheck load = getLoadMap().get(loadType.getFullCode());
            if (null != load)
            {
                load.setEppALoadType(loadType);
                load.setRegistryElement(getElement());

            }
        }

        Map<Long, Map<String, SppRegElementLoadCheck>> disciplineLoadMap = FefuRegistryManager.instance().dao().getRegistryElementTotalLoadMap(Collections.singleton(_elementId));
        disciplineLoadMap.put(_elementId, getLoadMap());
        FefuRegistryManager.instance().dao().saveRegistryElementTotalLoadMap(disciplineLoadMap);
        FefuRegistryManager.instance().dao().saveRegistryElExt(getSppRegElementExt());

        deactivate();
    }

    public SppRegElementLoadCheck getCurrentDisciplineLoad()
    {
        return getDisciplineLoad(getCurrentLoadType());
    }

    protected SppRegElementLoadCheck getDisciplineLoad(EppLoadType loadType)
    {
        return SafeMap.safeGet(getLoadMap(), loadType.getFullCode(), key -> {
            final SppRegElementLoadCheck load = new SppRegElementLoadCheck();
            load.setCheckValue(getDefaultLoadCheckMap().get(key) == null ? false : getDefaultLoadCheckMap().get(key));
            load.setUniplacesClassroomType(getDefaultLoadPlaceMap().get(key));
            return load;
        });
    }

    public void setDefaultLoadPlaceMap(final Map<String, UniplacesClassroomType> defaultLoadPlaceCheckMap)
    {
        _defaultLoadPlaceMap = defaultLoadPlaceCheckMap;
    }

    public Map<String, UniplacesClassroomType> getDefaultLoadPlaceMap()
    {
        final Map<String, UniplacesClassroomType> map = _defaultLoadPlaceMap;
        return (null != map ? map : Collections.<String, UniplacesClassroomType>emptyMap());
    }

    public void setDefaultLoadCheckMap(final Map<String, Boolean> defaultLoadCheckMap)
    {
        _defaultLoadCheckMap = defaultLoadCheckMap;
    }

    public Map<String, Boolean> getDefaultLoadCheckMap()
    {
        final Map<String, Boolean> map = _defaultLoadCheckMap;
        return (null != map ? map : Collections.<String, Boolean>emptyMap());
    }

    public Map<String, SppRegElementLoadCheck> getLoadMap()
    {
        return _loadMap;
    }

    public void setLoadMap(final Map<String, SppRegElementLoadCheck> loadMap)
    {
        _loadMap = loadMap;
    }

    public ISelectModel getClassroomTypes()
    {
        return _classroomTypes;
    }

    public void setClassroomTypes(ISelectModel classroomTypes)
    {
        _classroomTypes = classroomTypes;
    }

    public Map<String, EppLoadType> getLoadTypeMap()
    {
        return _loadTypeMap;
    }

    public void setLoadTypeMap(Map<String, EppLoadType> loadTypeMap)
    {
        _loadTypeMap = loadTypeMap;
    }

    public Collection<EppALoadType> getEppALoadTypes()
    {
        return EppLoadTypeUtils.getALoadTypeList(getLoadTypeMap());
    }

    public EppLoadType getCurrentLoadType()
    {
        return _currentLoadType;
    }

    public void setCurrentLoadType(final EppLoadType currentLoadType)
    {
        _currentLoadType = currentLoadType;
    }

    public SppRegElementExt getSppRegElementExt()
    {
        return _sppRegElementExt;
    }

    public void setSppRegElementExt(SppRegElementExt sppRegElementExt)
    {
        _sppRegElementExt = sppRegElementExt;
    }

    public Long getElementId()
    {
        return _elementId;
    }

    public void setElementId(Long elementId)
    {
        _elementId = elementId;
    }

    public EppRegistryElement getElement()
    {
        return _element;
    }

    public void setElement(EppRegistryElement element)
    {
        _element = element;
    }
}
