package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuBrsSumDataReport
        if (tool.tableExists("fefubrssumdatareport_t"))
        {
            if (!tool.columnExists("fefubrssumdatareport_t", "edulevel_p"))
            // создано свойство eduLevel
            {
                // создать колонку
                tool.createColumn("fefubrssumdatareport_t", new DBColumn("edulevel_p", DBType.createVarchar(255)));

            }
            if (!tool.columnExists("fefubrssumdatareport_t", "group_p"))
            // создано свойство group
            {
                // создать колонку
                tool.createColumn("fefubrssumdatareport_t", new DBColumn("group_p", DBType.createVarchar(255)));

            }
        }

    }
}