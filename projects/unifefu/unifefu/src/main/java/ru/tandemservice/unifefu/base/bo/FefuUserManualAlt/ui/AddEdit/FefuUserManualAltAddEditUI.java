/* $Id: FefuUserManualAltAddEditUI.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuUserManualAlt.ui.AddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.memsize.MemorySizeService;
import ru.tandemservice.unifefu.base.bo.FefuUserManualAlt.FefuUserManualAltManager;
import ru.tandemservice.unifefu.entity.FefuUserManual;

/**
 * @author Victor Nekrasov
 * @since 28.04.2014
 */
@Input({
        @Bind(key = "id", binding = "id")
})
public class FefuUserManualAltAddEditUI extends UIPresenter
{
    private Long _id;
    private FefuUserManual _entity;

    private IUploadFile _uploadDocument;

    @Override
    public void onComponentRefresh()
    {
        _entity = FefuUserManualAltManager.instance().fefuUserManualAltDao().prepare(_id);
    }

    public void onClickApply()
    {
        FefuUserManualAltManager.instance().fefuUserManualAltDao().doSaveOrUpdate(_entity, _uploadDocument);
        deactivate();
    }

    // Calculate

    public boolean isAddForm()
    {
        return _id == null;
    }

    public String getSticker()
    {
        return isAddForm() ? getConfig().getProperty("ui.sticker.add") : getConfig().getProperty("ui.sticker.edit");
    }

    // Getters & Setters

    public IUploadFile getUploadDocument()
    {
        return _uploadDocument;
    }

    public void setUploadDocument(IUploadFile uploadDocument)
    {
        _uploadDocument = uploadDocument;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public FefuUserManual getEntity()
    {
        return _entity;
    }

    public void setEntity(FefuUserManual entity)
    {
        _entity = entity;
    }

    public String getMaxFileUploadSizeMsg()
    {
        long maxSize = Long.parseLong(ApplicationRuntime.getProperty("fileUpload.maxSize"));
        if(-1 == maxSize)
            return getConfig().getProperty("ui.maxFileUploadSize", "без ограничений");
        else
            return getConfig().getProperty("ui.maxFileUploadSize", MemorySizeService.MEMORY_FORMATTER.format(maxSize));
    }
}
