package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAssignPaymentExtract;
import ru.tandemservice.unifefu.entity.gen.FullStateMaintenanceStuExtractGen;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О полном государственном обеспечении
 */
public class FullStateMaintenanceStuExtract extends FullStateMaintenanceStuExtractGen implements IAssignPaymentExtract
{
    // В полях с суммой хранится значение в сотых долях копейки
    private final static int CURRENCY_ABBREVIATION_DIGITS = 4;
    private final static long CURRENCY_ABBREVIATION_PART = (long) Math.pow(10D, CURRENCY_ABBREVIATION_DIGITS);

    public Double getTotalPaymentSumInRuble()
    {
        return getTotalPaymentSum() != null ? ((double) getTotalPaymentSum() / (double) CURRENCY_ABBREVIATION_PART) : null;
    }

    @Override
    public BigDecimal getPaymentAmount()
    {
        return isHasPayments() && getTotalPaymentSum() != null ? new BigDecimal(getTotalPaymentSum()).movePointLeft(CURRENCY_ABBREVIATION_DIGITS) : null;
    }

    @Override
    public Date getPaymentBeginDate()
    {
        return getStartPaymentPeriodDate();
    }
}