/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.ParametersLabor;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.settings.DataSettings;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.tapestry.component.list.SimpleListDataHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.unifefu.entity.FefuEduProgramKind2MinLaborRel;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 12.11.2014
 */
public class FefuSettingsParametersLaborUI extends UIPresenter
{
    public static final String DATA_SETTINGS_NAME = "fefuParametersLaborSetting";
    public static final String DS_ZET = "zet";   // Зачетная единица (ЗЕТ) (в часах)

    private SimpleListDataHolder<FefuEduProgramKind2MinLaborRel> _dataHolder = new SimpleListDataHolder<>();
    private Long _zet;

    @Override
    public void onComponentRefresh()
    {
        IDataSettings dataSettings = DataSettingsFacade.getSettings(DATA_SETTINGS_NAME);
        _zet = dataSettings.get(DS_ZET) != null ? (Long) dataSettings.get(DS_ZET) : 36;

        List<FefuEduProgramKind2MinLaborRel> list = Lists.newArrayList();
        Map<EduProgramKind, FefuEduProgramKind2MinLaborRel> kind2MinLaborRelMap = Maps.newHashMap();

        List<EduProgramKind> eduProgramKindList = DataAccessServices.dao().getList(EduProgramKind.class, EduProgramKind.priority().s());
        List<FefuEduProgramKind2MinLaborRel> relList = DataAccessServices.dao().getList(FefuEduProgramKind2MinLaborRel.class);

        for (FefuEduProgramKind2MinLaborRel rel : relList)
        {
            kind2MinLaborRelMap.put(rel.getEduProgramKind(), rel);
        }
        for (EduProgramKind kind : eduProgramKindList)
        {
            FefuEduProgramKind2MinLaborRel rel = kind2MinLaborRelMap.get(kind);
            if (null == rel)
            {
                rel = new FefuEduProgramKind2MinLaborRel();
                rel.setEduProgramKind(kind);
                rel.setLabor(0);
            }
            list.add(rel);
        }
        getDataHolder().setup(list);
    }

    public void onClickApply()
    {
        IDataSettings dataSettings = new DataSettings(DATA_SETTINGS_NAME);
        dataSettings.set(DS_ZET, _zet);
        DataSettingsFacade.saveSettings(dataSettings);

        Collection<FefuEduProgramKind2MinLaborRel> rows = getDataHolder().getRows();
        for (FefuEduProgramKind2MinLaborRel row : rows)
        {
            DataAccessServices.dao().saveOrUpdate(row);
        }
        deactivate();
    }

    public Long getZet()
    {
        return _zet;
    }

    public void setZet(Long zet)
    {
        _zet = zet;
    }

    public SimpleListDataHolder<FefuEduProgramKind2MinLaborRel> getDataHolder()
    {
        return _dataHolder;
    }

    public void setDataHolder(SimpleListDataHolder<FefuEduProgramKind2MinLaborRel> dataHolder)
    {
        _dataHolder = dataHolder;
    }

    public String getNumber()
    {
        return String.valueOf(getDataHolder().getCurrentNumber());
    }

    public FefuEduProgramKind2MinLaborRel getCurrent()
    {
        return getDataHolder().getCurrent();
    }

    public EduProgramKind getEduProgramKind()
    {
        return getCurrent().getEduProgramKind();
    }

    public Double getLaborAsDouble()
    {
        return getCurrent().getLaborAsDouble();
    }

    public void setLaborAsDouble(Double labor)
    {
        getCurrent().setLaborAsDouble(labor);
    }
}