/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic.FefuSystemActionDao;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic.IFefuSystemActionDao;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
@Configuration
public class FefuSystemActionManager extends BusinessObjectManager
{
    public static FefuSystemActionManager instance()
    {
        return instance(FefuSystemActionManager.class);
    }

    @Bean
    public IFefuSystemActionDao dao()
    {
        return new FefuSystemActionDao();
    }

    //Тип онлайн-абитуриента
    public final static Long ENTRANT_TYPE_DEFAULT = 1L;//Обычный
    public final static Long ENTRANT_TYPE_FOREIGN = 2L;//Иностранный

    @Bean
    public ItemListExtPoint<DataWrapper> entrantTypeExtPoint()
    {
        return itemList(DataWrapper.class).
                add(ENTRANT_TYPE_DEFAULT.toString(), new DataWrapper(ENTRANT_TYPE_DEFAULT, "ui.entrantType.default")).
                add(ENTRANT_TYPE_FOREIGN.toString(), new DataWrapper(ENTRANT_TYPE_FOREIGN, "ui.entrantType.foreign")).
                create();
    }
}
