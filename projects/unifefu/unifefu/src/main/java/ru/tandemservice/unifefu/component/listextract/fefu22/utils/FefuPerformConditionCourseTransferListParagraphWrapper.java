/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu22.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.*;

/**
 * @author Igor Belanov
 * @since 05.07.2016
 */
public class FefuPerformConditionCourseTransferListParagraphWrapper implements Comparable<FefuPerformConditionCourseTransferListParagraphWrapper>
{
    private final StudentCategory _studentCategory;
    private final DevelopCondition _developCondition;
    private final DevelopTech _developTech;
    private final DevelopPeriod _developPeriod;
    private final CompensationType _compensationType;
    private final EducationLevels _educationLevels;
    private final DevelopForm _developForm;
    private final Course _course;
    private final Group _group;

    private final ListStudentExtract _firstExtract;

    private final List<FefuPerformConditionCourseTransferListSubParagraphWrapper> subParagraphList = new ArrayList<>();

    public FefuPerformConditionCourseTransferListParagraphWrapper(StudentCategory studentCategory, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod, CompensationType compensationType, EducationLevels educationLevels, DevelopForm developForm, Course course, Group group, ListStudentExtract firstExtract)
    {
        _studentCategory = studentCategory;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
        _compensationType = compensationType;
        _educationLevels = educationLevels;
        _developForm = developForm;
        _course = course;
        _group = group;
        _firstExtract = firstExtract;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public EducationLevels getEducationLevels()
    {
        return _educationLevels;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public Course getCourse()
    {
        return _course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public List<FefuPerformConditionCourseTransferListSubParagraphWrapper> getSubParagraphList()
    {
        return subParagraphList;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuPerformConditionCourseTransferListParagraphWrapper))
            return false;
        FefuPerformConditionCourseTransferListParagraphWrapper anotherParagraphWrapper = (FefuPerformConditionCourseTransferListParagraphWrapper) o;

        return _studentCategory.equals(anotherParagraphWrapper.getStudentCategory()) &&
                _developCondition.equals(anotherParagraphWrapper.getDevelopCondition()) &&
                _developTech.equals(anotherParagraphWrapper.getDevelopTech()) &&
                _developPeriod.equals(anotherParagraphWrapper.getDevelopPeriod()) &&
                _compensationType.equals(anotherParagraphWrapper.getCompensationType()) &&
                _educationLevels.equals(anotherParagraphWrapper.getEducationLevels()) &&
                _developForm.equals(anotherParagraphWrapper.getDevelopForm()) &&
                _course.equals(anotherParagraphWrapper.getCourse()) &&
                _group.equals(anotherParagraphWrapper.getGroup());
    }

    @Override
    public int hashCode()
    {
        return _studentCategory.hashCode() &
                _developCondition.hashCode() &
                _developTech.hashCode() &
                _developPeriod.hashCode() &
                _compensationType.hashCode() &
                _educationLevels.hashCode() &
                _developForm.hashCode() &
                _course.hashCode() &
                _group.hashCode();
    }

    @Override
    public int compareTo(FefuPerformConditionCourseTransferListParagraphWrapper anotherParagraphWrapper)
    {
        int compareResult = _studentCategory.getCode().compareTo(anotherParagraphWrapper.getStudentCategory().getCode());

        if (compareResult == 0)
            compareResult = _compensationType.getCode().compareTo(anotherParagraphWrapper.getCompensationType().getCode());

        return compareResult;
    }
}
