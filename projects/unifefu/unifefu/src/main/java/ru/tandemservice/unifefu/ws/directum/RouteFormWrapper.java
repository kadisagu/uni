/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;

/**
 * @author Dmitry Seleznev
 * @since 18.07.2013
 */
public class RouteFormWrapper
{
    private FefuDirectumOrderType _orderType;

    private String _signer;
    private String _routeCode;
    private String _orgUnitCode;
    private String _orgUnitTitle;
    private String _executor;
    private String _commonOrderType;

    private boolean _dismiss;
    private boolean _restore;
    private boolean _offBudgetPayments;
    private boolean _foreignStudent;
    private boolean _grantMatAid;
    private boolean _social;
    private boolean _practice;
    private boolean _groupManager;
    private boolean _uvcStudents;
    private boolean _fvoStudents;
    private boolean _penalty;
    private boolean _mpr;
    private boolean _payments;
    private boolean _stateFormular;
    private boolean _international;
    private boolean _checkMedicalDocs;
    private boolean _diplomaSuccess;

    public RouteFormWrapper(FefuDirectumOrderType orderType)
    {
        _orderType = orderType;
    }

    public FefuDirectumOrderType getOrderType()
    {
        return _orderType;
    }

    public void setOrderType(FefuDirectumOrderType orderType)
    {
        _orderType = orderType;
    }

    public String getSigner()
    {
        return _signer;
    }

    public void setSigner(String signer)
    {
        _signer = signer;
    }

    public String getRouteCode()
    {
        return _routeCode;
    }

    public void setRouteCode(String routeCode)
    {
        _routeCode = routeCode;
    }

    public String getOrgUnitCode()
    {
        return _orgUnitCode;
    }

    public void setOrgUnitCode(String orgUnitCode)
    {
        _orgUnitCode = orgUnitCode;
    }

    public String getOrgUnitTitle()
    {
        return _orgUnitTitle;
    }

    public void setOrgUnitTitle(String orgUnitTitle)
    {
        _orgUnitTitle = orgUnitTitle;
    }

    public String getExecutor()
    {
        return _executor;
    }

    public void setExecutor(String executor)
    {
        _executor = executor;
    }

    public boolean isDismiss()
    {
        return _dismiss;
    }

    public void setDismiss(boolean dismiss)
    {
        _dismiss = dismiss;
    }

    public boolean isRestore()
    {
        return _restore;
    }

    public void setRestore(boolean restore)
    {
        _restore = restore;
    }

    public boolean isOffBudgetPayments()
    {
        return _offBudgetPayments;
    }

    public void setOffBudgetPayments(boolean offBudgetPayments)
    {
        _offBudgetPayments = offBudgetPayments;
    }

    public boolean isForeignStudent()
    {
        return _foreignStudent;
    }

    public void setForeignStudent(boolean foreignStudent)
    {
        _foreignStudent = foreignStudent;
    }

    public boolean isGrantMatAid()
    {
        return _grantMatAid;
    }

    public void setGrantMatAid(boolean grantMatAid)
    {
        _grantMatAid = grantMatAid;
    }

    public boolean isSocial()
    {
        return _social;
    }

    public void setSocial(boolean social)
    {
        _social = social;
    }

    public boolean isPractice()
    {
        return _practice;
    }

    public void setPractice(boolean practice)
    {
        _practice = practice;
    }

    public boolean isGroupManager()
    {
        return _groupManager;
    }

    public void setGroupManager(boolean groupManager)
    {
        _groupManager = groupManager;
    }

    public boolean isUvcStudents()
    {
        return _uvcStudents;
    }

    public void setUvcStudents(boolean uvcStudents)
    {
        _uvcStudents = uvcStudents;
    }

    public boolean isFvoStudents()
    {
        return _fvoStudents;
    }

    public void setFvoStudents(boolean fvoStudents)
    {
        _fvoStudents = fvoStudents;
    }

    public boolean isPenalty()
    {
        return _penalty;
    }

    public void setPenalty(boolean penalty)
    {
        _penalty = penalty;
    }

    public boolean isMpr()
    {
        return _mpr;
    }

    public void setMpr(boolean mpr)
    {
        _mpr = mpr;
    }

    public boolean isPayments()
    {
        return _payments;
    }

    public void setPayments(boolean payments)
    {
        _payments = payments;
    }

    public boolean isStateFormular()
    {
        return _stateFormular;
    }

    public void setStateFormular(boolean stateFormular)
    {
        _stateFormular = stateFormular;
    }

    public boolean isInternational()
    {
        return _international;
    }

    public void setInternational(boolean international)
    {
        _international = international;
    }

    public boolean isCheckMedicalDocs()
    {
        return _checkMedicalDocs;
    }

    public void setCheckMedicalDocs(boolean checkMedicalDocs)
    {
        _checkMedicalDocs = checkMedicalDocs;
    }

    public boolean isDiplomaSuccess()
    {
        return _diplomaSuccess;
    }

    public void setDiplomaSuccess(boolean diplomaSuccess)
    {
        _diplomaSuccess = diplomaSuccess;
    }

    public String getCommonOrderType()
    {
        return _commonOrderType;
    }

    public void setCommonOrderType(String commonOrderType)
    {
        _commonOrderType = commonOrderType;
    }
}