/* $Id: FefuAddressBaseListener.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.events.dset;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressInter;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.fias.base.entity.AddressString;
import ru.tandemservice.unifefu.dao.daemon.FEFUAllStudExportDaemonDAO;
import ru.tandemservice.unifefu.dao.daemon.IFEFUAllStudExportDaemonDAO;
import ru.tandemservice.unifefu.entity.ws.MdbViewAddress;

import java.util.Collection;

/**
 * @author Victor Nekrasov
 * @since 15.04.2014
 */

public class FefuAddressBaseListener extends ParamTransactionCompleteListener<Boolean>
{
    @SuppressWarnings("unchecked")
        public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, AddressRu.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, AddressInter.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, AddressDetailed.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, AddressString.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, AddressRu.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, AddressInter.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, AddressDetailed.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, AddressString.class, this);

        }

        @Override
        public Boolean beforeCompletion(final Session session, final Collection<Long> params)
        {
            IFEFUAllStudExportDaemonDAO.instance.get().doRegisterEntity(MdbViewAddress.ENTITY_NAME, params);
            FEFUAllStudExportDaemonDAO.DAEMON.registerAfterCompleteWakeUp(session);
            return true;
        }

        @Override
        @Transactional(readOnly = false)
        public void afterCompletion(Session session, int status, Collection<Long> params, Boolean beforeCompletionResult)
        {
//            IFEFUAllStudExportDaemonDAO.instance.get().doRegisterEntity(MdbViewAddress.ENTITY_NAME, params);
//            FEFUAllStudExportDaemonDAO.DAEMON.wakeUpDaemon();
        }
}

