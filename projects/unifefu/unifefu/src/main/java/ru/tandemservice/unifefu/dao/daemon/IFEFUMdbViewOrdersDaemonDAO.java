package ru.tandemservice.unifefu.dao.daemon;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

import java.util.Collection;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 17.02.14
 * Time: 16:22
 * To change this template use File | Settings | File Templates.
 */
public interface IFEFUMdbViewOrdersDaemonDAO {

    String GLOBAL_DAEMON_LOCK = IFEFUMdbViewOrdersDaemonDAO.class.getName() + ".global-lock";
    SpringBeanCache<IFEFUMdbViewOrdersDaemonDAO> instance = new SpringBeanCache<>(IFEFUMdbViewOrdersDaemonDAO.class.getName());

   /* public void doExportOrders();   */

    void deleteAllEntities();
    List<Long> getExtractIds(Boolean b);
    List<Long> getOtherExtractIds(Boolean b);

    @Transactional(readOnly = false)
    void export_OtherOrders(Collection<Long> ids);

    @Transactional(readOnly = false)
    void export_Orders(Collection<Long> ids);

    @Transactional(readOnly = false)
    void doExportSelectedOrders(Session session, List<Long> ids);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doExportNewOrders();
    void doExportNewOtherOrders();

}
