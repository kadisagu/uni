package ru.tandemservice.unifefu.component.report;

import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.util.report.IReportRow;

import java.util.Comparator;
import java.util.Map;

public class FefuRequestedEnrollmentDirectionComparator implements Comparator<IReportRow>
{
    private Map<CompetitionKind, Integer> _competitionKindPriorities;

    /**
     * @param competitionKindPriorities приоритеты видов конкурса
     */
    public FefuRequestedEnrollmentDirectionComparator(Map<CompetitionKind, Integer> competitionKindPriorities)
    {
        _competitionKindPriorities = competitionKindPriorities;
    }

    @Override
    public int compare(IReportRow o1, IReportRow o2)
    {
        // получаем выбранные направления
        RequestedEnrollmentDirection r1 = o1.getRequestedEnrollmentDirection();
        RequestedEnrollmentDirection r2 = o2.getRequestedEnrollmentDirection();

        int result;

        CompetitionKind c1 = r1.getCompetitionKind();
        CompetitionKind c2 = r2.getCompetitionKind();

        // сравнить по приоритетам видов конкурса
        result = _competitionKindPriorities.get(c1) - _competitionKindPriorities.get(c2);

        // сначала сравниваем по сумме баллов
        if (result == 0)
            result = - UniBaseUtils.compare(o1.getFinalMark(), o2.getFinalMark(), true);

        // теперь по баллам за профильное вступительное
        if (result == 0)
            result = - UniBaseUtils.compare(o1.getProfileMark(), o2.getProfileMark(), true);

        // теперь по призаку "закончил профильное обр. учреждение"
        if (result == 0)
            result = - o1.isGraduatedProfileEduInstitution().compareTo(o2.isGraduatedProfileEduInstitution());

        // теперь по среднему баллу аттестата
        if (result == 0)
            result = - UniBaseUtils.compare(o1.getCertificateAverageMark(), o2.getCertificateAverageMark(), true);

        // все осмысленное кончилось, теперь по фио
        if (result == 0)
            result = CommonCollator.RUSSIAN_COLLATOR.compare(o1.getFio(), o2.getFio());

        if (result == 0)
            result = Long.compare(r1.getId(), r2.getId());

        return result;
    }
}
