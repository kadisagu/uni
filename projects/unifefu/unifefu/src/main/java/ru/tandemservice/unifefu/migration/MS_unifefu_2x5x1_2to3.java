package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author dperminov
 * @since 08.04.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.5.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.5.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEntrantOrderExtension
        // создано свойство directumTask
        if (!tool.columnExists("fefuentrantorderextension_t", "directumtask_p"))
        {
			// создать колонку
			tool.createColumn("fefuentrantorderextension_t", new DBColumn("directumtask_p", DBType.createVarchar(255)));
            // заполнить колонку
            tool.getStatement().executeUpdate("update t1 set t1.directumtask_p=jt.DIRECTUM_TASK" +
                    " from FEFUENTRANTORDEREXTENSION_T as t1" +
                    " join (select t2.ID id," +
                    " (select top 1 dl.DIRECTUMID_P from FEFUDIRECTUMLOG_T as dl" +
                    " where dl.ENRORDEREXT_ID=t2.ID and dl.OPERATIONTYPE_P like 'Создание задачи%' and dl.DIRECTUMID_P is not null" +
                    " order by dl.ID desc) as DIRECTUM_TASK" +
                    " from FEFUENTRANTORDEREXTENSION_T t2) jt" +
                    " on jt.id = t1.ID");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuStudentOrderExtension
		// создано свойство directumTask
        if (!tool.columnExists("fefustudentorderextension_t", "directumtask_p"))
		{
			// создать колонку
			tool.createColumn("fefustudentorderextension_t", new DBColumn("directumtask_p", DBType.createVarchar(255)));
            // заполнить колонку
            tool.getStatement().executeUpdate("update t1 set t1.directumtask_p=jt.DIRECTUM_TASK" +
                    " from FEFUSTUDENTORDEREXTENSION_T as t1" +
                    " join (select t2.ID id," +
                    " (select top 1 dl.DIRECTUMID_P from FEFUDIRECTUMLOG_T as dl" +
                    " where dl.ORDEREXT_ID=t2.ID and dl.OPERATIONTYPE_P like 'Создание задачи%' and dl.DIRECTUMID_P is not null" +
                    " order by dl.ID desc) as DIRECTUM_TASK" +
                    " from FEFUSTUDENTORDEREXTENSION_T t2) jt" +
                    " on jt.id = t1.ID");
		}
    }
}