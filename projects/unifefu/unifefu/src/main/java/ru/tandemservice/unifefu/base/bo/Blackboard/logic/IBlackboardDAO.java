/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.CourseVO;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.ColumnVO;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;

import java.util.Collection;

/**
 * @author Nikolay Fedorovskih
 * @since 12.03.2014
 */
public interface IBlackboardDAO extends INeedPersistenceSupport
{
    public static final String ARCHIVE_PREFIX = "(Архив)";

    /**
     * Привязка наборы курсов (ЭУК) к элементу реестра
     *
     * @param courses         курсы BB
     * @param registryElement дисциплина реестра
     * @param ppsEntries      список преподвателей
     */
    void linkCoursesWithEppRegistryElement(Collection<CourseVO> courses, EppRegistryElement registryElement, Collection<PpsEntry> ppsEntries);

    /**
     * Создание в нашей базе записи о курсе ЭО с привязкой преподавателей и дисциплины реестра
     *
     * @param bbPrimaryId     примари идентификатор курса
     * @param bbCourseId      не-примари-идентификатор курса
     * @param title           название
     * @param registryElement дисциплина реестра
     * @param ppsEntries      список преподвателей
     * @param internal        создан ли курс из Юни
     * @return созданный курс
     */
    BbCourse createCourse(String bbPrimaryId, String bbCourseId, String title, EppRegistryElement registryElement, Collection<PpsEntry> ppsEntries, boolean internal);

    /**
     * Нужно ли клонировать курс и списывать в архив после привязки к реализации. Это нужно, если выполняется 2 условия:
     * 1. Если в указанном семестре ещё не было связей данного курса с журналами.
     * 2. Если этот курс создан не из Юни либо для него в принципе были связи с журналами.
     * Второе условие нужно, чтобы только что созданный из Юни курс сразу же не списывался в архив при привязке к журналу.
     *
     * @param bbCourse    ЭУК
     * @param eppYearPart часть учебного года
     * @return есть или нет
     */
    boolean needArchiveCourseAfterLinkToJournal(BbCourse bbCourse, EppYearPart eppYearPart);

    /**
     * Привязка ЭУК к реализации дисциплилны
     *
     * @param bbCourse  ЭУК
     * @param trJournal реализация дисциплины
     */
    void linkCourseWithTrJournal(BbCourse bbCourse, TrJournal trJournal);

    /**
     * Создание копии курса и привязка к реализации дисциплины.
     * Если это первая привязка курса к реализации в данном семестре, то исходный курс списываем в архив.
     *
     * @param bbCourse  курс
     * @param trJournal реализация дисциплины
     */
    void cloneCourseForTrJournal(BbCourse bbCourse, TrJournal trJournal);

    /**
     * Создание нового курса на базе другого.
     *
     * @param srcCourse       исходный курс
     * @param registryElement элемент реестра, к которому будет привязка
     * @param ppsEntryList    список преподавателей
     * @return идентификатор нового курса
     */
    Long cloneCourse(CourseVO srcCourse, EppRegistryElement registryElement, Collection<PpsEntry> ppsEntryList);

    /**
     * Создание нового курса на базе другого. К новому курсу привязываются все те элементы реестра, которые были привязаны к исходному.
     *
     * @param srcCourse       исходный курс
     * @param registryElement элемент реестра, к которому будет привязка
     * @return идентификатор нового курса
     */
    Long cloneCourse(BbCourse srcCourse, EppRegistryElement registryElement);

    /**
     * Отправка курса в архив. Названию курса приписывается "{@value IBlackboardDAO#ARCHIVE_PREFIX}" и выставляется признак архивности
     *
     * @param bbCourse ЭУК
     */
    void sendCourseToArchive(BbCourse bbCourse);

    /**
     * Создание или обновление связи контрольного мероприятия в структуре чтения с мероприятием (колонкой) ЭУКа
     *
     * @param course  курс
     * @param event   событие реализации
     * @param bbEvent событие курса
     */
    void saveOrUpdateEventRelation(BbCourse course, TrJournalEvent event, ColumnVO bbEvent);

    /**
     * Удаление связи курса с дисциплиной реестра
     *
     * @param bbCourseId   идентификатор ЭУК
     * @param regElementId идентификатор дисциплины реестра
     */
    void deleteCourse2EppRegElementRelation(Long bbCourseId, Long regElementId);

    /**
     * Удаление связи курса с реализацией дисциплины
     *
     * @param bbCourseId   идентификатор ЭУК
     * @param trJournalId  идентификатор журнала
     */
    void deleteCourse2TrJournalRelation(Long bbCourseId, Long trJournalId);
}