/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRequest;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 18.06.2013
 */
public class IdmRequestDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PERSON_FILTER = "person";
    public static final String GUID_FILTER = "guid";

    public IdmRequestDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Person person = (Person) context.get(PERSON_FILTER);
        String guid = (String) context.get(GUID_FILTER);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuNsiRequest.class, "e").column("e");

        builder.where(eq(property(FefuNsiRequest.sent().fromAlias("e")), value(Boolean.TRUE)));
        builder.where(isNull(property(FefuNsiRequest.loginFromNSI().fromAlias("e"))));

        if (person != null)
            builder.where(eq(property(FefuNsiRequest.person().id().fromAlias("e")), value(person.getId())));

        if (guid != null)
            builder.where(eq(property(FefuNsiRequest.nsiGuid().guid().fromAlias("e")), value(guid)));

        List<FefuNsiRequest> requestsList = builder.createStatement(new DQLExecutionContext(context.getSession())).list();

        if (null != input.getEntityOrder())
            Collections.sort(requestsList, new EntityComparator<>(input.getEntityOrder()));

        return ListOutputBuilder.get(input, requestsList).pageable(true).build();
    }
}