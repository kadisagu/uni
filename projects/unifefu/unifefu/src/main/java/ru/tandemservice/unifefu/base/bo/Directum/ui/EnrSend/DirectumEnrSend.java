/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.EnrSend;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Dmitry Seleznev
 * @since 05.08.2013
 */
@Configuration
public class DirectumEnrSend extends BusinessComponentManager
{
    public static final String ORDER_TYPE_DS = "orderTypeDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ORDER_TYPE_DS, orderTypeDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler orderTypeDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName());
    }
}