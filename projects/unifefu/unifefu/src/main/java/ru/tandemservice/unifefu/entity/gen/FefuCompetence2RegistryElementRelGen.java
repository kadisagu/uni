package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.FefuCompetence2RegistryElementRel;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь компетенции с элементом реестра
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuCompetence2RegistryElementRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuCompetence2RegistryElementRel";
    public static final String ENTITY_NAME = "fefuCompetence2RegistryElementRel";
    public static final int VERSION_HASH = 1321344928;
    private static IEntityMeta ENTITY_META;

    public static final String L_FEFU_COMPETENCE = "fefuCompetence";
    public static final String L_REGISTRY_ELEMENT = "registryElement";

    private FefuCompetence _fefuCompetence;     // Компетенция(ДВФУ)
    private EppRegistryElement _registryElement;     // Элемент реестра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Компетенция(ДВФУ). Свойство не может быть null.
     */
    @NotNull
    public FefuCompetence getFefuCompetence()
    {
        return _fefuCompetence;
    }

    /**
     * @param fefuCompetence Компетенция(ДВФУ). Свойство не может быть null.
     */
    public void setFefuCompetence(FefuCompetence fefuCompetence)
    {
        dirty(_fefuCompetence, fefuCompetence);
        _fefuCompetence = fefuCompetence;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    /**
     * @param registryElement Элемент реестра. Свойство не может быть null.
     */
    public void setRegistryElement(EppRegistryElement registryElement)
    {
        dirty(_registryElement, registryElement);
        _registryElement = registryElement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuCompetence2RegistryElementRelGen)
        {
            setFefuCompetence(((FefuCompetence2RegistryElementRel)another).getFefuCompetence());
            setRegistryElement(((FefuCompetence2RegistryElementRel)another).getRegistryElement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuCompetence2RegistryElementRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuCompetence2RegistryElementRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuCompetence2RegistryElementRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "fefuCompetence":
                    return obj.getFefuCompetence();
                case "registryElement":
                    return obj.getRegistryElement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "fefuCompetence":
                    obj.setFefuCompetence((FefuCompetence) value);
                    return;
                case "registryElement":
                    obj.setRegistryElement((EppRegistryElement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "fefuCompetence":
                        return true;
                case "registryElement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "fefuCompetence":
                    return true;
                case "registryElement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "fefuCompetence":
                    return FefuCompetence.class;
                case "registryElement":
                    return EppRegistryElement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuCompetence2RegistryElementRel> _dslPath = new Path<FefuCompetence2RegistryElementRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuCompetence2RegistryElementRel");
    }
            

    /**
     * @return Компетенция(ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2RegistryElementRel#getFefuCompetence()
     */
    public static FefuCompetence.Path<FefuCompetence> fefuCompetence()
    {
        return _dslPath.fefuCompetence();
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2RegistryElementRel#getRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> registryElement()
    {
        return _dslPath.registryElement();
    }

    public static class Path<E extends FefuCompetence2RegistryElementRel> extends EntityPath<E>
    {
        private FefuCompetence.Path<FefuCompetence> _fefuCompetence;
        private EppRegistryElement.Path<EppRegistryElement> _registryElement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Компетенция(ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2RegistryElementRel#getFefuCompetence()
     */
        public FefuCompetence.Path<FefuCompetence> fefuCompetence()
        {
            if(_fefuCompetence == null )
                _fefuCompetence = new FefuCompetence.Path<FefuCompetence>(L_FEFU_COMPETENCE, this);
            return _fefuCompetence;
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCompetence2RegistryElementRel#getRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> registryElement()
        {
            if(_registryElement == null )
                _registryElement = new EppRegistryElement.Path<EppRegistryElement>(L_REGISTRY_ELEMENT, this);
            return _registryElement;
        }

        public Class getEntityClass()
        {
            return FefuCompetence2RegistryElementRel.class;
        }

        public String getEntityName()
        {
            return "fefuCompetence2RegistryElementRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
