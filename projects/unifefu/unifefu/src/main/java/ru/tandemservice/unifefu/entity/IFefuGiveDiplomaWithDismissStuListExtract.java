/* $Id$ */
package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author nvankov
 * @since 7/3/13
 */
public interface IFefuGiveDiplomaWithDismissStuListExtract
{
    Group getGroup();

    Course getCourse();

    DevelopPeriod getDevelopPeriod();

    CompensationType getCompensationType();

    boolean isPrintDiplomaQualification();
}
