/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu3.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubController;
import ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 18.03.2013
 */
public class Controller extends AbstractListExtractPubController<FefuAdmitToDiplomaStuListExtract, Model, IDAO>
{
}