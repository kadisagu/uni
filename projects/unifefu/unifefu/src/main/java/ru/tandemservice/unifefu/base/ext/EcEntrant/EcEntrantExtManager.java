/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EcEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uniec.base.bo.EcEntrant.logic.IEcEntrantDao;
import ru.tandemservice.unifefu.base.ext.EcEntrant.logic.FefuEcEntrantDao;

/**
 * @author Nikolay Fedorovskih
 * @since 19.06.2013
 */
@Configuration
public class EcEntrantExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEcEntrantDao dao()
    {
        return new FefuEcEntrantDao();
    }
}