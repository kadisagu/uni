/*$Id$*/
package ru.tandemservice.unifefu.component.group.FefuGroupStudentList;


import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.component.group.FefuGroupPub.FefuGroupStudentListCmp;

import java.util.Comparator;

/**
 * @author DMITRY KNYAZEV
 * @since 02.10.2014
 */
public class DAO extends ru.tandemservice.uni.component.group.GroupStudentList.DAO implements IDAO
{

	@Override
	protected Comparator<Student> getStudentComparator()
	{
		return new FefuGroupStudentListCmp();
	}

}
