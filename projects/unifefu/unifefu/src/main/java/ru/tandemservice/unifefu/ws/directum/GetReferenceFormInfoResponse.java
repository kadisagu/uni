/**
 * GetReferenceFormInfoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class GetReferenceFormInfoResponse  implements java.io.Serializable {
    private java.lang.String getReferenceFormInfoResult;

    public GetReferenceFormInfoResponse() {
    }

    public GetReferenceFormInfoResponse(
           java.lang.String getReferenceFormInfoResult) {
           this.getReferenceFormInfoResult = getReferenceFormInfoResult;
    }


    /**
     * Gets the getReferenceFormInfoResult value for this GetReferenceFormInfoResponse.
     * 
     * @return getReferenceFormInfoResult
     */
    public java.lang.String getGetReferenceFormInfoResult() {
        return getReferenceFormInfoResult;
    }


    /**
     * Sets the getReferenceFormInfoResult value for this GetReferenceFormInfoResponse.
     * 
     * @param getReferenceFormInfoResult
     */
    public void setGetReferenceFormInfoResult(java.lang.String getReferenceFormInfoResult) {
        this.getReferenceFormInfoResult = getReferenceFormInfoResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetReferenceFormInfoResponse)) return false;
        GetReferenceFormInfoResponse other = (GetReferenceFormInfoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getReferenceFormInfoResult==null && other.getGetReferenceFormInfoResult()==null) || 
             (this.getReferenceFormInfoResult!=null &&
              this.getReferenceFormInfoResult.equals(other.getGetReferenceFormInfoResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetReferenceFormInfoResult() != null) {
            _hashCode += getGetReferenceFormInfoResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetReferenceFormInfoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">GetReferenceFormInfoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getReferenceFormInfoResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "GetReferenceFormInfoResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
