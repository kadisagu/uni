/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu5;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuCourseTransferStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class FefuCourseTransferListExtractDao extends UniBaseDao implements IExtractComponentDao<FefuCourseTransferStuListExtract>
{
    @Override
    public void doCommit(FefuCourseTransferStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

	    doCaptainStudent(extract.getEntity(), extract.getGroup(), extract.getGroupNew());

        extract.getEntity().setCourse(extract.getCourseNew());
        extract.getEntity().setGroup(extract.getGroupNew());
    }

    @Override
    public void doRollback(FefuCourseTransferStuListExtract extract, Map parameters)
    {
	    doCaptainStudent(extract.getEntity(), extract.getGroupNew(), extract.getGroup());

        extract.getEntity().setCourse(extract.getCourse());
        extract.getEntity().setGroup(extract.getGroup());
    }

	private void doCaptainStudent(Student student, Group groupOld, Group groupNew)
	{
		if ((UniDaoFacade.getGroupDao().isCaptainStudent(groupOld, student)) && !UniDaoFacade.getGroupDao().isCaptainStudent(groupNew, student))
		{
			UniDaoFacade.getGroupDao().deleteCaptainStudent(groupOld, student);
			UniDaoFacade.getGroupDao().addCaptainStudent(groupNew, student);
		}
	}
}
