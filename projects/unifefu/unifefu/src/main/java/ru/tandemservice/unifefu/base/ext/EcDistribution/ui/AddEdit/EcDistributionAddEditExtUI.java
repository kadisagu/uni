/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EcDistribution.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.base.bo.EcDistribution.EcDistributionManager;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.EcDistributionDao;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.AddEdit.EcDistributionAddEdit;
import ru.tandemservice.uniec.base.bo.EcDistribution.ui.AddEdit.EcDistributionAddEditUI;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgQuotaDTO;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.Map;

/**
 * @author nvankov
 * @since 7/9/13
 */
public class EcDistributionAddEditExtUI extends UIAddon
{
    private Map<Long, Integer> _quotaPlanMap;              // id направления приема распределения -> план
    private Map<Long, Map<Long, Integer>> _taQuotaPlanMap; // id направления приема распределения -> вид цп -> план

    public EcDistributionAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        EcDistributionAddEditUI presenter = getPresenter();
        int wave = 0;
        if(null != presenter.getDistributionDTO().getPersistentId())
        {
            EcgDistribution distribution = DataAccessServices.dao().get(presenter.getDistributionDTO().getPersistentId());
            wave = distribution.getWave();
        }
        IEcgQuotaDTO quotaDTO = EcDistributionManager.instance().dao().getRecomendedQuotaDTO(presenter.getDistributionDTO().getConfigDTO(), wave > 0 ? EcDistributionDao.DISTRIBUTION_FIRST_WAVE == wave : false);
        _quotaPlanMap = quotaDTO.getQuotaMap();
        _taQuotaPlanMap = quotaDTO.getTaQuotaMap();
    }

    public Integer getCurrentQuotaPlan()
    {
        EcDistributionAddEditUI presenter = getPresenter();
        Long id = ((EnrollmentDirection) presenter.getConfig().getDataSource(EcDistributionAddEdit.DIRECTION_DS).getCurrent()).getId();
        Integer quota = _quotaPlanMap.get(id);
        return quota == null ? 0 : quota;
    }

    public Integer getCurrentQuotaTAPlan()
    {
        EcDistributionAddEditUI presenter = getPresenter();
        Long id = ((EnrollmentDirection) presenter.getConfig().getDataSource(EcDistributionAddEdit.DIRECTION_DS).getCurrent()).getId();
        Map<Long, Integer> map = _taQuotaPlanMap.get(id);
        if (map == null) return 0;
        Integer quota = map.get(presenter.getCurrentTaKind().getId());
        return quota == null ? 0 : quota;
    }
}
