/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.EntrantRequestAddEdit;

import org.tandemframework.core.component.IBusinessComponent;

/**
 * @author Nikolay Fedorovskih
 * @since 18.06.2013
 */
public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantRequestAddEdit.Controller
{
    @Override
    protected void validateFields(IBusinessComponent component)
    {
        super.validateFields(component);
        Model model = (Model) getModel(component);
        if (model.getTechnicalCommission() == null)
            component.getUserContext().getErrorCollector().add("Поле «Техническая комиссия» обязательно для заполнения.", "technicalCommission");
    }
}