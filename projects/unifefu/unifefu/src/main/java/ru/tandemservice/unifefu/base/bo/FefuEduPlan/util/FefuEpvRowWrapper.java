/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.util;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.Collection;
import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
public abstract class FefuEpvRowWrapper extends IdentifiableWrapper<EppEpvRow> implements IHierarchyItem
{
    public static final String COMPETENCES = "competences";
    public static final String TITLE = "title";
    public static final String EDIT_DISABLED = "editDisabled";

    private EppEpvRow _row;
    private String _competences;
    private Map<Long, Map<Long, Double>> _dataMap;

    public FefuEpvRowWrapper(EppEpvRow row, Collection<String> competences)
    {
        super(row);
        _row = row;
        _competences = CommonBaseStringUtil.joinNotEmpty(competences, ", ");
    }

    public String getCompetences(){ return _competences; }

    public EppEpvRow getRow(){ return _row; }

    public abstract IHierarchyItem getHierarhyParent();
    public boolean isEditDisabled()
    {
        return !(_row instanceof EppEpvTermDistributedRow);
    }

    public Map<Long, Map<Long, Double>> getDataMap(){ return _dataMap; }
    public void setDataMap(Map<Long, Map<Long, Double>> dataMap){ _dataMap = dataMap; }
}