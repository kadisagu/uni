/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuJournalReportRating.ui.History;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.IPublisherColumnBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuJournalReportRating.ui.PackagePub.FefuJournalReportRatingPackagePub;
import ru.tandemservice.unifefu.base.bo.FefuStudent.logic.FefuStudentFIOListDSHandler;
import ru.tandemservice.unifefu.entity.FefuRatingPackageStudentRow;
import ru.tandemservice.unifefu.entity.FefuSendingRatingPackage;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 28.04.2014
 */
@Configuration
public class FefuJournalReportRatingHistory extends BusinessComponentManager
{
	// data sources
	public static final String HISTORY_DS = "historyDS";
	public static final String FEFU_STUDENT_FIO_DS = "fefuStudentListDS";
	// поля фильтра
	public static final String FEFU_REQUEST_DATE_FROM = "requestDateFrom";
	public static final String FEFU_REQUEST_DATE_TO = "requestDateTo";
	public static final String FEFU_STUDENT_FIO = "fefuStudentFio";
	public static final String EXTERNAL_ID = "externalID";
	public static final String XML_DATA_SEARCH = "xmlDataSearch";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(searchListDS(HISTORY_DS, getHistoryDS(), historyDSHandler()))
				.addDataSource(selectDS(FEFU_STUDENT_FIO_DS, fefuStudentListDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint getHistoryDS()
	{
		IPublisherColumnBuilder linkResolverDateTime = publisherColumn(FefuSendingRatingPackage.P_SENDING_TIME,
		                                                               FefuSendingRatingPackage.P_SENDING_TIME)
				.publisherLinkResolver(
						new IPublisherLinkResolver()
						{
							@Override
							public Object getParameters(IEntity entity)
							{
								FefuSendingRatingPackage pack = ((FefuSendingRatingPackage) entity);
								Map<String, Object> params = new HashMap<>();
								params.put(UIPresenter.PUBLISHER_ID, pack.getId());
								return params;
							}

							@Override
							public String getComponentName(IEntity entity)
							{
								return FefuJournalReportRatingPackagePub.class.getSimpleName();
							}
						}
				);

		return columnListExtPointBuilder(HISTORY_DS)
				.addColumn(linkResolverDateTime.formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().permissionKey("menuFefuJournalPackagePub"))
				.addColumn(textColumn("reportRatingResult", FefuSendingRatingPackage.sendingResult()))
				.addColumn(textColumn("externalID", FefuSendingRatingPackage.externalId()))
				.create();
	}

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> historyDSHandler()
	{
		return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
		{
			@Override
			protected DSOutput execute(DSInput input, ExecutionContext context)
			{
				// recive data from filter
				Date requestDateFrom = context.get(FEFU_REQUEST_DATE_FROM);
				Date requestDateTo = context.get(FEFU_REQUEST_DATE_TO);
				List<Long> studentIdList = context.get(FEFU_STUDENT_FIO);
				String externalId = context.get(EXTERNAL_ID);
				String xmlDataSearch = context.get(XML_DATA_SEARCH);

				DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuSendingRatingPackage.class, "pack")
						.column("pack")
						.order(property("pack", (String) input.getEntityOrder().getKey()), input.getEntityOrder().getDirection());

				if (null != requestDateFrom)
				{
					requestDateFrom = CoreDateUtils.getDayFirstTimeMoment(requestDateFrom);
					builder.where(ge(property("pack", FefuSendingRatingPackage.sendingTime()), valueTimestamp(requestDateFrom)));
				}

				if (null != requestDateTo)
				{
					requestDateTo = CoreDateUtils.getNextDayFirstTimeMoment(requestDateTo, 1);
					builder.where(lt(property("pack", FefuSendingRatingPackage.sendingTime()), valueTimestamp(requestDateTo)));
				}

				if (null != externalId)
				{
					builder.where(likeUpper(property("pack", FefuSendingRatingPackage.externalId()), value(CoreStringUtils.escapeLike(externalId))));
				}

				if (null != xmlDataSearch)
				{
					builder.where(like(property("pack", FefuSendingRatingPackage.sendingData()), value(CoreStringUtils.escapeLike(xmlDataSearch))));
				}

				if (null != studentIdList && !studentIdList.isEmpty())
				{
					builder.where(exists(
							new DQLSelectBuilder().fromEntity(FefuRatingPackageStudentRow.class, "row").column("row.id")
									.where(eq(property("row", FefuRatingPackageStudentRow.L_RATING_PACKAGE), property("pack.id")))
									.where(in(property("row", FefuRatingPackageStudentRow.L_STUDENT), studentIdList))
									.buildQuery()
					));
				}

				return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
			}
		};
	}

	@Bean
	public IBusinessHandler<DSInput, DSOutput> fefuStudentListDSHandler()
	{
		return new FefuStudentFIOListDSHandler(getName())
				.order(Student.person().identityCard().fullFio())
				.pageable(true);
	}
}
