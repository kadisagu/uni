/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.EntrantTAListsAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.FefuEcReportManager;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.FefuEntrantTAListsReportEnrDirComboDSHandler;

/**
 * @author nvankov
 * @since 8/7/13
 */
@Configuration
public class FefuEcReportEntrantTAListsAdd extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String ENROLLMENT_CAMPAIGN_STAGE_DS = "enrollmentCampaignStageDS";
    public static final String STUDENT_CATEGORY_DS = "studentCategoryDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_ORG_UNIT_DS = "territorialOrgUnitDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String DEVELOP_CONDITION_DS = "developConditionDS";
    public static final String DEVELOP_TECH_DS = "developTechDS";
    public static final String DEVELOP_PERIOD_DS = "developPeriodDS";
    public static final String QUALIFICATIONS_DS = "qualificationsDS";
    public static final String CONTRACT_CANCELED_DS = "contractCanceledDS";
    public static final String INCLUDE_FOREIGN_DS = "includeForeignPersonDS";
    public static final String EXCLUDE_PARALLEL_DS = "excludeParallelDS";
    public static final String INCLUDE_ENROLLED_DS = "includeEnrolledDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_STAGE_DS, FefuEcReportManager.instance().enrollmentCampaignStageComboDSHandler()))
                .addDataSource(selectDS(STUDENT_CATEGORY_DS, studentCategoryDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(TERRITORIAL_ORG_UNIT_DS, territorialOrgUnitDSHandler()).addColumn(OrgUnit.territorialTitle().s()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, developFormDSHandler()))
                .addDataSource(selectDS(DEVELOP_CONDITION_DS, developConditionDSHandler()))
                .addDataSource(selectDS(DEVELOP_TECH_DS, developTechDSHandler()))
                .addDataSource(selectDS(DEVELOP_PERIOD_DS, developPeriodDSHandler()))
                .addDataSource(selectDS(QUALIFICATIONS_DS, qualificationsDSHandler()))
                .addDataSource(selectDS(CONTRACT_CANCELED_DS, FefuEcReportManager.instance().yesNoComboDSHandler()))
                .addDataSource(selectDS(INCLUDE_FOREIGN_DS, FefuEcReportManager.instance().yesNoComboDSHandler()))
                .addDataSource(selectDS(EXCLUDE_PARALLEL_DS, FefuEcReportManager.instance().yesNoComboDSHandler()))
                .addDataSource(selectDS(INCLUDE_ENROLLED_DS, FefuEcReportManager.instance().yesNoComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler studentCategoryDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), StudentCategory.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler qualificationsDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), Qualifications.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                ep.dqlBuilder.where(DQLExpressions.exists(new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, "edl").where(
                        DQLExpressions.eq(
                                DQLExpressions.property("e"),
                                DQLExpressions.property("edl", EducationLevelsHighSchool.educationLevel().qualification()))).buildQuery()

                ));
            }

            @Override
            protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareOrders(ep);
                ep.dqlBuilder.order(DQLExpressions.property("e", Qualifications.order()));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler()
    {
        return new FefuEntrantTAListsReportEnrDirComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler territorialOrgUnitDSHandler()
    {
        return new FefuEntrantTAListsReportEnrDirComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developFormDSHandler()
    {
        return new FefuEntrantTAListsReportEnrDirComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developConditionDSHandler()
    {
        return new FefuEntrantTAListsReportEnrDirComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developTechDSHandler()
    {
        return new FefuEntrantTAListsReportEnrDirComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developPeriodDSHandler()
    {
        return new FefuEntrantTAListsReportEnrDirComboDSHandler(getName());
    }
}
