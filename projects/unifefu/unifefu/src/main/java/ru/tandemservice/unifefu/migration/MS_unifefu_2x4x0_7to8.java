package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.Date;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x0_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuBrsDelayGradingReport
        if(tool.tableExists("fefubrsdelaygradingreport_t") && !tool.columnExists("fefubrsdelaygradingreport_t", "checkdate_p"))
		// создано обязательное свойство checkDate
		{
			// создать колонку
			tool.createColumn("fefubrsdelaygradingreport_t", new DBColumn("checkdate_p", DBType.DATE));

			// задать значение по умолчанию
			Date defaultCheckDate = new Date(new java.util.Date().getTime());
			tool.executeUpdate("update fefubrsdelaygradingreport_t set checkdate_p=? where checkdate_p is null", defaultCheckDate);

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefubrsdelaygradingreport_t", "checkdate_p", false);

		}


    }
}