/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.logic;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

/**
 * @author DMITRY KNYAZEV
 * @since 26.08.2014
 */
public class FefuSessionListDocumentWrapper extends ViewWrapper<SessionListDocument>
{
	public static final String STUDENT = "student";
	public static final String COMPENSATION_TYPE = "compensation";
	public static final String TERM = "term";

	public FefuSessionListDocumentWrapper(SessionListDocument document, Student student, CompensationType compensationType, EducationYear educationYear, YearDistributionPart yearPart)
	{
		super(document);
		setViewProperty(STUDENT, student.getFio());
		setViewProperty(COMPENSATION_TYPE, compensationType.getShortTitle());
		setViewProperty(TERM, yearPart.getTitle() + " " + educationYear.getTitle());
	}

}
