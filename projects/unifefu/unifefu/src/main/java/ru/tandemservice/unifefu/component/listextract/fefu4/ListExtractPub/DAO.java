/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class DAO extends AbstractListExtractPubDAO<FefuAcadGrantAssignStuEnrolmentListExtract, Model> implements IDAO
{
}