package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x8_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
                new ScriptDependency("org.tandemframework", "1.6.15"),
                new ScriptDependency("org.tandemframework.shared", "1.6.8"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.6.8")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuEduProgramKind2MinLaborRel

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("ffedprgrmknd2mnlbrrl_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("eduprogramkind_id", DBType.LONG).setNullable(false),
				new DBColumn("labor_p", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuEduProgramKind2MinLaborRel");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuRegistryElement2EduProgramKindRel

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("ffrgstryelmnt2edprgrmkndrl_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("registryelement_id", DBType.LONG).setNullable(false),
				new DBColumn("educationorgunit_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuRegistryElement2EduProgramKindRel");
        }
    }
}