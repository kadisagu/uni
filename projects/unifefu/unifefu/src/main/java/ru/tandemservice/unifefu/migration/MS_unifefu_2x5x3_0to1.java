package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.5.3"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.5.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность stuffCompensationStuExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("stuffcompensationstuextract_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("protocoldate_p", DBType.DATE).setNullable(false), 
				new DBColumn("protocolnumber_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("compensationsum_p", DBType.LONG).setNullable(false), 
				new DBColumn("immediatesum_p", DBType.LONG).setNullable(false), 
				new DBColumn("matchingpersonfio_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("responsiblepersonfio_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("matchingperson_id", DBType.LONG).setNullable(false), 
				new DBColumn("responsibleperson_id", DBType.LONG).setNullable(false), 
				new DBColumn("matchingdate_p", DBType.DATE).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("stuffCompensationStuExtract");

		}


    }
}