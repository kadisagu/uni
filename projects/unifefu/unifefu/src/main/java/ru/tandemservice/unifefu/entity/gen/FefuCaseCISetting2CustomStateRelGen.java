package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.unifefu.entity.FefuCaseCISetting2CustomStateRel;
import ru.tandemservice.unifefu.entity.FefuStudentCustomStateCISetting;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь настройки склонений доп. статусов студентов с доп. статусами
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuCaseCISetting2CustomStateRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuCaseCISetting2CustomStateRel";
    public static final String ENTITY_NAME = "fefuCaseCISetting2CustomStateRel";
    public static final int VERSION_HASH = -32647232;
    private static IEntityMeta ENTITY_META;

    public static final String L_CUSTOM_STATE = "customState";
    public static final String L_SETTING = "setting";

    private StudentCustomStateCI _customState;     // Наименование доп. статуса или их комбинаций
    private FefuStudentCustomStateCISetting _setting;     // Настройка печатного текста для дополнительного статуса студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Наименование доп. статуса или их комбинаций. Свойство не может быть null.
     */
    @NotNull
    public StudentCustomStateCI getCustomState()
    {
        return _customState;
    }

    /**
     * @param customState Наименование доп. статуса или их комбинаций. Свойство не может быть null.
     */
    public void setCustomState(StudentCustomStateCI customState)
    {
        dirty(_customState, customState);
        _customState = customState;
    }

    /**
     * @return Настройка печатного текста для дополнительного статуса студента. Свойство не может быть null.
     */
    @NotNull
    public FefuStudentCustomStateCISetting getSetting()
    {
        return _setting;
    }

    /**
     * @param setting Настройка печатного текста для дополнительного статуса студента. Свойство не может быть null.
     */
    public void setSetting(FefuStudentCustomStateCISetting setting)
    {
        dirty(_setting, setting);
        _setting = setting;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuCaseCISetting2CustomStateRelGen)
        {
            setCustomState(((FefuCaseCISetting2CustomStateRel)another).getCustomState());
            setSetting(((FefuCaseCISetting2CustomStateRel)another).getSetting());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuCaseCISetting2CustomStateRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuCaseCISetting2CustomStateRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuCaseCISetting2CustomStateRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "customState":
                    return obj.getCustomState();
                case "setting":
                    return obj.getSetting();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "customState":
                    obj.setCustomState((StudentCustomStateCI) value);
                    return;
                case "setting":
                    obj.setSetting((FefuStudentCustomStateCISetting) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "customState":
                        return true;
                case "setting":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "customState":
                    return true;
                case "setting":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "customState":
                    return StudentCustomStateCI.class;
                case "setting":
                    return FefuStudentCustomStateCISetting.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuCaseCISetting2CustomStateRel> _dslPath = new Path<FefuCaseCISetting2CustomStateRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuCaseCISetting2CustomStateRel");
    }
            

    /**
     * @return Наименование доп. статуса или их комбинаций. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCaseCISetting2CustomStateRel#getCustomState()
     */
    public static StudentCustomStateCI.Path<StudentCustomStateCI> customState()
    {
        return _dslPath.customState();
    }

    /**
     * @return Настройка печатного текста для дополнительного статуса студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCaseCISetting2CustomStateRel#getSetting()
     */
    public static FefuStudentCustomStateCISetting.Path<FefuStudentCustomStateCISetting> setting()
    {
        return _dslPath.setting();
    }

    public static class Path<E extends FefuCaseCISetting2CustomStateRel> extends EntityPath<E>
    {
        private StudentCustomStateCI.Path<StudentCustomStateCI> _customState;
        private FefuStudentCustomStateCISetting.Path<FefuStudentCustomStateCISetting> _setting;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Наименование доп. статуса или их комбинаций. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCaseCISetting2CustomStateRel#getCustomState()
     */
        public StudentCustomStateCI.Path<StudentCustomStateCI> customState()
        {
            if(_customState == null )
                _customState = new StudentCustomStateCI.Path<StudentCustomStateCI>(L_CUSTOM_STATE, this);
            return _customState;
        }

    /**
     * @return Настройка печатного текста для дополнительного статуса студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuCaseCISetting2CustomStateRel#getSetting()
     */
        public FefuStudentCustomStateCISetting.Path<FefuStudentCustomStateCISetting> setting()
        {
            if(_setting == null )
                _setting = new FefuStudentCustomStateCISetting.Path<FefuStudentCustomStateCISetting>(L_SETTING, this);
            return _setting;
        }

        public Class getEntityClass()
        {
            return FefuCaseCISetting2CustomStateRel.class;
        }

        public String getEntityName()
        {
            return "fefuCaseCISetting2CustomStateRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
