/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubController;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class Controller extends AbstractListParagraphPubController<FefuAcadGrantAssignStuEnrolmentListExtract, IDAO, Model>
{
}