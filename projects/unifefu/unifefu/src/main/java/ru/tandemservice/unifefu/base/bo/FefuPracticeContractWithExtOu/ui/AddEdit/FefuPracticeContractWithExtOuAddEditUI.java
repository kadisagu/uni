/* $Id: FefuPracticeContractWithExtOuAddEditUI.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.FefuPracticeContractWithExtOuManager;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/28/13
 * Time: 6:01 PM
 */

@Input
        ({
                @Bind(key = UIPresenter.PUBLISHER_ID, binding = "practicContractId"),
        })
public class FefuPracticeContractWithExtOuAddEditUI extends UIPresenter
{
    private Long _practicContractId;
    private FefuPracticeContractWithExtOu _practicContract;

    public Long getPracticContractId()
    {
        return _practicContractId;
    }

    public void setPracticContractId(Long practicContractId)
    {
        _practicContractId = practicContractId;
    }

    public FefuPracticeContractWithExtOu getPracticContract()
    {
        return _practicContract;
    }

    public void setPracticContract(FefuPracticeContractWithExtOu practicContract)
    {
        _practicContract = practicContract;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _practicContractId)
        {
            _practicContract = new FefuPracticeContractWithExtOu();
        }
        else
        {
            _practicContract = DataAccessServices.dao().get(_practicContractId);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if("jurCtrDS".equals(dataSource.getName()))
        {
            if(null != _practicContract.getExternalOrgUnit())
                dataSource.put("externalOrgUnit", _practicContract.getExternalOrgUnit().getId());
            dataSource.put("showAllWoExtOuId", Boolean.FALSE);
        }
    }

    public boolean getAddForm()
    {
        return null == _practicContractId;
    }

    public boolean getEditForm()
    {
        return !getAddForm();
    }

    public void onClickApply()
    {
        FefuPracticeContractWithExtOuManager.instance().dao().createOrUpdate(_practicContract);
        deactivate();
    }
}
