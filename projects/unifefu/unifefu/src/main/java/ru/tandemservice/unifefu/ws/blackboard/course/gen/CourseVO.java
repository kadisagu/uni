
package ru.tandemservice.unifefu.ws.blackboard.course.gen;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CourseVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CourseVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="allowGuests" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="allowObservers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="available" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="batchUid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buttonStyleBbId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buttonStyleShape" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buttonStyleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cartridgeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="classificationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="courseDuration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="courseId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coursePace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="courseServiceLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataSourceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="decAbsoluteLimit" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="enrollmentAccessCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enrollmentEndDate" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="enrollmentStartDate" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="enrollmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expansionData" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="fee" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="hasDescriptionPage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="institutionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="localeEnforced" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="lockedOut" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="navCollapsable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="navColorBg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="navColorFg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="navigationStyle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numberOfDaysOfUse" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="organization" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="showInCatalog" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="softLimit" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="uploadLimit" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CourseVO", namespace = "http://course.ws.blackboard/xsd", propOrder = {
    "allowGuests",
    "allowObservers",
    "available",
    "batchUid",
    "buttonStyleBbId",
    "buttonStyleShape",
    "buttonStyleType",
    "cartridgeId",
    "classificationId",
    "courseDuration",
    "courseId",
    "coursePace",
    "courseServiceLevel",
    "dataSourceId",
    "decAbsoluteLimit",
    "description",
    "endDate",
    "enrollmentAccessCode",
    "enrollmentEndDate",
    "enrollmentStartDate",
    "enrollmentType",
    "expansionData",
    "fee",
    "hasDescriptionPage",
    "id",
    "institutionName",
    "locale",
    "localeEnforced",
    "lockedOut",
    "name",
    "navCollapsable",
    "navColorBg",
    "navColorFg",
    "navigationStyle",
    "numberOfDaysOfUse",
    "organization",
    "showInCatalog",
    "softLimit",
    "startDate",
    "uploadLimit"
})
public class CourseVO {

    protected Boolean allowGuests;
    protected Boolean allowObservers;
    protected Boolean available;
    @XmlElementRef(name = "batchUid", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> batchUid;
    @XmlElementRef(name = "buttonStyleBbId", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> buttonStyleBbId;
    @XmlElementRef(name = "buttonStyleShape", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> buttonStyleShape;
    @XmlElementRef(name = "buttonStyleType", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> buttonStyleType;
    @XmlElementRef(name = "cartridgeId", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> cartridgeId;
    @XmlElementRef(name = "classificationId", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> classificationId;
    @XmlElementRef(name = "courseDuration", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> courseDuration;
    @XmlElementRef(name = "courseId", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> courseId;
    @XmlElementRef(name = "coursePace", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> coursePace;
    @XmlElementRef(name = "courseServiceLevel", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> courseServiceLevel;
    @XmlElementRef(name = "dataSourceId", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> dataSourceId;
    protected Long decAbsoluteLimit;
    @XmlElementRef(name = "description", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> description;
    protected Long endDate;
    @XmlElementRef(name = "enrollmentAccessCode", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> enrollmentAccessCode;
    protected Long enrollmentEndDate;
    protected Long enrollmentStartDate;
    @XmlElementRef(name = "enrollmentType", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> enrollmentType;
    @XmlElement(nillable = true)
    protected List<String> expansionData;
    protected Float fee;
    protected Boolean hasDescriptionPage;
    @XmlElementRef(name = "id", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> id;
    @XmlElementRef(name = "institutionName", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> institutionName;
    @XmlElementRef(name = "locale", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> locale;
    protected Boolean localeEnforced;
    protected Boolean lockedOut;
    @XmlElementRef(name = "name", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> name;
    protected Boolean navCollapsable;
    @XmlElementRef(name = "navColorBg", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> navColorBg;
    @XmlElementRef(name = "navColorFg", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> navColorFg;
    @XmlElementRef(name = "navigationStyle", namespace = "http://course.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> navigationStyle;
    protected Integer numberOfDaysOfUse;
    protected Boolean organization;
    protected Boolean showInCatalog;
    protected Long softLimit;
    protected Long startDate;
    protected Long uploadLimit;

    /**
     * Gets the value of the allowGuests property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowGuests() {
        return allowGuests;
    }

    /**
     * Sets the value of the allowGuests property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowGuests(Boolean value) {
        this.allowGuests = value;
    }

    /**
     * Gets the value of the allowObservers property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowObservers() {
        return allowObservers;
    }

    /**
     * Sets the value of the allowObservers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowObservers(Boolean value) {
        this.allowObservers = value;
    }

    /**
     * Gets the value of the available property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAvailable() {
        return available;
    }

    /**
     * Sets the value of the available property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAvailable(Boolean value) {
        this.available = value;
    }

    /**
     * Gets the value of the batchUid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBatchUid() {
        return batchUid;
    }

    /**
     * Sets the value of the batchUid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBatchUid(JAXBElement<String> value) {
        this.batchUid = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the buttonStyleBbId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getButtonStyleBbId() {
        return buttonStyleBbId;
    }

    /**
     * Sets the value of the buttonStyleBbId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setButtonStyleBbId(JAXBElement<String> value) {
        this.buttonStyleBbId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the buttonStyleShape property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getButtonStyleShape() {
        return buttonStyleShape;
    }

    /**
     * Sets the value of the buttonStyleShape property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setButtonStyleShape(JAXBElement<String> value) {
        this.buttonStyleShape = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the buttonStyleType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getButtonStyleType() {
        return buttonStyleType;
    }

    /**
     * Sets the value of the buttonStyleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setButtonStyleType(JAXBElement<String> value) {
        this.buttonStyleType = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the cartridgeId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCartridgeId() {
        return cartridgeId;
    }

    /**
     * Sets the value of the cartridgeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCartridgeId(JAXBElement<String> value) {
        this.cartridgeId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the classificationId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClassificationId() {
        return classificationId;
    }

    /**
     * Sets the value of the classificationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClassificationId(JAXBElement<String> value) {
        this.classificationId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the courseDuration property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCourseDuration() {
        return courseDuration;
    }

    /**
     * Sets the value of the courseDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCourseDuration(JAXBElement<String> value) {
        this.courseDuration = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the courseId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCourseId() {
        return courseId;
    }

    /**
     * Sets the value of the courseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCourseId(JAXBElement<String> value) {
        this.courseId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the coursePace property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCoursePace() {
        return coursePace;
    }

    /**
     * Sets the value of the coursePace property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCoursePace(JAXBElement<String> value) {
        this.coursePace = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the courseServiceLevel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCourseServiceLevel() {
        return courseServiceLevel;
    }

    /**
     * Sets the value of the courseServiceLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCourseServiceLevel(JAXBElement<String> value) {
        this.courseServiceLevel = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the dataSourceId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataSourceId() {
        return dataSourceId;
    }

    /**
     * Sets the value of the dataSourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataSourceId(JAXBElement<String> value) {
        this.dataSourceId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the decAbsoluteLimit property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDecAbsoluteLimit() {
        return decAbsoluteLimit;
    }

    /**
     * Sets the value of the decAbsoluteLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDecAbsoluteLimit(Long value) {
        this.decAbsoluteLimit = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEndDate(Long value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the enrollmentAccessCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEnrollmentAccessCode() {
        return enrollmentAccessCode;
    }

    /**
     * Sets the value of the enrollmentAccessCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEnrollmentAccessCode(JAXBElement<String> value) {
        this.enrollmentAccessCode = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the enrollmentEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEnrollmentEndDate() {
        return enrollmentEndDate;
    }

    /**
     * Sets the value of the enrollmentEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEnrollmentEndDate(Long value) {
        this.enrollmentEndDate = value;
    }

    /**
     * Gets the value of the enrollmentStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEnrollmentStartDate() {
        return enrollmentStartDate;
    }

    /**
     * Sets the value of the enrollmentStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEnrollmentStartDate(Long value) {
        this.enrollmentStartDate = value;
    }

    /**
     * Gets the value of the enrollmentType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEnrollmentType() {
        return enrollmentType;
    }

    /**
     * Sets the value of the enrollmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEnrollmentType(JAXBElement<String> value) {
        this.enrollmentType = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the expansionData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expansionData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpansionData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getExpansionData() {
        if (expansionData == null) {
            expansionData = new ArrayList<>();
        }
        return this.expansionData;
    }

    /**
     * Gets the value of the fee property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getFee() {
        return fee;
    }

    /**
     * Sets the value of the fee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setFee(Float value) {
        this.fee = value;
    }

    /**
     * Gets the value of the hasDescriptionPage property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasDescriptionPage() {
        return hasDescriptionPage;
    }

    /**
     * Sets the value of the hasDescriptionPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasDescriptionPage(Boolean value) {
        this.hasDescriptionPage = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setId(JAXBElement<String> value) {
        this.id = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the institutionName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInstitutionName() {
        return institutionName;
    }

    /**
     * Sets the value of the institutionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInstitutionName(JAXBElement<String> value) {
        this.institutionName = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locale property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocale() {
        return locale;
    }

    /**
     * Sets the value of the locale property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocale(JAXBElement<String> value) {
        this.locale = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the localeEnforced property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLocaleEnforced() {
        return localeEnforced;
    }

    /**
     * Sets the value of the localeEnforced property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLocaleEnforced(Boolean value) {
        this.localeEnforced = value;
    }

    /**
     * Gets the value of the lockedOut property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLockedOut() {
        return lockedOut;
    }

    /**
     * Sets the value of the lockedOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLockedOut(Boolean value) {
        this.lockedOut = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the navCollapsable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNavCollapsable() {
        return navCollapsable;
    }

    /**
     * Sets the value of the navCollapsable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNavCollapsable(Boolean value) {
        this.navCollapsable = value;
    }

    /**
     * Gets the value of the navColorBg property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNavColorBg() {
        return navColorBg;
    }

    /**
     * Sets the value of the navColorBg property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNavColorBg(JAXBElement<String> value) {
        this.navColorBg = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the navColorFg property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNavColorFg() {
        return navColorFg;
    }

    /**
     * Sets the value of the navColorFg property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNavColorFg(JAXBElement<String> value) {
        this.navColorFg = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the navigationStyle property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNavigationStyle() {
        return navigationStyle;
    }

    /**
     * Sets the value of the navigationStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNavigationStyle(JAXBElement<String> value) {
        this.navigationStyle = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the numberOfDaysOfUse property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfDaysOfUse() {
        return numberOfDaysOfUse;
    }

    /**
     * Sets the value of the numberOfDaysOfUse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfDaysOfUse(Integer value) {
        this.numberOfDaysOfUse = value;
    }

    /**
     * Gets the value of the organization property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOrganization() {
        return organization;
    }

    /**
     * Sets the value of the organization property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOrganization(Boolean value) {
        this.organization = value;
    }

    /**
     * Gets the value of the showInCatalog property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowInCatalog() {
        return showInCatalog;
    }

    /**
     * Sets the value of the showInCatalog property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowInCatalog(Boolean value) {
        this.showInCatalog = value;
    }

    /**
     * Gets the value of the softLimit property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSoftLimit() {
        return softLimit;
    }

    /**
     * Sets the value of the softLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSoftLimit(Long value) {
        this.softLimit = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setStartDate(Long value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the uploadLimit property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getUploadLimit() {
        return uploadLimit;
    }

    /**
     * Sets the value of the uploadLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setUploadLimit(Long value) {
        this.uploadLimit = value;
    }

}
