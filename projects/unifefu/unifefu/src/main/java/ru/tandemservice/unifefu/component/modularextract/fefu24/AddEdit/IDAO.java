/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu24.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract;

/**
 * @author Andrey Andreev
 * @since 12.01.2016
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuAdmittedToGIAExtract, Model>
{
}