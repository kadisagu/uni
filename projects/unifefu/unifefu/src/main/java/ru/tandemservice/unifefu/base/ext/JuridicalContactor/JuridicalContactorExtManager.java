/* $Id$ */
package ru.tandemservice.unifefu.base.ext.JuridicalContactor;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author nvankov
 * @since 8/15/13
 */
@Configuration
public class JuridicalContactorExtManager extends BusinessObjectExtensionManager
{
}
