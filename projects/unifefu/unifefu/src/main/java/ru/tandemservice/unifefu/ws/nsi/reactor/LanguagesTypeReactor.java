/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.LanguagesType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class LanguagesTypeReactor extends SimpleCatalogEntityNewReactor<LanguagesType, ForeignLanguage>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<ForeignLanguage> getEntityClass()
    {
        return ForeignLanguage.class;
    }

    @Override
    public Class<LanguagesType> getNSIEntityClass()
    {
        return LanguagesType.class;
    }

    @Override
    public LanguagesType getCatalogElementRetrieveRequestObject(String guid)
    {
        LanguagesType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createLanguagesType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public LanguagesType createEntity(CoreCollectionUtils.Pair<ForeignLanguage, FefuNsiIds> entityPair)
    {
        LanguagesType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createLanguagesType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setLanguageID(getUserCodeForNSIChecked(entityPair.getX().getUserCode()));
        nsiEntity.setLanguageName(entityPair.getX().getTitle());
        return nsiEntity;
    }

    @Override
    public ForeignLanguage createEntity(LanguagesType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getLanguageName()) return null;

        ForeignLanguage entity = new ForeignLanguage();
        entity.setTitle(nsiEntity.getLanguageName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(ForeignLanguage.class));
        entity.setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getLanguageID()));
        return entity;
    }

    @Override
    public ForeignLanguage updatePossibleDuplicateFields(LanguagesType nsiEntity, CoreCollectionUtils.Pair<ForeignLanguage, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), ForeignLanguage.title().s()))
                entityPair.getX().setTitle(nsiEntity.getLanguageName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), ForeignLanguage.userCode().s()))
                entityPair.getX().setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getLanguageID()));
        }
        return entityPair.getX();
    }

    @Override
    public LanguagesType updateNsiEntityFields(LanguagesType nsiEntity, ForeignLanguage entity)
    {
        nsiEntity.setLanguageName(entity.getTitle());
        nsiEntity.setLanguageID(getUserCodeForNSIChecked(entity.getUserCode()));
        return nsiEntity;
    }
}