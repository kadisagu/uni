package ru.tandemservice.unifefu.component.modularextract.fefu1;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuChangeDateStateExaminationStuExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/17/12
 * Time: 4:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class FefuChangeDateStateExaminationStuExtractDao extends UniBaseDao implements IExtractComponentDao<FefuChangeDateStateExaminationStuExtract>
{
    public void doCommit(FefuChangeDateStateExaminationStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);
    }

    public void doRollback(FefuChangeDateStateExaminationStuExtract extract, Map parameters)
    {
    }
}
