package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Строка истории автопроведения приказов
 */
public class FefuOrderAutoCommitLogRow extends FefuOrderAutoCommitLogRowGen
{
    public String getTitle()
    {
        return "Строка истории синхронизации с СЭД Directum";
    }
}