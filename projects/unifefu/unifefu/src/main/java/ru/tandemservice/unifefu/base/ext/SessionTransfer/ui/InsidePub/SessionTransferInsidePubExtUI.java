/* $Id $ */
package ru.tandemservice.unifefu.base.ext.SessionTransfer.ui.InsidePub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsidePub.SessionTransferInsidePubUI;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideDocument;

/**
 * @author Rostuncev Savva
 * @since 20.03.2014
 */

public class SessionTransferInsidePubExtUI extends UIAddon
{
    public SessionTransferInsidePubExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onPrintStudentsReport()
    {
        String COMPONENT_NAME = "ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTabPrintRtf";
        SessionTransferInsideDocument presenter = ((SessionTransferInsidePubUI) getPresenter()).getDocument();
        getActivationBuilder().asRegion(COMPONENT_NAME)
                .parameter("sessionTransferDocumentId", presenter.getId())
                .activate();
    }
}
