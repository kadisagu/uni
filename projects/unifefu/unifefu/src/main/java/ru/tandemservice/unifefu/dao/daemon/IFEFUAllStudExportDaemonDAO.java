package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 08.02.14
 * Time: 16:22
 * To change this template use File | Settings | File Templates.
 */
public interface IFEFUAllStudExportDaemonDAO {

    String GLOBAL_DAEMON_LOCK = IFEFUAllStudExportDaemonDAO.class.getName() + ".global-lock";
    SpringBeanCache<IFEFUAllStudExportDaemonDAO> instance = new SpringBeanCache<>(IFEFUAllStudExportDaemonDAO.class.getName());

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doExportStudents();

    @Transactional(readOnly = false)
    void doDeleteAllEntities();

    Collection<Long> getPersonIds(Boolean withBatch);

    @Transactional(readOnly = false)
    void export_Person(final Collection<Long> ids);

    void doExportChildEntities();

    void doRegisterEntity(String entityName, Collection<Long> entityIds);

}
