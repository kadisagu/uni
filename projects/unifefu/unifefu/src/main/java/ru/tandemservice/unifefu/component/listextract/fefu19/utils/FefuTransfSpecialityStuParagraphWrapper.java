/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19.utils;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 18.05.2015
 */
public class FefuTransfSpecialityStuParagraphWrapper implements Comparable<FefuTransfSpecialityStuParagraphWrapper>
{
    private final Course _course;
    private final Group _group;
    private final Group _groupNew;
    private final CompensationType _compensationType;
    private final EducationLevels _educationLevelsOld;
    private final EducationLevels _parentEduLevelNew;
    private final OrgUnit _formativeOrgUnit;
    private final OrgUnit _territorialOrgUnit;
    private final DevelopForm _developForm;
    private final DevelopCondition _developCondition;
    private final DevelopTech _developTech;
    private final DevelopPeriod _developPeriod;
    private final Qualifications _qualification;
    private final StudentCategory _studentCategory;
    private final Long _baseEdu;


    private final ListStudentExtract _firstExtract;

    public FefuTransfSpecialityStuParagraphWrapper(Course course, Group group, Group groupNew, CompensationType compensationType, EducationLevelsHighSchool educationLevels,
                                                   EducationLevels parentEduLevelNew, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit, DevelopForm developForm,
                                                   DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod, Qualifications qualification,
                                                   ListStudentExtract firstExtract, Long baseEdu)
    {
        _course = course;
        _group = group;
        _groupNew = groupNew;
        _compensationType = compensationType;
        _educationLevelsOld = EducationOrgUnitUtil.getParentLevel(educationLevels);
        _parentEduLevelNew = parentEduLevelNew;
        _formativeOrgUnit = formativeOrgUnit;
        _territorialOrgUnit = territorialOrgUnit;
        _developForm = developForm;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
        _qualification = qualification;
        _firstExtract = firstExtract;
        _studentCategory = firstExtract.getEntity().getStudentCategory();
        _baseEdu = baseEdu;
    }

    private List<FefuTransfSpecialityStuParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<FefuTransfSpecialityStuParagraphPartWrapper>();

    public Course getCourse()
    {
        return _course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public EducationLevels getEducationLevelsOld()
    {
        return _educationLevelsOld;
    }

    public EducationLevels getParentEduLevelNew()
    {
        return _parentEduLevelNew;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public Qualifications getQualification()
    {
        return _qualification;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public List<FefuTransfSpecialityStuParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public Long getBaseEdu()
    {
        return _baseEdu;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuTransfSpecialityStuParagraphWrapper))
            return false;

        FefuTransfSpecialityStuParagraphWrapper that = (FefuTransfSpecialityStuParagraphWrapper) o;

        boolean result =  _studentCategory.equals(that.getStudentCategory())
                && _course.equals(that.getCourse())
                && _group.equals(that.getGroup())
                && _groupNew.equals(that.getGroupNew())
                && _compensationType.equals(that.getCompensationType())
                && _educationLevelsOld.equals(that.getEducationLevelsOld())
                && _parentEduLevelNew.equals(that.getParentEduLevelNew())
                && _formativeOrgUnit.equals(that.getFormativeOrgUnit())
                && _territorialOrgUnit.equals(that.getTerritorialOrgUnit())
                && _developForm.equals(that.getDevelopForm())
                && _developCondition.equals(that.getDevelopCondition())
                && _developTech.equals(that.getDevelopTech())
                && _developPeriod.equals(that.getDevelopPeriod());
       return result && ((_baseEdu != null && that.getBaseEdu() != null && _baseEdu.equals(that.getBaseEdu())) || (_baseEdu == null && that.getBaseEdu() == null));
    }

    @Override
    public int hashCode()
    {
        return _studentCategory.hashCode() & _course.hashCode() & _group.hashCode() & _groupNew.hashCode() & _compensationType.hashCode() & _educationLevelsOld.hashCode()& _parentEduLevelNew.hashCode()
                & _formativeOrgUnit.hashCode() & _territorialOrgUnit.hashCode() & _developForm.hashCode() & _developCondition.hashCode() & _developTech.hashCode()
                & _developPeriod.hashCode();
    }

    @Override
    public int compareTo(FefuTransfSpecialityStuParagraphWrapper o)
    {
       int result =  _studentCategory.getCode().compareTo(o.getStudentCategory().getCode());

        if (result == 0)
            result = _compensationType.getCode().compareTo(o.getCompensationType().getCode());

        if (result == 0)
        {
            if (_course.getIntValue() > o.getCourse().getIntValue()) return 1;
            else if (_course.getIntValue() < o.getCourse().getIntValue()) return -1;
        }

        if (result == 0)
            result = _group.getTitle().compareTo(o.getGroup().getTitle());


        if (result != 0)
            return result;

        StructureEducationLevels thisLevels = _educationLevelsOld.getLevelType();
        StructureEducationLevels thatLevels = o.getEducationLevelsOld().getLevelType();

        if (thisLevels == null || thatLevels == null)
            return thisLevels == null ? (thatLevels == null ? 0 : 1) : -1;

        boolean isThisChild = thisLevels.isSpecialty() || thisLevels.isProfile();
        boolean isThatChild = thatLevels.isSpecialty() || thatLevels.isProfile();

        if (isThisChild != isThatChild)
            return isThisChild ? 1 : -1;

        result = _educationLevelsOld.getTitle().compareTo(o.getEducationLevelsOld().getTitle());

        if (result == 0)
            result = _groupNew.getTitle().compareTo(o.getGroupNew().getTitle());

        if (result !=0) return result;

        StructureEducationLevels thisLevelsNew = _parentEduLevelNew.getLevelType();
        StructureEducationLevels thatLevelsNew = o.getParentEduLevelNew().getLevelType();

        if (thisLevelsNew == null || thatLevelsNew == null)
            return thisLevelsNew == null ? (thatLevelsNew == null ? 0 : 1) : -1;

        boolean isThisChildNew = thisLevelsNew.isSpecialty() || thisLevelsNew.isProfile();
        boolean isThatChildNew = thatLevelsNew.isSpecialty() || thatLevelsNew.isProfile();

        if (isThisChildNew != isThatChildNew)
            return isThisChildNew ? 1 : -1;

        result = _parentEduLevelNew.getTitle().compareTo(o.getParentEduLevelNew().getTitle());

        String formativeOrgUnitStr1 = null != _formativeOrgUnit.getNominativeCaseTitle() ? _formativeOrgUnit.getNominativeCaseTitle() : _formativeOrgUnit.getFullTitle();
        String formativeOrgUnitStr2 = null != o.getFormativeOrgUnit().getNominativeCaseTitle() ? o.getFormativeOrgUnit().getNominativeCaseTitle() : o.getFormativeOrgUnit().getFullTitle();

        if (result == 0)
            result = formativeOrgUnitStr1.compareTo(formativeOrgUnitStr2);

        String territorialOrgUnitStr1 = null != _territorialOrgUnit.getNominativeCaseTitle() ? _territorialOrgUnit.getNominativeCaseTitle() : _territorialOrgUnit.getFullTitle();
        String territorialOrgUnitStr2 = null != o.getTerritorialOrgUnit().getNominativeCaseTitle() ? o.getTerritorialOrgUnit().getNominativeCaseTitle() : o.getTerritorialOrgUnit().getFullTitle();

        if (result == 0)
            result = territorialOrgUnitStr1.compareTo(territorialOrgUnitStr2);

        if (result == 0)
            result = _developForm.getCode().compareTo(o.getDevelopForm().getCode());

        if (result == 0)
            result = _developCondition.getCode().compareTo(o.getDevelopCondition().getCode());

        if (result == 0)
            result = _developTech.getCode().compareTo(o.getDevelopTech().getCode());

        if (result == 0)
            result = _developPeriod.getCode().compareTo(o.getDevelopPeriod().getCode());

        return result;
    }
}