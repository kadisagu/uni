/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.Test1C;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.BaseHibernateSupportedHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectFragmentBuilder;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.entity.Fefu_1C_log;
import ru.tandemservice.unifefu.entity.Fefu_1C_pack;
import ru.tandemservice.unifefu.ws.c1.src.OrderMap_1C;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 13.12.2013
 */
@Configuration
public class FefuSystemActionTest1C extends BusinessComponentManager
{
    public static final String EXTRACT_DS = "extractDS";

    public static final String CHECKBOX_COLUMN = "checkbox";
    private static final String CREATE_DATE_PROP = "orderCreateDate";
    private static final String FIO_PROP = "title";
    private static final String GROUP_PROP = "group";
    private static final String ORDER_NUMBER_PROP = "orderNumber";
    private static final String ORDER_TYPE_PROP = "orderType";
    private static final String ORDER_ID_PROP = "orderId";
    public static final String SYNC_PROP = "syncProp";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EXTRACT_DS, getExtractDS(), extractDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getExtractDS()
    {
        return columnListExtPointBuilder(EXTRACT_DS)
                .addColumn(checkboxColumn(CHECKBOX_COLUMN))
                .addColumn(publisherColumn(CREATE_DATE_PROP, CREATE_DATE_PROP).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).primaryKeyPath(ORDER_ID_PROP))
                .addColumn(textColumn(FIO_PROP, FIO_PROP))
                .addColumn(textColumn(GROUP_PROP, GROUP_PROP))
                .addColumn(textColumn(ORDER_NUMBER_PROP, ORDER_NUMBER_PROP))
                .addColumn(textColumn(ORDER_TYPE_PROP, ORDER_TYPE_PROP))
                .addColumn(dateColumn(SYNC_PROP, SYNC_PROP).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> extractDSHandler()
    {
        return new BaseHibernateSupportedHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput dsInput, ExecutionContext context)
            {
                DQLSelectBuilder entrants = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e")
                        /* 0 */.column(property("e.id"), "id")
                        /* 1 */.column(property("o", IAbstractOrder.P_CREATE_DATE), "createDate")
                        /* 2 */.column(property("e", EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullFio()), "fio")
                        /* 3 */.column(value(""), "groupTitle")
                        /* 4 */.column(property("o", IAbstractOrder.P_NUMBER), "orderNumber")
                        /* 5 */.column(property("o", EnrollmentOrder.type().title()), "orderType")
                        /* 6 */.column(property("o.id"), "orderId")
                        .joinPath(DQLJoinType.inner, EnrollmentExtract.paragraph().fromAlias("e"), "p")
                        .joinEntity("e", DQLJoinType.inner, EnrollmentOrder.class, "o", eq("o.id", "p.order"))
                        .where(eq(property("o", EnrollmentOrder.state().code()), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)));

                DQLSelectFragmentBuilder students = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                        /* 0 */.column("e.id")
                        /* 1 */.column(property("o", IAbstractOrder.P_CREATE_DATE))
                        /* 2 */.column(property("e", AbstractStudentExtract.entity().person().identityCard().fullFio()))
                        /* 3 */.column(property("g", Group.P_TITLE))
                        /* 4 */.column(property("o", IAbstractOrder.P_NUMBER))
                        /* 5 */.column(property("e", AbstractStudentExtract.type().title()))
                        /* 6 */.column(property("o.id"))
                        .joinPath(DQLJoinType.left, AbstractStudentExtract.entity().group().fromAlias("e"), "g")
                        .joinPath(DQLJoinType.inner, AbstractStudentExtract.paragraph().order().fromAlias("e"), "o")
                        .where(notIn(property("e.id"), new DQLSelectBuilder().fromEntity(OtherStudentExtract.class, "other").column("other.id").buildQuery()))
                        .where(eq(property("o", AbstractStudentOrder.state().code()), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
                        .where(in(property("e", AbstractStudentExtract.type().code()), OrderMap_1C.ORDER_CODES_MAP.keySet()));

                String fioFilter = context.get("fio");
                if (StringUtils.isNotEmpty(fioFilter))
                {
                    fioFilter = CoreStringUtils.escapeLike(fioFilter, true);
                    students.where(likeUpper(property("e", AbstractStudentExtract.entity().person().identityCard().fullFio()), value(fioFilter)));
                    entrants.where(likeUpper(property("e", EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullFio()), value(fioFilter)));
                }

                String orderNumberFilter = context.get("orderNumber");
                if (StringUtils.isNotEmpty(orderNumberFilter))
                {
                    orderNumberFilter = CoreStringUtils.escapeLike(orderNumberFilter, true);
                    students.where(likeUpper(property("o", IAbstractOrder.P_NUMBER), value(orderNumberFilter)));
                    entrants.where(likeUpper(property("o", IAbstractOrder.P_NUMBER), value(orderNumberFilter)));
                }

                entrants.unionAll(students.buildSelectRule());

                DQLSelectBuilder builder = new DQLSelectBuilder().fromDataSource(entrants.buildQuery(), "ds")
                        .column(property("ds.id"))
                        .column(property("ds.createDate"))
                        .column(property("ds.fio"))
                        .column(property("ds.groupTitle"))
                        .column(property("ds.orderNumber"))
                        .column(property("ds.orderType"))
                        .column(property("ds.orderId"))
                        .column(new DQLSelectBuilder()
                                        .fromEntity(Fefu_1C_log.class, "log")
                                        .joinPath(DQLJoinType.left, Fefu_1C_log.pack().fromAlias("log"), "pack")
                                        .where(eq(property("ds.id"), property("log", Fefu_1C_log.extract().id())))
                                        .where(isNotNull(property("pack.id")))
                                        .where(eq(property("pack", Fefu_1C_pack.success()), value(Boolean.TRUE)))
                                        .top(1)
                                        .column(property("pack", Fefu_1C_pack.dispatchDate()))
                                        .buildQuery())

                        .order(property("ds.fio"))
                        .order(property("ds.orderNumber"));


                DSOutput output = DQLSelectOutputBuilder.get(dsInput, builder, context.getSession()).pageable(true).build();

                List<Object> list = new ArrayList<>(output.getCountRecord());
                for (Object[] item : output.<Object[]>getRecordList())
                {
                    DataWrapper wrapper = new DataWrapper((Long)item[0], (String) item[2]);
                    wrapper.setProperty(CREATE_DATE_PROP, item[1]);
                    wrapper.setProperty(GROUP_PROP, item[3]);
                    wrapper.setProperty(ORDER_NUMBER_PROP, item[4]);
                    wrapper.setProperty(ORDER_TYPE_PROP, item[5]);
                    wrapper.setProperty(ORDER_ID_PROP, item[6]);
                    wrapper.setProperty(SYNC_PROP, item[7]);
                    list.add(wrapper);
                }
                output.setRecordList(list);

                return output;
            }
        };
    }

}