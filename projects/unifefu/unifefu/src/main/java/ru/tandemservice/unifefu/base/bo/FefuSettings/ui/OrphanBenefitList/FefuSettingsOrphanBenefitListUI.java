/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrphanBenefitList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import ru.tandemservice.unifefu.entity.ws.FefuOrphanSettings;
import ru.tandemservice.unifefu.entity.ws.gen.FefuOrphanSettingsGen;

/**
 * @author Dmitry Seleznev
 * @since 14.04.2014
 */
public class FefuSettingsOrphanBenefitListUI extends UIPresenter
{
    public void onClickChangeOrphanBenefit()
    {
        Benefit benefit = DataAccessServices.dao().getNotNull(Benefit.class, getListenerParameterAsLong());
        FefuOrphanSettings settings = DataAccessServices.dao().getByNaturalId(new FefuOrphanSettingsGen.NaturalId(benefit));
        if (null == settings)
        {
            settings = new FefuOrphanSettings();
            settings.setBenefit(benefit);
            settings.setOrphanBenefit(true);
        } else
        {
            settings.setOrphanBenefit(!settings.isOrphanBenefit());
        }
        DataAccessServices.dao().saveOrUpdate(settings);
    }
}