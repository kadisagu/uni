package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IAssignPaymentExtract;
import ru.tandemservice.unifefu.entity.gen.FefuSocGrantResumptionStuExtractGen;
import ru.tandemservice.unimove.IAbstractExtract;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О возобновлении выплаты социальной стипендии
 */
public class FefuSocGrantResumptionStuExtract extends FefuSocGrantResumptionStuExtractGen implements IAssignPaymentExtract
{
    @Override
    public Date getBeginDate()
    {
        return getPayResumeDate();
    }

    @Override
    public BigDecimal getPaymentAmount()
    {
        IAbstractExtract socGrantExtract = getSocGrantExtract();
        if (socGrantExtract != null && (socGrantExtract instanceof IAssignPaymentExtract))
            return ((IAssignPaymentExtract) socGrantExtract).getPaymentAmount();

        return null;
    }

    @Override
    public Date getPaymentBeginDate()
    {
        return getPayResumeDate();
    }
}