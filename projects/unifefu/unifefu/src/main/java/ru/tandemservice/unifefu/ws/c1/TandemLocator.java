/**
 * TandemLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.c1;

public class TandemLocator extends org.apache.axis.client.Service implements ru.tandemservice.unifefu.ws.c1.Tandem {

    public TandemLocator() {
    }


    public TandemLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public TandemLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for TandemSoap

    /**

     */
    private java.lang.String TandemSoap_address = "http://92.50.226.242:27080/DVFU_stud/ws/DVFU_Tandem.1cws";

    public java.lang.String getTandemSoapAddress() {
        return TandemSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String TandemSoapWSDDServiceName = "TandemSoap";

    public java.lang.String getTandemSoapWSDDServiceName() {
        return TandemSoapWSDDServiceName;
    }

    public void setTandemSoapWSDDServiceName(java.lang.String name) {
        TandemSoapWSDDServiceName = name;
    }

    public ru.tandemservice.unifefu.ws.c1.TandemPortType getTandemSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(TandemSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getTandemSoap(endpoint);
    }

    public ru.tandemservice.unifefu.ws.c1.TandemPortType getTandemSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ru.tandemservice.unifefu.ws.c1.TandemSoapBindingStub _stub = new ru.tandemservice.unifefu.ws.c1.TandemSoapBindingStub(portAddress, this);
            _stub.setPortName(getTandemSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setTandemSoapEndpointAddress(java.lang.String address) {
        TandemSoap_address = address;
    }


    // Use to get a proxy class for TandemSoap12
    private java.lang.String TandemSoap12_address = "http://92.50.226.242:27080/DVFU_stud/ws/DVFU_Tandem.1cws";

    public java.lang.String getTandemSoap12Address() {
        return TandemSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String TandemSoap12WSDDServiceName = "TandemSoap12";

    public java.lang.String getTandemSoap12WSDDServiceName() {
        return TandemSoap12WSDDServiceName;
    }

    public void setTandemSoap12WSDDServiceName(java.lang.String name) {
        TandemSoap12WSDDServiceName = name;
    }

    public ru.tandemservice.unifefu.ws.c1.TandemPortType getTandemSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(TandemSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getTandemSoap12(endpoint);
    }

    public ru.tandemservice.unifefu.ws.c1.TandemPortType getTandemSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ru.tandemservice.unifefu.ws.c1.TandemSoap12BindingStub _stub = new ru.tandemservice.unifefu.ws.c1.TandemSoap12BindingStub(portAddress, this);
            _stub.setPortName(getTandemSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setTandemSoap12EndpointAddress(java.lang.String address) {
        TandemSoap12_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ru.tandemservice.unifefu.ws.c1.TandemPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                ru.tandemservice.unifefu.ws.c1.TandemSoapBindingStub _stub = new ru.tandemservice.unifefu.ws.c1.TandemSoapBindingStub(new java.net.URL(TandemSoap_address), this);
                _stub.setPortName(getTandemSoapWSDDServiceName());
                return _stub;
            }
            if (ru.tandemservice.unifefu.ws.c1.TandemPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                ru.tandemservice.unifefu.ws.c1.TandemSoap12BindingStub _stub = new ru.tandemservice.unifefu.ws.c1.TandemSoap12BindingStub(new java.net.URL(TandemSoap12_address), this);
                _stub.setPortName(getTandemSoap12WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("TandemSoap".equals(inputPortName)) {
            return getTandemSoap();
        }
        else if ("TandemSoap12".equals(inputPortName)) {
            return getTandemSoap12();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "Tandem");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "TandemSoap"));
            ports.add(new javax.xml.namespace.QName("http://www.DVFU_Tandem.org", "TandemSoap12"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("TandemSoap".equals(portName)) {
            setTandemSoapEndpointAddress(address);
        }
        else 
if ("TandemSoap12".equals(portName)) {
            setTandemSoap12EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
