/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unifefu.ws.nsi.datagram.EmployeeType;

/**
 * @author Dmitry Seleznev
 * @since 02.07.2014
 */
public class EmployeeUtil extends SimpleNsiCatalogUtil<EmployeeType, EmployeePost>
{
    @Override
    public long getNsiCodeHash(EmployeeType nsiEntity)
    {
        return 0;
    }

    @Override
    public long getNsiTitleHash(EmployeeType nsiEntity, boolean caseInsensitive)
    {
        return 0;
    }

    @Override
    public long getCodeHash(EmployeePost entity)
    {
        return 0;
    }

    @Override
    public long getTitleHash(EmployeePost entity, boolean caseInsensitive)
    {
        return 0;
    }
}