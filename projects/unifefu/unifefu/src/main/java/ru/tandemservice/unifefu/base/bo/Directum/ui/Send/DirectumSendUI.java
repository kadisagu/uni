/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.Send;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.commonbase.utils.ooffice.OpenOfficeServiceUtil;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.movestudent.component.customorder.ICustomPrintFormContainer;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaSuccessWithDismissStuListExtract;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuDirectumOrderTypeCodes;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings;
import ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension;
import ru.tandemservice.unifefu.entity.ws.FefuOrphanSettings;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 15.07.2013
 */
@Input({
               @Bind(key = "orderId", binding = "orderId", required = true)
       })
public class DirectumSendUI extends UIPresenter
{
    private Long _orderId;
    private AbstractStudentOrder _order;
    private AbstractStudentExtract _firstExtract;

    private DQLFullCheckSelectModel _orderTypeModel;
    private FefuDirectumOrderType _orderType;

    private FefuDirectumSettings _directumSettings;
    private FefuOrgUnitDirectumExtension _orgUnitExt;

    private OrgUnit _orgUnit;
    private DataWrapper _signer;
    private String _routeCode;
    private String _orgUnitTitle;
    private String _orgUnitCode;
    private String _documentType;

    private boolean _dismiss;
    private boolean _restore;
    private boolean _offBudgetPayments;
    private boolean _foreignStudent;
    private boolean _grantMatAid;
    private boolean _social;
    private boolean _practice;
    private boolean _groupManager;
    private boolean _uvcStudents;
    private boolean _fvoStudents;
    private boolean _penalty;
    private boolean _magister;
    private boolean _payments;
    private boolean _stateFormular;
    private boolean _checkMedicalDocs;
    private boolean _diplomaSuccess;

    @Override
    public void onComponentRefresh()
    {
        _order = DataAccessServices.dao().getNotNull(AbstractStudentOrder.class, _orderId);

        if (_order.getExtractCount() == 0)
            throw new ApplicationException("Нельзя отправить на согласование пустой приказ.");

        _orderTypeModel = new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                return new DQLSelectBuilder().fromEntity(FefuDirectumOrderType.class, alias)
                        .where(ne(property(FefuDirectumOrderType.code().fromAlias(alias)), value(FefuDirectumOrderTypeCodes.ENR)));
            }
        };

        _firstExtract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(_orderId);
        StudentFefuExt student = DataAccessServices.dao().getByNaturalId(new StudentFefuExt.NaturalId(_firstExtract.getEntity()));
        EducationOrgUnit educationOrgUnit = getSpecialEducationOrgUnitForRoute(_firstExtract);
        if (educationOrgUnit == null)
            educationOrgUnit = _firstExtract.getEntity().getEducationOrgUnit();
        StructureEducationLevels struct = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getLevelType();

        _orderType = DataAccessServices.dao().getByCode(FefuDirectumOrderType.class, FefuDirectumOrderTypeCodes.VPO);
        if (struct.isMiddleGos2() || struct.isMiddleGos3())
            _orderType = DataAccessServices.dao().getByCode(FefuDirectumOrderType.class, FefuDirectumOrderTypeCodes.SPO);
        else if (struct.isAdditional())
            _orderType = DataAccessServices.dao().getByCode(FefuDirectumOrderType.class, FefuDirectumOrderTypeCodes.DPO);

        initializeSettings();

        // Off Budget Payments calculating (no one knows how calculate it)
        _offBudgetPayments = false;

        // Foreign students calculating
        DQLSelectBuilder foreignStudentsCount = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .where(eq(property("e", AbstractStudentExtract.paragraph().order()), value(_order)))
                .where(ne(property("e", AbstractStudentExtract.entity().person().identityCard().citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
        _foreignStudent = ISharedBaseDao.instance.get().existsEntity(foreignStudentsCount.buildQuery());

        DQLSelectBuilder orphanStudentsCount = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .joinPath(DQLJoinType.inner, AbstractStudentExtract.entity().fromAlias("e"), "s")
                .joinEntity("s", DQLJoinType.inner, PersonBenefit.class, "pb", eq(property(Student.person().fromAlias("s")), property(PersonBenefit.person().id().fromAlias("pb"))))
                .joinEntity("pb", DQLJoinType.inner, FefuOrphanSettings.class, "os", eq(property(PersonBenefit.benefit().id().fromAlias("pb")), property(FefuOrphanSettings.benefit().id().fromAlias("os"))))
                .where(eq(property("e", AbstractStudentExtract.paragraph().order()), value(_order)))
                .where(eq(property("os", FefuOrphanSettings.orphanBenefit()), value(Boolean.TRUE)));
        _social = ISharedBaseDao.instance.get().existsEntity(orphanStudentsCount.buildQuery());

        _uvcStudents = null != student && student.isUvcStudent();
        _fvoStudents = null != student && student.isFvoStudent();

        _diplomaSuccess = false;
        List<AbstractStudentExtract> extractList = DataAccessServices.dao().getList(AbstractStudentExtract.class, AbstractStudentExtract.paragraph().order(), _order);

        for (AbstractStudentExtract extract : extractList)
        {
            if (_diplomaSuccess) continue;

            // Для всех приказов типа «О присвоении квалификации» автоматически проставляется параметр «Диплом с отличием»
            if (extract instanceof FefuGiveDiplomaSuccessWithDismissStuListExtract
                    || extract instanceof GiveDiplSuccessStuListExtract
                    || extract instanceof GiveDiplSuccessExtStuListExtract)
            {
                _diplomaSuccess = true;
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (DirectumSend.SIGNER_DS.equals(dataSource.getName()))
        {
            String[] preItemList = StringUtils.split(_uiConfig.getProperty("ui.signerDS.itemslist"), ";");
            String[] codesList = StringUtils.split(_uiConfig.getProperty("ui.signerDS.itemcodes"), ";");
            List<DataWrapper> recordList = new ArrayList<>(preItemList.length);
            for (int i = 0; i < preItemList.length; i++)
                recordList.add(new DataWrapper(Long.parseLong(codesList[i]), preItemList[i], preItemList[i]));
            dataSource.put(UIDefines.COMBO_OBJECT_LIST, recordList);
        }
    }

    public void onChangeOrderType()
    {
        initializeSettings();
    }

    public void initializeSettings()
    {
        _routeCode = _orderType.getRouteCode();
        _documentType = _orderType.getDocumentType();

        // Сложная логика вычисления маршрута
        EducationOrgUnit educationOrgUnit = getSpecialEducationOrgUnitForRoute(_firstExtract);
        if (educationOrgUnit == null)
            _orgUnit = _order.getOrgUnit();
        else
        {
            if (educationOrgUnit.getTerritorialOrgUnit().isTop())
                _orgUnit = educationOrgUnit.getFormativeOrgUnit();
            else
                _orgUnit = educationOrgUnit.getTerritorialOrgUnit();
        }
        _orgUnitExt = DataAccessServices.dao().getByNaturalId(new FefuOrgUnitDirectumExtension.NaturalId(_orgUnit, _orderType));

        if (null != _orgUnitExt)
        {
            _orgUnitTitle = _orgUnitExt.getOrgUnitTitle();
            _orgUnitCode = _orgUnitExt.getOrgUnitCode();
        }
        else
        {
            _orgUnitTitle = null;
            _orgUnitCode = null;
        }

        if (_order instanceof StudentModularOrder || _order instanceof StudentOtherOrder)
        {
            _directumSettings = DataAccessServices.dao().getByNaturalId(new FefuDirectumSettings.NaturalId(_firstExtract.getType(), _orderType));
        }
        else if (_order instanceof StudentListOrder)
        {
            _directumSettings = DataAccessServices.dao().getByNaturalId(new FefuDirectumSettings.NaturalId(((StudentListOrder) _order).getType(), _orderType));
        }

        if (null != _directumSettings)
        {
            if (null != _directumSettings.getSigner())
            {
                String[] preItemList = StringUtils.split(_uiConfig.getProperty("ui.signerDS.itemslist"), ";");
                String[] codesList = StringUtils.split(_uiConfig.getProperty("ui.signerDS.itemcodes"), ";");
                for (int i = 0; i < preItemList.length; i++)
                {
                    if (preItemList[i].equalsIgnoreCase(_directumSettings.getSigner()))
                        _signer = new DataWrapper(Long.parseLong(codesList[i]), preItemList[i], preItemList[i]);
                }
            }
            _dismiss = _directumSettings.isDismiss();
            _restore = _directumSettings.isRestore();
            _grantMatAid = _directumSettings.isGrantMatAid();
            _social = _directumSettings.isSocial();
            _practice = _directumSettings.isPractice();
            _groupManager = _directumSettings.isGroupManager();
            _penalty = _directumSettings.isPenalty();
            _magister = _directumSettings.isMagisterProgramFixing();
            _payments = _directumSettings.isPayments();
            _stateFormular = _directumSettings.isStateFormular();
            _checkMedicalDocs = _directumSettings.isCheckMedicalDocs();
        }
        else
        {
            _signer = null;
            _dismiss = false;
            _restore = false;
            _grantMatAid = false;
            _social = false;
            _practice = false;
            _groupManager = false;
            _penalty = false;
            _magister = false;
            _payments = false;
            _stateFormular = false;
            _checkMedicalDocs = false;
            _offBudgetPayments = false;
            _foreignStudent = false;
            _uvcStudents = false;
            _fvoStudents = false;
            _diplomaSuccess = false;
        }
    }

    public void onClickApply()
    {
        OpenOfficeServiceUtil.checkPdfConverterAvailable();

        if (null == _orgUnit)
            throw new ApplicationException("Невозможно отправить приказ на согласование, поскольку для приказа не указано формирующее подразделение.");

        if (null == _orgUnitExt)
            throw new ApplicationException("Невозможно отправить приказ на согласование, поскольку для формирующего подразделения приказа не указан внутренний код.");

        if (_order instanceof StudentModularOrder)
        {
            boolean individual = MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual((StudentModularOrder) _order);
            if (individual && _firstExtract instanceof ICustomPrintFormContainer)
            {
                ICustomPrintFormContainer container = (ICustomPrintFormContainer) _firstExtract;
                byte[] template = container.getFile() != null ? container.getFile().getContent() : null;
                if (null == template)
                    throw new ApplicationException("Невозможно отправить приказ на согласование, поскольку у приказа отсутствует печатная форма.");
            }
        }

        FefuDirectumSendingOrder sendingOrder = DataAccessServices.dao().getByNaturalId(new FefuDirectumSendingOrder.NaturalId(_order));
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();

        if (null == sendingOrder) sendingOrder = new FefuDirectumSendingOrder();
        else
        {
            String preamble = "Невозможно отправить приказ на согласование, поскольку приказ уже ";
            if (!sendingOrder.isDoneSend())
                throw new ApplicationException(preamble + "находится в очереди на согласование.");
            if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(_order.getState().getCode()))
                throw new ApplicationException(preamble + "согласован.");
        }

        sendingOrder.setSendDateTime(null);
        sendingOrder.setDoneSend(false);
        sendingOrder.setOrder(_order);
        sendingOrder.setFirstExtract(_firstExtract);
        sendingOrder.setOrderType(_orderType);
        sendingOrder.setSigner(null != _signer ? _signer.getTitle() : null);
        sendingOrder.setRouteCode(_routeCode);
        sendingOrder.setOrgUnitCode(_orgUnitCode);
        sendingOrder.setOrgUnitTitle(_orgUnitTitle);
        sendingOrder.setDismiss(_dismiss);
        sendingOrder.setRestore(_restore);
        sendingOrder.setOffBudgetPayments(_offBudgetPayments);
        sendingOrder.setForeignStudent(_foreignStudent);
        sendingOrder.setGrantMatAid(_grantMatAid);
        sendingOrder.setSocial(_social);
        sendingOrder.setPractice(_practice);
        sendingOrder.setGroupManager(_groupManager);
        sendingOrder.setUvcStudents(_uvcStudents);
        sendingOrder.setFvoStudents(_fvoStudents);
        sendingOrder.setPenalty(_penalty);
        sendingOrder.setMagister(_magister);
        sendingOrder.setPayments(_payments);
        sendingOrder.setStateFormular(_stateFormular);
        sendingOrder.setCheckMedicalDocs(_checkMedicalDocs);
        sendingOrder.setDiplomaSuccess(_diplomaSuccess);
        sendingOrder.setExecutor(principalContext);

        DataAccessServices.dao().saveOrUpdate(sendingOrder);
        deactivate();
    }

    /**
     * Для некоторых типоы выписок (например, о восстановлении) необходимо брать не текущее НПП студента, а которое будет после проведения.
     */
    public static EducationOrgUnit getSpecialEducationOrgUnitForRoute(AbstractStudentExtract extract)
    {
        List<String> specialTypes = Arrays.asList(
                StudentExtractTypeCodes.RESTORATION_VARIANT_1_MODULAR_ORDER, // О восстановлении (Вариант 1)
                StudentExtractTypeCodes.RESTORATION_VARIANT_2_MODULAR_ORDER, // О восстановлении (Вариант 2)
                StudentExtractTypeCodes.RESTORATION_COURSE_MODULAR_ORDER, // О восстановлении (Еще один вариант)
                StudentExtractTypeCodes.RESTORATION_ADMIT_REPEAT_STATE_EXAMS_MODULAR_ORDER, // О восстановлении и повторном допуске к сдаче государственного экзамена (пересдача ГЭ)
                StudentExtractTypeCodes.RESTORATION_ADMIT_ABSCENCE_STATE_EXAMS_MODULAR_ORDER, // О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ)
                StudentExtractTypeCodes.RESTORATION_ADMIT_TO_DIPLOMA_MODULAR_ORDER, // О восстановлении и допуске к подготовке ВКР
                StudentExtractTypeCodes.RESTORATION_ADMIT_TO_DIPLOMA_DEFEND_ABSCENCE_MODULAR_ORDER, // О восстановлении и допуске к защите ВКР (неявка на защиту)
                StudentExtractTypeCodes.RESTORATION_ADMIT_TO_REPEAT_DIPLOMA_DEFEND_MODULAR_ORDER  // О восстановлении и допуске к защите ВКР (повторная защита)
        );

        if (specialTypes.contains(extract.getType().getCode()) && extract.getEntityMeta().getProperty("educationOrgUnitNew") != null)
        {
            return (EducationOrgUnit) FastBeanUtils.getValue(extract, "educationOrgUnitNew");
        }

        return null;
    }

    public String getExtractTitle()
    {
        StringBuilder builder = new StringBuilder();
        if (null != _order.getCommitDate())
            builder.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_order.getCommitDate()));
        if (null != _order.getNumber())
            builder.append(builder.length() > 0 ? " " : "").append("№ ").append(_order.getNumber());
        builder.append(builder.length() > 0 ? " " : "").append(_firstExtract.getType().getTitle());
        return builder.toString();
    }

    public String getStudentFio()
    {
        if (_order instanceof StudentListOrder) return "Списочный приказ";
        return _firstExtract.getEntity().getPerson().getFullFio();
    }

    public boolean isVpo()
    {
        return null != _orderType && FefuDirectumOrderTypeCodes.VPO.equals(_orderType.getCode());
    }

    public boolean isSpo()
    {
        return null != _orderType && FefuDirectumOrderTypeCodes.SPO.equals(_orderType.getCode());
    }

    public boolean isDpo()
    {
        return null != _orderType && FefuDirectumOrderTypeCodes.DPO.equals(_orderType.getCode());
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public AbstractStudentOrder getOrder()
    {
        return _order;
    }

    public void setOrder(AbstractStudentOrder order)
    {
        _order = order;
    }

    public AbstractStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public void setFirstExtract(AbstractStudentExtract firstExtract)
    {
        _firstExtract = firstExtract;
    }

    public DQLFullCheckSelectModel getOrderTypeModel()
    {
        return _orderTypeModel;
    }

    public void setOrderTypeModel(DQLFullCheckSelectModel orderTypeModel)
    {
        _orderTypeModel = orderTypeModel;
    }

    public FefuDirectumOrderType getOrderType()
    {
        return _orderType;
    }

    public void setOrderType(FefuDirectumOrderType orderType)
    {
        _orderType = orderType;
    }

    public FefuDirectumSettings getDirectumSettings()
    {
        return _directumSettings;
    }

    public void setDirectumSettings(FefuDirectumSettings directumSettings)
    {
        _directumSettings = directumSettings;
    }

    public FefuOrgUnitDirectumExtension getOrgUnitExt()
    {
        return _orgUnitExt;
    }

    public void setOrgUnitExt(FefuOrgUnitDirectumExtension orgUnitExt)
    {
        _orgUnitExt = orgUnitExt;
    }

    public String getOrgUnitTitle()
    {
        return _orgUnitTitle;
    }

    public void setOrgUnitTitle(String orgUnitTitle)
    {
        _orgUnitTitle = orgUnitTitle;
    }

    public DataWrapper getSigner()
    {
        return _signer;
    }

    public void setSigner(DataWrapper signer)
    {
        _signer = signer;
    }

    public String getRouteCode()
    {
        return _routeCode;
    }

    public void setRouteCode(String routeCode)
    {
        _routeCode = routeCode;
    }

    public String getOrgUnit()
    {
        return _orgUnitTitle;
    }

    public void setOrgUnit(String orgUnitTitle)
    {
        _orgUnitTitle = orgUnitTitle;
    }

    public String getOrgUnitCode()
    {
        return _orgUnitCode;
    }

    public void setOrgUnitCode(String orgUnitCode)
    {
        _orgUnitCode = orgUnitCode;
    }

    public String getDocumentType()
    {
        return _documentType;
    }

    public void setDocumentType(String documentType)
    {
        _documentType = documentType;
    }

    public boolean isDismiss()
    {
        return _dismiss;
    }

    public void setDismiss(boolean dismiss)
    {
        _dismiss = dismiss;
    }

    public boolean isRestore()
    {
        return _restore;
    }

    public void setRestore(boolean restore)
    {
        _restore = restore;
    }

    public boolean isOffBudgetPayments()
    {
        return _offBudgetPayments;
    }

    public void setOffBudgetPayments(boolean offBudgetPayments)
    {
        _offBudgetPayments = offBudgetPayments;
    }

    public boolean isForeignStudent()
    {
        return _foreignStudent;
    }

    public void setForeignStudent(boolean foreignStudent)
    {
        _foreignStudent = foreignStudent;
    }

    public boolean isGrantMatAid()
    {
        return _grantMatAid;
    }

    public void setGrantMatAid(boolean grantMatAid)
    {
        _grantMatAid = grantMatAid;
    }

    public boolean isSocial()
    {
        return _social;
    }

    public void setSocial(boolean social)
    {
        _social = social;
    }

    public boolean isPractice()
    {
        return _practice;
    }

    public void setPractice(boolean practice)
    {
        _practice = practice;
    }

    public boolean isGroupManager()
    {
        return _groupManager;
    }

    public void setGroupManager(boolean groupManager)
    {
        _groupManager = groupManager;
    }

    public boolean isUvcStudents()
    {
        return _uvcStudents;
    }

    public void setUvcStudents(boolean uvcStudents)
    {
        _uvcStudents = uvcStudents;
    }

    public boolean isFvoStudents()
    {
        return _fvoStudents;
    }

    public void setFvoStudents(boolean fvoStudents)
    {
        _fvoStudents = fvoStudents;
    }

    public boolean isPenalty()
    {
        return _penalty;
    }

    public void setPenalty(boolean penalty)
    {
        _penalty = penalty;
    }

    public boolean isMagister()
    {
        return _magister;
    }

    public void setMagister(boolean magister)
    {
        _magister = magister;
    }

    public boolean isPayments()
    {
        return _payments;
    }

    public void setPayments(boolean payments)
    {
        _payments = payments;
    }

    public boolean isStateFormular()
    {
        return _stateFormular;
    }

    public void setStateFormular(boolean stateFormular)
    {
        _stateFormular = stateFormular;
    }

    public boolean isCheckMedicalDocs()
    {
        return _checkMedicalDocs;
    }

    public void setCheckMedicalDocs(boolean checkMedicalDocs)
    {
        _checkMedicalDocs = checkMedicalDocs;
    }

    public boolean isDiplomaSuccess()
    {
        return _diplomaSuccess;
    }

    public void setDiplomaSuccess(boolean diplomaSuccess)
    {
        _diplomaSuccess = diplomaSuccess;
    }
}