/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import ru.tandemservice.unifefu.entity.ws.FefuContractor;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.ContractorType;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 07.05.2014
 */
public class ContractorUtil extends SimpleNsiCatalogUtil<ContractorType, FefuContractor>
{
    public static final String CONTRACTOR_ID_NSI_FIELD = "ContractorID";
    public static final String CONTRACTOR_NAME_FULL_NSI_FIELD = "ContractorNameFull";
    public static final String INN_NSI_FIELD = "ContractorINN";
    public static final String KPP_NSI_FIELD = "ContractorKPP";
    public static final String OGRN_NSI_FIELD = "ContractorOGRN";
    public static final String OKPO_NSI_FIELD = "ContractorOKPO";

    @Override
    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            super.getNsiFields();
            NSI_FIELDS.add(CONTRACTOR_ID_NSI_FIELD);
            NSI_FIELDS.add(CONTRACTOR_NAME_FULL_NSI_FIELD);
            NSI_FIELDS.add(INN_NSI_FIELD);
            NSI_FIELDS.add(KPP_NSI_FIELD);
            NSI_FIELDS.add(OGRN_NSI_FIELD);
            NSI_FIELDS.add(OKPO_NSI_FIELD);
        }
        return NSI_FIELDS;
    }

    @Override
    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            super.getEntityFields();
            ENTITY_FIELDS.add(FefuContractor.contractorID().s());
            ENTITY_FIELDS.add(FefuContractor.nameFull().s());
            ENTITY_FIELDS.add(FefuContractor.inn().s());
            ENTITY_FIELDS.add(FefuContractor.kpp().s());
            ENTITY_FIELDS.add(FefuContractor.ogrn().s());
            ENTITY_FIELDS.add(FefuContractor.okpo().s());
        }
        return ENTITY_FIELDS;
    }

    @Override
    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            super.getNsiToObFieldsMap();
            NSI_TO_OB_FIELDS_MAP.put(CONTRACTOR_ID_NSI_FIELD, FefuContractor.contractorID().s());
            NSI_TO_OB_FIELDS_MAP.put(CONTRACTOR_NAME_FULL_NSI_FIELD, FefuContractor.nameFull().s());
            NSI_TO_OB_FIELDS_MAP.put(INN_NSI_FIELD, FefuContractor.inn().s());
            NSI_TO_OB_FIELDS_MAP.put(KPP_NSI_FIELD, FefuContractor.kpp().s());
            NSI_TO_OB_FIELDS_MAP.put(OGRN_NSI_FIELD, FefuContractor.ogrn().s());
            NSI_TO_OB_FIELDS_MAP.put(OKPO_NSI_FIELD, FefuContractor.okpo().s());
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    @Override
    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            super.getObToNsiFieldsMap();
            OB_TO_NSI_FIELDS_MAP.put(FefuContractor.contractorID().s(), CONTRACTOR_ID_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuContractor.nameFull().s(), CONTRACTOR_NAME_FULL_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuContractor.inn().s(), INN_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuContractor.kpp().s(), KPP_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuContractor.ogrn().s(), OGRN_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuContractor.okpo().s(), OKPO_NSI_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    public long getNsiFefuContractorIDHash(ContractorType nsiEntity)
    {
        if (null == nsiEntity.getContractorID()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getContractorID());
    }

    public long getNsiFefuContractorNameFullHash(ContractorType nsiEntity)
    {
        if (null == nsiEntity.getContractorNameFull()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getContractorNameFull());
    }

    public long getNsiFefuContractorINNHash(ContractorType nsiEntity)
    {
        if (null == nsiEntity.getContractorINN()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getContractorINN());
    }

    public long getNsiFefuContractorKPPHash(ContractorType nsiEntity)
    {
        if (null == nsiEntity.getContractorKPP()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getContractorKPP());
    }

    public long getNsiFefuContractorOGRNHash(ContractorType nsiEntity)
    {
        if (null == nsiEntity.getContractorOGRN()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getContractorOGRN());
    }

    public long getNsiFefuContractorOKPOHash(ContractorType nsiEntity)
    {
        if (null == nsiEntity.getContractorOKPO()) return 0;
        return MD5HashBuilder.getCheckSum(nsiEntity.getContractorOKPO());
    }

    @Override
    public boolean isFieldEmpty(ContractorType nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case CONTRACTOR_ID_NSI_FIELD:
                return null == nsiEntity.getContractorID();
            case CONTRACTOR_NAME_FULL_NSI_FIELD:
                return null == nsiEntity.getContractorNameFull();
            case INN_NSI_FIELD:
                return null == nsiEntity.getContractorINN();
            case KPP_NSI_FIELD:
                return null == nsiEntity.getContractorKPP();
            case OGRN_NSI_FIELD:
                return null == nsiEntity.getContractorOGRN();
            case OKPO_NSI_FIELD:
                return null == nsiEntity.getContractorOKPO();
            default:
                return super.isFieldEmpty(nsiEntity, fieldName);
        }
    }

    @Override
    public long getNsiFieldHash(ContractorType nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case CONTRACTOR_ID_NSI_FIELD:
                return getNsiFefuContractorIDHash(nsiEntity);
            case CONTRACTOR_NAME_FULL_NSI_FIELD:
                return getNsiFefuContractorNameFullHash(nsiEntity);
            case INN_NSI_FIELD:
                return getNsiFefuContractorINNHash(nsiEntity);
            case KPP_NSI_FIELD:
                return getNsiFefuContractorKPPHash(nsiEntity);
            case OGRN_NSI_FIELD:
                return getNsiFefuContractorOGRNHash(nsiEntity);
            case OKPO_NSI_FIELD:
                return getNsiFefuContractorOKPOHash(nsiEntity);
            default:
                return super.getNsiFieldHash(nsiEntity, fieldName, caseInsensitive);
        }
    }

    public long getFefuContractorIDHash(FefuContractor entity)
    {
        if (null == entity.getContractorID()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getContractorID());
    }

    public long getFefuContractorNameFullHash(FefuContractor entity)
    {
        if (null == entity.getNameFull()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getNameFull());
    }

    public long getFefuContractorINNHash(FefuContractor entity)
    {
        if (null == entity.getInn()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getInn());
    }

    public long getFefuContractorKPPHash(FefuContractor entity)
    {
        if (null == entity.getKpp()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getKpp());
    }

    public long getFefuContractorOGRNHash(FefuContractor entity)
    {
        if (null == entity.getOgrn()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getOgrn());
    }

    public long getFefuContractorOKPOHash(FefuContractor entity)
    {
        if (null == entity.getOkpo()) return 0;
        return MD5HashBuilder.getCheckSum(entity.getOkpo());
    }

    @Override
    public long getEntityFieldHash(FefuContractor entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case FefuContractor.P_CONTRACTOR_I_D:
                return getFefuContractorIDHash(entity);
            case FefuContractor.P_NAME_FULL:
                return getFefuContractorNameFullHash(entity);
            case ICatalogItem.CATALOG_ITEM_TITLE:
                return getFefuContractorNameFullHash(entity);
            case FefuContractor.P_INN:
                return getFefuContractorINNHash(entity);
            case FefuContractor.P_KPP:
                return getFefuContractorKPPHash(entity);
            case FefuContractor.P_OGRN:
                return getFefuContractorOGRNHash(entity);
            case FefuContractor.P_OKPO:
                return getFefuContractorOKPOHash(entity);
            default:
                return super.getEntityFieldHash(entity, nsiId, fieldName, caseInsensitive);
        }
    }
}