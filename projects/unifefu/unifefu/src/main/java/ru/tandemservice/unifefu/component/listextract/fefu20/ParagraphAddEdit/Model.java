/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20.ParagraphAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;

import java.util.List;

/**
 * @author Andrey Andreev
 * @since 13.01.2016
 */
public class Model extends AbstractListParagraphAddEditModel<FefuAdmittedToGIAListExtract> implements IGroupModel
{
    private StudentCustomStateCI _studentCustomStateCI;

    private Integer _year;
    private String _season;
    private List<String> _seasons;

    //фильтры
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private ISelectModel _groupListModel;
    private List<CompensationType> _compensationTypeList;

    private boolean _courseAndGroupDisabled;

    public String getSeason()
    {
        return _season;
    }

    public void setSeason(String season)
    {
        _season = season;
    }

    public List<String> getSeasons()
    {
        return _seasons;
    }

    public void setSeasons(List<String> seasons)
    {
        _seasons = seasons;
    }

    public Integer getYear()
    {
        return _year;
    }

    public void setYear(Integer year)
    {
        _year = year;
    }


    public boolean isNotFirstParagraph()
    {
        return !isParagraphOnlyOneInTheOrder();
    }

    public StudentCustomStateCI getStudentCustomStateCI()
    {
        return _studentCustomStateCI;
    }

    public void setStudentCustomStateCI(StudentCustomStateCI studentCustomStateCI)
    {
        _studentCustomStateCI = studentCustomStateCI;
    }


    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public boolean isCourseAndGroupDisabled()
    {
        return _courseAndGroupDisabled;
    }

    public void setCourseAndGroupDisabled(boolean courseAndGroupDisabled)
    {
        _courseAndGroupDisabled = courseAndGroupDisabled;
    }
}