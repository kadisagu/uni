package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Программа ДПО/ДО для студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuAdditionalProfessionalEducationProgramForStudentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent";
    public static final String ENTITY_NAME = "fefuAdditionalProfessionalEducationProgramForStudent";
    public static final int VERSION_HASH = -780479761;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_PROGRAM = "program";
    public static final String P_EDUCATION_START_DATE = "educationStartDate";
    public static final String P_EDUCATION_END_DATE = "educationEndDate";
    public static final String L_INDIVIDUAL_PARAMETERS = "individualParameters";

    private Student _student;     // Студент
    private FefuAdditionalProfessionalEducationProgram _program;     // Программа ДПО/ДО
    private Date _educationStartDate;     // Дата начала обучения
    private Date _educationEndDate;     // Дата окончания обучения
    private FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters _individualParameters;     // Индивидуальные параметры программы ДПО/ДО для студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Программа ДПО/ДО.
     */
    public FefuAdditionalProfessionalEducationProgram getProgram()
    {
        return _program;
    }

    /**
     * @param program Программа ДПО/ДО.
     */
    public void setProgram(FefuAdditionalProfessionalEducationProgram program)
    {
        dirty(_program, program);
        _program = program;
    }

    /**
     * @return Дата начала обучения.
     */
    public Date getEducationStartDate()
    {
        return _educationStartDate;
    }

    /**
     * @param educationStartDate Дата начала обучения.
     */
    public void setEducationStartDate(Date educationStartDate)
    {
        dirty(_educationStartDate, educationStartDate);
        _educationStartDate = educationStartDate;
    }

    /**
     * @return Дата окончания обучения.
     */
    public Date getEducationEndDate()
    {
        return _educationEndDate;
    }

    /**
     * @param educationEndDate Дата окончания обучения.
     */
    public void setEducationEndDate(Date educationEndDate)
    {
        dirty(_educationEndDate, educationEndDate);
        _educationEndDate = educationEndDate;
    }

    /**
     * @return Индивидуальные параметры программы ДПО/ДО для студента.
     */
    public FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters getIndividualParameters()
    {
        return _individualParameters;
    }

    /**
     * @param individualParameters Индивидуальные параметры программы ДПО/ДО для студента.
     */
    public void setIndividualParameters(FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters individualParameters)
    {
        dirty(_individualParameters, individualParameters);
        _individualParameters = individualParameters;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuAdditionalProfessionalEducationProgramForStudentGen)
        {
            setStudent(((FefuAdditionalProfessionalEducationProgramForStudent)another).getStudent());
            setProgram(((FefuAdditionalProfessionalEducationProgramForStudent)another).getProgram());
            setEducationStartDate(((FefuAdditionalProfessionalEducationProgramForStudent)another).getEducationStartDate());
            setEducationEndDate(((FefuAdditionalProfessionalEducationProgramForStudent)another).getEducationEndDate());
            setIndividualParameters(((FefuAdditionalProfessionalEducationProgramForStudent)another).getIndividualParameters());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuAdditionalProfessionalEducationProgramForStudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuAdditionalProfessionalEducationProgramForStudent.class;
        }

        public T newInstance()
        {
            return (T) new FefuAdditionalProfessionalEducationProgramForStudent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "program":
                    return obj.getProgram();
                case "educationStartDate":
                    return obj.getEducationStartDate();
                case "educationEndDate":
                    return obj.getEducationEndDate();
                case "individualParameters":
                    return obj.getIndividualParameters();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "program":
                    obj.setProgram((FefuAdditionalProfessionalEducationProgram) value);
                    return;
                case "educationStartDate":
                    obj.setEducationStartDate((Date) value);
                    return;
                case "educationEndDate":
                    obj.setEducationEndDate((Date) value);
                    return;
                case "individualParameters":
                    obj.setIndividualParameters((FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "program":
                        return true;
                case "educationStartDate":
                        return true;
                case "educationEndDate":
                        return true;
                case "individualParameters":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "program":
                    return true;
                case "educationStartDate":
                    return true;
                case "educationEndDate":
                    return true;
                case "individualParameters":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "program":
                    return FefuAdditionalProfessionalEducationProgram.class;
                case "educationStartDate":
                    return Date.class;
                case "educationEndDate":
                    return Date.class;
                case "individualParameters":
                    return FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuAdditionalProfessionalEducationProgramForStudent> _dslPath = new Path<FefuAdditionalProfessionalEducationProgramForStudent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuAdditionalProfessionalEducationProgramForStudent");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Программа ДПО/ДО.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent#getProgram()
     */
    public static FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> program()
    {
        return _dslPath.program();
    }

    /**
     * @return Дата начала обучения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent#getEducationStartDate()
     */
    public static PropertyPath<Date> educationStartDate()
    {
        return _dslPath.educationStartDate();
    }

    /**
     * @return Дата окончания обучения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent#getEducationEndDate()
     */
    public static PropertyPath<Date> educationEndDate()
    {
        return _dslPath.educationEndDate();
    }

    /**
     * @return Индивидуальные параметры программы ДПО/ДО для студента.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent#getIndividualParameters()
     */
    public static FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters.Path<FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters> individualParameters()
    {
        return _dslPath.individualParameters();
    }

    public static class Path<E extends FefuAdditionalProfessionalEducationProgramForStudent> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> _program;
        private PropertyPath<Date> _educationStartDate;
        private PropertyPath<Date> _educationEndDate;
        private FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters.Path<FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters> _individualParameters;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Программа ДПО/ДО.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent#getProgram()
     */
        public FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram> program()
        {
            if(_program == null )
                _program = new FefuAdditionalProfessionalEducationProgram.Path<FefuAdditionalProfessionalEducationProgram>(L_PROGRAM, this);
            return _program;
        }

    /**
     * @return Дата начала обучения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent#getEducationStartDate()
     */
        public PropertyPath<Date> educationStartDate()
        {
            if(_educationStartDate == null )
                _educationStartDate = new PropertyPath<Date>(FefuAdditionalProfessionalEducationProgramForStudentGen.P_EDUCATION_START_DATE, this);
            return _educationStartDate;
        }

    /**
     * @return Дата окончания обучения.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent#getEducationEndDate()
     */
        public PropertyPath<Date> educationEndDate()
        {
            if(_educationEndDate == null )
                _educationEndDate = new PropertyPath<Date>(FefuAdditionalProfessionalEducationProgramForStudentGen.P_EDUCATION_END_DATE, this);
            return _educationEndDate;
        }

    /**
     * @return Индивидуальные параметры программы ДПО/ДО для студента.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent#getIndividualParameters()
     */
        public FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters.Path<FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters> individualParameters()
        {
            if(_individualParameters == null )
                _individualParameters = new FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters.Path<FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters>(L_INDIVIDUAL_PARAMETERS, this);
            return _individualParameters;
        }

        public Class getEntityClass()
        {
            return FefuAdditionalProfessionalEducationProgramForStudent.class;
        }

        public String getEntityName()
        {
            return "fefuAdditionalProfessionalEducationProgramForStudent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
