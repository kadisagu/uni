/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

/**
 * @author Dmitry Seleznev
 * @since 12.01.2015
 */
public interface IFefuNsiContactDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IFefuNsiContactDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IFefuNsiContactDaemonDao> instance = new SpringBeanCache<>(IFefuNsiContactDaemonDao.class.getName());

    /**
     * Производит синхронизацию контактных данных чисто студенческих физ лиц (не связанных с сотрудниками) с НСИ.
     * Берет порцию из 100 физ лиц и генерирует по ним связанные объекты контактных данных НСИ. Далее данные отсылаются в НСИ.
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doSyncContactsWithNSI();
}