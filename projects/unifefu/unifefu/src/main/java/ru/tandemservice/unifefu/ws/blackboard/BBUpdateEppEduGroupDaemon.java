/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.unifefu.entity.blackboard.*;
import ru.tandemservice.unifefu.ws.blackboard.course.BBCourseUtils;
import ru.tandemservice.unifefu.ws.blackboard.membership.BBMembershipUtils;
import ru.tandemservice.unifefu.ws.blackboard.membership.CourseMembershipWSConstants;
import ru.tandemservice.unifefu.ws.blackboard.user.BBUserUtils;
import ru.tandemservice.unifefu.ws.blackboard.user.gen.UserVO;
import ru.tandemservice.unifefu.ws.nsi.dao.IFefuNsiSyncDAO;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import javax.transaction.Status;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Демон обновляет группы и студентов на курсах Blackboard (ЭУК) по актуальным данным из строк УГС в реализациях дисциплин,
 * к которым привязаны курсы BB
 *
 * @author Nikolay Fedorovskih
 * @since 28.03.2014
 */
public class BBUpdateEppEduGroupDaemon extends UniBaseDao implements IBBUpdateEppEduGroupDaemon
{
    public static final String GLOBAL_DAEMON_LOCK = BBUpdateEppEduGroupDaemon.class.getSimpleName() + ".lock";

    public static final SyncDaemon DAEMON = new SyncDaemon(BBUpdateEppEduGroupDaemon.class.getName(), 24 * 60, GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            if (!BBSettingsVO.getCachedVO().isEppDaemonActive())
                return;

            try
            {
                // Делаем в разных транзакциях, т.к. метод удаления групп сначала удаляет группы из ВВ, а затем из базы.
                // Если вдруг транзакция откатится, вернуть удаленные в ВВ группы не получится. Поэтому лучше независимые операции разбить по разным транзакциям.
                // Порядок вызова методов не менять!
                IBBUpdateEppEduGroupDaemon.instance.get().cleanGroupMemberships();
                IBBUpdateEppEduGroupDaemon.instance.get().cleanCourseStudents();
                IBBUpdateEppEduGroupDaemon.instance.get().deleteOrphanGroups();
                IBBUpdateEppEduGroupDaemon.instance.get().doUpdate();
            }
            catch (Throwable t)
            {
                logger.error(t.getMessage(), t);
                Debug.exception(t.getMessage(), t);
            }
        }
    };

    @Override
    protected void initDao()
    {
        // Регистрируем возбуждение демона на события
        BBUpdateEppEduGroupDaemon.DAEMON.registerWakeUpListener4Class(BbCourse2TrJournalRel.class);
    }

    @Override
    public void deleteOrphanGroups()
    {
        Debug.begin("deleteOrphanGroups-start");
        try
        {
            // Получаем пустые группы ВВ
            DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromEntity(BbGroup.class, "g")
                            .where(notExists(
                                    new DQLSelectBuilder().fromEntity(BbGroupMembership.class, "m").column("m.id")
                                            .where(eq(property("m", BbGroupMembership.L_BB_GROUP), property("g.id")))
                                            .buildQuery()
                            ))
            );
            int bbCoursePrimaryId = dql.column(property("g", BbGroup.bbCourse2TrJournalRel().bbCourse().bbPrimaryId()));
            int bbGroupPrimaryId = dql.column(property("g", BbGroup.P_BB_PRIMARY_ID));
            int bbGroupId = dql.column("g.id");

            // Группируем идентификаторы групп по ЭУК
            Multimap<String, String> orphanGroupsMap = ArrayListMultimap.create();
            for (Object[] item : scrollRows(dql.getDql().createStatement(getSession())))
            {
                orphanGroupsMap.put((String) item[bbCoursePrimaryId], (String) item[bbGroupPrimaryId]);

                // Сразу удаляем группу из базы. Если удаление из ВВ свалится, то транзакция откатится и в базе группа останется.
                delete((Long) item[bbGroupId]);
            }

            // Удаляем группы из BB
            for (Map.Entry<String, Collection<String>> entry : orphanGroupsMap.asMap().entrySet())
            {
                BBCourseUtils.deleteGroups(entry.getKey(), (List<String>) entry.getValue());
            }
        }
        finally
        {
            Debug.end();
        }
    }

    @Override
    public void cleanCourseStudents()
    {
        Debug.begin("cleanCourseStudents-start");
        try
        {
            // Просто удаляем всех тех студентов курсов ВВ, на которых нет ни одного ссылающегося участника группы
            DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromEntity(BbCourseStudent.class, "s")
                            .where(notExists(
                                    new DQLSelectBuilder().fromEntity(BbGroupMembership.class, "m").column("m.id")
                                            .where(eq(property("m", BbGroupMembership.bbCourseStudent()), property("s.id")))
                                            .buildQuery()
                            ))
            );
            int courseStudentIdCol = dql.column("s.id");
            int bbCoursePrimaryIdCol = dql.column(property("s", BbCourseStudent.bbCourse().bbPrimaryId()));
            int bbCourseStudentPrimaryId = dql.column(property("s", BbCourseStudent.bbPrimaryId()));

            Multimap<String, String> courseMap = ArrayListMultimap.create();
            for (Object[] item : scrollRows(dql.getDql().createStatement(getSession())))
            {
                // Удаляем из базы. Если удаление из ВВ свалится, то транзакция откатится и в базе этот объект останется.
                delete((Long) item[courseStudentIdCol]);

                // Подготавливаем мапу для удаления из ВВ
                courseMap.put((String) item[bbCoursePrimaryIdCol], (String) item[bbCourseStudentPrimaryId]);
            }

            // Удаляем из ВВ
            for (Map.Entry<String, Collection<String>> entry : courseMap.asMap().entrySet())
            {
                BBMembershipUtils.deleteCourseMemberships(entry.getKey(), (List<String>) entry.getValue());
            }
        }
        finally
        {
            Debug.end();
        }
    }

    @Override
    public void cleanGroupMemberships()
    {
        Debug.begin("cleanGroupMemberships-start");
        try
        {
            DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromEntity(BbGroupMembership.class, "m")
                            .joinPath(DQLJoinType.left, BbGroupMembership.groupRow().fromAlias("m"), "grpRow")
                            .where(or(
                                    isNull("grpRow.id"),
                                    isNotNull(property("grpRow", EppRealEduGroup4LoadTypeRow.P_REMOVAL_DATE))
                            ))
            );

            int groupMembershipIdCol = dql.column("m.id");
            int coursePrimaryIdCol = dql.column(property("m", BbGroupMembership.bbCourseStudent().bbCourse().bbPrimaryId()));
            int groupMembershipPrimaryIdCol = dql.column(property("m", BbGroupMembership.bbPrimaryId()));

            Multimap<String, String> courseMap = ArrayListMultimap.create();

            for (Object[] item : scrollRows(dql.getDql().createStatement(getSession())))
            {
                // Удаляем из базы. Если удаление из ВВ свалится, то транзакция откатится и в базе этот объект останется.
                delete((Long) item[groupMembershipIdCol]);

                // Подготавливаем мапу для удаления из ВВ
                courseMap.put((String) item[coursePrimaryIdCol], (String) item[groupMembershipPrimaryIdCol]);
            }

            for (Map.Entry<String, Collection<String>> entry : courseMap.asMap().entrySet())
            {
                // Удаляем из ВВ
                BBMembershipUtils.deleteGroupMemberships(entry.getKey(), (List<String>) entry.getValue());
            }
        }
        finally
        {
            Debug.end();
        }
    }

    @Override
    public void doUpdate()
    {
        Debug.begin("doUpdate-create-new-groups-and-student-rows");
        try
        {
            final Session session = getSession();

            // Получаем список студентов к зачислению на курсы и в группы
            DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "grpRow")
                            .predicate(DQLPredicateType.distinct)
                            .joinPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.group().fromAlias("grpRow"), "grp")
                            .joinEntity("grp", DQLJoinType.inner, TrJournalGroup.class, "trGroup",
                                        eq(property("trGroup", TrJournalGroup.L_GROUP), property("grp.id"))
                            )
                            .joinEntity("trGroup", DQLJoinType.inner, BbCourse2TrJournalRel.class, "rel",
                                        eq(property("trGroup", TrJournalGroup.L_JOURNAL), property("rel", BbCourse2TrJournalRel.L_TR_JOURNAL))
                            )
                            .joinPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().fromAlias("grpRow"), "s")
                            .joinPath(DQLJoinType.left, Student.group().fromAlias("s"), "g")
                            .joinEntity("rel", DQLJoinType.left, BbGroup.class, "bbg", and(
                                    eq(property("bbg", BbGroup.L_BB_COURSE2_TR_JOURNAL_REL), property("rel.id")),
                                    eqNullSafe(property("bbg", BbGroup.L_GROUP), property("g.id"))
                            ))
                            .where(isNull(property("grpRow", EppRealEduGroup4LoadTypeRow.P_REMOVAL_DATE)))
                            .where(notExists(
                                    // Те, на кого уже созданы объекты, исключаем
                                    new DQLSelectBuilder().fromEntity(BbGroupMembership.class, "m").column("m.id")
                                            .where(eq(property("s.id"), property("m", BbGroupMembership.bbCourseStudent().student())))
                                            .where(eq(property("rel.id"), property("m", BbGroupMembership.bbGroup().bbCourse2TrJournalRel())))
                                            .where(eqNullSafe(property("g.id"), property("m", BbGroupMembership.bbGroup().group())))
                                            .buildQuery()
                            ))
            );

            int groupRowIdCol = dql.column("grpRow.id");
            int groupIdCol = dql.column("g.id"); // идентификатор группы
            int trCourseRelIdCol = dql.column("rel.id"); // идентификатор связи курс-реализация
            int bbCourseIdCol = dql.column(property("rel", BbCourse2TrJournalRel.bbCourse().id())); // идентификатор курса
            int bbCoursePrimaryIdCol = dql.column(property("rel", BbCourse2TrJournalRel.bbCourse().bbPrimaryId())); // идентификатор курса ВВ (примари)
            int studentIdCol = dql.column("s.id"); // идентификатор студента
            int personIdCol = dql.column(property("s", Student.person().id())); // идентификатор персоны (достаем сразу, чтобы отдельным запросом не получать объект студента)
            int bbGroupIdCol = dql.column("bbg.id");
            int bbGroupPrimaryIdCol = dql.column(property("bbg", BbGroup.P_BB_PRIMARY_ID));

            NewBBObjectsCreationRollbackAction rollbackCleaner = new NewBBObjectsCreationRollbackAction(session);

            // кэши объектов
            Map<Long, String> personGuidCache = new HashMap<>();
            Map<Long, UserVO> userVOCache = new HashMap<>();
            Map<MultiKey, BbGroup> newGroups = new HashMap<>();
            Set<MultiKey> newMemberships = new HashSet<>();

            for (Object[] item : scrollRows(dql.getDql().createStatement(session)))
            {
                Long groupRowId = (Long) item[groupRowIdCol];
                Long groupId = (Long) item[groupIdCol];
                Long relId = (Long) item[trCourseRelIdCol];
                Long courseId = (Long) item[bbCourseIdCol];
                String coursePrimaryId = (String) item[bbCoursePrimaryIdCol];
                Long studentId = (Long) item[studentIdCol];
                Long personId = (Long) item[personIdCol];

                // Ищем участника курса в нашей базе
                BbCourseStudent courseStudent = getByNaturalId(new BbCourseStudent.NaturalId(
                        BBUpdateEppEduGroupDaemon.<BbCourse>proxy(courseId),
                        BBUpdateEppEduGroupDaemon.<Student>proxy(studentId)
                ));

                if (courseStudent == null)
                {
                    // Если студент еще не зачислен на курс, зачисляем его

                    // Берем гуид персоны из кэша или из базы
                    String personGUID = personGuidCache.get(personId);
                    {
                        final boolean inCache = personGUID != null || personGuidCache.containsKey(personId);
                        if (personGUID == null && !inCache)
                        {
                            personGuidCache.put(personId, personGUID = IFefuNsiSyncDAO.instance.get().getNsiIdGuid(personId));
                        }

                        if (personGUID == null)
                        {
                            if (!inCache)
                            {
                                logger.error(this.<Person>get(personId).getFullFio() + " has no GUID");
                            }
                            continue;
                        }
                    }

                    UserVO userVO = userVOCache.get(personId);
                    {
                        final boolean inCache = userVO != null || userVOCache.containsKey(personId);
                        if (userVO == null && !inCache)
                        {
                            userVOCache.put(personId, userVO = BBUserUtils.findUser(personGUID));
                        }

                        if (userVO == null)
                        {
                            if (!inCache)
                            {
                                logger.error("Blackboard user for " + this.<Person>get(personId).getFullFio() + " (id " + personId + ") is not found by GUID " + personGUID);
                            }
                            continue;
                        }

                        //System.out.println("Create new course membership: " + this.<BbCourse>get(courseId).getTitle() + " - " + this.<Person>get(personId).getFullFio());

                        // Создаем в ВВ
                        String newCourseMembershipId = BBMembershipUtils.saveCourseMembership(coursePrimaryId, userVO.getId().getValue(), CourseMembershipWSConstants.STUDENT_ROLE);
                        rollbackCleaner.addCourseMembership(coursePrimaryId, newCourseMembershipId);

                        // Создаем в нашей базе
                        courseStudent = new BbCourseStudent();
                        courseStudent.setBbCourse(BBUpdateEppEduGroupDaemon.<BbCourse>proxy(courseId));
                        courseStudent.setAccess(true);
                        courseStudent.setStudent((Student) proxy(studentId));
                        courseStudent.setBbPrimaryId(newCourseMembershipId);
                        save(courseStudent);
                    }
                }

                // Ищем группу курса в нашей базе
                MultiKey newGroupKey = new MultiKey(relId, groupId);
                BbGroup bbGroup = item[bbGroupIdCol] != null ?
                        BBUpdateEppEduGroupDaemon.<BbGroup>proxy((Long) item[bbGroupIdCol]) :
                        newGroups.get(newGroupKey);
                String bbGroupPrimaryId;

                if (bbGroup == null)
                {
                    String groupTitle, groupDescription;
                    Group group = null;
                    if (groupId != null)
                    {
                        group = getNotNull(groupId);
                        groupTitle = group.getTitle();
                        groupDescription = group.getFullTitle();
                    }
                    else
                    {
                        groupTitle = "Без группы";
                        groupDescription = "Студенты без академической группы";
                    }

                    //System.out.println("Create new BB group: " + this.<BbCourse>get(courseId).getTitle() + " - " + groupTitle);

                    // Создаем группу в ВВ
                    bbGroupPrimaryId = BBCourseUtils.createNewGroup(coursePrimaryId, groupTitle, groupDescription);
                    rollbackCleaner.addGroup(coursePrimaryId, bbGroupPrimaryId);

                    // Создаем группу в нашей базе
                    bbGroup = new BbGroup();
                    bbGroup.setBbPrimaryId(bbGroupPrimaryId);
                    bbGroup.setTitle(groupTitle);
                    bbGroup.setBbCourse2TrJournalRel(BBUpdateEppEduGroupDaemon.<BbCourse2TrJournalRel>proxy(relId));
                    bbGroup.setGroup(group);
                    save(bbGroup);

                    newGroups.put(newGroupKey, bbGroup);
                }
                else
                {
                    bbGroupPrimaryId = (String) item[bbGroupPrimaryIdCol];
                    if (bbGroupPrimaryId == null)
                        bbGroupPrimaryId = bbGroup.getBbPrimaryId();
                }

                MultiKey newGroupMembershipKey = new MultiKey(bbGroupPrimaryId, courseStudent.getBbPrimaryId());
                if (newMemberships.contains(newGroupMembershipKey))
                    continue;

                //System.out.println("Create new BB groupMembership: " + this.<BbCourse>get(courseId).getTitle() + " - " + bbGroup.getTitle() + " - " + this.<Person>get(personId).getFullFio());

                // Создаем нового участника группы в Blackboard
                String newGroupMembershipId = BBMembershipUtils.saveGroupMembership(coursePrimaryId, courseStudent.getBbPrimaryId(), bbGroupPrimaryId);
                rollbackCleaner.addGroupMembership(coursePrimaryId, newGroupMembershipId);

                newMemberships.add(newGroupMembershipKey);

                // Сохраняем его к нам в базу
                BbGroupMembership groupMembership = new BbGroupMembership();
                groupMembership.setBbCourseStudent(courseStudent);
                groupMembership.setBbGroup(bbGroup);
                groupMembership.setGroupRow(BBUpdateEppEduGroupDaemon.<EppRealEduGroup4LoadTypeRow>proxy(groupRowId));
                groupMembership.setBbPrimaryId(newGroupMembershipId);
                save(groupMembership);
            }
        }
        finally
        {
            Debug.end();
        }
    }


    /**
     * Класс для отката действий в ВВ, в случае отката транзакции - удаляются созданные в ВВ объекты, если что-то пошло не так.
     */
    private class NewBBObjectsCreationRollbackAction extends TransactionCompleteAction<Boolean>
    {
        private Map<String, List<String>> groupsMap = new HashMap<>();
        private Map<String, List<String>> groupMembershipMap = new HashMap<>();
        private Map<String, List<String>> courseMembershipMap = new HashMap<>();

        private NewBBObjectsCreationRollbackAction(Session session)
        {
            register(session);
        }

        public void addGroup(String coursePrimaryId, String newGroupId)
        {
            logger.info("Created new group: " + newGroupId + " on course " + coursePrimaryId);
            SafeMap.safeGet(groupsMap, coursePrimaryId, ArrayList.class).add(newGroupId);
        }

        public void addCourseMembership(String coursePrimaryId, String newCourseMembershipId)
        {
            logger.info("Created new course membership: " + newCourseMembershipId + " on course " + coursePrimaryId);
            SafeMap.safeGet(courseMembershipMap, coursePrimaryId, ArrayList.class).add(newCourseMembershipId);
        }

        public void addGroupMembership(String coursePrimaryId, String newGroupMembershipId)
        {
            logger.info("Created new group membership: " + newGroupMembershipId);
            SafeMap.safeGet(groupMembershipMap, coursePrimaryId, ArrayList.class).add(newGroupMembershipId);
        }

        @Override
        public void afterCompletion(Session session, int status, Boolean beforeCompleteResult)
        {
            if (status == Status.STATUS_ROLLEDBACK)
            {
                logger.error("Transaction is rolled back. Start delete created objects from Blackboard");

                if (!groupMembershipMap.isEmpty())
                {
                    for (Map.Entry<String, List<String>> entry : groupMembershipMap.entrySet())
                    {
                        if (!BBMembershipUtils.deleteGroupMemberships(entry.getKey(), entry.getValue()))
                            logger.error(entry.getValue().size() + " group memberships was not deleted from course " + entry.getKey());
                    }
                }

                if (!courseMembershipMap.isEmpty())
                {
                    for (Map.Entry<String, List<String>> entry : courseMembershipMap.entrySet())
                    {
                        if (!BBMembershipUtils.deleteCourseMemberships(entry.getKey(), entry.getValue()))
                            logger.error(entry.getValue().size() + " course memberships was not deleted from course " + entry.getKey());
                    }
                }

                if (!groupsMap.isEmpty())
                {
                    for (Map.Entry<String, List<String>> entry : groupsMap.entrySet())
                    {
                        if (!BBCourseUtils.deleteGroups(entry.getKey(), entry.getValue()))
                            logger.error(entry.getValue().size() + " groups was not deleted from course " + entry.getKey());
                    }
                }
            }
        }
    }

}