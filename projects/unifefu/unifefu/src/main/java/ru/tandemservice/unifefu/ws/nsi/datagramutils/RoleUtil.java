/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import org.apache.commons.collections.map.HashedMap;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRole;
import ru.tandemservice.unifefu.ws.nsi.datagram.RoleType;

import javax.xml.namespace.QName;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 12.02.2015
 */
public class RoleUtil implements INsiEntityUtil<RoleType, FefuNsiRole>
{
    public static final String ID_NSI_FIELD = "ID";
    public static final String GUID_FIELD = "GUID";
    public static final String ROLE_NAME_FIELD = "RoleName";

    protected List<String> NSI_FIELDS;
    protected List<String> ENTITY_FIELDS;

    protected Map<String, String> NSI_TO_OB_FIELDS_MAP;
    protected Map<String, String> OB_TO_NSI_FIELDS_MAP;

    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            NSI_FIELDS = new ArrayList<>();
            NSI_FIELDS.add(ID_NSI_FIELD);
            NSI_FIELDS.add(ROLE_NAME_FIELD);
        }

        return NSI_FIELDS;
    }

    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            ENTITY_FIELDS = new ArrayList<>();
            ENTITY_FIELDS.add(GUID_FIELD);
            ENTITY_FIELDS.add(FefuNsiRole.name().s());
        }

        return ENTITY_FIELDS;
    }

    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            NSI_TO_OB_FIELDS_MAP = new HashedMap();
            NSI_TO_OB_FIELDS_MAP.put(ID_NSI_FIELD, GUID_FIELD);
            NSI_TO_OB_FIELDS_MAP.put(ROLE_NAME_FIELD, FefuNsiRole.name().s());
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            OB_TO_NSI_FIELDS_MAP = new HashedMap();
            OB_TO_NSI_FIELDS_MAP.put(GUID_FIELD, ID_NSI_FIELD);
            OB_TO_NSI_FIELDS_MAP.put(FefuNsiRole.name().s(), ROLE_NAME_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    @Override
    public long getNsiIdHash(RoleType nsiEntity)
    {
        return MD5HashBuilder.getCheckSum(nsiEntity.getID().toLowerCase());
    }

    @Override
    public long getNsiCodeHash(RoleType nsiEntity)
    {
        return 0L;
    }

    @Override
    public long getNsiTitleHash(RoleType nsiEntity, boolean caseInsensitive)
    {
        return 0L;
    }

    @Override
    public long getNsiShortTitleHash(RoleType nsiEntity, boolean caseInsensitive)
    {
        return 0L;
    }

    public long getNsiRoleNameHash(RoleType nsiEntity, boolean caseInsensitive)
    {
        if (null == nsiEntity.getShortTitle()) return 0;
        if (caseInsensitive)
            return MD5HashBuilder.getCheckSum(nsiEntity.getRoleName().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(nsiEntity.getRoleName());
    }

    @Override
    public long getNsiFieldHash(RoleType nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case ID_NSI_FIELD:
                return getNsiIdHash(nsiEntity);
            case ROLE_NAME_FIELD:
                return getNsiRoleNameHash(nsiEntity, caseInsensitive);
            default:
                throw new UnsupportedOperationException("Field '" + fieldName + "' not found for '" + nsiEntity.getClass().getSimpleName() + "' entity type.");
        }
    }

    @Override
    public boolean isFieldEmpty(RoleType nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case ID_NSI_FIELD:
                return null == nsiEntity.getID();
            case ROLE_NAME_FIELD:
                return null == nsiEntity.getRoleName();
            default:
                throw new UnsupportedOperationException("Field '" + fieldName + "' not found for '" + nsiEntity.getClass().getSimpleName() + "' entity type.");
        }
    }

    @Override
    public long getNsiEntityHash(RoleType nsiEntity, boolean takeId, boolean takeCode, boolean caseInsensitive)
    {
        Set<String> excludeFieldsSet = new HashSet();
        if (!takeId) excludeFieldsSet.add(ID_NSI_FIELD);

        MD5HashBuilder builder = new MD5HashBuilder();
        for (String fieldName : getNsiFields())
        {
            if (!excludeFieldsSet.contains(fieldName))
                builder.add(getNsiFieldHash(nsiEntity, fieldName, caseInsensitive));
        }
        return builder.getCheckSum();
    }

    @Override
    public long getNsiEntityHash(RoleType nsiEntity, boolean caseInsensitive)
    {
        return getNsiEntityHash(nsiEntity, true, true, caseInsensitive);
    }

    @Override
    public long getGuidHash(FefuNsiIds nsiId)
    {
        if (null == nsiId || null == nsiId.getGuid()) return 0;
        return MD5HashBuilder.getCheckSum(nsiId.getGuid().toLowerCase());
    }

    @Override
    public long getCodeHash(FefuNsiRole entity)
    {
        return 0;
    }

    @Override
    public long getTitleHash(FefuNsiRole entity, boolean caseInsensitive)
    {
        return 0;
    }

    @Override
    public long getShortTitleHash(FefuNsiRole entity, boolean caseInsensitive)
    {
        return 0;
    }

    public long getRoleNameHash(FefuNsiRole entity, boolean caseInsensitive)
    {
        if (null != entity.getName())
        {
            if (caseInsensitive)
                return MD5HashBuilder.getCheckSum(entity.getName().trim().toUpperCase());
            return MD5HashBuilder.getCheckSum(entity.getName());
        }

        return 0;
    }

    @Override
    public long getEntityFieldHash(FefuNsiRole entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case GUID_FIELD:
                return getGuidHash(nsiId);
            case FefuNsiRole.P_NAME:
                return getRoleNameHash(entity, caseInsensitive);
            default:
                throw new UnsupportedOperationException("Field '" + fieldName + "' not found for '" + entity.getClass().getSimpleName() + "' entity type.");
        }
    }

    @Override
    public long getEntityHash(FefuNsiRole entity, FefuNsiIds nsiIds, boolean takeId, boolean takeCode, boolean caseInsensitive)
    {
        Set<String> excludeFieldsSet = new HashSet();
        if (!takeId) excludeFieldsSet.add(GUID_FIELD);

        MD5HashBuilder builder = new MD5HashBuilder();
        for (String fieldName : getEntityFields())
        {
            if (!excludeFieldsSet.contains(fieldName))
                builder.add(getEntityFieldHash(entity, nsiIds, fieldName, caseInsensitive));
        }
        return builder.getCheckSum();
    }

    @Override
    public long getEntityHash(FefuNsiRole entity, FefuNsiIds nsiIds, boolean caseInsensitive)
    {
        return getEntityHash(entity, nsiIds, true, true, caseInsensitive);
    }

    private String getEntityFieldNameByNsiEntityFieldName(String nsiFieldName)
    {
        if (!getNsiToObFieldsMap().containsKey(nsiFieldName))
            throw new UnsupportedOperationException("Field '" + nsiFieldName + "' not found.");
        return getNsiToObFieldsMap().get(nsiFieldName);
    }

    private String getNsiEntityFieldNameByEntityFieldName(String fieldName)
    {
        if (!getObToNsiFieldsMap().containsKey(fieldName))
            throw new UnsupportedOperationException("Field '" + fieldName + "' not found.");
        return getObToNsiFieldsMap().get(fieldName);
    }

    @Override
    public boolean isTwoObjectsIdenticalInGeneral(RoleType nsiEntity, FefuNsiRole entity, FefuNsiIds nsiIds)
    {
        return getEntityHash(entity, nsiIds, false, false, true) == getNsiEntityHash(nsiEntity, false, false, true);
    }

    @Override
    public boolean isTwoObjectsIdenticalFieldByField(RoleType nsiEntity, FefuNsiRole entity, FefuNsiIds nsiIds)
    {
        // Поскольку идентичность примитивных справочников сравнивается исключительно по названию элемента,
        // то hash сравнения поле-в-поле совпадает с hash'ем всего объекта
        return isTwoObjectsIdenticalInGeneral(nsiEntity, entity, nsiIds);
    }

    @Override
    public boolean isFieldChanged(RoleType nsiEntity, FefuNsiRole entity, FefuNsiIds nsiIds, String nsiFieldName)
    {
        if (isFieldEmpty(nsiEntity, nsiFieldName))
            return false; // Поля, в которых сидит null мы не имеем права обновлять, поскольку из НСИ приходят только изменившиеся поля
        return getNsiFieldHash(nsiEntity, nsiFieldName, false) != getEntityFieldHash(entity, nsiIds, getEntityFieldNameByNsiEntityFieldName(nsiFieldName), false);
    }

    @Override
    public boolean isFieldChanged(FefuNsiRole entity, RoleType nsiEntity, FefuNsiIds nsiIds, String fieldName)
    {
        if (isFieldEmpty(nsiEntity, getNsiEntityFieldNameByEntityFieldName(fieldName)))
            return false; // Поля, в которых сидит null мы не имеем права обновлять, поскольку из НСИ приходят только изменившиеся поля
        return getEntityFieldHash(entity, nsiIds, fieldName, false) != getNsiFieldHash(nsiEntity, getNsiEntityFieldNameByEntityFieldName(fieldName), false);
    }

    @Override
    public boolean isEntityChanged(RoleType nsiEntity, FefuNsiRole entity, FefuNsiIds nsiIds)
    {
        return getEntityHash(entity, nsiIds, false, false, false) != getNsiEntityHash(nsiEntity, false, false, false);
    }

    @Override
    public List<String> getMergeDuplicatesList(RoleType nsiEntity)
    {
        List<String> result = new ArrayList<>();
        String mergeDuplicates = null != nsiEntity.getMergeDublicates() ? nsiEntity.getMergeDublicates() : nsiEntity.getOtherAttributes().get(new QName("mergeDuplicates"));

        if (null != mergeDuplicates)
        {
            String[] duplicates = mergeDuplicates.split(";");
            for (String duplicate : duplicates) result.add(duplicate.trim());
        }

        if (result.size() < 2) return null;
        return result;
    }
}