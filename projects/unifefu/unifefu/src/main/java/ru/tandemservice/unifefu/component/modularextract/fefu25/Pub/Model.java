/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu25.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public class Model extends ModularStudentExtractPubModel<FefuConditionalTransferCourseStuExtract>
{
}
