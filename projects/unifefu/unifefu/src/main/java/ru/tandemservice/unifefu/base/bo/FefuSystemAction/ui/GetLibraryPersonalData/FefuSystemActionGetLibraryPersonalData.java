/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.GetLibraryPersonalData;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Dmitry Seleznev
 * @since 27.08.2013
 */
@Configuration
public class FefuSystemActionGetLibraryPersonalData extends BusinessComponentManager
{
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder().create();
    }
}