/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import ru.tandemservice.unifefu.ws.nsi.datagram.AgreementType;
import ru.tandemservice.unifefu.ws.nsi.datagram.ContractorType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import javax.xml.namespace.QName;


/**
 * @author Dmitry Seleznev
 * @since 08.05.2014
 */
public class NsiLocalAccessServiceAgreementTest extends NsiLocalAccessBaseTest
{
    public static final String[][] RETRIEVE_WHOLE_PARAMS = new String[][]{{null}};
    public static final String[][] RETRIEVE_NON_EXISTS_PARAMS = new String[][]{{"aaaaaaaa-6a03-3221-90ad-83af3fcd7b64"}};
    public static final String[][] RETRIEVE_SINGLE_PARAMS = new String[][]{{"90800349-619a-11e0-a335-001a4be8a71c"}};
    public static final String[][] RETRIEVE_FEW_PARAMS = new String[][]{{"90800349-619a-11e0-a335-001a4be8a71c"}, {"413ef0ad-df05-3767-a50b-c36871a297e6"}, {"aaaaaaaa-6a03-3221-90ad-83af3fcd7b64"}, {"64b89a99-f2cb-3275-83b6-666d85144294"}};

    public static final String[][] INSERT_NEW_PARAMS = new String[][]{{"xxxxxxxx-619a-11e0-a335-xxxxxxxxxxxY", "Тестовый Договор", "345345", "16407a76-2a5b-4a6f-ac39-5c5f4dc1594c"}};
    public static final String[][] INSERT_NEW_ANALOG_PARAMS = new String[][]{{"90800349-619a-11e0-a335-xxxxxxxxxxxZ", "Тестовый профессор", "3"}};
    public static final String[][] INSERT_NEW_GUID_EXISTS_PARAMS = new String[][]{{"90800349-619a-11e0-a335-xxxxxxxxxxxZ", "Тестовый профессор1", "31"}};
    public static final String[][] INSERT_NEW_MERGE_GUID_EXISTS_PARAMS = new String[][]{{"90800349-619a-11e0-a335-yyyyyyyyyyyY", "Тестовый профессор2", "5", "90800349-619a-11e0-a335-yyyyyyyyyyyY; 90800349-619a-11e0-a335-xxxxxxxxxxxZ; 90800349-619a-11e0-a335-xxxxxxxxxxx1"}};
    public static final String[] INSERT_MASS_TEMPLATE_PARAMS = new String[]{"Тестовый профессор", "mass"};

    public static final String[][] DELETE_EMPTY_PARAMS = new String[][]{{null}};
    public static final String[][] DELETE_NON_EXISTS_PARAMS = new String[][]{{"0b9f3ef1-98e6-39a7-8139-bb46f462ccbX"}};
    public static final String[][] DELETE_SINGLE_PARAMS = new String[][]{{"f38bfdf2-9bcd-3dbc-8005-4777cd1f43c0"}};
    public static final String[][] DELETE_NON_DELETABLE_PARAMS = new String[][]{{"9080034d-619a-11e0-a335-001a4be8a71c"}};
    public static final String[][] DELETE_MASS_PARAMS = new String[][]{{"5b9433ef-52b4-33d3-b901-9c2acc5663bc"}};

    @Override
    protected INsiEntity createNsiEntity(String[] fieldValues)
    {
        AgreementType entity = NsiLocalAccessServiceTestUtil.FACTORY.createAgreementType();
        if (null != fieldValues)
        {
            if (fieldValues.length > 0) entity.setID(fieldValues[0]);
            if (fieldValues.length > 1) entity.setAgreementName(fieldValues[1]);
            if (fieldValues.length > 2) entity.setAgreementID(fieldValues[2]);
            if (fieldValues.length > 3)
            {
                AgreementType.ContractorID contractorID = NsiLocalAccessServiceTestUtil.FACTORY.createAgreementTypeContractorID();
                ContractorType contractor = NsiLocalAccessServiceTestUtil.FACTORY.createContractorType();
                contractor.setID(fieldValues[3]);
                contractorID.setContractor(contractor);
                entity.setContractorID(contractorID);
            }
            if (fieldValues.length > 4 && null != fieldValues[4])
                entity.getOtherAttributes().put(new QName("mergeDuplicates"), fieldValues[4]);
        }
        return entity;
    }

}