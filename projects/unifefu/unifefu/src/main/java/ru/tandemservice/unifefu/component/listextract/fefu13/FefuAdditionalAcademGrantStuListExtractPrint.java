package ru.tandemservice.unifefu.component.listextract.fefu13;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuAdditionalAcademGrantStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author ilunin
 * @since 24.10.2014
 */
public class FefuAdditionalAcademGrantStuListExtractPrint implements IPrintFormCreator<FefuAdditionalAcademGrantStuListExtract>, IListParagraphPrintFormCreator<FefuAdditionalAcademGrantStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuAdditionalAcademGrantStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        Student student = extract.getEntity();

        CommonExtractPrint.modifyEducationStr(modifier, student.getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());

        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        Group group = student.getGroup();
        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");

        modifier.put("promotedActivityType_GF", DeclinableManager.instance().dao().getPropertyValue(extract.getPromotedActivityType(), "title", IUniBaseDao.instance.get().getByCode(InflectorVariant.class, InflectorVariantCodes.RU_GENITIVE)) );
        modifier.put("dateStart", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDateStart()));
        modifier.put("dateEnd", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDateEnd()));

        modifier.put("finAidSize", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getAdditionalAcademGrantSize()));
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(group), "fefuShortFastExtendedOptionalText");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuAdditionalAcademGrantStuListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuAdditionalAcademGrantStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuAdditionalAcademGrantStuListExtract firstExtract)
    {
    }
}