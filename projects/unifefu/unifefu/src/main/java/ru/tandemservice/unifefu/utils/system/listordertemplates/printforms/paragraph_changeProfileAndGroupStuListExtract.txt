\keep\keepn\fi709\qj {parNumberConditional}Следующих {studentsCategory_G} {course} курса{groupInternal_G}, обучающихся{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStr} по {fefuEducationStrOld_D} {orgUnitPrep} {formativeOrgUnitStr_P} по {developForm_DF} форме обучения, перевести для обучения по {eduTypeString}, {profileString_D} «{specializationNew}» и перераспределить в группу:
 \par\pard\par\fi709
 Группа {groupNew}
 \par\pard\par
 {STUDENT_LIST}
 \par\pard