package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.unifefu.entity.ws.MdbViewStudent;
import ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительный статус студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewStudentCustomStateGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState";
    public static final String ENTITY_NAME = "mdbViewStudentCustomState";
    public static final int VERSION_HASH = 469981656;
    private static IEntityMeta ENTITY_META;

    public static final String L_MDB_VIEW_STUDENT = "mdbViewStudent";
    public static final String L_STUDENT_CUSTOM_STATE = "studentCustomState";
    public static final String P_CUSTOM_STATE = "customState";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_DESCRIPTION = "description";

    private MdbViewStudent _mdbViewStudent;     // Студент
    private StudentCustomState _studentCustomState;     // Дополнительный статус студента
    private String _customState;     // Дополнительный статус студента
    private Date _beginDate;     // Дата начала действия статуса
    private Date _endDate;     // Дата окончания действия статуса
    private String _description;     // Примечание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public MdbViewStudent getMdbViewStudent()
    {
        return _mdbViewStudent;
    }

    /**
     * @param mdbViewStudent Студент. Свойство не может быть null.
     */
    public void setMdbViewStudent(MdbViewStudent mdbViewStudent)
    {
        dirty(_mdbViewStudent, mdbViewStudent);
        _mdbViewStudent = mdbViewStudent;
    }

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     */
    @NotNull
    public StudentCustomState getStudentCustomState()
    {
        return _studentCustomState;
    }

    /**
     * @param studentCustomState Дополнительный статус студента. Свойство не может быть null.
     */
    public void setStudentCustomState(StudentCustomState studentCustomState)
    {
        dirty(_studentCustomState, studentCustomState);
        _studentCustomState = studentCustomState;
    }

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCustomState()
    {
        return _customState;
    }

    /**
     * @param customState Дополнительный статус студента. Свойство не может быть null.
     */
    public void setCustomState(String customState)
    {
        dirty(_customState, customState);
        _customState = customState;
    }

    /**
     * @return Дата начала действия статуса.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала действия статуса.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания действия статуса.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания действия статуса.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Примечание.
     */
    public String getDescription()
    {
        initLazyForGet("description");
        return _description;
    }

    /**
     * @param description Примечание.
     */
    public void setDescription(String description)
    {
        initLazyForSet("description");
        dirty(_description, description);
        _description = description;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewStudentCustomStateGen)
        {
            setMdbViewStudent(((MdbViewStudentCustomState)another).getMdbViewStudent());
            setStudentCustomState(((MdbViewStudentCustomState)another).getStudentCustomState());
            setCustomState(((MdbViewStudentCustomState)another).getCustomState());
            setBeginDate(((MdbViewStudentCustomState)another).getBeginDate());
            setEndDate(((MdbViewStudentCustomState)another).getEndDate());
            setDescription(((MdbViewStudentCustomState)another).getDescription());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewStudentCustomStateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewStudentCustomState.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewStudentCustomState();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mdbViewStudent":
                    return obj.getMdbViewStudent();
                case "studentCustomState":
                    return obj.getStudentCustomState();
                case "customState":
                    return obj.getCustomState();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "description":
                    return obj.getDescription();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mdbViewStudent":
                    obj.setMdbViewStudent((MdbViewStudent) value);
                    return;
                case "studentCustomState":
                    obj.setStudentCustomState((StudentCustomState) value);
                    return;
                case "customState":
                    obj.setCustomState((String) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mdbViewStudent":
                        return true;
                case "studentCustomState":
                        return true;
                case "customState":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "description":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mdbViewStudent":
                    return true;
                case "studentCustomState":
                    return true;
                case "customState":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "description":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mdbViewStudent":
                    return MdbViewStudent.class;
                case "studentCustomState":
                    return StudentCustomState.class;
                case "customState":
                    return String.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "description":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewStudentCustomState> _dslPath = new Path<MdbViewStudentCustomState>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewStudentCustomState");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getMdbViewStudent()
     */
    public static MdbViewStudent.Path<MdbViewStudent> mdbViewStudent()
    {
        return _dslPath.mdbViewStudent();
    }

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getStudentCustomState()
     */
    public static StudentCustomState.Path<StudentCustomState> studentCustomState()
    {
        return _dslPath.studentCustomState();
    }

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getCustomState()
     */
    public static PropertyPath<String> customState()
    {
        return _dslPath.customState();
    }

    /**
     * @return Дата начала действия статуса.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания действия статуса.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    public static class Path<E extends MdbViewStudentCustomState> extends EntityPath<E>
    {
        private MdbViewStudent.Path<MdbViewStudent> _mdbViewStudent;
        private StudentCustomState.Path<StudentCustomState> _studentCustomState;
        private PropertyPath<String> _customState;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _description;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getMdbViewStudent()
     */
        public MdbViewStudent.Path<MdbViewStudent> mdbViewStudent()
        {
            if(_mdbViewStudent == null )
                _mdbViewStudent = new MdbViewStudent.Path<MdbViewStudent>(L_MDB_VIEW_STUDENT, this);
            return _mdbViewStudent;
        }

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getStudentCustomState()
     */
        public StudentCustomState.Path<StudentCustomState> studentCustomState()
        {
            if(_studentCustomState == null )
                _studentCustomState = new StudentCustomState.Path<StudentCustomState>(L_STUDENT_CUSTOM_STATE, this);
            return _studentCustomState;
        }

    /**
     * @return Дополнительный статус студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getCustomState()
     */
        public PropertyPath<String> customState()
        {
            if(_customState == null )
                _customState = new PropertyPath<String>(MdbViewStudentCustomStateGen.P_CUSTOM_STATE, this);
            return _customState;
        }

    /**
     * @return Дата начала действия статуса.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(MdbViewStudentCustomStateGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания действия статуса.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(MdbViewStudentCustomStateGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewStudentCustomState#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(MdbViewStudentCustomStateGen.P_DESCRIPTION, this);
            return _description;
        }

        public Class getEntityClass()
        {
            return MdbViewStudentCustomState.class;
        }

        public String getEntityName()
        {
            return "mdbViewStudentCustomState";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
