/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu2.ParagraphAddEdit;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 15.03.2013
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuAdmitToStateExamsStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentCustomStateCI(DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), UniFefuDefines.ADMITTED_TO_STATE_EXAMES));
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setCourseAndGroupDisabled(!model.isParagraphOnlyOneInTheOrder());

        FefuAdmitToStateExamsStuListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            model.setCourse(firstExtract.getCourse());
            model.setGroup(firstExtract.getGroup());
            model.setCompensationType(firstExtract.getCompensationType());
            model.setNotNeedAdmissionToGIA(firstExtract.isNotNeedAdmissionToGIA());
        }
    }

    @Override
    public void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourse()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected FefuAdmitToStateExamsStuListExtract createNewInstance(Model model)
    {
        return new FefuAdmitToStateExamsStuListExtract();
    }

    @Override
    protected void fillExtract(FefuAdmitToStateExamsStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
        extract.setNotNeedAdmissionToGIA(model.getNotNeedAdmissionToGIA());
    }
}