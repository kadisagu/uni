/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTrHomePage.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author Nikolay Fedorovskih
 * @since 13.02.2014
 */
@Transactional
public interface IFefuTrHomePageDao extends INeedPersistenceSupport
{
    @Transactional(propagation= Propagation.SUPPORTS, readOnly = true)
    EppYearPart getLastYearPart();

    void doSendToCoordinateRegistryElement(TrJournal journal);
}