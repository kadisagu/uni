package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Разработчик
 */
public class FefuDeveloper extends FefuDeveloperGen
{
    public FefuDeveloper()
    {

    }

    public FefuDeveloper(EppEduPlanVersionBlock block)
    {
        this.setBlock(block);
    }
}