/* $Id$ */
package ru.tandemservice.unifefu.base.ext.ExternalOrgUnit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author nvankov
 * @since 8/13/13
 */
@Configuration
public class ExternalOrgUnitMangerExt extends BusinessObjectExtensionManager
{
}
