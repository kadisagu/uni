package ru.tandemservice.unifefu.component.report.EntrantsRating.EntrantsRatingAdd;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uniec.component.report.EntrantsRating.EntrantsRatingAdd.Model;

@Zlo
public class DAO extends ru.tandemservice.uniec.component.report.EntrantsRating.EntrantsRatingAdd.DAO {

    @Override
    protected DatabaseFile getReportContent(Model model, Session session)
    {
        return new EntrantsRatingReportBuilder(model, session).getContent();
    }
}
