/* $Id$ */
package ru.tandemservice.unifefu.utils;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.util.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author Dmitry Seleznev
 * @since 31.05.2013
 */
public class FefuMailSender
{
    public static void sendEmail(String to, String subject, String text)
    {
        // создаем email сообщение
        String host = ApplicationRuntime.getProperty("fefu.entrant.mail.host");
        String from = ApplicationRuntime.getProperty("fefu.entrant.mail.from");
        String name = ApplicationRuntime.getProperty("fefu.entrant.mail.name");
        String pass = ApplicationRuntime.getProperty("fefu.entrant.mail.pass");

        notBlank(host, "host");
        notBlank(host, "from");
        if (!StringUtils.hasLength(name)) name = null;
        if (!StringUtils.hasLength(pass)) pass = null;

        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject(subject);
        message.setTo(to);
        message.setFrom(from);
        message.setText(text);

        JavaMailSenderImpl mail = new JavaMailSenderImpl();
        mail.setHost(host);
        mail.setUsername(name);
        mail.setPassword(pass);
        mail.setDefaultEncoding("UTF-8");

        try
        {
            mail.send(message);
        } catch (MailException e)
        {
            e.printStackTrace();
            System.out.println("Почтовый сервер отказывается отсылать письмо на указанный адрес.");
        }
    }

    private static void notBlank(String string, String name)
    {
        if (!StringUtils.hasLength(string))
            throw new RuntimeException("Field '" + name + "' cannot be empty");
    }
}