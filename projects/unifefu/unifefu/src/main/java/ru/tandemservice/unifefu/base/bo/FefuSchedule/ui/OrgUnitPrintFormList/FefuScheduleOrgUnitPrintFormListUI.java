/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSchedule.ui.OrgUnitPrintFormList;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.ui.PrintFormAdd.FefuSchedulePrintFormAdd;
import ru.tandemservice.unifefu.entity.SchedulePrintFormFefuExt;
import ru.tandemservice.unispp.base.entity.SppSchedulePrintForm;

/**
 * @author nvankov
 * @since 9/25/13
 */
public class FefuScheduleOrgUnitPrintFormListUI extends OrgUnitUIPresenter
{
    private BaseSearchListDataSource _printFormDS;

    public BaseSearchListDataSource getPrintFormDS()
    {
        if (null == _printFormDS)
            _printFormDS = _uiConfig.getDataSource(FefuScheduleOrgUnitPrintFormList.PRINT_FORM_DS);
        return _printFormDS;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuScheduleOrgUnitPrintFormList.PRINT_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.put("orgUnitId", getOrgUnit().getId());
            dataSource.putAll(_uiSettings.getAsMap(true, "admin", "groups"));
        }

        if (FefuScheduleOrgUnitPrintFormList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put("orgUnitId", getOrgUnit().getId());
        }
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickDownload()
    {
        SchedulePrintFormFefuExt fefuPrintForm = DataAccessServices.dao().get(getListenerParameterAsLong());
        SppSchedulePrintForm printForm = fefuPrintForm.getSppSchedulePrintForm();
        byte[] content = printForm.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("xls").fileName("schedule.xls").document(content), true);
    }

    public void onClickAddSchedulePrint()
    {
        _uiActivation.asRegion(FefuSchedulePrintFormAdd.class).parameter("orgUnitId", getOrgUnit().getId()).activate();
    }
}
