/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.settings.FefuStudentPaymentsAndBonusesSizeEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 12.03.2013
 */
public interface IDAO extends IUniDao<Model>
{
}