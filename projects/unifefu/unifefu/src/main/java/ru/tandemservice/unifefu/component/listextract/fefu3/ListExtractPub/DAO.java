/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu3.ListExtractPub;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 18.03.2013
 */
public class DAO extends AbstractListExtractPubDAO<FefuAdmitToDiplomaStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentCustomStateCI(DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), UniFefuDefines.ADMITTED_TO_DIPLOMA));
    }
}