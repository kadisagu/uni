/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu3.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 19.03.2013
 */
public class FefuAdmitToDiplomaParagraphWrapper implements Comparable<FefuAdmitToDiplomaParagraphWrapper>
{
    private final CompensationType _compensationType;
    private final ListStudentExtract _firstExtract;

    public FefuAdmitToDiplomaParagraphWrapper(CompensationType compensationType, ListStudentExtract firstExtract)
    {
        _compensationType = compensationType;
        _firstExtract = firstExtract;
    }

    private final List<FefuAdmitToDiplomaParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<>();

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public List<FefuAdmitToDiplomaParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FefuAdmitToDiplomaParagraphWrapper that = (FefuAdmitToDiplomaParagraphWrapper) o;

        return _compensationType.equals(that._compensationType);
    }

    @Override
    public int hashCode()
    {
        return _compensationType.hashCode();
    }

    @Override
    public int compareTo(FefuAdmitToDiplomaParagraphWrapper o)
    {
        return _compensationType.getCode().compareTo(o.getCompensationType().getCode());
    }
}