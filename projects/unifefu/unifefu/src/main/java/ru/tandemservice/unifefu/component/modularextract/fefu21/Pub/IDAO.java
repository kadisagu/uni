/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu21.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuExcludeStuDPOExtract, Model>
{
}