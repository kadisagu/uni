/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu19.AddEdit;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract;

import java.util.Date;
import java.util.List;

/**
* @author Ekaterina Zvereva
* @since 14.11.2014
*/
public class Model extends CommonModularStudentExtractAddEditModel<FefuTransfAcceleratedTimeStuExtract>
{
    private Group _groupNew;
    private ISelectModel _groupNewListModel;
    private CommonExtractModel _eduModel;

    private DevelopCondition _developConditionNew = CatalogManager.instance().dao().getCatalogItem(DevelopCondition.class, DevelopConditionCodes.ACCELERATED_PERIOD);
    private DevelopPeriod _developPeriodNew;

    private ISelectModel _developPeriodsListModel;

    public CommonExtractModel getEduModel()
    {
        return _eduModel;
    }

    public void setEduModel(CommonExtractModel eduModel)
    {
        _eduModel = eduModel;
    }

    private String _season;
    private List<String> _seasons;


    public String getSeason()
    {
        return _season;
    }

    public void setSeason(String season)
    {
        _season = season;
    }

    public List<String> getSeasons()
    {
        return _seasons;
    }

    public void setSeasons(List<String> seasons)
    {
        _seasons = seasons;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    public void setGroupNew(Group groupNew)
    {
        _groupNew = groupNew;
    }


    public ISelectModel getGroupNewListModel()
    {
        return _groupNewListModel;
    }

    public void setGroupNewListModel(ISelectModel groupNewListModel)
    {
        _groupNewListModel = groupNewListModel;
    }

    public DevelopCondition getDevelopConditionNew()
    {
        return _developConditionNew;
    }

    public void setDevelopConditionNew(DevelopCondition developConditionNew)
    {
        _developConditionNew = developConditionNew;
    }

    public DevelopPeriod getDevelopPeriodNew()
    {
        return _developPeriodNew;
    }

    public void setDevelopPeriodNew(DevelopPeriod developPeriodNew)
    {
        _developPeriodNew = developPeriodNew;
    }

    public ISelectModel getDevelopPeriodsListModel()
    {
        return _developPeriodsListModel;
    }

    public void setDevelopPeriodsListModel(ISelectModel developPeriodsListModel)
    {
        _developPeriodsListModel = developPeriodsListModel;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _eduModel.getFormativeOrgUnit();
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _eduModel.getTerritorialOrgUnit();
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _eduModel.getEducationLevelsHighSchool();
    }

    public DevelopForm getDevelopForm()
    {
        return _eduModel.getDevelopForm();
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developConditionNew;
    }

    public DevelopTech getDevelopTech()
    {
        return _eduModel.getDevelopTech();
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriodNew;
    }

    public EducationLevels getParentEduLevel()
    {
        return EducationOrgUnitUtil.getParentLevel(_eduModel.getGroup().getEducationOrgUnit().getEducationLevelHighSchool());
    }

}