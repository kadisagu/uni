/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu10.ParagraphAddEdit;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract;

import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 11/21/13
 */
public class Model extends AbstractListParagraphAddEditModel<FullStateMaintenanceEnrollmentStuListExtract> implements IGroupModel
{
    private Course _course;
    private Group _group;
    private CompensationType _compensationType;
    private StudentCustomStateCI _studentCustomStateCI;

    private Date _enrollDate;     // Дата зачисления
    private String _protocol;     // Протокол
    private Date _startPaymentDate;     // Начисление выплат за период с
    private Date paymentEndDate;
    private EmployeePost _responsibleForPayments;     // Ответственный за начисление выплат
    private EmployeePost _responsibleForAgreement;     // Ответственный за согласование начислений выплат

    private ISelectModel _employeePostModel;
    private ISelectModel _groupListModel;
    private List<CompensationType> _compensationTypeList;

    private boolean _editCourseDisabled;

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        _group = group;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public StudentCustomStateCI getStudentCustomStateCI()
    {
        return _studentCustomStateCI;
    }

    public void setStudentCustomStateCI(StudentCustomStateCI studentCustomStateCI)
    {
        _studentCustomStateCI = studentCustomStateCI;
    }

    public Date getEnrollDate()
    {
        return _enrollDate;
    }

    public void setEnrollDate(Date enrollDate)
    {
        _enrollDate = enrollDate;
    }

    public String getProtocol()
    {
        return _protocol;
    }

    public void setProtocol(String protocol)
    {
        _protocol = protocol;
    }

    public Date getStartPaymentDate()
    {
        return _startPaymentDate;
    }

    public void setStartPaymentDate(Date startPaymentDate)
    {
        _startPaymentDate = startPaymentDate;
    }

    public EmployeePost getResponsibleForPayments()
    {
        return _responsibleForPayments;
    }

    public void setResponsibleForPayments(EmployeePost responsibleForPayments)
    {
        _responsibleForPayments = responsibleForPayments;
    }

    public EmployeePost getResponsibleForAgreement()
    {
        return _responsibleForAgreement;
    }

    public void setResponsibleForAgreement(EmployeePost responsibleForAgreement)
    {
        _responsibleForAgreement = responsibleForAgreement;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }

    public boolean isEditCourseDisabled()
    {
        return _editCourseDisabled;
    }

    public void setEditCourseDisabled(boolean editCourseDisabled)
    {
        _editCourseDisabled = editCourseDisabled;
    }

    public Date getPaymentEndDate()
    {
        return paymentEndDate;
    }

    public void setPaymentEndDate(Date paymentEndDate)
    {
        this.paymentEndDate = paymentEndDate;
    }
}
