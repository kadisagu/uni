\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
{reason}\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_A}\cell\b0
{studentCategory_A} {courseOld} курса{groupInternalOld_G}, {custom_learned_A}{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStrOld} по {fefuEducationStrOld_D} по {developFormOld_DF} форме обучения, перевести на {courseNew} курс{groupNewCnd} для обучения{fefuShortFastExtendedOptionalTextNew} {fefuCompensationTypeStrNew} по {fefuEducationStrNew_D} {orgUnitPrep} {formativeOrgUnitStrWithTerritorialNew_P} по {developFormNew_DF} форме обучения{techRemote}.\par
\par{liquidateEduPlanDifference}\par
\fi0\cell\row\pard

\keepn\nowidctlpar\qj
Основание: {listBasics}.