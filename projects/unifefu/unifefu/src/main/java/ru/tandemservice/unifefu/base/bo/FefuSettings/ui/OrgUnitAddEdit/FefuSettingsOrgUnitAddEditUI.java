/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrgUnitAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;

/**
 * @author Dmitry Seleznev
 * @since 30.07.2013
 */
@Input({
        @Bind(key = "orderSettingId", binding = "orderSettingId")
})
public class FefuSettingsOrgUnitAddEditUI extends UIPresenter
{
    public static final String ORDER_SETTINGS_ID = "orderSettingId";

    private Long _orderSettingId;
    private FefuOrgUnitDirectumExtension _ext;
    private boolean _addForm = true;

    private DQLFullCheckSelectModel _orderTypeModel;

    @Override
    public void onComponentRefresh()
    {
        _orderTypeModel = new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                return new DQLSelectBuilder().fromEntity(FefuDirectumOrderType.class, alias);
            }
        };

        if (null != _orderSettingId)
        {
            _addForm = false;
            _ext = DataAccessServices.dao().getNotNull(FefuOrgUnitDirectumExtension.class, _orderSettingId);
        } else
        {
            _ext = new FefuOrgUnitDirectumExtension();
        }
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_ext);
        deactivate();
    }

    public Long getOrderSettingId()
    {
        return _orderSettingId;
    }

    public void setOrderSettingId(Long orderSettingId)
    {
        _orderSettingId = orderSettingId;
    }

    public FefuOrgUnitDirectumExtension getExt()
    {
        return _ext;
    }

    public void setExt(FefuOrgUnitDirectumExtension ext)
    {
        _ext = ext;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public DQLFullCheckSelectModel getOrderTypeModel()
    {
        return _orderTypeModel;
    }

    public void setOrderTypeModel(DQLFullCheckSelectModel orderTypeModel)
    {
        _orderTypeModel = orderTypeModel;
    }
}