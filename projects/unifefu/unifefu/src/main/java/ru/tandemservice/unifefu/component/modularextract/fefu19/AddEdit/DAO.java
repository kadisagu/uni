/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu19.AddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;

/**
 * @author Ekaterina Zvereva
 * @since 14.11.2014
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuTransfAcceleratedTimeStuExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected FefuTransfAcceleratedTimeStuExtract createNewInstance()
    {
        return new FefuTransfAcceleratedTimeStuExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        CommonExtractModel eduModel = MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract(), true);

        if (model.isAddForm())
        {
            // Заполняем значения по умолчания - текущие данные студента
            Student student = model.getExtract().getEntity();
            EducationOrgUnit orgUnit = student.getEducationOrgUnit();

            eduModel.setFormativeOrgUnit(orgUnit.getFormativeOrgUnit());
            eduModel.setTerritorialOrgUnit(orgUnit.getTerritorialOrgUnit());
            eduModel.setCourse(student.getCourse());
            eduModel.setGroup(student.getGroup());
            eduModel.setEducationLevelsHighSchool(orgUnit.getEducationLevelHighSchool());
            eduModel.setDevelopForm(orgUnit.getDevelopForm());
            eduModel.setDevelopCondition(orgUnit.getDevelopCondition());
            eduModel.setDevelopTech(orgUnit.getDevelopTech());
            eduModel.setDevelopPeriod(orgUnit.getDevelopPeriod());
            eduModel.setCompensationType(student.getCompensationType());
        }
        model.setEduModel(eduModel);

        model.setGroupNewListModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, "g")
                        .column("g")
                        .where(eq(property(Group.course().fromAlias("g")), value(model.getExtract().getEntity().getCourse())))
                        .where(eq(property(Group.archival().fromAlias("g")), value(Boolean.FALSE)));

                if (o != null)
                {
                    if (o instanceof Long)
                        builder.where(eq(property("g" + ".id"), commonValue(o, PropertyType.LONG)));
                    else if (o instanceof Collection)
                        builder.where(in(property("g" + ".id"), (Collection) o));
                }

                if (model.getExtract().getEntity().getEducationOrgUnit().getFormativeOrgUnit() == null)
                {
                    return new DQLListResultBuilder(builder.where(nothing()));
                }

                builder
                        .where(eq(property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("g")), value(model.getFormativeOrgUnit())))
                        .where(eq(property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("g")), value(model.getTerritorialOrgUnit())));


                EducationLevels educationLevels = EducationOrgUnitUtil.getParentLevel(model.getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
                builder.where(or(
                        eq(property("g", Group.educationOrgUnit().educationLevelHighSchool().educationLevel()), value(educationLevels)),
                        eq(property("g", Group.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel()), value(educationLevels))))

                        .where(eq(property(Group.educationOrgUnit().developForm().fromAlias("g")), value(model.getDevelopForm())))
                        .where(eq(property(Group.educationOrgUnit().developCondition().fromAlias("g")), value(model.getDevelopCondition())))
                        .where(eq(property(Group.educationOrgUnit().developTech().fromAlias("g")), value(model.getDevelopTech())))
                        .where(eq(property(Group.educationOrgUnit().developPeriod().fromAlias("g")), value(model.getDevelopPeriod())))
                        .where(eq(property(Group.educationOrgUnit().used().fromAlias("g")), value(Boolean.TRUE)))
                        .where(eq(property(Group.educationOrgUnit().educationLevelHighSchool().allowStudents().fromAlias("g")), value(Boolean.TRUE)))
                        .order(property(Group.title().fromAlias("g")));

                MoveStudentDaoFacade.getMoveStudentDao().addCustomConditionToGroupSelectBuilder(builder, "g");
                List<Group> list = builder.createStatement(getSession()).list();

                return new DQLListResultBuilder(builder);

            }
        });




        model.setDevelopPeriodsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getEduModel().getGroup() == null || model.getDevelopConditionNew() == null) return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "org")
                        .column(property(EducationOrgUnit.developPeriod().fromAlias("org")))
                        .where(eq(property("org", EducationOrgUnit.formativeOrgUnit()), value(model.getExtract().getEntity().getEducationOrgUnit().getFormativeOrgUnit())))
                        .where(eq(property("org", EducationOrgUnit.educationLevelHighSchool()), value(model.getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool())))
                        .where(eq(property("org", EducationOrgUnit.territorialOrgUnit()), value(model.getExtract().getEntity().getEducationOrgUnit().getTerritorialOrgUnit())))
                        .where(eq(property("org", EducationOrgUnit.developCondition()), value(model.getDevelopConditionNew())))
                        .where(eq(property("org", EducationOrgUnit.used()), value(true)))
                        .where(eq(property("org", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_ALLOW_STUDENTS), value(true)))
                        .order(property("org", EducationOrgUnit.L_DEVELOP_PERIOD + "." + DevelopPeriod.P_CODE))
                        .distinct();

                List<DevelopPeriod> list = builder.createStatement(getSession()).list();
                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });


        List<String> seasons = Arrays.asList((ApplicationRuntime.getProperty("seasons_A")).split(";"));
        model.setSeasons(seasons);
        model.setSeason(seasons.contains(model.getExtract().getSeason())? model.getExtract().getSeason() : null);
        if (!model.isAddForm())
        {
            model.setDevelopPeriodNew(model.getExtract().getEducationOrgUnitNew().getDevelopPeriod());
            model.setGroupNew(model.getExtract().getGroupNew());
        }

    }

    private List<CompensationType> getCompTypeList()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CompensationType.class, "comp").column("comp");
        final List<CompensationType> list = builder.createStatement(getSession()).list();
        return list;
    }

    @Override
    public void update(Model model)
    {
        FefuTransfAcceleratedTimeStuExtract extract = model.getExtract();
        //save rollback data
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), extract);

        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou");
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_FORM, extract.getEntity().getEducationOrgUnit().getDevelopForm()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_TECH, extract.getEntity().getEducationOrgUnit().getDevelopTech()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionNew()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriodNew()));
        List<EducationOrgUnit> acceptableEduOrgUnitsList = builder.getResultList(getSession());

        if (acceptableEduOrgUnitsList.isEmpty())
            errCollector.add("Студент " + extract.getEntity().getPerson().getFullFio() + " не может быть переведен на указанное условие освоения, поскольку для направления подготовки «" + extract.getEntity().getEducationOrgUnit().getTitle() + "», на котором числится студент, не существует...");
        else
            extract.setEducationOrgUnitNew(acceptableEduOrgUnitsList.get(0));

        extract.setGroupNew(model.getGroupNew());
        extract.setSeason(model.getSeason());
        super.update(model);
    }

}