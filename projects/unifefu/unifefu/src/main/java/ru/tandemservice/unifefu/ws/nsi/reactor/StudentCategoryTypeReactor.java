/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.StudentCategoryType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class StudentCategoryTypeReactor extends SimpleCatalogEntityNewReactor<StudentCategoryType, StudentCategory>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<StudentCategory> getEntityClass()
    {
        return StudentCategory.class;
    }

    @Override
    public Class<StudentCategoryType> getNSIEntityClass()
    {
        return StudentCategoryType.class;
    }

    @Override
    public StudentCategoryType getCatalogElementRetrieveRequestObject(String guid)
    {
        StudentCategoryType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createStudentCategoryType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public StudentCategoryType createEntity(CoreCollectionUtils.Pair<StudentCategory, FefuNsiIds> entityPair)
    {
        StudentCategoryType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createStudentCategoryType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setStudentCategoryID(entityPair.getX().getCode());
        nsiEntity.setStudentCategoryName(entityPair.getX().getTitle());
        return nsiEntity;
    }

    @Override
    public StudentCategory createEntity(StudentCategoryType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getStudentCategoryName()) return null;

        StudentCategory entity = new StudentCategory();
        entity.setTitle(nsiEntity.getStudentCategoryName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(StudentCategory.class));

        return entity;
    }

    @Override
    public StudentCategory updatePossibleDuplicateFields(StudentCategoryType nsiEntity, CoreCollectionUtils.Pair<StudentCategory, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), StudentCategory.title().s()))
                entityPair.getX().setTitle(nsiEntity.getStudentCategoryName());
        }
        return entityPair.getX();
    }

    @Override
    public StudentCategoryType updateNsiEntityFields(StudentCategoryType nsiEntity, StudentCategory entity)
    {
        nsiEntity.setStudentCategoryID(entity.getCode());
        nsiEntity.setStudentCategoryName(entity.getTitle());
        return nsiEntity;
    }
}