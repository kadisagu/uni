/* $Id$ */
package ru.tandemservice.unifefu.component.student.MoveStudentPub;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

/**
 * @author nvankov
 * @since 11/8/13
 */
public class Model extends ru.tandemservice.movestudent.component.student.MoveStudentPub.Model
{
    private IMultiSelectModel _thematicGroupListModel;

    public IMultiSelectModel getThematicGroupListModel()
    {
        return _thematicGroupListModel;
    }

    public void setThematicGroupListModel(IMultiSelectModel thematicGroupListModel)
    {
        _thematicGroupListModel = thematicGroupListModel;
    }
}
