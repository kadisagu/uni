package ru.tandemservice.unifefu.entity.eduPlan;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unifefu.entity.eduPlan.gen.*;

/**
 * Состояние проверки УПв
 */
public class FefuEpvCheckState extends FefuEpvCheckStateGen
{
    public FefuEpvCheckState()
    {

    }

    public FefuEpvCheckState(EppEduPlanVersion version)
    {
        this.setVersion(version);
    }
}