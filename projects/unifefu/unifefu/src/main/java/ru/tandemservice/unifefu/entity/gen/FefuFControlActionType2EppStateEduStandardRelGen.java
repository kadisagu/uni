package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь форм итогового контроля с ГОС (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuFControlActionType2EppStateEduStandardRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel";
    public static final String ENTITY_NAME = "fefuFControlActionType2EppStateEduStandardRel";
    public static final int VERSION_HASH = 17498592;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_STANDARD = "eduStandard";
    public static final String L_CONTROL_ACTION = "controlAction";
    public static final String P_MAX_PART_YEAR = "maxPartYear";
    public static final String P_MAX_YEAR = "maxYear";

    private EppStateEduStandard _eduStandard;     // ГОС
    private EppFControlActionType _controlAction;     // Форма итогового контроля
    private int _maxPartYear;     // Максимальное количество в части учебного года
    private int _maxYear;     // Максимальное количество в учебном году

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ГОС. Свойство не может быть null.
     */
    @NotNull
    public EppStateEduStandard getEduStandard()
    {
        return _eduStandard;
    }

    /**
     * @param eduStandard ГОС. Свойство не может быть null.
     */
    public void setEduStandard(EppStateEduStandard eduStandard)
    {
        dirty(_eduStandard, eduStandard);
        _eduStandard = eduStandard;
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     */
    @NotNull
    public EppFControlActionType getControlAction()
    {
        return _controlAction;
    }

    /**
     * @param controlAction Форма итогового контроля. Свойство не может быть null.
     */
    public void setControlAction(EppFControlActionType controlAction)
    {
        dirty(_controlAction, controlAction);
        _controlAction = controlAction;
    }

    /**
     * @return Максимальное количество в части учебного года. Свойство не может быть null.
     */
    @NotNull
    public int getMaxPartYear()
    {
        return _maxPartYear;
    }

    /**
     * @param maxPartYear Максимальное количество в части учебного года. Свойство не может быть null.
     */
    public void setMaxPartYear(int maxPartYear)
    {
        dirty(_maxPartYear, maxPartYear);
        _maxPartYear = maxPartYear;
    }

    /**
     * @return Максимальное количество в учебном году. Свойство не может быть null.
     */
    @NotNull
    public int getMaxYear()
    {
        return _maxYear;
    }

    /**
     * @param maxYear Максимальное количество в учебном году. Свойство не может быть null.
     */
    public void setMaxYear(int maxYear)
    {
        dirty(_maxYear, maxYear);
        _maxYear = maxYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuFControlActionType2EppStateEduStandardRelGen)
        {
            setEduStandard(((FefuFControlActionType2EppStateEduStandardRel)another).getEduStandard());
            setControlAction(((FefuFControlActionType2EppStateEduStandardRel)another).getControlAction());
            setMaxPartYear(((FefuFControlActionType2EppStateEduStandardRel)another).getMaxPartYear());
            setMaxYear(((FefuFControlActionType2EppStateEduStandardRel)another).getMaxYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuFControlActionType2EppStateEduStandardRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuFControlActionType2EppStateEduStandardRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuFControlActionType2EppStateEduStandardRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduStandard":
                    return obj.getEduStandard();
                case "controlAction":
                    return obj.getControlAction();
                case "maxPartYear":
                    return obj.getMaxPartYear();
                case "maxYear":
                    return obj.getMaxYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduStandard":
                    obj.setEduStandard((EppStateEduStandard) value);
                    return;
                case "controlAction":
                    obj.setControlAction((EppFControlActionType) value);
                    return;
                case "maxPartYear":
                    obj.setMaxPartYear((Integer) value);
                    return;
                case "maxYear":
                    obj.setMaxYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduStandard":
                        return true;
                case "controlAction":
                        return true;
                case "maxPartYear":
                        return true;
                case "maxYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduStandard":
                    return true;
                case "controlAction":
                    return true;
                case "maxPartYear":
                    return true;
                case "maxYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduStandard":
                    return EppStateEduStandard.class;
                case "controlAction":
                    return EppFControlActionType.class;
                case "maxPartYear":
                    return Integer.class;
                case "maxYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuFControlActionType2EppStateEduStandardRel> _dslPath = new Path<FefuFControlActionType2EppStateEduStandardRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuFControlActionType2EppStateEduStandardRel");
    }
            

    /**
     * @return ГОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel#getEduStandard()
     */
    public static EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
    {
        return _dslPath.eduStandard();
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel#getControlAction()
     */
    public static EppFControlActionType.Path<EppFControlActionType> controlAction()
    {
        return _dslPath.controlAction();
    }

    /**
     * @return Максимальное количество в части учебного года. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel#getMaxPartYear()
     */
    public static PropertyPath<Integer> maxPartYear()
    {
        return _dslPath.maxPartYear();
    }

    /**
     * @return Максимальное количество в учебном году. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel#getMaxYear()
     */
    public static PropertyPath<Integer> maxYear()
    {
        return _dslPath.maxYear();
    }

    public static class Path<E extends FefuFControlActionType2EppStateEduStandardRel> extends EntityPath<E>
    {
        private EppStateEduStandard.Path<EppStateEduStandard> _eduStandard;
        private EppFControlActionType.Path<EppFControlActionType> _controlAction;
        private PropertyPath<Integer> _maxPartYear;
        private PropertyPath<Integer> _maxYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ГОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel#getEduStandard()
     */
        public EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
        {
            if(_eduStandard == null )
                _eduStandard = new EppStateEduStandard.Path<EppStateEduStandard>(L_EDU_STANDARD, this);
            return _eduStandard;
        }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel#getControlAction()
     */
        public EppFControlActionType.Path<EppFControlActionType> controlAction()
        {
            if(_controlAction == null )
                _controlAction = new EppFControlActionType.Path<EppFControlActionType>(L_CONTROL_ACTION, this);
            return _controlAction;
        }

    /**
     * @return Максимальное количество в части учебного года. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel#getMaxPartYear()
     */
        public PropertyPath<Integer> maxPartYear()
        {
            if(_maxPartYear == null )
                _maxPartYear = new PropertyPath<Integer>(FefuFControlActionType2EppStateEduStandardRelGen.P_MAX_PART_YEAR, this);
            return _maxPartYear;
        }

    /**
     * @return Максимальное количество в учебном году. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel#getMaxYear()
     */
        public PropertyPath<Integer> maxYear()
        {
            if(_maxYear == null )
                _maxYear = new PropertyPath<Integer>(FefuFControlActionType2EppStateEduStandardRelGen.P_MAX_YEAR, this);
            return _maxYear;
        }

        public Class getEntityClass()
        {
            return FefuFControlActionType2EppStateEduStandardRel.class;
        }

        public String getEntityName()
        {
            return "fefuFControlActionType2EppStateEduStandardRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
