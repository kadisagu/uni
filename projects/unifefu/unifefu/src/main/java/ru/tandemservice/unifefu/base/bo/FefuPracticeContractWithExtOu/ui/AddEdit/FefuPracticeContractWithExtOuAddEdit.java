/* $Id: FefuPracticeContractWithExtOuAddEdit.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.FefuPracticeContractWithExtOuManager;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.logic.FefuPracticeContractWithExtOuDSHandler;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/28/13
 * Time: 6:01 PM
 */
@Configuration
public class FefuPracticeContractWithExtOuAddEdit extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS("extOuDS", FefuPracticeContractWithExtOuManager.instance().extOuComboDSHandler()).addColumn(ExternalOrgUnit.title().s()).addColumn(ExternalOrgUnit.legalAddress().titleWithFlat().s()))
                .addDataSource(selectDS("jurCtrDS", FefuPracticeContractWithExtOuManager.instance().jurCtrComboDSHandler()).addColumn(JuridicalContactor.person().fullFio().s()).addColumn(JuridicalContactor.post().s()))
                .create();
    }
}
