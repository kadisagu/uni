package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.PersonFefuExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Персона (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PersonFefuExtGen extends EntityBase
 implements INaturalIdentifiable<PersonFefuExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.PersonFefuExt";
    public static final String ENTITY_NAME = "personFefuExt";
    public static final int VERSION_HASH = -918621662;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON = "person";
    public static final String P_VIP_DATE = "vipDate";
    public static final String P_COMMENT_VIP = "commentVip";
    public static final String P_FIO_CURATOR_VIP = "fioCuratorVip";

    private Person _person;     // Персона
    private Date _vipDate;     // Признак vip
    private String _commentVip;     // Комментарий для vip
    private String _fioCuratorVip;     // ФИО куратора vip

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Персона. Свойство не может быть null и должно быть уникальным.
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Признак vip.
     */
    public Date getVipDate()
    {
        return _vipDate;
    }

    /**
     * @param vipDate Признак vip.
     */
    public void setVipDate(Date vipDate)
    {
        dirty(_vipDate, vipDate);
        _vipDate = vipDate;
    }

    /**
     * @return Комментарий для vip.
     */
    public String getCommentVip()
    {
        return _commentVip;
    }

    /**
     * @param commentVip Комментарий для vip.
     */
    public void setCommentVip(String commentVip)
    {
        dirty(_commentVip, commentVip);
        _commentVip = commentVip;
    }

    /**
     * @return ФИО куратора vip.
     */
    @Length(max=255)
    public String getFioCuratorVip()
    {
        return _fioCuratorVip;
    }

    /**
     * @param fioCuratorVip ФИО куратора vip.
     */
    public void setFioCuratorVip(String fioCuratorVip)
    {
        dirty(_fioCuratorVip, fioCuratorVip);
        _fioCuratorVip = fioCuratorVip;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PersonFefuExtGen)
        {
            if (withNaturalIdProperties)
            {
                setPerson(((PersonFefuExt)another).getPerson());
            }
            setVipDate(((PersonFefuExt)another).getVipDate());
            setCommentVip(((PersonFefuExt)another).getCommentVip());
            setFioCuratorVip(((PersonFefuExt)another).getFioCuratorVip());
        }
    }

    public INaturalId<PersonFefuExtGen> getNaturalId()
    {
        return new NaturalId(getPerson());
    }

    public static class NaturalId extends NaturalIdBase<PersonFefuExtGen>
    {
        private static final String PROXY_NAME = "PersonFefuExtNaturalProxy";

        private Long _person;

        public NaturalId()
        {}

        public NaturalId(Person person)
        {
            _person = ((IEntity) person).getId();
        }

        public Long getPerson()
        {
            return _person;
        }

        public void setPerson(Long person)
        {
            _person = person;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PersonFefuExtGen.NaturalId) ) return false;

            PersonFefuExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getPerson(), that.getPerson()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPerson());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPerson());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PersonFefuExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PersonFefuExt.class;
        }

        public T newInstance()
        {
            return (T) new PersonFefuExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "person":
                    return obj.getPerson();
                case "vipDate":
                    return obj.getVipDate();
                case "commentVip":
                    return obj.getCommentVip();
                case "fioCuratorVip":
                    return obj.getFioCuratorVip();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "vipDate":
                    obj.setVipDate((Date) value);
                    return;
                case "commentVip":
                    obj.setCommentVip((String) value);
                    return;
                case "fioCuratorVip":
                    obj.setFioCuratorVip((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "person":
                        return true;
                case "vipDate":
                        return true;
                case "commentVip":
                        return true;
                case "fioCuratorVip":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "person":
                    return true;
                case "vipDate":
                    return true;
                case "commentVip":
                    return true;
                case "fioCuratorVip":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "person":
                    return Person.class;
                case "vipDate":
                    return Date.class;
                case "commentVip":
                    return String.class;
                case "fioCuratorVip":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PersonFefuExt> _dslPath = new Path<PersonFefuExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PersonFefuExt");
    }
            

    /**
     * @return Персона. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.PersonFefuExt#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Признак vip.
     * @see ru.tandemservice.unifefu.entity.PersonFefuExt#getVipDate()
     */
    public static PropertyPath<Date> vipDate()
    {
        return _dslPath.vipDate();
    }

    /**
     * @return Комментарий для vip.
     * @see ru.tandemservice.unifefu.entity.PersonFefuExt#getCommentVip()
     */
    public static PropertyPath<String> commentVip()
    {
        return _dslPath.commentVip();
    }

    /**
     * @return ФИО куратора vip.
     * @see ru.tandemservice.unifefu.entity.PersonFefuExt#getFioCuratorVip()
     */
    public static PropertyPath<String> fioCuratorVip()
    {
        return _dslPath.fioCuratorVip();
    }

    public static class Path<E extends PersonFefuExt> extends EntityPath<E>
    {
        private Person.Path<Person> _person;
        private PropertyPath<Date> _vipDate;
        private PropertyPath<String> _commentVip;
        private PropertyPath<String> _fioCuratorVip;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.PersonFefuExt#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Признак vip.
     * @see ru.tandemservice.unifefu.entity.PersonFefuExt#getVipDate()
     */
        public PropertyPath<Date> vipDate()
        {
            if(_vipDate == null )
                _vipDate = new PropertyPath<Date>(PersonFefuExtGen.P_VIP_DATE, this);
            return _vipDate;
        }

    /**
     * @return Комментарий для vip.
     * @see ru.tandemservice.unifefu.entity.PersonFefuExt#getCommentVip()
     */
        public PropertyPath<String> commentVip()
        {
            if(_commentVip == null )
                _commentVip = new PropertyPath<String>(PersonFefuExtGen.P_COMMENT_VIP, this);
            return _commentVip;
        }

    /**
     * @return ФИО куратора vip.
     * @see ru.tandemservice.unifefu.entity.PersonFefuExt#getFioCuratorVip()
     */
        public PropertyPath<String> fioCuratorVip()
        {
            if(_fioCuratorVip == null )
                _fioCuratorVip = new PropertyPath<String>(PersonFefuExtGen.P_FIO_CURATOR_VIP, this);
            return _fioCuratorVip;
        }

        public Class getEntityClass()
        {
            return PersonFefuExt.class;
        }

        public String getEntityName()
        {
            return "personFefuExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
