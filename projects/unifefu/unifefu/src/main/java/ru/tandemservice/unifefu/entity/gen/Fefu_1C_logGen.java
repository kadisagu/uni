package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.Fefu_1C_log;
import ru.tandemservice.unifefu.entity.Fefu_1C_pack;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.entity.visa.gen.IAbstractDocumentGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лог синхронизации приказов с подсистемой БУ/УК (1С)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Fefu_1C_logGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.Fefu_1C_log";
    public static final String ENTITY_NAME = "fefu_1C_log";
    public static final int VERSION_HASH = 864322188;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_PACK = "pack";
    public static final String P_FORMAT = "format";

    private IAbstractDocument _extract;     // Выписка
    private Fefu_1C_pack _pack;     // Пакет, в составе которого передана информация по выписке
    private int _format;     // Формат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка. Свойство не может быть null.
     */
    @NotNull
    public IAbstractDocument getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка. Свойство не может быть null.
     */
    public void setExtract(IAbstractDocument extract)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && extract!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractDocument.class);
            IEntityMeta actual =  extract instanceof IEntity ? EntityRuntime.getMeta((IEntity) extract) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Пакет, в составе которого передана информация по выписке.
     */
    public Fefu_1C_pack getPack()
    {
        return _pack;
    }

    /**
     * @param pack Пакет, в составе которого передана информация по выписке.
     */
    public void setPack(Fefu_1C_pack pack)
    {
        dirty(_pack, pack);
        _pack = pack;
    }

    /**
     * @return Формат. Свойство не может быть null.
     */
    @NotNull
    public int getFormat()
    {
        return _format;
    }

    /**
     * @param format Формат. Свойство не может быть null.
     */
    public void setFormat(int format)
    {
        dirty(_format, format);
        _format = format;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Fefu_1C_logGen)
        {
            setExtract(((Fefu_1C_log)another).getExtract());
            setPack(((Fefu_1C_log)another).getPack());
            setFormat(((Fefu_1C_log)another).getFormat());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Fefu_1C_logGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Fefu_1C_log.class;
        }

        public T newInstance()
        {
            return (T) new Fefu_1C_log();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "pack":
                    return obj.getPack();
                case "format":
                    return obj.getFormat();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((IAbstractDocument) value);
                    return;
                case "pack":
                    obj.setPack((Fefu_1C_pack) value);
                    return;
                case "format":
                    obj.setFormat((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "pack":
                        return true;
                case "format":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "pack":
                    return true;
                case "format":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return IAbstractDocument.class;
                case "pack":
                    return Fefu_1C_pack.class;
                case "format":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Fefu_1C_log> _dslPath = new Path<Fefu_1C_log>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Fefu_1C_log");
    }
            

    /**
     * @return Выписка. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_log#getExtract()
     */
    public static IAbstractDocumentGen.Path<IAbstractDocument> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Пакет, в составе которого передана информация по выписке.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_log#getPack()
     */
    public static Fefu_1C_pack.Path<Fefu_1C_pack> pack()
    {
        return _dslPath.pack();
    }

    /**
     * @return Формат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_log#getFormat()
     */
    public static PropertyPath<Integer> format()
    {
        return _dslPath.format();
    }

    public static class Path<E extends Fefu_1C_log> extends EntityPath<E>
    {
        private IAbstractDocumentGen.Path<IAbstractDocument> _extract;
        private Fefu_1C_pack.Path<Fefu_1C_pack> _pack;
        private PropertyPath<Integer> _format;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_log#getExtract()
     */
        public IAbstractDocumentGen.Path<IAbstractDocument> extract()
        {
            if(_extract == null )
                _extract = new IAbstractDocumentGen.Path<IAbstractDocument>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Пакет, в составе которого передана информация по выписке.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_log#getPack()
     */
        public Fefu_1C_pack.Path<Fefu_1C_pack> pack()
        {
            if(_pack == null )
                _pack = new Fefu_1C_pack.Path<Fefu_1C_pack>(L_PACK, this);
            return _pack;
        }

    /**
     * @return Формат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.Fefu_1C_log#getFormat()
     */
        public PropertyPath<Integer> format()
        {
            if(_format == null )
                _format = new PropertyPath<Integer>(Fefu_1C_logGen.P_FORMAT, this);
            return _format;
        }

        public Class getEntityClass()
        {
            return Fefu_1C_log.class;
        }

        public String getEntityName()
        {
            return "fefu_1C_log";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
