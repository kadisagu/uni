/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu18;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e86.utils.EmployeePostDecl;
import ru.tandemservice.unifefu.entity.StuffCompensationStuExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 22.05.2014
 */
public class StuffCompensationStuExtractPrint implements IPrintFormCreator<StuffCompensationStuExtract>
{
	@Override
	public RtfDocument createPrintForm(byte[] template, StuffCompensationStuExtract extract)
	{
		final RtfDocument document = new RtfReader().read(template);
		RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

		modifier.put("protocol",String.format("%s года № %s", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(extract.getProtocolDate()), extract.getProtocolNumber()));

		modifier.put("responsiblePerson", StringUtils.capitalize(EmployeePostDecl.getEmployeeStrDat(extract.getResponsiblePerson())));
		modifier.put("matchingPerson", EmployeePostDecl.getEmployeeStrInst(extract.getMatchingPerson()));

		modifier.put("sEnd", extract.getEntity().getPerson().isMale() ? "его" : "ую");
		modifier.put("compensationSum", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getCompensationSum() / 100D) );
		modifier.put("immediateSum", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getImmediateSum() / 100D) );
		modifier.put("matchingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getMatchingDate()));

		CommonExtractPrint.initFefuGroup(modifier, "fefuGroup", extract.getEntity().getGroup(), extract.getEntity().getEducationOrgUnit().getDevelopForm(), " группы ");
		CommonExtractPrint.modifyEducationStr(modifier, extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(), new String[]{"fefuEducationStrDirection", "educationStrDirection"}, false);

		modifier.modify(document);
		CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
		return document;
	}
}
