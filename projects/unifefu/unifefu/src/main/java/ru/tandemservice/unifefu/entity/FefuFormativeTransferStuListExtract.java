package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITransferExtract;
import ru.tandemservice.unifefu.entity.gen.FefuFormativeTransferStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «Об изменении формирующего подразделения»
 */
public class FefuFormativeTransferStuListExtract extends FefuFormativeTransferStuListExtractGen implements ITransferExtract
{
    @Override
    public Date getTransferOrOrderDate()
    {
        return getBeginDate();
    }
}