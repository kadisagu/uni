package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.sec.entity.gen.IPrincipalContextGen;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приказ отправляемый на согласование в Directum
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuDirectumSendingOrderGen extends EntityBase
 implements INaturalIdentifiable<FefuDirectumSendingOrderGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder";
    public static final String ENTITY_NAME = "fefuDirectumSendingOrder";
    public static final int VERSION_HASH = -1880287395;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String L_FIRST_EXTRACT = "firstExtract";
    public static final String L_ORDER_TYPE = "orderType";
    public static final String P_DONE_SEND = "doneSend";
    public static final String P_SEND_DATE_TIME = "sendDateTime";
    public static final String P_SIGNER = "signer";
    public static final String P_ROUTE_CODE = "routeCode";
    public static final String P_ORG_UNIT_CODE = "orgUnitCode";
    public static final String P_ORG_UNIT_TITLE = "orgUnitTitle";
    public static final String P_DISMISS = "dismiss";
    public static final String P_RESTORE = "restore";
    public static final String P_OFF_BUDGET_PAYMENTS = "offBudgetPayments";
    public static final String P_FOREIGN_STUDENT = "foreignStudent";
    public static final String P_GRANT_MAT_AID = "grantMatAid";
    public static final String P_SOCIAL = "social";
    public static final String P_PRACTICE = "practice";
    public static final String P_GROUP_MANAGER = "groupManager";
    public static final String P_UVC_STUDENTS = "uvcStudents";
    public static final String P_FVO_STUDENTS = "fvoStudents";
    public static final String P_PENALTY = "penalty";
    public static final String P_MAGISTER = "magister";
    public static final String P_PAYMENTS = "payments";
    public static final String P_STATE_FORMULAR = "stateFormular";
    public static final String P_INTERNATIONAL = "international";
    public static final String P_CHECK_MEDICAL_DOCS = "checkMedicalDocs";
    public static final String P_DIPLOMA_SUCCESS = "diplomaSuccess";
    public static final String L_EXECUTOR = "executor";

    private AbstractStudentOrder _order;     // Приказ по движению студентов
    private AbstractStudentExtract _firstExtract;     // Первая выписка приказа
    private FefuDirectumOrderType _orderType;     // Вид приказа в Directum
    private boolean _doneSend;     // Отправка выполнена
    private Date _sendDateTime;     // Дата и время отправки приказа на согласование
    private String _signer;     // Подписывает
    private String _routeCode;     // Код маршрута
    private String _orgUnitCode;     // Код подразделения в Directum
    private String _orgUnitTitle;     // Наименование подразделения в Directum
    private boolean _dismiss;     // Отчисление (выпуск)
    private boolean _restore;     // Восстановление / перевод из числа слушателей в студенты
    private boolean _offBudgetPayments;     // Выплаты из внебюджетных средств
    private boolean _foreignStudent;     // Иностранный студент
    private boolean _grantMatAid;     // Назначение стипендии / материальной помощи / восстановление стипендии
    private boolean _social;     // По студентам-сиротам (ДМП)
    private boolean _practice;     // Практики
    private boolean _groupManager;     // Назначение / снятие старост групп
    private boolean _uvcStudents;     // Обучающиеся в УВЦ
    private boolean _fvoStudents;     // Обучающиеся на ФВО
    private boolean _penalty;     // Наложение взысканий
    private boolean _magister;     // Закрепление за магистерской программой
    private boolean _payments;     // Приказ по выплатам (СПО)
    private boolean _stateFormular;     // Документ гос образца (ДПО)
    private boolean _international;     // Международные отношения (Прием)
    private boolean _checkMedicalDocs;     // Проверка мед. справок / заключений ВК
    private boolean _diplomaSuccess;     // Диплом с отличием
    private IPrincipalContext _executor;     // Принципал, отправивший приказ на согласование

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ по движению студентов. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AbstractStudentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ по движению студентов. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrder(AbstractStudentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Первая выписка приказа. Свойство не может быть null.
     */
    @NotNull
    public AbstractStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    /**
     * @param firstExtract Первая выписка приказа. Свойство не может быть null.
     */
    public void setFirstExtract(AbstractStudentExtract firstExtract)
    {
        dirty(_firstExtract, firstExtract);
        _firstExtract = firstExtract;
    }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     */
    @NotNull
    public FefuDirectumOrderType getOrderType()
    {
        return _orderType;
    }

    /**
     * @param orderType Вид приказа в Directum. Свойство не может быть null.
     */
    public void setOrderType(FefuDirectumOrderType orderType)
    {
        dirty(_orderType, orderType);
        _orderType = orderType;
    }

    /**
     * @return Отправка выполнена. Свойство не может быть null.
     */
    @NotNull
    public boolean isDoneSend()
    {
        return _doneSend;
    }

    /**
     * @param doneSend Отправка выполнена. Свойство не может быть null.
     */
    public void setDoneSend(boolean doneSend)
    {
        dirty(_doneSend, doneSend);
        _doneSend = doneSend;
    }

    /**
     * @return Дата и время отправки приказа на согласование.
     */
    public Date getSendDateTime()
    {
        return _sendDateTime;
    }

    /**
     * @param sendDateTime Дата и время отправки приказа на согласование.
     */
    public void setSendDateTime(Date sendDateTime)
    {
        dirty(_sendDateTime, sendDateTime);
        _sendDateTime = sendDateTime;
    }

    /**
     * @return Подписывает.
     */
    @Length(max=255)
    public String getSigner()
    {
        return _signer;
    }

    /**
     * @param signer Подписывает.
     */
    public void setSigner(String signer)
    {
        dirty(_signer, signer);
        _signer = signer;
    }

    /**
     * @return Код маршрута. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRouteCode()
    {
        return _routeCode;
    }

    /**
     * @param routeCode Код маршрута. Свойство не может быть null.
     */
    public void setRouteCode(String routeCode)
    {
        dirty(_routeCode, routeCode);
        _routeCode = routeCode;
    }

    /**
     * @return Код подразделения в Directum.
     */
    @Length(max=255)
    public String getOrgUnitCode()
    {
        return _orgUnitCode;
    }

    /**
     * @param orgUnitCode Код подразделения в Directum.
     */
    public void setOrgUnitCode(String orgUnitCode)
    {
        dirty(_orgUnitCode, orgUnitCode);
        _orgUnitCode = orgUnitCode;
    }

    /**
     * @return Наименование подразделения в Directum.
     */
    @Length(max=255)
    public String getOrgUnitTitle()
    {
        return _orgUnitTitle;
    }

    /**
     * @param orgUnitTitle Наименование подразделения в Directum.
     */
    public void setOrgUnitTitle(String orgUnitTitle)
    {
        dirty(_orgUnitTitle, orgUnitTitle);
        _orgUnitTitle = orgUnitTitle;
    }

    /**
     * @return Отчисление (выпуск). Свойство не может быть null.
     */
    @NotNull
    public boolean isDismiss()
    {
        return _dismiss;
    }

    /**
     * @param dismiss Отчисление (выпуск). Свойство не может быть null.
     */
    public void setDismiss(boolean dismiss)
    {
        dirty(_dismiss, dismiss);
        _dismiss = dismiss;
    }

    /**
     * @return Восстановление / перевод из числа слушателей в студенты. Свойство не может быть null.
     */
    @NotNull
    public boolean isRestore()
    {
        return _restore;
    }

    /**
     * @param restore Восстановление / перевод из числа слушателей в студенты. Свойство не может быть null.
     */
    public void setRestore(boolean restore)
    {
        dirty(_restore, restore);
        _restore = restore;
    }

    /**
     * @return Выплаты из внебюджетных средств. Свойство не может быть null.
     */
    @NotNull
    public boolean isOffBudgetPayments()
    {
        return _offBudgetPayments;
    }

    /**
     * @param offBudgetPayments Выплаты из внебюджетных средств. Свойство не может быть null.
     */
    public void setOffBudgetPayments(boolean offBudgetPayments)
    {
        dirty(_offBudgetPayments, offBudgetPayments);
        _offBudgetPayments = offBudgetPayments;
    }

    /**
     * @return Иностранный студент. Свойство не может быть null.
     */
    @NotNull
    public boolean isForeignStudent()
    {
        return _foreignStudent;
    }

    /**
     * @param foreignStudent Иностранный студент. Свойство не может быть null.
     */
    public void setForeignStudent(boolean foreignStudent)
    {
        dirty(_foreignStudent, foreignStudent);
        _foreignStudent = foreignStudent;
    }

    /**
     * @return Назначение стипендии / материальной помощи / восстановление стипендии. Свойство не может быть null.
     */
    @NotNull
    public boolean isGrantMatAid()
    {
        return _grantMatAid;
    }

    /**
     * @param grantMatAid Назначение стипендии / материальной помощи / восстановление стипендии. Свойство не может быть null.
     */
    public void setGrantMatAid(boolean grantMatAid)
    {
        dirty(_grantMatAid, grantMatAid);
        _grantMatAid = grantMatAid;
    }

    /**
     * @return По студентам-сиротам (ДМП). Свойство не может быть null.
     */
    @NotNull
    public boolean isSocial()
    {
        return _social;
    }

    /**
     * @param social По студентам-сиротам (ДМП). Свойство не может быть null.
     */
    public void setSocial(boolean social)
    {
        dirty(_social, social);
        _social = social;
    }

    /**
     * @return Практики. Свойство не может быть null.
     */
    @NotNull
    public boolean isPractice()
    {
        return _practice;
    }

    /**
     * @param practice Практики. Свойство не может быть null.
     */
    public void setPractice(boolean practice)
    {
        dirty(_practice, practice);
        _practice = practice;
    }

    /**
     * @return Назначение / снятие старост групп. Свойство не может быть null.
     */
    @NotNull
    public boolean isGroupManager()
    {
        return _groupManager;
    }

    /**
     * @param groupManager Назначение / снятие старост групп. Свойство не может быть null.
     */
    public void setGroupManager(boolean groupManager)
    {
        dirty(_groupManager, groupManager);
        _groupManager = groupManager;
    }

    /**
     * @return Обучающиеся в УВЦ. Свойство не может быть null.
     */
    @NotNull
    public boolean isUvcStudents()
    {
        return _uvcStudents;
    }

    /**
     * @param uvcStudents Обучающиеся в УВЦ. Свойство не может быть null.
     */
    public void setUvcStudents(boolean uvcStudents)
    {
        dirty(_uvcStudents, uvcStudents);
        _uvcStudents = uvcStudents;
    }

    /**
     * @return Обучающиеся на ФВО. Свойство не может быть null.
     */
    @NotNull
    public boolean isFvoStudents()
    {
        return _fvoStudents;
    }

    /**
     * @param fvoStudents Обучающиеся на ФВО. Свойство не может быть null.
     */
    public void setFvoStudents(boolean fvoStudents)
    {
        dirty(_fvoStudents, fvoStudents);
        _fvoStudents = fvoStudents;
    }

    /**
     * @return Наложение взысканий. Свойство не может быть null.
     */
    @NotNull
    public boolean isPenalty()
    {
        return _penalty;
    }

    /**
     * @param penalty Наложение взысканий. Свойство не может быть null.
     */
    public void setPenalty(boolean penalty)
    {
        dirty(_penalty, penalty);
        _penalty = penalty;
    }

    /**
     * @return Закрепление за магистерской программой. Свойство не может быть null.
     */
    @NotNull
    public boolean isMagister()
    {
        return _magister;
    }

    /**
     * @param magister Закрепление за магистерской программой. Свойство не может быть null.
     */
    public void setMagister(boolean magister)
    {
        dirty(_magister, magister);
        _magister = magister;
    }

    /**
     * @return Приказ по выплатам (СПО). Свойство не может быть null.
     */
    @NotNull
    public boolean isPayments()
    {
        return _payments;
    }

    /**
     * @param payments Приказ по выплатам (СПО). Свойство не может быть null.
     */
    public void setPayments(boolean payments)
    {
        dirty(_payments, payments);
        _payments = payments;
    }

    /**
     * @return Документ гос образца (ДПО). Свойство не может быть null.
     */
    @NotNull
    public boolean isStateFormular()
    {
        return _stateFormular;
    }

    /**
     * @param stateFormular Документ гос образца (ДПО). Свойство не может быть null.
     */
    public void setStateFormular(boolean stateFormular)
    {
        dirty(_stateFormular, stateFormular);
        _stateFormular = stateFormular;
    }

    /**
     * @return Международные отношения (Прием). Свойство не может быть null.
     */
    @NotNull
    public boolean isInternational()
    {
        return _international;
    }

    /**
     * @param international Международные отношения (Прием). Свойство не может быть null.
     */
    public void setInternational(boolean international)
    {
        dirty(_international, international);
        _international = international;
    }

    /**
     * @return Проверка мед. справок / заключений ВК. Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckMedicalDocs()
    {
        return _checkMedicalDocs;
    }

    /**
     * @param checkMedicalDocs Проверка мед. справок / заключений ВК. Свойство не может быть null.
     */
    public void setCheckMedicalDocs(boolean checkMedicalDocs)
    {
        dirty(_checkMedicalDocs, checkMedicalDocs);
        _checkMedicalDocs = checkMedicalDocs;
    }

    /**
     * @return Диплом с отличием. Свойство не может быть null.
     */
    @NotNull
    public boolean isDiplomaSuccess()
    {
        return _diplomaSuccess;
    }

    /**
     * @param diplomaSuccess Диплом с отличием. Свойство не может быть null.
     */
    public void setDiplomaSuccess(boolean diplomaSuccess)
    {
        dirty(_diplomaSuccess, diplomaSuccess);
        _diplomaSuccess = diplomaSuccess;
    }

    /**
     * @return Принципал, отправивший приказ на согласование. Свойство не может быть null.
     */
    @NotNull
    public IPrincipalContext getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Принципал, отправивший приказ на согласование. Свойство не может быть null.
     */
    public void setExecutor(IPrincipalContext executor)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && executor!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPrincipalContext.class);
            IEntityMeta actual =  executor instanceof IEntity ? EntityRuntime.getMeta((IEntity) executor) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_executor, executor);
        _executor = executor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuDirectumSendingOrderGen)
        {
            if (withNaturalIdProperties)
            {
                setOrder(((FefuDirectumSendingOrder)another).getOrder());
            }
            setFirstExtract(((FefuDirectumSendingOrder)another).getFirstExtract());
            setOrderType(((FefuDirectumSendingOrder)another).getOrderType());
            setDoneSend(((FefuDirectumSendingOrder)another).isDoneSend());
            setSendDateTime(((FefuDirectumSendingOrder)another).getSendDateTime());
            setSigner(((FefuDirectumSendingOrder)another).getSigner());
            setRouteCode(((FefuDirectumSendingOrder)another).getRouteCode());
            setOrgUnitCode(((FefuDirectumSendingOrder)another).getOrgUnitCode());
            setOrgUnitTitle(((FefuDirectumSendingOrder)another).getOrgUnitTitle());
            setDismiss(((FefuDirectumSendingOrder)another).isDismiss());
            setRestore(((FefuDirectumSendingOrder)another).isRestore());
            setOffBudgetPayments(((FefuDirectumSendingOrder)another).isOffBudgetPayments());
            setForeignStudent(((FefuDirectumSendingOrder)another).isForeignStudent());
            setGrantMatAid(((FefuDirectumSendingOrder)another).isGrantMatAid());
            setSocial(((FefuDirectumSendingOrder)another).isSocial());
            setPractice(((FefuDirectumSendingOrder)another).isPractice());
            setGroupManager(((FefuDirectumSendingOrder)another).isGroupManager());
            setUvcStudents(((FefuDirectumSendingOrder)another).isUvcStudents());
            setFvoStudents(((FefuDirectumSendingOrder)another).isFvoStudents());
            setPenalty(((FefuDirectumSendingOrder)another).isPenalty());
            setMagister(((FefuDirectumSendingOrder)another).isMagister());
            setPayments(((FefuDirectumSendingOrder)another).isPayments());
            setStateFormular(((FefuDirectumSendingOrder)another).isStateFormular());
            setInternational(((FefuDirectumSendingOrder)another).isInternational());
            setCheckMedicalDocs(((FefuDirectumSendingOrder)another).isCheckMedicalDocs());
            setDiplomaSuccess(((FefuDirectumSendingOrder)another).isDiplomaSuccess());
            setExecutor(((FefuDirectumSendingOrder)another).getExecutor());
        }
    }

    public INaturalId<FefuDirectumSendingOrderGen> getNaturalId()
    {
        return new NaturalId(getOrder());
    }

    public static class NaturalId extends NaturalIdBase<FefuDirectumSendingOrderGen>
    {
        private static final String PROXY_NAME = "FefuDirectumSendingOrderNaturalProxy";

        private Long _order;

        public NaturalId()
        {}

        public NaturalId(AbstractStudentOrder order)
        {
            _order = ((IEntity) order).getId();
        }

        public Long getOrder()
        {
            return _order;
        }

        public void setOrder(Long order)
        {
            _order = order;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuDirectumSendingOrderGen.NaturalId) ) return false;

            FefuDirectumSendingOrderGen.NaturalId that = (NaturalId) o;

            if( !equals(getOrder(), that.getOrder()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOrder());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOrder());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuDirectumSendingOrderGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuDirectumSendingOrder.class;
        }

        public T newInstance()
        {
            return (T) new FefuDirectumSendingOrder();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "firstExtract":
                    return obj.getFirstExtract();
                case "orderType":
                    return obj.getOrderType();
                case "doneSend":
                    return obj.isDoneSend();
                case "sendDateTime":
                    return obj.getSendDateTime();
                case "signer":
                    return obj.getSigner();
                case "routeCode":
                    return obj.getRouteCode();
                case "orgUnitCode":
                    return obj.getOrgUnitCode();
                case "orgUnitTitle":
                    return obj.getOrgUnitTitle();
                case "dismiss":
                    return obj.isDismiss();
                case "restore":
                    return obj.isRestore();
                case "offBudgetPayments":
                    return obj.isOffBudgetPayments();
                case "foreignStudent":
                    return obj.isForeignStudent();
                case "grantMatAid":
                    return obj.isGrantMatAid();
                case "social":
                    return obj.isSocial();
                case "practice":
                    return obj.isPractice();
                case "groupManager":
                    return obj.isGroupManager();
                case "uvcStudents":
                    return obj.isUvcStudents();
                case "fvoStudents":
                    return obj.isFvoStudents();
                case "penalty":
                    return obj.isPenalty();
                case "magister":
                    return obj.isMagister();
                case "payments":
                    return obj.isPayments();
                case "stateFormular":
                    return obj.isStateFormular();
                case "international":
                    return obj.isInternational();
                case "checkMedicalDocs":
                    return obj.isCheckMedicalDocs();
                case "diplomaSuccess":
                    return obj.isDiplomaSuccess();
                case "executor":
                    return obj.getExecutor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((AbstractStudentOrder) value);
                    return;
                case "firstExtract":
                    obj.setFirstExtract((AbstractStudentExtract) value);
                    return;
                case "orderType":
                    obj.setOrderType((FefuDirectumOrderType) value);
                    return;
                case "doneSend":
                    obj.setDoneSend((Boolean) value);
                    return;
                case "sendDateTime":
                    obj.setSendDateTime((Date) value);
                    return;
                case "signer":
                    obj.setSigner((String) value);
                    return;
                case "routeCode":
                    obj.setRouteCode((String) value);
                    return;
                case "orgUnitCode":
                    obj.setOrgUnitCode((String) value);
                    return;
                case "orgUnitTitle":
                    obj.setOrgUnitTitle((String) value);
                    return;
                case "dismiss":
                    obj.setDismiss((Boolean) value);
                    return;
                case "restore":
                    obj.setRestore((Boolean) value);
                    return;
                case "offBudgetPayments":
                    obj.setOffBudgetPayments((Boolean) value);
                    return;
                case "foreignStudent":
                    obj.setForeignStudent((Boolean) value);
                    return;
                case "grantMatAid":
                    obj.setGrantMatAid((Boolean) value);
                    return;
                case "social":
                    obj.setSocial((Boolean) value);
                    return;
                case "practice":
                    obj.setPractice((Boolean) value);
                    return;
                case "groupManager":
                    obj.setGroupManager((Boolean) value);
                    return;
                case "uvcStudents":
                    obj.setUvcStudents((Boolean) value);
                    return;
                case "fvoStudents":
                    obj.setFvoStudents((Boolean) value);
                    return;
                case "penalty":
                    obj.setPenalty((Boolean) value);
                    return;
                case "magister":
                    obj.setMagister((Boolean) value);
                    return;
                case "payments":
                    obj.setPayments((Boolean) value);
                    return;
                case "stateFormular":
                    obj.setStateFormular((Boolean) value);
                    return;
                case "international":
                    obj.setInternational((Boolean) value);
                    return;
                case "checkMedicalDocs":
                    obj.setCheckMedicalDocs((Boolean) value);
                    return;
                case "diplomaSuccess":
                    obj.setDiplomaSuccess((Boolean) value);
                    return;
                case "executor":
                    obj.setExecutor((IPrincipalContext) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "firstExtract":
                        return true;
                case "orderType":
                        return true;
                case "doneSend":
                        return true;
                case "sendDateTime":
                        return true;
                case "signer":
                        return true;
                case "routeCode":
                        return true;
                case "orgUnitCode":
                        return true;
                case "orgUnitTitle":
                        return true;
                case "dismiss":
                        return true;
                case "restore":
                        return true;
                case "offBudgetPayments":
                        return true;
                case "foreignStudent":
                        return true;
                case "grantMatAid":
                        return true;
                case "social":
                        return true;
                case "practice":
                        return true;
                case "groupManager":
                        return true;
                case "uvcStudents":
                        return true;
                case "fvoStudents":
                        return true;
                case "penalty":
                        return true;
                case "magister":
                        return true;
                case "payments":
                        return true;
                case "stateFormular":
                        return true;
                case "international":
                        return true;
                case "checkMedicalDocs":
                        return true;
                case "diplomaSuccess":
                        return true;
                case "executor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "firstExtract":
                    return true;
                case "orderType":
                    return true;
                case "doneSend":
                    return true;
                case "sendDateTime":
                    return true;
                case "signer":
                    return true;
                case "routeCode":
                    return true;
                case "orgUnitCode":
                    return true;
                case "orgUnitTitle":
                    return true;
                case "dismiss":
                    return true;
                case "restore":
                    return true;
                case "offBudgetPayments":
                    return true;
                case "foreignStudent":
                    return true;
                case "grantMatAid":
                    return true;
                case "social":
                    return true;
                case "practice":
                    return true;
                case "groupManager":
                    return true;
                case "uvcStudents":
                    return true;
                case "fvoStudents":
                    return true;
                case "penalty":
                    return true;
                case "magister":
                    return true;
                case "payments":
                    return true;
                case "stateFormular":
                    return true;
                case "international":
                    return true;
                case "checkMedicalDocs":
                    return true;
                case "diplomaSuccess":
                    return true;
                case "executor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return AbstractStudentOrder.class;
                case "firstExtract":
                    return AbstractStudentExtract.class;
                case "orderType":
                    return FefuDirectumOrderType.class;
                case "doneSend":
                    return Boolean.class;
                case "sendDateTime":
                    return Date.class;
                case "signer":
                    return String.class;
                case "routeCode":
                    return String.class;
                case "orgUnitCode":
                    return String.class;
                case "orgUnitTitle":
                    return String.class;
                case "dismiss":
                    return Boolean.class;
                case "restore":
                    return Boolean.class;
                case "offBudgetPayments":
                    return Boolean.class;
                case "foreignStudent":
                    return Boolean.class;
                case "grantMatAid":
                    return Boolean.class;
                case "social":
                    return Boolean.class;
                case "practice":
                    return Boolean.class;
                case "groupManager":
                    return Boolean.class;
                case "uvcStudents":
                    return Boolean.class;
                case "fvoStudents":
                    return Boolean.class;
                case "penalty":
                    return Boolean.class;
                case "magister":
                    return Boolean.class;
                case "payments":
                    return Boolean.class;
                case "stateFormular":
                    return Boolean.class;
                case "international":
                    return Boolean.class;
                case "checkMedicalDocs":
                    return Boolean.class;
                case "diplomaSuccess":
                    return Boolean.class;
                case "executor":
                    return IPrincipalContext.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuDirectumSendingOrder> _dslPath = new Path<FefuDirectumSendingOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuDirectumSendingOrder");
    }
            

    /**
     * @return Приказ по движению студентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getOrder()
     */
    public static AbstractStudentOrder.Path<AbstractStudentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Первая выписка приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getFirstExtract()
     */
    public static AbstractStudentExtract.Path<AbstractStudentExtract> firstExtract()
    {
        return _dslPath.firstExtract();
    }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getOrderType()
     */
    public static FefuDirectumOrderType.Path<FefuDirectumOrderType> orderType()
    {
        return _dslPath.orderType();
    }

    /**
     * @return Отправка выполнена. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isDoneSend()
     */
    public static PropertyPath<Boolean> doneSend()
    {
        return _dslPath.doneSend();
    }

    /**
     * @return Дата и время отправки приказа на согласование.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getSendDateTime()
     */
    public static PropertyPath<Date> sendDateTime()
    {
        return _dslPath.sendDateTime();
    }

    /**
     * @return Подписывает.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getSigner()
     */
    public static PropertyPath<String> signer()
    {
        return _dslPath.signer();
    }

    /**
     * @return Код маршрута. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getRouteCode()
     */
    public static PropertyPath<String> routeCode()
    {
        return _dslPath.routeCode();
    }

    /**
     * @return Код подразделения в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getOrgUnitCode()
     */
    public static PropertyPath<String> orgUnitCode()
    {
        return _dslPath.orgUnitCode();
    }

    /**
     * @return Наименование подразделения в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getOrgUnitTitle()
     */
    public static PropertyPath<String> orgUnitTitle()
    {
        return _dslPath.orgUnitTitle();
    }

    /**
     * @return Отчисление (выпуск). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isDismiss()
     */
    public static PropertyPath<Boolean> dismiss()
    {
        return _dslPath.dismiss();
    }

    /**
     * @return Восстановление / перевод из числа слушателей в студенты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isRestore()
     */
    public static PropertyPath<Boolean> restore()
    {
        return _dslPath.restore();
    }

    /**
     * @return Выплаты из внебюджетных средств. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isOffBudgetPayments()
     */
    public static PropertyPath<Boolean> offBudgetPayments()
    {
        return _dslPath.offBudgetPayments();
    }

    /**
     * @return Иностранный студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isForeignStudent()
     */
    public static PropertyPath<Boolean> foreignStudent()
    {
        return _dslPath.foreignStudent();
    }

    /**
     * @return Назначение стипендии / материальной помощи / восстановление стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isGrantMatAid()
     */
    public static PropertyPath<Boolean> grantMatAid()
    {
        return _dslPath.grantMatAid();
    }

    /**
     * @return По студентам-сиротам (ДМП). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isSocial()
     */
    public static PropertyPath<Boolean> social()
    {
        return _dslPath.social();
    }

    /**
     * @return Практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isPractice()
     */
    public static PropertyPath<Boolean> practice()
    {
        return _dslPath.practice();
    }

    /**
     * @return Назначение / снятие старост групп. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isGroupManager()
     */
    public static PropertyPath<Boolean> groupManager()
    {
        return _dslPath.groupManager();
    }

    /**
     * @return Обучающиеся в УВЦ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isUvcStudents()
     */
    public static PropertyPath<Boolean> uvcStudents()
    {
        return _dslPath.uvcStudents();
    }

    /**
     * @return Обучающиеся на ФВО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isFvoStudents()
     */
    public static PropertyPath<Boolean> fvoStudents()
    {
        return _dslPath.fvoStudents();
    }

    /**
     * @return Наложение взысканий. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isPenalty()
     */
    public static PropertyPath<Boolean> penalty()
    {
        return _dslPath.penalty();
    }

    /**
     * @return Закрепление за магистерской программой. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isMagister()
     */
    public static PropertyPath<Boolean> magister()
    {
        return _dslPath.magister();
    }

    /**
     * @return Приказ по выплатам (СПО). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isPayments()
     */
    public static PropertyPath<Boolean> payments()
    {
        return _dslPath.payments();
    }

    /**
     * @return Документ гос образца (ДПО). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isStateFormular()
     */
    public static PropertyPath<Boolean> stateFormular()
    {
        return _dslPath.stateFormular();
    }

    /**
     * @return Международные отношения (Прием). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isInternational()
     */
    public static PropertyPath<Boolean> international()
    {
        return _dslPath.international();
    }

    /**
     * @return Проверка мед. справок / заключений ВК. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isCheckMedicalDocs()
     */
    public static PropertyPath<Boolean> checkMedicalDocs()
    {
        return _dslPath.checkMedicalDocs();
    }

    /**
     * @return Диплом с отличием. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isDiplomaSuccess()
     */
    public static PropertyPath<Boolean> diplomaSuccess()
    {
        return _dslPath.diplomaSuccess();
    }

    /**
     * @return Принципал, отправивший приказ на согласование. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getExecutor()
     */
    public static IPrincipalContextGen.Path<IPrincipalContext> executor()
    {
        return _dslPath.executor();
    }

    public static class Path<E extends FefuDirectumSendingOrder> extends EntityPath<E>
    {
        private AbstractStudentOrder.Path<AbstractStudentOrder> _order;
        private AbstractStudentExtract.Path<AbstractStudentExtract> _firstExtract;
        private FefuDirectumOrderType.Path<FefuDirectumOrderType> _orderType;
        private PropertyPath<Boolean> _doneSend;
        private PropertyPath<Date> _sendDateTime;
        private PropertyPath<String> _signer;
        private PropertyPath<String> _routeCode;
        private PropertyPath<String> _orgUnitCode;
        private PropertyPath<String> _orgUnitTitle;
        private PropertyPath<Boolean> _dismiss;
        private PropertyPath<Boolean> _restore;
        private PropertyPath<Boolean> _offBudgetPayments;
        private PropertyPath<Boolean> _foreignStudent;
        private PropertyPath<Boolean> _grantMatAid;
        private PropertyPath<Boolean> _social;
        private PropertyPath<Boolean> _practice;
        private PropertyPath<Boolean> _groupManager;
        private PropertyPath<Boolean> _uvcStudents;
        private PropertyPath<Boolean> _fvoStudents;
        private PropertyPath<Boolean> _penalty;
        private PropertyPath<Boolean> _magister;
        private PropertyPath<Boolean> _payments;
        private PropertyPath<Boolean> _stateFormular;
        private PropertyPath<Boolean> _international;
        private PropertyPath<Boolean> _checkMedicalDocs;
        private PropertyPath<Boolean> _diplomaSuccess;
        private IPrincipalContextGen.Path<IPrincipalContext> _executor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ по движению студентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getOrder()
     */
        public AbstractStudentOrder.Path<AbstractStudentOrder> order()
        {
            if(_order == null )
                _order = new AbstractStudentOrder.Path<AbstractStudentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Первая выписка приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getFirstExtract()
     */
        public AbstractStudentExtract.Path<AbstractStudentExtract> firstExtract()
        {
            if(_firstExtract == null )
                _firstExtract = new AbstractStudentExtract.Path<AbstractStudentExtract>(L_FIRST_EXTRACT, this);
            return _firstExtract;
        }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getOrderType()
     */
        public FefuDirectumOrderType.Path<FefuDirectumOrderType> orderType()
        {
            if(_orderType == null )
                _orderType = new FefuDirectumOrderType.Path<FefuDirectumOrderType>(L_ORDER_TYPE, this);
            return _orderType;
        }

    /**
     * @return Отправка выполнена. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isDoneSend()
     */
        public PropertyPath<Boolean> doneSend()
        {
            if(_doneSend == null )
                _doneSend = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_DONE_SEND, this);
            return _doneSend;
        }

    /**
     * @return Дата и время отправки приказа на согласование.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getSendDateTime()
     */
        public PropertyPath<Date> sendDateTime()
        {
            if(_sendDateTime == null )
                _sendDateTime = new PropertyPath<Date>(FefuDirectumSendingOrderGen.P_SEND_DATE_TIME, this);
            return _sendDateTime;
        }

    /**
     * @return Подписывает.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getSigner()
     */
        public PropertyPath<String> signer()
        {
            if(_signer == null )
                _signer = new PropertyPath<String>(FefuDirectumSendingOrderGen.P_SIGNER, this);
            return _signer;
        }

    /**
     * @return Код маршрута. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getRouteCode()
     */
        public PropertyPath<String> routeCode()
        {
            if(_routeCode == null )
                _routeCode = new PropertyPath<String>(FefuDirectumSendingOrderGen.P_ROUTE_CODE, this);
            return _routeCode;
        }

    /**
     * @return Код подразделения в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getOrgUnitCode()
     */
        public PropertyPath<String> orgUnitCode()
        {
            if(_orgUnitCode == null )
                _orgUnitCode = new PropertyPath<String>(FefuDirectumSendingOrderGen.P_ORG_UNIT_CODE, this);
            return _orgUnitCode;
        }

    /**
     * @return Наименование подразделения в Directum.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getOrgUnitTitle()
     */
        public PropertyPath<String> orgUnitTitle()
        {
            if(_orgUnitTitle == null )
                _orgUnitTitle = new PropertyPath<String>(FefuDirectumSendingOrderGen.P_ORG_UNIT_TITLE, this);
            return _orgUnitTitle;
        }

    /**
     * @return Отчисление (выпуск). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isDismiss()
     */
        public PropertyPath<Boolean> dismiss()
        {
            if(_dismiss == null )
                _dismiss = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_DISMISS, this);
            return _dismiss;
        }

    /**
     * @return Восстановление / перевод из числа слушателей в студенты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isRestore()
     */
        public PropertyPath<Boolean> restore()
        {
            if(_restore == null )
                _restore = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_RESTORE, this);
            return _restore;
        }

    /**
     * @return Выплаты из внебюджетных средств. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isOffBudgetPayments()
     */
        public PropertyPath<Boolean> offBudgetPayments()
        {
            if(_offBudgetPayments == null )
                _offBudgetPayments = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_OFF_BUDGET_PAYMENTS, this);
            return _offBudgetPayments;
        }

    /**
     * @return Иностранный студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isForeignStudent()
     */
        public PropertyPath<Boolean> foreignStudent()
        {
            if(_foreignStudent == null )
                _foreignStudent = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_FOREIGN_STUDENT, this);
            return _foreignStudent;
        }

    /**
     * @return Назначение стипендии / материальной помощи / восстановление стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isGrantMatAid()
     */
        public PropertyPath<Boolean> grantMatAid()
        {
            if(_grantMatAid == null )
                _grantMatAid = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_GRANT_MAT_AID, this);
            return _grantMatAid;
        }

    /**
     * @return По студентам-сиротам (ДМП). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isSocial()
     */
        public PropertyPath<Boolean> social()
        {
            if(_social == null )
                _social = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_SOCIAL, this);
            return _social;
        }

    /**
     * @return Практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isPractice()
     */
        public PropertyPath<Boolean> practice()
        {
            if(_practice == null )
                _practice = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_PRACTICE, this);
            return _practice;
        }

    /**
     * @return Назначение / снятие старост групп. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isGroupManager()
     */
        public PropertyPath<Boolean> groupManager()
        {
            if(_groupManager == null )
                _groupManager = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_GROUP_MANAGER, this);
            return _groupManager;
        }

    /**
     * @return Обучающиеся в УВЦ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isUvcStudents()
     */
        public PropertyPath<Boolean> uvcStudents()
        {
            if(_uvcStudents == null )
                _uvcStudents = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_UVC_STUDENTS, this);
            return _uvcStudents;
        }

    /**
     * @return Обучающиеся на ФВО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isFvoStudents()
     */
        public PropertyPath<Boolean> fvoStudents()
        {
            if(_fvoStudents == null )
                _fvoStudents = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_FVO_STUDENTS, this);
            return _fvoStudents;
        }

    /**
     * @return Наложение взысканий. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isPenalty()
     */
        public PropertyPath<Boolean> penalty()
        {
            if(_penalty == null )
                _penalty = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_PENALTY, this);
            return _penalty;
        }

    /**
     * @return Закрепление за магистерской программой. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isMagister()
     */
        public PropertyPath<Boolean> magister()
        {
            if(_magister == null )
                _magister = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_MAGISTER, this);
            return _magister;
        }

    /**
     * @return Приказ по выплатам (СПО). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isPayments()
     */
        public PropertyPath<Boolean> payments()
        {
            if(_payments == null )
                _payments = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_PAYMENTS, this);
            return _payments;
        }

    /**
     * @return Документ гос образца (ДПО). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isStateFormular()
     */
        public PropertyPath<Boolean> stateFormular()
        {
            if(_stateFormular == null )
                _stateFormular = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_STATE_FORMULAR, this);
            return _stateFormular;
        }

    /**
     * @return Международные отношения (Прием). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isInternational()
     */
        public PropertyPath<Boolean> international()
        {
            if(_international == null )
                _international = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_INTERNATIONAL, this);
            return _international;
        }

    /**
     * @return Проверка мед. справок / заключений ВК. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isCheckMedicalDocs()
     */
        public PropertyPath<Boolean> checkMedicalDocs()
        {
            if(_checkMedicalDocs == null )
                _checkMedicalDocs = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_CHECK_MEDICAL_DOCS, this);
            return _checkMedicalDocs;
        }

    /**
     * @return Диплом с отличием. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#isDiplomaSuccess()
     */
        public PropertyPath<Boolean> diplomaSuccess()
        {
            if(_diplomaSuccess == null )
                _diplomaSuccess = new PropertyPath<Boolean>(FefuDirectumSendingOrderGen.P_DIPLOMA_SUCCESS, this);
            return _diplomaSuccess;
        }

    /**
     * @return Принципал, отправивший приказ на согласование. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder#getExecutor()
     */
        public IPrincipalContextGen.Path<IPrincipalContext> executor()
        {
            if(_executor == null )
                _executor = new IPrincipalContextGen.Path<IPrincipalContext>(L_EXECUTOR, this);
            return _executor;
        }

        public Class getEntityClass()
        {
            return FefuDirectumSendingOrder.class;
        }

        public String getEntityName()
        {
            return "fefuDirectumSendingOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
