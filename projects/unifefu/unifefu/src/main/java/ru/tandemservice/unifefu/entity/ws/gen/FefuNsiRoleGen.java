package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.sec.entity.LocalRoleEntity;
import org.tandemframework.sec.entity.Role;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRole;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Роль пользователя НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuNsiRoleGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuNsiRole";
    public static final String ENTITY_NAME = "fefuNsiRole";
    public static final int VERSION_HASH = -612680432;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROLE = "role";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_LOCAL_ROLE_ENTITY = "localRoleEntity";
    public static final String P_NAME = "name";

    private Role _role;     // Роль
    private OrgUnit _orgUnit;     // Подразделение
    private LocalRoleEntity _localRoleEntity;     // Объект проверки прав для локальной роли
    private String _name;     // Наименование роли

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Роль. Свойство не может быть null.
     */
    @NotNull
    public Role getRole()
    {
        return _role;
    }

    /**
     * @param role Роль. Свойство не может быть null.
     */
    public void setRole(Role role)
    {
        dirty(_role, role);
        _role = role;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Объект проверки прав для локальной роли.
     */
    public LocalRoleEntity getLocalRoleEntity()
    {
        return _localRoleEntity;
    }

    /**
     * @param localRoleEntity Объект проверки прав для локальной роли.
     */
    public void setLocalRoleEntity(LocalRoleEntity localRoleEntity)
    {
        dirty(_localRoleEntity, localRoleEntity);
        _localRoleEntity = localRoleEntity;
    }

    /**
     * @return Наименование роли.
     */
    @Length(max=255)
    public String getName()
    {
        return _name;
    }

    /**
     * @param name Наименование роли.
     */
    public void setName(String name)
    {
        dirty(_name, name);
        _name = name;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuNsiRoleGen)
        {
            setRole(((FefuNsiRole)another).getRole());
            setOrgUnit(((FefuNsiRole)another).getOrgUnit());
            setLocalRoleEntity(((FefuNsiRole)another).getLocalRoleEntity());
            setName(((FefuNsiRole)another).getName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuNsiRoleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuNsiRole.class;
        }

        public T newInstance()
        {
            return (T) new FefuNsiRole();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "role":
                    return obj.getRole();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "localRoleEntity":
                    return obj.getLocalRoleEntity();
                case "name":
                    return obj.getName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "role":
                    obj.setRole((Role) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "localRoleEntity":
                    obj.setLocalRoleEntity((LocalRoleEntity) value);
                    return;
                case "name":
                    obj.setName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "role":
                        return true;
                case "orgUnit":
                        return true;
                case "localRoleEntity":
                        return true;
                case "name":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "role":
                    return true;
                case "orgUnit":
                    return true;
                case "localRoleEntity":
                    return true;
                case "name":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "role":
                    return Role.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "localRoleEntity":
                    return LocalRoleEntity.class;
                case "name":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuNsiRole> _dslPath = new Path<FefuNsiRole>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuNsiRole");
    }
            

    /**
     * @return Роль. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRole#getRole()
     */
    public static Role.Path<Role> role()
    {
        return _dslPath.role();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRole#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Объект проверки прав для локальной роли.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRole#getLocalRoleEntity()
     */
    public static LocalRoleEntity.Path<LocalRoleEntity> localRoleEntity()
    {
        return _dslPath.localRoleEntity();
    }

    /**
     * @return Наименование роли.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRole#getName()
     */
    public static PropertyPath<String> name()
    {
        return _dslPath.name();
    }

    public static class Path<E extends FefuNsiRole> extends EntityPath<E>
    {
        private Role.Path<Role> _role;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private LocalRoleEntity.Path<LocalRoleEntity> _localRoleEntity;
        private PropertyPath<String> _name;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Роль. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRole#getRole()
     */
        public Role.Path<Role> role()
        {
            if(_role == null )
                _role = new Role.Path<Role>(L_ROLE, this);
            return _role;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRole#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Объект проверки прав для локальной роли.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRole#getLocalRoleEntity()
     */
        public LocalRoleEntity.Path<LocalRoleEntity> localRoleEntity()
        {
            if(_localRoleEntity == null )
                _localRoleEntity = new LocalRoleEntity.Path<LocalRoleEntity>(L_LOCAL_ROLE_ENTITY, this);
            return _localRoleEntity;
        }

    /**
     * @return Наименование роли.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiRole#getName()
     */
        public PropertyPath<String> name()
        {
            if(_name == null )
                _name = new PropertyPath<String>(FefuNsiRoleGen.P_NAME, this);
            return _name;
        }

        public Class getEntityClass()
        {
            return FefuNsiRole.class;
        }

        public String getEntityName()
        {
            return "fefuNsiRole";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
