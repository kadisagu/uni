package ru.tandemservice.unifefu.base.bo.FefuTrJournal.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.utils.SharedDQLUtils;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author amakarova
 */
public class FefuTrJournalDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    protected DQLOrderDescriptionRegistry buildOrderRegistry()
    {
        return new DQLOrderDescriptionRegistry(TrJournal.class, "e");
    }

    public static final IdentifiableWrapper YES_ITEM = new IdentifiableWrapper(1L, "Да");

    public FefuTrJournalDSHandler(final String ownerId)
    {
        super(ownerId);
    }

    public static final String PARAM_ORG_UNIT = "orgUnit";
    public static final String PARAM_YEAR_PART = "yearPart";
    public static final String PARAM_NUMBER = "number";
    public static final String PARAM_PERIOD_DATE_FROM = "periodDateFrom";
    public static final String PARAM_PERIOD_DATE_TO = "periodDateTo";
    public static final String PARAM_REG_ELEMENT = "registryElementPart";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_RESPONSIBLE = "responsible";
    public static final String PARAM_TUTOR = "tutor";
    public static final String PARAM_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String PARAM_EDUCATIONLEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String PARAM_FIND_IN_CHILD = "findInChild";

    public static final String COLUMN_PPS = "pps";
    public static final String COLUMN_HAS_MARKS_ALERT = "hasMarksAlert";
    public static final String COLUMN_UGS = "groups";

    @Override
    @SuppressWarnings("unchecked")
    protected DSOutput execute(final DSInput input, final ExecutionContext context)
    {
        final Session session = context.getSession();

        DQLSelectBuilder jDQL = registry.buildDQLSelectBuilder();
//        .where(eq(property(TrJournal.registryElementPart().registryElement().owner().fromAlias("e")), value(orgUnit)))
//        .where(eq(property(TrJournal.yearPart().fromAlias("e")), value(yearPart)));

        jDQL = this.applyFilters(jDQL, context);

        DSOutput output = new DSOutput(input);
        if (input.getEntityOrder().getKeyString().equals("number"))
        {
            jDQL.column(property("e.id"));
            jDQL.column(property(TrJournal.number().fromAlias("e")));
            Comparator<Object[]> comparator = output.getEntityOrder().getDirection() == OrderDirection.asc ? UniBaseDao.ID_VALUE_PAIR__NUMBER_AS_STRING__COMPARATOR_DIRECT : UniBaseDao.ID_VALUE_PAIR__NUMBER_AS_STRING__COMPARATOR_INDIRECT;

            List<Object[]> rows = jDQL.createStatement(session).proxyList();
            Collections.sort(rows, comparator);

            final List<Long> ids = UniBaseUtils.getColumn(rows, 0);

            final int count = ids.size();
            output.setTotalSize(count);

            final List<Long> subIds = ids.subList(output.getStartRecord(), Math.min(output.getStartRecord() + output.getCountRecord(), count));
            output.getRecordList().addAll(CommonDAO.sort(IUniBaseDao.instance.get().getList(TrJournal.class, "id", subIds), subIds));
        }
        else
        {
            jDQL.column("e");
            registry.applyOrder(jDQL, input.getEntityOrder());
            output = DQLSelectOutputBuilder.get(input, jDQL, session).build();
        }

        final List<DataWrapper> wrappers = DataWrapper.wrap(output, "id", "title");
        for (final DataWrapper wrapper : wrappers)
        {
            wrapper.put(COLUMN_PPS, StringUtils.EMPTY);
            wrapper.put(COLUMN_HAS_MARKS_ALERT, StringUtils.EMPTY);
        }


        BatchUtils.execute(wrappers, 256, new BatchUtils.Action<DataWrapper>()
        {
            @Override
            public void execute(final Collection<DataWrapper> wrappers)
            {

                final Map<Long, DataWrapper> id2wrapper = CommonDAO.map(wrappers);

                // pps
                {
                    final List<Object[]> rows = new DQLSelectBuilder()
                            .fromEntity(TrJournalGroup.class, "j2g").where(in(property(TrJournalGroup.journal().id().fromAlias("j2g")), id2wrapper.keySet()))
                            .fromEntity(EppPpsCollectionItem.class, "pps2g").where(eq(property(EppPpsCollectionItem.list().fromAlias("pps2g")), property(TrJournalGroup.group().fromAlias("j2g"))))
                            .column(property(TrJournalGroup.journal().id().fromAlias("j2g")))
                            .column(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("pps2g")))
                            .predicate(DQLPredicateType.distinct)
                            .createStatement(session).list();

                    final Map<Long, Collection<String>> id2fio = SafeMap.get(SafeMap.<Long, Collection<String>>createCallback(ArrayList.class), id2wrapper.size());
                    for (final Object[] row : rows)
                    {
                        final Long journalId = (Long) row[0];
                        final String fullFio = (String) row[1];
                        id2fio.get(journalId).add(fullFio);
                    }

                    for (final DataWrapper wrapper : wrappers)
                    {
                        final Collection<String> fio = id2fio.get(wrapper.getId());
                        wrapper.put(COLUMN_PPS, null == fio ? StringUtils.EMPTY : StringUtils.join(fio, '\n'));
                    }
                }

                // marks
                {
                    final List<Long> rows = new DQLSelectBuilder()
                            .fromEntity(TrJournalEvent.class, "e")
                            .where(in(property(TrJournalEvent.journalModule().journal().id().fromAlias("e")), id2wrapper.keySet()))
                            .where(eq(property(TrJournalEvent.hasMarks().fromAlias("e")), value(Boolean.TRUE)))
                            .column(property(TrJournalEvent.journalModule().journal().id().fromAlias("e")))
                            .createStatement(session).list();

                    for (final Long id : rows)
                    {
                        id2wrapper.get(id).put(COLUMN_HAS_MARKS_ALERT, " В реализации уже выставлены оценки. При ее удалении они будут удалены без возможности восстановления.");
                    }
                }

                // groups
                {
                    final List<Object[]> rows = new DQLSelectBuilder()
                            .fromEntity(TrJournalGroup.class, "j2g").where(in(property(TrJournalGroup.journal().id().fromAlias("j2g")), id2wrapper.keySet()))
                            .column(property(TrJournalGroup.journal().id().fromAlias("j2g")))
                            .column(property(TrJournalGroup.group().title().fromAlias("j2g")))
                            .createStatement(session).list();

                    final Map<Long, Collection<String>> id2groupTitle = SafeMap.get(SafeMap.<Long, Collection<String>>createCallback(ArrayList.class), id2wrapper.size());
                    for (final Object[] row : rows)
                    {
                        final Long journalId = (Long) row[0];
                        final String groupTitle = (String) row[1];
                        id2groupTitle.get(journalId).add(groupTitle);
                    }
                    for (final DataWrapper wrapper : wrappers)
                    {
                        final Collection<String> groupsTitle = id2groupTitle.get(wrapper.getId());
                        wrapper.put(COLUMN_UGS, null == groupsTitle ? StringUtils.EMPTY : StringUtils.join(groupsTitle, '\n'));
                    }
                }
            }
        });

        return output;
    }


    protected DQLSelectBuilder applyFilters(final DQLSelectBuilder dql, final ExecutionContext context)
    {
        EppYearPart yearPart = context.get(PARAM_YEAR_PART);
        if (yearPart != null)
        {
            dql.where(eq(property("e", TrJournal.L_YEAR_PART), value(yearPart)));
        }

        // Ответственное подразделение
        {
            final OrgUnit orgUnit = context.get(PARAM_ORG_UNIT);
            if (orgUnit != null)
            {
                final IEntity findIdChild = context.get(PARAM_FIND_IN_CHILD);

                if (findIdChild == null || !YES_ITEM.getId().equals(findIdChild.getId()))
                {
                    dql.where(eq(property(TrJournal.registryElementPart().registryElement().owner().fromAlias("e")), value(orgUnit)));
                }
                else
                {
                    DQLCTEBuilder2 cte = SharedDQLUtils.createChildTreeRecursiveCTE("tree", OrgUnit.class, orgUnit.getId());
                    dql.with(cte.buildCTE());
                    dql.where(eqSubquery(property(TrJournal.registryElementPart().registryElement().owner().fromAlias("e")), DQLSubselectType.any,
                                         new DQLSelectBuilder().fromEntity("tree", "t").buildQuery()));
                }
            }
        }

        // номер
        {
            final String number = context.get(PARAM_NUMBER);
            if (number != null)
            {
                dql.where(DQLExpressions.likeUpper(DQLExpressions.property(TrJournal.number().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(number))));
            }
        }

        // по пересечению периодов
        {
            final Date periodDateFrom = context.get(PARAM_PERIOD_DATE_FROM);
            if (periodDateFrom != null)
            {
                dql.where(ge(property(TrJournal.durationEndDate().fromAlias("e")), valueDate(CoreDateUtils.getDayFirstTimeMoment(periodDateFrom))));
            }

            final Date periodDateTo = context.get(PARAM_PERIOD_DATE_TO);
            if (periodDateTo != null)
            {
                dql.where(lt(property(TrJournal.durationBeginDate().fromAlias("e")), valueDate(CoreDateUtils.getNextDayFirstTimeMoment(periodDateTo, 1))));
            }
        }

        final EppRegistryElementPart registryElement = context.get(PARAM_REG_ELEMENT);
        if (null != registryElement)
            dql.where(eq(property(TrJournal.registryElementPart().fromAlias("e")), value(registryElement)));

        final PpsEntry responsible = context.get(PARAM_RESPONSIBLE);
        if (null != responsible)
            dql.where(eq(property(TrJournal.responsible().fromAlias("e")), value(responsible)));

        final PpsEntry tutor = context.get(PARAM_TUTOR);
        if (null != tutor)
        {
            dql.joinEntity("e", DQLJoinType.inner, TrJournalGroup.class, "grpRel", eq(property(TrJournalGroup.journal().fromAlias("grpRel")), property("e")));
            dql.joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("grpRel"), "grp");
            dql.joinEntity("grp", DQLJoinType.inner, EppPpsCollectionItem.class, "ppsC", eq(property(EppPpsCollectionItem.list().fromAlias("ppsC")), property("grp")));
            dql.where(eq(property(EppPpsCollectionItem.pps().fromAlias("ppsC")), value(tutor)));
        }

        final EppState state = context.get(PARAM_STATE);
        if (null != state)
            dql.where(eq(property(TrJournal.state().fromAlias("e")), value(state)));

        OrgUnit formativeOrgUnit = context.get(PARAM_FORMATIVE_ORG_UNIT);
        EducationLevelsHighSchool educationLevelsHighSchool = context.get(PARAM_EDUCATIONLEVEL_HIGH_SCHOOL);
        if (formativeOrgUnit != null || educationLevelsHighSchool != null)
        {
            DQLSelectBuilder studentsBuilder = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "stu")
                    .column(property("stu", TrJournalGroupStudent.group().journal().id()))
                    .joinPath(DQLJoinType.inner, TrJournalGroupStudent.studentWpe().student().educationOrgUnit().fromAlias("stu"), "eou");

            if (formativeOrgUnit != null)
                studentsBuilder.where(eq(property("eou", EducationOrgUnit.formativeOrgUnit()), value(formativeOrgUnit)));

            if (educationLevelsHighSchool != null)
                studentsBuilder.where(eq(property("eou", EducationOrgUnit.educationLevelHighSchool()), value(educationLevelsHighSchool)));

            dql.where(in("e.id", studentsBuilder.buildQuery()));
        }

        return dql;
    }

}
