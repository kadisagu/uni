/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.registry.FefuRegElementCompetenceList;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.FefuCompetence2RegistryElementRel;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
@Input( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id")
} )
public class Model
{
    private Long _id;
    private final StaticListDataSource<FefuCompetence2RegistryElementRel> _dataSource = new StaticListDataSource<>();
    private String _addCompetencePermissionKey;

    public boolean isDisabledCompetence()
    {
        EppRegistryElement element = DataAccessServices.dao().get(getId());
        if (null == element) return false;

        String stateCode = element.getState().getCode();
        return !stateCode.equals(EppState.STATE_FORMATIVE);
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public StaticListDataSource<FefuCompetence2RegistryElementRel> getDataSource()
    {
        return _dataSource;
    }

    public String getAddCompetencePermissionKey()
    {
        return _addCompetencePermissionKey;
    }

    public void setAddCompetencePermissionKey(String addCompetencePermissionKey)
    {
        _addCompetencePermissionKey = addCompetencePermissionKey;
    }
}