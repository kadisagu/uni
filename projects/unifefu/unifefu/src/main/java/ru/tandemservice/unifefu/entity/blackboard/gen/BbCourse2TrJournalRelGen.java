package ru.tandemservice.unifefu.entity.blackboard.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse2TrJournalRel;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь ЭУК с реализацией дисциплины
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class BbCourse2TrJournalRelGen extends EntityBase
 implements INaturalIdentifiable<BbCourse2TrJournalRelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.blackboard.BbCourse2TrJournalRel";
    public static final String ENTITY_NAME = "bbCourse2TrJournalRel";
    public static final int VERSION_HASH = 156296785;
    private static IEntityMeta ENTITY_META;

    public static final String L_BB_COURSE = "bbCourse";
    public static final String L_TR_JOURNAL = "trJournal";

    private BbCourse _bbCourse;     // Электронный учебный курс (ЭУК) в системе Blackboard
    private TrJournal _trJournal;     // Реализация дисциплины

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     */
    @NotNull
    public BbCourse getBbCourse()
    {
        return _bbCourse;
    }

    /**
     * @param bbCourse Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     */
    public void setBbCourse(BbCourse bbCourse)
    {
        dirty(_bbCourse, bbCourse);
        _bbCourse = bbCourse;
    }

    /**
     * @return Реализация дисциплины. Свойство не может быть null.
     */
    @NotNull
    public TrJournal getTrJournal()
    {
        return _trJournal;
    }

    /**
     * @param trJournal Реализация дисциплины. Свойство не может быть null.
     */
    public void setTrJournal(TrJournal trJournal)
    {
        dirty(_trJournal, trJournal);
        _trJournal = trJournal;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof BbCourse2TrJournalRelGen)
        {
            if (withNaturalIdProperties)
            {
                setBbCourse(((BbCourse2TrJournalRel)another).getBbCourse());
                setTrJournal(((BbCourse2TrJournalRel)another).getTrJournal());
            }
        }
    }

    public INaturalId<BbCourse2TrJournalRelGen> getNaturalId()
    {
        return new NaturalId(getBbCourse(), getTrJournal());
    }

    public static class NaturalId extends NaturalIdBase<BbCourse2TrJournalRelGen>
    {
        private static final String PROXY_NAME = "BbCourse2TrJournalRelNaturalProxy";

        private Long _bbCourse;
        private Long _trJournal;

        public NaturalId()
        {}

        public NaturalId(BbCourse bbCourse, TrJournal trJournal)
        {
            _bbCourse = ((IEntity) bbCourse).getId();
            _trJournal = ((IEntity) trJournal).getId();
        }

        public Long getBbCourse()
        {
            return _bbCourse;
        }

        public void setBbCourse(Long bbCourse)
        {
            _bbCourse = bbCourse;
        }

        public Long getTrJournal()
        {
            return _trJournal;
        }

        public void setTrJournal(Long trJournal)
        {
            _trJournal = trJournal;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof BbCourse2TrJournalRelGen.NaturalId) ) return false;

            BbCourse2TrJournalRelGen.NaturalId that = (NaturalId) o;

            if( !equals(getBbCourse(), that.getBbCourse()) ) return false;
            if( !equals(getTrJournal(), that.getTrJournal()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBbCourse());
            result = hashCode(result, getTrJournal());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBbCourse());
            sb.append("/");
            sb.append(getTrJournal());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends BbCourse2TrJournalRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) BbCourse2TrJournalRel.class;
        }

        public T newInstance()
        {
            return (T) new BbCourse2TrJournalRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "bbCourse":
                    return obj.getBbCourse();
                case "trJournal":
                    return obj.getTrJournal();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "bbCourse":
                    obj.setBbCourse((BbCourse) value);
                    return;
                case "trJournal":
                    obj.setTrJournal((TrJournal) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "bbCourse":
                        return true;
                case "trJournal":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "bbCourse":
                    return true;
                case "trJournal":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "bbCourse":
                    return BbCourse.class;
                case "trJournal":
                    return TrJournal.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<BbCourse2TrJournalRel> _dslPath = new Path<BbCourse2TrJournalRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "BbCourse2TrJournalRel");
    }
            

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2TrJournalRel#getBbCourse()
     */
    public static BbCourse.Path<BbCourse> bbCourse()
    {
        return _dslPath.bbCourse();
    }

    /**
     * @return Реализация дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2TrJournalRel#getTrJournal()
     */
    public static TrJournal.Path<TrJournal> trJournal()
    {
        return _dslPath.trJournal();
    }

    public static class Path<E extends BbCourse2TrJournalRel> extends EntityPath<E>
    {
        private BbCourse.Path<BbCourse> _bbCourse;
        private TrJournal.Path<TrJournal> _trJournal;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Blackboard. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2TrJournalRel#getBbCourse()
     */
        public BbCourse.Path<BbCourse> bbCourse()
        {
            if(_bbCourse == null )
                _bbCourse = new BbCourse.Path<BbCourse>(L_BB_COURSE, this);
            return _bbCourse;
        }

    /**
     * @return Реализация дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbCourse2TrJournalRel#getTrJournal()
     */
        public TrJournal.Path<TrJournal> trJournal()
        {
            if(_trJournal == null )
                _trJournal = new TrJournal.Path<TrJournal>(L_TR_JOURNAL, this);
            return _trJournal;
        }

        public Class getEntityClass()
        {
            return BbCourse2TrJournalRel.class;
        }

        public String getEntityName()
        {
            return "bbCourse2TrJournalRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
