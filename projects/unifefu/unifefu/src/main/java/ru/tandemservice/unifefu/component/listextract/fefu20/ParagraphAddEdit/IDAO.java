/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;

/**
 * @author Andrey Andreev
 * @since 13.01.2016
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuAdmittedToGIAListExtract, Model>
{
}
