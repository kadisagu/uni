/* $Id$ */
package ru.tandemservice.unifefu.dao;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.primitives.Doubles;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionConfig;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.EntranceDiscipline;
import ru.tandemservice.uniec.entity.examset.ExamPriorityComparator;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionServiceDao;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;


/**
 * @author Ekaterina Zvereva
 * @since 06.08.2014
 */
public class FefuEnrollmentDistributionServiceDao extends EnrollmentDistributionServiceDao
{
    @Override
    protected List<Discipline2RealizationWayRelation> getDiscipline2RealizationWayRelations(EcgDistributionConfig config, EnrollmentCampaign enrollmentCampaign, List<StudentCategory> studentCategoryList, List<EnrollmentDirection> distributionDirectionList)
    {
        Collection<Discipline2RealizationWayRelation> listDiscipline = super.getDiscipline2RealizationWayRelations(config, enrollmentCampaign, studentCategoryList, distributionDirectionList);
        Collection<EntranceDiscipline> sortedDisciplineList = getDisciplineList(studentCategoryList, distributionDirectionList);
        List<Discipline2RealizationWayRelation> result = new ArrayList<>();
        for (EntranceDiscipline disc : sortedDisciplineList)
        {
            if (disc.getDiscipline() instanceof Discipline2RealizationWayRelation && listDiscipline.contains(disc.getDiscipline()))
                result.add((Discipline2RealizationWayRelation) disc.getDiscipline());
        }
        return result;
    }

    private Collection<EntranceDiscipline> getDisciplineList(List<StudentCategory> studentCategoryList, List<EnrollmentDirection> distributionDirectionList)
    {
        Collection<EntranceDiscipline> resultList = null;
        ExamPriorityComparator comparator = new ExamPriorityComparator();

        List<EntranceDiscipline> entranceDisciplineList = new DQLSelectBuilder().fromEntity(EntranceDiscipline.class, "d")
                .column("d")
                .where(in(property(EntranceDiscipline.enrollmentDirection().fromAlias("d")), distributionDirectionList))
                .where(in(property(EntranceDiscipline.studentCategory().fromAlias("d")), studentCategoryList))
                .createStatement(getSession()).list();

        Table<EnrollmentDirection, StudentCategory, Collection<EntranceDiscipline>> table = HashBasedTable.create(entranceDisciplineList.size() / studentCategoryList.size(), studentCategoryList.size());
        for (EntranceDiscipline discipline : entranceDisciplineList)
        {
            Collection<EntranceDiscipline> cell = table.get(discipline.getEnrollmentDirection(), discipline.getStudentCategory());
            if (cell == null)
            {
                cell = new TreeSet<>(comparator);
                table.put(discipline.getEnrollmentDirection(), discipline.getStudentCategory(), cell);
            }
            cell.add(discipline);
        }

        for (Collection<EntranceDiscipline> cell : table.values())
        {
            if (resultList == null)
            {
                resultList = cell;
            }
            else
            {
                if (cell.size() != resultList.size())
                    throw new ApplicationException("В рамках конкурсной группы наборы ВИ отличаются.");

                //списки отсортированы по приоритету, нужно проверить последовательность дисциплин
                Iterator<EntranceDiscipline> originalIterator = resultList.iterator();
                Iterator<EntranceDiscipline> currentIterator = cell.iterator();
                while (originalIterator.hasNext())
                {
                    if (!originalIterator.next().getDiscipline().equals(currentIterator.next().getDiscipline()))
                        throw new ApplicationException("В рамках конкурсной группы наборы ВИ отличаются составом или приоритетом.");
                }
            }
        }
        return resultList;
    }

    @Override
    protected void sortEntrantNodeIfNeed(List<EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow> list, EcgDistributionConfig config, EnrollmentDistributionEnvironmentNode env)
    {
        Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap = new HashMap<>();
        for (EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow row : env.targetAdmissionKind.row)
        {
            targetAdmissionKindMap.put(row.id, row);
        }

        EntrantRowComparator comparator = new EntrantRowComparator(UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(config.getEcgItem().getEnrollmentCampaign()), config.getEcgItem().getEnrollmentCampaign().isPrioritiesAdmissionTest(), targetAdmissionKindMap);
        Collections.sort(list, comparator);
    }


    private class EntrantRowComparator implements Comparator<EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow>
    {
        private Map<String, Integer> _competitionKindPriorities = new HashMap<>();
        private boolean isConsiderPriorityDisc;
        private Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> _targetMap;

        public EntrantRowComparator(Map<CompetitionKind, Integer> competitionKindPriorities, boolean priorityConsider, Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> mapTarget)
        {
            isConsiderPriorityDisc = priorityConsider;
            for (CompetitionKind temp : competitionKindPriorities.keySet())
            {
                _competitionKindPriorities.put(temp == null ? null : temp.getCode(), competitionKindPriorities.get(temp));
            }
            _targetMap = mapTarget;
        }

        @Override
        public int compare(EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow o1, EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow o2)
        {
            int result;

            String targetAdmission1 = o1.targetAdmissionKind;
            String targetAdmission2 = o2.targetAdmissionKind;

            if (targetAdmission1 != null && targetAdmission2 != null)
            {
                // если оба по целевому приему, то сравниваем по приоритетам целевого приема
                result = _targetMap.get(targetAdmission1).priority - _targetMap.get(targetAdmission2).priority;
                if (result != 0)
                    return result;

                // сравниваем по приоритетам видов конкурса
                String competitionKindCode1 = o1.competitionKind;
                String competitionKindCode2 = o2.competitionKind;

                // сравнить по приоритетам видов конкурса
                result = _competitionKindPriorities.get(competitionKindCode1) - _competitionKindPriorities.get(competitionKindCode2);
            }
            else
            {
                // получаем виды конкурса, для целевого приема — null
                String competitionKindCode1 = targetAdmission1 != null ? null : o1.competitionKind;
                String competitionKindCode2 = targetAdmission2 != null ? null : o2.competitionKind;

                // сравнить по приоритетам видов конкурса
                result = _competitionKindPriorities.get(competitionKindCode1) - _competitionKindPriorities.get(competitionKindCode2);
            }

            if (result != 0)
                return result;

            // сначала сравниваем по сумме баллов
            result = -UniBaseUtils.compare(safelyParseStringToDouble(o1.finalMark), safelyParseStringToDouble(o2.finalMark), true);
            if (result != 0) return result;

            //по баллам за дисциплину с наивысшим приоритетом
            //по логике программы список оценок должен иметь одинаковую длину
            if (isConsiderPriorityDisc)
            {
                for (int i = 0; i < o1.marks.size(); i++)
                {
                    result = -UniBaseUtils.compare(safelyParseStringToDouble(o1.marks.get(i)), safelyParseStringToDouble(o2.marks.get(i)), true);
                    if (result != 0) return result;
                }
            }
            else
            {
                // теперь по баллам за профильное вступительное
                result = -UniBaseUtils.compare(safelyParseStringToDouble(o1.profileMark), safelyParseStringToDouble(o2.profileMark), true);
            }

            // теперь по среднему баллу аттестата
            if (result == 0)
                result = -UniBaseUtils.compare(safelyParseStringToDouble(o1.averageEduInstitutionMark), safelyParseStringToDouble(o2.averageEduInstitutionMark), true);

            // все осмысленное кончилось, теперь по фио
            if (result == 0)
                result = CommonCollator.RUSSIAN_COLLATOR.compare(o1.fio, o2.fio);

            return result;
        }

        private Double safelyParseStringToDouble(String value)
        {
            if (value == null) return null;

            return Doubles.tryParse(value);
        }
    }
}