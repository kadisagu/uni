/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu11.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 25.04.2013
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuAcadGrantAssignStuEnrolmentExtract>
{
    private double _grantSize;
    private double _groupManagerBonusSize;

    public double getGrantSize()
    {
        return _grantSize;
    }

    public void setGrantSize(double grantSize)
    {
        _grantSize = grantSize;
    }

    public double getGroupManagerBonusSize()
    {
        return _groupManagerBonusSize;
    }

    public void setGroupManagerBonusSize(double groupManagerBonusSize)
    {
        _groupManagerBonusSize = groupManagerBonusSize;
    }
}