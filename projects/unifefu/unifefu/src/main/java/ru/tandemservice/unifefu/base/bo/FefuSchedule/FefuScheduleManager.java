/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSchedule;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.logic.*;

/**
 * @author nvankov
 * @since 9/2/13
 */
@Configuration
public class FefuScheduleManager extends BusinessObjectManager
{
    public static FefuScheduleManager instance()
    {
        return instance(FefuScheduleManager.class);
    }

    @Bean
    public IFefuScheduleDAO dao()
    {
        return new FefuScheduleDAO();
    }

    @Bean
    public IFefuScheduleEventsDAO eventsDao()
    {
        return new FefuScheduleEventsDAO();
    }


//    @Bean
//    public IDefaultSearchDataSourceHandler fefuScheduleDSHandler()
//    {
//        return new FefuScheduleDSHandler(getName());
//    }
//
//    @Bean
//    public IDefaultSearchDataSourceHandler fefuScheduleBellsDSHandler()
//    {
//        return new FefuScheduleBellsDSHandler(getName());
//    }

    //    @Bean
//    public IDefaultComboDataSourceHandler orgUnitComboDSHandler()
//    {
//        return new FefuOrgUnitComboDSHandler(getName());
//    }
//
//    @Bean
//    public IDefaultSearchDataSourceHandler fefuScheduleSeasonDSHandler()
//    {
//        return new FefuScheduleSeasonDSHandler(getName());
//    }
//
//    @Bean
//    public IDefaultComboDataSourceHandler groupComboDSHandler()
//    {
//        return new FefuScheduleGroupComboDSHandler(getName());
//    }
//
//    @Bean
//    public IDefaultComboDataSourceHandler seasonComboDSHandler()
//    {
//        return new DefaultComboDataSourceHandler(getName(), SppScheduleSeason.class, SppScheduleSeason.title());
//    }
//
    @Bean
    public IDefaultComboDataSourceHandler schedulesComboDSHandler()
    {
        return new FefuSchedulesComboDSHandler(getName());
    }

    @Bean
    public XStream xStream()
    {
        return new XStream(new StaxDriver());
    }

    @Bean
    public IDefaultComboDataSourceHandler termComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(termOptionsExtPoint());
    }

    //Семестры
    public final static Long TERM_SPRING = 1L;
    public final static Long TERM_AUTUMN = 2L;


    @Bean
    public ItemListExtPoint<DataWrapper> termOptionsExtPoint()
    {
        return itemList(DataWrapper.class).
                add(TERM_SPRING.toString(), new DataWrapper(TERM_SPRING, "ui.term.spring")).
                add(TERM_AUTUMN.toString(), new DataWrapper(TERM_AUTUMN, "ui.term.autumn")).
                create();
    }

    @Bean
    public IDefaultComboDataSourceHandler fefuGroupComboDSHandler()
    {
        return new GroupComboDSHandler(getName());
    }
}
