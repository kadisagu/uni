/* $Id$ */
package ru.tandemservice.unifefu.ws.eventsAxis2;

import ru.tandemservice.unifefu.utils.fefuICal.FefuScheduleVEventICalXST;
import ru.tandemservice.unifefu.utils.fefuICal.FefuTeacherScheduleVEventICalXST;

import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 9/20/13
 */
public class FefuTeacherScheduleICalCommits
{

    private Map<String, FefuTeacherScheduleVEventICalXST> _vEventsMap;
    private List<FefuTeacherScheduleVEventICalXST> _commitedVEvents;
    private List<FefuTeacherScheduleVEventICalXST> _failedVEvents;
    private boolean _requestFailed = false;

    public Map<String, FefuTeacherScheduleVEventICalXST> getvEventsMap()
    {
        return _vEventsMap;
    }

    public void setvEventsMap(Map<String, FefuTeacherScheduleVEventICalXST> vEventsMap)
    {
        _vEventsMap = vEventsMap;
    }

    public List<FefuTeacherScheduleVEventICalXST> getCommitedVEvents()
    {
        return _commitedVEvents;
    }

    public void setCommitedVEvents(List<FefuTeacherScheduleVEventICalXST> commitedVEvents)
    {
        _commitedVEvents = commitedVEvents;
    }

    public List<FefuTeacherScheduleVEventICalXST> getFailedVEvents()
    {
        return _failedVEvents;
    }

    public void setFailedVEvents(List<FefuTeacherScheduleVEventICalXST> failedVEvents)
    {
        _failedVEvents = failedVEvents;
    }

    public boolean isRequestFailed()
    {
        return _requestFailed;
    }

    public void setRequestFailed(boolean requestFailed)
    {
        _requestFailed = requestFailed;
    }
}
