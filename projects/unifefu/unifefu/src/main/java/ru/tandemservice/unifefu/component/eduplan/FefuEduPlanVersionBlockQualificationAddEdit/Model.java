/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionBlockQualificationAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unifefu.entity.FefuQualification;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
@Input({
    @Bind(key = "qualificationId", binding = "qualificationId"),
    @Bind(key = "blockId", binding = "blockId")
})
public class Model
{
    private Long _qualificationId;
    private Long _blockId;
    private FefuQualification _qualification;
    private Integer _number;

    public Long getQualificationId()
    {
        return _qualificationId;
    }

    public void setQualificationId(Long qualificationId)
    {
        _qualificationId = qualificationId;
    }

    public Long getBlockId()
    {
        return _blockId;
    }

    public void setBlockId(Long blockId)
    {
        _blockId = blockId;
    }

    public FefuQualification getQualification()
    {
        return _qualification;
    }

    public void setQualification(FefuQualification qualification)
    {
        _qualification = qualification;
    }

    public Integer getNumber()
    {
        return _number;
    }

    public void setNumber(Integer number)
    {
        _number = number;
    }
}