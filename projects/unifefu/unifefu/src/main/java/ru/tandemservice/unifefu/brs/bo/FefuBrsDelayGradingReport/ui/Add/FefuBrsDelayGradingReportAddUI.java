/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unifefu.brs.base.BaseFefuReportAddUI;
import ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport.FefuBrsDelayGradingReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport.logic.FefuBrsDelayGradingReportParams;
import ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport.ui.View.FefuBrsDelayGradingReportView;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.report.FefuBrsDelayGradingReport;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * @author nvankov
 * @since 12/10/13
 */
public class FefuBrsDelayGradingReportAddUI extends BaseFefuReportAddUI<FefuBrsDelayGradingReportParams>
{
    @Override
    protected void fillDefaults(FefuBrsDelayGradingReportParams reportSettings)
    {
        super.fillDefaults(reportSettings);
        reportSettings.setFormativeOrgUnit(getCurrentFormativeOrgUnit());
        reportSettings.setCheckDate(new Date());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuBrsReportManager.GROUP_DS.equals(dataSource.getName()))
        {
            if (null != getReportSettings().getFormativeOrgUnit())
                dataSource.put(FefuBrsReportManager.PARAM_FORMATIVE_OU, Collections.singletonList(getReportSettings().getFormativeOrgUnit().getId()));
        }
    }

    public void onClickApply()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getReportSettings().getCheckDate());
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        getReportSettings().setCheckDate(calendar.getTime());
        FefuBrsDelayGradingReport report = FefuBrsDelayGradingReportManager.instance().dao().createReport(getReportSettings());
        deactivate();
        _uiActivation.asDesktopRoot(FefuBrsDelayGradingReportView.class).parameter(UIPresenter.PUBLISHER_ID, report.getId()).activate();
    }
}
