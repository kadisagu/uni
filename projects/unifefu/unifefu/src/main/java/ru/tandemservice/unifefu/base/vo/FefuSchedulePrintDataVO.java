package ru.tandemservice.unifefu.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

import java.util.Date;
import java.util.List;

public class FefuSchedulePrintDataVO
{
    private OrgUnit _formativeOrgUnit; // формирующее подразделение
    private OrgUnit _territorialOrgUnit; // территориальное подразделение
    private List<EducationLevelsHighSchool> _eduLevels; // направление подготовки (специальность)
    private DevelopForm _developForm;
    private Course _course; // курс
    private SppScheduleSeason _season; // период расписания - обяз. сел;
    private ScheduleBell _bells; // звонковое расписание - обяз. сел;
    private List<SppSchedule> _schedules; // расписание - мультиселект - (формат - колонки название расписания, группа)
    private DataWrapper _term; // семестр - селект, обяз;
    private Date _eduStartDate; // дата начала занятий - дата обяз.
    private EmployeePost _chief; // руководитель подразделения
    private EmployeePost _chiefUMU; // начальник УМУ подразделения
    private EmployeePost _admin; // администратор ООП - обяз.сел. - по-умолчанию текущий пользовател2.
    private String _adminPhoneNum; // телефон администратора ООП - текст. - подставлять телефон адм. ООП;
    private EducationYear _educationYear;
    private OrgUnit _currentOrgUnit;

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public List<EducationLevelsHighSchool> getEduLevels()
    {
        return _eduLevels;
    }

    public void setEduLevels(List<EducationLevelsHighSchool> eduLevels)
    {
        _eduLevels = eduLevels;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public SppScheduleSeason getSeason()
    {
        return _season;
    }

    public void setSeason(SppScheduleSeason season)
    {
        _season = season;
    }

    public ScheduleBell getBells()
    {
        return _bells;
    }

    public void setBells(ScheduleBell bells)
    {
        _bells = bells;
    }

    public List<SppSchedule> getSchedules()
    {
        return _schedules;
    }

    public void setSchedules(List<SppSchedule> schedules)
    {
        _schedules = schedules;
    }

    public DataWrapper getTerm()
    {
        return _term;
    }

    public void setTerm(DataWrapper term)
    {
        _term = term;
    }

    public Date getEduStartDate()
    {
        return _eduStartDate;
    }

    public void setEduStartDate(Date eduStartDate)
    {
        _eduStartDate = eduStartDate;
    }

    public EmployeePost getAdmin()
    {
        return _admin;
    }

    public void setAdmin(EmployeePost admin)
    {
        _admin = admin;
    }

    public String getAdminPhoneNum()
    {
        return _adminPhoneNum;
    }

    public void setAdminPhoneNum(String adminPhoneNum)
    {
        _adminPhoneNum = adminPhoneNum;
    }

    public OrgUnit getCurrentOrgUnit()
    {
        return _currentOrgUnit;
    }

    public void setCurrentOrgUnit(OrgUnit currentOrgUnit)
    {
        _currentOrgUnit = currentOrgUnit;
    }

    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        _educationYear = educationYear;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public EmployeePost getChief()
    {
        return _chief;
    }

    public void setChief(EmployeePost chief)
    {
        _chief = chief;
    }

    public EmployeePost getChiefUMU()
    {
        return _chiefUMU;
    }

    public void setChiefUMU(EmployeePost chiefUMU)
    {
        _chiefUMU = chiefUMU;
    }
}
