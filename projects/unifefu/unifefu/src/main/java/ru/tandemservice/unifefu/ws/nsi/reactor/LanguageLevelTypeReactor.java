/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguageSkill;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.LanguageLevelType;

/**
 * @author Dmitry Seleznev
 * @since 24.08.2013
 */
public class LanguageLevelTypeReactor extends SimpleCatalogEntityNewReactor<LanguageLevelType, ForeignLanguageSkill>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<ForeignLanguageSkill> getEntityClass()
    {
        return ForeignLanguageSkill.class;
    }

    @Override
    public Class<LanguageLevelType> getNSIEntityClass()
    {
        return LanguageLevelType.class;
    }

    @Override
    public LanguageLevelType getCatalogElementRetrieveRequestObject(String guid)
    {
        LanguageLevelType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createLanguageLevelType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public LanguageLevelType createEntity(CoreCollectionUtils.Pair<ForeignLanguageSkill, FefuNsiIds> entityPair)
    {
        LanguageLevelType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createLanguageLevelType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setLanguageLevelID(getUserCodeForNSIChecked(entityPair.getX().getUserCode()));
        nsiEntity.setLanguageLevelName(entityPair.getX().getTitle());
        return nsiEntity;
    }

    @Override
    public ForeignLanguageSkill createEntity(LanguageLevelType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getLanguageLevelName()) return null;

        ForeignLanguageSkill entity = new ForeignLanguageSkill();
        entity.setTitle(nsiEntity.getLanguageLevelName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(ForeignLanguageSkill.class));
        entity.setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getLanguageLevelID()));
        return entity;
    }

    @Override
    public ForeignLanguageSkill updatePossibleDuplicateFields(LanguageLevelType nsiEntity, CoreCollectionUtils.Pair<ForeignLanguageSkill, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), ForeignLanguageSkill.title().s()))
                entityPair.getX().setTitle(nsiEntity.getLanguageLevelName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), ForeignLanguageSkill.userCode().s()))
                entityPair.getX().setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getLanguageLevelID()));
        }
        return entityPair.getX();
    }

    @Override
    public LanguageLevelType updateNsiEntityFields(LanguageLevelType nsiEntity, ForeignLanguageSkill entity)
    {
        nsiEntity.setLanguageLevelName(entity.getTitle());
        nsiEntity.setLanguageLevelID(getUserCodeForNSIChecked(entity.getUserCode()));
        return nsiEntity;
    }
}