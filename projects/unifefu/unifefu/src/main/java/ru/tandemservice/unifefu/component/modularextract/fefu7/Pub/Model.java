/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu7.Pub;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract;
import ru.tandemservice.movestudent.utils.SessionPartModel;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class Model extends ModularStudentExtractPubModel<FefuAcadGrantAssignStuExtract>
{
    public String getSessionTitle()
    {
        return SessionPartModel.getSessionShortTitle(getExtract().getSessionType(), GrammaCase.NOMINATIVE);
    }
}