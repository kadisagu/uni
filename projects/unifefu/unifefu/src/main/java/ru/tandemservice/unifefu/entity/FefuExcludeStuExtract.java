package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.unifefu.entity.gen.FefuExcludeStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. Об отчислении (ДВФУ)
 */
public class FefuExcludeStuExtract extends FefuExcludeStuExtractGen implements IExcludeExtract
{
    @Override
    public Date getExcludeDate()
    {
        return getBeginDate();
    }
}