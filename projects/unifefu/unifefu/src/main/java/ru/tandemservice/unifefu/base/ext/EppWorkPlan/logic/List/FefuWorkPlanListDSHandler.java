/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EppWorkPlan.logic.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.logic.List.EppWorkPlanListDSHandler;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.unifefu.entity.eduPlan.FefuChangedWorkPlan;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkPlanAgreementCheckingData;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Katkov
 * @since 10.03.2016
 */
public class FefuWorkPlanListDSHandler extends EppWorkPlanListDSHandler
{
    public static final String P_EXIST_ACTIVE_STUDENT = "existActiveStudent";
    public static final String P_CHANGED = "changed";
    public static final String P_EXIST_UNALLOCATED_DISCIPLINE = "existUnallocatedDiscipline";

    public FefuWorkPlanListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        DSOutput output = super.execute(input, context);

        Set<Long> workPlanIdWithActiveStudent = getWorkPlanIdWithActiveStudent(output);
        Map<Long, Boolean> workPlanIdChanged = getWorkPlanIdChanged(output);
        Map<Long, Boolean> workPlanIdWithCheckedData = getWorkPlanIdWithCheckedData(output);

        for(DataWrapper dataWrapper : DataWrapper.wrap(output)){
            Long id = dataWrapper.getId();
            dataWrapper.setProperty(P_EXIST_ACTIVE_STUDENT, workPlanIdWithActiveStudent.contains(id));
            dataWrapper.setProperty(P_CHANGED, workPlanIdChanged.getOrDefault(id, false));
            dataWrapper.setProperty(P_EXIST_UNALLOCATED_DISCIPLINE, workPlanIdWithCheckedData.getOrDefault(id, false));
        }
        return output;
    }

    private Set<Long> getWorkPlanIdWithActiveStudent(DSOutput output) {
        String ewp = "ewp";
        String ewpv = "ewpv";
        String swp = "swp";

        DQLSelectBuilder EWPVStudentBuilder = new DQLSelectBuilder().fromEntity(EppStudent2WorkPlan.class, swp)
                .column(property(ewpv, EppWorkPlanVersion.parent().id()))
                .where(isNull(property(swp,EppStudent2WorkPlan.removalDate())))
                .where(eq(property(swp, EppStudent2WorkPlan.studentEduPlanVersion().student().status().active()), value(Boolean.TRUE)))
                .where(eq(property(swp, EppStudent2WorkPlan.studentEduPlanVersion().student().archival()), value(Boolean.FALSE)))
                .joinEntity(swp, DQLJoinType.inner, EppWorkPlanVersion.class, ewpv, eq(property(ewpv, EppWorkPlanVersion.P_ID), property(swp, EppStudent2WorkPlan.workPlan().id())));

        DQLSelectBuilder EWPStudentBuilder = new DQLSelectBuilder().fromEntity(EppStudent2WorkPlan.class, swp)
                .column(property(ewp, EppWorkPlan.id()))
                .where(in(EppWorkPlan.id().fromAlias(ewp), output.getRecordIds()))
                .where(isNull(property(swp,EppStudent2WorkPlan.removalDate())))
                .where(eq(property(swp, EppStudent2WorkPlan.studentEduPlanVersion().student().status().active()), value(Boolean.TRUE)))
                .where(eq(property(swp, EppStudent2WorkPlan.studentEduPlanVersion().student().archival()), value(Boolean.FALSE)))
                .joinEntity(swp, DQLJoinType.inner, EppWorkPlan.class, ewp, eq(property(ewp, EppWorkPlan.P_ID), property(swp, EppStudent2WorkPlan.workPlan().id())))
                .union(EWPVStudentBuilder.buildQuery());

        return new HashSet<>(EWPStudentBuilder.createStatement(getSession()).<Long>list());
    }

    private Map<Long, Boolean> getWorkPlanIdWithCheckedData(DSOutput output){
        DQLSelectBuilder EWPStudentBuilder = new DQLSelectBuilder()
                .fromEntity(FefuWorkPlanAgreementCheckingData.class, "d")
                .column(property("d", FefuWorkPlanAgreementCheckingData.eppWorkPlan().id()))
                .column(property("d", FefuWorkPlanAgreementCheckingData.P_NO_UNALLOCATED_DISCIPLINE))
                .where(in(FefuWorkPlanAgreementCheckingData.eppWorkPlan().id().fromAlias("d"), output.getRecordIds()));
        Map<Long, Boolean> resultMap = new HashMap<>();
        for (Object[] item : EWPStudentBuilder.createStatement(getSession()).<Object[]>list())
        {
            Long workPlanId = (Long) item[0];
            Boolean noUnallocatedDiscipline = (Boolean) item[1];
            resultMap.put(workPlanId, noUnallocatedDiscipline);
        }
        return resultMap;
    }

    private Map<Long, Boolean> getWorkPlanIdChanged(DSOutput output)
    {
        DQLSelectBuilder EWPStudentBuilder = new DQLSelectBuilder()
                .fromEntity(FefuChangedWorkPlan.class, "c")
                .column(property("c", FefuChangedWorkPlan.eppWorkPlan().id()))
                .column(property("c", FefuChangedWorkPlan.P_CHANGED))
                .where(in(FefuChangedWorkPlan.eppWorkPlan().id().fromAlias("c"), output.getRecordIds()));
        Map<Long, Boolean> resultMap = new HashMap<>();
        for (Object[] item : EWPStudentBuilder.createStatement(getSession()).<Object[]>list())
        {
            Long workPlanId = (Long) item[0];
            Boolean changed = (Boolean) item[1];
            resultMap.put(workPlanId, changed);
        }
        return resultMap;
    }
}

