/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.FefuEduInstitutionStep;

/**
 * @author Nikolay Fedorovskih
 * @since 16.05.2013
 */
public class Model extends ru.tandemservice.uniec.component.wizard.EduInstitutionStep.Model
{
    private String eduInstitutionStr;

    public String getEduInstitutionStr()
    {
        return eduInstitutionStr;
    }

    public void setEduInstitutionStr(String eduInstitutionStr)
    {
        this.eduInstitutionStr = eduInstitutionStr;
    }
}