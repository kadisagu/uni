/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.FastCreator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.EcEnrollmentOrderTypeHandler;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.OrderBasicsDSHandler;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderReason;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.logic.PreStudentHandler;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.logic.QualificationHandler;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.vo.PreStudentVO;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.EnrCampaignFormativeOrgUnitDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.EnrCampaignTerritorialOrgUnitDSHandler;
import ru.tandemservice.unifefu.entity.catalog.FefuEnrollmentStep;

import java.util.Arrays;

/**
 * @author Nikolay Fedorovskih
 * @since 17.07.2013
 */
@Configuration
public class FefuEcOrderFastCreator extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String ORDER_TYPE_DS = "orderTypeDS";
    public static final String EMPLOYEE_POST_DS = "employeePostDS";
    public static final String ORDER_REASON_DS = "orderReasonDS";
    public static final String ORDER_BASICS_DS = "orderBasicsDS";

    public static final String QUALIFICATION_DS = "qualificationDS";
    public static final String FORMATIVE_ORGUNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_ORGUNIT_DS = "territorialOrgUnitDS";
    public static final String CITIZENSHIP_DS = "citizenshipDS";
    public static final String ENROLLMENT_STEP_DS = "enrollmentStepDS";

    public static final String PRE_STUDENT_DS = "preStudentsDS";

    public static final String ENROLLMENT_CAMPAIGN_PARAM = EcEnrollmentOrderTypeHandler.ENROLLMENT_CAMPAIGN_PARAM;
    public static final String ORDER_REASON_PARAM = OrderBasicsDSHandler.ORDER_REASON_PARAM;

    public static final String CHECKBOX_COLUMN = "checkbox";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(selectDS(ORDER_TYPE_DS, orderTypeDSHandler()))
                .addDataSource(selectDS(EMPLOYEE_POST_DS, employeePostDSHandler()).addColumn(EmployeePost.P_FULL_TITLE))
                .addDataSource(selectDS(ORDER_REASON_DS, orderReasonDSHandler()))
                .addDataSource(selectDS(ORDER_BASICS_DS, orderBasicsDSHandler()))
                .addDataSource(selectDS(QUALIFICATION_DS, qualificationDSHandler()))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developTechDSConfig())
                .addDataSource(selectDS(FORMATIVE_ORGUNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(TERRITORIAL_ORGUNIT_DS, territorialOrgUnitDSHandler()))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(CITIZENSHIP_DS, citizenshipDSHandler()))
                .addDataSource(selectDS(ENROLLMENT_STEP_DS, enrollmentStepDSHandler()))
                .addDataSource(searchListDS(PRE_STUDENT_DS, getPreStudentsDS(), preStudentDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getPreStudentsDS()
    {
        return columnListExtPointBuilder(PRE_STUDENT_DS)
                .addColumn(checkboxColumn(CHECKBOX_COLUMN))
                .addColumn(textColumn("formativeOrgUnit", PreStudentVO.PRE_STUDENT + "." + PreliminaryEnrollmentStudent.educationOrgUnit().formativeOrgUnit().title().s()))
                .addColumn(textColumn("direction", PreStudentVO.PRE_STUDENT + "." + PreliminaryEnrollmentStudent.educationOrgUnit().title().s()))
                .addColumn(textColumn("fio", PreStudentVO.PRE_STUDENT + "." + PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fullFio().s()))
                .addColumn(textColumn("competitionKind", PreStudentVO.PRE_STUDENT + "." + PreliminaryEnrollmentStudent.requestedEnrollmentDirection().competitionKind().title().s()))
                .addColumn(textColumn("targetAdmissionKind", PreStudentVO.TARGET_ADMISSION_KIND_TITLE))
                .addColumn(textColumn("finalMark", PreStudentVO.FINAL_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS))
                .addColumn(textColumn("profileMark", PreStudentVO.PROFILE_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS))
                .addColumn(textColumn("graduatedProfileEduInstitution", PreStudentVO.GRADUATED_PROFILE_EDU_INSTITUTION_TITLE))
                .addColumn(textColumn("certificateAverageMark", PreStudentVO.CERTIFICATE_AVERAGE_MARK).formatter(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS))
                .addColumn(textColumn("developPeriod", PreStudentVO.PRE_STUDENT + "." + PreliminaryEnrollmentStudent.educationOrgUnit().developPeriod().title().s()))
                .addColumn(booleanColumn("originals", PreStudentVO.PRE_STUDENT + "." + PreliminaryEnrollmentStudent.requestedEnrollmentDirection().originalDocumentHandedIn().s()))
                .addColumn(textColumn("enrollmentConditions", PreStudentVO.PRE_STUDENT + "." + PreliminaryEnrollmentStudent.enrollmentConditions().s()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> preStudentDSHandler()
    {
        return new PreStudentHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> qualificationDSHandler()
    {
        return new QualificationHandler(getName())
                .filter(Qualifications.title())
                .order(Qualifications.order());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new EnrCampaignFormativeOrgUnitDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> territorialOrgUnitDSHandler()
    {
        return new EnrCampaignTerritorialOrgUnitDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> citizenshipDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(PreStudentHandler.RUSSIAN_CITIZENS, PreStudentHandler.NOT_RUSSIAN_CITIZENS));
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orderTypeDSHandler()
    {
        return new EcEnrollmentOrderTypeHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> employeePostDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EmployeePost.class)
                .order(EmployeePost.person().identityCard().fullFio())
                .filter(EmployeePost.person().identityCard().fullFio())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orderReasonDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrollmentOrderReason.class)
                .order(EnrollmentOrderReason.title())
                .filter(EnrollmentOrderReason.title())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orderBasicsDSHandler()
    {
        return new OrderBasicsDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> enrollmentStepDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FefuEnrollmentStep.class)
                        .order(FefuEnrollmentStep.code());
    }
}