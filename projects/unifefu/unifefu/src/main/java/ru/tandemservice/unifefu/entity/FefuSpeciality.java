package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Специальность
 */
public class FefuSpeciality extends FefuSpecialityGen
{
    public FefuSpeciality()
    {

    }

    public FefuSpeciality(EppEduPlanVersionBlock block)
    {
        this.setBlock(block);
    }
}