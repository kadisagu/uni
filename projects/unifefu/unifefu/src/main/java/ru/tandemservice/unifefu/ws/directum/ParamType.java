/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import ru.tandemservice.unifefu.entity.catalog.codes.FefuDirectumOrderTypeCodes;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 16.07.2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Param", propOrder = {})
public class ParamType
{
    public static final String REFERENCE_TYPE = "Reference";
    public static final String BOOLEAN_TYPE = "boolean";
    public static final String STRING_TYPE = "String";
    public static final String PICK_TYPE = "String";//"Pick";

    public static final String SHKOLA_NAME = "Школа";
    public static final String OKD_NAME = "ОКД0";
    public static final String DEIP_NAME = "ДЭиП";
    public static final String DOMO_NAME = "ДОМО";
    public static final String DMP2_NAME = "ДМП2";
    public static final String DMP_NAME = "ДМП";
    public static final String ORPHAN_NAME = "ПоСиротам";
    public static final String PRIKAZ_PO_PRAKTIKAM_NAME = "ПриказПоПрактикам0";
    public static final String POSTAR_NAME = "ПОСТАР0";
    public static final String UVC_NAME = "УВЦ0";
    public static final String FVO_NAME = "ФВО";
    public static final String O_NALOZHENII_VZYSKANIYA_NAME = "О наложении взыскания?";
    public static final String PODRAZDELENIE_NAME = "Подразделение0";
    public static final String MPR_NAME = "МПР";
    public static final String PRIKAZ_PO_VYPLATAM_NAME = "ПриказПоВыплатам";
    public static final String GOS_OBRAZEC_NAME = "ГосОбразец";
    public static final String MEZHDUNARODNYE_OTNOSHENIYA_NAME = "МеждународныеОтношения";
    public static final String PROVERKA_MED_DOKUMENTOV_NAME = "ПроверкаМедДокументов";
    public static final String DIPLOM_S_OTLICHIEM_NAME = "ДипломСОтличием";
    public static final String PODPISYVAET_NAME = "Подписывает0";
    public static final String INITIATOR_TANDEM = "ИнициаторТандем";
    public static final String ORDER_TYPE = "ТипПриказа";

    public static final String REFERENCE_TYPE_DEFAULT_VALUE = "НастройкиПУД";

    @XmlValue
    protected String value;
    @XmlAttribute(name = "Name")
    protected String name;
    @XmlAttribute(name = "Type")
    protected String type;
    @XmlAttribute(name = "ReferenceName")
    protected String referenceName;

    public static final List<ParamType> createSingleTaskParamsList(RouteFormWrapper routeWrapper)
    {
        if (null == routeWrapper || null == routeWrapper.getOrderType()) return null;

        if (FefuDirectumOrderTypeCodes.VPO.equals(routeWrapper.getOrderType().getCode()))
            return createSingleVpoTaskParamsList(routeWrapper);

        if (FefuDirectumOrderTypeCodes.SPO.equals(routeWrapper.getOrderType().getCode()))
            return createSingleSpoTaskParamsList(routeWrapper);

        if (FefuDirectumOrderTypeCodes.DPO.equals(routeWrapper.getOrderType().getCode()))
            return createSingleDpoTaskParamsList(routeWrapper);

        if (FefuDirectumOrderTypeCodes.ENR.equals(routeWrapper.getOrderType().getCode()))
            return createSingleEnrTaskParamsList(routeWrapper);

        return null;
    }

    public static final List<ParamType> createSingleVpoTaskParamsList(RouteFormWrapper routeWrapper)
    {
        List<ParamType> result = new ArrayList<>();
        result.add(createShkolaParam(routeWrapper.getOrgUnitCode()));
        result.add(createOKDParam(routeWrapper.isDismiss(), routeWrapper.isRestore()));
        result.add(createDEiPParam(routeWrapper.isOffBudgetPayments()));
        result.add(createDOMOParam(routeWrapper.isForeignStudent()));
        result.add(createDMP2Param(routeWrapper.isGrantMatAid()));
        //result.add(createDMPParam(routeWrapper.isSocial())); TODO: нужны требования о том, как формировать данный признак
        result.add(createOrphanParam(routeWrapper.isSocial()));
        result.add(createPrikazPoPraktikamParam(routeWrapper.isPractice()));
        result.add(createPOSTARParam(routeWrapper.isGroupManager()));
        result.add(createUVCParam(routeWrapper.isUvcStudents()));
        result.add(createFVOParam(routeWrapper.isFvoStudents()));
        result.add(createONalozheniiVzyskaniyaParam(routeWrapper.isPenalty()));
        result.add(createMPRParam(routeWrapper.isMpr()));
        result.add(createPodpisyvaetParam(routeWrapper.getSigner()));
        result.add(createInitiatorTandemParam(routeWrapper.getExecutor()));
        result.add(createProverkaMedDokumentov(routeWrapper.isCheckMedicalDocs()));
        result.add(createDiplomSOtlichiem(routeWrapper.isDiplomaSuccess()));
        result.add(createOrderTypeParam(routeWrapper.getCommonOrderType()));
        return result;
    }

    public static final List<ParamType> createSingleSpoTaskParamsList(RouteFormWrapper routeWrapper)
    {
        List<ParamType> result = new ArrayList<>();
        result.add(createPodrazdelenieParam(routeWrapper.getOrgUnitTitle()));
        result.add(createPrikazPoVyplatamParam(routeWrapper.isPayments()));
        result.add(createInitiatorTandemParam(routeWrapper.getExecutor()));
        result.add(createOrderTypeParam(routeWrapper.getCommonOrderType()));
        return result;
    }

    public static final List<ParamType> createSingleDpoTaskParamsList(RouteFormWrapper routeWrapper)
    {
        List<ParamType> result = new ArrayList<>();
        result.add(createShkolaParam(routeWrapper.getOrgUnitCode()));
        result.add(createGosObrazecParam(routeWrapper.isStateFormular()));
        result.add(createInitiatorTandemParam(routeWrapper.getExecutor()));
        result.add(createOrderTypeParam(routeWrapper.getCommonOrderType()));
        return result;
    }

    public static final List<ParamType> createSingleEnrTaskParamsList(RouteFormWrapper routeWrapper)
    {
        List<ParamType> result = new ArrayList<>();
        result.add(createShkolaParam(routeWrapper.getOrgUnitCode()));
        result.add(createMezhdunarodnyeOtnosheniyaParam(routeWrapper.isInternational()));
        result.add(createInitiatorTandemParam(routeWrapper.getExecutor()));
        result.add(createOrderTypeParam(routeWrapper.getCommonOrderType()));
        return result;
    }

    public static final ParamType createShkolaParam(String shkolaCode)
    {
        ParamType param = new ParamType();
        //param.setReferenceName(REFERENCE_TYPE_DEFAULT_VALUE);
        //param.setType(REFERENCE_TYPE);
        param.setType(STRING_TYPE);
        param.setName(SHKOLA_NAME);
        param.setValue(shkolaCode);
        return param;
    }

    public static final ParamType createPodrazdelenieParam(String podrazdelenieTitle)
    {
        ParamType param = new ParamType();
        //param.setReferenceName(REFERENCE_TYPE_DEFAULT_VALUE);
        //param.setType(REFERENCE_TYPE);
        param.setType(STRING_TYPE);
        param.setName(PODRAZDELENIE_NAME);
        param.setValue(podrazdelenieTitle);
        return param;
    }

    public static final ParamType createOKDParam(boolean dismiss, boolean restore)
    {
        ParamType param = new ParamType();
        param.setType(PICK_TYPE);
        param.setName(OKD_NAME);
        if (dismiss) param.setValue("Приказ на отчисление с связи с окон.обуч.(выпуск)");
        else if (restore) param.setValue("Приказ на восстановление студентов");
        else param.setValue("Нет");
        return param;
    }

    public static final ParamType createDEiPParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(DEIP_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createDOMOParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(DOMO_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createDMP2Param(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(DMP2_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createDMPParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(PICK_TYPE);
        param.setName(DMP_NAME);
        param.setValue(value ? "Да" : "Нет");
        return param;
    }

    public static final ParamType createOrphanParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(ORPHAN_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createPrikazPoPraktikamParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(PICK_TYPE);
        param.setName(PRIKAZ_PO_PRAKTIKAM_NAME);
        param.setValue(value ? "Да" : "Нет");
        return param;
    }

    public static final ParamType createPOSTARParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(PICK_TYPE);
        param.setName(POSTAR_NAME);
        param.setValue(value ? "Да" : "Нет");
        return param;
    }

    public static final ParamType createUVCParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(PICK_TYPE);
        param.setName(UVC_NAME);
        param.setValue(value ? "Да" : "Нет");
        return param;
    }

    public static final ParamType createFVOParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(FVO_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createONalozheniiVzyskaniyaParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(O_NALOZHENII_VZYSKANIYA_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createMPRParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(MPR_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createPrikazPoVyplatamParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(PRIKAZ_PO_VYPLATAM_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createGosObrazecParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(GOS_OBRAZEC_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createMezhdunarodnyeOtnosheniyaParam(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(MEZHDUNARODNYE_OTNOSHENIYA_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createProverkaMedDokumentov(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(PROVERKA_MED_DOKUMENTOV_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createDiplomSOtlichiem(boolean value)
    {
        ParamType param = new ParamType();
        param.setType(BOOLEAN_TYPE);
        param.setName(DIPLOM_S_OTLICHIEM_NAME);
        param.setValue(String.valueOf(value));
        return param;
    }

    public static final ParamType createPodpisyvaetParam(String value)
    {
        ParamType param = new ParamType();
        param.setType(PICK_TYPE);
        param.setName(PODPISYVAET_NAME);
        param.setValue(value);
        return param;
    }

    public static final ParamType createInitiatorTandemParam(String value)
    {
        ParamType param = new ParamType();
        param.setType(STRING_TYPE);
        param.setName(INITIATOR_TANDEM);
        param.setValue(value);
        return param;
    }

    public static final ParamType createOrderTypeParam(String value)
    {
        ParamType param = new ParamType();
        param.setType(STRING_TYPE);
        param.setName(ORDER_TYPE);
        param.setValue(value);
        return param;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getReferenceName()
    {
        return referenceName;
    }

    public void setReferenceName(String referenceName)
    {
        this.referenceName = referenceName;
    }
}