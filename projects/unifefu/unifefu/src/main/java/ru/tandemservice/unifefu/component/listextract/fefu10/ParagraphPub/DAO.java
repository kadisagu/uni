/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu10.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract;

/**
 * @author nvankov
 * @since 11/21/13
 */
public class DAO extends AbstractListParagraphPubDAO<FullStateMaintenanceEnrollmentStuListExtract, Model> implements IDAO
{
}
