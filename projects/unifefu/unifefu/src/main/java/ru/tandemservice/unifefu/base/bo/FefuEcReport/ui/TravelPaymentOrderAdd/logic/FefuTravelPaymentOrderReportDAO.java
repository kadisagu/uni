/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.TravelPaymentOrderAdd.logic;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 04.09.2013
 */
public class FefuTravelPaymentOrderReportDAO extends UniBaseDao implements IFefuTravelPaymentOrderReportDAO
{

    private final static String PRIMORSKIY_KRAY = "25"; // Код субъекта РФ для приморского края
    private final static List<String> NATURAL_DISCIPLINES = Arrays.asList("ИНФОРМАТИКА", "ФИЗИКА", "БИОЛОГИЯ", "ХИМИЯ", "ГЕОГРАФИЯ");


    @Override
    public byte[] buildReport(EnrollmentCampaign campaign,
                              List<EntrantEnrollmentOrderType> orderTypeList,
                              Collection<EnrollmentOrder> orderList,
                              Double minSumMark3,
                              Double minSumMark4,
                              Double minMark,
                              boolean includeForeignPerson)
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "fefuTravelPaymentOrderReport");
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        if (CollectionUtils.isEmpty(orderList))
            orderList = getOrderList(campaign, orderTypeList);

        // Грузим список абитуриентов
        List<EntrantVO> entrantVOList = getEntrants(orderList, minSumMark3, minSumMark4, minMark, includeForeignPerson);

        // только приказы с абитуриента попвшими в выборку
        orderList = getUsedOrders(entrantVOList.stream().map(entrant -> entrant.redId).collect(Collectors.toList()));

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("orders", getOrderListString(orderList));
        injectModifier.put("paragraph1", getPar1(entrantVOList, minSumMark3, minSumMark4));
        injectModifier.put("paragraph2", getPar2(entrantVOList, minSumMark3, minSumMark4));
        injectModifier.put("paragraph3", getPar3(entrantVOList, minSumMark3, minSumMark4));
        injectModifier.put("eduYear", campaign.getEducationYear().getTitle());
        injectModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }

    private List<EnrollmentOrder> getOrderList(EnrollmentCampaign enrollmentCampaign, List<EntrantEnrollmentOrderType> entrantEnrollmentOrderTypeList)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrollmentOrder.class, "e")
                .column("e")
                .where(eq(property(EnrollmentOrder.enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)))
                .where(eq(property("e", EnrollmentOrder.state().code()), value(OrderStatesCodes.FINISHED)))
                .order(property(EnrollmentOrder.commitDate().fromAlias("e")));

        CommonBaseFilterUtil.applySelectFilter(builder, "e", EnrollmentOrder.type().s(), entrantEnrollmentOrderTypeList);

        return getList(builder);
    }

    private String getOrderListString(Collection<EnrollmentOrder> enrollmentOrders)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return enrollmentOrders.stream()
                .map(order -> (order.getCommitDate() != null ? "от " + dateFormat.format(order.getCommitDate()) + ", " : "") + "№ " + order.getNumber())
                .collect(Collectors.joining(", "));
    }

    private List<EntrantVO> getEntrants(Collection<EnrollmentOrder> orderList,
                                        Double minSumMark3,
                                        Double minSumMark4,
                                        Double minMark,
                                        boolean includeForeignPerson)
    {
        // Набор лайков для поиска дисциплин по названию
        IDQLExpression[] disciplinesLikes = new IDQLExpression[NATURAL_DISCIPLINES.size()];
        int idx = 0;
        for (String discipline : NATURAL_DISCIPLINES)
        {
            disciplinesLikes[idx++] = likeUpper(
                    property("d", ChosenEntranceDiscipline.enrollmentCampaignDiscipline().educationSubject().title()),
                    value("%" + discipline.toUpperCase() + "%")
            );
        }

        // Подзапрос получения ВНП, с оценкой по естественнонаучной дисциплине >= minMark
        DQLSelectBuilder naturalSubQuery = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, "d")
                .column(property("d", ChosenEntranceDiscipline.chosenEnrollmentDirection().id()))
                .where(ge(property("d", ChosenEntranceDiscipline.finalMark()), value(minMark)))
                .where(or(disciplinesLikes));


        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "extract")
                .joinPath(DQLJoinType.inner, EnrollmentExtract.entity().requestedEnrollmentDirection().fromAlias("extract"), "red")
                .joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().entrant().person().identityCard().fromAlias("red"), "card")
                .joinPath(DQLJoinType.inner, EnrollmentExtract.entity().educationOrgUnit().fromAlias("extract"), "ou")
                .joinEntity("card", DQLJoinType.left, AddressRu.class, "addr", eq(property("card", IdentityCard.L_ADDRESS), property("addr")))
                .joinPath(DQLJoinType.left, AddressRu.settlement().fromAlias("addr"), "city")

                // [0] ФИО
                .column(property("card", IdentityCard.fullFio()))
                // [1] вид конкурса (код элемента справочника)
                .column(property("red", RequestedEnrollmentDirection.competitionKind().code()))
                // [2] код субъекта РФ
                .column(property("city", AddressItem.inheritedRegionCode()))
                // [3] код вида затрат
                .column(property("extract", EnrollmentExtract.entity().compensationType().code()))
                // [4] сумма баллов
                .column(DQLFunctions.sum(property("disc", ChosenEntranceDiscipline.finalMark())))
                // [5] НПП
                .column(property("ou", EducationOrgUnit.id()))
                // [6] количество экзаменов
                .column(DQLFunctions.count(DQLPredicateType.distinct, property("disc")))
                // [7] RequestedEnrollmentDirection
                .column(property("red", RequestedEnrollmentDirection.id()))

                .joinEntity("red", DQLJoinType.left, ChosenEntranceDiscipline.class, "disc",
                            eq(
                                    property(RequestedEnrollmentDirection.id().fromAlias("red")),
                                    property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("disc"))
                            ))

                .group(property("red", RequestedEnrollmentDirection.id()))
                .group(property("red", RequestedEnrollmentDirection.competitionKind().code()))
                .group(property("city", AddressItem.inheritedRegionCode()))
                .group(property("card", IdentityCard.fullFio()))
                .group(property("extract", EnrollmentExtract.entity().compensationType().code()))
                .group(property("ou", EducationOrgUnit.id()))
                .having(
                        or(
                                // либо набрали >= minSumMark3 баллов (при 3 или меньше сданных экзаменах), либо >= minSumMark4 баллов (при 4 и больше сданных экзаменах)
                                ge(DQLFunctions.sum(property("disc", ChosenEntranceDiscipline.finalMark())),
                                   new DQLCaseExpressionBuilder()
                                           .when(le(DQLFunctions.count(DQLPredicateType.distinct, property("disc")), value(3)), value(minSumMark3))
                                           .otherwise(value(minSumMark4))
                                           .build()
                                ),

                                // либо без экзаменов прошли
                                eq(property("red", RequestedEnrollmentDirection.competitionKind().code()), value(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES)),
                                // либо по какой-либо естественнонаучной дисциплине >= 70 баллов
                                in(property("red", RequestedEnrollmentDirection.id()), naturalSubQuery.buildQuery())
                        )
                )
                // нужны только зачисленные
                .where(eq(property("red", RequestedEnrollmentDirection.state().code()), value(UniecDefines.ENTRANT_STATE_ENROLED_CODE)))
                // только бакалавры и специалисты
                .where(in(property("ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().code()), Arrays.asList(QualificationsCodes.BAKALAVR, QualificationsCodes.SPETSIALIST)))
                // только очники
                .where(eq(property("ou", EducationOrgUnit.developForm().code()), value(DevelopFormCodes.FULL_TIME_FORM)))
                // только из проведенных приказов в выбранной приемной кампании
                .where(in(property("extract", EnrollmentExtract.paragraph().order()), orderList))
                .order(property("card", IdentityCard.fullFio()));

        if (!includeForeignPerson)                        // только граждане РФ
            builder.where(eq(property("card", IdentityCard.citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));

        return this.<Object[]>getList(builder).stream()
                .map(row -> {
                    EntrantVO entrantVO = new EntrantVO();
                    entrantVO.fio = (String) row[0];
                    entrantVO.withoutDisciplines = UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(row[1]);
                    entrantVO.notFromPrimorie = !PRIMORSKIY_KRAY.equals(row[2]);
                    entrantVO.isBudget = UniDefines.COMPENSATION_TYPE_BUDGET.equals(row[3]);
                    entrantVO.finalSum = (Double) row[4];
                    entrantVO.eduOrgUnit = DataAccessServices.dao().getNotNull((Long) row[5]);
                    entrantVO.examCount = (Long) row[6];
                    entrantVO.redId = (Long) row[7];
                    return entrantVO;
                })
                .collect(Collectors.toList());
    }

    private Collection<EnrollmentOrder> getUsedOrders(List<Long> redIds)
    {
        Set<EnrollmentOrder> orders = new HashSet<>();
        String alias = "ext";
        for (List<Long> part : Lists.partition(redIds, DQL.MAX_VALUES_ROW_NUMBER))
        {
            orders.addAll(
                    getList(new DQLSelectBuilder()
                                    .fromEntity(EnrollmentExtract.class, alias)
                                    .column(property(alias, EnrollmentExtract.paragraph().order()))
                                    .where(in(property(alias, EnrollmentExtract.entity().requestedEnrollmentDirection().id()), part))
                    ));
        }
        return orders;
    }

    static String getLevelTypeStr(StructureEducationLevels levelType)
    {
        if (levelType.isSpecialization() || (levelType.isProfile() && (levelType.isMaster() || levelType.isBachelor())))
        {
            if (levelType.isSpecialization()) return "Специализация";
            if (levelType.isMaster()) return "Магистерская программа";
            return "Бакалаврский профиль";
        }
        return levelType.isSpecialty() ? "Специальность" : "Направление";
    }

    private RtfString createPar(List<EntrantVO> entrantList)
    {
        RtfString rtf = new RtfString();

        // Группируем по территориальному подразделению, в рамках территорального по формирующему, в рамках формирующего по направлению подготовки
        final Long vladivostok = OrgUnitManager.instance().dao().getTopOrgUnitId();
        Map<OrgUnit, List<EntrantVO>> territorialMap = entrantList.stream()
                .collect(Collectors.groupingBy(item -> item.eduOrgUnit.getTerritorialOrgUnit(),
                                               () -> new TreeMap<>((o1, o2) -> {
                                                   if (o1.getId().equals(vladivostok))
                                                   {
                                                       if (o2.getId().equals(vladivostok))
                                                           return o1.getTitle().compareTo(o2.getTitle());
                                                       return -1;
                                                   }
                                                   if (o2.getId().equals(vladivostok))
                                                       return 1;
                                                   return o1.getTitle().compareTo(o2.getTitle());
                                               }),
                                               Collectors.mapping(item -> item, Collectors.toList())));

        for (Iterator<OrgUnit> i0 = territorialMap.keySet().iterator(); i0.hasNext(); )
        {
            OrgUnit territorialOrgUnit = i0.next();
            if (!territorialOrgUnit.isTop())
                rtf.boldBegin().append(territorialOrgUnit.getTitle()).boldEnd().par();

            // Группируем по формирующему
            Map<OrgUnit, List<EntrantVO>> formativeMap = territorialMap.get(territorialOrgUnit).stream()
                    .collect(Collectors.groupingBy(item -> item.eduOrgUnit.getFormativeOrgUnit(),
                                                   () -> new TreeMap<>((o1, o2) -> o1.getTitle().compareTo(o2.getTitle())),
                                                   Collectors.mapping(item -> item, Collectors.toList())));

            addFormativeOrgUnitPart(rtf, formativeMap, territorialOrgUnit.getId());

            if (i0.hasNext())
                rtf.par();
        }

        return rtf;
    }

    private void addFormativeOrgUnitPart(RtfString rtf, Map<OrgUnit, List<EntrantVO>> map, Long territorialOrgUnitId)
    {
        boolean firstFormative = true;
        Iterator<Map.Entry<OrgUnit, List<EntrantVO>>> iterator = map.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<OrgUnit, List<EntrantVO>> entry = iterator.next();
            OrgUnit formativeOrgUnit = entry.getKey();
            if (!firstFormative || !formativeOrgUnit.getId().equals(territorialOrgUnitId))
                rtf.boldBegin().append(formativeOrgUnit.getTitle()).boldEnd().par();

            firstFormative = false;
            // Группируем по направлению
            Map<EducationOrgUnit, List<EntrantVO>> dirMap = entry.getValue().stream()
                    .collect(Collectors.groupingBy(item -> item.eduOrgUnit,
                                                   () -> new TreeMap<>((o1, o2) -> o1.getEducationLevelHighSchool().getTitle()
                                                           .compareTo(o2.getEducationLevelHighSchool().getTitle())),
                                                   Collectors.mapping(item -> item, Collectors.toList())));

            addEducationOrgUnitPart(rtf, dirMap);

            if (iterator.hasNext())
                rtf.par();
        }
    }

    private void addEducationOrgUnitPart(RtfString rtf, Map<EducationOrgUnit, List<EntrantVO>> map)
    {
        Iterator<Map.Entry<EducationOrgUnit, List<EntrantVO>>> iterator = map.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<EducationOrgUnit, List<EntrantVO>> entry = iterator.next();
            EducationOrgUnit educationOrgUnit = entry.getKey();
            rtf.append(getLevelTypeStr(educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getLevelType()));
            rtf.append(" ")
                    .boldBegin()
                    .append(StringUtils.uncapitalize(educationOrgUnit.getEducationLevelHighSchool().getTitle()))
                    .boldEnd().par();

            int idx = 1;
            for (EntrantVO item : entry.getValue())
            {
                rtf.append("      ").append("" + idx++).append("    ");
                rtf.append(item.fio);
                if (item.withoutDisciplines || item.finalSum != null)
                {
                    rtf.append(" (");
                    if (item.withoutDisciplines)
                        rtf.append("без экзаменов");
                    else
                    {
                        rtf.append(CommonBaseStringUtil.numberWithPostfixCase(item.finalSum.longValue(), "балл", "балла", "баллов"));
                    }
                    rtf.append(")");
                }
                rtf.par();
            }
            if (iterator.hasNext())
                rtf.par();
        }
    }

    /**
     * В первом параграфе идут те, кто поступил на бюджет с суммой баллов больше необходимого для повышенной стипендии либо без экзаменов.
     */
    private RtfString getPar1(List<EntrantVO> entrantVOList, Double minSumMark3, Double minSumMark4)
    {
        List<EntrantVO> parList = entrantVOList.stream()
                .filter(entrantVO -> entrantVO.isBudget)
                .filter(entrantVO -> entrantVO.withoutDisciplines ||
                        (entrantVO.finalSum != null && entrantVO.isFinalSumGreat(minSumMark3, minSumMark4)))
                .collect(Collectors.toList());
        return createPar(parList);
    }

    /**
     * Во втором параграфе идут те, кто поступил на бюджет и набрал меньше необходимого для повышенной стипендии
     * но по какой-либо естественнонаучной дисциплине получил больше необходимого
     */
    private RtfString getPar2(List<EntrantVO> entrantVOList, Double minSumMark3, Double minSumMark4)
    {
        List<EntrantVO> parList = entrantVOList.stream()
                .filter(entrantVO -> entrantVO.isBudget)
                .filter(entrantVO -> entrantVO.finalSum != null && !entrantVO.isFinalSumGreat(minSumMark3, minSumMark4))
                .collect(Collectors.toList());
        return createPar(parList);
    }

    /**
     * В третьем параграфе идут зачисленные на бюджет и контракт, не из Приморского края,
     * с суммой баллов больше необходимого для повышенной стипендии либо без экзаменов.
     */
    private RtfString getPar3(List<EntrantVO> entrantVOList, Double minSumMark3, Double minSumMark4)
    {
        List<EntrantVO> parList = entrantVOList.stream()
                .filter(entrantVO -> entrantVO.notFromPrimorie)
                .filter(entrantVO -> entrantVO.withoutDisciplines || (entrantVO.finalSum != null && entrantVO.isFinalSumGreat(minSumMark3, minSumMark4)))
                .collect(Collectors.toList());
        return createPar(parList);
    }

    private class EntrantVO
    {
        String fio;
        Double finalSum;
        EducationOrgUnit eduOrgUnit;
        boolean isBudget, notFromPrimorie, withoutDisciplines;
        Long examCount;
        Long redId;

        boolean isFinalSumGreat(Double minSumMark3, Double minSumMark4)
        {
            if (examCount > 3)
                return finalSum >= minSumMark4;
            else
                return finalSum >= minSumMark3;
        }
    }
}