/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;


import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 24.07.2013
 */
public class OrderFastCreatorFiltersVO
{
    private List<Qualifications> qualificationList;
    private DevelopForm developForm;
    private DevelopCondition developCondition;
    private List<DevelopTech> developTechList;
    private List<OrgUnit> formativeOrgUnitList;
    private OrgUnit territorialOrgUnit;
    private DataWrapper citizenship;

    public List<Qualifications> getQualificationList()
    {
        return qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        this.qualificationList = qualificationList;
    }

    public DevelopForm getDevelopForm()
    {
        return developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        this.developForm = developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        this.developCondition = developCondition;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        this.developTechList = developTechList;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        this.territorialOrgUnit = territorialOrgUnit;
    }

    public DataWrapper getCitizenship()
    {
        return citizenship;
    }

    public void setCitizenship(DataWrapper citizenship)
    {
        this.citizenship = citizenship;
    }
}