package ru.tandemservice.unifefu.dao.daemon;

import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class FEFUPersonGuidDaemonDao extends UniBaseDao implements IFEFUPersonGuidDaemonDao
{
    public static final SyncDaemon DAEMON = new SyncDaemon(FEFUPersonGuidDaemonDao.class.getName(), 120, IFEFUPersonGuidDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
//            final IFEFUPersonGuidDaemonDao dao = IFEFUPersonGuidDaemonDao.instance.get();
//
//            try
//            {
//                dao.doActualizePersonGuidsTable();
//            } catch (final Throwable t)
//            {
//                Debug.exception(t.getMessage(), t);
//                this.logger.warn(t.getMessage(), t);
//            }

        }
    };

    protected Session lock4update()
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, IFEFUPersonGuidDaemonDao.GLOBAL_DAEMON_LOCK);
        return session;
    }

    @Override
    public boolean doActualizePersonGuidsTable()
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(FefuNsiIds.class, "ids").column(property(FefuNsiIds.entityId().fromAlias("ids")))
                .where(eq(property(FefuNsiIds.entityType().fromAlias("ids")), value("person")));

        DQLSelectBuilder studentPersonBuilder = new DQLSelectBuilder()
                .fromEntity(Student.class, "s").column(property(Student.person().id().fromAlias("s")))
                .where(notIn(property(Student.person().id().fromAlias("s")), subBuilder.buildQuery()));

        List<Long> personIds = studentPersonBuilder.createStatement(getSession()).list();

        DQLSelectBuilder entrantPersonBuilder = new DQLSelectBuilder()
                .fromEntity(Entrant.class, "e").column(property(Entrant.person().id().fromAlias("e")))
                .where(notIn(property(Entrant.person().id().fromAlias("e")), subBuilder.buildQuery()));

        personIds.addAll(entrantPersonBuilder.createStatement(getSession()).<Long>list());


        Set addedPersonIds = new HashSet();
        for (Long personId : personIds)
        {
            if (addedPersonIds.contains(personId)) continue; // пропускаем повторы

            addedPersonIds.add(personId);
            String guid = UUID.nameUUIDFromBytes(String.valueOf(personId).getBytes()).toString();
            FefuNsiIds fefuNsiIds = new FefuNsiIds();
            fefuNsiIds.setEntityId(personId);
            fefuNsiIds.setEntityType("person");
            fefuNsiIds.setHasFormFilled(false);
            fefuNsiIds.setGuid(guid);
            save(fefuNsiIds);
        }

        return personIds.size() > 0;
    }

    @Override
    public FefuNsiIds doActualizePersonGuid(Long personId)
    {
        List<FefuNsiIds> actualPersonGuidList = new DQLSelectBuilder()
                .fromEntity(FefuNsiIds.class, "ids").column("ids")
                .where(eq(property(FefuNsiIds.entityId().fromAlias("ids")), value(personId)))
                .createStatement(getSession()).list();

        if (actualPersonGuidList.size() == 0)
        {
            String guid = UUID.nameUUIDFromBytes(String.valueOf(personId).getBytes()).toString();
            FefuNsiIds fefuNsiIds = new FefuNsiIds();
            fefuNsiIds.setEntityId(personId);
            fefuNsiIds.setEntityType("person");
            fefuNsiIds.setHasFormFilled(false);
            fefuNsiIds.setGuid(guid);
            save(fefuNsiIds);

            return fefuNsiIds;
        }
        return actualPersonGuidList.get(0);
    }

    @Override
    public FefuNsiIds doRegisterFictiveGuid(String guid)
    {
        if (null == guid) return null;
        FefuNsiIds fictiveGuid = new FefuNsiIds();
        fictiveGuid.setEntityId(System.currentTimeMillis());
        fictiveGuid.setEntityType("person");
        fictiveGuid.setGuid(guid);
        getSession().save(fictiveGuid);
        return fictiveGuid;
    }
}